CREATE OR REPLACE PACKAGE BODY FUTURE_COST_SQL AS
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Package Variables
----------------------------------------------------------------------------------------
LP_vdate   DATE                     := get_vdate;
LP_action  COST_EVENT.ACTION%TYPE   := NULL;
LP_call    VARCHAR2(4)              := NULL;
LP_user    VARCHAR2(30) := get_user;
----------------------------------------------------------------------------------------
-- Private Functions
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
-- Function Name: REFRESH_DATA_STRUCTURES
-- Purpose      : Removes any data from the future cost global temporary tables .
----------------------------------------------------------------------------------------
FUNCTION REFRESH_DATA_STRUCTURES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- Function Name: LOCK_FUTURE_COST
-- Purpose      : Locks future cost table records.
----------------------------------------------------------------------------------------
FUNCTION LOCK_FUTURE_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------------------
-- alter session set plsql_ccflags = 'UTPLSQL:TRUE'
$if $$UTPLSQL=FALSE or $$UTPLSQL is NULL $then
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_NIL
   -- Purpose      : Adds new item/location records into future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_NIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_NIL_CC
   -- Purpose      : Adds future cost changes for new item/location.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_NIL_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_NIL_RECLASS
   -- Purpose      : Adds future reclass records for new item/location.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_NIL_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_DEAL
   -- Purpose      : Adds all applicable deal records for new item/location.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_COST_CHANGE
   -- Purpose      : Calls appropriate function to process cost change based on the action type.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_action                IN     COST_EVENT.ACTION%TYPE,
                                I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: REMOVE_CC
   -- Purpose      : Removes cost changes from the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION REMOVE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ADD_CC
   -- Purpose      : Adds cost changes to the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION ADD_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_CC
   -- Purpose      : Populate future_cost_gtt with future cost records related to the cost
   --                change on item/supplier/country and item/supplier/country/location.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_CC
   -- Purpose      : Find future cost event records on the timeline on the same date or prior
   --                to the cost change. Records on the same date as the cost change will have
   --                their cost values updated with the values on the cost change. If only
   --                prior records exists, then insert a new row for the cost change.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_RECLASS
   -- Purpose      : Calls appropriate function to process reclass based on the action type.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                            I_action                IN     COST_EVENT.ACTION%TYPE,
                            I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ADD_RECLASS
   -- Purpose      : Insert rows into future cost records affected by reclass.
   ----------------------------------------------------------------------------------------
   FUNCTION ADD_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: REMOVE_RECLASS
   -- Purpose      : Remove reclass records from future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION REMOVE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_RECLASS
   -- Purpose      : Insert a row for the reclass on the timeline or if a row exists on the
   --                same date as the reclass, update the row with the reclass number and
   --                the new merch hier.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ELC_DATE_TO_FC
   -- Purpose      : Insert a row for the ELC change on the timeline that is vdate, or if a
   --                row exists on the same date then do not insert a row.
   ----------------------------------------------------------------------------------------
   FUNCTION ELC_DATE_TO_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_MERGE_MERCH_HIER
   -- Purpose      : Explodes merchandise hierarchy changes to all affected
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_MERGE_MERCH_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_MERGE_ORG_HIER
   -- Purpose      : Explodes organization hierarchy changes to all affected
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_MERGE_ORG_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_MERGE_SUPP_HIER
   -- Purpose      : Explodes supplier hierarchy changes to all affected
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_MERGE_SUPP_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_ELC
   -- Purpose      : Explodes ELC to all affected item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_ELC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_COST_ZONE
   -- Purpose      : Explodes cost zone changes to all affected item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_COST_ZONE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_ITEM_CZG
   -- Purpose      : Explodes item's cost zone group changes to all affected
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_ITEM_CZG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_DEAL
   -- Purpose      : Calls appropriate function to process deal-effect based on the action type.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_action                IN     COST_EVENT.ACTION%TYPE,
                         I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: EXPLODE_PRIMARY_PACK_COST
   -- Purpose      : Explodes primary pack cost changes to all affected
   --                item/location/supplier/origin country.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_PRIMARY_PACK_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                      I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: REMOVE_DEAL
   -- Purpose      : Removes deals from the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION REMOVE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: ADD_DEAL
   -- Purpose      : Adds deals to the future cost table.
   ----------------------------------------------------------------------------------------
   FUNCTION ADD_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MODIFY_DEAL
   -- Purpose      : Modifies deals close date on future cost tables.
   ----------------------------------------------------------------------------------------
   FUNCTION MODIFY_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: DEAL_DATES_TO_FC
   -- Purpose      : Adds rows into future cost for the start and end (day after end) of
   --                all the deals affected by the current deal for roll forward to process.
   ----------------------------------------------------------------------------------------
   FUNCTION DEAL_DATES_TO_FC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: LOAD_DEALS
   -- Purpose      : Moves existing deal records to the deal gtt.
   ----------------------------------------------------------------------------------------
   FUNCTION LOAD_DEALS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: LOAD_FUTURE_COST_FOR_DEALS
   -- Purpose      : Gets future cost records covered by the deal being modified/removed
   --                into the future cost global temporary table.
   ----------------------------------------------------------------------------------------
   FUNCTION LOAD_FUTURE_COST_FOR_DEALS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: SYNC_PRIMARY_PACK
   -- Purpose      : Create future_cost_gtt rows for items that have primary costing packs
   --                based off of the primary costing pack rows.
   ----------------------------------------------------------------------------------------
   FUNCTION SYNC_PRIMARY_PACK(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                             I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: PROCESS_SUPP_CNTR
   -- Purpose      : Add/remove supplier/country records into future cost and future cost deal.
   ----------------------------------------------------------------------------------------
   FUNCTION PROCESS_SUPP_CNTR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_action                IN     COST_EVENT.ACTION%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------
   -- Function Name : EXPLODE_DEAL_PASSTHRU
   -- Purpose       : Calls appropriate function to process deal_passthru for the affected company stores.
   -------------------------------------------------------------------------------------------------------
   FUNCTION EXPLODE_DEAL_PASSTHRU(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                  I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------
   -- Function Name : EXPLODE_TMPL_CHG
   -- Purpose       : Calls appropriate function to process template changes for the affected company stores.
   -------------------------------------------------------------------------------------------------------
   FUNCTION EXPLODE_TMPL_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------
   -- Function Name : EXPLODE_TMP_RELATIONSHIP_CHG
   -- Purpose       : Calls appropriate function to process template relationship changes based on the action type
   --                 for the company stores.
   -----------------------------------------------------------------------------------------------------------------
   FUNCTION EXPLODE_TMP_RELATIONSHIP_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                          I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                          I_action                IN     COST_EVENT.ACTION%TYPE,
                                          I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   -------------------------------------------------------------------------------------------------------
   -- Function Name : COMPUTE_CC_TAX
   -- Purpose       : Calls the tax engine to compute negotiated_item_cost, extended_base_cost and wac_tax
   --                 values for cost change related events.
   -----------------------------------------------------------------------------------------------------------------
   FUNCTION COMPUTE_CC_TAX(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   -----------------------------------------------------------------------------------------------------------------

   -- Function Name : EXPLODE_FC
   -- Purpose       : Explodes item/supp/county/loc for the change of costing location of a franchise store
   -------------------------------------------------------------------------------------------------------                           
   FUNCTION EXPLODE_FC(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;   
   ------------------------------------------------------------------------------------------------------- 

   FUNCTION MERGE_FC     (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name : WRITE_ERROR
   -- Purpose       : Writes error messages to the cost_event_result table.
   ----------------------------------------------------------------------------------------

   PROCEDURE WRITE_ERROR(I_error_message         IN RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE);
   ----------------------------------------------------------------------------------------
   -- Procedure Name: WRITE_SUCCESS
   -- Purpose       : Writes success messages to the cost_event_result table.
   ----------------------------------------------------------------------------------------
   PROCEDURE WRITE_SUCCESS(I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE);
   ----------------------------------------------------------------------------------------
   -- Function Name: GET_CURRENT_ELC
   -- Purpose      : This function gets the current elc records of the cost event.
   ----------------------------------------------------------------------------------------
   FUNCTION GET_CURRENT_ELC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                            I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: CALC_FUTURE_ELC
   -- Purpose      : This function calculates the new elc and hts records.
   ----------------------------------------------------------------------------------------
   FUNCTION CALC_FUTURE_ELC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: DEAL_THLD_CHG_DATE_TO_FC
   -- Purpose      : Insert a row on the vdate date when deal threshold is edited, or if a
   --                row exists on the same date then do not insert a row.
   ----------------------------------------------------------------------------------------
   FUNCTION DEAL_THLD_CHG_DATE_TO_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;

   ----------------------------------------------------------------------------------------
   --Function Name: RECALC_ELC
   --Purpose      : Function to calculate all item expenses and assessments.
   ----------------------------------------------------------------------------------------
   FUNCTION RECALC_ELC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_est_value           IN OUT   NUMBER,
                       I_dtl_flag            IN       VARCHAR2,
                       I_comp_id             IN       ELC_COMP.COMP_ID%TYPE,
                       I_calc_type           IN       VARCHAR2,
                       I_item                IN       ITEM_MASTER.ITEM%TYPE,
                       I_supplier            IN       SUPS.SUPPLIER%TYPE,
                       I_item_exp_type       IN       ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                       I_item_exp_seq        IN       ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                       I_hts                 IN       HTS.HTS%TYPE,
                       I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                       I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                       I_effect_from         IN       HTS.EFFECT_FROM%TYPE,
                       I_effect_to           IN       HTS.EFFECT_TO%TYPE,
                       I_new_cost            IN       FUTURE_COST.BASE_COST%TYPE DEFAULT NULL,
                       I_active_date         IN       DATE DEFAULT NULL,
                       I_location            IN       NUMBER DEFAULT NULL)
      RETURN BOOLEAN;

   -----------------------------------------------------------------------------------------
   --Function Name: UPDATE_FC_TARIFF_RATES
   --Purpose      : This function updates the tariff rate of the item.
   -----------------------------------------------------------------------------------------
   FUNCTION UPDATE_FC_TARIFF_RATES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_calc_type           IN       VARCHAR2,
                                   I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_seq_no              IN       ORDSKU_HTS.SEQ_NO%TYPE,
                                   I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                   I_pack_item           IN       ITEM_MASTER.ITEM%TYPE,
                                   I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                   I_hts                 IN       HTS.HTS%TYPE,
                                   I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                                   I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                                   I_effect_from         IN       HTS.EFFECT_FROM%TYPE,
                                   I_effect_to           IN       HTS.EFFECT_TO%TYPE,
                                   I_comp_id             IN       ELC_COMP.COMP_ID%TYPE)
      RETURN BOOLEAN;
   ------------------------------------------------------------------------------------------------------- 
   -- Function Name : PROCESS_PRICE_CHG 
   -- Purpose       : Calls appropriate function to process price changes for the affected franchise stores
   --                 if a valid retail template is active during the same period
   -------------------------------------------------------------------------------------------------------                           
   FUNCTION PROCESS_PRICE_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;   
   ----------------------------------------------------------------------------------------    
   -- Function Name: EXPLODE_PC
   -- Purpose      : Populates future_cost_temp with future cost records related to the price 
   --                change for franchice stores whose pricing cost will be recalculated.
   ----------------------------------------------------------------------------------------
   FUNCTION EXPLODE_PC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
   -- Function Name: MERGE_PC
   -- Purpose      : Find future cost event records on the timeline on the same date or prior
   --                to the price change of franchise stores. Records on the same date as the 
   --                price change will have their pricing cost values updated  if a retail 
   --                based template is active during that time frame.
   ----------------------------------------------------------------------------------------
   FUNCTION MERGE_PC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
   ----------------------------------------------------------------------------------------
$end

FUNCTION REFRESH_DATA_STRUCTURES(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   --make sure we start with empty temp tables and end with empty temp tables
   delete from future_cost_gtt;
   delete from deal_item_loc_explode_gtt;
   delete from future_cost_working_gtt;
   delete from future_cost_buyget_help_gtt;
   delete from future_cost_comp_gtt;
   delete from gtt_fc_item_hts_assess;
   delete from gtt_fc_item_exp_detail;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.REFRESH_DATA_STRUCTURES',
                                            to_char(SQLCODE));
      return FALSE;
END REFRESH_DATA_STRUCTURES;
----------------------------------------------------------------------------------------
FUNCTION LOCK_FUTURE_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(100) := 'FUTURE_COST';
   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.LOCK_FUTURE_COST';
   L_return        NUMBER(1);

   --lock the rows from explode
   cursor C_LOCK_FUTURE_COST0 is
      select fc.rowid
        from future_cost fc,
             future_cost_gtt gtt
       where gtt.item              = fc.item
         and gtt.supplier          = fc.supplier
         and gtt.origin_country_id = fc.origin_country_id
         and gtt.location          = fc.location
         for update of fc.item nowait;

   --have item lock pack
   cursor C_LOCK_FUTURE_COST1 is
      select fc.rowid
        from future_cost fc,
             future_cost_gtt gtt
       where gtt.primary_cost_pack is not null
         and gtt.primary_cost_pack = fc.item
         and gtt.supplier          = fc.supplier
         and gtt.origin_country_id = fc.origin_country_id
         and gtt.location          = fc.location
         for update of fc.item nowait;

   --have pack lock item
   cursor C_LOCK_FUTURE_COST2 is
      select fc.rowid
        from future_cost fc,
             future_cost_gtt gtt
       where gtt.simple_pack_ind = 'Y'
         and gtt.item              = fc.primary_cost_pack
         and gtt.supplier          = fc.supplier
         and gtt.origin_country_id = fc.origin_country_id
         and gtt.location          = fc.location
         for update of fc.item nowait;

BEGIN

   open C_LOCK_FUTURE_COST0;
   close C_LOCK_FUTURE_COST0;

   open C_LOCK_FUTURE_COST1;
   close C_LOCK_FUTURE_COST1;

   open C_LOCK_FUTURE_COST2;
   close C_LOCK_FUTURE_COST2;

   return TRUE;


EXCEPTION
   when RECORD_LOCKED then
      O_error_message := sql_lib.create_msg('RECORD_LOCKED',
                                             L_table,
                                             'cost change event id: '||I_cost_event_process_id);
       return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOCK_FUTURE_COST;
----------------------------------------------------------------------------------------
FUNCTION MERGE_NIL_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   -- assumes cost_susp_sup_detail and cost_susp_sup_detail are mutially exclusive
   -- assumes only one cost change per item-loc-supplier-origin_cntry

   -- cost changes with no loc detail
   merge into future_cost_gtt fc
   using ( select *From (
                  select fc.item,
                         fc.supplier,
                         fc.origin_country_id,
                         fc.location,
                         fc.loc_type,
                         fc.active_date,
                         fc.base_cost,
                         fc.net_cost,
                         fc.net_net_cost,
                         fc.dead_net_net_cost,
                         fc.pricing_cost,
                         fc.calc_date,
                         fc.start_ind,
                         fc.primary_supp_country_ind,
                         fc.currency_code,
                         fc.division,
                         fc.group_no,
                         fc.dept,
                         fc.class,
                         fc.subclass,
                         fc.item_grandparent,
                         fc.item_parent,
                         fc.diff_1,
                         fc.diff_2,
                         fc.diff_3,
                         fc.diff_4,
                         fc.chain,
                         fc.area,
                         fc.region,
                         fc.district,
                         fc.supp_hier_lvl_1,
                         fc.supp_hier_lvl_2,
                         fc.supp_hier_lvl_3,
                         fc.reclass_no,
                         fc.cost_change,
                         fc.simple_pack_ind,
                         fc.primary_cost_pack,
                         fc.primary_cost_pack_qty,
                         ch.cost_change cost_change_cost_change,
                         cd.unit_cost cost_change_cost,
                         ch.active_date cost_change_date,
                         fc.store_type,
                         fc.costing_loc,
                         fc.negotiated_item_cost,
                         fc.extended_base_cost,
                         fc.wac_tax,
                         fc.default_costing_type,
                         decode(cd.item, fc.item, 1, fc.item_parent, 2, 3) item_level,  -- 1 identifies a transaction level item
                         min(decode(cd.item,  fc.item, 1, fc.item_parent, 2, 3))
                           over (partition by fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location,
                                              ch.active_date) min_item_level,
                         rank()
                           over (partition by ch.cost_change,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from future_cost_gtt fc,
                         cost_susp_sup_head ch,
                         cost_susp_sup_detail cd,
                         sups s
                   where nvl(fc.store_type,'X') not in ('W','F')
                     and (fc.item              = cd.item or
                          fc.item_parent       = cd.item or
                          fc.item_grandparent  = cd.item)
                     and fc.supplier          = cd.supplier
                     and cd.supplier          = s.supplier
                     and fc.origin_country_id = cd.origin_country_id
                     and cd.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                         'Y', cd.default_bracket_ind)
                     and cd.cost_change       = ch.cost_change
                     and ch.status            = 'A'
                     and fc.active_date      <= ch.active_date
                     and ch.active_date      >= LP_vdate) inner
            where item_level = min_item_level
              and ranking    = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.cost_change_date)
   when matched then
   update
      set fc.cost_change        = use_this.cost_change_cost_change,
          fc.base_cost          = use_this.cost_change_cost,
          fc.net_cost           = use_this.cost_change_cost,
          fc.net_net_cost       = use_this.cost_change_cost,
          fc.dead_net_net_cost  = use_this.cost_change_cost,
          fc.pricing_cost       = use_this.cost_change_cost
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.cost_change_date,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           null,
           use_this.cost_change_cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   -- cost changes with loc detail
   merge into future_cost_gtt fc
   using ( select *From (
                  select fc.item,
                         fc.supplier,
                         fc.origin_country_id,
                         fc.location,
                         fc.loc_type,
                         fc.active_date,
                         fc.base_cost,
                         fc.net_cost,
                         fc.net_net_cost,
                         fc.dead_net_net_cost,
                         fc.pricing_cost,
                         fc.calc_date,
                         fc.start_ind,
                         fc.primary_supp_country_ind,
                         fc.currency_code,
                         fc.division,
                         fc.group_no,
                         fc.dept,
                         fc.class,
                         fc.subclass,
                         fc.item_grandparent,
                         fc.item_parent,
                         fc.diff_1,
                         fc.diff_2,
                         fc.diff_3,
                         fc.diff_4,
                         fc.chain,
                         fc.area,
                         fc.region,
                         fc.district,
                         fc.supp_hier_lvl_1,
                         fc.supp_hier_lvl_2,
                         fc.supp_hier_lvl_3,
                         fc.reclass_no,
                         fc.cost_change,
                         fc.simple_pack_ind,
                         fc.primary_cost_pack,
                         fc.primary_cost_pack_qty,
                         ch.cost_change cost_change_cost_change,
                         cdl.unit_cost cost_change_cost,
                         ch.active_date cost_change_date,
                         fc.store_type,
                         fc.costing_loc,
                         fc.negotiated_item_cost,
                         fc.extended_base_cost,
                         fc.wac_tax,
                         fc.default_costing_type,
                         decode(cdl.item, fc.item, 1, fc.item_parent, 2, 3) item_level,
                         min(decode(cdl.item, fc.item, 1, fc.item_parent, 2, 3))
                           over (partition by fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location,
                                              ch.active_date) min_item_level,
                         rank()
                           over (partition by ch.cost_change,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from future_cost_gtt fc,
                         cost_susp_sup_head ch,
                         cost_susp_sup_detail_loc cdl,
                         sups s
                   where nvl(fc.store_type,'X') not in ('W','F')
                     and (fc.item              = cdl.item or
                          fc.item_parent       = cdl.item or
                          fc.item_grandparent  = cdl.item)
                     and fc.supplier          = cdl.supplier
                     and fc.origin_country_id = cdl.origin_country_id
                     and fc.location          = cdl.loc
                     and cdl.supplier         = s.supplier
                     and cdl.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                          'Y', cdl.default_bracket_ind)
                     and cdl.cost_change      = ch.cost_change
                     and ch.status            = 'A'
                     and fc.active_date      <= ch.active_date
                     and ch.active_date      >= LP_vdate) inner
            where inner.item_level = inner.min_item_level
              and inner.ranking   = 1) use_this
   on (    fc.item              = use_this.item
       and fc.supplier          = use_this.supplier
       and fc.origin_country_id = use_this.origin_country_id
       and fc.location          = use_this.location
       and fc.active_date       = use_this.cost_change_date)
   when matched then
   update
      set fc.cost_change        = use_this.cost_change_cost_change,
          fc.base_cost          = use_this.cost_change_cost,
          fc.net_cost           = use_this.cost_change_cost,
          fc.net_net_cost       = use_this.cost_change_cost,
          fc.dead_net_net_cost  = use_this.cost_change_cost,
          fc.pricing_cost       = use_this.cost_change_cost
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.cost_change_date,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.cost_change_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           null,
           use_this.cost_change_cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.MERGE_NIL_CC',
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_NIL_CC;
----------------------------------------------------------------------------------------
FUNCTION MERGE_NIL_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   merge into future_cost_gtt fc
   using ( select * from (
                  select fc.item,
                         fc.supplier,
                         fc.origin_country_id,
                         fc.location,
                         fc.loc_type,
                         fc.active_date,
                         fc.base_cost,
                         fc.net_cost,
                         fc.net_net_cost,
                         fc.dead_net_net_cost,
                         fc.pricing_cost,
                         fc.calc_date,
                         fc.start_ind,
                         fc.primary_supp_country_ind,
                         fc.currency_code,
                         fc.division,
                         fc.group_no,
                         fc.dept,
                         fc.class,
                         fc.subclass,
                         fc.item_grandparent,
                         fc.item_parent,
                         fc.diff_1,
                         fc.diff_2,
                         fc.diff_3,
                         fc.diff_4,
                         fc.chain,
                         fc.area,
                         fc.region,
                         fc.district,
                         fc.supp_hier_lvl_1,
                         fc.supp_hier_lvl_2,
                         fc.supp_hier_lvl_3,
                         fc.reclass_no,
                         fc.cost_change,
                         fc.simple_pack_ind,
                         fc.primary_cost_pack,
                         fc.primary_cost_pack_qty,
                         fc.store_type,
                         fc.costing_loc,
                         rh.reclass_no reclass_reclass_no,
                         rh.reclass_date reclass_reclass_date,
                         g.division reclass_division,
                         g.group_no reclass_group_no,
                         rh.to_dept reclass_to_dept,
                         rh.to_class reclass_to_class,
                         rh.to_subclass reclass_to_subclass,
                         fc.negotiated_item_cost,
                         fc.extended_base_cost,
                         fc.wac_tax,
                         fc.default_costing_type,
                         rank()
                           over (partition by rh.reclass_no,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from future_cost_gtt fc,
                         reclass_head rh,
                         reclass_item  ri,
                         groups g,
                         deps d
                   where nvl(fc.store_type,'X')   not in('W','F')
                     and (fc.item              = ri.item or
                          fc.item_parent       = ri.item or
                          fc.item_grandparent  = ri.item)
                     and fc.active_date  <= LP_vdate
                     and ri.reclass_no    = rh.reclass_no
                     and rh.reclass_date >= LP_vdate
                     and rh.to_dept       = d.dept
                     and d.group_no       = g.group_no) inner
            where inner.ranking = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.reclass_reclass_date)
   when matched then
   update
      set fc.reclass_no = use_this.reclass_reclass_no,
          fc.division   = use_this.reclass_division,
          fc.group_no   = use_this.reclass_group_no,
          fc.dept       = use_this.reclass_to_dept,
          fc.class      = use_this.reclass_to_class,
          fc.subclass   = use_this.reclass_to_subclass
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.reclass_reclass_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.reclass_division,
           use_this.reclass_group_no,
           use_this.reclass_to_dept,
           use_this.reclass_to_class,
           use_this.reclass_to_subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_reclass_no,
           null,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.MERGE_NIL_RECLASS',
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_NIL_RECLASS;
----------------------------------------------------------------------------------------
FUNCTION MERGE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN
   LP_call :='MDLC';

   --seed future cost deal gtt
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

/*is: filter out W/F stores?*/

   merge into deal_item_loc_explode_gtt fcd
   using
    (select distinct gtt.item,
            gtt.supplier,
            gtt.origin_country_id,
            gtt.location,
            gtt.loc_type,
            dd.deal_id,
            dd.deal_detail_id,
            DECODE(crec.reclass_date,NULL,dh.active_date,
                                     (CASE
                                         WHEN crec.reclass_date > dh.active_date THEN
                                            crec.reclass_date
                                         ELSE
                                            dh.active_date
                                      END)) deal_active_date,
            dh.close_date deal_close_date,
            dd.cost_appl_ind,
            dd.price_cost_appl_ind,
            dd.deal_class,
            dd.threshold_value_type,
            dd.qty_thresh_buy_item,
            dd.qty_thresh_get_type,
            dd.qty_thresh_get_value,
            dd.qty_thresh_buy_qty,
            dd.qty_thresh_recur_ind,
            dd.qty_thresh_buy_target,
            dd.qty_thresh_buy_avg_loc,
            dd.qty_thresh_get_item,
            dd.qty_thresh_get_qty,
            dd.qty_thresh_free_item_unit_cost,
            dil.merch_level setup_merch_level,
            decode(dil.merch_level, 2, dil.division, NULL) setup_division,
            decode(dil.merch_level, 3, dil.group_no, NULL) setup_group_no,
            NULL setup_dept,
            NULL setup_class,
            NULL setup_subclass,
            NULL setup_item_parent,
            NULL setup_item_grandparent,
            NULL setup_diff_1,
            NULL setup_diff_2,
            NULL setup_diff_3,
            NULL setup_diff_4,
            dil.org_level setup_org_level,
            decode(dil.org_level, 1, dil.chain,    NULL) setup_chain,
            decode(dil.org_level, 2, dil.area,     NULL) setup_area,
            decode(dil.org_level, 3, dil.region,   NULL) setup_region,
            decode(dil.org_level, 4, dil.district, NULL) setup_district,
            decode(gtt.loc_type,'W',wh.physical_wh,decode(dil.org_level, 5, dil.location, NULL)) setup_location,
            dh.type deal_head_type,
            dh.partner_type,
            dh.partner_id,
            dh.create_datetime,
            dd.application_order deal_detail_application_order,
            dd.get_free_discount,
            dil.excl_ind
            --
       from deal_head dh,
            deal_detail dd,
            deal_itemloc_div_grp dil,
            future_cost_gtt gtt,
            wh,
            (select cer1.item,
                    cer1.reclass_date
               from cost_event_reclass cer1,
                    cost_event_result cer2
              where cer2.cost_event_process_id = cer1.cost_event_process_id
                and cer2.status = 'N') crec
      where dh.status = 'A'
        and dh.type in('A', 'P')
        and (dh.close_date is null or dh.close_date >= gtt.active_date)
        and dh.deal_id        = dd.deal_id
        and dd.deal_id        = dil.deal_id
        and dd.deal_detail_id = dil.deal_detail_id
        and dil.active_ind    = 'Y'
        and gtt.origin_country_id = NVL(dil.origin_country_id, gtt.origin_country_id)
        -- Merch level matches
        and (
             (dil.merch_level = 1)
             or
             (    dil.merch_level  in (2,3)
              and gtt.division     = nvl(dil.division, gtt.division)
              and gtt.group_no     = nvl(dil.group_no, gtt.group_no)))
        -- Org level matches
        and gtt.location = wh.wh(+)
        and ((NVL(dil.org_level, 0) = 0)
              or
             (    nvl(dil.org_level, 0)             in (1,2,3,4,5)
              and nvl(gtt.chain,-999)                         = nvl(dil.chain, nvl(gtt.chain,-999))
              and nvl(gtt.area,-999)                          = nvl(dil.area, nvl(gtt.area,-999))
              and nvl(gtt.region,-999)                        = nvl(dil.region, nvl(gtt.region,-999))
              and nvl(gtt.district,-999)                      = nvl(dil.district, nvl(gtt.district,-999))
              and nvl(wh.physical_wh, gtt.location) = nvl(dil.location, nvl(wh.physical_wh, gtt.location))))
        -- Supplier hier level matches
        and (   (dh.partner_type = 'S'  and gtt.supplier in (select supplier
                                                              from sups
                                                             where supplier_parent = dh.supplier
                                                                or supplier = dh.supplier))
             or (dh.partner_type = 'S1' and gtt.supp_hier_lvl_1 = dh.partner_id)
             or (dh.partner_type = 'S2' and gtt.supp_hier_lvl_2 = dh.partner_id)
             or (dh.partner_type = 'S3' and gtt.supp_hier_lvl_3 = dh.partner_id))
        and gtt.item = crec.item(+)
     union
     select distinct gtt.item,
            gtt.supplier,
            gtt.origin_country_id,
            gtt.location,
            gtt.loc_type,
            dd.deal_id,
            dd.deal_detail_id,
            DECODE(crec.reclass_date,NULL,dh.active_date,
                                     (CASE
                                         WHEN crec.reclass_date > dh.active_date THEN
                                            crec.reclass_date
                                         ELSE
                                            dh.active_date
                                      END)) deal_active_date,
            dh.close_date deal_close_date,
            dd.cost_appl_ind,
            dd.price_cost_appl_ind,
            dd.deal_class,
            dd.threshold_value_type,
            dd.qty_thresh_buy_item,
            dd.qty_thresh_get_type,
            dd.qty_thresh_get_value,
            dd.qty_thresh_buy_qty,
            dd.qty_thresh_recur_ind,
            dd.qty_thresh_buy_target,
            dd.qty_thresh_buy_avg_loc,
            dd.qty_thresh_get_item,
            dd.qty_thresh_get_qty,
            dd.qty_thresh_free_item_unit_cost,
            dil.merch_level setup_merch_level,
            NULL setup_division,
            NULL setup_group_no,
            dil.dept setup_dept,
            dil.class setup_class,
            dil.subclass setup_subclass,
            NULL setup_item_parent,
            NULL setup_item_grandparent,
            NULL setup_diff_1,
            NULL setup_diff_2,
            NULL setup_diff_3,
            NULL setup_diff_4,
            dil.org_level setup_org_level,
            decode(dil.org_level, 1, dil.chain,    NULL) setup_chain,
            decode(dil.org_level, 2, dil.area,     NULL) setup_area,
            decode(dil.org_level, 3, dil.region,   NULL) setup_region,
            decode(dil.org_level, 4, dil.district, NULL) setup_district,
            decode(gtt.loc_type,'W',wh.physical_wh,decode(dil.org_level, 5, dil.location, NULL)) setup_location,
            dh.type deal_head_type,
            dh.partner_type,
            dh.partner_id,
            dh.create_datetime,
            dd.application_order deal_detail_application_order,
            dd.get_free_discount,
            dil.excl_ind
            --
       from deal_head dh,
            deal_detail dd,
            deal_itemloc_dcs dil,
            future_cost_gtt gtt,
            wh,
            (select cer1.item,
                    cer1.reclass_date
               from cost_event_reclass cer1,
                    cost_event_result cer2
              where cer2.cost_event_process_id = cer1.cost_event_process_id
                and cer2.status = 'N') crec
      where dh.status = 'A'
        and dh.type in('A', 'P')
        and (dh.close_date is null or dh.close_date >= gtt.active_date)
        and dh.deal_id        = dd.deal_id
        and dd.deal_id        = dil.deal_id
        and dd.deal_detail_id = dil.deal_detail_id
        and dil.active_ind    = 'Y'
        and gtt.origin_country_id = NVL(dil.origin_country_id, gtt.origin_country_id)
        -- Merch level matches
        and dil.merch_level in (4,5,6)
        and gtt.dept = dil.dept
        and gtt.class = nvl(dil.class, gtt.class)
        and gtt.subclass = nvl(dil.subclass, gtt.subclass)
        -- Org level matches
        and gtt.location = wh.wh(+)
        and ((NVL(dil.org_level, 0) = 0)
              or
             (    nvl(dil.org_level, 0)             in (1,2,3,4,5)
              and nvl(gtt.chain,-999)                         = nvl(dil.chain, nvl(gtt.chain,-999))
              and nvl(gtt.area,-999)                          = nvl(dil.area, nvl(gtt.area,-999))
              and nvl(gtt.region,-999)                        = nvl(dil.region, nvl(gtt.region,-999))
              and nvl(gtt.district,-999)                      = nvl(dil.district, nvl(gtt.district,-999))
              and nvl(wh.physical_wh, gtt.location) = nvl(dil.location, nvl(wh.physical_wh, gtt.location))))
        -- Supplier hier level matches
        and (   (dh.partner_type = 'S'  and gtt.supplier in (select supplier
                                                              from sups
                                                             where supplier_parent = dh.supplier
                                                                or supplier = dh.supplier))
             or (dh.partner_type = 'S1' and gtt.supp_hier_lvl_1 = dh.partner_id)
             or (dh.partner_type = 'S2' and gtt.supp_hier_lvl_2 = dh.partner_id)
             or (dh.partner_type = 'S3' and gtt.supp_hier_lvl_3 = dh.partner_id))
        and gtt.item = crec.item(+)
     union
     select distinct gtt.item,
            gtt.supplier,
            gtt.origin_country_id,
            gtt.location,
            gtt.loc_type,
            dd.deal_id,
            dd.deal_detail_id,
            DECODE(crec.reclass_date,NULL,dh.active_date,
                                     (CASE
                                         WHEN crec.reclass_date > dh.active_date THEN
                                            crec.reclass_date
                                         ELSE
                                            dh.active_date
                                      END)) deal_active_date,
            dh.close_date deal_close_date,
            dd.cost_appl_ind,
            dd.price_cost_appl_ind,
            dd.deal_class,
            dd.threshold_value_type,
            dd.qty_thresh_buy_item,
            dd.qty_thresh_get_type,
            dd.qty_thresh_get_value,
            dd.qty_thresh_buy_qty,
            dd.qty_thresh_recur_ind,
            dd.qty_thresh_buy_target,
            dd.qty_thresh_buy_avg_loc,
            dd.qty_thresh_get_item,
            dd.qty_thresh_get_qty,
            dd.qty_thresh_free_item_unit_cost,
            dil.merch_level setup_merch_level,
            NULL setup_division,
            NULL setup_group_no,
            NULL setup_dept,
            NULL setup_class,
            NULL setup_subclass,
            dil.item_parent setup_item_parent,
            dil.item_grandparent setup_item_grandparent,
            dil.diff_1 setup_diff_1,
            dil.diff_2 setup_diff_2,
            dil.diff_3 setup_diff_3,
            dil.diff_4 setup_diff_4,
            dil.org_level setup_org_level,
            decode(dil.org_level, 1, dil.chain,    NULL) setup_chain,
            decode(dil.org_level, 2, dil.area,     NULL) setup_area,
            decode(dil.org_level, 3, dil.region,   NULL) setup_region,
            decode(dil.org_level, 4, dil.district, NULL) setup_district,
            decode(gtt.loc_type,'W',wh.physical_wh,decode(dil.org_level, 5, dil.location, NULL)) setup_location,
            dh.type deal_head_type,
            dh.partner_type,
            dh.partner_id,
            dh.create_datetime,
            dd.application_order deal_detail_application_order,
            dd.get_free_discount,
            dil.excl_ind
            --
       from deal_head dh,
            deal_detail dd,
            deal_itemloc_parent_diff dil,
            future_cost_gtt gtt,
            wh,
            (select cer1.item,
                    cer1.reclass_date
               from cost_event_reclass cer1,
                    cost_event_result cer2
              where cer2.cost_event_process_id = cer1.cost_event_process_id
                and cer2.status = 'N') crec
      where dh.status = 'A'
        and dh.type in('A', 'P')
        and (dh.close_date is null or dh.close_date >= gtt.active_date)
        and dh.deal_id        = dd.deal_id
        and dd.deal_id        = dil.deal_id
        and dd.deal_detail_id = dil.deal_detail_id
        and dil.active_ind    = 'Y'
        and gtt.origin_country_id = NVL(dil.origin_country_id, gtt.origin_country_id)
        -- Merch level matches
        and dil.merch_level in(7,8,9,10,11)
        and dil.item_parent = gtt.item_parent
        and 1 = case when dil.merch_level = 7 and dil.item_parent = gtt.item_parent then 1
                     when dil.merch_level != 7 then 1
                     else 0 end
        and 1 = case when dil.merch_level = 8 and dil.item_parent = gtt.item_parent and dil.diff_1 = gtt.diff_1 then 1
                     when dil.merch_level != 8 then 1
                     else 0 end
        and 1 = case when dil.merch_level = 9 and dil.item_parent = gtt.item_parent and dil.diff_2 = gtt.diff_2 then 1
                     when dil.merch_level != 9 then 1
                     else 0 end
        and 1 = case when dil.merch_level = 10 and dil.item_parent = gtt.item_parent and dil.diff_3 = gtt.diff_3 then 1
                     when dil.merch_level != 10 then 1
                     else 0 end
        and 1 = case when dil.merch_level = 11 and dil.item_parent = gtt.item_parent and dil.diff_4 = gtt.diff_4 then 1
                     when dil.merch_level != 11 then 1
                     else 0 end
        -- Org level matches
        and gtt.location = wh.wh(+)
        and ((NVL(dil.org_level, 0) = 0)
              or
             (    nvl(dil.org_level, 0)             in (1,2,3,4,5)
              and nvl(gtt.chain,-999)                         = nvl(dil.chain, nvl(gtt.chain,-999))
              and nvl(gtt.area,-999)                          = nvl(dil.area, nvl(gtt.area,-999))
              and nvl(gtt.region,-999)                        = nvl(dil.region, nvl(gtt.region,-999))
              and nvl(gtt.district,-999)                      = nvl(dil.district, nvl(gtt.district,-999))
              and nvl(wh.physical_wh, gtt.location) = nvl(dil.location, nvl(wh.physical_wh, gtt.location))))
        -- Supplier hier level matches
        and (   (dh.partner_type = 'S'  and gtt.supplier in (select supplier
                                                              from sups
                                                             where supplier_parent = dh.supplier
                                                                or supplier = dh.supplier))
             or (dh.partner_type = 'S1' and gtt.supp_hier_lvl_1 = dh.partner_id)
             or (dh.partner_type = 'S2' and gtt.supp_hier_lvl_2 = dh.partner_id)
             or (dh.partner_type = 'S3' and gtt.supp_hier_lvl_3 = dh.partner_id))
        and gtt.item = crec.item(+)
     union
     select distinct gtt.item,
            gtt.supplier,
            gtt.origin_country_id,
            gtt.location,
            gtt.loc_type,
            dd.deal_id,
            dd.deal_detail_id,
            DECODE(crec.reclass_date,NULL,dh.active_date,
                                     (CASE
                                         WHEN crec.reclass_date > dh.active_date THEN
                                            crec.reclass_date
                                         ELSE
                                            dh.active_date
                                      END)) deal_active_date,
            dh.close_date deal_close_date,
            dd.cost_appl_ind,
            dd.price_cost_appl_ind,
            dd.deal_class,
            dd.threshold_value_type,
            dd.qty_thresh_buy_item,
            dd.qty_thresh_get_type,
            dd.qty_thresh_get_value,
            dd.qty_thresh_buy_qty,
            dd.qty_thresh_recur_ind,
            dd.qty_thresh_buy_target,
            dd.qty_thresh_buy_avg_loc,
            dd.qty_thresh_get_item,
            dd.qty_thresh_get_qty,
            dd.qty_thresh_free_item_unit_cost,
            dil.merch_level setup_merch_level,
            NULL setup_division,
            NULL setup_group_no,
            NULL setup_dept,
            NULL setup_class,
            NULL setup_subclass,
            NULL setup_item_parent,
            NULL setup_item_grandparent,
            NULL setup_diff_1,
            NULL setup_diff_2,
            NULL setup_diff_3,
            NULL setup_diff_4,
            dil.org_level setup_org_level,
            decode(dil.org_level, 1, dil.chain,    NULL) setup_chain,
            decode(dil.org_level, 2, dil.area,     NULL) setup_area,
            decode(dil.org_level, 3, dil.region,   NULL) setup_region,
            decode(dil.org_level, 4, dil.district, NULL) setup_district,
            decode(gtt.loc_type,'W',wh.physical_wh,decode(dil.org_level, 5, dil.location, NULL)) setup_location,
            dh.type deal_head_type,
            dh.partner_type,
            dh.partner_id,
            dh.create_datetime,
            dd.application_order deal_detail_application_order,
            dd.get_free_discount,
            dil.excl_ind
            --
       from deal_head dh,
            deal_detail dd,
            deal_itemloc_item dil,
            future_cost_gtt gtt,
            wh,
            (select cer1.item,
                    cer1.reclass_date
               from cost_event_reclass cer1,
                    cost_event_result cer2
              where cer2.cost_event_process_id = cer1.cost_event_process_id
                and cer2.status = 'N') crec
      where dh.status = 'A'
        and dh.type in('A', 'P')
        and (dh.close_date is null or dh.close_date >= gtt.active_date)
        and dh.deal_id        = dd.deal_id
        and dd.deal_id        = dil.deal_id
        and dd.deal_detail_id = dil.deal_detail_id
        and dil.active_ind    = 'Y'
        and gtt.origin_country_id = NVL(dil.origin_country_id, gtt.origin_country_id)
        -- Merch level matches
        and gtt.item = dil.item
        -- Org level matches
        and gtt.location = wh.wh(+)
        and ((NVL(dil.org_level, 0) = 0)
              or
             (    nvl(dil.org_level, 0)             in (1,2,3,4,5)
              and nvl(gtt.chain,-999)                         = nvl(dil.chain, nvl(gtt.chain,-999))
              and nvl(gtt.area,-999)                          = nvl(dil.area, nvl(gtt.area,-999))
              and nvl(gtt.region,-999)                        = nvl(dil.region, nvl(gtt.region,-999))
              and nvl(gtt.district,-999)                      = nvl(dil.district, nvl(gtt.district,-999))
              and nvl(wh.physical_wh, gtt.location) = nvl(dil.location, nvl(wh.physical_wh, gtt.location))))
        and gtt.item = crec.item(+)
        -- Supplier hier level matches
        and (   (dh.partner_type = 'S'  and gtt.supplier in (select supplier
                                                              from sups
                                                             where supplier_parent = dh.supplier
                                                                or supplier = dh.supplier))
             or (dh.partner_type = 'S1' and gtt.supp_hier_lvl_1 = dh.partner_id)
             or (dh.partner_type = 'S2' and gtt.supp_hier_lvl_2 = dh.partner_id)
             or (dh.partner_type = 'S3' and gtt.supp_hier_lvl_3 = dh.partner_id))) use_this
   on (    fcd.item                  = use_this.item
       and fcd.supplier              = use_this.supplier
       and fcd.origin_country_id     = use_this.origin_country_id
       and fcd.location              = use_this.location
       and fcd.deal_id               = use_this.deal_id
       and fcd.deal_detail_id        = use_this.deal_detail_id
       and NVL(fcd.setup_division,-999) = NVL(use_this.setup_division,-999)
       and NVL(fcd.setup_group_no,-999) = NVL(use_this.setup_group_no,-999)
       and NVL(fcd.setup_dept,-999)     = NVL(use_this.setup_dept,-999)
       and NVL(fcd.setup_class,-999)    = NVL(use_this.setup_class,-999)
       and NVL(fcd.setup_subclass,-999) = NVL(use_this.setup_subclass,-999))
   when matched then
      update set fcd.retain_ind = 'Y'   -- retain matching since the deal still applies to the hierarchy
   when not matched then
   insert (     item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                deal_id,
                deal_detail_id,
                active_date,
                close_date,
                cost_appl_ind,
                price_cost_appl_ind,
                deal_class,
                threshold_value_type,
                qty_thresh_buy_item,
                qty_thresh_get_type,
                qty_thresh_get_value,
                qty_thresh_buy_qty,
                qty_thresh_recur_ind,
                qty_thresh_buy_target,
                qty_thresh_buy_avg_loc,
                qty_thresh_get_item,
                qty_thresh_get_qty,
                qty_thresh_free_item_unit_cost,
                --
                setup_merch_level,
                setup_division,
                setup_group_no,
                setup_dept,
                setup_class,
                setup_subclass,
                setup_item_parent,
                setup_item_grandparent,
                setup_diff_1,
                setup_diff_2,
                setup_diff_3,
                setup_diff_4,
                setup_org_level,
                setup_chain,
                setup_area,
                setup_region,
                setup_district,
                setup_location,
                --
                deal_head_type,
                partner_type,
                partner_id,
                create_datetime,
                deal_detail_application_order,
                get_free_discount,
                excl_ind,
                --
                retain_ind)
   values (     use_this.item,
                use_this.supplier,
                use_this.origin_country_id,
                use_this.location,
                use_this.loc_type,
                use_this.deal_id,
                use_this.deal_detail_id,
                use_this.deal_active_date,
                use_this.deal_close_date,
                use_this.cost_appl_ind,
                use_this.price_cost_appl_ind,
                use_this.deal_class,
                use_this.threshold_value_type,
                use_this.qty_thresh_buy_item,
                use_this.qty_thresh_get_type,
                use_this.qty_thresh_get_value,
                use_this.qty_thresh_buy_qty,
                use_this.qty_thresh_recur_ind,
                use_this.qty_thresh_buy_target,
                use_this.qty_thresh_buy_avg_loc,
                use_this.qty_thresh_get_item,
                use_this.qty_thresh_get_qty,
                use_this.qty_thresh_free_item_unit_cost,
                --
                use_this.setup_merch_level,
                use_this.setup_division,
                use_this.setup_group_no,
                use_this.setup_dept,
                use_this.setup_class,
                use_this.setup_subclass,
                use_this.setup_item_parent,
                use_this.setup_item_grandparent,
                use_this.setup_diff_1,
                use_this.setup_diff_2,
                use_this.setup_diff_3,
                use_this.setup_diff_4,
                use_this.setup_org_level,
                use_this.setup_chain,
                use_this.setup_area,
                use_this.setup_region,
                use_this.setup_district,
                use_this.setup_location,
                --
                use_this.deal_head_type,
                use_this.partner_type,
                use_this.partner_id,
                use_this.create_datetime,
                use_this.deal_detail_application_order,
                use_this.get_free_discount,
                use_this.excl_ind,
                --
                'Y');  -- retain_ind set to 'Y' since the deal applies to the hierarchy

   --Remove exclude records
   delete from deal_item_loc_explode_gtt dg1
    where exists (select 'x'
                    from deal_item_loc_explode_gtt dg2
                   where dg2.excl_ind              = 'Y'
                     and dg2.item                  = dg1.item
                     and dg2.supplier              = dg1.supplier
                     and dg2.origin_country_id     = dg1.origin_country_id
                     and dg2.location              = dg1.location
                     and dg2.deal_id               = dg1.deal_id
                     and dg2.deal_detail_id        = dg1.deal_detail_id);

   --future_cost records for start and close+1 dates of deal
   if DEAL_DATES_TO_FC(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
     return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.MERGE_DEAL',
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_DEAL;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_NIL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   
   cursor C_DEFAULT_TAX_TYPE is
   select default_tax_type
     from system_options;

BEGIN

   --Retrieve default_tax_type from system_options
   open C_DEFAULT_TAX_TYPE;
   fetch C_DEFAULT_TAX_TYPE into L_default_tax_type;
   close C_DEFAULT_TAX_TYPE;
  
   -- Add a row in gtt on vdate-1 to help with the calculation
   --create seed record
   --this is the only place the W/F stores will show up in future_cost_temp before roll forward.
   insert into future_cost_gtt  (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 active_date,
                                 base_cost,
                                 net_cost,
                                 net_net_cost,
                                 dead_net_net_cost,
                                 pricing_cost,
                                 calc_date,
                                 start_ind,
                                 primary_supp_country_ind,
                                 currency_code,
                                 --
                                 division,
                                 group_no,
                                 dept,
                                 class,
                                 subclass,
                                 item_grandparent,
                                 item_parent,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 --
                                 chain,
                                 area,
                                 region,
                                 district,
                                 --
                                 supp_hier_lvl_1,
                                 supp_hier_lvl_2,
                                 supp_hier_lvl_3,
                                 --
                                 reclass_no,
                                 cost_change,
                                 simple_pack_ind,
                                 primary_cost_pack,
                                 primary_cost_pack_qty,
                                 store_type,
                                 costing_loc,
                                 templ_id,
                                 --
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 wac_tax,
                                 default_costing_type)
         select cen.item,
                iscl.supplier,
                iscl.origin_country_id,
                cen.location,
                il.loc_type,
                LP_vdate-1,
                DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --base_cost 
                DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --net_cost
                DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --net_net_cost
                DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --dead_net_net_cost
                --Pricing cost is dead_net_net_cost + wac_tax
                NVL(iscl.base_cost, 0) + (NVL(iscl.extended_base_cost, 0) - NVL(iscl.base_cost, 0)) , --pricing_cost
                LP_vdate,
                'Y',
                DECODE(il.primary_supp, iscl.supplier,
                DECODE(il.primary_cntry, iscl.origin_country_id,
                               'Y', 'N'), 'N'),
                 s.currency_code,
                 --
                 cen.division,
                 cen.group_no,
                 cen.dept,
                 cen.class,
                 cen.subclass,
                 cen.item_grandparent,
                 cen.item_parent,
                 cen.diff_1,
                 cen.diff_2,
                 cen.diff_3,
                 cen.diff_4,
                 --
                 cen.chain,
                 cen.area,
                 cen.region,
                 cen.district,
                 --
                 iscl.supp_hier_lvl_1,
                 iscl.supp_hier_lvl_2,
                 iscl.supp_hier_lvl_3,
                 --
                 NULL,
                 NULL,
                 cen.simple_pack_ind,
                 il.primary_cost_pack,
                 pi.pack_qty,
                 cen.store_type,
                 DECODE(cen.store_type, 'C', NULL, il.costing_loc),
                 NULL,
                 --
                 --Source negotiated_item_cost, extended_base_cost from ITEM_SUPP_COUNTRY table.
                 --ITEM_SUPP_COUNTRY table will have updated values from tax engine by the time a new itemloc cost event is triggered.
                 DECODE(L_default_tax_type, 'GTAX', iscl.negotiated_item_cost, NULL),
                 DECODE(L_default_tax_type, 'GTAX', iscl.extended_base_cost, NULL),
                 --wac_tax is extended_base_cost - base_cost
                 DECODE(L_default_tax_type, 'GTAX', iscl.extended_base_cost, NULL) - DECODE(L_default_tax_type, 'GTAX', iscl.base_cost, NULL), --wac_tax
                 NVL(ca.default_po_cost, 'BC')
            from sups s,
                 cost_event_nil cen,
                 cost_event_thread ct,
                 item_supp_country_loc iscl,
                 item_loc il,
                 packitem pi,
                 country_attrib ca
           where ct.cost_event_process_id = I_cost_event_process_id
             and ct.thread_id             = I_thread_id
             and ct.item                  = iscl.item
             and ct.supplier              = iscl.supplier
             and ct.origin_country_id     = iscl.origin_country_id
             and ct.location              = iscl.loc
             and ct.cost_event_process_id = cen.cost_event_process_id
             and cen.item                 = iscl.item
             and cen.location             = iscl.loc
             and ct.item                  = il.item
             and ct.location              = il.loc
             and iscl.supplier            = s.supplier
             and il.primary_cost_pack     = pi.pack_no(+)
             and ct.origin_country_id     = ca.country_id (+);

       --add FUTURE_COST_ELC as needed
       if ELC_DATE_TO_FC(O_error_message,
                         I_cost_event_process_id,
                         I_thread_id) = FALSE THEN
          return FALSE;
       end if;

       --add FUTURE_COST_RECLASS as needed
       if MERGE_NIL_RECLASS(O_error_message,
                            I_cost_event_process_id,
                            I_thread_id) = FALSE THEN
          return FALSE;
       end if;

       --add FUTURE_COST_COST_CHANGE as needed, deal with no loc cost changes
       if MERGE_NIL_CC(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE THEN
          return FALSE;
       end if;

       --Compute tax fields for any future_cost_temp record for a cost change
       if COMPUTE_CC_TAX(O_error_message,
                         I_cost_event_process_id,
                         I_thread_id) = FALSE THEN
          return FALSE;
       end if;

       --add DEAL_ITEM_LOC_EXPLODE as needed, deal with hierarchy, exception issues
       if MERGE_DEAL(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE THEN
          return FALSE;
       end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.EXPLODE_NIL',
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_NIL;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_COST_CHANGE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_action                IN     COST_EVENT.ACTION%TYPE,
                             I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_COST_CHANGE';

BEGIN

   if EXPLODE_CC(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
      if REMOVE_CC(O_error_message,
                   I_cost_event_process_id,
                   I_thread_id) = FALSE then
         return FALSE;
      end if;
   elsif I_action = FUTURE_COST_EVENT_SQL.ADD_EVENT then
      if ADD_CC(O_error_message,
                I_cost_event_process_id,
                I_thread_id) = FALSE then
         return FALSE;
      end if;
   end if;

   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_COST_CHANGE;
----------------------------------------------------------------------------------------
FUNCTION REMOVE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(100) := 'FUTURE_COST_SQL.REMOVE_CC';

BEGIN

   ---REMOVE
   update future_cost_gtt
      set cost_change = null
    where cost_change in (select cost_change
                            from cost_event_cost_chg
                           where cost_event_process_id = I_cost_event_process_id);
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REMOVE_CC;
----------------------------------------------------------------------------------------
FUNCTION ADD_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(100) := 'FUTURE_COST_SQL.ADD_CC';

BEGIN

   ---MERGE
   if MERGE_CC(O_error_message,
               I_cost_event_process_id,
               I_thread_id) = FALSE then
      return FALSE;
   end if;

   --Compute tax fields for any future_cost_temp record for a cost change
   if COMPUTE_CC_TAX(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_CC;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_CC';
   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   
   cursor C_DEFAULT_TAX_TYPE is
      select default_tax_type
        from system_options;

BEGIN
   open C_DEFAULT_TAX_TYPE;
   fetch C_DEFAULT_TAX_TYPE into L_default_tax_type;
   close C_DEFAULT_TAX_TYPE;

      insert into future_cost_gtt (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   calc_date,
                                   start_ind,
                                   acquisition_cost,
                                   elc_amt,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_parent,
                                   item_grandparent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select distinct fc.item,
                   fc.supplier,
                   fc.origin_country_id,
                   fc.location,
                   fc.loc_type,
                   fc.active_date,
                   fc.base_cost,
                   fc.net_cost,
                   fc.net_net_cost,
                   fc.dead_net_net_cost,
                   fc.pricing_cost,
                   fc.calc_date,
                   fc.start_ind,
                   fc.acquisition_cost,
                   fc.elc_amt,
                   fc.primary_supp_country_ind,
                   fc.currency_code,
                   fc.division,
                   fc.group_no,
                   fc.dept,
                   fc.class,
                   fc.subclass,
                   fc.item_parent,
                   fc.item_grandparent,
                   fc.diff_1,
                   fc.diff_2,
                   fc.diff_3,
                   fc.diff_4,
                   fc.chain,
                   fc.area,
                   fc.region,
                   fc.district,
                   fc.supp_hier_lvl_1,
                   fc.supp_hier_lvl_2,
                   fc.supp_hier_lvl_3,
                   fc.reclass_no,
                   fc.cost_change,
                   fc.simple_pack_ind,
                   fc.primary_cost_pack,
                   fc.primary_cost_pack_qty,
                   fc.store_type,
                   fc.costing_loc,
                   fc.templ_id,
                   fc.passthru_pct,
                   DECODE(L_default_tax_type,'GTAX',fc.negotiated_item_cost,NULL),
                   DECODE(L_default_tax_type,'GTAX',fc.extended_base_cost,NULL),
                   DECODE(L_default_tax_type,'GTAX',fc.wac_tax,NULL),
                   fc.default_costing_type,
          null as processing_seq_no
     from cost_event_thread ct,
          future_cost fc
    where ct.cost_event_process_id = I_cost_event_process_id
      and ct.thread_id             = I_thread_id
      and (ct.item                 = fc.item or ct.item = fc.item_parent)
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location
      and nvl(fc.store_type, 'X') !='F'
      and fc.active_date      >= ( select max(active_date) 
                                       from future_cost fc1 
                                       where fc1.item      =  fc.item 
                                       and fc1.supplier  =  fc.supplier 
                                       and fc1.origin_country_id = fc.origin_country_id 
                                       and fc1.location  =  fc.location 
                                       and fc1.active_date <= LP_vdate ); 


   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_CC;
----------------------------------------------------------------------------------------
FUNCTION MERGE_CC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                  I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS
   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;

   cursor C_DEFAULT_TAX_TYPE is
      select default_tax_type
        from system_options;
BEGIN
   open C_DEFAULT_TAX_TYPE;
   fetch C_DEFAULT_TAX_TYPE into L_default_tax_type;
   close C_DEFAULT_TAX_TYPE;
   -- Find future cost event records on the timeline on the same date or prior
   -- to the cost change.
   -- Records on the same date as the cost change will have their cost values
   -- updated with the values on the cost change.
   -- If only prior records exists, then insert a new row for the cost change.

   --cost change at supplier level (no loc)
   merge into future_cost_gtt fc
   using (
      select *From (
         (select /*+ ORDERED */ fc.*,
                cd.cost_change new_cost_change,
                cd.unit_cost new_unit_cost,
                ch.active_date new_active_date,
                decode(cd.item,  fc.item, 1, fc.item_parent, 2, 3)  item_level, -- level of fc item in relation to cc item
                min(decode(cd.item,  fc.item, 1, fc.item_parent, 2, 3))
                   over (partition by fc.item,
                                      fc.supplier,
                                      fc.origin_country_id,
                                      fc.location,
                                      ch.active_date) min_item_level,
                rank() over(partition by fc.item,
                                         fc.supplier,
                                         fc.origin_country_id,
                                         fc.location
                                order by fc.active_date desc) as rank_value
           from cost_event_cost_chg cc,
                cost_susp_sup_detail cd,
                cost_susp_sup_head ch,
                sups s,
                future_cost_gtt fc
          where cc.cost_event_process_id = I_cost_event_process_id
            and cc.src_tmp_ind           = 'N'
            and cc.cost_change           = cd.cost_change
            --
            and (fc.item              = cd.item or
                 fc.item_parent       = cd.item or
                 fc.item_grandparent  = cd.item)
            and fc.supplier              = cd.supplier
            and cd.supplier              = s.supplier
            and fc.origin_country_id     = cd.origin_country_id
            and cd.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                'Y', cd.default_bracket_ind)
            and cd.cost_change           = ch.cost_change
            and fc.active_date          <= ch.active_date)
          union
         (select /*+ ORDERED */ fc.*,
                 cct.cost_change new_cost_change,
                 cct.unit_cost_new new_unit_cost,
                  ch.active_date new_active_date,
                 decode(cct.item,  fc.item, 1, fc.item_parent, 2, 3)  item_level, -- level of fc item in relation to cc item
                 min(decode(cct.item,  fc.item, 1, fc.item_parent, 2, 3))
                    over (partition by fc.item,
                                       fc.supplier,
                                       fc.origin_country_id,
                                       fc.location,
                                       ch.active_date) min_item_level,
                 rank() over(partition by fc.item,
                                          fc.supplier,
                                          fc.origin_country_id,
                                          fc.location
                                 order by fc.active_date desc) as rank_value
            from cost_event_cost_chg cc,
                 cost_change_temp cct,
                 cost_susp_sup_head ch,
                 sups s,
                 future_cost_gtt fc
           where cc.cost_event_process_id = I_cost_event_process_id
             and cc.src_tmp_ind           = 'Y'
             and cc.cost_change           = cct.cost_change
             --
             and (fc.item              = cct.item or
                  fc.item_parent       = cct.item or
                  fc.item_grandparent  = cct.item)
             and fc.supplier           = cct.supplier
             and cct.supplier          = s.supplier
             and fc.origin_country_id  = cct.origin_country_id
             and cct.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                 'Y', cct.default_bracket_ind)
             and cct.cost_change          = ch.cost_change
             and cct.unit_cost_new        IS NOT NULL
             and fc.active_date          <= ch.active_date))
    where item_level = min_item_level
      and rank_value = 1) use_this
   on (    fc.item              = use_this.item
       and fc.supplier          = use_this.supplier
       and fc.origin_country_id = use_this.origin_country_id
       and fc.location          = use_this.location
       and fc.active_date       = use_this.new_active_date)
   when matched then
   update
      set fc.cost_change           = use_this.new_cost_change,
          fc.base_cost             = use_this.new_unit_cost,
          fc.net_cost              = use_this.new_unit_cost,
          fc.net_net_cost          = use_this.new_unit_cost,
          fc.dead_net_net_cost     = use_this.new_unit_cost,
          fc.pricing_cost          = use_this.new_unit_cost,
          fc.negotiated_item_cost  = DECODE(L_default_tax_type,'GTAX',use_this.new_unit_cost,NULL)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.new_active_date,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.new_cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           DECODE(L_default_tax_type,'GTAX',use_this.new_unit_cost,NULL),
           DECODE(L_default_tax_type,'GTAX',use_this.extended_base_cost,NULL),
           DECODE(L_default_tax_type,'GTAX',use_this.wac_tax,NULL),
           use_this.default_costing_type);

   --cost change at loc level
   merge into future_cost_gtt fc
   using (
      select *From (
         (select /*+ ORDERED */ fc.*,
                cd.cost_change new_cost_change,
                cd.unit_cost new_unit_cost,
                ch.active_date new_active_date,
                decode(cd.item,  fc.item, 1, fc.item_parent, 2, 3)  item_level, -- level of fc item in relation to cc item
                min(decode(cd.item,  fc.item, 1, fc.item_parent, 2, 3))
                   over (partition by fc.item,
                                      fc.supplier,
                                      fc.origin_country_id,
                                      fc.location,
                                      ch.active_date) min_item_level,
                rank() over(partition by fc.item,
                                         fc.supplier,
                                         fc.origin_country_id,
                                         fc.location
                                order by fc.active_date desc) as rank_value
           from cost_event_cost_chg cc,
                cost_susp_sup_detail_loc cd,
                cost_susp_sup_head ch,
                sups s,
                future_cost_gtt fc
          where cc.cost_event_process_id = I_cost_event_process_id
            and cc.src_tmp_ind           = 'N'
            and cc.cost_change           = cd.cost_change
            --
            and (fc.item              = cd.item or
                 fc.item_parent       = cd.item or
                 fc.item_grandparent  = cd.item)
            and fc.supplier              = cd.supplier
            and cd.supplier              = s.supplier
            and fc.origin_country_id     = cd.origin_country_id
            and fc.location              = cd.loc
            and cd.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                'Y', cd.default_bracket_ind)
            and cd.cost_change           = ch.cost_change
            and fc.active_date          <= ch.active_date)
          union --For stores
         (select /*+ ORDERED */ fc.*,
                cclt.cost_change new_cost_change,
                cclt.unit_cost_new new_unit_cost,
                ch.active_date new_active_date,
                decode(cclt.item,  fc.item, 1, fc.item_parent, 2, 3)  item_level, -- level of fc item in relation to cc item
                min(decode(cclt.item,  fc.item, 1, fc.item_parent, 2, 3))
                   over (partition by fc.item,
                                      fc.supplier,
                                      fc.origin_country_id,
                                      fc.location,
                                      ch.active_date) min_item_level,
                rank() over(partition by fc.item,
                                         fc.supplier,
                                         fc.origin_country_id,
                                         fc.location
                                order by fc.active_date desc) as rank_value
           from cost_event_cost_chg cc,
                cost_change_loc_temp cclt,
                cost_susp_sup_head ch,
                sups s,
                future_cost_gtt fc
          where cc.cost_event_process_id = I_cost_event_process_id
            and cc.src_tmp_ind           = 'Y'
            and cc.cost_change           = cclt.cost_change
            and cclt.loc_type            = 'S'
            --
            and (fc.item              = cclt.item or
                 fc.item_parent       = cclt.item or
                 fc.item_grandparent  = cclt.item)
            and fc.supplier              = cclt.supplier
            and cclt.supplier            =    s.supplier
            and fc.origin_country_id     = cclt.origin_country_id
            and fc.location              = cclt.location
            and cclt.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                  'Y', cclt.default_bracket_ind)
            and cclt.cost_change         = ch.cost_change
            and cclt.unit_cost_new       IS NOT NULL
            and fc.active_date          <= ch.active_date)
          union --For warehouses
         (select /*+ ORDERED */ fc.*,
                cclt.cost_change new_cost_change,
                cclt.unit_cost_new new_unit_cost,
                ch.active_date new_active_date,
                decode(cclt.item,  fc.item, 1, fc.item_parent, 2, 3)  item_level, -- level of fc item in relation to cc item
                min(decode(cclt.item,  fc.item, 1, fc.item_parent, 2, 3))
                   over (partition by fc.item,
                                      fc.supplier,
                                      fc.origin_country_id,
                                      fc.location,
                                      ch.active_date) min_item_level,
                rank() over(partition by fc.item,
                                         fc.supplier,
                                         fc.origin_country_id,
                                         fc.location
                                order by fc.active_date desc) as rank_value
           from cost_event_cost_chg cc,
                cost_change_loc_temp cclt,
                cost_susp_sup_head ch,
                sups s,
                future_cost_gtt fc,
                wh
          where cc.cost_event_process_id = I_cost_event_process_id
            and cc.src_tmp_ind           = 'Y'
            and cc.cost_change           = cclt.cost_change
            and cclt.loc_type            = 'W'
            and cclt.location            = wh.physical_wh
            --
            and (fc.item              = cclt.item or
                 fc.item_parent       = cclt.item or
                 fc.item_grandparent  = cclt.item)
            and fc.supplier              = cclt.supplier
            and cclt.supplier            =    s.supplier
            and fc.origin_country_id     = cclt.origin_country_id
            and fc.location              = wh.wh
            and cclt.default_bracket_ind = DECODE(s.bracket_costing_ind, 'Y',
                                                  'Y', cclt.default_bracket_ind)
            and cclt.cost_change         = ch.cost_change
            and cclt.unit_cost_new       IS NOT NULL
            and fc.active_date          <= ch.active_date))
    where item_level = min_item_level
      and rank_value = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.new_active_date)
   when matched then
   update
      set fc.cost_change           = use_this.new_cost_change,
          fc.base_cost             = use_this.new_unit_cost,
          fc.net_cost              = use_this.new_unit_cost,
          fc.net_net_cost          = use_this.new_unit_cost,
          fc.dead_net_net_cost     = use_this.new_unit_cost,
          fc.pricing_cost          = use_this.new_unit_cost,
          fc.negotiated_item_cost  = DECODE(L_default_tax_type,'GTAX',use_this.new_unit_cost,NULL)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.new_active_date,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.new_unit_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.new_cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           DECODE(L_default_tax_type,'GTAX',use_this.new_unit_cost,NULL),
           DECODE(L_default_tax_type,'GTAX',use_this.extended_base_cost,NULL),
           DECODE(L_default_tax_type,'GTAX',use_this.wac_tax,NULL),
           use_this.default_costing_type);

--Cost change for Buyer pack item.
merge into future_cost_gtt gtt
      using (select distinct pack_no item,
                    supplier,
                    origin_country_id,
                    location,
                    loc_type,
                    new_active_date active_date,
                    new_base_cost,
                    net_cost,
                    net_net_cost,
                    dead_net_net_cost,
                    pricing_cost,
                    calc_date,
                    start_ind,
                    acquisition_cost,
                    primary_ind primary_supp_country_ind,
                    currency_code,
                    elc_amt,
                    division,
                    group_no,
                    dept,
                    class,
                    subclass,
                    item_grandparent,
                    item_parent,
                    diff_1,
                    diff_2,
                    diff_3,
                    diff_4,
                    chain,
                    area,
                    region,
                    district,
                    supp_hier_lvl_1,
                    supp_hier_lvl_2,
                    supp_hier_lvl_3,
                    reclass_no,
                    new_cost_change cost_change,
                    simple_pack_ind,
                    primary_cost_pack,
                    primary_cost_pack_qty,
                    store_type,
                    costing_loc,
                    templ_id,
                    passthru_pct,
                    negotiated_item_cost,
                    extended_base_cost,
                    wac_tax,
                    default_costing_type
               from (select fc.item,
                            fc.supplier,
                            fc.origin_country_id,
                            fc.location,
                            fc.loc_type,
                            pack.base_cost net_cost,
                            pack.base_cost net_net_cost,
                            pack.base_cost dead_net_net_cost,
                            pack.base_cost pricing_cost,
                            fc.calc_date,
                            fc.start_ind,
                            pack.acq_cost acquisition_cost,
                            fc.currency_code,
                            pack.elc elc_amt,
                            fc.division,
                            fc.group_no,
                            fc.dept,
                            fc.class,
                            fc.subclass,
                            null item_grandparent,
                            null item_parent,
                            null diff_1,
                            null diff_2,
                            null diff_3,
                            null diff_4,
                            fc.chain,
                            fc.area,
                            fc.region,
                            fc.district,
                            null supp_hier_lvl_1,
                            null supp_hier_lvl_2,
                            null supp_hier_lvl_3,
                            null reclass_no,
                            fc.simple_pack_ind,
                            null primary_cost_pack,
                            null primary_cost_pack_qty,
                            fc.store_type,
                            fc.costing_loc,
                            fc.templ_id,
                            fc.passthru_pct,
                            pack.base_cost negotiated_item_cost,
                            pack.base_cost extended_base_cost,                    
                            fc.wac_tax,
                            fc.default_costing_type,
                            pack.pack_no pack_no,
                            pack.base_cost new_base_cost,
                            ch.cost_change new_cost_change,
                            ch.active_date new_active_date,
                            case
                               when il.primary_supp = pack.supplier and
                                    il.primary_cntry = pack.origin_country_id then
                                  'Y'
                               else
                                  'N'
                            end primary_ind,
                             rank() over(partition by fc.item,
                                                    fc.supplier,
                                                    fc.origin_country_id,
                                                    fc.location
                                                    order by fc.active_date desc) as rank_value
                       from (select sum(base_cost*qty) BASE_COST,
                                    sum(acq_cost*qty) ACQ_COST,
                                    sum(elc*qty) ELC,
                                    pack_no,
                                    supplier,
                                    origin_country_id,
                                    location
                               from (select nvl(base_cost,0) base_cost,
                                            qty,
                                            item,
                                            supplier,
                                            origin_country_id,
                                            location,
                                            pack_no,
                                            nvl(acq_cost,0) acq_cost,
                                            nvl(elc,0) elc
                                       from (select fc.*,
                                                    vpq.qty qty,
                                                    vpq.pack_no pack_no,
                                                    fc.acquisition_cost acq_cost,
                                                    fc.elc_amt elc,
                                                    rank() over(partition by fc.item,
                                                    fc.supplier,
                                                    fc.origin_country_id,
                                                    fc.location
                                                    order by fc.active_date desc) as rank_value
                                              from future_cost fc,
                                                   (select vpq2.pack_no pack_no,
                                                           vpq2.qty qty,
                                                           vpq2.item  item
                                                      from v_packsku_qty vpq2
                                                     where exists (select 1
                                                                     from v_packsku_qty vpq1,
                                                                          cost_event_thread cet
                                                                     where vpq2.pack_no=vpq1.pack_no
                                                                       and cet.item=vpq1.item
                                                                       and cet.cost_event_process_id = I_cost_event_process_id
                                                                       and cet.thread_id = I_thread_id)) vpq,
                                                    cost_susp_sup_head ch,
                                                    cost_event_cost_chg cc,                                                    
                                                    cost_event_thread ct,
                                                    item_master im,
                                                    item_master im2
                                             where fc.item(+) = vpq.item
                                               and fc.item = im2.item
                                               and im.item = vpq.pack_no
                                               and im.pack_ind = 'Y'
                                               and im.pack_type = 'B'
                                               and fc.active_date <= ch.active_date
                                               and cc.cost_event_process_id = I_cost_event_process_id
                                               and cc.src_tmp_ind           = 'N'
                                               and cc.cost_change           = ch.cost_change                                               
                                               and fc.active_date          <= ch.active_date
                                               and not exists( select 1
                                                                 from cost_event_thread ct
                                                                where fc.item = ct.item
                                                                  and ct.location=fc.location
                                                                  and ct.cost_event_process_id = I_cost_event_process_id)
                                               and ct.cost_event_process_id = cc.cost_event_process_id                                             
                                               and ct.location=fc.location)                                               
                                      where rank_value=1
                                  UNION ALL
                                     select base_cost,
                                            qty,
                                            item,
                                            supplier,
                                            origin_country_id,
                                            location,
                                            pack_no,
                                            nvl(acq_cost,0) acq_cost,
                                            nvl(elc,0) elc
                                     from
                                     (select fc.base_cost base_cost,
                                             pb.item_qty qty,
                                             fc.item,
                                             fc.supplier,
                                             fc.location,
                                             fc.origin_country_id,
                                             pb.pack_no,
                                             fc.acquisition_cost acq_cost,
                                             fc.elc_amt elc,
                                             rank() over(partition by fc.item,
                                                                    fc.supplier,
                                                        fc.origin_country_id,
                                                    fc.location
                                                    order by fc.active_date desc) as rank_value
                                       from future_cost_gtt fc,
                                            packitem_breakout pb,
                                            item_master im,
                                            item_master im2
                                      where (fc.item = pb.item or fc.item = pb.item_parent)
                                        and (im2.item = fc.item or im2.item_parent = fc.item)
                                        and im.item = pb.pack_no
                                        and im.pack_ind = 'Y'
                                        and im.pack_type = 'B')
                                        where rank_value = 1 )
                             group by pack_no,
                                      supplier,
                                      origin_country_id,
                                      location)pack,
                                      future_cost_gtt fc,
                                      cost_susp_sup_head ch,
                                      cost_event_cost_chg cc,
                                      item_loc il
                      where fc.active_date <= ch.active_date and
                            cc.cost_event_process_id = I_cost_event_process_id
                            and fc.supplier = pack.supplier
                            and fc.origin_country_id = pack.origin_country_id
                            and fc.location = pack.location
                            and cc.src_tmp_ind ='N'
                            and cc.cost_change = ch.cost_change
                            and pack.pack_no = il.item
                            and pack.location = il.loc) where rank_value=1
) use_this
   on (    gtt.item = use_this.item
       and gtt.supplier          = use_this.supplier
       and gtt.origin_country_id = use_this.origin_country_id
       and gtt.location          = use_this.location
       and gtt.active_date       = use_this.active_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           acquisition_cost,
           primary_supp_country_ind,
           currency_code,
           elc_amt,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           templ_id,
           passthru_pct,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.active_date ,
           use_this.new_base_cost,
           use_this.new_base_cost,
           use_this.new_base_cost,
           use_this.new_base_cost,
           use_this.new_base_cost,
           LP_vdate,
           use_this.start_ind,
           use_this.acquisition_cost,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.elc_amt,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.templ_id,
           use_this.passthru_pct,
           DECODE(L_default_tax_type,'GTAX',use_this.new_base_cost,NULL),
           DECODE(L_default_tax_type,'GTAX',use_this.new_base_cost,NULL),
           DECODE(L_default_tax_type,'GTAX',use_this.wac_tax,NULL),
           use_this.default_costing_type)
   when matched then
   update set gtt.base_cost = use_this.new_base_cost,
              gtt.net_cost = use_this.new_base_cost,
              gtt.net_net_cost = use_this.new_base_cost,
              gtt.dead_net_net_cost = use_this.new_base_cost,
              gtt.pricing_cost = use_this.new_base_cost,
              gtt.negotiated_item_cost=DECODE(L_default_tax_type,'GTAX',use_this.new_base_cost,NULL);
         
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.MERGE_CC',
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_CC;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_action                IN     COST_EVENT.ACTION%TYPE,
                         I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
      if REMOVE_RECLASS(O_error_message,
                        I_cost_event_process_id,
                        I_thread_id) = FALSE then
         return FALSE;
      end if;
   elsif I_action = FUTURE_COST_EVENT_SQL.ADD_EVENT then
      if ADD_RECLASS(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.EXPLODE_RECLASS',
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_RECLASS;
----------------------------------------------------------------------------------------
FUNCTION ADD_RECLASS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.ADD_RECLASS';
   L_return        NUMBER(1);

BEGIN

   -- insert rows into future_cost_temp affected by reclass
   insert into future_cost_gtt(item,
                                supplier,
                                origin_country_id,
                                location,
                                loc_type,
                                active_date,
                                base_cost,
                                net_cost,
                                net_net_cost,
                                dead_net_net_cost,
                                pricing_cost,
                                calc_date,
                                start_ind,
                                acquisition_cost,
                                elc_amt,
                                primary_supp_country_ind,
                                currency_code,
                                division,
                                group_no,
                                dept,
                                class,
                                subclass,
                                item_parent,
                                item_grandparent,
                                diff_1,
                                diff_2,
                                diff_3,
                                diff_4,
                                chain,
                                area,
                                region,
                                district,
                                supp_hier_lvl_1,
                                supp_hier_lvl_2,
                                supp_hier_lvl_3,
                                reclass_no,
                                cost_change,
                                simple_pack_ind,
                                primary_cost_pack,
                                primary_cost_pack_qty,
                                store_type,
                                costing_loc,
                                templ_id,
                                passthru_pct,
                                negotiated_item_cost,
                                extended_base_cost,
                                wac_tax,
                                default_costing_type,
                                processing_seq_no)
   select distinct fc.item,
                   fc.supplier,
                   fc.origin_country_id,
                   fc.location,
                   fc.loc_type,
                   fc.active_date,
                   fc.base_cost,
                   fc.net_cost,
                   fc.net_net_cost,
                   fc.dead_net_net_cost,
                   fc.pricing_cost,
                   fc.calc_date,
                   fc.start_ind,
                   fc.acquisition_cost,
                   fc.elc_amt,
                   fc.primary_supp_country_ind,
                   fc.currency_code,
                   fc.division,
                   fc.group_no,
                   fc.dept,
                   fc.class,
                   fc.subclass,
                   fc.item_parent,
                   fc.item_grandparent,
                   fc.diff_1,
                   fc.diff_2,
                   fc.diff_3,
                   fc.diff_4,
                   fc.chain,
                   fc.area,
                   fc.region,
                   fc.district,
                   fc.supp_hier_lvl_1,
                   fc.supp_hier_lvl_2,
                   fc.supp_hier_lvl_3,
                   fc.reclass_no,
                   fc.cost_change,
                   fc.simple_pack_ind,
                   fc.primary_cost_pack,
                   fc.primary_cost_pack_qty,
                   fc.store_type,
                   fc.costing_loc,
                   fc.templ_id,
                   fc.passthru_pct,
                   fc.negotiated_item_cost,
                   fc.extended_base_cost,
                   fc.wac_tax,
                   fc.default_costing_type,
          null as processing_seq_no
     from cost_event_thread ct,
          future_cost fc
    where ct.cost_event_process_id = I_cost_event_process_id
      and ct.thread_id             = I_thread_id
      and ct.item                  = fc.item
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   ---
   if MERGE_RECLASS(O_error_message,
                    I_cost_event_process_id,
                    I_thread_id) = FALSE then
      return FALSE;
   end if;

   --roll foward the reclass
   L_return := FUTURE_COST_ROLLFWD_SQL.ADD_PROCESSING_SEQ_NO(O_error_message,
                                                             I_cost_event_process_id,
                                                             I_thread_id);
   if L_return = 0 then
      return FALSE;
   end if;

   L_return := FUTURE_COST_ROLLFWD_SQL.ROLL_RECLASS(O_error_message,
                                                    I_cost_event_process_id,
                                                    I_thread_id);
   if L_return = 0 then
      return FALSE;
   end if;

   --add deals
   if MERGE_DEAL(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_RECLASS;
----------------------------------------------------------------------------------------
FUNCTION REMOVE_RECLASS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100) := 'FUTURE_COST_SQL.REMOVE_RECLASS';
   L_return    NUMBER(1);

BEGIN

   insert into future_cost_gtt (item,
                                supplier,
                                origin_country_id,
                                location,
                                loc_type,
                                active_date,
                                base_cost,
                                net_cost,
                                net_net_cost,
                                dead_net_net_cost,
                                pricing_cost,
                                acquisition_cost,
                                calc_date,
                                start_ind,
                                primary_supp_country_ind,
                                currency_code,
                                elc_amt,
                                division,
                                group_no,
                                dept,
                                class,
                                subclass,
                                item_grandparent,
                                item_parent,
                                diff_1,
                                diff_2,
                                diff_3,
                                diff_4,
                                chain,
                                area,
                                region,
                                district,
                                supp_hier_lvl_1,
                                supp_hier_lvl_2,
                                supp_hier_lvl_3,
                                reclass_no,
                                cost_change,
                                simple_pack_ind,
                                primary_cost_pack,
                                primary_cost_pack_qty,
                                store_type,
                                costing_loc,
                                templ_id,
                                passthru_pct,
                                negotiated_item_cost,
                                extended_base_cost,
                                wac_tax,
                                default_costing_type,
                                processing_seq_no)
      select fc.item,
             fc.supplier,
             fc.origin_country_id,
             fc.location,
             fc.loc_type,
             fc.active_date,
             fc.base_cost,
             fc.net_cost,
             fc.net_net_cost,
             fc.dead_net_net_cost,
             fc.pricing_cost,
             fc.acquisition_cost,
             fc.calc_date,
             fc.start_ind,
             fc.primary_supp_country_ind,
             fc.currency_code,
             fc.elc_amt,
             g.division,
             g.group_no,
             im.dept,
             im.class,
             im.subclass,
             fc.item_grandparent,
             fc.item_parent,
             fc.diff_1,
             fc.diff_2,
             fc.diff_3,
             fc.diff_4,
             fc.chain,
             fc.area,
             fc.region,
             fc.district,
             fc.supp_hier_lvl_1,
             fc.supp_hier_lvl_2,
             fc.supp_hier_lvl_3,
             NULL as reclass_no,
             fc.cost_change,
             fc.simple_pack_ind,
             fc.primary_cost_pack,
             fc.primary_cost_pack_qty,
             fc.store_type,
             fc.costing_loc,
             fc.templ_id,
             fc.passthru_pct,
             fc.negotiated_item_cost,
             fc.extended_base_cost,
             fc.wac_tax,
             fc.default_costing_type,
             NULL as processing_seq_no
        from cost_event_reclass cr,
             cost_event_thread ct,
             future_cost fc,
             item_master im,
             groups g,
             deps d
       where cr.cost_event_process_id = I_cost_event_process_id
         and fc.active_date >= cr.reclass_date
         and im.item = fc.item
         and ct.cost_event_process_id = cr.cost_event_process_id
         and ct.thread_id = I_thread_id
         and ct.item = fc.item
         and (fc.item = cr.item or
              fc.item_parent = cr.item or
              fc.item_grandparent = cr.item)
         and ct.supplier = fc.supplier
         and ct.origin_country_id = fc.origin_country_id
         and ct.location = fc.location
         and im.dept = d.dept
         and d.group_no = g.group_no;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   --roll foward the reclass
   L_return := FUTURE_COST_ROLLFWD_SQL.ADD_PROCESSING_SEQ_NO(O_error_message,
                                                             I_cost_event_process_id,
                                                             I_thread_id);
   if L_return = 0 then
      return FALSE;
   end if;

   L_return := FUTURE_COST_ROLLFWD_SQL.ROLL_RECLASS(O_error_message,
                                                    I_cost_event_process_id,
                                                    I_thread_id);
   if L_return = 0 then
      return FALSE;
   end if;

   --add deals
   if MERGE_DEAL(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REMOVE_RECLASS;
----------------------------------------------------------------------------------------
FUNCTION MERGE_RECLASS(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN
   -- insert a row for the reclass on the timeline or if a row exists on the same date
   -- as the reclass, update the row with the reclass number and the new merch hier
   merge into future_cost_gtt fc
   using ( select * from (
                  select fc.item,
                         fc.supplier,
                         fc.origin_country_id,
                         fc.location,
                         fc.loc_type,
                         fc.active_date,
                         fc.base_cost,
                         fc.net_cost,
                         fc.net_net_cost,
                         fc.dead_net_net_cost,
                         fc.pricing_cost,
                         fc.calc_date,
                         fc.start_ind,
                         fc.primary_supp_country_ind,
                         fc.currency_code,
                         fc.division,
                         fc.group_no,
                         fc.dept,
                         fc.class,
                         fc.subclass,
                         fc.item_grandparent,
                         fc.item_parent,
                         fc.diff_1,
                         fc.diff_2,
                         fc.diff_3,
                         fc.diff_4,
                         fc.chain,
                         fc.area,
                         fc.region,
                         fc.district,
                         fc.supp_hier_lvl_1,
                         fc.supp_hier_lvl_2,
                         fc.supp_hier_lvl_3,
                         fc.reclass_no,
                         fc.cost_change,
                         fc.simple_pack_ind,
                         fc.primary_cost_pack,
                         fc.primary_cost_pack_qty,
                         fc.store_type,
                         fc.costing_loc,
                         fc.negotiated_item_cost,
                         fc.extended_base_cost,
                         fc.wac_tax,
                         fc.default_costing_type,
                         cer.reclass_no reclass_reclass_no,
                         cer.reclass_date reclass_reclass_date,
                         g.division reclass_division,
                         g.group_no reclass_group_no,
                         cer.to_dept reclass_to_dept,
                         cer.to_class reclass_to_class,
                         cer.to_subclass reclass_to_subclass,
                         rank()
                           over (partition by cer.reclass_no,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from cost_event_reclass cer,
                         future_cost_gtt fc,
                         groups g,
                         deps d
                   where cer.cost_event_process_id = I_cost_event_process_id
                     and (fc.item              = cer.item or
                          fc.item_parent       = cer.item or
                          fc.item_grandparent  = cer.item)
                     and fc.active_date   <= cer.reclass_date
                     and cer.to_dept      = d.dept
                     and d.group_no       = g.group_no) inner
            where inner.ranking = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.reclass_reclass_date)
   when matched then
   update
      set fc.reclass_no = use_this.reclass_reclass_no,
          fc.division   = use_this.reclass_division,
          fc.group_no   = use_this.reclass_group_no,
          fc.dept       = use_this.reclass_to_dept,
          fc.class      = use_this.reclass_to_class,
          fc.subclass   = use_this.reclass_to_subclass
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.reclass_reclass_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.reclass_division,
           use_this.reclass_group_no,
           use_this.reclass_to_dept,
           use_this.reclass_to_class,
           use_this.reclass_to_subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.MERGE_RECLASS',
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_RECLASS;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_MERGE_MERCH_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                  I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_MERGE_MERCH_HIER';

BEGIN

   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          nvl(mh.new_division, fc.division),
          nvl(mh.new_group_no, fc.group_no),
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from cost_event_merch_hier mh,
          cost_event_thread ct,
          future_cost fc
    where mh.cost_event_process_id = I_cost_event_process_id
      and fc.division              = nvl(mh.old_division, fc.division)
      and fc.group_no              = mh.old_group_no
      and fc.dept                  = nvl(mh.dept, fc.dept)
      and ct.cost_event_process_id = mh.cost_event_process_id
      and ct.thread_id             = I_thread_id
      and ct.item                  = fc.item
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location;


   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   if MERGE_DEAL(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_MERGE_MERCH_HIER;
-------------------------------------------------------------------------------- --------
FUNCTION EXPLODE_MERGE_ORG_HIER(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_MERGE_ORG_HIER';

BEGIN

   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          decode(fc.loc_type,'W',oh.new_chain,nvl(oh.new_chain, fc.chain)),
          decode(fc.loc_type,'W',oh.new_area,nvl(oh.new_area, fc.area)),
          decode(fc.loc_type,'W',oh.new_region,nvl(oh.new_region, fc.region)),
          decode(fc.loc_type,'W',oh.new_district,nvl(oh.new_district, fc.district)),
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from cost_event_org_hier oh,
          cost_event_thread ct,
          future_cost fc
    where oh.cost_event_process_id = I_cost_event_process_id
      and nvl(fc.chain,-999)       = nvl(oh.old_chain, nvl(fc.chain,-999))
      and nvl(fc.area,-999)        = nvl(oh.old_area, nvl(fc.area,-999))
      and nvl(fc.region,-999)      = nvl(oh.old_region, nvl(fc.region,-999))
      and nvl(fc.district,-999)    = nvl(oh.old_district, nvl(fc.district,-999))
      and (fc.loc_type = 'W'
           and exists (select 'x'
                         from wh
                        where wh = fc.location
                          and physical_wh = oh.location)
           or ( fc.location  = nvl(oh.location, fc.location)))
      and ct.cost_event_process_id = oh.cost_event_process_id
      and ct.thread_id             = I_thread_id
      and ct.item                  = fc.item
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location;
      
   --add a new row on Vdate into the future_cost_gtt
   if ELC_DATE_TO_FC(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE then
      return FALSE;
   end if;
   

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   --add deals
   if MERGE_DEAL(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_MERGE_ORG_HIER;
-------------------------------------------------------------------------------- --------
FUNCTION EXPLODE_MERGE_SUPP_HIER(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(100)   := 'FUTURE_COST_SQL.EXPLODE_MERGE_SUPP_HIER';

BEGIN

   insert into future_cost_gtt  (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 active_date,
                                 base_cost,
                                 net_cost,
                                 net_net_cost,
                                 dead_net_net_cost,
                                 pricing_cost,
                                 acquisition_cost,
                                 calc_date,
                                 start_ind,
                                 primary_supp_country_ind,
                                 currency_code,
                                 division,
                                 group_no,
                                 dept,
                                 class,
                                 subclass,
                                 item_grandparent,
                                 item_parent,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 chain,
                                 area,
                                 region,
                                 district,
                                 supp_hier_lvl_1,
                                 supp_hier_lvl_2,
                                 supp_hier_lvl_3,
                                 reclass_no,
                                 cost_change,
                                 simple_pack_ind,
                                 primary_cost_pack,
                                 primary_cost_pack_qty,
                                 store_type,
                                 costing_loc,
                                 templ_id,
                                 passthru_pct,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 wac_tax,
                                 default_costing_type,
                                 processing_seq_no)
   select fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          sh.new_supp_hier_lvl_1,
          sh.new_supp_hier_lvl_2,
          sh.new_supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from cost_event_supp_hier sh,
          cost_event_thread ct,
          future_cost fc
    where sh.cost_event_process_id  = I_cost_event_process_id
      and fc.item                   = sh.item
      and fc.supplier               = sh.supplier
      and fc.origin_country_id      = sh.origin_country_id
      and fc.location               = sh.location
      and ct.cost_event_process_id  = sh.cost_event_process_id
      and ct.thread_id              = I_thread_id
      and ct.item                   = fc.item
      and ct.supplier               = fc.supplier
      and ct.origin_country_id      = fc.origin_country_id
      and ct.location               = fc.location
    union
   select fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.new_active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          sh.new_supp_hier_lvl_1,
          sh.new_supp_hier_lvl_2,
          sh.new_supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          NULL as processing_seq_no
     from cost_event_supp_hier sh,
          cost_event_thread ct,
          (select *
             from (select f_c.*,
                          LP_vdate new_active_date,
                          rank() over (partition by f_c.item,
                                                    f_c.supplier,
                                                    f_c.origin_country_id,
                                                    f_c.location
                                           order by f_c.active_date desc) ranking
                     from future_cost f_c
                    where f_c.active_date <= LP_vdate) inner
            where inner.ranking = 1) fc
    where sh.cost_event_process_id = I_cost_event_process_id
      and fc.item = sh.item
      and fc.supplier = sh.supplier
      and fc.origin_country_id = sh.origin_country_id
      and fc.location = sh.location
      and ct.cost_event_process_id = sh.cost_event_process_id
      and ct.thread_id = I_thread_id
      and ct.item = fc.item
      and ct.supplier = fc.supplier
      and ct.origin_country_id = fc.origin_country_id
      and ct.location = fc.location;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   if MERGE_DEAL(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_MERGE_SUPP_HIER;

----------------------------------------------------------------------------------------
FUNCTION EXPLODE_ELC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_ELC';

BEGIN

   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select distinct
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          LP_VDATE,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from future_cost fc,
          cost_event_thread ct
    where ct.cost_event_process_id  = I_cost_event_process_id
      and ct.thread_id              = I_thread_id
      and ct.item                   = fc.item
      and ct.supplier               = fc.supplier
      and ct.origin_country_id      = fc.origin_country_id
      and ct.location               = fc.location;

   if ELC_DATE_TO_FC(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE then
      return FALSE;
   end if;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   --seed future cost deal gtt
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_ELC;
-----------------------------------------------------------------------------------------
FUNCTION ELC_DATE_TO_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN is

   L_program   VARCHAR2(100) := 'FUTURE_COST_SQL.ELC_DATE_TO_FC';

BEGIN

   merge into future_cost_gtt fc
   using ( select distinct * from (
                           select fc.*,
                                  LP_vdate elc_date, --the day the elc is active
                                  rank() over (partition by fc.item,
                                                            fc.supplier,
                                                            fc.origin_country_id,
                                                            fc.location
                                                   order by fc.active_date desc) ranking
                             from future_cost_gtt fc
                            where fc.active_date          <=  LP_vdate) inner
            where inner.ranking = 1) use_this
   on (fc.item                      = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.elc_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           acquisition_cost,
           elc_amt,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           templ_id,
           passthru_pct,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type,
           processing_seq_no)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.elc_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           'Y',
           use_this.acquisition_cost,
           use_this.elc_amt,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           NULL,
           NULL,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.templ_id,
           use_this.passthru_pct,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type,
           use_this.processing_seq_no);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ELC_DATE_TO_FC;
-------------------------------------------------------------------------------- --------
FUNCTION EXPLODE_COST_ZONE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_COST_ZONE';

BEGIN

   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from future_cost fc,
          cost_event_thread ct
    where ct.cost_event_process_id  = I_cost_event_process_id
      and ct.thread_id              = I_thread_id
      and ct.item                   = fc.item
      and ct.supplier               = fc.supplier
      and ct.origin_country_id      = fc.origin_country_id
      and ct.location               = fc.location;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   --load deals
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_COST_ZONE;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_ITEM_CZG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_ITEM_CZG';

BEGIN

   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   calc_date,
                                   start_ind,
                                   acquisition_cost,
                                   elc_amt,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_parent,
                                   item_grandparent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
            select fc.item,
                   fc.supplier,
                   fc.origin_country_id,
                   fc.location,
                   fc.loc_type,
                   fc.active_date,
                   fc.base_cost,
                   fc.net_cost,
                   fc.net_net_cost,
                   fc.dead_net_net_cost,
                   fc.pricing_cost,
                   fc.calc_date,
                   fc.start_ind,
                   fc.acquisition_cost,
                   fc.elc_amt,
                   fc.primary_supp_country_ind,
                   fc.currency_code,
                   fc.division,
                   fc.group_no,
                   fc.dept,
                   fc.class,
                   fc.subclass,
                   fc.item_parent,
                   fc.item_grandparent,
                   fc.diff_1,
                   fc.diff_2,
                   fc.diff_3,
                   fc.diff_4,
                   fc.chain,
                   fc.area,
                   fc.region,
                   fc.district,
                   fc.supp_hier_lvl_1,
                   fc.supp_hier_lvl_2,
                   fc.supp_hier_lvl_3,
                   fc.reclass_no,
                   fc.cost_change,
                   fc.simple_pack_ind,
                   fc.primary_cost_pack,
                   fc.primary_cost_pack_qty,
                   fc.store_type,
                   fc.costing_loc,
                   fc.templ_id,
                   fc.passthru_pct,
                   fc.negotiated_item_cost,
                   fc.extended_base_cost,
                   fc.wac_tax,
                   fc.default_costing_type,
          null as processing_seq_no
     from cost_event_thread ct,
          future_cost fc
    where ct.cost_event_process_id  = I_cost_event_process_id
      and ct.thread_id              = I_thread_id
      and ct.item                   = fc.item
      and ct.supplier               = fc.supplier
      and ct.origin_country_id      = fc.origin_country_id
      and ct.location               = fc.location;
      
   --add a new row on Vdate into the future_cost_gtt
   if ELC_DATE_TO_FC(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE THEN
      return FALSE;
   end if;   
   

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   --load deals
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_ITEM_CZG;
-------------------------------------------------------------------------------- --------
FUNCTION EXPLODE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_action                IN     COST_EVENT.ACTION%TYPE,
                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   LP_action := I_action;

   if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
      if REMOVE_DEAL(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE then
         return FALSE;
      end if;
   elsif I_action = FUTURE_COST_EVENT_SQL.ADD_EVENT then
      if ADD_DEAL(O_error_message,
                  I_cost_event_process_id,
                  I_thread_id) = FALSE then
         return FALSE;
      end if;
   elsif I_action = FUTURE_COST_EVENT_SQL.MODIFY_EVENT then
      if MODIFY_DEAL(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.EXPLODE_DEAL',
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_DEAL;
----------------------------------------------------------------------------------------

FUNCTION EXPLODE_PRIMARY_PACK_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                   I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_PRIMARY_PACK_COST';

BEGIN

   --get the item/locs for the event -- get right pack info on the rows
   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   elc_amt,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select distinct
          fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.elc_amt,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          cpp.pack_no,
          pi.pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from future_cost fc,
          cost_event_prim_pack cpp,
          cost_event_thread ct,
          packitem pi
    where cpp.cost_event_process_id  = I_cost_event_process_id
      and cpp.item                   = fc.item
      and cpp.pack_no                = pi.pack_no(+)
      and ct.cost_event_process_id   = cpp.cost_event_process_id
      and ct.thread_id               = I_thread_id
      and ct.item                    = fc.item
      and ct.supplier                = fc.supplier
      and ct.origin_country_id       = fc.origin_country_id
      and ct.location                = fc.location
      and fc.active_date            >= LP_vdate
    union
   select fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.new_active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.elc_amt,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          cpp.pack_no,
          pi.pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          NULL as processing_seq_no
     from cost_event_prim_pack cpp,
          packitem pi,
          cost_event_thread ct,
          (select *
             from (select f_c.*,
                          LP_vdate new_active_date,
                          rank() over (partition by f_c.item,
                                                    f_c.supplier,
                                                    f_c.origin_country_id,
                                                    f_c.location
                                           order by f_c.active_date desc) ranking
                     from future_cost f_c
                    where f_c.active_date <= LP_vdate) inner
            where inner.ranking = 1) fc
    where cpp.cost_event_process_id  = I_cost_event_process_id
      and cpp.item                   = fc.item
      and cpp.pack_no                = pi.pack_no(+)
      and ct.cost_event_process_id   = cpp.cost_event_process_id
      and ct.thread_id               = I_thread_id
      and ct.item                    = fc.item
      and ct.supplier                = fc.supplier
      and ct.origin_country_id       = fc.origin_country_id
      and ct.location                = fc.location;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   --if pack is being removed(or if pack is not supplier by sup/cntry/loc),
   --delete all but the first record, this will act as a seed record

   if I_persist_ind = 'Y' then

      delete from future_cost fc
       where fc.active_date > LP_vdate 
         and (fc.item, fc.supplier, fc.origin_country_id, fc.location) in
          (select inner.item, inner.supplier, inner.origin_country_id, inner.location
             from (select gtt2.*,
                          rank() over(partition by gtt2.item,
                                                   gtt2.supplier,
                                                   gtt2.origin_country_id,
                                                   gtt2.location
                                          order by gtt2.active_date) date_rank
                     from future_cost_gtt gtt2
                    where gtt2.primary_cost_pack is null
                       or not exists(select 'x'
                                       from future_cost fc_pack
                                      where fc_pack.item                  = gtt2.primary_cost_pack
                                        and fc_pack.supplier              = gtt2.supplier
                                        and fc_pack.origin_country_id     = gtt2.origin_country_id
                                        and fc_pack.location              = gtt2.location)
                  ) inner
            where inner.date_rank != 1);
   end if;
   
   delete from future_cost_gtt gtt
    where (rowid) in
       (select inner.rowid
          from (select gtt2.rowid,
                       rank() over(partition by gtt2.item,
                                                gtt2.supplier,
                                                gtt2.origin_country_id,
                                                gtt2.location
                                       order by gtt2.active_date) date_rank
                  from future_cost_gtt gtt2
                 where gtt2.primary_cost_pack is null
                    or not exists(select 'x'
                                    from future_cost fc_pack
                                   where fc_pack.item                  = gtt2.primary_cost_pack
                                     and fc_pack.supplier              = gtt2.supplier
                                     and fc_pack.origin_country_id     = gtt2.origin_country_id
                                     and fc_pack.location              = gtt2.location)
               ) inner
         where inner.date_rank != 1);

   --reset the cost based on item_supp_country_loc for our seed record
   --when a primary_cost_pack is being removed
   merge into  future_cost_gtt gtt
   using (select iscl.item,
                 iscl.supplier,
                 iscl.origin_country_id,
                 iscl.loc,
                 NVL(iscl.base_cost ,0) base_cost
            from item_supp_country_loc iscl,
                 future_cost_gtt gtt
           where iscl.item                 = gtt.item
             and iscl.supplier             = gtt.supplier
             and iscl.origin_country_id    = gtt.origin_country_id
             and iscl.loc                  = gtt.location
             and (gtt.primary_cost_pack is null or
                  not exists(select 'x'
                               from future_cost fc_pack
                              where fc_pack.item                  = gtt.primary_cost_pack
                                and fc_pack.supplier              = gtt.supplier
                                and fc_pack.origin_country_id     = gtt.origin_country_id
                                and fc_pack.location              = gtt.location))) use_this
   on (     gtt.item                  = use_this.item
        and gtt.supplier              = use_this.supplier
        and gtt.origin_country_id     = use_this.origin_country_id
        and gtt.location              = use_this.loc
   )
   when matched then
      update set gtt.base_cost         = use_this.base_cost,
                 gtt.net_cost          = use_this.base_cost,
                 gtt.net_net_cost      = use_this.base_cost,
                 gtt.dead_net_net_cost = use_this.base_cost,
                 gtt.pricing_cost      = use_this.base_cost,
                 gtt.elc_amt        = 0;

   --if pack is being removed look up reclassifications that need to be applied
   --does for every thing, but will be cleaned up in SYNC_PRIMAY_PACK
   if MERGE_NIL_RECLASS(O_error_message,
                        I_cost_event_process_id,
                        I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   --if pack is being removed look up cost changes that need to be applied
   --does for every thing, but will be cleaned up in SYNC_PRIMAY_PACK
   if MERGE_NIL_CC(O_error_message,
                   I_cost_event_process_id,
                   I_thread_id) = FALSE THEN
      return FALSE;
   end if;
   ---
   --packs do not need NIL type logic as the deals are maintained at item level even when
   --a primary costing pack is setup.
   ---
   --load deals
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   --if pack is being removed then add future_cost records for start and close+1 dates
   if DEAL_DATES_TO_FC(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
     return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_PRIMARY_PACK_COST;
----------------------------------------------------------------------------------------
FUNCTION REMOVE_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.REMOVE_DEAL';

BEGIN

   --get fc records covered by the deal being removed into the fc_gtt
   if LOAD_FUTURE_COST_FOR_DEALS(O_error_message,
                                 I_cost_event_process_id,
                                 I_thread_id) = FALSE then
      return FALSE;
   end if;
   ---
   --get the future cost deal records affected by the future cost records populated
   -- in first insert/selectmove over existing deal records to the deal gtt.
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;
   ---
   delete from deal_item_loc_explode_gtt
    where deal_id in (select deal_id
                        from cost_event_deal
                       where cost_event_process_id = I_cost_event_process_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END REMOVE_DEAL;
-------------------------------------------------------------------------------- --------
FUNCTION ADD_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                  I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(100) := 'FUTURE_COST_SQL.ADD_DEAL';

BEGIN

   -- insert details of deal being processed to the deal gtt
   insert into deal_item_loc_explode_gtt (
                item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                deal_id,
                deal_detail_id,
                active_date,
                close_date,
                cost_appl_ind,
                price_cost_appl_ind,
                deal_class,
                threshold_value_type,
                qty_thresh_buy_item,
                qty_thresh_get_type,
                qty_thresh_get_value,
                qty_thresh_buy_qty,
                qty_thresh_recur_ind,
                qty_thresh_buy_target,
                qty_thresh_buy_avg_loc,
                qty_thresh_get_item,
                qty_thresh_get_qty,
                qty_thresh_free_item_unit_cost,
                setup_merch_level,
                setup_division,
                setup_group_no,
                setup_dept,
                setup_class,
                setup_subclass,
                setup_item_parent,
                setup_item_grandparent,
                setup_diff_1,
                setup_diff_2,
                setup_diff_3,
                setup_diff_4,
                setup_org_level,
                setup_chain,
                setup_area,
                setup_region,
                setup_district,
                setup_location,
                deal_head_type,
                partner_type,
                partner_id,
                create_datetime,
                deal_detail_application_order,
                get_free_discount,
                excl_ind)
         select dile.*, 'N'
           from cost_event_deal cd,
                cost_event_thread ct,
                deal_item_loc_explode dile
          where cd.cost_event_process_id = I_cost_event_process_id
            and ct.cost_event_process_id = cd.cost_event_process_id
            and ct.thread_id             = I_thread_id
            and ct.item                  = dile.item
            and ct.supplier              = dile.supplier
            and ct.origin_country_id     = dile.origin_country_id
            and ct.location              = dile.location
            and cd.deal_id               = dile.deal_id;

   -- get fc records covered by the new deal into the fc_gtt
   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select distinct fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.templ_id,
          fc.passthru_pct,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from deal_item_loc_explode_gtt d,
          future_cost fc
    where fc.item                  = d.item
      and fc.supplier              = d.supplier
      and fc.origin_country_id     = d.origin_country_id
      and fc.location              = d.location
      and fc.active_date      >= ( select max(active_date) 
                                   from future_cost fc1 
                                   where fc1.item      =  fc.item 
                                   and fc1.supplier  =  fc.supplier
                                   and fc1.origin_country_id = fc.origin_country_id
                                   and fc1.location  =  fc.location
                                   and fc1.active_date <= LP_vdate );
                                                                            
       if LOCK_FUTURE_COST(O_error_message,
          I_cost_event_process_id,
          I_thread_id) = FALSE then
      return FALSE;
   end if;

   -- move over existing deal records to the deal gtt.
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   -- get a row into fc for the start and end (day after end) of the new deal
   -- and affected deals for roll forward to process
   if DEAL_DATES_TO_FC(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
     return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_DEAL;
--------------------------------------------------------------------------------
FUNCTION MODIFY_DEAL(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.MODIFY_DEAL';

BEGIN

   --get fc records covered by the deal being modified into the fc_gtt
   if LOAD_FUTURE_COST_FOR_DEALS(O_error_message,
                                 I_cost_event_process_id,
                                 I_thread_id) = FALSE then
      return FALSE;
   end if;
   ---
   --get the future cost deal records affected by the future cost records populated
   --in first insert/select move over existing deal records to the deal gtt.
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;
   ---
   --update deal close date
   update deal_item_loc_explode_gtt fcd
      set fcd.close_date = (select dh.close_date
                              from deal_head dh,
                                   cost_event_deal cd
                             where cd.cost_event_process_id = I_cost_event_process_id
                               and cd.deal_id               = dh.deal_id
                               and dh.deal_id               = fcd.deal_id)
    where fcd.deal_id   in  (select cd.deal_id
                               from cost_event_deal cd
                              where cd.cost_event_process_id = I_cost_event_process_id);
   ---
   -- get a row into fc for the end (day after end) of the modified deal.
   if DEAL_DATES_TO_FC(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
     return FALSE;
   end if;

    -- get a row into fc on the day deal threshold is edited.
    if DEAL_THLD_CHG_DATE_TO_FC(O_error_message,
                                I_cost_event_process_id,
                                I_thread_id) = FALSE then
      return FALSE;
    end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MODIFY_DEAL;
--------------------------------------------------------------------------------
FUNCTION DEAL_DATES_TO_FC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   --get a row into fc for the start and end (day after end) of all the deals
   -- affected by the current deal for roll forward to process
   merge /*+ index(fc, PK_FUTURE_COST_GTT) */ into future_cost_gtt fc 
   using ( select /*+ LEADING(fcd) index(fc, PK_FUTURE_COST_GTT) */ distinct * from ( 

                    select fc.*,
                         'Y' deal_start_ind,
                         CASE WHEN fcd.active_date < LP_vdate then LP_vdate 
                              ELSE fcd.active_date END deal_date, --the day the deal starts
                         rank()
                           over (partition by fcd.deal_id,
                                              fcd.deal_detail_id,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from future_cost_gtt fc,
                         deal_item_loc_explode_gtt fcd
                   where fc.item              = fcd.item
                     and fc.supplier          = fcd.supplier
                     and fc.origin_country_id = fcd.origin_country_id
                     and fc.location          = fcd.location
                     and fcd.deal_head_type  != 'O' -- Exclude PO specific deals
                     and fc.active_date      <= greatest(fcd.active_date, LP_vdate)) inner
            where inner.ranking = 1
              and inner.deal_date >= LP_vdate) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.deal_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.deal_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           use_this.deal_start_ind,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           null,
           null,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   merge into future_cost_gtt fc
   using ( select distinct * from (
                  select fc.*,
                         'N' deal_start_ind,
                         fcd.close_date+1 deal_date, --the day after the  deal ends
                         rank()
                           over (partition by fcd.deal_id,
                                              fcd.deal_detail_id,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from future_cost_gtt fc,
                         deal_item_loc_explode_gtt fcd
                   where fc.item              = fcd.item
                     and fc.supplier          = fcd.supplier
                     and fc.origin_country_id = fcd.origin_country_id
                     and fc.location          = fcd.location
                     and fcd.deal_head_type  != 'O' -- Exclude PO specific deals
                     and fc.active_date      <= fcd.close_date) inner
            where inner.ranking = 1
              and inner.deal_date >= LP_vdate) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.deal_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.deal_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           use_this.deal_start_ind,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           null,
           null,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.DEAL_DATES_TO_FC',
                                            to_char(SQLCODE));
      return FALSE;
END DEAL_DATES_TO_FC;
--------------------------------------------------------------------------------
FUNCTION DEAL_THLD_CHG_DATE_TO_FC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                  I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE)

RETURN BOOLEAN is

BEGIN

 merge into future_cost_gtt fc
   using ( select distinct * from (
                  select fc.*,
                         LP_vdate deal_edit_date, --the day when deal threshold is edited
                         rank()
                           over (partition by fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from future_cost_gtt fc
                   where fc.active_date <=  LP_vdate
                     and exists (select 'X'
                                   from cost_event_deal ced,
                                        deal_head dh,
                                        deal_threshold_rev dtr
                                  where ced.cost_event_process_id = I_cost_event_process_id
                                    and ced.deal_id = dh.deal_id
                                    and ced.deal_id = dtr.deal_id
                                    and dtr.revision_date = LP_vdate
                                    and dh.active_date < LP_vdate)) inner
            where inner.ranking = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.deal_edit_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.deal_edit_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           LP_vdate,
           'N',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           null,
           null,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc);

    return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.DEAL_THLD_CHG_DATE_TO_FC',
                                            to_char(SQLCODE));
      return FALSE;
end DEAL_THLD_CHG_DATE_TO_FC;
--------------------------------------------------------------------------------
FUNCTION LOAD_DEALS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(100) := 'FUTURE_COST_SQL.LOAD_DEALS';

BEGIN

   -- move over existing deal records to the deal gtt.
   insert into deal_item_loc_explode_gtt (
                item,
                supplier,
                origin_country_id,
                location,
                loc_type,
                deal_id,
                deal_detail_id,
                active_date,
                close_date,
                cost_appl_ind,
                price_cost_appl_ind,
                deal_class,
                threshold_value_type,
                qty_thresh_buy_item,
                qty_thresh_get_type,
                qty_thresh_get_value,
                qty_thresh_buy_qty,
                qty_thresh_recur_ind,
                qty_thresh_buy_target,
                qty_thresh_buy_avg_loc,
                qty_thresh_get_item,
                qty_thresh_get_qty,
                qty_thresh_free_item_unit_cost,
                setup_merch_level,
                setup_division,
                setup_group_no,
                setup_dept,
                setup_class,
                setup_subclass,
                setup_item_parent,
                setup_item_grandparent,
                setup_diff_1,
                setup_diff_2,
                setup_diff_3,
                setup_diff_4,
                setup_org_level,
                setup_chain,
                setup_area,
                setup_region,
                setup_district,
                setup_location,
                deal_head_type,
                partner_type,
                partner_id,
                create_datetime,
                deal_detail_application_order,
                get_free_discount,
                excl_ind
                )
      select distinct deals.*,'N'
        from future_cost_gtt il,
             deal_item_loc_explode deals
       where il.item                  = deals.item
         and il.supplier              = deals.supplier
         and il.origin_country_id     = deals.origin_country_id
         and il.location              = deals.location
         --To exclude already existing records if function was called by add_deal function
         and (LP_action           != FUTURE_COST_EVENT_SQL.ADD_EVENT
               or not exists (select 'x'
                                from cost_event_deal cd
                               where cd.cost_event_process_id = I_cost_event_process_id
                                 and cd.deal_id               = deals.deal_id));

      if LP_call <> 'MDLC' then
         merge into deal_item_loc_explode_gtt fcd
         using
          (select distinct gtt.item,
                  gtt.supplier,
                  gtt.origin_country_id,
                  gtt.location,
                  gtt.loc_type,
                  dd.deal_id,
                  dd.deal_detail_id,
                  dh.active_date deal_active_date,
                  dh.close_date deal_close_date,
                  dd.cost_appl_ind,
                  dd.price_cost_appl_ind,
                  dd.deal_class,
                  dd.threshold_value_type,
                  dd.qty_thresh_buy_item,
                  dd.qty_thresh_get_type,
                  dd.qty_thresh_get_value,
                  dd.qty_thresh_buy_qty,
                  dd.qty_thresh_recur_ind,
                  dd.qty_thresh_buy_target,
                  dd.qty_thresh_buy_avg_loc,
                  dd.qty_thresh_get_item,
                  dd.qty_thresh_get_qty,
                  dd.qty_thresh_free_item_unit_cost,
                  dil.merch_level setup_merch_level,
                  DECODE(dil.merch_level, 2, dil.division, NULL) setup_division,
                  DECODE(dil.merch_level, 3, dil.group_no, NULL) setup_group_no,
                  NULL setup_dept,
                  NULL setup_class,
                  NULL setup_subclass,
                  NULL setup_item_parent,
                  NULL setup_item_grandparent,
                  NULL setup_diff_1,
                  NULL setup_diff_2,
                  NULL setup_diff_3,
                  NULL setup_diff_4,
                  dil.org_level setup_org_level,
                  DECODE(dil.org_level, 1, dil.chain,    NULL) setup_chain,
                  DECODE(dil.org_level, 2, dil.area,     NULL) setup_area,
                  DECODE(dil.org_level, 3, dil.region,   NULL) setup_region,
                  DECODE(dil.org_level, 4, dil.district, NULL) setup_district,
                  DECODE(gtt.loc_type,'W',wh.physical_wh,DECODE(dil.org_level, 5, dil.location, NULL)) setup_location,
                  dh.type deal_head_type,
                  dh.partner_type,
                  dh.partner_id,
                  dh.create_datetime,
                  dd.application_order deal_detail_application_order,
                  dd.get_free_discount,
                  dil.excl_ind
                  --
             from deal_head dh,
                  deal_detail dd,
                  deal_itemloc_div_grp dil,
                  future_cost_gtt gtt,
                  wh
            where dh.status = 'A'
              and dh.type in('A', 'P')
              and (dh.close_date is NULL or dh.close_date >= gtt.active_date)
              and dh.deal_id        = dd.deal_id
              and dd.deal_id        = dil.deal_id
              and dd.deal_detail_id = dil.deal_detail_id
              and dil.active_ind    = 'Y'
              and gtt.origin_country_id = NVL(dil.origin_country_id,gtt.origin_country_id)
              -- Merch level matches
              and (
                   (dil.merch_level = 1)
                   or
                   (    dil.merch_level  in (2,3)
                    and gtt.division     = NVL(dil.division, gtt.division)
                    and gtt.group_no     = NVL(dil.group_no, gtt.group_no)))
              -- Org level matches
              and gtt.location = wh.wh(+)
              and ((NVL(dil.org_level, 0) = 0)
                    or
                   ( NVL(dil.org_level, 0)             in (1,2,3,4,5)
                    and NVL(gtt.chain,-999)               = NVL(dil.chain, NVL(gtt.chain,-999))
                    and NVL(gtt.area,-999)                = NVL(dil.area, NVL(gtt.area,-999))
                    and NVL(gtt.region,-999)              = NVL(dil.region, NVL(gtt.region,-999))
                    and NVL(gtt.district,-999)            = NVL(dil.district, NVL(gtt.district,-999))
                    and NVL(wh.physical_wh, gtt.location) = NVL(dil.location, NVL(wh.physical_wh, gtt.location))))
              -- Supplier hier level matches
              and (   (dh.partner_type = 'S'  and gtt.supplier in (select supplier
                                                                     from sups
                                                                    where supplier_parent = dh.supplier
                                                                       or supplier = dh.supplier))
                   or (dh.partner_type = 'S1' and gtt.supp_hier_lvl_1 = dh.partner_id)
                   or (dh.partner_type = 'S2' and gtt.supp_hier_lvl_2 = dh.partner_id)
                   or (dh.partner_type = 'S3' and gtt.supp_hier_lvl_3 = dh.partner_id))
           union
           select distinct gtt.item,
                  gtt.supplier,
                  gtt.origin_country_id,
                  gtt.location,
                  gtt.loc_type,
                  dd.deal_id,
                  dd.deal_detail_id,
                  dh.active_date deal_active_date,
                  dh.close_date deal_close_date,
                  dd.cost_appl_ind,
                  dd.price_cost_appl_ind,
                  dd.deal_class,
                  dd.threshold_value_type,
                  dd.qty_thresh_buy_item,
                  dd.qty_thresh_get_type,
                  dd.qty_thresh_get_value,
                  dd.qty_thresh_buy_qty,
                  dd.qty_thresh_recur_ind,
                  dd.qty_thresh_buy_target,
                  dd.qty_thresh_buy_avg_loc,
                  dd.qty_thresh_get_item,
                  dd.qty_thresh_get_qty,
                  dd.qty_thresh_free_item_unit_cost,
                  dil.merch_level setup_merch_level,
                  NULL setup_division,
                  NULL setup_group_no,
                  dil.dept setup_dept,
                  dil.class setup_class,
                  dil.subclass setup_subclass,
                  NULL setup_item_parent,
                  NULL setup_item_grandparent,
                  NULL setup_diff_1,
                  NULL setup_diff_2,
                  NULL setup_diff_3,
                  NULL setup_diff_4,
                  dil.org_level setup_org_level,
                  DECODE(dil.org_level, 1, dil.chain,    NULL) setup_chain,
                  DECODE(dil.org_level, 2, dil.area,     NULL) setup_area,
                  DECODE(dil.org_level, 3, dil.region,   NULL) setup_region,
                  DECODE(dil.org_level, 4, dil.district, NULL) setup_district,
                  DECODE(gtt.loc_type,'W',wh.physical_wh,DECODE(dil.org_level, 5, dil.location, NULL)) setup_location,
                  dh.type deal_head_type,
                  dh.partner_type,
                  dh.partner_id,
                  dh.create_datetime,
                  dd.application_order deal_detail_application_order,
                  dd.get_free_discount,
                  dil.excl_ind
                  --
             from deal_head dh,
                  deal_detail dd,
                  deal_itemloc_dcs dil,
                  future_cost_gtt gtt,
                  wh
            where dh.status = 'A'
              and dh.type in('A', 'P')
              and (dh.close_date is null or dh.close_date >= gtt.active_date)
              and dh.deal_id        = dd.deal_id
              and dd.deal_id        = dil.deal_id
              and dd.deal_detail_id = dil.deal_detail_id
              and dil.active_ind    = 'Y'
              and gtt.origin_country_id = NVL(dil.origin_country_id,gtt.origin_country_id)
              -- Merch level matches
              and dil.merch_level in (4,5,6)
              and gtt.dept = dil.dept
              and gtt.class = NVL(dil.class, gtt.class)
              and gtt.subclass = NVL(dil.subclass, gtt.subclass)
              -- Org level matches
              and gtt.location = wh.wh(+)
              and ((NVL(dil.org_level, 0) = 0)
                    or
                   (    NVL(dil.org_level, 0)             in (1,2,3,4,5)
                    and NVL(gtt.chain,-999)                         = NVL(dil.chain, nvl(gtt.chain,-999))
                    and NVL(gtt.area,-999)                          = NVL(dil.area, nvl(gtt.area,-999))
                    and NVL(gtt.region,-999)                        = NVL(dil.region, nvl(gtt.region,-999))
                    and NVL(gtt.district,-999)                      = NVL(dil.district, nvl(gtt.district,-999))
                    and NVL(wh.physical_wh, gtt.location) = NVL(dil.location, NVL(wh.physical_wh, gtt.location))))
              -- Supplier hier level matches
              and (   (dh.partner_type = 'S'  and gtt.supplier in (select supplier
                                                                    from sups
                                                                   where supplier_parent = dh.supplier
                                                                      or supplier = dh.supplier))
                   or (dh.partner_type = 'S1' and gtt.supp_hier_lvl_1 = dh.partner_id)
                   or (dh.partner_type = 'S2' and gtt.supp_hier_lvl_2 = dh.partner_id)
                   or (dh.partner_type = 'S3' and gtt.supp_hier_lvl_3 = dh.partner_id))) use_this
          on (fcd.item                         = use_this.item
              and fcd.supplier                 = use_this.supplier
              and fcd.origin_country_id        = use_this.origin_country_id
              and fcd.location                 = use_this.location
              and fcd.deal_id                  = use_this.deal_id
              and fcd.deal_detail_id           = use_this.deal_detail_id
              and NVL(fcd.setup_division,-999) = NVL(use_this.setup_division,-999)
              and NVL(fcd.setup_group_no,-999) = NVL(use_this.setup_group_no,-999)
              and NVL(fcd.setup_dept,-999)     = NVL(use_this.setup_dept,-999)
              and NVL(fcd.setup_class,-999)    = NVL(use_this.setup_class,-999)
              and NVL(fcd.setup_subclass,-999) = NVL(use_this.setup_subclass,-999))
         when not matched then
              insert (item,
                      supplier,
                      origin_country_id,
                      location,
                      loc_type,
                      deal_id,
                      deal_detail_id,
                      active_date,
                      close_date,
                      cost_appl_ind,
                      price_cost_appl_ind,
                      deal_class,
                      threshold_value_type,
                      qty_thresh_buy_item,
                      qty_thresh_get_type,
                      qty_thresh_get_value,
                      qty_thresh_buy_qty,
                      qty_thresh_recur_ind,
                      qty_thresh_buy_target,
                      qty_thresh_buy_avg_loc,
                      qty_thresh_get_item,
                      qty_thresh_get_qty,
                      qty_thresh_free_item_unit_cost,
                      --
                      setup_merch_level,
                      setup_division,
                      setup_group_no,
                      setup_dept,
                      setup_class,
                      setup_subclass,
                      setup_item_parent,
                      setup_item_grandparent,
                      setup_diff_1,
                      setup_diff_2,
                      setup_diff_3,
                      setup_diff_4,
                      setup_org_level,
                      setup_chain,
                      setup_area,
                      setup_region,
                      setup_district,
                      setup_location,
                      --
                      deal_head_type,
                      partner_type,
                      partner_id,
                      create_datetime,
                      deal_detail_application_order,
                      get_free_discount,
                      excl_ind,
                      --
                      retain_ind)
              values (use_this.item,
                      use_this.supplier,
                      use_this.origin_country_id,
                      use_this.location,
                      use_this.loc_type,
                      use_this.deal_id,
                      use_this.deal_detail_id,
                      use_this.deal_active_date,
                      use_this.deal_close_date,
                      use_this.cost_appl_ind,
                      use_this.price_cost_appl_ind,
                      use_this.deal_class,
                      use_this.threshold_value_type,
                      use_this.qty_thresh_buy_item,
                      use_this.qty_thresh_get_type,
                      use_this.qty_thresh_get_value,
                      use_this.qty_thresh_buy_qty,
                      use_this.qty_thresh_recur_ind,
                      use_this.qty_thresh_buy_target,
                      use_this.qty_thresh_buy_avg_loc,
                      use_this.qty_thresh_get_item,
                      use_this.qty_thresh_get_qty,
                      use_this.qty_thresh_free_item_unit_cost,
                      --
                      use_this.setup_merch_level,
                      use_this.setup_division,
                      use_this.setup_group_no,
                      use_this.setup_dept,
                      use_this.setup_class,
                      use_this.setup_subclass,
                      use_this.setup_item_parent,
                      use_this.setup_item_grandparent,
                      use_this.setup_diff_1,
                      use_this.setup_diff_2,
                      use_this.setup_diff_3,
                      use_this.setup_diff_4,
                      use_this.setup_org_level,
                      use_this.setup_chain,
                      use_this.setup_area,
                      use_this.setup_region,
                      use_this.setup_district,
                      use_this.setup_location,
                      --
                      use_this.deal_head_type,
                      use_this.partner_type,
                      use_this.partner_id,
                      use_this.create_datetime,
                      use_this.deal_detail_application_order,
                      use_this.get_free_discount,
                      use_this.excl_ind,
                      --
                      'Y');  -- retain_ind set to 'Y' since the deal applies to the hierarchy
      end if;

      --Remove exclude records
      delete from deal_item_loc_explode_gtt dg1
            where exists (select 'x'
                            from deal_item_loc_explode_gtt dg2
                           where dg2.excl_ind              = 'Y'
                             and dg2.item                  = dg1.item
                             and dg2.supplier              = dg1.supplier
                             and dg2.origin_country_id     = dg1.origin_country_id
                             and dg2.location              = dg1.location
                             and dg2.deal_id               = dg1.deal_id
                             and dg2.deal_detail_id        = dg1.deal_detail_id);

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_DEALS;
----------------------------------------------------------------------------------------
FUNCTION LOAD_FUTURE_COST_FOR_DEALS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.LOAD_FUTURE_COST_FOR_DEALS';

BEGIN

   --get fc records covered by the deal being modified into the fc_gtt
   insert into future_cost_gtt    (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   acquisition_cost,
                                   elc_amt,
                                   calc_date,
                                   start_ind,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_grandparent,
                                   item_parent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
   select distinct fc.item,
          fc.supplier,
          fc.origin_country_id,
          fc.location,
          fc.loc_type,
          fc.active_date,
          fc.base_cost,
          fc.net_cost,
          fc.net_net_cost,
          fc.dead_net_net_cost,
          fc.pricing_cost,
          fc.acquisition_cost,
          fc.elc_amt,
          fc.calc_date,
          fc.start_ind,
          fc.primary_supp_country_ind,
          fc.currency_code,
          fc.division,
          fc.group_no,
          fc.dept,
          fc.class,
          fc.subclass,
          fc.item_grandparent,
          fc.item_parent,
          fc.diff_1,
          fc.diff_2,
          fc.diff_3,
          fc.diff_4,
          fc.chain,
          fc.area,
          fc.region,
          fc.district,
          fc.supp_hier_lvl_1,
          fc.supp_hier_lvl_2,
          fc.supp_hier_lvl_3,
          fc.reclass_no,
          fc.cost_change,
          fc.simple_pack_ind,
          fc.primary_cost_pack,
          fc.primary_cost_pack_qty,
          fc.store_type,
          fc.costing_loc,
          fc.negotiated_item_cost,
          fc.extended_base_cost,
          fc.wac_tax,
          fc.default_costing_type,
          null as processing_seq_no
     from future_cost fc,
          cost_event_thread ct,
          cost_event_deal d
    where d.cost_event_process_id = I_cost_event_process_id
      and ct.cost_event_process_id = d.cost_event_process_id
      and ct.thread_id             = I_thread_id
      and ct.item                  = fc.item
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location;

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END LOAD_FUTURE_COST_FOR_DEALS;
----------------------------------------------------------------------------------------
FUNCTION PUSH_BACK_DEAL_ITEM_LOC_EXP(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                     I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
RETURN BOOLEAN IS

BEGIN

   if I_persist_ind = 'Y' then
      merge into deal_item_loc_explode fcd
      using (select inner.item,
                    inner.supplier,
                    inner.origin_country_id,
                    inner.location,
                    inner.loc_type,
                    inner.deal_id,
                    inner.deal_detail_id,
                    inner.active_date,
                    inner.close_date,
                    inner.cost_appl_ind,
                    inner.price_cost_appl_ind,
                    inner.deal_class,
                    inner.threshold_value_type,
                    inner.qty_thresh_buy_item,
                    inner.qty_thresh_get_type,
                    inner.qty_thresh_get_value,
                    inner.qty_thresh_buy_qty,
                    inner.qty_thresh_recur_ind,
                    inner.qty_thresh_buy_target,
                    inner.qty_thresh_buy_avg_loc,
                    inner.qty_thresh_get_item,
                    inner.qty_thresh_get_qty,
                    inner.qty_thresh_free_item_unit_cost,
                    inner.setup_merch_level,
                    inner.setup_division,
                    inner.setup_group_no,
                    inner.setup_dept,
                    inner.setup_class,
                    inner.setup_subclass,
                    inner.setup_item_parent,
                    inner.setup_item_grandparent,
                    inner.setup_diff_1,
                    inner.setup_diff_2,
                    inner.setup_diff_3,
                    inner.setup_diff_4,
                    inner.setup_org_level,
                    inner.setup_chain,
                    inner.setup_area,
                    inner.setup_region,
                    inner.setup_district,
                    inner.setup_location,
                    inner.deal_head_type,
                    inner.partner_type,
                    inner.partner_id,
                    inner.create_datetime,
                    inner.deal_detail_application_order,
                    inner.get_free_discount,
                    inner.retain_ind
               from
                    (select gtt.item,
                            gtt.supplier,
                            gtt.origin_country_id,
                            gtt.location,
                            gtt.loc_type,
                            gtt.deal_id,
                            gtt.deal_detail_id,
                            gtt.active_date,
                            gtt.close_date,
                            gtt.cost_appl_ind,
                            gtt.price_cost_appl_ind,
                            gtt.deal_class,
                            gtt.threshold_value_type,
                            gtt.qty_thresh_buy_item,
                            gtt.qty_thresh_get_type,
                            gtt.qty_thresh_get_value,
                            gtt.qty_thresh_buy_qty,
                            gtt.qty_thresh_recur_ind,
                            gtt.qty_thresh_buy_target,
                            gtt.qty_thresh_buy_avg_loc,
                            gtt.qty_thresh_get_item,
                            gtt.qty_thresh_get_qty,
                            gtt.qty_thresh_free_item_unit_cost,
                            gtt.setup_merch_level,
                            gtt.setup_division,
                            gtt.setup_group_no,
                            gtt.setup_dept,
                            gtt.setup_class,
                            gtt.setup_subclass,
                            gtt.setup_item_parent,
                            gtt.setup_item_grandparent,
                            gtt.setup_diff_1,
                            gtt.setup_diff_2,
                            gtt.setup_diff_3,
                            gtt.setup_diff_4,
                            gtt.setup_org_level,
                            gtt.setup_chain,
                            gtt.setup_area,
                            gtt.setup_region,
                            gtt.setup_district,
                            gtt.setup_location,
                            gtt.deal_head_type,
                            gtt.partner_type,
                            gtt.partner_id,
                            gtt.create_datetime,
                            gtt.deal_detail_application_order,
                            gtt.get_free_discount,
                            gtt.retain_ind,
                            rank() over (partition by gtt.item,
                                                      gtt.supplier,
                                                      gtt.origin_country_id,
                                                      gtt.location,
                                                      gtt.deal_id,
                                                      gtt.deal_detail_id,
                                                      gtt.active_date
                                             order by gtt.setup_division,
                                                      gtt.setup_group_no,
                                                      gtt.setup_dept ,
                                                      gtt.setup_class ,
                                                      gtt.setup_subclass) inner_rank_value
                       from deal_item_loc_explode_gtt gtt) inner
              where inner.inner_rank_value = 1
      ) use_this
      on (     fcd.item              = use_this.item
           and fcd.supplier          = use_this.supplier
           and fcd.origin_country_id = use_this.origin_country_id
           and fcd.location          = use_this.location
           and fcd.deal_id           = use_this.deal_id
           and fcd.deal_detail_id    = use_this.deal_detail_id
      )
      when not matched then
      insert (item,
              supplier,
              origin_country_id,
              location,
              loc_type,
              deal_id,
              deal_detail_id,
              active_date,
              close_date,
              cost_appl_ind,
              price_cost_appl_ind,
              deal_class,
              threshold_value_type,
              qty_thresh_buy_item,
              qty_thresh_get_type,
              qty_thresh_get_value,
              qty_thresh_buy_qty,
              qty_thresh_recur_ind,
              qty_thresh_buy_target,
              qty_thresh_buy_avg_loc,
              qty_thresh_get_item,
              qty_thresh_get_qty,
              qty_thresh_free_item_unit_cost,
              setup_merch_level,
              setup_division,
              setup_group_no,
              setup_dept,
              setup_class,
              setup_subclass,
              setup_item_parent,
              setup_item_grandparent,
              setup_diff_1,
              setup_diff_2,
              setup_diff_3,
              setup_diff_4,
              setup_org_level,
              setup_chain,
              setup_area,
              setup_region,
              setup_district,
              setup_location,
              deal_head_type,
              partner_type,
              partner_id,
              create_datetime,
              deal_detail_application_order,
              get_free_discount)
      values (use_this.item,
              use_this.supplier,
              use_this.origin_country_id,
              use_this.location,
              use_this.loc_type,
              use_this.deal_id,
              use_this.deal_detail_id,
              use_this.active_date,
              use_this.close_date,
              use_this.cost_appl_ind,
              use_this.price_cost_appl_ind,
              use_this.deal_class,
              use_this.threshold_value_type,
              use_this.qty_thresh_buy_item,
              use_this.qty_thresh_get_type,
              use_this.qty_thresh_get_value,
              use_this.qty_thresh_buy_qty,
              use_this.qty_thresh_recur_ind,
              use_this.qty_thresh_buy_target,
              use_this.qty_thresh_buy_avg_loc,
              use_this.qty_thresh_get_item,
              use_this.qty_thresh_get_qty,
              use_this.qty_thresh_free_item_unit_cost,
              use_this.setup_merch_level,
              use_this.setup_division,
              use_this.setup_group_no,
              use_this.setup_dept,
              use_this.setup_class,
              use_this.setup_subclass,
              use_this.setup_item_parent,
              use_this.setup_item_grandparent,
              use_this.setup_diff_1,
              use_this.setup_diff_2,
              use_this.setup_diff_3,
              use_this.setup_diff_4,
              use_this.setup_org_level,
              use_this.setup_chain,
              use_this.setup_area,
              use_this.setup_region,
              use_this.setup_district,
              use_this.setup_location,
              use_this.deal_head_type,
              use_this.partner_type,
              use_this.partner_id,
              use_this.create_datetime,
              use_this.deal_detail_application_order,
              use_this.get_free_discount)
      when matched then
         update set fcd.close_date = use_this.close_date;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PUSH_BACK_DEAL_ITEM_LOC_EXP',
                                            to_char(SQLCODE));
      return FALSE;
END PUSH_BACK_DEAL_ITEM_LOC_EXP;
----------------------------------------------------------------------------------------

FUNCTION PUSH_BACK_FUTURE_COST(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                               I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
RETURN BOOLEAN IS

BEGIN

   if I_persist_ind = 'Y' then

      merge into future_cost fc
      using (select gtt.item,
                    gtt.supplier,
                    gtt.origin_country_id,
                    gtt.location,
                    gtt.loc_type,
                    gtt.active_date,
                    gtt.base_cost,
                    gtt.net_cost,
                    gtt.net_net_cost,
                    gtt.dead_net_net_cost,
                    gtt.pricing_cost,
                    gtt.calc_date,
                    gtt.start_ind,
                    gtt.acquisition_cost,
                    gtt.primary_supp_country_ind,
                    gtt.currency_code,
                    gtt.elc_amt,
                    gtt.division,
                    gtt.group_no,
                    gtt.dept,
                    gtt.class,
                    gtt.subclass,
                    gtt.item_grandparent,
                    gtt.item_parent,
                    gtt.diff_1,
                    gtt.diff_2,
                    gtt.diff_3,
                    gtt.diff_4,
                    gtt.chain,
                    gtt.area,
                    gtt.region,
                    gtt.district,
                    gtt.supp_hier_lvl_1,
                    gtt.supp_hier_lvl_2,
                    gtt.supp_hier_lvl_3,
                    gtt.reclass_no,
                    gtt.cost_change,
                    gtt.simple_pack_ind,
                    gtt.primary_cost_pack,
                    gtt.primary_cost_pack_qty,
                    gtt.store_type,
                    gtt.costing_loc,
                    gtt.templ_id,
                    gtt.passthru_pct,
                    gtt.negotiated_item_cost,
                    gtt.extended_base_cost,
                    gtt.wac_tax,
                    gtt.default_costing_type
               from future_cost_gtt gtt
              where gtt.active_date >= LP_vdate
      ) use_this
      on (     fc.item              = use_this.item
           and fc.supplier          = use_this.supplier
           and fc.origin_country_id = use_this.origin_country_id
           and fc.location          = use_this.location
           and fc.active_date       = use_this.active_date
      )
      when not matched then
      insert (item,
              supplier,
              origin_country_id,
              location,
              loc_type,
              active_date,
              base_cost,
              net_cost,
              net_net_cost,
              dead_net_net_cost,
              pricing_cost,
              calc_date,
              start_ind,
              acquisition_cost,
              primary_supp_country_ind,
              currency_code,
              elc_amt,
              division,
              group_no,
              dept,
              class,
              subclass,
              item_grandparent,
              item_parent,
              diff_1,
              diff_2,
              diff_3,
              diff_4,
              chain,
              area,
              region,
              district,
              supp_hier_lvl_1,
              supp_hier_lvl_2,
              supp_hier_lvl_3,
              reclass_no,
              cost_change,
              simple_pack_ind,
              primary_cost_pack,
              primary_cost_pack_qty,
              store_type,
              costing_loc,
              templ_id,
              passthru_pct,
              negotiated_item_cost,
              extended_base_cost,
              wac_tax,
              default_costing_type)
      values (use_this.item,
              use_this.supplier,
              use_this.origin_country_id,
              use_this.location,
              use_this.loc_type,
              use_this.active_date,
              use_this.base_cost,
              use_this.net_cost,
              use_this.net_net_cost,
              use_this.dead_net_net_cost,
              use_this.pricing_cost,
              LP_vdate,
              use_this.start_ind,
              use_this.acquisition_cost,
              use_this.primary_supp_country_ind,
              use_this.currency_code,
              use_this.elc_amt,
              use_this.division,
              use_this.group_no,
              use_this.dept,
              use_this.class,
              use_this.subclass,
              use_this.item_grandparent,
              use_this.item_parent,
              use_this.diff_1,
              use_this.diff_2,
              use_this.diff_3,
              use_this.diff_4,
              use_this.chain,
              use_this.area,
              use_this.region,
              use_this.district,
              use_this.supp_hier_lvl_1,
              use_this.supp_hier_lvl_2,
              use_this.supp_hier_lvl_3,
              use_this.reclass_no,
              use_this.cost_change,
              use_this.simple_pack_ind,
              use_this.primary_cost_pack,
              use_this.primary_cost_pack_qty,
              use_this.store_type,
              use_this.costing_loc,
              use_this.templ_id,
              use_this.passthru_pct,
              use_this.negotiated_item_cost,
              use_this.extended_base_cost,
              use_this.wac_tax,
              use_this.default_costing_type)
      when matched then
         update set fc.base_cost = use_this.base_cost,
                    fc.net_cost = use_this.net_cost,
                    fc.net_net_cost = use_this.net_net_cost,
                    fc.dead_net_net_cost = use_this.dead_net_net_cost,
                    fc.pricing_cost = use_this.pricing_cost,
                    fc.acquisition_cost = use_this.acquisition_cost,
                    fc.calc_date = LP_vdate,
                    fc.start_ind = use_this.start_ind,
                    fc.primary_supp_country_ind = use_this.primary_supp_country_ind,
                    fc.elc_amt = use_this.elc_amt,
                    fc.division = use_this.division,
                    fc.group_no = use_this.group_no,
                    fc.dept = use_this.dept,
                    fc.class = use_this.class,
                    fc.subclass = use_this.subclass,
                    fc.chain = use_this.chain,
                    fc.area = use_this.area,
                    fc.region = use_this.region,
                    fc.district = use_this.district,
                    fc.supp_hier_lvl_1 = use_this.supp_hier_lvl_1,
                    fc.supp_hier_lvl_2 = use_this.supp_hier_lvl_2,
                    fc.supp_hier_lvl_3 = use_this.supp_hier_lvl_3,
                    fc.reclass_no = use_this.reclass_no,
                    fc.cost_change = use_this.cost_change,
                    fc.simple_pack_ind = use_this.simple_pack_ind,
                    fc.primary_cost_pack = use_this.primary_cost_pack,
                    fc.primary_cost_pack_qty = use_this.primary_cost_pack_qty,
                    fc.templ_id = use_this.templ_id,
                    fc.passthru_pct = use_this.passthru_pct,
                    fc.negotiated_item_cost = use_this.negotiated_item_cost,
                    fc.extended_base_cost = use_this.extended_base_cost,
                    fc.wac_tax = use_this.wac_tax,
                    fc.default_costing_type = use_this.default_costing_type,
                    fc.costing_loc = use_this.costing_loc;

   else

      insert into future_cost_workspace (
           cost_event_process_id,
           item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           elc_amount,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change)
    select I_cost_event_process_id,
           item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           elc_amt,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change
      from future_cost_gtt
     where active_date >= LP_vdate;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PUSH_BACK_FUTURE_COST',
                                            to_char(SQLCODE));
      return FALSE;
END PUSH_BACK_FUTURE_COST;
----------------------------------------------------------------------------------------
FUNCTION PURGE_FUTURE_COST(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   cursor c_fc_history_days is
      select future_cost_history_days
        from system_options;

   L_fc_history_days   system_options.future_cost_history_days%TYPE := NULL;

BEGIN

   open c_fc_history_days;
   fetch c_fc_history_days into L_fc_history_days;
   close c_fc_history_days;

   -- keep only x number of days in future cost where x = future_cost_history_days
   -- in system_options, ensure that there is at least one future cost record per
   -- item supplier country location combination
   merge into future_cost_gtt fc
   using (select -999 as processing_seq_no,
                 inner.item,
                 inner.supplier,
                 inner.origin_country_id,
                 inner.location,
                 inner.active_date
            from (select gtt1.item,
                         gtt1.supplier,
                         gtt1.origin_country_id,
                         gtt1.location,
                         gtt1.active_date,
                         rank() over(partition by gtt1.item,
                                                  gtt1.supplier,
                                                  gtt1.origin_country_id,
                                                  gtt1.location
                                         order by gtt1.active_date desc) rank
                    from future_cost_gtt gtt1
                   where gtt1.active_date <= (LP_vdate - NVL(L_fc_history_days, 0))) inner
           where inner.rank > 1
   ) use_this
   on (     fc.item                  = use_this.item
        and fc.supplier              = use_this.supplier
        and fc.origin_country_id     = use_this.origin_country_id
        and fc.location              = use_this.location
        and fc.active_date           = use_this.active_date
   )
   when matched then
      update set fc.processing_seq_no = use_this.processing_seq_no;
      
   delete from future_cost fc
   where exists (select 'x'
                   from future_cost_gtt gtt
                  where gtt.item               = fc.item
                    and gtt.supplier           = fc.supplier
                    and gtt.origin_country_id  = fc.origin_country_id
                    and gtt.location           = fc.location
                    and gtt.active_date        = fc.active_date
                    and gtt.processing_seq_no  = -999);

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PURGE_FUTURE_COST',
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_FUTURE_COST;
----------------------------------------------------------------------------------------
FUNCTION PURGE_DEAL_ITEM_LOC_EXPLODE(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

  delete from deal_item_loc_explode fcd
   where exists (select 'x'
                   from deal_item_loc_explode_gtt gtt
                  where gtt.item                  = fcd.item
                    and gtt.supplier              = fcd.supplier
                    and gtt.origin_country_id     = fcd.origin_country_id
                    and gtt.location              = fcd.location
                    and gtt.active_date           = fcd.active_date
                    and gtt.deal_id               = fcd.deal_id
                    and gtt.deal_detail_id        = fcd.deal_detail_id
                    and gtt.retain_ind is NULL);

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PURGE_DEAL_ITEM_LOC_EXPLODE',
                                            to_char(SQLCODE));
      return FALSE;
END PURGE_DEAL_ITEM_LOC_EXPLODE;
----------------------------------------------------------------------------------------
FUNCTION SYNC_PRIMARY_PACK(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                          I_persist_ind           IN     COST_EVENT.PERSIST_IND%TYPE)
RETURN BOOLEAN IS

BEGIN

   --if an item with a primary costing pack is in the future_cost_gtt and the primary
   --costing pack is not in the gtt -- add the primary costing pack to the gtt
   insert into future_cost_gtt (
           item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           acquisition_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           elc_amt,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           templ_id,
           passthru_pct,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
    select distinct fc.item,
           fc.supplier,
           fc.origin_country_id,
           fc.location,
           fc.loc_type,
           fc.active_date,
           fc.base_cost,
           fc.net_cost,
           fc.net_net_cost,
           fc.dead_net_net_cost,
           fc.pricing_cost,
           fc.acquisition_cost,
           fc.calc_date,
           fc.start_ind,
           fc.primary_supp_country_ind,
           fc.currency_code,
           fc.elc_amt,
           fc.division,
           fc.group_no,
           fc.dept,
           fc.class,
           fc.subclass,
           fc.item_grandparent,
           fc.item_parent,
           fc.diff_1,
           fc.diff_2,
           fc.diff_3,
           fc.diff_4,
           fc.chain,
           fc.area,
           fc.region,
           fc.district,
           fc.supp_hier_lvl_1,
           fc.supp_hier_lvl_2,
           fc.supp_hier_lvl_3,
           fc.reclass_no,
           fc.cost_change,
           fc.simple_pack_ind,
           fc.primary_cost_pack,
           fc.primary_cost_pack_qty,
           fc.store_type,
           fc.costing_loc,
           fc.templ_id,
           fc.passthru_pct,
           fc.negotiated_item_cost,
           fc.extended_base_cost,
           fc.wac_tax,
           fc.default_costing_type
      from future_cost fc,
           future_cost_gtt gtt
     where gtt.primary_cost_pack is not null
       and gtt.primary_cost_pack = fc.item
       and gtt.supplier          = fc.supplier
       and gtt.origin_country_id = fc.origin_country_id
       and gtt.location          = fc.location
       and not exists(select 'x'
                        from future_cost_gtt gtt2
                       where gtt2.item              = fc.item
                         and gtt2.supplier          = fc.supplier
                         and gtt2.origin_country_id = fc.origin_country_id
                         and gtt2.location          = fc.location);

   --delete records for items that have primary costing pack.  they will be rebuilt
   --based on the the rows from their primary costing pack.
   if I_persist_ind = 'Y' then
      delete from future_cost fc
       where (fc.item, fc.supplier, fc.origin_country_id, fc.location)
          in (select gtt2.item,
                     gtt2.supplier,
                     gtt2.origin_country_id,
                     gtt2.location
                from future_cost_gtt gtt2
               where gtt2.primary_cost_pack is not null
                 and exists (select 'x'
                               from future_cost_gtt gtt3
                              where gtt2.primary_cost_pack = gtt3.item
                                and gtt2.supplier          = gtt3.supplier
                                and gtt2.origin_country_id = gtt3.origin_country_id
                                and gtt2.location          = gtt3.location));
   end if;

   delete from future_cost_gtt gtt
    where (gtt.rowid)
       in (select gtt2.rowid
             from future_cost_gtt gtt2
            where gtt2.primary_cost_pack is not null
              and exists (select 'x'
                            from future_cost_gtt gtt3
                           where gtt2.primary_cost_pack = gtt3.item
                             and gtt2.supplier          = gtt3.supplier
                             and gtt2.origin_country_id = gtt3.origin_country_id
                             and gtt2.location          = gtt3.location));

   --create future_cost_gtt rows for items that have primary costing packs based
   --off of the primary costing pack rows.
   insert into future_cost_gtt (
           item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           elc_amt,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           templ_id,
           passthru_pct,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
    select distinct pi.item,
           gtt.supplier,
           gtt.origin_country_id,
           gtt.location,
           gtt.loc_type,
           gtt.active_date,
           base_cost / pi.pack_qty,
           net_cost / pi.pack_qty,
           net_net_cost / pi.pack_qty,
           dead_net_net_cost / pi.pack_qty,
           pricing_cost / pi.pack_qty,
           gtt.calc_date,
           gtt.start_ind,
           gtt.primary_supp_country_ind,
           gtt.currency_code,
           gtt.elc_amt / pi.pack_qty,
           gtt.division,
           gtt.group_no,
           gtt.dept,
           gtt.class,
           gtt.subclass,
           gtt.item_grandparent,
           gtt.item_parent,
           gtt.diff_1,
           gtt.diff_2,
           gtt.diff_3,
           gtt.diff_4,
           gtt.chain,
           gtt.area,
           gtt.region,
           gtt.district,
           gtt.supp_hier_lvl_1,
           gtt.supp_hier_lvl_2,
           gtt.supp_hier_lvl_3,
           gtt.reclass_no,
           gtt.cost_change,
           'N',
           gtt.item,
           pi.pack_qty,
           gtt.store_type,
           gtt.costing_loc,
           gtt.templ_id,
           gtt.passthru_pct,
           negotiated_item_cost / pi.pack_qty,
           extended_base_cost / pi.pack_qty,
           gtt.wac_tax,
           gtt.default_costing_type
      from future_cost_gtt gtt,
           packitem pi,
           item_loc il
     where gtt.simple_pack_ind  = 'Y'
       and gtt.item             = pi.pack_no
       and il.item              = pi.item
       and il.loc               = gtt.location
       and il.primary_cost_pack = pi.pack_no
       and ( gtt.costing_loc is null 
             or 
             (gtt.costing_loc is not null and exists (select 1 
                                                      from item_loc ilc
                                                     where ilc.item = il.item
                                                       and ilc.loc = gtt.costing_loc
                                                       and ilc.primary_cost_pack = pi.pack_no
                                                       and ilc.primary_cost_pack is not null)
             )
            );

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.SYNC_PRIMAY_PACK',
                                            to_char(SQLCODE));
      return FALSE;
END SYNC_PRIMARY_PACK;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_SUPP_CNTR(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_action                IN     COST_EVENT.ACTION%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   
   cursor C_DEFAULT_TAX_TYPE is
   select default_tax_type
     from system_options;

BEGIN
   --Retrieve default_tax_type from system_options
   open C_DEFAULT_TAX_TYPE;
   fetch C_DEFAULT_TAX_TYPE into L_default_tax_type;
   close C_DEFAULT_TAX_TYPE;
/*
   if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
      insert into future_cost_gtt (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   calc_date,
                                   start_ind,
                                   acquisition_cost,
                                   elc_amt,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_parent,
                                   item_grandparent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
      select distinct fc.*,
             NULL as processing_seq_no
        from cost_event_thread ct,
             future_cost fc
       where ct.cost_event_process_id = I_cost_event_process_id
         and ct.thread_id             = I_thread_id
         and ct.item                  = fc.item
         and ct.supplier              = fc.supplier
         and ct.origin_country_id     = fc.origin_country_id
         and ct.location              = fc.location;
         
*/

if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
           delete from future_cost wf
              where exists (select 'x'
                               from cost_event_thread ct
                              where ct.cost_event_process_id  = I_cost_event_process_id
                                and ct.thread_id              = I_thread_id
                                and ct.item                   = wf.item
                                and ct.supplier               = wf.supplier
                                and ct.origin_country_id      = wf.origin_country_id
                                and ct.location               = wf.location  
                                and wf.active_date          > = LP_vdate);
   
 elsif I_action = FUTURE_COST_EVENT_SQL.ADD_EVENT then
      --create seed record
      insert into future_cost_gtt  (item,
                                    supplier,
                                    origin_country_id,
                                    location,
                                    loc_type,
                                    active_date,
                                    base_cost,
                                    net_cost,
                                    net_net_cost,
                                    dead_net_net_cost,
                                    pricing_cost,
                                    calc_date,
                                    start_ind,
                                    primary_supp_country_ind,
                                    currency_code,
                                    --
                                    division,
                                    group_no,
                                    dept,
                                    class,
                                    subclass,
                                    item_grandparent,
                                    item_parent,
                                    diff_1,
                                    diff_2,
                                    diff_3,
                                    diff_4,
                                    --
                                    chain,
                                    area,
                                    region,
                                    district,
                                    --
                                    supp_hier_lvl_1,
                                    supp_hier_lvl_2,
                                    supp_hier_lvl_3,
                                    --
                                    reclass_no,
                                    cost_change,
                                    simple_pack_ind,
                                    primary_cost_pack,
                                    primary_cost_pack_qty,
                                    store_type,
                                    costing_loc,
                                    --
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    wac_tax,
                                    default_costing_type)
               select ct.item,
                      ct.supplier,
                      ct.origin_country_id,
                      ct.location,
                      il.loc_type,
                      LP_vdate-1,
                      DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --base_cost
                      DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --net_cost
                      DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --net_net_cost
                      DECODE(L_default_tax_type, 'GTAX', NVL(iscl.base_cost, 0), NVL(iscl.unit_cost, 0)), --dead_net_net_cost
                      --Pricing cost is default_po_cost + wac_tax
                      NVL(iscl.unit_cost, 0) + (NVL(iscl.extended_base_cost, 0) - NVL(iscl.base_cost, 0)) , --pricing_cost
                      LP_vdate,
                      NULL,
                      DECODE(il.primary_supp, iscl.supplier,
                             DECODE(il.primary_cntry, iscl.origin_country_id,
                                    'Y', 'N'), 'N'),
                      s.currency_code,
                      --
                      vim.division,
                      vim.group_no,
                      vim.dept,
                      vim.class,
                      vim.subclass,
                      vim.item_grandparent,
                      vim.item_parent,
                      vim.diff_1,
                      vim.diff_2,
                      vim.diff_3,
                      vim.diff_4,
                      --
                      org.chain,
                      org.area,
                      org.region,
                      org.district,
                      --
                      iscl.supp_hier_lvl_1,
                      iscl.supp_hier_lvl_2,
                      iscl.supp_hier_lvl_3,
                      --
                      NULL,
                      NULL,
                      vim.simple_pack_ind,
                      il.primary_cost_pack,
                      pi.pack_qty,
                      ct.store_type,
                      il.costing_loc,
                      --
                      --Source negotiated_item_cost, extended_base_cost from ITEM_SUPP_COUNTRY table.
                      --ITEM_SUPP_COUNTRY table will have updated values from tax engine by the time a new itemloc cost event is triggered.
                      DECODE(L_default_tax_type, 'GTAX', iscl.negotiated_item_cost, NULL),
                      DECODE(L_default_tax_type, 'GTAX', iscl.extended_base_cost, NULL),
                      --wac_tax is extended_base_cost - base_cost
                      DECODE(L_default_tax_type, 'GTAX', iscl.extended_base_cost, NULL) - DECODE(L_default_tax_type, 'GTAX', iscl.base_cost, NULL), --wac_tax
                      NVL(ca.default_po_cost, 'BC')
                 from cost_event_thread ct,
                      item_supp_country_loc iscl,
                      item_loc il,
                      v_item_master vim,
                      sups s,
                      packitem pi,
                      (select a.chain,
                              a.area,
                              r.region,
                              d.district,
                              s.store location,
                              'S' loc_type,
                              s.store_type
                         from area a,
                              region r,
                              district d,
                              store s
                        where s.district = d.district
                          and d.region = r.region
                          and r.area = a.area
                       union all
                       select NULL chain,
                              NULL area,
                              NULL region,
                              NULL district,
                              w.wh location,
                              'W' loc_type,
                              NULL store_type
                         from wh w
                        where stockholding_ind = 'Y'
                          and (org_hier_type = 1 or org_hier_type = 50 or
                               org_hier_type is NULL)
                       union all
                       select w.org_hier_value chain,
                              NULL area,
                              NULL region,
                              NULL district,
                              w.wh location,
                              'W' loc_type,
                              NULL store_type
                         from wh w
                        where stockholding_ind = 'Y'
                          and org_hier_type = 10
                       union all
                       select a.chain chain,
                              w.org_hier_value area,
                              NULL region,
                              NULL district,
                              w.wh location,
                              'W' loc_type,
                              NULL store_type
                         from wh w,
                              area a
                        where stockholding_ind = 'Y'
                          and org_hier_type = 20
                          and org_hier_value = a.area
                       union all
                       select a.chain chain,
                              r.area area,
                              org_hier_value region,
                              NULL district,
                              w.wh location,
                              'W' loc_type,
                              NULL store_type
                         from wh w,
                              area a,
                              region r
                        where stockholding_ind = 'Y'
                          and org_hier_type = 30
                          and org_hier_value = r.region
                          and r.area = a.area
                       union all
                       select a.chain chain,
                              r.area area,
                              d.region region,
                              org_hier_value district,
                              w.wh location,
                              'W' loc_type,
                              NULL store_type
                         from wh w,
                              area a,
                              region r,
                              district d
                        where stockholding_ind = 'Y'
                          and org_hier_type = 40
                          and org_hier_value = d.district
                          and d.region = r.region
                          and r.area = a.area) org,
                      country_attrib ca
                where ct.cost_event_process_id = I_cost_event_process_id
                  and ct.thread_id             = I_thread_id
                  and ct.item                  = iscl.item
                  and ct.location              = iscl.loc
                  and ct.supplier              = iscl.supplier
                  and ct.origin_country_id     = iscl.origin_country_id
                  and ct.item                  = il.item
                  and ct.location              = il.loc
                  and ct.item                  = vim.item
                  and iscl.supplier            = s.supplier
                  and il.primary_cost_pack     = pi.pack_no(+)
                  and ct.location              = org.location(+)
                  and ct.origin_country_id     = ca.country_id (+);

   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_ACTION_TYPE',
                                            I_action,
                                            'NULL',
                                            'NULL');
      return FALSE;
   end if;

   --add FUTURE_COST_ELC as needed
   if ELC_DATE_TO_FC(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   --add FUTURE_COST_RECLASS as needed
   if MERGE_NIL_RECLASS(O_error_message,
                        I_cost_event_process_id,
                        I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   --add FUTURE_COST_COST_CHANGE as needed, deal with no loc cost changes
   if MERGE_NIL_CC(O_error_message,
                   I_cost_event_process_id,
                   I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   --add DEAL_ITEM_LOC_EXPLODE as needed, deal with hierarchy, exception issues
   if MERGE_DEAL(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PROCESS_SUPP_CNTR',
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_SUPP_CNTR;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_DEAL_PASSTHRU(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN

   insert into future_cost_gtt (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 active_date,
                                 base_cost,
                                 net_cost,
                                 net_net_cost,
                                 dead_net_net_cost,
                                 pricing_cost,
                                 acquisition_cost,
                                 calc_date,
                                 start_ind,
                                 primary_supp_country_ind,
                                 currency_code,
                                 division,
                                 group_no,
                                 dept,
                                 class,
                                 subclass,
                                 item_grandparent,
                                 item_parent,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 chain,
                                 area,
                                 region,
                                 district,
                                 supp_hier_lvl_1,
                                 supp_hier_lvl_2,
                                 supp_hier_lvl_3,
                                 reclass_no,
                                 cost_change,
                                 simple_pack_ind,
                                 primary_cost_pack,
                                 primary_cost_pack_qty,
                                 store_type,
                                 costing_loc,
                                 templ_id,
                                 passthru_pct,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 wac_tax,
                                 default_costing_type,
                                 processing_seq_no)
      select use_this.item,
             use_this.supplier,
             use_this.origin_country_id,
             use_this.location,
             use_this.loc_type,
             use_this.active_date,
             use_this.base_cost,
             use_this.net_cost,
             use_this.net_net_cost,
             use_this.dead_net_net_cost,
             use_this.pricing_cost,
             use_this.acquisition_cost,
             use_this.calc_date,
             use_this.start_ind,
             use_this.primary_supp_country_ind,
             use_this.currency_code,
             use_this.division,
             use_this.group_no,
             use_this.dept,
             use_this.class,
             use_this.subclass,
             use_this.item_grandparent,
             use_this.item_parent,
             use_this.diff_1,
             use_this.diff_2,
             use_this.diff_3,
             use_this.diff_4,
             use_this.chain,
             use_this.area,
             use_this.region,
             use_this.district,
             use_this.supp_hier_lvl_1,
             use_this.supp_hier_lvl_2,
             use_this.supp_hier_lvl_3,
             use_this.reclass_no,
             use_this.cost_change,
             use_this.simple_pack_ind,
             use_this.primary_cost_pack,
             use_this.primary_cost_pack_qty,
             use_this.store_type,
             use_this.costing_loc,
             use_this.templ_id,
             use_this.passthru_pct,
             use_this.negotiated_item_cost,
             use_this.extended_base_cost,
             use_this.wac_tax,
             use_this.default_costing_type,
             use_this.processing_seq_no
         from
         (select fc.item,
                 fc.supplier,
                 fc.origin_country_id,
                 fc.location,
                 fc.loc_type,
                 fc.active_date,
                 fc.base_cost,
                 fc.net_cost,
                 fc.net_net_cost,
                 fc.dead_net_net_cost,
                 fc.pricing_cost,
                 fc.acquisition_cost,
                 fc.calc_date,
                 fc.start_ind,
                 fc.primary_supp_country_ind,
                 fc.currency_code,
                 fc.division,
                 fc.group_no,
                 fc.dept,
                 fc.class,
                 fc.subclass,
                 fc.item_grandparent,
                 fc.item_parent,
                 fc.diff_1,
                 fc.diff_2,
                 fc.diff_3,
                 fc.diff_4,
                 fc.chain,
                 fc.area,
                 fc.region,
                 fc.district,
                 fc.supp_hier_lvl_1,
                 fc.supp_hier_lvl_2,
                 fc.supp_hier_lvl_3,
                 fc.reclass_no,
                 fc.cost_change,
                 fc.simple_pack_ind,
                 fc.primary_cost_pack,
                 fc.primary_cost_pack_qty,
                 fc.store_type,
                 fc.costing_loc,
                 fc.templ_id,
                 passthru_pct,
                 fc.negotiated_item_cost,
                 fc.extended_base_cost,
                 fc.wac_tax,
                 fc.default_costing_type,
                 null as processing_seq_no,
                 ct.cost_event_process_id,
                 ct.thread_id
            from future_cost fc,
                 cost_event_thread ct
           where ct.cost_event_process_id  = I_cost_event_process_id
             and ct.thread_id              = I_thread_id
             and ct.item                   = fc.item
             and ct.supplier               = fc.supplier
             and ct.origin_country_id      = fc.origin_country_id
             and ct.location               = fc.location) use_this;

   -- move over existing deal records to the deal gtt.
   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'FUTURE_COST_SQL.EXPLODE_DEAL_PASSTHRU',
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_DEAL_PASSTHRU;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_TMPL_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS
BEGIN

/* for cost based template , only 1 item will be present in cost_event_thread table , hence only that item will be inserted into future_cost_gtt*/
/* for remaing template, all the items belonging to the dept/class/subclass will be inserted*/
      insert into future_cost_gtt  (item,
                                    supplier,
                                    origin_country_id,
                                    location,
                                    loc_type,
                                    active_date,
                                    base_cost,
                                    net_cost,
                                    net_net_cost,
                                    dead_net_net_cost,
                                    pricing_cost,
                                    acquisition_cost,
                                    calc_date,
                                    start_ind,
                                    primary_supp_country_ind,
                                    currency_code,
                                    division,
                                    group_no,
                                    dept,
                                    class,
                                    subclass,
                                    item_grandparent,
                                    item_parent,
                                    diff_1,
                                    diff_2,
                                    diff_3,
                                    diff_4,
                                    chain,
                                    area,
                                    region,
                                    district,
                                    supp_hier_lvl_1,
                                    supp_hier_lvl_2,
                                    supp_hier_lvl_3,
                                    reclass_no,
                                    cost_change,
                                    simple_pack_ind,
                                    primary_cost_pack,
                                    primary_cost_pack_qty,
                                    store_type,
                                    costing_loc,
                                    templ_id,
                                    passthru_pct,
                                    negotiated_item_cost,
                                    extended_base_cost,
                                    wac_tax,
                                    default_costing_type,
                                    processing_seq_no)
         select use_this.item,
                use_this.supplier,
                use_this.origin_country_id,
                use_this.location,
                use_this.loc_type,
                use_this.active_date,
                use_this.base_cost,
                use_this.net_cost,
                use_this.net_net_cost,
                use_this.dead_net_net_cost,
                use_this.pricing_cost,
                use_this.acquisition_cost,
                use_this.calc_date,
                use_this.start_ind,
                use_this.primary_supp_country_ind,
                use_this.currency_code,
                use_this.division,
                use_this.group_no,
                use_this.dept,
                use_this.class,
                use_this.subclass,
                use_this.item_grandparent,
                use_this.item_parent,
                use_this.diff_1,
                use_this.diff_2,
                use_this.diff_3,
                use_this.diff_4,
                use_this.chain,
                use_this.area,
                use_this.region,
                use_this.district,
                use_this.supp_hier_lvl_1,
                use_this.supp_hier_lvl_2,
                use_this.supp_hier_lvl_3,
                use_this.reclass_no,
                use_this.cost_change,
                use_this.simple_pack_ind,
                use_this.primary_cost_pack,
                use_this.primary_cost_pack_qty,
                use_this.store_type,
                use_this.costing_loc,
                use_this.templ_id,
                use_this.passthru_pct,
                use_this.negotiated_item_cost,
                use_this.extended_base_cost,
                use_this.wac_tax,
                use_this.default_costing_type,
                use_this.processing_seq_no
            from
            (select fc.item,
                    fc.supplier,
                    fc.origin_country_id,
                    fc.location,
                    fc.loc_type,
                    case
                     when fc.active_date >= LP_vdate then fc.active_date
                       else LP_vdate end active_date,
                    fc.base_cost,
                    fc.net_cost,
                    fc.net_net_cost,
                    fc.dead_net_net_cost,
                    fc.pricing_cost,
                    fc.acquisition_cost,
                    fc.calc_date,
                    fc.start_ind,
                    fc.primary_supp_country_ind,
                    fc.currency_code,
                    fc.division,
                    fc.group_no,
                    fc.dept,
                    fc.class,
                    fc.subclass,
                    fc.item_grandparent,
                    fc.item_parent,
                    fc.diff_1,
                    fc.diff_2,
                    fc.diff_3,
                    fc.diff_4,
                    fc.chain,
                    fc.area,
                    fc.region,
                    fc.district,
                    fc.supp_hier_lvl_1,
                    fc.supp_hier_lvl_2,
                    fc.supp_hier_lvl_3,
                    fc.reclass_no,
                    fc.cost_change,
                    fc.simple_pack_ind,
                    fc.primary_cost_pack,
                    fc.primary_cost_pack_qty,
                    fc.store_type,
                    fc.costing_loc,
                    cec.templ_id,
                    passthru_pct,
                    fc.negotiated_item_cost,
                    fc.extended_base_cost,
                    fc.wac_tax,
                    fc.default_costing_type,
                    null as processing_seq_no,
                    ct.cost_event_process_id,
                    ct.thread_id,
                    rank() over(partition by fc.item,
                                             fc.supplier,
                                             fc.origin_country_id,
                                             fc.location
                               order by fc.active_date desc) as rank_value
               from future_cost fc,
                    cost_event_thread ct,
                    cost_event_cost_tmpl cec
              where ct.cost_event_process_id  = I_cost_event_process_id
                and ct.thread_id              = I_thread_id
                and cec.cost_event_process_id  = ct.cost_event_process_id
                and ct.item                   = fc.item
                and ct.supplier               = fc.supplier
                and ct.origin_country_id      = fc.origin_country_id
                and ct.location               = fc.location) use_this
        where use_this.rank_value = 1;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'FUTURE_COST_SQL.EXPLODE_TMPL_CHG',
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_TMPL_CHG;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_TMP_RELATIONSHIP_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                      I_action                IN     COST_EVENT.ACTION%TYPE,
                                      I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_TMP_RELATIONSHIP_CHG';

BEGIN

/* for cost based template , only 1 item will be present in cost_event_thread table , hence only that item will be inserted into future_cost_gtt*/
/* for remaing template, all the items belonging to the dept/class/subclass will be inserted*/
   insert into future_cost_gtt  (item,
                                 supplier,
                                 origin_country_id,
                                 location,
                                 loc_type,
                                 active_date,
                                 base_cost,
                                 net_cost,
                                 net_net_cost,
                                 dead_net_net_cost,
                                 pricing_cost,
                                 acquisition_cost,
                                 calc_date,
                                 start_ind,
                                 primary_supp_country_ind,
                                 currency_code,
                                 division,
                                 group_no,
                                 dept,
                                 class,
                                 subclass,
                                 item_grandparent,
                                 item_parent,
                                 diff_1,
                                 diff_2,
                                 diff_3,
                                 diff_4,
                                 chain,
                                 area,
                                 region,
                                 district,
                                 supp_hier_lvl_1,
                                 supp_hier_lvl_2,
                                 supp_hier_lvl_3,
                                 reclass_no,
                                 cost_change,
                                 simple_pack_ind,
                                 primary_cost_pack,
                                 primary_cost_pack_qty,
                                 store_type,
                                 costing_loc,
                                 passthru_pct,
                                 negotiated_item_cost,
                                 extended_base_cost,
                                 wac_tax,
                                 default_costing_type,
                                 processing_seq_no)
      select use_this.item,
             use_this.supplier,
             use_this.origin_country_id,
             use_this.location,
             use_this.loc_type,
             use_this.active_date,
             use_this.base_cost,
             use_this.net_cost,
             use_this.net_net_cost,
             use_this.dead_net_net_cost,
             use_this.pricing_cost,
             use_this.acquisition_cost,
             use_this.calc_date,
             use_this.start_ind,
             use_this.primary_supp_country_ind,
             use_this.currency_code,
             use_this.division,
             use_this.group_no,
             use_this.dept,
             use_this.class,
             use_this.subclass,
             use_this.item_grandparent,
             use_this.item_parent,
             use_this.diff_1,
             use_this.diff_2,
             use_this.diff_3,
             use_this.diff_4,
             use_this.chain,
             use_this.area,
             use_this.region,
             use_this.district,
             use_this.supp_hier_lvl_1,
             use_this.supp_hier_lvl_2,
             use_this.supp_hier_lvl_3,
             use_this.reclass_no,
             use_this.cost_change,
             use_this.simple_pack_ind,
             use_this.primary_cost_pack,
             use_this.primary_cost_pack_qty,
             use_this.store_type,
             use_this.costing_loc,
             use_this.passthru_pct,
             use_this.negotiated_item_cost,
             use_this.extended_base_cost,
             use_this.wac_tax,
             use_this.default_costing_type,
             use_this.processing_seq_no
         from
         (select distinct fc.item,
                 fc.supplier,
                 fc.origin_country_id,
                 fc.location,
                 fc.loc_type,
                 fc.active_date,   
                 fc.base_cost,
                 fc.net_cost,
                 fc.net_net_cost,
                 fc.dead_net_net_cost,
                 fc.pricing_cost,
                 fc.acquisition_cost,
                 fc.calc_date,
                 fc.start_ind,
                 fc.primary_supp_country_ind,
                 fc.currency_code,
                 fc.division,
                 fc.group_no,
                 fc.dept,
                 fc.class,
                 fc.subclass,
                 fc.item_grandparent,
                 fc.item_parent,
                 fc.diff_1,
                 fc.diff_2,
                 fc.diff_3,
                 fc.diff_4,
                 fc.chain,
                 fc.area,
                 fc.region,
                 fc.district,
                 fc.supp_hier_lvl_1,
                 fc.supp_hier_lvl_2,
                 fc.supp_hier_lvl_3,
                 fc.reclass_no,
                 fc.cost_change,
                 fc.simple_pack_ind,
                 fc.primary_cost_pack,
                 fc.primary_cost_pack_qty,
                 fc.store_type,
                 fc.costing_loc,
                 passthru_pct,
                 fc.negotiated_item_cost,
                 fc.extended_base_cost,
                 fc.wac_tax,
                 fc.default_costing_type,
                 null as processing_seq_no,
                 ct.cost_event_process_id,
                 ct.thread_id,
                 rank() over(partition by fc.item,
                                          fc.supplier,
                                          fc.origin_country_id,
                                          fc.location,
                                          cec.new_start_date
                            order by fc.active_date desc) as rank_value
            from future_cost fc,
                 cost_event_thread ct,
                 cost_event_cost_relationship cec
           where ct.cost_event_process_id  = I_cost_event_process_id
             and ct.thread_id              = I_thread_id
             and cec.cost_event_process_id = ct.cost_event_process_id
             and ct.item                   = fc.item
             and ct.supplier               = fc.supplier
             and ct.origin_country_id      = fc.origin_country_id
             and ct.location               = fc.location
             and fc.active_date           <= cec.new_start_date
             and fc.item                   = NVL(cec.item,ct.item)
             and not exists
                 ( select 1 from future_cost_gtt fcg
                   where fcg.item               = fc.item
                     and fcg.supplier           = fc.supplier
                     and fcg.origin_country_id  = fc.origin_country_id
                     and fcg.location           = fc.location
                     and fcg.active_date        = fc.active_date        
                  )) use_this
        where use_this.rank_value = 1;
        

     if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
        delete from future_cost wf
           where exists (select 'x'
                            from cost_event_thread ct,
                                 cost_event_cost_relationship cec
                           where ct.cost_event_process_id  = I_cost_event_process_id
                             and ct.thread_id              = I_thread_id
                             and cec.cost_event_process_id = ct.cost_event_process_id
                             and ct.item                   = wf.item
                             and ct.supplier               = wf.supplier
                             and ct.origin_country_id      = wf.origin_country_id
                             and ct.location               = wf.costing_loc
                             and cec.location              = wf.location
                             and wf.item                   = NVL(cec.item,ct.item)
                             and wf.active_date            > LP_vdate
                             and wf.active_date           between cec.new_start_date and  cec.new_end_date+1 );

   end if;
   
   if I_action in (FUTURE_COST_EVENT_SQL.MODIFY_EVENT, FUTURE_COST_EVENT_SQL.ADD_EVENT) then
           delete from future_cost wf
              where exists (select 'x'
                               from cost_event_thread ct,
                                    cost_event_cost_relationship cec
                              where ct.cost_event_process_id  = I_cost_event_process_id
                                and ct.thread_id              = I_thread_id
                                and cec.cost_event_process_id = ct.cost_event_process_id
                                and ct.item                   = wf.item
                                and ct.supplier               = wf.supplier
                                and ct.origin_country_id      = wf.origin_country_id
                                and ct.location               = wf.costing_loc
                                and cec.location              = wf.location
                                and wf.item                   = NVL(cec.item,ct.item)
                                and wf.active_date            > LP_vdate
                                and wf.active_date           between cec.old_start_date and  cec.old_end_date+1 );
   
   end if;
  
         if MERGE_NIL_CC(O_error_message,
                   I_cost_event_process_id,
                   I_thread_id) = FALSE then
            return FALSE;
         end if;
   
      if MERGE_DEAL(O_error_message,
                    I_cost_event_process_id,
                    I_thread_id) = FALSE then
         return FALSE;
   end if;   


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_TMP_RELATIONSHIP_CHG;
-----------------------------------------------------------------------------------------------------------------
FUNCTION COMPUTE_CC_TAX(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   L_program                  VARCHAR2(100)                        := 'FUTURE_COST_SQL.COMPUTE_CC_TAX';
   L_obj_comp_item_cost_tbl   OBJ_COMP_ITEM_COST_TBL;
   L_obj_comp_item_cost_head_tbl   OBJ_COMP_ITEM_COST_TBL;

   cursor C_DEFAULT_TAX_TYPE is
      select default_tax_type
        from system_options;

   cursor C_GET_OBJECT_HEAD is
      select OBJ_COMP_ITEM_COST_REC (
             fc.item,                       --I_item
             NVL(ich.nic_static_ind, 'N'),  --I_nic_static_ind
             fc.supplier,                   --I_supplier
             ca.default_loc,                --I_location
             ca.default_loc_type,           --I_loc_type
             fc.active_date,                --I_effective_date,
             'ITEMSUPPCTRY',                --I_calling_form --does not persist
             fc.origin_country_id,          --I_origin_country_id
             ich.delivery_country_id,       --I_delivery_country_id
             NULL,                          --I_prim_dlvy_ctry_ind
             NULL,                          --I_update_itemcost_ind
             NULL,                          --I_update_itemcost_child_ind
             ca.item_cost_tax_incl_ind,     --I_item_cost_tax_incl_ind
             fc.base_cost,                  --O_base_cost
             NULL,                          --O_extended_base_cost
             NULL,                          --O_inclusive_cost
             fc.base_cost,                  --O_negotiated_item_cost
             NULL,                          --svat_tax_rate
             NULL,                          --tax_loc_type
             NULL,                          --pack_ind
             NULL,                          --pack_type
             NULL,                          --dept
             NULL,                          --prim_supp_currency_code
             NULL,                          --loc_prim_country
             NULL,                          --loc_prim_country_tax_incl_ind
             NULL,                          --gtax_total_tax_amount
             NULL,                          --gtax_total_tax_amount_nic
             NULL)                          --gtax_total_recover_amount
        from future_cost_gtt fc,
             item_cost_head ich,
             country_attrib ca,
             mv_l10n_entity mle
       where fc.cost_change = (select cost_change 
                                 from cost_event_cost_chg 
                                where cost_event_process_id = I_cost_event_process_id)
         and fc.item = ich.item
         and fc.supplier = ich.supplier
         and fc.origin_country_id = ich.origin_country_id
         and ca.country_id = mle.country_id
         and mle.entity_id = TO_CHAR(fc.location)
         and mle.country_id = ich.delivery_country_id
         and mle.entity = 'LOC';

   cursor C_GET_OBJECT_GTAX is
      select OBJ_COMP_ITEM_COST_REC (
             fc.item,                       --I_item
             NVL(ich.nic_static_ind, 'N'),  --I_nic_static_ind
             fc.supplier,                   --I_supplier
             fc.location,                   --I_location
             fc.loc_type,                   --I_loc_type
             fc.active_date,                --I_effective_date,
             'ITEMSUPPCTRYLOC',             --I_calling_form --does not persist
             fc.origin_country_id,          --I_origin_country_id
             NULL,                          --I_delivery_country_id
             NULL,                          --I_prim_dlvy_ctry_ind
             NULL,                          --I_update_itemcost_ind
             NULL,                          --I_update_itemcost_child_ind
             NVL(ich.nic_static_ind, 'N'),  --I_item_cost_tax_incl_ind
             use_this.O_base_cost,            --O_base_cost
             NULL,                          --O_extended_base_cost
             NULL,                          --O_inclusive_cost
             fc.negotiated_item_cost,       --O_negotiated_item_cost
             NULL,                          --svat_tax_rate
             NULL,                          --tax_loc_type
             NULL,                          --pack_ind
             NULL,                          --pack_type
             NULL,                          --dept
             NULL,                          --prim_supp_currency_code
             NULL,                          --loc_prim_country
             NULL,                          --loc_prim_country_tax_incl_ind
             NULL,                          --gtax_total_tax_amount
             NULL,                          --gtax_total_tax_amount_nic
             NULL)                          --gtax_total_recover_amount
        from future_cost_gtt fc,
             item_cost_head ich,
             mv_l10n_entity mle,
             TABLE(CAST(L_obj_comp_item_cost_head_tbl as OBJ_COMP_ITEM_COST_TBL)) use_this
       where fc.cost_change = (select cost_change 
                                 from cost_event_cost_chg 
                                where cost_event_process_id = I_cost_event_process_id)
         and fc.item = ich.item
         and fc.supplier = ich.supplier
         and fc.origin_country_id = ich.origin_country_id
         and mle.entity_id = TO_CHAR(fc.location)
         and mle.country_id = ich.delivery_country_id
         and mle.entity = 'LOC'
         and use_this.I_item = fc.item
         and use_this.I_supplier = fc.supplier
         and use_this.I_effective_date = fc.active_date
         and use_this.I_origin_country_id = fc.origin_country_id
         and use_this.I_delivery_country_id = mle.country_id;

BEGIN

   --Retrieve default_tax_type from system_options
   open C_DEFAULT_TAX_TYPE;
   fetch C_DEFAULT_TAX_TYPE into L_default_tax_type;
   close C_DEFAULT_TAX_TYPE;

   if L_default_tax_type != 'GTAX' then
      return TRUE;
   end if;

   if L_default_tax_type = 'GTAX' then
      open C_GET_OBJECT_HEAD;
      fetch C_GET_OBJECT_HEAD BULK COLLECT into L_obj_comp_item_cost_head_tbl;
      close C_GET_OBJECT_HEAD;

      if L_obj_comp_item_cost_head_tbl.count > 0 then
         if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK(O_error_message,
                                                 L_obj_comp_item_cost_head_tbl) = FALSE then
            return FALSE;
         end if;
      end if;

      open C_GET_OBJECT_GTAX;
      fetch C_GET_OBJECT_GTAX BULK COLLECT into L_obj_comp_item_cost_tbl;
      close C_GET_OBJECT_GTAX;

   end if;

   if L_obj_comp_item_cost_tbl.count > 0 then
      if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK(O_error_message,
                                              L_obj_comp_item_cost_tbl) = FALSE then
         return FALSE;
      end if;

      --Update future_cost_temp with computed values for negotiated_item_cost, extended_base_cost and wac_tax
      FORALL i IN 1..L_obj_comp_item_cost_tbl.COUNT
         update future_cost_gtt fc
            set fc.negotiated_item_cost = NVL(L_obj_comp_item_cost_tbl(i).O_negotiated_item_cost,0),
                fc.extended_base_cost   = NVL(L_obj_comp_item_cost_tbl(i).O_extended_base_cost,0),
                fc.wac_tax              = NVL(L_obj_comp_item_cost_tbl(i).O_extended_base_cost,0) -
                                          NVL(L_obj_comp_item_cost_tbl(i).O_base_cost,0),
                fc.base_cost            = NVL(L_obj_comp_item_cost_tbl(i).O_base_cost,0),
                fc.net_cost             = NVL(L_obj_comp_item_cost_tbl(i).O_base_cost,0),
                fc.net_net_cost         = NVL(L_obj_comp_item_cost_tbl(i).O_base_cost,0),
                fc.dead_net_net_cost    = NVL(L_obj_comp_item_cost_tbl(i).O_base_cost,0)
          where fc.item                 = L_obj_comp_item_cost_tbl(i).I_item
            and fc.supplier             = L_obj_comp_item_cost_tbl(i).I_supplier
            and fc.origin_country_id    = L_obj_comp_item_cost_tbl(i).I_origin_country_id
            and fc.location             = L_obj_comp_item_cost_tbl(i).I_location
            and fc.active_date          = L_obj_comp_item_cost_tbl(i).I_effective_date;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END COMPUTE_CC_TAX;
-----------------------------------------------------------------------------------------------------------------
FUNCTION EXPLODE_FC    (O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

 L_program                  VARCHAR2(100)                        := 'FUTURE_COST_SQL.EXPLODE_FC';

BEGIN

/* delete records if there are any entries for franchise location with the old costing location for the active date above vdate */
      delete from future_cost wf
         where exists (select 'x'
                          from cost_event_thread ct 
                         where ct.cost_event_process_id=I_cost_event_process_id
                           and wf.item = ct.item
                           and wf.supplier = ct.supplier
                           and wf.origin_country_id = ct.origin_country_id
                           and wf.location= ct.location
                           and wf.store_type='F'
                           and wf.active_date > LP_vdate
                           and rownum <= 1);

   
          insert into future_cost_gtt(item,
                                supplier,
                                origin_country_id,
                                location,
                                loc_type,
                                active_date,
                                base_cost,
                                net_cost,
                                net_net_cost,
                                dead_net_net_cost,
                                pricing_cost,
                                calc_date,
                                start_ind,
                                acquisition_cost,
                                elc_amt,
                                primary_supp_country_ind,
                                currency_code,
                                division,
                                group_no,
                                dept,
                                class,
                                subclass,
                                item_parent,
                                item_grandparent,
                                diff_1,
                                diff_2,
                                diff_3,
                                diff_4,
                                chain,
                                area,
                                region,
                                district,
                                supp_hier_lvl_1,
                                supp_hier_lvl_2,
                                supp_hier_lvl_3,
                                reclass_no,
                                cost_change,
                                simple_pack_ind,
                                primary_cost_pack,
                                primary_cost_pack_qty,
                                store_type,
                                costing_loc,
                                templ_id,
                                passthru_pct,
                                negotiated_item_cost,
                                extended_base_cost,
                                wac_tax,
                                default_costing_type,
                                processing_seq_no)
   select /*+ index(FC, FUTURE_COST_I4) */ distinct 
                    fc.item,
                    fc.supplier,
                   fc.origin_country_id,
                   fc.location,
                   fc.loc_type,
                   fc.active_date,
                   fc.base_cost,
                   fc.net_cost,
                   fc.net_net_cost,
                   fc.dead_net_net_cost,
                   fc.pricing_cost,
                   fc.calc_date,
                   fc.start_ind,
                   fc.acquisition_cost,
                   fc.elc_amt,
                   fc.primary_supp_country_ind,
                   fc.currency_code,
                   fc.division,
                   fc.group_no,
                   fc.dept,
                   fc.class,
                   fc.subclass,
                   fc.item_parent,
                   fc.item_grandparent,
                   fc.diff_1,
                   fc.diff_2,
                   fc.diff_3,
                   fc.diff_4,
                   fc.chain,
                   fc.area,
                   fc.region,
                   fc.district,
                   fc.supp_hier_lvl_1,
                   fc.supp_hier_lvl_2,
                   fc.supp_hier_lvl_3,
                   fc.reclass_no,
                   fc.cost_change,
                   fc.simple_pack_ind,
                   fc.primary_cost_pack,
                   fc.primary_cost_pack_qty,
                   fc.store_type,
                   fc.costing_loc,
                   NULL templ_id,
                   fc.passthru_pct,
                   fc.negotiated_item_cost,
                   fc.extended_base_cost,
                   fc.wac_tax,
                   fc.default_costing_type,
          null as processing_seq_no
     from cost_event_thread ct,
          future_cost fc
    where ct.cost_event_process_id = I_cost_event_process_id
      and (( ct.thread_id           = I_thread_id )or (ct.thread_id =0))
      and ct.item                  = fc.item
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location
     and not exists 
        (   select 1 from 
            future_cost_gtt fcg
            where fcg.item               = fc.item
              and fcg.supplier           = fc.supplier
              and fcg.origin_country_id  = fc.origin_country_id
              and fcg.location           = fc.location
         );


   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

  
 ---
   if MERGE_FC (O_error_message,
                    I_cost_event_process_id,
                    I_thread_id) = FALSE then
      return FALSE;
   end if;
    
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_FC;
-----------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------

FUNCTION MERGE_FC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

BEGIN
   -- insert a row for the new row for franchise loc for the new costing location on the timeline or if a row exists on the same date
   --  update the row with the costing location
   merge into future_cost_gtt fc
   using ( select * from (
                  select fc.item,
                         fc.supplier,
                         fc.origin_country_id,
                         fc.location,
                         fc.loc_type,
                         fc.active_date,
                         fc.base_cost,
                         fc.net_cost,
                         fc.net_net_cost,
                         fc.dead_net_net_cost,
                         fc.pricing_cost,
                         fc.calc_date,
                         fc.start_ind,
                         fc.primary_supp_country_ind,
                         fc.currency_code,
                         fc.division,
                         fc.group_no,
                         fc.dept,
                         fc.class,
                         fc.subclass,
                         fc.item_grandparent,
                         fc.item_parent,
                         fc.diff_1,
                         fc.diff_2,
                         fc.diff_3,
                         fc.diff_4,
                         fc.chain,
                         fc.area,
                         fc.region,
                         fc.district,
                         fc.supp_hier_lvl_1,
                         fc.supp_hier_lvl_2,
                         fc.supp_hier_lvl_3,
                         fc.reclass_no,
                         fc.cost_change,
                         fc.simple_pack_ind,
                         fc.primary_cost_pack,
                         fc.primary_cost_pack_qty,
                         fc.store_type,
                         fc.costing_loc,
                         fc.negotiated_item_cost,
                         fc.extended_base_cost,
                         fc.wac_tax,
                         fc.default_costing_type,
                         cen.cost_loc_change_date   new_cost_loc_change_date,
                         cen.costing_loc  new_cost_loc,
                         rank()
                           over (partition by cen.costing_loc,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from cost_event_cl cen,
                         future_cost_gtt fc
                   where cen.cost_event_process_id = I_cost_event_process_id
                     and (fc.item              = cen.item or
                          fc.item_parent       = cen.item or
                          fc.item_grandparent  = cen.item)
                     and cen.franchise_loc          = fc.location   /* this is to ensure only the costing loc records gets inserted*/
                     and fc.active_date   <= cen.cost_loc_change_date) inner
                   where inner.ranking = 1) use_this
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.new_cost_loc_change_date)
   when matched then
   update
      set fc.costing_loc= use_this.new_cost_loc
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.new_cost_loc_change_date, /* new active date*/
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           use_this.start_ind,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.new_cost_loc, /* new costing location */
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);



   -- records of the new costing location , this will be required to seed the cost events of costing location to the franchise store
   
   merge into future_cost_gtt fc
   using ( select * from (
                  select fc.item,
                         fc.supplier,
                         fc.origin_country_id,
                         fc.location,
                         fc.loc_type,
                         fc.active_date,
                         fc.base_cost,
                         fc.net_cost,
                         fc.net_net_cost,
                         fc.dead_net_net_cost,
                         fc.pricing_cost,
                         fc.calc_date,
                         fc.start_ind,
                         fc.primary_supp_country_ind,
                         fc.currency_code,
                         fc.division,
                         fc.group_no,
                         fc.dept,
                         fc.class,
                         fc.subclass,
                         fc.item_grandparent,
                         fc.item_parent,
                         fc.diff_1,
                         fc.diff_2,
                         fc.diff_3,
                         fc.diff_4,
                         fc.chain,
                         fc.area,
                         fc.region,
                         fc.district,
                         fc.supp_hier_lvl_1,
                         fc.supp_hier_lvl_2,
                         fc.supp_hier_lvl_3,
                         fc.reclass_no,
                         fc.cost_change,
                         fc.simple_pack_ind,
                         fc.primary_cost_pack,
                         fc.primary_cost_pack_qty,
                         fc.store_type,
                         fc.costing_loc,
                         fc.negotiated_item_cost,
                         fc.extended_base_cost,
                         fc.wac_tax,
                         fc.default_costing_type,
                         cen.cost_loc_change_date   new_cost_loc_change_date,
                         cen.costing_loc  new_cost_loc,
                         rank()
                           over (partition by cen.costing_loc,
                                              fc.item,
                                              fc.supplier,
                                              fc.origin_country_id,
                                              fc.location
                                     order by fc.active_date desc) ranking
                    from cost_event_cl cen,
                         future_cost_gtt fc
                   where cen.cost_event_process_id = I_cost_event_process_id
                     and (fc.item              = cen.item or
                          fc.item_parent       = cen.item or
                          fc.item_grandparent  = cen.item)
                     and fc.active_date       <= cen.cost_loc_change_date     
                     and cen.costing_loc       = fc.location  )inner  /* this is to ensure only the costing loc records gets inserted*/
                   where inner.ranking = 1) use_this 
   on (    fc.item                  = use_this.item
       and fc.supplier              = use_this.supplier
       and fc.origin_country_id     = use_this.origin_country_id
       and fc.location              = use_this.location
       and fc.active_date           = use_this.new_cost_loc_change_date)
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.new_cost_loc_change_date, /* new active date*/
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.pricing_cost,
           use_this.calc_date,
           use_this.start_ind,
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc, /* old costing loc - null for company store/wh*/
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

/* Now update the cost of franchise locations with the cost of the new costing location*/
          merge into future_cost_gtt fct
          using (
                select distinct -- distinct to make sure only get dates from wh records
                      fc.item,
                      fc.supplier,
                      fc.origin_country_id,
                      wf.location, 
                      wf.loc_type,
                      fc.active_date,
                      fc.base_cost,
                      fc.net_cost,
                      fc.net_net_cost,
                      fc.dead_net_net_cost,
                      fc.pricing_cost,
                      fc.calc_date,
                      fc.start_ind,
                      fc.acquisition_cost,
                      fc.elc_amt,
                      fc.primary_supp_country_ind,
                      fc.currency_code,
                      fc.division,
                      fc.group_no,
                      fc.dept,
                      fc.class,
                      fc.subclass,
                      fc.item_parent,
                      fc.item_grandparent,
                      fc.diff_1,
                      fc.diff_2,
                      fc.diff_3,
                      fc.diff_4,
                      wf.chain,
                      wf.area,
                      wf.region,
                      wf.district,
                      fc.supp_hier_lvl_1,
                      fc.supp_hier_lvl_2,
                      fc.supp_hier_lvl_3,
                      fc.reclass_no,
                      fc.cost_change,
                      fc.simple_pack_ind,
                      fc.primary_cost_pack,
                      fc.primary_cost_pack_qty,
                      wf.store_type,
                      wf.costing_loc as costing_loc,
                      NULL templ_id,
                      NULL passthru_pct,
                      fc.negotiated_item_cost,
                      fc.extended_base_cost,
                      fc.wac_tax,
                      fc.default_costing_type
             from future_cost_gtt fc,
                  future_cost_gtt wf,
                  cost_event_cl  cen                
            where cen.cost_event_process_id = I_cost_event_process_id
              and (fc.item              = cen.item or
                   fc.item_parent       = cen.item or
                   fc.item_grandparent  = cen.item)
              and cen.franchise_loc     = wf.location 
              and fc.location           = wf.costing_loc  /* this would be having the new costing loc from above merge statement*/
              and fc.item               = wf.item
              and fc.supplier           = wf.supplier
              and fc.origin_country_id  = wf.origin_country_id) use_this 
               on (    fct.item                  = use_this.item
                   and fct.supplier              = use_this.supplier
                   and fct.origin_country_id     = use_this.origin_country_id
                   and fct.location              = use_this.location
                   and fct.active_date           = use_this.active_date)
           when MATCHED then
           update
              set fct.base_cost             = use_this.base_cost,
                  fct.net_cost              = use_this.net_cost,
                  fct.net_net_cost          = use_this.net_net_cost,
                  fct.dead_net_net_cost     = use_this.dead_net_net_cost,
                  fct.pricing_cost          = use_this.pricing_cost,
                  fct.negotiated_item_cost  = use_this.negotiated_item_cost,
                  fct.extended_base_cost    = use_this.extended_base_cost,
                  fct.wac_tax               = use_this.wac_tax;

           
      if ADD_CC(O_error_message,
                I_cost_event_process_id,
                I_thread_id) = FALSE then
         return FALSE;
      end if;

   if LOAD_DEALS(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;   
              

   return TRUE;
   
   

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.MERGE_FC',
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_FC;

-----------------------------------------------------------------------------------------------------------------

PROCEDURE WRITE_ERROR(I_error_message         IN RTK_ERRORS.RTK_TEXT%TYPE,
                      I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                      I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE) AS
BEGIN

   -- Write error to the specified cost event process id and thread id and update the latest retry attempt record.
   -- If no record exists then insert a new row with retry attempt of 1.
   merge into cost_event_result cer
   using (select I_cost_event_process_id as cost_event_process_id,
                 I_thread_id as thread_id,
                 nvl(max(attempt_num), 1) as max_attempt_num
            from cost_event_result
           where cost_event_process_id = I_cost_event_process_id
             and thread_id             = I_thread_id) inner
   on (    cer.cost_event_process_id = inner.cost_event_process_id
       and cer.thread_id             = inner.thread_id
       and cer.attempt_num           = inner.max_attempt_num)
   when matched then
   update set cer.status        = FUTURE_COST_EVENT_SQL.ERROR_COST_EVENT,
              cer.error_message = I_error_message
   when not matched then
   insert (cost_event_process_id,
           thread_id,
           attempt_num,
           status,
           retry_user_id,
           error_message,
           create_datetime)
    values(I_cost_event_process_id,
           I_thread_id,
           1,
           FUTURE_COST_EVENT_SQL.ERROR_COST_EVENT,
           LP_user,
           I_error_message,
           SYSDATE);

END;
----------------------------------------------------------------------------------------
PROCEDURE WRITE_SUCCESS(I_cost_event_process_id IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_thread_id             IN COST_EVENT_THREAD.THREAD_ID%TYPE) AS

   I_error_message   RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN

   -- Write error to the specified cost event process id and thread id and update the latest retry attempt record.
   -- If no record exists then insert a new row with retry attempt of 1.
   merge into cost_event_result cer
   using (select I_cost_event_process_id as cost_event_process_id,
                 I_thread_id as thread_id,
                 nvl(max(attempt_num), 1) as max_attempt_num
            from cost_event_result
           where cost_event_process_id = I_cost_event_process_id
             and thread_id             = I_thread_id) inner
   on (    cer.cost_event_process_id = inner.cost_event_process_id
       and cer.thread_id             = inner.thread_id
       and cer.attempt_num           = inner.max_attempt_num)
   when matched then
   update set cer.status        = FUTURE_COST_EVENT_SQL.COMPLETE_COST_EVENT,
              cer.error_message = NULL
   when not matched then
   insert (cost_event_process_id,
           thread_id,
           attempt_num,
           status,
           retry_user_id,
           error_message,
           create_datetime)
    values(I_cost_event_process_id,
           I_thread_id,
           1,
           FUTURE_COST_EVENT_SQL.COMPLETE_COST_EVENT,
           LP_user,
           NULL,
           SYSDATE);

EXCEPTION
   when OTHERS then
      I_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.WRITE_SUCCESS',
                                            to_char(SQLCODE));
      WRITE_ERROR(I_error_message, I_cost_event_process_id, I_thread_id);
END;
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id               IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                              O_return_code                OUT NUMBER,
                              O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE) IS

   cursor c_event is
      select action,
             event_type,
             persist_ind
        from cost_event
       where cost_event_process_id =  I_cost_event_process_id;

   L_event_type       COST_EVENT.EVENT_TYPE%TYPE;
   L_persist_ind      COST_EVENT.PERSIST_IND%TYPE;
   L_return_code      BOOLEAN;

BEGIN

   L_return_code :=  REFRESH_DATA_STRUCTURES(O_error_message,
                                             I_cost_event_process_id,
                                             I_thread_id);
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

   open c_event;
   fetch c_event into LP_action,
                      L_event_type,
                      L_persist_ind;
   close c_event;
   ---
   L_return_code := GET_CURRENT_ELC(O_error_message,
                                    I_cost_event_process_id,
                                    I_thread_id);

   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;
   ---
   case L_event_type

   when FUTURE_COST_EVENT_SQL.NEW_ITEM_LOC_COST_EVENT_TYPE then
      -- new item loc for approved items
      L_return_code := EXPLODE_NIL(O_error_message,
                                   I_cost_event_process_id,
                                   I_thread_id);

   when FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE then
      -- cost change approval and change from approved status
      L_return_code := EXPLODE_COST_CHANGE(O_error_message,
                                           I_cost_event_process_id,
                                           LP_action,
                                           I_thread_id);

   when FUTURE_COST_EVENT_SQL.RECLASS_COST_EVENT_TYPE then
      -- reclassification
      L_return_code := EXPLODE_RECLASS(O_error_message,
                                       I_cost_event_process_id,
                                       LP_action,
                                       I_thread_id);

   when FUTURE_COST_EVENT_SQL.MERCH_HIER_COST_EVENT_TYPE then
      -- merch hierarchy change
      L_return_code := EXPLODE_MERGE_MERCH_HIER(O_error_message,
                                                I_cost_event_process_id,
                                                I_thread_id);

   when FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE then
      -- org hierarchy change
      L_return_code := EXPLODE_MERGE_ORG_HIER(O_error_message,
                                              I_cost_event_process_id,
                                              I_thread_id);

   when FUTURE_COST_EVENT_SQL.SUPP_HIER_COST_EVENT_TYPE then
      -- supplier hierarchy change
      L_return_code := EXPLODE_MERGE_SUPP_HIER(O_error_message,
                                               I_cost_event_process_id,
                                               I_thread_id);

   when FUTURE_COST_EVENT_SQL.ELC_COST_EVENT_TYPE then
      -- item expense change
      L_return_code := EXPLODE_ELC(O_error_message,
                                   I_cost_event_process_id,
                                   I_thread_id);

   when FUTURE_COST_EVENT_SQL.COST_ZONE_COST_EVENT_TYPE then
      -- location cost zone change
      L_return_code := EXPLODE_COST_ZONE(O_error_message,
                                         I_cost_event_process_id,
                                         I_thread_id);

   when FUTURE_COST_EVENT_SQL.ITEM_COST_ZONE_GRP_EVENT_TYPE then
      -- item cost zone group change
      L_return_code := EXPLODE_ITEM_CZG(O_error_message,
                                        I_cost_event_process_id,
                                        I_thread_id);

   when FUTURE_COST_EVENT_SQL.DEAL_COST_EVENT_TYPE then
      -- deals
      L_return_code := EXPLODE_DEAL(O_error_message,
                                    I_cost_event_process_id,
                                    LP_action,
                                    I_thread_id);

   when FUTURE_COST_EVENT_SQL.PRIMARY_PACK_COST_EVENT_TYPE then
      -- primary cost pack
      L_return_code := EXPLODE_PRIMARY_PACK_COST(O_error_message,
                                                 I_cost_event_process_id,
                                                 I_thread_id,
                                                 L_persist_ind);

   when FUTURE_COST_EVENT_SQL.SUPP_COUNTRY_COST_EVENT_TYPE then
      -- add/delete item/loc/supp/country
      L_return_code := PROCESS_SUPP_CNTR(O_error_message,
                                         I_cost_event_process_id,
                                         LP_action,
                                         I_thread_id);

   when FUTURE_COST_EVENT_SQL.DEAL_PASSTHRU_COST_EVENT_TYPE then
      -- Process deal passthru
      L_return_code := EXPLODE_DEAL_PASSTHRU(O_error_message,
                                             I_cost_event_process_id,
                                             I_thread_id);

   when FUTURE_COST_EVENT_SQL.TEMPLATE_COST_EVENT_TYPE then
      -- Process deal passthru
      L_return_code := EXPLODE_TMPL_CHG(O_error_message,
                                        I_cost_event_process_id,
                                        I_thread_id);

   when FUTURE_COST_EVENT_SQL.TEMPLATE_RELN_COST_EVENT_TYPE then
      -- Process deal passthru
      L_return_code := EXPLODE_TMP_RELATIONSHIP_CHG(O_error_message,
                                                    I_cost_event_process_id,
                                                    LP_action,
                                                    I_thread_id);
                                                    
   when FUTURE_COST_EVENT_SQL.FRAN_COST_LOC_CHG_EVENT_TYPE then
      -- Process deal passthru
      L_return_code := EXPLODE_FC (O_error_message,
                                   I_cost_event_process_id,
                                   I_thread_id);

   when FUTURE_COST_EVENT_SQL.RETAIL_CHANGE_COST_EVENT_TYPE then
      L_return_code := PROCESS_PRICE_CHG (O_error_message,
                                          I_cost_event_process_id,
                                          I_thread_id);
   else
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_EVENT_TYPE');
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end case;

   --explode check
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

   --Calc Future ELC
   L_return_code := CALC_FUTURE_ELC(O_error_message);

   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

  --roll-forward
  if L_event_type <> FUTURE_COST_EVENT_SQL.RETAIL_CHANGE_COST_EVENT_TYPE  then
      L_return_code :=  FUTURE_COST_ROLLFWD_SQL.ROLL_FORWARD(O_error_message,
                                                             LP_action,
                                                             L_event_type,
                                                             I_cost_event_process_id,
                                                             I_thread_id);
   end if;

   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

   --deal with primary packs
   L_return_code :=  SYNC_PRIMARY_PACK(O_error_message,
                                      I_cost_event_process_id,
                                      I_thread_id,
                                      L_persist_ind);
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

   --move updated data from GTTs to future_cost and deal_item_loc_explode
   L_return_code :=  PUSH_BACK_DEAL_ITEM_LOC_EXP(O_error_message,
                                                 I_cost_event_process_id,
                                                 I_thread_id,
                                                 L_persist_ind);
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

   L_return_code :=  PUSH_BACK_FUTURE_COST(O_error_message,
                                           I_cost_event_process_id,
                                           I_thread_id,
                                           L_persist_ind);
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;
   
   if L_event_type = FUTURE_COST_EVENT_SQL.RECLASS_COST_EVENT_TYPE then
      delete from future_cost wf
            where exists (select 'x'
                            from cost_event_thread ct,
                                 cost_event_reclass cer
                           where ct.cost_event_process_id  = I_cost_event_process_id
                             and ct.thread_id              = I_thread_id
                             and cer.cost_event_process_id = ct.cost_event_process_id
                             and ct.item                   = wf.item
                             and ct.supplier               = wf.supplier
                             and ct.origin_country_id      = wf.origin_country_id
                             and ct.location               = wf.costing_loc
                             and wf.item                   = cer.item
                             and wf.active_date            > cer.reclass_date
                           );
   end if;

   L_return_code :=  PURGE_FUTURE_COST(O_error_message,
                                       I_cost_event_process_id,
                                       I_thread_id);
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;

   ---
   ---remove deals from deal_item_loc_explode that are no longer applicable due to hier changes
   ---
   if L_event_type in (FUTURE_COST_EVENT_SQL.RECLASS_COST_EVENT_TYPE,
                       FUTURE_COST_EVENT_SQL.MERCH_HIER_COST_EVENT_TYPE,
                       FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE,
                       FUTURE_COST_EVENT_SQL.SUPP_HIER_COST_EVENT_TYPE) then

      L_return_code :=  PURGE_DEAL_ITEM_LOC_EXPLODE(O_error_message,
                                                    I_cost_event_process_id,
                                                    I_thread_id);
      if not L_return_code then
         O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
         WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
         return;
      end if;
   end if;

   L_return_code :=  REFRESH_DATA_STRUCTURES(O_error_message,
                                             I_cost_event_process_id,
                                             I_thread_id);
   if not L_return_code then
      O_return_code := FUTURE_COST_EVENT_SQL.FAILURE;
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      return;
   end if;
   delete from future_cost_wf_helper_temp
    where cost_event_process_id = I_cost_event_process_id;

   WRITE_SUCCESS(I_cost_event_process_id, I_thread_id);

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PROCESS_COST_EVENTS',
                                            to_char(SQLCODE));
      WRITE_ERROR(O_error_message, I_cost_event_process_id, I_thread_id);
      O_return_code :=  FUTURE_COST_EVENT_SQL.FAILURE;

END PROCESS_COST_EVENTS;
----------------------------------------------------------------------------------------
PROCEDURE PROCESS_COST_EVENTS(I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE) IS

   L_return_code      NUMBER := FUTURE_COST_EVENT_SQL.SUCCESS;
   L_error_message    RTK_ERRORS.RTK_TEXT%TYPE;

BEGIN
   FUTURE_COST_SQL.PROCESS_COST_EVENTS(I_cost_event_process_id,
                                       I_thread_id,
                                       L_return_code,
                                       L_error_message);

   if L_return_code = FUTURE_COST_EVENT_SQL.FAILURE then
      WRITE_ERROR(L_error_message, I_cost_event_process_id, I_thread_id);
   end if;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.PROCESS_COST_EVENTS',
                                            to_char(SQLCODE));
      WRITE_ERROR(L_error_message, I_cost_event_process_id, I_thread_id);

END PROCESS_COST_EVENTS;
----------------------------------------------------------------------------------------
FUNCTION GET_CURRENT_ELC(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_cost_event_process_id   IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                         I_thread_id               IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   --
   merge into gtt_fc_item_exp_detail gfi
   using
   (SELECT ied.*, 
           cet.origin_country_id, 
           LP_vdate active_date,
           cet.LOCATION, 
           cet.pack_no
      FROM (SELECT cet_inner.item, 
                   cet_inner.supplier,
                   cet_inner.origin_country_id, 
                   cet_inner.LOCATION,
                   vpq.pack_no pack_no,
                   vpq.item pack_items,
                   'Y' buyer_complex_pack,
                   im.cost_zone_group_id cost_zone_group_id
              FROM cost_event_thread cet_inner,
                   item_master im,
                   v_packsku_qty vpq
             WHERE cost_event_process_id = I_cost_event_process_id
               AND thread_id = I_thread_id
               AND cet_inner.item = im.item
               AND im.pack_ind = 'Y'
               AND im.simple_pack_ind = 'N'
               AND im.pack_type = 'B'
               AND im.item = vpq.pack_no
            UNION ALL
            SELECT cet_inner.item, 
                   cet_inner.supplier,
                   cet_inner.origin_country_id, 
                   cet_inner.LOCATION,
                   '-999' pack_no, 
                   NULL pack_items, 
                   'N' buyer_complex_pack,
                   im.cost_zone_group_id cost_zone_group_id
              FROM cost_event_thread cet_inner, item_master im
             WHERE cost_event_process_id = I_cost_event_process_id
               AND thread_id = I_thread_id
               AND cet_inner.item = im.item
               AND (im.pack_type != 'B'
                    OR im.pack_ind != 'Y'
                    OR im.simple_pack_ind != 'N')) cet,
            item_exp_detail ied,
            item_exp_head ieh,
            cost_zone_group_loc czgl
    WHERE ied.item = DECODE (buyer_complex_pack,'N',cet.item,'Y', cet.pack_items)
      AND ied.item = ieh.item
      AND ieh.zone_id = czgl.zone_id
      AND ((ieh.discharge_port = czgl.primary_discharge_port) OR (ieh.base_exp_ind = 'Y'))
      AND ied.supplier = ieh.supplier
      AND ieh.zone_group_id = czgl.zone_group_id
      AND ied.item_exp_type = ieh.item_exp_type
      AND ied.item_exp_seq = ieh.item_exp_seq
      AND ied.supplier = cet.supplier
      AND czgl.LOCATION = cet.LOCATION
      AND ied.item_exp_type = 'Z'
    UNION ALL
    SELECT ied.*, 
           ieh.origin_country_id, 
           LP_vdate active_date,
           cet.LOCATION, 
           cet.pack_no
      FROM (SELECT cet_inner.item, 
                   cet_inner.supplier,
                   cet_inner.origin_country_id, 
                   cet_inner.LOCATION,
                   vpq.pack_no pack_no, 
                   vpq.item pack_items,
                   'Y' buyer_complex_pack,
                   im.cost_zone_group_id cost_zone_group_id
              FROM cost_event_thread cet_inner,
                   item_master im,
                   v_packsku_qty vpq
             WHERE cost_event_process_id = I_cost_event_process_id
               AND thread_id = I_thread_id
               AND cet_inner.item = im.item
               AND im.pack_ind = 'Y'
               AND im.simple_pack_ind = 'N'
               AND im.pack_type = 'B'
               AND im.item = vpq.pack_no
            UNION ALL
            SELECT cet_inner.item, 
                   cet_inner.supplier,
                   cet_inner.origin_country_id, 
                   cet_inner.LOCATION,
                   '-999' pack_no,
                   NULL pack_items,
                   'N' buyer_complex_pack,
                   im.cost_zone_group_id cost_zone_group_id
              FROM cost_event_thread cet_inner, item_master im
             WHERE cost_event_process_id = I_cost_event_process_id
               AND thread_id = I_thread_id
               AND cet_inner.item = im.item
               AND (im.pack_type != 'B'
                    OR im.pack_ind != 'Y'
                    OR im.simple_pack_ind != 'N')) cet,
           item_exp_detail ied,
           item_exp_head ieh
     WHERE ied.item = DECODE (buyer_complex_pack,'N', cet.item,'Y', cet.pack_items)
       AND ied.item = ieh.item
       AND ied.supplier = ieh.supplier
       AND ied.item_exp_type = ieh.item_exp_type
       AND ied.item_exp_seq = ieh.item_exp_seq
       AND ied.supplier = cet.supplier
       AND ieh.origin_country_id = cet.origin_country_id
       AND ied.item_exp_type = 'C') use_this
   on (gfi.item = use_this.item
       and gfi.supplier = use_this.supplier
       and gfi.origin_country_id = use_this.origin_country_id
       and gfi.location = use_this.location
       and gfi.active_date = use_this.active_date
       and gfi.item_exp_type = use_this.item_exp_type
       and gfi.item_exp_seq = use_this.item_exp_seq
       and gfi.comp_id = use_this.comp_id
       and gfi.pack_no = use_this.pack_no)
   when not matched then
   insert (item,
           supplier,
           item_exp_type,
           item_exp_seq,
           comp_id,
           cvb_code,
           comp_rate,
           comp_currency,
           per_count,
           per_count_uom,
           est_exp_value,
           nom_flag_1,
           nom_flag_2,
           nom_flag_3,
           nom_flag_4,
           nom_flag_5,
           display_order,
           create_datetime,
           last_update_datetime,
           last_update_id,
           defaulted_from,
           key_value_1,
           key_value_2,
           origin_country_id,
           active_date,
           location,
           pack_no)
    values (use_this.item,
            use_this.supplier,
            use_this.item_exp_type,
            use_this.item_exp_seq,
            use_this.comp_id,
            use_this.cvb_code,
            use_this.comp_rate,
            use_this.comp_currency,
            use_this.per_count,
            use_this.per_count_uom,
            use_this.est_exp_value,
            use_this.nom_flag_1,
            use_this.nom_flag_2,
            use_this.nom_flag_3,
            use_this.nom_flag_4,
            use_this.nom_flag_5,
            use_this.display_order,
            use_this.create_datetime,
            use_this.last_update_datetime,
            use_this.last_update_id,
            use_this.defaulted_from,
            use_this.key_value_1,
            use_this.key_value_2,
            use_this.origin_country_id,
            use_this.active_date,
            use_this.location,
            use_this.pack_no);

   if L_system_options_rec.hts_tracking_level = 'S' then

      insert into gtt_fc_item_hts_assess(item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         comp_id,cvb_code,
                                         comp_rate,per_count,
                                         per_count_uom,
                                         est_assess_value,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         supplier,
                                         active_date)
                                  select iha.item,
                                         iha.hts,
                                         iha.import_country_id,
                                         iha.origin_country_id,
                                         iha.effect_from,
                                         iha.effect_to,
                                         iha.comp_id,cvb_code,
                                         iha.comp_rate,per_count,
                                         iha.per_count_uom,
                                         iha.est_assess_value,
                                         iha.nom_flag_1,
                                         iha.nom_flag_2,
                                         iha.nom_flag_3,
                                         iha.nom_flag_4,
                                         iha.nom_flag_5,
                                         iha.display_order,
                                         iha.create_datetime,
                                         iha.last_update_datetime,
                                         iha.last_update_id,
                                         cet.supplier,
                                         LP_vdate
                                    from (select DISTINCT item,
                                                 supplier,
                                                 origin_country_id
                                            from cost_event_thread
                                           where cost_event_process_id = I_cost_event_process_id
                                             and thread_id = I_thread_id) cet,
                                         item_hts_assess iha
                                   where cet.item = iha.item
                                     and cet.origin_country_id = iha.origin_country_id;

   elsif L_system_options_rec.hts_tracking_level = 'M' then

      insert into gtt_fc_item_hts_assess(item,
                                            hts,
                                            import_country_id,
                                            origin_country_id,
                                            effect_from,
                                            effect_to,
                                            comp_id,cvb_code,
                                            comp_rate,per_count,
                                            per_count_uom,
                                            est_assess_value,
                                            nom_flag_1,
                                            nom_flag_2,
                                            nom_flag_3,
                                            nom_flag_4,
                                            nom_flag_5,
                                            display_order,
                                            create_datetime,
                                            last_update_datetime,
                                            last_update_id,
                                            supplier,
                                            active_date)
                                     select iha.item,
                                            iha.hts,
                                            iha.import_country_id,
                                            iha.origin_country_id,
                                            iha.effect_from,
                                            iha.effect_to,
                                            iha.comp_id,cvb_code,
                                            iha.comp_rate,per_count,
                                            iha.per_count_uom,
                                            iha.est_assess_value,
                                            iha.nom_flag_1,
                                            iha.nom_flag_2,
                                            iha.nom_flag_3,
                                            iha.nom_flag_4,
                                            iha.nom_flag_5,
                                            iha.display_order,
                                            iha.create_datetime,
                                            iha.last_update_datetime,
                                            iha.last_update_id,
                                            cet.supplier,
                                            LP_vdate
                                 from (select DISTINCT item,
                                              supplier,
                                              origin_country_id
                                         from cost_event_thread
                                        where cost_event_process_id = I_cost_event_process_id
                                          and thread_id = I_thread_id) cet,
                                      item_hts_assess iha,
                                      item_supp_manu_country ispmc
                                where cet.item = iha.item
                                  and cet.item = ispmc.item
                                  and cet.supplier = ispmc.supplier
                                  and cet.origin_country_id = ispmc.manu_country_id
                                  and ispmc.primary_manu_ctry_ind = 'Y'
                                  and ispmc.manu_country_id = iha.origin_country_id;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.GET_CURRENT_ELC',
                                            to_char(SQLCODE));
      return FALSE;
END GET_CURRENT_ELC;

--------------------------------------------------------------------------------------------
FUNCTION CALC_FUTURE_ELC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_prev_item   ITEM_MASTER.ITEM%TYPE := NULL;
   
   cursor C_GET_FUTURE_COST_EVENT is
      select distinct DECODE(default_costing_type, 'NIC', negotiated_item_cost, base_cost) bc,
             item,
             origin_country_id,
             supplier,
             fcg.active_date,
             location
        from future_cost_gtt fcg
       where cost_change is not null
         and fcg.active_date > LP_vdate
       order by item;


   cursor C_GET_ITEM_EXP(L_item VARCHAR2,
                         L_supplier NUMBER,
                         L_origin_country_id VARCHAR2,
                         L_active_date DATE,
                         L_location NUMBER) is
      select *
        from gtt_fc_item_exp_detail ied
       where item = L_item
         and supplier = L_supplier
         and origin_country_id = L_origin_country_id
         and active_date = L_active_date
         and active_date > LP_vdate
         and location = L_location
       order by display_order,
                cvb_code desc;

   cursor C_GET_ITEM_HTS(L_item VARCHAR2) is
      select ihg.*
        from gtt_fc_item_hts_assess ihg
       where ihg.item = L_item
         and active_date > LP_vdate;

   L_est_value ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE;

BEGIN

   for i in C_GET_FUTURE_COST_EVENT loop
      insert into gtt_fc_item_exp_detail(item,
                                         supplier,
                                         item_exp_type,
                                         item_exp_seq,
                                         comp_id,
                                         cvb_code,
                                         comp_rate,
                                         comp_currency,
                                         per_count,
                                         per_count_uom,
                                         est_exp_value,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         create_datetime,
                                         last_update_datetime,
                                         last_update_id,
                                         defaulted_from,
                                         key_value_1,
                                         key_value_2,
                                         origin_country_id,
                                         active_date,
                                         location,
                                         pack_no)
                                  select item,
                                         supplier,
                                         item_exp_type,
                                         item_exp_seq,
                                         comp_id,
                                         cvb_code,
                                         comp_rate,
                                         comp_currency,
                                         per_count,
                                         per_count_uom,
                                         0,
                                         nom_flag_1,
                                         nom_flag_2,
                                         nom_flag_3,
                                         nom_flag_4,
                                         nom_flag_5,
                                         display_order,
                                         create_datetime,
                                         sysdate,
                                         LP_user,
                                         defaulted_from,
                                         key_value_1,
                                         key_value_2,
                                         origin_country_id,
                                         i.active_date,
                                         i.location,
                                         pack_no
                                    from gtt_fc_item_exp_detail ieg
                                   where ieg.active_date = LP_vdate
                                     and ieg.item = i.item
                                     and ieg.supplier = i.supplier
                                     and origin_country_id = i.origin_country_id
                                     and ieg.location      = i.location;

      for j in C_GET_ITEM_EXP(i.item, i.supplier, i.origin_country_id, i.active_date, i.location) loop
         if RECALC_ELC(O_error_message,
                       L_est_value,
                       'D',         -- Passing a 'D' for 'Component Details'
                       j.comp_id,
                       'IE',-- I_item_exp_detail.calc_type,
                       j.item,
                       j.supplier,
                       j.item_exp_type,
                       j.item_exp_seq,
                       NULL,
                       NULL,
                       j.origin_country_id,
                       NULL,
                       NULL,
                       i.bc,
                       i.active_date,
                       i.location) = FALSE then
            return FALSE;
         end if;

         -- Update the table with the calculated value of the component
         update gtt_fc_item_exp_detail
            set est_exp_value = L_est_value,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where item = j.item
            and supplier = j.supplier
            and item_exp_type = j.item_exp_type
            and item_exp_seq = j.item_exp_seq
            and comp_id = j.comp_id
            and active_date = i.active_date
            and origin_country_id = j.origin_country_id
            and location      = j.location;
         ---
         if RECALC_ELC(O_error_message,
                       L_est_value,
                       'F',         -- Passing a 'F' for 'Nomination Flags'
                       j.comp_id,
                       'IE',        -- I_item_exp_detail.calc_type,
                       j.item,
                       j.supplier,
                       j.item_exp_type,
                       j.item_exp_seq,
                       NULL,
                       NULL,
                       j.origin_country_id,
                       NULL,
                       NULL,
                       i.bc,
                       i.active_date,
                       i.location) = FALSE then
            return FALSE;
         end if;
         ---
         -- Update the table by adding the value of the Component Flags
         -- to the components base value.
         ---
         update gtt_fc_item_exp_detail
            set est_exp_value = (est_exp_value + L_est_value),
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where item = j.item
            and supplier = j.supplier
            and item_exp_type = j.item_exp_type
            and item_exp_seq = j.item_exp_seq
            and comp_id = j.comp_id
            and active_date = i.active_date
            and origin_country_id = j.origin_country_id
            and location      = j.location;
     end loop; -- C_GET_ITEM_EXP

     if L_prev_item IS NULL or i.item <> L_prev_item then
        insert into gtt_fc_item_hts_assess(item,
                                           hts,
                                           import_country_id,
                                           origin_country_id,
                                           effect_from,
                                           effect_to,
                                           comp_id,cvb_code,
                                           comp_rate,per_count,
                                           per_count_uom,
                                           est_assess_value,
                                           nom_flag_1,
                                           nom_flag_2,
                                           nom_flag_3,
                                           nom_flag_4,
                                           nom_flag_5,
                                           display_order,
                                           create_datetime,
                                           last_update_datetime,
                                           last_update_id,
                                           supplier,
                                           active_date)
                                    select item,
                                           hts,
                                           import_country_id,
                                           origin_country_id,
                                           effect_from,
                                           effect_to,
                                           comp_id,cvb_code,
                                           comp_rate,per_count,
                                           per_count_uom,
                                           0,
                                           nom_flag_1,
                                           nom_flag_2,
                                           nom_flag_3,
                                           nom_flag_4,
                                           nom_flag_5,
                                           display_order,
                                           create_datetime,
                                           last_update_datetime,
                                           last_update_id,
                                           supplier,
                                           i.active_date
                                      from gtt_fc_item_hts_assess ihg
                                     where ihg.active_date = LP_vdate
                                       and ihg.item = i.item;

        for k in C_GET_ITEM_HTS(i.item) loop
           if UPDATE_FC_TARIFF_RATES(O_error_message,
                                     'IA',
                                     NULL,
                                     NULL,
                                     k.item,
                                     NULL,
                                     k.supplier,
                                     k.hts,
                                     k.import_country_id,
                                     k.origin_country_id,
                                     k.effect_from,
                                     k.effect_to,
                                     k.comp_id) = FALSE then
               return FALSE;
            end if;
            ---
            if RECALC_ELC(O_error_message,
                          L_est_value,
                          'D',          -- Passing a 'D' for 'Component Details'
                          k.comp_id,
                          'IA',
                          k.item,
                          k.supplier,
                          NULL,
                          NULL,
                          k.hts,
                          k.import_country_id,
                          k.origin_country_id,
                          k.effect_from,
                          k.effect_to,
                          i.bc,
                          i.active_date) = FALSE then
               return FALSE;
            end if;
            ---
            update gtt_fc_item_hts_assess
               set est_assess_value = L_est_value,
                   last_update_datetime = sysdate,
                   last_update_id = LP_user
             where item = k.item
               and hts = k.hts
               and import_country_id = k.import_country_id
               and origin_country_id = k.origin_country_id
               and effect_from = k.effect_from
               and effect_to = k.effect_to
               and comp_id = k.comp_id
               and active_date = i.active_date;
            ---
            if RECALC_ELC(O_error_message,
                          L_est_value,
                          'F',        -- Passing a 'F' for 'Nomination Flags'
                          k.comp_id,
                          'IA',
                          k.item,
                          k.supplier,
                          NULL,
                          NULL,
                          k.hts,
                          k.import_country_id,
                          k.origin_country_id,
                          k.effect_from,
                          k.effect_to,
                          i.bc,
                          i.active_date) = FALSE then
               return FALSE;
            end if;
            ---
            update gtt_fc_item_hts_assess
               set est_assess_value = (est_assess_value + L_est_value),
                   last_update_datetime = sysdate,
                   last_update_id = LP_user
             where item  = k.item
               and hts = k.hts
               and import_country_id = k.import_country_id
               and origin_country_id = k.origin_country_id
               and supplier = k.supplier
               and effect_from = k.effect_from
               and effect_to = k.effect_to
               and comp_id = k.comp_id
               and active_date = i.active_date;
        end loop;
        L_prev_item := i.item;
     end if;
     -- Recalculate expenses again becuase expenses could be dependen on the assessment
     update gtt_fc_item_exp_detail
        set est_exp_value = 0,
            last_update_datetime = sysdate,
            last_update_id = LP_user
      where item = i.item
        and supplier = i.supplier
        and active_date = i.active_date
        and origin_country_id = i.origin_country_id
        and location      = i.location;

     for j in C_GET_ITEM_EXP(i.item, i.supplier, i.origin_country_id, i.active_date, i.location) loop
         if RECALC_ELC(O_error_message,
                       L_est_value,
                       'D',         -- Passing a 'D' for 'Component Details'
                       j.comp_id,
                       'IE',        -- I_item_exp_detail.calc_type,
                       j.item,
                       j.supplier,
                       j.item_exp_type,
                       j.item_exp_seq,
                       NULL,
                       NULL,
                       j.origin_country_id,
                       NULL,
                       NULL,
                       i.bc,
                       i.active_date,
                       i.location) = FALSE then
            return FALSE;
         end if;

         -- Update the table with the calculated value of the component
         update gtt_fc_item_exp_detail
            set est_exp_value = L_est_value,
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where item = j.item
            and supplier = j.supplier
            and item_exp_type = j.item_exp_type
            and item_exp_seq = j.item_exp_seq
            and comp_id = j.comp_id
            and active_date = i.active_date
            and origin_country_id = j.origin_country_id
            and location      = j.location;
         ---
         if RECALC_ELC(O_error_message,
                       L_est_value,
                       'F',         -- Passing a 'F' for 'Nomination Flags'
                       j.comp_id,
                       'IE',        -- I_item_exp_detail.calc_type,
                       j.item,
                       j.supplier,
                       j.item_exp_type,
                       j.item_exp_seq,
                       NULL,
                       NULL,
                       j.origin_country_id,
                       NULL,
                       NULL,
                       i.bc,
                       i.active_date,
                       i.location) = FALSE then
            return FALSE;
         end if;
         ---
         -- Update the table by adding the value of the Component Flags
         -- to the components base value.
         ---
         update gtt_fc_item_exp_detail
            set est_exp_value = (est_exp_value + L_est_value),
                last_update_datetime = sysdate,
                last_update_id = LP_user
          where item = j.item
            and supplier = j.supplier
            and item_exp_type = j.item_exp_type
            and item_exp_seq = j.item_exp_seq
            and comp_id = j.comp_id
            and active_date = i.active_date
            and origin_country_id = j.origin_country_id
            and location      = j.location;
      end loop;
   end loop; -- C_GET_FUTURE_COST_EVENT
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_SQL.CALC_FUTURE_ELC',
                                            to_char(SQLCODE));
      return FALSE;
END CALC_FUTURE_ELC;
----------------------------------------------------------------------------------------------
FUNCTION RECALC_ELC(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_est_value           IN OUT   NUMBER,
                    I_dtl_flag            IN       VARCHAR2,
                    I_comp_id             IN       ELC_COMP.COMP_ID%TYPE,
                    I_calc_type           IN       VARCHAR2,
                    I_item                IN       ITEM_MASTER.ITEM%TYPE,
                    I_supplier            IN       SUPS.SUPPLIER%TYPE,
                    I_item_exp_type       IN       ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                    I_item_exp_seq        IN       ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                    I_hts                 IN       HTS.HTS%TYPE,
                    I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                    I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                    I_effect_from         IN       HTS.EFFECT_FROM%TYPE,
                    I_effect_to           IN       HTS.EFFECT_TO%TYPE,
                    I_new_cost            IN       FUTURE_COST.BASE_COST%TYPE DEFAULT NULL,
                    I_active_date         IN       DATE DEFAULT NULL,
                    I_location            IN       NUMBER DEFAULT NULL)
   RETURN BOOLEAN IS

   -- if I_calc_type is 'IE' then the function will calculate Item Expenses
   --                   'IA' Item Assessments
   --                   'PE' Purchase Order Expenses
   --                   'PA' Purchase Order Assessments

   L_program                   VARCHAR2(62)  := 'RECALC_ELC';
   L_oper                      VARCHAR2(1)   := '+';
   L_counter                   NUMBER;
   L_amount                    NUMBER   := 0;
   L_amount_temp               NUMBER   := 0;
   L_value                     NUMBER   := 0;
   L_per_unit_value            NUMBER   := 0;
   L_greater                   CVB_HEAD.COMBO_OPER%TYPE               := '>';
   L_smaller                   CVB_HEAD.COMBO_OPER%TYPE               := '<';
   ----
   L_exp_dtl_amt               NUMBER   := 0;
   L_exp_dtl_amt_prim          NUMBER   := 0;
   L_exp_dtl_amt_zone          NUMBER   := 0;
   L_exp_dtl_amt_zone_prim     NUMBER   := 0;
   L_exp_dtl_amt_ctry          NUMBER   := 0;
   L_exp_dtl_amt_ctry_prim     NUMBER   := 0;
   L_exp_assess_dtl_amt        NUMBER   := 0;
   L_exp_assess_dtl_amt_prim   NUMBER   := 0;
   L_exp_flag_amt              NUMBER   := 0;
   L_exp_flag_amt_prim         NUMBER   := 0;
   L_exp_flag_amt_zone         NUMBER   := 0;
   L_exp_flag_amt_zone_prim    NUMBER   := 0;
   L_exp_flag_amt_ctry         NUMBER   := 0;
   L_exp_flag_amt_ctry_prim    NUMBER   := 0;
   L_exp_assess_flag_amt       NUMBER   := 0;
   L_exp_assess_flag_amt_prim  NUMBER   := 0;
   L_assess_dtl_amt            NUMBER   := 0;
   L_assess_dtl_amt_prim       NUMBER   := 0;
   L_assess_flag_amt           NUMBER   := 0;
   L_assess_flag_amt_prim      NUMBER   := 0;
   L_assess_exp_dtl_amt        NUMBER   := 0;
   L_assess_exp_dtl_amt_prim   NUMBER   := 0;
   L_assess_exp_flag_amt       NUMBER   := 0;
   L_assess_exp_flag_amt_prim  NUMBER   := 0;
   ---
   L_calc_basis                ELC_COMP.CALC_BASIS%TYPE;
   L_cvb_code                  CVB_HEAD.CVB_CODE%TYPE;
   L_comp_rate                 ELC_COMP.COMP_RATE%TYPE;
   L_cost_basis                ELC_COMP.COST_BASIS%TYPE;
   L_comp_currency             CURRENCIES.CURRENCY_CODE%TYPE;
   L_per_count                 ELC_COMP.PER_COUNT%TYPE;
   L_per_count_uom             UOM_CLASS.UOM%TYPE;
   L_zone_group_id             COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
   L_nom_flag_1                ELC_COMP.NOM_FLAG_1%TYPE;
   L_nom_flag_2                ELC_COMP.NOM_FLAG_1%TYPE;
   L_nom_flag_3                ELC_COMP.NOM_FLAG_1%TYPE;
   L_nom_flag_4                ELC_COMP.NOM_FLAG_1%TYPE;
   L_nom_flag_5                ELC_COMP.NOM_FLAG_1%TYPE;
   L_consolidation_ind         SYSTEM_OPTIONS.CONSOLIDATION_IND%TYPE;
   L_orig_currency_code_prim   CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_code_prim        CURRENCIES.CURRENCY_CODE%TYPE;
   L_currency_rate_prim        CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_currency_code_sup         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_type             CURRENCY_RATES.EXCHANGE_TYPE%TYPE;
   L_exchange_rate             CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_currency_cost_dec         CURRENCIES.CURRENCY_COST_DEC%TYPE;
   L_vdate                     DATE;
   L_exists                    BOOLEAN;
   L_supplier                  SUPS.SUPPLIER%TYPE;
   L_origin_country_id         COUNTRY.COUNTRY_ID%TYPE;
   L_supp_pack_size            ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;
   L_ship_carton_wt            ITEM_SUPP_COUNTRY_DIM.WEIGHT%TYPE;
   L_weight_uom                ITEM_SUPP_COUNTRY_DIM.WEIGHT_UOM%TYPE;
   L_ship_carton_len           ITEM_SUPP_COUNTRY_DIM.LENGTH%TYPE;
   L_ship_carton_hgt           ITEM_SUPP_COUNTRY_DIM.HEIGHT%TYPE;
   L_ship_carton_wid           ITEM_SUPP_COUNTRY_DIM.WIDTH%TYPE;
   L_dimension_uom             ITEM_SUPP_COUNTRY_DIM.LWH_UOM%TYPE;
   L_liquid_volume             ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME%TYPE;
   L_liquid_volume_uom         ITEM_SUPP_COUNTRY_DIM.LIQUID_VOLUME_UOM%TYPE;
   L_standard_uom              UOM_CLASS.UOM%TYPE;
   L_uom                       UOM_CLASS.UOM%TYPE;
   L_standard_class            UOM_CLASS.UOM_CLASS%TYPE;
   L_uom_class                 UOM_CLASS.UOM_CLASS%TYPE;
   L_uom_conv_factor           ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_unit_of_work              IF_ERRORS.UNIT_OF_WORK%TYPE;
   L_emu_participating_ind     BOOLEAN;
   L_primary_curr_to_euro_rate CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_euro_comp_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   L_euro_comp_currency_exists VARCHAR2(1) := 'N';
   L_temp                      NUMBER      := 0;
   L_ti                        ITEM_SUPP_COUNTRY.TI%TYPE;
   L_hi                        ITEM_SUPP_COUNTRY.HI%TYPE;
   L_assess_value              ORDSKU_HTS_ASSESS.EST_ASSESS_VALUE%TYPE;
   L_comp_id                   CVB_DETAIL.COMP_ID%TYPE;
   L_curr_code                 CURRENCIES.CURRENCY_CODE%TYPE;
   L_first_cycle               VARCHAR2(1) := 'N';
   L_oper_flag                 CVB_HEAD.COMBO_OPER%TYPE := NULL;
   cursor C_GET_PRIM_CURR_INFO is
      select r.exchange_rate,
             c.currency_cost_dec
        from currencies c,
             currency_rates r
       where c.currency_code  = L_currency_code_prim
         and c.currency_code  = r.currency_code
         and r.exchange_type  = L_exchange_type
         and r.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.exchange_type = L_exchange_type
                                    and cr.currency_code = L_currency_code_prim
                                    and cr.effective_date <= L_vdate);

   cursor C_GET_CVB_FLAGS is
      select nom_flag_1,
             nom_flag_2,
             nom_flag_3,
             nom_flag_4,
             nom_flag_5
        from cvb_head
       where cvb_code = L_cvb_code;

   -------------------------------------------
   -- Item Expense Cursors
   ---

   cursor C_ITEM_EXP_INFO is
      select elc.calc_basis,
             ieg.cvb_code,
             ieg.comp_rate,
             ieg.comp_currency,
             ieg.per_count,
             ieg.per_count_uom
        from elc_comp elc,
             gtt_fc_item_exp_detail ieg
       where ieg.item = I_item
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ieg.comp_id = I_comp_id
         and ieg.comp_id = elc.comp_id
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   ---
   cursor C_SUM_EXP_DTLS is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location      = I_location;
   ---
   cursor C_EURO_SUM_EXP_DTLS_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type  = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                    from currency_rates cr
                                   where cr.currency_code = ieg.comp_currency
                                     and cr.exchange_type = L_exchange_type
                                     and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_EXP_DTLS_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             cvb_detail cd,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location      = I_location;

   cursor C_GET_CURR_DTLS is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             cvb_detail cd
       where ieg.item = I_item
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ieg.comp_currency != L_currency_code_prim
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location      = I_location;
   ---
   cursor C_SUM_EXP_DTLS_PRIM is
      select NVL(SUM(ieg.est_exp_value),0)
        from gtt_fc_item_exp_detail ieg,
             cvb_detail cd
       where ieg.item = I_item
         and ieg.comp_currency = L_currency_code_prim
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location      = I_location;
   ---
   cursor C_SUM_EXP_DTLS_CTRY is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.base_exp_ind = 'Y'
         and ih.item_exp_type = 'C'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_EURO_SUM_EXP_DTLS_CTRY_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                  from currency_rates cr
                                 where cr.currency_code = ieg.comp_currency
                                   and cr.exchange_type = L_exchange_type
                                   and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ieg.item_exp_seq = ih.item_exp_seq
         and ih.base_exp_ind = 'Y'
         and ih.item_exp_type = 'C'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_EXP_DTLS_CTRY_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ieg.item_exp_seq = ih.item_exp_seq
         and ih.base_exp_ind = 'Y'
         and ih.item_exp_type = 'C'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_GET_CURR_DTLS_CTRY is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd
       where ih.item = I_item
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ieg.item_exp_seq = ih.item_exp_seq
         and ih.base_exp_ind = 'Y'
         and ih.item_exp_type = 'C'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.comp_currency != L_currency_code_prim
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_SUM_EXP_DTLS_CTRY_PRIM is
      select NVL(SUM(ieg.est_exp_value),0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd
       where ieg.item = I_item
         and ieg.comp_currency = L_currency_code_prim
         and ieg.supplier = I_supplier
         and ieg.comp_id = cd.comp_id
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.base_exp_ind = 'Y'
         and ih.item_exp_type = 'C'
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date;
   ---
   cursor C_SUM_EXP_DTLS_ZONE is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ih.supplier = ieg.supplier
         and ih.base_exp_ind = 'Y'
         and ih.item_exp_type = 'Z'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
          and ieg.active_date = I_active_date
          and ieg.location = I_location;
   ---
   cursor C_EURO_SUM_EXP_DTLS_ZONE_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_EXP_DTLS_ZONE_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location  = I_location;

   cursor C_GET_CURR_DTLS_ZONE is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd
       where ih.item = I_item
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.comp_currency != L_currency_code_prim
         and ieg.active_date = I_active_date
         and ieg.location          = I_location;
   ---
   cursor C_SUM_EXP_DTLS_ZONE_PRIM is
      select NVL(SUM(ieg.est_exp_value),0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd
       where ieg.item = I_item
         and ieg.comp_currency = L_currency_code_prim
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_ASSESS_DTLS is
      select NVL(SUM(a.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess a,
             elc_comp e,
             cvb_detail cd,
             currency_rates c
       where a.item = I_item
         and e.comp_id = a.comp_id
         and e.comp_currency = c.currency_code
         and e.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and a.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and a.active_date = i_active_date;
   ---
   cursor C_EURO_SUM_EXP_ASSESS_DTLS_1 is
      select NVL(SUM(a.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess a,
             elc_comp e,
             cvb_detail cd,
             currency_rates c
       where a.item = I_item
         and e.comp_id = a.comp_id
         and e.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and a.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and a.active_date = i_active_date;

   cursor C_EURO_SUM_EXP_ASSESS_DTLS_2 is
      select NVL(SUM(a.est_assess_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_hts_assess a,
             elc_comp e,
             cvb_detail cd,
             currency_rates c,
             euro_exchange_rate euro
       where a.item = I_item
         and e.comp_id = a.comp_id
         and e.comp_currency = euro.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and a.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and a.active_date = i_active_date;

   cursor C_GET_CURR_EA_DTLS is
      select distinct e.comp_currency
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             cvb_detail cd
       where it.item = I_item
         and it.comp_id = e.comp_id
         and it.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and e.comp_currency != L_currency_code_prim
         and it.active_date = i_active_date;
   ---
   cursor C_SUM_EXP_ASSESS_DTLS_PRIM is
      select NVL(SUM(a.est_assess_value),0)
        from gtt_fc_item_hts_assess a,
             elc_comp e,
             cvb_detail cd
       where a.item = I_item
         and a.comp_id = e.comp_id
         and e.comp_currency = L_currency_code_prim
         and a.comp_id  = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and a.active_date = i_active_date;
   ---
   cursor C_SUM_EXP_FLAGS is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_EURO_SUM_EXP_FLAGS_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_EURO_SUM_EXP_FLAGS_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_GET_CURR_FLAGS is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg
       where item = I_item
         and supplier = I_supplier
         and item_exp_type = I_item_exp_type
         and item_exp_seq = I_item_exp_seq
         and comp_currency != L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and nom_flag_5 = L_oper))
        and ieg.active_date = I_active_date
        and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_FLAGS_PRIM is
      select NVL(SUM(ieg.est_exp_value), 0)
        from gtt_fc_item_exp_detail ieg
       where ieg.item = I_item
         and ieg.comp_currency = L_currency_code_prim
         and ieg.supplier = I_supplier
         and ieg.item_exp_type = I_item_exp_type
         and ieg.item_exp_seq = I_item_exp_seq
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_FLAGS_ZONE is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   ---
   cursor C_EURO_SUM_EXP_FLAGS_ZONE_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_EXP_FLAGS_ZONE_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_GET_CURR_FLAGS_ZONE is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih
       where ieg.item = I_item
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.comp_currency != L_currency_code_prim
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_FLAGS_ZONE_PRIM is
      select NVL(SUM(ieg.est_exp_value), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih
       where ieg.item = I_item
         and ieg.comp_currency = L_currency_code_prim
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.item_exp_type = 'Z'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_FLAGS_CTRY is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code   = ieg.comp_currency
                                    and cr.exchange_type   = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq  = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.item_exp_type = 'C'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date  = I_active_date
         and ieg.location = I_location;

   ---
   cursor C_EURO_SUM_EXP_FLAGS_CTRY_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c
       where ieg.item = I_item
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.item_exp_type = 'C'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_EXP_FLAGS_CTRY_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq  = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.item_exp_type = 'C'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_GET_CURR_FLAGS_CTRY is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih
       where ieg.item = I_item
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.item_exp_type = 'C'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.comp_currency != L_currency_code_prim
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_FLAGS_CTRY_PRIM is
      select NVL(SUM(ieg.est_exp_value), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih
       where ieg.item = I_item
         and ieg.comp_currency = L_currency_code_prim
         and ieg.supplier = I_supplier
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ih.origin_country_id = L_origin_country_id
         and ih.item_exp_type = 'C'
         and ih.base_exp_ind = 'Y'
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_EXP_ASSESS_FLAGS is
      select NVL(SUM(it.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             currency_rates c
       where it.item = I_item
         and it.comp_id = e.comp_id
         and e.comp_currency = c.currency_code
         and e.comp_currency != L_currency_code_prim
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1   = 'Y' and it.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and it.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and it.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and it.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and it.nom_flag_5 = L_oper))
         and it.active_date = i_active_date;
   ---
   cursor C_EURO_SUM_EXP_ASSESS_FLAGS_1 is
      select NVL(SUM(a.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess a,
             elc_comp e,
             currency_rates c
       where a.item = I_item
         and e.comp_id = a.comp_id
         and e.comp_currency = c.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1   = 'Y' and a.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and a.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and a.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and a.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and a.nom_flag_5 = L_oper))
         and a.active_date = i_active_date;

   cursor C_EURO_SUM_EXP_ASSESS_FLAGS_2 is
      select NVL(SUM(a.est_assess_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_hts_assess a,
             elc_comp e,
             currency_rates c,
             euro_exchange_rate euro
       where a.item = I_item
         and a.comp_id = e.comp_id
         and e.comp_currency = euro.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and a.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and a.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and a.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and a.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and a.nom_flag_5 = L_oper))
         and a.active_date = i_active_date;

   cursor C_GET_CURR_EA_FLAGS is
      select distinct e.comp_currency
        from gtt_fc_item_hts_assess a,
             elc_comp e
       where a.item = I_item
         and a.comp_id = e.comp_id
         and e.comp_currency != L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and a.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and a.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and a.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and a.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and a.nom_flag_5 = L_oper))
         and a.active_date = i_active_date;
   ---
   cursor C_SUM_EXP_ASSESS_FLAGS_PRIM is
      select NVL(SUM(it.est_assess_value), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e
       where it.item = I_item
         and it.comp_id = e.comp_id
         and e.comp_currency = L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and it.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and it.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and it.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and it.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and it.nom_flag_5 = L_oper))
         and it.active_date = i_active_date;

   -------------------------------------------
   -- Item Assessment Cursors
   -------------------------------------------
   cursor C_ITEM_ASSESS_INFO is
      select elc.calc_basis,
             itm.cvb_code,
             itm.comp_rate,
             elc.comp_currency,
             itm.per_count,
             itm.per_count_uom
        from elc_comp elc,
             gtt_fc_item_hts_assess itm
       where itm.item = I_item
         and itm.hts = I_hts
         and itm.import_country_id = I_import_country_id
         and itm.origin_country_id = I_origin_country_id
         and itm.effect_from = I_effect_from
         and itm.effect_to = I_effect_to
         and itm.comp_id = I_comp_id
         and itm.comp_id = elc.comp_id
         and itm.active_date = i_active_date;
   ---
   cursor C_SUM_ASSESS_DTLS is
      select NVL(SUM(it.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             cvb_detail cd,
             currency_rates c
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency != L_currency_code_prim
         and e.comp_currency = c.currency_code
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and it.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and it.active_date = i_active_date;

   ---
   cursor C_EURO_SUM_ASSESS_DTLS_1 is
      select NVL(SUM(it.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             cvb_detail cd,
             currency_rates c
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency = c.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and it.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and it.active_date = i_active_date;

   cursor C_EURO_SUM_ASSESS_DTLS_2 is
      select NVL(SUM(it.est_assess_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             cvb_detail cd,
             currency_rates c,
             euro_exchange_rate euro
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency = euro.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and it.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and it.active_date = i_active_date;

   cursor C_GET_CURR_ASS_DTLS is
      select distinct e.comp_currency
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             cvb_detail cd
       where it.item = I_item
         and it.comp_id = e.comp_id
         and it.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and e.comp_currency != L_currency_code_prim
         and it.active_date = i_active_date;
   ---
   cursor C_SUM_ASSESS_DTLS_PRIM is
      select NVL(SUM(it.est_assess_value), 0)
        from gtt_fc_item_hts_assess it,
             cvb_detail cd
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = cd.comp_id
         and L_comp_currency = L_currency_code_prim
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and it.active_date = i_active_date;
   ---
   cursor C_SUM_ASSESS_EXP_DTLS is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency != L_currency_code_prim
         and ieg.comp_currency = c.currency_code
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_EURO_SUM_ASSESS_EXP_DTLS_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select max(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_ASSESS_EXP_DTLS_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_GET_CURR_AE_DTLS is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency != L_currency_code_prim
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_ASSESS_EXP_DTLS_PRIM is
      select NVL(SUM(ieg.est_exp_value), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             cvb_detail cd
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency = L_currency_code_prim
         and ieg.comp_id = cd.comp_id
         and cd.cvb_code = L_cvb_code
         and cd.combo_oper = L_oper
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_ASSESS_FLAGS is
      select NVL(SUM(it.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             currency_rates c
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency != L_currency_code_prim
         and e.comp_currency = c.currency_code
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and it.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and it.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and it.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and it.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and it.nom_flag_5 = L_oper))
         and it.active_date = I_active_date;
   ---
   cursor C_EURO_SUM_ASSESS_FLAGS_1 is
      select NVL(SUM(it.est_assess_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             currency_rates c
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency = c.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = e.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and it.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and it.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and it.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and it.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and it.nom_flag_5 = L_oper))
         and it.active_date = I_active_date;

   cursor C_EURO_SUM_ASSESS_FLAGS_2 is
      select NVL(SUM(it.est_assess_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_hts_assess it,
             elc_comp e,
             currency_rates c,
             euro_exchange_rate euro
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency = euro.currency_code
         and e.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and it.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and it.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and it.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and it.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and it.nom_flag_5 = L_oper))
          and it.active_date = I_active_date;

   cursor C_GET_CURR_A_FLAGS is
      select distinct comp_currency
        from gtt_fc_item_hts_assess it,
             elc_comp e
       where it.item = I_item
         and it.hts = I_hts
         and it.import_country_id = I_import_country_id
         and it.origin_country_id = I_origin_country_id
         and it.effect_from = I_effect_from
         and it.effect_to = I_effect_to
         and it.comp_id = e.comp_id
         and e.comp_currency != L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and it.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and it.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and it.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and it.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and it.nom_flag_5 = L_oper))
         and it.active_date = I_active_date;
   ---
   cursor C_SUM_ASSESS_FLAGS_PRIM is
      select NVL(SUM(est_assess_value), 0)
        from gtt_fc_item_hts_assess
       where item = I_item
         and hts = I_hts
         and import_country_id = I_import_country_id
         and origin_country_id = I_origin_country_id
         and effect_from = I_effect_from
         and effect_to = I_effect_to
         and L_comp_currency = L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and nom_flag_5 = L_oper))
         and active_date = I_active_date;
   ---
   cursor C_SUM_ASSESS_EXP_FLAGS is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency != L_currency_code_prim
         and ieg.comp_currency = c.currency_code
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_EURO_SUM_ASSESS_EXP_FLAGS_1 is
      select NVL(SUM(ieg.est_exp_value * (L_currency_rate_prim/c.exchange_rate)), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency = c.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = ieg.comp_currency
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_EURO_SUM_ASSESS_EXP_FLAGS_2 is
      select NVL(SUM(ieg.est_exp_value/euro.exchange_rate/L_primary_curr_to_euro_rate), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih,
             currency_rates c,
             euro_exchange_rate euro
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency = euro.currency_code
         and ieg.comp_currency = L_euro_comp_currency
         and c.currency_code = 'EUR'
         and c.exchange_type = L_exchange_type
         and c.effective_date = (select MAX(cr.effective_date)
                                   from currency_rates cr
                                  where cr.currency_code = 'EUR'
                                    and cr.exchange_type = L_exchange_type
                                    and cr.effective_date <= L_vdate)
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_GET_CURR_AE_FLAGS is
      select distinct comp_currency
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C' and
                                         ih.origin_country_id = I_origin_country_id))
         and ieg.item_exp_seq = ih.item_exp_seq
         and ieg.comp_currency != L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and ieg.nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and ieg.nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and ieg.nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and ieg.nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and ieg.nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;
   ---
   cursor C_SUM_ASSESS_EXP_FLAGS_PRIM is
      select NVL(SUM(ieg.est_exp_value), 0)
        from gtt_fc_item_exp_detail ieg,
             item_exp_head ih
       where ieg.item = I_item
         and ih.supplier = L_supplier
         and ih.base_exp_ind = 'Y'
         and ieg.item = ih.item
         and ieg.supplier = ih.supplier
         and ieg.item_exp_type = ih.item_exp_type
         and ieg.item_exp_seq  = ih.item_exp_seq
         and ieg.comp_currency = L_currency_code_prim
         and ((L_nom_flag_1 = 'Y' and nom_flag_1 = L_oper)
             or (L_nom_flag_2 = 'Y' and nom_flag_2 = L_oper)
             or (L_nom_flag_3 = 'Y' and nom_flag_3 = L_oper)
             or (L_nom_flag_4 = 'Y' and nom_flag_4 = L_oper)
             or (L_nom_flag_5 = 'Y' and nom_flag_5 = L_oper))
         and ieg.active_date = I_active_date
         and ieg.location = I_location;

   cursor C_FETCH_CVB_DETAIL is
      -- If both the component being calculated and the detail component are assessments, then the HTS/countries/dates should match. 
       select NVL(it.est_assess_value,0) est_assess_value,
              e.comp_currency
         from cvb_detail cd,
              gtt_fc_item_hts_assess it,
              elc_comp e
        where it.item              = I_item
          and it.hts               = I_hts
          and it.import_country_id = I_import_country_id
          and it.origin_country_id = I_origin_country_id
          and it.effect_from       = I_effect_from
          and it.effect_to         = I_effect_to
          and it.comp_id           = e.comp_id
          and it.comp_id           = cd.comp_id
          and cd.cvb_code          = L_cvb_code
          and it.active_date       = I_active_date
      UNION ALL
      -- If the detail component is an expense, and the component being calculated is an assessment or has a different expense type (C/Z), use the base expense profile
      select NVL(it.est_exp_value, 0),
             e.comp_currency
        from gtt_fc_item_exp_detail it,
             item_exp_head ih,
             cvb_detail cd,
             elc_comp e
       where it.item          = I_item
         and ih.supplier      = L_supplier
         and ih.base_exp_ind  = 'Y'
         and (ih.item_exp_type = 'Z' or (ih.item_exp_type = 'C'
                                         and ih.origin_country_id = I_origin_country_id))
         and it.item          = ih.item
         and it.supplier      = ih.supplier
         and it.item_exp_type = ih.item_exp_type
         and it.item_exp_seq  = ih.item_exp_seq
         and it.comp_id       = cd.comp_id
         and it.comp_id       = e.comp_id
         and cd.cvb_code      = L_cvb_code
         and it.active_date   = I_active_date
         and it.location      = I_location;
       
   -------------------------------------------
   -- General Information Cursors
   -------------------------------------------
   cursor C_SUPPLIER is
      select i.supplier
        from item_supp_country i
       where i.item = I_item
         and i.origin_country_id = I_origin_country_id
         and (i.primary_supp_ind = 'Y'
              or not exists (select 'x'
                               from item_supp_country s
                              where s.origin_country_id = I_origin_country_id
                                and s.item = I_item
                                and s.primary_supp_ind = 'Y'));

   cursor C_GET_UNIT_COST is
      select unit_cost
        from item_supp_country
       where item = I_item
         and ((supplier = L_supplier
               and L_supplier is not NULL)
          or (primary_supp_ind = 'Y'
              and L_supplier is NULL))
         and ((origin_country_id = L_origin_country_id
               and L_origin_country_id is not NULL)
          or (primary_country_ind = 'Y'
              and L_origin_country_id is NULL));

   cursor C_GET_DIMENSION is
      select i.supp_pack_size,
             id.weight,
             id.weight_uom,
             id.length,
             id.height,
             id.width,
             id.lwh_uom,
             id.liquid_volume,
             id.liquid_volume_uom,
             i.ti,
             i.hi
       from  item_supp_country i,
             item_supp_country_dim id
       where i.item = id.item
         and i.supplier = id.supplier
         and i.origin_country_id = id.origin_country
         and id.dim_object = 'CA'
         and i.item       = I_item
         and ((i.supplier = L_supplier and
               L_supplier is NOT NULL) or
              (i.primary_supp_ind = 'Y' and
               L_supplier is NULL))
         and ((i.origin_country_id = L_origin_country_id and
               L_origin_country_id is NOT NULL) or
              (i.primary_country_ind = 'Y' and
               L_origin_country_id is NULL));

   cursor C_GET_MISC_VALUE is
      select value
        from item_supp_uom
       where item = I_item
         and supplier = L_supplier
         and uom = L_per_count_uom;

   cursor C_EURO_COMP_CURR_EXISTS is
      select 'Y'
        from euro_exchange_rate
       where currency_code = L_euro_comp_currency;

   cursor C_CHECK_OPER_EXISTS is
      select distinct combo_oper
        from cvb_head
       where cvb_code = L_cvb_code
         and combo_oper in (L_greater,L_smaller);
BEGIN
   O_est_value := 0;
   L_origin_country_id := I_origin_country_id;
   L_supplier := I_supplier;
   ---
   L_vdate := GET_VDATE;
   ---
   if SYSTEM_OPTIONS_SQL.CONSOLIDATION_IND(O_error_message,
                                           L_consolidation_ind) = FALSE then
      return FALSE;
   end if;
   ---
   if L_consolidation_ind = 'Y' then
      L_exchange_type := 'C';
   else
      L_exchange_type := 'O';
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                       L_currency_code_prim) = FALSE then
      return FALSE;
   end if;
   ---
   --Check if L_currency_code_prim is EMU member.
   ---
   if CURRENCY_SQL.CHECK_EMU_COUNTRIES(O_error_message,
                                       L_emu_participating_ind,
                                       L_currency_code_prim) = FALSE then
      return FALSE;
   end if;
   ---
   --Set the original primary currency code for final currency conversion.
   ---
   L_orig_currency_code_prim := L_currency_code_prim;
   ---
   --Set currency code to 'EUR' for EMU members.
   ---
   if L_emu_participating_ind = TRUE then
      L_currency_code_prim := 'EUR';
   end if;
   ---
   -- Get the primary currency information to be used to convert the estimated values
   -- in the cursors as they are summed together.
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_PRIM_CURR_INFO',
                     'CURRENCIES, CURRENCY_RATES',
                     NULL);
   open C_GET_PRIM_CURR_INFO;
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_PRIM_CURR_INFO',
                    'CURRENCIES, CURRENCY_RATES',
                    NULL);
   fetch C_GET_PRIM_CURR_INFO into L_currency_rate_prim,
                                   L_currency_cost_dec;
   ---
   if C_GET_PRIM_CURR_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_CURR_INFO_FOUND',
                                            L_currency_code_prim,
                                            NULL,
                                            NULL);
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_PRIM_CURR_INFO',
                       'CURRENCIES, CURRENCY_RATES',
                       NULL);
      close C_GET_PRIM_CURR_INFO;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_PRIM_CURR_INFO',
                    'CURRENCIES, CURRENCY_RATES',
                    NULL);
   close C_GET_PRIM_CURR_INFO;
   ---
   --Retrieve the 'EUR' exchange rate if the primary currency is not one of the EMU
   --countries.
   ---
   if L_currency_code_prim != 'EUR' then
      if CURRENCY_SQL.GET_RATE(O_error_message,
                               L_primary_curr_to_euro_rate,
                               'EUR',
                               L_consolidation_ind,
                               L_vdate) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   if I_calc_type = 'IE' then
      if I_supplier is NULL then
         if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP(O_error_message,
                                                  L_exists,
                                                  L_supplier,
                                                  I_item) = FALSE then

            return FALSE;
         end if;
      end if;
      ---
      if I_item_exp_type = 'Z' then
         if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_COUNTRY(O_error_message,
                                                     L_origin_country_id,
                                                     I_item,
                                                     L_supplier) = FALSE then
            return FALSE;
         end if;
      end if;
   elsif I_calc_type = 'IA' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_SUPPLIER',
                       'ITEM_SUPP_COUNTRY',
                       'Item: ' ||I_item ||
                       ' Origin Country: ' || I_origin_country_id);
      open C_SUPPLIER;
      SQL_LIB.SET_MARK('FETCH',
                       'C_SUPPLIER',
                       'ITEM_SUPP_COUNTRY',
                       'Item: ' ||I_item ||
                       ' Origin Country: ' || I_origin_country_id);
      fetch C_SUPPLIER into L_supplier;
      ---
      if C_SUPPLIER%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE',
                          'C_SUPPLIER',
                          'ITEM_SUPP_COUNTRY',
                          'Item: ' ||I_item ||
                          ' Origin Country: ' || I_origin_country_id);
         close C_SUPPLIER;
         O_error_message := SQL_LIB.CREATE_MSG('NO_ITEM_ORIGIN_REC',
                                               I_origin_country_id,
                                               NULL,
                                               NULL);
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_SUPPLIER',
                       'ITEM_SUPP_COUNTRY',
                       'Item: ' ||I_item ||
                       ' Origin Country: ' || I_origin_country_id);
      close C_SUPPLIER;
   end if;
   ---
   if I_calc_type = 'IE' then
      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEM_EXP_INFO',
                       'ITEM_EXP_DETAIL',
                       NULL);
      open C_ITEM_EXP_INFO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEM_EXP_INFO',
                       'ITEM_EXP_DETAIL',
                       NULL);
      fetch C_ITEM_EXP_INFO into L_calc_basis,
                                 L_cvb_code,
                                 L_comp_rate,
                                 L_comp_currency,
                                 L_per_count,
                                 L_per_count_uom;
      if C_ITEM_EXP_INFO%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMP',
                                               NULL,
                                               NULL,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ITEM_EXP_INFO',
                          'ITEM_EXP_DETAIL',
                          NULL);
         close C_ITEM_EXP_INFO;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_EXP_INFO',
                       'ITEM_EXP_DETAIL',
                       NULL);
      close C_ITEM_EXP_INFO;

   elsif I_calc_type = 'IA' then
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_ITEM_ASSESS_INFO',
                       'ITEM_HTS_ASSESS',
                       NULL);
      open C_ITEM_ASSESS_INFO;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ITEM_ASSESS_INFO',
                       'ITEM_HTS_ASSESS',
                       NULL);
      fetch C_ITEM_ASSESS_INFO into L_calc_basis,
                                    L_cvb_code,
                                    L_comp_rate,
                                    L_comp_currency,
                                    L_per_count,
                                    L_per_count_uom;
      if C_ITEM_ASSESS_INFO%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_COMP',
                                               NULL,
                                               NULL,
                                               NULL);
         SQL_LIB.SET_MARK('CLOSE',
                          'C_ITEM_ASSESS_INFO',
                          'ITEM_HTS_ASSESS',
                          NULL);
         close C_ITEM_ASSESS_INFO;
         return FALSE;
      end if;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ITEM_ASSESS_INFO',
                       'ITEM_HTS_ASSESS',
                       NULL);
      close C_ITEM_ASSESS_INFO;
      ---
   end if;
   ---
    ---
      -- If the cvb_code of a component is not NULL then we need to sum up the
      -- values of the components of the cvb.
      ---
      if L_cvb_code is not NULL then
         if I_dtl_flag = 'F' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_CVB_FLAGS',
                             'CVB_HEAD',
                             'CVB Code: '||L_cvb_code);
            open C_GET_CVB_FLAGS;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_CVB_FLAGS',
                             'CVB_HEAD',
                             'CVB Code: '||L_cvb_code);
            fetch C_GET_CVB_FLAGS into L_nom_flag_1,
                                       L_nom_flag_2,
                                       L_nom_flag_3,
                                       L_nom_flag_4,
                                       L_nom_flag_5;
            if C_GET_CVB_FLAGS%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_CVB',
                                                     NULL,
                                                     NULL,
                                                     NULL);
               SQL_LIB.SET_MARK('OPEN',
                                'C_GET_CVB_FLAGS',
                                'CVB_HEAD',
                                'CVB Code: '||L_cvb_code);
               close C_GET_CVB_FLAGS;
               return FALSE;
            end if;
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_CVB_FLAGS',
                             'CVB_HEAD',
                             'CVB Code: '||L_cvb_code);
            close C_GET_CVB_FLAGS;
            ---
         end if;
         ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_OPER_EXISTS','CVB_HEAD','CVB code: '||L_cvb_code);
      open C_CHECK_OPER_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_OPER_EXISTS','CVB_HEAD','CVB code: '||L_cvb_code);
      fetch C_CHECK_OPER_EXISTS into L_oper_flag;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_OPER_EXISTS','CVB_HEAD','CVB code: '||L_cvb_code);
      close C_CHECK_OPER_EXISTS;

      if L_oper_flag is null then -- this will handle for operations of + and - 
         -- Components of cvb's all have a combination operation of either '+' or '-'.
         -- Here we will sum the components with a combination operator of '+'.  Then
         -- we will subtract all of the components with a combination operator of '-'
         -- from the value gotten here.
         ---
         L_counter := 1;
         ---
         LOOP
            EXIT when L_counter = 3;
            ---
            if L_counter = 1 then
               L_oper := '+';
            else
               L_oper := '-';
            end if;
            ---
            if I_dtl_flag = 'D' then
               ---
               -- Sum '+' expense detail components.
               ---
               if I_calc_type = 'IE' then

                  if L_currency_code_prim = 'EUR' then
                     ---
                     -- This cursor sums the expense components of the CVB that
                     -- is attached to the component passed into this function,
                     -- that have a component currency that is different from the primary
                     -- currency.  As it sums the estimated expense values, it
                     -- converts the values into the primary currency.
                     -- If the primary currency is an EMU member, this cursor will then
                     -- sum all expenses for all countries, including the member country.
                     -- 'EUR' values will not be summed and will be considered to be the
                     -- primary currency.
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_DTLS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_EXP_DTLS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_DTLS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_EXP_DTLS into L_exp_dtl_amt;

                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_DTLS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);                     close C_SUM_EXP_DTLS;
                     ---
                  else  --L_currency_code_prim is not 'EUR'--
                     ---
                     -- This cursor sums the expense components of the CVB that
                     -- is attached to the component passed into this function,
                     -- that have a component currency that is different from the primary
                     -- currency.  As it sums the estimated expense values, it
                     -- converts the values into the primary currency.
                     -- This cursor sums both EMU countries and non-EMU countries.
                     ---
                     L_exp_dtl_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_DTLS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_DTLS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_EXP_DTLS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_DTLS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_EXP_DTLS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_DTLS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_EXP_DTLS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_DTLS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_EXP_DTLS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_DTLS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_EXP_DTLS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_DTLS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_EXP_DTLS_1;
                        end if;
                        ---
                        L_exp_dtl_amt := L_exp_dtl_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  -- This cursor sums the expense components of the CVB
                  -- that have a component currency that is the same as
                  -- the primary currency.  No conversion is needed.
                  -- If the primary currency is an EMU member, 'EUR' will be considered
                  -- the primary currency.
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_EXP_DTLS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  open C_SUM_EXP_DTLS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_EXP_DTLS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  fetch C_SUM_EXP_DTLS_PRIM into L_exp_dtl_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_EXP_DTLS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  close C_SUM_EXP_DTLS_PRIM;
                  ---
                  -- The following cursors are being called to retrieve expense values of the opposite
                  -- item expense type to be used in the calculation of the current component.  Zone level
                  -- expenses may be based on country level expenses and vice versa...
                  ---
                  if I_item_exp_type = 'Z' and I_comp_id <> 'TEXPZ' then
                     if L_currency_code_prim = 'EUR' then
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_SUM_EXP_DTLS_CTRY',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        open C_SUM_EXP_DTLS_CTRY;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_SUM_EXP_DTLS_CTRY',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        fetch C_SUM_EXP_DTLS_CTRY into L_exp_dtl_amt_ctry;

                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_SUM_EXP_DTLS_CTRY',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        close C_SUM_EXP_DTLS_CTRY;
                        ---
                     else  --L_currency_code_prim is not 'EUR'--
                        ---
                        L_exp_dtl_amt_ctry := 0;
                        ---
                        for Curr_rec in C_GET_CURR_DTLS_CTRY loop
                           L_euro_comp_currency := Curr_rec.comp_currency;
                           ---
                           L_euro_comp_currency_exists := 'N';
                           ---
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           open C_EURO_COMP_CURR_EXISTS;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           close C_EURO_COMP_CURR_EXISTS;
                           ---
                           L_temp := 0;
                           ---
                           if L_euro_comp_currency_exists = 'Y' then
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_DTLS_CTRY_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_DTLS_CTRY_2;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_DTLS_CTRY_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_DTLS_CTRY_2 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_DTLS_CTRY_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_DTLS_CTRY_2;
                           else
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_DTLS_CTRY_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_DTLS_CTRY_1;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_DTLS_CTRY_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_DTLS_CTRY_1 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_DTLS_CTRY_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_DTLS_CTRY_1;
                           end if;
                           ---
                           L_exp_dtl_amt_ctry := L_exp_dtl_amt_ctry + L_temp;
                        end loop;
                     end if;
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_DTLS_CTRY_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_EXP_DTLS_CTRY_PRIM;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_DTLS_CTRY_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_EXP_DTLS_CTRY_PRIM into L_exp_dtl_amt_ctry_prim;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_DTLS_CTRY_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_EXP_DTLS_CTRY_PRIM;
                     ---
                  elsif I_item_exp_type = 'C' and I_comp_id <> 'TEXPC' then
                     if L_currency_code_prim = 'EUR' then
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_SUM_EXP_DTLS_ZONE',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        open C_SUM_EXP_DTLS_ZONE;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_SUM_EXP_DTLS_ZONE',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        fetch C_SUM_EXP_DTLS_ZONE into L_exp_dtl_amt_zone;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_SUM_EXP_DTLS_ZONE',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        close C_SUM_EXP_DTLS_ZONE;
                        ---
                     else  --L_currency_code_prim is not 'EUR'--
                        ---
                        L_exp_dtl_amt_zone := 0;
                        ---
                        for Curr_rec in C_GET_CURR_DTLS_ZONE loop
                           L_euro_comp_currency := Curr_rec.comp_currency;
                           ---
                           L_euro_comp_currency_exists := 'N';
                           ---
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           open C_EURO_COMP_CURR_EXISTS;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           close C_EURO_COMP_CURR_EXISTS;
                           ---
                           L_temp := 0;
                           ---
                           if L_euro_comp_currency_exists = 'Y' then
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_DTLS_ZONE_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_DTLS_ZONE_2;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_DTLS_ZONE_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_DTLS_ZONE_2 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_DTLS_ZONE_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_DTLS_ZONE_2;
                           else
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_DTLS_ZONE_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_DTLS_ZONE_1;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_DTLS_ZONE_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_DTLS_ZONE_1 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_DTLS_ZONE_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_DTLS_ZONE_1;
                           end if;
                           ---
                           L_exp_dtl_amt_zone := L_exp_dtl_amt_zone + L_temp;
                        end loop;
                     end if;
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_DTLS_ZONE_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_EXP_DTLS_ZONE_PRIM;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_DTLS_ZONE_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_EXP_DTLS_ZONE_PRIM into L_exp_dtl_amt_zone_prim;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_DTLS_ZONE_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_EXP_DTLS_ZONE_PRIM;
                  end if;
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     -- This cursor sums the assessment components of the CVB
                     -- that have a component currency different from the
                     -- primary currency.  As it sums the estimated assessment
                     -- values, it converts the values into the primary currency.
                     -- If the primary currency is an EMU member, this cursor will then
                     -- sum all expenses for all countries, including the member country.
                     -- 'EUR' values will not be summed and will be considered to be the
                     -- primary currency.
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_ASSESS_DTLS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     open C_SUM_EXP_ASSESS_DTLS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_ASSESS_DTLS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     fetch C_SUM_EXP_ASSESS_DTLS into L_exp_assess_dtl_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_ASSESS_DTLS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     close C_SUM_EXP_ASSESS_DTLS;
                     ---
                  else  --L_currency_code_prim is not 'EUR'--
                     ---
                     -- This cursor sums the assessment components of the CVB
                     -- that have a component currency different from the
                     -- primary currency.  As it sums the estimated assessment
                     -- values, it converts the values into the primary currency.
                     -- This cursor sums both EMU countries and non-EMU countries.
                     ---
                     L_exp_assess_dtl_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_EA_DTLS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_ASSESS_DTLS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_EXP_ASSESS_DTLS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_ASSESS_DTLS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_EXP_ASSESS_DTLS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_ASSESS_DTLS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_EXP_ASSESS_DTLS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_ASSESS_DTLS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_EXP_ASSESS_DTLS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_ASSESS_DTLS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_EXP_ASSESS_DTLS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_ASSESS_DTLS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_EXP_ASSESS_DTLS_1;
                        end if;
                        ---
                        L_exp_assess_dtl_amt := L_exp_assess_dtl_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  -- This cursor sums the assessment components of the CVB
                  -- that have a component currency that is the same as
                  -- the primary currency.  No conversion is needed.
                  -- If the primary currency is an EMU member, 'EUR' will be considered
                  -- the primary currency.
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_EXP_ASSESS_DTLS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  open C_SUM_EXP_ASSESS_DTLS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_EXP_ASSESS_DTLS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  fetch C_SUM_EXP_ASSESS_DTLS_PRIM into L_exp_assess_dtl_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_EXP_ASSESS_DTLS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  close C_SUM_EXP_ASSESS_DTLS_PRIM;

               elsif I_calc_type = 'IA' then
                  ---
                  -- Sum '+' assessment detail components.
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     -- This cursor sums the assessment components of the CVB that
                     -- is attached to the component passed into this function,
                     -- that have a component currency that is different from the primary
                     -- currency.  As it sums the estimated expense values, it
                     -- converts the values into the primary currency.
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_ASSESS_DTLS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     open C_SUM_ASSESS_DTLS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_ASSESS_DTLS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     fetch C_SUM_ASSESS_DTLS into L_assess_dtl_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_ASSESS_DTLS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     close C_SUM_ASSESS_DTLS;
                     ---
                  else   --L_currency_code_prim != 'EUR'--
                     ---
                     -- This cursor sums the assessment components of the CVB that
                     -- is attached to the component passed into this function,
                     -- that have a component currency that is different from the primary
                     -- currency.  As it sums the estimated expense values, it
                     -- converts the values into the primary currency.
                     ---
                     L_assess_dtl_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_ASS_DTLS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_DTLS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_EXP_ASSESS_DTLS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_DTLS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_EXP_ASSESS_DTLS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_DTLS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_EXP_ASSESS_DTLS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_DTLS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_ASSESS_DTLS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_DTLS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_DTLS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_DTLS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_ASSESS_DTLS_1;
                        end if;
                        ---
                        L_assess_dtl_amt := L_assess_dtl_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  -- This cursor sums the expense components of the CVB
                  -- that have a component currency that is the same as
                  -- the primary currency.  No conversion is needed.
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_ASSESS_DTLS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  open C_SUM_ASSESS_DTLS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_ASSESS_DTLS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  fetch C_SUM_ASSESS_DTLS_PRIM into L_assess_dtl_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_ASSESS_DTLS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  close C_SUM_ASSESS_DTLS_PRIM;
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     -- This cursor sums the expense components of the CVB
                     -- that have a component currency different from the
                     -- primary currency.  As it sums the estimated assessment
                     -- values, it converts the values into the primary currency.
                     -- Since assessments are attached at the Item header level,
                     -- we do not have all of the specific information such as
                     -- a supplier, a lading port, a discharge port, or a zone.
                     -- Therefore, in this case, we sum the expenses that exist for
                     -- the primary supplier, and the 'Base' set of expenses.
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_ASSESS_EXP_DTLS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_ASSESS_EXP_DTLS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_ASSESS_EXP_DTLS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_ASSESS_EXP_DTLS into L_assess_exp_dtl_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_ASSESS_EXP_DTLS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_ASSESS_EXP_DTLS;
                     ---
                  else  --L_currency_code_prim != 'EUR'--
                     ---
                     L_assess_exp_dtl_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_AE_DTLS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_EXP_DTLS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_ASSESS_EXP_DTLS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_EXP_DTLS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_EXP_DTLS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_EXP_DTLS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_ASSESS_EXP_DTLS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_EXP_DTLS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_ASSESS_EXP_DTLS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_EXP_DTLS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_EXP_DTLS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_EXP_DTLS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_ASSESS_EXP_DTLS_1;
                        end if;
                        ---
                        L_assess_exp_dtl_amt := L_assess_exp_dtl_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  -- This cursor sums the assessment components of the CVB
                  -- that have a component currency that is the same as
                  -- the primary currency.  No conversion is needed.
                  -- Since assessments are attached at the Item header level,
                  -- we do not have all of the specific information such as
                  -- a supplier, a lading port, a discharge port, or a zone.
                  -- Therefore, in this case, we sum the expenses that exist for
                  -- the primary supplier, and the 'Base' set of expenses.
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_ASSESS_EXP_DTLS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  open C_SUM_ASSESS_EXP_DTLS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_ASSESS_EXP_DTLS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  fetch C_SUM_ASSESS_EXP_DTLS_PRIM into L_assess_exp_dtl_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_ASSESS_EXP_DTLS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  close C_SUM_ASSESS_EXP_DTLS_PRIM;
                  ---
               end if;

            elsif I_dtl_flag = 'F' then
               if I_calc_type = 'IE' then
                  ---
                  -- If a component has a cvb, and that cvb has a flag set to 'Y',
                  -- then these cursors will sum up all components that have the
                  -- same flags set to '+'.
                  ---
                  -- Sum '+' expense flag components.
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     -- This cursor will some all expense components with the appropriate
                     -- flags set to '+' where the component's currency is not the same
                     -- as the primary currency.
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_FLAGS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_EXP_FLAGS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_FLAGS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_EXP_FLAGS into L_exp_flag_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_FLAGS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_EXP_FLAGS;
                     ---
                  else --L_currency_code_prim is not 'EUR'--
                     ---
                     L_exp_flag_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_FLAGS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_FLAGS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_EXP_FLAGS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_FLAGS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_EXP_FLAGS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_FLAGS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_EXP_FLAGS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_FLAGS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_EXP_FLAGS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_FLAGS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_EXP_FLAGS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_FLAGS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_EXP_FLAGS_1;
                        end if;
                        ---
                        L_exp_flag_amt := L_exp_flag_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  -- This cursor will some all expense components with the appropriate
                  -- flags set to '+' where the component's currency is the same
                  -- as the primary currency.
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_EXP_FLAGS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  open C_SUM_EXP_FLAGS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_EXP_FLAGS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  fetch C_SUM_EXP_FLAGS_PRIM into L_exp_flag_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_EXP_FLAGS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  close C_SUM_EXP_FLAGS_PRIM;
                  ---
                  -- The following cursors are being called to retrieve expense values of the opposite
                  -- item expense type to be used in the calculation of the current component.  Zone level
                  -- expenses may be based on country level expenses and vice versa...
                  ---
                  if I_item_exp_type = 'Z' and I_comp_id <> 'TEXPZ' then
                     if L_currency_code_prim = 'EUR' then
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_SUM_EXP_FLAGS_CTRY',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        open C_SUM_EXP_FLAGS_CTRY;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_SUM_EXP_FLAGS_CTRY',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        fetch C_SUM_EXP_FLAGS_CTRY into L_exp_flag_amt_ctry;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_SUM_EXP_FLAGS_CTRY',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        close C_SUM_EXP_FLAGS_CTRY;
                        ---
                     else --L_currency_code_prim is not 'EUR'--
                        ---
                        L_exp_flag_amt_ctry := 0;
                        ---
                        for Curr_rec in C_GET_CURR_FLAGS_CTRY loop
                           L_euro_comp_currency := Curr_rec.comp_currency;
                           ---
                           L_euro_comp_currency_exists := 'N';
                           ---
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           open C_EURO_COMP_CURR_EXISTS;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           close C_EURO_COMP_CURR_EXISTS;
                           ---
                           L_temp := 0;
                           ---
                           if L_euro_comp_currency_exists = 'Y' then
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_FLAGS_CTRY_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_FLAGS_CTRY_2;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_FLAGS_CTRY_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_FLAGS_CTRY_2 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_FLAGS_CTRY_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_FLAGS_CTRY_2;
                           else
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_FLAGS_CTRY_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_FLAGS_CTRY_1;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_FLAGS_CTRY_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_FLAGS_CTRY_1 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_FLAGS_CTRY_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_FLAGS_CTRY_1;
                           end if;
                           ---
                           L_exp_flag_amt_ctry := L_exp_flag_amt_ctry + L_temp;
                        end loop;
                     end if;
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_FLAGS_CTRY_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_EXP_FLAGS_CTRY_PRIM;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_FLAGS_CTRY_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_EXP_FLAGS_CTRY_PRIM into L_exp_flag_amt_ctry_prim;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_FLAGS_CTRY_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_EXP_FLAGS_CTRY_PRIM;
                  elsif I_item_exp_type = 'C' and I_comp_id <> 'TEXPC' then
                     if L_currency_code_prim = 'EUR' then
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_SUM_EXP_FLAGS_ZONE',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        open C_SUM_EXP_FLAGS_ZONE;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_SUM_EXP_FLAGS_ZONE',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        fetch C_SUM_EXP_FLAGS_ZONE into L_exp_flag_amt_zone;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_SUM_EXP_FLAGS_ZONE',
                                         'ITEM_EXP_DETAIL',
                                         NULL);
                        close C_SUM_EXP_FLAGS_ZONE;
                        ---
                     else --L_currency_code_prim is not 'EUR'--
                        ---
                        L_exp_flag_amt_zone := 0;
                        ---
                        for Curr_rec in C_GET_CURR_FLAGS_ZONE loop
                           L_euro_comp_currency := Curr_rec.comp_currency;
                           ---
                           L_euro_comp_currency_exists := 'N';
                           ---
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           open C_EURO_COMP_CURR_EXISTS;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_COMP_CURR_EXISTS',
                                            'EURO_EXCHANGE_RATE',
                                            NULL);
                           close C_EURO_COMP_CURR_EXISTS;
                           ---
                           L_temp := 0;
                           ---
                           if L_euro_comp_currency_exists = 'Y' then
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_FLAGS_ZONE_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_FLAGS_ZONE_2;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_FLAGS_ZONE_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_FLAGS_ZONE_2 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_FLAGS_ZONE_2',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_FLAGS_ZONE_2;
                           else
                              SQL_LIB.SET_MARK('OPEN',
                                               'C_EURO_SUM_EXP_FLAGS_ZONE_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              open C_EURO_SUM_EXP_FLAGS_ZONE_1;
                              SQL_LIB.SET_MARK('FETCH',
                                               'C_EURO_SUM_EXP_FLAGS_ZONE_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              fetch C_EURO_SUM_EXP_FLAGS_ZONE_1 into L_temp;
                              SQL_LIB.SET_MARK('CLOSE',
                                               'C_EURO_SUM_EXP_FLAGS_ZONE_1',
                                               'ITEM_EXP_DETAIL',
                                               NULL);
                              close C_EURO_SUM_EXP_FLAGS_ZONE_1;
                           end if;
                           ---
                           L_exp_flag_amt_zone := L_exp_flag_amt_zone + L_temp;
                        end loop;
                     end if;
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_FLAGS_ZONE_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_EXP_FLAGS_ZONE_PRIM;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_FLAGS_ZONE_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_EXP_FLAGS_ZONE_PRIM into L_exp_flag_amt_zone_prim;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_FLAGS_ZONE_PRIM',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_EXP_FLAGS_ZONE_PRIM;
                  end if;
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     -- This cursor will some all assessment components with the appropriate
                     -- flags set to '+' where the component's currency is not the same
                     -- as the primary currency.
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_EXP_ASSESS_FLAGS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     open C_SUM_EXP_ASSESS_FLAGS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_EXP_ASSESS_FLAGS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     fetch C_SUM_EXP_ASSESS_FLAGS into L_exp_assess_flag_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_EXP_ASSESS_FLAGS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     close C_SUM_EXP_ASSESS_FLAGS;
                     ---
                  else --L_currency_code_prim is not 'EUR'--
                     ---
                     L_exp_assess_flag_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_EA_FLAGS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_ASSESS_FLAGS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_EXP_ASSESS_FLAGS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_ASSESS_FLAGS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_EXP_ASSESS_FLAGS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_ASSESS_FLAGS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_EXP_ASSESS_FLAGS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_EXP_ASSESS_FLAGS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_EXP_ASSESS_FLAGS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_EXP_ASSESS_FLAGS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_EXP_ASSESS_FLAGS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_EXP_ASSESS_FLAGS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_EXP_ASSESS_FLAGS_1;
                        end if;
                        ---
                        L_exp_assess_flag_amt := L_exp_assess_flag_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  -- This cursor will some all expense components with the appropriate
                  -- flags set to '+' where the component's currency is the same
                  -- as the primary currency.
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_EXP_ASSESS_FLAGS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  open C_SUM_EXP_ASSESS_FLAGS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_EXP_ASSESS_FLAGS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  fetch C_SUM_EXP_ASSESS_FLAGS_PRIM into L_exp_assess_flag_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_EXP_ASSESS_FLAGS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  close C_SUM_EXP_ASSESS_FLAGS_PRIM;
---
-- The comments above (where I_dtl_flag = 'F' and I_calc_type = 'IE') apply as well
-- to the following cursors (where I_calc_type = 'IA').
---
               elsif I_calc_type = 'IA' then
                  ---
                  -- Sum '+' assessment flag components.
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_ASSESS_FLAGS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     open C_SUM_ASSESS_FLAGS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_ASSESS_FLAGS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     fetch C_SUM_ASSESS_FLAGS into L_assess_flag_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_ASSESS_FLAGS',
                                      'ITEM_HTS_ASSESS',
                                      NULL);
                     close C_SUM_ASSESS_FLAGS;
                     ---
                  else   --L_currency_code_prim is not 'EUR'--
                     ---
                     L_assess_flag_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_A_FLAGS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_FLAGS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_ASSESS_FLAGS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_FLAGS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_FLAGS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_FLAGS_2',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_ASSESS_FLAGS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_FLAGS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           open C_EURO_SUM_ASSESS_FLAGS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_FLAGS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_FLAGS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_FLAGS_1',
                                            'ITEM_HTS_ASSESS',
                                            NULL);
                           close C_EURO_SUM_ASSESS_FLAGS_1;
                        end if;
                        ---
                        L_assess_flag_amt := L_assess_flag_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_ASSESS_FLAGS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  open C_SUM_ASSESS_FLAGS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_ASSESS_FLAGS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  fetch C_SUM_ASSESS_FLAGS_PRIM into L_assess_flag_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_ASSESS_FLAGS_PRIM',
                                   'ITEM_HTS_ASSESS',
                                   NULL);
                  close C_SUM_ASSESS_FLAGS_PRIM;
                  ---
                  if L_currency_code_prim = 'EUR' then
                     ---
                     SQL_LIB.SET_MARK('OPEN',
                                      'C_SUM_ASSESS_EXP_FLAGS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     open C_SUM_ASSESS_EXP_FLAGS;
                     SQL_LIB.SET_MARK('FETCH',
                                      'C_SUM_ASSESS_EXP_FLAGS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     fetch C_SUM_ASSESS_EXP_FLAGS into L_assess_exp_flag_amt;
                     SQL_LIB.SET_MARK('CLOSE',
                                      'C_SUM_ASSESS_EXP_FLAGS',
                                      'ITEM_EXP_DETAIL',
                                      NULL);
                     close C_SUM_ASSESS_EXP_FLAGS;
                     ---
                  else   --L_currency_code_prim is not 'EUR'--
                     ---
                     L_assess_exp_flag_amt := 0;
                     ---
                     for Curr_rec in C_GET_CURR_AE_FLAGS loop
                        L_euro_comp_currency := Curr_rec.comp_currency;
                        ---
                        L_euro_comp_currency_exists := 'N';
                        ---
                        SQL_LIB.SET_MARK('OPEN',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        open C_EURO_COMP_CURR_EXISTS;
                        SQL_LIB.SET_MARK('FETCH',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        fetch C_EURO_COMP_CURR_EXISTS into L_euro_comp_currency_exists;
                        SQL_LIB.SET_MARK('CLOSE',
                                         'C_EURO_COMP_CURR_EXISTS',
                                         'EURO_EXCHANGE_RATE',
                                         NULL);
                        close C_EURO_COMP_CURR_EXISTS;
                        ---
                        L_temp := 0;
                        ---
                        if L_euro_comp_currency_exists = 'Y' then
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_EXP_FLAGS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_ASSESS_EXP_FLAGS_2;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_EXP_FLAGS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_EXP_FLAGS_2 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_EXP_FLAGS_2',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_ASSESS_EXP_FLAGS_2;
                        else
                           SQL_LIB.SET_MARK('OPEN',
                                            'C_EURO_SUM_ASSESS_EXP_FLAGS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           open C_EURO_SUM_ASSESS_EXP_FLAGS_1;
                           SQL_LIB.SET_MARK('FETCH',
                                            'C_EURO_SUM_ASSESS_EXP_FLAGS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           fetch C_EURO_SUM_ASSESS_EXP_FLAGS_1 into L_temp;
                           SQL_LIB.SET_MARK('CLOSE',
                                            'C_EURO_SUM_ASSESS_EXP_FLAGS_1',
                                            'ITEM_EXP_DETAIL',
                                            NULL);
                           close C_EURO_SUM_ASSESS_EXP_FLAGS_1;
                        end if;
                        ---
                        L_assess_exp_flag_amt := L_assess_exp_flag_amt + L_temp;
                     end loop;
                  end if;
                  ---
                  SQL_LIB.SET_MARK('OPEN',
                                   'C_SUM_ASSESS_EXP_FLAGS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  open C_SUM_ASSESS_EXP_FLAGS_PRIM;
                  SQL_LIB.SET_MARK('FETCH',
                                   'C_SUM_ASSESS_EXP_FLAGS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  fetch C_SUM_ASSESS_EXP_FLAGS_PRIM into L_assess_exp_flag_amt_prim;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'C_SUM_ASSESS_EXP_FLAGS_PRIM',
                                   'ITEM_EXP_DETAIL',
                                   NULL);
                  close C_SUM_ASSESS_EXP_FLAGS_PRIM;
                  ---
               end if;
               ---
            end if;
            ---
            -- Add components to the total.
            L_amount_temp := (  L_exp_dtl_amt          + L_exp_dtl_amt_prim
                              + L_exp_dtl_amt_zone     + L_exp_dtl_amt_zone_prim
                              + L_exp_dtl_amt_ctry     + L_exp_dtl_amt_ctry_prim
                              + L_exp_assess_dtl_amt   + L_exp_assess_dtl_amt_prim
                              + L_exp_flag_amt         + L_exp_flag_amt_prim
                              + L_exp_flag_amt_zone    + L_exp_flag_amt_zone_prim
                              + L_exp_flag_amt_ctry    + L_exp_flag_amt_ctry_prim
                              + L_exp_assess_flag_amt  + L_exp_assess_flag_amt_prim
                              + L_assess_dtl_amt       + L_assess_dtl_amt_prim
                              + L_assess_exp_dtl_amt   + L_assess_exp_dtl_amt_prim
                              + L_assess_flag_amt      + L_assess_flag_amt_prim
                              + L_assess_exp_flag_amt  + L_assess_exp_flag_amt_prim);
            ---
            if L_counter = 1 then
               L_amount := L_amount + L_amount_temp;
            else
               L_amount := L_amount - L_amount_temp;
            end if;
            ---
            L_counter := L_counter + 1;
         END LOOP;
         ---
         if L_emu_participating_ind = TRUE then
            ---
            --Convert L_amount from 'EUR' to the primary currency.
            ---
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_amount,
                                    'EUR',
                                    L_orig_currency_code_prim,
                                    L_amount,
                                    'C',
                                    NULL,
                                    NULL,
                                    NULL,
                                    NULL) = FALSE then
               return FALSE;
            end if;
         end if;
   else -- if L_oper_flag is '>' or '<'
      if I_dtl_flag = 'D' then  
         if I_calc_type = 'IA' then
            
            --- fetch the cvb details records for oper >
            SQL_LIB.SET_MARK('FETCH','C_FETCH_CVB_DETAIL','CVB_DETAIL',NULL);
       L_first_cycle := 'Y';
            FOR C_rec in C_FETCH_CVB_DETAIL LOOP
               L_assess_value     := C_rec.est_assess_value;
               L_curr_code        := C_rec.comp_currency;
                  
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_assess_value,
                                       L_curr_code,
                                       NULL,
                                       L_amount_temp,
                                       'C',
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       'N') = FALSE then
                  return FALSE;
               end if;
          
                  ---check for the greater/smaller component
          if L_oper_flag = L_greater then
                  if L_amount_temp > L_amount then
                     L_amount := L_amount_temp;
                  end if;
          else
             if L_first_cycle = 'Y' then 
                     L_amount      := L_amount_temp;
                     L_first_cycle := 'N';
                             
                  elsif L_amount_temp < L_amount then
                      L_amount := L_amount_temp;
                  end if;
                   
               end if;
               END LOOP;    -- end of cvb detail loop 
            end if; --for calc_type 'PA'
         end if; -- for dtl flag 'D'               
      end if; -- end of oper flag (+,-,>)



      end if;  -- L_cvb_code is not NULL

   if L_calc_basis = 'V' then    -- The expense is a 'Value' based expense.
      if L_cvb_code is NULL then
         ---
         -- When the component has a 'Calculation Basis' of 'Value', but has no cvb,
         -- the expenses or assessments are based on the Supplier's unit cost or the
         -- Order's unit cost.
         ---
         if I_dtl_flag = 'D' then
            L_amount := i_new_cost;
            ---
            -- Convert L_amount from supplier currency to primary currency.
            ---
            if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                L_supplier,
                                                'V',
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL,
                                                L_amount,
                                                L_amount,
                                                'C',
                                                NULL,
                                                NULL) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; -- L_cvb_code is NULL.   

      -- Calculate the Estimate Expense Value.  When the Calculation Basis is Value, the
      -- expense or assessment is simply a percentage of an amount.
      O_est_value := L_amount * (L_comp_rate/100);
      -- Convert O_est_value from primary currency to the Component's currency.
      if CURRENCY_SQL.CONVERT(O_error_message,
                              O_est_value,
                              NULL,
                              L_comp_currency,
                              O_est_value,
                              'C',
                              NULL,
                              NULL,
                              NULL,
                              NULL) = FALSE then
         return FALSE;
      end if;
   elsif L_calc_basis = 'S' then  -- The expense is a 'Specific' expense.

      if I_dtl_flag = 'D' then
         ---
         -- Get all of the Dimension information.
         ---
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_DIMENSION',
                          'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                          NULL);
         open C_GET_DIMENSION;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_DIMENSION',
                          'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                          NULL);
         fetch C_GET_DIMENSION into L_supp_pack_size,
                                    L_ship_carton_wt,
                                    L_weight_uom,
                                    L_ship_carton_len,
                                    L_ship_carton_hgt,
                                    L_ship_carton_wid,
                                    L_dimension_uom,
                                    L_liquid_volume,
                                    L_liquid_volume_uom,
                                    L_ti,
                                    L_hi;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_DIMENSION',
                          'ITEM_SUPP_COUNTRY, ITEM_SUPP_COUNTRY_DIM',
                          NULL);
         close C_GET_DIMENSION;
         ---
         if UOM_SQL.GET_CLASS(O_error_message,
                              L_uom_class,
                              L_per_count_uom) = FALSE then
            return FALSE;
         end if;
         ---
         if L_uom_class in ('QTY') then
            if ITEM_ATTRIB_SQL.GET_STANDARD_UOM(O_error_message,
                                                L_standard_uom,
                                                L_standard_class,
                                                L_per_unit_value, -- standard UOM conversion factor
                                                I_item,
                                                'N') = FALSE then
               return FALSE;
            end if;
            ---
            if L_per_unit_value is NULL then
               if L_standard_uom <> 'EA' then
                  L_per_unit_value := 0;
                  if I_calc_type in ('IE','IA') then
                     L_unit_of_work := 'Item '||I_item||
                                       ', Component '||I_comp_id;
                  end if;
                  ---
                  if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                          SQL_LIB.GET_MESSAGE_TEXT('NO_CONV_FACTOR',
                                                                                   I_item,
                                                                                   NULL,
                                                                                   NULL),
                                                          L_program,
                                                          L_unit_of_work) = FALSE then
                     return FALSE;
                  end if;
               else
                  L_per_unit_value := 1;
               end if;
            end if;
            ---
            L_uom  := 'EA';

         elsif L_uom_class = 'PACK' then
            L_value := 1 / L_supp_pack_size;
         elsif L_uom_class = 'PALLET' then
            L_value := 1 /(L_ti * L_hi * L_supp_pack_size);
         elsif L_uom_class = 'MISC' then
            SQL_LIB.SET_MARK('OPEN',
                             'C_GET_MISC_VALUE',
                             'ITEM_SUPP_UOM',
                             NULL);
            open C_GET_MISC_VALUE;
            SQL_LIB.SET_MARK('FETCH',
                             'C_GET_MISC_VALUE',
                             'ITEM_SUPP_UOM',
                             NULL);
            fetch C_GET_MISC_VALUE into L_value;
            ---
            if C_GET_MISC_VALUE%NOTFOUND then
               L_value := 0;
               ---
               if I_calc_type in ('IE','IA') then
                  L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                    ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('NO_MISC_CONV_INFO',
                                                                                 I_item,
                                                                                 to_char(L_supplier),
                                                                                 NULL),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            end if;
            ---
            SQL_LIB.SET_MARK('CLOSE',
                             'C_GET_MISC_VALUE',
                             'ITEM_SUPP_UOM',
                             NULL);
            close C_GET_MISC_VALUE;

         elsif L_uom_class = 'MASS' then
            if L_ship_carton_wt is NULL then
               L_per_unit_value := 0;
               ---
               if I_calc_type in ('IE','IA') then
                  L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                    ', Origin Country '||L_origin_country_id||
                                    ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('NO_WT_INFO',
                                                                                I_item,
                                                                                to_char(L_supplier),
                                                                                L_origin_country_id),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
           else
               L_per_unit_value := L_ship_carton_wt/L_supp_pack_size;
               L_uom := L_weight_uom;
            end if;

         elsif L_uom_class = 'LVOL' then
            if L_liquid_volume_uom is NULL then
               L_per_unit_value := 0;
               ---
               if I_calc_type in ('IE','IA') then
                  L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                    ', Origin Country '||L_origin_country_id||
                                    ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('NO_LVOL_INFO',
                                                                                I_item,
                                                                                to_char(L_supplier),
                                                                                L_origin_country_id),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else
               L_per_unit_value := L_liquid_volume/L_supp_pack_size;
               L_uom := L_liquid_volume_uom;
            end if;

         elsif L_uom_class = 'VOL' then
            if L_ship_carton_len is NULL or L_ship_carton_wid is NULL or L_ship_carton_hgt is NULL then
               L_per_unit_value := 0;
               ---
               if I_calc_type in ('IE','IA') then
                  L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                    ', Origin Country '||L_origin_country_id||
                                    ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('NO_DIMEN_INFO',
                                                                                I_item,
                                                                                to_char(L_supplier),
                                                                                L_origin_country_id),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else
               ---
               -- Get the per unit ship carton volume.
               ---
               L_per_unit_value := (L_ship_carton_len * L_ship_carton_hgt * L_ship_carton_wid)
                                    /L_supp_pack_size;
               ---
               L_uom := L_dimension_uom||'3';
            end if;
         elsif L_uom_class = 'AREA' then
            if L_ship_carton_len is NULL or L_ship_carton_wid is NULL then
               L_per_unit_value := 0;
               ---
               if I_calc_type in ('IE','IA') then
                  L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                    ', Origin Country '||L_origin_country_id||
                                    ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('NO_DIMEN_INFO',
                                                                                I_item,
                                                                                to_char(L_supplier),
                                                                                L_origin_country_id),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else
               ---
               -- Get the per unit ship carton area.
               ---
               L_per_unit_value := (L_ship_carton_len * L_ship_carton_wid)/L_supp_pack_size;
               ---
               L_uom := L_dimension_uom||'2';
            end if;
         elsif L_uom_class = 'DIMEN' then
            if L_ship_carton_len is NULL then
               L_per_unit_value := 0;
               ---
               if I_calc_type in ('IE','IA') then
                  L_unit_of_work := 'Item '||I_item||', Supplier '||to_char(L_supplier)||
                                    ', Origin Country '||L_origin_country_id||
                                    ', Component '||I_comp_id;
               end if;
               ---
               if INTERFACE_SQL.INSERT_INTERFACE_ERROR(O_error_message,
                                                       SQL_LIB.GET_MESSAGE_TEXT('NO_DIMEN_INFO',
                                                                                I_item,
                                                                                to_char(L_supplier),
                                                                                L_origin_country_id),
                                                       L_program,
                                                       L_unit_of_work) = FALSE then
                  return FALSE;
               end if;
            else
               ---
               -- Get the per unit ship carton length.
               ---
               L_per_unit_value := L_ship_carton_len/L_supp_pack_size;
               ---
               L_uom := L_dimension_uom;
            end if;
         end if;
         ---
         if L_uom_class in ('VOL','AREA','DIMEN','QTY','MASS','LVOL') then
            if L_per_unit_value <> 0 then
               if UOM_SQL.WITHIN_CLASS(O_error_message,
                                       L_value,
                                       L_per_count_uom,
                                       L_per_unit_value,
                                       L_uom,
                                       L_uom_class) = FALSE then
                  return FALSE;
               end if;
            else
               L_value := 0;
            end if;
         end if;
         ---
         -- Calculate the Estimate Expense Value
         ---
         ---If cvb code exists, then the values of the components of the cvb are summed up and the amount 
         ---calculated is used to calculate the o_est_value
         ---If cvb code is null, then comp rate is used to calculate the o_est_value
         if L_cvb_code is null then 
            O_est_value := L_value * (L_comp_rate/L_per_count);
         else
            O_est_value := L_value * (L_comp_rate/100) * (L_amount/L_per_count);
         end if;      
     end if;  -- if I_dtl_flag = 'D'
   end if; -- if calc_basis = 'S' ('Specific')
   ---
   if O_est_value is NULL then
      O_est_value := 0;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END RECALC_ELC;
----------------------------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_FC_TARIFF_RATES(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                I_calc_type           IN       VARCHAR2,
                                I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                I_seq_no              IN       ORDSKU_HTS.SEQ_NO%TYPE,
                                I_item                IN       ITEM_MASTER.ITEM%TYPE,
                                I_pack_item           IN       ITEM_MASTER.ITEM%TYPE,
                                I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                I_hts                 IN       HTS.HTS%TYPE,
                                I_import_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                                I_origin_country_id   IN       COUNTRY.COUNTRY_ID%TYPE,
                                I_effect_from         IN       HTS.EFFECT_FROM%TYPE,
                                I_effect_to           IN       HTS.EFFECT_TO%TYPE,
                                I_comp_id             IN       ELC_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(62) := 'FUTURE_COST_SQL.UPDATE_FC_TARIFF_RATES';
   L_item               ITEM_MASTER.ITEM%TYPE;
   L_cvd_rate           ELC_COMP.COMP_RATE%TYPE;
   L_ad_rate            ELC_COMP.COMP_RATE%TYPE;
   L_rate               ELC_COMP.COMP_RATE%TYPE;
   L_tariff_treatment   HTS_TARIFF_TREATMENT.TARIFF_TREATMENT%TYPE;
   L_qty_1              NUMBER;
   L_qty_2              NUMBER;
   L_qty_3              NUMBER;
   L_units_1            HTS.UNITS_1%TYPE;
   L_units_2            HTS.UNITS_2%TYPE;
   L_units_3            HTS.UNITS_3%TYPE;
   L_specific_rate      HTS_TARIFF_TREATMENT.SPECIFIC_RATE%TYPE;
   L_av_rate            HTS_TARIFF_TREATMENT.AV_RATE%TYPE;
   L_other_rate         HTS_TARIFF_TREATMENT.OTHER_RATE%TYPE;
   L_cvd_case_no        HTS_CVD.CASE_NO%TYPE;
   L_ad_case_no         HTS_AD.CASE_NO%TYPE;
   L_duty_comp_code     HTS.DUTY_COMP_CODE%TYPE;
   L_origin_country_id  COUNTRY.COUNTRY_ID%TYPE;
   L_exists             BOOLEAN;
   L_tax_comp_code      HTS_TAX.TAX_COMP_CODE%TYPE;
   L_tax_av_rate        HTS_TAX.TAX_AV_RATE%TYPE;
   L_tax_specific_rate  HTS_TAX.TAX_SPECIFIC_RATE%TYPE;
   L_fee_comp_code      HTS_FEE.FEE_COMP_CODE%TYPE;
   L_fee_av_rate        HTS_FEE.FEE_AV_RATE%TYPE;
   L_fee_specific_rate  HTS_FEE.FEE_SPECIFIC_RATE%TYPE;
   L_system_options_rec SYSTEM_OPTIONS%ROWTYPE;
   L_comp_rate_code     HTS_COMPUTATION.COMP_RATE_CODE%TYPE;
   L_default_comp_rate  ELC_COMP.COMP_RATE%TYPE;
   L_configurable_ind   BOOLEAN := FALSE;
   cursor C_GET_CVD_RATE is
      select rate
        from hts_cvd
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and origin_country_id = L_origin_country_id
         and case_no           = L_cvd_case_no;

   cursor C_GET_AD_RATE is
      select rate
        from hts_ad
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and origin_country_id = L_origin_country_id
         and case_no           = L_ad_case_no;

   cursor C_TAX_COMP is
      select tax_specific_rate,
             tax_av_rate,
             tax_comp_code
        from hts_tax
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and ((tax_comp_code in ('1','2','4','5','C','D','7','9')
               and tax_type||tax_comp_code||'A'||I_import_country_id = I_comp_id)
              or (tax_comp_code in ('4','5','D')
                  and tax_type||tax_comp_code||'B'||I_import_country_id = I_comp_id))
         and not exists (select 1 
                      from hts_computation hc1,
                           hts_tax ht
                     where hc1.computation_code  = ht.tax_comp_code
                            and hc1.import_country_id = ht.import_country_id)
      UNION ALL
      select ht.tax_specific_rate,
             ht.tax_av_rate,
             ht.tax_comp_code
        from hts_tax ht,
             hts_tax_zone htz,
             hts_computation hc
       where ht.hts                   = I_hts
         and ht.import_country_id     = I_import_country_id
         and ht.effect_from           = I_effect_from
         and ht.effect_to             = I_effect_to
         and htz.hts(+)               = ht.hts
         and htz.import_country_id(+) = ht.import_country_id
         and htz.effect_from(+)       = ht.effect_from
         and htz.effect_to(+)         = ht.effect_to
         and htz.tax_type(+)          = ht.tax_type                                                                     
         and ht.tax_comp_code         = hc.computation_code      
         and ht.import_country_id     = hc.import_country_id                                                                                                                      
         and ht.tax_type||ht.tax_comp_code||hc.comp_seq||hc.import_country_id = I_comp_id;
   cursor C_FEE_COMP is
      select fee_specific_rate,
             fee_av_rate,
             fee_comp_code
        from hts_fee
       where hts               = I_hts
         and import_country_id = I_import_country_id
         and effect_from       = I_effect_from
         and effect_to         = I_effect_to
         and ((fee_comp_code in ('1','2','4','5','C','D','7','9')
               and fee_type||fee_comp_code||'A'||I_import_country_id = I_comp_id)
             or (fee_comp_code in ('4','5','D')
                 and fee_type||fee_comp_code||'B'||I_import_country_id = I_comp_id))
         and not exists (select 1 
                      from hts_computation hc1,
                           hts_fee hf
                     where hc1.computation_code  = hf.fee_comp_code
                            and hc1.import_country_id = hf.import_country_id)
      UNION ALL
      select hf.fee_specific_rate,
             hf.fee_av_rate,
             hf.fee_comp_code
        from hts_fee hf,
             hts_fee_zone hfz,
             hts_computation hc
       where hf.hts                   = I_hts
         and hf.import_country_id     = I_import_country_id
         and hf.effect_from           = I_effect_from
         and hf.effect_to             = I_effect_to
         and hfz.hts(+)               = hf.hts
         and hfz.import_country_id(+) = hf.import_country_id
         and hfz.effect_from(+)       = hf.effect_from
         and hfz.effect_to(+)         = hf.effect_to
         and hfz.fee_type(+)          = hf.fee_type
         and hf.fee_comp_code         = hc. computation_code                                                                                                                            
         and hf.fee_type||hf.fee_comp_code||hc.comp_seq||hc.import_country_id = I_comp_id;

   cursor C_COMP_RATE_CODE is
      select hc.comp_rate_code,
             ec.comp_rate
        from hts_computation hc,
             elc_comp ec
       where ec.comp_id           = I_comp_id
         and ec.comp_id like     '%'||hc.computation_code||hc.comp_seq||hc.import_country_id
         and hc.import_country_id = I_import_country_id
    and rownum               = 1;
BEGIN
   ---
   -- System Generated assessments (assessments that are automatically attached
   -- when an HTS code is attached to an Item or Order/Item) need to have their
   -- rates updated before calculating.
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   ---
   if I_pack_item is not NULL then
      L_item := I_pack_item;
   else
      L_item := I_item;
   end if;
   ---
   if I_origin_country_id is NULL then
      if ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY(O_error_message,
                                                  L_exists,
                                                  L_origin_country_id,
                                                  I_order_no,
                                                  L_item) = FALSE then
         return FALSE;
      end if;
   else
      L_origin_country_id := I_origin_country_id;
   end if;
   ---
   open  C_COMP_RATE_CODE;
   fetch C_COMP_RATE_CODE into L_comp_rate_code,L_default_comp_rate;
     if C_COMP_RATE_CODE%FOUND then
        L_configurable_ind := TRUE;
     end if;
   close C_COMP_RATE_CODE;

   ---
   if I_comp_id in ('DTY0A'||I_import_country_id,'DTY1A'||I_import_country_id,
                    'DTY2A'||I_import_country_id,'DTY3A'||I_import_country_id,
                    'DTY4A'||I_import_country_id,'DTY5A'||I_import_country_id,
                    'DTY6A'||I_import_country_id,'DTY7A'||I_import_country_id,
                    'DTY9A'||I_import_country_id,'DTYCA'||I_import_country_id,
                    'DTYDA'||I_import_country_id,'DTYEA'||I_import_country_id,
                    'DTY3B'||I_import_country_id,'DTY4B'||I_import_country_id,
                    'DTY5B'||I_import_country_id,'DTY6B'||I_import_country_id,
                    'DTYDB'||I_import_country_id,'DTYEB'||I_import_country_id,
                    'DTY6C'||I_import_country_id,'DTYEC'||I_import_country_id,
                    'DUTY'||I_import_country_id,'CVD'||I_import_country_id,
                    'AD'||I_import_country_id)  then
      if I_calc_type = 'IA' then
         if L_system_options_rec.hts_tracking_level = 'S' then
            if ITEM_HTS_SQL.GET_HTS_DETAILS(O_error_message,
                                            L_tariff_treatment,
                                            L_qty_1,
                                            L_qty_2,
                                            L_qty_3,
                                            L_units_1,
                                            L_units_2,
                                            L_units_3,
                                            L_specific_rate,
                                            L_av_rate,
                                            L_other_rate,
                                            L_cvd_case_no,
                                            L_ad_case_no,
                                            L_duty_comp_code,
                                            I_item,
                                            I_supplier,
                                            I_hts,
                                            I_import_country_id,
                                            L_origin_country_id,
                                            NULL,
                                            I_effect_from,
                                            I_effect_to) = FALSE then
               return FALSE;
            end if;
         else
            if ITEM_HTS_SQL.GET_HTS_DETAILS(O_error_message,
                                            L_tariff_treatment,
                                            L_qty_1,
                                            L_qty_2,
                                            L_qty_3,
                                            L_units_1,
                                            L_units_2,
                                            L_units_3,
                                            L_specific_rate,
                                            L_av_rate,
                                            L_other_rate,
                                            L_cvd_case_no,
                                            L_ad_case_no,
                                            L_duty_comp_code,
                                            I_item,
                                            I_supplier,
                                            I_hts,
                                            I_import_country_id,
                                            NULL,
                                            L_origin_country_id,
                                            I_effect_from,
                                            I_effect_to) = FALSE then
               return FALSE;
            end if;
         end if;
      end if;
      ---
      if L_cvd_case_no is not NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_CVD_RATE',
                          'HTS_CVD',
                          NULL);
         open C_GET_CVD_RATE;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_CVD_RATE',
                          'HTS_CVD',
                          NULL);
         fetch C_GET_CVD_RATE into L_cvd_rate;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_CVD_RATE',
                          'HTS_CVD',
                          NULL);
         close C_GET_CVD_RATE;
      end if;
      ---
      if L_ad_case_no is not NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_GET_AD_RATE',
                          'HTS_AD',
                          NULL);
         open C_GET_AD_RATE;
         SQL_LIB.SET_MARK('FETCH',
                          'C_GET_AD_RATE',
                          'HTS_AD',
                          NULL);
         fetch C_GET_AD_RATE into L_ad_rate;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_GET_AD_RATE',
                          'HTS_AD',
                          NULL);
         close C_GET_AD_RATE;
      end if;
      ---
      if L_configurable_ind and I_comp_id like 'DTY%' then
         if L_comp_rate_code = 'S' then 
            L_rate := L_specific_rate;
         elsif L_comp_rate_code = 'A' then 
            L_rate := L_av_rate;
         elsif L_comp_rate_code = 'O' then 
            L_rate := L_other_rate;
         else
            L_rate := L_default_comp_rate;
         end if;
      else

         if L_duty_comp_code in ('1','2','3','4','5','6','C','D','E') then
            L_rate := L_specific_rate;
         elsif L_duty_comp_code in ('0','7','9') then
            L_rate := L_av_rate;
         end if;
         ---
         if I_comp_id in ('DTY4B'||I_import_country_id,'DTY5B'||I_import_country_id,
                          'DTYDB'||I_import_country_id,'DTY6C'||I_import_country_id,
                          'DTYEC'||I_import_country_id) then
            L_rate := L_av_rate;
         elsif I_comp_id in ('DTY3B'||I_import_country_id,'DTY6B'||I_import_country_id,
                             'DTYEB'||I_import_country_id) then
            L_rate := L_other_rate;
         elsif I_comp_id = 'CVD'||I_import_country_id then
            L_rate := L_cvd_rate;
         elsif I_comp_id = 'AD'||I_import_country_id then
            L_rate := L_ad_rate;
         elsif I_comp_id = 'DUTY'||I_import_country_id then
            L_rate := 100;
         end if;
      end if;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_TAX_COMP',
                       'HTS_TAX',
                       NULL);
      open C_TAX_COMP;
      SQL_LIB.SET_MARK('FETCH',
                       'C_TAX_COMP',
                       'HTS_TAX',
                       NULL);
      fetch C_TAX_COMP into L_tax_specific_rate,
                            L_tax_av_rate,
                            L_tax_comp_code;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_TAX_COMP',
                       'HTS_TAX',
                       NULL);
      close C_TAX_COMP;
      ---
      if L_tax_specific_rate is NULL then
         SQL_LIB.SET_MARK('OPEN',
                          'C_FEE_COMP',
                          'HTS_FEE',
                          NULL);
         open C_FEE_COMP;
         SQL_LIB.SET_MARK('FETCH',
                          'C_FEE_COMP',
                          'HTS_FEE',
                          NULL);
         fetch C_FEE_COMP into L_fee_specific_rate,
                               L_fee_av_rate,
                               L_fee_comp_code;
         SQL_LIB.SET_MARK('CLOSE',
                          'C_FEE_COMP',
                          'HTS_FEE',
                          NULL);
         close C_FEE_COMP;
         ---
         if L_fee_comp_code is not NULL then
            if L_configurable_ind then
               if L_comp_rate_code = 'S' then 
                  L_rate := L_fee_specific_rate;
               elsif L_comp_rate_code = 'A' then 
                  L_rate := L_fee_av_rate;
               else
                  L_rate := L_default_comp_rate;
               end if;
       else
          if L_fee_comp_code in ('1','2','4','5','C','D') then
                  L_rate := L_fee_specific_rate;


               elsif L_fee_comp_code in ('7','9') then
                  L_rate := L_fee_av_rate;

          end if;
       end if;
    end if;
      else
         if L_configurable_ind then   
            if L_comp_rate_code = 'S' then 
                 L_rate := L_tax_specific_rate;
            elsif L_comp_rate_code = 'A' then 
                 L_rate := L_tax_av_rate;
            else
                 L_rate := L_default_comp_rate;
            end if;
         else
       if L_tax_comp_code in ('1','2','4','5','C','D') then
               L_rate := L_tax_specific_rate;


       elsif L_tax_comp_code in ('7','9') then
               L_rate := L_tax_av_rate;
       end if;
         end if;
      end if;
   end if;
   ---
   if (I_calc_type = 'IA' and L_rate is not NULL) then---here
      update gtt_fc_item_hts_assess
         set comp_rate = L_rate,
             last_update_datetime = sysdate,
             last_update_id = LP_user
       where item = I_item
         and hts = I_hts
         and import_country_id = I_import_country_id
         and origin_country_id = L_origin_country_id
         and effect_from = I_effect_from
         and effect_to = I_effect_to
         and comp_id = I_comp_id;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_FC_TARIFF_RATES;
----------------------------------------------------------------------------------------
FUNCTION PROCESS_PRICE_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(100) := 'FUTURE_COST_SQL.PROCESS_PRICE_CHG';

BEGIN

   if EXPLODE_PC(O_error_message,
                 I_cost_event_process_id,
                 I_thread_id) = FALSE then
      return FALSE;
   end if;

   if MERGE_PC(O_error_message,
                     I_cost_event_process_id,
                     I_thread_id) = FALSE THEN
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END PROCESS_PRICE_CHG;
----------------------------------------------------------------------------------------
FUNCTION EXPLODE_PC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                    I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.EXPLODE_PC';

BEGIN

      insert into future_cost_gtt (item,
                                   supplier,
                                   origin_country_id,
                                   location,
                                   loc_type,
                                   active_date,
                                   base_cost,
                                   net_cost,
                                   net_net_cost,
                                   dead_net_net_cost,
                                   pricing_cost,
                                   calc_date,
                                   start_ind,
                                   acquisition_cost,
                                   elc_amt,
                                   primary_supp_country_ind,
                                   currency_code,
                                   division,
                                   group_no,
                                   dept,
                                   class,
                                   subclass,
                                   item_parent,
                                   item_grandparent,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   chain,
                                   area,
                                   region,
                                   district,
                                   supp_hier_lvl_1,
                                   supp_hier_lvl_2,
                                   supp_hier_lvl_3,
                                   reclass_no,
                                   cost_change,
                                   simple_pack_ind,
                                   primary_cost_pack,
                                   primary_cost_pack_qty,
                                   store_type,
                                   costing_loc,
                                   templ_id,
                                   passthru_pct,
                                   negotiated_item_cost,
                                   extended_base_cost,
                                   wac_tax,
                                   default_costing_type,
                                   processing_seq_no)
select /*+ index(FC, FUTURE_COST_I4) */ distinct fc.item,
                   fc.supplier,
                   fc.origin_country_id,
                   fc.location,
                   fc.loc_type,
                   fc.active_date,
                   fc.base_cost,
                   fc.net_cost,
                   fc.net_net_cost,
                   fc.dead_net_net_cost,
                   fc.pricing_cost,
                   fc.calc_date,
                   fc.start_ind,
                   fc.acquisition_cost,
                   fc.elc_amt,
                   fc.primary_supp_country_ind,
                   fc.currency_code,
                   fc.division,
                   fc.group_no,
                   fc.dept,
                   fc.class,
                   fc.subclass,
                   fc.item_parent,
                   fc.item_grandparent,
                   fc.diff_1,
                   fc.diff_2,
                   fc.diff_3,
                   fc.diff_4,
                   fc.chain,
                   fc.area,
                   fc.region,
                   fc.district,
                   fc.supp_hier_lvl_1,
                   fc.supp_hier_lvl_2,
                   fc.supp_hier_lvl_3,
                   fc.reclass_no,
                   fc.cost_change,
                   fc.simple_pack_ind,
                   fc.primary_cost_pack,
                   fc.primary_cost_pack_qty,
                   fc.store_type,
                   fc.costing_loc,
                   fc.templ_id,
                   fc.passthru_pct,
                   fc.negotiated_item_cost,
                   fc.extended_base_cost,
                   fc.wac_tax,
                   fc.default_costing_type,
                   null as processing_seq_no
     from cost_event_thread ct,
          future_cost fc
    where ct.cost_event_process_id = I_cost_event_process_id
      and ct.thread_id             = I_thread_id
      and ct.item                  = fc.item
      and ct.supplier              = fc.supplier
      and ct.origin_country_id     = fc.origin_country_id
      and ct.location              = fc.location
      and nvl(fc.store_type, 'X')  ='F';

   if LOCK_FUTURE_COST(O_error_message,
                       I_cost_event_process_id,
                       I_thread_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END EXPLODE_PC;
----------------------------------------------------------------------------------------
FUNCTION MERGE_PC(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  I_cost_event_process_id IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                  I_thread_id             IN     COST_EVENT_THREAD.THREAD_ID%TYPE)
RETURN BOOLEAN IS
   
   L_program       VARCHAR2(100) := 'FUTURE_COST_SQL.MERGE_PC';

BEGIN

   -- Find future cost event records on the timeline on the same date or prior
   -- to the price changes for franchise stores.
   -- Records on the same date as the price change will have their pricing cost updated
   -- if there is an active retail template applicable during the time of price change
   -- If only prior records exists, then insert a new row for the price change.

   merge into future_cost_gtt fc
   using (
      select * from (
      select fc.*,
            ((cer.unit_retail * mc.exchange_rate) * (1 - (cer.margin_pct/100))) new_pricing_cost,
            fc.active_date new_active_date,
            cer.start_date,
            cer.end_date, 
            rank()
              over (partition by fc.item,
                                 fc.supplier,
                                 fc.origin_country_id,
                                 fc.location
                        order by fc.active_date desc) ranking
       from cost_event_retail_change cer,
            future_cost_gtt fc,
            store st,
            sups s,
            (select from_currency,
                    to_currency,
                    effective_date,
                    exchange_rate,
                    rank() over
                        (PARTITION BY from_currency, to_currency, exchange_type
                             ORDER BY effective_date DESC) date_rank
               from mv_currency_conversion_rates 
              where exchange_type = 'C'
                     and effective_date   <= (LP_vdate + 1)  ) mc             
      where cer.cost_event_process_id = I_cost_event_process_id
        and fc.item                   = cer.item
        and fc.templ_id               = cer.templ_id 
        and cer.loc                   = fc.location    
        and fc.location               = st.store
        and fc.supplier               = s.supplier
        and mc.from_currency          = st.currency_code
        and mc.to_currency            = s.currency_code
        and mc.date_rank              = 1
        and fc.active_date            >=  cer.start_date
        and fc.active_date            <   cer.end_date 
        and fc.active_date  > (LP_vdate + 1) 
      UNION
      select * from               
         (select fc.*,       
                 ((cer.unit_retail * mc.exchange_rate) * (1 - (cer.margin_pct/100))) new_pricing_cost,
                 case               
                   when fc.active_date > (LP_vdate + 1) then fc.active_date         
                      else (LP_vdate + 1) 
                 end new_active_date,           
                 cer.start_date,               
                 cer.end_date,                
                 rank()               
                  over (partition by fc.item,               
                                     fc.supplier,               
                                     fc.origin_country_id,               
                                     fc.location               
                            order by fc.active_date desc) as ranking               
            from cost_event_retail_change cer,               
                 future_cost_gtt fc,
                 store st,
                 sups s,
                 (select from_currency,
                        to_currency,
                        effective_date,
                        exchange_rate,
                        rank() over
                            (PARTITION BY from_currency, to_currency, exchange_type
                                 ORDER BY effective_date DESC) date_rank
                   from mv_currency_conversion_rates 
                  where exchange_type = 'C'
                          and effective_date   <= (LP_vdate + 1)  ) mc          
           where cer.cost_event_process_id = I_cost_event_process_id               
             and fc.item                   = cer.item
             and fc.templ_id               = cer.templ_id 
             and cer.loc                   = fc.location                    
             and fc.active_date           >= cer.start_date               
             and fc.active_date           <  cer.end_date        
             and fc.location               = st.store
             and fc.supplier               = s.supplier
             and mc.from_currency          = st.currency_code
             and mc.to_currency            = s.currency_code
             and mc.date_rank              = 1 
             and fc.active_date  <= (LP_vdate + 1))               
             where  ranking = 1)) use_this
   on (    fc.item              = use_this.item
       and fc.supplier          = use_this.supplier
       and fc.origin_country_id = use_this.origin_country_id
       and fc.location          = use_this.location
       and fc.active_date       = use_this.new_active_date)
   when matched then
   update
      set  
        fc.pricing_cost     = use_this.new_pricing_cost 
   when not matched then
   insert (item,
           supplier,
           origin_country_id,
           location,
           loc_type,
           active_date,
           base_cost,
           net_cost,
           net_net_cost,
           dead_net_net_cost,
           pricing_cost,
           calc_date,
           start_ind,
           primary_supp_country_ind,
           currency_code,
           division,
           group_no,
           dept,
           class,
           subclass,
           item_grandparent,
           item_parent,
           diff_1,
           diff_2,
           diff_3,
           diff_4,
           chain,
           area,
           region,
           district,
           supp_hier_lvl_1,
           supp_hier_lvl_2,
           supp_hier_lvl_3,
           reclass_no,
           cost_change,
           simple_pack_ind,
           primary_cost_pack,
           primary_cost_pack_qty,
           store_type,
           costing_loc,
           negotiated_item_cost,
           extended_base_cost,
           wac_tax,
           default_costing_type)
   values (use_this.item,
           use_this.supplier,
           use_this.origin_country_id,
           use_this.location,
           use_this.loc_type,
           use_this.new_active_date,
           use_this.base_cost,
           use_this.net_cost,
           use_this.net_net_cost,
           use_this.dead_net_net_cost,
           use_this.new_pricing_cost,
           use_this.calc_date,
           'Y',
           use_this.primary_supp_country_ind,
           use_this.currency_code,
           use_this.division,
           use_this.group_no,
           use_this.dept,
           use_this.class,
           use_this.subclass,
           use_this.item_grandparent,
           use_this.item_parent,
           use_this.diff_1,
           use_this.diff_2,
           use_this.diff_3,
           use_this.diff_4,
           use_this.chain,
           use_this.area,
           use_this.region,
           use_this.district,
           use_this.supp_hier_lvl_1,
           use_this.supp_hier_lvl_2,
           use_this.supp_hier_lvl_3,
           use_this.reclass_no,
           use_this.cost_change,
           use_this.simple_pack_ind,
           use_this.primary_cost_pack,
           use_this.primary_cost_pack_qty,
           use_this.store_type,
           use_this.costing_loc,
           use_this.negotiated_item_cost,
           use_this.extended_base_cost,
           use_this.wac_tax,
           use_this.default_costing_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END MERGE_PC;
-----------------------------------------------------------------------------------------------------------------------------------
END FUTURE_COST_SQL;
/