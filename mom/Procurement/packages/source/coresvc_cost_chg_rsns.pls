CREATE OR REPLACE PACKAGE CORESVC_COST_CHG_REASON AUTHID CURRENT_USER AS
   template_key                  CONSTANT VARCHAR2(255):='COST_CHG_DATA';
   action_new                    VARCHAR2(25)          := 'NEW';
   action_mod                    VARCHAR2(25)          := 'MOD';
   action_del                    VARCHAR2(25)          := 'DEL';
   COST_CHG_RSN_TL_sheet         VARCHAR2(255)         := 'COST_CHG_REASON';
   COST_CHG_RSN_TL$Action        NUMBER                :=1;
   COST_CHG_RSN_TL$REASON_DESC   NUMBER                :=3;
   COST_CHG_RSN_TL$REASON        NUMBER                :=2;
   
   COST_CHG_RSN_LANG_sheet         VARCHAR2(255)         := 'COST_CHG_REASON_TL';
   COST_CHG_RSN_LANG$Action        NUMBER                :=1;
   COST_CHG_RSN_LANG$LANG          NUMBER                :=2;
   COST_CHG_RSN_LANG$REASON        NUMBER                :=3;
   COST_CHG_RSN_LANG$REASON_DESC   NUMBER                :=4;

   sheet_name_trans             S9T_PKG.trans_map_typ;
   action_column                VARCHAR2(255) := 'ACTION';
   template_category            CODE_DETAIL.CODE%TYPE := 'RMSPCO';

   TYPE COST_CHG_RSN_TL_rec_tab IS TABLE OF V_COST_CHG_REASON_TL%ROWTYPE;
-----------------------------------------------------------------------------------   
   FUNCTION CREATE_S9T(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_file_id             IN OUT   S9T_FOLDER.FILE_ID%TYPE,
                       I_template_only_ind   IN       CHAR DEFAULT 'N')
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------   
   FUNCTION PROCESS_S9T(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE ,
                        O_error_count     IN OUT   NUMBER,
                        I_file_id         IN       S9T_FOLDER.FILE_ID%TYPE,
                        I_process_id      IN       NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------   
   FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_error_count     IN OUT   NUMBER,   
                    I_process_id      IN       NUMBER,
                    I_chunk_id        IN       NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------   
   FUNCTION GET_SHEET_NAME_TRANS(I_sheet_name   IN   VARCHAR2)
   RETURN VARCHAR2;
-----------------------------------------------------------------------------------
END CORESVC_COST_CHG_REASON;
/