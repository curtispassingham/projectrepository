
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TRANS_FINALIZE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
--Function Name : FINALIZE
--Purpose       : Moves data from the transportation module into the
--                customs entry module.
--------------------------------------------------------------------------------------
FUNCTION FINALIZE(O_error_message   IN OUT  VARCHAR2,
                  I_vessel_id       IN      TRANSPORTATION.VESSEL_ID%TYPE,
                  I_voyage_flt_id   IN      TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                  I_est_depart_date IN      TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                  I_entry_no        IN      CE_HEAD.ENTRY_NO%TYPE,
                  I_import_country  IN      CE_HEAD.IMPORT_COUNTRY_ID%TYPE,
                  I_currency_code   IN      CE_HEAD.CURRENCY_CODE%TYPE,
                  I_broker_id       IN      CE_HEAD.BROKER_ID%TYPE)
         RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function: CUSTOM_VAL
-- Purpose : This function will be used for validationg the custom function.
--------------------------------------------------------------------------------------------
FUNCTION CUSTOM_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_vessel_id         IN       TRANSPORTATION.VESSEL_ID%TYPE,
                    I_voyage_flt_id     IN       TRANSPORTATION.VOYAGE_FLT_ID%TYPE,
                    I_est_depart_date   IN       TRANSPORTATION.ESTIMATED_DEPART_DATE%TYPE,
                    I_function_key      IN       VARCHAR2,
                    I_seq_no            IN       NUMBER)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
END TRANS_FINALIZE_SQL;
/
