
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_ITEM_VALIDATE_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------------
-- Name:     EXIST_ORDLOC_WKSHT
-- Purpose:  This function will determine if a record exists on ordloc_wksht 
--           the specified order.
--------------------------------------------------------------------------------
FUNCTION EXIST_ORDLOC_WKSHT(O_error_message  IN OUT  VARCHAR2,
                            O_exists         IN OUT  BOOLEAN,
                            I_order_no       IN      ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:     EXIST_ORDLOC
-- Purpose:  This function will determine if a record exists on ordloc_wksht
--           for the specified order/loc_type, order/item, or order/item/loc
--           type/location combination.
--------------------------------------------------------------------------------
FUNCTION EXIST_ORDLOC(O_error_message  IN OUT  VARCHAR2,
                      O_exists         IN OUT  BOOLEAN,
                      I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                      I_item           IN      ITEM_MASTER.ITEM%TYPE,
                      I_location       IN      ORDLOC.LOCATION%TYPE,
                      I_loc_type       IN      ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Name:     EXIST_ORDSKU
-- Purpose:  This function will determine if a record exists on ordsku
--           for the specified order or order/item combination.
--------------------------------------------------------------------------------
FUNCTION EXIST_ORDSKU(O_error_message  IN OUT  VARCHAR2,
                      O_exists         IN OUT  BOOLEAN,
                      I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                      I_item           IN      ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:     CHECK_SHIPPED
-- Purpose:  This function will determine if any quantity has been received
--           for the specified order and, optionally, item or location.
-----------------------------------------------------------------------------------
FUNCTION CHECK_SHIPPED(O_error_message  IN OUT  VARCHAR2,
                       O_shipped        IN OUT  BOOLEAN,
                       I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                       I_item           IN      ITEM_MASTER.ITEM%TYPE,
                       I_location       IN      ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:     TSF_PO_LINK_EXISTS
-- Purpose:  This function will check if a transfer/purchase order link number(s) is 
-- -         associated with the combination of input parameters that contain values.  
--           All input variables can be NULL.  For example:  if all input parameters are NULL, 
--           check if a link exists on any orders; if I_tsf_po_link_no has a value and 
--           all other input parameters are NULL, check if that transfer/PO link number
--           exists for any orders; if I_tsf_po_link_no and I_order_no contain values 
--           check if that transfer/PO link number exists for the passed in order; 
--           if I_order_no and I_item contain values but all other input parameters are NULL, 
--           check if a transfer/PO link number exists for the order/item combination; and so on. 
--           If a location is passed in, it must be a virtual location.
---------------------------------------------------------------------------------
FUNCTION TSF_PO_LINK_EXISTS(O_error_message   IN OUT   VARCHAR,
                            O_exists          IN OUT   BOOLEAN,
                            I_order_no        IN       ORDLOC.ORDER_NO%TYPE,
                            I_item            IN       ORDLOC.ITEM%TYPE,
                            I_location        IN       ORDLOC.LOCATION%TYPE,
                            I_tsf_po_link_no  IN       ORDLOC.TSF_PO_LINK_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:     CHECK_QTY_CHANGE
-- Purpose:  This function will determine if either the passed in new quantity 
--           ordered for the order/item/location can be changed to that value.
--           The location on the order should be passed into I_location.  This
--           means, in a multichannel environment a virtual location will be 
--           passed.  I_new_qty_ordered should contain what the user is changing 
--           the qty ordered to, I_qty_ordered should contain the previous
--           qty ordered and I_qty_cancelled should contain the previous
--           qty cancelled.  In I_ord_or_cancel_chg, pass 'O' if the qty ordered
--           has been changed, and 'C' if the qty cancelled has been changed.
--           I_supp_pack_size is only needed if the user has changed the qty
--           cancelled UOP.  This will allow error messages to display the
--           qty in the UOP.
------------------------------------------------------------------------
FUNCTION CHECK_QTY_CHANGE(O_error_message     IN OUT VARCHAR2,
                          O_valid_qty_change  IN OUT BOOLEAN,
                          I_order_no          IN     ORDLOC.ORDER_NO%TYPE,
                          I_item              IN     ORDLOC.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_new_qty_ordered   IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_ordered       IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_allocated     IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_received      IN     ORDLOC.QTY_RECEIVED%TYPE,
                          I_qty_cancelled     IN     ORDLOC.QTY_CANCELLED%TYPE,
                          I_orig_repl_qty     IN     ORDLOC.ORIGINAL_REPL_QTY%TYPE,
                          I_min_repl_qty      IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_max_repl_qty      IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_supp_pack_size    IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                          I_ord_or_cancel_chg IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
-- Name:     GET_QTY_CHANGE
-- Purpose:  This function will determine the quantity thresholds within which
--           the ordered/cancelled quantity can be changed 
--           In I_ord_or_cancel_chg, pass 'O' if the qty ordered thresholds are
--           required, and 'C' if the qty cancelled thresholds are required.
------------------------------------------------------------------------
FUNCTION GET_QTY_CHANGE(O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_low_val           IN OUT ORDLOC.QTY_ORDERED%TYPE,
                          O_high_val          IN OUT ORDLOC.QTY_ORDERED%TYPE,
                          I_order_no          IN     ORDLOC.ORDER_NO%TYPE,
                          I_item              IN     ORDLOC.ITEM%TYPE,
                          I_location          IN     ORDLOC.LOCATION%TYPE,
                          I_loc_type          IN     ORDLOC.LOC_TYPE%TYPE,
                          I_qty_ordered       IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_allocated     IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_qty_received      IN     ORDLOC.QTY_RECEIVED%TYPE,
                          I_qty_cancelled     IN     ORDLOC.QTY_CANCELLED%TYPE,
                          I_orig_repl_qty     IN     ORDLOC.ORIGINAL_REPL_QTY%TYPE,
                          I_min_repl_qty      IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_max_repl_qty      IN     ORDLOC.QTY_ORDERED%TYPE,
                          I_supp_pack_size    IN     ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE,
                          I_ord_or_cancel_chg IN     VARCHAR2)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
END;
/
