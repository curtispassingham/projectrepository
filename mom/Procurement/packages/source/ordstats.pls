CREATE OR REPLACE PACKAGE ORDER_STATUS_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------
-- Function Name: CHECK_SUBMIT_APPROVE
-- Purpose:       This function will check to ensure that the order is
--                completed and ready for submission or approval.
-----------------------------------------------------------------------
FUNCTION CHECK_SUBMIT_APPROVE(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_invalid_date       IN OUT   VARCHAR2,
                              O_store_closed_ind   IN OUT   VARCHAR2,
                              O_store_opened_ind   IN OUT   VARCHAR2,
                              O_clearance_ind      IN OUT   VARCHAR2,
                              O_wksht_recs_ind     IN OUT   VARCHAR2,
                              O_hts_ind            IN OUT   VARCHAR2,
                              O_valid_lc_ind       IN OUT   VARCHAR2,
                              O_scale_ind          IN OUT   VARCHAR2,
                              O_recalc_ind         IN OUT   VARCHAR2,
                              O_round_ind          IN OUT   VARCHAR2,
                              O_invalid_items      IN OUT   VARCHAR2,
                              O_error_collection   IN OUT   NOCOPY OBJ_ERROR_TBL,
                              I_source             IN       VARCHAR2 DEFAULT NULL,
                              I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                              I_new_status         IN       ORDHEAD.STATUS%TYPE,
                              I_not_before_date    IN       ORDHEAD.NOT_BEFORE_DATE%TYPE,
                              I_not_after_date     IN       ORDHEAD.NOT_AFTER_DATE%TYPE,
                              I_supplier           IN       ORDHEAD.SUPPLIER%TYPE,
                              I_dept               IN       ORDHEAD.DEPT%TYPE,
                              I_check_loc_status   IN       VARCHAR2 DEFAULT      'N')
RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION CHECK_LOC_STATUS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_approved_ord    IN OUT   VARCHAR2,
                          I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: CHECK_SHIPMENT
-- Purpose:       This function will check for any outstanding
--                shipments for the order/item/location being cancelled.
-----------------------------------------------------------------------
FUNCTION CHECK_SHIPMENT(O_error_message IN OUT VARCHAR2,
                        O_err_flag      IN OUT BOOLEAN,
                        I_order_no      IN     ordloc.order_no%TYPE,
                        I_item          IN     ordloc.item%TYPE,
                        I_location      IN     ordloc.location%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: CANCEL_ALL
-- Purpose:       This function will execute logic needed to cancel
--                all item/location records on an order.
-----------------------------------------------------------------------
FUNCTION CANCEL_ALL(O_error_message     IN OUT   VARCHAR2,
                    I_order_no          IN       ORDLOC.ORDER_NO%TYPE,
                    I_cancel_code       IN       ORDLOC.CANCEL_CODE%TYPE,
                    I_cancel_id         IN       ORDLOC.CANCEL_ID%TYPE,
                    I_alloc_close_ind   IN       VARCHAR2)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: REINSTATE_ALL
-- Purpose:       This function will execute logic needed to reinstate
--                all item/location records on an order.
-----------------------------------------------------------------------
FUNCTION REINSTATE_ALL(O_error_message IN OUT VARCHAR2,
                       I_order_no      IN     ordhead.order_no%TYPE)
                       RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: DELETE_WKSHT
-- Purpose:       This function, called on order approval, will delete
--                all outstanding records on the worksheet tables.
-----------------------------------------------------------------------
FUNCTION DELETE_WKSHT(O_error_message IN OUT VARCHAR2,
                      I_order_no      IN     ordhead.order_no%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
--Function Name: AUTO_APPROVE_ORDER
--Purpose:       This function will perform all necessary actions to automatically approve
--               an order. It will first conduct a series of checks. The function will fail
--               if any of the required conditions for an approved order are not met.
--               If the checks are performed successfully, the order will be
--               approved.
-----------------------------------------------------------------------
FUNCTION AUTO_APPROVE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                            I_supplier        IN       ORDHEAD.SUPPLIER%TYPE,
                            I_dept            IN       ORDHEAD.DEPT%TYPE,
                            I_not_aft_date    IN       ORDHEAD.NOT_AFTER_DATE%TYPE,
                            I_user_id         IN       ORDHEAD.ORIG_APPROVAL_ID%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: SET_INIT_COST
-- Purpose:       This function will update the unit_cost_init field on
--                ORDLOC with the current ITEM_SUPP_COUNTRY_LOC cost on
--                order approval
-----------------------------------------------------------------------
FUNCTION SET_INIT_COST (O_error_message   IN OUT VARCHAR2,
                        I_supplier        IN     ordhead.supplier%TYPE,
                        I_order_no        IN     ordhead.order_no%TYPE,
                        I_order_currency  IN     ordhead.currency_code%TYPE DEFAULT NULL,
                        I_exchange_rate   IN     ordhead.exchange_rate%TYPE DEFAULT NULL)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: DELETE_DEAL_CALC_QUEUE
-- Purpose:       This function, called when an order is closed or canceled.
--                It will delete the order's record on the deal_calc_queue table.
-----------------------------------------------------------------------
FUNCTION DELETE_DEAL_CALC_QUEUE(O_error_message IN OUT VARCHAR2,
                                I_order_no      IN     ordhead.order_no%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: CLOSE_ALLOCS
-- Purpose:       This function will close all outstanding Allocations
--                for the specified order.
-----------------------------------------------------------------------
FUNCTION CHECK_OPEN_ALLOC(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exists          IN OUT BOOLEAN,
                          O_ASN_exists      IN OUT BOOLEAN,
                          I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------
-- Function Name: UPDATE_ALLOC_STATUS
-- Purpose:       This function will set the allocation status to 'A'pproved
--                for the specified order.
-----------------------------------------------------------------------
FUNCTION UPDATE_ALLOC_STATUS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------
-- Function Name: INSERT_ORDER_REVISION
-- Purpose:       This function will insert record into the revision tables
--                ordhead_rev, ordsku_rev and ordloc_rev for the Approved order
-------------------------------------------------------------------------------
FUNCTION INSERT_ORDER_REVISION(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no             IN     ORDHEAD.ORDER_NO%TYPE,
                               I_status               IN     ORDHEAD.STATUS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function Name: CHECK_SHIPMENT_EXISTS
-- Purpose:       This function will check for any outstanding shipments
--                for the order.
--------------------------------------------------------------------------------
FUNCTION CHECK_SHIPMENT_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                               O_err_flag      IN OUT BOOLEAN)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: NEXT_TAX_EVENT_ID
-- Purpose:       This function will get the tax_event_id for tax break up
--------------------------------------------------------------------------------
FUNCTION NEXT_TAX_EVENT_ID (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_tax_event_id  IN OUT TAX_CALC_EVENT.TAX_EVENT_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: INSERT_ORD_TAX_BREAKUP
-- Purpose:       This function will call the localised packages to insert the
--                records into ord_tax_breakup table for both SYNC and ASYNC
--                tax_event_run_type.run_type
--------------------------------------------------------------------------------
FUNCTION INSERT_ORD_TAX_BREAKUP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name: CANCEL_SHIPMENT
-- Purpose:       This function will cancel all the open shipments of the
--                order when the user does a cancel all item for the order.
--------------------------------------------------------------------------------
FUNCTION CANCEL_SHIPMENT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
-- Function Name:  ADJUST_DSD_ORDER
-- Purpose:        Performs necessary logic to adjust DSD orders
--------------------------------------------------------------------------------
FUNCTION ADJUST_DSD_ORDER(O_error_message IN OUT VARCHAR2,
                          I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                          I_item          IN     ORDLOC.ITEM%TYPE,
                          I_location      IN     ORDLOC.LOCATION%TYPE,
                          I_adjust_qty    IN     ORDLOC.QTY_RECEIVED%TYPE,
                          I_cancel_id     IN     ORDLOC.CANCEL_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------
-- Function Name:  CHECK_SUBMIT_APPROVE_WRP
-- Purpose:        Wrapper function to call CHECK_SUBMIT_APPROVE
--------------------------------------------------------------------------------
FUNCTION CHECK_SUBMIT_APPROVE_WRP(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_invalid_date       IN OUT   VARCHAR2,
                                  O_store_closed_ind   IN OUT   VARCHAR2,
                                  O_store_opened_ind   IN OUT   VARCHAR2,
                                  O_clearance_ind      IN OUT   VARCHAR2,
                                  O_wksht_recs_ind     IN OUT   VARCHAR2,
                                  O_hts_ind            IN OUT   VARCHAR2,
                                  O_valid_lc_ind       IN OUT   VARCHAR2,
                                  O_scale_ind          IN OUT   VARCHAR2,
                                  O_recalc_ind         IN OUT   VARCHAR2,
                                  O_round_ind          IN OUT   VARCHAR2,
                                  O_invalid_items      IN OUT   VARCHAR2,
                                  O_error_collection   IN OUT   NOCOPY OBJ_ERROR_TBL,
                                  I_source             IN       VARCHAR2 DEFAULT NULL,
                                  I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                                  I_new_status         IN       ORDHEAD.STATUS%TYPE,
                                  I_not_before_date    IN       ORDHEAD.NOT_BEFORE_DATE%TYPE,
                                  I_not_after_date     IN       ORDHEAD.NOT_AFTER_DATE%TYPE,
                                  I_supplier           IN       ORDHEAD.SUPPLIER%TYPE,
                                  I_dept               IN       ORDHEAD.DEPT%TYPE,
                                  I_check_loc_status   IN       VARCHAR2 DEFAULT      'N')
RETURN INTEGER;
----------------------------------------------------------------------------------
--Function Name : REJECT_PO_NOTIFICATION
--Purpose       : This function creates notification when a po is rejected 
--                by the approver and is intended to notify the user that created 
--                the po. 
--------------------------------------------------------------------------------
FUNCTION REJECT_PO_NOTIFICATION(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
END ORDER_STATUS_SQL;
/
