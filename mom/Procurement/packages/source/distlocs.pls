create or replace PACKAGE DISTRIBUTE_LOCATION_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: FINISH_ORDER
-- Purpose:       This function will distribute an item by store grade on the basis of quantity,
--                percentage or ratio.It will update the orderloc_wksht table based on  distribution
--                information present in the LOCATION_DIST_TEMP table.
---------------------------------------------------------------------------------------------
FUNCTION FINISH_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_dist_uom_type   IN       ORDLOC_WKSHT.UOP%TYPE,
                      I_dist_uom        IN       ORDLOC_WKSHT.UOP%TYPE,
                      I_purch_uom       IN       ORDLOC_WKSHT.UOP%TYPE,
                      I_distribute_by   IN       VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: FINISH_CONTRACT
-- Purpose:       This function will distribute an item by store grade on the basis of quantity,
--                percentage or ratio.It will update the orderloc_wksht table based on  distribution
--                information present in the LOCATION_DIST_TEMP table.
---------------------------------------------------------------------------------------------
FUNCTION FINISH_CONTRACT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_contract_no     IN       CONTRACT_HEADER.CONTRACT_NO%TYPE,
                         I_distribute_by   IN       VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_LOCATION
-- Purpose:       This function will be used to validate the location .
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_error_type      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_country_id      IN OUT   COUNTRY.COUNTRY_ID%TYPE,
                           I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                           I_row_number      IN       VARCHAR2,
                           I_contract_no     IN       ORDHEAD.CONTRACT_NO%TYPE,
                           I_loc_type        IN       LOCATION_DIST_TEMP.LOC_TYPE%TYPE,
                           I_location        IN       LOCATION_DIST_TEMP.LOCATION%TYPE,
                           I_supplier        IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                           I_physical_wh     IN       V_WH.PHYSICAL_WH%TYPE,
                           I_before_date     IN       DATE,
                           I_after_date      IN       DATE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_STORE_STATUS
-- Purpose:       This function will be used to validate the Store is Active  .
---------------------------------------------------------------------------------------------
FUNCTION CHECK_STORE_STATUS(O_error_message IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_error_type    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_store         IN       STORE.STORE%TYPE ,
                            I_before_date   IN       DATE,
                            I_after_date    IN       DATE)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/