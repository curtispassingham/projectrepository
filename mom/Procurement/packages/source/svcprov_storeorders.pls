CREATE OR REPLACE PACKAGE SVCPROV_STOREORDER AUTHID CURRENT_USER AS

/******************************************************************************
 * PROCEDURE CREATE_LOC_PO_TSF 
 * It will create a PO or Transfer to RMS
 ******************************************************************************/
PROCEDURE CREATE_LOC_PO_TSF(O_serviceOperationStatus    IN OUT "RIB_ServiceOpStatus_REC",
                            O_businessObject            OUT    "RIB_LocPOTsfRef_REC", 
                            I_businessObject            IN     "RIB_LocPOTsfDesc_REC"); 

/******************************************************************************
 * PROCEDURE CREATE_LOC_PO_TSF_DETAIL
 * It will create a PO or Transfer Details to RMS                 
 ******************************************************************************/
PROCEDURE CREATE_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                   I_businessObject         IN     "RIB_LocPOTsfDesc_REC");

 /******************************************************************************
 * PROCEDURE MOD_LOC_PO_TSF
 * It will modify a PO or Transfer to RMS                   
 ******************************************************************************/
PROCEDURE MOD_LOC_PO_TSF(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                         I_businessObject         IN     "RIB_LocPOTsfDesc_REC");

/******************************************************************************
 * PROCEDURE MOD_LOC_PO_TSF_DETAIL
 * It will modify a PO or Transfer Details to RMS                  
 ******************************************************************************/  
PROCEDURE MOD_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                I_businessObject         IN     "RIB_LocPOTsfDesc_REC");

/******************************************************************************
 * PROCEDURE DELETE_LOC_PO_TSF
 * It will Delete a PO or Transfer in RMS.                
 ******************************************************************************/ 
PROCEDURE DELETE_LOC_PO_TSF(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                            I_businessObject         IN     "RIB_LocPOTsfDesc_REC");

/******************************************************************************
 * PROCEDURE DELETE_LOC_PO_TSF_DETAIL
 * It will Delete a PO or Transfer Details to RMS.                   
 ******************************************************************************/
PROCEDURE DELETE_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                   I_businessObject         IN     "RIB_LocPOTsfDesc_REC");

/******************************************************************************

 * PROCEDURE QUERY_LOC_PO_TSF_DEALS
 * It will retrieve Deals Information from RMS     
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_DEALS(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                 O_businessObject         OUT    "RIB_LocPOTsfDealsColDesc_REC",
                                 I_businessObject         IN     "RIB_LocPOTsfDealsCriVo_REC");

/******************************************************************************
 * PROCEDURE QUERY_LOC_PO_TSF_ITEMSALES
 * It will retrieve the Item Sales information from RMS.   
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_ITEMSALES(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                     O_businessObject         OUT    "RIB_LocPOTsfItmSlsColDesc_REC",
                                     I_businessObject         IN     "RIB_LocPOTsfItmSlsCriVo_REC");

/******************************************************************************
 * PROCEDURE QUERY_LOC_PO_TSF_HEADER
 * It will Retrieve header level PO or transfer information from RMS.   
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_HEADER(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                  O_businessObject         OUT    "RIB_LocPOTsfHdrColDesc_REC",
                                  I_businessObject         IN     "RIB_LocPOTsfHdrCriVo_REC");

/******************************************************************************
 * PROCEDURE QUERY_LOC_PO_TSF_DETAIL
 * It will retrieve the header/details of a PO or transfer from RMS.
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                  O_businessObject         OUT    "RIB_LocPOTsfDesc_REC",
                                  I_businessObject         IN     "RIB_LocPOTsfDtlsCriVo_REC");

END SVCPROV_STOREORDER;
/