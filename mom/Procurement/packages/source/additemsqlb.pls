CREATE OR REPLACE PACKAGE BODY ADD_LINE_ITEM_SQL IS
--------------------------------------------------------------------------
UNKNOWN_TRUCK             NUMBER := 3;
NUM_SPLIT_CNST            NUMBER := 2;
LP_SUP_INV_MGMT_ROWID     VARCHAR2(30);
--------------------------------------------------------------------------------------------
FUNCTION FILL_SUP_STRUCT(I_get_sup_info            IN OUT ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                         I_contract                IN     ORDHEAD.CONTRACT_NO%TYPE,
                         I_qc_ind                  IN     ORDHEAD.QC_IND%TYPE,
                         I_terms                   IN     ORDHEAD.TERMS%TYPE,
                         I_freight_terms           IN     ORDHEAD.FREIGHT_TERMS%TYPE,
                         I_payment_method          IN     ORDHEAD.PAYMENT_METHOD%TYPE,
                         I_ship_method             IN     ORDHEAD.SHIP_METHOD%TYPE,
                         I_edi_po_ind              IN     ORDHEAD.EDI_PO_IND%TYPE,
                         I_currency_code           IN     ORDHEAD.CURRENCY_CODE%TYPE,
                         I_pre_mark_ind            IN     ORDHEAD.PRE_MARK_IND%TYPE,
                         I_inv_mgmt_lvl            IN     SUPS.INV_MGMT_LVL%TYPE,
                         I_dept_level_ord          IN     VARCHAR2,
                         I_agent                   IN     ORDHEAD.AGENT%TYPE,
                         I_discharge_port          IN     ORDHEAD.DISCHARGE_PORT%TYPE,
                         I_lading_port             IN     ORDHEAD.LADING_PORT%TYPE,
                         I_seq_no                  IN     NUMBER,
                         I_buyer                   IN     ORDHEAD.BUYER%TYPE,
                         I_min_cnstr_lvl           IN     ORD_INV_MGMT.MIN_CNSTR_LVL%TYPE,
                         I_scale_cnstr_ind         IN     ORD_INV_MGMT.SCALE_CNSTR_IND%TYPE,
                         I_truck_split_ind         IN     ORD_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
                         I_truck_method            IN     ORD_INV_MGMT.TRUCK_SPLIT_METHOD%TYPE,
                         I_cnst_type1              IN     ORD_INV_MGMT.MIN_CNSTR_TYPE1%TYPE,
                         I_cnst_uom1               IN     ORD_INV_MGMT.MIN_CNSTR_UOM1%TYPE,
                         I_cnst_value1             IN     ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE,
                         I_cnst_threshold1         IN     VARCHAR2,
                         I_cnst_type2              IN     ORD_INV_MGMT.MIN_CNSTR_TYPE2%TYPE,
                         I_cnst_uom2               IN     ORD_INV_MGMT.MIN_CNSTR_UOM2%TYPE,
                         I_cnst_value2             IN     ORD_INV_MGMT.MIN_CNSTR_VAL2%TYPE,
                         I_cnst_threshold2         IN     NUMBER,
                         I_single_loc_ind          IN     ORD_INV_MGMT.SINGLE_LOC_IND%TYPE,
                         I_due_ord_process_ind     IN     ORD_INV_MGMT.DUE_ORD_IND%TYPE,
                         I_due_ord_lvl             IN     ORD_INV_MGMT.DUE_ORD_LVL%TYPE,
                         I_non_due_ord_create_ind  IN     VARCHAR2,
                         I_purchase_type           IN     ORDHEAD.PURCHASE_TYPE%TYPE,
                         I_pickup_loc              IN     ORDHEAD.PICKUP_LOC%TYPE,
                         I_partner_type_1          IN     ORDHEAD.PARTNER_TYPE_1%TYPE,
                         I_partner_1               IN     ORDHEAD.PARTNER1%TYPE,
                         I_partner_type_2          IN     ORDHEAD.PARTNER_TYPE_2%TYPE,
                         I_partner_2               IN     ORDHEAD.PARTNER2%TYPE,
                         I_partner_type_3          IN     ORDHEAD.PARTNER_TYPE_3%TYPE,
                         I_partner_3               IN     ORDHEAD.PARTNER3%TYPE,
                         I_factory                 IN     ORDHEAD.FACTORY%TYPE,
                         I_sup_inv_mgmt_rowid      IN     VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION ADD_ITEM(I_ordsku_tbl          IN OUT ADD_LINE_ITEM_SQL.ORDSKU_TBL,
                  I_get_cur_rec         IN OUT ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC,
                  I_due_process_flag    IN       VARCHAR2,
                  I_loop_flag           IN OUT NUMBER,
                  O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
FUNCTION ADD_LOC(I_ordloc_tbl          IN OUT ADD_LINE_ITEM_SQL.ORDLOC_TBL,
                 I_due_process_flag    IN     VARCHAR2,
                 I_get_cur_rec         IN OUT ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC,
                 O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------
/* Retrieve some system level parameters into global variables. */
FUNCTION SET_GLOBALS(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(30) := 'ADD_LINE_ITEM_SQL.SET_GLOBALS';

   CURSOR c_set_globals IS
      SELECT p.vdate,
             NVL(so.repl_order_days, 0),
             so.elc_ind,
             so.alloc_method,
             so.base_country_id,
             NVL(so.latest_ship_days, 0),
             NVL(so.fob_title_pass, '-1'),
             NVL(so.fob_title_pass_desc, 'NONE'),
             NVL(so.max_scaling_iterations, 0),
             so.ord_appr_amt_code,
             so.import_ind
        FROM system_options so,
             period p;

BEGIN

   /* populate global variables */
   open C_SET_GLOBALS;
   fetch C_SET_GLOBALS INTO ADD_LINE_ITEM_SQL.LP_vdate,
                            ADD_LINE_ITEM_SQL.LP_repl_order_days,
                            ADD_LINE_ITEM_SQL.LP_elc_ind,
                            ADD_LINE_ITEM_SQL.LP_alloc_method,
                            ADD_LINE_ITEM_SQL.LP_base_country_id,
                            ADD_LINE_ITEM_SQL.LP_latest_ship_days,
                            ADD_LINE_ITEM_SQL.LP_fob_title_pass,
                            ADD_LINE_ITEM_SQL.LP_fob_title_pass_desc,
                            ADD_LINE_ITEM_SQL.LP_max_scale_iterations,
                            ADD_LINE_ITEM_SQL.LP_ord_appr_amt_code,
                            ADD_LINE_ITEM_SQL.LP_import_ind;
   if C_SET_GLOBALS%NOTFOUND then
      SQL_LIB.SET_MARK('fetch',
                       'C_SET_GLOBALS',
                       'system_options, period',
                       NULL);
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM(SQLCODE),
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
   end if;
   close C_SET_GLOBALS;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END SET_GLOBALS; /*end set_globals*/


/**********************************************************************
***********************************************************************
*             BEGIN TABLE BUILDING FUNCTIONS                          *
***********************************************************************
**********************************************************************/

/*
 * This function will add the item/location to the current ordering table type
 * or if no records exist, create a new record to hold the item location.
 *
 * This function loops on the current ordhead table until it is empty.  If the item/loc
 * cannot be added to any of the existing orders in the table( by add_item() ), a new
 * order is added to the table to hold the item/loc.  The new orders are always
 * added at the end of the order table type (make the first order as big as possible to keep
 * ordering group together).
 *
 * Logic to determine if/how an order will be created is held here
 *   If there is a IB line item on the order
 *      the IB flag is turned on
 *      the ordhead write flag is turned on
 *      the order due flag is turned on
 *      the item/loc due flag is turned on
 *
 *   If due order processing is on and the level is ORDER
 *      the ordhead write flag is turned on when the total eso > total aso
 *      the order due flag is turned on when the total eso > total aso
 *      the item/loc due flag is turned on when one item/loc has a eso > aso
 *
 *   If due order processing is on and the level is LOCATION
 *      the ordhead write flag is turned on when the item/loc due flag is on
 *      the order due flag is turned on when the item/loc due flag is on
 *      the item/loc due flag is turned on when one item/loc has a eso > aso
 *
 * Each time a new item/loc is added to an order, the above flags are updated if needed.
 * Other fields like aso, eso, lead_time, and import_ind are also updated.
 *
 * Certain order splitting functionality is done here or in its helper functions.
 *   If supplier, pool_supplier, contract, or order status are different the
 *     line items cannot go on the same order.
 *   If dept level ordering is on a dept will be populated or ordhead.  The dept will
 *     be of the first item added to that ordhead record.  After that, only items with the
 *     same dept will be placed on that order.
 *   If single loc ordering is on (only one loc per/order).  The first WH added to the
 *     order will be written to ordhead record.  After that only stores or records
 *     going to the same WH will be added to that ordhead.  This is done at the
 *     physical_wh level.
 */
--------------------------------------------------------------------------------------
FUNCTION ADD_TO_ORDER(I_ordhead_tbl        IN OUT  ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                      I_get_cur_info       IN OUT  ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC,
                      I_get_sup_info       IN      ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                      I_order_no           IN      ORDHEAD.ORDER_NO%TYPE,
                      O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program       VARCHAR2(30) := 'ADD_LINE_ITEM_SQL.ADD_TO_ORDER';

   L_loop_flag     NUMBER := 1;
   L_order_no      VARCHAR2(10);

   /* std plsql vars */
   L_failed        NUMBER := 0;
   L_exception     NUMBER := 0;
   L_err_message   VARCHAR2(2000);
   L_sqlcode       VARCHAR2(20) := 0;

   L_ctr           NUMBER := 0;
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   L_table         VARCHAR2(30) := 'ORDHEAD';
   L_sql_code      VARCHAR2(30) := SQLCODE;
   L_idx           BINARY_INTEGER := 0;

BEGIN
   L_idx := I_ordhead_tbl.COUNT;
   if L_idx = 0 then
      L_idx := L_idx + 1;
   end if;
   while (L_loop_flag <> 0) LOOP
      
      if L_idx > I_ordhead_tbl.COUNT then /* need a new order to be added to tbl */
         /* if 1st line item is IB and days to event is > 0,
            don't create the order */
         if I_get_cur_info.source_type = 'IB' AND I_get_cur_info.ib_days_to_event > 0 then
            return TRUE;
         end if;

         if I_order_no IS NOT NULL then
            L_order_no := I_order_no;
         else
            if NOT ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER(O_error_message,
                                                      L_order_no) then
               return FALSE;
            end if;
         end if;

         /* fill the new ordhead record */

         I_ordhead_tbl(L_idx).order_no := L_order_no;

         I_ordhead_tbl(L_idx).supplier := I_get_cur_info.supplier;

         I_ordhead_tbl(L_idx).pool_supp := I_get_cur_info.pool_supp;

         I_ordhead_tbl(L_idx).file_id := I_get_cur_info.file_id;

         I_ordhead_tbl(L_idx).contract := I_get_cur_info.contract;

         I_ordhead_tbl(L_idx).contract_type := I_get_cur_info.contract_type;

         I_ordhead_tbl(L_idx).contract_terms := I_get_cur_info.contract_terms;
         I_ordhead_tbl(L_idx).contract_approval_ind := I_get_cur_info.contract_approval_ind;
         I_ordhead_tbl(L_idx).contract_header_rowid := I_get_cur_info.contract_header_rowid;
         I_ordhead_tbl(L_idx).order_status := I_get_cur_info.order_status;
         I_ordhead_tbl(L_idx).import_country_id := I_get_cur_info.loc_country;
         I_ordhead_tbl(L_idx).aso := I_get_cur_info.aso;
         I_ordhead_tbl(L_idx).eso := I_get_cur_info.eso;

         I_ordhead_tbl(L_idx).min_supp_lead_time   := I_get_cur_info.supp_lead_time;
         I_ordhead_tbl(L_idx).max_supp_lead_time   := I_get_cur_info.supp_lead_time;
         I_ordhead_tbl(L_idx).min_pickup_lead_time := I_get_cur_info.pickup_lead_time;
         I_ordhead_tbl(L_idx).max_pickup_lead_time := I_get_cur_info.pickup_lead_time;

         if I_get_cur_info.source_type = 'IB' then
            I_ordhead_tbl(L_idx).ib_flag := 'Y';
            I_ordhead_tbl(L_idx).sup_info.due_ord_ind := 'Y';
            I_ordhead_tbl(L_idx).write_ind := 'Y';
            I_ordhead_tbl(L_idx).itemloc_due_ind := 'Y';
         else /* source type not IB */
            I_ordhead_tbl(L_idx).ib_flag := 'N';

            if I_get_cur_info.due_ind = 'Y' then
               I_ordhead_tbl(L_idx).sup_info.due_ord_ind := 'Y';
               I_ordhead_tbl(L_idx).write_ind := 'Y';
               I_ordhead_tbl(L_idx).itemloc_due_ind := 'Y';
            else
               I_ordhead_tbl(L_idx).sup_info.due_ord_ind := 'N';
               if I_get_sup_info.non_due_ord_create_ind = 'Y' then
                  I_ordhead_tbl(L_idx).write_ind := 'Y';
               else
                  I_ordhead_tbl(L_idx).write_ind := 'N';
               end if;
               I_ordhead_tbl(L_idx).itemloc_due_ind := 'N';
            end if;
         end if; /* source type */

         I_ordhead_tbl(L_idx).sup_info.qc_ind := I_get_sup_info.qc_ind;
         I_ordhead_tbl(L_idx).sup_info.terms := I_get_sup_info.terms;
         I_ordhead_tbl(L_idx).sup_info.freight_terms := I_get_sup_info.freight_terms;
         I_ordhead_tbl(L_idx).sup_info.payment_method := I_get_sup_info.payment_method;
         I_ordhead_tbl(L_idx).sup_info.ship_method := I_get_sup_info.ship_method;
         I_ordhead_tbl(L_idx).sup_info.edi_po_ind := I_get_sup_info.edi_po_ind;
         I_ordhead_tbl(L_idx).sup_info.currency_code := I_get_sup_info.currency_code;
         I_ordhead_tbl(L_idx).sup_info.pre_mark_ind := I_get_sup_info.pre_mark_ind;
         I_ordhead_tbl(L_idx).sup_info.agent := I_get_sup_info.agent;

         I_ordhead_tbl(L_idx).sup_info.discharge_port := I_get_sup_info.discharge_port;
         I_ordhead_tbl(L_idx).sup_info.lading_port := I_get_sup_info.lading_port;
         I_ordhead_tbl(L_idx).sup_info.seq_no := I_get_sup_info.seq_no;
         I_ordhead_tbl(L_idx).sup_info.buyer := I_get_sup_info.buyer;
         I_ordhead_tbl(L_idx).sup_info.single_loc_ind := I_get_sup_info.single_loc_ind;
         I_ordhead_tbl(L_idx).sup_info.due_ord_process_ind := I_get_sup_info.due_ord_process_ind;

         I_ordhead_tbl(L_idx).sup_info.due_ord_lvl := I_get_sup_info.due_ord_lvl;
         I_ordhead_tbl(L_idx).sup_info.non_due_ord_create_ind := I_get_sup_info.non_due_ord_create_ind;
         I_ordhead_tbl(L_idx).sup_info.purchase_type := I_get_sup_info.purchase_type;
         I_ordhead_tbl(L_idx).sup_info.pickup_loc := I_get_sup_info.pickup_loc;
         I_ordhead_tbl(L_idx).sup_info.sup_inv_mgmt_rowid := I_get_sup_info.sup_inv_mgmt_rowid;
         I_ordhead_tbl(L_idx).sup_info.ord_inv_mgmt_rowid := I_get_sup_info.ord_inv_mgmt_rowid;
         I_ordhead_tbl(L_idx).sup_info.min_cnstr_lvl := I_get_sup_info.min_cnstr_lvl;
         I_ordhead_tbl(L_idx).sup_info.dept_level_ord := I_get_sup_info.dept_level_ord;
         I_ordhead_tbl(L_idx).sup_info.ltl_flag := UNKNOWN_TRUCK;

         I_ordhead_tbl(L_idx).sup_info.partner_type_1 := I_get_sup_info.partner_type_1;
         I_ordhead_tbl(L_idx).sup_info.partner_1 := I_get_sup_info.partner_1;
         I_ordhead_tbl(L_idx).sup_info.partner_type_2 := I_get_sup_info.partner_type_2;
         I_ordhead_tbl(L_idx).sup_info.partner_2 := I_get_sup_info.partner_2;
         I_ordhead_tbl(L_idx).sup_info.partner_type_3 := I_get_sup_info.partner_type_3;
         I_ordhead_tbl(L_idx).sup_info.partner_3 := I_get_sup_info.partner_3;
         I_ordhead_tbl(L_idx).sup_info.factory := I_get_sup_info.factory;

         /* if this is a single dept order, populate the header level dept
            field with the dept of the item */
         I_ordhead_tbl(L_idx).sup_info.inv_mgmt_lvl := I_get_sup_info.inv_mgmt_lvl;
         if I_get_sup_info.dept_level_ord = 'Y' then
             I_ordhead_tbl(L_idx).dept  := I_get_cur_info.dept;
         else
             I_ordhead_tbl(L_idx).dept := '-1';
         end if;

         /* if single location ordering is on and populate the ordhead loc field */
         if I_ordhead_tbl(L_idx).sup_info.single_loc_ind = 'Y' then
            if I_get_cur_info.phy_loc <> '-1' then
               I_ordhead_tbl(L_idx).order_loc := I_get_cur_info.phy_loc;
            else
               I_ordhead_tbl(L_idx).order_loc := I_get_cur_info.loc;
            end if;
            I_ordhead_tbl(L_idx).order_loc_type := I_get_cur_info.loc_type;
         else
            I_ordhead_tbl(L_idx).order_loc := NULL;
            I_ordhead_tbl(L_idx).order_loc_type := NULL;
         end if;

         /* if the base import country is different from this record's import country
             this is an import order */
         if I_get_cur_info.loc_country <> I_get_cur_info.ctry then
            I_ordhead_tbl(L_idx).import_order := 'Y';
         else
            I_ordhead_tbl(L_idx).import_order := 'N';
         end if;

         /* deal with truck splitting specific variables */
         I_ordhead_tbl(L_idx).split_ref_ord_no := I_get_cur_info.split_ref_ord_no;
         I_ordhead_tbl(L_idx).sup_info.ltl_flag := I_get_sup_info.ltl_flag;
         I_ordhead_tbl(L_idx).sup_info.cnst_index := I_get_sup_info.cnst_index;
         I_ordhead_tbl(L_idx).sup_info.scale_cnstr_ind := I_get_sup_info.scale_cnstr_ind;
         I_ordhead_tbl(L_idx).sup_info.truck_split_ind := I_get_sup_info.truck_split_ind;
         I_ordhead_tbl(L_idx).sup_info.truck_method := I_get_sup_info.truck_method;
         
         for l_ctr in 1..NUM_SPLIT_CNST
         LOOP
            I_ordhead_tbl(L_idx).sup_info.cnst_type(l_ctr) := I_get_sup_info.cnst_type(l_ctr);
            I_ordhead_tbl(L_idx).sup_info.cnst_uom(l_ctr) := I_get_sup_info.cnst_uom(l_ctr);
            I_ordhead_tbl(L_idx).sup_info.cnst_value(l_ctr) := I_get_sup_info.cnst_value(l_ctr);
            I_ordhead_tbl(L_idx).sup_info.cnst_threshold(l_ctr) := I_get_sup_info.cnst_threshold(l_ctr);
            I_ordhead_tbl(L_idx).sup_info.total_cnst_qty(l_ctr) := 0;
            I_ordhead_tbl(L_idx).sup_info.cnst_value_reset(l_ctr) := I_get_sup_info.cnst_value_reset(l_ctr);
         END LOOP; 
         I_ordhead_tbl(L_idx).sup_info.cnst_index := I_get_sup_info.cnst_index;

         /* add the item and loc to the new order */
         if add_item(I_ordhead_tbl(L_idx).ordsku_tbl,
                     I_get_cur_info,
                     I_get_sup_info.due_ord_process_ind,
                     L_loop_flag,
                     O_error_message) = FALSE then
            return FALSE;
         end if;

         return TRUE;
      else /* try to add the item/ctry/loc to the current order if possible */
            /*
            must have same supplier, pool_supplier, contract, and order status
            AND
            if we are splitting by dept and the depts are the same or
            if we are not splitting by dept, try to add the item/location
            to the current order.
            AND
            if single location ordering is on, make sure each LOC is
            placed on a distinct order.
            AND
            if the new line item is ib with a dte of > 0 and the existing order
            is not ib and not going to be written, don't add the new line item.
            */
 
         if (
              ( I_ordhead_tbl(L_idx).supplier = I_get_cur_info.supplier AND
                NVL(I_ordhead_tbl(L_idx).pool_supp,-1) = NVL(I_get_cur_info.pool_supp,-1) AND
                I_ordhead_tbl(L_idx).order_status = I_get_cur_info.order_status AND
                NVL(I_ordhead_tbl(L_idx).contract,-1) = NVL(I_get_cur_info.contract,-1)
              )
             AND
              (
                I_get_sup_info.dept_level_ord = 'N' OR
                I_ordhead_tbl(L_idx).dept = I_get_cur_info.dept
              )
             AND
              (
                I_get_sup_info.single_loc_ind = 'N'
                OR
                I_ordhead_tbl(L_idx).order_loc = I_get_cur_info.phy_loc
              )
             AND
              (
                 I_get_cur_info.source_type = 'IB'
                OR
                 (
                  I_ordhead_tbl(L_idx).ib_flag = 'Y' OR
                  I_get_cur_info.ib_days_to_event = 0 OR
                  I_ordhead_tbl(L_idx).write_ind = 'Y'
                  )
              )
            ) then
            /* add the item and loc to the new order */

            if add_item(I_ordhead_tbl(L_idx).ordsku_tbl,
                        I_get_cur_info,
                        I_get_sup_info.due_ord_process_ind,
                        L_loop_flag,
                        O_error_message) = FALSE then
               return FALSE;
            end if;
         end if;
      end if; /* end else */
     /* move to next order */

      if (L_loop_flag <> 0) then
         L_idx := L_idx + 1;

      /* else added item/loc to the current order, update the min and max lead times
         check if we are now an import order, update the write flag and sum the
         eso and aso */
      else
         if ((I_get_cur_info.supp_lead_time < I_ordhead_tbl(L_idx).min_supp_lead_time) AND
             (I_get_cur_info.supp_lead_time <> -1)) then
            I_ordhead_tbl(L_idx).min_supp_lead_time := I_get_cur_info.supp_lead_time;
         end if;
         if (I_get_cur_info.supp_lead_time > I_ordhead_tbl(L_idx).max_supp_lead_time) then
            I_ordhead_tbl(L_idx).max_supp_lead_time := I_get_cur_info.supp_lead_time;
         end if;

         if ((I_get_cur_info.pickup_lead_time < I_ordhead_tbl(L_idx).min_pickup_lead_time) AND
             (I_get_cur_info.pickup_lead_time <> -1)) then
            I_ordhead_tbl(L_idx).min_pickup_lead_time := I_get_cur_info.pickup_lead_time;
         end if;

         if (I_get_cur_info.pickup_lead_time > I_ordhead_tbl(L_idx).max_pickup_lead_time) then
            I_ordhead_tbl(L_idx).max_pickup_lead_time := I_get_cur_info.pickup_lead_time;
         end if;

         if (I_get_cur_info.loc_country <> I_get_cur_info.ctry) then
            I_ordhead_tbl(L_idx).import_order := 'Y';
         end if;
         /* sum up the aso and eso for all item locations on the order
            they will determine if the order is due if the due order
            level is set to 'O'rder */
         I_ordhead_tbl(L_idx).aso := I_ordhead_tbl(L_idx).aso + I_get_cur_info.aso;
         I_ordhead_tbl(L_idx).eso := I_ordhead_tbl(L_idx).eso + I_get_cur_info.eso;

        /* set the ordering flags */
        if I_get_cur_info.source_type = 'IB' OR
           I_ordhead_tbl(L_idx).ib_flag = 'Y' then
           I_ordhead_tbl(L_idx).sup_info.due_ord_ind := 'Y';
           I_ordhead_tbl(L_idx).write_ind := 'Y';
           I_ordhead_tbl(L_idx).itemloc_due_ind := 'Y';
           I_ordhead_tbl(L_idx).ib_flag := 'Y';
        else /* not investment buy */
           if I_get_cur_info.due_ind = 'Y' then
              I_ordhead_tbl(L_idx).sup_info.due_ord_ind := 'Y';
              I_ordhead_tbl(L_idx).write_ind := 'Y';
           elsif I_ordhead_tbl(L_idx).sup_info.non_due_ord_create_ind = 'Y' then
              I_ordhead_tbl(L_idx).write_ind := 'Y';
          end if;
        end if;
      end if; /* end update order added to */
   END LOOP; /* end while */

return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END ADD_TO_ORDER; /*end add_to_order*/
--------------------------------------------------------------------------------------
/*
 * This function takes in a item record and checks if the current item is in the
 * record.  If the item is in the item record with the same origin country, the add_loc()
 * function is called.  If the item is in the record with a different origin country,
 * control is returned to add_to_order() with a flag set telling it to move to
 * the next order.  A item can only have one origin country per order.  If the item
 * is not in the item record, it is added.
 * Logic also determines if the item needs to be written to the ordering tables.
 */
--------------------------------------------------------------------------------------
FUNCTION ADD_ITEM(I_ordsku_tbl          IN OUT ADD_LINE_ITEM_SQL.ORDSKU_TBL,
                  I_get_cur_rec         IN OUT ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC,
                  I_due_process_flag    IN     VARCHAR2,
                  I_loop_flag           IN OUT NUMBER,
                  O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program      VARCHAR2(30):= 'ADD_LINE_ITEM_SQL.ADD_ITEM';

   L_sku_ctr      NUMBER := 0;

BEGIN
   L_sku_ctr := I_ordsku_tbl.COUNT;
   if L_sku_ctr = 0 then
      L_sku_ctr := L_sku_ctr + 1;
   end if;
   LOOP
   if L_sku_ctr > I_ordsku_tbl.COUNT then
      I_ordsku_tbl(L_sku_ctr).item := I_get_cur_rec.item;
      I_ordsku_tbl(L_sku_ctr).ctry := I_get_cur_rec.ctry;
      I_ordsku_tbl(L_sku_ctr).pack_size := I_get_cur_rec.pack_size;
      I_ordsku_tbl(L_sku_ctr).non_scale_ind := I_get_cur_rec.non_scale_ind;

      /* if there is qty to order we need to write an ordsku record
         when the due_process_flag is YES */
      if I_due_process_flag = 'Y' then
         if (I_get_cur_rec.qty > 0) then
            I_ordsku_tbl(L_sku_ctr).write_ind := 'Y';
         else
            I_ordsku_tbl(L_sku_ctr).write_ind := 'N';
         end if;
      else /* due_process_flag = NO */
      /* if there the item/loc due flag is yes, we need an ordsku record */
         if I_get_cur_rec.due_ind = 'Y' then
            I_ordsku_tbl(L_sku_ctr).write_ind := 'Y';
         else
            I_ordsku_tbl(L_sku_ctr).write_ind := 'N';
         end if;
      end if;

      I_ordsku_tbl(L_sku_ctr).min_supp_lead_time := I_get_cur_rec.supp_lead_time;
      I_ordsku_tbl(L_sku_ctr).max_supp_lead_time := I_get_cur_rec.supp_lead_time;

      /* splitting specific variables */
      I_ordsku_tbl(L_sku_ctr).splitting_increment := I_get_cur_rec.splitting_increment;
      I_ordsku_tbl(L_sku_ctr).round_pct := I_get_cur_rec.round_pct;
      I_ordsku_tbl(L_sku_ctr).model_qty := I_get_cur_rec.model_qty;
       
      L_sku_ctr := I_ordsku_tbl.count; 
      I_loop_flag := 0;
      EXIT;
   end if;
   /* item is in record with diff ctry, leave this function and try next order */
   if I_ordsku_tbl(L_sku_ctr).item = I_get_cur_rec.item AND I_ordsku_tbl(L_sku_ctr).ctry <> I_get_cur_rec.ctry then
      EXIT;
   end if;
   /* item is in record with same ctry, just add the location */
   if I_ordsku_tbl(L_sku_ctr).item = I_get_cur_rec.item AND I_ordsku_tbl(L_sku_ctr).ctry = I_get_cur_rec.ctry then

      /* if there is qty to order we need to write an ordsku record */
      if I_due_process_flag = 'Y' then
         if (I_get_cur_rec.qty > 0) then
            I_ordsku_tbl(L_sku_ctr).write_ind := 'Y';
         end if;
      else /* due_ord_process_flag is NO */
         if I_get_cur_rec.due_ind = 'Y' then
            I_ordsku_tbl(L_sku_ctr).write_ind := 'Y';
         end if;
      end if;

      if (I_get_cur_rec.supp_lead_time < I_ordsku_tbl(L_sku_ctr).min_supp_lead_time) AND
          (I_get_cur_rec.supp_lead_time != -1) then
         I_ordsku_tbl(L_sku_ctr).min_supp_lead_time := I_get_cur_rec.supp_lead_time;
      end if;
      if (I_get_cur_rec.supp_lead_time > I_ordsku_tbl(L_sku_ctr).max_supp_lead_time) then
         I_ordsku_tbl(L_sku_ctr).max_supp_lead_time := I_get_cur_rec.supp_lead_time;
      end if;
      I_loop_flag := 0;
      EXIT;
      end if;
      L_sku_ctr := L_sku_ctr + 1;
   END LOOP; 
  
   if (I_loop_flag = 0) then
      if add_loc(I_ordsku_tbl(L_sku_ctr).ordloc_tbl,
                 I_due_process_flag,
                 I_get_cur_rec,
                 O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
       O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ADD_ITEM; /*end add_item*/

/*
 * This function takes in a loc table type and adds an item/location to it.  If the loc
 * already exist in the table, its qty is incremented.  If the loc is not in the
 * table it is added.
 */
--------------------------------------------------------------------------------------
FUNCTION ADD_LOC(I_ordloc_tbl          IN OUT ADD_LINE_ITEM_SQL.ORDLOC_TBL,
                 I_due_process_flag    IN     VARCHAR2,
                 I_get_cur_rec         IN OUT ADD_LINE_ITEM_SQL.DRIV_CUR_INFO_PROCESS_REC,
                 O_error_message       IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(30):= 'ADD_LINE_ITEM_SQL.ADD_LOC';

   L_added        NUMBER := 0;

   L_cost_source  VARCHAR2(6);
   L_loc_ctr      BINARY_INTEGER := 0;
   
BEGIN
   L_loc_ctr := I_ordloc_tbl.COUNT;
   if L_loc_ctr = 0 then
      L_loc_ctr := L_loc_ctr + 1;
   end if;
   LOOP
      if L_loc_ctr > I_ordloc_tbl.COUNT then 
         if (L_added <> 0) then 
            EXIT;
         end if;
         
         I_ordloc_tbl(L_loc_ctr).source_type  := I_get_cur_rec.source_type;
         I_ordloc_tbl(L_loc_ctr).loc          := I_get_cur_rec.loc;
         I_ordloc_tbl(L_loc_ctr).phy_loc      := I_get_cur_rec.phy_loc;
         I_ordloc_tbl(L_loc_ctr).repl_wh_link := I_get_cur_rec.repl_wh_link;
         I_ordloc_tbl(L_loc_ctr).loc_type     := I_get_cur_rec.loc_type;
         I_ordloc_tbl(L_loc_ctr).due_ind      := I_get_cur_rec.due_ind;
         I_ordloc_tbl(L_loc_ctr).item         := I_get_cur_rec.item;
         I_ordloc_tbl(L_loc_ctr).pack_ind     := I_get_cur_rec.pack_ind;
         I_ordloc_tbl(L_loc_ctr).xdock_ind    := I_get_cur_rec.xdock_ind;
         I_ordloc_tbl(L_loc_ctr).xdock_store  := I_get_cur_rec.xdock_store;
         I_ordloc_tbl(L_loc_ctr).tsf_po_link  := I_get_cur_rec.tsf_po_link;
         I_ordloc_tbl(L_loc_ctr).rr_rowid     := I_get_cur_rec.rr_rowid;
         I_ordloc_tbl(L_loc_ctr).ir_rowid     := I_get_cur_rec.ir_rowid;
         I_ordloc_tbl(L_loc_ctr).bw_rowid     := I_get_cur_rec.bw_rowid;
       
         I_ordloc_tbl(L_loc_ctr).non_scale_ind := I_get_cur_rec.non_scale_ind;
         I_ordloc_tbl(L_loc_ctr).unit_cost := I_get_cur_rec.unit_cost;
         I_ordloc_tbl(L_loc_ctr).unit_cost_init := I_get_cur_rec.unit_cost_init;
         if I_ordloc_tbl(L_loc_ctr).unit_cost <> I_ordloc_tbl(L_loc_ctr).unit_cost_init then
            I_ordloc_tbl(L_loc_ctr).cost_source := 'MANL';
         elsif I_get_cur_rec.contract <> '-1' then
            I_ordloc_tbl(L_loc_ctr).cost_source := 'CONT';
         else
            I_ordloc_tbl(L_loc_ctr).cost_source := 'NORM';
         end if;
       
         I_ordloc_tbl(L_loc_ctr).qty := I_get_cur_rec.qty;
         I_ordloc_tbl(L_loc_ctr).last_rounded_qty := I_get_cur_rec.last_rounded_qty;
       
         /* This qty already represents the sum of associated line items */
         I_ordloc_tbl(L_loc_ctr).last_grp_rounded_qty := I_get_cur_rec.last_grp_rounded_qty;
       
     
         /* splitting specific variables */
         for l_ctr in 1..NUM_SPLIT_CNST
         LOOP
            I_ordloc_tbl(L_loc_ctr).cnst_qty(l_ctr) := I_get_cur_rec.cnst_qty(l_ctr);
            I_ordloc_tbl(L_loc_ctr).cnst_map(l_ctr) := I_get_cur_rec.cnst_map(l_ctr);
         END LOOP; 
     
         if I_due_process_flag = 'Y' then
            /* if there is qty to order we need to write an ordsku record */
            if (I_get_cur_rec.qty > 0) then
               I_ordloc_tbl(L_loc_ctr).write_ind := 'Y';
            else
               I_ordloc_tbl(L_loc_ctr).write_ind := 'N';
            end if;
         else /* due_process_flag = NO */
            /* special case - if this is a xdock total record and
               it's qty is greater than zero, it will be written */
            if I_ordloc_tbl(L_loc_ctr).xdock_ind = 'Y' AND
                 I_ordloc_tbl(L_loc_ctr).xdock_store = '-1' then
               /* if there is qty to order we need to write an ordsku record */
               if (I_get_cur_rec.qty > 0) then
                  I_ordloc_tbl(L_loc_ctr).write_ind := 'Y';
               else /* qty is <= 0 */
                  I_ordloc_tbl(L_loc_ctr).write_ind := 'N';
               end if;
            else /* not a xdock total record go by the due flag */
               /* if there the due flag is yes, we need an ordsku record */
               if I_get_cur_rec.due_ind = 'Y' then
                  I_ordloc_tbl(L_loc_ctr).write_ind := 'Y';
               else
                  I_ordloc_tbl(L_loc_ctr).write_ind := 'N';
               end if;
            end if;
         end if; --due_process_flag
         L_loc_ctr := I_ordloc_tbl.COUNT;
         EXIT;
      end if; 

      /* loc is the same, increment the qty for that loc */
      if I_ordloc_tbl(L_loc_ctr).loc = I_get_cur_rec.loc AND
         I_ordloc_tbl(L_loc_ctr).xdock_store = I_get_cur_rec.xdock_store then
         /* existing +, new +, add together */

         if ((I_get_cur_rec.qty >= 0) AND (I_ordloc_tbl(L_loc_ctr).qty >= 0)) then

            I_ordloc_tbl(L_loc_ctr).qty := I_ordloc_tbl(L_loc_ctr).qty + I_get_cur_rec.qty;
            I_ordloc_tbl(L_loc_ctr).last_rounded_qty := I_ordloc_tbl(L_loc_ctr).last_rounded_qty + I_get_cur_rec.last_rounded_qty;

            for l_ctr IN 1..NUM_SPLIT_CNST LOOP
               I_ordloc_tbl(L_loc_ctr).cnst_qty(l_ctr) := I_ordloc_tbl(L_loc_ctr).cnst_qty(l_ctr) + I_get_cur_rec.cnst_qty(l_ctr);
            END LOOP; 
         /* existing -, new +, use new */
         elsif ((I_get_cur_rec.qty >= 0) AND (I_ordloc_tbl(L_loc_ctr).qty < 0)) then
            I_ordloc_tbl(L_loc_ctr).qty := I_get_cur_rec.qty;
            I_ordloc_tbl(L_loc_ctr).last_rounded_qty := I_get_cur_rec.last_rounded_qty;

            for l_ctr IN 1..NUM_SPLIT_CNST LOOP
               I_ordloc_tbl(L_loc_ctr).cnst_qty(l_ctr) := I_get_cur_rec.cnst_qty(l_ctr);
            END LOOP;
         end if;
         /* existing +, new -, use existing */
            /* do nothing */
         /* existing -, new -, use direct to wh record, this will be existing
            (see ordering in driving cursor) */
            /* do nothing */

         /* if there is qty to order we need to write an ordsku record */
         if I_due_process_flag = 'Y' then
            if (I_get_cur_rec.qty > 0) then
               I_ordloc_tbl(L_loc_ctr).write_ind := 'Y';
            end if;
         else /* due_ord_process_flag is NO */
            if I_get_cur_rec.due_ind = 'Y' then
               I_ordloc_tbl(L_loc_ctr).write_ind := 'Y';
            end if;
         end if;

         /* this takes care of non-seg inv ib roq cannot be scaled */
         if I_get_cur_rec.non_scale_ind = 'Y' then
            I_ordloc_tbl(L_loc_ctr).non_scale_ind := I_get_cur_rec.non_scale_ind;
         end if;

         if I_get_cur_rec.xdock_ind = 'Y' then
            I_ordloc_tbl(L_loc_ctr).xdock_ind := I_get_cur_rec.xdock_ind;
         end if;

         if I_get_cur_rec.rr_rowid IS NULL then
            I_ordloc_tbl(L_loc_ctr).rr_rowid := I_get_cur_rec.rr_rowid;
         end if;
         if I_get_cur_rec.ir_rowid IS NOT NULL then
            I_ordloc_tbl(L_loc_ctr).ir_rowid := I_get_cur_rec.ir_rowid;
         end if;
         if I_get_cur_rec.bw_rowid IS NOT NULL then
            I_ordloc_tbl(L_loc_ctr).bw_rowid := I_get_cur_rec.bw_rowid;
         end if;
         if I_get_cur_rec.unit_cost <> I_get_cur_rec.unit_cost_init then
            L_cost_source := 'MANL';
         elsif I_get_cur_rec.contract <> '-1' then
            L_cost_source := 'CONT';
         else
            L_cost_source := 'NORM';
         end if;

         /* if cost source is not contract */
         if L_cost_source <> 'CONT' then
            /* if cost sources are the same take the lowest */
            if I_ordloc_tbl(L_loc_ctr).cost_source = L_cost_source then
               if TO_NUMBER(I_get_cur_rec.unit_cost) < TO_NUMBER(I_ordloc_tbl(L_loc_ctr).unit_cost) then
                  I_ordloc_tbl(L_loc_ctr).unit_cost:= I_get_cur_rec.unit_cost;
                  I_ordloc_tbl(L_loc_ctr).unit_cost_init := I_get_cur_rec.unit_cost_init;
               end if;
            else /* if cost sources are differnt use the MANL cost */

               if L_cost_source = 'MANL' then
                  I_ordloc_tbl(L_loc_ctr).unit_cost := I_get_cur_rec.unit_cost;
                  I_ordloc_tbl(L_loc_ctr).unit_cost_init := I_get_cur_rec.unit_cost_init;
               end if;
            end if;
         end if;

         L_added := 1;

      end if;

      /* if seg inventory is being used, make sure an ib roq
         shuts off scalling for the channel */
      if I_ordloc_tbl(L_loc_ctr).phy_loc = I_get_cur_rec.phy_loc AND
         I_ordloc_tbl(L_loc_ctr).repl_wh_link = I_get_cur_rec.repl_wh_link AND
         I_get_cur_rec.source_type = 'IB' then
         I_ordloc_tbl(L_loc_ctr).non_scale_ind := 'Y';
      end if;
       
      L_loc_ctr := L_loc_ctr + 1;
   END LOOP;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;
END ADD_LOC; /*end add_loc*/
--------------------------------------------------------------------------------------
FUNCTION GET_INV_MGMT(I_sup_info      IN OUT ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                      O_pool_supp     IN OUT ORDHEAD.SUPPLIER%TYPE,
                      O_file_id       IN OUT ORD_INV_MGMT.FILE_ID%TYPE,
                      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_program              VARCHAR2(30) := 'ADD_LINE_ITEM_SQL.GET_INV_MGMT';

   L_qc_ind               VARCHAR2(1);
   L_terms                ORDHEAD.TERMS%TYPE;
   L_freight_terms        SUPS.FREIGHT_TERMS%TYPE;
   L_payment_method       SUPS.PAYMENT_METHOD%TYPE;
   L_ship_method          SUPS.SHIP_METHOD%TYPE;
   L_edi_po_ind           VARCHAR2(1);
   L_currency_code        VARCHAR2(3);
   L_pre_mark_ind         VARCHAR2(1);
   L_inv_mgmt_lvl         VARCHAR2(1);
   L_agent                SUP_IMPORT_ATTR.AGENT%TYPE;
   L_discharge_port       ORDHEAD.DISCHARGE_PORT%TYPE;
   L_lading_port          ORDHEAD.LADING_PORT%TYPE;
   L_seq_no               NUMBER;
   L_buyer                ORDHEAD.BUYER%TYPE;
   L_single_loc_ind       VARCHAR2(1);
   L_due_ord_process_ind  VARCHAR2(1);
   L_due_ord_ind          VARCHAR2(1);
   L_due_ord_lvl          VARCHAR2(1);

   L_non_due_ord_create_inD  VARCHAR2(1);

   L_min_cnstr_lvl        VARCHAR2(1);
   L_min_cnstr_lvl_ind    VARCHAR2(1);
   L_scale_cnstr_ind      VARCHAR2(1);

   L_truck_split_ind      VARCHAR2(1);
   L_truck_method         VARCHAR2(6);

   L_cnst_type1           ORD_INV_MGMT.TRUCK_CNSTR_TYPE1%TYPE;

   L_cnst_uom1            ORD_INV_MGMT.TRUCK_CNSTR_UOM1%TYPE;
   L_cnst_value1          ORD_INV_MGMT.TRUCK_CNSTR_VAL1%TYPE;
   L_cnst_threshold1      NUMBER;

   L_cnst_type2           ORD_INV_MGMT.TRUCK_CNSTR_TYPE1%TYPE;
   L_cnst_uom2            ORD_INV_MGMT.TRUCK_CNSTR_UOM1%TYPE;
   L_cnst_value2          ORD_INV_MGMT.TRUCK_CNSTR_VAL1%TYPE;
   L_cnst_threshold2      NUMBER;

   L_pool_supp            ORDHEAD.SUPPLIER%TYPE;
   L_file_id              VARCHAR2(30);

   L_purchase_type        VARCHAR2(6);
   L_pickup_loc           NUMBER(10);

   L_ord_inv_mgmt_rowid   VARCHAR2(30);
   L_dept                 ORDHEAD.DEPT%TYPE;

   CURSOR c_inv_mgmt IS
      SELECT oh.qc_ind,
             oh.terms,
             oh.freight_terms,
             oh.payment_method,
             oh.ship_method,
             oh.edi_po_ind,
             oh.currency_code,
             oh.pre_mark_ind,
             'S',                /* inv_mgmt_lvl - not used for anything...*/
             oh.agent,
             oh.discharge_port,
             oh.lading_port,
             oh.supp_add_seq_no,
             NVL(oh.buyer, -1),
             'Y',                /* single loc ind */
             'N',                /* due ord process ind */
             'N',                /* due ord ind */
             'N',                /* due ord lvvl */
             'N',                /* non due ord create ind */
             oim.min_cnstr_lvl,
             oim.scale_cnstr_ind,
             oim.truck_split_ind,
             oim.truck_split_method,
             oim.truck_cnstr_type1,
             oim.truck_cnstr_uom1,
             NVL(oim.truck_cnstr_val1,0),
             NVL(oim.truck_cnstr_tol1/100,0),
             oim.truck_cnstr_type2,
             oim.truck_cnstr_uom2,
             NVL(oim.truck_cnstr_val2,0),
             NVL(oim.truck_cnstr_tol2/100,0),
             oim.pool_supplier,
             oim.file_id,
             oh.purchase_type,
             oh.pickup_loc,
             ROWIDTOCHAR(oim.rowid),
             NVL(oh.dept, '-1')
        FROM ord_inv_mgmt oim,
             ordhead oh
       WHERE oh.order_no = TO_NUMBER(I_order_no)
         AND oh.order_no = oim.order_no;
         
BEGIN
   
   /* populate global variables */
   open c_inv_mgmt;
   fetch c_inv_mgmt INTO L_qc_ind,
                         L_terms,
                         L_freight_terms,
                         L_payment_method,
                         L_ship_method,
                         L_edi_po_ind,
                         L_currency_code,
                         L_pre_mark_ind,
                         L_inv_mgmt_lvl,
                         L_agent,
                         L_discharge_port,
                         L_lading_port,
                         L_seq_no,
                         L_buyer,
                         L_single_loc_ind,
                         L_due_ord_process_ind,
                         L_due_ord_ind,
                         L_due_ord_lvl,
                         L_non_due_ord_create_ind,
                         L_min_cnstr_lvl_ind,
                         L_scale_cnstr_ind,
                         L_truck_split_ind,
                         L_truck_method,
                         L_cnst_type1,
                         L_cnst_uom1,
                         L_cnst_value1,
                         L_cnst_threshold1,
                         L_cnst_type2,
                         L_cnst_uom2,
                         L_cnst_value2,
                         L_cnst_threshold2,
                         L_pool_supp,
                         L_file_id,
                         L_purchase_type,
                         L_pickup_loc,
                         L_ord_inv_mgmt_rowid,
                         L_dept;
   close c_inv_mgmt;

   I_sup_info.qc_ind := L_qc_ind;
   I_sup_info.terms := L_terms;
   if (L_freight_terms IS NOT NULL) then
      I_sup_info.freight_terms := L_freight_terms;
   else
      I_sup_info.freight_terms := NULL;
   end if;

   if (l_payment_method IS NOT NULL) then
      I_sup_info.payment_method := L_payment_method;
   else
      I_sup_info.payment_method := NULL;
   end if;

   if (L_ship_method IS NOT NULL) then
      I_sup_info.ship_method := L_ship_method;
   else
      I_sup_info.ship_method := NULL;
   end if;

   I_sup_info.edi_po_ind := L_edi_po_ind;
   I_sup_info.currency_code := L_currency_code;
   I_sup_info.pre_mark_ind := L_pre_mark_ind;
   I_sup_info.inv_mgmt_lvl := L_inv_mgmt_lvl;

   if L_dept = '-1' then
      I_sup_info.dept_level_ord := 'N';
   else
      I_sup_info.dept_level_ord := 'Y';
   end if;

   if (L_agent IS NOT NULL) then
      I_sup_info.agent := L_agent;
   else
      I_sup_info.agent := NULL;
   end if;

   if (L_discharge_port IS NOT NULL) then
      I_sup_info.discharge_port := L_discharge_port;
   else
      I_sup_info.discharge_port := NULL;
   end if;

   if (L_lading_port IS NOT NULL) then
      I_sup_info.lading_port := L_lading_port;
   else
      I_sup_info.lading_port := NULL;
   end if;

   I_sup_info.seq_no := L_seq_no;
   if (L_buyer IS NOT NULL) then
      I_sup_info.buyer := L_buyer;
   else
      I_sup_info.buyer := NULL;
   end if;

   I_sup_info.single_loc_ind := L_single_loc_ind;
   I_sup_info.due_ord_process_ind := L_due_ord_process_ind;
   I_sup_info.due_ord_ind := L_due_ord_ind;
   I_sup_info.due_ord_lvl := L_due_ord_lvl;
   I_sup_info.non_due_ord_create_ind := L_non_due_ord_create_ind;

   if (L_min_cnstr_lvl_ind IS NOT NULL) then
      I_sup_info.min_cnstr_lvl := L_min_cnstr_lvl;
   else
      I_sup_info.min_cnstr_lvl := NULL;
   end if;

   I_sup_info.scale_cnstr_ind := L_scale_cnstr_ind;
   I_sup_info.truck_split_ind := L_truck_split_ind;
   if (L_truck_method IS NOT NULL) then
      I_sup_info.truck_method := L_truck_method;
   else
      I_sup_info.truck_method := NULL;
   end if;

   if (L_cnst_type1 IS NOT NULL) then
      I_sup_info.cnst_type(1) := L_cnst_type1;
   else
      I_sup_info.cnst_type(1) := L_cnst_type1;
   end if;

   if (L_cnst_uom1 IS NOT NULL) then
      I_sup_info.cnst_uom(1) := L_cnst_uom1;
   else
      I_sup_info.cnst_uom(1) := NULL;
   end if;

   I_sup_info.cnst_value(1) := L_cnst_value1;
   I_sup_info.cnst_threshold(1) := L_cnst_threshold1;
   if (L_cnst_type2 IS NOT NULL) then
      I_sup_info.cnst_type(2) := L_cnst_type2;
   else
      I_sup_info.cnst_type(2) := NULL;
   end if;

   if (L_cnst_uom2 IS NOT NULL) then
      I_sup_info.cnst_uom(2) := L_cnst_uom2;
   else
      I_sup_info.cnst_uom(2) := NULL;
   end if;

   I_sup_info.cnst_value(2) := L_cnst_value2;
   I_sup_info.cnst_threshold(2) := L_cnst_threshold2;


   if (L_pool_supp IS NOT NULL) then
      O_pool_supp := L_pool_supp;
   else
      O_pool_supp := NULL;
   end if;

   if (L_file_id IS NOT NULL) then
      O_file_id := L_file_id;
   else
      O_file_id := NULL;
   end if;

   I_sup_info.total_cnst_qty(1) := 0;
   I_sup_info.total_cnst_qty(2) := 0;
   I_sup_info.cnst_value_reset(1) := 0;
   I_sup_info.cnst_value_reset(2) := 0;
   I_sup_info.ltl_flag := UNKNOWN_TRUCK;
   I_sup_info.cnst_index := -1;

   if (L_purchase_type IS NOT NULL) then
      I_sup_info.purchase_type := L_purchase_type;
   else
      I_sup_info.purchase_type := NULL;
   end if;

   if (L_pickup_loc IS NOT NULL) then
      I_sup_info.pickup_loc := L_pickup_loc;
   else
      I_sup_info.pickup_loc := NULL;
   end if;

   I_sup_info.sup_inv_mgmt_rowid := NULL;
   I_sup_info.ord_inv_mgmt_rowid := L_ord_inv_mgmt_rowid;

   /* initialize partner type variables */
   I_sup_info.partner_type_1 := NULL;
   I_sup_info.partner_1 := NULL;
   I_sup_info.partner_type_2 := NULL;
   I_sup_info.partner_2 := NULL;
   I_sup_info.partner_type_3 := NULL;
   I_sup_info.partner_3 := NULL;
   I_sup_info.factory := NULL;

   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
   return FALSE;
END GET_INV_MGMT; /* end get_inv_mgmt */
--------------------------------------------------------------------------------------
/*
 * For each supplier/dept/loc, info is needed from the SUPS, SUP_IMPORT_ATTR,
 * ADDR, DEPS and SUP_INV_MGMT tables.  This info is fetched from the tables
 * put into the passed int suppier info struct by this function.  It holds the
 * results of the previous call to this function in static variables.  Thus, if
 * it is call for the same input more than once in a row, it can avoid hitting the
 * data base more than once.
 * The SUP_INV_MGMT info can be held at the supplier, supplier/dept, supplier/loc,
 * or supplier/dept/loc level.  The locations will only be physical warehouses.
 * It also possible that they may not exist at all.
 *
 * -- why multi cursors against sup_inv_mgmt --
 *   If the sups.inv_mgmt_lvl is A, the info will be retrieved at
 *     the supp/dept/loc level.  If no record is found, the info will be retrieved
 *     at the supp/dept level.  If no record is found there, the info will be
 *     retreived at the supp level.  If no record is found there, the info will
 *     be defaulted.
 *   If the sups.inv_mgmt_lvl is D, the info will be retrieved at
 *     the supp/dept level.  If no record is found, the info will be retrieved
 *     at the supp level.  If no record is found there, the info will be defaulted.
 *   If the sups.inv_mgmt_lvl is L, the info will be retrieved at
 *     the supp/loc level.  If no record is found, the info will be retrieved
 *     at the supp level.  If no record is found there, the info will be defaulted.
 *   If the sups.inv_mgmt_lvl is S, the info will be retrieved at the supp level.
 *     If no record is found there, the info will be defaulted.
 */
--------------------------------------------------------------------------------------
FUNCTION GET_SUP_INFO(I_supp_info        IN OUT ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                      I_contract         IN     ORDHEAD.CONTRACT_NO%TYPE,
                      I_supplier         IN     ORDHEAD.SUPPLIER%TYPE,
                      I_dept             IN     ORDHEAD.DEPT%TYPE,
                      I_loc              IN     ORDHEAD.LOCATION%TYPE,
                      O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(30) := 'ADD_LINE_ITEM_SQL.GET_SUP_INFO';

   /* if these dimensions change, need to reload the below variables */
   L_supplier                 ORDHEAD.SUPPLIER%TYPE;
   L_dept                     ORDHEAD.DEPT%TYPE;
   L_loc                      ORDHEAD.LOCATION%TYPE;
   L_inv_mgmt_lvl             SUPS.INV_MGMT_LVL%TYPE;

   L_terms                    ORDHEAD.TERMS%TYPE;
   L_freight_terms            SUPS.FREIGHT_TERMS%TYPE;

   L_payment_method           SUPS.PAYMENT_METHOD%TYPE;
   L_ship_method              SUPS.SHIP_METHOD%TYPE;
   L_edi_po_ind               ORDHEAD.EDI_PO_IND%TYPE;
   L_currency_code            ORDHEAD.CURRENCY_CODE%TYPE;
   L_pre_mark_ind             ORDHEAD.PRE_MARK_IND%TYPE;
   L_agent                    SUP_IMPORT_ATTR.AGENT%TYPE;
   L_discharge_port           ORDHEAD.DISCHARGE_PORT%TYPE;
   L_lading_port              ORDHEAD.LADING_PORT%TYPE;
   L_seq_no                   NUMBER(10);
   L_buyer                    ORDHEAD.BUYER%TYPE;
   L_single_loc_ind           ORD_INV_MGMT.SINGLE_LOC_IND%TYPE;
   L_due_ord_process_ind      ORD_INV_MGMT.DUE_ORD_IND%TYPE;
   L_due_ord_ind              ORD_INV_MGMT.DUE_ORD_IND%TYPE;
   L_due_ord_lvl              ORD_INV_MGMT.DUE_ORD_LVL%TYPE;
   L_non_due_ord_create_ind   VARCHAR2(1);
   L_min_cnstr_lvl            ORD_INV_MGMT.MIN_CNSTR_LVL%TYPE;
   L_min_cnstr_lvl_ind        VARCHAR2(1);
   L_scale_cnstr_ind          VARCHAR2(1);

   L_truck_split_ind          VARCHAR2(1);
   L_truck_method             ORD_INV_MGMT.TRUCK_SPLIT_METHOD%TYPE;
   L_cnst_type1               ORD_INV_MGMT.MIN_CNSTR_TYPE1%TYPE;
   L_cnst_uom1                ORD_INV_MGMT.MIN_CNSTR_UOM1%TYPE;
   L_cnst_value1              ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE;
   L_cnst_threshold1          NUMBER(10);


   L_cnst_type2               ORD_INV_MGMT.MIN_CNSTR_TYPE2%TYPE;
   L_cnst_uom2                ORD_INV_MGMT.MIN_CNSTR_UOM2%TYPE;
   L_cnst_value2              ORD_INV_MGMT.MIN_CNSTR_VAL2%TYPE;
   L_cnst_threshold2          NUMBER(10);


   L_pool_supp                ORDHEAD.SUPPLIER%TYPE;
   L_file_id                  ORD_INV_MGMT.FILE_ID%TYPE;

   L_purchase_type            ORDHEAD.PURCHASE_TYPE%TYPE;
   L_pickup_loc               ORDHEAD.PICKUP_LOC%TYPE;

   L_ord_inv_mgmt_rowid       VARCHAR2(30);
   L_dept_level_ord           VARCHAR2(1);

   L_qc_ind                   ORDHEAD.QC_IND%TYPE;
   L_partner_type_1           ORDHEAD.PARTNER_TYPE_1%TYPE;
   L_partner_1                ORDHEAD.PARTNER1%TYPE;
   L_partner_type_2           ORDHEAD.PARTNER_TYPE_2%TYPE;
   L_partner_2                ORDHEAD.PARTNER2%TYPE;
   L_partner_type_3           ORDHEAD.PARTNER_TYPE_3%TYPE;
   L_partner_3                ORDHEAD.PARTNER3%TYPE;
   L_factory                  ORDHEAD.FACTORY%TYPE;

   CURSOR c_check_dept IS
      SELECT s.qc_ind,
             s.terms,
             s.freight_terms,
             s.payment_method,
             s.ship_method,
             s.edi_po_ind,
             s.currency_code,
             s.pre_mark_ind,
             s.inv_mgmt_lvl,
             a.seq_no,
             DECODE(s.inv_mgmt_lvl,
                    'A', 'Y',
                    'L', 'Y',
                    'N'),
             DECODE(s.inv_mgmt_lvl,
                    'A', 'Y',
                    'D', 'Y',
                    'N')
        FROM sups s,
             addr a
       WHERE s.supplier         = TO_NUMBER(I_supplier)
         AND a.key_value_1      = I_supplier
         AND a.module           = 'SUPP'
         AND a.addr_type        = '04'
         AND a.primary_addr_ind = 'Y';

   CURSOR c_sup_dept_loc IS
      SELECT sia.agent,
             sia.discharge_port,
             sia.lading_port,
             sim.single_loc_ind,
             sim.due_ord_process_ind,
             sim.due_ord_lvl,
             sim.non_due_ord_create_ind,
             d.buyer,
             sim.min_cnstr_lvl,
             sim.scale_cnstr_ind,
             sim.truck_split_ind,
             sim.truck_split_method,
             sim.truck_cnstr_type1,
             sim.truck_cnstr_uom1,
             sim.truck_cnstr_val1,
             sim.truck_cnstr_tol1/100,
             sim.truck_cnstr_type2,
             sim.truck_cnstr_uom2,
             sim.truck_cnstr_val2,
             sim.truck_cnstr_tol2/100,
             sim.purchase_type,
             sim.pickup_loc,
             sia.partner_type_1,
             sia.partner_1,
             sia.partner_type_2,
             sia.partner_2,
             sia.partner_type_3,
             sia.partner_3,
             sia.factory,
             ROWIDTOCHAR(sim.rowid)
        FROM sup_import_attr sia,
             sup_inv_mgmt sim,
             deps d
       WHERE sim.supplier       = TO_NUMBER(I_supplier)
         AND sim.supplier       = sia.supplier(+)
         AND sim.dept           = TO_NUMBER(I_dept)
         AND sim.location       = TO_NUMBER(I_loc)
         AND d.dept             = TO_NUMBER(I_dept);

   CURSOR c_sup_dept IS
      SELECT sia.agent,
             sia.discharge_port,
             sia.lading_port,
             sim.single_loc_ind,
             sim.due_ord_process_ind,
             sim.due_ord_lvl,
             sim.non_due_ord_create_ind,
             d.buyer,
             sim.min_cnstr_lvl,
             sim.scale_cnstr_ind,
             sim.truck_split_ind,
             sim.truck_split_method,
             sim.truck_cnstr_type1,
             sim.truck_cnstr_uom1,
             sim.truck_cnstr_val1,
             sim.truck_cnstr_tol1/100,
             sim.truck_cnstr_type2,
             sim.truck_cnstr_uom2,
             sim.truck_cnstr_val2,
             sim.truck_cnstr_tol2/100,
             sim.purchase_type,
             sim.pickup_loc,
             sia.partner_type_1,
             sia.partner_1,
             sia.partner_type_2,
             sia.partner_2,
             sia.partner_type_3,
             sia.partner_3,
             sia.factory,
             ROWIDTOCHAR(sim.rowid)
        FROM sup_import_attr sia,
             sup_inv_mgmt sim,
             deps d
       WHERE sim.supplier       = TO_NUMBER(I_supplier)
         AND sim.supplier       = sia.supplier(+)
         AND sim.dept           = TO_NUMBER(I_dept)
         AND d.dept             = TO_NUMBER(I_dept)
         AND sim.location       IS NULL;

   CURSOR c_sup_loc IS
      SELECT sia.agent,
             sia.discharge_port,
             sia.lading_port,
             sim.single_loc_ind,
             sim.due_ord_process_ind,
             sim.due_ord_lvl,
             sim.non_due_ord_create_ind,
             /* only want a buyer if the order level is DEPT or ALL */
             DECODE(L_inv_mgmt_lvl,
                    'D', d.buyer,
                    'A', d.buyer,
                    NULL),
             sim.min_cnstr_lvl,
             sim.scale_cnstr_ind,
             sim.truck_split_ind,
             sim.truck_split_method,
             sim.truck_cnstr_type1,
             sim.truck_cnstr_uom1,
             sim.truck_cnstr_val1,
             sim.truck_cnstr_tol1/100,
             sim.truck_cnstr_type2,
             sim.truck_cnstr_uom2,
             sim.truck_cnstr_val2,
             sim.truck_cnstr_tol2/100,
             sim.purchase_type,
             sim.pickup_loc,
             sia.partner_type_1,
             sia.partner_1,
             sia.partner_type_2,
             sia.partner_2,
             sia.partner_type_3,
             sia.partner_3,
             sia.factory,
             ROWIDTOCHAR(sim.rowid)
        FROM sup_import_attr sia,
             sup_inv_mgmt sim,
             deps d
       WHERE sim.supplier       = TO_NUMBER(I_supplier)
         AND sim.supplier       = sia.supplier(+)
         AND sim.location       = TO_NUMBER(I_loc)
         AND d.dept             = TO_NUMBER(I_dept)
         AND sim.dept           IS NULL;

   CURSOR c_sup IS
      SELECT sia.agent,
             sia.discharge_port,
             sia.lading_port,
             sim.single_loc_ind,
             sim.due_ord_process_ind,
             sim.due_ord_lvl,
             sim.non_due_ord_create_ind,
             /* only want a buyer if the order level is DEPT */
             DECODE(L_inv_mgmt_lvl,
                    'D', d.buyer, NULL),
             sim.min_cnstr_lvl,
             sim.scale_cnstr_ind,
             sim.truck_split_ind,
             sim.truck_split_method,
             sim.truck_cnstr_type1,
             sim.truck_cnstr_uom1,
             sim.truck_cnstr_val1,
             sim.truck_cnstr_tol1/100,
             sim.truck_cnstr_type2,
             sim.truck_cnstr_uom2,
             sim.truck_cnstr_val2,
             sim.truck_cnstr_tol2/100,
             sim.purchase_type,
             sim.pickup_loc,
             sia.partner_type_1,
             sia.partner_1,
             sia.partner_type_2,
             sia.partner_2,
             sia.partner_type_3,
             sia.partner_3,
             sia.factory,
             ROWIDTOCHAR(sim.rowid)
        FROM sup_import_attr sia,
             sup_inv_mgmt sim,
             deps d
       WHERE sim.supplier       = TO_NUMBER(I_supplier)
         AND sim.supplier       = sia.supplier(+)
         AND sim.dept           IS NULL
         AND sim.location       IS NULL
         AND d.dept             = TO_NUMBER(I_dept);

   CURSOR c_default_import IS
      SELECT sia.agent,
             sia.discharge_port,
             sia.partner_type_1,
             sia.partner_1,
             sia.partner_type_2,
             sia.partner_2,
             sia.partner_type_3,
             sia.partner_3,
             sia.factory,
             sia.lading_port
        FROM sup_import_attr sia
       WHERE sia.supplier       = TO_NUMBER(I_supplier);

   CURSOR c_default_buyer IS
      SELECT buyer
        FROM deps
       WHERE dept = TO_NUMBER(I_dept);
BEGIN 
   /* no change in supplier or dept from last call, use the data
      left over from the last call to populate the supplier struct */
   if NVL(L_supplier,-999) = I_supplier AND NVL(L_dept,-999) = I_dept AND NVL(L_loc,-999) = I_loc then 
      if fill_sup_struct(I_supp_info,
                         I_contract,
                         L_qc_ind,
                         L_terms,
                         L_freight_terms,
                         L_payment_method,
                         L_ship_method,
                         L_edi_po_ind,
                         L_currency_code,
                         L_pre_mark_ind,
                         L_inv_mgmt_lvl,
                         L_dept_level_ord,
                         L_agent,
                         L_discharge_port,
                         L_lading_port,
                         L_seq_no,
                         L_buyer,
                         L_min_cnstr_lvl,
                         L_scale_cnstr_ind,
                         L_truck_split_ind,
                         L_truck_method,
                         L_cnst_type1,
                         L_cnst_uom1,
                         L_cnst_value1,
                         L_cnst_threshold1,
                         L_cnst_type2,
                         L_cnst_uom2,
                         L_cnst_value2,
                         L_cnst_threshold2,
                         L_single_loc_ind,
                         L_due_ord_process_ind,
                         L_due_ord_lvl,
                         L_non_due_ord_create_ind,
                         L_purchase_type,
                         L_pickup_loc,
                         L_partner_type_1,
                         L_partner_1,
                         L_partner_type_2,
                         L_partner_2,
                         L_partner_type_3,
                         L_partner_3,
                         L_factory,
                         LP_SUP_INV_MGMT_ROWID) = FALSE then
          return FALSE; 
       end if;
       return TRUE;
   end if;

   /* if supplier changed, get info that varies by supplier */
   if NVL(L_supplier,-999) <> I_supplier then 
      /* get supplier level info, this will determine what level to
         hit sup_inv_mgmt at */
      open C_CHECK_DEPT;
      fetch C_CHECK_DEPT INTO L_qc_ind,
                              L_terms,
                              L_freight_terms,
                              L_payment_method,
                              L_ship_method,
                              L_edi_po_ind,
                              L_currency_code,
                              L_pre_mark_ind,
                              L_inv_mgmt_lvl,
                              L_seq_no,
                              L_single_loc_ind,
                              L_dept_level_ord;
      close C_CHECK_DEPT;
   end if; 
   /* the holders have changed, populate the holders, attributes for the next vendor line */
   L_supplier := I_supplier;
   L_dept := I_dept;
   L_loc := I_loc;

   /* if the info is held at the supp dept loc level, look for a supp dept loc level record */
   if NVL(L_inv_mgmt_lvl,'X') = 'A' then 
      open C_SUP_DEPT_LOC;
      fetch C_SUP_DEPT_LOC into l_agent,
                                L_discharge_port,
                                L_lading_port,
                                L_single_loc_ind,
                                L_due_ord_process_ind,
                                L_due_ord_lvl,
                                L_non_due_ord_create_ind,
                                L_buyer,
                                L_min_cnstr_lvl,
                                L_scale_cnstr_ind,
                                L_truck_split_ind,
                                L_truck_method,
                                L_cnst_type1,
                                L_cnst_uom1,
                                L_cnst_value1,
                                L_cnst_threshold1,
                                L_cnst_type2,
                                L_cnst_uom2,
                                L_cnst_value2,
                                L_cnst_threshold2,
                                L_purchase_type,
                                L_pickup_loc,
                                L_partner_type_1,
                                L_partner_1,
                                L_partner_type_2,
                                L_partner_2,
                                L_partner_type_3,
                                L_partner_3,
                                L_factory,
                                LP_sup_inv_mgmt_rowid;
      if C_SUP_DEPT_LOC%NOTFOUND then  
         if fill_sup_struct(I_supp_info,
                            I_contract,
                            L_qc_ind,
                            L_terms,
                            L_freight_terms,
                            L_payment_method,
                            L_ship_method,
                            L_edi_po_ind,
                            L_currency_code,
                            L_pre_mark_ind,
                            L_inv_mgmt_lvl,
                            L_dept_level_ord,
                            L_agent,
                            L_discharge_port,
                            L_lading_port,
                            L_seq_no,
                            L_buyer,
                            L_min_cnstr_lvl,
                            L_scale_cnstr_ind,
                            L_truck_split_ind,
                            L_truck_method,
                            L_cnst_type1,
                            L_cnst_uom1,
                            L_cnst_value1,
                            L_cnst_threshold1,
                            L_cnst_type2,
                            L_cnst_uom2,
                            L_cnst_value2,
                            L_cnst_threshold2,
                            L_single_loc_ind,
                            L_due_ord_process_ind,
                            L_due_ord_lvl,
                            L_non_due_ord_create_ind,
                            L_purchase_type,
                            L_pickup_loc,
                            L_partner_type_1,
                            L_partner_1,
                            L_partner_type_2,
                            L_partner_2,
                            L_partner_type_3,
                            L_partner_3,
                            L_factory,
                            LP_sup_inv_mgmt_rowid) = FALSE then
             return FALSE; 
          end if;
          return TRUE;
       end if;
       close C_SUP_DEPT_LOC;
   end if; 

   /* if the info is held at the supp dept level, look for a supp dept level record */
   if NVL(L_inv_mgmt_lvl,'X') = 'D' OR NVL(L_inv_mgmt_lvl,'X') = 'A' then 
      open C_SUP_DEPT;
      fetch C_SUP_DEPT into l_agent,
                            L_discharge_port,
                            L_lading_port,
                            L_single_loc_ind,
                            L_due_ord_process_ind,
                            L_due_ord_lvl,
                            L_non_due_ord_create_ind,
                            L_buyer,
                            L_min_cnstr_lvl,
                            L_scale_cnstr_ind,
                            L_truck_split_ind,
                            L_truck_method,
                            L_cnst_type1,
                            L_cnst_uom1,
                            L_cnst_value1,
                            L_cnst_threshold1,
                            L_cnst_type2,
                            L_cnst_uom2,
                            L_cnst_value2,
                            L_cnst_threshold2,
                            L_purchase_type,
                            L_pickup_loc,
                            L_partner_type_1,
                            L_partner_1,
                            L_partner_type_2,
                            L_partner_2,
                            L_partner_type_3,
                            L_partner_3,
                            L_factory,
                            LP_sup_inv_mgmt_rowid;

      if C_SUP_DEPT%NOTFOUND then
         if fill_sup_struct(I_supp_info,
                            I_contract,
                            L_qc_ind,
                            L_terms,
                            L_freight_terms,
                            L_payment_method,
                            L_ship_method,
                            L_edi_po_ind,
                            L_currency_code,
                            L_pre_mark_ind,
                            L_inv_mgmt_lvl,
                            L_dept_level_ord,
                            L_agent,
                            L_discharge_port,
                            L_lading_port,
                            L_seq_no,
                            L_buyer,
                            L_min_cnstr_lvl,
                            L_scale_cnstr_ind,
                            L_truck_split_ind,
                            L_truck_method,
                            L_cnst_type1,
                            L_cnst_uom1,
                            L_cnst_value1,
                            L_cnst_threshold1,
                            L_cnst_type2,
                            L_cnst_uom2,
                            L_cnst_value2,
                            L_cnst_threshold2,
                            L_single_loc_ind,
                            L_due_ord_process_ind,
                            L_due_ord_lvl,
                            L_non_due_ord_create_ind,
                            L_purchase_type,
                            L_pickup_loc,
                            L_partner_type_1,
                            L_partner_1,
                            L_partner_type_2,
                            L_partner_2,
                            L_partner_type_3,
                            L_partner_3,
                            L_factory,
                            LP_sup_inv_mgmt_rowid) = FALSE then
             return FALSE; 
          end if; 
          return TRUE;

      end if;
      close C_SUP_DEPT;
   end if;

   /* if the info is held at the supp loc level, look for a supp loc level record */
   if NVL(L_inv_mgmt_lvl,'X') = 'L' then 
      open C_SUP_LOC;
      fetch C_SUP_LOC into l_agent,
                           L_discharge_port,
                           L_lading_port,
                           L_single_loc_ind,
                           L_due_ord_process_ind,
                           L_due_ord_lvl,
                           L_non_due_ord_create_ind,
                           L_buyer,
                           L_min_cnstr_lvl,
                           L_scale_cnstr_ind,
                           L_truck_split_ind,
                           L_truck_method,
                           L_cnst_type1,
                           L_cnst_uom1,
                           L_cnst_value1,
                           L_cnst_threshold1,
                           L_cnst_type2,
                           L_cnst_uom2,
                           L_cnst_value2,
                           L_cnst_threshold2,
                           L_purchase_type,
                           L_pickup_loc,
                           L_partner_type_1,
                           L_partner_1,
                           L_partner_type_2,
                           L_partner_2,
                           L_partner_type_3,
                           L_partner_3,
                           L_factory,
                           LP_sup_inv_mgmt_rowid;

      if C_SUP_LOC%NOTFOUND then 
         if fill_sup_struct(I_supp_info,
                            I_contract,
                            L_qc_ind,
                            L_terms,
                            L_freight_terms,
                            L_payment_method,
                            L_ship_method,
                            L_edi_po_ind,
                            L_currency_code,
                            L_pre_mark_ind,
                            L_inv_mgmt_lvl,
                            L_dept_level_ord,
                            L_agent,
                            L_discharge_port,
                            L_lading_port,
                            L_seq_no,
                            L_buyer,
                            L_min_cnstr_lvl,
                            L_scale_cnstr_ind,
                            L_truck_split_ind,
                            L_truck_method,
                            L_cnst_type1,
                            L_cnst_uom1,
                            L_cnst_value1,
                            L_cnst_threshold1,
                            L_cnst_type2,
                            L_cnst_uom2,
                            L_cnst_value2,
                            L_cnst_threshold2,
                            L_single_loc_ind,
                            L_due_ord_process_ind,
                            L_due_ord_lvl,
                            L_non_due_ord_create_ind,
                            L_purchase_type,
                            L_pickup_loc,
                            L_partner_type_1,
                            L_partner_1,
                            L_partner_type_2,
                            L_partner_2,
                            L_partner_type_3,
                            L_partner_3,
                            L_factory,
                            LP_sup_inv_mgmt_rowid) = FALSE then
             return FALSE;
          end if; 
          return TRUE; 
       end if; 
       close C_SUP_LOC;
   end if; 
   /* if the info is held at the supplier level or it could not be found at a lover level, */
   /* look for a supplier level record */
   open C_SUP;
   fetch C_SUP INTO L_agent,
                    L_discharge_port,
                    L_lading_port,
                    L_single_loc_ind,
                    L_due_ord_process_ind,
                    L_due_ord_lvl,
                    L_non_due_ord_create_ind,
                    L_buyer,
                    L_min_cnstr_lvl,
                    L_scale_cnstr_ind,
                    L_truck_split_ind,
                    L_truck_method,
                    L_cnst_type1,
                    L_cnst_uom1,
                    L_cnst_value1,
                    L_cnst_threshold1,
                    L_cnst_type2,
                    L_cnst_uom2,
                    L_cnst_value2,
                    L_cnst_threshold2,
                    L_purchase_type,
                    L_pickup_loc,
                    L_partner_type_1,
                    L_partner_1,
                    L_partner_type_2,
                    L_partner_2,
                    L_partner_type_3,
                    L_partner_3,
                    L_factory,
                    LP_sup_inv_mgmt_rowid;

   if C_SUP%FOUND then  
      if fill_sup_struct(I_supp_info,
                      I_contract,
                      L_qc_ind,
                      L_terms,
                      L_freight_terms,
                      L_payment_method,
                      L_ship_method,
                      L_edi_po_ind,
                      L_currency_code,
                      L_pre_mark_ind,
                      L_inv_mgmt_lvl,
                      L_dept_level_ord,
                      L_agent,
                      L_discharge_port,
                      L_lading_port,
                      L_seq_no,
                      L_buyer,
                      L_min_cnstr_lvl,
                      L_scale_cnstr_ind,
                      L_truck_split_ind,
                      L_truck_method,
                      L_cnst_type1,
                      L_cnst_uom1,
                      L_cnst_value1,
                      L_cnst_threshold1,
                      L_cnst_type2,
                      L_cnst_uom2,
                      L_cnst_value2,
                      L_cnst_threshold2,
                      L_single_loc_ind,
                      L_due_ord_process_ind,
                      L_due_ord_lvl,
                      L_non_due_ord_create_ind,
                      L_purchase_type,
                      L_pickup_loc,
                      L_partner_type_1,
                      L_partner_1,
                      L_partner_type_2,
                      L_partner_2,
                      L_partner_type_3,
                      L_partner_3,
                      L_factory,
                      LP_sup_inv_mgmt_rowid) = FALSE then
         return FALSE;
      end if;
      return TRUE;
   end if;  
   close C_SUP; 
   /* no record exist at the supplier/dept or supplier level on SUPS_INV_MGMT, so we
      will default values and set the rowid to NULL (a default record will
      be put on ord_inv_mgmt) */
   L_due_ord_process_ind := 'N';
   L_due_ord_lvl := 'ORDER';
   L_due_ord_lvl := 0;  /* set to 0 to enable non-null ls_due_ord_lvl */
   L_non_due_ord_create_ind := 'N';
   L_purchase_type := NULL;
   L_pickup_loc := NULL;
   LP_sup_inv_mgmt_rowid := NULL;
   
   open C_DEFAULT_IMPORT;
   fetch C_DEFAULT_IMPORT INTO L_agent,
                               L_discharge_port,
                               L_partner_type_1,
                               L_partner_1,
                               L_partner_type_2,
                               L_partner_2,
                               L_partner_type_3,
                               L_partner_3,
                               L_factory,
                               L_lading_port;
   close C_DEFAULT_IMPORT; 
   
   if NVL(L_inv_mgmt_lvl,'X') = 'D' then
      open c_default_buyer;
      fetch c_default_buyer INTO L_buyer;
      close c_default_buyer;
   else  /* if not at dept level, default buyer to null */
      l_buyer := -1;
   end if; 

   if fill_sup_struct(I_supp_info,
                      I_contract,
                      L_qc_ind,
                      L_terms,
                      L_freight_terms,
                      L_payment_method,
                      L_ship_method,
                      L_edi_po_ind,
                      L_currency_code,
                      L_pre_mark_ind,
                      L_inv_mgmt_lvl,
                      L_dept_level_ord,
                      L_agent,
                      L_discharge_port,
                      L_lading_port,
                      L_seq_no,
                      L_buyer,
                      L_min_cnstr_lvl,
                      L_scale_cnstr_ind,
                      L_truck_split_ind,
                      L_truck_method,
                      L_cnst_type1,
                      L_cnst_uom1,
                      L_cnst_value1,
                      L_cnst_threshold1,
                      L_cnst_type2,
                      L_cnst_uom2,
                      L_cnst_value2,
                      L_cnst_threshold2,
                      L_single_loc_ind,
                      L_due_ord_process_ind,
                      L_due_ord_lvl,
                      L_non_due_ord_create_ind,
                      L_purchase_type,
                      L_pickup_loc,
                      L_partner_type_1,
                      L_partner_1,
                      L_partner_type_2,
                      L_partner_2,
                      L_partner_type_3,
                      L_partner_3,
                      L_factory,
                      LP_SUP_INV_MGMT_ROWID) = FALSE then
      return FALSE;   
   end if;
   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
   return FALSE;

END GET_SUP_INFO; /*end get_sup_info*/

/*
 * This function takes in a supplier info struct and populates it with the passed
 * in variables.
 */
--------------------------------------------------------------------------------------
FUNCTION FILL_SUP_STRUCT(I_get_sup_info            IN OUT ADD_LINE_ITEM_SQL.SUPPLIER_INFO_REC,
                         I_contract                IN     ORDHEAD.CONTRACT_NO%TYPE,
                         I_qc_ind                  IN     ORDHEAD.QC_IND%TYPE,
                         I_terms                   IN     ORDHEAD.TERMS%TYPE,
                         I_freight_terms           IN     ORDHEAD.FREIGHT_TERMS%TYPE,
                         I_payment_method          IN     ORDHEAD.PAYMENT_METHOD%TYPE,
                         I_ship_method             IN     ORDHEAD.SHIP_METHOD%TYPE,
                         I_edi_po_ind              IN     ORDHEAD.EDI_PO_IND%TYPE,
                         I_currency_code           IN     ORDHEAD.CURRENCY_CODE%TYPE,
                         I_pre_mark_ind            IN     ORDHEAD.PRE_MARK_IND%TYPE,
                         I_inv_mgmt_lvl            IN     SUPS.INV_MGMT_LVL%TYPE,
                         I_dept_level_ord          IN     VARCHAR2,
                         I_agent                   IN     ORDHEAD.AGENT%TYPE,
                         I_discharge_port          IN     ORDHEAD.DISCHARGE_PORT%TYPE,
                         I_lading_port             IN     ORDHEAD.LADING_PORT%TYPE,
                         I_seq_no                  IN     NUMBER,
                         I_buyer                   IN     ORDHEAD.BUYER%TYPE,
                         I_min_cnstr_lvl           IN     ORD_INV_MGMT.MIN_CNSTR_LVL%TYPE,
                         I_scale_cnstr_ind         IN     ORD_INV_MGMT.SCALE_CNSTR_IND%TYPE,
                         I_truck_split_ind         IN     ORD_INV_MGMT.TRUCK_SPLIT_IND%TYPE,
                         I_truck_method            IN     ORD_INV_MGMT.TRUCK_SPLIT_METHOD%TYPE,
                         I_cnst_type1              IN     ORD_INV_MGMT.MIN_CNSTR_TYPE1%TYPE,
                         I_cnst_uom1               IN     ORD_INV_MGMT.MIN_CNSTR_UOM1%TYPE,
                         I_cnst_value1             IN     ORD_INV_MGMT.MIN_CNSTR_VAL1%TYPE,
                         I_cnst_threshold1         IN     VARCHAR2,
                         I_cnst_type2              IN     ORD_INV_MGMT.MIN_CNSTR_TYPE2%TYPE,
                         I_cnst_uom2               IN     ORD_INV_MGMT.MIN_CNSTR_UOM2%TYPE,
                         I_cnst_value2             IN     ORD_INV_MGMT.MIN_CNSTR_VAL2%TYPE,
                         I_cnst_threshold2         IN     NUMBER,
                         I_single_loc_ind          IN     ORD_INV_MGMT.SINGLE_LOC_IND%TYPE,
                         I_due_ord_process_ind     IN     ORD_INV_MGMT.DUE_ORD_IND%TYPE,
                         I_due_ord_lvl             IN     ORD_INV_MGMT.DUE_ORD_LVL%TYPE,
                         I_non_due_ord_create_ind  IN     VARCHAR2,
                         I_purchase_type           IN     ORDHEAD.PURCHASE_TYPE%TYPE,
                         I_pickup_loc              IN     ORDHEAD.PICKUP_LOC%TYPE,
                         I_partner_type_1          IN     ORDHEAD.PARTNER_TYPE_1%TYPE,
                         I_partner_1               IN     ORDHEAD.PARTNER1%TYPE,
                         I_partner_type_2          IN     ORDHEAD.PARTNER_TYPE_2%TYPE,
                         I_partner_2               IN     ORDHEAD.PARTNER2%TYPE,
                         I_partner_type_3          IN     ORDHEAD.PARTNER_TYPE_3%TYPE,
                         I_partner_3               IN     ORDHEAD.PARTNER3%TYPE,
                         I_factory                 IN     ORDHEAD.FACTORY%TYPE,
                         I_sup_inv_mgmt_rowid      IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program           VARCHAR2(30) := 'FILL_SUP_STRUCT';
   
   O_error_message     VARCHAR2(2000);

BEGIN

   if I_qc_ind IS NULL then
      I_get_sup_info.qc_ind := I_qc_ind;
   else
      I_get_sup_info.qc_ind := 'N'; /* default req'd qc_ind to 'N' in ordhead */
   end if;

   I_get_sup_info.terms := I_terms;
   I_get_sup_info.freight_terms := I_freight_terms;

   if (I_payment_method IS NOT NULL) then
      I_get_sup_info.payment_method := I_payment_method;
   else
      I_get_sup_info.payment_method := NULL;
   end if;

   if (I_ship_method IS NOT NULL) then
      I_get_sup_info.ship_method := I_ship_method;
   else
      I_get_sup_info.ship_method := NULL;
   end if;  

   I_get_sup_info.edi_po_ind := I_edi_po_ind;
   I_get_sup_info.currency_code := I_currency_code;
   I_get_sup_info.pre_mark_ind := I_pre_mark_ind;
   I_get_sup_info.inv_mgmt_lvl := I_inv_mgmt_lvl;
   I_get_sup_info.dept_level_ord := I_dept_level_ord;

   if (I_agent IS NOT NULL) then
      I_get_sup_info.agent := I_agent;
   else
      I_get_sup_info.agent := NULL;
   end if;

   if (I_discharge_port IS NOT NULL) then
      I_get_sup_info.discharge_port := I_discharge_port;
   else
      I_get_sup_info.discharge_port := NULL;
   end if;

   if (i_lading_port IS NOT NULL) then
      I_get_sup_info.lading_port := I_lading_port;
   else
      I_get_sup_info.lading_port := NULL;
   end if;

   I_get_sup_info.seq_no := I_seq_no;

   if (i_buyer IS NOT NULL) then
      I_get_sup_info.buyer := I_buyer;
   else
      I_get_sup_info.buyer := NULL;
   end if;

   if I_contract = '-1' then
      I_get_sup_info.single_loc_ind := I_single_loc_ind;
   else
      I_get_sup_info.single_loc_ind := 'N';
   end if;

   I_get_sup_info.due_ord_process_ind := I_due_ord_process_ind;
   I_get_sup_info.due_ord_ind := NULL;

   if (i_due_ord_lvl IS NOT NULL) then
      I_get_sup_info.due_ord_lvl := I_due_ord_lvl;
   else
      I_get_sup_info.due_ord_lvl := NULL;
   end if;

   I_get_sup_info.non_due_ord_create_ind := I_non_due_ord_create_ind;

   if (I_purchase_type IS NOT NULL) then
      I_get_sup_info.purchase_type := I_purchase_type;
   else
      I_get_sup_info.purchase_type := NULL;
   end if;

   if (I_pickup_loc IS NOT NULL) then
      I_get_sup_info.pickup_loc := I_pickup_loc;
   else
      I_get_sup_info.pickup_loc := NULL;
   end if;

   I_get_sup_info.sup_inv_mgmt_rowid := I_sup_inv_mgmt_rowid;
   I_get_sup_info.ord_inv_mgmt_rowid := NULL;

   if (I_min_cnstr_lvl IS NOT NULL) then
      I_get_sup_info.min_cnstr_lvl := I_min_cnstr_lvl;
   else
      I_get_sup_info.min_cnstr_lvl := NULL;
   end if;

   I_get_sup_info.scale_cnstr_ind := I_scale_cnstr_ind;
   I_get_sup_info.truck_split_ind := I_truck_split_ind;

   if (I_truck_method IS NOT NULL) then
      I_get_sup_info.truck_method := I_truck_method;
   else
      I_get_sup_info.truck_method := NULL;
   end if;

   if(I_cnst_type1 IS NOT NULL) then
      I_get_sup_info.cnst_type(1) := I_cnst_type1;
   else
      I_get_sup_info.cnst_type(1) := NULL;
   end if;

   if(I_cnst_uom1 IS NOT NULL) then
      I_get_sup_info.cnst_uom(1) := I_cnst_uom1;
   else
      I_get_sup_info.cnst_uom(1) := NULL;
   end if;

   if(I_cnst_value1 IS NOT NULL) then
      I_get_sup_info.cnst_value(1) := I_cnst_value1;
   else
      I_get_sup_info.cnst_value(1) := 0;
   end if;

   if(I_cnst_threshold1 IS NOT NULL) then
      I_get_sup_info.cnst_threshold(1) := I_cnst_threshold1;
   else
      I_get_sup_info.cnst_threshold(1) := 0;
   end if;

   if(I_cnst_type2 IS NOT NULL) then
      I_get_sup_info.cnst_type(2):= I_cnst_type2;
   else
      I_get_sup_info.cnst_type(2) := NULL;
   end if;

   if(I_cnst_uom2 IS NOT NULL) then
      I_get_sup_info.cnst_uom(2) := I_cnst_uom2;
   else
      I_get_sup_info.cnst_uom(2) := NULL;
   end if;

   if(I_cnst_value2 IS NOT NULL) then
      I_get_sup_info.cnst_value(2) := I_cnst_value2;
   else
      I_get_sup_info.cnst_value(2) := 0;
   end if;

   if(I_cnst_threshold2 IS NOT NULL) then
      I_get_sup_info.cnst_threshold(2) := I_cnst_threshold2;
   else
      I_get_sup_info.cnst_threshold(2) := 0;
   end if;

   if(I_partner_type_1 IS NOT NULL) then
      I_get_sup_info.partner_type_1 := I_partner_type_1;
   else
      I_get_sup_info.partner_type_1 := NULL;
   end if;

   if(I_partner_1 IS NOT NULL) then
      I_get_sup_info.partner_1 := I_partner_1;
   else
      I_get_sup_info.partner_1 := NULL;
   end if;

   if(I_partner_type_2 IS NOT NULL) then
      I_get_sup_info.partner_type_2 := I_partner_type_2;
   else
      I_get_sup_info.partner_type_2 := NULL;
   end if;

   if(I_partner_2 IS NOT NULL) then
      I_get_sup_info.partner_2 := I_partner_2;
   else
      I_get_sup_info.partner_2 := NULL;
   end if;

   if(I_partner_type_3 IS NOT NULL) then
      I_get_sup_info.partner_type_3 := I_partner_type_3;
   else
      I_get_sup_info.partner_type_3 := NULL;
   end if;

   if(I_partner_3 IS NOT NULL) then
      I_get_sup_info.partner_3 := I_partner_3;
   else
      I_get_sup_info.partner_3 := NULL;
   end if;

   if(I_factory IS NOT NULL) then
      I_get_sup_info.factory := I_factory;
   else
      I_get_sup_info.factory := NULL;
   end if;

   I_get_sup_info.total_cnst_qty(1) := 0;
   I_get_sup_info.total_cnst_qty(2) := 0;
   I_get_sup_info.cnst_value_reset(1) := 0;
   I_get_sup_info.cnst_value_reset(2) := 0;
   I_get_sup_info.ltl_flag := UNKNOWN_TRUCK;
   I_get_sup_info.cnst_index := -1;
   
   return TRUE;
EXCEPTION
when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
   return FALSE;
END FILL_SUP_STRUCT; /*end fill_sup_struct*/
----------------------------------------------------------------------------------
END ADD_LINE_ITEM_SQL;
/