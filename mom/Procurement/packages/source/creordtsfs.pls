



CREATE OR REPLACE PACKAGE CREATE_ORD_TSF_SQL AUTHID CURRENT_USER AS

   TYPE ord_rec is RECORD (supplier         SUPS.SUPPLIER%TYPE,
                           item             ITEM_MASTER.ITEM%TYPE,
                           need_qty         STORE_ORDERS.NEED_QTY%TYPE,
                           need_date        STORE_ORDERS.NEED_DATE%TYPE,
                           item_loc_status  ITEM_LOC.STATUS%TYPE,
                           costing_loc      ITEM_LOC.COSTING_LOC%TYPE,
                           costing_loc_type ITEM_LOC.COSTING_LOC_TYPE%TYPE);

   TYPE tsf_rec is RECORD (tsf_no     TSFHEAD.TSF_NO%TYPE,
                           item       ITEM_MASTER.ITEM%TYPE,
                           dept       ITEM_MASTER.DEPT%TYPE,
                           from_loc   TSFHEAD.FROM_LOC%TYPE,
                           to_loc     TSFHEAD.TO_LOC%TYPE,
                           cost       ITEM_LOC_SOH.UNIT_COST%TYPE,
                           retail     ITEM_LOC.UNIT_RETAIL%TYPE,
                           need_qty   STORE_ORDERS.NEED_QTY%TYPE,
                           need_date  STORE_ORDERS.NEED_DATE%TYPE,
                           appr_ind   VARCHAR2(1));

   TYPE item_tbl_tsf is TABLE of tsf_rec INDEX BY BINARY_INTEGER;
   TYPE item_tbl_ord is TABLE of ord_rec INDEX BY BINARY_INTEGER;
---------------------------------------------------------------------------
-- Name:    CREATE_ORD_TSF
-- Purpose: This function is used to create new orders or transfers to fulfill.
--          item requirements coming in from SIM.
----------------------------------------------------------------------------
FUNCTION CREATE_ORD_TSF (O_error_message  IN OUT  VARCHAR2,
                         I_store          IN      ITEM_LOC.LOC%TYPE,
                         I_item_tbl       IN      INV_REQUEST_SQL.ITEM_TBL)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Name:    CREATE_TSF_IMP_EXP
-- Purpose: This function is used to insert new transfers into tsfhead table.
--          coming in from importer/exporter entity.
----------------------------------------------------------------------------
FUNCTION CREATE_TSF_IMP_EXP (O_error_message      IN OUT   VARCHAR2,
                             O_tsf_no             IN OUT   TSFHEAD.TSF_NO%TYPE,
                             I_create_tsf         IN OUT   VARCHAR2,
                             I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                             I_item_tbl           IN       ITEM_TBL_TSF,
                             O_total_chrgs_prim   IN OUT   ITEM_EXP_DETAIL.EST_EXP_VALUE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Name:    CREATE_TSF_DETAIL
-- Purpose: This function is used to insert new transfers into tsfdetail table.
--          coming in from importer/exporter entity.
----------------------------------------------------------------------------
FUNCTION CREATE_TSF_DETAIL (O_error_message  IN OUT  VARCHAR2,
                            I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                            I_sg_tsf_ind     IN      VARCHAR2,
                            I_tsf_dtl_rec    IN      TSF_REC)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Name:    BUILD_TRAN_DATA_SG
-- Purpose: This function is used to insert tran code 37 and 38 for new
--          create System Generated transfer coming in from importer/exporter entity.
----------------------------------------------------------------------------
FUNCTION BUILD_TRAN_DATA_SG (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_item            IN       ITEM_MASTER.ITEM%TYPE,
                             I_cost            IN       TRAN_DATA.TOTAL_COST%TYPE,
                             I_retail          IN       TRAN_DATA.TOTAL_RETAIL%TYPE,
                             I_qty             IN       TRAN_DATA.UNITS%TYPE,
                             I_distro_no       IN       TSFHEAD.TSF_NO%TYPE,
                             I_ref_pack_no     IN       TRAN_DATA.REF_PACK_NO%TYPE,
							 I_tran_date       IN       DATE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Name:    BUILD_ADJ_TRAN_DATA_SG
-- Purpose: This function is used to insert tran code 37 and 38 for
--          adjusted System Generated transfer coming in from importer/exporter entity.
----------------------------------------------------------------------------
FUNCTION BUILD_ADJ_TRAN_DATA_SG (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                                 I_item            IN       ITEM_MASTER.ITEM%TYPE,
                                 I_cost            IN       TRAN_DATA.TOTAL_COST%TYPE,
                                 I_retail          IN       TRAN_DATA.TOTAL_RETAIL%TYPE,
                                 I_qty             IN       TRAN_DATA.UNITS%TYPE,
                                 I_ref_pack_no     IN       TRAN_DATA.REF_PACK_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
-- Name:    UPDATE_SG_TSF_DETAIL
-- Purpose: This function is used to update the transfer records for System Generated
--          transfers when cost adjustment is done on the order.
----------------------------------------------------------------------------
FUNCTION UPDATE_SG_TSF_DETAIL (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no       IN      TSFHEAD.ORDER_NO%TYPE,
                               I_location       IN      TSFHEAD.TO_LOC%TYPE,
                               I_loc_type       IN      TSFHEAD.TO_LOC_TYPE%TYPE,
                               I_item           IN      TSFDETAIL.ITEM%TYPE,
                               I_new_cost       IN      TSFDETAIL.TSF_PRICE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------
END CREATE_ORD_TSF_SQL;
/
