
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE TIMELINE_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_TMLN_NO
-- Purpose      : Automatically generate the next timeline number on the timeline table.
---------------------------------------------------------------------------------------------
FUNCTION NEXT_TMLN_NO( O_timeline_no   IN OUT timeline_head.timeline_no%TYPE,
                       O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: NEXT_STEP_NO
-- Purpose      : Automatically generate the next timeline step number given a timeline number.
---------------------------------------------------------------------------------------------
FUNCTION NEXT_STEP_NO( O_step_no       IN OUT timeline_step_comp.step_no%TYPE,
                       O_error_message IN OUT VARCHAR2,
                       I_timeline_type IN     timeline_step_comp.timeline_type%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_BASE_DESC
-- Purpose      : Get the timeline_base description from the code_detail table given the
--                timeline number.
---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_DESC( I_timeline_no   IN     timeline_head.timeline_no%TYPE,
                        O_base_code     IN OUT timeline_head.timeline_base%TYPE,
                        O_base_desc     IN OUT code_detail.code_desc%TYPE,
                        O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_TMLN_DESC
-- Purpose      : Get the timeline description from the timeline table given the timeline number.
---------------------------------------------------------------------------------------------
FUNCTION GET_TMLN_DESC( I_timeline_no      IN     timeline_head.timeline_no%TYPE,
                        O_timeline_desc    IN OUT timeline_head.timeline_desc%TYPE,
                        O_error_message    IN OUT VARCHAR2)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_STEP_DESC
-- Purpose      : This function fetch the Step Description(s) from Timeline_Steps table given
--                the Timeline_No and Step_No.
---------------------------------------------------------------------------------------------
FUNCTION GET_STEP_DESC  ( I_step_no       IN     timeline_step_comp.step_no%TYPE,
                          I_timeline_type IN     timeline_step_comp.timeline_type%TYPE,
                          O_step_desc     IN OUT timeline_step_comp.step_desc%TYPE,
                          O_error_message IN OUT VARCHAR2 )
   RETURN BOOLEAN; 

---------------------------------------------------------------------------------------------
-- Function Name: GET_ORIG_DATE
-- Purpose      : This function fetch the Days_Completed from Timeline_Steps table given the
--                Timeline_No and Step_No. Then calculate the Original Dates for each Timeline
--                Step.
---------------------------------------------------------------------------------------------
FUNCTION GET_ORIG_DATE   ( I_timeline_no   IN     timeline_steps.timeline_no%TYPE,
                           I_step_no       IN     timeline_steps.step_no%TYPE,
                           I_timeline_type IN     timeline_steps.timeline_type%TYPE,
                           I_base_date     IN     DATE,
                           O_orig_date     IN OUT DATE,
                           O_error_message IN OUT VARCHAR2 )
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_BASE_DATE
-- Purpose      : Get the timeline base date from the ordhead table given the timeline base code.
---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_DATE( I_order_no      IN     ordhead.order_no%TYPE,
                        I_timeline_base IN     timeline_head.timeline_base%TYPE,
                        O_base_date     IN OUT DATE,
                        O_error_message IN OUT VARCHAR2)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: GET_BASE_DATE_FROM_ORIG
-- Purpose      : Get the timeline base date from the Original Date and the Days Completed.
---------------------------------------------------------------------------------------------
FUNCTION GET_BASE_DATE_FROM_ORIG( O_error_message IN OUT VARCHAR2,
				  O_base_date     IN OUT DATE,
				  I_timeline_no   IN     timeline_steps.timeline_no%TYPE,
				  I_step_no  	   IN     timeline_steps.step_no%TYPE,
				  I_timeline_type IN     timeline_steps.timeline_type%TYPE,
				  I_orig_date     IN     DATE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_STEP
-- Purpose      : Validate that Step_No is on the timeline table.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STEP( O_error_message IN OUT 	VARCHAR2,
         O_exists       IN OUT BOOLEAN,
         I_step_no      IN     timeline.step_no%TYPE,
			I_timeline_no	IN     timeline.timeline_no%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_STEP_TYPE
-- Purpose      : Determine to which timeline type the step number belongs.
-- Called in    : FM_TIMESTEP
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STEP_TYPE( O_error_message IN OUT VARCHAR2,
                             O_exists        IN OUT BOOLEAN,
                             O_step_desc     IN OUT TIMELINE_STEP_COMP.STEP_DESC%TYPE,
                             I_timeline_type IN     TIMELINE_STEP_COMP.TIMELINE_TYPE%TYPE,
                             I_step_no       IN     TIMELINE_STEP_COMP.STEP_NO%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: TIMELINE_COUNT
-- Purpose      : Returns either a 1 or 2 depending on whether the number of timelines
--                associated with the order number is 1 or greater.
---------------------------------------------------------------------------------------------
FUNCTION TIMELINE_COUNT(O_error_message IN OUT 	VARCHAR2,
			O_num_timelines	IN OUT 	NUMBER,
			O_timeline_no 	IN OUT	timeline.timeline_no%TYPE,
			I_key_value_1	IN 	timeline.key_value_1%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: TIMELINE_DATES
-- Purpose      : Returns ORIGINAL,REVISED, and ACTUAL dates for the input step.
---------------------------------------------------------------------------------------------
FUNCTION TIMELINE_DATES(O_error_message IN OUT 	VARCHAR2,
			O_original_date IN OUT	DATE,
			O_revised_date	IN OUT	DATE,
			O_actual_date	IN OUT	DATE,
			I_timeline_no 	IN 	timeline.timeline_no%TYPE,
			I_key_value_1	IN 	timeline.key_value_1%TYPE,
			I_key_value_2	IN 	timeline.key_value_2%TYPE,
			I_step_no	IN	timeline.step_no%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  TIMELINES_EXIST
-- Purpose      :  Checks the existence of timelines with a given timeline type,
--                 key value 1 and key value 2.
---------------------------------------------------------------------------------------------
FUNCTION TIMELINES_EXIST(O_error_message  IN OUT  VARCHAR2,
                         O_exists         IN OUT  BOOLEAN,
                         I_timeline_type  IN      TIMELINE.TIMELINE_TYPE%TYPE,
                         I_key_value_1    IN      TIMELINE.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN      TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name :  LOCK_TIMELINES
-- Purpose       :  to lock timeline records according to timeline type,
--                  key value 1 and key value 1 prior to deletion
---------------------------------------------------------------------------------------------
FUNCTION LOCK_TIMELINES (O_error_message  IN OUT  VARCHAR2,
                         I_timeline_type  IN      TIMELINE.TIMELINE_TYPE%TYPE,
                         I_key_value_1    IN      TIMELINE.KEY_VALUE_1%TYPE,
                         I_key_value_2    IN      TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  DELETE_TIMELINES
-- Purpose      :  to delete timelines according to timeline type, key value
--                 1 and key value 2 after the locking logic has been called.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_TIMELINES (O_error_message   IN OUT VARCHAR2,
                           I_timeline_type   IN     TIMELINE.TIMELINE_TYPE%TYPE,
                           I_key_value_1     IN     TIMELINE.KEY_VALUE_1%TYPE,
                           I_key_value_2     IN     TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name:  GET_DEFAULTS
-- Purpose      :  to default timelines from a specific timeline type
--                 to another timeline type.
---------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULTS (O_error_message      IN OUT VARCHAR2,
                       I_from_timeline_type IN     TIMELINE.TIMELINE_TYPE%TYPE,
                       I_to_timeline_type   IN     TIMELINE.TIMELINE_TYPE%TYPE,
                       I_from_key_value_1   IN     TIMELINE.KEY_VALUE_1%TYPE,
                       I_to_key_value_1     IN     TIMELINE.KEY_VALUE_1%TYPE,
                       I_from_key_value_2   IN     TIMELINE.KEY_VALUE_2%TYPE,
                       I_to_key_value_2     IN     TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CALC_DATES
-- Purpose      : Updates REVISED dates based on the input difference.
---------------------------------------------------------------------------------------------
FUNCTION CALC_DATES (O_error_message 	IN OUT 	VARCHAR2,
			       I_timeline_no 	IN 	timeline.timeline_no%TYPE,
			       I_key_value_1	IN 	timeline.key_value_1%TYPE,
			       I_key_value_2	IN 	timeline.key_value_2%TYPE,
			       I_difference	IN	NUMBER)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: CALC_DATES_NEW
-- Purpose      : Updates REVISED dates based on the input difference when recalculation 
--                is based on an actual date.
---------------------------------------------------------------------------------------------
FUNCTION CALC_DATES_NEW (O_error_message 	IN OUT 	VARCHAR2,
			       I_timeline_no 	IN 	timeline.timeline_no%TYPE,
			       I_key_value_1	IN 	timeline.key_value_1%TYPE,
			       I_key_value_2	IN 	timeline.key_value_2%TYPE,
			       I_difference	IN	NUMBER,
                         I_step_no         IN    timeline.step_no%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-- Function Name:  DELETE_SELECTED_TIMELINE
-- Purpose      :  to delete currently selected timeline according using the unique
--                 timeline_key value
---------------------------------------------------------------------------------------------
FUNCTION DELETE_SELECTED_TIMELINE(O_error_message   IN OUT VARCHAR2,
                                  I_timeline_no     IN     TIMELINE.TIMELINE_NO%TYPE,
                                  I_key_value_1     IN     TIMELINE.KEY_VALUE_1%TYPE,
                                  I_key_value_2     IN     TIMELINE.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: INSERT_DOWN_PARENT
-- Purpose      : To copy the children's information with the parent's 
--                at the time of creation of a child item.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_DOWN_PARENT (O_error_message  IN OUT VARCHAR2,
                             I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: COPY_DOWN_PARENT
-- Purpose      : To overwrite the children's information with the parent's when 
--                parent timeline records entered or modified.
---------------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT (O_error_message  IN OUT VARCHAR2,
                           I_item           IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_STEP_TYPE
-- Purpose      : Determine to which timeline type the step number belongs.
-- Called in    : FM_TIMESCTO
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STEP_TYPE( O_error_message IN OUT VARCHAR2,
                             O_exists	     IN OUT BOOLEAN,
                             I_timeline_type IN     TIMELINE_STEP_COMP.TIMELINE_TYPE%TYPE,
                             I_step_no       IN     TIMELINE_STEP_COMP.STEP_NO%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
END;
/


