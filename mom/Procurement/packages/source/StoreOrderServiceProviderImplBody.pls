/******************************************************************************
* Service Name     : StoreOrderService
* Namespace        : http://www.oracle.com/retail/rms/integration/services/StoreOrderService/v1
* Description      : StoreOrder web service
* 
*******************************************************************************/
CREATE OR REPLACE PACKAGE BODY StoreOrderServiceProviderImpl AS


/******************************************************************************
 *
 * Operation       : createLocPOTsfDesc
 * Description     : 
        Create a PO or Transfer in RMS based on request information from SIM.
                 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the PO/Transfer information.
        
 * 
 * Output          : "RIB_LocPOTsfRef_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfRef/v1
 * Description     : 
          The output object consists of either the order number or the transfer number created in RMS.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createLocPOTsfDesc(I_serviceOperationContext IN OUT "RIB_ServiceOpContext_REC",
                             I_businessObject          IN     "RIB_LocPOTsfDesc_REC",
                             O_serviceOperationStatus  OUT    "RIB_ServiceOpStatus_REC",
                             O_businessObject          OUT    "RIB_LocPOTsfRef_REC")
    IS
   L_program                  VARCHAR2(60)            := 'StoreOrderServiceProviderImpl.createLocPOTsfDesc';
   L_status                   "RIB_SuccessStatus_REC" := NULL;
   L_success_message          RTK_ERRORS.RTK_TEXT%TYPE :='createLocPOTsfDesc service call was successful.';
   L_error_message            RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code              VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL        "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                     "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL           "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;

   SVCPROV_STOREORDER.CREATE_LOC_PO_TSF(O_serviceOperationStatus,
                                           O_businessObject,
                                           I_businessObject);
   -- For any error in the call to CREATE_LOC_PO_TSF, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.
   
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END createLocPOTsfDesc;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deleteLocPOTsfDesc
 * Description     : 
        Delete a PO or Transfer in RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the PO/Transfer to be deleted.
        
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
          InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deleteLocPOTsfDesc(
                          I_serviceOperationContext  IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject           IN     "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus   OUT    "RIB_ServiceOpStatus_REC",
                          O_businessObject           OUT "RIB_InvocationSuccess_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.deleteLocPOTsfDesc';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='deleteLocPOTsfDesc service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();   
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   
   SVCPROV_STOREORDER.DELETE_LOC_PO_TSF(O_serviceOperationStatus,
                                        I_businessObject);
   -- For any error in the call to DELETE_LOC_PO_TSF, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.
   
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
      O_businessObject := "RIB_InvocationSuccess_REC"(0,L_success_message);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END deleteLocPOTsfDesc;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : modifyLocPOTsfDesc
 * Description     : 
        Modify a PO or Transfer in RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the PO/Transfer information.
        
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
          InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE modifyLocPOTsfDesc(
                          I_serviceOperationContext  IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject           IN     "RIB_LocPOTsfDesc_REC",
                          O_serviceOperationStatus   OUT    "RIB_ServiceOpStatus_REC",
                          O_businessObject           OUT    "RIB_InvocationSuccess_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.modifyLocPOTsfDesc';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='modifyLocPOTsfDesc service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.MOD_LOC_PO_TSF(O_serviceOperationStatus,
                                     I_businessObject);
                                     
   -- For any error in the call to MOD_LOC_PO_TSF, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
      O_businessObject := "RIB_InvocationSuccess_REC"(0,L_success_message);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END modifyLocPOTsfDesc;

/******************************************************************************/



/******************************************************************************
 *
 * Operation       : createDetailLocPOTsfDesc
 * Description     : 
        Create PO/Transfer Details in RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the PO/Transfer information.
        
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
          InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE createDetailLocPOTsfDesc(
                              I_serviceOperationContext  IN OUT "RIB_ServiceOpContext_REC",
                              I_businessObject           IN  "RIB_LocPOTsfDesc_REC",
                              O_serviceOperationStatus   OUT "RIB_ServiceOpStatus_REC",
                              O_businessObject           OUT "RIB_InvocationSuccess_REC")
    IS                   
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.createDetailLocPOTsfDesc';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='createDetailLocPOTsfDesc service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   O_serviceOperationStatus  :="RIB_ServiceOpStatus_REC"(0,NULL);
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.CREATE_LOC_PO_TSF_DETAIL(O_serviceOperationStatus,
                                               I_businessObject);
   
   -- For any error in the call to CREATE_LOC_PO_TSF_DETAIL, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
      O_businessObject := "RIB_InvocationSuccess_REC"(0,L_success_message);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END createDetailLocPOTsfDesc;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : deleteDetailLocPOTsfDesc
 * Description     : 
        Delete PO/Transfer Details in RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the PO/Transfer information.
        
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
          InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE deleteDetailLocPOTsfDesc(
                                I_serviceOperationContext  IN OUT  "RIB_ServiceOpContext_REC",
                                I_businessObject           IN      "RIB_LocPOTsfDesc_REC", 
                                O_serviceOperationStatus   OUT     "RIB_ServiceOpStatus_REC",
                                O_businessObject           OUT     "RIB_InvocationSuccess_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.deleteDetailLocPOTsfDesc';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='deleteDetailLocPOTsfDesc service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.DELETE_LOC_PO_TSF_DETAIL(O_serviceOperationStatus,
                                               I_businessObject);
   
   -- For any error in the call to DELETE_LOC_PO_TSF_DETAIL, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
      O_businessObject := "RIB_InvocationSuccess_REC"(0,L_success_message);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END deleteDetailLocPOTsfDesc;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : modifyDetailLocPOTsfDesc
 * Description     : 
        Modify PO/Transfer Details in RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the PO/Transfer information.
        
 * 
 * Output          : "RIB_InvocationSuccess_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/InvocationSuccess/v1
 * Description     : 
          InvocationSuccess object contains the success or failure status of processing the 
                              confirmation requests.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE modifyDetailLocPOTsfDesc(
                                I_serviceOperationContext  IN OUT "RIB_ServiceOpContext_REC",
                                I_businessObject           IN     "RIB_LocPOTsfDesc_REC",
                                O_serviceOperationStatus   OUT    "RIB_ServiceOpStatus_REC",
                                O_businessObject           OUT    "RIB_InvocationSuccess_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.modifyDetailLocPOTsfDesc';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='modifyDetailLocPOTsfDesc service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.MOD_LOC_PO_TSF_DETAIL(O_serviceOperationStatus,
                                            I_businessObject);
   -- For any error in the call to MOD_LOC_PO_TSF_DETAIL, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
      O_businessObject := "RIB_InvocationSuccess_REC"(0,L_success_message);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END modifyDetailLocPOTsfDesc;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryDeal
 * Description     : 
        Retrieve Deals Information from RMS
                 
 * 
 * Input           : "RIB_LocPOTsfDealsCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDealsCriVo/v1
 * Description     : 
          This object consists of the store, item for which the Deals information need to be retrieved.
        
 * 
 * Output          : "RIB_LocPOTsfDealsColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDealsColDesc/v1
 * Description     : 
          This object consists of the deals for the input store,item.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryDeal(
                    I_serviceOperationContext  IN OUT  "RIB_ServiceOpContext_REC",
                    I_businessObject           IN      "RIB_LocPOTsfDealsCriVo_REC",
                    O_serviceOperationStatus   OUT     "RIB_ServiceOpStatus_REC",
                    O_businessObject           OUT     "RIB_LocPOTsfDealsColDesc_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.queryDeal';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='queryDeal service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_DEALS(O_serviceOperationStatus,
                                                O_businessObject,
                                                I_businessObject);
   
   -- For any error in the call to QUERY_LOC_PO_TSF_DEALS, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END queryDeal;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryItemSales
 * Description     : 
        Retrieve the Item Sales information from RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfItmSlsCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfItmSlsCriVo/v1
 * Description     : 
          This object consists of the input Store,Item for which sales information need to be retrieved.
        
 * 
 * Output          : "RIB_LocPOTsfItmSlsColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfItmSlsColDesc/v1
 * Description     : 
          This object consists of the item sales information for the input store,item.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryItemSales(I_serviceOperationContext  IN OUT  "RIB_ServiceOpContext_REC",
                         I_businessObject           IN      "RIB_LocPOTsfItmSlsCriVo_REC",
                         O_serviceOperationStatus   OUT     "RIB_ServiceOpStatus_REC",
                         O_businessObject           OUT     "RIB_LocPOTsfItmSlsColDesc_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.queryItemSales';
   L_status                 "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='queryItemSales service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_ITEMSALES (O_serviceOperationStatus,
                                                  O_businessObject,
                                                  I_businessObject);
   -- For any error in the call to QUERY_LOC_PO_TSF_ITEMSALES, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program); 
END queryItemSales;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryStoreOrder
 * Description     : 
        Retrieve header level PO or transfer information from RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfHdrCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfHdrCriVo/v1
 * Description     : 
          This object contains the search criteria for PO or transfer in RMS.
        
 * 
 * Output          : "RIB_LocPOTsfHdrColDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfHdrColDesc/v1
 * Description     : 
          This object contains the header level information for POs and transfers retrieved from RMS based on the input.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryStoreOrder(
                          I_serviceOperationContext  IN OUT "RIB_ServiceOpContext_REC",
                          I_businessObject           IN     "RIB_LocPOTsfHdrCriVo_REC",
                          O_serviceOperationStatus   OUT    "RIB_ServiceOpStatus_REC",
                          O_businessObject           OUT    "RIB_LocPOTsfHdrColDesc_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.queryStoreOrder ';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='queryStoreOrder service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_HEADER(O_serviceOperationStatus,
                                                 O_businessObject,
                                                 I_businessObject);
                                                 
   -- For any error in the call to QUERY_LOC_PO_TSF_HEADER, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END queryStoreOrder;
/******************************************************************************/



/******************************************************************************
 *
 * Operation       : queryStoreOrderDetail
 * Description     : 
        Retrieve the header/details of a PO or transfer from RMS.
                 
 * 
 * Input           : "RIB_LocPOTsfDtlsCriVo_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDtlsCriVo/v1
 * Description     : 
          This object contains the search criteria for a PO or transfer in RMS, including the PO/Transfer number.
        
 * 
 * Output          : "RIB_LocPOTsfDesc_REC"
 * Namespace       : http://www.oracle.com/retail/integration/base/bo/LocPOTsfDesc/v1
 * Description     : 
          This object contains the store order details related to a PO or transfer based on the input.
        
 * 
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalArgumentWSFaultException
 * Description     : Throw this exception when a "soap:Client" side
          message problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.EntityAlreadyExistsWSFaultException
 * Description     : Throw this exception when the attempt made to
          create a object that already exists.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.IllegalStateWSFaultException
 * Description     : Throw this exception when an unknown
          "soap:Server" side problem occurs.
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : Validation fault to be thrown by all operations
 *
 * Valid errorType : com.oracle.retail.integration.services.exception.v1.ValidationWSFaultException
 * Description     : use this errorType to throw validation errors.
 
 * Valid errorType : java.lang.UnsupportedOperationException
 * Description     : If the service operation is not implemented in this release use this errorType.
 * 
 *     
 ******************************************************************************/
PROCEDURE queryStoreOrderDetail(I_serviceOperationContext  IN OUT  "RIB_ServiceOpContext_REC",
                                I_businessObject           IN      "RIB_LocPOTsfDtlsCriVo_REC",
                                O_serviceOperationStatus   OUT     "RIB_ServiceOpStatus_REC",
                                O_businessObject           OUT     "RIB_LocPOTsfDesc_REC")
    IS
   L_program                 VARCHAR2(60)             := 'StoreOrderServiceProviderImpl.queryStoreOrderDetail ';
   L_status                  "RIB_SuccessStatus_REC"  := NULL;
   L_success_message         RTK_ERRORS.RTK_TEXT%TYPE :='queryStoreOrderDetail service call was successful.';
   L_error_message           RTK_ERRORS.RTK_TEXT%TYPE;
   L_status_code             VARCHAR2(1) := API_CODES.SUCCESS;
   L_successStatus_TBL       "RIB_SuccessStatus_TBL" := "RIB_SuccessStatus_TBL"();
   L_fail                    "RIB_FailStatus_REC"    := NULL;
   L_failStatus_TBL          "RIB_FailStatus_TBL"    := "RIB_FailStatus_TBL"();
BEGIN
   if SVCPROV_CONTEXT.SET_SVCPROV_CONTEXT(O_serviceOperationStatus,
                                          I_serviceOperationContext) = FALSE then
      return;
   end if;
   SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_DETAIL(O_serviceOperationStatus,
                                                 O_businessObject,
                                                 I_businessObject);
                                                 
   -- For any error in the call to QUERY_LOC_PO_TSF_DETAIL, the FailStatus_TBL will be populated. 
   -- If the FailStatus_TBL is null, populate output service object with success message.                                     
   if O_serviceOperationStatus.FailStatus_TBL is NULL or O_serviceOperationStatus.FailStatus_TBL.count = 0 then
      L_status := "RIB_SuccessStatus_REC"(0, L_success_message);
      L_successStatus_TBL.EXTEND;
      L_successStatus_TBL(1) := L_status;
      O_serviceOperationStatus := "RIB_ServiceOpStatus_REC"(0, L_successStatus_TBL);
   end if;
EXCEPTION
   when OTHERS then
      L_status_code := API_CODES.UNHANDLED_ERROR;
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_program, 
                                             to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              L_status_code,
                                              L_error_message,
                                              L_program);
END queryStoreOrderDetail;
/******************************************************************************/

END StoreOrderServiceProviderImpl;
/