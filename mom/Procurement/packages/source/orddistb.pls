CREATE OR REPLACE PACKAGE BODY ORDER_DIST_SQL AS
-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_LOCATION
-- Purpose:       Validates the filter criteria for locations before distributing the order.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_qty_ind        IN OUT  VARCHAR2,
                            I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                            I_complete_ind   IN      VARCHAR2,
                            I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
                            RETURN BOOLEAN IS

   L_cursor1         INTEGER;
   L_rows_processed  INTEGER;
   L_tot_recs        NUMBER(6);
   L_tot_qty_recs    NUMBER(6);
   L_where_clause    FILTER_TEMP.WHERE_CLAUSE%TYPE;

   BEGIN
      L_where_clause := I_where_clause;

      if L_where_clause is not NULL then
         L_where_clause := ' and '||L_where_clause;
      end if;

      /* Check for records that have locations that have already been distributed. If so, return false */


      L_tot_recs := NULL;
      SQL_LIB.SET_MARK('FETCH', 'cursor1', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
      EXECUTE IMMEDIATE 'select count(*) from ordloc_wksht where location is not NULL and order_no = :l_order_no ' ||
                     L_where_clause into L_tot_recs USING I_order_no;

      /* evaluate if rows are returned */
      if L_tot_recs > 0 then
         O_error_message := ('LOC_DISTR');
         return FALSE;
      end if;

   /* If a mask has been completed check for records having quantities . If so, return false */

      if I_complete_ind = 'Y' then

         /* parse and execute the select statement */
         SQL_LIB.SET_MARK('FETCH', 'cursor2', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
         EXECUTE IMMEDIATE 'select count(*) from ordloc_wksht where act_qty > 0 and order_no = :l_order_no ' ||
                        L_where_clause into L_tot_recs USING I_order_no;

         if L_tot_recs > 0 then
            O_error_message := ('SKU_MASK_NO_CONT_QTY');
            return FALSE;
         end if;
      else         /* No mask completed, Check for all or no records having quantities. If so, return false */

         /* parse and execute the select statement */
         SQL_LIB.SET_MARK('FETCH', 'cursor3', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
         EXECUTE IMMEDIATE 'select count (*) from ordloc_wksht where order_no = :l_order_no ' ||
                        L_where_clause into L_tot_recs USING I_order_no;

         /* parse and execute the select statement */
         SQL_LIB.SET_MARK('FETCH', 'cursor4', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
         EXECUTE IMMEDIATE 'select count (*) from ordloc_wksht where act_qty > 0 and order_no = :l_order_no ' ||
                        L_where_clause into L_tot_qty_recs USING I_order_no;


         /* evaluating if both cursors return the same number of rows and L_tot_qty_recs is not zero */
       /*
        *if L_tot_recs != L_tot_qty_recs and L_tot_qty_recs != 0 then
        *   O_error_message := ('INV_DIST_QTY');
        *   return FALSE;
        *end if;
        */
      end if;

      /* Set quantity indicator to yes if all records contain quantities */

      if L_tot_recs = L_tot_qty_recs and L_tot_qty_recs > 0 then
         O_qty_ind := 'Y';
      else
         O_qty_ind := 'N';
      end if;

      return TRUE;


EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_DIST_SQL.VALIDATE_LOCATION',
                                             to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;
      RETURN FALSE;

END VALIDATE_LOCATION;

-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_STORE_GRADE
-- Purpose:       Validates the filter criteria for store grades before distributing the order.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_GRADE( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_qty_ind        IN OUT  VARCHAR2,
                               I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                               I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
                               RETURN BOOLEAN IS

   L_cursor1         INTEGER;
   L_rows_processed  INTEGER;
   L_tot_recs        NUMBER(4);
   L_tot_qty_recs    NUMBER(4);
   L_where_clause    FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_store_grade_group_id  ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE := NULL;

   BEGIN
      L_where_clause := I_where_clause;

      if L_where_clause is not NULL then
         L_where_clause := ' and '||L_where_clause;
      end if;

   /* Check for records that have store grade that have already been distributed. If so, return false */


      /* parse and execute the select statement */
      SQL_LIB.SET_MARK('FETCH', 'cursor1', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
      EXECUTE IMMEDIATE 'select count(*) from ordloc_wksht where (location is not NULL or store_grade_group_id is not NULL) and order_no = :l_order_no ' ||
                        L_where_clause into L_tot_recs USING I_order_no;

      /* evaluate if rows are returned */

      if L_tot_recs > 0 then
         O_error_message := ('LOC_GRADE_DISTR');
         --DBMS_SQL.CLOSE_CURSOR(L_cursor1);
         return FALSE;
      end if;
      L_tot_recs := NULL;

   /* Check for all or no records having quantities. If so, return false */

      /* parse and execute the select statement */
      SQL_LIB.SET_MARK('FETCH', 'cursor2', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
      EXECUTE IMMEDIATE 'select count (*) from ordloc_wksht where order_no = :l_order_no '||
                     L_where_clause into L_tot_recs USING I_order_no;

      /* parse and execute the select statement */
      SQL_LIB.SET_MARK('FETCH', 'cursor3', 'ORDLOC_WKSHT', 'order_no: '||to_char(I_order_no));
      EXECUTE IMMEDIATE 'select count (*) from ordloc_wksht where act_qty > 0 and order_no = :l_order_no ' ||
                        L_where_clause into L_tot_qty_recs USING I_order_no;

     /* evaluating if both cursors return the same number of rows and L_tot_qty_recs is not zero */

      if L_tot_recs != L_tot_qty_recs and L_tot_qty_recs != 0 then
         O_error_message := ('INV_DIST_QTY');
         return FALSE;
      end if;

      /* Set quantity indicator to yes if all records contain quantities */

      if L_tot_recs = L_tot_qty_recs and L_tot_qty_recs > 0 then
         O_qty_ind := 'Y';
      else
         O_qty_ind := 'N';
      end if;

      return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_DIST_SQL.VALIDATE_STORE_GRADE',
                                             to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;
      RETURN FALSE;
END VALIDATE_STORE_GRADE;
---------------------------------------------------------------------------------------------
--    Name: VALIDATE_DIFF
-- Purpose: Validates the filter criteria for diffs before distributing the order using
--          an individual diff.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_qty_ind        IN OUT  VARCHAR2,
                       O_diff_group     IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                       I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE,
                       I_diff_no        IN      NUMBER)
RETURN BOOLEAN IS

   L_error_message        RTK_ERRORS.RTK_TEXT%TYPE;
   L_where_clause         FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_statement            VARCHAR2(9000);
   L_dummy                VARCHAR2(1);
   L_tot_recs             NUMBER(4)  := 0;
   L_tot_qty_recs         NUMBER(4)  := 0;
   L_count                NUMBER;
   L_id_group_ind         V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;

   TYPE ORD_CURSOR is REF CURSOR;
   C_REF_CURSOR           ORD_CURSOR;

BEGIN

   --- Set the local where clause
   L_where_clause := I_where_clause;
   ---
   if L_where_clause is not NULL then
      L_where_clause := ' and ' || L_where_clause;
   end if;


   ------------------------------------------------------------------------------
   --- Check for records with item rather than item_parent.
   ------------------------------------------------------------------------------
   L_statement := 'select ''x'' '
               || '  from ordloc_wksht '
               || ' where order_no = :l_order_no '
               || '   and item is not NULL '
               || L_where_clause;

   L_dummy := NULL;
   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_dummy;
   close C_REF_CURSOR;
   ---
   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DIST_DIFF_ABOVE_TRAN');
      return FALSE;
   end if;


   ------------------------------------------------------------------------------
   --- All records must have a quantity or all records must have NO quantity.
   ------------------------------------------------------------------------------
   L_statement := ' select count(*) '
               || '   from ordloc_wksht '
               || '  where order_no = :l_order_no '
               || '    and act_qty > 0 '
               || L_where_clause;

   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_tot_qty_recs;
   close C_REF_CURSOR;

   --- If records exist with act_qty > 0 then we need to count all the records
   --- on the order and make sure the count is the same as L_tot_qty_recs.
   if L_tot_qty_recs > 0 then

      L_statement := ' select count(*) '
                  || '   from ordloc_wksht '
                  || '  where order_no = :l_order_no '
                  || L_where_clause;

      EXECUTE IMMEDIATE L_statement USING I_order_no;
      open C_REF_CURSOR for L_statement USING I_order_no;
      fetch C_REF_CURSOR into L_tot_recs;
      close C_REF_CURSOR;

      --- If the cursor counts do not return the same number of rows
      --- then return a failure.
      if L_tot_recs != L_tot_qty_recs then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DIST_QTY');
         return FALSE;
      end if;

   end if;

   --- Set quantity indicator to yes if all records contain quantities
   if L_tot_recs = L_tot_qty_recs and L_tot_qty_recs > 0 then
      O_qty_ind := 'Y';
   else
      O_qty_ind := 'N';
   end if;


   ------------------------------------------------------------------------------
   --- Check for all parents having the same diff_x by counting distinct
   --- diff_x of all item parents on the order
   ------------------------------------------------------------------------------
   L_statement := 'select count(distinct nvl(im.diff_'||I_diff_no||', ''-*-'')) '
               || '  from item_master im '
               || ' where item in (select item_parent '
               || '                  from ordloc_wksht '
               || '                 where order_no = :l_order_no '
               ||                         L_where_clause
               || '                                      )';

   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_count;
   close C_REF_CURSOR;

   --- If the diff count is > 1 then we cannot distribute by this diff.
   if NVL(L_count, 0) > 1 then
      O_error_message := SQL_LIB.CREATE_MSG('SIZE_GROUP_STYLES');
      return FALSE;
   end if;


   ------------------------------------------------------------------------------
   --- Fetch diff_x. If diff_x is not a diff group then return an error.
   ------------------------------------------------------------------------------
   L_statement := 'select nvl(im.diff_'||I_diff_no||', ''-*-''), '
               || '       vd.id_group_ind '
               || '  from item_master im, '
               || '       v_diff_id_group_type vd '
               || ' where im.diff_'||I_diff_no||' = vd.id_group '
               || '   and item in (select item_parent '
               || '                  from ordloc_wksht '
               || '                 where order_no = :l_order_no '
               ||                         L_where_clause || ')';

   EXECUTE IMMEDIATE L_statement  USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into O_diff_group,
                           L_id_group_ind;
   close C_REF_CURSOR;
   ---
   if NVL(L_id_group_ind, 'ID') != 'GROUP' then
      O_error_message := SQL_LIB.CREATE_MSG('MUST_HAVE_DIFF_GROUP', to_char(I_diff_no));
      return FALSE;
   end if;


   ------------------------------------------------------------------------------
   --- Check for records that have already been distributed by diff_x.
   ------------------------------------------------------------------------------
   L_statement := 'select ''x'' '
               || '  from ordloc_wksht '
               || ' where order_no = :l_order_no '
               || ' and diff_'||I_diff_no||' is NOT NULL '
               || L_where_clause;

   L_dummy := NULL;
   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_dummy;
   close C_REF_CURSOR;
   ---
   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DIST_DIFF_X', I_diff_no);
      return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DIST_SQL.VALIDATE_DIFF',
                                            to_char(SQLCODE));
      RETURN FALSE;
END VALIDATE_DIFF;

---------------------------------------------------------------------------------------------
--    Name: VALIDATE_DIFFS
-- Purpose: Validates the filter criteria for diffs before distributing the order using
--          multiple diffs (diff matrix form).
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFFS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_qty_ind        IN OUT  VARCHAR2,
                        O_diff_1         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        O_diff_2         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        O_diff_3         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        O_diff_4         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                        I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
RETURN BOOLEAN IS

   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   L_where_clause          FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_statement             VARCHAR2(9000);
   L_dummy                 VARCHAR2(1);
   L_tot_recs              NUMBER(4);
   L_tot_qty_recs          NUMBER(4);
   L_available_diff_count  NUMBER(1);

   --- Create a record to hold data for each diff
   TYPE DIFF_RECORD IS RECORD
      (DIFF                V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
       ID_GROUP_IND        V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE,
       COUNT               NUMBER(10));

   --- Create a table to hold data for all 4 diffs
   TYPE DIFF_TABLE IS TABLE OF DIFF_RECORD
      INDEX BY BINARY_INTEGER;
   L_diffs                DIFF_TABLE;

   TYPE ORD_CURSOR is REF CURSOR;
   C_REF_CURSOR           ORD_CURSOR;

BEGIN

   --- Set the local where clause
   L_where_clause := I_where_clause;
   ---
   if L_where_clause is not NULL then
      L_where_clause := ' and ' || L_where_clause;
   end if;

   --- Initialize the 4 records in the local table
   for x in 1..4 loop
      L_diffs(x).diff         := NULL;
      L_diffs(x).id_group_ind := NULL;
      L_diffs(x).count        := NULL;
   end loop;

   ------------------------------------------------------------------------------
   --- Check for records with item rather than item_parent.
   ------------------------------------------------------------------------------
   L_statement := 'select ''x'' '
               || '  from ordloc_wksht '
               || ' where order_no = :I_order_no '
               || '   and item is not NULL '
               ||         L_where_clause;

   L_dummy := NULL;
   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_dummy;
   close C_REF_CURSOR;

   ---
   if L_dummy is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('DIST_DIFF_ABOVE_TRAN');
      return FALSE;
   end if;

   ------------------------------------------------------------------------------
   --- All records must have a quantity or all records must have NO quantity.
   ------------------------------------------------------------------------------
   L_statement := ' select count(*) '
               || '   from ordloc_wksht '
               || '  where order_no = :l_order_no '
               || '    and act_qty > 0 '
               ||          L_where_clause;

   EXECUTE IMMEDIATE L_statement USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_tot_qty_recs;
   close C_REF_CURSOR;

   --- If records exist with act_qty > 0 then we need to count all the records
   --- on the order and make sure the count is the same as L_tot_qty_recs.
   if L_tot_qty_recs > 0 then

      L_statement := ' select count(*) '
                  || '   from ordloc_wksht '
                  || '  where order_no = :l_order_no '
                  ||          L_where_clause;

      EXECUTE IMMEDIATE L_statement USING I_order_no;
      open C_REF_CURSOR for L_statement USING I_order_no;
      fetch C_REF_CURSOR into L_tot_recs;
      close C_REF_CURSOR;

      --- If the cursor counts do not return the same number of rows
      --- then return a failure.
      if L_tot_recs != L_tot_qty_recs then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DIST_QTY');
         return FALSE;
      end if;

   end if;

   --- Set quantity indicator to yes if all records contain quantities
   if L_tot_recs = L_tot_qty_recs and L_tot_qty_recs > 0 then
      O_qty_ind := 'Y';
   else
      O_qty_ind := 'N';
   end if;

   ------------------------------------------------------------------------------
   --- Check for parents having the same diff_x by counting distinct
   --- diffs of all item parents on the order
   ------------------------------------------------------------------------------
   L_statement := 'select count(distinct nvl(im.diff_1, ''-*-'')), '
               || '       count(distinct nvl(im.diff_2, ''-*-'')), '
               || '       count(distinct nvl(im.diff_3, ''-*-'')), '
               || '       count(distinct nvl(im.diff_4, ''-*-''))  '
               || '  from item_master im '
               || ' where item in (select item_parent '
               || '                  from ordloc_wksht '
               || '                 where order_no = :l_order_no '
               ||                         L_where_clause
               || '                                      )';

   EXECUTE IMMEDIATE L_statement  USING I_order_no;
   open C_REF_CURSOR for L_statement USING I_order_no;
   fetch C_REF_CURSOR into L_diffs(1).count,
                           L_diffs(2).count,
                           L_diffs(3).count,
                           L_diffs(4).count;
   close C_REF_CURSOR;


   ------------------------------------------------------------------------------
   --- To use the diff matrix form we need to have 2 diff groups available for
   --- distribution (i.e., 2 diffs that are the same for each item_parent, in the
   --- same position, and have not been distributed by that diff yet).
   ------------------------------------------------------------------------------
   --- Loop through each diff record on the local table
   for x in 1..4 loop
      --- If the diff count is 1 that means all item_parents have that same diff
      --- in the same position, so we want to get the diff and id_group_ind.
      if L_diffs(x).count = 1 then

         L_statement := ' select im.diff_'||x||', '
                     || '        vd.id_group_ind '
                     || '   from item_master im, '
                     || '        v_diff_id_group_type vd '
                     || '  where im.diff_'||x||' = vd.id_group '
                     || '    and item in (select item_parent '
                     || '                   from ordloc_wksht '
                     || '                  where order_no = :l_order_no '
                     ||                          L_where_clause
                     || '                                      )';


         EXECUTE IMMEDIATE L_statement USING I_order_no;
         open C_REF_CURSOR for L_statement USING I_order_no;
         fetch C_REF_CURSOR into L_diffs(x).diff,
                                 L_diffs(x).id_group_ind;
         close C_REF_CURSOR;
         --- If this diff is a group then we want to check the ordloc_wksht table
         --- to see if the order has already been distributed by this diff.
         if NVL(L_diffs(x).id_group_ind,'ID') = 'GROUP' then
            L_statement := ' select ''x'' '
                        || '   from ordloc_wksht '
                        || '  where order_no = :l_order_no '
                        || '  and diff_'||x||' is NOT NULL '
                        || L_where_clause;

            L_dummy := NULL;
            EXECUTE IMMEDIATE L_statement USING I_order_no;
            open C_REF_CURSOR for L_statement USING I_order_no;
            fetch C_REF_CURSOR into L_dummy;
            close C_REF_CURSOR;

            --- If we did not find any records distributed by this diff then
            --- increment the available diff count and populate O_diff_x with this diff.
            if L_dummy is NULL then
               L_available_diff_count := NVL(L_available_diff_count,0) + 1;
               if x = 1 then
                  O_diff_1 := L_diffs(x).diff;
               elsif x = 2 then
                  O_diff_2 := L_diffs(x).diff;
               elsif x = 3 then
                  O_diff_3 := L_diffs(x).diff;
               elsif x = 4 then
                  O_diff_4 := L_diffs(x).diff;
               end if;
            end if;

         end if; -- 'GROUP'

      end if; -- count = 1

   end loop;
   --- If there are less than 2 available diffs to distribute then
   --- return an error.
   if L_available_diff_count < 2 then
         O_error_message := SQL_LIB.CREATE_MSG('DIST_DIFF_AVAILABLE');
         return FALSE;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DIST_SQL.VALIDATE_DIFFS',
                                            to_char(SQLCODE));
      RETURN FALSE;
END VALIDATE_DIFFS;
--------------------------------------------------------------------------
FUNCTION REDIST_DIFFS(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no        IN      ORDLOC.ORDER_NO%TYPE,
                      I_where_clause    IN      FILTER_TEMP.WHERE_CLAUSE%TYPE,
                      I_diff_no         IN      NUMBER)
   RETURN BOOLEAN IS

   L_program               VARCHAR2(40) := 'ORDER_DIST_SQL.REDIST_DIFFS2';
   L_where_clause          FILTER_TEMP.WHERE_CLAUSE%TYPE          := NULL;
   L_diff_clause           VARCHAR2(20);
   L_other_diff_clause     VARCHAR2(20);
   L_rejected              BOOLEAN;
   ---
   L_cursor1               INTEGER;
   L_select_statement      VARCHAR2(9000);
   L_delete                VARCHAR2(5000);

   TYPE L_redist_rec IS RECORD(
         item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE,
         diff1                ORDLOC_WKSHT.DIFF_1%TYPE,
         diff2                ORDLOC_WKSHT.DIFF_1%TYPE,
         diff3                ORDLOC_WKSHT.DIFF_1%TYPE,
         store_grade          ORDLOC_WKSHT.STORE_GRADE%TYPE,
         store_grade_grp_id   ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE,
         loc_type             ORDLOC_WKSHT.LOC_TYPE%TYPE,
         location             ORDLOC_WKSHT.LOCATION%TYPE,
         calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE,
         act_qty              ORDLOC_WKSHT.ACT_QTY%TYPE,
         orig_ctry_id         ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
         stand_uom            ORDLOC_WKSHT.STANDARD_UOM%TYPE,
         wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE,
         uop                  ORDLOC_WKSHT.UOP%TYPE,
         supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE);

   TYPE L_redist_tbl_type IS TABLE OF L_redist_rec;
   L_redist_tbl  L_redist_tbl_type;
   ---
BEGIN
   --- check input parameters
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   L_where_clause := I_where_clause;
   if L_where_clause is NOT NULL then
      L_where_clause := ' and ' || L_where_clause;
   end if;
   ---
   --- open the cursor for processing
   ---
   if I_diff_no = 1 then
      L_diff_clause := 'diff_1';
      L_other_diff_clause := 'diff_2,' || 'diff_3,' || 'diff_4';
   elsif I_diff_no = 2 then
      L_diff_clause := 'diff_2';
      L_other_diff_clause := 'diff_1,' || 'diff_3,' || 'diff_4';
   elsif I_diff_no = 3 then
      L_diff_clause := 'diff_3';
      L_other_diff_clause := 'diff_1,' || 'diff_2,' || 'diff_4';
   elsif I_diff_no = 4 then
      L_diff_clause := 'diff_4';
      L_other_diff_clause := 'diff_1,' || 'diff_2,' || 'diff_3';
   end if;


   L_select_statement := 'select distinct item_parent, ' ||
                                L_other_diff_clause || ' ,' ||
                                'store_grade, ' ||
                                'store_grade_group_id, ' ||
                                'loc_type, ' ||
                                'location, ' ||
                                'sum(calc_qty), ' ||
                                'sum(act_qty), ' ||
                                'origin_country_id, ' ||
                                'standard_uom, ' ||
                                'sum(wksht_qty), ' ||
                                'uop, ' ||
                                'supp_pack_size ' ||
                          'from ordloc_wksht ' ||
                          'where order_no = :l_order_no ' ||
                          ' and ' || L_diff_clause || ' is NOT NULL';
   if L_where_clause is NOT NULL then
      L_select_statement := L_select_statement || L_where_clause;
   end if;
   L_select_statement := L_select_statement || ' group by ' ||
                         'item_parent, ' || L_other_diff_clause || ', store_grade, ' ||
                         'store_grade_group_id, ' ||
                         'loc_type, location, origin_country_id, ' ||
                         'standard_uom, uop, supp_pack_size ';
   --- parse and execute the select statement
   EXECUTE IMMEDIATE L_select_statement BULK COLLECT into L_redist_tbl USING I_order_no;


   --- Fetch loop
   if L_redist_tbl.FIRST is NOT NULL then
      for item_int in L_redist_tbl.FIRST..L_redist_tbl.LAST
      LOOP

         BEGIN
            insert into ordloc_wksht (order_no,
                                      item_parent,
                                      diff_1,
                                      diff_2,
                                      diff_3,
                                      diff_4,
                                      store_grade,
                                      store_grade_group_id,
                                      loc_type,
                                      location,
                                      calc_qty,
                                      act_qty,
                                      origin_country_id,
                                      standard_uom,
                                      wksht_qty,
                                      uop,
                                      supp_pack_size)
                              values (I_order_no,
                                      L_redist_tbl(item_int).item_parent,
                                      decode(I_diff_no, 1, NULL, L_redist_tbl(item_int).diff1),
                                      decode(I_diff_no, 1, L_redist_tbl(item_int).diff1, 2, NULL, 3, L_redist_tbl(item_int).diff2, 4, L_redist_tbl(item_int).diff2),
                                      decode(I_diff_no, 1, L_redist_tbl(item_int).diff2, 2, L_redist_tbl(item_int).diff2, 3, NULL, 4, L_redist_tbl(item_int).diff3),
                                      decode(I_diff_no, 1, L_redist_tbl(item_int).diff3, 2, L_redist_tbl(item_int).diff3, 3, L_redist_tbl(item_int).diff3, 4, NULL),
                                      L_redist_tbl(item_int).store_grade,
                                      L_redist_tbl(item_int).store_grade_grp_id,
                                      L_redist_tbl(item_int).loc_type,
                                      L_redist_tbl(item_int).location,
                                      L_redist_tbl(item_int).calc_qty,
                                      L_redist_tbl(item_int).act_qty,
                                      L_redist_tbl(item_int).orig_ctry_id,
                                      L_redist_tbl(item_int).stand_uom,
                                      L_redist_tbl(item_int).wksht_qty,
                                      L_redist_tbl(item_int).uop,
                                      L_redist_tbl(item_int).supp_pack_size);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               if I_diff_no = 1 then
                  update ordloc_wksht
                     set wksht_qty = wksht_qty + L_redist_tbl(item_int).wksht_qty,
                         act_qty = act_qty + L_redist_tbl(item_int).act_qty,
                         calc_qty = calc_qty + L_redist_tbl(item_int).calc_qty
                   where order_no = I_order_no
                     and item_parent = L_redist_tbl(item_int).item_parent
                     and diff_1 is NULL
                     and ((diff_2 = L_redist_tbl(item_int).diff1 or diff_2 is NULL)
                     and (diff_3 = L_redist_tbl(item_int).diff2 or diff_3 is NULL)
                     and (diff_4 = L_redist_tbl(item_int).diff3 or diff_4 is NULL));
               elsif I_diff_no = 2 then
                  update ordloc_wksht
                     set wksht_qty = wksht_qty + L_redist_tbl(item_int).wksht_qty,
                         act_qty = act_qty + L_redist_tbl(item_int).act_qty,
                         calc_qty = calc_qty + L_redist_tbl(item_int).calc_qty
                   where order_no = I_order_no
                     and item_parent = L_redist_tbl(item_int).item_parent
                     and ((diff_1 = L_redist_tbl(item_int).diff1 or diff_1 is NULL)
                     and (diff_3 = L_redist_tbl(item_int).diff2 or diff_3 is NULL)
                     and (diff_4 = L_redist_tbl(item_int).diff3 or diff_4 is NULL))
                     and diff_2 is NULL;
               elsif I_diff_no = 3 then
                  update ordloc_wksht
                     set wksht_qty = wksht_qty + L_redist_tbl(item_int).wksht_qty,
                         act_qty = act_qty + L_redist_tbl(item_int).act_qty,
                         calc_qty = calc_qty + L_redist_tbl(item_int).calc_qty
                   where order_no = I_order_no
                     and item_parent = L_redist_tbl(item_int).item_parent
                     and ((diff_1 = L_redist_tbl(item_int).diff1 or diff_1 is NULL)
                     and (diff_2 = L_redist_tbl(item_int).diff2 or diff_2 is NULL)
                     and (diff_4 = L_redist_tbl(item_int).diff3 or diff_4 is NULL))
                     and diff_3 is NULL;
               elsif I_diff_no = 4 then
                  update ordloc_wksht
                     set wksht_qty = wksht_qty + L_redist_tbl(item_int).wksht_qty,
                         act_qty = act_qty + L_redist_tbl(item_int).act_qty,
                         calc_qty = calc_qty + L_redist_tbl(item_int).calc_qty
                   where order_no = I_order_no
                     and item_parent = L_redist_tbl(item_int).item_parent
                     and ((diff_1 = L_redist_tbl(item_int).diff1 or diff_1 is NULL)
                     and (diff_2 = L_redist_tbl(item_int).diff2 or diff_2 is NULL)
                     and (diff_3 = L_redist_tbl(item_int).diff3 or diff_3 is NULL))
                     and diff_4 is NULL;
               end if;
         END;
         ---
      END LOOP;
   end if;

   ---
   L_delete := 'delete from ordloc_wksht ' ||
               'where order_no = :l_order_no ' ||
               ' and ' || L_diff_clause || ' is NOT NULL ' ||
                L_where_clause;
   ---
   EXECUTE IMMEDIATE L_delete  USING I_order_no;

   --- If the item parent has a diff ID as one of its diffs we need to make sure
   --- that field isn't null after redistributing.
   --- the where clause can be ignored because doing this update on all records is ok.
   if I_diff_no = 1 then
      update ordloc_wksht ow
         set diff_1 = NVL((select d.diff_id
                             from diff_ids d,
                                  item_master im
                            where im.item = ow.item_parent
                              and im.diff_1 = d.diff_id), diff_1)
       where ow.order_no = I_order_no;
   elsif I_diff_no = 2 then
      update ordloc_wksht ow
         set diff_2 = NVL((select d.diff_id
                             from diff_ids d,
                                  item_master im
                            where im.item = ow.item_parent
                              and im.diff_2 = d.diff_id), diff_2)
       where ow.order_no = I_order_no;
   elsif I_diff_no = 3 then
      update ordloc_wksht ow
         set diff_3 = NVL((select d.diff_id
                             from diff_ids d,
                                  item_master im
                            where im.item = ow.item_parent
                              and im.diff_3 = d.diff_id), diff_3)
       where ow.order_no = I_order_no;
   elsif I_diff_no = 4 then
      update ordloc_wksht ow
         set diff_4 = NVL((select d.diff_id
                             from diff_ids d,
                                  item_master im
                            where im.item = ow.item_parent
                              and im.diff_4 = d.diff_id), diff_4)
       where ow.order_no = I_order_no;
   end if;

   if NOT ORDER_SETUP_SQL.POP_CHECK_CHILD(O_error_message,
                                          L_rejected, -- this won't happen.
                                          I_order_no) then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;
      ---
      return FALSE;
END REDIST_DIFFS;
--------------------------------------------------------------------------------------------
FUNCTION REDIST_STORE_GRADE
         (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
          I_order_no        IN      ORDLOC.ORDER_NO%TYPE,
          I_where_clause    IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40) := 'ORDER_DIST_SQL.REDIST_STORE_GRADE';
   L_where_clause       FILTER_TEMP.WHERE_CLAUSE%TYPE       := NULL;
   ---
   L_cursor1            INTEGER;
   L_select_statement   VARCHAR2(9000);
   L_delete             VARCHAR2(5000);
   TYPE L_redist_grd_rec IS RECORD(
      item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE,
      item                 ORDLOC_WKSHT.ITEM%TYPE,
      ref_item             ORDLOC_WKSHT.REF_ITEM%TYPE,
      diff1                ORDLOC_WKSHT.DIFF_1%TYPE,
      diff2                ORDLOC_WKSHT.DIFF_2%TYPE,
      diff3                ORDLOC_WKSHT.DIFF_3%TYPE,
      diff4                ORDLOC_WKSHT.DIFF_4%TYPE,
      loc_type             ORDLOC_WKSHT.LOC_TYPE%TYPE,
      location             ORDLOC_WKSHT.LOCATION%TYPE,
      calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE,
      act_qty              ORDLOC_WKSHT.ACT_QTY%TYPE,
      orig_ctry_id         ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
      stand_uom            ORDLOC_WKSHT.STANDARD_UOM%TYPE,
      wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE,
      uop                  ORDLOC_WKSHT.UOP%TYPE,
      supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE);

   TYPE L_redist_grd_tbl_type IS TABLE OF L_redist_grd_rec;
   L_redist_grd_tbl  L_redist_grd_tbl_type;
BEGIN
   --- check input parameters
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   L_where_clause := I_where_clause;
   if L_where_clause is NOT NULL then
      L_where_clause := ' and ' || L_where_clause;
   end if;
   ---

   L_select_statement := 'select distinct item_parent, ' ||
                         'item, ' ||
                         'ref_item, ' ||
                         'diff_1, ' ||
                         'diff_2, ' ||
                         'diff_3, ' ||
                         'diff_4, ' ||
                         'loc_type, ' ||
                         'location, ' ||
                         'sum(calc_qty), ' ||
                         'sum(act_qty), ' ||
                         'origin_country_id, ' ||
                         'standard_uom, ' ||
                         'sum(wksht_qty), ' ||
                         'uop, ' ||
                         'supp_pack_size';

   L_select_statement := L_select_statement || ' from ordloc_wksht ' ||
                        'where order_no = :l_order_no '||
                        ' and (store_grade is NOT NULL or location is NOT NULL) ';
   if L_where_clause is NOT NULL then
      L_select_statement := L_select_statement || L_where_clause;
   end if;
   L_select_statement := L_select_statement || ' group by ' ||
                         'item_parent, ' ||
                         'item, ' ||
                         'ref_item, ' ||
                         'diff_1, ' ||
                         'diff_2, ' ||
                         'diff_3, ' ||
                         'diff_4, ' ||
                         'loc_type, ' ||
                         'location, ' ||
                         'origin_country_id, ' ||
                         'standard_uom, ' ||
                         'uop, ' ||
                         'supp_pack_size';
   --- parse and execute the select statement
   EXECUTE IMMEDIATE L_select_statement BULK COLLECT into L_redist_grd_tbl  USING I_order_no;

   --- fetch loop
   if L_redist_grd_tbl.FIRST is not NULL then
      for grade_int in L_redist_grd_tbl.FIRST..L_redist_grd_tbl.LAST
      LOOP
         BEGIN
            insert into ordloc_wksht (order_no,
                                      item_parent,
                                      item,
                                      ref_item,
                                      diff_1,
                                      diff_2,
                                      diff_3,
                                      diff_4,
                                      loc_type,
                                      location,
                                      calc_qty,
                                      act_qty,
                                      origin_country_id,
                                      standard_uom,
                                      wksht_qty,
                                      uop,
                                      supp_pack_size)
                              values (I_order_no,
                                      L_redist_grd_tbl(grade_int).item_parent,
                                      L_redist_grd_tbl(grade_int).item,
                                      L_redist_grd_tbl(grade_int).ref_item,
                                      L_redist_grd_tbl(grade_int).diff1,
                                      L_redist_grd_tbl(grade_int).diff2,
                                      L_redist_grd_tbl(grade_int).diff3,
                                      L_redist_grd_tbl(grade_int).diff4,
                                      L_redist_grd_tbl(grade_int).loc_type,
                                      L_redist_grd_tbl(grade_int).location,
                                      L_redist_grd_tbl(grade_int).calc_qty,
                                      L_redist_grd_tbl(grade_int).act_qty,
                                      L_redist_grd_tbl(grade_int).orig_ctry_id,
                                      L_redist_grd_tbl(grade_int).stand_uom,
                                      L_redist_grd_tbl(grade_int).wksht_qty,
                                      L_redist_grd_tbl(grade_int).uop,
                                      L_redist_grd_tbl(grade_int).supp_pack_size);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               update ordloc_wksht
                  set wksht_qty = wksht_qty + L_redist_grd_tbl(grade_int).wksht_qty,
                      act_qty = act_qty + L_redist_grd_tbl(grade_int).act_qty,
                      calc_qty = calc_qty + L_redist_grd_tbl(grade_int).calc_qty
                where order_no = I_order_no
                  and (item_parent = L_redist_grd_tbl(grade_int).item_parent or
                       item = L_redist_grd_tbl(grade_int).item)
                  and (store_grade is null or location is NULL);
         END;
         ---
      END LOOP;
   end if;
   ---
   L_delete := 'delete from ordloc_wksht ' ||
               'where order_no = :l_order_no '||
               ' and (store_grade is NOT NULL) ' ||
                L_where_clause;
   ---
   EXECUTE IMMEDIATE L_delete USING I_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;
      ---
      return FALSE;
END REDIST_STORE_GRADE;
--------------------------------------------------------------------------------------------
FUNCTION REDIST_LOCATION
         (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
          I_order_no        IN      ORDLOC.ORDER_NO%TYPE,
          I_where_clause    IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(40) := 'ORDER_DIST_SQL.REDIST_LOCATION';
   L_where_clause       FILTER_TEMP.WHERE_CLAUSE%TYPE       := NULL;
   ---
   L_cursor1            INTEGER;
   L_select_statement   VARCHAR2(9000);
   L_delete             VARCHAR2(5000);
   TYPE L_ordwksht_rec IS RECORD(
      item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE,
      item                 ORDLOC_WKSHT.ITEM%TYPE,
      ref_item             ORDLOC_WKSHT.REF_ITEM%TYPE,
      diff1                ORDLOC_WKSHT.DIFF_1%TYPE,
      diff2                ORDLOC_WKSHT.DIFF_2%TYPE,
      diff3                ORDLOC_WKSHT.DIFF_3%TYPE,
      diff4                ORDLOC_WKSHT.DIFF_4%TYPE,
      calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE,
      act_qty              ORDLOC_WKSHT.ACT_QTY%TYPE,
      orig_ctry_id         ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
      stand_uom            ORDLOC_WKSHT.STANDARD_UOM%TYPE,
      wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE,
      uop                  ORDLOC_WKSHT.UOP%TYPE,
      supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE);

   TYPE L_ordwksht_tbl_type IS TABLE OF L_ordwksht_rec;
   L_ordwksht_tbl  L_ordwksht_tbl_type;
BEGIN
   --- check input parameters
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   L_where_clause := I_where_clause;
   if L_where_clause is NOT NULL then
      L_where_clause := ' and ' || L_where_clause;
   end if;
   ---
   L_select_statement := 'select distinct item_parent, ' ||
                         'item, ' ||
                         'ref_item, ' ||
                         'diff_1, ' ||
                         'diff_2, ' ||
                         'diff_3, ' ||
                         'diff_4, ' ||
                         'sum(calc_qty), ' ||
                         'sum(act_qty), ' ||
                         'origin_country_id, ' ||
                         'standard_uom, ' ||
                         'sum(wksht_qty), ' ||
                         'uop, ' ||
                         'supp_pack_size';
   L_select_statement := L_select_statement || ' from ordloc_wksht ' ||
                        'where order_no = :l_order_no ' ||
                        ' and location is NOT NULL';
   if L_where_clause is NOT NULL then
      L_select_statement := L_select_statement || L_where_clause;
   end if;
   L_select_statement := L_select_statement || ' group by ' ||
                        'item_parent, ' ||
                        'item, ' ||
                        'ref_item, ' ||
                        'diff_1, ' ||
                        'diff_2, ' ||
                        'diff_3, ' ||
                        'diff_4, ' ||
                        'origin_country_id, ' ||
                        'standard_uom, ' ||
                        'uop, ' ||
                        'supp_pack_size';
   --- parse and execute the select statement
   EXECUTE IMMEDIATE L_select_statement BULK COLLECT into L_ordwksht_tbl USING I_order_no;

   --- fetch loop
   if L_ordwksht_tbl.FIRST is not NULL then
      for item_int in L_ordwksht_tbl.FIRST..L_ordwksht_tbl.LAST
      LOOP
         BEGIN
         insert into ordloc_wksht (order_no,
                                   item_parent,
                                   item,
                                   ref_item,
                                   diff_1,
                                   diff_2,
                                   diff_3,
                                   diff_4,
                                   calc_qty,
                                   act_qty,
                                   origin_country_id,
                                   standard_uom,
                                   wksht_qty,
                                   uop,
                                   supp_pack_size)
                           values (I_order_no,
                                   L_ordwksht_tbl(item_int).item_parent,
                                   L_ordwksht_tbl(item_int).item,
                                   L_ordwksht_tbl(item_int).ref_item,
                                   L_ordwksht_tbl(item_int).diff1,
                                   L_ordwksht_tbl(item_int).diff2,
                                   L_ordwksht_tbl(item_int).diff3,
                                   L_ordwksht_tbl(item_int).diff4,
                                   L_ordwksht_tbl(item_int).calc_qty,
                                   L_ordwksht_tbl(item_int).act_qty,
                                   L_ordwksht_tbl(item_int).orig_ctry_id,
                                   L_ordwksht_tbl(item_int).stand_uom,
                                   L_ordwksht_tbl(item_int).wksht_qty,
                                   L_ordwksht_tbl(item_int).uop,
                                   L_ordwksht_tbl(item_int).supp_pack_size);
         EXCEPTION
            when DUP_VAL_ON_INDEX then
               update ordloc_wksht
                  set wksht_qty = wksht_qty + L_ordwksht_tbl(item_int).wksht_qty,
                      act_qty = act_qty + L_ordwksht_tbl(item_int).act_qty,
                      calc_qty = calc_qty + L_ordwksht_tbl(item_int).calc_qty
                where order_no = I_order_no
                  and (item_parent = L_ordwksht_tbl(item_int).item_parent or
                       item = L_ordwksht_tbl(item_int).item)
                  and location is NULL;
         END;
         ---
      END LOOP;
   end if;
   ---
   L_delete := 'delete from ordloc_wksht ' ||
               'where order_no = :l_order_no ' ||
               ' and location is NOT NULL ' ||
                L_where_clause;
   ---
   EXECUTE IMMEDIATE L_delete USING I_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;
      ---
      return FALSE;
END REDIST_LOCATION;
----------------------------------------------------------------------------------------------
FUNCTION MOVE_ITEM_TO_WKSHT (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                             I_supplier         IN      ORDHEAD.SUPPLIER%TYPE,
                             I_item_value       IN      ITEM_MASTER.ITEM%TYPE,
                             I_item_level       IN      ITEM_MASTER.ITEM_LEVEL%TYPE,
                             I_tran_level       IN      ITEM_MASTER.TRAN_LEVEL%TYPE)
RETURN BOOLEAN IS

   --  This is an internal function just called by MOVE_TO_WKSHT.

   L_exist                   BOOLEAN;
   L_item_level              ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level              ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_item                    ITEM_MASTER.ITEM%TYPE;
   L_dummy                   VARCHAR2(1);
   L_alloc_appt_exist        BOOLEAN;
   L_aa_exist_item           VARCHAR2(1)  := 'N';

   cursor C_ALLOC_APPT is
      select 'x'
        from alloc_header
       where alloc_header.order_no = I_order_no
      UNION
      select 'x'
        from appt_detail
       where appt_detail.doc      = I_order_no
         and appt_detail.doc_type = 'P';

   cursor C_ALL_ITEMS is
      select item
        from ordsku
       where order_no = I_order_no
         and (    not exists (select 'x'
                               from alloc_header
                              where alloc_header.item = ordsku.item
                                and alloc_header.order_no = ordsku.order_no)
              and not exists (select 'x'
                               from appt_detail
                              where appt_detail.item     = ordsku.item
                                and appt_detail.doc      = ordsku.order_no
                                and appt_detail.doc_type = 'P'));

   cursor C_ALLOC_APPT_ITEM is
      select 'x'
        from alloc_header
       where order_no = I_order_no
         and item = I_item_value
      UNION
      select 'x'
        from appt_detail
       where doc      = I_order_no
         and doc_type = 'P'
         and item = I_item_value;

   cursor C_ALLOC_APPT_PARENT is
      select 'x'
        from alloc_header
       where order_no = I_order_no
         and exists (select 'x'
                       from item_master
                      where alloc_header.item = item_master.item
                        and (item_master.item_parent = I_item_value
                             or item_master.item_grandparent = I_item_value))
      UNION
      select 'x'
        from appt_detail
       where doc      = I_order_no
         and doc_type = 'P'
         and exists (select 'x'
                       from item_master
                      where appt_detail.item        = item_master.item
                        and (item_master.item_parent = I_item_value
                             or item_master.item_grandparent = I_item_value));

   cursor C_PARENT_ITEMS is
      select item
        from ordsku
       where order_no = I_order_no
         and not exists (select 'x'
                           from alloc_header
                          where alloc_header.item = ordsku.item
                            and alloc_header.order_no = ordsku.order_no)
         and not exists (select 'x'
                           from appt_detail
                          where appt_detail.item     = ordsku.item
                            and appt_detail.doc      = ordsku.order_no
                            and appt_detail.doc_type = 'P')
         and exists (select 'x'
                       from item_master
                      where ordsku.item = item_master.item
                        and (item_master.item_parent = I_item_value
                             or item_master.item_grandparent = I_item_value));


BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   if I_supplier is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;

   ---
   -- Action is assumed to be the redistribution of an order.  This order
   -- may or may not have allocations associated with it.  The existence of
   -- allocations or Appointments determines what values are inserted into the ordloc_wksht.
   ---
   if I_item_value is NULL then

      insert into ordloc_wksht(order_no,
                               item_parent,
                               item,
                               ref_item,
                               diff_1,
                               diff_2,
                               diff_3,
                               diff_4,
                               store_grade,
                               store_grade_group_id,
                               loc_type,
                               location,
                               calc_qty,
                               act_qty,
                               variance_qty,
                               origin_country_id,
                               standard_uom,
                               wksht_qty,
                               uop,
                               supp_pack_size)
                       (select I_order_no,
                               im.item_parent,
                               os.item,
                               os.ref_item,
                               d1.diff_id,
                               d2.diff_id,
                               d3.diff_id,
                               d4.diff_id,
                               NULL, -- store grade
                               NULL, -- store grade group id
                               ol.loc_type, -- loc type
                               ol.location, -- location
                               NULL, -- calc_qty
                               ol.qty_ordered, -- act_qty
                               NULL,  -- variance_qty
                               os.origin_country_id,
                               im.standard_uom,
                               (ol.qty_ordered/os.supp_pack_size), --wksht_qty
                               DECODE(os.supp_pack_size,
                                      1, im.standard_uom,
                                         isp.case_name),
                               os.supp_pack_size
                          from item_master im,
                               item_supplier isp,
                               ordloc ol,
                               ordsku os,
                               diff_ids d1,
                               diff_ids d2,
                               diff_ids d3,
                               diff_ids d4
                         where ol.order_no = I_order_no
                           and ol.order_no = os.order_no
                           and ol.item = os.item
                           and ol.item = im.item
                           and im.diff_1 = d1.diff_id (+)
                           and im.diff_2 = d2.diff_id (+)
                           and im.diff_3 = d3.diff_id (+)
                           and im.diff_4 = d4.diff_id (+)
                           and isp.item = os.item
                           and isp.supplier = I_supplier
                           and not exists (select 'x'
                                             from ordloc_wksht
                                            where order_no = I_order_no
                                              and item = ol.item
                                              and location = ol.location)
                           and not exists (select 'x'
                                             from alloc_header
                                            where order_no = I_order_no
                                              and item = ol.item)
                           and not exists (select 'x'
                                             from appt_detail
                                            where doc      = I_order_no
                                              and item = ol.item));

      open C_ALLOC_APPT;
      fetch C_ALLOC_APPT into L_dummy;
      if C_ALLOC_APPT%FOUND then
         L_alloc_appt_exist := TRUE;
      else
         L_alloc_appt_exist := FALSE;
      end if;
      close C_ALLOC_APPT;

      if NOT L_alloc_appt_exist then
         ---
         -- Lock and delete the coresponding records from the ordloc and ordsku tables
         --   As this insert occurs for an entire order, I_item
         -- is passed in NULL.
         if ORDER_DIST_SQL.COPY_ORDLOC(O_error_message,
                                       I_order_no,
                                       NULL,
                                       'Y') = FALSE then
            return FALSE;
         end if;
         ---
         if ORDER_SETUP_SQL.LOCK_DETAIL (O_error_message,
                                         I_order_no,
                                         NULL,
                                         NULL,
                                        'ordredit') = FALSE then
            return FALSE;
         end if;
         ---
         if ORDER_SETUP_SQL.DELETE_ORDER (O_error_message,
                                          I_order_no,
                                          NULL,
                                          NULL,
                                          NULL,
                                          'ordredit') = FALSE then
            return FALSE;
         end if;
         ---
      else
         --- Some items have allocs or Appts. and therefore weren't added to
         --- ordloc_wksht.  Therefore we can't call the following packages for them.
         for rec in C_ALL_ITEMS LOOP
            L_item := rec.item;
            if ORDER_DIST_SQL.COPY_ORDLOC(O_error_message,
                                          I_order_no,
                                          L_item,
                                          'Y') = FALSE then
               return FALSE;
            end if;
            ---
            -- Lock and delete the corresponding records from the ordloc and ordsku tables.
            -- As this insert occurs for a specific item, I_sku is passed as I_item_value.
            -- I_loc_type and I_location are passed in as null.
            if ORDER_SETUP_SQL.LOCK_DETAIL(O_error_message,
                                           I_order_no,
                                           L_item,
                                           NULL,
                                           'ordredit') = FALSE then
               return FALSE;
            end if;
            ---
            if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                            I_order_no,
                                            L_item,
                                            NULL,
                                            NULL,
                                            'ordredit') = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
   elsif I_item_value is NOT NULL then

      if I_item_level = I_tran_level then
          ---
         open C_ALLOC_APPT_ITEM;
         fetch C_ALLOC_APPT_ITEM into L_aa_exist_item;
         close C_ALLOC_APPT_ITEM;
         ---
         if L_aa_exist_item = 'N' then -- no allocs or appts exist for the item - Redistribute.
            insert into ordloc_wksht(order_no,
                                     item_parent,
                                     item,
                                     ref_item,
                                     diff_1,
                                     diff_2,
                                     diff_3,
                                     diff_4,
                                     store_grade,
                                     store_grade_group_id,
                                     loc_type,
                                     location,
                                     calc_qty,
                                     act_qty,
                                     variance_qty,
                                     origin_country_id,
                                     standard_uom,
                                     wksht_qty,
                                     uop,
                                     supp_pack_size)
                             (select ol.order_no,
                                     im.item_parent,
                                     ol.item,
                                     os.ref_item,
                                     d1.diff_id,
                                     d2.diff_id,
                                     d3.diff_id,
                                     d4.diff_id,
                                     NULL, -- store grade
                                     NULL, -- store grade group id
                                     ol.loc_type,-- loc_type
                                     ol.location,  -- location
                                     NULL, -- calc_qty
                                     ol.qty_ordered, -- act_qty
                                     NULL,  -- variance_qty
                                     os.origin_country_id,
                                     im.standard_uom,
                                     (ol.qty_ordered/os.supp_pack_size), --wksht_qty
                                     DECODE(os.supp_pack_size,
                                            1, im.standard_uom,
                                               isp.case_name),
                                     os.supp_pack_size
                                from item_master im,
                                     item_supplier isp,
                                     ordloc ol,
                                     ordsku os,
                                     diff_ids d1,
                                     diff_ids d2,
                                     diff_ids d3,
                                     diff_ids d4
                               where ol.order_no = I_order_no
                                     and ol.item = I_item_value
                                     and ol.order_no = os.order_no
                                     and ol.item = os.item
                                     and ol.item = im.item
                                     and isp.item = os.item
                                     and isp.supplier = I_supplier
                                     and im.diff_1 = d1.diff_id (+)
                                     and im.diff_2 = d2.diff_id (+)
                                     and im.diff_3 = d3.diff_id (+)
                                     and im.diff_4 = d4.diff_id (+)
                                     and not exists (select 'x'
                                                       from ordloc_wksht
                                                      where order_no = I_order_no
                                                        and item = I_item_value
                                                        and location = ol.location));

           ---
            if ORDER_DIST_SQL.COPY_ORDLOC(O_error_message,
                                          I_order_no,
                                          I_item_value,
                                          'Y') = FALSE then
               return FALSE;
            end if;
            ---
               -- Lock and delete the corresponding records from the ordloc and ordsku tables.
               -- As this insert occurs for a specific item, I_sku is passed as I_item_value.
               -- I_loc_type and I_location are passed in as null.
            if ORDER_SETUP_SQL.LOCK_DETAIL(O_error_message,
                                           I_order_no,
                                           I_item_value,
                                           NULL,
                                           'ordredit') = FALSE then
               return FALSE;
            end if;
            ---
            if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                            I_order_no,
                                            I_item_value,
                                            NULL,
                                            NULL,
                                            'ordredit') = FALSE then
               return FALSE;
            end if;
         end if; --L_aa_item_exist = 'N'
      elsif I_item_level < I_tran_level then
         -- Action occurs at the parent level.  Every item in the parent that is on the ordloc
         -- table for the order should be inserted into ordloc_wksht.
         insert into ordloc_wksht(order_no,
                                  item_parent,
                                  item,
                                  ref_item,
                                  diff_1,
                                  diff_2,
                                  diff_3,
                                  diff_4,
                                  store_grade,
                                  store_grade_group_id,
                                  loc_type,
                                  location,
                                  calc_qty,
                                  act_qty,
                                  variance_qty,
                                  origin_country_id,
                                  standard_uom,
                                  wksht_qty,
                                  uop,
                                  supp_pack_size)
                          (select ol.order_no,
                                  I_item_value,
                                  im.item,
                                  os.ref_item,
                                  d1.diff_id,
                                  d2.diff_id,
                                  d3.diff_id,
                                  d4.diff_id,
                                  NULL, -- store grade
                                  NULL, -- store grade group id
                                  ol.loc_type, -- loc_type
                                  ol.location, -- location
                                  NULL, -- calc_qty
                                  ol.qty_ordered, -- act_qty
                                  NULL,  -- variance_qty
                                  os.origin_country_id,
                                  im.standard_uom,
                                  (ol.qty_ordered/os.supp_pack_size), --wksht_qty
                                   DECODE(os.supp_pack_size,
                                          1, im.standard_uom,
                                             isp.case_name),
                                  os.supp_pack_size
                             from item_master im,
                                  item_supplier isp,
                                  ordloc ol,
                                  ordsku os,
                                  diff_ids d1,
                                  diff_ids d2,
                                  diff_ids d3,
                                  diff_ids d4
                            where ol.order_no = I_order_no
                              and ol.order_no = os.order_no
                              and (im.item_parent = I_item_value or
                                   im.item_grandparent = I_item_value)
                              and ol.item = os.item
                              and ol.item = im.item
                              and im.diff_1 = d1.diff_id (+)
                              and im.diff_2 = d2.diff_id (+)
                              and im.diff_3 = d3.diff_id (+)
                              and im.diff_4 = d4.diff_id (+)
                              and isp.item = os.item
                              and isp.supplier = I_supplier
                              and not exists (select 'x'
                                                from ordloc_wksht
                                               where order_no = I_order_no
                                                 and item_parent = I_item_value
                                                 and item = ol.item
                                                 and location = ol.location)
                              and not exists (select 'x'
                                                from alloc_header a,
                                                     item_master im
                                               where a.order_no = I_order_no
                                                and ol.item = a.item
                                                and ((im.item_grandparent = I_item_value
                                                     and a.item = im.item)
                                                    or (im.item_parent = I_item_value
                                                       and a.item = im.item)))
                              and not exists (select 'x'
                                                from appt_detail d,
                                                     item_master i
                                               where d.doc      = I_order_no
                                                 and d.doc_type = 'P'
                                                 and ((i.item_grandparent = I_item_value
                                                       and d.item = i.item)
                                                     or (i.item_parent = I_item_value
                                                       and d.item = i.item))));

      open C_ALLOC_APPT_PARENT;
      fetch C_ALLOC_APPT_PARENT into L_dummy;
      if C_ALLOC_APPT_PARENT%FOUND then
         L_alloc_appt_exist := TRUE;
      else
         L_alloc_appt_exist := FALSE;
      end if;
      close C_ALLOC_APPT_PARENT;

      if NOT L_alloc_appt_exist then
         ---
         if ORDER_DIST_SQL.COPY_ORDLOC(O_error_message,
                                       I_order_no,
                                       I_item_value,
                                       'Y') = FALSE then
            return FALSE;
         end if;
         ---
         if ORDER_SETUP_SQL.LOCK_DETAIL (O_error_message,
                                         I_order_no,
                                         I_item_value,
                                         NULL,
                                        'ordredit') = FALSE then
            return FALSE;
         end if;
         ---
         if ORDER_SETUP_SQL.DELETE_ORDER (O_error_message,
                                          I_order_no,
                                          I_item_value,
                                          NULL,
                                          NULL,
                                          'ordredit') = FALSE then
            return FALSE;
         end if;
         ---
      else
         --- Some items have allocs and therefore weren't added to
         --- ordloc_wksht.  Therefore we can't call the following packages for them.
         for rec in C_PARENT_ITEMS LOOP
            L_item := rec.item;
            if ORDER_DIST_SQL.COPY_ORDLOC(O_error_message,
                                          I_order_no,
                                          L_item,
                                          'Y') = FALSE then
               return FALSE;
            end if;
            ---
            -- Lock and delete the corresponding records from the ordloc and ordsku tables.
            -- As this insert occurs for a specific item, I_sku is passed as I_item_value.
            -- I_loc_type and I_location are passed in as null.
            if ORDER_SETUP_SQL.LOCK_DETAIL(O_error_message,
                                           I_order_no,
                                           L_item,
                                           NULL,
                                           'ordredit') = FALSE then
               return FALSE;
            end if;
            ---
            if ORDER_SETUP_SQL.DELETE_ORDER(O_error_message,
                                            I_order_no,
                                            L_item,
                                            NULL,
                                            NULL,
                                            'ordredit') = FALSE then
               return FALSE;
            end if;
         END LOOP;
      end if;
      end if; -- ending if I_item_level = I_tran_level
      ---
      ---
   end if;  -- ending if I_item is NULL.
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                             'ORDER_DIST_SQL.MOVE_ITEM_TO_WKSHT',
                                              to_char(SQLCODE));
      return FALSE;
END MOVE_ITEM_TO_WKSHT;
----------------------------------------------------------------------------------------------
FUNCTION MOVE_TO_WKSHT (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                        I_supplier         IN      ORDHEAD.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_item        ITEM_MASTER.ITEM%TYPE;
   L_item_level  ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level  ITEM_MASTER.TRAN_LEVEL%TYPE;


   cursor C_REDIST is
      select item,
             item_level,
             tran_level
        from ordredst_temp
       where order_no = I_order_no;

BEGIN
   for rec in C_REDIST LOOP
      L_item       := rec.item;
      L_item_level := rec.item_level;
      L_tran_level := rec.tran_level;

      if NOT MOVE_ITEM_TO_WKSHT(O_error_message,
                                I_order_no,
                                I_supplier,
                                L_item,
                                L_item_level,
                                L_tran_level) then
         return FALSE;
      end if;
   END LOOP;

   delete from ordredst_temp
    where order_no = I_order_no;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              'ORDER_DIST_SQL.MOVE_TO_WKSHT',
                                              to_char(SQLCODE));
      return FALSE;
END MOVE_TO_WKSHT;
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_DIST_QTY_UOM( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_indicator      IN OUT  VARCHAR2,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN IS

   L_cursor1              INTEGER;
   L_cursor2              INTEGER;
   L_rows_processed       INTEGER := -1;
   L_rows_fetched         NUMBER(4);
   L_unit_count1          NUMBER(4);
   L_unit_count2          NUMBER(4);
   L_where_clause         FILTER_TEMP.WHERE_CLAUSE%TYPE;

   BEGIN

      --- check input parameters
      if I_order_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                                 NULL,
                                                 NULL,
                                                 NULL);
         return FALSE;
      end if;
      ---
      L_where_clause := I_where_clause;
      ---
      if L_where_clause is not NULL then
         L_where_clause := ' and '||L_where_clause;
      end if;
      ---
      EXECUTE IMMEDIATE 'select count(distinct uop) from ordloc_wksht where order_no = :l_order_no ' ||
                                  L_where_clause into L_unit_count1 USING I_order_no;

      if L_unit_count1 != 1 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_UOM', NULL, NULL, NULL);
         O_indicator := 'P';
      else
         -- next check if there are multiple standard UOMs
         EXECUTE IMMEDIATE 'select count(distinct standard_uom) from ordloc_wksht where order_no = :l_order_no '||
                                  L_where_clause into L_unit_count2 USING I_order_no;
         ---
         if L_unit_count2 != 1 then
            O_error_message := SQL_LIB.CREATE_MSG('NO_DIST_UOM', NULL, NULL, NULL);
            O_indicator := 'S';
         else -- there is only one uom for all items
            O_indicator := 'Y';
         end if;
      end if;
      ---
      return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_DIST_SQL.CHECK_DIST_QTY_UOM',
                                             to_char(SQLCODE));
      if DBMS_SQL.IS_OPEN (L_cursor1) then
         DBMS_SQL.CLOSE_CURSOR (L_cursor1);
      end if;
      RETURN FALSE;

END CHECK_DIST_QTY_UOM;
-----------------------------------------------------------------------------------------------
FUNCTION WO_TO_WKSHT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no       IN      ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN is
---
BEGIN
   --- check input parameter
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',
                                             SQLERRM,
                                             'ORDER_DIST_SQL.WO_TO_WKSHT',
                                             to_char(SQLCODE));
      return FALSE;
   end if;
   ---
   insert into ordloc_wksht(order_no,
                            item_parent,
                            item,
                            origin_country_id,
                            ref_item,
                            diff_1,
                            diff_2,
                            diff_3,
                            diff_4,
                            loc_type,
                            location,
                            xdock_phys_wh)
                    (select distinct ol.order_no,
                            im.item_parent,
                            ol.item,
                            os.origin_country_id,
                            os.ref_item,
                            d1.diff_id,
                            d2.diff_id,
                            d3.diff_id,
                            d4.diff_id,
                            ad.to_loc_type,
                            w2.physical_wh,
                            w.physical_wh
                       from ordloc ol,
                            ordsku os,
                            item_master im,
                            diff_ids d1,
                            diff_ids d2,
                            diff_ids d3,
                            diff_ids d4,
                            alloc_header ah,
                            alloc_detail ad,
                            wh w,
                            wh w2
                        where ol.order_no = I_order_no
                        and ol.loc_type = 'W'
                        and ol.qty_ordered > 0
                        and ol.item = im.item
                        and ol.item = ah.item
                        and ol.order_no = os.order_no
                        and ol.item = os.item
                        and ol.location = ah.wh
                        and ad.to_loc_type = 'W'
                        and ad.alloc_no = ah.alloc_no
                        and ol.order_no = ah.order_no
                        and ol.location = w.wh
                        and ad.to_loc = w2.wh
                        and im.diff_1 = d1.diff_id (+)
                        and im.diff_2 = d2.diff_id (+)
                        and im.diff_3 = d3.diff_id (+)
                        and im.diff_4 = d4.diff_id (+)
                        and NOT EXISTS (select 'x'
                                          from wo_detail wd, wo_head wo
                                         where wd.item = ol.item
                                           and wd.location = w2.physical_wh
                                           and wo.order_no = I_order_no
                                           and wo.wo_id = wd.wo_id));                
                                           
   ---
   insert into ordloc_wksht(order_no,
                            item_parent,
                            item,
                            origin_country_id,
                            ref_item,
                            diff_1,
                            diff_2,
                            diff_3,
                            diff_4,
                            loc_type,
                            location)
                    (select distinct ol.order_no,
                            im.item_parent,
                            ol.item,
                            os.origin_country_id,
                            os.ref_item,
                            d1.diff_id,
                            d2.diff_id,
                            d3.diff_id,
                            d4.diff_id,
                            'W',
                            w.physical_wh
                       from ordloc ol,
                            ordsku os,
                            item_master im,
                            diff_ids d1,
                            diff_ids d2,
                            diff_ids d3,
                            diff_ids d4,
                            wh w
                      where ol.order_no = I_order_no
                        and ol.loc_type = 'W'
                        and ol.qty_ordered > 0
                        and ol.item = im.item
                        and ol.location = w.wh
                        and ol.order_no = os.order_no
                        and ol.item = os.item
                        and im.diff_1 = d1.diff_id (+)
                        and im.diff_2 = d2.diff_id (+)
                        and im.diff_3 = d3.diff_id (+)
                        and im.diff_4 = d4.diff_id (+)
                        and NOT EXISTS (select 'x'
                                          from wo_detail wd, wo_head wo
                                         where wd.item = ol.item
                                           and wd.location = w.physical_wh
                                           and wd.wo_id = wo.wo_id
                                           and wo.order_no = I_order_no)
						/* Purpose: this will handle cases where the virtual warehouses reside in the same physical warehouse */
						and NOT EXISTS (select 'x' from ordloc_wksht ow
                                         where ow.order_no = I_order_no
                                           and ow.item = ol.item
                                           and ow.location = w.physical_wh));

   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_DIST_SQL.WO_TO_WKSHT',
                                             to_char(SQLCODE));
      RETURN FALSE;
END WO_TO_WKSHT;
-----------------------------------------------------------------------------------------------
FUNCTION COPY_ORDLOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                     I_item           IN      ORDSKU.ITEM%TYPE,
                     I_redist_ind     IN      VARCHAR2 DEFAULT 'N')

RETURN BOOLEAN is
   L_approved             VARCHAR2(10) := NULL;
   L_exist                BOOLEAN;
   L_status               ORDHEAD.STATUS%TYPE;
   L_item_level           ITEM_MASTER.ITEM_LEVEL%TYPE := 1;
   L_tran_level           ITEM_MASTER.TRAN_LEVEL%TYPE := 1;
   L_pack_ind             ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_type            ITEM_MASTER.PACK_TYPE%TYPE;
BEGIN


   if ORDER_ATTRIB_SQL.GET_STATUS(O_error_message,
                                  L_exist,
                                  L_status,
                                  I_order_no) = FALSE then
      return FALSE;
   end if;

   ---
   if I_item is NOT NULL then
      if NOT ITEM_ATTRIB_SQL.GET_LEVELS(O_error_message,
                                        L_item_level,
                                        L_tran_level,
                                        I_item) then
         return FALSE;
      end if;
      if NOT ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                           L_pack_ind,
                                           L_sellable_ind,
                                           L_orderable_ind,
                                           L_pack_type,
                                           I_item) then
         return FALSE;
      end if;
   end if;

   if L_status = 'W' then
      if ORDER_ATTRIB_SQL.PREV_APPROVED(O_error_message,
                                        L_approved,
                                        I_order_no) = FALSE then
         return FALSE;
      end if;
   end if;

   ---
   if L_item_level < L_tran_level then
      insert into ordloc_temp(order_no,
                              item,
                              location,
                              loc_type,
                              unit_retail,
                              qty_ordered,
                              qty_prescaled,
                              qty_received,
                              last_received,
                              last_rounded_qty,
                              last_grp_rounded_qty,
                              qty_cancelled,
                              cancel_code,
                              cancel_date,
                              cancel_id,
                              original_repl_qty,
                              unit_cost,
                              unit_cost_init,
                              cost_source,
                              non_scale_ind,
                              tsf_po_link_no)
                      (select o.order_no,
                              o.item,
                              o.location,
                              o.loc_type,
                              o.unit_retail,
                              o.qty_ordered,
                              o.qty_prescaled,
                              o.qty_received,
                              o.last_received,
                              o.last_rounded_qty,
                              o.last_grp_rounded_qty,
                              o.qty_cancelled,
                              o.cancel_code,
                              o.cancel_date,
                              o.cancel_id,
                              o.original_repl_qty,
                              o.unit_cost,
                              o.unit_cost_init,
                              o.cost_source,
                              o.non_scale_ind,
                              o.tsf_po_link_no
                         from ordloc o,
                              item_master im
                        where o.order_no = I_order_no
                          and (im.item_parent = I_item
                           or im.item_grandparent = I_item)
                          and im.item = o.item);

      ---
      if L_status != 'A' and L_approved = 'N' then
         insert into ordsku_temp(order_no,
                                 item,
                                 ref_item,
                                 origin_country_id,
                                 earliest_ship_date,
                                 latest_ship_date,
                                 supp_pack_size,
                                 non_scale_ind,
                                 pickup_loc,
                                 pickup_no)
                         (select o.order_no,
                                 o.item,
                                 o.ref_item,
                                 o.origin_country_id,
                                 o.earliest_ship_date,
                                 o.latest_ship_date,
                                 o.supp_pack_size,
                                 o.non_scale_ind,
                                 o.pickup_loc,
                                 o.pickup_no
                            from ordsku o,
                                 item_master im
                           where o.order_no = I_order_no
                             and (im.item_parent = I_item
                              or im.item_grandparent = I_item)
                             and im.item = o.item);
         ---
         insert into timeline_temp(timeline_key,
                                   timeline_no,
                                   timeline_type,
                                   key_value_1,
                                   key_value_2,
                                   base_date,
                                   step_no,
                                   display_seq,
                                   original_date,
                                   revised_date,
                                   actual_date,
                                   reason_code,
                                   comment_desc)
                           (select timeline_key,
                                   timeline_no,
                                   timeline_type,
                                   key_value_1,
                                   key_value_2,
                                   base_date,
                                   step_no,
                                   display_seq,
                                   original_date,
                                   revised_date,
                                   actual_date,
                                   reason_code,
                                   comment_desc
                              from timeline,
                                   item_master
                             where key_value_1 = TO_CHAR(I_order_no)
                               and (item_parent = I_item
                                or item_grandparent = I_item)
                               and key_value_2 = item
                               and timeline_type = 'POIT');
         ---
         insert into req_doc_temp(doc_key,
                                  module,
                                  key_value_1,
                                  key_value_2,
                                  doc_id,
                                  doc_text)
                          (select doc_key,
                                  module,
                                  key_value_1,
                                  key_value_2,
                                  doc_id,
                                  doc_text
                             from req_doc,
                                  item_master
                            where key_value_1 = TO_CHAR(I_order_no)
                              and (item_parent = I_item
                               or item_grandparent = I_item)
                              and key_value_2 = item
                              and module = 'POIT');
         ---
         insert into ordsku_hts_temp(order_no,
                                     seq_no,
                                     item,
                                     pack_item,
                                     hts,
                                     import_country_id,
                                     origin_country_id,
                                     effect_from,
                                     effect_to,
                                     status)
                             (select o.order_no,
                                     o.seq_no,
                                     o.item,
                                     o.pack_item,
                                     o.hts,
                                     o.import_country_id,
                                     o.origin_country_id,
                                     o.effect_from,
                                     o.effect_to,
                                     o.status
                                from ordsku_hts o,
                                     item_master im
                               where o.order_no = I_order_no
                                 and (im.item_parent = I_item
                                  or im.item_grandparent = I_item)
                                 and im.item = o.item);
         ---
         insert into ordsku_hts_assess_temp(order_no,
                                            seq_no,
                                            comp_id,
                                            cvb_code,
                                            comp_rate,
                                            per_count,
                                            per_count_uom,
                                            est_assess_value,
                                            nom_flag_1,
                                            nom_flag_2,
                                            nom_flag_3,
                                            nom_flag_4,
                                            nom_flag_5,
                                            display_order)
                                    (select order_no,
                                            seq_no,
                                            comp_id,
                                            cvb_code,
                                            comp_rate,
                                            per_count,
                                            per_count_uom,
                                            est_assess_value,
                                            nom_flag_1,
                                            nom_flag_2,
                                            nom_flag_3,
                                            nom_flag_4,
                                            nom_flag_5,
                                            display_order
                                       from ordsku_hts_assess
                                      where order_no = I_order_no
                                        and seq_no in (select o.seq_no
                                                         from ordsku_hts o, item_master im
                                                        where o.order_no = I_order_no
                                                          and (item_parent = I_item
                                                           or item_grandparent = I_item)
                                                          and o.item = im.item));
      end if; /* end of L_status != 'A' and L_approved = 'N' */
      ---

      insert into ordloc_exp_temp(order_no,
                                  seq_no,
                                  item,
                                  pack_item,
                                  location,
                                  loc_type,
                                  comp_id,
                                  cvb_code,
                                  cost_basis,
                                  comp_rate,
                                  comp_currency,
                                  exchange_rate,
                                  per_count,
                                  per_count_uom,
                                  est_exp_value,
                                  nom_flag_1,
                                  nom_flag_2,
                                  nom_flag_3,
                                  nom_flag_4,
                                  nom_flag_5,
                                  origin,
                                  display_order,
                                  defaulted_from,
                                  key_value_1,
                                  key_value_2)
                          (select o.order_no,
                                  o.seq_no,
                                  o.item,
                                  o.pack_item,
                                  o.location,
                                  o.loc_type,
                                  o.comp_id,
                                  o.cvb_code,
                                  o.cost_basis,
                                  o.comp_rate,
                                  o.comp_currency,
                                  o.exchange_rate,
                                  o.per_count,
                                  o.per_count_uom,
                                  o.est_exp_value,
                                  o.nom_flag_1,
                                  o.nom_flag_2,
                                  o.nom_flag_3,
                                  o.nom_flag_4,
                                  o.nom_flag_5,
                                  o.origin,
                                  o.display_order,
                                  o.defaulted_from,
                                  o.key_value_1,
                                  o.key_value_2
                             from ordloc_exp o,
                                  item_master im
                            where o.order_no = I_order_no
                              and (im.item_parent = I_item
                               or im.item_grandparent = I_item)
                              and im.item = o.item);

      ---
      insert into ordloc_discount_temp(order_no,
                                       seq_no,
                                       item,
                                       location,
                                       deal_id,
                                       deal_detail_id,
                                       pack_no,
                                       discount_value,
                                       discount_type,
                                       discount_amt_per_unit,
                                       application_order,
                                       paid_ind,
                                       last_calc_date)
                               (select o.order_no,
                                       o.seq_no,
                                       o.item,
                                       o.location,
                                       o.deal_id,
                                       o.deal_detail_id,
                                       o.pack_no,
                                       o.discount_value,
                                       o.discount_type,
                                       o.discount_amt_per_unit,
                                       o.application_order,
                                       o.paid_ind,
                                       o.last_calc_date
                                  from ordloc_discount o,
                                       item_master im
                                 where o.order_no = I_order_no
                                   and (im.item_parent = I_item
                                    or im.item_grandparent = I_item)
                                   and im.item = o.item);

      ---
      insert into alloc_header_temp(alloc_no,
                                    order_no,
                                    wh,
                                    item,
                                    status,
                                    alloc_desc,
                                    po_type,
                                    alloc_method,
                                    release_date,
                                    order_type,
                                    origin_ind)
                            (select a.alloc_no,
                                    a.order_no,
                                    a.wh,
                                    a.item,
                                    a.status,
                                    a.alloc_desc,
                                    a.po_type,
                                    a.alloc_method,
                                    a.release_date,
                                    a.order_type,
                                    a.origin_ind
                               from alloc_header a,
                                    item_master im
                              where a.order_no = I_order_no
                                and (im.item_parent = I_item
                                 or im.item_grandparent = I_item)
                                and im.item = a.item);

      ---
      insert into alloc_detail_temp (alloc_no,
                                     to_loc,
                                     to_loc_type,
                                     qty_transferred,
                                     qty_allocated,
                                     qty_prescaled,
                                     qty_distro,
                                     qty_selected,
                                     qty_cancelled,
                                     qty_received,
                                     po_rcvd_qty,
                                     non_scale_ind)
                              select alloc_no,
                                     to_loc,
                                     to_loc_type,
                                     qty_transferred,
                                     qty_allocated,
                                     qty_prescaled,
                                     qty_distro,
                                     qty_selected,
                                     qty_cancelled,
                                     qty_received,
                                     po_rcvd_qty,
                                     non_scale_ind
                                from alloc_detail
                               where alloc_no in (select alloc_no
                                                    from alloc_header ah,
                                                         item_master im
                                                   where ah.order_no = I_order_no
                                                     and (im.item_parent = I_item
                                                      or im.item_grandparent = I_item)
                                                         and im.item = ah.item);
      ---
      insert into alloc_chrg_temp(alloc_no,
                                  to_loc,
                                  item,
                                  comp_id,
                                  to_loc_type,
                                  pack_item,
                                  comp_rate,
                                  per_count,
                                  per_count_uom,
                                  up_chrg_group,
                                  comp_currency,
                                  display_order)
                           select alloc_no,
                                  to_loc,
                                  item,
                                  comp_id,
                                  to_loc_type,
                                  pack_item,
                                  comp_rate,
                                  per_count,
                                  per_count_uom,
                                  up_chrg_group,
                                  comp_currency,
                                  display_order
                             from alloc_chrg
                            where alloc_no in (select alloc_no
                                                 from alloc_header ah,
                                                      item_master im
                                                where ah.order_no = I_order_no
                                                  and (im.item_parent = I_item
                                                   or im.item_grandparent = I_item)
                                                      and im.item  = ah.item);
      ---
      insert into wo_head_temp(wo_id,
                               tsfalloc_no,
                               order_no)
                       (select wo_id,
                               tsfalloc_no,
                               order_no
                          from wo_head wh
                         where order_no = I_order_no
                           and NOT EXISTS (select 'x'
                                             from wo_detail w
                                            where w.wo_id = wh.wo_id));
      ---
      insert into wo_detail_temp(wo_id,
                                 wh,
                                 item,
                                 location,
                                 loc_type,
                                 seq_no,
                                 wip_code)
                         (select wd.wo_id,
                                 wd.wh,
                                 wd.item,
                                 wd.location,
                                 wd.loc_type,
                                 wd.seq_no,
                                 wd.wip_code
                            from wo_detail wd, wo_head wo, item_master im
                           where wd.wo_id = wo.wo_id
                             and wo.order_no = I_order_no
                             and (im.item_parent = I_item or
                                  im.item_grandparent = I_item)
                             and im.item = wd.item);
      ---
   else -- tran level item

      insert into ordloc_temp(order_no,
                              item,
                              location,
                              loc_type,
                              unit_retail,
                              qty_ordered,
                              qty_prescaled,
                              qty_received,
                              last_received,
                              last_rounded_qty,
                              last_grp_rounded_qty,
                              qty_cancelled,
                              cancel_code,
                              cancel_date,
                              cancel_id,
                              original_repl_qty,
                              unit_cost,
                              unit_cost_init,
                              cost_source,
                              non_scale_ind,
                              tsf_po_link_no)
                      (select order_no,
                              item,
                              location,
                              loc_type,
                              unit_retail,
                              qty_ordered,
                              qty_prescaled,
                              qty_received,
                              last_received,
                              last_rounded_qty,
                              last_grp_rounded_qty,
                              qty_cancelled,
                              cancel_code,
                              cancel_date,
                              cancel_id,
                              original_repl_qty,
                              unit_cost,
                              unit_cost_init,
                              cost_source,
                              non_scale_ind,
                              tsf_po_link_no
                         from ordloc
                        where order_no = I_order_no
                          and item = NVL(I_item, item));

      if L_status != 'A' and L_approved = 'N' then
         insert into ordsku_temp(order_no,
                                 item,
                                 ref_item,
                                 origin_country_id,
                                 earliest_ship_date,
                                 latest_ship_date,
                                 supp_pack_size,
                                 non_scale_ind,
                                 pickup_loc,
                                 pickup_no)
                         (select order_no,
                                 item,
                                 ref_item,
                                 origin_country_id,
                                 earliest_ship_date,
                                 latest_ship_date,
                                 supp_pack_size,
                                 non_scale_ind,
                                 pickup_loc,
                                 pickup_no
                            from ordsku
                           where order_no = I_order_no
                             and item = NVL(I_item, item));

         -- Copy the entire record from the repl_results
         -- table to the temp table. The temp table is assumed
         -- to always have the same structure as the repl results
         -- table.
        insert into repl_results_temp(source_type,
                                      status,
                                      ord_temp_seq_no,
                                      tsf_po_link_no,
                                      order_no,
                                      alloc_no,
                                      master_item,
                                      item,
                                      item_type,
                                      dept,
                                      class,
                                      subclass,
                                      buyer,
                                      pack_qty,
                                      loc_type,
                                      location,
                                      physical_wh,
                                      primary_repl_supplier,
                                      origin_country_id,
                                      pool_supplier,
                                      supp_unit_cost,
                                      unit_cost,
                                      orig_raw_roq,
                                      orig_raw_roq_pack,
                                      raw_roq,
                                      raw_roq_pack,
                                      prescale_roq,
                                      order_roq,
                                      contract_roq,
                                      sourced_roq,
                                      last_rounded_qty,
                                      last_grp_rounded_qty,
                                      net_inventory,
                                      stock_on_hand,
                                      pack_comp_soh,
                                      on_order,
                                      in_transit_qty,
                                      pack_comp_intran,
                                      tsf_resv_qty,
                                      pack_comp_resv,
                                      tsf_expected_qty,
                                      pack_comp_exp,
                                      rtv_qty,
                                      alloc_in_qty,
                                      alloc_out_qty,
                                      non_sellable_qty,
                                      order_point,
                                      order_up_to_point,
                                      safety_stock,
                                      lost_sales,
                                      min_supply_days_forecast,
                                      max_supply_days_forecast,
                                      time_supply_horizon_forecast,
                                      inv_sell_days_forecast,
                                      review_time_forecast,
                                      order_lead_time_forecast,
                                      next_lead_time_forecast,
                                      accepted_stock_out,
                                      estimated_stock_out,
                                      due_ind,
                                      review_time,
                                      repl_order_ctrl,
                                      review_cycle,
                                      stock_cat,
                                      source_wh,
                                      activate_date,
                                      deactivate_date,
                                      pres_stock,
                                      demo_stock,
                                      repl_method,
                                      min_stock,
                                      max_stock,
                                      incr_pct,
                                      min_supply_days,
                                      max_supply_days,
                                      time_supply_horizon,
                                      inv_selling_days,
                                      service_level,
                                      lost_sales_factor,
                                      terminal_stock_qty,
                                      season_id,
                                      phase_id,
                                      reject_store_ord_ind,
                                      non_scaling_ind,
                                      max_scale_value,
                                      supp_lead_time,
                                      pickup_lead_time,
                                      wh_lead_time,
                                      next_order_lead_time,
                                      colt_added,
                                      nolt_added,
                                      inner_size,
                                      case_size,
                                      ti,
                                      hi,
                                      store_ord_mult,
                                      repl_date,
                                      recalc_type,
                                      recalc_qty,
                                      audsid)
                              (select source_type,
                                      status,
                                      ord_temp_seq_no,
                                      tsf_po_link_no,
                                      order_no,
                                      alloc_no,
                                      master_item,
                                      item,
                                      item_type,
                                      dept,
                                      class,
                                      subclass,
                                      buyer,
                                      pack_qty,
                                      loc_type,
                                      location,
                                      physical_wh,
                                      primary_repl_supplier,
                                      origin_country_id,
                                      pool_supplier,
                                      supp_unit_cost,
                                      unit_cost,
                                      orig_raw_roq,
                                      orig_raw_roq_pack,
                                      raw_roq,
                                      raw_roq_pack,
                                      prescale_roq,
                                      order_roq,
                                      contract_roq,
                                      sourced_roq,
                                      last_rounded_qty,
                                      last_grp_rounded_qty,
                                      net_inventory,
                                      stock_on_hand,
                                      pack_comp_soh,
                                      on_order,
                                      in_transit_qty,
                                      pack_comp_intran,
                                      tsf_resv_qty,
                                      pack_comp_resv,
                                      tsf_expected_qty,
                                      pack_comp_exp,
                                      rtv_qty,
                                      alloc_in_qty,
                                      alloc_out_qty,
                                      non_sellable_qty,
                                      order_point,
                                      order_up_to_point,
                                      safety_stock,
                                      lost_sales,
                                      min_supply_days_forecast,
                                      max_supply_days_forecast,
                                      time_supply_horizon_forecast,
                                      inv_sell_days_forecast,
                                      review_time_forecast,
                                      order_lead_time_forecast,
                                      next_lead_time_forecast,
                                      accepted_stock_out,
                                      estimated_stock_out,
                                      due_ind,
                                      review_time,
                                      repl_order_ctrl,
                                      review_cycle,
                                      stock_cat,
                                      source_wh,
                                      activate_date,
                                      deactivate_date,
                                      pres_stock,
                                      demo_stock,
                                      repl_method,
                                      min_stock,
                                      max_stock,
                                      incr_pct,
                                      min_supply_days,
                                      max_supply_days,
                                      time_supply_horizon,
                                      inv_selling_days,
                                      service_level,
                                      lost_sales_factor,
                                      terminal_stock_qty,
                                      season_id,
                                      phase_id,
                                      reject_store_ord_ind,
                                      non_scaling_ind,
                                      max_scale_value,
                                      supp_lead_time,
                                      pickup_lead_time,
                                      wh_lead_time,
                                      next_order_lead_time,
                                      colt_added,
                                      nolt_added,
                                      inner_size,
                                      case_size,
                                      ti,
                                      hi,
                                      store_ord_mult,
                                      repl_date,
                                      recalc_type,
                                      recalc_qty,
                                      audsid
                                 from repl_results
                                where order_no = I_order_no);

         ---
         if I_redist_ind = 'Y' then
            insert into timeline_temp(timeline_key,
                                      timeline_no,
                                      timeline_type,
                                      key_value_1,
                                      key_value_2,
                                      base_date,
                                      step_no,
                                      display_seq,
                                      original_date,
                                      revised_date,
                                      actual_date,
                                      reason_code,
                                      comment_desc)
                              (select timeline_key,
                                      timeline_no,
                                      timeline_type,
                                      key_value_1,
                                      key_value_2,
                                      base_date,
                                      step_no,
                                      display_seq,
                                      original_date,
                                      revised_date,
                                      actual_date,
                                      reason_code,
                                      comment_desc
                                 from timeline
                                where key_value_1 = TO_CHAR(I_order_no)
                                  and key_value_2 = nvl(I_item, key_value_2)
                                  and timeline_type = 'POIT');
            ---
            insert into req_doc_temp(doc_key,
                                     module,
                                     key_value_1,
                                     key_value_2,
                                     doc_id,
                                     doc_text)
                             (select doc_key,
                                     module,
                                     key_value_1,
                                     key_value_2,
                                     doc_id,
                                     doc_text
                                from req_doc
                               where key_value_1 = TO_CHAR(I_order_no)
                                 and key_value_2 = nvl(I_item, key_value_2)
                                 and module = 'POIT');
         else
            insert into timeline_temp(timeline_key,
                                      timeline_no,
                                      timeline_type,
                                      key_value_1,
                                      key_value_2,
                                      base_date,
                                      step_no,
                                      display_seq,
                                      original_date,
                                      revised_date,
                                      actual_date,
                                      reason_code,
                                      comment_desc)
                              (select timeline_key,
                                      timeline_no,
                                      timeline_type,
                                      key_value_1,
                                      key_value_2,
                                      base_date,
                                      step_no,
                                      display_seq,
                                      original_date,
                                      revised_date,
                                      actual_date,
                                      reason_code,
                                      comment_desc
                                 from timeline
                                where key_value_1 = TO_CHAR(I_order_no)
                                  and timeline_type = 'POIT');
            ---
            insert into req_doc_temp(doc_key,
                                     module,
                                     key_value_1,
                                     key_value_2,
                                     doc_id,
                                     doc_text)
                             (select doc_key,
                                     module,
                                     key_value_1,
                                     key_value_2,
                                     doc_id,
                                     doc_text
                                from req_doc
                               where key_value_1 = TO_CHAR(I_order_no)
                                 and module = 'POIT');
         end if;
         ---
         if L_pack_ind = 'Y' then
             insert into ordsku_hts_temp(order_no,
                                         seq_no,
                                         item,
                                         pack_item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         status)
                                 (select order_no,
                                         seq_no,
                                         item,
                                         pack_item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         status
                                    from ordsku_hts
                                   where order_no = I_order_no
                                      and pack_item = NVL(I_item, pack_item));
            ---
          insert into ordsku_hts_assess_temp(order_no,
                                             seq_no,
                                             comp_id,
                                             cvb_code,
                                             comp_rate,
                                             per_count,
                                             per_count_uom,
                                             est_assess_value,
                                             nom_flag_1,
                                             nom_flag_2,
                                             nom_flag_3,
                                             nom_flag_4,
                                             nom_flag_5,
                                             display_order)
                                     (select order_no,
                                             seq_no,
                                             comp_id,
                                             cvb_code,
                                             comp_rate,
                                             per_count,
                                             per_count_uom,
                                             est_assess_value,
                                             nom_flag_1,
                                             nom_flag_2,
                                             nom_flag_3,
                                             nom_flag_4,
                                             nom_flag_5,
                                             display_order
                                        from ordsku_hts_assess
                                       where order_no = I_order_no
                                         and seq_no in (select seq_no
                                                         from ordsku_hts
                                                        where order_no = I_order_no
                                                          and pack_item = NVL(I_item, pack_item)));
         else
             insert into ordsku_hts_temp(order_no,
                                         seq_no,
                                         item,
                                         pack_item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         status)
                                 (select order_no,
                                         seq_no,
                                         item,
                                         pack_item,
                                         hts,
                                         import_country_id,
                                         origin_country_id,
                                         effect_from,
                                         effect_to,
                                         status
                                    from ordsku_hts
                                   where order_no = I_order_no
                                     and item = NVL(I_item, item));
            ---
          insert into ordsku_hts_assess_temp(order_no,
                                             seq_no,
                                             comp_id,
                                             cvb_code,
                                             comp_rate,
                                             per_count,
                                             per_count_uom,
                                             est_assess_value,
                                             nom_flag_1,
                                             nom_flag_2,
                                             nom_flag_3,
                                             nom_flag_4,
                                             nom_flag_5,
                                             display_order)
                                     (select order_no,
                                             seq_no,
                                             comp_id,
                                             cvb_code,
                                             comp_rate,
                                             per_count,
                                             per_count_uom,
                                             est_assess_value,
                                             nom_flag_1,
                                             nom_flag_2,
                                             nom_flag_3,
                                             nom_flag_4,
                                             nom_flag_5,
                                             display_order
                                        from ordsku_hts_assess
                                       where order_no = I_order_no
                                         and seq_no in (select seq_no
                                                         from ordsku_hts
                                                        where order_no = I_order_no
                                                          and item = NVL(I_item, item)));
         end if;
         ---
      end if; /* end of L_status != 'A' and L_approved = 'N' */
      ---
      if L_pack_ind = 'Y' then
         insert into ordloc_exp_temp(order_no,
                                     seq_no,
                                     item,
                                     pack_item,
                                     location,
                                     loc_type,
                                     comp_id,
                                     cvb_code,
                                     cost_basis,
                                     comp_rate,
                                     comp_currency,
                                     exchange_rate,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     origin,
                                     display_order,
                                     defaulted_from,
                                     key_value_1,
                                     key_value_2)
                             (select order_no,
                                     seq_no,
                                     item,
                                     pack_item,
                                     location,
                                     loc_type,
                                     comp_id,
                                     cvb_code,
                                     cost_basis,
                                     comp_rate,
                                     comp_currency,
                                     exchange_rate,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     origin,
                                     display_order,
                                     defaulted_from,
                                     key_value_1,
                                     key_value_2
                                from ordloc_exp
                               where order_no = I_order_no
                                 and pack_item = NVL(I_item, pack_item));
      else
         insert into ordloc_exp_temp(order_no,
                                     seq_no,
                                     item,
                                     pack_item,
                                     location,
                                     loc_type,
                                     comp_id,
                                     cvb_code,
                                     cost_basis,
                                     comp_rate,
                                     comp_currency,
                                     exchange_rate,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     origin,
                                     display_order,
                                     defaulted_from,
                                     key_value_1,
                                     key_value_2)
                             (select order_no,
                                     seq_no,
                                     item,
                                     pack_item,
                                     location,
                                     loc_type,
                                     comp_id,
                                     cvb_code,
                                     cost_basis,
                                     comp_rate,
                                     comp_currency,
                                     exchange_rate,
                                     per_count,
                                     per_count_uom,
                                     est_exp_value,
                                     nom_flag_1,
                                     nom_flag_2,
                                     nom_flag_3,
                                     nom_flag_4,
                                     nom_flag_5,
                                     origin,
                                     display_order,
                                     defaulted_from,
                                     key_value_1,
                                     key_value_2
                                from ordloc_exp
                               where order_no = I_order_no
                                 and item = NVL(I_item, item));
      end if;
      ---
      if L_pack_ind = 'Y' then
         insert into ordloc_discount_temp(order_no,
                                          seq_no,
                                          item,
                                          location,
                                          deal_id,
                                          deal_detail_id,
                                          pack_no,
                                          discount_value,
                                          discount_type,
                                          discount_amt_per_unit,
                                          application_order,
                                          paid_ind,
                                          last_calc_date)
                                  (select order_no,
                                          seq_no,
                                          item,
                                          location,
                                          deal_id,
                                          deal_detail_id,
                                          pack_no,
                                          discount_value,
                                          discount_type,
                                          discount_amt_per_unit,
                                          application_order,
                                          paid_ind,
                                          last_calc_date
                                     from ordloc_discount
                                    where order_no = I_order_no
                                      and pack_no = NVL(I_item, pack_no));
      else
         insert into ordloc_discount_temp(order_no,
                                          seq_no,
                                          item,
                                          location,
                                          deal_id,
                                          deal_detail_id,
                                          pack_no,
                                          discount_value,
                                          discount_type,
                                          discount_amt_per_unit,
                                          application_order,
                                          paid_ind,
                                          last_calc_date)
                                  (select order_no,
                                          seq_no,
                                          item,
                                          location,
                                          deal_id,
                                          deal_detail_id,
                                          pack_no,
                                          discount_value,
                                          discount_type,
                                          discount_amt_per_unit,
                                          application_order,
                                          paid_ind,
                                          last_calc_date
                                     from ordloc_discount
                                    where order_no = I_order_no
                                      and item = NVL(I_item, item));
      end if;
      ---
      insert into alloc_header_temp(alloc_no,
                                    order_no,
                                    wh,
                                    item,
                                    status,
                                    alloc_desc,
                                    po_type,
                                    alloc_method,
                                    release_date,
                                    order_type,
                                    origin_ind)
                            (select alloc_no,
                                    order_no,
                                    wh,
                                    item,
                                    status,
                                    alloc_desc,
                                    po_type,
                                    alloc_method,
                                    release_date,
                                    order_type,
                                    origin_ind
                               from alloc_header
                              where order_no = I_order_no
                                and item = NVL(I_item, item));
      ---
      insert into alloc_detail_temp(alloc_no,
                                    to_loc,
                                    to_loc_type,
                                    qty_transferred,
                                    qty_allocated,
                                    qty_prescaled,
                                    qty_distro,
                                    qty_selected,
                                    qty_cancelled,
                                    qty_received,
                                    po_rcvd_qty,
                                    non_scale_ind)
                             select d.alloc_no,
                                    d.to_loc,
                                    d.to_loc_type,
                                    d.qty_transferred,
                                    d.qty_allocated,
                                    d.qty_prescaled,
                                    d.qty_distro,
                                    d.qty_selected,
                                    d.qty_cancelled,
                                    d.qty_received,
                                    d.po_rcvd_qty,
                                    d.non_scale_ind
                               from alloc_detail d
                              where d.alloc_no in (select h.alloc_no
                                                     from alloc_header h
                                                    where order_no = I_order_no
                                                      and item = NVL(I_item, item));

      insert into alloc_chrg_temp(alloc_no,
                                  to_loc,
                                  item,
                                  comp_id,
                                  to_loc_type,
                                  pack_item,
                                  comp_rate,
                                  per_count,
                                  per_count_uom,
                                  up_chrg_group,
                                  comp_currency,
                                  display_order)
                           select alloc_no,
                                  to_loc,
                                  item,
                                  comp_id,
                                  to_loc_type,
                                  pack_item,
                                  comp_rate,
                                  per_count,
                                  per_count_uom,
                                  up_chrg_group,
                                  comp_currency,
                                  display_order
                             from alloc_chrg
                            where alloc_no in (select alloc_no
                                                 from alloc_header
                                                where order_no = I_order_no
                                                  and item = NVL(I_item, item));
      ---
      if I_redist_ind = 'Y' then
         insert into wo_head_temp(wo_id,
                                  tsfalloc_no,
                                  order_no)
                          (select wo_id,
                                  tsfalloc_no,
                                  order_no
                             from wo_head wh
                            where order_no = I_order_no);

         ---
          insert into wo_detail_temp(wo_id,
                                     wh,
                                     item,
                                     location,
                                     loc_type,
                                     seq_no,
                                     wip_code)
                             (select wo_id,
                                     wh,
                                     item,
                                     location,
                                     loc_type,
                                     seq_no,
                                     wip_code
                                from wo_detail
                               where wo_id in (select wo_id
                                                 from wo_head
                                                where order_no = I_order_no)
                                                  and item = NVL(I_item, item));

      else   /* if I_redist_ind != 'Y' */
         insert into wo_head_temp(wo_id,
                                  tsfalloc_no,
                                  order_no)
                          (select wo_id,
                                  tsfalloc_no,
                                  order_no
                             from wo_head wh
                            where order_no = I_order_no);
         ---
          insert into wo_detail_temp(wo_id,
                                     wh,
                                     item,
                                     location,
                                     loc_type,
                                     seq_no,
                                     wip_code)
                             (select wo_id,
                                     wh,
                                     item,
                                     location,
                                     loc_type,
                                     seq_no,
                                     wip_code
                                from wo_detail
                               where wo_id in (select wo_id
                                                 from wo_head
                                                where order_no = I_order_no));
      end if;
   end if; /* end I_item_type != 'F' */
   ---
   commit;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG( 'PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_DIST_SQL.COPY_ORDLOC',
                                             to_char(SQLCODE));
      RETURN FALSE;
END COPY_ORDLOC;
----------------------------------------------------------------------------------------------
FUNCTION GET_ORD_RDST_QTY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item           IN OUT  ORDSKU.ITEM%TYPE,
                          O_qty_to_order   IN OUT  ORDLOC.QTY_ORDERED%TYPE,
                          I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_item                 ORDLOC_TEMP.ITEM%TYPE         := NULL;
   L_qty_ordered_temp     ORDLOC_TEMP.QTY_ORDERED%TYPE  := NULL;
   L_qty_ordered          ORDLOC.QTY_ORDERED%TYPE       := NULL;
   L_program              TRAN_DATA.PGM_NAME%TYPE   := 'ORDER_DIST_SQL.GET_ORD_RDST_QTY';

   CURSOR C_SKU is
      select item
        from ordsku
       where order_no = I_order_no;

   cursor C_OLT_SUM is
      select sum(qty_ordered)
        from ordloc_temp
       where item = L_item
         and order_no = I_order_no;

   cursor C_OL_SUM is
      select sum(qty_ordered)
        from ordloc
       where item = L_item
         and order_no = I_order_no;

BEGIN
  -- Loop through items on order.
  FOR rec in C_SKU LOOP
      L_item := rec.item;
      -- Get the sum of the qty_ordered and the sum of the act_qty.
      SQL_LIB.SET_MARK('OPEN', 'C_OLT_SUM', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
      open C_OLT_SUM;
      SQL_LIB.SET_MARK('FETCH', 'C_OLT_SUM', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
      fetch C_OLT_SUM into L_qty_ordered_temp;
      SQL_LIB.SET_MARK('CLOSE', 'C_OLT_SUM', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
      close C_OLT_SUM;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_OL_SUM', 'ordloc', 'order_no: '||to_char(I_order_no));
      open C_OL_SUM;
      SQL_LIB.SET_MARK('FETCH', 'C_OL_SUM', 'ordloc', 'order_no: '||to_char(I_order_no));
      fetch C_OL_SUM into L_qty_ordered;
      SQL_LIB.SET_MARK('CLOSE', 'C_OL_SUM', 'ordloc', 'order_no: '||to_char(I_order_no));
      close C_OL_SUM;
      -- Redistribution cannot continue until the correct qty is ordered for the item.
      if L_qty_ordered_temp != L_qty_ordered then
         O_item := rec.item;
         O_qty_to_order := L_qty_ordered_temp;
         return TRUE;
      else
         O_item := NULL;
         O_qty_to_order := NULL;
        end if;
   END LOOP;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END GET_ORD_RDST_QTY;
----------------------------------------------------------------------------------------------
FUNCTION REDIST_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_exist                VARCHAR2(1) := 'N';
   L_program              TRAN_DATA.PGM_NAME%TYPE := 'ORDER_DIST_SQL.REDIST_ORDER';
   L_count_ordloc         NUMBER;
   L_count_ordloc_wksht   NUMBER;
   L_loc                  ORDLOC_TEMP.LOCATION%TYPE := NULL;
   L_item                 ORDLOC_TEMP.ITEM%TYPE := NULL;
   L_status               VARCHAR2(1);
   L_dcq_exist            VARCHAR2(1) := 'N';

   CURSOR C_STATUS is
      select status
        from ordhead
       where order_no = I_order_no;

   CURSOR C_COMPARE_TEMP is
      select count(ot.item)
        from ordloc_temp ot
       where ot.order_no = I_order_no;

   CURSOR C_COMPARE_ORDLOC is
      select count(o.item)
        from ordloc o
       where o.order_no = I_order_no;

   CURSOR C_MINUS_TEMP is
      select item,
             location
        from ordloc_temp
       where order_no = I_order_no
       minus
      select item, location
        from ordloc
       where order_no = I_order_no;

   CURSOR C_MINUS_ORDLOC is
      select item,
             location
        from ordloc
       where order_no = I_order_no
       minus
      select item, location
        from ordloc_temp
       where order_no = I_order_no;

   CURSOR C_REV_ORDERS_REC is
      select 'X'
        from rev_orders
       where order_no = I_order_no;

   CURSOR C_DEAL_CALC_QUEUE_REC is
      select 'Y'
        from deal_calc_queue
       where order_no = I_order_no;
BEGIN

   SQL_LIB.SET_MARK('OPEN', 'C_COMPARE_TEMP', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
   open C_COMPARE_TEMP;
   SQL_LIB.SET_MARK('FETCH', 'C_COMPARE_TEMP', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
   fetch C_COMPARE_TEMP into L_count_ordloc;
   SQL_LIB.SET_MARK('CLOSE', 'C_COMPARE_TEMP', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
   close C_COMPARE_TEMP;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_COMPARE_ORDLOC', 'ordloc', 'order_no: '||to_char(I_order_no));
   open C_COMPARE_ORDLOC;
   SQL_LIB.SET_MARK('FETCH', 'C_COMPARE_ORDLOC', 'ordloc', 'order_no: '||to_char(I_order_no));
   fetch C_COMPARE_ORDLOC into L_count_ordloc_wksht;
   SQL_LIB.SET_MARK('CLOSE', 'C_COMPARE_ORDLOC', 'ordloc', 'order_no: '||to_char(I_order_no));
   close C_COMPARE_ORDLOC;
   ---
   if L_count_ordloc = L_count_ordloc_wksht then
      SQL_LIB.SET_MARK('OPEN', 'C_MINUS_TEMP', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
      open C_MINUS_TEMP;
      SQL_LIB.SET_MARK('FETCH', 'C_MINUS_TEMP', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
      fetch C_MINUS_TEMP into L_item,
                              L_loc;
      SQL_LIB.SET_MARK('CLOSE', 'C_MINUS_TEMP', 'ordloc_temp', 'order_no: '||to_char(I_order_no));
      close C_MINUS_TEMP;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_MINUS_ORDLOC', 'ordloc', 'order_no: '||to_char(I_order_no));
      open C_MINUS_ORDLOC;
      SQL_LIB.SET_MARK('FETCH', 'C_MINUS_ORDLOC', 'ordloc', 'order_no: '||to_char(I_order_no));
      fetch C_MINUS_ORDLOC into L_item,
                                L_loc;
      SQL_LIB.SET_MARK('CLOSE', 'C_MINUS_ORDLOC', 'ordloc', 'order_no: '||to_char(I_order_no));
      close C_MINUS_ORDLOC;
      ---
      if L_item is NOT NULL then
         SQL_LIB.SET_MARK('OPEN', 'C_REV_ORDERS_REC', 'rev_orders', 'order_no: '||to_char(I_order_no));
         open C_REV_ORDERS_REC;
         SQL_LIB.SET_MARK('FETCH', 'C_REV_ORDERS_REC', 'rev_orders', 'order_no: '||to_char(I_order_no));
         fetch C_REV_ORDERS_REC into L_exist;
         SQL_LIB.SET_MARK('CLOSE', 'C_REV_ORDERS_REC', 'rev_orders', 'order_no: '||to_char(I_order_no));
         close C_REV_ORDERS_REC;
         ---
         if L_exist = 'N' then
            insert into rev_orders(order_no)
               values(I_order_no);
         end if;
         ---
         delete from ordloc_temp
          where order_no = I_order_no;
         ---
         delete from ordsku_temp
          where order_no = I_order_no;
         ---
         delete from ordloc_exp_temp
          where order_no = I_order_no;
         ---
         delete from ordsku_hts_temp
          where order_no = I_order_no;
         ---
         delete from ordsku_hts_assess_temp
          where order_no = I_order_no;
         ---
         delete from ordloc_discount_temp
          where order_no = I_order_no;
         ---
         delete from alloc_chrg_temp
          where alloc_no in (select alloc_no
                               from alloc_header_temp
                              where order_no = I_order_no);
         ---
         delete from alloc_detail_temp
          where alloc_no in (select alloc_no
                               from alloc_header_temp
                              where order_no = I_order_no);
         ---
         delete from alloc_header_temp
          where order_no = I_order_no;
         ---
         delete from timeline_temp
          where key_value_1 = TO_CHAR(I_order_no);
         ---
         delete from req_doc_temp
          where key_value_1 = TO_CHAR(I_order_no);
         ---
         delete from wo_detail
          where wo_id in (select wo_id
                            from wo_head_temp
                           where order_no = I_order_no);
         ---
         delete from wo_head_temp
          where order_no = I_order_no;

      end if;
   else /* L_count_ordloc != L_count_ordloc_wksht */
      open C_REV_ORDERS_REC;
      fetch C_REV_ORDERS_REC into L_exist;
      close C_REV_ORDERS_REC;
      ---
      if L_exist = 'N' then
         insert into rev_orders(order_no)
            values(I_order_no);
      end if;
      ---
      delete from ordloc_temp
       where order_no = I_order_no;
      ---
      delete from ordsku_temp
       where order_no = I_order_no;
         ---
      delete from ordloc_exp_temp
       where order_no = I_order_no;
      ---
      delete from ordsku_hts_temp
       where order_no = I_order_no;
      ---
      delete from ordsku_hts_assess_temp
       where order_no = I_order_no;
      ---
      delete from ordloc_discount_temp
       where order_no = I_order_no;
      ---
      delete from alloc_chrg_temp
       where alloc_no in (select alloc_no
                            from alloc_header_temp
                           where order_no = I_order_no);
      ---
      delete from alloc_detail_temp
       where alloc_no in (select alloc_no
                            from alloc_header_temp
                           where order_no = I_order_no);
      ---
      delete from alloc_header_temp
       where order_no = I_order_no;
      ---
      delete from timeline_temp
       where key_value_1 = TO_CHAR(I_order_no);
      ---
      delete from req_doc_temp
       where key_value_1 = TO_CHAR(I_order_no);
      ---
      delete from wo_detail_temp
       where wo_id in (select wo_id
                         from wo_head_temp
                        where order_no = I_order_no);
      ---
      delete from wo_head_temp
       where order_no = I_order_no;
   end if;
   --if the status is 'A'pproved then write a record to deal_calc_queue
   SQL_LIB.SET_MARK('OPEN', 'C_STATUS', 'deal_calc_queue', 'order_no: '||to_char(I_order_no));
   open C_STATUS;
   SQL_LIB.SET_MARK('FETCH', 'C_STATUS', 'deal_calc_queue', 'order_no: '||to_char(I_order_no));
   fetch C_STATUS into L_status;
   SQL_LIB.SET_MARK('CLOSE', 'C_STATUS', 'deal_calc_queue', 'order_no: '||to_char(I_order_no));
   close C_STATUS;
   ---
   if L_status = 'A' then
      SQL_LIB.SET_MARK('OPEN', 'C_DEAL_CALC_QUEUE_REC', 'deal_calc_queue', 'order_no: '||to_char(I_order_no));
      open C_DEAL_CALC_QUEUE_REC;
      SQL_LIB.SET_MARK('FETCH', 'C_DEAL_CALC_QUEUE_REC', 'deal_calc_queue', 'order_no: '||to_char(I_order_no));
      fetch C_DEAL_CALC_QUEUE_REC into L_dcq_exist;
      SQL_LIB.SET_MARK('CLOSE', 'C_DEAL_CALC_QUEUE_REC', 'deal_calc_queue', 'order_no: '||to_char(I_order_no));
      close C_DEAL_CALC_QUEUE_REC;
      if L_dcq_exist = 'N' then
         insert into deal_calc_queue(order_no,
                                     recalc_all_ind,
                                     override_manual_ind,
                                     order_appr_ind)
            values(I_order_no, 'N', 'N', 'N');
      end if;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END REDIST_ORDER;
-----------------------------------------------------------------------------------------------
FUNCTION REBUILD_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program              TRAN_DATA.PGM_NAME%TYPE := 'ORDER_DIST_SQL.REBUILD_ORDER';
   L_approved             VARCHAR2(10) := NULL;
BEGIN
   if ORDER_ATTRIB_SQL.PREV_APPROVED(O_error_message,
                                     L_approved,
                                     I_order_no) = FALSE then
      return FALSE;
   end if;
   ---
   if L_approved = 'N' then
      insert into ordsku(order_no,
                         item,
                         ref_item,
                         origin_country_id,
                         earliest_ship_date,
                         latest_ship_date,
                         supp_pack_size,
                         non_scale_ind,
                         pickup_loc,
                         pickup_no)
                 (select order_no,
                         item,
                         ref_item,
                         origin_country_id,
                         earliest_ship_date,
                         latest_ship_date,
                         supp_pack_size,
                         non_scale_ind,
                         pickup_loc,
                         pickup_no
                    from ordsku_temp
                   where order_no = I_order_no);

             insert into repl_results(source_type,
                                      status,
                                      ord_temp_seq_no,
                                      tsf_po_link_no,
                                      order_no,
                                      alloc_no,
                                      master_item,
                                      item,
                                      item_type,
                                      dept,
                                      class,
                                      subclass,
                                      buyer,
                                      pack_qty,
                                      loc_type,
                                      location,
                                      physical_wh,
                                      primary_repl_supplier,
                                      origin_country_id,
                                      pool_supplier,
                                      supp_unit_cost,
                                      unit_cost,
                                      orig_raw_roq,
                                      orig_raw_roq_pack,
                                      raw_roq,
                                      raw_roq_pack,
                                      prescale_roq,
                                      order_roq,
                                      contract_roq,
                                      sourced_roq,
                                      last_rounded_qty,
                                      last_grp_rounded_qty,
                                      net_inventory,
                                      stock_on_hand,
                                      pack_comp_soh,
                                      on_order,
                                      in_transit_qty,
                                      pack_comp_intran,
                                      tsf_resv_qty,
                                      pack_comp_resv,
                                      tsf_expected_qty,
                                      pack_comp_exp,
                                      rtv_qty,
                                      alloc_in_qty,
                                      alloc_out_qty,
                                      non_sellable_qty,
                                      order_point,
                                      order_up_to_point,
                                      safety_stock,
                                      lost_sales,
                                      min_supply_days_forecast,
                                      max_supply_days_forecast,
                                      time_supply_horizon_forecast,
                                      inv_sell_days_forecast,
                                      review_time_forecast,
                                      order_lead_time_forecast,
                                      next_lead_time_forecast,
                                      accepted_stock_out,
                                      estimated_stock_out,
                                      due_ind,
                                      review_time,
                                      repl_order_ctrl,
                                      review_cycle,
                                      stock_cat,
                                      source_wh,
                                      activate_date,
                                      deactivate_date,
                                      pres_stock,
                                      demo_stock,
                                      repl_method,
                                      min_stock,
                                      max_stock,
                                      incr_pct,
                                      min_supply_days,
                                      max_supply_days,
                                      time_supply_horizon,
                                      inv_selling_days,
                                      service_level,
                                      lost_sales_factor,
                                      terminal_stock_qty,
                                      season_id,
                                      phase_id,
                                      reject_store_ord_ind,
                                      non_scaling_ind,
                                      max_scale_value,
                                      supp_lead_time,
                                      pickup_lead_time,
                                      wh_lead_time,
                                      next_order_lead_time,
                                      colt_added,
                                      nolt_added,
                                      inner_size,
                                      case_size,
                                      ti,
                                      hi,
                                      store_ord_mult,
                                      repl_date,
                                      recalc_type,
                                      recalc_qty,
                                      audsid)
                              (select source_type,
                                      status,
                                      ord_temp_seq_no,
                                      tsf_po_link_no,
                                      order_no,
                                      alloc_no,
                                      master_item,
                                      item,
                                      item_type,
                                      dept,
                                      class,
                                      subclass,
                                      buyer,
                                      pack_qty,
                                      loc_type,
                                      location,
                                      physical_wh,
                                      primary_repl_supplier,
                                      origin_country_id,
                                      pool_supplier,
                                      supp_unit_cost,
                                      unit_cost,
                                      orig_raw_roq,
                                      orig_raw_roq_pack,
                                      raw_roq,
                                      raw_roq_pack,
                                      prescale_roq,
                                      order_roq,
                                      contract_roq,
                                      sourced_roq,
                                      last_rounded_qty,
                                      last_grp_rounded_qty,
                                      net_inventory,
                                      stock_on_hand,
                                      pack_comp_soh,
                                      on_order,
                                      in_transit_qty,
                                      pack_comp_intran,
                                      tsf_resv_qty,
                                      pack_comp_resv,
                                      tsf_expected_qty,
                                      pack_comp_exp,
                                      rtv_qty,
                                      alloc_in_qty,
                                      alloc_out_qty,
                                      non_sellable_qty,
                                      order_point,
                                      order_up_to_point,
                                      safety_stock,
                                      lost_sales,
                                      min_supply_days_forecast,
                                      max_supply_days_forecast,
                                      time_supply_horizon_forecast,
                                      inv_sell_days_forecast,
                                      review_time_forecast,
                                      order_lead_time_forecast,
                                      next_lead_time_forecast,
                                      accepted_stock_out,
                                      estimated_stock_out,
                                      due_ind,
                                      review_time,
                                      repl_order_ctrl,
                                      review_cycle,
                                      stock_cat,
                                      source_wh,
                                      activate_date,
                                      deactivate_date,
                                      pres_stock,
                                      demo_stock,
                                      repl_method,
                                      min_stock,
                                      max_stock,
                                      incr_pct,
                                      min_supply_days,
                                      max_supply_days,
                                      time_supply_horizon,
                                      inv_selling_days,
                                      service_level,
                                      lost_sales_factor,
                                      terminal_stock_qty,
                                      season_id,
                                      phase_id,
                                      reject_store_ord_ind,
                                      non_scaling_ind,
                                      max_scale_value,
                                      supp_lead_time,
                                      pickup_lead_time,
                                      wh_lead_time,
                                      next_order_lead_time,
                                      colt_added,
                                      nolt_added,
                                      inner_size,
                                      case_size,
                                      ti,
                                      hi,
                                      store_ord_mult,
                                      repl_date,
                                      recalc_type,
                                      recalc_qty,
                                      audsid
                                 from repl_results_temp
                                where order_no = I_order_no);

   end if; -- L_approved = 'N'
   ---
   insert into ordloc(order_no,
                      item,
                      location,
                      loc_type,
                      unit_retail,
                      qty_ordered,
                      qty_prescaled,
                      qty_received,
                      last_received,
                      last_rounded_qty,
                      last_grp_rounded_qty,
                      qty_cancelled,
                      cancel_code,
                      cancel_date,
                      cancel_id,
                      original_repl_qty,
                      unit_cost,
                      unit_cost_init,
                      cost_source,
                      non_scale_ind,
                      tsf_po_link_no,
                      estimated_instock_date)
              (select order_no,
                      item,
                      location,
                      loc_type,
                      unit_retail,
                      qty_ordered,
                      qty_prescaled,
                      qty_received,
                      last_received,
                      last_rounded_qty,
                      last_grp_rounded_qty,
                      qty_cancelled,
                      cancel_code,
                      cancel_date,
                      cancel_id,
                      original_repl_qty,
                      unit_cost,
                      unit_cost_init,
                      cost_source,
                      non_scale_ind,
                      tsf_po_link_no,
                      NULL
                 from ordloc_temp
                where order_no = I_order_no);
   ---
   insert into ordloc_exp(order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2)
                  (select order_no,
                          seq_no,
                          item,
                          pack_item,
                          location,
                          loc_type,
                          comp_id,
                          cvb_code,
                          cost_basis,
                          comp_rate,
                          comp_currency,
                          exchange_rate,
                          per_count,
                          per_count_uom,
                          est_exp_value,
                          nom_flag_1,
                          nom_flag_2,
                          nom_flag_3,
                          nom_flag_4,
                          nom_flag_5,
                          origin,
                          display_order,
                          defaulted_from,
                          key_value_1,
                          key_value_2
                     from ordloc_exp_temp
                    where order_no = I_order_no);
   ---
   if L_approved = 'N' then
      insert into ordsku_hts(order_no,
                             seq_no,
                             item,
                             pack_item,
                             hts,
                             import_country_id,
                             origin_country_id,
                             effect_from,
                             effect_to,
                             status)
                     (select order_no,
                             seq_no,
                             item,
                             pack_item,
                             hts,
                             import_country_id,
                             origin_country_id,
                             effect_from,
                             effect_to,
                             status
                        from ordsku_hts_temp
                       where order_no = I_order_no);
      ---
      insert into ordsku_hts_assess(order_no,
                                    seq_no,
                                    comp_id,
                                    cvb_code,
                                    comp_rate,
                                    per_count,
                                    per_count_uom,
                                    est_assess_value,
                                    nom_flag_1,
                                    nom_flag_2,
                                    nom_flag_3,
                                    nom_flag_4,
                                    nom_flag_5,
                                    display_order)
                            (select order_no,
                                    seq_no,
                                    comp_id,
                                    cvb_code,
                                    comp_rate,
                                    per_count,
                                    per_count_uom,
                                    est_assess_value,
                                    nom_flag_1,
                                    nom_flag_2,
                                    nom_flag_3,
                                    nom_flag_4,
                                    nom_flag_5,
                                    display_order
                               from ordsku_hts_assess_temp
                              where order_no = I_order_no);
   end if;
   ---
   insert into ordloc_discount(order_no,
                               seq_no,
                               item,
                               location,
                               deal_id,
                               deal_detail_id,
                               pack_no,
                               discount_value,
                               discount_type,
                               discount_amt_per_unit,
                               application_order,
                               paid_ind,
                               last_calc_date)
                       (select order_no,
                               seq_no,
                               item,
                               location,
                               deal_id,
                               deal_detail_id,
                               pack_no,
                               discount_value,
                               discount_type,
                               discount_amt_per_unit,
                               application_order,
                               paid_ind,
                               last_calc_date
                          from ordloc_discount_temp
                         where order_no = I_order_no);
   ---
   insert into alloc_header(alloc_no,
                            order_no,
                            wh,
                            item,
                            status,
                            alloc_desc,
                            po_type,
                            alloc_method,
                            release_date,
                            order_type,
                            origin_ind,
                            context_type,
                            context_value,
                            comment_desc)
                    (select alloc_no,
                            order_no,
                            wh,
                            item,
                            status,
                            alloc_desc,
                            po_type,
                            alloc_method,
                            release_date,
                            NVL(order_type, 'PREDIST'),
                            origin_ind,
                            NULL,
                            NULL,
                            NULL
                       from alloc_header_temp
                      where order_no = I_order_no);

   ---
   insert into alloc_detail(alloc_no,
                            to_loc,
                            to_loc_type,
                            qty_transferred,
                            qty_allocated,
                            qty_prescaled,
                            qty_distro,
                            qty_selected,
                            qty_cancelled,
                            qty_received,
                            po_rcvd_qty,
                            non_scale_ind)
                     select d.alloc_no,
                            d.to_loc,
                            d.to_loc_type,
                            d.qty_transferred,
                            d.qty_allocated,
                            d.qty_prescaled,
                            d.qty_distro,
                            d.qty_selected,
                            d.qty_cancelled,
                            d.qty_received,
                            d.po_rcvd_qty,
                            d.non_scale_ind
                       from alloc_detail_temp d
                      where d.alloc_no in (select h.alloc_no
                                             from alloc_header_temp h
                                            where order_no = I_order_no);
   ---
   insert into alloc_chrg(alloc_no,
                          to_loc,
                          item,
                          comp_id,
                          to_loc_type,
                          pack_item,
                          comp_rate,
                          per_count,
                          per_count_uom,
                          up_chrg_group,
                          comp_currency,
                          display_order)
                   select alloc_no,
                          to_loc,
                          item,
                          comp_id,
                          to_loc_type,
                          pack_item,
                          comp_rate,
                          per_count,
                          per_count_uom,
                          up_chrg_group,
                          comp_currency,
                          display_order
                     from alloc_chrg_temp
                    where alloc_no in (select alloc_no
                                         from alloc_header_temp
                                        where order_no = I_order_no);

   ---
   if L_approved = 'N' then
      insert into timeline(timeline_key,
                           timeline_no,
                           timeline_type,
                           key_value_1,
                           key_value_2,
                           base_date,
                           step_no,
                           display_seq,
                           original_date,
                           revised_date,
                           actual_date,
                           reason_code,
                           comment_desc,
                           create_datetime,
                           last_update_datetime,
                           last_update_id)
                   (select timeline_key,
                           timeline_no,
                           timeline_type,
                           key_value_1,
                           key_value_2,
                           base_date,
                           step_no,
                           display_seq,
                           original_date,
                           revised_date,
                           actual_date,
                           reason_code,
                           comment_desc,
                           sysdate,
                           sysdate,
                           user
                      from timeline_temp
                     where key_value_1 = TO_CHAR(I_order_no));
      ---
      insert into req_doc(doc_key,
                          module,
                          key_value_1,
                          key_value_2,
                          doc_id,
                          doc_text)
                  (select doc_key,
                          module,
                          key_value_1,
                          key_value_2,
                          doc_id,
                          doc_text
                     from req_doc_temp
                    where key_value_1 = TO_CHAR(I_order_no));
   end if;
   ---
   insert into wo_head(wo_id,
                       tsfalloc_no,
                       order_no)
               (select wo_id,
                       tsfalloc_no,
                       order_no
                  from wo_head_temp
                 where order_no = I_order_no);
   ---
   insert into wo_detail
              (wo_id,
               wh,
               item,
               location,
               loc_type,
               seq_no,
               wip_code)
              (select wo_id,
                      wh,
                      item,
                      location,
                      loc_type,
                      seq_no,
                      wip_code
                 from wo_detail_temp
                where wo_id in (select wo_id
                                  from wo_head_temp
                                 where order_no = I_order_no));
   ---
   delete from ordloc_wksht where
     order_no = I_order_no;
   ---
   if ORDER_SETUP_SQL.DELETE_TEMP_TABLES(O_error_message,
                                         I_order_no) = FALSE then
      return FALSE;
   end if;
   ---

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;

END REBUILD_ORDER;
--------------------------------------------------------------------------------
FUNCTION CHECK_QTY (O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                    I_where_clause    IN        FILTER_TEMP.WHERE_CLAUSE%TYPE,
                    O_exist           IN OUT    BOOLEAN)
RETURN BOOLEAN IS

   L_cursor1              INTEGER;
   L_order_no             ORDLOC_WKSHT.ORDER_NO%TYPE := NULL;
   L_statement            VARCHAR2(9000);
   L_rows_processed       INTEGER;

BEGIN
   if I_where_clause is NOT NULL then
      L_statement := 'select order_no '||
                       'from ordloc_wksht '||
                      'where ordloc_wksht.wksht_qty = 0 '||
                       ' and ' || I_where_clause;

   elsif I_where_clause is NULL then
      L_statement := 'select order_no from ordloc_wksht
                      where ordloc_wksht.wksht_qty = 0';
   end if;
   ---
   L_cursor1 := DBMS_SQL.OPEN_CURSOR;
   DBMS_SQL.PARSE(L_cursor1, L_statement, DBMS_SQL.V7);
   DBMS_SQL.DEFINE_COLUMN(L_cursor1, 1, L_order_no);
   ---
   L_rows_processed := DBMS_SQL.EXECUTE(L_cursor1);
   ---
   if DBMS_SQL.FETCH_ROWS(L_cursor1) > 0 then
      DBMS_SQL.COLUMN_VALUE(L_cursor1, 1, L_order_no);
         if L_order_no is NOT NULL then
            O_exist := TRUE;
            DBMS_SQL.CLOSE_CURSOR(L_cursor1);
            return TRUE;
         elsif L_order_no is NULL then
            O_exist := FALSE;
            DBMS_SQL.CLOSE_CURSOR(L_cursor1);
            return TRUE;
         end if;
   end if;
   DBMS_SQL.CLOSE_CURSOR(L_cursor1);

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DIST_SQL.CHECK_QTY',
                                            NULL);
      return FALSE;
END CHECK_QTY;
-------------------------------------------------------------------------------
FUNCTION ITEM_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists            IN OUT BOOLEAN,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_item_type         IN     CODE_DETAIL.CODE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'ORDER_REDST_SQL.ITEM_EXISTS';
   L_exists  VARCHAR2(1)  := 'N';

   cursor C_CHECK_EXISTS is
      select 'Y'
        from ordredst_temp
       where order_no = I_order_no;

   cursor C_ITEM_EXISTS is
      select 'Y'
        from ordredst_temp
       where item = I_item
         and order_no = I_order_no;

   cursor C_CHILD_EXISTS is
      select 'Y'
        from ordredst_temp o,
             item_master i
       where order_no = I_order_no
         and ((i.item_parent = I_item
               and o.item    = i.item)
             or (o.item      = I_item
                 and o.item  = i.item_parent));

   cursor C_GRANDCHILD_EXISTS is
      select 'Y'
        from ordredst_temp o,
             item_master i
       where order_no = I_order_no
         and ((i.item_grandparent = I_item
               and o.item         = i.item)
             or (o.item           = I_item
                 and o.item       = i.item_grandparent));

BEGIN
   O_exists := FALSE;
   ---
   if I_item_type = 'A' then
      ---
      -- If I_item_type = 'A' that means the user is adding 'All Items'
      -- to be redistributed.  So if any items exist already on the
      -- table, O_exist should be TRUE.
      ---
      SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','ORDREDST_TEMP',NULL);
      open C_CHECK_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','ORDREDST_TEMP',NULL);
      fetch C_CHECK_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','ORDREDST_TEMP',NULL);
      close C_CHECK_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   elsif I_item_type = 'I' then
      SQL_LIB.SET_MARK('OPEN','C_ITEM_EXISTS','ORDREDST_TEMP',NULL);
      open C_ITEM_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_ITEM_EXISTS','ORDREDST_TEMP',NULL);
      fetch C_ITEM_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_ITEM_EXISTS','ORDREDST_TEMP',NULL);
      close C_ITEM_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   elsif I_item_type = 'P' then
      SQL_LIB.SET_MARK('OPEN','C_CHILD_EXISTS','ORDREDST_TEMP,ITEM_MASTER',NULL);
      open C_CHILD_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_CHILD_EXISTS','ORDREDST_TEMP,ITEM_MASTER',NULL);
      fetch C_CHILD_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_CHILD_EXISTS','ORDREDST_TEMP,ITEM_MASTER',NULL);
      close C_CHILD_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   elsif I_item_type = 'G' then
      SQL_LIB.SET_MARK('OPEN','C_GRANDCHILD_EXISTS','ORDREDST_TEMP,ITEM_MASTER',NULL);
      open C_GRANDCHILD_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_GRANDCHILD_EXISTS','ORDREDST_TEMP,ITEM_MASTER',NULL);
      fetch C_GRANDCHILD_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_GRANDCHILD_EXISTS','ORDREDST_TEMP,ITEM_MASTER',NULL);
      close C_GRANDCHILD_EXISTS;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ITEM_EXISTS;
-------------------------------------------------------------------------------
FUNCTION ALLOCS_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists            IN OUT BOOLEAN)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_REDST_SQL.ALLOCS_EXIST';
   L_exists    VARCHAR2(1)  := 'N';
   L_order_no  ORDHEAD.ORDER_NO%TYPE;
   L_item      ITEM_MASTER.ITEM%TYPE;

   cursor C_GET_REDST is
      select order_no,
             item,
             item_level,
             tran_level
        from ordredst_temp;

   cursor C_CHECK_EXISTS is
      select 'Y'
        from alloc_header
       where order_no = L_order_no;

   cursor C_ITEM_EXISTS is
      select 'Y'
        from alloc_header
       where order_no = L_order_no
         and item     = L_item;

   cursor C_CHILD_EXISTS is
      select 'Y'
        from alloc_header a,
             item_master i
       where a.order_no      = L_order_no
         and i.item_parent = L_item
         and a.item    = i.item;

   cursor C_GRANDCHILD_EXISTS is
      select 'Y'
        from alloc_header a,
             item_master i
       where a.order_no           = L_order_no
         and i.item_grandparent = L_item
         and a.item         = i.item;

BEGIN
   for C_rec in C_GET_REDST loop
      O_exists   := FALSE;
      L_order_no := C_rec.order_no;
      L_item     := C_rec.item;
      ---
      if L_item is NULL then
         SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','ALLOC_HEADER',NULL);
         open C_CHECK_EXISTS;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','ALLOC_HEADER',NULL);
         fetch C_CHECK_EXISTS into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','ALLOC_HEADER',NULL);
         close C_CHECK_EXISTS;
      else
         if C_rec.item_level = C_rec.tran_level then
            SQL_LIB.SET_MARK('OPEN','C_ITEM_EXISTS','ALLOC_HEADER',NULL);
            open C_ITEM_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_ITEM_EXISTS','ALLOC_HEADER',NULL);
            fetch C_ITEM_EXISTS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_ITEM_EXISTS','ALLOC_HEADER',NULL);
            close C_ITEM_EXISTS;
         elsif C_rec.item_level = (C_rec.tran_level - 1) then
            SQL_LIB.SET_MARK('OPEN','C_CHILD_EXISTS','ALLOC_HEADER,ITEM_MASTER',NULL);
            open C_CHILD_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_CHILD_EXISTS','ALLOC_HEADER,ITEM_MASTER',NULL);
            fetch C_CHILD_EXISTS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHILD_EXISTS','ALLOC_HEADER,ITEM_MASTER',NULL);
            close C_CHILD_EXISTS;
         else
            SQL_LIB.SET_MARK('OPEN','C_GRANDCHILD_EXISTS','ALLOC_HEADER,ITEM_MASTER',NULL);
            open C_GRANDCHILD_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_GRANDCHILD_EXISTS','ALLOC_HEADER,ITEM_MASTER',NULL);
            fetch C_GRANDCHILD_EXISTS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_GRANDCHILD_EXISTS','ALLOC_HEADER,ITEM_MASTER',NULL);
            close C_GRANDCHILD_EXISTS;
         end if;
      end if;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
         exit;
      end if;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ALLOCS_EXIST;
-------------------------------------------------------------------------------
FUNCTION APPTS_EXIST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists            IN OUT BOOLEAN)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_DIST_SQL.APPTS_EXIST';
   L_exists    VARCHAR2(1)  := 'N';
   L_order_no  ORDHEAD.ORDER_NO%TYPE;
   L_item      ITEM_MASTER.ITEM%TYPE;

   cursor C_GET_REDST is
      select order_no,
             item,
             item_level,
             tran_level
        from ordredst_temp;

   cursor C_CHECK_EXISTS is
      select 'Y'
        from appt_detail
       where doc      = L_order_no
         and doc_type = 'P';

   cursor C_ITEM_EXISTS is
      select 'Y'
        from appt_detail
       where doc      = L_order_no
         and doc_type = 'P'
         and item     = L_item;

   cursor C_CHILD_EXISTS is
      select 'Y'
        from appt_detail a,
             item_master i
       where a.doc      = L_order_no
         and a.doc_type = 'P'
         and i.item_parent = L_item
         and a.item    = i.item;

   cursor C_GRANDCHILD_EXISTS is
      select 'Y'
        from appt_detail a,
             item_master i
       where a.doc                = L_order_no
         and a.doc_type           = 'P'
         and i.item_grandparent = L_item
         and a.item         = i.item;


BEGIN
   for C_rec in C_GET_REDST loop
      O_exists   := FALSE;
      L_order_no := C_rec.order_no;
      L_item     := C_rec.item;
      ---
      if L_item is NULL then
         SQL_LIB.SET_MARK('OPEN','C_CHECK_EXISTS','APPT_DETAIL',NULL);
         open C_CHECK_EXISTS;
         SQL_LIB.SET_MARK('FETCH','C_CHECK_EXISTS','APPT_DETAIL',NULL);
         fetch C_CHECK_EXISTS into L_exists;
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_EXISTS','APPT_DETAIL',NULL);
         close C_CHECK_EXISTS;
      else
         if C_rec.item_level = C_rec.tran_level then
            SQL_LIB.SET_MARK('OPEN','C_ITEM_EXISTS','APPT_DETAIL',NULL);
            open C_ITEM_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_ITEM_EXISTS','APPT_DETAIL',NULL);
            fetch C_ITEM_EXISTS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_ITEM_EXISTS','APPT_DETAIL',NULL);
            close C_ITEM_EXISTS;
         elsif C_rec.item_level = (C_rec.tran_level - 1) then
            SQL_LIB.SET_MARK('OPEN','C_CHILD_EXISTS','APPT_DETAIL,ITEM_MASTER',NULL);
            open C_CHILD_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_CHILD_EXISTS','APPT_DETAIL,ITEM_MASTER',NULL);
            fetch C_CHILD_EXISTS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_CHILD_EXISTS','APPT_DETAIL,ITEM_MASTER',NULL);
            close C_CHILD_EXISTS;
         else
            SQL_LIB.SET_MARK('OPEN','C_GRANDCHILD_EXISTS','APPT_DETAIL,ITEM_MASTER',NULL);
            open C_GRANDCHILD_EXISTS;
            SQL_LIB.SET_MARK('FETCH','C_GRANDCHILD_EXISTS','APPT_DETAIL,ITEM_MASTER',NULL);
            fetch C_GRANDCHILD_EXISTS into L_exists;
            SQL_LIB.SET_MARK('CLOSE','C_GRANDCHILD_EXISTS','APPT_DETAIL,ITEM_MASTER',NULL);
            close C_GRANDCHILD_EXISTS;
         end if;
      end if;
      ---
      if L_exists = 'Y' then
         O_exists := TRUE;
         exit;
      end if;
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END APPTS_EXIST;
-------------------------------------------------------------------------------
FUNCTION CHECK_REDST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_alloc_exists      IN OUT BOOLEAN,
                     O_appt_exists       IN OUT BOOLEAN,
                     O_exist_to_redist   IN OUT BOOLEAN)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50) := 'ORDER_DIST_SQL.CHECK_REDST';
   L_order_no  ORDHEAD.ORDER_NO%TYPE;
   L_item      ITEM_MASTER.ITEM%TYPE;
   L_exist     VARCHAR2(1)  := 'N';

   cursor C_EXIST_TO_REDIST is
      select 'Y'
        from ordloc ol,
             ordredst_temp ot,
             item_master it
       where ol.order_no = ot.order_no
         and ((it.item_grandparent = nvl(ot.item, it.item_grandparent)
               and ol.item = it.item)
              or (it.item_parent = nvl(ot.item, it.item_parent)
                  and ol.item = it.item)
              or (ol.item = nvl(ot.item, ol.item)
              and ol.item = it.item))
         and not exists (select 'x'
                           from alloc_header a,
                                item_master im
                          where ot.order_no = a.order_no
                            and ol.item = a.item
                          and ((im.item_grandparent = nvl(ot.item, im.item_grandparent)
                                  and a.item = im.item)
                                or (im.item_parent = nvl(ot.item, im.item_parent)
                                  and a.item = im.item)
                                or (a.item = nvl(ot.item, a.item)
                                and a.item = im.item)))
         and not exists (select 'x'
                           from appt_detail d,
                                item_master i
                          where d.doc      = ot.order_no
                            and d.doc_type = 'P'
                            and ol.item = d.item
                            and ((i.item_grandparent = nvl(ot.item, i.item_grandparent)
                                  and d.item = i.item)
                                or (i.item_parent = nvl(ot.item, i.item_parent)
                                  and d.item = i.item)
                                or (d.item = nvl(ot.item, d.item)
                                and d.item = i.item)))
                                and rownum = 1;

BEGIN
   if ALLOCS_EXIST(O_error_message,
                   O_alloc_exists) = FALSE then
      return FALSE;
   end if;
   ---
   if APPTS_EXIST(O_error_message,
                  O_appt_exists) = FALSE then
      return FALSE;
   end if;
   ---
   O_exist_to_redist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_EXIST_TO_REDIST','ORDREDST_WKSHT, ORDLOC, ITEM_MASTER',NULL);
   open C_EXIST_TO_REDIST;
   SQL_LIB.SET_MARK('FETCH','C_EXIST_TO_REDIST','ORDREDST_WKSHT, ORDLOC, ITEM_MASTER',NULL);
   fetch C_EXIST_TO_REDIST into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_EXIST_TO_REDIST','ORDREDST_WKSHT, ORDLOC, ITEM_MASTER',NULL);
   close C_EXIST_TO_REDIST;
   ---
   if L_exist = 'Y' then
      O_exist_to_redist := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_REDST;
-------------------------------------------------------------------------------
FUNCTION CHECK_DIFFS_FOR_REDIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT BOOLEAN,
                                O_locations       IN OUT BOOLEAN,
                                I_item_parent     IN     ITEM_MASTER.ITEM%TYPE,
                                I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                                I_diff_1_ind      IN     VARCHAR2,
                                I_diff_2_ind      IN     VARCHAR2,
                                I_diff_3_ind      IN     VARCHAR2,
                                I_diff_4_ind      IN     VARCHAR2,
                                I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN is

   TYPE DIFF_CURSOR is REF CURSOR;
   C_DIFFS      DIFF_CURSOR;
   C_LOCS       DIFF_CURSOR;

   L_statement      VARCHAR2(9000);
   L_where_clause   FILTER_TEMP.WHERE_CLAUSE%TYPE;
   L_dummy          VARCHAR2(1)  := 'N';

BEGIN

   if I_where_clause is NOT NULL then
      L_where_clause := ' and ' || I_where_clause;
   else
      L_where_clause := NULL;
   end if;

   --- To be redistributed using the diff matrix, all records for an item parent must be
   --- distributed to the same level.  The diff indicators passed into this package indicate if
   --- each diff field had a value for the selected record in ordmtxws.  All other displayed records
   --- with the same parent must have the same combination.  If any differences are found then the item
   --- parent isn't valid for redistribution in the diff matrix.

   L_statement := 'select ''Y'''
                ||' from ordloc_wksht'
                ||' where order_no = :I_order_no'
                ||' and item_parent = :I_item_parent'
                ||' and (nvl(diff_1, ''-999'') = decode(:I_diff_1_ind, ''N'', diff_1, ''-999'')'
                ||' or nvl(diff_2, ''-999'') = decode(:I_diff_2_ind, ''N'', diff_2, ''-999'')'
                ||' or nvl(diff_3, ''-999'') = decode(:I_diff_3_ind, ''N'', diff_3, ''-999'')'
                ||' or nvl(diff_4, ''-999'') = decode(:I_diff_4_ind, ''N'', diff_4, ''-999''))'
                || L_where_clause;

   EXECUTE IMMEDIATE L_statement USING I_order_no, I_item_parent, I_diff_1_ind, I_diff_2_ind, I_diff_3_ind, I_diff_4_ind;
   open C_DIFFS for L_statement USING I_order_no, I_item_parent, I_diff_1_ind, I_diff_2_ind, I_diff_3_ind, I_diff_4_ind;
   fetch C_DIFFS into L_dummy;
   close C_DIFFS;

   if L_dummy = 'Y' then
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;

   if O_valid then
      --- Check if any records for the parent have location information.
      --- if so the user will be warned that the loc info will be lost upon redist.
      L_dummy := 'N';

      L_statement := 'select ''Y'''
                   ||' from ordloc_wksht'
                   ||' where order_no = :I_order_no'
                   ||' and item_parent = :I_item_parent'
                   ||' and location is NOT NULL'
                   || L_where_clause;

      EXECUTE IMMEDIATE L_statement USING I_order_no, I_item_parent;
      open C_LOCS for L_statement USING I_order_no, I_item_parent;
      fetch C_LOCS into L_dummy;
      close C_LOCS;

      if L_dummy = 'Y' then
         O_locations := TRUE;
      else
         O_locations := FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DIST_SQL.CHECK_DIFFS_FOR_REDIST',
                                            to_char(SQLCODE));
      return FALSE;
END CHECK_DIFFS_FOR_REDIST;

-------------------------------------------------------------------------------
--Function Name:  CLEAR_ORDREDST_TEMP
--Purpose      :  Deletes items for a specific order_no from the temp table
--                ORDREDST_TEMP.
-------------------------------------------------------------------------------
FUNCTION CLEAR_ORDREDST_TEMP (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no         IN      ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is
BEGIN
   delete
     from ordredst_temp
    where order_no = I_order_no;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_DIST_SQL.CLEAR_ORDREDST_TEMP',
                                            to_char(SQLCODE));

      return FALSE;
END CLEAR_ORDREDST_TEMP;
-------------------------------------------------------------------------------
--Function Name:  PROCESS_PACK_TMPL
--Purpose      :  Checks for the existence of the pack template related to the
--                entered pack and validates if there is an existing duplicate.
-------------------------------------------------------------------------------
FUNCTION PROCESS_PACK_TMPL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_pack_tmpl_exist     IN OUT   VARCHAR2,
                           O_dup_exist           IN OUT   VARCHAR2,
                           I_pack_no             IN       ORDLOC_WKSHT.ITEM%TYPE,
                           I_order_no            IN       ORDLOC_WKSHT.ORDER_NO%TYPE,
                           I_item_parent         IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                           I_pack_tmpl           IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                           I_pack_type           IN       VARCHAR2,
                           I_supplier            IN       SUPS.SUPPLIER%TYPE,
                           I_pack_desc           IN       ITEM_MASTER.ITEM_DESC%TYPE,
                           I_pack_short_desc     IN       ITEM_MASTER.ITEM_DESC%TYPE,
                           I_pack_vendor_desc    IN       ITEM_MASTER.ITEM_DESC%TYPE,
                           I_order_as_type       IN       VARCHAR2,
                           I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                           I_import_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)

   RETURN BOOLEAN IS

   L_program             VARCHAR2(64) := 'ORDER_DIST_SQL.VALIDATE_PACK_TMPL';
   L_item_rejected       BOOLEAN := FALSE;
   L_default_tax_type    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE;

   cursor C_PACK_TMPL_EXIST is
      select 'x'
        from item_master
       where item = I_pack_no;

   cursor C_CHECK_DUP is
      select 'Y'
        from ordloc_wksht
       where order_no = I_order_no
         and item = I_pack_no
         and location is NULL
         and store_grade is NULL
         and store_grade_group_id is NULL;

   cursor C_GET_DEF_TAX_TYPE is
      select default_tax_type
        from system_options;

BEGIN

   if I_pack_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_no',
                                            'NULL',
                                            'NOT NULL');
   end if;

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_item_parent is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item_parent',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_pack_tmpl is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_tmpl',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_pack_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_type',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_pack_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_desc',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_pack_short_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_short_desc',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_pack_vendor_desc is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_pack_vendor_desc',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_order_as_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_as_type',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_origin_country_id',
                                             'NULL',
                                             'NOT NULL');
   end if;

   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_import_country_id',
                                             'NULL',
                                             'NOT NULL');
   end if;

   --Initialize the values of the output parameters
   O_pack_tmpl_exist := NULL;
   O_dup_exist := 'N';

   open C_PACK_TMPL_EXIST;
   fetch C_PACK_TMPL_EXIST into O_pack_tmpl_exist;
   close C_PACK_TMPL_EXIST;

   if O_pack_tmpl_exist is NOT NULL then

      open C_CHECK_DUP;
      fetch C_CHECK_DUP into O_dup_exist;
      close C_CHECK_DUP;

   else
      if PACKITEM_ADD_SQL.NEW_PACK_TMPL(O_error_message,
                                        L_item_rejected,
                                        I_pack_no,
                                        I_item_parent,
                                        I_pack_tmpl,
                                        I_pack_type,
                                        I_supplier,
                                        I_pack_desc,
                                        I_pack_short_desc,
                                        I_pack_vendor_desc,
                                        I_order_as_type,
                                        I_origin_country_id,
                                        NULL,
                                        I_import_country_id) = FALSE then
         return FALSE;
      end if;

      open C_GET_DEF_TAX_TYPE;
      fetch C_GET_DEF_TAX_TYPE into L_default_tax_type;
      close C_GET_DEF_TAX_TYPE;

      if L_default_tax_type in ('GTAX') then
         if PACKITEM_ADD_SQL.UPD_ISC_PACK_TMPL (O_error_message) =  FALSE then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PROCESS_PACK_TMPL;
----------------------------------------------------------------------------------
--Function Name:  VALIDATE_NEW_ITEM
--Purpose      :  Validates new item and reference item number type
--                before adding to order worksheet.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_NEW_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid_item_ind     IN OUT   BOOLEAN,
                           I_contract_no        IN       CONTRACT_HEADER.CONTRACT_NO%TYPE,
                           I_dept_level         IN       PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE,
                           I_dept_order         IN       ITEM_MASTER.DEPT%TYPE,
                           I_order_type         IN       ORDHEAD.ORDER_TYPE%TYPE,
                           I_supplier           IN       ORDHEAD.SUPPLIER%TYPE,
                           I_location           IN       ORDLOC.LOCATION%TYPE,
                           I_fash_prepack_ind   IN       PACK_TMPL_HEAD.FASH_PREPACK_IND%TYPE,
                           I_item               IN       ITEM_MASTER.ITEM%TYPE,
                           I_ref_item           IN       ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program            VARCHAR2(50) := 'ORDER_DIST_SQL.VALIDATE_NEW_ITEM';

   L_item_record        ITEM_MASTER%ROWTYPE;
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;

   L_status             ITEM_MASTER.STATUS%TYPE;
   L_item_level         ITEM_MASTER.ITEM_LEVEL%TYPE;
   L_tran_level         ITEM_MASTER.TRAN_LEVEL%TYPE;
   L_purchase_type      DEPS.PURCHASE_TYPE%TYPE;

   L_item_supp_exist    BOOLEAN;
   L_item               ITEM_MASTER.ITEM%TYPE;
   L_item_desc          ITEM_MASTER.ITEM_DESC%TYPE;
   L_ref_item_desc      ITEM_MASTER.ITEM_DESC%TYPE;
   L_gparent            ITEM_MASTER.ITEM_GRANDPARENT%TYPE;
   L_gparent_desc       ITEM_MASTER.ITEM_DESC%TYPE;

   L_diff_1_desc        V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_1_type        V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_1_group       V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;

   L_diff_2_desc        V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_2_type        V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_2_group       V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;

   L_diff_3_desc        V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_3_type        V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_3_group       V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;

   L_diff_4_desc        V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE;
   L_diff_4_type        V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
   L_diff_4_group       V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;

   L_exists             VARCHAR2(1)     := NULL;
   L_delete_ind         VARCHAR2(1)     := NULL;
   L_stockholding_ind   VARCHAR2(1)     := NULL;
   L_direct_ship_ind    VARCHAR2(1)     := NULL;
   L_dummy              VARCHAR2(1)     := NULL;
   L_ind                VARCHAR2(1)     := NULL;
   L_cont_exists        BOOLEAN         := FALSE;
   L_pack_ind           VARCHAR2(1)     := 'N';
   L_del_ref_item_ind  VARCHAR2(1)      := 'N';

   cursor C_CHECK_DEL_ITEM (p_parent_item ITEM_MASTER.ITEM%TYPE,
                            p_grand_item  ITEM_MASTER.ITEM%TYPE)
   is
      select 'Y'
        from daily_purge
       where key_value in (I_item,p_parent_item,p_grand_item);

   cursor C_CHECK_PREPACK is
      select 'x'
        from packitem pi,
             pack_tmpl_head pth
       where pi.pack_no = I_item
         and pth.fash_prepack_ind = 'Y'
         and pi.pack_tmpl_id = pth.pack_tmpl_id;

   cursor C_CHECK_SKU is
      select 'x'
        from v_packsku_qty vpq,
             item_master im
       where vpq.pack_no = I_item
         and vpq.item = im.item
         and im.dept != I_dept_order;

   cursor C_CHECK_DEL_REF_ITEM is
      select 'Y'
        from daily_purge
       where key_value = I_ref_item;

BEGIN

   if (I_item is NULL and I_ref_item is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if (I_supplier is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_supplier',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if (I_dept_level is NULL) then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_dept_level',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   -- Validate ITEM
   if (I_item is NOT NULL and I_ref_item is NULL) then

      -- Get item master record for the item
      if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(L_error_message,
                                         L_item_record,
                                         I_item) = FALSE then
         O_error_message := L_error_message;
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;


      -- Item must be orderable.
      if L_item_record.orderable_ind != 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('INCORRECT_ORDERABLE_IND',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- For Customer Orders, the item must be sellable.
      if (I_order_type = 'CO' and L_item_record.sellable_ind = 'N') then
         O_error_message := SQL_LIB.CREATE_MSG('NO_NON_SELLABLE_CO',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Check Deposit Item Type
      if L_item_record.deposit_item_type = 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_DEPOSIT_ITEM_TYPE',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Catchweight and Deposit crate items cannot be added to a Customer Order
      if I_order_type = 'CO' then
         if L_item_record.catch_weight_ind = 'Y' then
                 O_error_message := SQL_LIB.CREATE_MSG('NO_CATCHWEIGHT_CO',
                                                        NULL,
                                                        NULL,
                                                        NULL);
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;
         ---
         if L_item_record.deposit_item_type = 'Z' then
                 O_error_message := SQL_LIB.CREATE_MSG('NO_DEPOSIT_CRATE_CO',
                                                        NULL,
                                                        NULL,
                                                        NULL);
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;
      end if;

      -- Item must be in 'A'pproved status and not candidate for deletion.
      if L_item_record.status != 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_APP_ORDER',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      else
         open C_CHECK_DEL_ITEM (L_item_record.item_parent,L_item_record.item_grandparent);
         fetch C_CHECK_DEL_ITEM into L_delete_ind;
         close C_CHECK_DEL_ITEM;

         if L_delete_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ORDER_FROM_DP',
                                                   I_item,
                                                   NULL,
                                                   NULL);
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;
         ---
      end if;

      if L_item_record.item_level = 1 and L_item_record.tran_level = 3 then
         O_error_message := SQL_LIB.CREATE_MSG('NO_GRAND_ON_ORD',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Get DIFF information
      if L_item_record.diff_1 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                   L_diff_1_desc,
                                   L_diff_1_type,
                                   L_diff_1_group,
                                   L_item_record.diff_1) = FALSE then
            O_error_message := L_error_message;
            return FALSE;
         end if;
      end if;

      if L_item_record.diff_2 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                   L_diff_2_desc,
                                   L_diff_2_type,
                                   L_diff_2_group,
                                   L_item_record.diff_2) = FALSE then
            O_error_message := L_error_message;
            return FALSE;
         end if;
      end if;

      if L_item_record.diff_3 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                   L_diff_3_desc,
                                   L_diff_3_type,
                                   L_diff_3_group,
                                   L_item_record.diff_3) = FALSE then
            O_error_message := L_error_message;
            return FALSE;
         end if;
      end if;

      if L_item_record.diff_4 is NOT NULL then
         if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                   L_diff_4_desc,
                                   L_diff_4_type,
                                   L_diff_4_group,
                                   L_item_record.diff_4) = FALSE then
            O_error_message := L_error_message;
            return FALSE;
         end if;
      end if;

      if ((L_item_record.item_level < L_item_record.tran_level) and
          (NVL(L_diff_1_group, 'ID') != 'GROUP') and
          (NVL(L_diff_2_group, 'ID') != 'GROUP') and
          (NVL(L_diff_3_group, 'ID') != 'GROUP') and
          (NVL(L_diff_4_group, 'ID') != 'GROUP')) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PARENT_WO_DIFF_ON_ORD',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Check if reference item
      if L_item_record.item_level > L_item_record.tran_level then
         O_error_message := SQL_LIB.CREATE_MSG('USE_REF_ITEM',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      if SUPP_ITEM_SQL.EXIST(L_error_message,
                             L_item_supp_exist,
                             I_item,
                             I_supplier) = FALSE then
         O_error_message := L_error_message;
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Check if item/supplier relationship exists.
      if L_item_supp_exist = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPP_ORDER',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      --For Drop Ship Customer Orders (location is a virtual store), the item should be
      --supported by Customer Direct Delivery by the supplier
      if I_order_type = 'CO' then
         if STORE_ATTRIB_SQL.GET_STOCKHOLDING_IND(L_error_message,
                                                  L_stockholding_ind,
                                                  I_location) = FALSE then
            O_error_message := L_error_message;
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;

         if L_stockholding_ind = 'N' then
            if ITEM_ATTRIB_SQL.GET_DIRECT_SHIP_IND(L_error_message,
                                                   L_direct_ship_ind,
                                                   I_item,
                                                   I_supplier) = FALSE then
               O_error_message := L_error_message;
               O_valid_item_ind := FALSE;
               return FALSE;
            end if;
            ---
            if L_direct_ship_ind = 'N' then
               O_error_message := SQL_LIB.CREATE_MSG('NOT_DIRECT_SHIP',
                                                     I_supplier,
                                                     I_item,
                                                     NULL);
               O_valid_item_ind := FALSE;
               return FALSE;
            elsif L_item_record.container_item is NOT NULL then
               if ITEM_ATTRIB_SQL.GET_DIRECT_SHIP_IND(L_error_message,
                                                      L_direct_ship_ind,
                                                      L_item_record.container_item,
                                                      I_supplier) = FALSE then
                  O_error_message := L_error_message;
                  O_valid_item_ind := FALSE;
                  return FALSE;
               end if;
               ---
               if L_direct_ship_ind = 'N' then
                  O_error_message := SQL_LIB.CREATE_MSG('NOT_DIRECT_SHIP',
                                                         I_supplier,
                                                         L_item_record.container_item,
                                                         NULL);
                  O_valid_item_ind := FALSE;
                  return FALSE;
               end if;
            end if;
         end if;
      end if; -- end if I_order_type = 'CO'

      -- Check that if dept_level = 'Y' and I_dept_order is not NULL
      -- and that the item is on the order's department.
      if ((I_dept_level = 'Y') and (I_dept_order is NOT NULL) and (L_item_record.dept != I_dept_order)) then
         O_error_message := SQL_LIB.CREATE_MSG('ITEM_NOT_IN_ORD_DEPT',
                                                I_item,
                                                I_dept_order,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Check if orders is associated with consignment
      if ITEM_ATTRIB_SQL.PURCHASE_TYPE(L_error_message,
                                       L_purchase_type,
                                       I_item) = FALSE then
         O_error_message := L_error_message;
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      if L_purchase_type = 1 then
         O_error_message := SQL_LIB.CREATE_MSG('CSMT_ITEMS',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Check if the SKU is on contract number
      if I_contract_no is not NULL then
         if CONTRACT_SQL.ITEM_ON_CONTRACT(L_error_message,
                                          L_cont_exists,
                                          L_ind,
                                          I_contract_no,
                                          I_item) = FALSE then
            O_error_message := L_error_message;
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;
         ---
         if L_cont_exists = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG('SKU_NOT_ON_CONTRACT',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;
      end if;

      if I_fash_prepack_ind = 'Y' and L_item_record.pack_ind = 'Y' then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PACK_ON_FASH_PREPACK',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Check prepack attributes
      open C_CHECK_PREPACK;
      fetch C_CHECK_PREPACK into L_exists;
      close C_CHECK_PREPACK;

      if L_exists is NOT NULL then
         O_error_message := SQL_LIB.CREATE_MSG('NO_PREPACK_ALLOWED',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Pack must be orderable
      if L_item_record.pack_ind = 'Y' then
         if L_item_record.orderable_ind = 'N' then
            O_error_message := SQL_LIB.CREATE_MSG('PACK_NOT_ORDERABLE',
                                                   NULL,
                                                   NULL,
                                                   NULL);
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;

         -- Check that the dept_level_orders = 'Y' and the item is a pack and dept
         -- is not NULL, that every sku in the pack is also in the order's department.
         if I_dept_level = 'Y' and I_dept_order is NOT NULL then
            if L_item_record.pack_type = 'B' and L_item_record.order_as_type = 'E' then
               ---
               open C_CHECK_SKU;
               fetch C_CHECK_SKU into L_dummy;
               close C_CHECK_SKU;
               ---
               if L_dummy is NOT NULL then
                  O_error_message := SQL_LIB.CREATE_MSG('PACK_SKU_DEPT',
                                                         NULL,
                                                         NULL,
                                                         NULL);
                  O_valid_item_ind := FALSE;
                  return FALSE;
               end if;
            end if;
         end if;
         ---
      end if;

   elsif (I_ref_item is NOT NULL and I_item is NULL) then

      -- Check if reference item is marked for deletion
      open  C_CHECK_DEL_REF_ITEM;
      fetch C_CHECK_DEL_REF_ITEM into L_del_ref_item_ind;
      close C_CHECK_DEL_REF_ITEM;

      if ITEM_ATTRIB_SQL.GET_DESC(L_error_message,
                                  L_ref_item_desc,
                                  L_status,
                                  L_item_level,
                                  L_tran_level,
                                  I_ref_item) = FALSE then
         O_error_message := L_error_message;
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      if L_status != 'A' then
         O_error_message := SQL_LIB.CREATE_MSG('PACK_SKU_DEPT',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      else
         if L_del_ref_item_ind = 'Y' then
            O_error_message := SQL_LIB.CREATE_MSG('NO_ORDER_FROM_DP',
                                                   I_ref_item,
                                                   NULL,
                                                   NULL);
            O_valid_item_ind := FALSE;
            return FALSE;
         end if;
      end if;

      if L_item_level <= L_tran_level then
         O_error_message := SQL_LIB.CREATE_MSG('USE_MAIN_ITEM',
                                                I_ref_item,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;
      ---
      if SUPP_ITEM_SQL.EXIST(L_error_message,
                             L_item_supp_exist,
                             I_ref_item,
                             I_supplier) = FALSE then
         O_error_message := L_error_message;
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      if L_item_supp_exist = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SUPP_ORDER',
                                                NULL,
                                                NULL,
                                                NULL);
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;
      ---
      if ITEM_ATTRIB_SQL.GET_PARENT_INFO(L_error_message,
                                         L_item,
                                         L_item_desc,
                                         L_gparent,
                                         L_gparent_desc,
                                         I_ref_item) = FALSE then
         O_error_message := L_error_message;
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;

      -- Call VALIDATE_NEW_ITEM passing the parent item (L_item) as main item
      if ORDER_DIST_SQL.VALIDATE_NEW_ITEM(O_error_message,
                                          O_valid_item_ind,
                                          I_contract_no,
                                          I_dept_level,
                                          I_dept_order,
                                          I_order_type,
                                          I_supplier,
                                          I_location,
                                          I_fash_prepack_ind,
                                          L_item,
                                          NULL) = FALSE then
         O_valid_item_ind := FALSE;
         return FALSE;
      end if;
   end if;
   ---
   O_valid_item_ind := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_NEW_ITEM;
-------------------------------------------------------------------------------------------
END;
/
