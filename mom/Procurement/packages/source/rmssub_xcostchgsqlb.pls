CREATE OR REPLACE PACKAGE BODY RMSSUB_XCOSTCHG_SQL AS

   LP_no_loc           BOOLEAN;
   LP_multi_loc        BOOLEAN;
   LP_single_loc       BOOLEAN;
   LP_ph_records_exist BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTION SPECS
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_ISCL_ITEMS
   -- Purpose      : This private function updates unit_cost in item_supp_country_loc table for the items.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISCL_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_items           IN       ITEM_TBL,
                           I_unit_cost       IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                           I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                           I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_ISC_ITEMS
   -- Purpose      : This private function updates unit_cost in item_supp_country table for the items.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISC_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_isc_rows        IN       RMSSUB_XCOSTCHG.ROWID_TBL,     -- Used by packs
                          I_items           IN       ITEM_TBL,
                          I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                          I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: INSERT_PRICE_HIST
   -- Purpose      : This private function inserts into the price_hist table for the items/locations
   --                included in the price_hist_rec node of the COST_CHANGE_RECTYPE record.
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_HIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_ITEMLOC_SOH
   -- Purpose      : This private function updates unit_cost in item_loc_soh table for the
   --                items/locations included in I_item_locs.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEMLOC_SOH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_items           IN       ITEM_TBL,
                            I_item_locs       IN       RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: UPDATE_ORDLOC
   -- Purpose      : This private function updates unit_cost in the ordloc table for the
   --                items/locations included in I_ord_recs.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_items           IN       ITEM_TBL,
                       I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
   -- Function Name: REBUILD_PACK_COST
   -- Purpose      : This private function rebuilds the pack costs. This inserts to the price_hist
   --                table.
-------------------------------------------------------------------------------------------------------
FUNCTION REBUILD_PACK_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_packs               IN       ITEM_TBL,
                           I_hier_locs           IN       LOC_TBL,
                           I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                           I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                           I_diff_id             IN       DIFF_IDS.DIFF_ID%TYPE,
                           I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                           I_currency_code       IN       STORE.CURRENCY_CODE%TYPE,
                           I_hier_level          IN       VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_CHANGE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_costchg_num         IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                            I_costchg_rec         IN       RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                            I_message             IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTION
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_costchg_rec     IN       RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                 I_message         IN       "RIB_XCostChgDesc_REC")

   RETURN BOOLEAN IS

   L_program         VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.PERSIST';

   TAB_packs         ITEM_TBL     := ITEM_TBL(NULL);

   L_loc_subquery    VARCHAR2(1000);
   L_packs_query     VARCHAR2(1000);

   L_table_bind_no   VARCHAR2(1) := '1';
   L_isc_exist       BOOLEAN     := TRUE;
   L_packs_count     NUMBER      := 0;
   L_costchg_num     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   L_reason          COST_SUSP_SUP_HEAD.REASON%TYPE;
   
   cursor c_get_cc_reason is
   select h.reason
     from cost_susp_sup_head h
    where h.cost_change = L_costchg_num;
BEGIN

   LP_no_loc     := FALSE;
   LP_multi_loc  := FALSE;
   LP_single_loc := FALSE;


   -- Check for number of locations
   if I_costchg_rec.single_itemloc_ind = 'Y' then
      LP_single_loc := TRUE;

   elsif I_message.xcostchghrdtl_tbl is NULL OR I_message.xcostchghrdtl_tbl.COUNT = 0 then
      LP_no_loc := TRUE;

   else   --this includes single location from store level with diff_id
      LP_multi_loc := TRUE;
   end if;
   ---

   -- Insert into cost change tables and create a Future cost event.
   if NOT INSERT_COST_CHANGE(O_error_message,
                             L_costchg_num,
                             I_costchg_rec,
                             I_message) then
      return FALSE;
   end if;
   ---
   open c_get_cc_reason;	
   fetch c_get_cc_reason into L_reason;
   close c_get_cc_reason;
   ---
   if COST_EXTRACT_SQL.BULK_UPDATE_COSTS(O_error_message,
                                         L_costchg_num,
                                         L_reason) = FALSE then
      return false;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END PERSIST;
-----------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-----------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISCL_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_items           IN       ITEM_TBL,
                           I_unit_cost       IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                           I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                           I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.UPDATE_ISCL_ITEMS';

BEGIN

   if I_items is NOT NULL AND I_items.COUNT > 0 then      -- NO Locs 
      if NOT UPDATE_BASE_COST.BULK_ISCL(O_error_message,
                                        I_items,
                                        I_unit_cost,
                                        I_supplier,
                                        I_country) then
         return FALSE;
      end if;
   else                                                   -- Multi/Single Locs 
      if NOT UPDATE_BASE_COST.BULK_ISCL(O_error_message,
                                        I_unit_cost) then
         return FALSE;
      end if;

   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ISCL_ITEMS;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ISC_ITEMS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_isc_rows        IN       RMSSUB_XCOSTCHG.ROWID_TBL,     -- Used by packs
                          I_items           IN       ITEM_TBL,
                          I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                          I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.UPDATE_ISC_ITEMS';

BEGIN

   if I_isc_rows is NOT NULL and I_isc_rows.count > 0 then
      if NOT UPDATE_BASE_COST.BULK_ISC(O_error_message,
                                       I_isc_rows,
                                       I_unit_cost) then
         return FALSE; 
      end if;
   else
      if NOT UPDATE_BASE_COST.BULK_ISC(O_error_message,
                                       I_items,
                                       I_unit_cost,
                                       I_supplier,
                                       I_country) then
         return FALSE; 
      end if;
   end if;

   ---

   return TRUE;

EXCEPTION
   when OTHERS then     
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ISC_ITEMS;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_HIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.INSERT_PRICE_HIST';

   L_new_price_rec    PRICE_HIST_SQL.NEW_PRICE_REC;
   L_reason           PRICE_HIST.REASON%TYPE := 4;
   L_record_type      VARCHAR2(1)            := 'C';
   L_tran_type        NUMBER(2)              := 2;
   L_exists           VARCHAR2(1);

   cursor C_PH_EXIST is
      select 'x' 
        from api_price_hist
       where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;

BEGIN
   LP_ph_records_exist := FALSE;

   open C_PH_EXIST;
   fetch C_PH_EXIST into L_exists;
   if C_PH_EXIST%FOUND then
      LP_ph_records_exist := TRUE;
   end if;
   close C_PH_EXIST;

   if LP_ph_records_exist then

      L_new_price_rec.tran_type               := L_tran_type;

      if NOT PRICE_HIST_SQL.BULK_INSERT_PRICE_HIST(O_error_message,
                                                   L_new_price_rec,
                                                   L_record_type,
                                                   L_reason,
                                                   NULL) then
         return FALSE;
      end if;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END INSERT_PRICE_HIST;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ITEMLOC_SOH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_items           IN       ITEM_TBL,
                            I_item_locs       IN       RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.UPDATE_ITEMLOC_SOH';

BEGIN

   if (I_items is NOT NULL AND I_items.COUNT > 0) AND
      (I_item_locs.locs is NOT NULL AND I_item_locs.locs.COUNT > 0) then
      if NOT UPDATE_BASE_COST.BULK_ITEMLOC_SOH(O_error_message,
                                               I_items,
                                               I_item_locs) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ITEMLOC_SOH;
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_ORDLOC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_items           IN       ITEM_TBL,
                       I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.UPDATE_ORDLOC';

BEGIN

   FOR i in I_items.FIRST..I_items.LAST LOOP
      if NOT COST_EXTRACT_SQL.UPDATE_APPROVED_ORDERS(O_error_message,
                                                     I_items(i),
                                                     I_unit_cost) then
         return FALSE;
      end if;
   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END UPDATE_ORDLOC;
-------------------------------------------------------------------------------------------------------
FUNCTION REBUILD_PACK_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_packs               IN       ITEM_TBL,
                           I_hier_locs           IN       LOC_TBL,
                           I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                           I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                           I_diff_id             IN       DIFF_IDS.DIFF_ID%TYPE,
                           I_unit_cost           IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                           I_currency_code       IN       STORE.CURRENCY_CODE%TYPE,
                           I_hier_level          IN       VARCHAR2)
   RETURN BOOLEAN IS

   L_program              VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.REBUILD_PACK_COST';

   TAB_iscl_rowids        RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_isc_rowids         RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_isc_rows           RMSSUB_XCOSTCHG.ROWID_TBL;
   TAB_packs              ITEM_TBL;

   L_price_hist_rec       RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE;

   L_iscl_pack_ind        BOOLEAN := FALSE;

   L_count                NUMBER;

BEGIN

   -- Refresh ISCL temp table
   delete api_iscl where seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;
   
   -- Populate the ISCL_TEMP for multi/single locs
   if I_hier_locs is NOT NULL and I_hier_locs.count > 0 then    -- Multi_locs 
      FOR j in I_packs.first..I_packs.last LOOP
         FORALL i in I_hier_locs.first..I_hier_locs.last
            insert into api_iscl (iscl_rowid,
                                  item,
                                  loc,
                                  primary_loc_ind,
                                  seq_no)
               select iscl.rowid,
                      iscl.item,
                      iscl.loc,
                      iscl.primary_loc_ind,
                      RMSSUB_XCOSTCHG.LP_api_seq_no
                 from item_supp_country_loc iscl
                where iscl.loc = I_hier_locs(i)
                  and iscl.origin_country_id = I_origin_country_id
                  and iscl.supplier = I_supplier
                  and iscl.item = I_packs(j);

         if NOT L_iscl_pack_ind and SQL%FOUND then
            L_iscl_pack_ind := TRUE;                  -- Indicates that there is a pack_record to be processed
         end if;
      end LOOP;
   else
      TAB_packs := I_packs;
   end if;

   if L_iscl_pack_ind then
      if NOT UPDATE_BASE_COST.BULK_PACK_ISCL(O_error_message,
                                             TAB_packs,
                                             I_origin_country_id,
                                             I_supplier) then
         return FALSE;
      end if;

      if NOT RMSSUB_XCOSTCHG_VALIDATE.BUILD_PACK_PRICE_RECORDS(O_error_message,
                                                               L_price_hist_rec,
                                                               TAB_isc_rows,
                                                               I_packs,
                                                               I_hier_locs,
                                                               I_hier_level,
                                                               I_supplier,
                                                               I_origin_country_id,
                                                               I_unit_cost,
                                                               I_currency_code) then
         return FALSE;
      end if;

      if NOT INSERT_PRICE_HIST(O_error_message) then
         return FALSE;
      end if;

      if NOT UPDATE_BASE_COST.BULK_PACK_ISC(O_error_message,
                                            TAB_isc_rows,
                                            TAB_packs,
                                            I_supplier,
                                            I_origin_country_id) then
            return FALSE;
      end if;
   else
      if NOT UPDATE_BASE_COST.BULK_PACK_ISC(O_error_message,
                                            NULL,
                                            TAB_packs,
                                            I_supplier,
                                            I_origin_country_id) then
         return FALSE;
      end if;

      -- Process all locations if no locations from the message was passed in. (NO locs processing)
      if I_hier_locs is NULL or I_hier_locs.count = 0 then
         if NOT UPDATE_BASE_COST.BULK_PACK_ISCL(O_error_message,
                                                TAB_packs,
                                                I_origin_country_id,
                                                I_supplier) then
            return FALSE;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;

END REBUILD_PACK_COST;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_CHANGE(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_costchg_num         IN OUT   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                            I_costchg_rec         IN       RMSSUB_XCOSTCHG.COST_CHANGE_RECTYPE,
                            I_message             IN       "RIB_XCostChgDesc_REC")
   RETURN BOOLEAN IS

   L_program                VARCHAR2(50) := 'RMSSUB_XCOSTCHG_SQL.INSERT_COST_CHANGE';   
   
   L_next_cc_num            COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   L_return_code            VARCHAR2(5);
   L_vdate                  period.vdate%TYPE := DATES_SQL.GET_VDATE;
   
   L_cost_event_process_id  COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_cost_changes_rec       OBJ_CC_COST_EVENT_REC;
   L_cost_changes_tbl       OBJ_CC_COST_EVENT_TBL;
   L_event_run_type         COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;
      
   cursor C_GET_RUN_TYPE is
      select event_run_type 
        from cost_event_run_type_config
       where event_type='CC'; 
      
BEGIN
   
   NEXT_COSTCHG_NUMBER(L_next_cc_num,
                       L_return_code,
                       O_error_message);
                       
   if L_return_code = 'FALSE' then
      return FALSE;
   end if;
   
   insert into cost_susp_sup_head(cost_change,
                                  cost_change_desc,
                                  reason,
                                  active_date,
                                  status,
                                  cost_change_origin,
                                  create_date,
                                  create_id,
                                  approval_date,
                                  approval_id)
                           values(L_next_cc_num,
                                  'XCOSTCHG',
                                  10,
                                  L_vdate,
                                  'E',
                                  'SUP',
                                  L_vdate,
                                  User,
                                  L_vdate,
                                  User);

   if LP_no_loc then
      insert into cost_susp_sup_detail(cost_change,
                                       supplier,
                                       origin_country_id,
                                       item,
                                       unit_cost,
                                       cost_change_type,
                                       cost_change_value,
                                       recalc_ord_ind,
                                       default_bracket_ind)
                                select L_next_cc_num,
                                       I_message.supplier,
                                       I_message.origin_country_id,
                                       value(xitem),
                                       I_message.unit_cost,
                                       'F',
                                       I_message.unit_cost,
                                       I_message.recalc_ord_ind,
                                       'N'
                                  from TABLE(cast(I_costchg_rec.items_no_locs as ITEM_TBL)) xitem;
   else                                 
      insert into cost_susp_sup_detail_loc(cost_change,
                                       supplier,
                                       origin_country_id,
                                       item,
                                       loc,
                                       loc_type,
                                       unit_cost,
                                       cost_change_type,
                                       cost_change_value,                                       
                                       recalc_ord_ind,
                                       default_bracket_ind)
                                select L_next_cc_num,
                                       I_message.supplier,
                                       I_message.origin_country_id,
                                       api.item,
                                       api.loc,
                                       iscl.loc_type,
                                       I_message.unit_cost,
                                       'F',
                                       I_message.unit_cost,
                                       I_message.recalc_ord_ind,
                                       'N'
                                  from api_iscl api,
                                       item_supp_country_loc iscl
                                 where iscl.item = api.item
                                   and iscl.loc = api.loc
                                   and iscl.supplier = I_message.supplier
                                   and iscl.origin_country_id = I_message.origin_country_id
                                   and api.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no;
   end if;
   

   O_costchg_num := L_next_cc_num;
   L_cost_changes_tbl := new OBJ_CC_COST_EVENT_TBL();
   L_cost_changes_rec := new OBJ_CC_COST_EVENT_REC(L_next_cc_num,
                                                   'N'); --cost change sourced from temp tables indicator.
   L_cost_changes_tbl.EXTEND;
   L_cost_changes_tbl(L_cost_changes_tbl.COUNT) := L_cost_changes_rec;
   ---
   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;
   ---
   if L_event_run_type = 'SYNC' then
   
      if FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT(O_error_message,
                                                     L_cost_event_process_id,
                                                     FUTURE_COST_EVENT_SQL.ADD_EVENT,
                                                     L_cost_changes_tbl,
                                                     'Y',
                                                     User,
                                                     FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE) = FALSE then
         return FALSE;
      end if;     
      
   else   
      
      if FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT(O_error_message,
                                                     L_cost_event_process_id,
                                                     FUTURE_COST_EVENT_SQL.ADD_EVENT,
                                                     L_cost_changes_tbl,
                                                     'Y',
                                                     User) = FALSE then
         return FALSE;
      end if;
   end if;   
 
  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
      
END INSERT_COST_CHANGE;
-------------------------------------------------------------------------------------------------------   
END RMSSUB_XCOSTCHG_SQL;
/
