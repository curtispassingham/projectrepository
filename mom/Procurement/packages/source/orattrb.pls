CREATE OR REPLACE PACKAGE BODY ORDER_ATTRIB_SQL AS
-------------------------------------------------------------------
FUNCTION FREIGHT_TERMS_DESC(O_error_message IN OUT VARCHAR2,
                            I_code          IN     VARCHAR2,
                            O_desc          IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.FREIGHT_TERMS_DESC';

   cursor C_ATTRIB is
      select term_desc
        from v_freight_terms_tl
       where freight_terms = I_code;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ATTRIB','V_FREIGHT_TERMS_TL','FREIGHT_TERMS: '||I_code);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH','C_ATTRIB','V_FREIGHT_TERMS_TL','FREIGHT_TERMS: '||I_code);
   fetch C_ATTRIB into O_desc;
   ---
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_FREIGHT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','V_FREIGHT_TERMS_TL','FREIGHT_TERMS: '||I_code);
      close C_ATTRIB;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','V_FREIGHT_TERMS_TL','FREIGHT_TERMS: '||I_code);
   close C_ATTRIB;
   ---
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END FREIGHT_TERMS_DESC;
-------------------------------------------------------------------
FUNCTION PAY_TERMS_DESC(O_error_message IN OUT VARCHAR2,
                        I_code          IN     VARCHAR2,
                        O_terms_code    IN OUT VARCHAR2,
                        O_desc          IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_function VARCHAR2(50) := 'ORDER_ATTRIB_SQL.PAY_TERMS_DESC';

   cursor C_ATTRIB is
      select terms_code,
             terms_desc
        from v_terms_head_tl
       where terms = I_code;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
   fetch C_ATTRIB into O_terms_code, O_desc;
   ---
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_TERMS',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
      close C_ATTRIB;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
   close C_ATTRIB;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function, 
                                             to_char(SQLCODE));
   return FALSE;
END PAY_TERMS_DESC;
--------------------------------------------------------------------------
FUNCTION GET_PO_TYPE_DESC(O_error_message IN OUT VARCHAR2,
                          I_po_type       IN     VARCHAR2,
                          O_po_type_desc  IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'ORDER_ATTRIB_SQL.GET_PO_TYPE_DESC';

   cursor C_GET_DESC is
      select po_type_desc
        from v_po_type_tl
       where po_type = I_po_type;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_GET_DESC','V_PO_TYPE_TL','PO type: '||I_po_type);
   open C_GET_DESC;
   SQL_LIB.SET_MARK('FETCH','C_GET_DESC','V_PO_TYPE_TL','PO type: '||I_po_type);
   fetch C_GET_DESC into O_PO_TYPE_DESC;
   if C_GET_DESC%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_PO_TYPE',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_GET_DESC','po_type',
                       'PO type: '||I_po_type);
      close C_GET_DESC;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_DESC','V_PO_TYPE_TL','PO type: '||I_po_type);
   close C_GET_DESC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;  
END GET_PO_TYPE_DESC;
-------------------------------------------------------------------------
FUNCTION DELIVERY_MONTH(O_error_message  IN OUT VARCHAR2,
                        O_delivery_month IN OUT NUMBER,
                        I_not_after_date IN OUT ORDHEAD.NOT_AFTER_DATE%TYPE)
   RETURN BOOLEAN IS

   L_function            VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.DELIVERY_MONTH';
   L_not_after_date      VARCHAR2(8);
   return_code           VARCHAR2(5);

BEGIN
   L_not_after_date := to_char(I_not_after_date, 'MMDDYYYY');
   ---
   MID_MONTH(L_not_after_date, 
             return_code, 
             O_error_message); 
   ---
   if return_code = 'FALSE' then
      return FALSE;
   end if;
   ---
   O_delivery_month := to_number(to_char(to_date(L_not_after_date,'DDMMYYYY'),'YYYYMM'));
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function, 
                                             to_char(SQLCODE));
      RETURN FALSE;
END DELIVERY_MONTH;
-------------------------------------------------------------------------
FUNCTION GET_PACK_TOTAL(O_error_message     IN OUT VARCHAR2,
                        O_pack_total        IN OUT ITEM_MASTER.ORIGINAL_RETAIL%TYPE,
                        I_pack_no           IN     PACKITEM.PACK_NO%TYPE,
                        I_uom               IN     UOM_CLASS.UOM%TYPE,
                        I_quantity          IN     PACKITEM.PACK_QTY%TYPE,
                        I_supplier          IN     SUPS.SUPPLIER%TYPE,
                        I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE) 
RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'ORDER_ATTRIB_SQL.GET_PACK_TOTAL';
   L_subtotal    ITEM_MASTER.ORIGINAL_RETAIL%TYPE;
   L_qty_ordered PACKITEM.PACK_QTY%TYPE;
   L_item        PACKITEM.ITEM%TYPE;
   L_pack_total  ITEM_MASTER.ORIGINAL_RETAIL%TYPE := 0;

   cursor C_PACK_ITEM_QTY is
      select item,
             (qty * I_quantity) qty 
        from v_packsku_qty  
       where pack_no = I_pack_no; 

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_PACK_ITEM_QTY',NULL,NULL);
   for CURSOR1 in C_PACK_ITEM_QTY loop
      L_item        := CURSOR1.item;
      L_qty_ordered := CURSOR1.qty;

      if UOM_SQL.CONVERT(O_error_message,
                         L_subtotal,
                         I_uom,
                         L_qty_ordered,
                         NULL,        --- order in standard UOM fetched in function
                         L_item,
                         I_supplier,
                         I_origin_country_id) = FALSE then
         return FALSE;
      end if;

      L_pack_total := L_pack_total + L_subtotal;
   end loop;

   O_pack_total := L_pack_total;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PACK_TOTAL;
-------------------------------------------------------------------------
FUNCTION GET_QUANTITY(O_error_message     IN OUT VARCHAR2,
                      O_total             IN OUT ITEM_MASTER.ORIGINAL_RETAIL%TYPE,
                      I_uom               IN     UOM_CLASS.UOM%TYPE,
                      I_item              IN     ITEM_MASTER.ITEM%TYPE,
                      I_quantity          IN     PACKITEM.PACK_QTY%TYPE,
                      I_supplier          IN     SUPS.SUPPLIER%TYPE,
                      I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE) 
RETURN BOOLEAN IS 
   L_pack_type     ITEM_MASTER.PACK_TYPE%TYPE;
   L_program       VARCHAR2(50) := 'ORDER_ATTRIB_SQL.GET_QUANTITY';
   L_pack_ind      ITEM_MASTER.PACK_IND%TYPE;
   L_sellable_ind  ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind ITEM_MASTER.ORDERABLE_IND%TYPE;

BEGIN
   if ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                    L_pack_ind,
                                    L_sellable_ind,
                                    L_orderable_ind,
                                    L_pack_type,
                                    I_item) = FALSE then
      return FALSE;
   end if;
   ---
   if L_pack_ind = 'Y' and L_pack_type = 'B' then
      if ORDER_ATTRIB_SQL.GET_PACK_TOTAL(O_error_message,
                                         O_total,
                                         I_item,
                                         I_uom,
                                         I_quantity,
                                         I_supplier,
                                         I_origin_country_id) = FALSE then
         return FALSE;
      end if;
   else
      if UOM_SQL.CONVERT(O_error_message,
                         O_total,
                         I_uom,
                         I_quantity,
                         NULL,      --- order in standard UOM fetched in convert
                         I_item,
                         I_supplier,
                         I_origin_country_id) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_QUANTITY;
-------------------------------------------------------------------------
FUNCTION GET_TOTAL_UOM(I_order_no       IN     ORDLOC.ORDER_NO%TYPE,
                       I_item           IN     ORDLOC.ITEM%TYPE,
                       I_location       IN     ORDLOC.LOCATION%TYPE,
                       I_uom            IN     UOM_CLASS.UOM%TYPE,
                       I_supplier       IN     SUPS.SUPPLIER%TYPE,
                       I_order_qty_type IN     VARCHAR2,
                       O_quantity       IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       O_qty_prescaled  IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                       O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_item                         ORDLOC.ITEM%TYPE;
   L_qty_ordered                  ORDLOC.QTY_ORDERED%TYPE;
   L_qty_prescaled                ORDLOC.QTY_PRESCALED%TYPE;
   L_supplier                     SUPS.SUPPLIER%TYPE;
   L_origin_country_id            COUNTRY.COUNTRY_ID%TYPE;
   L_qty_subtotal                 NUMBER       := 0;
   L_qty_prescaled_subtotal       NUMBER       := 0;
   L_program                      VARCHAR2(50) := 'ORDER_ATTRIB_SQL.GET_TOTAL_UOM';

   cursor C_GET_SUPPLIER is
      select supplier
        from ordhead
       where order_no = I_order_no;

   cursor C_ORDER is
      select ol.item,
             os.origin_country_id,
             NVL(SUM(ol.qty_ordered),0) qty,
             NVL(SUM(ol.qty_prescaled),0) qty_prescaled
        from ordloc ol,
             ordsku os
       where ol.order_no = I_order_no
         and os.order_no = ol.order_no
         and os.item     = ol.item
       group by ol.item,
                os.origin_country_id;

   cursor C_ORDER_LOC is
      select ol.item,             
             os.origin_country_id,
             NVL(ol.qty_ordered,0) qty,
             NVL(ol.qty_prescaled,0) qty_prescaled
        from ordloc ol,
             ordsku os,
             wh wh
       where ol.order_no = I_order_no
         and (wh.physical_wh = I_location
          or ol.location = I_location)
         and os.order_no = ol.order_no
         and os.item     = ol.item
         and ol.location = wh.wh(+);

   cursor C_ORDER_SKU is
      select ol.item,
             os.origin_country_id,
             NVL(SUM(ol.qty_ordered),0) qty,
             NVL(SUM(ol.qty_prescaled),0) qty_prescaled
        from ordloc ol,
             ordsku os,
             wh wh
       where ol.order_no = I_order_no
         and ol.item     = I_item
         and (wh.physical_wh = nvl(I_location, wh.physical_wh)
          or ol.location = nvl(I_location, ol.location))
         and os.order_no = ol.order_no
         and os.item     = ol.item
         and ol.location = wh.wh(+)
    group by ol.item, 
             os.origin_country_id 
       UNION ALL
      select ol.item,
             os.origin_country_id,
             NVL(SUM(ol.qty_ordered),0) qty,
             NVL(SUM(ol.qty_prescaled),0) qty_prescaled
        from ordloc ol,
             ordsku os,
             item_master im,
             wh wh
       where ol.order_no = I_order_no
         and (wh.physical_wh = nvl(I_location, wh.physical_wh)
          or ol.location = nvl(I_location, ol.location))
         and os.order_no = ol.order_no
         and os.item     = ol.item
         and ol.item     = im.item
         and (im.item_parent = I_item
          or im.item_grandparent = I_item)
         and ol.location = wh.wh(+)
    group by ol.item, 
             os.origin_country_id;

BEGIN
   O_quantity := 0;
   O_qty_prescaled := 0;
   ---
   if I_supplier is NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_SUPPLIER','ORDHEAD',
                       'Order_no.: ' || to_char(I_order_no));
      open C_GET_SUPPLIER;
      SQL_LIB.SET_MARK('FETCH','C_GET_SUPPLIER','ORDHEAD',
                       'Order_no.: ' || to_char(I_order_no));
      fetch C_GET_SUPPLIER into L_supplier;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUPPLIER','ORDHEAD',
                       'Order_no.: ' || to_char(I_order_no));
      close C_GET_SUPPLIER;
   else
      L_supplier := I_supplier;
   end if;
   ---
   if I_item is NULL then 
      if I_location is NULL then
         SQL_LIB.SET_MARK('OPEN','C_ORDER','ordloc, ordsku','Order_no.: ' || to_char(I_order_no));
         for C_ORDER_REC in C_ORDER loop
            L_item              := C_ORDER_REC.item;
            L_origin_country_id := C_ORDER_REC.origin_country_id;
            L_qty_ordered       := C_ORDER_REC.qty;
            L_qty_prescaled     := C_ORDER_REC.qty_prescaled;
            ---
            if I_order_qty_type in ('B', 'O') then
               if ORDER_ATTRIB_SQL.GET_QUANTITY(O_error_message,
                                                L_qty_subtotal,
                                                I_uom,
                                                L_item,
                                                L_qty_ordered,
                                                L_supplier,
                                                L_origin_country_id) = FALSE then
                  return FALSE;
               end if;
               ---
               -- add item/pack total to order total 
               ---
               O_quantity := O_quantity + L_qty_subtotal;
            end if;
            ---
            if I_order_qty_type in ('B', 'P') then
               if ORDER_ATTRIB_SQL.GET_QUANTITY(O_error_message,
                                                L_qty_prescaled_subtotal,
                                                I_uom,
                                                L_item,
                                                L_qty_prescaled,
                                                L_supplier,
                                                L_origin_country_id) = FALSE then
                  return FALSE;
               end if;
               ---
               -- add item/pack total to prescaled order total 
               ---
               O_qty_prescaled := O_qty_prescaled + L_qty_prescaled_subtotal;
            end if;
         end loop;
      elsif I_location is not NULL then 
         SQL_LIB.SET_MARK('OPEN','C_ORDER_LOC','ordloc, ordsku',
                          'Order_no.: ' || to_char(I_order_no));
         for C_ORDER_REC in C_ORDER_LOC loop
            L_item              := C_ORDER_REC.item;
            L_origin_country_id := C_ORDER_REC.origin_country_id;
            L_qty_ordered       := C_ORDER_REC.qty;
            L_qty_prescaled     := C_ORDER_REC.qty_prescaled;
            if I_order_qty_type in ('B', 'O') then
               if ORDER_ATTRIB_SQL.GET_QUANTITY(O_error_message,
                                                L_qty_subtotal,
                                                I_uom,
                                                L_item,
                                                L_qty_ordered,
                                                L_supplier,
                                                L_origin_country_id) = FALSE then
                  return FALSE;
               end if;

               --- add item/pack total to order total 
               O_quantity := O_quantity + L_qty_subtotal;
            end if;
            if I_order_qty_type in ('B', 'P') then
               if ORDER_ATTRIB_SQL.GET_QUANTITY(O_error_message,
                                                L_qty_prescaled_subtotal,
                                                I_uom,
                                                L_item,
                                                L_qty_prescaled,
                                                L_supplier,
                                                L_origin_country_id) = FALSE then
                  return FALSE;
               end if;

               --- add item/pack total to prescaled order total 
               O_qty_prescaled := O_qty_prescaled + L_qty_prescaled_subtotal;
            end if;
         end loop;
      end if;  -- if I_item is null
   elsif I_item is not NULL then
      -- one cursor handles NULL and NOT NULL I_location.
      SQL_LIB.SET_MARK('OPEN', 'C_ORDER_SKU', 'ORDSKU, ORDLOC', 'Order no: '||to_char(I_order_no));
      open C_ORDER_SKU;

      SQL_LIB.SET_MARK('FETCH', 'C_ORDER_SKU', 'ORDSKU, ORDLOC', 'Order no: '||to_char(I_order_no));
      fetch C_ORDER_SKU into L_item,
                             L_origin_country_id,
                             L_qty_ordered,
                             L_qty_prescaled;
      if C_ORDER_SKU%FOUND then
         ---
         if I_order_qty_type in ('B', 'O') then
            if ORDER_ATTRIB_SQL.GET_QUANTITY(O_error_message,
                                             O_quantity,
                                             I_uom,
                                             L_item,
                                             L_qty_ordered,
                                             L_supplier,
                                             L_origin_country_id) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
         if I_order_qty_type in ('B', 'P') then
            if ORDER_ATTRIB_SQL.GET_QUANTITY(O_error_message,
                                             O_qty_prescaled,
                                             I_uom,
                                             L_item,
                                             L_qty_prescaled,
                                             L_supplier,
                                             L_origin_country_id) = FALSE then
               return FALSE;
            end if;
         end if;
         ---
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDER_SKU', 'ORDSKU, ORDLOC', 'Order no: '||to_char(I_order_no));
      close C_ORDER_SKU;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_UOM;
-------------------------------------------------------------------------
FUNCTION GET_COUNTRY_OF_ORIGIN (O_error_message     IN OUT VARCHAR2,
                                O_country_of_origin IN OUT ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(64) := 'ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN';
   L_count       NUMBER(3);
   ---
   cursor C_COUNT_ORDSKU is
      select count(distinct origin_country_id)
        from ordsku
       where order_no = I_order_no;
   ---
   cursor C_ORDSKU_COUNTRY is
      select distinct origin_country_id
        from ordsku
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_COUNT_ORDSKU', 'ORDSKU', 'Order no: '||to_char(I_order_no));
   open C_COUNT_ORDSKU;
   SQL_LIB.SET_MARK('FETCH', 'C_COUNT_ORDSKU', 'ORDSKU', 'Order no: '||to_char(I_order_no));
   fetch C_COUNT_ORDSKU into L_count;
   SQL_LIB.SET_MARK('CLOSE', 'C_COUNT_ORDSKU', 'ORDSKU', 'Order no: '||to_char(I_order_no));
   close C_COUNT_ORDSKU;
   ---
   if L_count = 1 then
      SQL_LIB.SET_MARK('OPEN', 'C_ORDSKU_COUNTRY', 'ORDSKU', 'Order no: '||to_char(I_order_no));
      open C_ORDSKU_COUNTRY;
      SQL_LIB.SET_MARK('FETCH', 'C_ORDSKU_COUNTRY', 'ORDSKU', 'Order no: '||to_char(I_order_no));
      fetch C_ORDSKU_COUNTRY into O_country_of_origin;
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDSKU_COUNTRY', 'ORDSKU', 'Order no: '||to_char(I_order_no));
      close C_ORDSKU_COUNTRY;
   elsif L_count > 1 then
      O_country_of_origin := '99'; -- multiple countries 
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_COUNTRY_OF_ORIGIN;
-------------------------------------------------------------------------
FUNCTION GET_WRITTEN_DATE (O_error_message    IN OUT VARCHAR2,
                           O_written_date     IN OUT ORDHEAD.WRITTEN_DATE%TYPE,
                           I_order_no         IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program       VARCHAR2(264) := 'ORDER_ATTRIB_SQL.GET_WRITTEN_DATE';
   ---
   cursor C_ORDHEAD is
      select written_date
        from ordhead
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_written_date;
   if C_ORDHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_ORDHEAD;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_WRITTEN_DATE;
-------------------------------------------------------------------------
FUNCTION GET_CURRENCY_RATE (O_error_message   IN OUT VARCHAR2,
                            O_currency_code   IN OUT ORDHEAD.CURRENCY_CODE%TYPE,
                            O_exchange_rate   IN OUT ORDHEAD.EXCHANGE_RATE%TYPE,
                            I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program  VARCHAR2(264) := 'ORDER_ATTRIB_SQL.GET_CURRENCY_RATE';
   ---
   cursor C_ORDHEAD is
      select currency_code,
             exchange_rate 
        from ordhead
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_currency_code, 
                        O_exchange_rate;
   if C_ORDHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_ORDHEAD;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_CURRENCY_RATE;
-------------------------------------------------------------------------
FUNCTION GET_DIS_PORT(O_error_message  IN OUT VARCHAR2,
                      O_discharge_port IN OUT ORDHEAD.DISCHARGE_PORT%TYPE,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(264) := 'ORDER_ATTRIB_SQL.GET_DIS_PORT';
   ---
   cursor C_ORDHEAD is
      select discharge_port
        from ordhead
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_discharge_port;
   if C_ORDHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_ORDHEAD;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DIS_PORT;
-------------------------------------------------------------------------
FUNCTION GET_LAD_PORT(O_error_message  IN OUT VARCHAR2,
                      O_lading_port    IN OUT ORDHEAD.LADING_PORT%TYPE,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(264) := 'ORDER_ATTRIB_SQL.GET_LAD_PORT';
   ---
   cursor C_ORDHEAD is
      select lading_port
        from ordhead
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_lading_port;
   if C_ORDHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_ORDHEAD;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_LAD_PORT;
-------------------------------------------------------------------------
FUNCTION ITEMS_EXIST(O_error_message  IN OUT VARCHAR2,
                     O_exists         IN OUT BOOLEAN,
                     I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(264)  := 'ORDER_ATTRIB_SQL.ITEMS_EXIST';
   L_dummy      VARCHAR2(1);
   ---
   cursor C_ORDER_ITEMS is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
      union all
      select 'x'
        from ordsku
       where order_no = I_order_no;
BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ORDER_ITEMS', 'ORDLOC_WKSHT, ORDSKU', 'Order no: '||to_char(I_order_no));
   open C_ORDER_ITEMS;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDER_ITEMS', 'ORDLOC_WKSHT, ORDSKU', 'Order no: '||to_char(I_order_no));
   fetch C_ORDER_ITEMS into L_dummy;
   if C_ORDER_ITEMS%FOUND then
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDER_ITEMS', 'ORDLOC_WKSHT, ORDSKU', 'Order no: '||to_char(I_order_no));
   close C_ORDER_ITEMS;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ITEMS_EXIST;
-------------------------------------------------------------------------
FUNCTION GET_SHIP_DATES(O_error_message       IN OUT VARCHAR2,
                        O_earliest_ship_date  IN OUT ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                        O_latest_ship_date    IN OUT ORDHEAD.LATEST_SHIP_DATE%TYPE,
                        I_order_no            IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(264) := 'ORDER_ATTRIB_SQL.GET_SHIP_DATES';
   ---
   cursor C_ORDHEAD is
      select earliest_ship_date,
             latest_ship_date
        from ordhead
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_earliest_ship_date, 
                        O_latest_ship_date;
   if C_ORDHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_ORDHEAD;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_SHIP_DATES;
-------------------------------------------------------------------------
FUNCTION GET_RECEIPT_DATES (O_error_message   IN OUT  VARCHAR2,
                            O_not_before_date IN OUT  ORDHEAD.NOT_BEFORE_DATE%TYPE,
                            O_not_after_date  IN OUT  ORDHEAD.NOT_AFTER_DATE%TYPE,
                            I_order_no        IN      ORDHEAD.ORDER_NO%TYPE)
      RETURN BOOLEAN IS

   L_program     VARCHAR2(40) := 'ORDER_ATTRIB_SQL.GET_RECEIPT_DATES';
   cursor C_RECEIPT_DATES is
      select not_before_date, 
             not_after_date
        from ordhead
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_RECEIPT_DATES', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_RECEIPT_DATES;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_RECEIPT_DATES', 'ORDHEAD', 'Order no: ' || to_char(I_order_no));
   fetch C_RECEIPT_DATES into O_not_before_date,
                              O_not_after_date;
   if C_RECEIPT_DATES%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_RECEIPT_DATES', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_RECEIPT_DATES;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_RECEIPT_DATES', 'ORDHEAD', 'Order no: '|| to_char(I_order_no));
   close C_RECEIPT_DATES;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_RECEIPT_DATES;
-------------------------------------------------------------------------
FUNCTION GET_IMPORT_IND(O_error_message     IN OUT VARCHAR2,
                        O_import_order_ind  IN OUT ORDHEAD.IMPORT_ORDER_IND%TYPE,
                        I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(264)  := 'ORDER_ATTRIB_SQL.GET_IMPORT_IND';
   ---
   cursor C_ORDHEAD is
      select import_order_ind
        from ordhead
       where order_no = I_order_no;
BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_import_order_ind;
   if C_ORDHEAD%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('NO_IMPORT_IND_FOUND',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_ORDHEAD;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_IMPORT_IND;
-------------------------------------------------------------------------
FUNCTION GET_IMPORT_COUNTRY(O_error_message     IN OUT VARCHAR2,
                            O_import_country_id IN OUT ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                            I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(264)  := 'ORDER_ATTRIB_SQL.GET_IMPORT_COUNTRY';

   cursor C_ORDHEAD is
      select import_country_id
        from ordhead
       where order_no = I_order_no;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDHEAD',
                    'ORDHEAD','Order no:
                    '||to_char(I_order_no));
   open C_ORDHEAD;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDHEAD',
                    'ORDHEAD',
                    'Order no:
                    '||to_char(I_order_no));
   fetch C_ORDHEAD into O_import_country_id;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDHEAD',
                    'ORDHEAD',
                    'Order no:
                    '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_IMPORT_COUNTRY;
-------------------------------------------------------------------------
FUNCTION GET_PREPACK_IND(O_error_message IN OUT VARCHAR2,
                         O_exists        IN OUT BOOLEAN,
                         I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program VARCHAR2(64) := 'ORDER_ATTRIB_SQL.GET_PREPACK_IND';
   L_exists  VARCHAR2(1);

   cursor C_FASH_IND is
      select 'Y'
        from ordsku o,
             packitem p,
             pack_tmpl_head pth
       where o.order_no = I_order_no
         and o.item = p.pack_no
         and p.pack_tmpl_id = pth.pack_tmpl_id
         and pth.fash_prepack_ind = 'Y'
   UNION ALL
      select 'Y'
        from ordloc_wksht o,
             packitem p,
             pack_tmpl_head pth
       where o.order_no = I_order_no
         and o.item = p.pack_no
         and p.pack_tmpl_id = pth.pack_tmpl_id
         and pth.fash_prepack_ind = 'Y';

BEGIN
   SQL_LIB.SET_MARK('OPEN',
                    'C_fash_ind',
                    'ordsku, packitem, pack_tmpl_head',
                    'ORDER_NO: '||to_char(I_order_no));

   open C_FASH_IND;

   SQL_LIB.SET_MARK('FETCH',
                    'C_fash_ind',
                    'ordsku, packitem, pack_tmpl_head',
                    'ORDER_NO: '||to_char(I_order_no));

   fetch C_FASH_IND into L_exists;

   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_fash_ind',
                    'ordsku, packitem, pack_tmpl_head',
                    'ORDER_NO: '||to_char(I_order_no));

   close C_FASH_IND;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_PREPACK_IND;
----------------------------------------------------------------------
FUNCTION GET_TOTAL_UNITS(O_error_message       IN OUT VARCHAR2,
                         O_total_alloc_ord_qty IN OUT ORDLOC.QTY_ORDERED%TYPE,
                         O_total_prescale_qty  IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                         I_order_no            IN     ORDHEAD.ORDER_NO%TYPE,
                         I_alloc_no            IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                         I_item                IN     ITEM_MASTER.ITEM%TYPE,
                         I_location            IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_ALLOC_QTYS is
      select ad.qty_allocated,
             ad.qty_prescaled
        from alloc_header ah,
             alloc_detail ad
       where ah.order_no = I_order_no
         and ah.alloc_no = I_alloc_no
         and ah.item     = I_item
         and ah.alloc_no = ad.alloc_no
         and ad.to_loc   = I_location;

   cursor C_ORDER_LOC is
      select nvl(SUM(qty_ordered),0),
             nvl(SUM(qty_prescaled),0)
        from ordloc
       where order_no = I_order_no
         and location = NVL(I_location, location);

   cursor C_ORDER_SKU_LOC is
      select NVL(SUM(ol.qty_ordered),0),
             NVL(SUM(ol.qty_prescaled),0)
        from ordloc ol, 
             item_master im
       where ol.order_no             = I_order_no
         and ol.item                 = im.item
         and ol.location             = NVL(I_location, ol.location)
         and (im.item                = I_item
              or im.item_parent      = I_item
              or im.item_grandparent = I_item);

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                              'I_order_no',
                                              'NULL',
                                              'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_alloc_no is NOT NULL then
      if I_item is null then
         O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                                'I_item',
                                                'NULL',
                                                'NOT NULL');
         return FALSE;
      end if;
      if I_location is null then
         O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                                'I_location',
                                                'NULL',
                                                'NOT NULL');
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_ALLOC_QTYS',
                       'alloc_header, alloc_detail',
                       'Order No:' || to_char(I_order_no)||
                       ', Alloc_no: '||to_char(I_alloc_no));
      open C_GET_ALLOC_QTYS;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_ALLOC_QTYS',
                       'alloc_header, alloc_detail',
                       'Order No:' || to_char(I_order_no)||
                       ', Alloc_no: '||to_char(I_alloc_no));
      fetch C_GET_ALLOC_QTYS into O_total_alloc_ord_qty,
                                  O_total_prescale_qty;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_ALLOC_QTYS',
                       'alloc_header, alloc_detail',
                       'Order No:' || to_char(I_order_no)||
                       ', Alloc_no: '||to_char(I_alloc_no));
      close C_GET_ALLOC_QTYS;
   else  -- Alloc_no is NULL
      if I_item is NULL then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_ORDER_LOC','ORDLOC',
                          'ORDER NO:' || to_char(I_order_no));
         ---
         open C_ORDER_LOC;
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_ORDER_LOC', 'ORDLOC',
                             'ORDER NO:' || to_char(I_order_no));
         ---
         fetch C_ORDER_LOC into O_total_alloc_ord_qty,
                                O_total_prescale_qty;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_ORDER_LOC', 'ORDLOC',
                             'ORDER NO:' || to_char(I_order_no));
         ---
         close C_ORDER_LOC;
         ---
      elsif I_item is NOT NULL then
         ---
         SQL_LIB.SET_MARK('OPEN', 'C_ORDER_SKU_LOC', 'ORDLOC, ITEM_MASTER',
                          'ORDER NO:' || to_char(I_order_no));
         ---
         open C_ORDER_SKU_LOC;
         ---
         SQL_LIB.SET_MARK('FETCH', 'C_ORDER_SKU_LOC', 'ORDLOC, ITEM_MASTER', 
                          'ORDER NO:' || to_char(I_order_no));
         ---
         fetch C_ORDER_SKU_LOC into O_total_alloc_ord_qty,
                                    O_total_prescale_qty;
         ---
         SQL_LIB.SET_MARK('CLOSE', 'C_ORDER_SKU_LOC', 'ORDLOC, ITEM_MASTER', 
                          'ORDER NO:' || to_char(I_order_no));
         ---
         close C_ORDER_SKU_LOC;
         ---
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.GET_TOTAL_UNITS',
                                             to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_UNITS;
----------------------------------------------------------------------
FUNCTION ORDER_ITEM_EXISTS (O_error_message IN OUT VARCHAR2,
                            O_exists        IN OUT BOOLEAN,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_exists    VARCHAR2(1) := 'N';

   cursor C_EXISTS is
      select 'Y'
        from ordsku os, item_master im
       where os.item              = im.item
         and (im.item             = I_item
          or  im.item_parent      = I_item
          or  im.item_grandparent = I_item);

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_EXISTS', NULL, NULL);
   open C_EXISTS;
   SQL_LIB.SET_MARK('FETCH', 'C_EXISTS', NULL, NULL);
   fetch C_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE', 'C_EXISTS', NULL, NULL);
   close C_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   else
      O_exists := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.ORDER_ITEM_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END ORDER_ITEM_EXISTS;
----------------------------------------------------------------------
FUNCTION GET_STATUS(O_error_message  IN OUT VARCHAR2,
                    O_exists         IN OUT BOOLEAN,
                    O_status         IN OUT ORDHEAD.STATUS%TYPE,
                    I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists   VARCHAR2(1);

   cursor C_ORDER is
      select status 
        from ordhead
       where order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' STATUS: '||O_status);
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' STATUS: '||O_status);
   fetch C_ORDER into O_status;
   if C_ORDER%NOTFOUND then
      O_exists := FALSE;
   else 
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' STATUS: '||O_status);
   close C_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             'ORDER_ATTRIB_SQL.GET_STATUS',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_STATUS;
-------------------------------------------------------------------
FUNCTION GET_SUPPLIER(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      O_supplier       IN OUT ORDHEAD.SUPPLIER%TYPE,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists   VARCHAR2(1);

   cursor C_ORDER is
      select supplier 
        from ordhead
       where order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' SUPPLIER: '||to_char(O_supplier));
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' SUPPLIER: '||to_char(O_supplier));
   fetch C_ORDER into O_supplier;
   if C_ORDER%NOTFOUND then
      O_exists := FALSE;
   else 
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' SUPPLIER: '||to_char(O_supplier));
   close C_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             'ORDER_ATTRIB_SQL.GET_SUPPLIER',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_SUPPLIER;
-------------------------------------------------------------------
FUNCTION GET_DEPT(O_error_message  IN OUT VARCHAR2,
                  O_exists         IN OUT BOOLEAN,
                  O_dept           IN OUT ORDHEAD.DEPT%TYPE,
                  I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exists    VARCHAR2(1);

   cursor C_ORDER is
      select dept 
        from ordhead
       where order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' DEPT: '||to_char(O_dept));
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' DEPT: '||to_char(O_dept));
   fetch C_ORDER into O_dept;
   if C_ORDER%NOTFOUND then
      O_exists := FALSE;
   else 
      O_exists := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_ORDER','ORDHEAD','ORDER NO: '||to_char(I_order_no)||' DEPT: '||to_char(O_dept));
   close C_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             'ORDER_ATTRIB_SQL.GET_DEPT',
                                             to_char(SQLCODE));
      RETURN FALSE;
END GET_DEPT;
----------------------------------------------------------------------
FUNCTION GET_BANK_LC_ID(O_error_message     IN OUT  VARCHAR2,
                        O_bank_lc_id        IN OUT  LC_HEAD.BANK_LC_ID%TYPE,
                        I_order_no          IN      ORDHEAD.ORDER_NO%TYPE )
RETURN BOOLEAN IS

   L_program    VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_BANK_LC_ID';
   ---
   cursor C_GET_LC_DETAIL is
      select lch.bank_lc_id
        from lc_head lch,
             lc_detail lcd
       where lch.lc_ref_id = lcd.lc_ref_id
         and lcd.order_no  = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_LC_DETAIL', 'LC_HEAD', 'Order no: '||to_char(I_order_no));
   open  C_GET_LC_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_LC_DETAIL', 'LC_HEAD', 'Order no: '||to_char(I_order_no));
   fetch C_GET_LC_DETAIL into O_bank_lc_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_LC_DETAIL', 'LC_HEAD', 'Order no: '||to_char(I_order_no));
   close C_GET_LC_DETAIL;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BANK_LC_ID;
-------------------------------------------------------------------------
FUNCTION GET_PAYMENT_METHOD(O_error_message     IN OUT VARCHAR2,
                            O_payment_method    IN OUT ORDHEAD.PAYMENT_METHOD%TYPE,
                            I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program    VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_PAYMENT_METHOD';
   ---
   cursor C_GET_PAYMENT_METHOD is
      select payment_method
        from ordhead
       where order_no  = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_PAYMENT_METHOD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   open C_GET_PAYMENT_METHOD;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_PAYMENT_METHOD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   fetch C_GET_PAYMENT_METHOD into O_payment_method;
   if O_payment_method is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('NO_PAYMENT_METHOD',to_char(I_order_no),NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_PAYMENT_METHOD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
      close C_GET_PAYMENT_METHOD;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_PAYMENT_METHOD', 'ORDHEAD', 'Order no: '||to_char(I_order_no));
   close C_GET_PAYMENT_METHOD;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_PAYMENT_METHOD;
-------------------------------------------------------------------------
FUNCTION LC_INFO_EXISTS(O_error_message    IN OUT  VARCHAR2,
                        O_exists           IN OUT  BOOLEAN,
                        I_order_no         IN      ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50) :=  'ORDER_ATTRIB_SQL.LC_INFO_EXISTS';
   L_exists    VARCHAR2(1);
   ---
   cursor C_ORDER_LC is
      select 'x' 
        from ordlc
       where order_no = I_order_no;

BEGIN
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDER_LC','ORDLC','Order No: '||to_char(I_order_no));
   open C_ORDER_LC;
   SQL_LIB.SET_MARK('FETCH','C_ORDER_LC','ORDLC','Order No: '||to_char(I_order_no));
   fetch C_ORDER_LC into L_exists;
   ---
   if C_ORDER_LC%NOTFOUND then
      O_exists := FALSE;
   else 
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDER_LC','ORDLC','Order No: '||to_char(I_order_no));
   close C_ORDER_LC;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      RETURN FALSE;
END LC_INFO_EXISTS;
-------------------------------------------------------------------
FUNCTION VALID_ORDLC_LC_REF_ID(O_error_message    IN OUT  VARCHAR2,
                               O_valid            IN OUT  BOOLEAN,
                               I_lc_ref_id        IN      ORDLC.LC_REF_ID%TYPE,
                               I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                               I_beneficiary      IN      ORDLC.BENEFICIARY%TYPE,
                               I_applicant        IN      ORDLC.APPLICANT%TYPE)
   RETURN BOOLEAN IS

   L_function     VARCHAR2(50)        :=  'ORDER_ATTRIB_SQL.VALID_ORDLC_LC_REF_ID';      
   L_lc_ref_id    ORDLC.LC_REF_ID%TYPE;
   ---
   cursor C_LC_REF_ID is
      select l.lc_ref_id
         from lc_head l,
              ordhead o
       where o.order_no            = I_order_no
         and l.lc_ref_id           = I_lc_ref_id
         and l.lc_type            != 'O'
         and l.status             != 'L'
         and l.beneficiary         = I_beneficiary
         and l.applicant           = I_applicant
         and l.fob_title_pass      = o.fob_title_pass
         and UPPER(l.fob_title_pass_desc) = UPPER(o.fob_title_pass_desc)
         and l.purchase_type       = o.purchase_type;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_LC_REF_ID','ORDLC','Order No: '||to_char(I_order_no));
   open C_LC_REF_ID;
   SQL_LIB.SET_MARK('FETCH','C_LC_REF_ID','ORDLC','Order No: '||to_char(I_order_no));
   fetch C_LC_REF_ID into L_lc_ref_id;
   if C_LC_REF_ID%NOTFOUND then
      O_valid := FALSE;
   else
      O_valid := TRUE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_LC_REF_ID','ORDLC','Order No: '||to_char(I_order_no));
   close C_LC_REF_ID;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      RETURN FALSE;
END VALID_ORDLC_LC_REF_ID;
-------------------------------------------------------------------
FUNCTION DELETE_ORDLC(O_error_message    IN OUT  VARCHAR2,
                      I_order_no         IN      ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program       VARCHAR2(50)    :=  'ORDER_ATTRIB_SQL.DELETE_ORDLC';
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);
   L_table         VARCHAR2(50);
   L_exists        VARCHAR2(1);
   ---
   cursor C_EXISTS is
      select 'x'
        from ordlc
       where order_no = I_order_no;
   ---
   cursor C_LOCK_ORDLC is
      select 'x'
        from ordlc
       where order_no = I_order_no
       for update nowait;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_EXISTS','ORDLC','Order No: '||to_char(I_order_no));
   open  C_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_EXISTS','ORDLC','Order No: '||to_char(I_order_no));
   fetch C_EXISTS into L_exists;
   if L_exists is not NULL then
      ---
      L_table := 'ORDLC';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLC',L_table,'Order No: '||to_char(I_order_no));
      open  C_LOCK_ORDLC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLC',L_table,'Order No: '||to_char(I_order_no));
      close C_LOCK_ORDLC;
      ---
      SQL_LIB.SET_MARK('DELETE', NULL,'ORDLC','Order No: '||to_char(I_order_no));
      delete from ordlc
        where order_no = I_order_no;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_EXISTS','ORDLC','Order No: '||to_char(I_order_no));
   close C_EXISTS;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            I_order_no,
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_ORDLC;
-------------------------------------------------------------------
FUNCTION PREV_APPROVED (O_error_message      IN OUT     VARCHAR2,
                        O_approved           IN OUT     VARCHAR2,
                        I_order_no           IN         ORDHEAD.ORDER_NO%TYPE)

RETURN BOOLEAN IS

   L_function   VARCHAR2(50)        :=  'ORDER_ATTRIB_SQL.C_PREV_APPROVED';
   L_approved   DATE;

   cursor C_PREV_APPROVED is
      select orig_approval_date
        from ordhead
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_PREV_APPROVED','ORDHEAD','Order No: '||to_char(I_order_no));
   open C_PREV_APPROVED;
   SQL_LIB.SET_MARK('FETCH','C_PREV_APPROVED','ORDHEAD','Order No: '||to_char(I_order_no));
   fetch C_PREV_APPROVED into L_approved;
   ---
   if  L_approved is NOT NULL then
      O_approved := 'Y';
   else 
      O_approved := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_PREV_APPROVED','ORDHEAD','Order No: '||to_char(I_order_no));
   close C_PREV_APPROVED;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END PREV_APPROVED;
----------------------------------------------------------------------------------------------
FUNCTION CHILDREN_ITEMS_EXIST (O_error_message IN OUT VARCHAR2,
                               O_exist         IN OUT BOOLEAN,
                               I_order_no      IN ORDHEAD.ORDER_NO%TYPE) 
RETURN BOOLEAN IS

   L_dummy   VARCHAR2(1);

   cursor C_ORD_EXIST is
      select 'x'
        from item_master im,
             ordsku os
       where os.order_no = I_order_no
         and ((im.item   = os.item
               and im.item_level   > 1
               and not exists (select 'x'
                                 from packitem p
                                where os.item = p.pack_no)
               and rownum = 1)
              or 
              (im.item = os.item
               and exists (select 'x' 
                             from packitem p,item_master im1
							 where os.item   = p.pack_no
                              and im1.item       = p.item
                              and im1.item_level > 1)));

BEGIN
   open C_ORD_EXIST;
   fetch C_ORD_EXIST into L_dummy;
   ---
   if C_ORD_EXIST%NOTFOUND then
      O_exist := FALSE;
      O_error_message := sql_lib.create_msg('NO_CHILD_ORD',I_order_no,null,null);
   else
      O_exist := TRUE;
   end if;
   ---
   close C_ORD_EXIST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
       O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                              SQLERRM,
                                              'ORDER_ATTRIB_SQL.CHILDREN_ITEMS_EXIST', 
                                              to_char(SQLCODE));
   return FALSE;
END CHILDREN_ITEMS_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION REPL_RESULTS_EXIST(O_error_message IN OUT   VARCHAR2,
                            O_exist         IN OUT   BOOLEAN,
                            I_order_no      IN       ORDHEAD.ORDER_NO%TYPE,
                            I_item          IN       ITEM_MASTER.ITEM%TYPE,
                            I_location      IN       ITEM_LOC.LOC%TYPE)
   RETURN BOOLEAN IS

   L_exist       VARCHAR2(1);

   cursor C_RR_ORDER_CHECK is
      select 'x'
        from repl_results
       where order_no = I_order_no;

   cursor C_RR_ORDER_ITEM_CHECK is
      select 'x'
        from repl_results
       where order_no = I_order_no
         and item = I_item;

   cursor C_RR_ORD_ITEM_LOC_CHECK is
      select 'x'
        from repl_results
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   if (I_item is NULL and I_location is NULL) then  -- check order only
      SQL_LIB.SET_MARK('OPEN',
                       'C_RR_ORDER_CHECK',
                       'repl_results',
                       'Order No: '||to_char(I_order_no));
      open C_RR_ORDER_CHECK;

      SQL_LIB.SET_MARK('FETCH',
                       'C_RR_ORDER_CHECK',
                       'repl_results',
                       'Order No: '||to_char(I_order_no));
      fetch C_RR_ORDER_CHECK into L_exist;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_RR_ORDER_CHECK',
                       'repl_results',
                       'Order No: '||to_char(I_order_no));
      close C_RR_ORDER_CHECK;
   else  
      if I_location is NULL then  -- check order/item
         SQL_LIB.SET_MARK('OPEN',
                          'C_RR_ORDER_ITEM_CHECK',
                          'repl_results',
                          'Order No: '||to_char(I_order_no));
         open C_RR_ORDER_ITEM_CHECK;

         SQL_LIB.SET_MARK('FETCH',
                          'C_RR_ORDER_ITEM_CHECK',
                          'repl_results',
                          'Order No: '||to_char(I_order_no));
         fetch C_RR_ORDER_ITEM_CHECK into L_exist;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_RR_ORDER_ITEM_CHECK',
                          'repl_results',
                          'Order No: '||to_char(I_order_no));
         close C_RR_ORDER_ITEM_CHECK;
      else  -- check ord/item/loc
         SQL_LIB.SET_MARK('OPEN',
                          'C_RR_ORD_ITEM_LOC_CHECK',
                          'repl_results',
                          'Order No: '||to_char(I_order_no));
         open C_RR_ORD_ITEM_LOC_CHECK;

         SQL_LIB.SET_MARK('FETCH',
                          'C_RR_ORD_ITEM_LOC_CHECK',
                          'repl_results',
                          'Order No: '||to_char(I_order_no));
         fetch C_RR_ORD_ITEM_LOC_CHECK into L_exist;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_RR_ORD_ITEM_LOC_CHECK',
                          'repl_results',
                          'Order No: '||to_char(I_order_no));
         close C_RR_ORD_ITEM_LOC_CHECK;
      end if;
   end if;
   ---
   if L_exist = 'x' then
      O_exist := TRUE;
   else
      O_exist := FALSE; 
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ATTRIB_SQL.REPL_RESULTS_EXIST', 
                                            to_char(SQLCODE));
   return FALSE;
END REPL_RESULTS_EXIST;
-----------------------------------------------------------------------------------
FUNCTION REPL_RESULTS_RECALC_EXIST(O_error_message IN OUT VARCHAR2,
                                   O_exist         IN OUT BOOLEAN,
                                   I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exist          VARCHAR2(1);

   cursor C_RECALC_EXIST is
      select 'x' 
        from repl_results
       where order_no = I_order_no
         and recalc_type in ('Q','A');

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_RECALC_EXIST',
                    'repl_results',
                    'Order No: '||to_char(I_order_no));
   open C_RECALC_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_RECALC_EXIST',
                    'repl_results',
                    'Order No: '||to_char(I_order_no));
   fetch C_RECALC_EXIST into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_RECALC_EXIST',
                    'repl_results',
                    'Order No: '||to_char(I_order_no));
   close C_RECALC_EXIST;
   ---
   if L_exist = 'x' then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ATTRIB_SQL.REPL_RESULTS_RECALC_EXIST', 
                                            to_char(SQLCODE));
         return FALSE;
END REPL_RESULTS_RECALC_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION ORDLOC_DISCOUNT_EXIST(O_error_message  IN OUT   VARCHAR2,
                               O_exist          IN OUT   BOOLEAN,
                               I_order_no       IN       ORDHEAD.ORDER_NO%TYPE,
                               I_item           IN       ORDLOC_DISCOUNT.ITEM%TYPE,
                               I_location       IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_exist  VARCHAR2(1)   :=  'N';

   cursor C_ORDLOC_DISCOUNT_EXIST is
      select 'Y'
        from ordloc_discount
       where order_no  = I_order_no
         and item      = I_item
         and (location = (select physical_wh
                           from wh
                          where wh = I_location)
          or location  = I_location);
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_item',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_location',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   ---
   O_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ORDLOC_DISCOUNT_EXIST','ORDLOC_DISCOUNT',NULL);
   open C_ORDLOC_DISCOUNT_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_ORDLOC_DISCOUNT_EXIST','ORDLOC_DISCOUNT',NULL);
   fetch C_ORDLOC_DISCOUNT_EXIST into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_ORDLOC_DISCOUNT_EXIST','ORDLOC_DISCOUNT',NULL);
   close C_ORDLOC_DISCOUNT_EXIST;
   ---
   if L_exist = 'Y' then
      O_exist := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ATTRIB_SQL.ORDLOC_DISCOUNT_EXIST', 
                                            to_char(SQLCODE));
      return FALSE;
END ORDLOC_DISCOUNT_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_CASES(O_error_message        IN OUT   VARCHAR2,
                         O_total_cases          IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                         O_total_prescale_cases IN OUT   ORDLOC.QTY_PRESCALED%TYPE,
                         I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                         I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                         I_location             IN       ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_function   VARCHAR2(40) := 'ORDER_ATTRIB_SQL.GET_TOTAL_CASES';

   cursor C_ORDER is
      select NVL(SUM(ol.qty_ordered/os.supp_pack_size),0),
             NVL(SUM(ol.qty_prescaled/os.supp_pack_size),0)
        from ordloc ol,
             ordsku os,
             wh wh
       where ol.order_no = I_order_no
         and (wh.physical_wh = NVL(I_location,wh.physical_wh) 
          or ol.location = NVL(I_location, ol.location))
         and ol.item     = NVL(I_item, ol.item)
         and ol.order_no = os.order_no
         and ol.item     = os.item
         and ol.location = wh.wh(+);

BEGIN
   if I_order_no is null then
         O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                                'I_order_no',
                                                'NULL',
                                                'NOT NULL');
         return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ORDER', 'ordloc, ordsku',
                    'ORDER NO:' || to_char(I_order_no));
   open C_ORDER;
   --- 
   SQL_LIB.SET_MARK('FETCH', 'C_ORDER', 'ordloc, ordsku',
                    'ORDER NO:' || to_char(I_order_no));
   fetch C_ORDER into O_total_cases,
                      O_total_prescale_cases;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ordloc, ordsku',
                    'ORDER NO:' || to_char(I_order_no));
   close C_ORDER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_CASES;
---------------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_PALLETS(O_error_message          IN OUT   VARCHAR2,
                           O_total_pallets          IN OUT   NUMBER,
                           O_total_prescale_pallets IN OUT   NUMBER,
                           I_order_no               IN       ORDHEAD.ORDER_NO%TYPE,
                           I_supplier               IN       ORDHEAD.SUPPLIER%TYPE,
                           I_item                   IN       ITEM_MASTER.ITEM%TYPE,
                           I_location               IN       ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_function          VARCHAR2(40) := 'ORDER_ATTRIB_SQL.GET_TOTAL_PALLETS';
   L_supplier          ordhead.supplier%TYPE  :=  I_supplier;
   L_origin_country_id ordsku.origin_country_id%TYPE;

   cursor C_GET_SUPPLIER is
      select supplier
        from ordhead
       where order_no = I_order_no;

   cursor C_ORDER is
      select nvl(SUM(ol.qty_ordered/(isc.ti * isc.hi * isc.supp_pack_size)),0),
             nvl(SUM(ol.qty_prescaled/(isc.ti * isc.hi * isc.supp_pack_size)),0) 
        from ordloc ol,
             item_supp_country isc,
             ordsku os,
             wh wh
       where ol.order_no           = I_order_no
         and (wh.physical_wh       = nvl(I_location, wh.physical_wh)
           or ol.location          = nvl(I_location, ol.location))
         and ol.item               = nvl(I_item, ol.item)
         and ol.item               = isc.item        
         and ol.order_no           = os.order_no
         and ol.item               = os.item
         and isc.supplier          = L_supplier
         and isc.origin_country_id = os.origin_country_id
         and ol.location           = wh.wh(+);

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   if I_supplier is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_GET_SUPPLIER',
                       'ordhead',
                       'Order No: '||to_char(I_order_no));
      open C_GET_SUPPLIER;
      SQL_LIB.SET_MARK('FETCH',
                       'C_GET_SUPPLIER',
                       'ordhead',
                       'Order No: '||to_char(I_order_no));
      fetch C_GET_SUPPLIER into L_supplier;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_GET_SUPPLIER',
                       'ordhead',
                       'Order No: '||to_char(I_order_no));
      close C_GET_SUPPLIER;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ORDER', 'ORDLOC,ORDSKU',
                    'ORDER NO:' || to_char(I_order_no));
   ---
   open C_ORDER;
   ---
   SQL_LIB.SET_MARK('FETCH', 'C_ORDER', 'ORDLOC,ORDSKU', 
                    'ORDER NO:' || to_char(I_order_no));
   ---
   fetch C_ORDER into O_total_pallets,
                      O_total_prescale_pallets;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDER', 'ORDLOC,ORDSKU', 
                    'ORDER NO:' || to_char(I_order_no));
   ---
   close C_ORDER;
   ---
   return TRUE;
   ---

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_PALLETS;
-------------------------------------------------------------------------------
FUNCTION SCALING_ELIGIBILITY(O_error_message   IN OUT   VARCHAR2,
                             O_scaling_status  IN OUT   BOOLEAN, 
                             O_partial_receipt IN OUT   BOOLEAN,
                             O_order_status    IN OUT   ordhead.status%TYPE,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_orig_ind             ordhead.orig_ind%TYPE;
   L_order_type           ordhead.order_type%TYPE;
   L_contract_no          ordhead.contract_no%TYPE;
   L_orig_approval_date   ordhead.orig_approval_date%TYPE;
   L_item_received        BOOLEAN;

   cursor C_GET_ORDHEAD_INFO is
      select order_type,
             orig_ind,
             status,
             contract_no,
             orig_approval_date
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_ORDHEAD_INFO',
                    'ordhead',
                    'Order No: '||to_char(I_order_no));
   open C_GET_ORDHEAD_INFO;

   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_ORDHEAD_INFO',
                    'ordhead',
                    'Order No: '||to_char(I_order_no));
   fetch C_GET_ORDHEAD_INFO into L_order_type,
                                 L_orig_ind,
                                 O_order_status,
                                 L_contract_no,
                                 L_orig_approval_date;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_ORDHEAD_INFO',
                    'ordhead',
                    'Order No: '||to_char(I_order_no));
   close C_GET_ORDHEAD_INFO;
   ---
   -- if any of the following conditions are met, no scaling allowed
   if (O_order_status != 'W' or L_orig_approval_date is NOT NULL) OR  -- order not worksheet or was approved
   (L_orig_ind = 5) OR               -- order is a vendor generated order
   (L_contract_no is NOT NULL) then  -- order is a contract order
      O_scaling_status := FALSE;
   else
      O_scaling_status := TRUE;
   end if;
   ---
   -- also, if any part of the order is received, cannot be scaled
   if not ORDER_ITEM_ATTRIB_SQL.ITEM_RECD(O_error_message,
                                          L_item_received,
                                          I_order_no,
                                          NULL) then
      return FALSE;
   end if;
   ---
   if L_item_received = TRUE then
      O_scaling_status := FALSE;
      O_partial_receipt := TRUE;
   else
      O_partial_receipt := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.SCALING_ELIGIBILITY',
                                             to_char(SQLCODE));
      return FALSE;
END SCALING_ELIGIBILITY;
-------------------------------------------------------------------------------
FUNCTION CHECK_TEMP_TABLES(O_error_message IN OUT VARCHAR2,
                           O_exist         IN OUT BOOLEAN, 
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exist          VARCHAR2(1);

   cursor C_ORDSKU_TEMP_EXIST is
      select 'x'
        from ordsku_temp
       where order_no = I_order_no;

   cursor C_REPL_RESULTS_TEMP_EXIST is
      select 'x'
        from repl_results_temp
       where order_no = I_order_no;

BEGIN  
   -- This function determines if an order which was previously
   -- scaled can be unscaled. First, it looks at the ordsku_temp
   -- table to determine if any data exists from which to rebuild the
   -- order. However, sometimes replenishment may generate an order 
   -- with a negative recommended order qty. In that case, there may be no ordsku
   -- record. Therefore, the repl_results_temp table should also be checked. Both
   -- tables are required because non-replenishment orders will not have a repl_results
   -- record.
   ---
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDSKU_TEMP_EXIST',
                    'ordsku_temp',
                    'Order No: '||to_char(I_order_no));
   open C_ORDSKU_TEMP_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDSKU_TEMP_EXIST',
                    'ordsku_temp',
                    'Order No: '||to_char(I_order_no));
   fetch C_ORDSKU_TEMP_EXIST into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDSKU_TEMP_EXIST',
                    'ordsku_temp',
                    'Order No: '||to_char(I_order_no));
   close C_ORDSKU_TEMP_EXIST;

   if L_exist = 'x' then
      O_exist := TRUE;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_REPL_RESULTS_TEMP_EXIST',
                       'repl_results_temp',
                       'Order No: '||to_char(I_order_no));
      open C_REPL_RESULTS_TEMP_EXIST;

      SQL_LIB.SET_MARK('FETCH',
                       'C_REPL_RESULTS_TEMP_EXIST',
                       'repl_results_temp',
                       'Order No: '||to_char(I_order_no));
      fetch C_REPL_RESULTS_TEMP_EXIST into L_exist;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_REPL_RESULTS_TEMP_EXIST',
                       'repl_results_temp',
                       'Order No: '||to_char(I_order_no));
      close C_REPL_RESULTS_TEMP_EXIST;

      if L_exist = 'x' then
         O_exist := TRUE;
      else
         O_exist := FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.CHECK_TEMP_TABLES',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_TEMP_TABLES;
-------------------------------------------------------------------------------
FUNCTION ORD_INV_MGMT_EXIST(O_error_message IN OUT VARCHAR2,
                            O_exist         IN OUT BOOLEAN, 
                            I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exist          VARCHAR2(1);

   cursor C_ORD_INV_MGMT_EXIST is
      select 'x'
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORD_INV_MGMT_EXIST',
                    'ord_inv_mgmt',
                    'Order No: '||to_char(I_order_no));
   open C_ORD_INV_MGMT_EXIST;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ORD_INV_MGMT_EXIST',
                    'ord_inv_mgmt',
                    'Order No: '||to_char(I_order_no));
   fetch C_ORD_INV_MGMT_EXIST into L_exist;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORD_INV_MGMT_EXIST',
                    'ord_inv_mgmt',
                    'Order No: '||to_char(I_order_no));
   close C_ORD_INV_MGMT_EXIST;

   if L_exist = 'x' then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.ORD_INV_MGMT_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END ORD_INV_MGMT_EXIST;
-------------------------------------------------------------------------------
FUNCTION GET_SCALE_CNSTR_IND(O_error_message     IN OUT   VARCHAR2,
                             O_scale_cnstr_ind   IN OUT   ord_inv_mgmt.scale_cnstr_ind%TYPE, 
                             I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_SCALE_CNSTR_IND is
      select scale_cnstr_ind
        from ord_inv_mgmt
       where order_no = I_order_no;

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SCALE_CNSTR_IND', 'ord_inv_mgmt', 'Order No: '||to_char(I_order_no));
   open C_GET_SCALE_CNSTR_IND;

   SQL_LIB.SET_MARK('FETCH', 'C_GET_SCALE_CNSTR_IND', 'ord_inv_mgmt', 'Order No: '||to_char(I_order_no));
   fetch C_GET_SCALE_CNSTR_IND into O_scale_cnstr_ind;
   if C_GET_SCALE_CNSTR_IND%NOTFOUND then
      O_scale_cnstr_ind := 'N';
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SCALE_CNSTR_IND', 'ord_inv_mgmt', 'Order No: '||to_char(I_order_no));
   close C_GET_SCALE_CNSTR_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.GET_SCALE_CNSTR_IND',
                                             to_char(SQLCODE));
      return FALSE;
END GET_SCALE_CNSTR_IND;
-------------------------------------------------------------------------------
FUNCTION CHECK_COST_SOURCES(O_error_message     IN OUT   VARCHAR2,
                            O_manual_exist      IN OUT   BOOLEAN, 
                            I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1)   := 'N';

   cursor C_CHECK_MANL is
      select 'Y' 
        from ordloc
       where order_no = I_order_no
         and cost_source = 'MANL';

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_MANL', 'ordloc', 'Order No: '||to_char(I_order_no));
   open C_CHECK_MANL;

   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_MANL', 'ordloc', 'Order No: '||to_char(I_order_no));
   fetch C_CHECK_MANL into L_exist;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_MANL', 'ordloc', 'Order No: '||to_char(I_order_no));
   close C_CHECK_MANL;
   ---
   if L_exist = 'Y' then
      O_manual_exist := TRUE;
   else
      O_manual_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.CHECK_COST_SOURCES',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_COST_SOURCES;
--------------------------------------------------------------------------------------------
FUNCTION GET_SUPP_DATE(O_error_message      IN OUT VARCHAR2,
                       O_supplier           IN OUT ORDHEAD.SUPPLIER%TYPE,
                       O_not_before_date    IN OUT ORDHEAD.NOT_BEFORE_DATE%TYPE,
                       I_order_no           IN     ORDHEAD.ORDER_NO%TYPE) 
   RETURN BOOLEAN is

  cursor C_GET_SUPP_DATE is
     select supplier,
            not_before_date
       from ordhead 
      where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_SUPP_DATE','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
   open C_GET_SUPP_DATE;
   SQL_LIB.SET_MARK('FETCH','C_GET_SUPP_DATE','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
   fetch C_GET_SUPP_DATE into O_supplier, O_not_before_date;
   if C_GET_SUPP_DATE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO', NULL, NULL, NULL);
      SQL_LIB.SET_MARK('CLOSE','C_GET_SUPP_DATE','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
      close C_GET_SUPP_DATE;
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE','C_GET_SUPP_DATE','ORDHEAD','ORDER_NO: '||to_char(I_order_no));
   close C_GET_SUPP_DATE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ATTRIB_SQL.GET_SUPP_DATE',
                                            to_char(SQLCODE));
      return FALSE;
END GET_SUPP_DATE;
-------------------------------------------------------------------------------
FUNCTION GET_ORIG_IND(O_error_message     IN OUT   VARCHAR2,
                      O_orig_ind          IN OUT   ordhead.orig_ind%TYPE, 
                      I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_ORIG_IND is
      select orig_ind
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   open  C_GET_ORIG_IND;
   fetch C_GET_ORIG_IND into O_orig_ind;
   ---
   if C_GET_ORIG_IND%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_ORDER_NO',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      close C_GET_ORIG_IND;
      return FALSE;
   else
      close C_GET_ORIG_IND;
      return TRUE;
   end if;
   ---

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.GET_ORIG_IND',
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORIG_IND;
-------------------------------------------------------------------------------
FUNCTION GET_COMMENTS(O_error_message IN OUT VARCHAR2,
                      O_comment_desc  IN OUT ORDHEAD.COMMENT_DESC%TYPE,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'ORDER_ATTRIB_SQL.GET_COMMENTS';

   cursor C_GET_COMMENTS is
      select comment_desc
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COMMENTS','ORDHEAD','order_no: ' ||
                     to_char(I_order_no));
   open C_GET_COMMENTS;
   SQL_LIB.SET_MARK('FETCH','C_GET_COMMENTS','ORDHEAD','order_no: ' || 
                     to_char(I_order_no));
   fetch C_GET_COMMENTS into O_comment_desc;
   SQL_LIB.SET_MARK('CLOSE','C_GET_COMMENTS','ORDHEAD','order_no: ' || 
                     to_char(I_order_no));
   close C_GET_COMMENTS;

   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;  
END GET_COMMENTS;
-------------------------------------------------------------------------------
FUNCTION UPDATE_COMMENTS(O_error_message IN OUT VARCHAR2,
                         I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                         I_comment_desc  IN     ORDHEAD.COMMENT_DESC%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(64) := 'ORDER_ATTRIB_SQL.UPDATE_COMMENTS';
   RECORD_LOCKED               EXCEPTION;
   PRAGMA                      EXCEPTION_INIT(RECORD_LOCKED, -54);
   ---
   cursor C_LOCK_HEAD is
      select 'x'
        from ordhead
       where order_no = I_order_no
         for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_LOCK_HEAD', 'ORDHEAD', 'order_no: '||
                     to_char(I_order_no));
   open  C_LOCK_HEAD;
   SQL_LIB.SET_MARK('CLOSE', 'C_LOCK_HEAD', 'ORDHEAD', 'order_no: '||
                     to_char(I_order_no));
   close C_LOCK_HEAD;      
   --- 
   SQL_LIB.SET_MARK('UPDATE', NULL, 'ordhead', 'order_no: '||to_char(I_order_no));

   update ordhead
      set comment_desc = I_comment_desc,
          last_update_id = get_user,
          last_update_datetime = sysdate
    where order_no = I_order_no;

   return TRUE;
   --- 
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('ORDHEAD_REC_LOC',
                                             to_char(I_order_no),
                                             NULL,
                                             NULL);
      return FALSE;
   ---  
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE; 
END UPDATE_COMMENTS;
-------------------------------------------------------------------------------
FUNCTION ORDHEAD_DISCOUNT_EXIST(O_error_message  IN OUT VARCHAR2,
                                O_exist          IN OUT BOOLEAN,
                                I_order_no       IN     ORDHEAD_DISCOUNT.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)  := 'ORDER_ATTRIB_SQL.ORDHEAD_DISCOUNT_EXIST';
   L_exist      VARCHAR2(1);
   ---
   cursor C_ORDHEAD_DISCOUNT_EXIST is
      select 'x'
        from ordhead_discount
       where order_no = I_order_no;
BEGIN
   O_exist := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_ORDHEAD_DISCOUNT_EXIST', 'ORDHEAD_DISCOUNT', 'Order no: '||to_char(I_order_no));
   open C_ORDHEAD_DISCOUNT_EXIST;
   SQL_LIB.SET_MARK('FETCH', 'C_ORDHEAD_DISCOUNT_EXIST', 'ORDHEAD_DISCOUNT', 'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD_DISCOUNT_EXIST into L_exist;
   if C_ORDHEAD_DISCOUNT_EXIST%FOUND then
      O_exist := TRUE;
   else
      O_exist := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_ORDHEAD_DISCOUNT_EXIST', 'ORDHEAD_DISCOUNT', 'Order no: '||to_char(I_order_no));
   close C_ORDHEAD_DISCOUNT_EXIST;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ORDHEAD_DISCOUNT_EXIST;
--------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_MFG_REL_IND(O_error_message  IN OUT VARCHAR2,
                         O_mfg_id         IN OUT SUP_IMPORT_ATTR.MFG_ID%TYPE,
                         O_related_ind    IN OUT SUP_IMPORT_ATTR.related_ind%TYPE,
                         I_order_no       IN ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(64)  := 'ORDER_ATTRIB_SQL.GET_MFG_REL_IND';
   L_exist      VARCHAR2(1);
   ---
   cursor C_GET_SUPP_MFG is
      select sia.mfg_id,
             sia.related_ind
        from sup_import_attr sia,
             ordhead oh
       where oh.order_no = I_order_no
         and oh.supplier = sia.supplier;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   O_mfg_id      := NULL;
   O_related_ind := NULL;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_SUPP_MFG', 'ORDHEAD, SUP_IMPORT_ATTR', 'Order no: '||to_char(I_order_no));
   open C_GET_SUPP_MFG;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_SUPP_MFG', 'ORDHEAD, SUP_IMPORT_ATTR', 'Order no: '||to_char(I_order_no));
   fetch C_GET_SUPP_MFG into O_mfg_id,
                             O_related_ind;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_SUPP_MFG', 'ORDHEAD, SUP_IMPORT_ATTR', 'Order no: '||to_char(I_order_no));
   close C_GET_SUPP_MFG;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_MFG_REL_IND;
-------------------------------------------------------------------------------
FUNCTION CHECK_BUYER_PACKS(O_error_message     IN OUT   VARCHAR2,
                           O_buyer_packs_exist IN OUT   BOOLEAN, 
                           I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_exist             VARCHAR2(1)   := 'N';

   cursor C_CHECK_BUYER_PACKS is
      select 'Y' 
        from ordloc o,
             item_master i
       where o.order_no  = I_order_no
         and o.item      = i.item
         and i.pack_type = 'B';

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_BUYER_PACKS' , 'ordloc', 'Order No: '||to_char(I_order_no));
   open C_CHECK_BUYER_PACKS;

   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_BUYER_PACKS', 'ordloc', 'Order No: '||to_char(I_order_no));
   fetch C_CHECK_BUYER_PACKS into L_exist;

   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_BUYER_PACKS', 'ordloc', 'Order No: '||to_char(I_order_no));
   close C_CHECK_BUYER_PACKS;
   ---
   if L_exist = 'Y' then
      O_buyer_packs_exist := TRUE;
   else
      O_buyer_packs_exist := FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.CHECK_COST_SOURCES',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_BUYER_PACKS;
-------------------------------------------------------------------------------
FUNCTION GET_DATES(O_error_message     IN OUT   VARCHAR2,
                   O_written_date      IN OUT   ORDHEAD.WRITTEN_DATE%TYPE,           
                   O_not_before_date   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE, 
                   O_not_after_date    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                   O_otb_eow_date      IN OUT   ORDHEAD.OTB_EOW_DATE%TYPE,           
                   O_ealiest_ship_date IN OUT   ORDHEAD.EARLIEST_SHIP_DATE%TYPE,
                   O_latest_ship_date  IN OUT   ORDHEAD.LATEST_SHIP_DATE%TYPE,
                   O_close_date        IN OUT   ORDHEAD.CLOSE_DATE%TYPE,             
                   I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS


   cursor C_DATES is
      select written_date,          
             not_before_date,        
             not_after_date,       
             otb_eow_date,           
             earliest_ship_date,
             latest_ship_date,
             close_date    
        from ordhead
       where order_no = I_order_no;

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_DATES ', 'ordhead', 'Order No: '||to_char(I_order_no));
   open C_DATES;

   SQL_LIB.SET_MARK('FETCH', 'C_DATES ', 'ordhead', 'Order No: '||to_char(I_order_no));
   fetch C_DATES into O_written_date,           
                      O_not_before_date, 
                      O_not_after_date,
                      O_otb_eow_date,           
                      O_ealiest_ship_date,
                      O_latest_ship_date,
                      O_close_date;

   SQL_LIB.SET_MARK('CLOSE', 'C_DATES ', 'ordhead', 'Order No: '||to_char(I_order_no));
   close C_DATES ;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.CHECK_COST_SOURCES',
                                             to_char(SQLCODE));
      return FALSE;
END GET_DATES;
-------------------------------------------------------------------------------
FUNCTION CHECK_CONTRACT_ORDER(O_error_message     IN OUT   VARCHAR2,
                              O_contract_exists   IN OUT   BOOLEAN,
                              I_order_no          IN       ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN is

   L_dummy   VARCHAR2(1);

   cursor C_CONT_ORD is
      select 'x'
        from ordhead
       where order_no = I_order_no
         and contract_no is NOT NULL;  

BEGIN
   if I_order_no is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             'I_order_no',
                                             'NULL',
                                             'NOT NULL');
      return FALSE;
   end if;

   O_contract_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN', 'C_CONT_ORD ', 'ORDHEAD', 'Order No: '||to_char(I_order_no));
   open C_CONT_ORD;
   SQL_LIB.SET_MARK('FETCH', 'C_CONT_ORD ', 'ORDHEAD', 'Order No: '||to_char(I_order_no));
   fetch C_CONT_ORD into L_dummy;
   ---
   if C_CONT_ORD%FOUND then
      O_contract_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE', 'C_CONT_ORD ', 'ORDHEAD', 'Order No: '||to_char(I_order_no));
   close C_CONT_ORD;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.CHECK_CONTRACT_ORDER',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_CONTRACT_ORDER;
-------------------------------------------------------------------------------
FUNCTION MULTIPLE_LOCS_EXIST(O_error_message       IN OUT VARCHAR2,
                             O_location            IN OUT ORDLOC.LOCATION%TYPE,
                             O_loc_type            IN OUT ORDLOC.LOC_TYPE%TYPE,
                             O_multiple_locs_exist IN OUT BOOLEAN,
                             O_virtual_wh          IN OUT WH.WH%TYPE,
                             I_order_no            IN     ORDHEAD.ORDER_NO%TYPE)
RETURN  BOOLEAN IS

   L_loc_count           NUMBER;
   L_location            ORDLOC.LOCATION%TYPE;
   L_loc_type            ORDLOC.LOC_TYPE%TYPE;
   L_phys_wh             WH.PHYSICAL_WH%TYPE;
   L_virtual_count       NUMBER;
   L_virtual_wh          WH.WH%TYPE;

   cursor C_MULTI_CHANNEL_LOCATION is
      select count(distinct (nvl(wh.physical_wh, ol.location))),
             max(nvl(wh.physical_wh, ol.location)),
             max(ol.loc_type),
             max(wh.physical_wh),
             count(distinct ol.location),
             max(ol.location)
        from wh,
             ordloc ol
       where ol.order_no = I_order_no
         and ol.location = wh.wh(+);

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE ;
   end if;
   O_multiple_locs_exist := FALSE; 

   SQL_LIB.SET_MARK('OPEN',
                    'C_MULTI_CHANNEL_LOCATION',
                    'ORDLOC, WH',
                    'ORDER NO: '||to_char(I_order_no));
   open C_MULTI_CHANNEL_LOCATION;
   SQL_LIB.SET_MARK('FETCH',
                    'C_MULTI_CHANNEL_LOCATION',
                    'ORDLOC, WH',
                    'ORDER NO: '||to_char(I_order_no));
   fetch C_MULTI_CHANNEL_LOCATION into L_loc_count,
                                       L_location,
                                       L_loc_type,
                                       L_phys_wh,
                                       L_virtual_count,
                                       L_virtual_wh;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_MULTI_CHANNEL_LOCATION',
                    'ORDLOC, WH',
                    'ORDER NO: '||to_char(I_order_no));
   close C_MULTI_CHANNEL_LOCATION;

   /* If there is only one distinct location on the order, check if it's */
   /* a physical warehouse.  If so, check if there is only one distinct virtual */
   /* location on the order.  If there is one virtual location on the order */
   /* pass out the virtual location. */

   if L_loc_count = 1 then
      if L_phys_wh is NOT NULL and L_virtual_count = 1 then
         O_virtual_wh := L_virtual_wh;
      else
         O_virtual_wh := NULL;
      end if;
   else
      O_virtual_wh := NULL;
   end if;

   /* If multichannel is on, O_mutiple_locs_exists will be TRUE if there */
   /* are multiple stores or multiple physical warehouses on the order. If */
   /* there are multiple virtual warehouses on the order but they all have */
   /* the same physical warehouse, this is still considered as a one location */
   /* order. Therefore, O_multiple_locs_exist will be FALSE. */

   if L_loc_count = 1 then 
      O_multiple_locs_exist := FALSE;
      O_location := L_location;
      O_loc_type := L_loc_type;
   elsif L_loc_count > 1 then
      O_multiple_locs_exist := TRUE;
      O_location := NULL;
      O_loc_type := NULL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.MULTIPLE_LOCS_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END MULTIPLE_LOCS_EXIST;
--------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOC_DEAL_DISCOUNTS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_total_tran_disc       IN OUT ORDLOC_DISCOUNT.DISCOUNT_AMT_PER_UNIT%TYPE,
                                     O_total_non_tran_disc   IN OUT ORDLOC_DISCOUNT.DISCOUNT_AMT_PER_UNIT%TYPE,
                                     I_order_no              IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_TOTAL_TRAN_DISC is
      select sum(nvl(total_discount_amt,0))
        from v_ord_deal_summ
       where order_no = I_order_no
         and tran_discount_ind = 'Y';

   cursor C_GET_TOTAL_NON_TRAN_DISC is
      select sum(nvl(total_discount_amt,0))
        from v_ord_deal_summ
       where order_no = I_order_no
         and tran_discount_ind = 'N';

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            'ORDER_ATTRIB_SQL.GET_ITEM_LOC_DEAL_DISCOUNTS',
                                            NULL);
      return FALSE;
   end if; 

   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TOTAL_TRAN_DISC',
                    'V_ORD_DEAL_SUMM',
                    'Order No: '||I_order_no);
   open C_GET_TOTAL_TRAN_DISC;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TOTAL_TRAN_DISC',
                    'V_ORD_DEAL_SUMM',
                    'Order No: '||I_order_no);
   fetch C_GET_TOTAL_TRAN_DISC into O_total_tran_disc;
   
   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TOTAL_TRAN_DISC',
                    'V_ORD_DEAL_SUMM',
                    'Order No: '||I_order_no);
   close C_GET_TOTAL_TRAN_DISC;
   
   if O_total_tran_disc is NULL then
      O_total_tran_disc := 0;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_GET_TOTAL_NON_TRAN_DISC',
                    'V_ORD_DEAL_SUMM',
                    'Order No: '||I_order_no);
   open C_GET_TOTAL_NON_TRAN_DISC;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_GET_TOTAL_NON_TRAN_DISC',
                    'V_ORD_DEAL_SUMM',
                    'Order No: '||I_order_no);
   fetch C_GET_TOTAL_NON_TRAN_DISC into O_total_non_tran_disc;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_GET_TOTAL_NON_TRAN_DISC',
                    'V_ORD_DEAL_SUMM',
                    'Order No: '||I_order_no);
   close C_GET_TOTAL_NON_TRAN_DISC;
   
   if O_total_non_tran_disc is NULL then
      O_total_non_tran_disc := 0;
   end if;

   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ATTRIB_SQL.GET_ITEM_LOC_DEAL_DISCOUNTS',
                                            to_char(SQLCODE));
   return FALSE;
END GET_ITEM_LOC_DEAL_DISCOUNTS;          
--------------------------------------------------------------------------------------------
FUNCTION GET_TOTAL_STATCASE(O_error_message            IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_total_statcase           IN OUT NUMBER,
                            O_total_prescale_statcase  IN OUT NUMBER,
                            I_order_no                 IN     ORDHEAD.ORDER_NO%TYPE,
                            I_supplier                 IN     ORDHEAD.SUPPLIER%TYPE,
                            I_item                     IN     ITEM_MASTER.ITEM%TYPE,
                            I_location                 IN     ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_supplier         ORDHEAD.SUPPLIER%TYPE;
   L_exists           BOOLEAN;
   cursor C_ORDER is   
      select NVL(SUM(ol.qty_ordered/os.supp_pack_size * isc.stat_cube),0),
             NVL(SUM(ol.qty_prescaled/os.supp_pack_size * isc.stat_cube),0)
        from ordloc ol,
             ordsku os,
             item_supp_country_dim isc,
             wh wh
       where ol.order_no        = I_order_no
         and (wh.physical_wh    = NVL(I_location,wh.physical_wh) 
              or ol.location    = NVL(I_location, ol.location))
         and ol.item            = NVL(I_item, ol.item)
         and ol.order_no        = os.order_no
         and ol.item            = os.item
         and ol.item            = isc.item
         and isc.supplier       = L_supplier
         and isc.origin_country = os.origin_country_id
         and ol.location        = wh.wh(+)
         and isc.dim_object     = 'CA';

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE ;
   end if;
   ---
   if I_supplier is NULL then
      if ORDER_ATTRIB_SQL.GET_SUPPLIER(O_error_message,
                                       L_exists,
                                       L_supplier,
                                       I_order_no) = FALSE then
         return FALSE;
      end if;
   else  
      L_supplier := I_supplier;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDER',
                    'ORDLOC, ORDSKU, ITEM_SUPP_COUNTRY_DIM, WH',
                    'Order_no.: ' || to_char(I_order_no));
   open C_ORDER;
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDER',
                    'ORDLOC, ORDSKU, ITEM_SUPP_COUNTRY_DIM, WH',
                    'Order_no.: ' || to_char(I_order_no));
   fetch C_ORDER into O_total_statcase,
                      O_total_prescale_statcase;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDER',
                    'ORDLOC, ORDSKU, ITEM_SUPP_COUNTRY_DIM, WH',
                    'Order_no.: ' || to_char(I_order_no));
   close C_ORDER;        
   ---         
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ATTRIB_SQL.GET_TOTAL_STATCASE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_TOTAL_STATCASE;
-------------------------------------------------------------------
FUNCTION GET_LOCATION(O_error_message  IN OUT VARCHAR2,
                      O_exists         IN OUT BOOLEAN,
                      O_loc_type       IN OUT ORDHEAD.LOC_TYPE%TYPE,
                      O_location       IN OUT ORDHEAD.LOCATION%TYPE,
                      I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   cursor C_GET_LOC is
      select loc_type,
             location
        from ordhead
       where order_no = I_order_no;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_LOC','ORDHEAD','ORDER NO: '||to_char(I_order_no));
   open C_GET_LOC;
   SQL_LIB.SET_MARK('FETCH','C_GET_LOC','ORDHEAD','ORDER NO: '||to_char(I_order_no));
   fetch C_GET_LOC into O_loc_type,
                        O_location;
   ---
   if C_GET_LOC%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_LOC','ORDHEAD','ORDER NO: '||to_char(I_order_no));
   close C_GET_LOC;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             'ORDER_ATTRIB_SQL.GET_LOCATION',
                                             to_char(SQLCODE));
      return FALSE;
END GET_LOCATION;
----------------------------------------------------------------------------------------------------------------------------
FUNCTION PAY_TERMS_CODE(O_error_message IN OUT VARCHAR2,
                        O_terms         IN OUT TERMS.TERMS%TYPE,
                        O_terms_desc    IN OUT TERMS.TERMS_DESC%TYPE,
                        O_terms_code    IN OUT TERMS.TERMS_CODE%TYPE,
                        I_code          IN     TERMS.TERMS_CODE%TYPE)
   RETURN BOOLEAN IS

   L_function VARCHAR2(50)      := 'ORDER_ATTRIB_SQL.PAY_TERMS_CODE';
   L_vdate    PERIOD.VDATE%TYPE := GET_VDATE;
   L_dummy    VARCHAR2(1)       := NULL;
   ---

   cursor C_ATTRIB is
      select terms,
             terms_code,
             terms_desc
        from v_terms_head_tl
       where terms_code = I_code;
	   
      cursor C_CHECK_DATES is
         select 'x'
           from terms
          where UPPER(terms_code) = UPPER(O_terms_code)
            and (NVL(start_date_active, to_date('00010101', 'YYYYMMDD')) > L_vdate
             or NVL(end_date_active, to_date('99990101', 'YYYYMMDD')) < L_vdate
             or enabled_flag = 'N');

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
   fetch C_ATTRIB into O_terms, O_terms_code, O_terms_desc;
   ---
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_TERMS',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
      close C_ATTRIB;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','V_TERMS_HEAD_TL','TERMS: '||I_code);
   close C_ATTRIB;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_DATES','TERMS','TERMS CODE: '||to_char(I_code));
   open C_CHECK_DATES;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_DATES','TERMS','TERMS CODE: '||to_char(I_code));
   fetch C_CHECK_DATES into L_dummy;
   --- check for inactive terms based on start and end dates
   if C_CHECK_DATES%FOUND then
      O_error_message := sql_lib.create_msg('INACTIVE_TERMS',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_DATES','TERMS','TERMS CODE: '||to_char(I_code));
      close C_CHECK_DATES;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_DATES','TERMS','TERMS CODE: '||to_char(I_code));
   close C_CHECK_DATES;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function, 
                                             to_char(SQLCODE));
   return FALSE;
END PAY_TERMS_CODE;
----------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_TYPE(O_error_message IN OUT VARCHAR2,
                        O_order_type    IN OUT ORDHEAD.ORDER_TYPE%TYPE,
                        I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_ORDER_TYPE';

   cursor C_ORDER_TYPE is
      select order_type
        from ordhead
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ORDER_TYPE','ORDHEAD','ORDER_TYPE: '||I_order_no);
   open C_ORDER_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_ORDER_TYPE','ORDHEAD','ORDER_TYPE: '||I_order_no);
   fetch C_ORDER_TYPE into O_order_type;
   ---
   if C_ORDER_TYPE%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM_IN_FUNC','I_old_order', I_order_no, L_function );
      SQL_LIB.SET_MARK('CLOSE','C_ORDER_TYPE','ORDHEAD','ORDER_TYPE: '||I_order_no);
      close C_ORDER_TYPE;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ORDER_TYPE','ORDHEAD','ORDER_TYPE: '||I_order_no);
   close C_ORDER_TYPE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDER_TYPE;
-------------------------------------------------------------------------------------------------
FUNCTION GET_ORDHEAD_ROW(O_error_message IN OUT        VARCHAR2,
                         O_ordhead_row      OUT NOCOPY ORDHEAD%ROWTYPE,
                         I_order_no      IN            ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_ORDHEAD_ROW';

   cursor C_ROW is
      select *
        from ordhead
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ROW','ORDHEAD','ORDER_NO: '||I_order_no);
   open C_ROW;
   SQL_LIB.SET_MARK('FETCH','C_ROW','ORDHEAD','ORDER_NO: '||I_order_no);
   fetch C_ROW into O_ordhead_row;
   ---
   if C_ROW%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('ORDER_NOT_EXIST',NULL, NULL, NULL );
      SQL_LIB.SET_MARK('CLOSE','C_ROW','ORDHEAD','ORDER_NO: '||I_order_no);
      close C_ROW;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ROW','ORDHEAD','ORDER_NO: '||I_order_no);
   close C_ROW;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDHEAD_ROW;
-------------------------------------------------------------------
FUNCTION GET_ORDSKU_ROW(O_error_message IN OUT        VARCHAR2,
                        O_exists        IN OUT        BOOLEAN,
                        O_ordsku_row       OUT NOCOPY ORDSKU%ROWTYPE,
                        I_order_no      IN            ORDSKU.ORDER_NO%TYPE,
                        I_item          IN            ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_ORDSKU_ROW';

   cursor C_ROW is
      select *
        from ordsku
       where order_no = I_order_no
         and item = I_item;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ROW','ORDSKU','ORDER_NO: '||I_order_no||' ITEM: '||I_item);
   open C_ROW;
   SQL_LIB.SET_MARK('FETCH','C_ROW','ORDSKU','ORDER_NO: '||I_order_no||' ITEM: '||I_item);
   fetch C_ROW into O_ordsku_row;
   ---
   if C_ROW%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ROW','ORDSKU','ORDER_NO: '||I_order_no||' ITEM: '||I_item);
   close C_ROW;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDSKU_ROW;
-------------------------------------------------------------------
FUNCTION GET_ORDLOC_ROW(O_error_message IN OUT        VARCHAR2,
                        O_exists        IN OUT        BOOLEAN,
                        O_ordloc_row       OUT NOCOPY ORDLOC%ROWTYPE,
                        I_order_no      IN            ORDLOC.ORDER_NO%TYPE,
                        I_item          IN            ORDLOC.ITEM%TYPE,
                        I_location      IN            ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_ORDLOC_ROW';

   L_ordloc_row   ORDLOC%ROWTYPE;

   cursor C_ROW is
      select *
        from ordloc
       where order_no = I_order_no
         and item = I_item
         and location = I_location;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_ROW','ORDLOC',
                    'ORDER_NO: '||I_order_no||' ITEM: '||I_item|| ' LOCATION: '||I_location);
   open C_ROW;
   SQL_LIB.SET_MARK('FETCH','C_ROW','ORDLOC',
                    'ORDER_NO: '||I_order_no||' ITEM: '||I_item|| ' LOCATION: '||I_location);
   fetch C_ROW into O_ordloc_row;
   ---
   if C_ROW%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ROW','ORDLOC',
                    'ORDER_NO: '||I_order_no||' ITEM: '||I_item|| ' LOCATION: '||I_location);
   close C_ROW;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDLOC_ROW;

----------------------------------------------------------------------------
FUNCTION ORD_ONLY_ITEM(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists         IN OUT   BOOLEAN,
                       I_order_no       IN       ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program   VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.ORD_ONLY_ITEM';
   L_dummy     VARCHAR2(1)   := NULL;
   ---
   cursor C_CHECK_ORDER is
      select 'X'
        from ordloc o,
             item_master i
       where o.order_no       = I_order_no
         and o.item           = i.item
         and i.orderable_ind  = 'Y'
         and i.sellable_ind   = 'N'
         and i.item_xform_ind = 'N'
         and (i.pack_ind = 'N' 
             or (i.pack_ind = 'Y'  
                 and exists (select 1 
                               from v_packsku_qty vpq,
                                    item_master im                                   
                              where vpq.pack_no       = i.item
                                and im.item           = vpq.item
                                and im.orderable_ind  = 'Y'
                                and im.sellable_ind   = 'N'
                                and im.item_xform_ind = 'N')));

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_ORDER', 'ORDLOC, ITEM_MASTER', 
                    'Order no: '||to_char(I_order_no));
   open C_CHECK_ORDER;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_ORDER', 'ORDLOC, ITEM_MASTER', 
                    'Order no: '||to_char(I_order_no));
   fetch C_CHECK_ORDER into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_ORDER', 'ORDLOC, ITEM_MASTER', 
                       'Order no: '||to_char(I_order_no));
   close C_CHECK_ORDER;
   ---

   O_exists := (L_dummy is NOT NULL);

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                             SQLERRM, 
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END ORD_ONLY_ITEM;

--------------------------------------------------------------------------------------------
-- Function: COMPLEX_PACK_EXISTS
-- Purpose : This new function will determine if a PO contains a complex pack.
-- This is needed for the ORDLOC.FMB form in enabling / disabling the new button "Pack Components".
--------------------------------------------------------------------------------------------
FUNCTION COMPLEX_PACK_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_exists          IN OUT   BOOLEAN,
                              I_order           IN       ORDHEAD.ORDER_NO%TYPE)


   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_ATTRIB_SQL.COMPLEX_PACK_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_CHECK_COMPLEX_ORDER IS
      select  'x' 
         from ordloc o,
              item_master im
        where o.order_no = I_order
          and o.item = im.item
          and im.pack_ind = 'Y'
          and im.simple_pack_ind = 'N';

BEGIN

   if I_order is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_order',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   open C_CHECK_COMPLEX_ORDER;
   fetch C_CHECK_COMPLEX_ORDER into L_dummy;
   ---
   if C_CHECK_COMPLEX_ORDER%NOTFOUND then
      O_exists := FALSE;
   else
      O_exists := TRUE;
   end if;
   ---
   close C_CHECK_COMPLEX_ORDER;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END COMPLEX_PACK_EXISTS;
--------------------------------------------------------------------------------------------------
-- Function: PARTIAL_RECEIVED_ORDER
-- Purpose : This new function will determine if a PO has been partially received or not none has 
--           been received yet.
-- This is needed for the ORDHEAD.FMB form in enabling the estimated_in_stock_date.
--------------------------------------------------------------------------------------------------
FUNCTION PARTIAL_NONE_RCVD_ORD(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_exists          IN OUT   BOOLEAN,
                               I_order           IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_ATTRIB_SQL.PARTIAL_NONE_RCVD_ORD';
   L_dummy     VARCHAR2(1)   := NULL;
   cursor C_CHECK_COMPLEX_ORDER IS
      select 'x'
        from ordloc
       where order_no = I_order
         and qty_ordered - nvl(qty_received, 0) > 0
         and rownum = 1;

BEGIN
   if I_order is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_order',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('OPEN', 
                    'C_CHECK_COMPLEX_ORDER', 
                    'ORDLOC', 
                    'Order no: '||to_char(I_order));
   open C_CHECK_COMPLEX_ORDER;
   SQL_LIB.SET_MARK('FETCH', 
                    'C_CHECK_COMPLEX_ORDER', 
                    'ORDLOC', 
                    'Order no: '||to_char(I_order));
   fetch C_CHECK_COMPLEX_ORDER into L_dummy;
   SQL_LIB.SET_MARK('CLOSE', 
                    'C_CHECK_COMPLEX_ORDER', 
                    'ORDLOC', 
                    'Order no: '||to_char(I_order));
   close C_CHECK_COMPLEX_ORDER;   
   O_exists := (L_dummy is NOT NULL);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END PARTIAL_NONE_RCVD_ORD;
-------------------------------------------------------------------
FUNCTION FREIGHT_TERMS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_code               IN     VARCHAR2,
                       O_desc               IN OUT VARCHAR2)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.FREIGHT_TERMS';
   L_vdate PERIOD.VDATE%TYPE := GET_VDATE;
   L_dummy     VARCHAR2(1)   := NULL;

   cursor C_ATTRIB is
      select term_desc
        from v_freight_terms_tl 
       where freight_terms = I_code;

   cursor C_CHECK_INACTIVE is
      select 'x'
        from freight_terms
       where UPPER(freight_terms) = UPPER(I_code)
         and (NVL(start_date_active, to_date('00010101', 'YYYYMMDD')) > L_vdate
          or NVL(end_date_active, to_date('99990101', 'YYYYMMDD')) < L_vdate
          or enabled_flag = 'N');

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_ATTRIB','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
   open C_ATTRIB;
   SQL_LIB.SET_MARK('FETCH','C_ATTRIB','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
   fetch C_ATTRIB into O_desc;
   ---
   if C_ATTRIB%NOTFOUND then
      O_error_message := sql_lib.create_msg('INV_FREIGHT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
      close C_ATTRIB;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ATTRIB','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
   close C_ATTRIB;
   ---
   SQL_LIB.SET_MARK('OPEN','C_CHECK_INACTIVE','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
   open C_CHECK_INACTIVE;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_INACTIVE','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
   fetch C_CHECK_INACTIVE into L_dummy;
   --- check for inactive terms based on start and end dates and enabled flag
   if C_CHECK_INACTIVE%FOUND then
      O_error_message := sql_lib.create_msg('INACTIVE_FREIGHT',NULL,NULL,NULL);
      SQL_LIB.SET_MARK('CLOSE','C_CHECK_INACTIVE','FREIGHT_TERMS','FREIGHT_TERMS: '||I_code);
      close C_CHECK_INACTIVE;
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_CHECK_INACTIVE','TERMS','FREIGHT_TERMS: '||I_code);
   close C_CHECK_INACTIVE; 
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM, 
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END FREIGHT_TERMS;
--------------------------------------------------------------------------------------------------
FUNCTION ORDER_LOCATION_EXISTS (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists          IN OUT   BOOLEAN,
                                I_order           IN       ORDLOC.ORDER_NO%TYPE,
                                I_location        IN       ORDLOC.LOCATION%TYPE,
                                I_loc_type        IN       ORDLOC.LOC_TYPE%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_ATTRIB_SQL.ORDER_LOCATION_EXISTS';
   L_dummy     VARCHAR2(1);

   cursor C_STORE_EXISTS is
      select 'Y'
        from ordloc ol,
             store st
       where ol.location = st.store
         and ol.order_no = I_order
         and st.store    = I_location
         and st.stockholding_ind = 'Y';

   cursor C_WH_EXISTS is
      select 'Y'
        from ordloc ol,
             wh wh
       where ol.location    = wh.wh
         and ol.order_no    = I_order
         and wh.physical_wh = I_location;

BEGIN

   if I_order is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_order',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   ---
   if I_loc_type is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_loc_type',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                           'I_location',
                                           L_program,
                                           'NULL');
      return FALSE;
   end if;

   O_exists := FALSE;
   
   if I_loc_type = 'S' then
      open C_STORE_EXISTS;
      fetch C_STORE_EXISTS into L_dummy;

      if C_STORE_EXISTS%FOUND then
         O_exists := TRUE;
      end if;
      close C_STORE_EXISTS;
   elsif I_loc_type = 'W' then
      open C_WH_EXISTS;
      fetch C_WH_EXISTS into L_dummy;

      if C_WH_EXISTS%FOUND then
         O_exists := TRUE;
      end if;      
      close C_WH_EXISTS;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END ORDER_LOCATION_EXISTS;
--------------------------------------------------------------------------------------------------
FUNCTION CUSTOMER_LOCS_EXISTS (O_error_message    IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cust_loc_exits   IN OUT   BOOLEAN,
                               I_order_no         IN       ORDLOC.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program     VARCHAR2(50) := 'ORDER_ATTRIB_SQL.CUSTOMER_LOCS_EXISTS';
   L_wf_ord_no  TSFHEAD.WF_ORDER_NO%TYPE;


   cursor C_CUST_LOC_EXISTS is
      select th.wf_order_no
        from tsfhead th, tsfdetail td, ordloc ol
       where th.tsf_no          =  td.tsf_no
         and td.tsf_po_link_no  =  ol.tsf_po_link_no
         and ol.order_no        =  I_order_no;
  
BEGIN
   O_cust_loc_exits := FALSE;

   open C_CUST_LOC_EXISTS;

   fetch C_CUST_LOC_EXISTS into L_wf_ord_no; 

   close C_CUST_LOC_EXISTS;

   if L_wf_ord_no is NOT NULL then
      O_cust_loc_exits := TRUE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;

END CUSTOMER_LOCS_EXISTS;
--------------------------------------------------------------------------------------------------
FUNCTION GET_CLEARING_ZONE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_clearing_zone_id  IN OUT ORDHEAD.CLEARING_ZONE_ID%TYPE,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(64) := 'ORDER_ATTRIB_SQL.GET_CLEARING_ZONE';
   L_exists     BOOLEAN      := TRUE;
   ---
   cursor C_ORDHEAD is
      select clearing_zone_id
        from ordhead
       where order_no = I_order_no;
BEGIN
   O_clearing_zone_id := NULL;
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_ORDHEAD',
                    'ORDHEAD',
                    'Order no: '||to_char(I_order_no));
   open C_ORDHEAD;
   
   SQL_LIB.SET_MARK('FETCH',
                    'C_ORDHEAD',
                    'ORDHEAD',
                    'Order no: '||to_char(I_order_no));
   fetch C_ORDHEAD into O_clearing_zone_id;
   --
   if C_ORDHEAD%NOTFOUND then
      L_exists := FALSE;
   end if;
   --
   SQL_LIB.SET_MARK('CLOSE',
                    'C_ORDHEAD',
                    'ORDHEAD',
                    'Order no: '||to_char(I_order_no));
   close C_ORDHEAD;
   ---
   if L_exists = FALSE then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_CLEARING_ZONE;
--------------------------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_CLEARING_ZONE_ID(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_clearing_zone_id  IN OUT ORDHEAD.CLEARING_ZONE_ID%TYPE,
                                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                      I_import_country_id IN     ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_program                  VARCHAR2(64) := 'ORDER_ATTRIB_SQL.GET_DEFAULT_CLEARING_ZONE_ID';
   L_exists                   BOOLEAN;
   L_ordsku_count             NUMBER;
   L_prim_clearing_zone_id    OUTLOC.OUTLOC_ID%TYPE;
   L_outloc_desc              OUTLOC.OUTLOC_DESC%TYPE;
   ---
   cursor C_ORDSKU_COUNT is
      select count(1)
        from ordsku
       where order_no = I_order_no;

   cursor C_CLEARING_ZONE is
      select clearing_zone_id
        from item_hts ih,
             ordsku osk
       where osk.order_no = I_order_no
         and ih.item = osk.item
         and ih.import_country_id = I_import_country_id
       group by clearing_zone_id
      having count(distinct ih.item) = L_ordsku_count;
BEGIN
   O_clearing_zone_id := NULL;
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_import_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_import_country_id',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- Check if the import country is associated to any clearing zones
   if OUTSIDE_LOCATION_SQL.GET_PRIM_CLEAR_ZONE_IMP_CTRY(O_error_message,
                                                        L_exists,
                                                        L_prim_clearing_zone_id,
                                                        L_outloc_desc,
                                                        I_import_country_id) = FALSE then
      return FALSE;
   end if;
   ---
   if L_exists = TRUE then
      open  C_ORDSKU_COUNT;
      fetch C_ORDSKU_COUNT into L_ordsku_count;
      close C_ORDSKU_COUNT;
      ---
      open  C_CLEARING_ZONE;
      fetch C_CLEARING_ZONE into O_clearing_zone_id;
      close C_CLEARING_ZONE;
      ---
      if O_clearing_zone_id is NULL then
         O_clearing_zone_id := L_prim_clearing_zone_id;
      end if;
   end if;
   ---   
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_DEFAULT_CLEARING_ZONE_ID;
--------------------------------------------------------------------------------------------------
FUNCTION MULTIPLE_LOC_TYPE_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  O_wh_exists            OUT BOOLEAN,
                                  O_loc_exists           OUT BOOLEAN,
                                  I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS
   
   L_program                  VARCHAR2(64) := 'ORDER_ATTRIB_SQL.MULTIPLE_LOC_TYPE_EXISTS';
   L_loc_exists               VARCHAR2(1)  := 'N';
   L_wh_exists                VARCHAR2(1)  := 'N';
   
   cursor C_COUNT_WH is
      select 'X'
        from ordloc
       where order_no = I_order_no
         and loc_type = 'W';
       
   cursor C_LOC_EXISTS is
      select 'X'
        from ordloc
       where order_no = I_order_no;
   
BEGIN
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_COUNT_WH',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   open C_COUNT_WH;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_COUNT_WH',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   fetch C_COUNT_WH into L_wh_exists;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_COUNT_WH',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   close C_COUNT_WH;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOC_EXISTS',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   open C_LOC_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOC_EXISTS',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   fetch C_LOC_EXISTS into L_loc_exists;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOC_EXISTS',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   close C_LOC_EXISTS;
   ---
   
   if L_loc_exists = 'X' then
      O_loc_exists := TRUE;
   else
      O_loc_exists := FALSE;
   end if;
   
   if L_wh_exists = 'X' then
      O_wh_exists := TRUE;
   else
      O_wh_exists := FALSE;
   end if;

   return TRUE;
      
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;   

END MULTIPLE_LOC_TYPE_EXISTS;
-------------------------------------------------------------------------------------------------- 
FUNCTION CHECK_SUPP_LOC_OU(O_error_message  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_same_ou        IN OUT   BOOLEAN,
                           I_order_no       IN       ORDHEAD.ORDER_NO%TYPE,
                           I_supplier       IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN IS

   L_program        VARCHAR2(2000) := 'ORDER_ATTRIB_SQL.CHECK_SUPP_LOC_OU';
   L_exists         VARCHAR2(1) := NULL;

   cursor C_LOC_EXISTS is
    select 'X'
      from ordloc ol, 
           store st
     where order_no = I_order_no
       and ol.loc_type = 'S'
       and st.store = ol.location
       and st.org_unit_id not in (select org_unit_id from partner_org_unit where partner = I_supplier)
   UNION ALL
   select 'X'
      from ordloc ol,
           wh
     where order_no = I_order_no
       and ol.loc_type = 'W'
       and wh.wh = ol.location
       and wh.org_unit_id not in (select org_unit_id from partner_org_unit where partner = I_supplier);

BEGIN
   
   SQL_LIB.SET_MARK('OPEN',
                    'C_LOC_EXISTS',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   open C_LOC_EXISTS;
   ---
   SQL_LIB.SET_MARK('FETCH',
                    'C_LOC_EXISTS',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   fetch C_LOC_EXISTS into L_exists;
   ---
   SQL_LIB.SET_MARK('CLOSE',
                    'C_LOC_EXISTS',
                    'ORDLOC',
                    'Order no: '||to_char(I_order_no));
   close C_LOC_EXISTS;
   ---
                      
   if L_exists = 'X' then
      O_same_ou := FALSE;
   else
      O_same_ou := TRUE;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE; 
END CHECK_SUPP_LOC_OU;      
----------------------------------------------------------------------------------------------------------------------------
FUNCTION GET_PURCHASE_TYPE(O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                           O_purchase_type   IN OUT  ORDHEAD.PURCHASE_TYPE%TYPE,
                           I_order_no        IN      ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_function  VARCHAR2(50)  := 'ORDER_ATTRIB_SQL.GET_PURCHASE_TYPE';

   cursor C_ORDER_TYPE is
      select purchase_type
        from ordhead
       where order_no = I_order_no;

BEGIN

   open C_ORDER_TYPE;
   fetch C_ORDER_TYPE into O_purchase_type;
   close C_ORDER_TYPE;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_function,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_PURCHASE_TYPE;
--------------------------------------------------------------------------------------------------
FUNCTION GET_ORDER_INFO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_order_type      IN OUT   ORDHEAD.PURCHASE_TYPE%TYPE,
                        O_status          IN OUT   ORDHEAD.STATUS%TYPE,
                        O_contract_no     IN OUT   ORDHEAD.CONTRACT_NO%TYPE,
                        O_dept            IN OUT   ORDHEAD.DEPT%TYPE,
                        O_loc_type        IN OUT   ORDHEAD.LOC_TYPE%TYPE,
                        O_location        IN OUT   ORDHEAD.LOCATION%TYPE,
                        O_loc_ind         IN OUT   ORDHEAD.LOC_TYPE%TYPE,
                        I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_ATTRIB_SQL.GET_ORDER_INFO';

   cursor C_GET_ORDER_INFO is
      select o.order_type, 
             o.status,
             o.contract_no,
             o.dept,
             o.loc_type,
             o.location,
             decode(wh.stockholding_ind,'Y','V','P') loc_ind
        from ordhead o,
             wh
       where o.order_no = I_order_no
         and wh.wh      = o.location
         and o.loc_type = 'W'
       union all
      select o.order_type,
             o.status,
             o.contract_no,
             o.dept,
             o.loc_type,
             o.location,
             'S' loc_ind
        from ordhead o,
             store s
       where o.order_no = I_order_no
         and s.store    = o.location
         and o.loc_type = 'S'
       union all
      select o.order_type,
             o.status, 
             o.contract_no,
             o.dept,
             o.loc_type,
             o.location,
             'N' loc_ind
        from ordhead o
       where o.order_no = I_order_no
         and o.loc_type is NULL;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_GET_ORDER_INFO;
   fetch C_GET_ORDER_INFO into O_order_type,
                               O_status,
                               O_contract_no,
                               O_dept,
                               O_loc_type,
                               O_location,
                               O_loc_ind;
   close C_GET_ORDER_INFO;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_ORDER_INFO;
--------------------------------------------------------------------------------------------------
FUNCTION ORDLOC_WKSHT_EXIST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists          IN OUT   VARCHAR2,
                            I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'ORDER_ATTRIB_SQL.ORDLOC_WKSHT_EXIST';

   cursor C_ORDLOC_WKSHT_EXIST is
      select 'x'
        from ordloc_wksht
       where order_no = I_order_no
         and rownum = 1;

BEGIN

   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order_no',
                                            L_program,
                                            NULL);
      return FALSE;
   end if;
   ---
   open C_ORDLOC_WKSHT_EXIST;
   fetch C_ORDLOC_WKSHT_EXIST into O_exists;
   close C_ORDLOC_WKSHT_EXIST;
   ---
   return TRUE; 

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', 
                                            SQLERRM, 
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ORDLOC_WKSHT_EXIST;
--------------------------------------------------------------------------------------------------
END ORDER_ATTRIB_SQL;
/
