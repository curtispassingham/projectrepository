
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE PO_DIFF_MATRIX_SQL AUTHID CURRENT_USER AS

TYPE diff_x_ratio_record IS RECORD(diff_x_id     DIFF_IDS.DIFF_ID%TYPE,
                                   seq_no        DIFF_X_TEMP.SEQ_NO%TYPE,
                                   ratio_value   DIFF_RATIO_DETAIL.PCT%TYPE);

TYPE diff_x_ratio_tab IS TABLE OF diff_x_ratio_record INDEX BY BINARY_INTEGER;

-----------------------------------------------------------------------------
-- FUNCTION: EXPLODE_PROMPTS
-- PURPOSE:  This function takes the XY head info (podiffmx.fmb) and blows the 
--           id/group/range/ratio down to the id level.  The Y diff ids are stored 
--           on po_diff_matrix_temp, the X diff ids are returned as a plsql table.
--           The diff ind would be an I (id),G (group), or R (range)
--           the diff grouping value would be the id, group or associated range group 
--           (diff_range_head.diff_group_ 1, 2, or 3) depending on the corresponding diff ind
-----------------------------------------------------------------------------
FUNCTION EXPLODE_PROMPTS(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                         I_x_diff_ind            IN     VARCHAR2,
                         I_x_diff_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE, 
                         I_x_diff_grouping_value IN     VARCHAR2,
                         I_x_range_group_no      IN     INTEGER,
                         I_y_diff_ind            IN     VARCHAR2,
                         I_y_diff_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                         I_y_diff_grouping_value IN     VARCHAR2,
                         I_y_range_group_no      IN     INTEGER,
                         I_z_diff_ind            IN     VARCHAR2,
                         I_z_diff_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,
                         I_z_diff_grouping_value IN     VARCHAR2,
                         I_z_range_group_no      IN     INTEGER)

   return BOOLEAN;
-----------------------------------------------------------------------------
-- FUNCTION: POPULATE_DIFF_Z
-- PURPOSE:  This function takes Z head info on podifffmx.fmb 
--           and blows the id/group/range/ratio down to the diff_id level.  
--           The list of diff ids are stored on the diff_z_temp table.
--
-----------------------------------------------------------------------
FUNCTION POPULATE_DIFF_Z(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_diff_ind              IN     VARCHAR2,
                         I_diff_z_group_id       IN     DIFF_GROUP_DETAIL.DIFF_GROUP_ID%TYPE,   
                         I_diff_grouping_value   IN     VARCHAR2,
                         I_range_group_no        IN   VARCHAR2)
  return BOOLEAN;
----------------------------------------------------------------------- 
-- FUNCTION: DISTRIBUTE_TO_DIFF
-- PURPOSE:  This function takes the po_matrix_temp, plssql X_table, and diff_z_temp tables 
--           and blows the records out 1 for each possible combination of diffs x, y, and z
--           (a cartisean product may result, this is valid here) and converting the 
--          the value into Quantity terms depending on the matrix(z) qty type ('Q'uantity,
--         'R'atio, or 'P'ercent) 
--         following example uses quantity type of 'P'ercent for both matrix and z and 
--            a total ordered quatity of 100:
--            X_TABLE:
--            red
--           blue
--            
--             DIFF_Y  VALUE1  VALUE2 [ VALUE(3-30) NULL ]
--            size sm      5      10  
--            size m      20      15
--            size lg     25      25    
--             
--           DIFF_Z_TEMP:
--            DIFF_Z     VALUE
--            cotton      50
--            burlap      50
--    this function would place the following combinations on the diff_dist_matrix table:
--           DIFF_X    DIFF_Y   DIFF_Z    QTY  
--           size sm    red    cotton      2.5
--           size sm    red    burlap      2.5
--           size sm   blue    cotton      5  
--           size sm   blue    burlap      5
--           size m    red     cotton      10 
--           size m    red     burlap      10
--           size m    blue    cotton      7.5
--           size m    blue    burlap      7.5
--           size lg   red     cotton      12.5
--           size lg   red     burlap      12.5
--           size lg   blue    cotton      12.5
--           size lg   blue    burlap      12.5
--           
--
-----------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TO_DIFF(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_matrix_total     IN     NUMBER,
                            I_matrix_qty_type  IN     VARCHAR2,
                            I_z_total          IN     NUMBER,
                            I_z_qty_type       IN     VARCHAR2,
                            I_z_axis_populated IN    BOOLEAN)
   return BOOLEAN;
------------------------------------------------------------------------
-- FUNCTION: DISTRIBUTE_TO_ORD_ITEMS
-- PURPOSE:  This function  takes the diff/qty combinations from diff_dist_matrix
--           and blows values out to the item level (populating ordloc_wksht) 
-----------------------------------------------------------------------------
FUNCTION DISTRIBUTE_TO_ORD_ITEMS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_diff_x_no     IN     INTEGER,
                                 I_diff_y_no     IN     INTEGER,
                                 I_diff_z_no     IN     INTEGER,
                                 I_dist_type     IN     VARCHAR2,
                                 I_dist_uom_type IN     VARCHAR2,
                                 I_dist_uom      IN     UOM_CLASS.UOM%TYPE,
                                 I_uop           IN     ORDLOC_WKSHT.UOP%TYPE,
                                 I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                                 I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
-- FUNCTION: UPDATE_ORDER_DIFF
-- PURPOSE:  This function will update filtered-out records in ordmtxws.fmb that 
--           (on ordloc_wksht) that have diff combinations that match those in the
--           po diff matrix (on diff_dist_matrix).
-----------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_DIFF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no      IN     ORDLOC_WKSHT.ORDER_NO%TYPE,
                           I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                           I_dist_type     IN     VARCHAR2,
                           I_dist_uom_type IN     VARCHAR2,
                           I_dist_uom      IN     VARCHAR2,
                           I_uop           IN     ORDLOC_WKSHT.UOP%TYPE)
  return BOOLEAN;
------------------------------------------------------------------------
-- FUNCTION: REDIST_TO_ORD_ITEMS
-- PURPOSE:  This function will insert records into ordloc_wksht after a
--           redistribution has taken place in the po diff matrix. 
-----------------------------------------------------------------------------
FUNCTION REDIST_TO_ORD_ITEMS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_diff_x_no         IN     INTEGER,
                             I_diff_y_no         IN     INTEGER,
                             I_diff_z_no         IN     INTEGER,
                             I_dist_uom_type     IN     VARCHAR2,
                             I_dist_uom          IN     UOM_CLASS.UOM%TYPE,
                             I_standard_uom      IN     ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                             I_origin_country_id IN     ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,                         
                             I_uop               IN     ORDLOC_WKSHT.UOP%TYPE,
                             I_supp_pack_size    IN     ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE,
                             I_item_parent       IN     ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                             I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
------------------------------------------------------------------------
-- FUNCTION: UPDATE_REDIST_ORDER_DIFF
-- PURPOSE:  This function will update filtered-out records in ordmtxws.fmb
--           (on ordloc_wksht) that have diff combinations that match those
--           created during redistribution in the po diff matrix (on diff_dist_matrix).
-----------------------------------------------------------------------------
FUNCTION UPDATE_REDIST_ORDER_DIFF(O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE, 
                                  O_diff_1_statement      IN OUT  VARCHAR2, 
                                  O_diff_2_statement      IN OUT  VARCHAR2,
                                  O_diff_3_statement      IN OUT  VARCHAR2,
                                  O_diff_4_statement      IN OUT  VARCHAR2,
                                  O_diff_1_undist_id_ind  IN OUT  VARCHAR2,
                                  O_diff_2_undist_id_ind  IN OUT  VARCHAR2,
                                  O_diff_3_undist_id_ind  IN OUT  VARCHAR2,
                                  O_diff_4_undist_id_ind  IN OUT  VARCHAR2,
                                  I_diff_x_no             IN      INTEGER,
                                  I_diff_y_no             IN      INTEGER,
                                  I_diff_z_no             IN      INTEGER,
                                  I_item_parent           IN      ORDLOC_WKSHT.ITEM_PARENT%TYPE, 
                                  I_order_no              IN      ORDLOC_WKSHT.ORDER_NO%TYPE,
                                  I_dist_uom_type         IN      VARCHAR2,
                                  I_dist_uom              IN      VARCHAR2,
                                  I_uop                   IN      ORDLOC_WKSHT.UOP%TYPE)

RETURN BOOLEAN;
------------------------------------------------------------------------
--- FUNCTION: VALIDATE_COPY_DIFF
--- PURPOSE:  This function ensures the passed in diff is a valid diff z
---           value for copying. 
------------------------------------------------------------------------
FUNCTION VALIDATE_COPY_DIFF(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_valid         IN OUT BOOLEAN,
                            O_diff_desc     IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                            I_diff_id       IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN; 
-----------------------------------------------------------------------------
--- FUNCTION: COPY_DIFF_Z_MATRIX
--- PURPOSE: This function copies the corresponding matrix from the passed in
---          diff z value (I_copy_from_diff) to all matrices associated diff_z
---          that have a copy_ind = 'Y'es
-----------------------------------------------------------------------------
FUNCTION COPY_DIFF_Z_MATRIX(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_copy_from_diff    IN     DIFF_IDS.DIFF_ID%TYPE)
RETURN BOOLEAN; 
-----------------------------------------------------------------------------
-- FUNCTION : CONVERT_QTYS_BY_TYPE
-- PURPOSE : This function takes ratios or percentages and 
--           blows them out to order quantities.
-----------------------------------------------------------------------------
FUNCTION CONVERT_QTYS_BY_TYPE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_matrix_qty_type  IN     VARCHAR2,
                              I_z_total          IN     NUMBER,
                              I_z_qty_type       IN     VARCHAR2,
                              I_parent_total_qty IN     NUMBER,
                              I_qty_ind          IN     VARCHAR2)
  return BOOLEAN;
-----------------------------------------------------------------------------
-- FUNCTION : REPOP_MATRIX
-- PURPOSE :  this function repopulates the po diff matrix using diff/qty 
--            combinations from ordloc_wksht (ordmtxws.fmb).
-----------------------------------------------------------------------------
FUNCTION REPOP_MATRIX(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                      I_diff_x_no             IN     NUMBER,
                      I_diff_y_no             IN     NUMBER,
                      I_diff_z_no             IN     NUMBER,
                      I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item_parent           IN     ITEM_MASTER.ITEM%TYPE,
                      I_where_clause          IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                      I_dist_uom_type         IN     VARCHAR2)
   return BOOLEAN;
-----------------------------------------------------------------------------
-- FUNCTION : CLEAR_WKSHT_POST_REDIST
-- PURPOSE :  this function will delete all records from ordloc_wksht that 
--            have just been redistributed in the po diff matrix.
-----------------------------------------------------------------------------
FUNCTION CLEAR_WKSHT_POST_REDIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_standard_uom      IN OUT ORDLOC_WKSHT.STANDARD_UOM%TYPE,
                                 O_origin_country_id IN OUT ORDLOC_WKSHT.ORIGIN_COUNTRY_ID%TYPE,
                                 O_supp_pack_size    IN OUT ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE,
                                 I_item_parent     IN     ITEM_MASTER.ITEM%TYPE,
                                 I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                                 I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN;
-----------------------------------------------------------------------------
-- FUNCTION : APPLY_RATIO
-- PURPOSE : This function takes the ratio information from the form an applies
--           the ratio values to the matrix.  if the ratio applies to only the x
--           axis it passes out a plsql table containing the ratio values.
-----------------------------------------------------------------------------
FUNCTION APPLY_RATIO(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_diff_x_ratio       IN OUT DIFF_X_RATIO_TAB,
                     I_diff_ratio_id      IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE,
                     I_store              IN     DIFF_RATIO_DETAIL.STORE%TYPE,
                     I_diff_x_no          IN     NUMBER,
                     I_diff_y_no          IN     NUMBER,
                     I_diff_z_no          IN     NUMBER,
                     I_diff_z_id          IN     DIFF_IDS.DIFF_ID%TYPE,
                     I_range_x_no         IN     NUMBER,
                     I_range_y_no         IN     NUMBER,
                     I_range_z_no         IN     NUMBER,
                     I_range_id           IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : REFRESH_MATRIX
-- PURPOSE : This function will clear out the values of the currently display matrix
--            on podiffmx.fmb
-------------------------------------------------------------------------------
FUNCTION REFRESH_MATRIX (O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_diff_z         IN     DIFF_Z_TEMP.DIFF_Z%TYPE,
                         I_x_count        IN     NUMBER)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : DELETE_TEMP_AND_WKSHT
-- PURPOSE : This function will be called upon exiting the po diff matrix form and
--           will purge the temp tables used by that form. It will also delete any
--           records on ordloc_wksht that were just distributed using the po diff
--           matrix form.
-------------------------------------------------------------------------------
FUNCTION DELETE_TEMP_AND_WKSHT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_diff_x_no     IN     INTEGER,
                               I_diff_y_no     IN     INTEGER,
                               I_diff_z_no     IN     INTEGER,
                               I_order_no      IN     ORDLOC_WKSHT.ORDER_NO%TYPE,
                               I_where_clause  IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : ORPHAN_VALUES_EXIST
-- PURPOSE : This function checks for two conditions in the po diff matrix form:
--           1: if a value (qty, ratio, pct) has been entered into the x/y matrix
--              and the corresponding value on the z axis is zero or NULL.
--           2: if a value (qty, ratio, pct) has been entered into the z axis and
--              no values greater than zero exist on the x/y matrix.
-------------------------------------------------------------------------------
FUNCTION ORPHAN_VALUES_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_exist         IN OUT BOOLEAN,
                             O_z_seq_no      IN OUT DIFF_Z_TEMP.SEQ_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : EMPTY_MATRIX
-- PURPOSE : This function will determine if no values have been entered into the
--           x/y matrix.
-------------------------------------------------------------------------------
FUNCTION EMPTY_MATRIX(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists        IN OUT BOOLEAN)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : CHECK_PCT_TOTAL
-- PURPOSE : This function will determine if any x/y matrices do not add up 
--           to a total of 100. This function will only be called from the 
--           po diff matrix (podiffmx.fmb) when the x/y matrix is being distributed
--           by percent and there is a z diff. 
-------------------------------------------------------------------------------
FUNCTION CHECK_PCT_TOTAL(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_inv_total_exist IN OUT BOOLEAN,
                         O_z_seq_no        IN OUT DIFF_Z_TEMP.SEQ_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
--- FUNCTION: COPY_DIFF_Z_MATRIX
--- PURPOSE: This function copies the corresponding matrix from the passed in
---          diff z value (I_copy_from_diff) to all matrices associated diff_z
---          that have a copy_ind = 'Y' also readjusts the over all diff_z sum to 100% 
---          in Percent mode.
-----------------------------------------------------------------------------
FUNCTION COPY_DIFF_Z_MATRIX(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_copy_from_diff    IN     DIFF_IDS.DIFF_ID%TYPE,
                            I_matrix_mode       IN     VARCHAR2)
RETURN BOOLEAN; 
-----------------------------------------------------------------------------
-- FUNCTION : CONVERT_TO_QTY
-- PURPOSE : This function takes Item parent qty and converts the matrix 
--           into quantities either from ratio/percentage
-----------------------------------------------------------------------------
FUNCTION CONVERT_TO_QTY(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_parent_total_qty IN     NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------
-- FUNCTION : NORMALIZE_PO_DIFF_MATRIX_TEMP
-- PURPOSE : This function adjusts the sum of the diff z to 100 and
--           normalize the PO_DIFF_MATRIX_TEMP table based on diff z value present in diff_z_temp table. 
-------------------------------------------------------------------------------

FUNCTION NORMALIZE_PO_DIFF_MATRIX_TEMP (O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_diff_z_populated_ind   IN     VARCHAR2)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
-- FUNCTION : ADJUST_MATRIX_DIST_VALUE
-- PURPOSE : This function readjusts the PO_DIFF_MATRIX_TEMP table for changed diff_x ,diff_y combination.
-------------------------------------------------------------------------------   
FUNCTION ADJUST_MATRIX_DIST_VALUE(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_matrix_mode      IN     VARCHAR2, 
                                  I_diff_z           IN     DIFF_Z_TEMP.DIFF_Z%TYPE,
                                  I_new_record_ind   IN     VARCHAR2,
                                  I_diff_z_old_value IN     DIFF_Z_TEMP.VALUE%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : GET_DIFF_Z_SUM
-- PURPOSE : This function  returns diff_z sum from the PO_DIFF_MATRIX_TEMP table.
------------------------------------------------------------------------------- 
FUNCTION GET_DIFF_Z_SUM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE, 
                        O_z_sum          OUT    DIFF_Z_TEMP.VALUE%TYPE,
                        I_diff_z         IN     DIFF_Z_TEMP.DIFF_Z%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : APPLY_DIFF_RATIO
-- PURPOSE : This function takes the ratio information from diff ratio detail table 
--           and updates the po_diff_matrix_temp.
--           It is called only for diff ratio associated for X/Y/Z or only Z.
--           When diff ratio associated for X/Y/Z then the whole PO_DIFF_MATRIX_TEMP table 
--           populated based on diff_ratio_detail table. 
--           When the diff ratio associated only for Z then it is same as copy_diff_z functionality.
------------------------------------------------------------------------------- 
FUNCTION APPLY_DIFF_RATIO(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_diff_ratio_id   IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE, 
                          I_store           IN     DIFF_RATIO_DETAIL.STORE%TYPE,
                          I_diff_x_no       IN     NUMBER,
                          I_diff_y_no       IN     NUMBER,
                          I_diff_z_no       IN     NUMBER,
                          I_diff_z_id       IN     DIFF_IDS.DIFF_ID%TYPE,
                          I_matrix_mode     IN     VARCHAR2,
                          I_range_x_no      IN     NUMBER,
                          I_range_y_no      IN     NUMBER,
                          I_range_z_no      IN     NUMBER,
                          I_range_id        IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE)
RETURN BOOLEAN;
   
-------------------------------------------------------------------------------
-- FUNCTION : APPLY_RATIO_TO_XY_DISTRIBUTION
-- PURPOSE : This function takes the ratio information from diff ratio detail table 
--           and updates the po_diff_matrix_temp.
--           It is called only for diff ratios associated for X/Y or X or Y.
--           When the diff ratio associated only for X or Y then the data from 
--           diff_ratio_detail is copied to XY_DIFF_RATIO_TEMP.
--           When the diff ratio associated for both for X and Y ,the data from diff_ratio_detail
--           is copied to PO_DIFF_MATRIX_TEMP table for the selected diff_z.
------------------------------------------------------------------------------- 
FUNCTION APPLY_RATIO_TO_XY_DISTRIBUTION(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_diff_ratio_id   IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE, 
                                        I_store           IN     DIFF_RATIO_DETAIL.STORE%TYPE,
                                        I_diff_z_id       IN     DIFF_Z_TEMP.DIFF_Z%TYPE,
                                        I_diff_x_no       IN     NUMBER,
                                        I_diff_y_no       IN     NUMBER,
                                        I_diff_z_no       IN     NUMBER, 
                                        I_range_id        IN     DIFF_RANGE_DETAIL.DIFF_RANGE%TYPE,
                                        I_range_x_no      IN     NUMBER,
                                        I_range_y_no      IN     NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- FUNCTION : IS_DIFF_ASSOCIATED_WITH_STORE
-- PURPOSE : This function returns TRUE is the given Diff is associated with Store else it returns FALSE.
------------------------------------------------------------------------------- 
FUNCTION IS_DIFF_ASSOCIATED_WITH_STORE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_store_associated  OUT    BOOLEAN,
                                       I_diff_ratio_id     IN     DIFF_RATIO_HEAD.DIFF_RATIO_ID%TYPE)
RETURN BOOLEAN;
------------------------------------------------------------------------------- 
END;
/
