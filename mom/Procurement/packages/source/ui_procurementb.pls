CREATE OR REPLACE PACKAGE Body PROCUREMENT_SQL IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrcost (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF
  -- NEW ARGUMENTS (4)
  -- ITEM          :B_FILTER_HEAD.TI_ITEM.................. -> I_TI_ITEM
  -- ITEM          :B_FILTER_HEAD.TI_ITEM_GRANDPARENT...... -> I_TI_ITEM_GRANDPARENT
  -- ITEM          :B_FILTER_HEAD.TI_ITEM_PARENT........... -> I_TI_ITEM_PARENT
  -- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_CNTRCOST(O_error_message          in OUT   VARCHAR2,
                              I_TI_ITEM                in       VARCHAR2,
                              I_TI_ITEM_GRANDPARENT    in       VARCHAR2,
                              I_TI_ITEM_PARENT         in       VARCHAR2,
                              P_PM_CONTRACT            in       NUMBER,
                              O_diff_desc              in OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                              I_diff_id                in       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                              I_diff_no                in       NUMBER)
  return BOOLEAN
is
  L_diff_type       V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
  L_id_group_ind    V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
  L_valid           VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_DIFF_CNTRCOST';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  ---
  cursor C_DIFF is
     select 'Y'
       from contract_cost c
      where c.contract_no  = P_PM_CONTRACT
        and ( (c.diff_1 = I_diff_id
        and I_diff_no = 1)
         or(c.diff_2  = I_diff_id
        and I_diff_no = 2)
         or(c.diff_3  = I_diff_id
        and I_diff_no = 3)
         or(c.diff_4  = I_diff_id
        and I_diff_no = 4))
        and (I_TI_ITEM is NULL
         or c.item = I_TI_ITEM)
        and (I_TI_ITEM_PARENT is NULL
         or c.item_parent = I_TI_ITEM_PARENT)
        and (I_TI_ITEM_GRANDPARENT is NULL
         or c.item_grandparent = I_TI_ITEM_GRANDPARENT);
BEGIN
  if I_diff_id is NOT NULL then
    open C_DIFF;
    fetch C_DIFF into L_valid;
    close C_DIFF;
    if L_valid = 'N' then
       O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    else
      if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                O_diff_desc,
                                L_diff_type,
                                L_id_group_ind,
                                I_diff_id) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    end if;
  else
    O_diff_desc := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end FILTER_DIFF_CNTRCOST;
---------------------------------------------------------------------------------------------
-- Module                   : cntrcost (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF_GROUP
-- NEW ARGUMENTS (1)
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_GROUP_CNTRCOST(O_error_message     in OUT   VARCHAR2,
                                    P_PM_CONTRACT       in       NUMBER,
                                    O_diff_group_desc   in OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                    I_diff_group_id     in       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                                    I_diff_no           in       NUMBER)
  return BOOLEAN
is
  L_diff_type       V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
  L_id_group_ind    V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
  L_valid           VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_DIFF_GROUP_CNTRCOST';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  ---
  cursor C_DIFF is
    select 'Y'
      from contract_cost c,
           item_master im,
           diff_group_head dg
    where c.contract_no  = P_PM_CONTRACT
      and ( (c.diff_1 is NULL
      and I_diff_no = 1)
       or(c.diff_2  is NULL
      and I_diff_no = 2)
       or(c.diff_3  is NULL
      and I_diff_no = 3)
       or(c.diff_4  is NULL
      and I_diff_no = 4))
      and im.item   = NVL(c.item, NVL(c.item_parent, c.item_grandparent))
      and ( (im.diff_1 = dg.diff_group_id
      and I_diff_no = 1)
       or(im.diff_2 = dg.diff_group_id
      and I_diff_no = 2)
       or(im.diff_3 = dg.diff_group_id
      and I_diff_no = 3)
       or(im.diff_4 = dg.diff_group_id
      and I_diff_no = 4))
      and dg.diff_group_id = I_diff_group_id;
BEGIN
  IF I_diff_group_id is NOT NULL then
    open C_DIFF;
    fetch C_DIFF into L_valid;
    close C_DIFF;
    if L_valid  = 'N' then
       O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    else
      if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                O_diff_group_desc,
                                L_diff_type,
                                L_id_group_ind,
                                I_diff_group_id) = FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
    end if;
  else
     O_diff_group_desc := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end FILTER_DIFF_GROUP_CNTRCOST;
---------------------------------------------------------------------------------------------
-- Module                   : cntrdetl (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF
-- NEW ARGUMENTS (4)
-- ITEM          :B_FILTER_HEAD.TI_ITEM.................. -> I_TI_ITEM
-- ITEM          :B_FILTER_HEAD.TI_ITEM_GRANDPARENT...... -> I_TI_ITEM_GRANDPARENT
-- ITEM          :B_FILTER_HEAD.TI_ITEM_PARENT........... -> I_TI_ITEM_PARENT
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
-- Purpose:   Validate that the diff is a valid diff and populate the description field
-------------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_CNTRDETL(O_error_message         in OUT   VARCHAR2,
                              I_TI_ITEM               in       VARCHAR2,
                              I_TI_ITEM_GRANDPARENT   in       VARCHAR2,
                              I_TI_ITEM_PARENT        in       VARCHAR2,
                              P_PM_CONTRACT           in       NUMBER,
                              O_diff_desc             in OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                              I_diff_id               in       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                              I_diff_no               in       NUMBER)
  return BOOLEAN
is
  L_diff_type       V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
  L_id_group_ind    V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
  L_valid           VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_DIFF_CNTRDETL';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  ---
  cursor C_DIFF is
     select 'Y'
       from contract_detail c
      where c.contract_no = P_PM_CONTRACT
        and ( (c.diff_1  = I_diff_id
        and I_diff_no = 1)
         or(c.diff_2 = I_diff_id
        and I_diff_no = 2)
         or(c.diff_3 = I_diff_id
        and I_diff_no = 3)
         or(c.diff_4 = I_diff_id
        and I_diff_no = 4))
        and (I_TI_ITEM is NULL
         or c.item = I_TI_ITEM)
        and (I_TI_ITEM_PARENT is NULL
         or c.item_parent = I_TI_ITEM_PARENT)
        and (I_TI_ITEM_GRANDPARENT is NULL
         or c.item_grandparent = I_TI_ITEM_GRANDPARENT);
BEGIN
  if I_diff_id is NOT NULL then
    open C_DIFF;
    fetch C_DIFF into L_valid;
    close C_DIFF;
       if L_valid = 'N' then
          O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',
                                                NULL,
                                                NULL,
                                                NULL);
       return FALSE;
       else
         if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                   O_diff_desc,
                                   L_diff_type,
                                   L_id_group_ind,
                                   I_diff_id) = FALSE then
            O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                  NULL,
                                                  NULL,
                                                  NULL);
        return FALSE;
      end if;
    end if;
  else
    O_diff_desc := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end FILTER_DIFF_CNTRDETL;
---------------------------------------------------------------------------------------------
-- Module                   : cntrdetl (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF_GROUP
-- NEW ARGUMENTS (1)
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_GROUP_CNTRDETL(O_error_message     in OUT   VARCHAR2,
                                    P_PM_CONTRACT       in       NUMBER,
                                    O_diff_group_desc   in OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                    I_diff_group_id     in       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                                    I_diff_no           in       NUMBER)
  return BOOLEAN
is
  L_diff_type       V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
  L_id_group_ind    V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
  L_valid           VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_DIFF_GROUP_CNTRDETL';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  ---
  cursor C_DIFF is
     select 'Y'
       from contract_detail c,
            v_item_master im,
            diff_group_head dg
      where c.contract_no  = P_PM_CONTRACT
        and ( (c.diff_1 is NULL
        and I_diff_no = 1)
         or(c.diff_2 is NULL
        and I_diff_no = 2)
         or(c.diff_3 is NULL
        and I_diff_no = 3)
         or(c.diff_4 is NULL
        and I_diff_no = 4))
        and im.item = NVL(c.item, NVL(c.item_parent, c.item_grandparent))
        and ( (im.diff_1 = dg.diff_group_id
        and I_diff_no = 1)
         or(im.diff_2 = dg.diff_group_id
        and I_diff_no = 2)
         or(im.diff_3 = dg.diff_group_id
        and I_diff_no = 3)
         or(im.diff_4 = dg.diff_group_id
        and I_diff_no  = 4))
        and dg.diff_group_id = I_diff_group_id;
BEGIN
  if I_diff_group_id is NOT NULL then
    open C_DIFF;
    fetch C_DIFF into L_valid;
    close C_DIFF;
    if L_valid  = 'N' then
       O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    else
      if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                O_diff_group_desc,
                                L_diff_type,
                                L_id_group_ind,
                                I_diff_group_id) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    end if;
  else
    O_diff_group_desc := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end FILTER_DIFF_GROUP_CNTRDETL;
---------------------------------------------------------------------------------------------
-- Module                   : cntrdetl (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_LOCATION
-- NEW ARGUMENTS (7)
-- ITEM          :B_FILTER_HEAD.LI_LOC_TYPE.............. -> I_LI_LOC_TYPE
-- ITEM          :B_FILTER_HEAD.TI_ITEM.................. -> I_TI_ITEM
-- ITEM          :B_FILTER_HEAD.TI_ITEM_GRANDPARENT...... -> I_TI_ITEM_GRANDPARENT
-- ITEM          :B_FILTER_HEAD.TI_ITEM_PARENT........... -> I_TI_ITEM_PARENT
-- ITEM          :B_FILTER_HEAD.TI_LOCATION.............. -> I_TI_LOCATION
-- ITEM          :B_FILTER_HEAD.TI_LOC_DESC.............. -> I_TI_LOC_DESC
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
---------------------------------------------------------------------------------------------
FUNCTION FILTER_LOCATION_CNTRDETL(O_error_message         in OUT   VARCHAR2,
                                  I_LI_LOC_TYPE           in       VARCHAR2,
                                  I_TI_ITEM               in       VARCHAR2,
                                  I_TI_ITEM_GRANDPARENT   in       VARCHAR2,
                                  I_TI_ITEM_PARENT        in       VARCHAR2,
                                  I_TI_LOCATION           in       NUMBER,
                                  I_TI_LOC_DESC           in OUT   VARCHAR2,
                                  P_PM_CONTRACT           in       NUMBER)
  return BOOLEAN
is
  L_loc                VARCHAR2(1) := 'N';
  L_error_message      RTK_ERRORS.RTK_TEXT%TYPE;
  L_valid              BOOLEAN;
  L_stockholding_ind   V_STORE.STOCKHOLDING_IND%TYPE;
  L_program            VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_LOCATION_CNTRDETL';
  cursor C_LOC is
     select 'Y'
       from contract_detail
      where location = I_TI_LOCATION
        and loc_type = I_LI_LOC_TYPE
        and contract_no = P_PM_CONTRACT
        and (I_TI_ITEM is NULL
         or item = I_TI_ITEM)
        and (I_TI_ITEM_PARENT is NULL
         or item_parent = I_TI_ITEM_PARENT)
        and (I_TI_ITEM_GRANDPARENT is NULL
         or item_grandparent = I_TI_ITEM_GRANDPARENT);
BEGIN
  if I_TI_LOCATION    is NOT NULL then
    if I_LI_LOC_TYPE  is NULL then
       O_error_message := SQL_LIB.CREATE_MSG('ENT_LOC_TYPE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    ---
    if (FILTER_LOV_VALIDATE_SQL.VALIDATE_LOCATION(L_error_message,
                                                  L_valid,
                                                  I_TI_LOC_DESC,
                                                  L_stockholding_ind,
                                                  I_TI_LOCATION) = FALSE) or (L_valid = FALSE) then
       O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    ---
    open C_LOC;
    fetch C_LOC into L_loc;
    close C_LOC;
    if L_loc = 'N' then
       O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    if I_LI_LOC_TYPE = 'S' then
      if STORE_ATTRIB_SQL.GET_NAME(L_error_message,
                                   I_TI_LOCATION,
                                   I_TI_LOC_DESC) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    else
      if WH_ATTRIB_SQL.GET_NAME(L_error_message,
                                I_TI_LOCATION,
                                I_TI_LOC_DESC) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    end if;
  else
    I_TI_LOC_DESC := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end FILTER_LOCATION_CNTRDETL;
---------------------------------------------------------------------------------------------
-- Module                   : cntrdist (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF
-- NEW ARGUMENTS (4)
-- ITEM          :B_FILTER_HEAD.TI_ITEM.................. -> I_TI_ITEM
-- ITEM          :B_FILTER_HEAD.TI_ITEM_GRANDPARENT...... -> I_TI_ITEM_GRANDPARENT
-- ITEM          :B_FILTER_HEAD.TI_ITEM_PARENT........... -> I_TI_ITEM_PARENT
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_CNTRDIST(O_error_message         in OUT   VARCHAR2,
                              I_TI_ITEM               in       VARCHAR2 ,
                              I_TI_ITEM_GRANDPARENT   in       VARCHAR2,
                              I_TI_ITEM_PARENT        in       VARCHAR2,
                              P_PM_CONTRACT           in       NUMBER,
                              O_diff_desc             in OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                              I_diff_id               in       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                              I_diff_no               in       NUMBER)
  return BOOLEAN
is
  L_diff_type       V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
  L_id_group_ind    V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
  L_valid           VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_DIFF_CNTRDIST';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  ---
  cursor C_DIFF is
     select 'Y'
       from contract_matrix_temp c
      where c.contract_no = P_PM_CONTRACT
        and ( (c.diff_1 = I_diff_id
        and I_diff_no = 1)
         or(c.diff_2 = I_diff_id
        and I_diff_no = 2)
         or(c.diff_3 = I_diff_id
        and I_diff_no = 3)
         or(c.diff_4 = I_diff_id
        and I_diff_no = 4))
        and (I_TI_ITEM is NULL
         or c.item = I_TI_ITEM)
        and (I_TI_ITEM_PARENT is NULL
         or c.item_parent = I_TI_ITEM_PARENT)
        and (I_TI_ITEM_GRANDPARENT is NULL
         or c.item_grandparent = I_TI_ITEM_GRANDPARENT);
BEGIN
  if I_diff_id is NOT NULL then
    open C_DIFF;
    fetch C_DIFF into L_valid;
    close C_DIFF;
    if L_valid = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    else
      if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                O_diff_desc,
                                L_diff_type,
                                L_id_group_ind,
                                I_diff_id) = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
    end if;
  else
    O_diff_desc := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
  return FALSE;
end FILTER_DIFF_CNTRDIST;
---------------------------------------------------------------------------------------------
-- Module                   : cntrdist (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF_GROUP
-- NEW ARGUMENTS (1)
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
--------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_GROUP_CNTRDIST(O_error_message     in OUT   VARCHAR2,
                                    P_PM_CONTRACT       in       NUMBER,
                                    O_diff_group_desc   in OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
                                    I_diff_group_id     in       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
                                    I_diff_no           in       NUMBER)
  return BOOLEAN
is
  L_diff_type       V_DIFF_ID_GROUP_TYPE.DIFF_TYPE%TYPE;
  L_id_group_ind    V_DIFF_ID_GROUP_TYPE.ID_GROUP_IND%TYPE;
  L_valid           VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_DIFF_GROUP_CNTRDIST';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  ---
  cursor C_DIFF is
     select 'Y'
       from contract_matrix_temp c,
            item_master im,
            diff_group_head dg
      where c.contract_no  = P_PM_CONTRACT
        and c.item is NULL
        and ( (c.diff_1 iS NULL
        and I_diff_no = 1)
         or(c.diff_2 is NULL
        and I_diff_no = 2)
         or(c.diff_3 is NULL
        and I_diff_no = 3)
         or(c.diff_4 is NULL
        and I_diff_no = 4))
        and im.item = NVL(c.item, NVL(c.item_parent, c.item_grandparent))
        and ( (im.diff_1 = dg.diff_group_id
        and I_diff_no = 1)
         or(im.diff_2 = dg.diff_group_id
        and I_diff_no = 2)
         or(im.diff_3 = dg.diff_group_id
        and I_diff_no = 3)
         or(im.diff_4 = dg.diff_group_id
        and I_diff_no = 4))
        and dg.diff_group_id = I_diff_group_id;
BEGIN
  if I_diff_group_id is NOT NULL then
    open C_DIFF;
    fetch C_DIFF into L_valid;
    close C_DIFF;
    if L_valid = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_DIFF_ID',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    else
      if DIFF_SQL.GET_DIFF_INFO(L_error_message,
                                O_diff_group_desc,
                                L_diff_type,
                                L_id_group_ind,
                                I_diff_group_id) = FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
    end if;
  else
    O_diff_group_desc := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
  return FALSE;
end FILTER_DIFF_GROUP_CNTRDIST;
---------------------------------------------------------------------------------------------
-- Module                   : cntrdist (Form Module)
-- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_LOCATION
-- NEW ARGUMENTS (7)
-- ITEM          :B_FILTER_HEAD.LI_LOC_TYPE.............. -> I_LI_LOC_TYPE
-- ITEM          :B_FILTER_HEAD.TI_ITEM.................. -> I_TI_ITEM
-- ITEM          :B_FILTER_HEAD.TI_ITEM_GRANDPARENT...... -> I_TI_ITEM_GRANDPARENT
-- ITEM          :B_FILTER_HEAD.TI_ITEM_PARENT........... -> I_TI_ITEM_PARENT
-- ITEM          :B_FILTER_HEAD.TI_LOCATION.............. -> I_TI_LOCATION
-- ITEM          :B_FILTER_HEAD.TI_LOC_DESC.............. -> I_TI_LOC_DESC
-- PARAMETER     :PARAMETER.PM_CONTRACT.................. -> P_PM_CONTRACT
---------------------------------------------------------------------------------------------
FUNCTION FILTER_LOCATION_CNTRDIST(O_error_message         in OUT   VARCHAR2,
                                  I_LI_LOC_TYPE           in       VARCHAR2,
                                  I_TI_ITEM               in       VARCHAR2,
                                  I_TI_ITEM_GRANDPARENT   in       VARCHAR2,
                                  I_TI_ITEM_PARENT        in       VARCHAR2,
                                  I_TI_LOCATION           in       NUMBER,
                                  I_TI_LOC_DESC           in OUT   VARCHAR2,
                                  P_PM_CONTRACT           in       NUMBER)
  return BOOLEAN
is
  L_loc             VARCHAR2(1) := 'N';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_valid           BOOLEAN;
  L_wh              V_WH%ROWTYPE;
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.FILTER_LOCATION_CNTRDIST';
  cursor C_LOC is
     select 'Y'
       from contract_matrix_temp
      where location = I_TI_LOCATION
        and loc_type = I_LI_LOC_TYPE
        and contract_no = P_PM_CONTRACT
        and (I_TI_ITEM is NULL
         or item = I_TI_ITEM)
        and (I_TI_ITEM_PARENT is NULL
         or item_parent = I_TI_ITEM_PARENT)
        and (I_TI_ITEM_GRANDPARENT is NULL
         or item_grandparent = I_TI_ITEM_GRANDPARENT);
BEGIN
  if I_TI_LOCATION is NOT NULL then
    if I_LI_LOC_TYPE is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('ENT_LOC_TYPE',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;
    ---
    open C_LOC;
    fetch C_LOC into L_loc;
    close C_LOC;
    if L_loc = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LOC',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;
    if I_LI_LOC_TYPE = 'S' then
      if (FILTER_LOV_VALIDATE_SQL.VALIDATE_STORE(L_error_message,
                                                 L_valid,
                                                 I_TI_LOC_DESC,
                                                 I_TI_LOCATION) = FALSE) or (L_valid = FALSE) then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
    else
      if (FILTER_LOV_VALIDATE_SQL.VALIDATE_WH(L_error_message,
                                              L_valid,
                                              L_wh,
                                              I_TI_LOCATION) = FALSE) or (L_valid = FALSE) then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
      I_TI_LOC_DESC := L_wh.wh_name;
    end if;
  else
    I_TI_LOC_DESC := NULL;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end FILTER_LOCATION_CNTRDIST;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> ZN_FORM_VALIDATION.ZONE_ID
-- NEW ARGUMENTS (3)
-- ITEM          :B_CZONE.ZONE_ID........................ -> I_ZONE_ID
-- ITEM          :B_CZONE_HEAD.TI_ZONE_GROUP_ID.......... -> I_TI_ZONE_GROUP_ID
-- Pack.Spec     ZN_FORM_VALIDATION...................... -> P_ZnFrmVldtn
---------------------------------------------------------------------------------------------
FUNCTION ZONE_ID(O_error_message      in OUT   VARCHAR2,
                 I_ZONE_ID            in       NUMBER,
                 I_TI_ZONE_GROUP_ID   in       NUMBER,
                 P_ZnFrmVldtn         in OUT   VARCHAR2 )
  return BOOLEAN
is
  L_id             COST_ZONE.ZONE_ID%TYPE;
  L_program        VARCHAR2(64) := 'PROCUREMENT_SQL.ZONE_ID';
  L_current_item   VARCHAR2 (255);
  cursor C_COST_ZONE is
     select c.zone_id
       from cost_zone c
      where c.zone_group_id = I_TI_ZONE_GROUP_ID
        and c.zone_id = I_ZONE_ID;
BEGIN
  P_ZnFrmVldtn := 'B_CZONE.ZONE_ID';
  if I_ZONE_ID is NOT NULL then
    open C_COST_ZONE;
    fetch C_COST_ZONE into L_id;
    if C_COST_ZONE%FOUND then
      O_error_message := SQL_LIB.CREATE_MSG('ZONE_EXISTS',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_COST_ZONE;
      return FALSE;
    end if;
    close C_COST_ZONE;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end ZONE_ID;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> P_NUMB_LOC
-- NEW ARGUMENTS (3)
-- ITEM          :B_CZONE.TI_NUMB_OF_LOC................. -> I_TI_NUMB_OF_LOC
-- ITEM          :B_CZONE.ZONE_ID........................ -> I_ZONE_ID
-- ITEM          :B_CZONEGRP.ZONE_GROUP_ID............... -> I_ZONE_GROUP_ID
---------------------------------------------------------------------------------------------
FUNCTION NUMB_LOC(O_error_message    in OUT   VARCHAR2,
                  I_TI_NUMB_OF_LOC   in OUT   NUMBER,
                  I_ZONE_ID          in       NUMBER,
                  I_ZONE_GROUP_ID    in       NUMBER)
  return BOOLEAN
is
  L_number    NUMBER(6);
  L_program   VARCHAR2(64) := 'PROCUREMENT_SQL.NUMB_LOC';
  cursor C_NO_OF_LOCS is
     select SUM (s)
           from
             (select COUNT (*) s
                from cost_zone_group_loc
               where zone_group_id = I_ZONE_GROUP_ID
                 and zone_id = I_ZONE_ID
                 and loc_type = 'S'
     union all
    select DISTINCT COUNT (*) s
      from wh w,
           cost_zone_group_loc c
     where w.wh = c.location
       and w.physical_wh = w.wh
       and zone_group_id = I_ZONE_GROUP_ID
       and zone_id = I_ZONE_ID);
  BEGIN
    open C_NO_OF_LOCS;
    fetch C_NO_OF_LOCS into L_number;
    close C_NO_OF_LOCS;
    I_TI_NUMB_OF_LOC := L_number;
    return TRUE;
  EXCEPTION
  when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
    return FALSE;
  end;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> P_LIKE_GROUP
  -- NEW ARGUMENTS (4)
  -- GLOBAL        :GLOBAL.CURRENCY_CODE................... -> G_CURRENCY_CODE
  -- ITEM          :B_CZONEGRP.DESCRIPTION................. -> I_DESCRIPTION
  -- ITEM          :B_CZONEGRP.TI_LIKE_GROUP............... -> I_TI_LIKE_GROUP
  -- ITEM          :B_CZONEGRP.ZONE_GROUP_ID............... -> I_ZONE_GROUP_ID
  ---------------------------------------------------------------------------------------------
FUNCTION LIKE_GROUP_COSTZONE(O_error_message   in OUT   VARCHAR2,
                             G_CURRENCY_CODE   in       VARCHAR2,
                             I_DESCRIPTION     in       VARCHAR2,
                             I_TI_LIKE_GROUP   in       NUMBER,
                             I_ZONE_GROUP_ID   in       NUMBER)
  return BOOLEAN
is
  L_cost_level      COST_ZONE_GROUP.COST_LEVEL%TYPE;
  L_store_default   STORE.STORE%TYPE;
  L_wh_default      WH.WH%TYPE;
  L_exists          VARCHAR2(1) := 'N';
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.LIKE_GROUP_COSTZONE';
  cursor C_COST_LEVEL is
     select cost_level
       from cost_zone_group
      where zone_group_id = I_ZONE_GROUP_ID;
  cursor C_GET_ST_DEFAULT is
     select store
       from store
      where rownum = 1;
  cursor C_GET_WH_DEFAULT is
     select wh
       from wh
      where wh = physical_wh
        and rownum = 1;
  cursor C_COST_ZONE is
     select 'Y'
       from cost_zone
      where zone_group_id = I_ZONE_GROUP_ID;
BEGIN
  open C_COST_LEVEL;
  fetch C_COST_LEVEL into L_cost_level;
  close C_COST_LEVEL;
  ---
  if L_cost_level = 'C' then
    open C_COST_ZONE;
    fetch C_COST_ZONE into L_exists;
    close C_COST_ZONE;
    if L_exists = 'N' then
      ---
      insert into cost_zone(zone_group_id,
                            zone_id,
                            description,
                            currency_code,
                            base_cost_ind)
                     values(I_ZONE_GROUP_ID,
                            I_ZONE_GROUP_ID,
                            I_DESCRIPTION,
                            G_CURRENCY_CODE,
                            'Y');
      ---
      insert into cost_zone_group_loc(zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id)
      select I_ZONE_GROUP_ID,
             store,
             'S',
             I_ZONE_GROUP_ID
        from store;
      ---
      insert into cost_zone_group_loc(zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id)
      select I_ZONE_GROUP_ID,
             wh,
             'W',
             I_ZONE_GROUP_ID
        from wh;
    end if;
    ---
  elsif L_cost_level = 'L' then
    open C_COST_ZONE;
    fetch C_COST_ZONE into L_exists;
    close C_COST_ZONE;
    if L_exists = 'N' then
      insert into cost_zone(zone_group_id,
                            zone_id,
                            description,
                            currency_code,
                            base_cost_ind)
      select I_ZONE_GROUP_ID,
             store,
             store_name,
             currency_code,
             'N'
        from store;
      ---
      insert into cost_zone(zone_group_id,
                            zone_id,
                            description,
                            currency_code,
                            base_cost_ind)
      select I_ZONE_GROUP_ID,
             wh,
             wh_name,
             currency_code,
             'N'
        from wh
       where wh = physical_wh;
      ---
      insert into cost_zone_group_loc(zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id)
      select I_ZONE_GROUP_ID,
             store,
             'S',
             store
        from store;
      ---
      insert into cost_zone_group_loc(zone_group_id,
                                      location,
                                      loc_type,
                                      zone_id)
      select I_ZONE_GROUP_ID,
             wh,
             'W',
             physical_wh
        from wh;
      ---
      --- Set one location to base.
      open C_GET_ST_DEFAULT;
      fetch C_GET_ST_DEFAULT into L_store_default;
      if C_GET_ST_DEFAULT%NOTFOUND then
        ---
        open C_GET_WH_DEFAULT;
        fetch C_GET_WH_DEFAULT into L_wh_default;
        ---
        update cost_zone
           set base_cost_ind = 'Y'
         where zone_id = L_wh_default;
        ---
        close C_GET_WH_DEFAULT;
      else
        ---
        update cost_zone
           set base_cost_ind = 'Y'
         where zone_id = L_store_default;
        ---
      end if;
      close C_GET_ST_DEFAULT;
    end if;
    ---
  elsif L_cost_level = 'Z' and I_TI_LIKE_GROUP is NOT NULL then
    insert into cost_zone(zone_group_id,
                          zone_id,
                          description,
                          currency_code,
                          base_cost_ind)
    select I_ZONE_GROUP_ID,
           zone_id,
           description,
           currency_code,
           base_cost_ind
      from cost_zone
     where zone_group_id = I_TI_LIKE_GROUP;
    ---
    insert into cost_zone_group_loc(zone_group_id,
                                    location,
                                    loc_type,
                                    zone_id)
    select I_ZONE_GROUP_ID,
           location,
           loc_type,
           zone_id
      from cost_zone_group_loc
     where zone_group_id = I_TI_LIKE_GROUP;
    ---
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> P_INSERT_DAILY_PURGE
-- NEW ARGUMENTS (1)
-- ITEM          :B_CZONEGRP.ZONE_GROUP_ID............... -> I_ZONE_GROUP_ID
---------------------------------------------------------------------------------------------
FUNCTION INSERT_DAILY_PURGE(O_error_message   in OUT   VARCHAR2,
                            I_ZONE_GROUP_ID   in        NUMBER)
  return BOOLEAN
is
  L_dummy     VARCHAR2(1);
  L_program   VARCHAR2(64) := 'PROCUREMENT_SQL.INSERT_DAILY_PURGE';
  cursor C_REC_EXISTS is
    select 'x'
      from daily_purge
     where table_name = 'COST_ZONE_GROUP'
       and key_value = to_char(I_ZONE_GROUP_ID);
BEGIN
  -- this message informs the user that the record will be deleted in the
  -- nightly batch run.
  open C_REC_EXISTS;
  fetch C_REC_EXISTS into L_dummy;
  if C_REC_EXISTS%NOTFOUND then
    insert into daily_purge
         values(to_char(I_ZONE_GROUP_ID),
                'COST_ZONE_GROUP',
                'D',
                1);
  end if;
  close C_REC_EXISTS;
  return TRUE;
EXCEPTION
   when OTHERS then
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> LOC_FORM_VALIDATION.LOCATION
-- NEW ARGUMENTS (7)
-- ITEM          :B_CZONELOC.LOCATION.................... -> I_LOCATION
-- ITEM          :B_CZONELOC.LOC_TYPE.................... -> I_LOC_TYPE
-- ITEM          :B_CZONELOC.TI_LOC_NAME................. -> I_TI_LOC_NAME
-- ITEM          :B_CZONELOC_HEAD.TI_CURRENCY_CODE....... -> I_TI_CURRENCY_CODE
-- ITEM          :B_CZONELOC_HEAD.TI_ZONE_GROUP_ID....... -> I_TI_ZONE_GROUP_ID
-- ITEM          :B_CZONELOC_HEAD.TI_ZONE_ID............. -> I_TI_ZONE_ID
-- Pack.Spec     LOC_FORM_VALIDATION..................... -> P_LcFrmVldtn
---------------------------------------------------------------------------------------------
FUNCTION LOCATION(O_error_message      in OUT   VARCHAR2,
                  I_LOCATION           in OUT   NUMBER,
                  I_LOC_TYPE           in       VARCHAR2,
                  I_TI_LOC_NAME        in OUT   VARCHAR2,
                  I_TI_CURRENCY_CODE   in       VARCHAR2,
                  I_TI_ZONE_GROUP_ID   in       NUMBER,
                  I_TI_ZONE_ID         in       NUMBER,
                  P_LcFrmVldtn         in OUT   VARCHAR2)
  return BOOLEAN
is
  L_error_message   VARCHAR2(255);
  L_exists          BOOLEAN;
  L_program         VARCHAR2(64):= 'PROCUREMENT_SQL.LOCATION';
  cursor C_STORE is
     select vst.store_name
       from store st,
            v_store_tl vst
      where st.store = I_LOCATION
        and st.currency_code = I_TI_CURRENCY_CODE 
        and st.store = vst.store;
  cursor C_WH is
     select vw.wh_name
       from wh w, 
            v_wh_tl vw
      where w.wh = I_LOCATION
        and w.currency_code = I_TI_CURRENCY_CODE
        and w.wh = w.physical_wh 
        and w.wh = vw.wh;
BEGIN
  P_LcFrmVldtn := 'B_CZONELOC.LOCATION';
  ---
  if I_LOCATION is NOT NULL and I_LOC_TYPE is NOT NULL then
    ---
    if COST_ZONE_SQL.COST_ZONE_EXIST(L_error_message,
                                     L_exists,
                                     I_TI_ZONE_GROUP_ID,
                                     NULL, I_LOCATION) = FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
    end if;
    ---
    if L_exists = TRUE then
       O_error_message := SQL_LIB.CREATE_MSG('LOC_EXISTS_ZONE',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
    end if;
    ---
    if I_LOC_TYPE = 'S' then
      open C_STORE;
      fetch C_STORE into I_TI_LOC_NAME;
      if C_STORE%NOTFOUND then
        O_error_message := SQL_LIB.CREATE_MSG('INV_CUR_LOC',
                                              I_TI_ZONE_ID,
                                              NULL,
                                              NULL);
        close C_STORE;
        return FALSE;
      end if;
      close C_STORE;
      ---
    elsif I_LOC_TYPE = 'W' then
      --- Check if the location is a Physical warehouse.
      if WH_ATTRIB_SQL.CHECK_PWH(L_error_message,
                                 L_exists,
                                 I_LOCATION) = FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
      if L_exists = FALSE then
        O_error_message := SQL_LIB.CREATE_MSG('ENTER_PWH',
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      else
        open C_WH;
        fetch C_WH into I_TI_LOC_NAME;
        if C_WH%NOTFOUND then
          O_error_message := SQL_LIB.CREATE_MSG('INV_CUR_LOC',
                                                I_TI_ZONE_ID,
                                                NULL,
                                                NULL);
          close C_WH;
          return FALSE;
        end if;
        close C_WH;
      end if;
      ---
    end if;
  else
    if I_LOCATION is NOT NULL then
      I_LOCATION  := NULL;
    end if;
    ---
    if I_TI_LOC_NAME is NOT NULL then
      I_TI_LOC_NAME := NULL;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end LOCATION;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> GRP_FORM_VALIDATION.ZONE_GROUP_ID
-- NEW ARGUMENTS (2)
-- ITEM          :B_CZONEGRP.ZONE_GROUP_ID............... -> I_ZONE_GROUP_ID
-- Pack.Spec     GRP_FORM_VALIDATION..................... -> P_GrpFrmVldtn
-- ITEM          :B_czonegrp.rowid  ..................... -> I_ROWID
---------------------------------------------------------------------------------------------
FUNCTION ZONE_GROUP_ID(O_error_message   in OUT   VARCHAR2,
                       I_ZONE_GROUP_ID   in       NUMBER,
                       I_ROWID           in       ROWID,
                       P_GrpFrmVldtn     in OUT   VARCHAR2 )
  return BOOLEAN
is
  L_id        COST_ZONE.ZONE_GROUP_ID%TYPE;
  L_program   VARCHAR2(64) := 'PROCUREMENT_SQL.ZONE_GROUP_ID';
  ---
  cursor C_CZ_GROUP is
    select zone_group_id
      from cost_zone_group
     where rowid = I_ROWID;
  ---
  cursor C_CZ_GROUP_NEW is
    select zone_group_id
      from cost_zone_group
     where zone_group_id = I_ZONE_GROUP_ID;
BEGIN
  P_GrpFrmVldtn      := 'B_CZONEGRP.ZONE_GROUP_ID';
  if I_ZONE_GROUP_ID is NOT NULL then
    if I_ROWID       is NOT NULL then
      open C_CZ_GROUP;
      fetch C_CZ_GROUP into L_id;
      if C_CZ_GROUP%FOUND and L_id != I_ZONE_GROUP_ID then
        O_error_message            := SQL_LIB.CREATE_MSG('ZONE_GROUP_UNIQUE',
                                                         NULL,
                                                         NULL,
                                                         NULL);
        close C_CZ_GROUP;
        return FALSE;
      end if;
      close C_CZ_GROUP;
    else
      open C_CZ_GROUP_NEW;
      fetch C_CZ_GROUP_NEW into L_id;
      if C_CZ_GROUP_NEW%FOUND then
        O_error_message := SQL_LIB.CREATE_MSG('ZONE_GROUP_UNIQUE',
                                              NULL,
                                              NULL,
                                              NULL);
        close C_CZ_GROUP_NEW;
        return FALSE;
      end if;
      close C_CZ_GROUP_NEW;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end ZONE_GROUP_ID;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> GRP_FORM_VALIDATION.LIKE_GROUP
-- NEW ARGUMENTS (3)
-- ITEM          :B_CZONEGRP.TI_LIKE_GROUP............... -> I_TI_LIKE_GROUP
-- ITEM          :B_CZONEGRP.ZONE_GROUP_ID............... -> I_ZONE_GROUP_ID
-- Pack.Spec     GRP_FORM_VALIDATION..................... -> P_GrpFrmVldtn
---------------------------------------------------------------------------------------------
FUNCTION LIKE_GROUP(O_error_message   in OUT   VARCHAR2,
                    I_TI_LIKE_GROUP   in       NUMBER,
                    I_ZONE_GROUP_ID   in       NUMBER,
                    P_GrpFrmVldtn     in OUT   VARCHAR2 )
  return BOOLEAN
is
  L_valid_zone   COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE;
  L_program      VARCHAR2(64) := 'PROCUREMENT_SQL.LIKE_GROUP';
  ---
  cursor C_ZNGRP_VALID is
    select zone_group_id
      from cost_zone_group
     where zone_group_id = I_TI_LIKE_GROUP;
  cursor C_CZ_REC_EXIST is
    select zone_group_id
      from cost_zone_group_loc
     where zone_group_id = I_TI_LIKE_GROUP;
BEGIN
  P_GrpFrmVldtn      := 'B_czonegrp.TI_like_group';
  if I_TI_LIKE_GROUP is NOT NULL then
    open C_ZNGRP_VALID;
    fetch C_ZNGRP_VALID into L_valid_zone;
    IF C_ZNGRP_VALID%NOTFOUND or L_valid_zone = I_ZONE_GROUP_ID then
      O_error_message := SQL_LIB.CREATE_MSG('INV_LIKE_GROUP',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_ZNGRP_VALID;
      return FALSE;
    end if;
    close C_ZNGRP_VALID;
    open C_CZ_REC_EXIST;
    fetch C_CZ_REC_EXIST into L_valid_zone;
    if C_CZ_REC_EXIST%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_CZ_SETUP',
                                            NULL,
                                            NULL,
                                            NULL);
      close C_CZ_REC_EXIST;
      return FALSE;
    end if;
  end if;
  return TRUE;
EXCEPTION
  when OTHERS then
    O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          to_char(SQLCODE));
  return FALSE;
end LIKE_GROUP;
---------------------------------------------------------------------------------------------
-- Module                   : costzone (Form Module)
-- Source Object            : PROCEDURE  -> P_UPDATE_ZN_LOC
-- NEW ARGUMENTS (3)
-- ITEM          :B_CZONE.TI_PRIMARY_DISCHARGE_PORT...... -> I_TI_PRIMARY_DISCHARGE_PO
-- ITEM          :B_CZONE.ZONE_ID........................ -> I_ZONE_ID
-- ITEM          :B_CZONE_HEAD.TI_ZONE_GROUP_ID.......... -> I_TI_ZONE_GROUP_ID
---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ZN_LOC(O_error_message             in OUT   VARCHAR2,
                       I_TI_PRIMARY_DISCHARGE_PO   in       VARCHAR2,
                       I_ZONE_ID                   in       NUMBER,
                       I_TI_ZONE_GROUP_ID          in       NUMBER)
  return BOOLEAN
is
  L_program VARCHAR2(64) := 'PROCUREMENT_SQL.UPDATE_ZN_LOC';
BEGIN
  update cost_zone_group_loc
     set primary_discharge_port = I_TI_PRIMARY_DISCHARGE_PO
   where zone_group_id = I_TI_ZONE_GROUP_ID
     and zone_id = I_ZONE_ID;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : ordfind (Form Module)
-- Source Object            : PROCEDURE  -> FORM_VALIDATION.VENDOR_ORDER_NO
-- NEW ARGUMENTS (4)
-- ITEM          :B_MAIN.TI_SUPPLIER..................... -> I_TI_SUPPLIER
-- ITEM          :B_MAIN.TI_SUPPLIER_SITE................ -> I_TI_SUPPLIER_SITE
-- ITEM          :B_MAIN.TI_VENDOR_ORDER_NO.............. -> I_TI_VENDOR_ORDER_NO
-- Pack.Spec     INTERNAL_VARIABLES.GV_SYSTEM_OPTIONS_REC -> P_IntrnlVrblsOptnsRcrd
-----------------------------------------------------------------------------------
FUNCTION VENDOR_ORDER_NO(O_error_message          in OUT   VARCHAR2,
                         I_TI_SUPPLIER            in       NUMBER,
                         I_TI_SUPPLIER_SITE       in       NUMBER,
                         I_TI_VENDOR_ORDER_NO     in       VARCHAR2,
                         P_IntrnlVrblsOptnsRcrd   in       VARCHAR2)
  return BOOLEAN
is
  L_valid      VARCHAR2(1) := 'N';
  L_supplier   SUPS.SUPPLIER%TYPE;
  L_program    VARCHAR2(64) := 'PROCUREMENT_SQL.VENDOR_ORDER_NO';
  cursor C_VALID_VENDOR_ORDER is
     select 'Y'
       from ordhead
      where vendor_order_no = I_TI_VENDOR_ORDER_NO
        and supplier = NVL(L_supplier, supplier);
BEGIN
  if P_IntrnlVrblsOptnsRcrd = 'Y' then
    L_supplier             := I_TI_SUPPLIER_SITE;
  else
    L_supplier := I_TI_SUPPLIER;
  end if;
  ---
  if I_TI_VENDOR_ORDER_NO is NOT NULL then
    open C_VALID_VENDOR_ORDER;
    fetch C_VALID_VENDOR_ORDER into L_valid;
    close C_VALID_VENDOR_ORDER;
    ---
    if L_valid = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('INV_VENDOR',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end VENDOR_ORDER_NO;
---------------------------------------------------------------------------------------------
-- Module                   : ordhead (Form Module)
-- Source Object            : PROCEDURE  -> FORM_VALIDATION.SHIP_METHOD
-- NEW ARGUMENTS (3)
-- ITEM          :B_ORDHEAD.SHIP_METHOD.................. -> I_SHIP_METHOD
-- ITEM          :B_ORDHEAD.TI_SHIP_METHOD_DESC.......... -> I_TI_SHIP_METHOD_DESC
-- PARAMETER     :PARAMETER.PM_MODE...................... -> P_PM_MODE
---------------------------------------------------------------------------------------------
FUNCTION SHIP_METHOD(O_error_message         in OUT   VARCHAR2,
                     I_SHIP_METHOD           in       VARCHAR2,
                     I_TI_SHIP_METHOD_DESC   in OUT   VARCHAR2,
                     P_PM_MODE               in       VARCHAR2)
  return BOOLEAN
is
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_pay             VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.SHIP_METHOD';
  cursor C_PAY is
     select 'Y'
       from code_detail
      where code = I_SHIP_METHOD
        and code_type = 'SHPM';
BEGIN
  if P_PM_MODE != 'VIEW' then
    if I_SHIP_METHOD is NOT NULL then
      open C_PAY;
      fetch C_PAY into L_pay;
      close C_PAY;
      ---
      if L_pay = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_SHIP_METHOD',
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
      ---
      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    'SHPM',
                                    I_SHIP_METHOD,
                                    I_TI_SHIP_METHOD_DESC)= FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
    else
      if I_TI_SHIP_METHOD_DESC is NOT NULL then
        I_TI_SHIP_METHOD_DESC  := NULL;
      end if;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end SHIP_METHOD;
---------------------------------------------------------------------------------------------
-- Module                   : ordhead (Form Module)
-- Source Object            : PROCEDURE  -> FORM_VALIDATION.SHIP_PAY_METHOD
-- NEW ARGUMENTS (3)
-- ITEM          :B_ORDHEAD.SHIP_PAY_METHOD.............. -> I_SHIP_PAY_METHOD
-- ITEM          :B_ORDHEAD.TI_SHIP_PAY_DESC............. -> I_TI_SHIP_PAY_DESC
-- PARAMETER     :PARAMETER.PM_MODE...................... -> P_PM_MODE
---------------------------------------------------------------------------------------------
FUNCTION SHIP_PAY_METHOD(O_error_message      in OUT   VARCHAR2,
                         I_SHIP_PAY_METHOD    in       VARCHAR2,
                         I_TI_SHIP_PAY_DESC   in OUT   VARCHAR2,
                         P_PM_MODE            in       VARCHAR2)
  return BOOLEAN
is
  L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
  L_pay             VARCHAR2(1)  := 'N';
  L_program         VARCHAR2(64) := 'PROCUREMENT_SQL.SHIP_PAY_METHOD';
  cursor C_PAY is
     select 'Y'
       from code_detail
      where code = I_SHIP_PAY_METHOD
        and code_type = 'SHMT';
BEGIN
  if P_PM_MODE != 'VIEW' then
    if I_SHIP_PAY_METHOD is NOT NULL then
      open C_PAY;
      fetch C_PAY into L_pay;
      close C_PAY;
      ---
      if L_pay = 'N' then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAY_METHOD',
                                               NULL,
                                               NULL,
                                               NULL);
        return FALSE;
      end if;
      ---
      if LANGUAGE_SQL.GET_CODE_DESC(L_error_message,
                                    'SHMT',
                                    I_SHIP_PAY_METHOD,
                                    I_TI_SHIP_PAY_DESC)= FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
    else
      if I_TI_SHIP_PAY_DESC is NOT NULL then
        I_TI_SHIP_PAY_DESC  := NULL;
      end if;
    end if;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end SHIP_PAY_METHOD;
---------------------------------------------------------------------------------------------
-- Module                   : ordhead (Form Module)
-- Source Object            : PROCEDURE  -> P_CHECK_UNRECEIVED_QTY
-- NEW ARGUMENTS (2)
-- ITEM          :B_ORDHEAD.ORDER_NO..................... -> I_ORDER_NO
-- Pack.Spec     INTERNAL_VARIABLES.GV_UNRECEIVED_QTY_EXI -> P_IntrnlVrblsGvUnrcvdQtyExst
---------------------------------------------------------------------------------------------
FUNCTION CHECK_UNRECEIVED_QTY(O_error_message                in OUT   VARCHAR2,
                              I_ORDER_NO                     in       NUMBER,
                              P_IntrnlVrblsGvUnrcvdQtyExst   in OUT   VARCHAR2)
  return BOOLEAN
is
  L_unreceived_qty   VARCHAR2(1)  := NULL;
  L_program          VARCHAR2(64) := 'PROCUREMENT_SQL.CHECK_UNRECEIVED_QTY';
  cursor C_GET_TOTAL_QTY is
     select 'x'
       from ordloc
      where order_no = I_ORDER_NO
        and ((NVL(qty_received,0) < qty_ordered)
         or qty_ordered = 0);
BEGIN
  open C_GET_TOTAL_QTY;
  fetch C_GET_TOTAL_QTY into L_unreceived_qty;
  if C_GET_TOTAL_QTY%FOUND then
    P_IntrnlVrblsGvUnrcvdQtyExst := 'Y';
  end if;
  close C_GET_TOTAL_QTY;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : recctadj (Form Module)
-- Source Object            : PROCEDURE  -> P_NEW_COST
-- NEW ARGUMENTS (20)
-- ITEM          :B_APPLY.CB_ADJ_MATCHED_RCPT............ -> I_CB_ADJ_MATCHED_RCPT
-- ITEM          :B_APPLY.TI_COST_NEW_ORD................ -> I_TI_COST_NEW_ORD
-- ITEM          :B_APPLY.TI_COST_OLD_ORD................ -> I_TI_COST_OLD_ORD
-- ITEM          :B_DETAIL.LOCATION...................... -> I_LOCATION
-- ITEM          :B_DETAIL.LOC_TYPE...................... -> I_LOC_TYPE
-- ITEM          :B_DETAIL.TI_LOCAL_CURRENCY............. -> I_TI_LOCAL_CURRENCY
-- ITEM          :B_HEAD.TI_ITEM......................... -> I_TI_ITEM
-- ITEM          :B_HEAD.TI_ORDER_NO..................... -> I_TI_ORDER_NO
-- ITEM          :B_HEAD.TI_SUPPLIER..................... -> I_TI_SUPPLIER
-- Pack.Spec     INTERNAL_VARIABLES.GP_ALC_STATUS........ -> P_IntrnlVrblsGpAlcStts
-- Pack.Spec     INTERNAL_VARIABLES.GP_ELC_IND........... -> P_IntrnlVrblsGpElcInd
-- Pack.Spec     INTERNAL_VARIABLES.GP_IMPORT_COUNTRY_ID. -> P_IntrnlVrblsGpImprtCntryId
-- Pack.Spec     INTERNAL_VARIABLES.GP_IMPORT_IND........ -> P_IntrnlVrblsGpImprtInd
-- Pack.Spec     INTERNAL_VARIABLES.GP_IMPORT_ORDER_IND.. -> P_IntrnlVrblsGpImprtOrdrInd
-- Pack.Spec     INTERNAL_VARIABLES.GP_ITEM_LEVEL........ -> P_IntrnlVrblsGpItmLvl
-- Pack.Spec     INTERNAL_VARIABLES.GP_ORIGIN_COUNTRY_ID. -> P_IntrnlVrblsGpOrgnCntryId
-- Pack.Spec     INTERNAL_VARIABLES.GP_PACK_IND.......... -> P_IntrnlVrblsGpPckInd
-- Pack.Spec     INTERNAL_VARIABLES.GP_PACK_TYPE......... -> P_IntrnlVrblsGpPckType
-- Pack.Spec     INTERNAL_VARIABLES.GP_PC_DIFF_ORD....... -> P_IntrnlVrblsGpPcDffOrd
-- Pack.Spec     INTERNAL_VARIABLES.GP_PRIM_CURRENCY..... -> P_IntrnlVrblsGpPrmCrrncy
-- Pack.Spec     INTERNAL_VARIABLES.GP_tran_level   ..... -> P_IntrnlVrblsGpTrnlvl
-- Pack.Spec     INTERNAL_VARIABLES.GP_order_as_type..... -> P_IntrnlVrblsGpOrdType
---------------------------------------------------------------------------------------------
FUNCTION NEW_COST(O_error_message               in OUT   VARCHAR2,
                  I_CB_ADJ_MATCHED_RCPT         in       VARCHAR2,
                  I_TI_COST_NEW_ORD             in       NUMBER,
                  I_TI_COST_OLD_ORD             in       NUMBER,
                  I_LOCATION                    in       NUMBER,
                  I_LOC_TYPE                    in       VARCHAR2,
                  I_TI_LOCAL_CURRENCY           in       VARCHAR2,
                  I_TI_ITEM                     in       VARCHAR2,
                  I_TI_ORDER_NO                 in       NUMBER,
                  I_TI_SUPPLIER                 in       NUMBER,
                  P_IntrnlVrblsGpAlcStts        in       ALC_HEAD.STATUS%TYPE,
                  P_IntrnlVrblsGpElcInd         in       VARCHAR2,
                  P_IntrnlVrblsGpImprtCntryId   in       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                  P_IntrnlVrblsGpImprtInd       in       VARCHAR2,
                  P_IntrnlVrblsGpImprtOrdrInd   in       VARCHAR2,
                  P_IntrnlVrblsGpItmLvl         in       ITEM_MASTER.ITEM_LEVEL%TYPE,
                  P_IntrnlVrblsGpOrgnCntryId    in       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                  P_IntrnlVrblsGpPckInd         in       ITEM_MASTER.PACK_IND%TYPE,
                  P_IntrnlVrblsGpPckType        in       ITEM_MASTER.PACK_TYPE%TYPE,
                  P_IntrnlVrblsGpPcDffOrd       in OUT   ORDLOC.UNIT_COST%TYPE,
                  P_IntrnlVrblsGpPrmCrrncy      in       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
                  P_IntrnlVrblsGpTrnlvl         in       ITEM_MASTER.TRAN_LEVEL%TYPE,
                  P_IntrnlVrblsGpOrdType        in       ITEM_MASTER.ORDER_AS_TYPE%TYPE )
  return BOOLEAN
is
  L_error_message       RTK_ERRORS.RTK_TEXT%TYPE := NULL;
  L_item                ITEM_MASTER.ITEM%TYPE;
  L_pack_item           ITEM_MASTER.ITEM%TYPE;
  L_total_elc           NUMBER;
  L_expenses            ORDLOC.UNIT_COST%TYPE;
  L_exp_currency        VARCHAR2(255);
  L_exchange_rate_exp   VARCHAR2(255);
  L_duty                VARCHAR2(255);
  L_duty_currency       VARCHAR2(255);
  L_program             VARCHAR2(64) := 'PROCUREMENT_SQL.NEW_COST';
  cursor C_GET_PACK_ITEMS is
     select item,
            qty
       from v_packsku_qty
      where pack_no = I_TI_ITEM;
BEGIN
  if I_TI_COST_NEW_ORD is NOT NULL then
    P_IntrnlVrblsGpPcDffOrd := I_TI_COST_NEW_ORD - I_TI_COST_OLD_ORD;
    --  update ordloc.unit_cost
    if P_IntrnlVrblsGpItmLvl = P_IntrnlVrblsGpTrnlvl then
      if P_IntrnlVrblsGpPckInd = 'Y' then
        if P_IntrnlVrblsGpPckType = 'B' then
          -- Populate comp item_elc_temp table with 'old landed cost' fpr
          -- each component itemin the buyer pack.
          FOR c_rec in c_get_pack_items
          LOOP
            if P_IntrnlVrblsGpAlcStts = 'PR' then
              ---- get elc total for finalized order
              if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                           L_total_elc,
                                           I_TI_ORDER_NO,
                                           I_TI_ITEM, c_rec.item, -- component item in pack
                                           I_LOCATION,
                                           I_TI_SUPPLIER,
                                           P_IntrnlVrblsGpOrgnCntryId,
                                           P_IntrnlVrblsGpImprtCntryId, NULL) = FALSE then
                  O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                        NULL,
                                                        NULL,
                                                        NULL);
                return FALSE;
              end if;
            else
              if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                          L_total_elc,
                                          L_expenses,
                                          L_exp_currency,
                                          L_exchange_rate_exp,
                                          L_duty,
                                          L_duty_currency,
                                          I_TI_ORDER_NO,
                                          I_TI_ITEM,
                                          c_rec.item, -- component item in pack
                                          NULL,
                                          I_LOCATION,
                                          I_TI_SUPPLIER,
                                          P_IntrnlVrblsGpOrgnCntryId,
                                          P_IntrnlVrblsGpImprtCntryId, NULL) = FALSE then
                  O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                        NULL,
                                                        NULL,
                                                        NULL);
                return FALSE;
              end if;
            end if;
            ---
            if CURRENCY_SQL.CONVERT(L_error_message,
                                    L_total_elc,
                                    P_IntrnlVrblsGpPrmCrrncy,
                                    I_TI_LOCAL_CURRENCY,
                                    L_total_elc,
                                    'C',
                                    NULL,
                                    NULL) = FALSE then
              O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                    NULL,
                                                    NULL,
                                                    NULL);
              return FALSE;
            end if;
            ---
            insert into comp_item_elc_temp(order_no,
                                           pack_item,
                                           item,
                                           location,
                                           landed_cost)
                                    values(I_TI_ORDER_NO,
                                           I_TI_ITEM,
                                           c_rec.item,
                                           I_LOCATION,
                                           L_total_elc);
          end LOOP;
        else -- Pack is a Vendor Pack
          --- if order is finalized
          if P_IntrnlVrblsGpAlcStts = 'PR' then
            ---- get elc total for finalized order
            if REC_COST_ADJ_SQL.CALC_ELC(L_error_message,
                                         L_total_elc,
                                         I_TI_ORDER_NO,
                                         I_TI_ITEM,
                                         NULL, -- Component item in Pack not needed in Vendor Packs
                                         I_LOCATION,
                                         I_TI_SUPPLIER,
                                         P_IntrnlVrblsGpOrgnCntryId,
                                         P_IntrnlVrblsGpImprtCntryId,
                                         NULL)= FALSE then
                O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                      NULL,
                                                      NULL,
                                                      NULL);
              return FALSE;
            end if;
          else
            -- Populate comp_item_elc_temp table with 'old landed cost' for Vendor Pack
            if ELC_CALC_SQL.CALC_TOTALS(L_error_message,
                                        L_total_elc,
                                        L_expenses,
                                        L_exp_currency,
                                        L_exchange_rate_exp,
                                        L_duty,
                                        L_duty_currency,
                                        I_TI_ORDER_NO,
                                        I_TI_ITEM,
                                        NULL, -- Component item in Pack not needed in Vendor Packs
                                        NULL,
                                        I_LOCATION,
                                        I_TI_SUPPLIER,
                                        P_IntrnlVrblsGpOrgnCntryId,
                                        P_IntrnlVrblsGpImprtCntryId,
                                        NULL)= FALSE then
               O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                     NULL,
                                                     NULL,
                                                     NULL);
              return FALSE;
            end if;
          end if;
          ---
          if CURRENCY_SQL.CONVERT(L_error_message,
                                  L_total_elc,
                                  P_IntrnlVrblsGpPrmCrrncy,
                                  I_TI_LOCAL_CURRENCY,
                                  L_total_elc,
                                  'C',
                                  NULL,
                                  NULL)= FALSE then
             O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                   NULL,
                                                   NULL,
                                                   NULL);
            return FALSE;
          end if;
          ---
          insert into comp_item_elc_temp(order_no,
                                         pack_item,
                                         item,
                                         location,
                                         landed_cost)
                                  values(I_TI_ORDER_NO,
                                         I_TI_ITEM,
                                         NULL,
                                         I_LOCATION,
                                         L_total_elc);
        end if;
      end if;
      ---
      if REC_COST_ADJ_SQL.UPDATE_ORDER_COST(L_error_message,
                                            I_TI_COST_NEW_ORD,
                                            I_TI_ORDER_NO,
                                            I_TI_ITEM,
                                            I_LOCATION,
                                            I_LOC_TYPE,
                                            I_CB_ADJ_MATCHED_RCPT)= FALSE then
        O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                              NULL,
                                              NULL,
                                              NULL);
        return FALSE;
      end if;
      ---
      -- if (ELC is on and ALC is off) or
      --    (ALC is on)
      if(P_IntrnlVrblsGpImprtInd = 'N' and P_IntrnlVrblsGpElcInd = 'Y')
         or (P_IntrnlVrblsGpImprtInd = 'Y') then
        -- If the item is a Buyer Pack with an Order As Type of Pack
        -- The Pack Number must be passed in the Pack Item Parameter
           if P_IntrnlVrblsGpPckType = 'B' and P_IntrnlVrblsGpOrdType = 'P' then
              L_item := NULL;
              L_pack_item := I_TI_ITEM;
           else
             L_item := I_TI_ITEM;
             L_pack_item := NULL;
        end if;
        ---
        -- re-calculate ELC based on new cost
        if ELC_CALC_SQL.CALC_COMP(L_error_message,
                                  'PE',                              -- 'Purchase Order Expenses'
                                  L_item,                                             -- comp sku if item is a buyer pack
                                  NULL,
                                  NULL,
                                  NULL,
                                  I_TI_ORDER_NO,
                                  NULL,
                                  L_pack_item, -- pack number if item is a buyer pack otherwise null
                                  NULL,
                                  NULL,
                                  P_IntrnlVrblsGpImprtCntryId,
                                  P_IntrnlVrblsGpOrgnCntryId,
                                  NULL,
                                  NULL)= FALSE then
          O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                NULL,
                                                NULL,
                                                NULL);
          return FALSE;
        end if;
        if P_IntrnlVrblsGpImprtOrdrInd = 'Y' then
          if ELC_CALC_SQL.CALC_COMP(L_error_message,
                                    'PA', -- 'Purchase Order Assessments'
                                    L_item,
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_TI_ORDER_NO,
                                    NULL,
                                    L_pack_item,
                                    NULL,
                                    NULL,
                                    P_IntrnlVrblsGpImprtCntryId,
                                    P_IntrnlVrblsGpOrgnCntryId,
                                    NULL,
                                    NULL)= FALSE then
            O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
          end if;
          if ELC_CALC_SQL.CALC_COMP(L_error_message,
                                    'PE',                 -- 'Purchase Order Expenses'
                                    L_item,               -- comp sku if item is a buyer pack
                                    NULL,
                                    NULL,
                                    NULL,
                                    I_TI_ORDER_NO,
                                    NULL,
                                    L_pack_item,          -- pack number if item is a buyer pack otherwise null
                                    NULL,
                                    NULL,
                                    P_IntrnlVrblsGpImprtCntryId,
                                    P_IntrnlVrblsGpOrgnCntryId,
                                    NULL,
                                    NULL)= FALSE then
            O_error_message := SQL_LIB.CREATE_MSG(L_error_message,
                                                  NULL,
                                                  NULL,
                                                  NULL);
            return FALSE;
          end if;
        end if;
        /* import ind is Y */
      end if;
      /* multiple inds */
    end if;
    /* system ind in 'f' or 'S' */
  end if;
  /* if cost is not NULL */
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : suppsku (Form Module)
-- Source Object            : FUNCTION   -> F_MIN_DATE
-- NEW ARGUMENTS (1)
-- Pack.Spec     INTERNAL_VARIABLES.GV_VDATE............. -> P_IntrnlVrblsGvVdte
--------------------------------------------------------------------------------------------
FUNCTION MIN_DATE(O_error_message       in OUT   VARCHAR2,
                  O_ret                 in OUT   DATE,
                  P_IntrnlVrblsGvVdte   in       PERIOD.VDATE%TYPE)
  return BOOLEAN
is
  ---
  L_cost_prior_create_days   FOUNDATION_UNIT_OPTIONS.COST_PRIOR_CREATE_DAYS%TYPE;
  L_vdate                    PERIOD.VDATE%TYPE := P_IntrnlVrblsGvVdte;
  L_min_date                 DATE;
  L_program                  VARCHAR2(64):= 'PROCUREMENT_SQL.MIN_DATE';
  ---
  cursor C_FOUNDATION_UNIT_OPTIONS is
     select NVL(cost_prior_create_days, 0)
       from foundation_unit_options;
  ---
BEGIN
  open C_FOUNDATION_UNIT_OPTIONS;
  fetch C_FOUNDATION_UNIT_OPTIONS into L_cost_prior_create_days;
  close C_FOUNDATION_UNIT_OPTIONS;
  ---
  if L_cost_prior_create_days < 1 then
    L_cost_prior_create_days := 1;
  end if;
  ---
  L_min_date := L_vdate + L_cost_prior_create_days;
  O_ret      := L_min_date;
  ---
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end;
---------------------------------------------------------------------------------------------
-- Module                   : timeline (Form Module)
-- Source Object            : FUNCTION   -> FORM_VALIDATION.VALIDATE_TMLN_STEPS
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TMLN_STEPS(I_timeline_no     in       timeline_steps.timeline_no%TYPE,
                             I_timeline_type   in       timeline_head.timeline_type%TYPE,
                             O_exists          in OUT   VARCHAR2,
                             O_error_message   in OUT   VARCHAR2)
  return BOOLEAN
is
  L_exists    VARCHAR2(1)  := 'N';
  L_program   VARCHAR2(64) := 'PROCUREMENT_SQL.VALIDATE_TMLN_STEPS';
  cursor C_PRIMARY is
     select 'Y'
       from timeline_steps s,
            timeline_head h
      where s.timeline_no = I_timeline_no
        and h.timeline_no  = s.timeline_no
        and h.timeline_type = I_timeline_type;
BEGIN
  if I_timeline_no is NOT NULL then
    open C_PRIMARY;
    fetch C_PRIMARY into L_exists;
    close C_PRIMARY;
    O_exists := L_exists;
    return TRUE;
  end if;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end VALIDATE_TMLN_STEPS;
---------------------------------------------------------------------------------------------
-- Module                   : timeline (Form Module)
-- Source Object            : FUNCTION   -> FORM_VALIDATION.VALIDATE_TIMELINE
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TIMELINE(I_key_value_1     in       timeline.key_value_1%TYPE,
                           I_key_value_2     in       timeline.key_value_2%TYPE,
                           I_timeline_no     in       timeline.timeline_no%TYPE,
                           O_exists          in OUT   VARCHAR2,
                           O_error_message   in OUT   VARCHAR2)
  return BOOLEAN
is
  L_exists    VARCHAR2(1)  := 'N';
  L_program   VARCHAR2(64) := 'PROCUREMENT_SQL.VALIDATE_TIMELINE';
  cursor C_CHECK_TIMELINE is
     select 'Y'
       from timeline
      where key_value_1 = I_key_value_1
        and (key_value_2 = I_key_value_2
         or key_value_2 is NULL)
        and timeline_no = I_timeline_no;
BEGIN
  open C_CHECK_TIMELINE;
  fetch C_CHECK_TIMELINE into L_exists;
  close C_CHECK_TIMELINE;
  O_exists := L_exists;
  return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
  return FALSE;
end VALIDATE_TIMELINE;
end PROCUREMENT_SQL;
/
