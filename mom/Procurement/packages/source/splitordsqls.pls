create or replace PACKAGE SPLIT_ORDER_SQL AUTHID CURRENT_USER AS

---------------------------------------------------------------------------------------------------------------
-- Function: SPLIT_PO
-- Purpose : Conversion program for split_po.pc
---------------------------------------------------------------------------------------------------------------
FUNCTION SPLIT_PO(I_ordhead_tbl       IN OUT    ADD_LINE_ITEM_SQL.ORDHEAD_TBL,
                  O_split_ind         IN OUT    NUMBER,
                  O_error_message     IN OUT    VARCHAR2) 
RETURN NUMBER;
---------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_ORDER(I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                      O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;

---------------------------------------------------------------------------------------------------------------
END SPLIT_ORDER_SQL;
/