
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ELC_ORDER_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: RECALC_COMP
--Purpose      : Function called by CALC_COMP to calculate all order
--               expenses and assessments.
-------------------------------------------------------------------------------
FUNCTION RECALC_COMP(O_error_message     IN OUT VARCHAR2,
                     O_est_value         IN OUT NUMBER,
                     I_dtl_flag          IN     VARCHAR2,
                     I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                     I_calc_type         IN     VARCHAR2,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_supplier          IN     SUPS.SUPPLIER%TYPE,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_ord_seq_no        IN     ORDLOC_EXP.SEQ_NO%TYPE,
                     I_pack_item         IN     ORDLOC_EXP.PACK_ITEM%TYPE,
                     I_location          IN     ORDLOC.LOCATION%TYPE,
                     I_hts               IN     HTS.HTS%TYPE,
                     I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                     I_effect_to         IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION RECALC_COMP(O_error_message            IN OUT VARCHAR2,
                     O_est_value                IN OUT NUMBER,
                     I_dtl_flag                 IN     VARCHAR2,
                     I_comp_id                  IN     ELC_COMP.COMP_ID%TYPE,
                     I_calc_type                IN     VARCHAR2,
                     I_item                     IN     ITEM_MASTER.ITEM%TYPE,
                     I_supplier                 IN     SUPS.SUPPLIER%TYPE,
                     I_order_no                 IN     ORDHEAD.ORDER_NO%TYPE,
                     I_ord_seq_no               IN     ORDLOC_EXP.SEQ_NO%TYPE,
                     I_pack_item                IN     ORDLOC_EXP.PACK_ITEM%TYPE,
                     I_location                 IN     ORDLOC.LOCATION%TYPE,
                     I_hts                      IN     HTS.HTS%TYPE,
                     I_import_country_id        IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_origin_country_id        IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_effect_from              IN     HTS.EFFECT_FROM%TYPE,
                     I_effect_to                IN     HTS.EFFECT_TO%TYPE,
                     I_extra_exp_info_populated IN     BOOLEAN,
                     I_calc_basis               IN     ELC_COMP.CALC_BASIS%TYPE,
                     I_cvb_code                 IN     ORDLOC_EXP.CVB_CODE%TYPE,
                     I_comp_rate                IN     ORDLOC_EXP.COMP_RATE%TYPE,
                     I_exchange_rate            IN     ORDLOC_EXP.EXCHANGE_RATE%TYPE,
                     I_cost_basis               IN     ORDLOC_EXP.COST_BASIS%TYPE,
                     I_comp_currency            IN     ORDLOC_EXP.COMP_CURRENCY%TYPE,
                     I_per_count                IN     ORDLOC_EXP.PER_COUNT%TYPE,
                     I_per_count_uom            IN     ORDLOC_EXP.PER_COUNT_UOM%TYPE,
                     I_import_ord_ind           IN     ORDHEAD.IMPORT_ORDER_IND%TYPE DEFAULT NULL)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ELC_ORDER_SQL;
/
