
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_DIST_SQL AUTHID CURRENT_USER AS
--------------------------------------------------------
--------------------------------------------------------
-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_LOCATION
-- Purpose:       Validates the filter criteria for locations before distributing the order.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_LOCATION( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_qty_ind        IN OUT  VARCHAR2,
                            I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                            I_complete_ind   IN      VARCHAR2,
                            I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
                            RETURN BOOLEAN;

-----------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_STORE_GRADE
-- Purpose:       Validates the filter criteria for store grades before distributing the order.
-----------------------------------------------------------------------------------------------
FUNCTION VALIDATE_STORE_GRADE( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_qty_ind        IN OUT  VARCHAR2,
                               I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                               I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
                               RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--    Name: VALIDATE_DIFF
-- Purpose: Validates the filter criteria for diffs before distributing the order using
--          an individual diff.
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFF(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_qty_ind        IN OUT  VARCHAR2,
                       O_diff_group     IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                       I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE,
                       I_diff_no        IN      NUMBER)
RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
--    Name: VALIDATE_DIFFS
-- Purpose: Validates the filter criteria for diffs before distributing the order using
--          multiple diffs (diff matrix form).
---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_DIFFS(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_qty_ind        IN OUT  VARCHAR2,
                        O_diff_1         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        O_diff_2         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        O_diff_3         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        O_diff_4         IN OUT  DIFF_GROUP_HEAD.DIFF_GROUP_ID%TYPE,
                        I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                        I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name:  REDIST_DIFFS
-- Purpose:        Allows the user to redistribute items by diffs by
--                 recombining all existing diff-distributed records for those
--                 items.
-----------------------------------------------------------------------------------------------
FUNCTION REDIST_DIFFS (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                       I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE,
                       I_diff_no        IN      NUMBER)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name:  REDIST_STORE_GRADE
-- Purpose:        Allows the user to redistribute items by store grade by
--                 recombining all existing grade-distributed and/or
--                 location-distributed records for those items
----------------------------------------------------------------------------------------------
FUNCTION REDIST_STORE_GRADE (O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no       IN      ORDLOC.ORDER_NO%TYPE,
                             I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name:  REDIST_LOCATION
-- Purpose:        Allows the user to redistribute items by location by
--                 recombining all existing location-distributed records
--                 for those items
----------------------------------------------------------------------------------------------
FUNCTION REDIST_LOCATION (O_error_message   IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no        IN      ORDLOC.ORDER_NO%TYPE,
                          I_where_clause    IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------------------
-- Function Name:  MOVE_TO_WKSHT
-- Purpose:        Takes all records from ordredst_temp and inserts into the ordloc_wksht to allow 
--                 the redistribution of items. Deletes from the appropriate order detail tables. 
----------------------------------------------------------------------------------------------
FUNCTION MOVE_TO_WKSHT (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no         IN      ORDHEAD.ORDER_NO%TYPE,
                        I_supplier         IN      ORDHEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
    
----------------------------------------------------------------------------------------------
-- Function Name:  CHECK_DIST_QTY_UOM
-- Purpose:        Verifies that for all items on I_order all items have the 
--                 same unit of measure on ordloc_wksht.
----------------------------------------------------------------------------------------------
FUNCTION CHECK_DIST_QTY_UOM( O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_indicator      IN OUT  VARCHAR2,
                             I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                             I_where_clause   IN      FILTER_TEMP.WHERE_CLAUSE%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: WO_TO_WKSHT
-- Purpose:       This function will insert records to the ordloc_wksht table for all items and 
--                locations on the ordsku and ordloc tables. These item/location combinations will 
--                be used to create new work orders.  
--                I_uop_suom is an indicator ('P' or 'S') for Unit of Purchase or Standard Unit
--                of Measure.
-----------------------------------------------------------------------------------------------
FUNCTION WO_TO_WKSHT(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no       IN      ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: COPY_ORDLOC
-- Purpose:       This function will copy all information from the ordloc, ordsku_exp, 
--                timeline, req_doc, ordsku_hts, wo_head, wo_wip, wo_sku_loc ordsku_hts_asses, 
--                ordsku_discount and ordsku tables to their respective temp tables for each item being 
--                redistributed on both approved and previously approved orders.  It will also delete
--                from the temp tables.
-----------------------------------------------------------------------------------------------
FUNCTION COPY_ORDLOC(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                     I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                     I_item           IN      ORDSKU.ITEM%TYPE,
                     I_redist_ind     IN      VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: GET_ORD_RDST_QTY
-- Purpose:       This function will retrieve and evaluate the total quantity originally ordered
--                from the ordloc_temp table and the new quantity ordered on the ordloc_wksht table
--                for each item being redistributed on the order number passed into the function.
-----------------------------------------------------------------------------------------------
FUNCTION GET_ORD_RDST_QTY(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                          O_item           IN OUT  ORDSKU.ITEM%TYPE,
                          O_qty_to_order   IN OUT  ORDLOC.QTY_ORDERED%TYPE,
                          I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: REDIST_ORDER
-- Purpose:  This function is responsible for initiated the versioning process for a successfully
--           redistributed order and deleting the temp records from ordloc_temp, ordsku_temp,
--           ordsku_exp_temp, ordsku_hts_assess_temp, ordsku_discount_temp.
-----------------------------------------------------------------------------------------------
FUNCTION REDIST_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: REBUILD_ORDER
-- Purpose:  This function is responsible for re-applying the ordloc_temp, ordsku_temp, 
--           ordsku_exp_temp, ordsku_hts_assess_temp, ordsku_discount_temp, alloc_header_temp,
--           timeline_temp, req_doc_temp, ordsku_hts_temp, wo_head_temp, wo_wip_temp,
--           wo_sku_loc_temp, alloc_chrg_temp and alloc_detail_temp records to the ordloc
--           and ordsku tables when the redistribution in cancelled.
-----------------------------------------------------------------------------------------------
FUNCTION REBUILD_ORDER(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name: CHECK_QTY
-- Purpose:  This function will make sure all of the records on the ordmtxws multirecord block
--           have a qty greater than 0 by using dynamic SQL.  The where clause will be taken from
--           the global parameter in the form which exists if the user has filtered out any records.
--           For example, we will not want to check the qtys for items which the user is not trying
--           to distribute.
-----------------------------------------------------------------------------------------------
FUNCTION CHECK_QTY (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                    O_exist           IN OUT BOOLEAN) 
RETURN BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  ITEM_EXISTS
--Purpose      :  Checks if the passed in Item exists on the Order Redistribution
--                Temp table (ordredst_temp).  If the Item Typs is passed in as
--                'A' (All Items), if any items exist on the temp table O_exists 
--                will be passed out as TRUE.  It the Item is an Item Parent 
--                or Grandparent if any child items exist on the temp table 
--                O_exists will be passed out as TRUE.
-------------------------------------------------------------------------------
FUNCTION ITEM_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists            IN OUT BOOLEAN,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_item_type         IN     CODE_DETAIL.CODE%TYPE)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
--Function Name:  CHECK_REDST
--Purpose      :  Checks if any Allocations or Appointments exist for the Items
--                on the Order Redistribution Temp table (ordredst_temp).
--                In the end it checks to ensure there are items that can be re-
--                distributed on the order.
-------------------------------------------------------------------------------
FUNCTION CHECK_REDST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_alloc_exists      IN OUT BOOLEAN,
                     O_appt_exists       IN OUT BOOLEAN,
                     O_exist_to_redist   IN OUT BOOLEAN)
RETURN BOOLEAN; 
-------------------------------------------------------------------------------
FUNCTION CHECK_DIFFS_FOR_REDIST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_valid           IN OUT BOOLEAN,
                                O_locations       IN OUT BOOLEAN,
                                I_item_parent     IN     ITEM_MASTER.ITEM%TYPE,
                                I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                                I_diff_1_ind      IN     VARCHAR2,
                                I_diff_2_ind      IN     VARCHAR2,
                                I_diff_3_ind      IN     VARCHAR2,
                                I_diff_4_ind      IN     VARCHAR2,
                                I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------
--Function Name:  CLEAR_ORDREDST_TEMP
--Purpose      :  Deletes items for a specific order_no from the temp table
--                ORDREDST_TEMP.
-------------------------------------------------------------------------------
FUNCTION CLEAR_ORDREDST_TEMP (O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no         IN      ORDHEAD.ORDER_NO%TYPE)
   return BOOLEAN;
-------------------------------------------------------------------------------     
--Function Name:  PROCESS_PACK_TMPL
--Purpose      :  Checks for the existence of the pack template related to the 
--                entered pack and validates if there is an existing duplicate. 
-------------------------------------------------------------------------------
FUNCTION PROCESS_PACK_TMPL(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_pack_tmpl_exist     IN OUT   VARCHAR2,
                           O_dup_exist           IN OUT   VARCHAR2,
                           I_pack_no             IN       ORDLOC_WKSHT.ITEM%TYPE,
                           I_order_no            IN       ORDLOC_WKSHT.ORDER_NO%TYPE,
                           I_item_parent         IN       ORDLOC_WKSHT.ITEM_PARENT%TYPE,
                           I_pack_tmpl           IN       PACK_TMPL_HEAD.PACK_TMPL_ID%TYPE,
                           I_pack_type           IN       VARCHAR2,
                           I_supplier            IN       SUPS.SUPPLIER%TYPE,
                           I_pack_desc           IN       ITEM_MASTER.ITEM_DESC%TYPE,
                           I_pack_short_desc     IN       ITEM_MASTER.ITEM_DESC%TYPE,
                           I_pack_vendor_desc    IN       ITEM_MASTER.ITEM_DESC%TYPE,
                           I_order_as_type       IN       VARCHAR2,
                           I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                           I_import_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)

   RETURN BOOLEAN;
----------------------------------------------------------------------------------
--Function Name:  VALIDATE_NEW_ITEM
--Purpose      :  Validates new item and reference item number type
--                before adding to order worksheet.
----------------------------------------------------------------------------------
FUNCTION VALIDATE_NEW_ITEM(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_valid_item_ind     IN OUT   BOOLEAN,
                           I_contract_no        IN       CONTRACT_HEADER.CONTRACT_NO%TYPE,
                           I_dept_level         IN       PROCUREMENT_UNIT_OPTIONS.DEPT_LEVEL_ORDERS%TYPE,
                           I_dept_order         IN       ITEM_MASTER.DEPT%TYPE,
                           I_order_type         IN       ORDHEAD.ORDER_TYPE%TYPE,
                           I_supplier           IN       ORDHEAD.SUPPLIER%TYPE,
                           I_location           IN       ORDLOC.LOCATION%TYPE,
                           I_fash_prepack_ind   IN       PACK_TMPL_HEAD.FASH_PREPACK_IND%TYPE,
                           I_item               IN       ITEM_MASTER.ITEM%TYPE,
                           I_ref_item           IN       ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------------                   
END ORDER_DIST_SQL;
/
