
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEAL_THRESHOLD_SQL AUTHID CURRENT_USER AS
-----------------------------------------------------------------------
-- Filename: For a package this will be dealthress.pls where dealthres is the unique 
-- prefix file name.
--
-- Purpose: This new package will support the new functionality to allow the 
--          user to change deal thresholds and save the revision information.   
-- Author:  Amran Sabar (Accenture - ERC)
-----------------------------------------------------------------------

----------------------------------------------------------------------------- 
-- Name:       NEXT_REV_NO
-- Purpose:    Return the next revision number for the deal threshold key combination.  
-- Created By: Amran Sabar
-----------------------------------------------------------------------------
FUNCTION NEXT_REV_NO (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_revision_no     IN OUT DEAL_THRESHOLD_REV.REV_NO%TYPE,
                      I_deal_id         IN     DEAL_THRESHOLD_REV.DEAL_ID%TYPE,
                      I_deal_detail_id  IN     DEAL_THRESHOLD_REV.DEAL_DETAIL_ID%TYPE,
                      I_lower_limit     IN     DEAL_THRESHOLD_REV.LOWER_LIMIT%TYPE,
                      I_upper_limit     IN     DEAL_THRESHOLD_REV.UPPER_LIMIT%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------

----------------------------------------------------------------------------- 
-- Name:       THRESHOLD_REV
-- Purpose:    It will capture the threshold updates/deletions/additions.
-- Created By: Amran Sabar
-----------------------------------------------------------------------------
FUNCTION THRESHOLD_REV (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_deal_id         IN     DEAL_THRESHOLD_REV.DEAL_ID%TYPE,
                        I_deal_detail_id  IN     DEAL_THRESHOLD_REV.DEAL_DETAIL_ID%TYPE,
                        I_revision_no     IN     DEAL_THRESHOLD_REV.REV_NO%TYPE,
                        I_lower_limit     IN     DEAL_THRESHOLD_REV.LOWER_LIMIT%TYPE,
                        I_upper_limit     IN     DEAL_THRESHOLD_REV.UPPER_LIMIT%TYPE,
                        I_action_ind      IN     DEAL_THRESHOLD_REV.ACTION%TYPE)
RETURN BOOLEAN;
----------------------------------------------------------------------------- 

END DEAL_THRESHOLD_SQL;


/
