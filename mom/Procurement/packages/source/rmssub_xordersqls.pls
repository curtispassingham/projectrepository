CREATE OR REPLACE PACKAGE RMSSUB_XORDER_SQL AUTHID CURRENT_USER AS

   TYPE ORDDTL_TAB IS TABLE OF SVC_ORDDETAIL%ROWTYPE;
-------------------------------------------------------------------------------------------------------
   -- Function Name:  CREATE_F_ORDER
   -- Purpose      :  Facilitates the creation of a Franchise Order.
-------------------------------------------------------------------------------------------------------
FUNCTION CREATE_F_ORDER(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                        I_orig_order_status   IN       ORDHEAD.STATUS%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_RECORDS(O_error_message      IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_svc_ordhead_rec   IN OUT NOCOPY  SVC_ORDHEAD%ROWTYPE,
                       IO_svc_orddetail_tbl IN OUT NOCOPY  ORDDTL_TAB,
                       I_message            IN             "RIB_XOrderDesc_REC",
                       I_message_type       IN             VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION BUILD_RECORDS(O_error_message      IN OUT         RTK_ERRORS.RTK_TEXT%TYPE,
                       IO_svc_ordhead_rec   IN OUT NOCOPY  SVC_ORDHEAD%ROWTYPE,
                       IO_svc_orddetail_tbl IN OUT NOCOPY  ORDDTL_TAB,
                       I_message            IN             "RIB_XOrderRef_REC",
                       I_message_type       IN             VARCHAR2)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
FUNCTION PERSIST(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                 IO_svc_ordhead_rec   IN OUT  SVC_ORDHEAD%ROWTYPE,
                 IO_svc_orddetail_tbl IN OUT  ORDDTL_TAB,
                 I_message_type       IN      VARCHAR2,
                 I_process_id         IN      SVC_PROCESS_TRACKER.PROCESS_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END RMSSUB_XORDER_SQL;
/