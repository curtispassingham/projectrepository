
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORD_PARENT_SUM_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------
--Function Name: GTT_POP_ORDITEM_SUM
--Purpose: Populates table gtt_orditem_sum based on a given
--         order number and supplier for display purposes
--         of form orditsumm.fmb.      
---------------------------------------------------------
FUNCTION POP_GTT_ORDITEM_SUM(O_error_message IN OUT VARCHAR2,
                             I_order_no      IN     ordloc.order_no%TYPE,
                             I_supplier      IN     item_supplier.supplier%TYPE)
                             RETURN BOOLEAN;
---------------------------------------------------------

END ORD_PARENT_SUM_SQL;
/
