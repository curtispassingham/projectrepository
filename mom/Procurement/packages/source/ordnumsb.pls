
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY ORDER_NUMBER_SQL IS
--------------------------------------------------------------------------------
FUNCTION NEXT_ORDER_NUMBER (O_error_message IN OUT VARCHAR2,
                            O_order_number  IN OUT ORDHEAD.ORDER_NO%TYPE) RETURN BOOLEAN IS

   L_order_sequence         ORDHEAD.ORDER_NO%TYPE;
   L_wrap_sequence_number   ORDHEAD.ORDER_NO%TYPE;
   L_first_time             VARCHAR2(3) := 'Yes';
   L_dummy                  VARCHAR2(1);

   cursor C_ORDHEAD_EXISTS is
      select 'x'
        from ordhead
       where order_no = O_order_number
          or master_po_no = O_order_number;

   cursor C_ORDPREISSUE_EXISTS is
      select 'x'
        from ord_preissue
       where order_no = O_order_number;

BEGIN

   LOOP
      select ORDER_SEQUENCE.NEXTVAL
        into L_order_sequence
        from sys.dual;

      if (L_first_time = 'Yes') then
          L_wrap_sequence_number := L_order_sequence;
          L_first_time := 'No';
      elsif (L_order_sequence = L_wrap_sequence_number) then
          O_error_message := 'Fatal error - no available order numbers';
          return FALSE;
      end if;

      O_order_number := L_order_sequence;

      open  C_ORDHEAD_EXISTS;
      fetch C_ORDHEAD_EXISTS into L_dummy;
      if (C_ORDHEAD_EXISTS%NOTFOUND) then
          open  C_ORDPREISSUE_EXISTS;
          fetch C_ORDPREISSUE_EXISTS into L_dummy;
          if (C_ORDPREISSUE_EXISTS%NOTFOUND) then
              close C_ORDPREISSUE_EXISTS;
              close C_ORDHEAD_EXISTS;
              exit;
          end if;
          close C_ORDPREISSUE_EXISTS;

      end if;
      close C_ORDHEAD_EXISTS;

   END LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER',
                                             TO_CHAR(SQLCODE));
      return FALSE;
END NEXT_ORDER_NUMBER;
---------------------------------------------------------------------------
FUNCTION PREISSUE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_ordnum_rec      IN OUT   "RIB_OrdNumColDesc_REC",
                   I_qty             IN       NUMBER,
                   I_expiry_date     IN       ORD_PREISSUE.EXPIRY_DATE%TYPE,
                   I_supplier        IN       ORD_PREISSUE.SUPPLIER%TYPE,
                   I_create_id       IN       ORD_PREISSUE.CREATE_ID%TYPE)
RETURN BOOLEAN IS

   L_program          VARCHAR2(60)               := 'ORDER_NUMBER_SQL.PREISSUE';
   L_counter          NUMBER(10)                 := 1;
   L_order_no         ORD_PREISSUE.ORDER_NO%TYPE := NULL;
   L_OrdNumDesc_rec   "RIB_OrdNumDesc_REC"       := NULL;
   L_OrdNumDesc_tbl   "RIB_OrdNumDesc_TBL"       := NULL;

BEGIN

   L_OrdNumDesc_tbl := "RIB_OrdNumDesc_TBL"(NULL);
   L_OrdNumDesc_tbl.DELETE;
   L_OrdNumDesc_rec := "RIB_OrdNumDesc_REC"(NULL,
                                            NULL,
                                            NULL,
                                            NULL);

   LOOP
      if ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER(O_error_message,
                                            L_order_no) = FALSE then
         return FALSE;
      end if;

      insert into ord_preissue (order_no,
                                expiry_date,
                                supplier,
                                create_id)
                        values (L_order_no,
                                I_expiry_date,
                                I_supplier,
                                I_create_id);

      L_OrdNumDesc_rec.supplier := I_supplier;
      L_OrdNumDesc_rec.expiry_date := I_expiry_date;
      L_OrdNumDesc_rec.order_no := L_order_no;

      L_OrdNumDesc_tbl.EXTEND;
      L_OrdNumDesc_tbl(L_OrdNumDesc_tbl.COUNT) := L_OrdNumDesc_rec;

      if L_counter = I_qty then
         exit;
      end if;
      L_counter := L_counter + 1;

   END LOOP;

   if L_OrdNumDesc_tbl is NOT NULL and L_OrdNumDesc_tbl.COUNT > 0 then
      O_ordnum_rec := "RIB_OrdNumColDesc_REC"(NULL,
                                              NULL);
      O_ordnum_rec.OrdNumDesc_TBL := L_OrdNumDesc_tbl;
      O_ordnum_rec.collection_size := L_OrdNumDesc_tbl.COUNT;
   else
      O_ordnum_rec := "RIB_OrdNumColDesc_REC"(0,      --rib_oid
                                              0,      --collection_size
                                              NULL);  --RIB_OrdNumDesc_TBL;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PREISSUE;
---------------------------------------------------------------------------
FUNCTION PREISSUE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_qty             IN       NUMBER,
                   I_expiry_date     IN       ORD_PREISSUE.EXPIRY_DATE%TYPE,
                   I_supplier        IN       ORD_PREISSUE.SUPPLIER%TYPE,
                   I_create_id       IN       ORD_PREISSUE.CREATE_ID%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(60)            := 'ORDER_NUMBER_SQL.PREISSUE';
   L_ordnum_rec   "RIB_OrdNumColDesc_REC" := NULL;

BEGIN

   if PREISSUE(O_error_message,
               L_ordnum_rec,
               I_qty,
               I_expiry_date,
               I_supplier,
               I_create_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END PREISSUE;
-------------------------------------------------------------------------------
FUNCTION GET_EXPIRY_DELAY_PRE_ISSUE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_expiry_delay_pre_issue  IN OUT PROCUREMENT_UNIT_OPTIONS.EXPIRY_DELAY_PRE_ISSUE%TYPE)
   RETURN BOOLEAN IS

   cursor C_DELAY is
      select expiry_delay_pre_issue
        from procurement_unit_options;

BEGIN
   open C_DELAY;
   fetch C_DELAY into O_expiry_delay_pre_issue;
   close C_DELAY;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_NUMBER_SQL.GET_EXPIRY_DELAY_PRE_ISSUE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_EXPIRY_DELAY_PRE_ISSUE;
--------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_PREISSUE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_preissue        IN OUT   VARCHAR2,
                               I_supplier        IN       ORD_PREISSUE.SUPPLIER%TYPE,
                               I_order_no        IN       ORD_PREISSUE.ORDER_NO%TYPE,
                               I_expiry_date     IN       ORD_PREISSUE.EXPIRY_DATE%TYPE)
   RETURN BOOLEAN IS

   L_expiry_date   ORD_PREISSUE.EXPIRY_DATE%TYPE   := NULL;
   L_dummy         VARCHAR2(1)                     := NULL;
   ---
   cursor C_ORDHEAD_EXISTS is
      select 'x'
        from ordhead
       where order_no = I_order_no;
   ---
   cursor C_ORDPREISSUE_EXISTS is
      select expiry_date
        from ord_preissue
       where supplier = I_supplier
         and order_no = I_order_no;
   ---
BEGIN
   if I_order_no is NULL then
      O_preissue := '0';
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORDPREISSUE_EXISTS',
                       'ord_preissue',
                       'order no : '||TO_CHAR(I_order_no) ||
                       ' supplier no: '||TO_CHAR(I_supplier));
      open C_ORDPREISSUE_EXISTS;

      SQL_LIB.SET_MARK('FETCH',
                       'C_ORDPREISSUE_EXISTS',
                       'ord_preissue',
                       'order no: '||TO_CHAR(I_order_no) ||
                       ' supplier no: '||TO_CHAR(I_supplier));
      fetch C_ORDPREISSUE_EXISTS into L_expiry_date;

      if L_expiry_date is NULL then
         O_preissue := '0';
      else
         -- Determine if the pre-issued order_no has already been used
         SQL_LIB.SET_MARK('OPEN',
                          'C_ORDHEAD_EXISTS',
                          'ordhead',
                          'order no: '||TO_CHAR(I_order_no));
         open C_ORDHEAD_EXISTS;

         SQL_LIB.SET_MARK('FETCH',
                          'C_ORDHEAD_EXISTS',
                          'ordhead',
                          'order no: '||TO_CHAR(I_order_no));
         fetch C_ORDHEAD_EXISTS into L_dummy;

         if L_dummy is NULL then
            -- Check to see if the pre-issed order_no has expired
            if I_expiry_date >= L_expiry_date then
               O_preissue := '-1';
               O_error_message := 'Warning error - Pre-issed order_no  '||to_char(I_order_no)||
                                  ' has expired!  Generated New Order Number ';
            else
               O_preissue := '1';
            end if;
         else
            O_preissue := '-1';
            O_error_message := 'Warning error - Order exist with Pre-issue order number '||
                               to_char(I_order_no)||'!  Generated New Order Number ';
         end if;

         SQL_LIB.SET_MARK('CLOSE',
                          'C_ORDHEAD_EXISTS',
                          'ordhead',
                          'order no: '||TO_CHAR(I_order_no));
         close C_ORDHEAD_EXISTS;

      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDPREISSUE_EXISTS',
                       'ord_preissue',
                       'order no: '||TO_CHAR(I_order_no) ||
                       ' supplier no: '||TO_CHAR(I_supplier));
      close C_ORDPREISSUE_EXISTS;

   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_NUMBER_SQL.CHECK_ORDER_PREISSUE',
                                             to_char(SQLCODE));
      return FALSE;
END CHECK_ORDER_PREISSUE;
--------------------------------------------------------------------------------------
END ORDER_NUMBER_SQL;
/
