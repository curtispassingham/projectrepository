CREATE OR REPLACE PACKAGE RMSSUB_XCOSTCHG AUTHID CURRENT_USER AS

----------------------------------------------------------------------------
   -- PACKAGE VARIABLES
----------------------------------------------------------------------------

   LP_mod_type       VARCHAR2(15) := 'xcostchgmod';
   LP_api_seq_no     NUMBER(10) := NULL;

----------------------------------------------------------------------------
   TYPE ROWID_TBL IS TABLE OF ROWID;

   TYPE PRICE_HIST_RECTYPE  IS RECORD(items                ITEM_TBL,
                                      locs                 LOC_TBL,
                                      loc_type             LOC_TYPE_TBL,
                                      unit_costs           UNIT_COST_TBL,
                                      retail_prices        UNIT_RETAIL_TBL);

   TYPE COST_CHANGE_RECTYPE IS RECORD(single_itemloc_ind   VARCHAR2(1),
                                      parent_item_ind      VARCHAR2(1),
                                      items_no_locs        ITEM_TBL,
                                      loc_items            ITEM_TBL,
                                      iscl_rows_tbl        ROWID_TBL,
                                      isc_rows_tbl         ROWID_TBL,
                                      price_hist_rec       PRICE_HIST_RECTYPE,
                                      soh_items            ITEM_TBL,
                                      packs                ITEM_TBL,
                                      hier_values          LOC_TBL);

----------------------------------------------------------------------------
PROCEDURE CONSUME (O_status_code     IN OUT   VARCHAR2,
                   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_message         IN       RIB_OBJECT,
                   I_message_type    IN       VARCHAR2);
----------------------------------------------------------------------------
   -- Function Name:  DELETE_API
   -- Purpose      :  This is used to delete API tables
----------------------------------------------------------------------------
FUNCTION DELETE_API(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN;
----------------------------------------------------------------------------
END RMSSUB_XCOSTCHG;
/
