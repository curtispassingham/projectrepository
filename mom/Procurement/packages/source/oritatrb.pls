CREATE OR REPLACE PACKAGE BODY ORDER_ITEM_ATTRIB_SQL AS
------------------------------------------------------------------------
FUNCTION GET_REF_ITEM(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_ref_item       IN OUT ORDSKU.REF_ITEM%TYPE,
                      I_order_no       IN     ORDSKU.ORDER_NO%TYPE,
                      I_item           IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

L_function  VARCHAR2(50)  := 'ORDER_ITEM_ATTRIB_SQL.GET_REF_ITEM';

   cursor C_REF_ITEM is
      select ref_item
        from ordsku
       where order_no = I_order_no
         and item     = I_item;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_REF_ITEM','ordsku',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_REF_ITEM;
   SQL_LIB.SET_MARK('FETCH','C_REF_ITEM','ordsku',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_REF_ITEM into O_ref_item;
   SQL_LIB.SET_MARK('CLOSE','C_REF_ITEM','ordsku',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_REF_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_function,
                                            to_char(SQLCODE));
      return FALSE;
END GET_REF_ITEM;
------------------------------------------------------------------------
FUNCTION GET_ALLOC_QTY(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_qty_allocated IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       I_order_no      IN     ORDLOC.ORDER_NO%TYPE,
                       I_item          IN     ORDLOC.ITEM%TYPE,
                       I_location      IN     ORDLOC.LOCATION%TYPE,
                       I_loc_type      IN     ORDLOC.LOC_TYPE%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.GET_ALLOC_QTY';

   cursor C_ALLOC_QTY is
      select SUM(NVL(ad.qty_allocated, 0))
        from alloc_detail ad,
             alloc_header ah
       where ah.order_no = I_order_no
         and ah.wh       = I_location
         and ah.item     = I_item
         and ah.alloc_no = ad.alloc_no;
BEGIN
   if I_loc_type = 'S' then
      O_qty_allocated := NULL;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ALLOC_QTY','alloc_header, alloc_detail',
                    'order no: '||to_char(I_order_no)||
                    ', wh: '||to_char(I_location)||
                    ', item: '||I_item);
   open C_ALLOC_QTY;
   ---
   SQL_LIB.SET_MARK('FETCH','C_ALLOC_QTY','alloc_header, alloc_detail',
                    'order no: '||to_char(I_order_no)||
                    ', wh: '||to_char(I_location)||
                    ', item: '||I_item);
   fetch C_ALLOC_QTY into O_qty_allocated;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ALLOC_QTY','alloc_header, alloc_detail',
                    'order no: '||to_char(I_order_no)||
                    ', wh: '||to_char(I_location)||
                    ', item: '||I_item);
   close C_ALLOC_QTY;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ALLOC_QTY;
------------------------------------------------------------------------
FUNCTION GET_ORIGIN_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN,
                            O_origin_country_id IN OUT ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                            I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item              IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.GET_ORIGIN_COUNTRY';

   cursor C_PO_ATTRIB is
      select os.origin_country_id
        from ordsku os
       where os.order_no = I_order_no
         and os.item     = I_item;
BEGIN
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_PO_ATTRIB','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_PO_ATTRIB;
   ---
   SQL_LIB.SET_MARK('FETCH','C_PO_ATTRIB','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_PO_ATTRIB into O_origin_country_id;
   if C_PO_ATTRIB%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_PO_ATTRIB','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_PO_ATTRIB;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORIGIN_COUNTRY;
------------------------------------------------------------------------
FUNCTION GET_HTS_ORIGIN_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists            IN OUT BOOLEAN,
                                O_origin_country_id IN OUT ORDSKU_HTS.ORIGIN_COUNTRY_ID%TYPE,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                I_item              IN     ORDSKU_HTS.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.GET_HTS_ORIGIN_COUNTRY';

   cursor C_PO_ATTRIB is
      select os.origin_country_id
        from ordsku_hts os
       where os.order_no = I_order_no
         and os.item     = I_item;
BEGIN
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_PO_ATTRIB','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_PO_ATTRIB;
   ---
   SQL_LIB.SET_MARK('FETCH','C_PO_ATTRIB','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_PO_ATTRIB into O_origin_country_id;
   if C_PO_ATTRIB%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_PO_ATTRIB','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_PO_ATTRIB;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_HTS_ORIGIN_COUNTRY;
------------------------------------------------------------------------
FUNCTION ITEM_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     O_exists            IN OUT BOOLEAN,
                     I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                     I_item              IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.ITEM_EXISTS';
   L_dummy      VARCHAR2(1);
   L_exists     BOOLEAN;

   cursor C_GET_ITEM is
      select 'x'
        from ordsku os,
             item_master im
       where os.order_no   = I_order_no
         and im.item       = os.item
         and (im.item                = I_item
              or im.item_parent      = I_item
              or im.item_grandparent = I_Item)
         and im.item_level = im.tran_level
         and im.status     = 'A';
BEGIN
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   if I_order_no is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_ITEM','ORDSKU',
                       'order no: '||to_char(I_order_no)||
                       ', item: '||I_item);
      open C_GET_ITEM;
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_ITEM','ORDSKU',
                       'order no: '||to_char(I_order_no)||
                       ', item: '||I_item);
      fetch C_GET_ITEM into L_dummy;
      if C_GET_ITEM%NOTFOUND then
         O_exists := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_ITEM','ORDSKU',
                       'order no: '||to_char(I_order_no)||
                       ', item: '||I_item);
      close C_GET_ITEM;
   else
      if ORDER_ATTRIB_SQL.ORDER_ITEM_EXISTS(O_error_message,
                                            O_exists,
                                            I_item) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_EXISTS;
------------------------------------------------------------------------
FUNCTION ORDER_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists            IN OUT BOOLEAN,
                      I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.ORDER_EXISTS';
   L_dummy      VARCHAR2(1);

   cursor C_GET_ORDER is
      select 'x'
        from ordsku
       where order_no = I_order_no;
BEGIN
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_GET_ORDER','ORDSKU',
                    'order no: '||to_char(I_order_no));
   open C_GET_ORDER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_ORDER','ORDSKU',
                    'order no: '||to_char(I_order_no));
   fetch C_GET_ORDER into L_dummy;
   if C_GET_ORDER%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER','ORDSKU',
                    'order no: '||to_char(I_order_no));
   close C_GET_ORDER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END ORDER_EXISTS;
------------------------------------------------------------------------
FUNCTION ITEM_RECD(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_item_recd      IN OUT BOOLEAN,
                   I_order_no       IN     ORDHEAD.ORDER_NO%TYPE,
                   I_item           IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

   L_dummy      VARCHAR2(1);
   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.ITEM_RECD';

   cursor C_RECD_ORDER_ITEM is
      select 'x'
        from ordloc
       where ordloc.order_no     = I_order_no
         and ordloc.item         = NVL(I_item,ordloc.item)
         and ordloc.qty_received > 0;
BEGIN
   O_item_recd := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_RECD_ORDER_ITEM','ORDLOC',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_RECD_ORDER_ITEM;
   ---
   SQL_LIB.SET_MARK('FETCH','C_RECD_ORDER_ITEM','ORDLOC',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_RECD_ORDER_ITEM into L_dummy;
   if C_RECD_ORDER_ITEM%NOTFOUND then
      O_item_recd := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_RECD_ORDER_ITEM','ORDLOC',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_RECD_ORDER_ITEM;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END ITEM_RECD;
------------------------------------------------------------------------
FUNCTION HTS_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    O_exists            IN OUT BOOLEAN,
                    I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                    I_item              IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.HTS_EXISTS';
   L_dummy      VARCHAR2(1);

   cursor C_GET_HTS is
      select 'x'
        from ordsku_hts
       where order_no      = I_order_no
         and (item         = I_item
              or pack_item = I_item);
BEGIN
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_GET_HTS','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_GET_HTS;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_HTS','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_GET_HTS into L_dummy;
   if C_GET_HTS%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_HTS','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_GET_HTS;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END HTS_EXISTS;
------------------------------------------------------------------------
FUNCTION GET_ORDSKU_PACK_SIZE(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_supp_pack_size IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                              I_item           IN     ORDSKU.ITEM%TYPE,
                              I_order_no       IN     ORDSKU.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.GET_ORDSKU_PACK_SIZE';

   cursor C_GET_SUPP_PACK_SIZE is
      select supp_pack_size
        from ordsku
       where order_no = I_order_no
         and item     = I_item;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_SUPP_PACK_SIZE','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_GET_SUPP_PACK_SIZE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_SUPP_PACK_SIZE','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_GET_SUPP_PACK_SIZE into O_supp_pack_size;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_SUPP_PACK_SIZE','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_GET_SUPP_PACK_SIZE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDSKU_PACK_SIZE;
------------------------------------------------------------------------
FUNCTION GET_QTY_RECEIVED (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists        IN OUT BOOLEAN,
                           O_qty_received  IN OUT ORDLOC.QTY_RECEIVED%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item          IN     ITEM_MASTER.ITEM%TYPE,
                           I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(64)              := 'ORDER_ITEM_ATTRIB_SQL.GET_QTY_RECEIVED';
   L_qty_received       ORDLOC.QTY_RECEIVED%TYPE;
   L_packitem_qty       V_PACKSKU_QTY.QTY%TYPE;

   cursor C_GET_QTY(L_item ITEM_MASTER.ITEM%TYPE) is
   select NVL(SUM(qty_received), 0)
        from ordloc
       where order_no = I_order_no
         and item     = L_item;

   cursor C_GET_PACKITEM_QTY is
   select qty
        from v_packsku_qty
       where pack_no  = I_pack_item
         and item     = I_item;
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   if I_pack_item is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_QTY','ORDLOC','order no: '||(I_order_no)||', item: '||I_pack_item);
      open C_GET_QTY(I_pack_item);
      SQL_LIB.SET_MARK('FETCH','C_GET_QTY','ORDLOC','order no: '||(I_order_no)||', item: '||I_pack_item);
      fetch C_GET_QTY into L_qty_received;
      ---
      if C_GET_QTY%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
         close C_GET_QTY;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||(I_order_no)||', item: '||I_pack_item);
      close C_GET_QTY;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      open C_GET_PACKITEM_QTY;
      SQL_LIB.SET_MARK('FETCH','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      fetch C_GET_PACKITEM_QTY into L_packitem_qty;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      close C_GET_PACKITEM_QTY;
      ---
      O_qty_received := L_qty_received * L_packitem_qty;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      open C_GET_QTY(I_item);
      SQL_LIB.SET_MARK('FETCH','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      ---
      fetch C_GET_QTY into O_qty_received;
      if C_GET_QTY%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
         close C_GET_QTY;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      close C_GET_QTY;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;                                            
END GET_QTY_RECEIVED;
------------------------------------------------------------------------
FUNCTION GET_QTY_ORDERED(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         O_qty_ordered   IN OUT ORDLOC.QTY_ORDERED%TYPE,
                         I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)              := 'ORDER_ITEM_ATTRIB_SQL.GET_QTY_ORDERED';
   L_qty_ordered        ORDLOC.QTY_ORDERED%TYPE;
   L_packitem_qty       V_PACKSKU_QTY.QTY%TYPE;

   cursor C_GET_QTY(L_item ITEM_MASTER.ITEM%TYPE) is
   select NVL(SUM(qty_ordered), 0)
        from ordloc
       where order_no = I_order_no
         and item     = L_item;

   cursor C_GET_PACKITEM_QTY is
   select qty
        from v_packsku_qty
       where pack_no  = I_pack_item
         and item     = I_item;
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   if I_pack_item is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_pack_item);
      open C_GET_QTY(I_pack_item);
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_pack_item);
      fetch C_GET_QTY into L_qty_ordered;
      if C_GET_QTY%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
         close C_GET_QTY;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_pack_item);
      close C_GET_QTY;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      open C_GET_PACKITEM_QTY;
      SQL_LIB.SET_MARK('FETCH','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      fetch C_GET_PACKITEM_QTY into L_packitem_qty;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      close C_GET_PACKITEM_QTY;
      ---
      O_qty_ordered := L_qty_ordered * L_packitem_qty;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      open C_GET_QTY(I_item);
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      fetch C_GET_QTY into O_qty_ordered;
      if C_GET_QTY%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
         close C_GET_QTY;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      close C_GET_QTY;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;                                            
END GET_QTY_ORDERED;
------------------------------------------------------------------------
FUNCTION GET_QTY_SHIPPED(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         O_qty_shipped   IN OUT SHIPSKU.QTY_EXPECTED%TYPE,
                         I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                         I_item          IN     ITEM_MASTER.ITEM%TYPE,
                         I_pack_item     IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64)              := 'ORDER_ITEM_ATTRIB_SQL.GET_QTY_SHIPPED';
   L_qty_shipped      SHIPSKU.QTY_EXPECTED%TYPE;
   L_packitem_qty     V_PACKSKU_QTY.QTY%TYPE;

   cursor C_GET_QTY(L_item ITEM_MASTER.ITEM%TYPE) is
   select NVL(SUM(qty_expected), 0)
        from shipment s,
             shipsku k
       where s.order_no = I_order_no
         and s.shipment = k.shipment
         and k.item     = L_item;

   cursor C_GET_PACKITEM_QTY is
   select qty
        from v_packsku_qty
       where pack_no  = I_pack_item
         and item     = I_item;
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   if I_pack_item is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_pack_item);
      open C_GET_QTY(I_pack_item);
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_pack_item);
      fetch C_GET_QTY into L_qty_shipped;
      if C_GET_QTY%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
         close C_GET_QTY;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_pack_item);
      close C_GET_QTY;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      open C_GET_PACKITEM_QTY;
      SQL_LIB.SET_MARK('FETCH','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      fetch C_GET_PACKITEM_QTY into L_packitem_qty;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PACKITEM_QTY','V_PACKSKU_QTY','pack: '||I_pack_item||', item: '||I_item);
      close C_GET_PACKITEM_QTY;
      ---
      O_qty_shipped := L_qty_shipped * L_packitem_qty;
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
      open C_GET_QTY(I_item);
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
      fetch C_GET_QTY into O_qty_shipped;
      if C_GET_QTY%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
         close C_GET_QTY;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
      close C_GET_QTY;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;                                            
END GET_QTY_SHIPPED;
------------------------------------------------------------------------
FUNCTION GET_UNIT_COST(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists          IN OUT   BOOLEAN,
                       O_unit_cost       IN OUT   ORDLOC.UNIT_COST%TYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_pack_item       IN       ITEM_MASTER.ITEM%TYPE,
                       I_location        IN       ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program            VARCHAR2(64)              := 'ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST';
   L_unit_cost          ORDLOC.UNIT_COST%TYPE;
   L_item               ITEM_MASTER.ITEM%TYPE;
   L_packitem_cost      V_PACKSKU_QTY.QTY%TYPE;
   L_total_pack_cost    V_PACKSKU_QTY.QTY%TYPE;
   L_supplier           SUPS.SUPPLIER%TYPE;
   L_origin_country_id  COUNTRY.COUNTRY_ID%TYPE;
   L_packitem_qty       V_PACKSKU_QTY.QTY%TYPE;
   L_total_pack_qty     ORDLOC.QTY_ORDERED%TYPE;

   cursor C_GET_COST is
      select SUM(unit_cost * DECODE(qty_ordered,0,nvl(qty_received,0),qty_ordered)) / DECODE(SUM(qty_ordered),0,DECODE(SUM(nvl(qty_received,0)),0,1,SUM(nvl(qty_received,0))),SUM(qty_ordered))
        from ordloc
       where order_no = I_order_no
         and item     = L_item
         and location = nvl(I_location, location);

   cursor C_GET_PACKITEM_COST is
      select (i.unit_cost * v.qty),
             v.qty
        from v_packsku_qty v,
             item_supp_country_loc i
       where v.pack_no           = I_pack_item
         and v.item              = I_item
         and v.item              = i.item
         and i.supplier          = L_supplier
         and i.origin_country_id = L_origin_country_id
         and ((i.loc = I_location and I_location is NOT NULL)
              or (i.primary_loc_ind = 'Y' and I_location is NULL));

   cursor C_SUM_PACKITEM_COST is
      select SUM(i.unit_cost * v.qty),
             SUM(v.qty)
        from v_packsku_qty v,
             item_supp_country_loc i
       where v.pack_no           = I_pack_item
         and v.item              = i.item
         and i.supplier          = L_supplier
         and i.origin_country_id = L_origin_country_id
         and ((i.loc = I_location and I_location is NOT NULL)
              or(i.primary_loc_ind = 'Y' and I_location is NULL));
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   if I_pack_item is NOT NULL then
      L_item := I_pack_item;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||(L_item));
      open C_GET_COST;
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||(L_item));
      fetch C_GET_COST into L_unit_cost;
      if C_GET_COST%NOTFOUND then
         O_exists := FALSE;
         SQL_LIB.SET_MARK('CLOSE','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||(L_item));
         close C_GET_COST;
         return TRUE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||(L_item));
      close C_GET_COST;
      ---
      if SUPP_ITEM_ATTRIB_SQL.GET_PRIMARY_SUPP_COUNTRY(O_error_message,
                                                       L_supplier,
                                                       L_origin_country_id,
                                                       I_item) = FALSE then
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      open C_GET_PACKITEM_COST;
      ---
      SQL_LIB.SET_MARK('FETCH','C_GET_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      fetch C_GET_PACKITEM_COST into L_packitem_cost,
                                     L_packitem_qty;
      SQL_LIB.SET_MARK('CLOSE','C_GET_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      close C_GET_PACKITEM_COST;
      ---
      SQL_LIB.SET_MARK('OPEN','C_SUM_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      open C_SUM_PACKITEM_COST;
      ---
      SQL_LIB.SET_MARK('FETCH','C_SUM_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      fetch C_SUM_PACKITEM_COST into L_total_pack_cost,
                                     L_total_pack_qty;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_SUM_PACKITEM_COST','V_PACKSKU_QTY,ITEM_SUPP_COUNTRY_LOC',NULL);
      close C_SUM_PACKITEM_COST;
      ---
      if L_total_pack_cost = 0 then
         O_unit_cost := L_unit_cost * ( 1 / L_total_pack_qty );
      else
         O_unit_cost := ((L_packitem_cost * L_unit_cost) / L_total_pack_cost)/L_packitem_qty;
      end if;
   else
      L_item := I_item;
      ---
      SQL_LIB.SET_MARK('OPEN','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      open C_GET_COST;
      SQL_LIB.SET_MARK('FETCH','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      fetch C_GET_COST into O_unit_cost;
      ---
      if C_GET_COST%NOTFOUND then
         O_exists := FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      close C_GET_COST;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_UNIT_COST;
------------------------------------------------------------------------
FUNCTION GET_SUPP_PACK_SIZE(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists            IN OUT BOOLEAN,
                            O_supp_pack_size    IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                            I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE';
   cursor C_GET_SUPP_PACK_SIZE is
      select /*+ USE_HASH(im,os)*/os.supp_pack_size
        from ordsku os,
                item_master im
      where os.order_no = I_order_no
         and os.item = im.item
         and (im.item = I_item
           or im.item_parent = I_item
           or im.item_grandparent = I_item);
BEGIN
   ---
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_SUPP_PACK_SIZE','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_GET_SUPP_PACK_SIZE;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_SUPP_PACK_SIZE','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_GET_SUPP_PACK_SIZE into O_supp_pack_size;
   ---
   if C_GET_SUPP_PACK_SIZE%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_SUPP_PACK_SIZE','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_GET_SUPP_PACK_SIZE;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_SUPP_PACK_SIZE;
------------------------------------------------------------------------
FUNCTION ORDER_ITEM_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_exists            IN OUT BOOLEAN,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item              IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS
   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.ORDER_ITEM_EXISTS';
   L_dummy      VARCHAR2(1);
   cursor C_GET_ORDER is
      select 'x'
        from ordsku
       where order_no = I_order_no
         and item     = I_item;
BEGIN
   -- Both order_no and item must be passed  into this function
   ---
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',NULL,NULL,NULL);
   end if;
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_GET_ORDER','ORDSKU',
                    'order no: '||to_char(I_order_no)||' item: '||I_item);
   open C_GET_ORDER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_ORDER','ORDSKU',
                    'order no: '||to_char(I_order_no)||' item: '||I_item);
   fetch C_GET_ORDER into L_dummy;
   if C_GET_ORDER%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER','ORDSKU',
                    'order no: '||to_char(I_order_no)||' item: '||I_item);
   close C_GET_ORDER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END ORDER_ITEM_EXISTS;
------------------------------------------------------------------------
FUNCTION GET_VALID_DIFF1(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT BOOLEAN,
                         O_diff_desc       IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                         I_diff1           IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_item_parent     IN     ITEM_MASTER.ITEM_PARENT%TYPE)
   RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  :=  'ORDER_ITEM_ATTRIB_SQL.GET_VALID_DIFF1';
   L_error_message  VARCHAR2(255);

   cursor C_GET_VALID_DIFF1 is
      select d.diff_desc
        from diff_ids d,
             item_master i
       where (i.item_parent         = I_item_parent
              or i.item_grandparent = I_item_parent)
         and d.diff_id              = I_diff1
         and i.diff_1               = d.diff_id;
BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_VALID_DIFF1','DIFF_IDS,ITEM_MASTER',
                    'diff_id: '||(I_diff1)||' item: '||(I_item_parent));
   open C_GET_VALID_DIFF1;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_VALID_DIFF1','DIFF_IDS,ITEM_MASTER',
                    'diff_id: '||(I_diff1)||' item: '||(I_item_parent));
   fetch C_GET_VALID_DIFF1 into O_diff_desc;
   ---
   if C_GET_VALID_DIFF1%NOTFOUND then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',NULL,NULL,NULL);
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_VALID_DIFF1','DIFF_IDS,ITEM_MASTER',
                    'diff_id: '||(I_diff1)||' item: '||(I_item_parent));
   close C_GET_VALID_DIFF1;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                   to_char(SQLCODE));
      return FALSE;
END GET_VALID_DIFF1;
------------------------------------------------------------------------
FUNCTION GET_VALID_DIFF2(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists          IN OUT BOOLEAN,
                         O_diff_desc       IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                         I_diff2           IN     DIFF_IDS.DIFF_ID%TYPE,
                         I_item_parent     IN     ITEM_MASTER.ITEM_PARENT%TYPE)
  RETURN BOOLEAN IS

   L_program        VARCHAR2(50)  :=  'ORDER_ITEM_ATTRIB_SQL.GET_VALID_DIFF2';
   L_error_message  VARCHAR2(255);

   cursor C_GET_VALID_DIFF2 is
      select d.diff_desc
        from diff_ids d,
             item_master i
       where (i.item_parent         = I_item_parent
              or i.item_grandparent = I_item_parent)
         and d.diff_id              = I_diff2
         and i.diff_2               = d.diff_id;

BEGIN
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_VALID_DIFF2','DIFF_IDS,ITEM_MASTER',
                    'diff_id: '||I_diff2||' item: '||I_item_parent);
   open C_GET_VALID_DIFF2;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_VALID_DIFF2','DIFF_IDS,ITEM_MASTER',
                    'diff_id: '||I_diff2||' item: '||I_item_parent);
   fetch C_GET_VALID_DIFF2 into O_diff_desc;
   ---
   if C_GET_VALID_DIFF2%NOTFOUND then
      O_exists := FALSE;
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_DIFF',NULL,NULL,NULL);
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_VALID_DIFF2','DIFF_IDS,ITEM_MASTER',
                    'diff_id: '||I_diff2||' item: '||I_item_parent);
   close C_GET_VALID_DIFF2;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_VALID_DIFF2;
------------------------------------------------------------------------
FUNCTION GET_ORDER_ITEM_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_exists            IN OUT BOOLEAN,
                                O_origin_country_id IN OUT ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                I_item              IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(50) := 'ORDER_ITEM_ATTRIB_SQL.GET_ORDER_ITEM_COUNTRY';

   cursor C_COUNTRY is
      select os.origin_country_id
        from ordsku os,
             item_master im
       where os.order_no             = I_order_no
         and os.item                 = im.item
         and (im.item                = I_item
              or im.item_parent      = I_item
              or im.item_grandparent = I_item);
BEGIN
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_COUNTRY','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_COUNTRY;
   ---
   SQL_LIB.SET_MARK('FETCH','C_COUNTRY','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_COUNTRY into O_origin_country_id;
   if C_COUNTRY%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_COUNTRY','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_COUNTRY;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDER_ITEM_COUNTRY;
------------------------------------------------------------------------
FUNCTION GET_HTS_ORDER_ITEM_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_exists            IN OUT BOOLEAN,
                                    O_origin_country_id IN OUT ORDSKU_HTS.ORIGIN_COUNTRY_ID%TYPE,
                                    I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                    I_item              IN     ORDSKU_HTS.ITEM%TYPE)
RETURN BOOLEAN IS

   L_function  VARCHAR2(50) := 'ORDER_ITEM_ATTRIB_SQL.GET_HTS_ORDER_ITEM_COUNTRY';

   cursor C_COUNTRY is
      select os.origin_country_id
        from ordsku_hts os,
             item_master im
       where os.order_no             = I_order_no
         and os.item                 = im.item
         and (im.item                = I_item
              or im.item_parent      = I_item
              or im.item_grandparent = I_item);
BEGIN
   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_COUNTRY','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_COUNTRY;
   ---
   SQL_LIB.SET_MARK('FETCH','C_COUNTRY','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_COUNTRY into O_origin_country_id;
   if C_COUNTRY%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_COUNTRY','ORDSKU_HTS',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_COUNTRY;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_HTS_ORDER_ITEM_COUNTRY;
------------------------------------------------------------------------
FUNCTION GET_COST_SOURCE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_exists        IN OUT BOOLEAN,
                         O_cost_source   IN OUT ORDLOC.COST_SOURCE%TYPE,
                         I_order_no      IN     ORDSKU.ORDER_NO%TYPE,
                         I_item          IN     ORDSKU.ITEM%TYPE,
                         I_location      IN     ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN IS

   L_cost_src_count   NUMBER := 0;

   cursor C_LOC_COST_SOURCE is
      select cost_source
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;

   cursor C_COUNT_COST_SOURCE is
      select count(distinct cost_source)
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

   cursor C_ITEM_COST_SOURCE is
      select distinct cost_source
        from ordloc
       where order_no = I_order_no
         and item     = I_item;
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_location is not NULL then
      SQL_LIB.SET_MARK('OPEN','C_LOC_COST_SOURCE','ORDLOC',
                       'order no: '|| to_char(I_order_no)||
                       ', item: '||I_item||', location: '||to_char(I_location));
      open C_LOC_COST_SOURCE;
      SQL_LIB.SET_MARK('FETCH','C_LOC_COST_SOURCE','ORDLOC',
                       'order no: '|| to_char(I_order_no)||
                       ', item: '||I_item||', location: '||to_char(I_location));
      fetch C_LOC_COST_SOURCE into O_cost_source;
      if C_LOC_COST_SOURCE%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_LOC_COST_SOURCE','ORDLOC',
                       'order no: '|| to_char(I_order_no)||
                       ', item: '||I_item||', location: '||to_char(I_location));
      close C_LOC_COST_SOURCE;
   else
      SQL_LIB.SET_MARK('OPEN','C_COUNT_COST_SOURCE','ORDLOC','order no: '|| to_char(I_order_no)||' item: '||I_item);
      open C_COUNT_COST_SOURCE;
      SQL_LIB.SET_MARK('FETCH','C_COUNT_COST_SOURCE','ORDLOC','order no: '|| to_char(I_order_no)||' item: '||I_item);
      fetch C_COUNT_COST_SOURCE into L_cost_src_count;
      if C_COUNT_COST_SOURCE%NOTFOUND then
         O_exists := FALSE;
      else
         O_exists := TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_COUNT_COST_SOURCE','ORDLOC','order no: '|| to_char(I_order_no)||' item: '||I_item);
      close C_COUNT_COST_SOURCE;
      ---
      if L_cost_src_count > 1 then
         O_cost_source := 'MULT';
      elsif L_cost_src_count = 1 then
         SQL_LIB.SET_MARK('OPEN','C_ITEM_COST_SOURCE','ORDLOC','order no: '|| to_char(I_order_no)||' item: '||I_item);
         open C_ITEM_COST_SOURCE;
         SQL_LIB.SET_MARK('FETCH','C_ITEM_COST_SOURCE','ORDLOC','order no: '|| to_char(I_order_no)||' item: '||I_item);
         fetch C_ITEM_COST_SOURCE into O_cost_source;
         SQL_LIB.SET_MARK('CLOSE','C_ITEM_COST_SOURCE','ORDLOC','order no: '|| to_char(I_order_no)||' item: '||I_item);
         close C_ITEM_COST_SOURCE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ITEM_ATTRIB_SQL.GET_COST_SOURCE',
                                             to_char(SQLCODE));
      return FALSE;
END GET_COST_SOURCE;
------------------------------------------------------------------------
FUNCTION GET_UNITS_COST(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_prescaled_units IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                        O_actual_units    IN OUT ORDLOC.QTY_ORDERED%TYPE,
                        O_prescaled_cost  IN OUT ORDLOC.UNIT_COST%TYPE,
                        O_actual_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                        I_order_no        IN     ORDLOC.ORDER_NO%TYPE,
                        I_alloc_no        IN     ALLOC_HEADER.ALLOC_NO%TYPE,
                        I_item            IN     ORDLOC.ITEM%TYPE,
                        I_location        IN     ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN is
   cursor C_ORDLOC is
      select sum(nvl(qty_prescaled, 0)),
             sum(nvl(qty_ordered, 0)),
             sum(nvl(unit_cost, 0) * nvl(qty_prescaled, 0)),
             sum(nvl(unit_cost, 0) * nvl(qty_ordered, 0))
        from ordloc
       where order_no = I_order_no
         and item     = I_item
         and location = I_location;
   ---
   cursor C_ALLOC is
      select sum(nvl(ad.qty_prescaled, 0)),
             sum(nvl(ad.qty_allocated, 0)),
             sum(nvl(ol.unit_cost, 0) * nvl(ad.qty_prescaled, 0)),
             sum(nvl(ol.unit_cost, 0) * nvl(ad.qty_allocated, 0))
        from alloc_detail ad,
             alloc_header ah,
             ordloc ol
       where ah.order_no = I_order_no
         and ah.item     = I_item
         and ad.to_loc   = I_location
         and ad.alloc_no = I_alloc_no
         and ol.order_no = ah.order_no
         and ad.alloc_no = ah.alloc_no
         and ol.item     = ah.item
         and ol.location = ah.wh;
BEGIN
   if I_order_no is null or I_item is null or I_location is null then
      O_error_message := SQL_LIB.CREATE_MSG ('INV_INPUT_GENERIC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   end if;
   ---
   if I_alloc_no is null then
      SQL_LIB.SET_MARK('OPEN',
                       'C_ORDLOC',
                       'ordloc',
                       'Order No: '||to_char(I_order_no)||
                       ', Item: '||I_item||
                       ', Location: '||to_char(I_location));
      open C_ORDLOC;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ORDLOC',
                       'ordloc',
                       'Order No: '||to_char(I_order_no)||
                       ', Item: '||I_item||
                       ', Location: '||to_char(I_location));
      fetch C_ORDLOC into O_prescaled_units,
                          O_actual_units,
                          O_prescaled_cost,
                          O_actual_cost;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_ORDLOC',
                       'ordloc',
                       'Order No: '||to_char(I_order_no)||
                       ', Item: '||I_item||
                       ', Location: '||to_char(I_location));
      close C_ORDLOC;
   else
      SQL_LIB.SET_MARK('OPEN',
                       'C_ALLOC',
                       'alloc_detail, alloc_header, ordloc',
                       'Order No: '||to_char(I_order_no)||
                       ', Item: '||I_item||
                       ', Location: '||to_char(I_location)||
                       ', Alloc No: '||to_char(I_alloc_no));
      open C_ALLOC;
      SQL_LIB.SET_MARK('FETCH',
                       'C_ALLOC',
                       'alloc_detail, alloc_header, ordloc',
                       'Order No: '||to_char(I_order_no)||
                       ', Item: '||I_item||
                       ', Location: '||to_char(I_location)||
                       ', Alloc No: '||to_char(I_alloc_no));
      fetch C_ALLOC into O_prescaled_units,
                         O_actual_units,
                         O_prescaled_cost,
                         O_actual_cost;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_ALLOC',
                       'alloc_detail, alloc_header, ordloc',
                       'Order No: '||to_char(I_order_no)||
                       ', Item: '||I_item||
                       ', Location: '||to_char(I_location)||
                       ', Alloc No: '||to_char(I_alloc_no));
      close C_ALLOC;
   end if;
   ---
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ITEM_ATTRIB_SQL.GET_UNITS_COST',
                                             to_char(SQLCODE));
      return FALSE;
END GET_UNITS_COST;
------------------------------------------------------------------------
FUNCTION GET_ORDSKU_NON_SCALING_IND(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_non_scale_ind     IN OUT ORDSKU.NON_SCALE_IND%TYPE,
                                    I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                                    I_item              IN     ORDSKU.ITEM%TYPE)
RETURN BOOLEAN IS
   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.GET_ORDSKU_NON_SCALING_IND';
   cursor C_NON_SCALE_IND is
      select os.non_scale_ind
        from ordsku os
       where os.order_no = I_order_no
         and os.item = I_item;
BEGIN
   SQL_LIB.SET_MARK('OPEN','C_NON_SCALE_IND','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   open C_NON_SCALE_IND;
   ---
   SQL_LIB.SET_MARK('FETCH','C_NON_SCALE_IND','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   fetch C_NON_SCALE_IND into O_non_scale_ind;
   if C_NON_SCALE_IND%NOTFOUND then
      O_non_scale_ind := 'N';
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_NON_SCALE_IND','ORDSKU',
                    'order no: '||to_char(I_order_no)||
                    ', item: '||I_item);
   close C_NON_SCALE_IND;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORDSKU_NON_SCALING_IND;
------------------------------------------------------------------------
FUNCTION GET_QTYS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                  O_exists        IN OUT BOOLEAN,
                  O_qty_ordered   IN OUT ORDLOC.QTY_ORDERED%TYPE,
                  O_qty_shipped   IN OUT SHIPSKU.QTY_EXPECTED%TYPE,
                  O_qty_prescaled IN OUT ORDLOC.QTY_PRESCALED%TYPE,
                  O_qty_received  IN OUT ORDLOC.QTY_RECEIVED%TYPE,
                  O_qty_cancelled IN OUT ORDLOC.QTY_CANCELLED%TYPE,
                  I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                  I_item          IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)  := 'ORDER_ITEM_ATTRIB_SQL.GET_QTYS';

   cursor C_GET_QTYS is
      select NVL(SUM(qty_ordered), 0),
             NVL(SUM(qty_prescaled), 0),
             NVL(SUM(qty_received), 0),
             NVL(SUM(qty_cancelled), 0)
        from ordloc
       where order_no = I_order_no
         and item     = I_item;

   cursor C_GET_SHIPPED_QTY is
   select NVL(SUM(qty_expected), 0)
        from shipment s,
             shipsku k
       where s.order_no = I_order_no
         and s.shipment = k.shipment
         and k.item     = I_item;

BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_QTYS','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
   open C_GET_QTYS;
   SQL_LIB.SET_MARK('FETCH','C_GET_QTYS','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
   fetch C_GET_QTYS into O_qty_ordered,
                         O_qty_prescaled,
                         O_qty_received,
                         O_qty_cancelled;
   ---
   if C_GET_QTYS%NOTFOUND then
      O_exists := FALSE;
      SQL_LIB.SET_MARK('CLOSE','C_GET_QTYS','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
      close C_GET_QTYS;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_QTYS','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
   close C_GET_QTYS;
   ---
   -- Get Qty Shipped.
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_SHIPPED_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
   open C_GET_SHIPPED_QTY;
   SQL_LIB.SET_MARK('FETCH','C_GET_SHIPPED_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
   fetch C_GET_SHIPPED_QTY into O_qty_shipped;
   ---
   if C_GET_SHIPPED_QTY%NOTFOUND then
      O_exists := FALSE;
      SQL_LIB.SET_MARK('CLOSE','C_GET_SHIPPED_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
      close C_GET_SHIPPED_QTY;
      return TRUE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_SHIPPED_QTY','SHIPMENT,SHIPSKU','order no: '||to_char(I_order_no)||', item: '||I_item);
   close C_GET_SHIPPED_QTY;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;                                            
END GET_QTYS;
------------------------------------------------------------------------
FUNCTION UPDATE_UNIT_COST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                          I_item          IN     ITEM_MASTER.ITEM%TYPE,
                          I_unit_cost     IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                          I_cost_source   IN     ORDLOC.COST_SOURCE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(60)             := 'ORDER_ITEM_ATTRIB_SQL.UPDATE_UNIT_COST';
   L_cost_source     ORDLOC.COST_SOURCE%TYPE  := 'MANL';
   L_location        ORDLOC.LOCATION%TYPE;
   L_unit_cost       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE;
   L_table           VARCHAR2(30)             := 'ORDLOC';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_GET_LOCS is
      select location
        from ordloc
       where order_no    = I_order_no
         and item        = I_item
         and cost_source = NVL(I_cost_source, cost_source);

   cursor C_GET_UNIT_COST is
      select unit_cost
        from item_supp_country_loc i,
             ordsku s,
             ordhead o
       where i.item              = I_item
         and o.order_no          = I_order_no
         and o.order_no          = s.order_no
         and o.supplier          = i.supplier
         and s.item              = i.item
         and s.origin_country_id = i.origin_country_id
         and i.loc               = L_location;

   cursor C_LOCK_ORDLOC is
      select 'x'
        from ordloc
       where order_no    = I_order_no
         and item        = I_item
         and location    = L_location
         and cost_source = NVL(I_cost_source, cost_source)
         for update nowait;
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   -- Need to loop through all locations for the item
   -- in order to determine what the cost source should
   -- be set to.  If the item/supp/ctry/loc unit cost
   -- is the same as the cost that the order/item is
   -- being changed to, then the cost source will be set
   -- to 'NORM' ('Supplier'), otherwise the cost source
   -- will be set to 'MANL' ('Manual').
   ---
   for C_rec in C_GET_LOCS loop
      L_location := C_rec.location;
      ---
      if I_cost_source is NULL then
         SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST',NULL,NULL);
         open C_GET_UNIT_COST;
         SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST',NULL,NULL);
         fetch C_GET_UNIT_COST into L_unit_cost;
         SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST',NULL,NULL);
         close C_GET_UNIT_COST;
         ---
         if L_unit_cost = I_unit_cost then
            L_cost_source := 'NORM';
         end if;
      else
         L_cost_source := I_cost_source;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLOC','ORDLOC',NULL);
      open C_LOCK_ORDLOC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLOC','ORDLOC',NULL);
      close C_LOCK_ORDLOC;
      ---
      SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLOC',NULL);
      update ordloc
         set unit_cost   = I_unit_cost,
             cost_source = L_cost_source
       where order_no    = I_order_no
         and item        = I_item
         and location    = L_location
         and cost_source = NVL(I_cost_source, cost_source);
   end loop;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order_no),
                                            I_item);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;                                            
END UPDATE_UNIT_COST;
------------------------------------------------------------------------
FUNCTION COST_SOURCE_EXISTS(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            I_order_no      IN     ORDSKU.ORDER_NO%TYPE,
                            I_item          IN     ORDSKU.ITEM%TYPE,
                            I_cost_source   IN     ORDLOC.COST_SOURCE%TYPE)
RETURN BOOLEAN IS

   L_exists    VARCHAR2(1) := 'N';

   cursor C_COST_SRC_EXISTS is
      select 'Y'
        from ordloc
       where order_no    = I_order_no
         and item        = I_item
         and cost_source = I_cost_source;

BEGIN
   if I_order_no is NULL or I_item is NULL or I_cost_source is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_COST_SRC_EXISTS','ORDLOC',NULL);
   open C_COST_SRC_EXISTS;
   SQL_LIB.SET_MARK('FETCH','C_COST_SRC_EXISTS','ORDLOC',NULL);
   fetch C_COST_SRC_EXISTS into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_COST_SRC_EXISTS','ORDLOC',NULL);
   close C_COST_SRC_EXISTS;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ITEM_ATTRIB_SQL.COST_SOURCE_EXISTS',
                                             to_char(SQLCODE));
      return FALSE;
END COST_SOURCE_EXISTS;
------------------------------------------------------------------------
FUNCTION UPDATE_PICKUP_LOC(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_pickup_loc    IN     ORDHEAD.PICKUP_LOC%TYPE,
                           I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_table           VARCHAR2(30) := 'ORDSKU';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDSKU is
      select 'x'
        from ordsku
       where order_no = I_order_no
         for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDSKU','ORDSKU','Order: '||to_char(I_order_no));
   open C_LOCK_ORDSKU;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDSKU','ORDSKU','Order: '||to_char(I_order_no));
   close C_LOCK_ORDSKU;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ORDSKU',NULL);
   update ordsku
      set pickup_loc = I_pickup_loc
    where order_no   = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ITEM_ATTRIB_SQL.UPDATE_PICKUP_LOC',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_PICKUP_LOC;
------------------------------------------------------------------------
FUNCTION UPDATE_PICKUP_NO(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_pickup_no     IN     ORDHEAD.PICKUP_NO%TYPE,
                          I_order_no      IN     ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_table           VARCHAR2(30)             := 'ORDSKU';
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(RECORD_LOCKED, -54);

   cursor C_LOCK_ORDSKU is
      select 'x'
        from ordsku
       where order_no = I_order_no
         for update nowait;

BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDSKU','ORDSKU','Order: '||to_char(I_order_no));
   open C_LOCK_ORDSKU;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDSKU','ORDSKU','Order: '||to_char(I_order_no));
   close C_LOCK_ORDSKU;
   ---
   SQL_LIB.SET_MARK('UPDATE',NULL,'ORDSKU',NULL);
   update ordsku
      set pickup_no = I_pickup_no
    where order_no  = I_order_no;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ITEM_ATTRIB_SQL.UPDATE_PICKUP_NO',
                                             to_char(SQLCODE));
      return FALSE;
END UPDATE_PICKUP_NO;
------------------------------------------------------------------------
FUNCTION ALLOCS_EXIST(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_exists        IN OUT BOOLEAN,
                      I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                      I_item          IN     ITEM_MASTER.ITEM%TYPE)
   RETURN BOOLEAN IS

   L_exists    VARCHAR2(1) := 'N';

   cursor C_ALLOCS_EXIST is
      select 'Y'
        from alloc_header
       where order_no = I_order_no
         and item     = I_item
         and rownum = 1;

BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := FALSE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_ALLOCS_EXIST','ALLOC_HEADER',NULL);
   open C_ALLOCS_EXIST;
   SQL_LIB.SET_MARK('FETCH','C_ALLOCS_EXIST','ALLOC_HEADER',NULL);
   fetch C_ALLOCS_EXIST into L_exists;
   SQL_LIB.SET_MARK('CLOSE','C_ALLOCS_EXIST','ALLOC_HEADER',NULL);
   close C_ALLOCS_EXIST;
   ---
   if L_exists = 'Y' then
      O_exists := TRUE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'ORDER_ITEM_ATTRIB_SQL.ALLOCS_EXIST',
                                             to_char(SQLCODE));
      return FALSE;
END ALLOCS_EXIST;
------------------------------------------------------------------------
FUNCTION GET_UNIT_COST_INIT(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_exists        IN OUT BOOLEAN,
                            O_unit_cost     IN OUT ORDLOC.UNIT_COST%TYPE,
                            I_order_no      IN     ORDHEAD.ORDER_NO%TYPE,
                            I_item          IN     ITEM_MASTER.ITEM%TYPE,
                            I_location      IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(75)  := 'ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST_INIT';

   cursor C_GET_COST is
   select SUM(NVL(unit_cost_init,1) * qty_ordered) / decode(SUM(qty_ordered), 0, 1, SUM(qty_ordered))
         from ordloc
        where order_no = I_order_no
          and item     = I_item
          and location = NVL(I_location, location);
BEGIN
   if I_order_no is NULL or I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   O_exists := TRUE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
   open C_GET_COST;
   SQL_LIB.SET_MARK('FETCH','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
   fetch C_GET_COST into O_unit_cost;
   ---
   if C_GET_COST%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_COST','ORDLOC','order no: '||to_char(I_order_no)||', item: '||I_item);
   close C_GET_COST;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;                                            
END GET_UNIT_COST_INIT;
------------------------------------------------------------------------
FUNCTION GET_ITEM_INFO(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_unit_cost         IN OUT ORDLOC.UNIT_COST%TYPE,
                       O_qty_ordered       IN OUT ORDLOC.QTY_ORDERED%TYPE,
                       O_supp_pack_size    IN OUT ORDSKU.SUPP_PACK_SIZE%TYPE,
                       O_origin_country_id IN OUT COUNTRY.COUNTRY_ID%TYPE,
                       I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                       I_item              IN     ITEM_MASTER.ITEM%TYPE,
                       I_location          IN     ORDLOC.LOCATION%TYPE,
                       I_origin_type       IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                       I_rev_no            IN     ORDLOC_REV.REV_NO%TYPE,
                       I_rev_date          IN     ORDLOC_REV.REV_DATE%TYPE)
   RETURN BOOLEAN IS

   
   L_is_rev_accp_qty      VARCHAR2(1)                 := 'X'; 
   L_rev_no_qty           ORDLOC_REV.REV_NO%TYPE      := NULL;
   L_origin_type_qty      ORDLOC_REV.ORIGIN_TYPE%TYPE := NULL;
   L_is_rev_accp_cost     VARCHAR2(1)                 := 'X'; 
   L_rev_no_cost          ORDLOC_REV.REV_NO%TYPE      := NULL;
   L_origin_type_cost     ORDLOC_REV.ORIGIN_TYPE%TYPE := NULL;
   L_rev_date             ORDLOC_REV.REV_DATE%TYPE    := NULL;

   cursor C_INFO_QTY is
      select ol.qty_ordered,
             os.supp_pack_size,
             os.origin_country_id
        from ordloc ol,
             ordsku os
       where ol.order_no = I_order_no
         and ol.item     = I_item
         and ol.location = I_location
         and ol.order_no = os.order_no
         and ol.item     = os.item;

cursor C_INFO_COST is
      select ol.unit_cost,
             os.supp_pack_size,
             os.origin_country_id
        from ordloc ol,
             ordsku os
       where ol.order_no = I_order_no
         and ol.item     = I_item
         and ol.location = I_location
         and ol.order_no = os.order_no
         and ol.item     = os.item;
        
   cursor C_IS_REV_ACCP_QTY is
      select 'X'
        from ordloc_rev olr
       where olr.order_no    = I_order_no
         and olr.item        = I_item
         and olr.location    = I_location
         and olr.origin_type = I_origin_type
         and olr.rev_no      = I_rev_no
         and olr.qty_status  <> 0;
         
   cursor C_IS_REV_ACCP_COST is
      select 'X'
        from ordloc_rev olr
       where olr.order_no    = I_order_no
         and olr.item        = I_item
         and olr.location    = I_location
         and olr.origin_type = I_origin_type
         and olr.rev_no      = I_rev_no
         and olr.cost_status  <> 0;         

   cursor C_MAX_REV_NO_QTY is
      select max(olri.rev_no) rev_no,
             olri.origin_type,
             olri.rev_date
        from ordloc_rev   olri
       where olri.order_no    = I_order_no
         and olri.item        = I_item
         and olri.location    = I_location
         and olri.origin_type = 'V'
         and ((I_origin_type = 'V' and olri.rev_no < I_rev_no) or 
              I_origin_type = 'R')
         and ((I_origin_type = 'V' and olri.rev_date <= I_rev_date) or
              (I_origin_type = 'R' and (olri.rev_date < I_rev_date or
               olri.rev_no = 0)))
       group by  olri.origin_type, olri.rev_date
       order by 3 desc;
       
   cursor C_MAX_REV_NO_COST is
      select max(olri.rev_no) rev_no,
             olri.origin_type,
             olri.rev_date
        from ordloc_rev   olri
       where olri.order_no    = I_order_no
         and olri.item        = I_item
         and olri.location    = I_location
         and olri.origin_type = 'V'
         and ((I_origin_type = 'V' and olri.rev_no < I_rev_no) or 
              I_origin_type = 'R')
         and ((I_origin_type = 'V' and olri.rev_date <= I_rev_date) or
              (I_origin_type = 'R' and (olri.rev_date < I_rev_date or
               olri.rev_no = 0)))
       group by  olri.origin_type, olri.rev_date
       order by 3 desc;      
   
   cursor C_OLD_INFO_QTY is
      select ol.qty_ordered,
             os.supp_pack_size,
             os.origin_country_id
        from ordloc_rev ol,
             ordsku_rev os
       where ol.order_no    = I_order_no
         and ol.item        = I_item
         and ol.location    = I_location
         and ol.order_no    = os.order_no
         and ol.item        = os.item
         and ol.rev_no      = os.rev_no
         and ol.origin_type = os.origin_type
         and ol.rev_no      = L_rev_no_qty
         and ol.origin_type = L_origin_type_qty;
         
   cursor C_OLD_INFO_COST is
      select ol.unit_cost,
             os.supp_pack_size,
             os.origin_country_id
        from ordloc_rev ol,
             ordsku_rev os
       where ol.order_no    = I_order_no
         and ol.item        = I_item
         and ol.location    = I_location
         and ol.order_no    = os.order_no
         and ol.item        = os.item
         and ol.rev_no      = os.rev_no
         and ol.origin_type = os.origin_type
         and ol.rev_no      = L_rev_no_cost
         and ol.origin_type = L_origin_type_cost;         


BEGIN
   if I_order_no is NULL or I_item is NULL or I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAMS',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   ---
   if I_origin_type = 'R' then
      --Check whether revision is accepted for quantity
      SQL_LIB.SET_MARK('OPEN',
                       'C_IS_REV_ACCP_QTY',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      open C_IS_REV_ACCP_QTY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_IS_REV_ACCP_QTY',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      fetch C_IS_REV_ACCP_QTY into L_is_rev_accp_qty;
      --
      if C_IS_REV_ACCP_QTY%NOTFOUND then
         L_is_rev_accp_qty := NULL;
      end if;
      --     
      SQL_LIB.SET_MARK('CLOSE',
                       'C_IS_REV_ACCP_QTY',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      close C_IS_REV_ACCP_QTY;
     
      --Check whether revision is accepted for cost
      SQL_LIB.SET_MARK('OPEN',
                       'C_IS_REV_ACCP_COST',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      open C_IS_REV_ACCP_COST;
      SQL_LIB.SET_MARK('FETCH',
                       'C_IS_REV_ACCP_COST',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      fetch C_IS_REV_ACCP_COST into L_is_rev_accp_cost;
      --
      if C_IS_REV_ACCP_COST%NOTFOUND then
         L_is_rev_accp_cost := NULL;
      end if;
      --
      SQL_LIB.SET_MARK('CLOSE',
                       'C_IS_REV_ACCP_COST',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      close C_IS_REV_ACCP_COST;
   end if;
   
   --if revision is not accepted for qty then fetch qty info from ORDLOC table
   if L_is_rev_accp_qty is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_INFO_QTY',
                       'ORDLOC,ORDSKU',
                       NULL);
      open C_INFO_QTY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_INFO_QTY',
                       'ORDLOC,ORDSKU',
                       NULL);
      fetch C_INFO_QTY into O_qty_ordered,
                            O_supp_pack_size,
                            O_origin_country_id;

      SQL_LIB.SET_MARK('CLOSE',
                       'C_INFO_QTY',
                       'ORDLOC,ORDSKU',
                       NULL);
      close C_INFO_QTY;
   end if;
    
   --if revision is not accepted for cost then fetch cost info from ORDLOC table
   if L_is_rev_accp_cost is NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_INFO_COST',
                       'ORDLOC,ORDSKU',
                       NULL);
      open C_INFO_COST;
      SQL_LIB.SET_MARK('FETCH',
                       'C_INFO_COST',
                       'ORDLOC,ORDSKU',
                       NULL);
      fetch C_INFO_COST into O_unit_cost,
                             O_supp_pack_size,
                             O_origin_country_id;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_INFO_COST',
                       'ORDLOC,ORDSKU',
                       NULL);
      close C_INFO_COST;
   end if;
   
   --find the max rev no when revision is accepted for qty
   if L_is_rev_accp_qty is not NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_MAX_REV_NO_QTY',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      open C_MAX_REV_NO_QTY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_MAX_REV_NO_QTY',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      fetch C_MAX_REV_NO_QTY into L_rev_no_qty,
                                  L_origin_type_qty,
                                  L_rev_date;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_MAX_REV_NO_QTY',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      close C_MAX_REV_NO_QTY;
   
      --find the old info
      SQL_LIB.SET_MARK('OPEN',
                       'C_OLD_INFO_QTY',
                       'ORDLOC,ORDSKU',
                       NULL);
      open C_OLD_INFO_QTY;
      SQL_LIB.SET_MARK('FETCH',
                       'C_OLD_INFO_QTY',
                       'ORDLOC,ORDSKU',
                       NULL);
      fetch C_OLD_INFO_QTY into O_qty_ordered,
                                O_supp_pack_size,
                                O_origin_country_id;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_OLD_INFO_QTY',
                       'ORDLOC,ORDSKU',
                       NULL);
      close C_OLD_INFO_QTY;
   end if;
   
   --find the max rev no when revision is accepted for cost
   if L_is_rev_accp_cost is not NULL then
      SQL_LIB.SET_MARK('OPEN',
                       'C_MAX_REV_NO_COST',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      open C_MAX_REV_NO_COST;
      SQL_LIB.SET_MARK('FETCH',
                       'C_MAX_REV_NO_COST',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      fetch C_MAX_REV_NO_COST into L_rev_no_cost,
                                   L_origin_type_cost,
                                   L_rev_date;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_MAX_REV_NO_COST',
                       'ORDLOC_REV',
                       'order_no: '||I_order_no);
      close C_MAX_REV_NO_COST;
   
      --find the old info
      SQL_LIB.SET_MARK('OPEN',
                       'C_OLD_INFO_COST',
                       'ORDLOC,ORDSKU',
                       NULL);
      open C_OLD_INFO_COST;
      SQL_LIB.SET_MARK('FETCH',
                       'C_OLD_INFO_COST',
                       'ORDLOC,ORDSKU',
                       NULL);
      fetch C_OLD_INFO_COST into O_unit_cost,
                                 O_supp_pack_size,
                                 O_origin_country_id;
      SQL_LIB.SET_MARK('CLOSE',
                       'C_OLD_INFO_COST',
                       'ORDLOC,ORDSKU',
                       NULL);
      close C_OLD_INFO_COST;
   end if;

   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ITEM_ATTRIB_SQL.GET_ITEM_INFO',
                                            to_char(SQLCODE));
      return FALSE;                                            
END GET_ITEM_INFO;
------------------------------------------------------------------------
FUNCTION GET_PREV_QTY_INFO(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_unit_cost         IN OUT ORDLOC.UNIT_COST%TYPE,
                           O_qty_ordered       IN OUT ORDLOC.QTY_ORDERED%TYPE,
                           I_revision_no       IN     ORDLOC_REV.REV_NO%TYPE,
                           I_origin_type       IN     ORDLOC_REV.ORIGIN_TYPE%TYPE,
                           I_order_no          IN     ORDHEAD.ORDER_NO%TYPE,
                           I_item              IN     ITEM_MASTER.ITEM%TYPE,
                           I_location          IN     ORDLOC.LOCATION%TYPE)
   RETURN BOOLEAN IS

   L_inv_param  VARCHAR2(30)            := NULL;
   L_program    TRAN_DATA.PGM_NAME%TYPE := 'ORDER_ITEM_ATTRIB_SQL.GET_PREV_QTY_INFO';

   cursor C_PREV_QTY_INFO is
   select olr.unit_cost,
          olr.qty_ordered
     from ordloc_rev olr
    where olr.rev_no      = I_revision_no - 1
      and olr.origin_type = DECODE(I_revision_no, 1, 'V', I_origin_type)
      and olr.order_no    = I_order_no
      and olr.item        = I_item
      and olr.location    = I_location;

BEGIN

   if I_revision_no is NULL then
      L_inv_param := 'I_revision_no';
   elsif I_origin_type is NULL then
      L_inv_param := 'I_origin_type';
   elsif I_order_no is NULL then
      L_inv_param := 'I_order_no';
   elsif I_item is NULL then
      L_inv_param := 'I_item';
   elsif I_location is NULL then
      L_inv_param := 'I_location';
   end if;

   if L_inv_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_inv_param,
                                            L_program,
                                            NULL);
     return FALSE;
   end if;
   --
   SQL_LIB.SET_MARK('OPEN','C_PREV_QTY_INFO','ORDLOC_REV',NULL);
   open C_PREV_QTY_INFO;
   SQL_LIB.SET_MARK('FETCH','C_PREV_QTY_INFO','ORDLOC_REV',NULL);
   fetch C_PREV_QTY_INFO into O_unit_cost,O_qty_ordered;
   SQL_LIB.SET_MARK('CLOSE','C_PREV_QTY_INFO','ORDLOC_REV',NULL);
   close C_PREV_QTY_INFO;
   ---
   return TRUE;

EXCEPTION
      when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'ORDER_ITEM_ATTRIB_SQL.GET_PREV_QTY_INFO',
                                            to_char(SQLCODE));
      return FALSE;

END GET_PREV_QTY_INFO;
------------------------------------------------------------------------
FUNCTION ORDLOC_EXISTS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       O_exists            IN OUT BOOLEAN,
                       I_order_no          IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_function   VARCHAR2(50)   := 'ORDER_ITEM_ATTRIB_SQL.ORDLOC_EXISTS';
   L_dummy      VARCHAR2(1);

   cursor C_GET_ORDER is
      select 'x'
        from ordloc
       where order_no = I_order_no
         and rownum = 1;
BEGIN
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_order_no',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   O_exists := TRUE;
   SQL_LIB.SET_MARK('OPEN','C_GET_ORDER','ORDLOC',
                    'order no: '||to_char(I_order_no));
   open C_GET_ORDER;
   ---
   SQL_LIB.SET_MARK('FETCH','C_GET_ORDER','ORDLOC',
                    'order no: '||to_char(I_order_no));
   fetch C_GET_ORDER into L_dummy;
   if C_GET_ORDER%NOTFOUND then
      O_exists := FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER','ORDLOC',
                    'order no: '||to_char(I_order_no));
   close C_GET_ORDER;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_function,
                                             to_char(SQLCODE));
      return FALSE;
END ORDLOC_EXISTS;
------------------------------------------------------------------------

FUNCTION ORD_NO_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_valid           IN OUT   BOOLEAN,
                       O_ordhead         IN OUT   V_ORDHEAD%ROWTYPE,
                       I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)       := 'ORDER_ITEM_ATTRIB.ORD_NO_EXISTS';

   cursor C_CHECK_ORDER_TB is
      select *
        from ordhead
       where order_no = I_order_no;

   cursor C_CHECK_ORDER_V is
      select *
        from v_ordhead
       where order_no = I_order_no;

v_ordhead2   ORDHEAD%ROWTYPE;     
BEGIN

   if I_order_no IS NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',I_order_no,L_program,NULL);
      RETURN FALSE;
   end if;

   O_valid := TRUE;

   open  C_CHECK_ORDER_V;
   fetch C_CHECK_ORDER_V into O_ordhead;
   close C_CHECK_ORDER_V;
   ---
   if O_ordhead.order_no is NULL then
      ---
      open  C_CHECK_ORDER_TB;
      fetch C_CHECK_ORDER_TB into O_ordhead;
      close C_CHECK_ORDER_TB;      
      ---
      if O_ordhead.order_no is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_SEARCH', I_order_no);
      else
         O_error_message := SQL_LIB.CREATE_MSG('NO_VISIBILITY_ORDER', I_order_no, NULL);
      end if;
      ---
      O_valid := FALSE;

   end if;

return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ORD_NO_EXISTS;
------------------------------------------------------------------------

FUNCTION OLD_UNIT_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_supplier_cost       IN OUT   ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                       I_pack_item           IN       PACKITEM_BREAKOUT.PACK_NO%TYPE,
                       I_item                IN       ORDSKU.ITEM%TYPE,     
                       I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                       I_location            IN       ORDHEAD.LOCATION%TYPE,
                       I_supplier            IN       ORDHEAD.SUPPLIER%TYPE,   
                       I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS
   L_program    VARCHAR2(100)   := 'ORDER_ITEM_ATTRIB_SQL.OLD_UNIT_COST';
   cursor C_GET_PACKITEM_COST is
      select i.unit_cost
        from v_packsku_qty v,
             item_supp_country_loc i
       where v.pack_no           = I_pack_item
         and v.item              = I_item
         and v.item              = i.item
         and i.supplier          = I_supplier
         and i.origin_country_id = I_origin_country_id
         and i.loc               = I_location;

BEGIN
   ---
   if I_pack_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_pack_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;                                                   
   ---
   if I_item is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_order',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;                                                   
   ---
   if I_supplier is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_supplier',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;                                                   
   ---
   if I_origin_country_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_origin_country_id',
                                             L_program,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN', 'C_GET_PACKITEM_COST', 'V_PACKSKU_QTY', 'pack item: '||I_pack_item||', item: '||I_item);
   open C_GET_PACKITEM_COST;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_PACKITEM_COST', 'V_PACKSKU_QTY', 'pack item: '||I_pack_item||', item: '||I_item);
   fetch C_GET_PACKITEM_COST into O_supplier_cost;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_PACKITEM_COST', 'V_PACKSKU_QTY', 'pack item: '||I_pack_item||', item: '||I_item);
   close C_GET_PACKITEM_COST;
   ---
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END OLD_UNIT_COST; 

------------------------------------------------------------------------
END ORDER_ITEM_ATTRIB_SQL;
/
