CREATE OR REPLACE PACKAGE BODY LC_SQL AS
-------------------------------------------------------------------------------------- 
FUNCTION VALID_LC_GET_INFO(O_error_message  IN OUT  VARCHAR2,
                           O_exist          IN OUT  BOOLEAN,
                           O_status         IN OUT  LC_HEAD.STATUS%TYPE,
                           O_lc_type        IN OUT  LC_HEAD.LC_TYPE%TYPE,
                           O_form_type      IN OUT  LC_HEAD.FORM_TYPE%TYPE,
                           O_country        IN OUT  LC_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                           O_currency       IN OUT  LC_HEAD.CURRENCY_CODE%TYPE,
                           O_applicant      IN OUT  LC_HEAD.APPLICANT%TYPE,
                           O_beneficiary    IN OUT  LC_HEAD.BENEFICIARY%TYPE,
                           O_issue_bank     IN OUT  LC_HEAD.ISSUING_BANK%TYPE,
                           O_advising_bank  IN OUT  LC_HEAD.ADVISING_BANK%TYPE,
                           IO_bank_lc_id    IN OUT  LC_HEAD.BANK_LC_ID%TYPE,
                           I_lc_ref_id      IN      LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(50) := 'LC_SQL.VALID_LC_GET_INFO';
   L_exists   VARCHAR2(1);

   cursor C_LC_HEAD is
      select status,
             lc_type,
             form_type,
             origin_country_id,
             currency_code,
             applicant,
             beneficiary,
             issuing_bank,
             advising_bank,
             bank_lc_id
        from lc_head
       where lc_ref_id = I_lc_ref_id;

   cursor C_BANK_LC_ID is
      select 'x'
        from lc_head
       where bank_lc_id = IO_bank_lc_id;

BEGIN
   O_exist := TRUE;

   --- if a LC Ref ID is passed in, then return corresponding details
   if I_lc_ref_id is not NULL then
      SQL_LIB.SET_MARK('OPEN', 'C_LC_HEAD', 'LC_HEAD',
                       'lc ref id: ' || to_char(I_lc_ref_id));
      open C_LC_HEAD;
      SQL_LIB.SET_MARK('FETCH', 'C_LC_HEAD', 'LC_HEAD',
                       'lc ref id: ' || to_char(I_lc_ref_id));
      fetch C_LC_HEAD into O_status,
                           O_lc_type,
                           O_form_type,
                           O_country,
                           O_currency,
                           O_applicant,
                           O_beneficiary,
                           O_issue_bank,
                           O_advising_bank,
                           IO_bank_lc_id;

      --- if no information was found, then invalid lc ref id was passed in
      if C_LC_HEAD%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_LC', NULL, NULL, NULL);
         O_exist         := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_LC_HEAD', 'LC_HEAD',
                       'lc ref id: ' || to_char(I_lc_ref_id));
      close C_LC_HEAD;
   elsif I_lc_ref_id is NULL and IO_bank_lc_id is not NULL then
      --- if a Bank LC ID is passed in, then verify the bank lc id exists
      SQL_LIB.SET_MARK('OPEN', 'C_BANK_LC_ID', 'LC_HEAD',
                       'bank lc id: ' || IO_bank_lc_id);
      open C_BANK_LC_ID;
      SQL_LIB.SET_MARK('FETCH', 'C_BANK_LC_ID', 'LC_HEAD',
                       'bank lc id: ' || IO_bank_lc_id);
      fetch C_BANK_LC_ID into L_exists;
      ---
      if C_BANK_LC_ID%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INVALID_BANK_LC_ID', NULL, NULL, NULL);
         O_exist         := FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_BANK_LC_ID', 'LC_HEAD',
                       'bank lc id: ' || IO_bank_lc_id);
      close C_BANK_LC_ID;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END VALID_LC_GET_INFO;

--------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message  IN OUT  VARCHAR2,
                       O_exist          IN OUT  BOOLEAN,
                       O_num_recs       IN OUT  NUMBER,
                       I_detail_type    IN      VARCHAR2,
                       I_lc_ref_id      IN      LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS


BEGIN

   if LC_SQL.DETAILS_EXIST(O_error_message,
                           O_exist,
                           O_num_recs,
                           NULL, --order number
                           I_detail_type,
                           I_lc_ref_id) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              'LC_SQL.DETAILS_EXIST',
                                              to_char(SQLCODE));
      return FALSE;
END DETAILS_EXIST;

--------------------------------------------------------------------------------------
FUNCTION NEXT_LC_REF_ID(O_error_message IN OUT VARCHAR2,
                        O_lc_ref_id     IN OUT LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_SQL.NEXT_LC_REF_ID';
   L_first_time  VARCHAR2(1)  := 'Y';
   L_exists      VARCHAR2(1);
   L_wrap_number LC_HEAD.LC_REF_ID%TYPE;
   L_lc_ref_id   LC_HEAD.LC_REF_ID%TYPE;

   cursor C_GET_NEXT_LC is
      select lc_ref_id_sequence.NEXTVAL
        from dual;

   cursor C_CHECK_LC is
      select 'x'
        from lc_head
       where lc_ref_id = L_lc_ref_id;
BEGIN
   LOOP
      --- Retrieve sequence number
      SQL_LIB.SET_MARK('OPEN','C_GET_NEXT_LC','DUAL',NULL);
      open C_GET_NEXT_LC;
      SQL_LIB.SET_MARK('FETCH','C_GET_NEXT_LC','DUAL',NULL);
      fetch C_GET_NEXT_LC into L_lc_ref_id;

      if C_GET_NEXT_LC%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT_LC','DUAL',NULL);
         close C_GET_NEXT_LC;
         O_error_message := SQL_LIB.CREATE_MSG('ERR_RETRIEVE_SEQ',NULL,NULL,NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_GET_NEXT_LC','DUAL',NULL);
      close C_GET_NEXT_LC;

      if (L_first_time = 'Y') then
         L_wrap_number := L_lc_ref_id;
         L_first_time := 'N';
      elsif (L_lc_ref_id = L_wrap_number) then
         O_error_message := SQL_LIB.CREATE_MSG('NO_SEQ_NO_AVAIL',NULL,NULL,NULL);
         return FALSE;
      end if;

      --- Check letter of credit reference existence
      SQL_LIB.SET_MARK('OPEN','C_CHECK_LC','LC_HEAD','lc ref id: '||to_char(L_lc_ref_id));
      open C_CHECK_LC;
      SQL_LIB.SET_MARK('FETCH','C_CHECK_LC','LC_HEAD','lc ref id: '||to_char(L_lc_ref_id));
      fetch C_CHECK_LC into L_exists;

      if C_CHECK_LC%NOTFOUND then
         SQL_LIB.SET_MARK('CLOSE','C_CHECK_LC','LC_HEAD','lc ref id: '||to_char(L_lc_ref_id));
         close C_CHECK_LC;
         O_lc_ref_id := L_lc_ref_id;
         EXIT;
      end if;

      SQL_LIB.SET_MARK('CLOSE','C_CHECK_LC','LC_HEAD','lc ref id: '||to_char(L_lc_ref_id));
      close C_CHECK_LC;
   END LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;
END NEXT_LC_REF_ID;

--------------------------------------------------------------------------------------
FUNCTION RECALC_COST(O_error_message       IN OUT VARCHAR2,
                     O_new_amount          IN OUT LC_DETAIL.COST%TYPE,
                     I_lc_ref_id           IN     LC_DETAIL.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'LC_SQL.RECALC_COST';

   cursor C_AMT_LC_DETAIL is
      select SUM(cost * NVL(qty,1))
        from lc_detail
       where lc_ref_id = I_lc_ref_id;

BEGIN

   SQL_LIB.SET_MARK('OPEN','C_AMT_LC_DETAIL','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   open C_AMT_LC_DETAIL;
   SQL_LIB.SET_MARK('FETCH','C_AMT_LC_DETAIL','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   fetch C_AMT_LC_DETAIL into O_new_amount;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_AMT_LC_DETAIL','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   close C_AMT_LC_DETAIL;
   ---
   if O_new_amount is NULL then
      O_new_amount := 0;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END RECALC_COST;

--------------------------------------------------------------------------------------
FUNCTION LOCK_LC_DETAILS(O_error_message  IN OUT VARCHAR2,
                         I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'LC_SQL.LOCK_LC_DETAILS';
   L_table      VARCHAR2(15);
   RECORD_LOCKED EXCEPTION;
   PRAGMA        exception_init (RECORD_LOCKED, -54);

   cursor C_LOCK_LC_DETAIL is
      select 'x'
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         for update nowait;

   cursor C_LOCK_LC_AMENDMENTS is
      select 'x'
        from lc_amendments
       where lc_ref_id = I_lc_ref_id
         for update nowait;

   cursor C_LOCK_LC_ACTIVITY is
      select 'x'
        from lc_activity
       where lc_ref_id = I_lc_ref_id
         for update nowait;

BEGIN
   --- lock req_docs table
   if DOCUMENTS_SQL.LOCK_REQ_DOCS(O_error_message,
                                  'PYMT',
                                  to_char(I_lc_ref_id),
                                  NULL) = FALSE then
      return FALSE;
   end if;

   --- lock lc_detail table
   L_table := 'LC_DETAIL';
   open C_LOCK_LC_DETAIL;
   close C_LOCK_LC_DETAIL;

   --- lock lc_amendments table
   L_table := 'LC_AMENDMENTS';
   open C_LOCK_LC_AMENDMENTS;
   close C_LOCK_LC_AMENDMENTS;

   --- lock lc_activity table
   L_table := 'LC_ACTIVITY';
   open C_LOCK_LC_ACTIVITY;
   close C_LOCK_LC_ACTIVITY;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('TABLE_LOCKED',
                                              L_table,
                                              to_char(I_lc_ref_id),
                                              NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END LOCK_LC_DETAILS;

--------------------------------------------------------------------------------------
FUNCTION DELETE_LC_DETAILS(O_error_message  IN OUT VARCHAR2,
                           I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'LC_SQL.DELETE_LC_DETAILS';

BEGIN
   --- delete child records from req_docs table
   if DOCUMENTS_SQL.DELETE_REQ_DOCS (O_error_message,
                                     'PYMT',
                                     to_char(I_lc_ref_id),
                                     NULL) = FALSE then
      return FALSE;
   end if;

   --- delete child records from lc_detail table
   delete from lc_detail
    where lc_ref_id = I_lc_ref_id;

   --- delete child records from lc_amendments table
   delete from lc_amendments
    where lc_ref_id = I_lc_ref_id;

   --- delete child records from lc_activity table
   delete from lc_activity
    where lc_ref_id = I_lc_ref_id;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END DELETE_LC_DETAILS;

--------------------------------------------------------------------------------------
FUNCTION LOCK_LCHEAD(O_error_message IN OUT VARCHAR2,
                     I_lc_ref_id     IN     LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_SQL.LOCK_LCHEAD';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        exception_init (RECORD_LOCKED, -54);

   cursor C_LOCK_LC_HEAD is
      select 'x'
        from lc_head
       where lc_ref_id = I_lc_ref_id
         for update nowait;

BEGIN
   open C_LOCK_LC_HEAD;
   close C_LOCK_LC_HEAD;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('LOCK_LC',
                                              NULL,
                                              NULL,
                                              NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END LOCK_LCHEAD;

--------------------------------------------------------------------------------------
FUNCTION UPDATE_LCHEAD(O_error_message       IN OUT VARCHAR2,
                       I_lc_ref_id           IN     LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'LC_SQL.UPDATE_LCHEAD';
   L_earliest_ship   LC_DETAIL.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship     LC_DETAIL.LATEST_SHIP_DATE%TYPE;
   L_transshp_ind    ORDLC.TRANSSHIPMENT_IND%TYPE            := 'N';
   L_partial_ind     ORDLC.PARTIAL_SHIPMENT_IND%TYPE         := 'N';
   L_order_no        LC_DETAIL.ORDER_NO%TYPE;
   L_origin_country  LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_country_old     LC_HEAD.ORIGIN_COUNTRY_ID%TYPE          := NULL;
   L_country_new     LC_HEAD.ORIGIN_COUNTRY_ID%TYPE          := NULL;
   L_update_amount   LC_HEAD.AMOUNT%TYPE                     := 0;
   L_form_type       LC_HEAD.STATUS%TYPE;
   L_vdate           PERIOD.VDATE%TYPE                       := GET_VDATE;

   cursor C_GET_TYPE is
      select form_type
        from lc_head
       where lc_ref_id = I_lc_ref_id;

   cursor C_GET_DATES is
      select MIN(earliest_ship_date),
             MAX(latest_ship_date)
        from lc_detail
       where lc_ref_id = I_lc_ref_id;

   cursor C_GET_DATES_LONG is
      select MIN(earliest_ship_date),
             MAX(latest_ship_date)
        from ordsku
       where order_no in (select order_no
                            from lc_detail
                           where lc_ref_id = I_lc_ref_id);

   cursor C_GET_TRANSSHP is
      select 'Y'
        from ordlc o,
             lc_detail l
       where l.lc_ref_id = I_lc_ref_id
         and o.order_no  = l.order_no
         and o.lc_ind    = 'Y'
         and o.transshipment_ind = 'Y';

   cursor C_PARTIAL_SHP is
      select 'Y'
        from ordlc o,
             lc_detail l
       where l.lc_ref_id = I_lc_ref_id
         and o.order_no  = l.order_no
         and o.lc_ind    = 'Y'
         and o.partial_shipment_ind = 'Y';

   cursor C_GET_ORDER is
      select order_no
        from lc_detail
       where lc_ref_id = I_lc_ref_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN','C_GET_TYPE','LC_HEAD',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   open C_GET_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_GET_TYPE','LC_HEAD',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   fetch C_GET_TYPE into L_form_type;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TYPE','LC_HEAD',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   close C_GET_TYPE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TRANSSHP','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   open C_GET_TRANSSHP;
   SQL_LIB.SET_MARK('FETCH','C_GET_TRANSSHP','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   fetch C_GET_TRANSSHP into L_transshp_ind;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TRANSSHP','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   close C_GET_TRANSSHP;
   ---
   SQL_LIB.SET_MARK('OPEN','C_PARTIAL_SHP','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   open C_PARTIAL_SHP;
   SQL_LIB.SET_MARK('FETCH','C_PARTIAL_SHP','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   fetch C_PARTIAL_SHP into L_partial_ind;
   SQL_LIB.SET_MARK('CLOSE','C_PARTIAL_SHP','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   close C_PARTIAL_SHP;
   ---
   if LC_SQL.RECALC_COST(O_error_message,
                         L_update_amount,
                         I_lc_ref_id) = FALSE then
      return FALSE;
   end if;

   -- Loop through the orders on lc_detail
   SQL_LIB.SET_MARK('LOOP','C_GET_ORDER','LC_DETAIL',NULL);
   ---
   FOR C_get_order_rec in C_GET_ORDER LOOP

      L_order_no := C_get_order_rec.order_no;
      ---
      if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(O_error_message,
                                                L_origin_country,
                                                L_order_no) = FALSE then
         return FALSE;
      end if;
      ---
      L_country_new := L_origin_country;
      ---
      if L_country_new = '99' then
         Exit;
      end if;
      ---
      if L_country_new != '99' then
         if L_country_old is NULL then
            L_country_old := L_country_new;
         end if;
         ---
         if L_country_new != L_country_old then
            L_country_new := '99';
            Exit;
         end if;
      end if;
   END LOOP;
   ---
   if L_form_type = 'S' then
      SQL_LIB.SET_MARK('OPEN','C_GET_DATES','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id));
      open C_GET_DATES;
      SQL_LIB.SET_MARK('FETCH','C_GET_DATES','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id));
      fetch C_GET_DATES into L_earliest_ship,
                             L_latest_ship;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DATES','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id));
      close C_GET_DATES;
      ---
   else
      SQL_LIB.SET_MARK('OPEN','C_GET_DATES_LONG','ORDSKU',NULL);
      open C_GET_DATES_LONG;
      SQL_LIB.SET_MARK('FETCH','C_GET_DATES_LONG','ORDLOC',NULL);
      fetch C_GET_DATES_LONG into L_earliest_ship,
                                  L_latest_ship;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DATES_LONG','ORDLOC',NULL);
      close C_GET_DATES_LONG;
   end if;
   ---
   update lc_head set earliest_ship_date   = NVL(L_earliest_ship, earliest_ship_date),
                      latest_ship_date     = NVL(L_latest_ship, latest_ship_date),
                      transshipment_ind    = NVL(L_transshp_ind, 'N'),
                      partial_shipment_ind = NVL(L_partial_ind, 'N'),
                      amount               = NVL(L_update_amount, 0),
                      origin_country_id    = L_country_new
                where lc_ref_id            = I_lc_ref_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END UPDATE_LCHEAD;

--------------------------------------------------------------------------------------
FUNCTION GET_LC_HEAD_INFO(O_error_message      IN OUT VARCHAR2,
                          O_exist              IN OUT BOOLEAN,
                          O_amount             IN OUT LC_HEAD.AMOUNT%TYPE,
                          O_earliest_ship_date IN OUT LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                          O_latest_ship_date   IN OUT LC_HEAD.LATEST_SHIP_DATE%TYPE,
                          O_expiration_date    IN OUT LC_HEAD.EXPIRATION_DATE%TYPE,
                          O_transshipment_ind  IN OUT LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                          O_part_shipment_ind  IN OUT LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                          O_origin_country_id  IN OUT LC_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                          I_lc_ref_id          IN     LC_HEAD.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(50) := 'LC_SQL.GET_LC_HEAD_INFO';

   cursor C_LC_HEAD_INFO is
      select amount,
             earliest_ship_date,
             latest_ship_date,
             expiration_date,
             transshipment_ind,
             partial_shipment_ind,
             origin_country_id
        from lc_head
       where lc_ref_id = I_lc_ref_id;

BEGIN
   O_exist := TRUE;

   SQL_LIB.SET_MARK('OPEN', 'C_LC_HEAD_INFO', 'LC_HEAD',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   open C_LC_HEAD_INFO;
   SQL_LIB.SET_MARK('FETCH', 'C_LC_HEAD_INFO', 'LC_HEAD',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   fetch C_LC_HEAD_INFO into O_amount,
                             O_earliest_ship_date,
                             O_latest_ship_date,
                             O_expiration_date,
                             O_transshipment_ind,
                             O_part_shipment_ind,
                             O_origin_country_id;

   --- if there is no information found, then O_exist is set to FALSE
   if C_LC_HEAD_INFO%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_LC',
                                             NULL,
                                             NULL,
                                             NULL);
      O_exist := FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE', 'C_LC_HEAD_INFO', 'LC_HEAD',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   close C_LC_HEAD_INFO;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_LC_HEAD_INFO;

--------------------------------------------------------------------------------------
FUNCTION LOCK_PO_DETAILS(O_error_message  IN OUT VARCHAR2,
                         I_lc_ref_id      IN     LC_DETAIL.LC_REF_ID%TYPE,
                         I_order_no       IN     LC_DETAIL.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_SQL.LOCK_PO_DETAILS';
   L_table       VARCHAR2(30);
   RECORD_LOCKED EXCEPTION;
   PRAGMA        exception_init (RECORD_LOCKED, -54);

   cursor C_LOCK_LC_AMEND is
      select 'x'
        from lc_amendments
       where lc_ref_id = I_lc_ref_id
         and order_no  = I_order_no
         for update nowait;

   cursor C_LOCK_LC_DETAIL is
      select 'x'
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         and order_no  = I_order_no
         for update nowait;

   cursor C_LOCK_ORD_LC_AMENDMENTS is
      select 'x'
        from ord_lc_amendments
       where order_no  = I_order_no
         for update nowait;

BEGIN
   --- lock req_docs table
   if DOCUMENTS_SQL.LOCK_REQ_DOCS(O_error_message,
                                  'PYMT',
                                  to_char(I_lc_ref_id),
                                  to_char(I_order_no)) = FALSE then
      return FALSE;
   end if;

   --- lock lc_amendments table
   L_table := 'LC_AMENDMENTS';
   SQL_LIB.SET_MARK ('OPEN', 'C_LOCK_LC_AMEND', 'LC_AMENDMENTS',
                     'lc ref id: ' || to_char(I_lc_ref_id) ||
                     ', Order No: ' || to_char(I_order_no));
   open C_LOCK_LC_AMEND;

   SQL_LIB.SET_MARK ('CLOSE', 'C_LOCK_LC_AMEND', 'LC_AMENDMENTS',
                     'lc ref id: ' || to_char(I_lc_ref_id) ||
                     ', Order No: ' || to_char(I_order_no));
   close C_LOCK_LC_AMEND;

   --- lock lc_detail table
   L_table := 'LC_DETAIL';
   SQL_LIB.SET_MARK ('OPEN', 'C_LOCK_LC_DETAIL', 'LC_DETAIL',
                     'lc ref id: ' || to_char(I_lc_ref_id) ||
                     ', Order No: ' || to_char(I_order_no));
   open C_LOCK_LC_DETAIL;

   SQL_LIB.SET_MARK ('CLOSE', 'C_LOCK_LC_DETAIL', 'LC_DETAIL',
                     'lc ref id: ' || to_char(I_lc_ref_id) ||
                     ', Order No: ' || to_char(I_order_no));
   close C_LOCK_LC_DETAIL;

   ---lock ord_lc_amendments table
   L_table := 'ORD_LC_AMENDMENTS';
   SQL_LIB.SET_MARK ('OPEN', 'C_LOCK_ORD_LC_AMENDMENTS', 'ORD_LC_AMENDMENTS',
                     'Order No: ' || to_char(I_order_no));
   open C_LOCK_ORD_LC_AMENDMENTS;

   SQL_LIB.SET_MARK ('CLOSE', 'C_LOCK_ORD_LC_AMENDMENTS', 'ORD_LC_AMENDMENTS',
                     'Order No: ' || to_char(I_order_no));
   close C_LOCK_ORD_LC_AMENDMENTS;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('TABLE_LOCKED',
                                              L_table,
                                              to_char(I_lc_ref_id),
                                              NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END LOCK_PO_DETAILS;

--------------------------------------------------------------------------------------
FUNCTION DELETE_PO_DETAILS(O_error_message IN OUT VARCHAR2,
                           I_lc_ref_id     IN     LC_DETAIL.LC_REF_ID%TYPE,
                           I_order         IN     LC_DETAIL.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program        VARCHAR2(50)          := 'LC_SQL.DELETE_PO_DETAILS';
   L_status         ORDHEAD.STATUS%TYPE;
   L_exists         BOOLEAN;
   ---
   L_table          VARCHAR2(30);
   RECORD_LOCKED    EXCEPTION;
   PRAGMA           EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ORDLC is
      select 'x'
        from ordlc
       where order_no = I_order
         for update nowait;


BEGIN
   --- delete from req_docs table
   if DOCUMENTS_SQL.DELETE_REQ_DOCS(O_error_message,
                                    'PYMT',
                                    to_char(I_lc_ref_id),
                                    to_char(I_order)) = FALSE then
      return FALSE;
   end if;

   --- delete from lc_amendments table
   SQL_LIB.SET_MARK ('DELETE', NULL, 'LC_AMENDMENTS',
                     'lc ref id: ' || to_char(I_lc_ref_id) ||
                     ', Order No: ' || to_char(I_order));
   delete from lc_amendments
    where lc_ref_id = I_lc_ref_id
      and order_no  = I_order;

   --- delete from lc_detail table
   SQL_LIB.SET_MARK ('DELETE', NULL, 'LC_DETAIL',
                     'lc ref id: ' || to_char(I_lc_ref_id) ||
                     ', Order No: ' || to_char(I_order));
   delete from lc_detail
    where lc_ref_id = I_lc_ref_id
      and order_no  = I_order;

   --- delete from ord_lc_amendment table
   SQL_LIB.SET_MARK ('DELETE', NULL, 'ORD_LC_AMENDMENTS',
                     'Order No: ' || to_char(I_order));
   delete from ord_lc_amendments
    where order_no = I_order;

   -- get status of order
   if ORDER_ATTRIB_SQL.GET_STATUS (O_error_message,
                                   L_exists,
                                   L_status,
                                   I_order) = FALSE then
      return FALSE;
   end if;
   ---
   if L_status = 'A' then
      if LC_SQL.WRITE_LCORDAPP(O_error_message,
                               TRUE,
                               I_order) = FALSE then
         return FALSE;
      end if;
   else  -- still update ordlc record
      L_table := 'ORDLC';
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLC','ORDLC',
                       'Order No: ' || to_char(I_order));
      open C_LOCK_ORDLC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLC','ORDLC',
                       'Order No: ' || to_char(I_order));
      close C_LOCK_ORDLC;

      SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLC',NULL);
      update ordlc set lc_ref_id = NULL,
                       lc_ind    = 'N'
                 where order_no = I_order;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            to_char(I_order),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END DELETE_PO_DETAILS;

--------------------------------------------------------------------------------------
FUNCTION CONVERT_AMOUNT(O_error_message     IN OUT  VARCHAR2,
                        O_amount_text       IN OUT  VARCHAR2,
                        I_capitalization    IN      VARCHAR2,
                        I_amount            IN      LC_HEAD.AMOUNT%TYPE,
                        I_currency_code     IN      LC_HEAD.CURRENCY_CODE%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_SQL.CONVERT_AMOUNT';
   L_exit        BOOLEAN;
   L_single_num  NUMBER;             --- holds the individual number (i.e. 1, 15, 20)
   L_group_num   NUMBER;             --- holds the number in millions, thousands, etc.
   L_temp        VARCHAR2(2000);     --- holds text of numbers (i.e. one)
   L_text        VARCHAR2(2000);     --- holds text of numbers (i.e. twenty one)
   L_amount_text VARCHAR2(2000);     --- holds text of whole amount (i.e. twenty one
                                     --- thousand two hundred and 17/100 USD)
   L_temp_text   VARCHAR2(2000);     --- holds text of numbers plus
                                     --- number group (i.e. twenty one thousand)
BEGIN
   --- index 1 limited to 5 (trillions)
   for i in reverse 1..5 loop
      L_temp_text := NULL;

     --- oracle can't handle trillions with the exponent
     if i = 5 then
        L_group_num := trunc(trunc(I_amount) / (1000000000000)) mod 1000;
     else
        L_group_num := trunc(trunc(I_amount) / (10 **((i-1)*3))) mod 1000;
     end if;

     --- convert numbers into words
     L_exit := FALSE;
     L_text   := NULL;

     for j in reverse 1..3 loop
        L_temp := NULL;

        L_single_num := trunc(trunc(L_group_num) / (10 ** (j-1))) mod 10;

        if j = 2 then
           if L_single_num = 1 then
              L_single_num := trunc(L_group_num) mod 100;
              if L_single_num = 10 then
                 L_temp := 'ten';
              elsif L_single_num = 11 then
                 L_temp := 'eleven';
              elsif L_single_num = 12 then
                 L_temp := 'twelve';
              elsif L_single_num = 13 then
                 L_temp := 'thirteen';
              elsif L_single_num = 14 then
                 L_temp := 'fourteen';
              elsif L_single_num = 15 then
                 L_temp := 'fifteen';
              elsif L_single_num = 16 then
                 L_temp := 'sixteen';
              elsif L_single_num = 17 then
                 L_temp := 'seventeen';
              elsif L_single_num = 18 then
                 L_temp := 'eighteen';
              elsif L_single_num = 19 then
                 L_temp := 'nineteen';
              end if;

              L_exit := TRUE;

           elsif L_single_num = 2 then
              L_temp := 'twenty';
           elsif L_single_num = 3 then
              L_temp := 'thirty';
           elsif L_single_num = 4 then
              L_temp := 'forty';
           elsif L_single_num = 5 then
              L_temp := 'fifty';
           elsif L_single_num = 6 then
              L_temp := 'sixty';
           elsif L_single_num = 7 then
              L_temp := 'seventy';
           elsif L_single_num = 8 then
              L_temp := 'eighty';
           elsif L_single_num = 9 then
              L_temp := 'ninety';
           end if;
        else
           if L_single_num = 1 then
              L_temp := 'one';
           elsif L_single_num = 2 then
              L_temp := 'two';
           elsif L_single_num = 3 then
              L_temp := 'three';
           elsif L_single_num = 4 then
              L_temp := 'four';
           elsif L_single_num = 5 then
              L_temp := 'five';
           elsif L_single_num = 6 then
              L_temp := 'six';
           elsif L_single_num = 7 then
              L_temp := 'seven';
           elsif L_single_num = 8 then
              L_temp := 'eight';
           elsif L_single_num = 9 then
              L_temp := 'nine';
           end if;

           if j = 3 and L_single_num > 0 then
              L_temp := L_temp || ' hundred';
           end if;
        end if;

        --- add words to the string representing the whole number
        if length(rtrim(L_temp)) > 0 then
           if length(rtrim(L_text)) > 0 and L_text is not NULL then
              L_text := L_text || ' ' || L_temp;
           else
              L_text := L_temp;
           end if;
        end if;

        exit when L_exit;
     end loop;

     L_temp_text := L_temp_text || L_text;

     if i = 5 and L_group_num > 0 then
        L_temp_text := L_temp_text || ' trillion';
     elsif i = 4 and L_group_num > 0 then
        L_temp_text := L_temp_text || ' billion';
     elsif i = 3 and L_group_num > 0 then
        L_temp_text := L_temp_text || ' million';
     elsif i = 2 and L_group_num > 0 then
        L_temp_text := L_temp_text || ' thousand';
     end if;

     --- add more words to the string representing the whole number
     if length (rtrim(L_temp_text)) > 0 and L_temp_text is not NULL then
        if length(rtrim(L_amount_text)) > 0 then
           L_amount_text := L_amount_text || ' ' || L_temp_text;
        else
           L_amount_text := L_temp_text;
        end if;
     end if;
  end loop;

   --- look of text (i.e. lowercase)
   if I_capitalization = 'L' or I_capitalization is NULL then
      NULL;
   elsif I_capitalization = 'U' then
      L_amount_text := upper(L_amount_text);
   elsif I_capitalization = 'I' then
      L_amount_text := initcap(L_amount_text);
   elsif I_capitalization = 'F' then
      L_amount_text := upper(substr(L_amount_text,1,1)) || substr(L_amount_text,2);
   else
      NULL;
   end if;

   --- round to two decimal places
   L_group_num := trunc(round(I_amount,2) * 100) mod 100;

   if length(rtrim(L_amount_text)) > 0 then
      L_amount_text := L_amount_text || ' and ' || to_char(L_group_num) || '/100' || ' ' || rtrim(I_currency_code);
   else
      L_amount_text := to_char(L_group_num) || '/100' || ' ' || rtrim(I_currency_code);
   end if;

   O_amount_text := L_amount_text;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CONVERT_AMOUNT;

-------------------------------------------------------------------------------------
FUNCTION GET_BANK_LC_ID(O_error_message     IN OUT  VARCHAR2,
                        O_bank_lc_id        IN OUT  LC_HEAD.BANK_LC_ID%TYPE,
                        I_order_no          IN      ORDHEAD.ORDER_NO%TYPE )
RETURN BOOLEAN IS
   L_program    VARCHAR2(50)  := 'LC_SQL.GET_BANK_LC_ID';
   ---
   cursor C_GET_LC_DETAIL is
      select lch.bank_lc_id
        from lc_head lch,
             lc_detail lcd
       where lch.lc_ref_id = lcd.lc_ref_id
         and lcd.order_no  = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_GET_LC_DETAIL', 'LC_HEAD', 'Order no: '||to_char(I_order_no));
   open  C_GET_LC_DETAIL;
   SQL_LIB.SET_MARK('FETCH', 'C_GET_LC_DETAIL', 'LC_HEAD', 'Order no: '||to_char(I_order_no));
   fetch C_GET_LC_DETAIL into O_bank_lc_id;
   SQL_LIB.SET_MARK('CLOSE', 'C_GET_LC_DETAIL', 'LC_HEAD', 'Order no: '||to_char(I_order_no));
   close C_GET_LC_DETAIL;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_BANK_LC_ID;

--------------------------------------------------------------------------------------
FUNCTION VALID_LC_ORDER(O_error_message        IN OUT  VARCHAR2,
                        O_valid                IN OUT  BOOLEAN,
                        I_applicant            IN      LC_HEAD.APPLICANT%TYPE,
                        I_beneficiary          IN      LC_HEAD.BENEFICIARY%TYPE,
                        I_purchase_type        IN      LC_HEAD.PURCHASE_TYPE%TYPE,
                        I_fob_title_pass       IN      LC_HEAD.FOB_TITLE_PASS%TYPE,
                        I_fob_title_pass_desc  IN      LC_HEAD.FOB_TITLE_PASS_DESC%TYPE,
                        I_lc_ref_id            IN      LC_DETAIL.LC_REF_ID%TYPE,
                        I_order_no             IN      ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_program          VARCHAR2(50) := 'LC_SQL.VALID_ORDER_LC';
   L_exists           VARCHAR2(1);
   L_lc_type          LC_HEAD.FORM_TYPE%TYPE;
   L_details_exists   BOOLEAN;
   L_num_recs         NUMBER;
   ---
   cursor C_CHECK_LC_HEAD is
      select 'x'
        from lc_head lch
       where lch.lc_ref_id    = I_lc_ref_id
         and status          != 'L'
         and applicant        = I_applicant
         and beneficiary      = I_beneficiary
         and purchase_type    = I_purchase_type
         and fob_title_pass   = I_fob_title_pass
         and UPPER(fob_title_pass_desc) = UPPER(I_fob_title_pass_desc);
   ---
   cursor C_GET_FORM_TYPE is
      select lc_type
        from lc_head
       where lc_ref_id = I_lc_ref_id;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_LC_HEAD', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
   open  C_CHECK_LC_HEAD;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_LC_HEAD', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
   fetch C_CHECK_LC_HEAD into L_exists;
   if L_exists is NULL then
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_LC_HEAD', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
      close C_CHECK_LC_HEAD;
      ---
      O_valid := FALSE;
   else
      ---
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_LC_HEAD', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
      close C_CHECK_LC_HEAD;
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_GET_FORM_TYPE', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
      open  C_GET_FORM_TYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_GET_FORM_TYPE', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
      fetch C_GET_FORM_TYPE into L_lc_type;
      SQL_LIB.SET_MARK('CLOSE', 'C_GET_FORM_TYPE', 'LC_HEAD', 'LC_REF_ID: '||to_char(I_lc_ref_id));
      close C_GET_FORM_TYPE;
      ---
      if L_lc_type = 'N' then
         ---
         if LC_SQL.DETAILS_EXIST(O_error_message,
                                 L_details_exists,
                                 L_num_recs,
                                 I_order_no,
                                 'D',
                                 I_lc_ref_id) = FALSE then
            return FALSE;
         end if;
         if L_details_exists = TRUE then
            O_valid := FALSE;
         else
            O_valid := TRUE;
         end if;
         ---
      else
         O_valid := TRUE;
      end if;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END VALID_LC_ORDER;

--------------------------------------------------------------------------------------
FUNCTION ATTACHED_LC(O_error_message    IN OUT VARCHAR2,
                     O_attached         IN OUT BOOLEAN,
                     I_order_no         IN     ORDHEAD.ORDER_NO%TYPE )
RETURN BOOLEAN IS
   L_program    VARCHAR2(50)  := 'LC_SQL.ATTACHED_LC';
   L_lc_ind     VARCHAR2(1);
   ---
   cursor C_CHECK_LC_IND is
      select lc_ind
        from ordlc
       where order_no = I_order_no;

BEGIN
   SQL_LIB.SET_MARK('OPEN', 'C_CHECK_LC_IND', 'ORDLC', 'Order no: '||to_char(I_order_no));
   open C_CHECK_LC_IND;
   SQL_LIB.SET_MARK('FETCH', 'C_CHECK_LC_IND', 'ORDLC', 'Order no: '||to_char(I_order_no));
   fetch C_CHECK_LC_IND into L_lc_ind;
   if L_lc_ind = 'Y' then
      O_attached := TRUE;
   else
      O_attached := FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_LC_IND', 'ORDLC', 'Order no: '||to_char(I_order_no));
   close C_CHECK_LC_IND;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ATTACHED_LC;

--------------------------------------------------------------------------------------
FUNCTION ADD_PO(O_error_message     IN OUT  VARCHAR2,
                I_lc_ref_id         IN      LC_HEAD.LC_REF_ID%TYPE,
                I_order_no          IN      ORDHEAD.ORDER_NO%TYPE,
                I_key_value_2       IN      REQ_DOC.KEY_VALUE_2%TYPE)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50) := 'LC_SQL.ADD_PO';
   L_exists                   BOOLEAN;
   L_origin_country_amend     BOOLEAN;
   L_lc_exp_days              SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE;
   L_seq_no                   LC_DETAIL.SEQ_NO%TYPE;
   L_req_doc_id               REQ_DOC.DOC_ID%TYPE;

   L_status                   LC_HEAD.STATUS%TYPE;
   L_form_type                LC_HEAD.FORM_TYPE%TYPE;
   L_lc_currency              LC_HEAD.CURRENCY_CODE%TYPE;
   L_lc_exchange_rate         LC_HEAD.EXCHANGE_RATE%TYPE;
   L_lc_earliest_ship_date    LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_lc_latest_ship_date      LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_lc_expiration_date       LC_HEAD.EXPIRATION_DATE%TYPE;
   L_lc_partial_shipment_ind  LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE;
   L_lc_transshipment_ind     LC_HEAD.TRANSSHIPMENT_IND%TYPE;
   L_lc_origin_country        LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_exchange_rate            LC_HEAD.EXCHANGE_RATE%TYPE;

   L_po_currency              ORDHEAD.CURRENCY_CODE%TYPE;
   L_po_exchange_rate         ORDHEAD.EXCHANGE_RATE%TYPE;
   L_po_earliest_ship_date    ORDHEAD.EARLIEST_SHIP_DATE%TYPE;
   L_po_latest_ship_date      ORDHEAD.LATEST_SHIP_DATE%TYPE;
   L_po_partial_shipment_ind  ORDLC.PARTIAL_SHIPMENT_IND%TYPE;
   L_po_transshipment_ind     ORDLC.TRANSSHIPMENT_IND%TYPE;
   L_po_origin_country        ORDSKU.ORIGIN_COUNTRY_ID%TYPE;
   L_merch_desc               LC_DETAIL.MERCH_DESC%TYPE;

   L_tsf_amend                LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_psf_amend                LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_esd_amend                LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_lsd_amend                LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_ed_amend                 LC_HEAD.EXPIRATION_DATE%TYPE;
   L_oc_amend                 LC_AMENDMENTS.NEW_VALUE%TYPE;

   L_po_total_cost            ORDLOC.UNIT_COST%TYPE := 0;
   L_prescaled_cost           ORDLOC.UNIT_COST%TYPE := 0;

   L_outstanding_cost         ORDLOC.UNIT_COST%TYPE;
   L_cancel_cost              ORDLOC.UNIT_COST%TYPE;

   L_po_item                  ORDLOC.ITEM%TYPE;
   L_po_item_qty              ORDLOC.QTY_ORDERED%TYPE;
   L_item_order_qty           ORDLOC.QTY_ORDERED%TYPE;
   L_item_order_cost          ORDLOC.UNIT_COST%TYPE;
   L_lading_port              ORDHEAD.LADING_PORT%TYPE;
   L_discharge_port           ORDHEAD.DISCHARGE_PORT%TYPE;   
   
   cursor C_GET_LC_INFO is
      select status,
             form_type,
             currency_code,
             exchange_rate,
             transshipment_ind,
             partial_shipment_ind,
             earliest_ship_date,
             latest_ship_date,
             expiration_date,
             origin_country_id
        from lc_head
       where lc_ref_id = I_lc_ref_id;

   cursor C_GET_ORDER_INFO is
      select oh.currency_code,
             oh.exchange_rate,
             oh.earliest_ship_date,
             oh.latest_ship_date,
             ol.merch_desc,
             ol.transshipment_ind,
             ol.partial_shipment_ind,
             oh.lading_port,
             oh.discharge_port             
        from ordhead oh,
             ordlc  ol
        where oh.order_no = I_order_no
          and ol.order_no = I_order_no;

   cursor C_GET_MAX_SEQ is
      select nvl(MAX(seq_no),0) + 1
        from lc_detail
       where lc_ref_id = I_lc_ref_id;

   cursor C_ITEM_ON_ORDER is
      select os.item,
             os.earliest_ship_date,
             os.latest_ship_date,
             SUM(o.qty_ordered) qty
        from ordloc o,
             ordsku os
       where o.order_no = I_order_no
         and os.order_no = o.order_no
         and o.item  = os.item
       group by os.item,
                os.earliest_ship_date,
                os.latest_ship_date;

   cursor C_REQ_DOC is
      select distinct r.doc_id
        from req_doc  r,
             doc_link dl,
             doc      d
       where d.lc_ind      = 'Y'
         and r.doc_id      = d.doc_id
         and d.doc_type    = dl.doc_type
         and dl.module     in ('LC','LCA')
         and ((r.module    = 'PO'
         and r.key_value_1 = to_char(I_order_no)
         and r.key_value_2 is NULL)
          or (r.module     = 'POIT'
         and r.key_value_1 = to_char(I_order_no)
         and r.key_value_2 is not NULL)
          or (r.module     = 'PTNR'
         and r.key_value_1 = 'BK'
         and r.key_value_2 = I_key_value_2))
         and r.doc_id not in (select doc_id
                                from req_doc
                               where (module = 'LC'
                                 and  key_value_1 = to_char(I_lc_ref_id)
                                 and  key_value_2 is NULL)
                                  or (module = 'LCA'
                                 and  key_value_1 = to_char(I_lc_ref_id)
                                 and  key_value_2 is NULL));

   cursor C_LOCK_ORDLC is
      select 'x'
        from ordlc
       where order_no = I_order_no
         for update nowait;

BEGIN
   if I_lc_ref_id is NULL or
      I_order_no  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   --- get letter of credit information
   SQL_LIB.SET_MARK('OPEN','C_GET_LC_INFO','LC_HEAD','LC Ref ID: '
                    || to_char(I_lc_ref_id));
   open C_GET_LC_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_LC_INFO','LC_HEAD','LC Ref ID: '
                    || to_char(I_lc_ref_id));
   fetch C_GET_LC_INFO into L_status,
                            L_form_type,
                            L_lc_currency,
                            L_lc_exchange_rate,
                            L_lc_transshipment_ind,
                            L_lc_partial_shipment_ind,
                            L_lc_earliest_ship_date,
                            L_lc_latest_ship_date,
                            L_lc_expiration_date,
                            L_lc_origin_country;

   if C_GET_LC_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_LC_INFO','LC_HEAD','LC Ref ID: '
                       || to_char(I_lc_ref_id));
      close C_GET_LC_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_LC_REF_ID',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_LC_INFO','LC_HEAD','LC Ref ID: '
                    || to_char(I_lc_ref_id));
   close C_GET_LC_INFO;

   --- puchase orders can not be attached to a closed letter of credit
   if L_status = 'L' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_ATTACH_PO',NULL,NULL,NULL);
      return FALSE;
   end if;

   --- get order information
   SQL_LIB.SET_MARK('OPEN','C_GET_ORDER_INFO','ORDHEAD, ORDLC',
                    'Order Number: ' || to_char(I_order_no));
   open C_GET_ORDER_INFO;
   SQL_LIB.SET_MARK('FETCH','C_GET_ORDER_INFO','ORDHEAD, ORDLC',
                    'Order Number: ' || to_char(I_order_no));
   fetch C_GET_ORDER_INFO into L_po_currency,
                               L_po_exchange_rate,
                               L_po_earliest_ship_date,
                               L_po_latest_ship_date,
                               L_merch_desc,
                               L_po_transshipment_ind,
                               L_po_partial_shipment_ind,
                               L_lading_port,
                               L_discharge_port;

   if C_GET_ORDER_INFO%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER_INFO','ORDHEAD, ORDLC',
                       'Order Number: ' || to_char(I_order_no));
      close C_GET_ORDER_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_GET_ORDER_INFO','ORDHEAD, ORDLC',
                    'Order Number: ' || to_char(I_order_no));
   close C_GET_ORDER_INFO;

   if SYSTEM_OPTIONS_SQL.GET_LC_EXP_DAYS(O_error_message,
                                         L_lc_exp_days) = FALSE then
      return FALSE;
   end if;

   if L_status = 'W' then   --- letter of credit in worksheet status (lc_detail table)
      if L_form_type = 'S' then    --- 'S'hort form deals with order totals
         if ORDER_CALC_SQL.TOTAL_COSTS(O_error_message,
                                       L_po_total_cost,
                     L_prescaled_cost,
                                       L_outstanding_cost,
                                       L_cancel_cost,
                                       I_order_no,
                     NULL,
                     NULL) = FALSE then
            return FALSE;
         end if;
         --- convert order cost to letter of credit currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_po_total_cost,
                                 L_po_currency,
                                 L_lc_currency,
                                 L_po_total_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 L_po_exchange_rate,
                                 L_lc_exchange_rate) = FALSE then
            return FALSE;
         end if;
         --- calculate next sequence number
         SQL_LIB.SET_MARK('OPEN','C_GET_MAX_SEQ','LC_DETAIL','LC Ref ID: '
                          || to_char(I_lc_ref_id));
         open C_GET_MAX_SEQ;
         SQL_LIB.SET_MARK('FETCH','C_GET_MAX_SEQ','LC_DETAIL','LC Ref ID: '
                          || to_char(I_lc_ref_id));
         fetch C_GET_MAX_SEQ into L_seq_no;
         SQL_LIB.SET_MARK('CLOSE','C_GET_MAX_SEQ','LC_DETAIL','LC Ref ID: '
                          || to_char(I_lc_ref_id));
         close C_GET_MAX_SEQ;

         SQL_LIB.SET_MARK('INSERT',NULL,'LC_DETAIL',NULL);
         insert into lc_detail(lc_ref_id,
                               seq_no,
                               order_no,
                               item,
                               cost,
                               qty,
                               earliest_ship_date,
                               latest_ship_date,
                               merch_desc,
                               comments)
                        values(I_lc_ref_id,
                               L_seq_no,
                               I_order_no,
                               NULL,
                               L_po_total_cost,
                               NULL,
                               L_po_earliest_ship_date,
                               L_po_latest_ship_date,
                               L_merch_desc,
                               NULL);

      elsif L_form_type = 'L' then   --- 'L'ong form deals with item totals
         for C_ITEM_ON_ORDER_REC in C_ITEM_ON_ORDER loop
            L_po_item                := C_ITEM_ON_ORDER_REC.item;
            L_po_earliest_ship_date := C_ITEM_ON_ORDER_REC.earliest_ship_date;
            L_po_latest_ship_date   := C_ITEM_ON_ORDER_REC.latest_ship_date;
            L_item_order_qty         := C_ITEM_ON_ORDER_REC.qty;

            if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                   L_exists,
                                                   L_item_order_cost,
                                                   I_order_no,
                                                   L_po_item,
                                                   NULL,                --  I_pack_item
                                                   NULL) = FALSE then   -- I_location
               return FALSE;
            end if;

            --- convert order cost to letter of credit currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_item_order_cost,
                                    L_po_currency,
                                    L_lc_currency,
                                    L_item_order_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    L_po_exchange_rate,
                                    L_lc_exchange_rate) = FALSE then
               return FALSE;
            end if;

            SQL_LIB.SET_MARK('OPEN','C_GET_MAX_SEQ','LC_DETAIL','LC Ref ID: '
                             || to_char(I_lc_ref_id));
            open C_GET_MAX_SEQ;
            SQL_LIB.SET_MARK('FETCH','C_GET_MAX_SEQ','LC_DETAIL','LC Ref ID: '
                             || to_char(I_lc_ref_id));
            fetch C_GET_MAX_SEQ into L_seq_no;
            SQL_LIB.SET_MARK('CLOSE','C_GET_MAX_SEQ','LC_DETAIL','LC Ref ID: '
                             || to_char(I_lc_ref_id));
            close C_GET_MAX_SEQ;

            SQL_LIB.SET_MARK('INSERT',NULL,'LC_DETAIL',NULL);
            insert into lc_detail(lc_ref_id,
                                  seq_no,
                                  order_no,
                                  item,
                                  cost,
                                  qty,
                                  earliest_ship_date,
                                  latest_ship_date,
                                  merch_desc,
                                  comments)
                           values(I_lc_ref_id,
                                  L_seq_no,
                                  I_order_no,
                                  L_po_item,
                                  L_item_order_cost,
                                  L_item_order_qty,
                                  L_po_earliest_ship_date,
                                  L_po_latest_ship_date,
                                  NULL,
                                  NULL);

            --- populate variable to hold total order cost
            L_po_total_cost := L_po_total_cost + (L_item_order_cost * L_item_order_qty);

         end loop;
      end if;  --- form type (short / long)

      --- adding a purchase order, then check for affected values
      SQL_LIB.SET_MARK('OPEN','C_REQ_DOC','REQ_DOC',NULL);
      for C_REQ_DOC_REC in C_REQ_DOC loop
         L_req_doc_id := C_REQ_DOC_REC.doc_id;

         if DOCUMENTS_SQL.INSERT_REQ_DOC(O_error_message,
                                         'LC',
                                         to_char(I_lc_ref_id),
                                         NULL,
                                         L_req_doc_id,
                                         NULL) = FALSE then
            return FALSE;
         end if;
      end loop;

      if L_lc_origin_country != '99' or L_lc_origin_country is NULL then
         if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(O_error_message,
                                                   L_po_origin_country,
                                                   I_order_no) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_lc_origin_country != L_po_origin_country then
         L_lc_origin_country := '99';    --- multiple origin countries
      end if;

      if L_po_transshipment_ind = 'Y' and
         L_lc_transshipment_ind = 'N' then
         L_lc_transshipment_ind := 'Y';
      end if;

      if L_po_partial_shipment_ind = 'Y' and
         L_lc_partial_shipment_ind = 'N' then
         L_lc_partial_shipment_ind := 'Y';
      end if;

      if L_po_earliest_ship_date < L_lc_earliest_ship_date then
         L_lc_earliest_ship_date := L_po_earliest_ship_date;
      end if;

      if L_po_latest_ship_date > L_lc_latest_ship_date then
         L_lc_latest_ship_date := L_po_latest_ship_date;
      end if;

      if L_po_latest_ship_date + L_lc_exp_days > L_lc_expiration_date then
         L_lc_expiration_date := L_po_latest_ship_date + L_lc_exp_days;
      end if;

      if LC_SQL.LOCK_LCHEAD(O_error_message,
                            I_lc_ref_id) = FALSE then
         return FALSE;
      end if;

      if CURRENCY_SQL.GET_RATE(O_error_message,
                               L_exchange_rate,
                               L_po_currency,
                               'L',
                               NULL) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('UPDATE',NULL,'LC_HEAD',NULL);
      update lc_head set amount               = amount + L_po_total_cost,
                         origin_country_id    = NVL(L_lc_origin_country, L_po_origin_country),
                         transshipment_ind    = L_lc_transshipment_ind,
                         partial_shipment_ind = L_lc_partial_shipment_ind,
                         earliest_ship_date   = L_lc_earliest_ship_date,
                         latest_ship_date     = L_lc_latest_ship_date,
                         expiration_date      = L_lc_expiration_date,
                         currency_code        = L_po_currency,
                         exchange_rate        = L_exchange_rate,
                         lading_port          = L_lading_port,
                         discharge_port       = L_discharge_port
                   where lc_ref_id            = I_lc_ref_id;

   else   --- letter of credit not in worksheet status (lc_amendments table)
      if L_form_type = 'S' then
         if ORDER_CALC_SQL.TOTAL_COSTS(O_error_message,
                                       L_po_total_cost,
                     L_prescaled_cost,
                                       L_outstanding_cost,
                                       L_cancel_cost,
                                       I_order_no,
                     NULL,
                     NULL) = FALSE then
            return FALSE;
         end if;

         --- convert order cost to letter of credit currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_po_total_cost,
                                 L_po_currency,
                                 L_lc_currency,
                                 L_po_total_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 L_po_exchange_rate,
                                 L_lc_exchange_rate) = FALSE then
            return FALSE;
         end if;

         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   'AO',
                                   to_char(0),
                                   to_char(L_po_total_cost),
                                   L_po_total_cost,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);

      else          --- long form
         for C_ITEM_ON_ORDER_REC in C_ITEM_ON_ORDER loop
            L_po_item         := C_ITEM_ON_ORDER_REC.item;
            L_item_order_qty  := C_ITEM_ON_ORDER_REC.qty;

            if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                   L_exists,
                                                   L_item_order_cost,
                                                   I_order_no,
                                                   L_po_item,
                                                   NULL,                --  I_pack_item
                                                   NULL) = FALSE then    -- I_location
               return FALSE;
            end if;


            --- convert order cost to letter of credit currency
            if CURRENCY_SQL.CONVERT(O_error_message,
                                    L_item_order_cost,
                                    L_po_currency,
                                    L_lc_currency,
                                    L_item_order_cost,
                                    'C',
                                    NULL,
                                    NULL,
                                    L_po_exchange_rate,
                                    L_lc_exchange_rate) = FALSE then
               return FALSE;
            end if;

            SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
            insert into lc_amendments(lc_ref_id,
                                      amend_no,
                                      order_no,
                                      item,
                                      amended_field,
                                      original_value,
                                      new_value,
                                      effect,
                                      status,
                                      accept_date,
                                      confirm_date,
                                      comments)
                               values(I_lc_ref_id,
                                      NULL,
                                      I_order_no,
                                      L_po_item,
                                      'AI',
                                      to_char(0),
                                      to_char(L_item_order_cost),
                                      L_item_order_cost * L_item_order_qty,
                                      'N',
                                      NULL,
                                      NULL,
                                      NULL);

         end loop;     --- items on order
      end if;   --- form type

      --- default the required documents from the order to the letter of credit.
      --- if the required document does not exist for the letter of credit, check
      --- if it exists as an amendment.  if the required document does not exist
      --- at either point, create an amendment.
      for C_REQ_DOC_REC in C_REQ_DOC loop
         L_req_doc_id := C_REQ_DOC_REC.doc_id;

         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                           values (I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'ARQD',
                                   NULL,
                                   to_char(L_req_doc_id),
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);
      end loop;

      --- call to amendments for latest change to letter of credit
      if LC_AMEND_SQL.GET_ORIGINAL_VALUES(O_error_message,
                                          L_esd_amend,
                                          L_lsd_amend,
                                          L_ed_amend,
                                          L_oc_amend,
                                          L_tsf_amend,
                                          L_psf_amend,
                                          I_lc_ref_id) = FALSE then
         return FALSE;
      end if;

      --- adding a purchase order, then check for affected values
      if L_po_transshipment_ind = 'Y' and
         L_tsf_amend            = 'N' then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'TSF',
                                   'N',
                                   'Y',
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);
      end if;

      if L_po_partial_shipment_ind = 'Y' and
         L_psf_amend               = 'N' then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'PSF',
                                   'N',
                                   'Y',
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);
      end if;

      if L_po_earliest_ship_date < L_esd_amend then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'ESD',
                                   to_char(L_esd_amend,'DD-MON-RR'),
                                   to_char(L_po_earliest_ship_date,'DD-MON-RR'),
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);
      end if;

      if L_po_latest_ship_date > L_lsd_amend then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'LSD',
                                   to_char(L_lsd_amend,'DD-MON-RR'),
                                   to_char(L_po_latest_ship_date,'DD-MON-RR'),
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);
      end if;

      if L_po_latest_ship_date + L_lc_exp_days > L_ed_amend then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'ED',
                                   to_char(L_ed_amend,'DD-MON-RR'),
                                   to_char((L_po_latest_ship_date + L_lc_exp_days),'DD-MON-RR'),
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);
      end if;

      --- if the letter of credit's origin country is not multiple countries (99)
      --- then check to see if the order's origin country is different from the
      --- letter of credit's origin country.  If so, create an amendment for
      --- origin country to be set to multiple countries (99).
      if L_oc_amend != '99' and L_oc_amend is not NULL then
         if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(O_error_message,
                                                   L_po_origin_country,
                                                   I_order_no) = FALSE then
            return FALSE;
         end if;

         if L_oc_amend != L_po_origin_country then
            L_origin_country_amend := TRUE;
         end if;

      elsif L_oc_amend is NULL then
         if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(O_error_message,
                                                   L_po_origin_country,
                                                   I_order_no) = FALSE then
            return FALSE;
         end if;

         L_oc_amend             := L_po_origin_country;
         L_origin_country_amend := TRUE;

      end if;

      if L_origin_country_amend = TRUE then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amend_no,
                                   order_no,
                                   item,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status,
                                   accept_date,
                                   confirm_date,
                                   comments)
                            values(I_lc_ref_id,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'OC',
                                   L_oc_amend,
                                   to_char(99),
                                   NULL,
                                   'N',
                                   NULL,
                                   NULL,
                                   NULL);

      end if;       --- letter of credit has multiple countries
   end if;          --- status

   --- set lc_ind to 'Y'es because a letter of credit has been
   --- attached to the given order
   SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLC','ORDLC','Order No: '
                    || to_char(I_order_no));
   open C_LOCK_ORDLC;
   SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLC','ORDLC','Order No: '
                    || to_char(I_order_no));
   close C_LOCK_ORDLC;

   update ordlc set lc_ref_id = I_lc_ref_id,
                    lc_ind    = 'Y'
              where order_no  = I_order_no;

   --- remove from lc_ordapply table which holds valid orders to be
   --- attached to a letter of credit (becomes invalid when attached)
   if LC_SQL.DELETE_LCORDAPP(O_error_message,
                             I_order_no) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_PO;

--------------------------------------------------------------------------------------
FUNCTION ADD_PO(O_error_message     IN OUT  VARCHAR2,
                I_lc_ref_id         IN      LC_HEAD.LC_REF_ID%TYPE,
                I_order_no          IN      ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS

L_program                  VARCHAR2(50) := 'LC_SQL.ADD_PO';

BEGIN
   if LC_SQL.ADD_PO(O_error_message,
                    I_lc_ref_id,
                    I_order_no,
                    NULL) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ADD_PO;
--------------------------------------------------------------------------------------
FUNCTION ORDER_UPDATE_LC(O_error_message  IN OUT VARCHAR2,
                         I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program                     VARCHAR2(50) := 'LC_SQL.ORDER_UPDATE_LC';
   L_order_item_change_exists    VARCHAR2(1)  := 'N';
   L_change_type                 ORD_LC_AMENDMENTS.CHANGE_TYPE%TYPE;
   L_new_value                   ORD_LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_lc_exp_days                 SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE;
   L_seq_no                      LC_DETAIL.SEQ_NO%TYPE;
   L_req_doc_id                  REQ_DOC.DOC_ID%TYPE;

   L_po_item                     ORDLOC.ITEM%TYPE;
   L_po_currency                 ORDHEAD.CURRENCY_CODE%TYPE;
   L_po_exchange_rate            ORDHEAD.EXCHANGE_RATE%TYPE;
   L_po_earliest_ship_date       ORDHEAD.EARLIEST_SHIP_DATE%TYPE;
   L_po_latest_ship_date         ORDHEAD.LATEST_SHIP_DATE%TYPE;
   L_po_lading_port              ORDHEAD.LADING_PORT%TYPE;
   L_po_discharge_port           ORDHEAD.DISCHARGE_PORT%TYPE;   
   L_po_merch_desc               ORDLC.MERCH_DESC%TYPE;

   L_lc_ref_id                   LC_HEAD.LC_REF_ID%TYPE;
   L_lc_status                   LC_HEAD.STATUS%TYPE;
   L_lc_form_type                LC_HEAD.FORM_TYPE%TYPE;
   L_lc_currency                 LC_HEAD.CURRENCY_CODE%TYPE;
   L_lc_exchange_rate            LC_HEAD.EXCHANGE_RATE%TYPE;
   L_lc_earliest_ship_date       LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_lc_latest_ship_date         LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_lc_expiration_date          LC_HEAD.EXPIRATION_DATE%TYPE;
   L_lc_transshipment_ind        LC_HEAD.TRANSSHIPMENT_IND%TYPE;
   L_lc_partial_shipment_ind     LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE;
   L_recalc_cost                 LC_HEAD.AMOUNT%TYPE;

   L_tsf_amend                   LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_psf_amend                   LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_esd_amend                   LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_lsd_amend                   LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_ed_amend                    LC_HEAD.EXPIRATION_DATE%TYPE;
   L_oc_amend                    LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_cost_amend                  LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_oq_amend                    LC_AMENDMENTS.NEW_VALUE%TYPE;

   L_esd_detail            LC_DETAIL.EARLIEST_SHIP_DATE%TYPE;
   L_lsd_detail            LC_DETAIL.LATEST_SHIP_DATE%TYPE;

   L_lc_detail_rec               VARCHAR2(1);
   L_yes_tsf_ind                 VARCHAR2(1);
   L_yes_psf_ind                 VARCHAR2(1);
   L_new_cost                    LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_new_qty                     LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_prev_item                   ITEM_MASTER.ITEM%TYPE;

   L_po_item_qty                 ORDLOC.QTY_ORDERED%TYPE;
   L_po_item_cost                ORDLOC.UNIT_COST%TYPE;
   L_po_total_cost               ORDLOC.UNIT_COST%TYPE := 0;
   L_prescaled_cost              ORDLOC.UNIT_COST%TYPE := 0;
   L_outstanding_cost            ORDLOC.UNIT_COST%TYPE;
   L_cancel_cost                 ORDLOC.UNIT_COST%TYPE;

   L_doc_dummy                   VARCHAR2(1) := 'y';

   L_exists                      BOOLEAN;

   cursor C_ORDER_HEADER_CHANGES is
      select distinct change_type,
             new_value
        from ord_lc_amendments ola
       where order_no    = I_order_no
         and item        is NULL
         and to_char(change_date,'YYYYMMDDHH24MISS') =
             (select MAX(to_char(change_date,'YYYYMMDDHH24MISS'))
                from ord_lc_amendments
               where order_no    = I_order_no
                 and item        is NULL
                 and change_type = ola.change_type);

   cursor C_ORDER_ITEM_CHANGES is
      select distinct item,
             change_type,
             new_value
        from ord_lc_amendments ola
       where order_no    = I_order_no
         and item        is not NULL
         and to_char(change_date,'YYYYMMDDHH24MISS') =
             (select MAX(to_char(change_date,'YYYYMMDDHH24MISS'))
                from ord_lc_amendments
               where order_no    = I_order_no
                 and item        = ola.item
                 and change_type = ola.change_type)
       order by item;

   cursor C_GET_MAX_SEQ is
      select nvl(MAX(seq_no),0) + 1
        from lc_detail
       where lc_ref_id = L_lc_ref_id;

   cursor C_LC_DETAIL is
      select cost,
             qty
        from lc_detail
       where lc_ref_id = L_lc_ref_id
         and order_no  = I_order_no
         and item      = L_po_item;

   cursor C_ORDER_ITEM_CHANGES_EXIST is
      select 'Y'
        from ord_lc_amendments
       where order_no = I_order_no
         and item is not NULL
         and change_type in ('OIC','OIQ','OIA','OID');

   cursor C_ORDER_INFO is
      select oh.currency_code,
             oh.exchange_rate,
             oh.earliest_ship_date,
             oh.latest_ship_date,
             ol.merch_desc,
             oh.lading_port,
             oh.discharge_port
        from ordhead oh,
             ordlc   ol
       where oh.order_no = I_order_no
         and oh.order_no = ol.order_no;

   cursor C_ITEM_INFO is
     select SUM(o.qty_ordered)
       from ordloc o
      where order_no = I_order_no
        and item = L_po_item
      group by item;


   cursor C_ITEM_EARLIEST_SHIP_DATE is
     select earliest_ship_date
       from ordsku
      where order_no = I_order_no
        and item = L_po_item;

  cursor C_ITEM_LATEST_SHIP_DATE is
     select latest_ship_date
       from ordsku
      where order_no = I_order_no
        and item = L_po_item;


  cursor C_PO_ITEM_ESD_AMEND is
     select to_date(new_value,'DD-MON-RR')
       from lc_amendments
      where lc_ref_id     = L_lc_ref_id
        and order_no      = I_order_no
        and item          = L_po_item
        and amended_field = 'ESD'
        and amend_no      = (select MAX(amend_no)
                               from lc_amendments
                              where lc_ref_id     = L_lc_ref_id
                                and order_no      = I_order_no
                                and item          = L_po_item
                                and amended_field = 'ESD');

  cursor C_PO_ITEM_LSD_AMEND is
     select to_date(new_value,'DD-MON-RR')
       from lc_amendments
      where lc_ref_id     = L_lc_ref_id
        and order_no      = I_order_no
        and item          = L_po_item
        and amended_field = 'LSD'
        and amend_no      = (select MAX(amend_no)
                               from lc_amendments
                              where lc_ref_id     = L_lc_ref_id
                                and order_no      = I_order_no
                                and item          = L_po_item
                                and amended_field = 'LSD');

  cursor C_LC_DETAIL_ESD is
     select earliest_ship_date
       from lc_detail
      where lc_ref_id     = L_lc_ref_id
        and order_no      = I_order_no
        and item          = L_po_item;

  cursor C_LC_DETAIL_LSD is
     select latest_ship_date
       from lc_detail
      where lc_ref_id     = L_lc_ref_id
        and order_no      = I_order_no
        and item          = L_po_item;

  cursor C_LC_INFO is
      select l.lc_ref_id,
             l.status,
             l.form_type,
             l.currency_code,
             l.exchange_rate,
             l.transshipment_ind,
             l.partial_shipment_ind,
             l.earliest_ship_date,
             l.latest_ship_date,
             l.expiration_date
        from lc_head l,
             ordlc   o
       where o.order_no = I_order_no
         and o.lc_ref_id = l.lc_ref_id;

   cursor C_LC_DETAIL_REC is
      select 'Y'
        from lc_detail     ld,
             lc_amendments la
       where (ld.lc_ref_id     = L_lc_ref_id
         and  ld.order_no      = I_order_no
         and  ld.item          = L_po_item)
          or (la.lc_ref_id     = L_lc_ref_id
         and  la.order_no      = I_order_no
         and  la.item          = L_po_item
         and  la.amended_field = 'AI');

   cursor C_LOCK_LC_DETAIL is
      select 'x'
        from lc_detail
       where lc_ref_id = L_lc_ref_id
         and order_no  = I_order_no
         for update nowait;

   cursor C_LOCK_ORD_LC_AMENDMENTS is
      select 'x'
        from ord_lc_amendments
       where order_no = I_order_no;

   cursor C_YES_TRANSSHIPMENT_IND is
      select 'Y'
        from ordlc
       where lc_ref_id         = L_lc_ref_id
         and transshipment_ind = 'Y';

   cursor C_YES_PARTIAL_SHIPMENT_IND is
      select 'Y'
        from ordlc
       where lc_ref_id            = L_lc_ref_id
         and partial_shipment_ind = 'Y';

   cursor C_PO_ITEM_OQ_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = L_lc_ref_id
         and amended_field = 'OQ'
         and order_no      = I_order_no
         and item          = L_po_item
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = L_lc_ref_id
                                 and order_no      = I_order_no
                                 and item          = L_po_item
                                 and amended_field = 'OQ');

   cursor C_PO_ITEM_AI_OQ_AMEND is
      select to_number(effect/new_value)
        from lc_amendments
       where lc_ref_id     = L_lc_ref_id
         and amended_field = 'AI'
         and order_no      = I_order_no
         and item          = L_po_item;
    
    cursor C_PO_ITEM_AI_C_AMEND is
       select new_value
         from lc_amendments
        where lc_ref_id     = L_lc_ref_id
          and order_no      = I_order_no
          and item          = L_po_item
          and amended_field = 'AI';
         
   cursor C_COST_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = L_lc_ref_id
         and amended_field = 'C'
         and order_no      = I_order_no
         and (item         = L_po_item
              or item is NULL)
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = L_lc_ref_id
                                 and order_no      = I_order_no
                                 and (item          = L_po_item
                                      or item is NULL)
                                 and amended_field = 'C');

   cursor C_COST_LCDETAIL is
      select SUM(cost)
        from lc_detail
       where lc_ref_id = L_lc_ref_id
         and order_no  = I_order_no;

   cursor C_PO_ITEM_C_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = L_lc_ref_id
         and order_no      = I_order_no
         and item          = L_po_item
         and amended_field = 'C'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = L_lc_ref_id
                                 and order_no      = I_order_no
                                 and item          = L_po_item
                                 and amended_field = 'C');

   cursor C_REQ_DOC is
      select distinct r.doc_id
        from req_doc  r,
             doc_link dl,
             doc      d
       where d.lc_ind      = 'Y'
         and r.doc_id      = d.doc_id
         and d.doc_type    = dl.doc_type
         and dl.module     in ('LC','LCA')
         and ((r.module     = 'PO'
         and r.key_value_1 = to_char(I_order_no)
         and r.key_value_2 is NULL)
          or (r.module     = 'POIT'
         and r.key_value_1 = to_char(I_order_no)
         and r.key_value_2 is not NULL))
         and r.doc_id not in (select doc_id
                                from req_doc
                               where (module = 'LC'
                                 and  key_value_1 = to_char(L_lc_ref_id)
                                 and  key_value_2 is NULL)
                                  or (module = 'LCA'
                                 and  key_value_1 = to_char(L_lc_ref_id)
                                 and  key_value_2 is NULL));

   cursor C_DOC_AMEND_EXISTS is
      select 'x'
        from lc_amendments
       where lc_ref_id = L_lc_ref_id
         and amended_field = 'ARQD'
         and new_value = to_char(L_req_doc_id);

BEGIN
   if I_order_no  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   --- order information
   open C_ORDER_INFO;
   fetch C_ORDER_INFO into L_po_currency,
                           L_po_exchange_rate,
                           L_po_earliest_ship_date,
                           L_po_latest_ship_date,
                           L_po_merch_desc,
                           L_po_lading_port,
                           L_po_discharge_port;
   ---
   if C_ORDER_INFO%NOTFOUND then
      close C_ORDER_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   close C_ORDER_INFO;
   ---
   --- letter of credit information
   open C_LC_INFO;
   fetch C_LC_INFO into L_lc_ref_id,
                        L_lc_status,
                        L_lc_form_type,
                        L_lc_currency,
                        L_lc_exchange_rate,
                        L_lc_transshipment_ind,
                        L_lc_partial_shipment_ind,
                        L_lc_earliest_ship_date,
                        L_lc_latest_ship_date,
                        L_lc_expiration_date;
   ---
   if C_LC_INFO%NOTFOUND then
      close C_LC_INFO;
      O_error_message := SQL_LIB.CREATE_MSG('LC_PO_NOT_ATTACHED',to_char(I_order_no),
                                            NULL,NULL);
      return FALSE;
   end if;
   close C_LC_INFO;
   ---
   --- if closed, nothing will be updated
   if L_lc_status = 'L' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_CHANGE_LC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_LC_EXP_DAYS(O_error_message,
                                         L_lc_exp_days) = FALSE then
      return FALSE;
   end if;
   ---
   --- call to amendments for latest change to letter of credit
   if LC_AMEND_SQL.GET_ORIGINAL_VALUES(O_error_message,
                                       L_esd_amend,
                                       L_lsd_amend,
                                       L_ed_amend,
                                       L_oc_amend,
                                       L_tsf_amend,
                                       L_psf_amend,
                                       L_lc_ref_id) = FALSE then
      return FALSE;
   end if;
   ---
   --- order amendments
   for C_ORDER_HEADER_CHANGES_REC in C_ORDER_HEADER_CHANGES loop
      L_change_type := C_ORDER_HEADER_CHANGES_REC.change_type;
      L_new_value   := C_ORDER_HEADER_CHANGES_REC.new_value;
      ---
      --- reset check indicators
      L_yes_tsf_ind   := 'N';
      L_yes_psf_ind   := 'N';
      ---
      --- transshipment indicator
      --- if the order's indicator is different than the value on the
      --- ord_lc_amendments table, need to update letter of credit dialog.
      --- if the letter of credit is in worksheet status, update lc_head.
      --- if the letter of credit is not in worksheet status and not
      --- closed, create an amendment for the change in the order's details.
      if L_change_type = 'TSF' then
         if L_lc_status = 'W' then
            ---
            if L_lc_transshipment_ind = 'N' and L_new_value = 'Y' then
               L_lc_transshipment_ind := 'Y';
            elsif L_lc_transshipment_ind = 'Y' and L_new_value = 'N' then
               --- check if any attached order had indicator of 'Y'es
                open C_YES_TRANSSHIPMENT_IND;
               fetch C_YES_TRANSSHIPMENT_IND into L_yes_tsf_ind;
               close C_YES_TRANSSHIPMENT_IND;
               ---
               if L_yes_tsf_ind = 'N' then
                   L_lc_transshipment_ind := 'N';
               end if;
            end if;
         else  -- lc status != 'W'
            --- if the letter of credit's transshipment indicator is
            --- currently 'N'o and the order's is 'Y'es then create record
            if L_tsf_amend = 'N' and
               L_new_value = 'Y' then
               insert into lc_amendments(lc_ref_id,
                                         order_no,
                                         item,
                                         amended_field,
                                         original_value,
                                         new_value,
                                         effect,
                                         status,
                                         accept_date,
                                         confirm_date,
                                         amend_no,
                                         comments)
                                  values(L_lc_ref_id,
                                         NULL,
                                         NULL,
                                         'TSF',
                                         'N',
                                         'Y',
                                         NULL,
                                         'N',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
            elsif L_tsf_amend = 'Y' and
               L_new_value = 'N' then
               ---
               --- check if any attached order had indicator of 'Y'es
                open C_YES_TRANSSHIPMENT_IND;
               fetch C_YES_TRANSSHIPMENT_IND into L_yes_tsf_ind;
               close C_YES_TRANSSHIPMENT_IND;
               ---
               if L_yes_tsf_ind = 'N' then
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            NULL,
                                            NULL,
                                            'TSF',
                                            'Y',
                                            'N',
                                            NULL,
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;
            end if;      --- tsf amend
         end if;         --- lc status
         ---
      --- partial shipment indicator
      --- if the order's indicator is different than the value on the
      --- ord_lc_amendments table, need to update letter of credit dialog.
      --- if the letter of credit is in worksheet status, update lc_head.
      --- if the letter of credit is not in worksheet status and not
      --- closed, create an amendment for the change in the order's details.
      elsif L_change_type = 'PSF' then
         if L_lc_status = 'W' then
            if L_lc_partial_shipment_ind = 'N' and
               L_new_value = 'Y' then
               ---
               L_lc_partial_shipment_ind := 'Y';
            elsif L_lc_partial_shipment_ind = 'Y' and
               L_new_value = 'N' then
               ---
               --- check if any attached order had indicator of 'Y'es
                open C_YES_PARTIAL_SHIPMENT_IND;
               fetch C_YES_PARTIAL_SHIPMENT_IND into L_yes_psf_ind;
               close C_YES_PARTIAL_SHIPMENT_IND;
               ---
               if L_yes_psf_ind = 'N' then
                  L_lc_partial_shipment_ind := 'N';
               end if;
            end if;
         else  -- lc status != 'W'
            --- if the letter of credit's partial shipment indicator is
            --- currently 'N'o and the order's is 'Y'es then create record
            if L_psf_amend = 'N' and
               L_new_value = 'Y' then
               insert into lc_amendments(lc_ref_id,
                                         order_no,
                                         item,
                                         amended_field,
                                         original_value,
                                         new_value,
                                         effect,
                                         status,
                                         accept_date,
                                         confirm_date,
                                         amend_no,
                                         comments)
                                  values(L_lc_ref_id,
                                         NULL,
                                         NULL,
                                         'PSF',
                                         'N',
                                         'Y',
                                         NULL,
                                         'N',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
            elsif L_psf_amend = 'Y' and
               L_new_value = 'N' then
               ---
               --- check if any attached order had indicator of 'Y'es
                open C_YES_PARTIAL_SHIPMENT_IND;
               fetch C_YES_PARTIAL_SHIPMENT_IND into L_yes_psf_ind;
               close C_YES_PARTIAL_SHIPMENT_IND;
               ---
               if L_yes_psf_ind = 'N' then
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            NULL,
                                            NULL,
                                            'PSF',
                                            'Y',
                                            'N',
                                            NULL,
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;
            end if;  --- psf amend
         end if;     --- lc status
         ---
      --- earliest ship date
      --- if the order's earliest ship date is different than the value on the
      --- ord_lc_amendments table, need to update letter of credit dialog.
      --- if the letter of credit is in worksheet status, update lc_head.
      --- if the letter of credit is not in worksheet status and not
      --- closed, create an amendment for the change in the order's details.
      elsif L_change_type = 'ESD' then
         if L_lc_status = 'W' then
            if to_date(L_new_value,'DD-MON-RR') < L_lc_earliest_ship_date then
               ---
               L_lc_earliest_ship_date := to_date(L_new_value,'DD-MON-RR');
            end if;
            ---
            if L_lc_form_type = 'S' then
                open C_LOCK_LC_DETAIL;
               close C_LOCK_LC_DETAIL;
               ---
               update lc_detail set earliest_ship_date = to_date(L_new_value,'DD-MON-RR')
                              where lc_ref_id          = L_lc_ref_id
                                and order_no           = I_order_no;
            end if;
            ---
         else --lc status != 'W'
            --- if the letter of credit's earliest ship date is later
            --- than the order's earliest ship date then create record
            if to_date(L_new_value,'DD-MON-RR') < L_esd_amend then
               insert into lc_amendments(lc_ref_id,
                                         order_no,
                                         item,
                                         amended_field,
                                         original_value,
                                         new_value,
                                         effect,
                                         status,
                                         accept_date,
                                         confirm_date,
                                         amend_no,
                                         comments)
                                  values(L_lc_ref_id,
                                         I_order_no,
                                         NULL,
                                         'ESD',
                                         to_char(L_esd_amend,'DD-MON-RR'),
                                         L_new_value,
                                         NULL,
                                         'N',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
            end if; -- new_value is not less then esd_amend
         end if;  --lc status
         ---
      --- latest ship date
      --- if the order's latest ship date is different than the value on the
      --- ord_lc_amendments table, need to update letter of credit dialog.
      --- if the letter of credit is in worksheet status, update lc_head.
      --- if the letter of credit is not in worksheet status and not
      --- closed, create an amendment for the change in the order's details.
      elsif L_change_type = 'LSD' then
         if L_lc_status = 'W' then
            if to_date(L_new_value,'DD-MON-RR') > L_lc_latest_ship_date then
               L_lc_latest_ship_date := L_new_value;
            end if;
            ---
            if to_date(L_new_value,'DD-MON-RR') + L_lc_exp_days > L_lc_expiration_date then
               L_lc_expiration_date := to_date(L_new_value) + L_lc_exp_days;
            end if;
            --
            if L_lc_form_type = 'S' then
                open C_LOCK_LC_DETAIL;
               close C_LOCK_LC_DETAIL;
            ---
            update lc_detail set latest_ship_date = to_date(L_new_value,'DD-MON-RR')
                              where lc_ref_id        = L_lc_ref_id
                                and order_no         = I_order_no;
            end if;
         else
            --- if the letter of credit's latest ship date is earlier
            --- than the order's latest ship date then create record
            if to_date(L_new_value,'DD-MON-RR') > L_lsd_amend then
               insert into lc_amendments(lc_ref_id,
                                         order_no,
                                         item,
                                         amended_field,
                                         original_value,
                                         new_value,
                                         effect,
                                         status,
                                         accept_date,
                                         confirm_date,
                                         amend_no,
                                         comments)
                                  values(L_lc_ref_id,
                                         I_order_no,
                                         NULL,
                                         'LSD',
                                         to_char(L_lsd_amend,'DD-MON-RR'),
                                         L_new_value,
                                         NULL,
                                         'N',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
               ---
               --- if the letter of credit's expiration date is earlier
               --- than the order's latest ship date plus systems expiration days
               --- then create record
               if L_ed_amend < to_date(L_new_value,'DD-MON-RR') + L_lc_exp_days then
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            I_order_no,
                                            NULL,
                                            'ED',
                                            to_char(L_ed_amend,'DD-MON-RR'),
                                            to_char((to_date(L_new_value,'DD-MON-RR') + L_lc_exp_days), 'DD-MON-RR'),
                                            NULL,
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;
            end if;
         end if;         --- letter of credit's status
      end if;            --- change type
   end loop;             --- order level changes
   ---
   --- update the letter of credit directly if in 'W'orksheet status.
   if L_lc_status = 'W' then
      if LC_SQL.LOCK_LCHEAD(O_error_message,
                            L_lc_ref_id) = FALSE then
         return FALSE;
      end if;
      ---
      update lc_head set transshipment_ind    = L_lc_transshipment_ind,
                         partial_shipment_ind = L_lc_partial_shipment_ind,
                         earliest_ship_date   = L_lc_earliest_ship_date,
                         latest_ship_date     = L_lc_latest_ship_date,
                         expiration_date      = L_lc_expiration_date,
                         lading_port          = L_po_lading_port,
                         discharge_port       = L_po_discharge_port
                   where lc_ref_id            = L_lc_ref_id;
   end if;
   ---
   --- Order Item Level processing
   if L_lc_form_type = 'S' then
      open C_ORDER_ITEM_CHANGES_EXIST;
      fetch C_ORDER_ITEM_CHANGES_EXIST into L_order_item_change_exists;
      close C_ORDER_ITEM_CHANGES_EXIST;
      ---
      if L_order_item_change_exists = 'Y' then
         if ORDER_CALC_SQL.TOTAL_COSTS(O_error_message,
                                       L_po_total_cost,
                                       L_prescaled_cost,
                                       L_outstanding_cost,
                                       L_cancel_cost,
                                       I_order_no,
                     NULL,
                     NULL) = FALSE then
            return FALSE;
         end if;
         ---
         --- convert order cost to letter of credit currency
         if CURRENCY_SQL.CONVERT(O_error_message,
                                 L_po_total_cost,
                                 L_po_currency,
                                 L_lc_currency,
                                 L_po_total_cost,
                                 'C',
                                 NULL,
                                 NULL,
                                 L_po_exchange_rate,
                                 L_lc_exchange_rate) = FALSE then
            return FALSE;
         end if;
         ---
         --- With the item change, need to update the letter of credit dialog.
         --- if the letter of credit is in 'W'orksheet status, update the details.
         --- if the letter of credit is not in 'W'orksheet or C'l'osed status,
         --- then create an amendment.
         if L_lc_status = 'W' then
            open C_LOCK_LC_DETAIL;
            close C_LOCK_LC_DETAIL;
            ---
            update lc_detail set cost      = L_po_total_cost
                           where lc_ref_id = L_lc_ref_id
                             and order_no  = I_order_no;
         ---
         else  --lc status not = 'W'
            --- get the latest cost of the order (check the amendment table
            --- first and then the detail table second).
            open C_COST_AMEND;
            fetch C_COST_AMEND into L_cost_amend;
            close C_COST_AMEND;
            ---
            if L_cost_amend is NULL then
               open C_COST_LCDETAIL;
               fetch C_COST_LCDETAIL into L_cost_amend;
               close C_COST_LCDETAIL;
            end if;
            ---
            --- if the letter of credit value is different from the
            --- order's value, then create an amendment
            if to_number(L_cost_amend) != L_po_total_cost then
               insert into lc_amendments(lc_ref_id,
                                         order_no,
                                         item,
                                         amended_field,
                                         original_value,
                                         new_value,
                                         effect,
                                         status,
                                         accept_date,
                                         confirm_date,
                                         amend_no,
                                         comments)
                                  values(L_lc_ref_id,
                                         I_order_no,
                                         NULL,
                                         'C',
                                         L_cost_amend,
                                         to_char(L_po_total_cost),
                                         (L_po_total_cost - to_number(L_cost_amend)),
                                         'N',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
            end if;
         end if;   --- lc status
      end if;      --- order item change exists
      ---
   elsif L_lc_form_type = 'L' then
      ---
      --- lock detail records for possible update
      if L_lc_status = 'W' then
         open C_LOCK_LC_DETAIL;
         close C_LOCK_LC_DETAIL;
      end if;
      ---
      for C_ORDER_ITEM_CHANGES_REC in C_ORDER_ITEM_CHANGES loop
         --- values from ord_lc_amendments table
         L_po_item      := C_ORDER_ITEM_CHANGES_REC.item;
         L_new_value   := C_ORDER_ITEM_CHANGES_REC.new_value;
         L_change_type := C_ORDER_ITEM_CHANGES_REC.change_type;
         ---
         --- reset cost and quantity variables
         L_oq_amend    := NULL;
         L_cost_amend  := NULL;
         L_esd_amend   := NULL;
         L_lsd_amend   := NULL;
         ---
         --- set flag for item changes
         L_order_item_change_exists := 'Y';
         L_lc_detail_rec            := 'N';
         ---
         --- need to track item changes
         if L_prev_item is NULL or L_prev_item != L_po_item then
            L_new_cost := NULL;
            L_new_qty  := NULL;
            L_prev_item := L_po_item;
         end if;
         ---
         --- if letter of credit status is 'W'orksheet, then
         --- changes will occur to the lc_detail table.
         if L_lc_status = 'W' then
            --- order cost change
            if L_change_type = 'OIC' then
               --- Convert new value from to letter of credit currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_new_value,
                                       L_po_currency,
                                       L_lc_currency,
                                       L_new_value,
                                       'C',
                                       NULL,
                                       NULL,
                                       L_po_exchange_rate,
                                       L_lc_exchange_rate) = FALSE then
                  return FALSE;
               end if;
               ---
               update lc_detail set cost      = L_new_value
                              where lc_ref_id = L_lc_ref_id
                                and order_no  = I_order_no
                                and item      = L_po_item;
               ---
            --- order quantity change
            elsif L_change_type = 'OIQ' then
                open C_ITEM_INFO;
               fetch C_ITEM_INFO into L_new_value;
               ---
               if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                         L_exists,
                                                         L_po_item_cost,
                                                         I_order_no,
                                                         L_po_item,
                                                         NULL,                --  I_pack_item
                                                         NULL) = FALSE then   -- I_location
                  return FALSE;
               end if;
               ---
               if C_ITEM_INFO%NOTFOUND then
                  close C_ITEM_INFO;
                  O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_COST_QTY',
                                                        L_po_item,
                                                        to_char(I_order_no),
                                                        NULL);
                  return FALSE;
               end if;
               ---
               close C_ITEM_INFO;
               ---
               update lc_detail set qty       = L_new_value
                              where lc_ref_id = L_lc_ref_id
                                and order_no  = I_order_no
                                and item      = L_po_item;
               ---
            --- order item deletion
            elsif L_change_type = 'OID' then
               delete from lc_detail
                     where lc_ref_id = L_lc_ref_id
                       and order_no  = I_order_no
                       and item      = L_po_item;
            ---
            --- order item addition
            elsif L_change_type = 'OIA' then
               --- check that the lc/order/item record does
               --- not exist already
                open C_LC_DETAIL_REC;
               fetch C_LC_DETAIL_REC into L_lc_detail_rec;
               close C_LC_DETAIL_REC;
               ---
               if L_lc_detail_rec = 'N' then
                  --- calculate next sequence number
                   open C_GET_MAX_SEQ;
                  fetch C_GET_MAX_SEQ into L_seq_no;
                  close C_GET_MAX_SEQ;
                  ---
                  --- order's cost and quantity ordered
                   open C_ITEM_INFO;
                  fetch C_ITEM_INFO into L_po_item_qty;
                  ---
                  if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                         L_exists,
                                                         L_po_item_cost,
                                                         I_order_no,
                                                         L_po_item,
                                                         NULL,                --  I_pack_item
                                                         NULL) = FALSE then   -- I_location
                     return FALSE;
                  end if;
                  ---
                  if C_ITEM_INFO%NOTFOUND then
                     close C_ITEM_INFO;
                     O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_COST_QTY',
                                                           L_po_item,
                                                           to_char(I_order_no),
                                                           NULL);
                     return FALSE;
                  end if;
                  ---
                  close C_ITEM_INFO;
                  ---
                  --- order's earliest ship date for each item
                   open C_ITEM_EARLIEST_SHIP_DATE;
                  fetch C_ITEM_EARLIEST_SHIP_DATE into L_po_earliest_ship_date;
                  ---
                  if C_ITEM_EARLIEST_SHIP_DATE%NOTFOUND then
                     close C_ITEM_EARLIEST_SHIP_DATE;
                     O_error_message := SQL_LIB.CREATE_MSG('ESD_NOT_FOUND',
                                                        L_po_item,
                                                        to_char(I_order_no),
                                                        NULL);
                     return FALSE;
                  end if;
                  ---
                  close C_ITEM_EARLIEST_SHIP_DATE;
                  ---
                  --- order's latest ship date for each item
                  open C_ITEM_LATEST_SHIP_DATE;
                  fetch C_ITEM_LATEST_SHIP_DATE into L_po_latest_ship_date;
                  if C_ITEM_LATEST_SHIP_DATE%NOTFOUND then
                     close C_ITEM_LATEST_SHIP_DATE;
                     O_error_message := SQL_LIB.CREATE_MSG('LSD_NOT_FOUND',
                                                           L_po_item,
                                                           to_char(I_order_no),
                                                           NULL);
                     return FALSE;
                  end if;
                  ---
                  close C_ITEM_LATEST_SHIP_DATE;
                  ---
                  --- Convert order cost from order to letter of credit currency
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_po_item_cost,
                                          L_po_currency,
                                          L_lc_currency,
                                          L_po_item_cost,
                                          'C',
                                          NULL,
                                          NULL,
                                          L_po_exchange_rate,
                                          L_lc_exchange_rate) = FALSE then
                     return FALSE;
                  end if;
                  ---
                  --- letter of credit details of a short form need to have
                  --- ship dates and merchandise descriptions populated
                  ---
                  insert into lc_detail(lc_ref_id,
                                        seq_no,
                                        order_no,
                                        item,
                                        cost,
                                        qty,
                                        earliest_ship_date,
                                        latest_ship_date,
                                        merch_desc,
                                        comments)
                                 values(L_lc_ref_id,
                                        L_seq_no,
                                        I_order_no,
                                        L_po_item,
                                        L_po_item_cost,
                                        L_po_item_qty,
                                        L_po_earliest_ship_date,
                                        L_po_latest_ship_date,
                                        NULL,
                                        NULL);
               end if;      --- lc/order/item record
            --- item earliest ship date change
            elsif L_change_type = 'ESD' then
               update lc_detail set earliest_ship_date = to_date(L_new_value,'DD-MON-RR')
                              where lc_ref_id          = L_lc_ref_id
                                and order_no           = I_order_no
                                and item               = L_po_item;
               ---
            ---item latest ship date change
            elsif L_change_type = 'LSD' then
               update lc_detail set latest_ship_date = to_date(L_new_value,'DD-MON-RR')
                              where lc_ref_id        = L_lc_ref_id
                                and order_no         = I_order_no
                                and item             = L_po_item;
            end if;         --- change type
            ---
         --- any changes when the letter of credit is not in 'W'orksheet or
         --- not in closed status, then create an amendment.
         else
            --- order cost change
            if L_change_type = 'OIC' then
               --- check for an amendment to the order's cost
                open C_PO_ITEM_C_AMEND;
               fetch C_PO_ITEM_C_AMEND into L_cost_amend;
               close C_PO_ITEM_C_AMEND;
               ---
               --- need to have newest quantity for effect of this amendment
               if L_new_qty is NULL then
                   open C_PO_ITEM_OQ_AMEND;
                  fetch C_PO_ITEM_OQ_AMEND into L_oq_amend;
                  close C_PO_ITEM_OQ_AMEND;
               else
                  L_oq_amend := L_new_qty;
               end if;
               ---
               --- L_oq_amend will hold the qty.
               --- L_cost_amend will hold the cost.
               if L_cost_amend is NULL then
                  --- if no amendment exists, use the cost from the detail table.
                  --- put the qty in another variable for further checking.
                   open C_LC_DETAIL;
                  fetch C_LC_DETAIL into L_cost_amend,
                                        L_po_item_qty;
                  close C_LC_DETAIL;
               else
               --- an amendment exists; therefore, use the amended cost.
               --- put the qty in another variable for further checking.
                   open C_ITEM_INFO;
                  fetch C_ITEM_INFO into L_po_item_qty;
                  ---
                  if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                         L_exists,
                                                         L_po_item_cost,
                                                         I_order_no,
                                                         L_po_item,
                                                         NULL,                --  I_pack_item
                                                         NULL) = FALSE then   -- I_location
                     return FALSE;
                  end if;
                  ---
                  if C_ITEM_INFO%NOTFOUND then
                     close C_ITEM_INFO;
                     O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_COST_QTY',
                                                           L_po_item,
                                                           to_char(I_order_no),
                                                           NULL);
                  return FALSE;
                  end if;
                  ---
                  close C_ITEM_INFO;
               end if;  -- cost amend
               --- L_oq_amend will hold the newest value.  If no amendment exists
               --- then assign L_oq_amend to the quantity retrieved from the item level.
               if L_oq_amend is NULL then
                  L_oq_amend := L_po_item_qty;
               end if;
               ---
               -- Convert new value from to letter of credit currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_new_value,
                                       L_po_currency,
                                       L_lc_currency,
                                       L_new_value,
                                       'C',
                                       NULL,
                                       NULL,
                                       L_po_exchange_rate,
                                       L_lc_exchange_rate) = FALSE then
                  return FALSE;
               end if;
               ---
               if L_cost_amend is NULL then
                  --- if no amendment exists, use the cost from the lc_amendment table.
                   open C_PO_ITEM_AI_C_AMEND;
                  fetch C_PO_ITEM_AI_C_AMEND into L_cost_amend;
                  close C_PO_ITEM_AI_C_AMEND;
               end if;	       
               ---               
               if to_number(L_cost_amend) != to_number(L_new_value) then
                  L_new_cost := L_new_value;
                  ---
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            I_order_no,
                                            L_po_item,
                                            'C',
                                            L_cost_amend,
                                            L_new_value,
                                            ((L_new_value - L_cost_amend) * L_oq_amend),
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;
               ---
            --- order quantity change
            elsif L_change_type = 'OIQ' then
               --- get the new value
                open C_ITEM_INFO;
               fetch C_ITEM_INFO into L_new_value;
               ---
               if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                      L_exists,
                                                      L_po_item_cost,
                                                      I_order_no,
                                                      L_po_item,
                                                      NULL,                --  I_pack_item
                                                      NULL) = FALSE then   -- I_location
                  return FALSE;
               end if;
               ---
               if C_ITEM_INFO%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_COST_QTY',
                                                        L_po_item,
                                                        to_char(I_order_no),
                                                        NULL);
                  return FALSE;
               end if;
               ---
               -- Convert new value from to letter of credit currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_po_item_cost,
                                       L_po_currency,
                                       L_lc_currency,
                                       L_po_item_cost,
                                       'C',
                                       NULL,
                                       NULL,
                                       L_po_exchange_rate,
                                       L_lc_exchange_rate) = FALSE then
                  return FALSE;
               end if;
               ---
               close C_ITEM_INFO;
               ---
               --- check for an amendment to the order's quantity
                open C_PO_ITEM_OQ_AMEND;
               fetch C_PO_ITEM_OQ_AMEND into L_oq_amend;
               close C_PO_ITEM_OQ_AMEND;
               ---
               --- need to have newest cost for effect of this amendment
               if L_new_cost is NULL then
                   open C_PO_ITEM_C_AMEND;
                  fetch C_PO_ITEM_C_AMEND into L_cost_amend;
                  close C_PO_ITEM_C_AMEND;
               else
                  L_cost_amend := L_new_cost;
               end if;
               ---
               --- if no amendment exists, use the quantity from the detail table
               --- L_cost_amend will be populated with the order item's cost
               if L_oq_amend is NULL then
                  --- if the quantity is null, retrieve it from lc_detail.
                  --- retrieve the cost for future checking.
                  open C_LC_DETAIL;
                  fetch C_LC_DETAIL into L_po_item_cost,
                                         L_oq_amend;
                  close C_LC_DETAIL;
               end if;
               ---
               --- if no amendment exists for the cost, populate cost variable
               --- with value from lc_detail or orditem
               if L_cost_amend is NULL then
                  L_cost_amend := L_po_item_cost;
               end if;
               ---
               if L_oq_amend is NULL then
                  --- if the quantity is null, retrieve it from lc_amendment for AI code type.
                    open C_PO_ITEM_AI_OQ_AMEND;
                   fetch C_PO_ITEM_AI_OQ_AMEND into L_oq_amend;
                   close C_PO_ITEM_AI_OQ_AMEND;
               end if;
               ---               
               if to_number(L_new_value) != to_number(L_oq_amend) then
                  L_new_qty := L_new_value;
                  ---
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            I_order_no,
                                            L_po_item,
                                            'OQ',
                                            L_oq_amend,
                                            L_new_value,
                                            ((L_new_value - L_oq_amend) * L_cost_amend),
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;
               ---
            --- order item deletion
            elsif L_change_type = 'OID' then
               --- check for an amendment to the order item's quantity or
               --- cost.
                open C_PO_ITEM_C_AMEND;
               fetch C_PO_ITEM_C_AMEND into L_cost_amend;
               close C_PO_ITEM_C_AMEND;
               ---
                open C_PO_ITEM_OQ_AMEND;
               fetch C_PO_ITEM_OQ_AMEND into L_oq_amend;
               close C_PO_ITEM_OQ_AMEND;
               ---
               --- if one or both of the values does not have an amendment,
               --- then get the value from the order dialog.  L_cost_amend
               --- and L_oq_amend are the variables that need to be
               --- populated for creating an amendment.
               if L_cost_amend is NULL or L_oq_amend is NULL then
                   open C_ITEM_INFO;
                  fetch C_ITEM_INFO into L_po_item_qty;
                  ---
                  if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                         L_exists,
                                                         L_po_item_cost,
                                                         I_order_no,
                                                         L_po_item,
                                                         NULL,                --  I_pack_item
                                                         NULL) = FALSE then   -- I_location
                     return FALSE;
                  end if;
                  ---
                  if C_ITEM_INFO%NOTFOUND then
                     O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_COST_QTY',
                                                           L_po_item,
                                                           to_char(I_order_no),
                                                           NULL);
                     return FALSE;
                  end if;
                  ---
                  close C_ITEM_INFO;
                  ---
                  --- order item's cost
                  if L_cost_amend is NULL then
                     L_cost_amend := L_po_item_cost;
                  end if;
                  ---
                  --- order item's quantity
                  if L_oq_amend is NULL then
                     L_oq_amend := L_po_item_qty;
                  end if;
               end if;  --- cost amend
               ---
               --- Convert new value from to letter of credit currency
               if CURRENCY_SQL.CONVERT(O_error_message,
                                       L_cost_amend,
                                       L_po_currency,
                                       L_lc_currency,
                                       L_cost_amend,
                                       'C',
                                       NULL,
                                       NULL,
                                       L_po_exchange_rate,
                                       L_lc_exchange_rate) = FALSE then
                  return FALSE;
               end if;
               ---
               insert into lc_amendments(lc_ref_id,
                                         order_no,
                                         item,
                                         amended_field,
                                         original_value,
                                         new_value,
                                         effect,
                                         status,
                                         accept_date,
                                         confirm_date,
                                         amend_no,
                                         comments)
                                  values(L_lc_ref_id,
                                         I_order_no,
                                         L_po_item,
                                         'RI',
                                         L_cost_amend,
                                         to_char(0),
                                         0 - (L_cost_amend * L_oq_amend),
                                         'N',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
            --- order item addition
            elsif L_change_type = 'OIA' then
               --- check that the lc/order/item record does
               --- not exist already
                open C_LC_DETAIL_REC;
               fetch C_LC_DETAIL_REC into L_lc_detail_rec;
               close C_LC_DETAIL_REC;
               ---
               if L_lc_detail_rec = 'N' then
                  --- Retrieve the order item's cost and quantity
                   open C_ITEM_INFO;
                  fetch C_ITEM_INFO into L_po_item_qty;
                  ---
                  if ORDER_ITEM_ATTRIB_SQL.GET_UNIT_COST(O_error_message,
                                                         L_exists,
                                                         L_po_item_cost,
                                                         I_order_no,
                                                         L_po_item,
                                                         NULL,                --  I_pack_item
                                                         NULL) = FALSE then   -- I_location
                      return FALSE;
                  end if;
                  ---
                  if C_ITEM_INFO%NOTFOUND then
                     close C_ITEM_INFO;
                     O_error_message := SQL_LIB.CREATE_MSG('ERR_RET_COST_QTY',
                                                           L_po_item,
                                                           to_char(I_order_no),
                                                           NULL);
                     return FALSE;
                  end if;
                  ---
                  close C_ITEM_INFO;
                  ---
                  --- Convert order cost from order to letter of credit currency
                  if CURRENCY_SQL.CONVERT(O_error_message,
                                          L_po_item_cost,
                                          L_lc_currency,
                                          NULL,
                                          L_po_item_cost,
                                          'C',
                                          NULL,
                                          NULL,
                                          L_po_exchange_rate,
                                          L_lc_exchange_rate) = FALSE then
                     return FALSE;
                  end if;
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            I_order_no,
                                            L_po_item,
                                            'AI',
                                            to_char(0),
                                            to_char(L_po_item_cost),
                                            L_po_item_cost * L_po_item_qty,
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;   --- lc/order/item record
               ---
            elsif L_change_type = 'ESD' then
               --- get the new value from ordsku.earliest_ship_date
                open C_ITEM_EARLIEST_SHIP_DATE;
               fetch C_ITEM_EARLIEST_SHIP_DATE into L_new_value;
               ---
               if C_ITEM_EARLIEST_SHIP_DATE%NOTFOUND then
                  close C_ITEM_EARLIEST_SHIP_DATE;
                  O_error_message := SQL_LIB.CREATE_MSG('ESD_NOT_FOUND',
                                                        L_po_item,
                                                        to_char(I_order_no),
                                                        NULL);
                  return FALSE;
               end if;
               close C_ITEM_EARLIEST_SHIP_DATE;
               ---
               --- check for an amendment to the order/item's earliest ship date
                open C_PO_ITEM_ESD_AMEND;
               fetch C_PO_ITEM_ESD_AMEND into L_esd_amend;
               close C_PO_ITEM_ESD_AMEND;
               ---
               --- if no amendment exists, use the date from the detail table
               if L_esd_amend is NULL then
                  open C_LC_DETAIL_ESD;
                  fetch C_LC_DETAIL_ESD into L_esd_detail;
                  L_esd_amend := L_esd_detail;
                  close C_LC_DETAIL_ESD;
               end if;
               ---
               if to_date(L_new_value,'DD-MON-RR') != L_esd_amend then
                  insert into lc_amendments(lc_ref_id,
                                  order_no,
                                  item,
                                  amended_field,
                                  original_value,
                                  new_value,
                                  effect,
                                  status,
                                  accept_date,
                                  confirm_date,
                                  amend_no,
                                  comments)
                                 values(L_lc_ref_id,
                                        I_order_no,
                                        L_po_item,
                                        'ESD',
                                        TO_CHAR(L_esd_amend,'DD-MON-RR'),
                                        L_new_value,
                                        NULL,
                                        'N',
                                        NULL,
                                        NULL,
                                        NULL,
                                        NULL);
               end if; --- end new value != esd amend
               ---
            elsif L_change_type = 'LSD' then
               --- get the new value from ordsku.latest_ship_date
               open C_ITEM_LATEST_SHIP_DATE;
               fetch C_ITEM_LATEST_SHIP_DATE into L_new_value;
               ---
               if C_ITEM_LATEST_SHIP_DATE%NOTFOUND then
                  close C_ITEM_LATEST_SHIP_DATE;
                  O_error_message := SQL_LIB.CREATE_MSG('LSD_NOT_FOUND',
                                                        L_po_item,
                                                        to_char(I_order_no),
                                                        NULL);
                  return FALSE;
               end if;
               close C_ITEM_LATEST_SHIP_DATE;
               ---
               --- check for an amendment to the order/item's earliest ship date
               open C_PO_ITEM_LSD_AMEND;
               fetch C_PO_ITEM_LSD_AMEND into L_lsd_amend;
               close C_PO_ITEM_LSD_AMEND;
               ---
               --- if no amendment exists, use the date from the detail table
               if L_lsd_amend is NULL then
                  open C_LC_DETAIL_LSD;
                  fetch C_LC_DETAIL_LSD into L_lsd_detail;
                  L_lsd_amend := L_lsd_detail;
                  close C_LC_DETAIL_LSD;
               end if;
               ---
               if to_date(L_new_value,'DD-MON-RR') != L_lsd_amend then
                  insert into lc_amendments(lc_ref_id,
                                            order_no,
                                            item,
                                            amended_field,
                                            original_value,
                                            new_value,
                                            effect,
                                            status,
                                            accept_date,
                                            confirm_date,
                                            amend_no,
                                            comments)
                                     values(L_lc_ref_id,
                                            I_order_no,
                                            L_po_item,
                                            'LSD',
                                            to_char(L_lsd_amend,'DD-MON-RR'),
                                            L_new_value,
                                            NULL,
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);
               end if;   --- new value != lsd amend
            end if;      --- change type
         end if;         --- form status
      end loop;          --- order item records/loop
   end if;               --- letter of credit form type - 'S'hort or 'L'ong
   ---
   --- default the required documents from the order to the letter of credit.
   --- if the required document does not exist for the letter of credit, check
   --- if it exists as an amendment.  if the required document does not exist
   --- at either point, create a record on the req_docs table (worksheet status)
   --- or an amendment (not worksheet status).
   for C_REQ_DOC_REC in C_REQ_DOC loop
      L_req_doc_id := C_REQ_DOC_REC.doc_id;
      ---
      if L_lc_status = 'W' then
         if DOCUMENTS_SQL.INSERT_REQ_DOC(O_error_message,
                                         'LC',
                                         to_char(L_lc_ref_id),
                                         NULL,
                                         L_req_doc_id,
                                         NULL) = FALSE then
            return FALSE;
         end if;
      else
         L_doc_dummy := 'y';
         open C_DOC_AMEND_EXISTS;
         fetch C_DOC_AMEND_EXISTS into L_doc_dummy;
         close C_DOC_AMEND_EXISTS;
         if L_doc_dummy = 'y' then
            insert into lc_amendments(lc_ref_id,
                                      order_no,
                                      item,
                                      amended_field,
                                      original_value,
                                      new_value,
                                      effect,
                                      status,
                                      accept_date,
                                      confirm_date,
                                      amend_no,
                                      comments)
                              values (L_lc_ref_id,
                                      NULL,
                                      NULL,
                                      'ARQD',
                                      NULL,
                                      L_req_doc_id,
                                      NULL,
                                      'N',
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL);
         end if;
      end if;  --- status
   end loop;   --- required documents
   ---
   --- Recalc costs calculates the value from the lc_detail table.
   --- So, only want to recalc if changes did exist (L_order_item_change_exists).
   --- Only want to update lc_head if the letter of credit is in
   --- worksheet status.
   if L_lc_status = 'W' and L_order_item_change_exists = 'Y' then
      if LC_SQL.RECALC_COST(O_error_message,
                            L_recalc_cost,
                            L_lc_ref_id) = FALSE then
         return FALSE;
      end if;
      ---
      if LC_SQL.UPDATE_LCHEAD(O_error_message,
                              L_lc_ref_id) = FALSE then
         return FALSE;
      end if;
      ---
      update lc_head set amount    = L_recalc_cost
                   where lc_ref_id = L_lc_ref_id;
   end if;
   ---
   --- delete records from table because just processed the records.
    open C_LOCK_ORD_LC_AMENDMENTS;
   close C_LOCK_ORD_LC_AMENDMENTS;
   ---
   delete from ord_lc_amendments
         where order_no = I_order_no;
   return TRUE;
   ---
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;
END ORDER_UPDATE_LC;

--------------------------------------------------------------------------------------
FUNCTION CHECK_AMEND_EXISTS(O_error_message  IN OUT VARCHAR2,
                            O_exists         IN OUT BOOLEAN,
                            I_lc_ref_id      IN     LC_HEAD.LC_REF_ID%TYPE,
                            I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(50) := 'LC_SQL.CHECK_AMEND_EXISTS';
   L_exists   VARCHAR2(1);

   cursor C_CHECK_AMEND is
      select 'x'
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and order_no      = I_order_no
         and amended_field = 'RO';

BEGIN
   O_exists := FALSE;

   SQL_LIB.SET_MARK('OPEN','C_CHECK_AMEND','LC_AMENDMENTS',NULL);
   open C_CHECK_AMEND;
   SQL_LIB.SET_MARK('FETCH','C_CHECK_AMEND','LC_AMENDMENTS',NULL);
   fetch C_CHECK_AMEND into L_exists;

   if C_CHECK_AMEND%FOUND then
      O_exists := TRUE;
      O_error_message := SQL_LIB.CREATE_MSG('AMEND_EXISTS',to_char(I_order_no),NULL,NULL);
   end if;

   SQL_LIB.SET_MARK('CLOSE','C_CHECK_AMEND','LC_AMENDMENTS',NULL);
   close C_CHECK_AMEND;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      RETURN FALSE;
END CHECK_AMEND_EXISTS;

--------------------------------------------------------------------------------------
FUNCTION WRITE_LCORDAPP(O_error_message  IN OUT VARCHAR2,
                        I_update_ordlc   IN     BOOLEAN,
                        I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_SQL.WRITE_LCORDAPP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        exception_init (RECORD_LOCKED, -54);

   cursor C_LOCK_ORDLC is
      select 'x'
        from ordlc
       where order_no = I_order_no
         for update nowait;

BEGIN
   --- check the required information has been passed in
   if I_order_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('INSERT',NULL,'LC_ORDAPPLY',NULL);
   insert into lc_ordapply(order_no,
                           lc_ref_id,
                           form_type,
                           lc_type,
                           error_code)
                    values(I_order_no,
                           NULL,
                           NULL,
                           NULL,
                           NULL);

   --- the order is not attached to a letter of credit
   if I_update_ordlc = TRUE then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ORDLC','ORDLC',
                       'Order No: ' || to_char(I_order_no));
      open C_LOCK_ORDLC;
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ORDLC','ORDLC',
                       'Order No: ' || to_char(I_order_no));
      close C_LOCK_ORDLC;

      SQL_LIB.SET_MARK('UPDATE',NULL,'ORDLC',NULL);
      update ordlc set lc_ref_id = NULL,
                       lc_ind    = 'N'
                 where order_no = I_order_no;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ORDLC',
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_LCORDAPP;
--------------------------------------------------------------------------------------
FUNCTION DELETE_LCORDAPP(O_error_message  IN OUT VARCHAR2,
                         I_order_no       IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_SQL.DELETE_LCORDAPP';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        exception_init (RECORD_LOCKED, -54);

   cursor C_LOCK_LC_ORDAPPLY is
      select 'x'
        from lc_ordapply
       where order_no = I_order_no
         for update nowait;

BEGIN
   open C_LOCK_LC_ORDAPPLY;
   close C_LOCK_LC_ORDAPPLY;

   delete from lc_ordapply
    where order_no = I_order_no;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'LC_ORDAPPLY',
                                            to_char(I_order_no),
                                            NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END DELETE_LCORDAPP;
--------------------------------------------------------------------------------------
FUNCTION LC_DOWNLOAD(O_error_message  IN OUT VARCHAR2,
                     I_lc_type        IN     VARCHAR2,
                     I_where_clause   IN     VARCHAR2)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'LC_SQL.LC_DOWNLOAD';
   L_text            VARCHAR2(9000);
   L_cursor          INTEGER;
   L_rows_processed  NUMBER;

BEGIN
   L_text := 'INSERT INTO lc_download SELECT lc_ref_id, :l_lc_type FROM lc_head WHERE '
             || I_where_clause || ' and lc_ref_id not in (select lc_ref_id from lc_download where lc_download_type = :l_lc_type)';
   L_cursor := DBMS_SQL.OPEN_CURSOR;
   DBMS_SQL.PARSE(L_cursor, L_text, DBMS_SQL.NATIVE);
   DBMS_SQL.BIND_VARIABLE(L_cursor, 'l_lc_type', I_lc_type);
   L_rows_processed := DBMS_SQL.EXECUTE(L_cursor); 
   DBMS_SQL.CLOSE_CURSOR(L_cursor);
   
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END LC_DOWNLOAD;

--------------------------------------------------------------------------------------
FUNCTION DETAILS_EXIST(O_error_message  IN OUT  VARCHAR2,
                       O_exist          IN OUT  BOOLEAN,
                       O_num_recs       IN OUT  NUMBER,
                       I_order_no       IN      ORDHEAD.ORDER_NO%TYPE,
                       I_detail_type    IN      VARCHAR2,
                       I_lc_ref_id      IN      LC_HEAD.LC_REF_ID%TYPE)

RETURN BOOLEAN IS
   L_program    VARCHAR2(50) := 'LC_SQL.DETAILS_EXIST';
   L_exists     VARCHAR2(1);
   L_req_exists BOOLEAN;

   cursor C_LC_DETAIL is
      select COUNT(distinct order_no)
        from lc_detail
       where lc_ref_id = I_lc_ref_id;

   cursor C_LC_DETAIL_ORD is
      select COUNT(distinct order_no)
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         and order_no != I_order_no;

   cursor C_LC_AMENDMENTS is
      select 'x'
        from lc_amendments
       where lc_ref_id = I_lc_ref_id;

   cursor C_LC_ACTIVITY is
      select 'x'
        from lc_activity
       where lc_ref_id = I_lc_ref_id;

BEGIN
   O_exist := TRUE;

   --- check the lc_detail table

   if I_detail_type = 'D' and I_order_no is not NULL then
      SQL_LIB.SET_MARK ('OPEN', 'C_LC_DETAIL_ORD', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      open C_LC_DETAIL_ORD;
      SQL_LIB.SET_MARK ('FETCH', 'C_LC_DETAIL_ORD', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      fetch C_LC_DETAIL_ORD into O_num_recs;

      if O_num_recs = 0 then
         O_error_message := SQL_LIB.CREATE_MSG ('NO_LC_DETLS_EXIST',
                                                 NULL,
                                                 NULL,
                                                 NULL);
         O_exist := FALSE;
      end if;

      SQL_LIB.SET_MARK ('CLOSE', 'C_LC_DETAIL_ORD', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      close C_LC_DETAIL_ORD;

   --- check the lc_amendments table
   elsif I_detail_type = 'D' and I_order_no is NULL then
      SQL_LIB.SET_MARK ('OPEN', 'C_LC_DETAIL', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      open C_LC_DETAIL;
      SQL_LIB.SET_MARK ('FETCH', 'C_LC_DETAIL', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      fetch C_LC_DETAIL into O_num_recs;

      if O_num_recs = 0 then
         O_error_message := SQL_LIB.CREATE_MSG ('NO_LC_DETLS_EXIST',
                                                 NULL,
                                                 NULL,
                                                 NULL);
         O_exist := FALSE;
      end if;

      SQL_LIB.SET_MARK ('CLOSE', 'C_LC_DETAIL', 'LC_DETAIL',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      close C_LC_DETAIL;

   elsif I_detail_type = 'A' then
      SQL_LIB.SET_MARK ('OPEN', 'C_LC_AMENDMENTS', 'LC_AMENDMENTS',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      open C_LC_AMENDMENTS;
      SQL_LIB.SET_MARK ('FETCH', 'C_LC_AMENDMENTS', 'LC_AMENDMENTS',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      fetch C_LC_AMENDMENTS into L_exists;

      if C_LC_AMENDMENTS%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_LC_AMEND_EXIST',
                                                NULL,
                                                NULL,
                                                NULL);
         O_exist := FALSE;
      end if;

      SQL_LIB.SET_MARK ('CLOSE', 'C_LC_AMENDMENTS', 'LC_AMENDMENTS',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      close C_LC_AMENDMENTS;

   --- check the lc_activity table
   elsif I_detail_type = 'C' then
      SQL_LIB.SET_MARK ('OPEN', 'C_LC_ACTIVITY', 'LC_ACTIVITY',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      open C_LC_ACTIVITY;
      SQL_LIB.SET_MARK ('FETCH', 'C_LC_ACTIVITY', 'LC_ACTIVITY',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      fetch C_LC_ACTIVITY into L_exists;

      if C_LC_ACTIVITY%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_LC_AMEND_EXIST',
                                                NULL,
                                                NULL,
                                                NULL);
         O_exist := FALSE;
      end if;

      SQL_LIB.SET_MARK ('CLOSE', 'C_LC_ACTIVITY', 'LC_ACTIVITY',
                        'lc ref id: ' || to_char(I_lc_ref_id));
      close C_LC_ACTIVITY;

   --- check the req_docs table
   elsif I_detail_type = 'R' then
      if DOCUMENTS_SQL.REQ_DOCS_EXIST_MOD_KEY(O_error_message,
                                              L_req_exists,
                                              'LC',
                                              to_char(I_lc_ref_id),
                                              NULL,
                                              NULL) = FALSE then
         return FALSE;
      end if;

      if L_req_exists = FALSE then
         O_error_message := SQL_LIB.CREATE_MSG('NO_LC_REQ_DOCS_EXIST',
                                                NULL,
                                                NULL,
                                                NULL);
         O_exist := FALSE;
      end if;

   --- invalid detail type
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END DETAILS_EXIST;

-----------------------------------------------------------------------------------
END LC_SQL;
/