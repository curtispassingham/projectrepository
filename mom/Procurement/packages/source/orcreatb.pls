CREATE OR REPLACE PACKAGE BODY ORDER_CREATE_SQL AS
   ---
   L_program        VARCHAR2(60):= NULL;
   L_order_type     ORDHEAD.ORDER_TYPE%TYPE := NULL;
   ---
-----------------------------------------------------------------------------
FUNCTION CREATE_ORDER(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_approve_ind     IN OUT   VARCHAR2,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                      I_supplier        IN       SUPS.SUPPLIER%TYPE,
                      I_dept            IN       DEPS.DEPT%TYPE,
                      I_order_curr      IN       CURRENCIES.CURRENCY_CODE%TYPE,
                      I_vdate           IN       DATE,
                      I_order_type      IN       ORDHEAD.ORDER_TYPE%TYPE)

RETURN BOOLEAN IS

   L_user_name          ORDHEAD.ORIG_APPROVAL_ID%TYPE;
   L_currency_code      CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate      CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_terms              SUPS.TERMS%TYPE;
   L_freight_terms      SUPS.FREIGHT_TERMS%TYPE;
   L_sup_status         SUPS.SUP_STATUS%TYPE;
   L_qc_ind             SUPS.QC_IND%TYPE;
   L_edi_po_ind         SUPS.EDI_PO_IND%TYPE;
   L_pre_mark_ind       SUPS.PRE_MARK_IND%TYPE;
   L_ship_method        SUPS.SHIP_METHOD%TYPE;
   L_payment_method     SUPS.PAYMENT_METHOD%TYPE;
   L_sup_seq_no         ADDR.SEQ_NO%TYPE;
   L_base_cntry         SYSTEM_OPTIONS.BASE_COUNTRY_ID%TYPE;
   L_in_dd              NUMBER(2)    := to_number(to_char(I_vdate , 'DD'));
   L_in_mm              NUMBER(2)    := to_number(to_char(I_vdate , 'MM'));
   L_in_yyyy            NUMBER(4)    := to_number(to_char(I_vdate , 'YYYY'));
   L_out_dd             NUMBER(2)    := NULL;
   L_out_mm             NUMBER(2)    := NULL;
   L_out_yyyy           NUMBER(4)    := NULL;
   L_otb_eow            DATE;
   L_return_code        VARCHAR2(20) := 'FALSE';
   L_order_cost_ord     ORDLOC.UNIT_COST%TYPE;
   L_order_cost_prim    ORDLOC.UNIT_COST%TYPE;
   L_outstand_cost_ord  ORDLOC.UNIT_COST%TYPE;
   L_outstand_cost_prim ORDLOC.UNIT_COST%TYPE;
   L_cancel_cost_ord    ORDLOC.UNIT_COST%TYPE;
   L_cancel_cost_prim   ORDLOC.UNIT_COST%TYPE;
   L_order_retail_ord   ORDLOC.UNIT_RETAIL%TYPE;
   L_order_retail_prim  ORDLOC.UNIT_RETAIL%TYPE;
   L_vat_ord            ORDLOC.UNIT_RETAIL%TYPE;
   L_vat_prim           ORDLOC.UNIT_RETAIL%TYPE;
   L_landed_cost_ord    NUMBER;
   L_landed_cost_prim   NUMBER;
   L_expenses_ord       NUMBER;
   L_expenses_prim      NUMBER;
   L_duty_ord           NUMBER;
   L_duty_prim          NUMBER;
   L_brkt_pct_off       ordloc.qty_ordered%TYPE;
   L_qty                ordloc.qty_ordered%TYPE;
   L_ord_appr_amt       rtk_role_privs.ord_appr_amt%TYPE:=NULL;
   L_ord_appr_code      system_options.ord_appr_amt_code%TYPE:=NULL;
   L_compare_amt        ORDLOC.UNIT_RETAIL%TYPE;
   L_subclass_list      VARCHAR2(154);
   L_excess_ind         VARCHAR2(5);
   L_import_id          SUPS_IMP_EXP.IMPORT_ID%TYPE;
   L_import_type        SUPS_IMP_EXP.IMPORT_TYPE%TYPE;
   L_routing_loc_id     SUPS_ROUTING_LOC.ROUTING_LOC_ID%TYPE;
   L_exists             BOOLEAN;
   L_max_import_country COUNTRY.COUNTRY_ID%TYPE;
   L_min_import_country COUNTRY.COUNTRY_ID%TYPE;
   L_import_ord         VARCHAR2(1) := 'N';
   L_cost_incl_tax_ord  NUMBER;
   L_cost_incl_tax_prim NUMBER;
   L_location           ORDHEAD.LOCATION%TYPE;

   L_not_before_date    ORDHEAD.NOT_BEFORE_DATE%TYPE;
   L_not_after_date     ORDHEAD.NOT_AFTER_DATE%TYPE;
   L_pickup_date        ORDHEAD.PICKUP_DATE%TYPE;
   L_10n_obj            L10N_OBJ := L10N_OBJ();

   cursor C_ORD_AUTO is
      select store
        from ordauto_temp
       where order_no = I_order_no;

   cursor C_SEQ is
      select seq_no
        from addr
       where key_value_1      = to_char(I_supplier)
         and module           = 'SUPP'
         and addr_type        = '04'
         and primary_addr_ind = 'Y';

   cursor C_SYSTEM_OPTIONS is
   select base_country_id,
          ord_appr_amt_code
     from system_options;

   cursor C_GET_IMPORT_COUNTRY is
   select min(country_id),
          max(country_id)
     from ordloc o,
          addr a
    where o.order_no = I_order_no
      and a.module in ('ST','WH','WFST')
      and a.key_value_1 = TO_CHAR(o.location)
      and a.primary_addr_ind = 'Y'
      and a.addr_type = decode(a.module,'WFST','07','01');


   cursor C_CHECK_IMPORT_ORDER is
   select 'Y'
     from ordsku
    where order_no = I_order_no
      and origin_country_id != L_min_import_country
      and rownum = 1;

BEGIN
   L_program := 'ORDER_CREATE_SQL.CREATE_ORDER';
   L_order_type  :=  I_order_type;
   ---
   if SUPP_ATTRIB_SQL.ORDER_SUP_DETAILS(O_error_message,
                                        L_currency_code,
                                        L_terms,
                                        L_freight_terms,
                                        L_sup_status,
                                        L_qc_ind,
                                        L_edi_po_ind,
                                        L_pre_mark_ind,
                                        L_ship_method,
                                        L_payment_method,
                                        I_supplier) = FALSE then
      return FALSE;
   end if;
   ---
   if L_payment_method = 'LC' then
      L_payment_method := NULL;
   end if;
   ---
   if (L_sup_status <> 'A') then
      O_error_message := SQL_LIB.CREATE_MSG('INACTIVE_SUPPLIER',
                                             to_char(I_supplier),
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_SEQ','addr',to_char(I_supplier));
   open C_SEQ;
   SQL_LIB.SET_MARK('FETCH','C_SEQ','addr',to_char(I_supplier));
   fetch C_SEQ into L_sup_seq_no;
   if C_SEQ%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ADD',
                                             to_char(I_supplier),
                                             NULL,
                                             NULL);
      SQL_LIB.SET_MARK('CLOSE','C_SEQ','addr',to_char(I_supplier));
      close C_SEQ;
      RETURN FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_SEQ','addr',to_char(I_supplier));
   close C_SEQ;
   ---
   CAL_TO_454_LDOW(L_in_dd,
                L_in_mm,
                L_in_yyyy,
                L_out_dd,
                L_out_mm,
                L_out_yyyy,
                L_return_code,
                O_error_message);
   if L_return_code = 'FALSE' then
      return FALSE;
   end if;
   ---
   L_otb_eow := TO_DATE(TO_CHAR(L_out_dd,'09')||TO_CHAR(L_out_mm,'09')
                            ||TO_CHAR(L_out_yyyy,'0999'),'DDMMYYYY');
   ---
   SQL_LIB.SET_MARK('OPEN','SYSTEM_OPTIONS','system_options',NULL);
   open C_SYSTEM_OPTIONS;
   SQL_LIB.SET_MARK('FETCH','SYSTEM_OPTIONS','system_options',NULL);
   fetch C_SYSTEM_OPTIONS into L_base_cntry,
                               L_ord_appr_code;
   SQL_LIB.SET_MARK('CLOSE','SYSTEM_OPTIONS','system_options',NULL);
   close C_SYSTEM_OPTIONS;
   ---
   if SQL_LIB.GET_USER_NAME(O_error_message,
                            L_user_name) = FALSE then
      return FALSE;
   end if;
   ---
   if CURRENCY_SQL.GET_RATE(O_error_message,
                            L_exchange_rate,
                            I_order_curr,
                            'P',
                            I_vdate) = FALSE then
      return FALSE;
   end if;
   ---
   if ORDER_CALC_SQL.CALC_HEADER_DATES(O_error_message,
                                       L_pickup_date,
                                       L_not_before_date,
                                       L_not_after_date,
                                       I_order_no,
                                       I_supplier) = FALSE then
      return FALSE;
   end if;
   ---
   open C_ORD_AUTO;
   fetch C_ORD_AUTO into L_location;
   close C_ORD_AUTO;
   ---
   SQL_LIB.SET_MARK('INSERT', NULL, 'ordhead', 'order no: '||to_char(I_order_no));
   insert into ordhead(order_no,
                       order_type,
                       dept,
                       buyer,
                       supplier,
                       supp_add_seq_no,
                       loc_type,
                       location,
                       promotion,
                       qc_ind,
                       written_date,
                       not_before_date,
                       not_after_date,
                       otb_eow_date,
                       earliest_ship_date,
                       latest_ship_date,
                       close_date,
                       terms,
                       freight_terms,
                       orig_ind,
                       payment_method,
                       ship_method,
                       purchase_type,
                       status,
                       orig_approval_date,
                       orig_approval_id,
                       ship_pay_method,
                       fob_trans_res,
                       fob_trans_res_desc,
                       fob_title_pass,
                       fob_title_pass_desc,
                       edi_sent_ind,
                       edi_po_ind,
                       import_order_ind,
                       import_country_id,
                       po_ack_recvd_ind,
                       include_on_order_ind,
                       vendor_order_no,
                       exchange_rate,
                       factory,
                       agent,
                       discharge_port,
                       lading_port,
                       freight_contract_no,
                       po_type,
                       pre_mark_ind,
                       currency_code,
                       reject_code,
                       contract_no,
                       last_sent_rev_no,
                       pickup_loc,
                       pickup_no,
                       pickup_date,
                       comment_desc,
                       import_id,
                       import_type,
                       routing_loc_id)
      values(I_order_no,
             I_order_type,
             I_dept,
             NULL,
             I_supplier,
             L_sup_seq_no,
             DECODE(I_order_type,
                    'DSD','S',
                          NULL),
             DECODE(I_order_type,
                    'DSD',L_location,
                          NULL),
             NULL,
             L_qc_ind,
             I_vdate,
             I_vdate,
             I_vdate,
             L_otb_eow,
             I_vdate,
             I_vdate,
             NULL,
             L_terms,
             L_freight_terms,
             DECODE(I_order_type,
                    'DSD',7,2),
             L_payment_method,
             L_ship_method,
             NULL,
             'W',
             DECODE(I_order_type,
                    'DSD', I_vdate,
                    'N/B', NULL),
             DECODE(I_order_type,
                    'DSD', L_user_name,
                    'N/B', NULL),
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'N',
             L_edi_po_ind,
             'N',
             L_base_cntry,
             'N',
             'Y',
             NULL,
             L_exchange_rate,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'N',
             I_order_curr,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             DECODE(I_order_type,
                    'DSD', I_vdate,
                           L_pickup_date), --L_pickup_date,
             NULL,
             NULL,
             NULL,
             NULL);

   if ORDER_CREATE_SQL.ORDER_ITEM(O_error_message,
                                  I_order_no,
                                  I_vdate) = FALSE then
      return FALSE;
   end if;
   --
   /*Postpone setting the status to Approve until after the ordloc and ordsku records have been inserted.
     This way, the ordhead trigger that creates the revisions will create revisions for ordloc and ordsku */
   update ordhead
      set status = 'A'
    where order_no   = I_order_no
      and order_type = 'DSD';
   ---
   -- Set the Import Country and Import Order Ind
   open C_GET_IMPORT_COUNTRY;
   fetch C_GET_IMPORT_COUNTRY into L_min_import_country,L_max_import_country;
   close C_GET_IMPORT_COUNTRY;
   --
   if L_min_import_country is NULL then
      L_min_import_country := L_base_cntry;
   elsif L_min_import_country!= L_max_import_country then
      O_error_message := SQL_LIB.CREATE_MSG('NOT_ALL_LOC_SAME_COUNTRY',
                                            NULL,
                                            NULL,
                                            NULL);
      return FALSE;
   end if;
   --
   open C_CHECK_IMPORT_ORDER;
   fetch C_CHECK_IMPORT_ORDER into L_import_ord;
   close C_CHECK_IMPORT_ORDER;
   ---

   if SUPP_ATTRIB_SQL.GET_IMPORT_ID(O_error_message,
                                    L_import_id,
                                    L_import_type,
                                    L_exists,
                                    I_supplier) = FALSE then
      return FALSE;
   else
      if L_exists = FALSE then
         L_import_id := NULL;
         L_import_type := NULL;
      end if;
   end if;

   if SUPP_ATTRIB_SQL.GET_ROUTING_LOC(O_error_message,
                                      L_routing_loc_id,
                                      L_exists,
                                      I_supplier) = FALSE then
      return FALSE;
   else
      if L_exists = FALSE then
         L_routing_loc_id := NULL;
      end if;
   end if;
   ---
   update ordhead
      set import_order_ind  = L_import_ord,
          import_country_id = L_min_import_country,
          import_id = DECODE(L_import_ord, 'Y', L_import_id),
          import_type = DECODE(L_import_ord, 'Y', L_import_type),
          routing_loc_id = DECODE(L_import_ord, 'Y',L_routing_loc_id),
          last_update_id = get_user,
          last_update_datetime = sysdate
    where order_no = I_order_no;
   ---

   L_10n_obj.procedure_key := 'CREATE_TRAN_UTIL_CODE';
   L_10n_obj.doc_type := 'PO';
   L_10n_obj.doc_id := I_order_no;
   L_10n_obj.country_id := L_min_import_country;

   --- Make a call to the l10n function
   if L10N_SQL.EXEC_FUNCTION (O_error_message,
                              L_10n_obj) = FALSE then
         return FALSE;
   end if;

   if ORDER_CALC_SQL.TOTAL_COST_RETAIL(O_error_message,
                                       L_order_cost_ord,
                                       L_order_cost_prim,
                                       L_outstand_cost_ord,
                                       L_outstand_cost_prim,
                                       L_cancel_cost_ord,
                                       L_cancel_cost_prim,
                                       L_order_retail_ord,
                                       L_order_retail_prim,
                                       L_vat_ord,
                                       L_vat_prim,
                                       L_landed_cost_ord,
                                       L_landed_cost_prim,
                                       L_expenses_ord,
                                       L_expenses_prim,
                                       L_duty_ord,
                                       L_duty_prim,
                                       L_brkt_pct_off,
                                       L_qty,
                                       L_cost_incl_tax_ord,
                                       L_cost_incl_tax_prim,
                                       I_order_no,
                                       L_exchange_rate,
                                       I_order_curr,
                                       L_base_cntry)= FALSE then
      return FALSE;
   end if;
   ---
   if ORDER_APPROVE_SQL.GET_APPROVAL_AMT(O_error_message,
                                         L_ord_appr_amt) = FALSE then
      return FALSE;
   end if;
   ---
   if L_ord_appr_code = 'R' then
      L_compare_amt := L_order_retail_prim;
   else
      L_compare_amt := L_order_cost_prim;
   end if;
   ---
   if L_compare_amt > L_ord_appr_amt then
      O_approve_ind   := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('NOT_ALLOW_APPR_ORD',
                                            to_char(L_ord_appr_amt),NULL,NULL);
      return TRUE;
   else
      O_approve_ind := 'Y';
   end if;
   ---
   if TICKET_SQL.APPROVE(O_error_message,
                         I_order_no,
                         'N') = FALSE then
      return FALSE;
   end if;
   ---
   if I_order_type != 'DSD' then
      if ROUNDING_SQL.ORDER_INTERFACE(O_error_message,
                                      I_order_no,
                                      I_supplier,
                                      'N', -- update_orig_repl_qty
                                      'Y') = FALSE then -- skip_alloc_round
         return FALSE;
      end if;
   end if;
   ---

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CREATE_ORDER;
-----------------------------------------------------------------------------
FUNCTION ORDER_ITEM(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                    I_vdate           IN       DATE)
RETURN BOOLEAN IS

   L_unit_cost         ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE;

   -- new variable to hold the instock date value calculated
   L_est_instock_date  ORDLOC.ESTIMATED_INSTOCK_DATE%TYPE;
   L_supp_pack_size    ITEM_SUPP_COUNTRY.SUPP_PACK_SIZE%TYPE;

L_elc_ind     SYSTEM_OPTIONS.ELC_IND%TYPE;
   L_pack_ind    ITEM_MASTER.ITEM%TYPE;

   TYPE s_pack_comp IS TABLE OF ITEM_MASTER.ITEM%TYPE;
   L_pack_comp   s_pack_comp;

   cursor C_ORD_AUTO is
      select item,
             ref_item,
             store,
             cost_source,
             unit_cost,
             origin_country_id,
             supplier,
             qty,
             unit_retail
        from ordauto_temp
       where order_no = I_order_no;
  cursor C_SYS_ELC_IND is
      select elc_ind
        from system_options;

   cursor C_PACK_IND (I_item item_master.item%TYPE) is
      select pack_ind
        from item_master
       where item = I_item;

   cursor C_PACK_COMP (I_item item_master.item%TYPE)is
      select item
        from packitem_breakout
       where pack_no = I_item;
BEGIN
   L_program := 'ORDER_CREATE_SQL.ORDER_ITEM';
   ---
 SQL_LIB.SET_MARK('OPEN','C_SYS_ELC_IND','system_options',to_char(I_order_no));
   open C_SYS_ELC_IND;
   SQL_LIB.SET_MARK('FETCH','C_SYS_ELC_IND','system_options',to_char(I_order_no));
   fetch C_SYS_ELC_IND into L_elc_ind;
   SQL_LIB.SET_MARK('CLOSE','C_SYS_ELC_IND','system_options',to_char(I_order_no));
   close C_SYS_ELC_IND;
   ---
   SQL_LIB.SET_MARK('FETCH','C_ORD_AUTO_LOOP','ordauto_temp',to_char(I_order_no));
   for rec_item in C_ORD_AUTO loop
      ---
      if ITEM_SUPP_COUNTRY_LOC_SQL.GET_COST(O_error_message,
                                            L_unit_cost,
                                            rec_item.item,
                                            rec_item.supplier,
                                            rec_item.origin_country_id,
                                            rec_item.store) = FALSE then
         return FALSE;
      end if;
      ---
      if SUPP_ITEM_ATTRIB_SQL.GET_SUPP_PACK_SIZE(O_error_message,
                                                 L_supp_pack_size,
                                                 rec_item.item,
                                                 rec_item.supplier,
                                                 rec_item.origin_country_id) = FALSE then
         return FALSE;
      end if;
      ---
      if L_order_type = 'DSD' then
         L_supp_pack_size := 1 ;
      end if;
      ---
      SQL_LIB.SET_MARK('INSERT', NULL, 'ordsku', 'order no: '||to_char(I_order_no));
      insert into ordsku(order_no,
                         item,
                         ref_item,
                         origin_country_id,
                         earliest_ship_date,
                         latest_ship_date,
                         supp_pack_size,
                         non_scale_ind)
                 values (I_order_no,
                         rec_item.item,
                         rec_item.ref_item,
                         rec_item.origin_country_id,
                         I_vdate,
                         I_vdate,
                         L_supp_pack_size,
                         'N');
      ---

      -- call the ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT function to calculate for the estimated instock date.
      if ORDER_INSTOCK_DATE_SQL.GET_INSTOCK_DT(O_error_message,
                                               L_est_instock_date,
                                               I_order_no,
                                               rec_item.item,
                                               rec_item.store,
                                               rec_item.supplier) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('INSERT', NULL, 'ordloc', 'order no: '||to_char(I_order_no));
      insert into ordloc(order_no,
                         item,
                         location,
                         loc_type,
                         unit_retail,
                         qty_ordered,
                         qty_prescaled,
                         qty_received,
                         last_received,
                         qty_cancelled,
                         cancel_code,
                         cancel_date,
                         cancel_id,
                         original_repl_qty,
                         unit_cost,
                         unit_cost_init,
                         cost_source,
                         non_scale_ind,
                         estimated_instock_date)
                 values (I_order_no,
                         rec_item.item,
                         rec_item.store,
                         'S',
                         rec_item.unit_retail,
                         rec_item.qty,
                         rec_item.qty,
                         NULL,        --qty_received
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         rec_item.unit_cost,
                         L_unit_cost,
                         rec_item.cost_source,
                         'N',
                         L_est_instock_date);

      ---
    --Defaulting Expenses
      ---
      SQL_LIB.SET_MARK('OPEN','C_PACK_IND','item_master',to_char(I_order_no));
      open C_PACK_IND(rec_item.item);
      SQL_LIB.SET_MARK('FETCH','C_PACK_IND','item_master',to_char(I_order_no));
      fetch C_PACK_IND into L_pack_ind;
      SQL_LIB.SET_MARK('CLOSE','C_PACK_IND','item_master',to_char(I_order_no));
      close C_PACK_IND;

      if L_elc_ind = 'Y' then
         if L_pack_ind = 'N' then
            if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                  I_order_no,
                                                  rec_item.item,
                                                  NULL,
                                                  rec_item.store,
                                                  'S') = FALSE then
               return FALSE;
            end if;
            ---
            if ORDER_HTS_SQL.DEFAULT_CALC_HTS(O_error_message,
                                              I_order_no,
                                              rec_item.item,
                                              NULL) = FALSE then
              return FALSE;
            end if;
         else
            SQL_LIB.SET_MARK('OPEN','C_PACK_COMP','packitem_breakout',to_char(I_order_no));
            open C_PACK_COMP(rec_item.item);
            SQL_LIB.SET_MARK('FETCH','C_PACK_COMP','packitem_breakout',to_char(I_order_no));
            fetch C_PACK_COMP bulk collect into L_pack_comp;
            ---
            for i in  L_pack_comp.FIRST..L_pack_comp.LAST loop
                if ORDER_EXPENSE_SQL.INSERT_COST_COMP(O_error_message,
                                                      I_order_no,
                                                      rec_item.item,
                                                      L_pack_comp(i),
                                                      rec_item.store,
                                                      'S') = FALSE then
                   return FALSE;
                end if;
                ---
                if ORDER_HTS_SQL.DEFAULT_CALC_HTS(O_error_message,
                                                  I_order_no,
                                                  rec_item.item,
                                                  L_pack_comp(i)) = FALSE then
                  return FALSE;
                end if;
            end loop;
            ---
            SQL_LIB.SET_MARK('CLOSE','C_PACK_COMP','packitem_breakout',to_char(I_order_no));
            close C_PACK_COMP;
            ---
         end if;
         ---
         if ELC_CALC_SQL.CALC_COMP(O_error_message,
                                   'PE',
                                   rec_item.item,
                                   NULL,
                                   NULL,
                                   NULL,
                                   I_order_no,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL) = FALSE then
            return FALSE;
         end if; --PO level expense recalc.
       end if; -- End ELC_IND = Y
   end loop;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ORDER_ITEM;
-----------------------------------------------------------------------------
END ORDER_CREATE_SQL;
/
