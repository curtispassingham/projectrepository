
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY UPDATE_OBJECT_SQL AS
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_GRADE (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                             I_type            IN     VARCHAR2,
                             I_sum_ratio       IN     NUMBER,
                             I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                             I_uop_type        IN     VARCHAR2) RETURN BOOLEAN IS
   TYPE ORD_CURSOR is REF CURSOR;
   C_UPDATE               ORD_CURSOR;
   L_item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE;
   L_item                 ORDLOC_WKSHT.ITEM%TYPE;
   L_diff_1               ORDLOC_WKSHT.DIFF_1%TYPE;
   L_diff_2               ORDLOC_WKSHT.DIFF_2%TYPE;
   L_diff_3               ORDLOC_WKSHT.DIFF_3%TYPE;
   L_diff_4               ORDLOC_WKSHT.DIFF_4%TYPE;
   L_store_grade_group_id ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE;
   L_store_grade          ORDLOC_WKSHT.STORE_GRADE%TYPE;
   L_calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE;
   L_statement            VARCHAR2(9000);
   L_standard_uom         ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_variance_qty         ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_act_qty              ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_uop                  ORDLOC_WKSHT.UOP%TYPE;
   L_supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple             ORDSKU.SUPP_PACK_SIZE%TYPE;
BEGIN
   L_statement := 'select o2.item_parent, o2.item, o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4, o2.store_grade_group_id,
                          o2.store_grade, o2.standard_uom, o2.wksht_qty, o2.uop, o2.supp_pack_size, ';
   if I_type = 'D' then
      L_statement := L_statement || 'o2.calc_qty + nvl(((ordloc_wksht.act_qty*store_grade_dist_temp.dist_pct)/100),0) ';
   elsif I_type = 'R' then
      L_statement := L_statement ||'o2.calc_qty + nvl(((ordloc_wksht.act_qty/:I_sum_ratio)*store_grade_dist_temp.dist_ratio),0) ';
   elsif I_type = 'Q' then
      L_statement := L_statement || 'o2.calc_qty + (nvl(store_grade_dist_temp.dist_qty,0) * decode(:I_uop_type,''C'',o2.supp_pack_size,1)) ';
   end if;
   ---
   L_statement := L_statement ||
        'from ordloc_wksht, store_grade_dist_temp, ordloc_wksht o2 ' ||
       'where ordloc_wksht.order_no = :I_order_no '  ||
              I_where_clause || ' ' ||
         'and o2.order_no = ordloc_wksht.order_no ' ||
         'and nvl(o2.item_parent,-1) = nvl(ordloc_wksht.item_parent,-1) '||
         'and nvl(o2.item,-1) = nvl(ordloc_wksht.item,-1) '||
         'and nvl(o2.diff_1,-1) = nvl(ordloc_wksht.diff_1,-1) ' ||
         'and nvl(o2.diff_2,-1) = nvl(ordloc_wksht.diff_2,-1) ' ||
         'and nvl(o2.diff_3,-1) = nvl(ordloc_wksht.diff_3,-1) ' ||
         'and nvl(o2.diff_4,-1) = nvl(ordloc_wksht.diff_4,-1) ' ||
         'and o2.store_grade_group_id = store_grade_dist_temp.store_grade_group_id ' ||
         'and o2.store_grade = store_grade_dist_temp.store_grade';
   ---
   if I_type = 'D' then
      EXECUTE IMMEDIATE L_statement USING I_order_no;
      open C_UPDATE for L_statement USING I_order_no;
   elsif I_type = 'R' then
      EXECUTE IMMEDIATE L_statement USING I_sum_ratio, I_order_no;
      open C_UPDATE for L_statement USING I_sum_ratio, I_order_no;
   elsif I_type = 'Q' then
      EXECUTE IMMEDIATE L_statement USING I_uop_type, I_order_no;
      open C_UPDATE for L_statement USING I_uop_type, I_order_no;
   end if;
   LOOP
      fetch C_UPDATE into L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_diff_3,
                          L_diff_4,
                          L_store_grade_group_id,
                          L_store_grade,
                          L_standard_uom,
                          L_wksht_qty,
                          L_uop,
                          L_supp_pack_size,
                          L_calc_qty;

      EXIT WHEN C_UPDATE%NOTFOUND;
      ---
      ---
      if L_uop = L_standard_uom then
         L_multiple := 1;
      else
         L_multiple := L_supp_pack_size;
      end if;
      ---
      if L_uop = L_standard_uom and
         L_standard_uom <> 'EA' then
         -- Round L_calc_qty to the fourth decimal
         L_wksht_qty := ROUND(L_calc_qty, 4);
         L_act_qty := L_wksht_qty;
         L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
      else -- UOP is case or standard UOM(eaches) - truncate these whole numbers
         -- Trunc L_calc_qty down to the next whole number
         L_wksht_qty := TRUNC(L_calc_qty/L_multiple);
         L_act_qty := (L_wksht_qty * L_multiple);
         L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
      end if;
      ---
      update ordloc_wksht
         set act_qty = L_act_qty,
             calc_qty = L_calc_qty,
             variance_qty = L_variance_qty,
             wksht_qty = L_wksht_qty
       where order_no = I_order_no
         and nvl(item_parent,-1) = nvl(L_item_parent,-1)
         and nvl(item,-1) = nvl(L_item,-1)
         and nvl(diff_1,-1) = nvl(L_diff_1,-1)
         and nvl(diff_2,-1) = nvl(L_diff_2,-1)
         and nvl(diff_3,-1) = nvl(L_diff_3,-1)
         and nvl(diff_4,-1) = nvl(L_diff_4,-1)
         and store_grade_group_id = L_store_grade_group_id
         and store_grade = L_store_grade;
      ---
   END LOOP;
   close C_UPDATE;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'UPDATE_OBJECT_SQL.UPDATE_ORDER_GRADE', NULL);
      return FALSE;
END UPDATE_ORDER_GRADE;
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_LOC (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                           I_type            IN     VARCHAR2,
                           I_sum_ratio       IN     NUMBER,
                           I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                           I_uop_type        IN     VARCHAR2) RETURN BOOLEAN IS
   TYPE ORD_CURSOR is REF CURSOR;
   C_UPDATE               ORD_CURSOR;
   L_item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE;
   L_item                 ORDLOC_WKSHT.ITEM%TYPE;
   L_diff_1               ORDLOC_WKSHT.DIFF_1%TYPE;
   L_diff_2               ORDLOC_WKSHT.DIFF_2%TYPE;
   L_diff_3               ORDLOC_WKSHT.DIFF_3%TYPE;
   L_diff_4               ORDLOC_WKSHT.DIFF_4%TYPE;
   L_store_grade_group_id ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE;
   L_store_grade          ORDLOC_WKSHT.STORE_GRADE%TYPE;
   L_loc_type             ORDLOC_WKSHT.LOC_TYPE%TYPE;
   L_location             ORDLOC_WKSHT.LOCATION%TYPE;
   L_calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE;
   L_statement            VARCHAR2(9000);
   L_standard_uom         ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_variance_qty         ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_act_qty              ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_uop                  ORDLOC_WKSHT.UOP%TYPE;
   L_supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple             ORDSKU.SUPP_PACK_SIZE%TYPE;
BEGIN
   L_statement := 'select o2.item_parent, o2.item, o2.diff_1, o2.diff_2, o2.diff_3, o2.diff_4, o2.store_grade_group_id, '||
                          'o2.store_grade, o2.loc_type, o2.location, o2.standard_uom, o2.wksht_qty, o2.uop, o2.supp_pack_size, ';
   if I_type = 'D' then
      L_statement := L_statement || 'o2.calc_qty + nvl(((ordloc_wksht.act_qty*location_dist_temp.dist_pct)/100),0) ';
   elsif I_type = 'R' then
      L_statement := L_statement ||'o2.calc_qty + nvl(((ordloc_wksht.act_qty/:I_sum_ratio)*location_dist_temp.dist_ratio),0) ';
   elsif I_type = 'Q' then
      L_statement := L_statement || 'o2.calc_qty + nvl(location_dist_temp.dist_qty, 0) * decode(:I_uop_type,''C'', o2.supp_pack_size,1) ';
   end if;
   ---
   L_statement := L_statement ||
        'from ordloc_wksht, location_dist_temp, ordloc_wksht o2 ' ||
       'where ordloc_wksht.order_no = :I_order_no ' ||
              I_where_clause || ' ' ||
         'and o2.order_no = ordloc_wksht.order_no ' ||
         'and nvl(o2.item,-1) = nvl(ordloc_wksht.item,-1) '||
         'and nvl(o2.item_parent,-1) = nvl(ordloc_wksht.item_parent,-1) '||
         'and nvl(o2.diff_1,-1) = nvl(ordloc_wksht.diff_1,-1) ' ||
         'and nvl(o2.diff_2,-1) = nvl(ordloc_wksht.diff_2,-1) ' ||
         'and nvl(o2.diff_3,-1) = nvl(ordloc_wksht.diff_3,-1) ' ||
         'and nvl(o2.diff_4,-1) = nvl(ordloc_wksht.diff_4,-1) ' ||
         'and nvl(o2.store_grade_group_id,-1) = nvl(ordloc_wksht.store_grade_group_id,-1) ' ||
         'and nvl(o2.store_grade,-1) = nvl(ordloc_wksht.store_grade,-1) ' ||
         'and o2.loc_type = location_dist_temp.loc_type ' ||
         'and o2.location = location_dist_temp.location';
   ---
   if I_type = 'D' then
      EXECUTE IMMEDIATE L_statement USING I_order_no;
      open C_UPDATE for L_statement USING I_order_no;
   elsif I_type = 'R' then
      EXECUTE IMMEDIATE L_statement USING I_sum_ratio, I_order_no;
      open C_UPDATE for L_statement USING I_sum_ratio, I_order_no;
   elsif I_type = 'Q' then
      EXECUTE IMMEDIATE L_statement USING I_uop_type, I_order_no;
      open C_UPDATE for L_statement USING I_uop_type, I_order_no;
   end if;
   LOOP
      fetch C_UPDATE into L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_diff_3,
                          L_diff_4,
                          L_store_grade_group_id,
                          L_store_grade,
                          L_loc_type,
                          L_location,
                          L_standard_uom,
                          L_wksht_qty,
                          L_uop,
                          L_supp_pack_size,
                          L_calc_qty;

      EXIT WHEN C_UPDATE%NOTFOUND;
      ---
      if L_uop = L_standard_uom then
         L_multiple := 1;
      else
         L_multiple := L_supp_pack_size;
      end if;
      ---
      if L_uop = L_standard_uom and
         L_standard_uom <> 'EA' then
         -- Round L_calc_qty to the fourth decimal
         L_wksht_qty := ROUND(L_calc_qty, 4);
         L_act_qty := L_wksht_qty;
         L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
      else -- UOP is case or standard UOM(eaches) - truncate these whole numbers
         -- Trunc L_calc_qty down to the next whole number
         L_wksht_qty := TRUNC(L_calc_qty/L_multiple);
         L_act_qty := (L_wksht_qty * L_multiple);
         L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
      end if;
      ---
      update ordloc_wksht
         set act_qty = L_act_qty,
             calc_qty = L_calc_qty,
             variance_qty = L_variance_qty,
             wksht_qty = L_wksht_qty
       where order_no = I_order_no
         and nvl(item_parent,-1) = nvl(L_item_parent,-1)
         and nvl(item,-1) = nvl(L_item,-1)
         and nvl(diff_1,-1) = nvl(L_diff_1,-1)
         and nvl(diff_2,-1) = nvl(L_diff_2,-1)
         and nvl(diff_3,-1) = nvl(L_diff_3,-1)
         and nvl(diff_4,-1) = nvl(L_diff_4,-1)
         and nvl(store_grade, -1) = nvl(L_store_grade, -1)
         and nvl(store_grade_group_id, -1) = nvl(L_store_grade_group_id, -1)
         and loc_type = L_loc_type
         and location = L_location;
   END LOOP;
   close C_UPDATE;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'UPDATE_OBJECT_SQL.UPDATE_ORDER_LOC', NULL);
      return FALSE;
END UPDATE_ORDER_LOC;
----------------------------------------------------------------------------
FUNCTION UPDATE_ORDER_DIFF(O_error_message   IN OUT VARCHAR2,
                           I_order_no        IN     ORDHEAD.ORDER_NO%TYPE,
                           I_type            IN     VARCHAR2,
                           I_sum_ratio       IN     NUMBER,
                           I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                           I_uop_type        IN     VARCHAR2,
                           I_diff_1_or_2     IN     NUMBER) RETURN BOOLEAN IS
   TYPE ORD_CURSOR is REF CURSOR;
   C_UPDATE               ORD_CURSOR;
   L_item_parent          ORDLOC_WKSHT.ITEM_PARENT%TYPE;
   L_item                 ORDLOC_WKSHT.ITEM%TYPE;
   L_diff_1               ORDLOC_WKSHT.DIFF_1%TYPE;
   L_diff_2               ORDLOC_WKSHT.DIFF_2%TYPE;
   L_store_grade_group_id ORDLOC_WKSHT.STORE_GRADE_GROUP_ID%TYPE;
   L_store_grade          ORDLOC_WKSHT.STORE_GRADE%TYPE;
   L_loc_type             ORDLOC_WKSHT.LOC_TYPE%TYPE;
   L_location             ORDLOC_WKSHT.LOCATION%TYPE;
   L_calc_qty             ORDLOC_WKSHT.CALC_QTY%TYPE;
   L_statement            VARCHAR2(9000);
   L_standard_uom         ORDLOC_WKSHT.STANDARD_UOM%TYPE;
   L_wksht_qty            ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_variance_qty         ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_act_qty              ORDLOC_WKSHT.WKSHT_QTY%TYPE;
   L_uop                  ORDLOC_WKSHT.UOP%TYPE;
   L_supp_pack_size       ORDLOC_WKSHT.SUPP_PACK_SIZE%TYPE;
   L_multiple             ORDSKU.SUPP_PACK_SIZE%TYPE;
BEGIN

   L_statement := 'select o2.item_parent, o2.item, o2.diff_1, o2.diff_2, o2.store_grade_group_id, '||
                  'o2.store_grade, o2.loc_type, o2.location, o2.standard_uom, o2.wksht_qty, o2.uop, o2.supp_pack_size, ';

   if I_type = 'P' then
      L_statement := L_statement ||'nvl(o2.calc_qty, o2.act_qty) + nvl(((ordloc_wksht.act_qty*dat.pct)/100),0) ';
   elsif I_type = 'R' and NVL(I_sum_ratio,0) <> 0 then
      L_statement := L_statement ||'nvl(o2.calc_qty, o2.act_qty) + nvl(((ordloc_wksht.act_qty/:I_sum_ratio)*dat.ratio),0) ';
   elsif I_type = 'R' and NVL(I_sum_ratio,0) = 0 then
      L_statement := L_statement ||'nvl(o2.calc_qty, o2.act_qty) + :I_sum_ratio ';
   elsif I_type = 'Q' then
      L_statement := L_statement ||'nvl(o2.calc_qty, o2.act_qty) + nvl(dat.qty,0) * decode(:I_uop_type,''C'', o2.supp_pack_size,1) ';
   end if;
   ---
   L_statement := L_statement ||
        'from ordloc_wksht, diff_apply_temp dat, ordloc_wksht o2 ' ||
       'where ordloc_wksht.order_no = :I_order_no '  ||
              I_where_clause || ' ' ||
         'and o2.order_no = ordloc_wksht.order_no ' ||
         'and o2.item_parent = ordloc_wksht.item_parent ' ||
         'and nvl(o2.store_grade_group_id,-1) = nvl(ordloc_wksht.store_grade_group_id,-1) ' ||
         'and nvl(o2.store_grade,-1) = nvl(ordloc_wksht.store_grade,-1) ' ||
         'and nvl(o2.loc_type,-1) = nvl(ordloc_wksht.loc_type,-1) ' ||
         'and nvl(o2.location,-1) = nvl(ordloc_wksht.location,-1) ' ||
         'and dat.status = ''A'' ';
   if I_diff_1_or_2 = 1 then
      L_statement := L_statement ||
                      'and o2.diff_1 = dat.diff_id ' ||
                      'and nvl(o2.diff_2,-1) = nvl(ordloc_wksht.diff_2,-1)';
   else
      L_statement := L_statement ||
                      'and o2.diff_2 = dat.diff_id ' ||
                      'and nvl(o2.diff_1,-1) = nvl(ordloc_wksht.diff_1,-1)';
   end if;
   ---
   if I_type = 'P' then
      EXECUTE IMMEDIATE L_statement USING I_order_no;
      open C_UPDATE for L_statement USING I_order_no;
   elsif I_type = 'R' then
      EXECUTE IMMEDIATE L_statement USING I_sum_ratio, I_order_no;
      open C_UPDATE for L_statement USING I_sum_ratio, I_order_no;
   elsif I_type = 'Q' then
      EXECUTE IMMEDIATE L_statement USING I_uop_type, I_order_no;
      open C_UPDATE for L_statement USING I_uop_type, I_order_no;
   end if;
   LOOP
      fetch C_UPDATE into L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_store_grade_group_id,
                          L_store_grade,
                          L_loc_type,
                          L_location,
                          L_standard_uom,
                          L_wksht_qty,
                          L_uop,
                          L_supp_pack_size,
                          L_calc_qty;

      EXIT WHEN C_UPDATE%NOTFOUND;

      if L_uop = L_standard_uom then
         L_multiple := 1;
      else
         L_multiple := L_supp_pack_size;
      end if;
      ---
      if L_uop = L_standard_uom and
         L_standard_uom <> 'EA' then
         -- Round L_calc_qty to the fourth decimal
         L_wksht_qty := ROUND(L_calc_qty, 4);
         L_act_qty := L_wksht_qty;
         if NVL(L_calc_qty, 0) = 0 then
            L_variance_qty := 0;   
         else
            L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
         end if;
      else -- UOP is case or standard UOM(eaches) - truncate these whole numbers
         -- Trunc L_calc_qty down to the next whole number
         L_wksht_qty := TRUNC(L_calc_qty/L_multiple);
         L_act_qty := (L_wksht_qty * L_multiple);
         if NVL(L_calc_qty, 0) = 0 then
            L_variance_qty := 0;   
         else
            L_variance_qty := ((L_act_qty - L_calc_qty)/L_calc_qty) * 100;
         end if;
      end if;
      ---
      update ordloc_wksht
         set act_qty = L_act_qty,
             calc_qty = L_calc_qty,
             variance_qty = L_variance_qty,
             wksht_qty = L_wksht_qty
       where order_no = I_order_no
         and nvl(item_parent,-1) = nvl(L_item_parent,-1)
         and nvl(store_grade, -1) = nvl(L_store_grade, -1)
         and nvl(store_grade_group_id, -1) = nvl(L_store_grade_group_id, -1)
         and nvl(loc_type, -1) = nvl(L_loc_type, -1)
         and nvl(location, -1) = nvl(L_location, -1)
         and ((I_diff_1_or_2 = 1
               and diff_1 = L_diff_1
               and nvl(diff_2, -1) = nvl(L_diff_2, -1))
             or (I_diff_1_or_2 = 2
               and diff_2 = L_diff_2
               and nvl(diff_1, -1) = nvl(L_diff_1, -1)));
   END LOOP;
   close C_UPDATE;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'UPDATE_OBJECT_SQL.UPDATE_ORDER_DIFF', NULL);
      return FALSE;
END UPDATE_ORDER_DIFF;
----------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_LOC (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                              I_type            IN     VARCHAR2,
                              I_sum_ratio       IN     NUMBER,
                              I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE) RETURN BOOLEAN IS
   TYPE CON_CURSOR is REF CURSOR;
   C_UPDATE               CON_CURSOR;
   L_item_grandparent     CONTRACT_MATRIX_TEMP.ITEM_GRANDPARENT%TYPE;
   L_item_parent          CONTRACT_MATRIX_TEMP.ITEM_PARENT%TYPE;
   L_item                 CONTRACT_MATRIX_TEMP.ITEM%TYPE;
   L_diff_1               CONTRACT_MATRIX_TEMP.DIFF_1%TYPE;
   L_diff_2               CONTRACT_MATRIX_TEMP.DIFF_2%TYPE;
   L_diff_3               CONTRACT_MATRIX_TEMP.DIFF_3%TYPE;
   L_diff_4               CONTRACT_MATRIX_TEMP.DIFF_4%TYPE;
   L_loc_type             CONTRACT_MATRIX_TEMP.LOC_TYPE%TYPE;
   L_location             CONTRACT_MATRIX_TEMP.LOCATION%TYPE;
   L_qty                  CONTRACT_MATRIX_TEMP.QTY%TYPE;
   L_ready_date           CONTRACT_MATRIX_TEMP.READY_DATE%TYPE;
   L_statement            VARCHAR2(9000);
BEGIN
   L_statement := 'select c2.item_grandparent, c2.item_parent, c2.item, c2.diff_1, c2.diff_2, c2.diff_3, c2.diff_4, '||
                         'c2.loc_type, c2.location, c2.ready_date,';
   ---
   if I_type = 'D' then
      L_statement := L_statement || 'nvl(c2.qty,0) + nvl(((contract_matrix_temp.qty*location_dist_temp.dist_pct)/100),0) ';
   elsif I_type = 'R' and NVL(I_sum_ratio,0) <> 0 then
      L_statement := L_statement ||'nvl(c2.qty,0) + nvl(((contract_matrix_temp.qty/:I_sum_ratio)*location_dist_temp.dist_ratio),0) ';
   elsif I_type = 'R' and NVL(I_sum_ratio,0) = 0 then
      L_statement := L_statement ||'nvl(c2.qty,0) + :I_sum_ratio ';
   elsif I_type = 'Q' then
      L_statement := L_statement || 'nvl(c2.qty,0) + nvl(location_dist_temp.dist_qty,0) ';
   end if;
   ---
   L_statement := L_statement ||
        'from contract_matrix_temp, location_dist_temp, contract_matrix_temp c2 ' ||
       'where contract_matrix_temp.contract_no = :I_contract_no ' ||
              I_where_clause || ' ' ||
         'and c2.contract_no = contract_matrix_temp.contract_no ' ||
         'and nvl(c2.item,-1) = nvl(contract_matrix_temp.item,-1) '||
         'and nvl(c2.item_parent,-1) = nvl(contract_matrix_temp.item_parent,-1) '||
         'and nvl(c2.item_grandparent,-1) = nvl(contract_matrix_temp.item_grandparent,-1) '||
         'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) ' ||
         'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) ' ||
         'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) ' ||
         'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1) ' ||
         'and (c2.ready_date = contract_matrix_temp.ready_date or '||
             '(c2.ready_date is NULL and contract_matrix_temp.ready_date is NULL)) '||
         'and c2.loc_type = location_dist_temp.loc_type ' ||
         'and c2.location = location_dist_temp.location';
   ---
   if I_type in ('D', 'Q') then
      EXECUTE IMMEDIATE L_statement USING I_contract_no;
      open C_UPDATE for L_statement USING I_contract_no;
   elsif I_type = 'R' then
      EXECUTE IMMEDIATE L_statement USING I_sum_ratio, I_contract_no;
      open C_UPDATE for L_statement USING I_sum_ratio, I_contract_no;
   end if;
   LOOP
      fetch C_UPDATE into L_item_grandparent,
                          L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_diff_3,
                          L_diff_4,
                          L_loc_type,
                          L_location,
                          L_ready_date,
                          L_qty;
      EXIT WHEN C_UPDATE%NOTFOUND;
      ---
      update contract_matrix_temp
         set qty =  TRUNC(L_qty)
       where contract_no = I_contract_no
         and nvl(item_parent,-1) = nvl(L_item_parent,-1)
         and nvl(item_grandparent,-1) = nvl(L_item_grandparent,-1)
         and nvl(item,-1) = nvl(L_item,-1)
         and nvl(diff_1,-1) = nvl(L_diff_1,-1)
         and nvl(diff_2,-1) = nvl(L_diff_2,-1)
         and nvl(diff_3,-1) = nvl(L_diff_3,-1)
         and nvl(diff_4,-1) = nvl(L_diff_4,-1)
         and (ready_date = L_ready_date or (ready_date is NULL and L_ready_date is NULL))
         and loc_type = L_loc_type
         and location = L_location;
      ---
   END LOOP;
   close C_UPDATE;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'UPDATE_OBJECT_SQL.UPDATE_CONTRACT_LOC', SQLCODE);
      return FALSE;
END UPDATE_CONTRACT_LOC;
----------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_DIFF (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                               I_type            IN     VARCHAR2,
                               I_sum_ratio       IN     NUMBER,
                               I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE,
                               I_diff_1234       IN     NUMBER) RETURN BOOLEAN IS

   TYPE CON_CURSOR is REF CURSOR;
   C_UPDATE               CON_CURSOR;
   L_item_grandparent     CONTRACT_MATRIX_TEMP.ITEM_GRANDPARENT%TYPE;
   L_item_parent          CONTRACT_MATRIX_TEMP.ITEM_PARENT%TYPE;
   L_item                 CONTRACT_MATRIX_TEMP.ITEM%TYPE;
   L_diff_1               CONTRACT_MATRIX_TEMP.DIFF_1%TYPE;
   L_diff_2               CONTRACT_MATRIX_TEMP.DIFF_2%TYPE;
   L_diff_3               CONTRACT_MATRIX_TEMP.DIFF_3%TYPE;
   L_diff_4               CONTRACT_MATRIX_TEMP.DIFF_4%TYPE;
   L_loc_type             CONTRACT_MATRIX_TEMP.LOC_TYPE%TYPE;
   L_location             CONTRACT_MATRIX_TEMP.LOCATION%TYPE;
   L_qty                  CONTRACT_MATRIX_TEMP.QTY%TYPE;
   L_ready_date           CONTRACT_MATRIX_TEMP.READY_DATE%TYPE;
   L_statement            VARCHAR2(9000);

BEGIN

   L_statement := 'select c2.item_grandparent, c2.item_parent, c2.item, c2.diff_1, c2.diff_2, c2.diff_3, c2.diff_4, '||
                          'c2.loc_type, c2.location, c2.ready_date,';
   ---
   if I_type = 'P' then
      L_statement := L_statement || 'nvl(c2.qty,0) + nvl(((contract_matrix_temp.qty*dat.pct)/100),0) ';
   elsif I_type = 'R' and NVL(I_sum_ratio,0) <> 0 then
      L_statement := L_statement ||'nvl(c2.qty,0) + nvl(((contract_matrix_temp.qty/:I_sum_ratio)*dat.ratio),0) ';
   elsif I_type = 'R' and NVL(I_sum_ratio,0) = 0 then
      L_statement := L_statement ||'nvl(c2.qty,0) + :I_sum_ratio ';
   elsif I_type = 'Q' then
      L_statement := L_statement || 'nvl(c2.qty,0) + nvl(dat.qty,0) ';
   else
      L_statement := L_statement || 'nvl(c2.qty,0) ';
   end if;
   ---
   L_statement := L_statement ||
        'from contract_matrix_temp, diff_apply_temp dat, contract_matrix_temp c2 ' ||
       'where contract_matrix_temp.contract_no = :I_contract_no ' ||
              I_where_clause || ' ' ||
         'and c2.contract_no = contract_matrix_temp.contract_no ' ||
         'and (c2.item_grandparent = contract_matrix_temp.item_grandparent or '||
              'c2.item_parent = contract_matrix_temp.item_parent) '||
         'and dat.status = ''A'' ' ||
         'and nvl(c2.loc_type,-1) = nvl(contract_matrix_temp.loc_type,-1) ' ||
         'and nvl(c2.location,-1) = nvl(contract_matrix_temp.location,-1) ' ||
         'and (c2.ready_date = contract_matrix_temp.ready_date or '||
              '(c2.ready_date is NULL and contract_matrix_temp.ready_date is NULL)) ';
   ---
   if I_diff_1234 = 1 then
      L_statement := L_statement ||
                      'and c2.diff_1 = dat.diff_id ' ||
                      'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) '||
                      'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) '||
                      'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1)';
   elsif I_diff_1234 = 2 then
      L_statement := L_statement ||
                      'and c2.diff_2 = dat.diff_id ' ||
                      'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) '||
                      'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) '||
                      'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1)';
   elsif I_diff_1234 = 3 then
      L_statement := L_statement ||
                      'and c2.diff_3 = dat.diff_id ' ||
                      'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) '||
                      'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) '||
                      'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1)';
   elsif I_diff_1234 = 4 then
      L_statement := L_statement ||
                      'and c2.diff_4 = dat.diff_id ' ||
                      'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) '||
                      'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) '||
                      'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1)';
   end if;
   ---
   if I_type in ('P', 'Q') then
      EXECUTE IMMEDIATE L_statement USING I_contract_no;
      open C_UPDATE for L_statement USING I_contract_no;
   elsif I_type = 'R' then
      EXECUTE IMMEDIATE L_statement USING I_sum_ratio, I_contract_no;
      open C_UPDATE for L_statement USING I_sum_ratio, I_contract_no;
   end if;
   LOOP
      fetch C_UPDATE into L_item_grandparent,
                          L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_diff_3,
                          L_diff_4,
                          L_loc_type,
                          L_location,
                          L_ready_date,
                          L_qty;
      EXIT WHEN C_UPDATE%NOTFOUND;
      ---
      update contract_matrix_temp
         set qty =  TRUNC(L_qty)
       where contract_no = I_contract_no
         and (item_grandparent = L_item_grandparent or
              item_parent = L_item_parent)
         and nvl(loc_type,-1) = nvl(L_loc_type,-1)
         and nvl(location,-1) = nvl(L_location,-1)
         and (ready_date = L_ready_date or (ready_date is NULL and L_ready_date is NULL))
         and ((I_diff_1234 = 1
               and diff_1 = L_diff_1
               and nvl(diff_2, -1) = nvl(L_diff_2, -1)
               and nvl(diff_3, -1) = nvl(L_diff_3, -1)
               and nvl(diff_4, -1) = nvl(L_diff_4, -1))
             or (I_diff_1234 = 2
               and diff_2 = L_diff_2
               and nvl(diff_1, -1) = nvl(L_diff_1, -1)
               and nvl(diff_3, -1) = nvl(L_diff_3, -1)
               and nvl(diff_4, -1) = nvl(L_diff_4, -1))
             or (I_diff_1234 = 3
               and diff_3 = L_diff_3
               and nvl(diff_1, -1) = nvl(L_diff_1, -1)
               and nvl(diff_2, -1) = nvl(L_diff_2, -1)
               and nvl(diff_4, -1) = nvl(L_diff_4, -1))
             or (I_diff_1234 = 4
               and diff_4 = L_diff_4
               and nvl(diff_1, -1) = nvl(L_diff_1, -1)
               and nvl(diff_2, -1) = nvl(L_diff_2, -1)
               and nvl(diff_3, -1) = nvl(L_diff_3, -1)));
   END LOOP;
   close C_UPDATE;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'UPDATE_OBJECT_SQL.UPDATE_CONTRACT_DIFF', NULL);
      return FALSE;
END UPDATE_CONTRACT_DIFF;
----------------------------------------------------------------------------
FUNCTION UPDATE_CONTRACT_DATE (O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               I_contract_no     IN     CONTRACT_HEADER.CONTRACT_NO%TYPE,
                               I_where_clause    IN     FILTER_TEMP.WHERE_CLAUSE%TYPE) RETURN BOOLEAN IS
   TYPE CON_CURSOR is REF CURSOR;
   C_UPDATE               CON_CURSOR;
   L_item_parent          CONTRACT_MATRIX_TEMP.ITEM_PARENT%TYPE;
   L_item_grandparent     CONTRACT_MATRIX_TEMP.ITEM_GRANDPARENT%TYPE;
   L_item                 CONTRACT_MATRIX_TEMP.ITEM%TYPE;
   L_diff_1               CONTRACT_MATRIX_TEMP.DIFF_1%TYPE;
   L_diff_2               CONTRACT_MATRIX_TEMP.DIFF_2%TYPE;
   L_diff_3               CONTRACT_MATRIX_TEMP.DIFF_3%TYPE;
   L_diff_4               CONTRACT_MATRIX_TEMP.DIFF_4%TYPE;
   L_loc_type             CONTRACT_MATRIX_TEMP.LOC_TYPE%TYPE;
   L_location             CONTRACT_MATRIX_TEMP.LOCATION%TYPE;
   L_qty                  CONTRACT_MATRIX_TEMP.QTY%TYPE;
   L_ready_date           CONTRACT_MATRIX_TEMP.READY_DATE%TYPE;
   L_statement            VARCHAR2(9000);
BEGIN

   L_statement := 'select c2.item_grandparent, c2.item_parent, c2.item, c2.diff_1, c2.diff_2, c2.diff_3, c2.diff_4, '||
                          'c2.loc_type, c2.location, c2.ready_date, '||
                          'c2.qty + nvl(((contract_matrix_temp.qty*date_dist_temp.dist_pct)/100),0) ';
   ---
   L_statement := L_statement ||
        'from contract_matrix_temp, date_dist_temp, contract_matrix_temp c2 ' ||
       'where contract_matrix_temp.contract_no = :I_contract_no ' ||
              I_where_clause || ' ' ||
         'and c2.contract_no = contract_matrix_temp.contract_no ' ||
         'and nvl(c2.item,-1) = nvl(contract_matrix_temp.item,-1) '||
         'and nvl(c2.item_parent,-1) = nvl(contract_matrix_temp.item_parent,-1) '||
         'and nvl(c2.item_grandparent,-1) = nvl(contract_matrix_temp.item_grandparent,-1) '||
         'and nvl(c2.diff_1,-1) = nvl(contract_matrix_temp.diff_1,-1) '||
         'and nvl(c2.diff_2,-1) = nvl(contract_matrix_temp.diff_2,-1) '||
         'and nvl(c2.diff_3,-1) = nvl(contract_matrix_temp.diff_3,-1) '||
         'and nvl(c2.diff_4,-1) = nvl(contract_matrix_temp.diff_4,-1) '||
         'and nvl(c2.loc_type,-1) = nvl(contract_matrix_temp.loc_type,-1) ' ||
         'and nvl(c2.location,-1) = nvl(contract_matrix_temp.location,-1) ' ||
         'and c2.ready_date = date_dist_temp.dist_date';
   ---
   EXECUTE IMMEDIATE L_statement USING I_contract_no;
   open C_UPDATE for L_statement USING I_contract_no;

   LOOP
      fetch C_UPDATE into L_item_grandparent,
                          L_item_parent,
                          L_item,
                          L_diff_1,
                          L_diff_2,
                          L_diff_3,
                          L_diff_4,
                          L_loc_type,
                          L_location,
                          L_ready_date,
                          L_qty;
      EXIT WHEN C_UPDATE%NOTFOUND;
      ---
      update contract_matrix_temp
         set qty =  TRUNC(L_qty)
       where contract_no = I_contract_no
         and nvl(item_grandparent,-1) = nvl(L_item_grandparent,-1)
         and nvl(item_parent,-1) = nvl(L_item_parent,-1)
         and nvl(item,-1) = nvl(L_item,-1)
         and nvl(diff_1,-1) = nvl(L_diff_1,-1)
         and nvl(diff_2,-1) = nvl(L_diff_2,-1)
         and nvl(diff_3,-1) = nvl(L_diff_3,-1)
         and nvl(diff_4,-1) = nvl(L_diff_4,-1)
         and nvl(loc_type,-1) = nvl(L_loc_type,-1)
         and nvl(location,-1) = nvl(L_location,-1)
         and ready_date = L_ready_date;
         ---
   END LOOP;
   close C_UPDATE;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'UPDATE_OBJECT_SQL.UPDATE_CONTRACT_DATE', NULL);
      return FALSE;
END UPDATE_CONTRACT_DATE;
--------------------------------------------------------------------------
END;
/
