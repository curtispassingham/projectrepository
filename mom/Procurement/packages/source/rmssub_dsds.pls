CREATE OR REPLACE PACKAGE RMSSUB_DSD AUTHID CURRENT_USER AS

DSD_CRE     CONSTANT   VARCHAR2(30)  := 'dsdreceiptcre';
DSD_MOD     CONSTANT   VARCHAR2(30)  := 'dsdreceiptmod';

--------------------------------------------------------------------------------
PROCEDURE CONSUME(O_status_code          IN OUT  VARCHAR2,
                  O_error_message        IN OUT  VARCHAR2,
                  I_message              IN      RIB_OBJECT,
                  I_message_type         IN      VARCHAR2,
                  O_rib_dsddeals_rec        OUT  RIB_OBJECT);
-------------------------------------------------------------------------------------------------------
END RMSSUB_DSD;
/
