CREATE OR REPLACE PACKAGE CORESVC_PO AUTHID CURRENT_USER AS
   ACTION_NEW         CONSTANT VARCHAR2(25)  :=  'NEW';
   ACTION_MOD         CONSTANT VARCHAR2(25)  :=  'MOD';
   ACTION_DEL         CONSTANT VARCHAR2(25)  :=  'DEL';
   P_WARNING          CONSTANT SVC_PROCESS_TRACKER.STATUS%TYPE := 'PW';
   P_ERROR            CONSTANT SVC_PROCESS_TRACKER.STATUS%TYPE := 'PE';
   P_SUCCESS          CONSTANT SVC_PROCESS_TRACKER.STATUS%TYPE := 'PS';
   ERROR              CONSTANT CORESVC_PO_ERR.ERROR_TYPE%TYPE := 'E';
   WARNING            CONSTANT CORESVC_PO_ERR.ERROR_TYPE%TYPE := 'W';
   PS_ERROR           CONSTANT VARCHAR2(1) := 'E';
   PS_PROCESSED       CONSTANT VARCHAR2(1) := 'P';
   PS_SPLIT           CONSTANT VARCHAR2(1) := 'S';
   PS_NEW             CONSTANT VARCHAR2(1) := 'N';
   CONSOLIDATION      CONSTANT SVC_ORDDETAIL.PROCESSING_TYPE%TYPE := 'C';
   CROSS_DOCK         CONSTANT SVC_ORDDETAIL.PROCESSING_TYPE%TYPE := 'X';
   
   Type OHE_rec_tab IS TABLE OF ORDHEAD%ROWTYPE;
   Type OSK_rec_tab IS TABLE OF ORDSKU%ROWTYPE;
   Type OLO_rec_tab IS TABLE OF ORDLOC%ROWTYPE;
   Type ORE_rec_tab IS TABLE OF ORDLOC_EXP%ROWTYPE;
   Type ORH_rec_tab IS TABLE OF ORDSKU_HTS%ROWTYPE;
   Type OHA_rec_tab IS TABLE OF ORDSKU_HTS_ASSESS%ROWTYPE;
   Type ORD_rec_tab IS TABLE OF ORDLC%ROWTYPE;
   Type ALH_rec_tab IS TABLE OF ALLOC_HEADER%ROWTYPE;
   Type ALD_rec_tab IS TABLE OF ALLOC_DETAIL%ROWTYPE;
   
-------------------------------------------------------------------------------------------------------
FUNCTION PROCESS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                 I_process_id      IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : COMPLETE_SVC_PO
-- Purpose       : Inserts a dummy svc_ordhead record in cases where the ordhead sheet does not contain
--                 a row for the order being processed in other PO induction sheets.
-------------------------------------------------------------------------------------------------------
FUNCTION COMPLETE_SVC_PO(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_process_id      IN       NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Procedure Name : PROCESS_PO_THREAD
-- Purpose        : This is a wrapper function that is called by the scheduler for each thread(chunk)
--                  that will be processed.
-------------------------------------------------------------------------------------------------------
PROCEDURE PROCESS_PO_THREAD(I_process_id   IN   SVC_PROCESS_TRACKER.PROCESS_ID%TYPE,
                            I_thread       IN   RESTART_PROGRAM_STATUS.THREAD_VAL%TYPE);
-------------------------------------------------------------------------------------------------------
-- Function Name : PURGE_SVC_PO_TABLES
-- Purpose       : This function selects and deletes records from the staging tables that satisfy 
--                 the following:
--                 1) Status of the process = 'P'rocessed Sucessfully or 'PW' processed with warnings
--                    where the destination is RMS or S9T
--                 2) Status of the process = ‘PE’ with no linked data
--                 3) Status for the process = ‘PE’ but all the order records associated with this 
--                    process ID have been processed successfully.
--                 4) Processes where action_date < (vdate – system_options.proc_data_retention_days)
--                 5) All orders within a process where all related records for the order in the other
--                    PO staging tables are successfully uploaded to RMS.  The process tracker record 
--                    should not be deleted if there are other orders that are not uploaded to RMS.
-------------------------------------------------------------------------------------------------------
FUNCTION PURGE_SVC_PO_TABLES(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : CUSTOM_APPR_VAL
-- Purpose       : Calls the custom package that will execute the client custom approval logic
-------------------------------------------------------------------------------------------------------
FUNCTION CUSTOM_APPR_VAL(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         O_ord_apprerr_exist IN OUT   BOOLEAN,
                         I_order_no          IN OUT   ORDHEAD.ORDER_NO%TYPE,
                         I_function_key      IN OUT   VARCHAR2,
                         I_seq_no            IN OUT   NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name : CHECK_USER_PO_PRIVS
-- Purpose       : This function is called to ensure that the order can be edited by the user.
-------------------------------------------------------------------------------------------------------
FUNCTION CHECK_USER_PO_PRIVS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                             O_edits_allowed   IN OUT   BOOLEAN,
                             I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                             I_called_from     IN       VARCHAR2 DEFAULT NULL,
                             I_action          IN       VARCHAR2 DEFAULT NULL)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
END CORESVC_PO;
/
