CREATE OR REPLACE PACKAGE BODY SVCPROV_STOREORDER AS
/******************************************************************************
 * PROCEDURE CREATE_LOC_PO_TSF 
 * It will create a PO or Transfer to RMS
 ******************************************************************************/
PROCEDURE CREATE_LOC_PO_TSF(O_serviceOperationStatus    IN OUT "RIB_ServiceOpStatus_REC",
                            O_businessObject            OUT    "RIB_LocPOTsfRef_REC",
                            I_businessObject            IN     "RIB_LocPOTsfDesc_REC") 
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.CREATE_LOC_PO_TSF';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN

   --check input object contains data 
   if I_businessObject is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.CREATE_MOD_LOCPO(L_error_message,
                                          O_businessObject,                                          
                                          I_businessObject,
                                          CORESVC_STOREORDER.LOCPOTSF_CRE 
                                          )= FALSE then
      raise PROGRAM_ERROR;
   end if;
   
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END CREATE_LOC_PO_TSF;

/******************************************************************************
 * PROCEDURE CREATE_LOC_PO_TSF_DETAIL
 * It will create a PO or Transfer Details to RMS                 
 ******************************************************************************/
PROCEDURE CREATE_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                  I_businessObject          IN     "RIB_LocPOTsfDesc_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.CREATE_LOC_PO_TSF_DETAIL';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   L_businessObject  "RIB_LocPOTsfRef_REC" := NULL;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL or
      I_businessObject.LocPOTsfDtl_TBL is NULL or 
      I_businessObject.LocPOTsfDtl_TBL.count <= 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.CREATE_MOD_LOCPO(L_error_message,
                                          L_businessObject,                                          
                                          I_businessObject,
                                          CORESVC_STOREORDER.LOCPOTSF_DTL_CRE                                          
                                          ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END CREATE_LOC_PO_TSF_DETAIL;

/******************************************************************************
 * PROCEDURE MOD_LOC_PO_TSF
 * It will modify a PO or Transfer to RMS                   
 ******************************************************************************/
PROCEDURE MOD_LOC_PO_TSF(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                         I_businessObject         IN     "RIB_LocPOTsfDesc_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.MOD_LOC_PO_TSF';
   L_businessObject  "RIB_LocPOTsfRef_REC" := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.CREATE_MOD_LOCPO(L_error_message,
                                          L_businessObject,                                          
                                          I_businessObject,
                                          CORESVC_STOREORDER.LOCPOTSF_MOD                                          
                                          ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END MOD_LOC_PO_TSF;

/******************************************************************************
 * PROCEDURE MOD_LOC_PO_TSF_DETAIL
 * It will modify a PO or Transfer Details to RMS                  
 ******************************************************************************/
PROCEDURE MOD_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                I_businessObject         IN     "RIB_LocPOTsfDesc_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.MOD_LOC_PO_TSF_DETAIL';
   L_businessObject  "RIB_LocPOTsfRef_REC" := NULL;
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL or
      I_businessObject.LocPOTsfDtl_TBL is NULL or 
      I_businessObject.LocPOTsfDtl_TBL.count <= 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.CREATE_MOD_LOCPO(L_error_message,
                                          L_businessObject,
                                          I_businessObject,
                                          CORESVC_STOREORDER.LOCPOTSF_DTL_MOD                                          
                                          ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END MOD_LOC_PO_TSF_DETAIL;

/******************************************************************************
 * PROCEDURE DELETE_LOC_PO_TSF
 * It will Delete a PO or Transfer in RMS.                
 ******************************************************************************/
PROCEDURE DELETE_LOC_PO_TSF(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                            I_businessObject         IN     "RIB_LocPOTsfDesc_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.DELETE_LOC_PO_TSF';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.DEL_LOCPO(L_error_message,
                                   I_businessObject,
                                   CORESVC_STOREORDER.LOCPOTSF_DEL                                          
                                   ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END DELETE_LOC_PO_TSF;

/******************************************************************************
 * PROCEDURE DELETE_LOC_PO_TSF_DETAIL
 * It will Delete a PO or Transfer Details to RMS.                   
 ******************************************************************************/
PROCEDURE DELETE_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                   I_businessObject         IN     "RIB_LocPOTsfDesc_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.DELETE_LOC_PO_TSF_DETAIL';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL or
      I_businessObject.LocPOTsfDtl_TBL is NULL or 
      I_businessObject.LocPOTsfDtl_TBL.count <= 0 then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.DEL_LOCPO(L_error_message,
                                   I_businessObject,
                                   CORESVC_STOREORDER.LOCPOTSF_DTL_DEL                                          
                                   ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END DELETE_LOC_PO_TSF_DETAIL;

/******************************************************************************

 * PROCEDURE QUERY_LOC_PO_TSF_DEALS
 * It will retrieve Deals Information from RMS     
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_DEALS(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                 O_businessObject         OUT    "RIB_LocPOTsfDealsColDesc_REC",
                                 I_businessObject         IN     "RIB_LocPOTsfDealsCriVo_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_DEALS';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.GET_DEALS(L_error_message,
                                   O_businessObject,
                                   I_businessObject
                                   ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END QUERY_LOC_PO_TSF_DEALS;

/******************************************************************************
 * PROCEDURE QUERY_LOC_PO_TSF_ITEMSALES
 * It will retrieve the Item Sales information from RMS.   
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_ITEMSALES(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                     O_businessObject         OUT    "RIB_LocPOTsfItmSlsColDesc_REC",
                                     I_businessObject         IN     "RIB_LocPOTsfItmSlsCriVo_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_ITEMSALES';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL then 
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.GET_ITEMS_SALES(L_error_message,
                                         O_businessObject,
                                         I_businessObject
                                         ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END QUERY_LOC_PO_TSF_ITEMSALES;

/******************************************************************************
 * PROCEDURE QUERY_LOC_PO_TSF_HEADER
 * It will Retrieve header level PO or transfer information from RMS.   
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_HEADER(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                  O_businessObject         OUT    "RIB_LocPOTsfHdrColDesc_REC",
                                  I_businessObject         IN     "RIB_LocPOTsfHdrCriVo_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_HEADER';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.GET_STORE_ORDERS(L_error_message,
                                          O_businessObject,
                                          I_businessObject
                                          ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END QUERY_LOC_PO_TSF_HEADER;

/******************************************************************************
 * PROCEDURE QUERY_LOC_PO_TSF_DETAIL
 * It will retrieve the header/details of a PO or transfer from RMS.
 ******************************************************************************/
PROCEDURE QUERY_LOC_PO_TSF_DETAIL(O_serviceOperationStatus IN OUT "RIB_ServiceOpStatus_REC",
                                  O_businessObject         OUT    "RIB_LocPOTsfDesc_REC",
                                  I_businessObject         IN     "RIB_LocPOTsfDtlsCriVo_REC")
    IS
   L_program         VARCHAR2(60) := 'SVCPROV_STOREORDER.QUERY_LOC_PO_TSF_DETAIL';
   L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
   PROGRAM_ERROR     EXCEPTION;
BEGIN
   --check input object contains data
   if I_businessObject is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_businessObject',
                                            L_program,
                                            NULL);
      raise PROGRAM_ERROR;
   end if;
   -- call core logic
   if CORESVC_STOREORDER.GET_STORE_ORDER_DETAILS(L_error_message,
                                                 O_businessObject,                                                 
                                                 I_businessObject
                                                 ) = FALSE then
      raise PROGRAM_ERROR;
   end if;
EXCEPTION
   when PROGRAM_ERROR then
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      SVCPROV_UTILITY.BUILD_SERVICE_OP_STATUS(O_serviceOperationStatus,
                                              API_CODES.UNHANDLED_ERROR,
                                              L_error_message,
                                              L_program);
END QUERY_LOC_PO_TSF_DETAIL;

END SVCPROV_STOREORDER;
/