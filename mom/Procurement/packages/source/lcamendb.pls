
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE BODY LC_AMEND_SQL AS
--------------------------------------------------------------------------------------
FUNCTION NET_EFFECT(O_error_message  IN OUT  VARCHAR2,
                    O_net_effect     IN OUT  NUMBER,
                    I_amend_no       IN      LC_AMENDMENTS.AMEND_NO%TYPE,
                    I_lc_ref_id      IN      LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program  VARCHAR2(50) := 'LC_AMEND_SQL.NET_EFFECT';

   cursor C_NET_EFFECT is
      select SUM(effect)
        from lc_amendments
       where lc_ref_id = I_lc_ref_id
         and amend_no  = I_amend_no;

BEGIN
   --- the lc ref id and amendment number are required to sum the effect
   if I_lc_ref_id is NULL or
      I_amend_no  is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN', 'C_NET_EFFECT', 'LC_AMENDMENTS',
                    'amend no: ' || to_char(I_amend_no) ||
                    ', lc ref id: ' || to_char(I_lc_ref_id));
   open C_NET_EFFECT;

   SQL_LIB.SET_MARK('FETCH', 'C_NET_EFFECT', 'LC_AMENDMENTS',
                    'amend no: ' || to_char(I_amend_no) ||
                    ', lc ref id: ' || to_char(I_lc_ref_id));
   fetch C_NET_EFFECT into O_net_effect;

   SQL_LIB.SET_MARK('CLOSE', 'C_NET_EFFECT', 'LC_AMENDMENTS',
                    'amend no: ' || to_char(I_amend_no) ||
                    ', lc ref id: ' || to_char(I_lc_ref_id));
   close C_NET_EFFECT;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END NET_EFFECT;

--------------------------------------------------------------------------------------
FUNCTION GENERATE(O_error_message   IN OUT   VARCHAR2,
                  I_lc_ref_id       IN       LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(50)                      := 'LC_AMEND_SQL.GENERATE';
   L_amended_field LC_AMENDMENTS.AMENDED_FIELD%TYPE  := NULL;
   L_order_no      LC_AMENDMENTS.ORDER_NO%TYPE       := NULL;
   L_new_value     LC_AMENDMENTS.NEW_VALUE%TYPE      := NULL;
   L_orig_value    LC_AMENDMENTS.ORIGINAL_VALUE%TYPE := NULL;
   L_next_amend    LC_AMENDMENTS.AMEND_NO%TYPE       := NULL;
   L_issuing_bank  LC_HEAD.ISSUING_BANK%TYPE         := NULL;
   L_net_effect    NUMBER                            := NULL;
   L_vdate         PERIOD.VDATE%TYPE                 := GET_VDATE;

   cursor C_NEXT_AMEND is
      select nvl(MAX(amend_no),0) + 1
        from lc_amendments
       where lc_ref_id = I_lc_ref_id;

   cursor C_ISSUING_BANK is
      select issuing_bank
        from lc_head
       where lc_ref_id = I_lc_ref_id;

   cursor C_CHECK_RO_ARQD is
      select amended_field,
             order_no,
             new_value,
             original_value
        from lc_amendments
       where lc_ref_id = I_lc_ref_id
         and status    = 'A'
         and amend_no  is NULL
         and amended_field in ('RO','ARQD','RRQD');
         
   cursor C_LOCK_AMENDMENTS is
      select 'x'
        from lc_amendments
       where lc_ref_id   = I_lc_ref_id
         and status      = 'A'
         and amend_no    is NULL
         for update nowait;

BEGIN
   --- get next amendment number
   --- if this number is greater than 99
   --- then return false. as per SIR #26691

   SQL_LIB.SET_MARK('OPEN', 'C_NEXT_AMEND', 'LC_AMENDMENTS',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   open C_NEXT_AMEND;
   SQL_LIB.SET_MARK('FETCH', 'C_NEXT_AMEND', 'LC_AMENDMENTS',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   fetch C_NEXT_AMEND into L_next_amend;

   SQL_LIB.SET_MARK('CLOSE', 'C_NEXT_AMEND', 'LC_AMENDMENTS',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   close C_NEXT_AMEND;
   if L_next_amend > 99 then
      O_error_message := SQL_LIB.CREATE_MSG('MAX_AMEND_EXCEEDED', NULL, NULL, NULL);
      return FALSE;
   end if;

   --- when accepting an amendment with an amended field of
   --- 'RO' (remove order) or 'ARQD' (add required document),
   --- need to loop through all records to currently update
   --- the corresponding tables.

   SQL_LIB.SET_MARK('OPEN','C_CHECK_RO_ARQD','LC_AMENDMENTS',
                    'lc ref id: ' || to_char(I_lc_ref_id));
   for C_CHECK_RO_ARQD_REC in C_CHECK_RO_ARQD loop
      L_amended_field := C_CHECK_RO_ARQD_REC.amended_field;
      L_order_no      := C_CHECK_RO_ARQD_REC.order_no;
      L_new_value     := C_CHECK_RO_ARQD_REC.new_value;
      L_orig_value    := C_CHECK_RO_ARQD_REC.original_value;
      
      --- need to add the specific required document
      if L_amended_field = 'ARQD' then
         if DOCUMENTS_SQL.INSERT_REQ_DOC(O_error_message,
                                         'LCA',
                                         to_char(I_lc_ref_id),
                                         NULL,
                                         L_new_value,
                                         NULL) = FALSE then
            return FALSE;
         end if;
      end if;

      --- need to update ordering tables for the specific order
      if L_amended_field = 'RO' then
         if LC_SQL.WRITE_LCORDAPP(O_error_message,
                                  TRUE,
                                  L_order_no) = FALSE then
            return FALSE;
         end if;
      end if;
      
      --- need to delete the specific required document if its module is 'LCA'
      if L_amended_field = 'RRQD' then
         if DOCUMENTS_SQL.LOCK_REQ_DOCS(O_error_message,
                                        'LCA',
                                        to_char(I_lc_ref_id),
                                        NULL,
                                        L_orig_value)  = FALSE then
            return FALSE;
         end if;
         
         if DOCUMENTS_SQL.DELETE_REQ_DOCS(O_error_message,
                                          'LCA',
                                          to_char(I_lc_ref_id),
                                          NULL,
                                          L_orig_value)  = FALSE then
            return FALSE;
         end if;
      end if;       
      
   end loop;
   
   open C_LOCK_AMENDMENTS;
   close C_LOCK_AMENDMENTS;

   update lc_amendments
      set amend_no    = L_next_amend,
          accept_date = L_vdate
    where lc_ref_id   = I_lc_ref_id
      and status      = 'A'
      and amend_no    is NULL;

   if LC_AMEND_SQL.NET_EFFECT(O_error_message,
                              L_net_effect,
                              L_next_amend,
                              I_lc_ref_id) = FALSE then
      return FALSE;
   end if;

   if L_net_effect is not NULL then
      if LC_ACTIVE_SQL.ACTIVITY(O_error_message,
                                I_lc_ref_id,
                                L_net_effect,
                                L_next_amend) = FALSE then
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN', 'C_ISSUING_BANK', 'LC_HEAD',
                       'lc ref id: ' || to_char(I_lc_ref_id));
      open C_ISSUING_BANK;

      SQL_LIB.SET_MARK('FETCH', 'C_ISSUING_BANK', 'LC_HEAD',
                       'lc ref id: ' || to_char(I_lc_ref_id));
      fetch C_ISSUING_BANK into L_issuing_bank;

      SQL_LIB.SET_MARK('CLOSE', 'C_ISSUING_BANK', 'LC_HEAD',
                       'lc ref id: ' || to_char(I_lc_ref_id));
      close C_ISSUING_BANK;

      if LC_AMEND_SQL.UPDATE_BANK(O_error_message,
                                  'A',
                                  L_net_effect,
                                  L_issuing_bank) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GENERATE;

--------------------------------------------------------------------------------------
FUNCTION GET_FIXED_FORMAT_VALUES(O_error_message IN OUT VARCHAR2,
                                 O_early_ship    IN OUT LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                                 O_late_ship     IN OUT LC_HEAD.LATEST_SHIP_DATE%TYPE,
                                 O_exp_date      IN OUT LC_HEAD.EXPIRATION_DATE%TYPE,
                                 O_place_of_exp  IN OUT LC_HEAD.PLACE_OF_EXPIRY%TYPE,
                                 O_net_amount    IN OUT LC_HEAD.AMOUNT%TYPE,
                                 O_neg_days      IN OUT LC_HEAD.LC_NEG_DAYS%TYPE,
                                 O_present_term  IN OUT LC_HEAD.PRESENTATION_TERMS%TYPE,
                                 O_transship     IN OUT LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                                 O_transfer      IN OUT LC_HEAD.TRANSFERABLE_IND%TYPE,
                                 O_part_ship     IN OUT LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                                 I_lc_ref_id     IN     LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(50) := 'LC_AMEND_SQL.GET_FIXED_FORMAT_VALUES';
   L_place_of_exp  LC_HEAD.PLACE_OF_EXPIRY%TYPE;
   L_net_amount    LC_HEAD.AMOUNT%TYPE;
   L_neg_days      LC_HEAD.LC_NEG_DAYS%TYPE;
   L_present_term  LC_HEAD.PRESENTATION_TERMS%TYPE;
   L_transfer      LC_HEAD.TRANSFERABLE_IND%TYPE;
   L_country       LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;

   cursor C_GET_PE_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'PE'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'PE');

   cursor C_GET_NA_AMEND is
      select to_number(new_value)
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'NA'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'NA');

   cursor C_GET_ND_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'ND'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'ND');

   cursor C_GET_PRT_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'PRT'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'PRT');

   cursor C_GET_TFF_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'TFF'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'TFF');

   --- cursor to select the values off the lc_head table
   cursor C_GET_LC_HEAD_INFO is
      select place_of_expiry,
             amount,
             lc_neg_days,
             presentation_terms,
             transferable_ind
        from lc_head
       where lc_ref_id = I_lc_ref_id;

BEGIN
   --- select the newest value (greatest amendment number)
   --- off the lc_amendments table.  If can't find an amendment
   --- then take the value of the lc_head table (original value).

   --- get values off the lc_head table
   open C_GET_LC_HEAD_INFO;
   fetch C_GET_LC_HEAD_INFO into L_place_of_exp,
                                 L_net_amount,
                                 L_neg_days,
                                 L_present_term,
                                 L_transfer;
   close C_GET_LC_HEAD_INFO;

   if LC_AMEND_SQL.GET_ORIGINAL_VALUES(O_error_message,
                                       O_early_ship,
                                       O_late_ship,
                                       O_exp_date,
                                       L_country,
                                       O_transship,
                                       O_part_ship,
                                       I_lc_ref_id) = FALSE then
      return FALSE;
   end if;

   --- place of expiry
   open C_GET_PE_AMEND;
   fetch C_GET_PE_AMEND into O_place_of_exp;

   if C_GET_PE_AMEND%NOTFOUND then
      O_place_of_exp := L_place_of_exp;
   end if;

   close C_GET_PE_AMEND;

   --- amount
   open C_GET_NA_AMEND;
   fetch C_GET_NA_AMEND into O_net_amount;

   if C_GET_NA_AMEND%NOTFOUND then
      O_net_amount := L_net_amount;
   end if;

   close C_GET_NA_AMEND;

   --- negotiation days
   open C_GET_ND_AMEND;
   fetch C_GET_ND_AMEND into O_neg_days;

   if C_GET_ND_AMEND%NOTFOUND then
      O_neg_days := L_neg_days;
   end if;

   close C_GET_ND_AMEND;

   --- presentation terms
   open C_GET_PRT_AMEND;
   fetch C_GET_PRT_AMEND into O_present_term;

   if C_GET_PRT_AMEND%NOTFOUND then
      O_present_term := L_present_term;
   end if;

   close C_GET_PRT_AMEND;

   --- transferable indicator
   open C_GET_TFF_AMEND;
   fetch C_GET_TFF_AMEND into O_transfer;

   if C_GET_TFF_AMEND%NOTFOUND then
      O_transfer := L_transfer;
   end if;

   close C_GET_TFF_AMEND;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_FIXED_FORMAT_VALUES;

--------------------------------------------------------------------------------------
FUNCTION GET_ORIGINAL_VALUES(O_error_message  IN OUT VARCHAR2,
                             O_early_ship     IN OUT LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                             O_late_ship      IN OUT LC_HEAD.LATEST_SHIP_DATE%TYPE,
                             O_exp_date       IN OUT LC_HEAD.EXPIRATION_DATE%TYPE,
                             O_country        IN OUT LC_HEAD.ORIGIN_COUNTRY_ID%TYPE,
                             O_transship      IN OUT LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                             O_part_ship      IN OUT LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                             I_lc_ref_id      IN     LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program       VARCHAR2(50) := 'LC_AMEND_SQL.GET_ORIGINAL_VALUES';
   L_early_ship    LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_late_ship     LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_exp_date      LC_HEAD.EXPIRATION_DATE%TYPE;
   L_country       LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_transship     LC_HEAD.TRANSSHIPMENT_IND%TYPE;
   L_part_ship     LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE;
   L_date_temp     VARCHAR(12);

   --- cursors to select the value of the greatest amendment number
   --- off of the lc_amendments table
   cursor C_GET_ESD_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'ESD'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'ESD');

   cursor C_GET_LSD_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'LSD'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'LSD');

   cursor C_GET_ED_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'ED'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'ED');

   cursor C_GET_OC_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'OC'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'OC');

   cursor C_GET_TSF_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'TSF'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'TSF');

   cursor C_GET_PSF_AMEND is
      select new_value
        from lc_amendments
       where lc_ref_id     = I_lc_ref_id
         and amended_field = 'PSF'
         and amend_no      = (select MAX(amend_no)
                                from lc_amendments
                               where lc_ref_id     = I_lc_ref_id
                                 and amended_field = 'PSF');

   --- cursor to select the values off the lc_head table
   cursor C_GET_LC_HEAD_INFO is
      select earliest_ship_date,
             latest_ship_date,
             expiration_date,
             origin_country_id,
             transshipment_ind,
             partial_shipment_ind
        from lc_head
       where lc_ref_id = I_lc_ref_id;

BEGIN
   --- select the newest value (greatest amendment number)
   --- off the lc_amendments table.  If can't find an amendment
   --- then take the value of the lc_head table (original value).

   --- get values off the lc_head table
   open C_GET_LC_HEAD_INFO;
   fetch C_GET_LC_HEAD_INFO into L_early_ship,
                                 L_late_ship,
                                 L_exp_date,
                                 L_country,
                                 L_transship,
                                 L_part_ship;
   close C_GET_LC_HEAD_INFO;


   --- earliest ship date
   open C_GET_ESD_AMEND;
   fetch C_GET_ESD_AMEND into L_date_temp;

   if C_GET_ESD_AMEND%NOTFOUND then
      O_early_ship := L_early_ship;
   else
      O_early_ship := TO_DATE(L_date_temp,'DD-MON-RR');
   end if;

   close C_GET_ESD_AMEND;

   --- latest ship date
   open C_GET_LSD_AMEND;
   fetch C_GET_LSD_AMEND into L_date_temp;

   if C_GET_LSD_AMEND%NOTFOUND then
      O_late_ship := L_late_ship;
   else
      O_late_ship := TO_DATE(L_date_temp,'DD-MON-RR');
   end if;

   close C_GET_LSD_AMEND;

   --- expiration date
   open C_GET_ED_AMEND;
   fetch C_GET_ED_AMEND into L_date_temp;

   if C_GET_ED_AMEND%NOTFOUND then
      O_exp_date := L_exp_date;
   else
      O_exp_date := TO_DATE(L_date_temp,'DD-MON-RR');
   end if;

   close C_GET_ED_AMEND;

   --- origin country id
   open C_GET_OC_AMEND;
   fetch C_GET_OC_AMEND into O_country;

   if C_GET_OC_AMEND%NOTFOUND then
      O_country := L_country;
   end if;

   close C_GET_OC_AMEND;

   --- transshipment indicator
   open C_GET_TSF_AMEND;
   fetch C_GET_TSF_AMEND into O_transship;

   if C_GET_TSF_AMEND%NOTFOUND then
      O_transship := L_transship;

   end if;

   close C_GET_TSF_AMEND;

   --- partial shipment indicator
   open C_GET_PSF_AMEND;
   fetch C_GET_PSF_AMEND into O_part_ship;

   if C_GET_PSF_AMEND%NOTFOUND then
      O_part_ship := L_part_ship;
   end if;

   close C_GET_PSF_AMEND;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END GET_ORIGINAL_VALUES;

--------------------------------------------------------------------------------------
FUNCTION AMEND_TEXT(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                    O_amend_text      IN OUT   LC_AMENDMENTS.NEW_VALUE%TYPE,
                    I_order_no        IN       LC_AMENDMENTS.ORDER_NO%TYPE,
                    I_item            IN       LC_AMENDMENTS.ITEM%TYPE,
                    I_coded_field     IN       CODE_DETAIL.CODE%TYPE,
                    I_field           IN       CODE_DETAIL.CODE_DESC%TYPE,
                    I_original        IN       LC_AMENDMENTS.ORIGINAL_VALUE%TYPE,
                    I_new             IN       LC_AMENDMENTS.NEW_VALUE%TYPE,
                    I_effect          IN       LC_AMENDMENTS.EFFECT%TYPE,
                    I_currency        IN       CURRENCIES.CURRENCY_CODE%TYPE,
                    I_parent_ind      IN       VARCHAR2)
RETURN BOOLEAN IS
   L_program                  VARCHAR2(50)                := 'LC_AMEND_SQL.AMEND_TEXT';
   L_amend_text               VARCHAR2(2000);
   L_amend_type               VARCHAR2(15);
   L_narrative                VARCHAR2(100);
   L_country_name_new         COUNTRY.COUNTRY_DESC%TYPE;
   L_country_name_original    COUNTRY.COUNTRY_DESC%TYPE;
   L_field                    CODE_DETAIL.CODE_DESC%TYPE;
   L_code_type                CODE_DETAIL.CODE_TYPE%TYPE;
   L_field_decode             CODE_DETAIL.CODE_DESC%TYPE  := I_field;
   L_decoded_value_new        CODE_DETAIL.CODE_DESC%TYPE;
   L_decoded_value_original   CODE_DETAIL.CODE_DESC%TYPE;
   L_qty                      LC_DETAIL.QTY%TYPE;
   L_exists                   BOOLEAN;
   L_doc_desc                 DOC.DOC_DESC%TYPE;
   L_text                     DOC.TEXT%TYPE;
   L_length                   NUMBER;
   L_item                     VARCHAR2(25);

BEGIN

   if I_parent_ind = 'Y' then
      L_item := 'Item Parent';
   else
      L_item := 'Item';
   end if;
   --- populate the beginning of the amendment text
   L_amend_text := '';

   --- establish 'L_amend_type'.
   if I_coded_field in ('NA','C','OQ') then
      L_amend_type := 'amount';
      L_narrative  := '-from ';
   elsif I_coded_field in ('LSD','ESD','ED') then
      L_amend_type := 'date';
      L_narrative  := '-from';
   elsif I_coded_field = 'TSF' then
      L_amend_type := 'switch';
      L_narrative  := 'Has been made';
      L_field      := 'Transshipable';
   elsif I_coded_field = 'TFF' then
      L_amend_type := 'switch';
      L_narrative  := 'Has been made';
      L_field      := 'Transferable';
   elsif I_coded_field = 'PE' then
      L_amend_type := 'decode';
      L_code_type  := 'LCPE';
      L_narrative  := 'Place of expiry-from';
   elsif I_coded_field = 'PRT' then
      L_amend_type := 'decode';
      L_code_type  := 'LCPT';
      L_narrative  := 'Presentation terms-from';
   elsif I_coded_field in ('NG','PSF') then
      L_amend_type := 'undefined';
      L_narrative  := NULL;
   else
      L_amend_type := 'undefined';
   end if;
   ---
   if I_field is NULL then
      if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                    'LCAF',
                                    I_coded_field,
                                    L_field_decode) = FALSE then
         return FALSE;
      end if;
   end if;
   ---
   -- generate text strings
   ---
   if I_new is not NULL or                   --- new value
      I_coded_field in ('RO','RRQD') then    --- remove items, no new value

      if L_amend_type = 'date' then
         ---
         L_amend_text := L_amend_text || L_field_decode || rtrim(L_narrative) || ' ' ||
                         I_original || ' to ' || I_new || '.';

      elsif L_amend_type = 'switch' then
         if I_new = 'N' then
            L_amend_text := L_amend_text || rtrim(L_narrative) || ' Non-' || L_field || '.';
         else
            L_amend_text := L_amend_text || rtrim(L_narrative) || ' ' || L_field || '.';
         end if;

      elsif L_amend_type = 'amount' then
         if I_coded_field = 'NA' then
            L_amend_text := L_amend_text || L_field_decode || L_narrative ||
                            I_original || ' to ' || I_new || ' , resulting effect is ' ||
                            to_char((I_effect)) || ' (' || I_currency || ').';
         else
            L_amend_text := L_amend_text || L_field_decode || L_narrative ||
                            I_original || ' to ' || I_new ||
                            ' for ' || L_item ||' '|| I_item || ', resulting effect is ' || to_char((I_effect))
                            || ' (' || I_currency || ').';
         end if;

      elsif L_amend_type = 'decode' then
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       L_code_type,
                                       I_original,
                                       L_decoded_value_original) = FALSE then
            return FALSE;
         end if;
         ---
         if LANGUAGE_SQL.GET_CODE_DESC(O_error_message,
                                       L_code_type,
                                       I_new,
                                       L_decoded_value_new) = FALSE then
            return FALSE;
         end if;
         ---
         L_amend_text := L_amend_text || rtrim(L_narrative) || ' ' ||
                         L_decoded_value_original || ' to ' ||
                         L_decoded_value_new || '.';

      elsif L_amend_type = 'undefined' then
         if I_coded_field = 'ND' then
            L_amend_text := L_amend_text || 'Documents must be presented no later ' ||
                            'than ' || I_new || ' day(s) from the date of issuance' ||
                            ' of bills of lading (or other shipping documents), ' ||
                            'but prior to the expiration date of this letter of ' ||
                            'credit.';

         elsif I_coded_field = 'PSF' then
            if I_new = 'N' then
               L_amend_text := L_amend_text || 'Does not permit partial shipments.';
            else
               L_amend_text := L_amend_text || 'Permits partial shipments.';
            end if;

         elsif I_coded_field = 'AI' then
            L_qty := abs(I_effect) / to_number(I_new);

            L_amend_text := L_amend_text || 'Purchase Order ' || to_char(I_order_no)
                            || ' with '|| L_item ||' ' || I_item || ' has been added'
                            || ' with a cost of ' || I_new || ' and a quantity of '
                            || to_char(L_qty) || ' resulting in an increase of '
                            || to_char(abs(I_effect)) || ' (' || I_currency || ').';

         elsif I_coded_field = 'AO' then
            L_amend_text := L_amend_text || 'Purchase Order ' || I_order_no ||
                            ' added resulting in an increase of ' ||
                            to_char(abs(I_effect))|| ' (' || I_currency || ').';

         elsif I_coded_field = 'RO' then
            L_amend_text := L_amend_text || 'Purchase Order ' || I_order_no ||
                            ' removed resulting in a decrease of ' ||
                            to_char(abs(I_effect)) || ' (' || I_currency || ').';

         elsif I_coded_field = 'ARQD' then
            if DOCUMENTS_SQL.GET_INFO(O_error_message,
                                      L_exists,
                                      L_doc_desc,
                                      L_text,
                                      I_new) = FALSE then
               return FALSE;
            end if;
            ---
            L_amend_text := L_amend_text || 'Document added-"';
            L_length := LENGTHB(L_amend_text);
            L_amend_text := L_amend_text || RTRIM(SUBSTRB(L_text,1,1997-L_length)) || '".';
            ---
         elsif I_coded_field = 'RRQD' then
            if DOCUMENTS_SQL.GET_INFO(O_error_message,
                                      L_exists,
                                      L_doc_desc,
                                      L_text,
                                      I_original) = FALSE then
               return FALSE;
            end if;
            ---
            L_amend_text := L_amend_text || 'Document removed-"';
            L_length     := LENGTHB(L_amend_text);
            L_amend_text := L_amend_text || RTRIM(SUBSTRB(L_text,1,1997-L_length)) || '".';

         elsif I_coded_field = 'NC' then
            L_amend_text := 'New comment added: '||I_new;
         end if;

      end if;
   end if;

   O_amend_text := L_amend_text;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END AMEND_TEXT;

--------------------------------------------------------------------------------------
FUNCTION WRITE_FIXED_AMEND(O_error_message      IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           I_old_early_ship     IN       LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                           I_new_early_ship     IN       LC_HEAD.EARLIEST_SHIP_DATE%TYPE,
                           I_old_late_ship      IN       LC_HEAD.LATEST_SHIP_DATE%TYPE,
                           I_new_late_ship      IN       LC_HEAD.LATEST_SHIP_DATE%TYPE,
                           I_old_exp_date       IN       LC_HEAD.EXPIRATION_DATE%TYPE,
                           I_new_exp_date       IN       LC_HEAD.EXPIRATION_DATE%TYPE,
                           I_old_place_of_exp   IN       LC_HEAD.PLACE_OF_EXPIRY%TYPE,
                           I_new_place_of_exp   IN       LC_HEAD.PLACE_OF_EXPIRY%TYPE,
                           I_old_net_amount     IN       LC_HEAD.AMOUNT%TYPE,
                           I_new_net_amount     IN       LC_HEAD.AMOUNT%TYPE,
                           I_old_neg_days       IN       LC_HEAD.LC_NEG_DAYS%TYPE,
                           I_new_neg_days       IN       LC_HEAD.LC_NEG_DAYS%TYPE,
                           I_old_present_term   IN       LC_HEAD.PRESENTATION_TERMS%TYPE,
                           I_new_present_term   IN       LC_HEAD.PRESENTATION_TERMS%TYPE,
                           I_old_transship      IN       LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                           I_new_transship      IN       LC_HEAD.TRANSSHIPMENT_IND%TYPE,
                           I_old_transferable   IN       LC_HEAD.TRANSFERABLE_IND%TYPE,
                           I_new_transferable   IN       LC_HEAD.TRANSFERABLE_IND%TYPE,
                           I_old_partial_ship   IN       LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                           I_new_partial_ship   IN       LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE,
                           I_add_req_doc        IN       REQ_DOC.DOC_ID%TYPE,
                           I_remove_req_doc     IN       REQ_DOC.DOC_ID%TYPE,
                           I_lc_exp_days        IN       SYSTEM_OPTIONS.LC_EXP_DAYS%TYPE,
                           I_lc_ref_id          IN       LC_HEAD.LC_REF_ID%TYPE,
                           I_new_comment        IN       LC_AMENDMENTS.NEW_VALUE%TYPE)

RETURN BOOLEAN IS
   L_program  VARCHAR2(50)        := 'LC_AMEND_SQL.WRITE_FIXED_AMEND';
   L_vdate    PERIOD.VDATE%TYPE   := GET_VDATE;
BEGIN
   if I_new_early_ship is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'ESD',
                                TO_CHAR(I_old_early_ship,'DD-MON-RR'),
                                TO_CHAR(I_new_early_ship, 'DD-MON-RR'),
                                'N');
   end if;

   if I_new_late_ship is not NULL then
      --- check expiration date is valid with new latest ship date
      if I_new_exp_date is NULL then
         if I_old_exp_date < I_new_late_ship + I_lc_exp_days then
            insert into lc_amendments(lc_ref_id,
                                      amended_field,
                                      original_value,
                                      new_value,
                                      status)
                               values(I_lc_ref_id,
                                      'ED',
                                      TO_CHAR(I_old_exp_date,'DD-MON-RR'),
                                      TO_CHAR((I_new_late_ship + I_lc_exp_days),'DD-MON-RR'),
                                      'N');
         end if;
      end if;

      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'LSD',
                                TO_CHAR(I_old_late_ship,'DD-MON-RR'),
                                TO_CHAR(I_new_late_ship,'DD-MON-RR'),
                                'N');
   end if;

   if I_new_exp_date is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'ED',
                                TO_CHAR(I_old_exp_date,'DD-MON-RR'),
                                TO_CHAR(I_new_exp_date,'DD-MON-RR'),
                                'N');
   end if;

   if I_new_place_of_exp is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'PE',
                                I_old_place_of_exp,
                                I_new_place_of_exp,
                                'N');
   end if;

   if I_new_net_amount is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                effect,
                                status)
                         values(I_lc_ref_id,
                                'NA',
                                I_old_net_amount,
                                I_new_net_amount,
                                (I_new_net_amount - I_old_net_amount),
                                'N');
   end if;

   if I_new_neg_days is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'ND',
                                I_old_neg_days,
                                I_new_neg_days,
                                'N');
   end if;

   if I_new_present_term is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'PRT',
                                I_old_present_term,
                                I_new_present_term,
                                'N');
   end if;

   if I_new_transship is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'TSF',
                                I_old_transship,
                                I_new_transship,
                                'N');
   end if;

   if I_new_transferable is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'TFF',
                                I_old_transferable,
                                I_new_transferable,
                                'N');
   end if;

   if I_new_partial_ship is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'PSF',
                                I_old_partial_ship,
                                I_new_partial_ship,
                                'N');
   end if;

   if I_add_req_doc is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                new_value,
                                status)
                         values(I_lc_ref_id,
                                'ARQD',
                                I_add_req_doc,
                                'N');
   end if;

   if I_remove_req_doc is not NULL then
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                status)
                         values(I_lc_ref_id,
                                'RRQD',
                                I_remove_req_doc,
                                'N');
   end if;

   if I_new_comment is not NULL then
      insert into lc_amendments (lc_ref_id,
                                 order_no,
                                 item,
                                 amended_field,
                                 original_value,
                                 new_value,
                                 effect,
                                 status,
                                 accept_date,
                                 confirm_date,
                                 amend_no,
                                 comments)
                         values (I_lc_ref_id,
                                 NULL,
                                 NULL,
                                 'NC',
                                 NULL,
                                 I_new_comment,
                                 NULL,
                                 'N',
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL);
   end if;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_FIXED_AMEND;
--------------------------------------------------------------------------------------
FUNCTION WRITE_ORDER_AMEND(O_error_message  IN OUT  VARCHAR2,
                           I_order_no       IN      LC_AMENDMENTS.ORDER_NO%TYPE,
                           I_lc_ref_id      IN      LC_AMENDMENTS.LC_REF_ID%TYPE)
RETURN BOOLEAN IS
   L_program         VARCHAR2(50) := 'LC_AMEND_SQL.WRITE_ORDER_AMEND';
   L_exist           BOOLEAN;

   L_amended_field   LC_AMENDMENTS.AMENDED_FIELD%TYPE;
   L_original_value  LC_AMENDMENTS.ORIGINAL_VALUE%TYPE;
   L_new_value       LC_AMENDMENTS.NEW_VALUE%TYPE;
   L_effect          LC_AMENDMENTS.EFFECT%TYPE;

   L_earliest_ship   LC_DETAIL.EARLIEST_SHIP_DATE%TYPE;
   L_latest_ship     LC_DETAIL.LATEST_SHIP_DATE%TYPE;
   L_order_no        LC_DETAIL.ORDER_NO%TYPE;
   L_transship_ind   LC_HEAD.TRANSSHIPMENT_IND%TYPE          := 'N';
   L_partial_ind     LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE       := 'N';
   L_origin_country  LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_country_old     LC_HEAD.ORIGIN_COUNTRY_ID%TYPE          := NULL;
   L_country_new     LC_HEAD.ORIGIN_COUNTRY_ID%TYPE          := NULL;
   L_amount          LC_HEAD.AMOUNT%TYPE;

   L_early_ship      LC_HEAD.EARLIEST_SHIP_DATE%TYPE;
   L_late_ship       LC_HEAD.LATEST_SHIP_DATE%TYPE;
   L_exp_date        LC_HEAD.EXPIRATION_DATE%TYPE;
   L_country         LC_HEAD.ORIGIN_COUNTRY_ID%TYPE;
   L_transship       LC_HEAD.TRANSSHIPMENT_IND%TYPE;
   L_part_ship       LC_HEAD.PARTIAL_SHIPMENT_IND%TYPE;
   L_form_type       LC_HEAD.FORM_TYPE%TYPE;

   cursor C_GET_TYPE is
      select form_type
        from lc_head
       where lc_ref_id = I_lc_ref_id;

   cursor C_GET_DATES is
      select MIN(earliest_ship_date),
             MAX(latest_ship_date)
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         and order_no != I_order_no;

   cursor C_GET_DATES_LONG is
      select MIN(earliest_ship_date),
             MAX(latest_ship_date)
        from ordhead
       where order_no in (select order_no
                            from lc_detail
                           where lc_ref_id = I_lc_ref_id
                             and order_no != I_order_no);

   cursor C_GET_TRANSSHP is
      select 'Y'
        from ordlc o,
             lc_detail l
       where l.lc_ref_id = I_lc_ref_id
         and o.order_no != I_order_no
         and o.order_no = l.order_no
         and o.lc_ref_id = I_lc_ref_id
         and o.lc_ind = 'Y'
         and o.transshipment_ind = 'Y';

   cursor C_PARTIAL_SHP is
      select 'Y'
        from ordlc o,
             lc_detail l
       where l.lc_ref_id = I_lc_ref_id
         and o.order_no != I_order_no
         and o.order_no = l.order_no
         and o.lc_ref_id = I_lc_ref_id
         and o.lc_ind = 'Y'
         and o.partial_shipment_ind = 'Y';

   cursor C_AMT_LC_DETAIL is
      select SUM(cost * NVL(qty,1))
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         and order_no  = I_order_no;

   cursor C_GET_ORDER is
      select order_no
        from lc_detail
       where lc_ref_id = I_lc_ref_id
         and order_no != I_order_no;

BEGIN
   if LC_AMEND_SQL.GET_ORIGINAL_VALUES (O_error_message,
                                        L_early_ship,
                                        L_late_ship,
                                        L_exp_date,
                                        L_country,
                                        L_transship,
                                        L_part_ship,
                                        I_lc_ref_id) = FALSE then

      return FALSE;
   end if;
   ---
   SQL_LIB.SET_MARK('OPEN','C_GET_TYPE','LC_HEAD',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   open C_GET_TYPE;
   SQL_LIB.SET_MARK('FETCH','C_GET_TYPE','LC_HEAD',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   fetch C_GET_TYPE into L_form_type;
   SQL_LIB.SET_MARK('CLOSE','C_GET_TYPE','LC_HEAD',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   close C_GET_TYPE;
   ---
   SQL_LIB.SET_MARK('OPEN','C_AMT_LC_DETAIL','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   open C_AMT_LC_DETAIL;
   SQL_LIB.SET_MARK('FETCH','C_AMT_LC_DETAIL','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   fetch C_AMT_LC_DETAIL into L_amount;
   SQL_LIB.SET_MARK('CLOSE','C_AMT_LC_DETAIL','LC_DETAIL',
                    'lc_ref_id: ' || to_char(I_lc_ref_id));
   close C_AMT_LC_DETAIL;
   ---
   if L_form_type = 'S' then
      SQL_LIB.SET_MARK('OPEN','C_GET_DATES','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      open C_GET_DATES;
      SQL_LIB.SET_MARK('FETCH','C_GET_DATES','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      fetch C_GET_DATES into L_earliest_ship,
                             L_latest_ship;

      if L_earliest_ship is NULL or L_latest_ship is NULL then
         SQL_LIB.SET_MARK('CLOSE','C_GET_DATES','LC_DETAIL',
                          'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                          'order_no: ' || to_char(I_order_no));
         close C_GET_DATES;
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   order_no,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status)
                            values(I_lc_ref_id,
                                   I_order_no,
                                   'RO',                  --- Remove order
                                   L_amount,              --- Total order cost
                                   0,
                                   (0 - L_amount),
                                   'N');
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DATES','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      close C_GET_DATES;

   elsif L_form_type = 'L' then

      SQL_LIB.SET_MARK('OPEN','C_GET_DATES_LONG','ORDSKU',NULL);
      open C_GET_DATES_LONG;
      SQL_LIB.SET_MARK('FETCH','C_GET_DATES_LONG','ORDSKU',NULL);
      fetch C_GET_DATES_LONG into L_earliest_ship,
                                  L_latest_ship;

      if L_earliest_ship is NULL or L_latest_ship is NULL then
         SQL_LIB.SET_MARK('CLOSE','C_GET_DATES_LONG','ORDSKU',NULL);
         close C_GET_DATES_LONG;
         ---
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   order_no,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status)
                            values(I_lc_ref_id,
                                   I_order_no,
                                   'RO',                  --- Remove order
                                   L_amount,              --- Total order cost
                                   0,
                                   (0 - L_amount),
                                   'N');
         return TRUE;
      end if;
      SQL_LIB.SET_MARK('CLOSE','C_GET_DATES_LONG','ORDSKU',NULL);
      close C_GET_DATES_LONG;
   end if;
   ---
   if L_transship = 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_GET_TRANSSHP','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      open C_GET_TRANSSHP;
      SQL_LIB.SET_MARK('FETCH','C_GET_TRANSSHP','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      fetch C_GET_TRANSSHP into L_transship_ind;
      SQL_LIB.SET_MARK('CLOSE','C_GET_TRANSSHP','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      close C_GET_TRANSSHP;

      --- create an amendment when the letter of credit's transshipment
      --- indicator = Yes and the order's transshipment indicator = No.
      if L_transship_ind = 'N' then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status)
                            values(I_lc_ref_id,
                                   'TSF',                 --- transshipment ind
                                   'Y',                   --- Yes
                                   L_transship_ind,       --- No
                                   NULL,
                                   'N');
      end if;
   end if;
   ---
   if L_part_ship = 'Y' then
      SQL_LIB.SET_MARK('OPEN','C_PARTIAL_SHP','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      open C_PARTIAL_SHP;
      SQL_LIB.SET_MARK('FETCH','C_PARTIAL_SHP','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      fetch C_PARTIAL_SHP into L_partial_ind;
      SQL_LIB.SET_MARK('CLOSE','C_PARTIAL_SHP','LC_DETAIL',
                       'lc_ref_id: ' || to_char(I_lc_ref_id) ||
                       'order_no: ' || to_char(I_order_no));
      close C_PARTIAL_SHP;

      --- create an amendment when the letter of credit's partial shipment
      --- indicator = Yes and the order's partial shipment indicator = No.
      if L_partial_ind = 'N' then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status)
                            values(I_lc_ref_id,
                                   'PSF',               --- partial shipment ind
                                   'Y',                 --- Yes
                                   L_partial_ind,       --- No
                                   NULL,
                                   'N');
      end if;
   end if;
   ---
   if L_early_ship != L_earliest_ship then
      SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                effect,
                                status)
                         values(I_lc_ref_id,
                                'ESD',               --- earliest ship date
                                TO_CHAR(L_early_ship,'DD-MON-RR'),
                                TO_CHAR(L_earliest_ship,'DD-MON-RR'),
                                NULL,
                                'N');
   end if;
   ---
   if L_late_ship != L_latest_ship then
      SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
      insert into lc_amendments(lc_ref_id,
                                amended_field,
                                original_value,
                                new_value,
                                effect,
                                status)
                         values(I_lc_ref_id,
                                'LSD',               --- latest ship date
                                TO_CHAR(L_late_ship,'DD-MON-RR'),
                                TO_CHAR(L_latest_ship,'DD-MON-RR'),
                                NULL,
                                'N');
   end if;
   ---
   if L_country = '99' then
      FOR C_get_order_rec in C_GET_ORDER LOOP

         L_order_no := C_get_order_rec.order_no;
         ---
         if ORDER_ATTRIB_SQL.GET_COUNTRY_OF_ORIGIN(O_error_message,
                                                   L_origin_country,
                                                   L_order_no) = FALSE then
            return FALSE;
         end if;
         ---
         L_country_new := L_origin_country;
         ---
         if L_country_new = '99' then
            Exit;
         end if;
         ---
         if L_country_new != '99' then
            if L_country_old is NULL then
               L_country_old := L_country_new;
            end if;
            ---
            if L_country_new != L_country_old then
               L_country_new := '99';
               Exit;
            end if;
         end if;
      END LOOP;
      ---
      if L_country_new != '99' then
         SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
         insert into lc_amendments(lc_ref_id,
                                   amended_field,
                                   original_value,
                                   new_value,
                                   effect,
                                   status)
                            values(I_lc_ref_id,
                                   'OC',            --- origin country id
                                   L_country,
                                   L_country_new,
                                   NULL,
                                   'N');
      end if;
   end if;
   ---
   SQL_LIB.SET_MARK('INSERT',NULL,'LC_AMENDMENTS',NULL);
   insert into lc_amendments(lc_ref_id,
                             order_no,
                             amended_field,
                             original_value,
                             new_value,
                             effect,
                             status)
                      values(I_lc_ref_id,
                             I_order_no,
                             'RO',                  --- Remove order
                             L_amount,              --- Total order cost
                             0,
                             (0 - L_amount),
                             'N');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END WRITE_ORDER_AMEND;
--------------------------------------------------------------------------------------
FUNCTION UPDATE_BANK(O_error_message  IN OUT VARCHAR2,
                     I_update_type    IN     VARCHAR2,
                     I_amount         IN     PARTNER.LINE_OF_CREDIT%TYPE,
                     I_issuing_bank   IN     PARTNER.PARTNER_ID%TYPE)
RETURN BOOLEAN IS
   L_program     VARCHAR2(50) := 'LC_AMEND_SQL.UPDATE_BANK';
   RECORD_LOCKED EXCEPTION;
   PRAGMA        exception_init (RECORD_LOCKED, -54);

   cursor C_LOCK_PARTNER is
      select 'x'
        from partner
       where partner_type = 'BK'
         and partner_id   = I_issuing_bank
         for update nowait;

BEGIN
   SQL_LIB.SET_MARK ('OPEN', 'C_LOCK_PARTNER', 'PARTNER',
                     'Bank: ' || I_issuing_bank);
   open C_LOCK_PARTNER;

   SQL_LIB.SET_MARK ('CLOSE', 'C_LOCK_PARTNER', 'PARTNER',
                     'Bank: ' || I_issuing_bank);
   close C_LOCK_PARTNER;

   --- when an amendment with a cost effect is generated or
   --- a letter of credit is confirmed
   if I_update_type in ('A','L') then
      update partner
         set outstand_credit = outstand_credit + I_amount,
             open_credit     = open_credit - I_amount,
             ytd_credit      = ytd_credit + I_amount
       where partner_type    = 'BK'
         and partner_id      = I_issuing_bank;

   --- when a drawdown activity is entered
   elsif I_update_type = 'D' then
      update partner
         set outstand_credit = outstand_credit - I_amount,
             open_credit     = open_credit + I_amount,
             ytd_drawdowns   = ytd_drawdowns + I_amount
       where partner_type    = 'BK'
         and partner_id      = I_issuing_bank;

   --- when an accepted amendment is deleted (removed)
   elsif I_update_type = 'R' then
      update partner
         set outstand_credit = outstand_credit - I_amount,
             open_credit     = open_credit + I_amount,
             ytd_credit      = ytd_credit - I_amount
       where partner_type    = 'BK'
         and partner_id      = I_issuing_bank;
   end if;

   return TRUE;
EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG ('TABLE_LOCKED',
                                             'PARTNER',
                                              I_issuing_bank,
                                              NULL);
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                              SQLERRM,
                                              L_program,
                                              to_char(SQLCODE));
      return FALSE;
END UPDATE_BANK;
--------------------------------------------------------------------------------------
FUNCTION PARENT_ROLLUP(O_error_message  IN OUT VARCHAR2,
                       O_new_value      IN OUT LC_AMENDMENTS.NEW_VALUE%TYPE,
                       O_original_value IN OUT LC_AMENDMENTS.ORIGINAL_VALUE%TYPE,
                       O_effect         IN OUT LC_AMENDMENTS.EFFECT%TYPE,
                       I_lc_ref_id      IN     LC_AMENDMENTS.LC_REF_ID%TYPE,
                       I_order_no       IN     LC_AMENDMENTS.ORDER_NO%TYPE,
                       I_item_parent    IN     LC_AMENDMENTS.ITEM%TYPE,
                       I_amended_field  IN     LC_AMENDMENTS.AMENDED_FIELD%TYPE,
                       I_amend_no       IN     LC_AMENDMENTS.AMEND_NO%TYPE)
   RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'LC_AMEND_SQL.PARENT_ROLLUP';
   L_cost_amend_exists VARCHAR2(1);
   L_esd_amend_exists  VARCHAR2(1);
   L_lsd_amend_exists  VARCHAR2(1);
   L_item              ITEM_MASTER.ITEM%TYPE;
   L_item_qty          LC_DETAIL.QTY%TYPE;
   L_item_parent_qty   LC_DETAIL.QTY%TYPE := 0;
   L_amended_field     LC_AMENDMENTS.AMENDED_FIELD%TYPE;
   L_cost              LC_DETAIL.COST%TYPE;
   L_ordloc_cost       ORDLOC.UNIT_COST%TYPE;
   L_date              DATE;
   ---
   cursor C_GET_COST_NEW is
      select MAX(la.new_value)
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.amended_field = L_amended_field
         and la.amend_no      = I_amend_no
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item;
   ---
   cursor C_AMENDMENTS_EXIST is
      select 'x'
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item
         and la.amended_field = L_amended_field
         and la.amend_no      < I_amend_no;
   ---
   cursor C_GET_AMEND_COST is
      select MAX(la.new_value)
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and im.item_level    = im.tran_level
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item
         and la.amended_field = L_amended_field
         and la.amend_no      = (select MAX(amend_no)
                                   from lc_amendments la2,
                                        item_master im2
                                  where la2.lc_ref_id    = la.lc_ref_id
                                   and la2.order_no      = la.order_no
                                   and la2.amended_field = la.amended_field
                                   and (  (im2.item_parent      = im.item_parent)
                                        or(im2.item_grandparent = im.item_grandparent))
                                   and la2.item          = im2.item
                                   and la2.amend_no      < I_amend_no);
   ---
   cursor C_GET_DETAIL_COST is
      select MAX(ld.cost)
        from lc_detail ld,
             item_master im
       where ld.lc_ref_id = I_lc_ref_id
         and ld.order_no  = I_order_no
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and ld.item      = im.item;
   ---
   cursor C_GET_ITEMS is
      select DISTINCT im.item
        from item_master im
       where im.item_level = im.tran_level
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and (exists (select 'x'
                        from lc_detail ld
                       where ld.lc_ref_id = I_lc_ref_id
                         and ld.order_no  = I_order_no
                         and ld.item      = im.item)
          or exists (select 'x'
                       from lc_amendments la
                      where la.lc_ref_id = I_lc_ref_id
                        and la.order_no  = I_order_no
                        and la.item      = im.item
                        and la.amend_no  <= I_amend_no));
   ---
   cursor C_GET_ITEMS_ORIG is
      select DISTINCT im.item
        from item_master im
       where im.item_level = im.tran_level
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and (exists (select 'x'
                        from lc_detail ld
                       where ld.lc_ref_id = I_lc_ref_id
                         and ld.order_no  = I_order_no
                         and ld.item      = im.item)
          or exists (select 'x'
                       from lc_amendments la
                      where la.lc_ref_id = I_lc_ref_id
                        and la.order_no  = I_order_no
                        and la.item      = im.item
                        and la.amend_no  < I_amend_no));
   ---
   cursor C_GET_TOTAL_AMEND_QTY is
      select la.new_value
        from lc_amendments la
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.item          = L_item
         and la.amended_field = 'OQ'
         and la.amend_no = (select MAX(la2.amend_no)
                              from lc_amendments la2
                             where la2.lc_ref_id     = la.lc_ref_id
                               and la2.order_no      = la.order_no
                               and la2.item          = la.item
                               and la2.amended_field = 'OQ'
                               and la2.amend_no      <= I_amend_no);
   ---
   cursor C_GET_DETAIL_QTY is
      select ld.qty
        from lc_detail ld
       where ld.lc_ref_id = I_lc_ref_id
         and ld.order_no  = I_order_no
         and ld.item      = L_item;
   ---
   cursor C_GET_ORDLOC_QTY is
      select SUM(ol.qty_ordered)
        from ordloc ol
       where ol.order_no = I_order_no
         and ol.item      = L_item;
   ---
   cursor C_GET_TOTAL_AMEND_QTY_ORIG is
      select la.new_value
        from lc_amendments la
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.item          = L_item
         and la.amended_field = 'OQ'
         and la.amend_no = (select MAX(la2.amend_no)
                              from lc_amendments la2
                             where la2.lc_ref_id     = la.lc_ref_id
                               and la2.order_no      = la.order_no
                               and la2.item          = la.item
                               and la2.amended_field = la.amended_field
                               and la2.amend_no      < I_amend_no);
   ---
   cursor C_GET_ESD is
      select TO_CHAR(MIN(TO_DATE(new_value, 'DD-MON-RR')), 'RRMMDD')
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.amend_no      = I_amend_no
         and la.amended_field = I_amended_field
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item;
   ---
   cursor C_GET_ESD_ORIG is
      select TO_CHAR(MIN(TO_DATE(new_value, 'DD-MON-RR')), 'RRMMDD')
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.amended_field = I_amended_field
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item
         and la.amend_no      = (select MAX(amend_no)
                                   from lc_amendments la2,
                                        item_master im2
                                  where la2.lc_ref_id     = la.lc_ref_id
                                    and la2.order_no      = la.order_no
                                    and la2.amended_field = la.amended_field
                                    and (  (im2.item_parent      = im.item_parent)
                                         or(im2.item_grandparent = im.item_grandparent))
                                    and la2.item          = im2.item
                                    and la2.amend_no      < I_amend_no);
   ---
   cursor C_GET_DETAIL_ESD is
      select TO_CHAR(MIN(earliest_ship_date), 'RRMMDD')
        from lc_detail ld,
             item_master im
       where ld.lc_ref_id   = I_lc_ref_id
         and ld.order_no    = I_order_no
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and ld.item        = im.item;
   ---
   cursor C_GET_LSD is
      select TO_CHAR(MAX(TO_DATE(new_value, 'DD-MON-RR')), 'RRMMDD')
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.amend_no      = I_amend_no
         and la.amended_field = I_amended_field
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item;
   ---
   cursor C_GET_LSD_ORIG is
      select TO_CHAR(MAX(TO_DATE(new_value, 'DD-MON-RR')), 'RRMMDD')
        from lc_amendments la,
             item_master im
       where la.lc_ref_id     = I_lc_ref_id
         and la.order_no      = I_order_no
         and la.amended_field = I_amended_field
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and la.item          = im.item
         and la.amend_no      = (select MAX(amend_no)
                                   from lc_amendments la2,
                                        item_master im2
                                  where la2.lc_ref_id     = la.lc_ref_id
                                    and la2.order_no      = la.order_no
                                    and la2.amended_field = la.amended_field
                                    and (  (im2.item_parent      = im.item_parent)
                                         or(im2.item_grandparent = im.item_grandparent))
                                    and la2.item          = im2.item
                                    and la2.amend_no      < I_amend_no);
   ---
   cursor C_GET_DETAIL_LSD is
      select TO_CHAR(MAX(latest_ship_date), 'RRMMDD')
        from lc_detail ld,
             item_master im
       where ld.lc_ref_id   = I_lc_ref_id
         and ld.order_no    = I_order_no
         and (  (im.item_parent      = I_item_parent)
              or(im.item_grandparent = I_item_parent))
         and ld.item      = im.item;
   ---
   cursor C_GET_ORDLOC_COST is
      select MAX(os.unit_cost)
        from ordloc os
       where os.order_no = I_order_no
         and exists (select 'x'
                       from lc_amendments la,
                            item_master im
                      where (  (im.item_parent      = I_item_parent)
                             or(im.item_grandparent = I_item_parent))
                        and im.item         = os.item
                        and im.item         = la.item
                        and la.lc_ref_id    = I_lc_ref_id
                        and la.order_no     = os.order_no
                        and la.amend_no     = I_amend_no);

BEGIN

   O_effect := 0;
   ---
   if I_lc_ref_id is NOT NULL or
      I_order_no is NOT NULL or
      I_item_parent is NOT NULL or
      I_amended_field is NOT NULL or
      I_amend_no is NOT NULL then
      ---
      if I_amended_field = 'C' then         --- 'C' Cost
         --- retrieve the new cost
         L_amended_field := I_amended_field;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_COST_NEW','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_GET_COST_NEW;
         SQL_LIB.SET_MARK('FETCH','C_GET_COST_NEW','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_GET_COST_NEW into O_new_value;
         SQL_LIB.SET_MARK('CLOSE','C_GET_COST_NEW','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_GET_COST_NEW;
         --- retrieve the original cost
         SQL_LIB.SET_MARK('OPEN','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_AMENDMENTS_EXIST;
         SQL_LIB.SET_MARK('FETCH','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_AMENDMENTS_EXIST into L_cost_amend_exists;
         SQL_LIB.SET_MARK('CLOSE','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_AMENDMENTS_EXIST;
         ---
         if L_cost_amend_exists is NOT NULL then
            SQL_LIB.SET_MARK('OPEN','C_GET_AMEND_COST','LC_AMENDMENTS, ITEM_MASTER',NULL);
            open C_GET_AMEND_COST;
            SQL_LIB.SET_MARK('FETCH','C_GET_AMEND_COST','LC_AMENDMENTS, ITEM_MASTER',NULL);
            fetch C_GET_AMEND_COST into O_original_value;
            SQL_LIB.SET_MARK('CLOSE','C_GET_AMEND_COST','LC_AMENDMENTS, ITEM_MASTER',NULL);
            close C_GET_AMEND_COST;
         else
            SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_COST','LC_DETAIL, ITEM_MASTER',NULL);
            open C_GET_DETAIL_COST;
            SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_COST','LC_DETAIL, ITEM_MASTER',NULL);
            fetch C_GET_DETAIL_COST into O_original_value;
            SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_COST','LC_DETAIL, ITEM_MASTER',NULL);
            close C_GET_DETAIL_COST;
         end if;
         --- calculate effect
         SQL_LIB.SET_MARK('OPEN','C_GET_ITEMS','ITEM_MASTER, LC_AMENDMENT, LC_DETAIL',NULL);
         for A_rec in C_GET_ITEMS LOOP
            L_item     := A_rec.item;
            L_item_qty := NULL;
            ---
            SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_AMEND_QTY','LC_AMENDMENTS',NULL);
            open C_GET_TOTAL_AMEND_QTY;
            SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_AMEND_QTY','LC_AMENDMENTS',NULL);
            fetch C_GET_TOTAL_AMEND_QTY into L_item_qty;
            SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_AMEND_QTY','LC_AMENDMENTS',NULL);
            close C_GET_TOTAL_AMEND_QTY;
            ---
            if L_item_qty is NULL then
               SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               open C_GET_DETAIL_QTY;
               SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               fetch C_GET_DETAIL_QTY into L_item_qty;
               SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               close C_GET_DETAIL_QTY;
               ---
               if L_item_qty is NULL then
                  SQL_LIB.SET_MARK('OPEN','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  open C_GET_ORDLOC_QTY;
                  SQL_LIB.SET_MARK('FETCH','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  fetch C_GET_ORDLOC_QTY into L_item_qty;
                  SQL_LIB.SET_MARK('CLOSE','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  close C_GET_ORDLOC_QTY;
               end if;
            end if;
            ---
            L_item_parent_qty := L_item_parent_qty + NVL(L_item_qty,0);
         END LOOP;
         ---
         O_effect := L_item_parent_qty * (O_new_value - O_original_value);
         ---
      elsif I_amended_field = 'OQ' then       --- 'OQ' Order Quantity
         --- retrieve the new quantity
         SQL_LIB.SET_MARK('OPEN','C_GET_ITEMS','ITEM_MASTER, LC_AMENDMENTS, LC_DETAIL',NULL);
         for B_rec in C_GET_ITEMS LOOP
            L_item     := B_rec.item;
            L_item_qty := NULL;
            ---
            SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_AMEND_QTY','LC_AMENDMENTS',NULL);
            open C_GET_TOTAL_AMEND_QTY;
            SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_AMEND_QTY','LC_AMENDMENTS',NULL);
            fetch C_GET_TOTAL_AMEND_QTY into L_item_qty;
            SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_AMEND_QTY','LC_AMENDMENTS',NULL);
            close C_GET_TOTAL_AMEND_QTY;
            ---
            if L_item_qty is NULL then
               SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               open C_GET_DETAIL_QTY;
               SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               fetch C_GET_DETAIL_QTY into L_item_qty;
               SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               close C_GET_DETAIL_QTY;
               ---
               if L_item_qty is NULL then
                  SQL_LIB.SET_MARK('OPEN','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  open C_GET_ORDLOC_QTY;
                  SQL_LIB.SET_MARK('FETCH','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  fetch C_GET_ORDLOC_QTY into L_item_qty;
                  SQL_LIB.SET_MARK('CLOSE','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  close C_GET_ORDLOC_QTY;
               end if;
            end if;
            ---
            L_item_parent_qty := L_item_parent_qty + NVL(L_item_qty,0);
         END LOOP;
         ---
         O_new_value := L_item_parent_qty;
         --- Calculate original qty
         L_item_parent_qty := 0;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_ITEMS_ORIG','ITEM_MASTER, LC_AMENDMENTS, LC_DETAIL',NULL);
         for C_rec in C_GET_ITEMS_ORIG LOOP
            L_item     := C_rec.item;
            L_item_qty := NULL;
            ---
            SQL_LIB.SET_MARK('OPEN','C_GET_TOTAL_AMEND_QTY_ORIG','LC_AMENDMENTS',NULL);
            open C_GET_TOTAL_AMEND_QTY_ORIG;
            SQL_LIB.SET_MARK('FETCH','C_GET_TOTAL_AMEND_QTY_ORIG','LC_AMENDMENTS',NULL);
            fetch C_GET_TOTAL_AMEND_QTY_ORIG into L_item_qty;
            SQL_LIB.SET_MARK('CLOSE','C_GET_TOTAL_AMEND_QTY_ORIG','LC_AMENDMENTS',NULL);
            close C_GET_TOTAL_AMEND_QTY_ORIG;
            ---
            if L_item_qty is NULL then
               SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               open C_GET_DETAIL_QTY;
               SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               fetch C_GET_DETAIL_QTY into L_item_qty;
               SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_QTY','LC_DETAIL',NULL);
               close C_GET_DETAIL_QTY;
               ---
               if L_item_qty is NULL then
                  SQL_LIB.SET_MARK('OPEN','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  open C_GET_ORDLOC_QTY;
                  SQL_LIB.SET_MARK('FETCH','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  fetch C_GET_ORDLOC_QTY into L_item_qty;
                  SQL_LIB.SET_MARK('CLOSE','C_GET_ORDLOC_QTY','ORDLOC',NULL);
                  close C_GET_ORDLOC_QTY;
               end if;
            end if;
            ---
            L_item_parent_qty := L_item_parent_qty + NVL(L_item_qty,0);
         END LOOP;
         ---
         O_original_value := L_item_parent_qty;
         --- calculate effect
         L_amended_field := 'C';
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_COST_NEW','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_GET_COST_NEW;
         SQL_LIB.SET_MARK('FETCH','C_GET_COST_NEW','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_GET_COST_NEW into L_cost;
         SQL_LIB.SET_MARK('CLOSE','C_GET_COST_NEW','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_GET_COST_NEW;
         ---
         if L_cost is NULL then
            SQL_LIB.SET_MARK('OPEN','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
            open C_AMENDMENTS_EXIST;
            SQL_LIB.SET_MARK('FETCH','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
            fetch C_AMENDMENTS_EXIST into L_cost_amend_exists;
            SQL_LIB.SET_MARK('CLOSE','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
            close C_AMENDMENTS_EXIST;
            ---
            if L_cost_amend_exists is NOT NULL then
               SQL_LIB.SET_MARK('OPEN','C_GET_AMEND_COST','LC_AMENDMENTS, ITEM_MASTER',NULL);
               open C_GET_AMEND_COST;
               SQL_LIB.SET_MARK('FETCH','C_GET_AMEND_COST','LC_AMENDMENTS, ITEM_MASTER',NULL);
               fetch C_GET_AMEND_COST into L_cost;
               SQL_LIB.SET_MARK('CLOSE','C_GET_AMEND_COST','LC_AMENDMENTS, ITEM_MASTER',NULL);
               close C_GET_AMEND_COST;
            else
               SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_COST','LC_DETAIL, ITEM_MASTER',NULL);
               open C_GET_DETAIL_COST;
               SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_COST','LC_DETAIL, ITEM_MASTER',NULL);
               fetch C_GET_DETAIL_COST into L_cost;
               SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_COST','LC_DETAIL, ITEM_MASTER',NULL);
               close C_GET_DETAIL_COST;
            end if;
            --- if no cost is returned, item_parent was added as an amendment and ordloc cost must be used
            if L_cost is NULL then
               SQL_LIB.SET_MARK('OPEN','C_GET_ORDLOC_COST','ORDLOC, ITEM_MASTER, LC_AMENDMENTS',NULL);
               open C_GET_ORDLOC_COST;
               SQL_LIB.SET_MARK('FETCH','C_GET_ORDLOC_COST','ORDLOC, ITEM_MASTER, LC_AMENDMENTS',NULL);
               fetch C_GET_ORDLOC_COST into L_cost;
               SQL_LIB.SET_MARK('CLOSE','C_GET_ORDLOC_COST','ORDLOC, ITEM_MASTER, LC_AMENDMENTS',NULL);
               close C_GET_ORDLOC_COST;
            end if;
         end if;
         ---
         O_effect := L_cost * (O_new_value - O_original_value);
      elsif I_amended_field = 'AI' then      --- 'AI' Add Item
         --- retrieve the new qty (ordloc.qty_ordered)
         for D_rec in C_GET_ITEMS LOOP
            L_item := D_rec.item;
            L_item_qty := NULL;
            ---
            open C_GET_ORDLOC_QTY;
            fetch C_GET_ORDLOC_QTY into L_item_qty;
            close C_GET_ORDLOC_QTY;
            ---
            L_item_parent_qty := L_item_parent_qty + NVL(L_item_qty,0);
         END LOOP;
         --- retrieve the new cost (ORDLOC.unit_cost)
         open C_GET_ORDLOC_COST;
         fetch C_GET_ORDLOC_COST into L_ordloc_cost;
         close C_GET_ORDLOC_COST;
         ---
         O_original_value := 0;
         O_new_value      := L_ordloc_cost;
         O_effect         := O_new_value * L_item_parent_qty;
      elsif I_amended_field = 'ESD' then     --- 'ESD' Earliest Ship Date
         L_amended_field := I_amended_field;
         --- retrieve new value
         SQL_LIB.SET_MARK('OPEN','C_GET_ESD','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_GET_ESD;
         SQL_LIB.SET_MARK('FETCH','C_GET_ESD','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_GET_ESD into O_new_value;
         SQL_LIB.SET_MARK('CLOSE','C_GET_ESD','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_GET_ESD;
         --- retrieve original value
         SQL_LIB.SET_MARK('OPEN','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_AMENDMENTS_EXIST;
         SQL_LIB.SET_MARK('FETCH','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_AMENDMENTS_EXIST into L_esd_amend_exists;
         SQL_LIB.SET_MARK('CLOSE','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_AMENDMENTS_EXIST;
         ---
         if L_esd_amend_exists is NOT NULL then
            SQL_LIB.SET_MARK('OPEN','C_GET_ESD_ORIG','LC_AMENDMENTS, ITEM_MASTER',NULL);
            open C_GET_ESD_ORIG;
            SQL_LIB.SET_MARK('FETCH','C_GET_ESD_ORIG','LC_AMENDMENTS, ITEM_MASTER',NULL);
            fetch C_GET_ESD_ORIG into O_original_value;
            SQL_LIB.SET_MARK('CLOSE','C_GET_ESD_ORIG','LC_AMENDMENTS, ITEM_MASTER',NULL);
            close C_GET_ESD_ORIG;
         else
            SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_ESD','LC_DETAIL, ITEM_MASTER',NULL);
            open C_GET_DETAIL_ESD;
            SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_ESD','LC_DETAIL, ITEM_MASTER',NULL);
            fetch C_GET_DETAIL_ESD into O_original_value;
            SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_ESD','LC_DETAIL, ITEM_MASTER',NULL);
            close C_GET_DETAIL_ESD;
         end if;
      elsif I_amended_field = 'LSD' then     --- 'LSD' Latest Ship Date
         L_amended_field := I_amended_field;
         --- retrieve new value;
         SQL_LIB.SET_MARK('OPEN','C_GET_LSD','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_GET_LSD;
         SQL_LIB.SET_MARK('FETCH','C_GET_LSD','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_GET_LSD into O_new_value;
         SQL_LIB.SET_MARK('CLOSE','C_GET_LSD','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_GET_LSD;
         --- retrieve original value
         SQL_LIB.SET_MARK('OPEN','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         open C_AMENDMENTS_EXIST;
         SQL_LIB.SET_MARK('FETCH','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         fetch C_AMENDMENTS_EXIST into L_lsd_amend_exists;
         SQL_LIB.SET_MARK('CLOSE','C_AMENDMENTS_EXIST','LC_AMENDMENTS, ITEM_MASTER',NULL);
         close C_AMENDMENTS_EXIST;
         ---
         if L_lsd_amend_exists is NOT NULL then
            SQL_LIB.SET_MARK('OPEN','C_LSD_ORIG','LC_AMENDMENTS, ITEM_MASTER',NULL);
            open C_GET_LSD_ORIG;
            SQL_LIB.SET_MARK('FETCH','C_LSD_ORIG','LC_AMENDMENTS, ITEM_MASTER',NULL);
            fetch C_GET_LSD_ORIG into O_original_value;
            SQL_LIB.SET_MARK('CLOSE','C_LSD_ORIG','LC_AMENDMENTS, ITEM_MASTER',NULL);
            close C_GET_LSD_ORIG;
         else
            SQL_LIB.SET_MARK('OPEN','C_GET_DETAIL_LSD','LC_DETAIL, ITEM_MASTER',NULL);
            open C_GET_DETAIL_LSD;
            SQL_LIB.SET_MARK('FETCH','C_GET_DETAIL_LSD','LC_DETAIL, ITEM_MASTER',NULL);
            fetch C_GET_DETAIL_LSD into O_original_value;
            SQL_LIB.SET_MARK('CLOSE','C_GET_DETAIL_LSD','LC_DETAIL, ITEM_MASTER',NULL);
            close C_GET_DETAIL_LSD;
         end if;
     end if;
   else
      O_error_message := SQL_LIB.CREATE_MSG('INV_PARAM_PROG_UNIT',
                                             L_program,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
   return FALSE;
END PARENT_ROLLUP;
---------------------------------------------------------------------------------------
END LC_AMEND_SQL;
/
