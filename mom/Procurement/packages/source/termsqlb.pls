CREATE OR REPLACE PACKAGE BODY TERMS_SQL AS

-----------------------------------------------------------------------------------------------
FUNCTION HEADER_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_terms           IN       TERMS_HEAD.TERMS%TYPE)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TERMS_SQL.HEADER_EXISTS';
   L_check        VARCHAR2(1)  := 'N';
   PROGRAM_ERROR  EXCEPTION;

   cursor C_HDR_CHK is
      select 'Y'
        from terms_head
       where terms = I_terms;

BEGIN

   open C_HDR_CHK;
   fetch C_HDR_CHK into L_check;
   close C_HDR_CHK;

   if L_check = 'N' then
      raise PROGRAM_ERROR;
   end if;

   return TRUE;

EXCEPTION
   when PROGRAM_ERROR then
      O_error_message := sql_lib.create_msg('INV_TERMS');
      return FALSE;
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                          to_char(SQLCODE));
      return FALSE;
END HEADER_EXISTS;

-----------------------------------------------------------------------------------------------
FUNCTION DETAIL_EXISTS(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_terms           IN       TERMS_HEAD.TERMS%TYPE,
                       I_terms_seq       IN       TERMS_DETAIL.TERMS_SEQ%TYPE)

RETURN BOOLEAN IS

   L_program      VARCHAR2(50) := 'TERMS_SQL.HEADER_EXISTS';
   L_check        VARCHAR2(1)  := 'N';
   PROGRAM_ERROR  EXCEPTION;

   cursor C_DTL_CHK is
      select 'Y'
        from terms_detail
       where terms = I_terms
         and terms_seq = I_terms_seq;

BEGIN

   if HEADER_EXISTS(o_error_message,
                    I_terms) = FALSE then
      return FALSE;
   end if;

   open C_DTL_CHK;
   fetch C_DTL_CHK into L_check;
   close C_DTL_CHK;

   if L_check = 'N' then
      raise PROGRAM_ERROR;
   end if;

   return TRUE;

EXCEPTION
   when PROGRAM_ERROR then
      O_error_message := sql_lib.create_msg('INV_TERMS');
      return FALSE;
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END DETAIL_EXISTS;

-------------------------------------------------------------------------------
FUNCTION MERGE_HEADER(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_paytermrec    IN     PAYTERM_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'TERMS_SQL.MERGE_HEADER';

BEGIN

   MERGE INTO terms_head th
      USING (SELECT I_paytermrec.terms_head_row.terms terms,
                    I_paytermrec.terms_head_row.rank rank
               FROM dual) tbl
         ON (th.terms = tbl.terms)
       WHEN MATCHED THEN
          UPDATE SET
             th.rank = tbl.rank
       WHEN NOT MATCHED THEN
          INSERT(terms,
                 rank)
          VALUES(tbl.terms,
                 tbl.rank);

   MERGE INTO terms_head_tl th
      USING (SELECT I_paytermrec.terms_head_row.terms terms,
                    I_paytermrec.terms_code terms_code,
                    I_paytermrec.terms_desc terms_desc
               FROM dual) tbl
         ON (th.terms = tbl.terms)
       WHEN MATCHED THEN
          UPDATE SET
             th.terms_code = tbl.terms_code,
             th.terms_desc = tbl.terms_desc,
             th.last_update_id = GET_USER,
             th.last_update_datetime = SYSDATE
           WHERE th.lang = GET_PRIMARY_LANG
       WHEN NOT MATCHED THEN
          INSERT(terms,
                 terms_code,
                 terms_desc,
                 lang,
                 orig_lang_ind,
                 reviewed_ind,
                 create_id,
                 create_datetime,
                 last_update_id,
                 last_update_datetime)
          VALUES(tbl.terms,
                 tbl.terms_code,
                 tbl.terms_desc,
                 GET_PRIMARY_LANG,
                 'Y',
                 'N',
                 GET_USER,
                 SYSDATE,
                 GET_USER,
                 SYSDATE);
                 
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END MERGE_HEADER;

-------------------------------------------------------------------------------
FUNCTION MERGE_DETAIL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      I_paytermrec    IN     PAYTERM_REC)
RETURN BOOLEAN IS

   L_program      VARCHAR2(50)  := 'TERMS_SQL.MERGE_DETAIL';

BEGIN

   if I_paytermrec.terms_details.COUNT > 0 then
      FOR dtl_ctr IN I_paytermrec.terms_details.FIRST .. I_paytermrec.terms_details.LAST LOOP
         MERGE INTO terms_detail td
            USING (SELECT I_paytermrec.terms_details(dtl_ctr).terms terms,
                          I_paytermrec.terms_details(dtl_ctr).terms_seq terms_seq,
                          I_paytermrec.terms_details(dtl_ctr).duedays duedays,
                          I_paytermrec.terms_details(dtl_ctr).due_max_amount due_max_amount,
                          I_paytermrec.terms_details(dtl_ctr).due_dom due_dom,
                          I_paytermrec.terms_details(dtl_ctr).due_mm_fwd due_mm_fwd,
                          I_paytermrec.terms_details(dtl_ctr).discdays discdays,
                          I_paytermrec.terms_details(dtl_ctr).percent percent,
                          I_paytermrec.terms_details(dtl_ctr).disc_dom disc_dom,
                          I_paytermrec.terms_details(dtl_ctr).disc_mm_fwd disc_mm_fwd,
                          I_paytermrec.terms_details(dtl_ctr).fixed_date fixed_date,
                          I_paytermrec.terms_details(dtl_ctr).enabled_flag enabled_flag,
                          I_paytermrec.terms_details(dtl_ctr).start_date_active start_date_active,
                          I_paytermrec.terms_details(dtl_ctr).end_date_active end_date_active,
                          I_paytermrec.terms_details(dtl_ctr).cutoff_day cutoff_day
                     FROM dual) tbl
               ON (td.terms = tbl.terms AND td.terms_seq = tbl.terms_seq)
             WHEN MATCHED THEN
                UPDATE SET td.duedays = tbl.duedays,
                           td.due_max_amount = tbl.due_max_amount,
                           td.due_dom = tbl.due_dom,
                           td.due_mm_fwd = tbl.due_mm_fwd,
                           td.discdays = tbl.discdays,
                           td.percent = tbl.percent,
                           td.disc_dom = tbl.disc_dom,
                           td.disc_mm_fwd = tbl.disc_mm_fwd,
                           td.fixed_date = tbl.fixed_date,
                           td.enabled_flag = tbl.enabled_flag,
                           td.start_date_active = tbl.start_date_active,
                           td.end_date_active = tbl.end_date_active,
                           td.cutoff_day = tbl.cutoff_day
             WHEN NOT MATCHED THEN
                INSERT(terms,
                       terms_seq,
                       duedays,
                       due_max_amount,
                       due_dom,
                       due_mm_fwd,
                       discdays,
                       percent,
                       disc_dom,
                       disc_mm_fwd,
                       fixed_date,
                       enabled_flag,
                       start_date_active,
                       end_date_active,
                       cutoff_day)
                VALUES(tbl.terms,
                       tbl.terms_seq,
                       tbl.duedays,
                       tbl.due_max_amount,
                       tbl.due_dom,
                       tbl.due_mm_fwd,
                       tbl.discdays,
                       tbl.percent,
                       tbl.disc_dom,
                       tbl.disc_mm_fwd,
                       tbl.fixed_date,
                       tbl.enabled_flag,
                       tbl.start_date_active,
                       tbl.end_date_active,
                       tbl.cutoff_day);
      END LOOP;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END MERGE_DETAIL;

--------------------------------------------------------------------------------
FUNCTION GET_NEXT_TERMS_VAL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                            O_terms          IN OUT  TERMS_HEAD.TERMS%TYPE)
RETURN BOOLEAN IS

   L_program                VARCHAR2(50)           := 'TERMS_SQL.GET_NEXT_TERMS_VAL';
   L_terms                  TERMS_HEAD.TERMS%TYPE;
   L_exists                 VARCHAR2(1)            := 'N';
   L_first_time             VARCHAR2(3)            := 'Y';
   L_first_sequence_number  NUMBER(15);


   cursor C_NEXTVAL is
      select TO_CHAR(terms_seq.NEXTVAL)
        from sys.dual;

   cursor C_SEQ_EXISTS is
      select 'Y'
        from terms_head
       where terms = L_terms;

BEGIN

   open C_NEXTVAL;

   LOOP
      fetch C_NEXTVAL into O_terms;
      if L_first_time = 'Y' then
          L_first_sequence_number := O_terms;
          L_first_time := 'N';
      elsif O_terms = L_first_sequence_number then
         -- Notify user to contact DBA if sequence runs out of numbers.
         O_error_message:= SQL_LIB.CREATE_MSG('NO_SEQ_NUM_AVAILABLE', 'terms_sequence');
         return FALSE;
      end if;

      -- Does sequence number already exist?
      L_exists := 'N';
      open C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;

      if L_exists = 'N' then
         EXIT; -- exit loop
      end if;

   end LOOP;

   close C_NEXTVAL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message:= SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           L_program,
                                           to_char(SQLCODE));
      return FALSE;
END GET_NEXT_TERMS_VAL;

-------------------------------------------------------------------------------
END TERMS_SQL;

/
