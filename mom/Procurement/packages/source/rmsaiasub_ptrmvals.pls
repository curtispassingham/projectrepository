CREATE OR REPLACE PACKAGE RMSAIASUB_PAYTERM_VALIDATE AUTHID CURRENT_USER AS

---------------------------------------------------------------
FUNCTION CHECK_MESSAGE(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                       O_paytermref_rec IN OUT  "RIB_PayTermRef_REC",
                       O_payterm_rec    IN OUT  TERMS_SQL.PAYTERM_REC,
                       I_message        IN      "RIB_PayTermDesc_REC",
                       I_message_type   IN      VARCHAR2)
RETURN BOOLEAN;

---------------------------------------------------------------------
END RMSAIASUB_PAYTERM_VALIDATE;

/
