CREATE OR REPLACE PACKAGE ITEM_EXPENSE_SQL AUTHID CURRENT_USER AS
---------------------------------------------------------------------------------------------
-- Function Name: GET_NEXT_SEQ
-- Purpose      : Creates next sequence number for indicated item, item supplier and item expense type.
---------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_SEQ(O_error_message IN OUT  VARCHAR2,
                      O_seq_no        IN OUT  ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                      I_item          IN      ITEM_MASTER.ITEM%TYPE,
                      I_supplier      IN      SUPS.SUPPLIER%TYPE,
                      I_item_exp_type IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: BASE_EXP_CHANGED
-- Purpose      : Sets base expense indicators for given item, supplier, and item expense type to 
--                'N' then checks if base expense indicator of passed-in item_exp_seq record is 'Y' 
--                and updates record accordingly.
---------------------------------------------------------------------------------------------
FUNCTION BASE_EXP_CHANGED (O_error_message       IN OUT  VARCHAR2,
                           I_item                IN      ITEM_MASTER.ITEM%TYPE,
                           I_supplier            IN      SUPS.SUPPLIER%TYPE,
                           I_item_exp_type       IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                           I_item_exp_seq        IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                           I_base_exp_ind        IN      ITEM_EXP_HEAD.BASE_EXP_IND%TYPE,
                           I_origin_country_id   IN      ITEM_EXP_HEAD.ORIGIN_COUNTRY_ID%TYPE)
   return BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: EXP_HEAD_EXIST
-- Purpose      : Checks if the given item expense header record already exists on the 
--                item expense header table. Sets O_exists to TRUE if finds record.
---------------------------------------------------------------------------------------------
FUNCTION EXP_HEAD_EXIST(O_error_message     IN OUT  VARCHAR2,
                        O_exists            IN OUT  BOOLEAN,
                        I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                        I_item              IN      ITEM_MASTER.ITEM%TYPE,
                        I_supplier          IN      SUPS.SUPPLIER%TYPE,
                        I_zone_id           IN      COST_ZONE.ZONE_ID%TYPE,
                        I_zone_group_id     IN      COST_ZONE.ZONE_GROUP_ID%TYPE,
                        I_origin_country_id IN      COUNTRY.COUNTRY_ID%TYPE,
                        I_lading_port       IN      OUTLOC.OUTLOC_ID%TYPE,
                        I_discharge_port    IN      OUTLOC.OUTLOC_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: EXP_DETAILS_EXIST
-- Purpose      : Checks if any item expense detail records exist on the item expense detail 
--                table for a given item expense header record. Sets O_exists to TRUE if record found.
---------------------------------------------------------------------------------------------
FUNCTION EXP_DETAILS_EXIST(O_error_message     IN OUT  VARCHAR2,
                           O_exists            IN OUT  BOOLEAN,
                           I_item              IN      ITEM_MASTER.ITEM%TYPE,
                           I_supplier          IN      SUPS.SUPPLIER%TYPE,
                           I_item_exp_type     IN   ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                           I_item_exp_seq      IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                           I_comp_id           IN      ELC_COMP.COMP_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: LOCK_EXP_DETAILS
-- Purpose      : This function locks item expense detail records. The passed-in variables should be  
--                that of the item expense header record to be deleted/updated. 
---------------------------------------------------------------------------------------------
FUNCTION LOCK_EXP_DETAILS(O_error_message     IN OUT  VARCHAR2,
                          I_item              IN      ITEM_MASTER.ITEM%TYPE,
                          I_supplier          IN      SUPS.SUPPLIER%TYPE,
                          I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                          I_item_exp_seq      IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------
-- Function Name: DEL_EXP_DETAILS
-- Purpose      : This function deletes item expense detail records so that an item expense
--                header record may be deleted.  The passed-in variables should be that of the 
--                item expense header record to be deleted.  
---------------------------------------------------------------------------------------------
FUNCTION DEL_EXP_DETAILS(O_error_message     IN OUT  VARCHAR2,
                         I_item              IN      ITEM_MASTER.ITEM%TYPE,
                         I_supplier          IN      SUPS.SUPPLIER%TYPE,
                         I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                         I_item_exp_seq      IN      ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: CHECK_HEADER_NO_DETAILS
-- Purpose      : Checks for Item Expense Header records that do not have any associated Item 
--                Expense Detail records.  Sets O_exists to TRUE if records found with no detail records.
---------------------------------------------------------------------------------------------
FUNCTION CHECK_HEADER_NO_DETAILS(O_error_message     IN OUT  VARCHAR2,
                                 O_exists            IN OUT  BOOLEAN,
                                 I_item              IN      ITEM_MASTER.ITEM%TYPE,
                                 I_supplier          IN      SUPS.SUPPLIER%TYPE,
                                 I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function Name: DELETE_HEADER
-- Purpose      : Deletes Item Expense Header records that do not have any associated Item 
--                Expense Detail records.
---------------------------------------------------------------------------------------------
FUNCTION DELETE_HEADER(O_error_message     IN OUT  VARCHAR2,
                       I_item              IN      ITEM_MASTER.ITEM%TYPE,
                       I_supplier          IN      SUPS.SUPPLIER%TYPE,
                       I_item_exp_type     IN      ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: DEFAULT_EXPENSES
-- Purpose : Inserts Expense Profiles and Always Default expenses in the the Item Expense
--           tables when a Supplier is attached to an Item and when an Origin Country
--           is attached to an Item/Supplier combination.
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_EXPENSES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_supplier          IN     SUPS.SUPPLIER%TYPE,
                          I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
FUNCTION DEFAULT_EXPENSES(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          I_item              IN     ITEM_MASTER.ITEM%TYPE,
                          I_supplier          IN     SUPS.SUPPLIER%TYPE,
                          I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                          I_cost_zone_group   IN      ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: INSERT_EXPENSES
-- Purpose : Performs the actual inserts into Item Expense Head and Item Expense
--           Detail.  Called by DEFAULT_EXPENSES.
---------------------------------------------------------------------------------------------
FUNCTION INSERT_EXPENSES(O_error_message     IN OUT VARCHAR2,
                         O_records_inserted  IN OUT VARCHAR2,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_supplier          IN     SUPS.SUPPLIER%TYPE,
                         I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                         I_zone_group_id     IN     COST_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                         I_module            IN     EXP_PROF_HEAD.MODULE%TYPE,
                         I_exp_type          IN     EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                         I_seq_no            IN     ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                         I_partner_type      IN     ITEM_EXP_DETAIL.KEY_VALUE_1%TYPE,
                         I_partner           IN     ITEM_EXP_DETAIL.KEY_VALUE_2%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: INSERT_ALWAYS_EXPENSES
-- Purpose : Performs the actual inserts of Always Default Expenses into Item Expense Detail
---------------------------------------------------------------------------------------------
FUNCTION INSERT_ALWAYS_EXPENSES(O_error_message     IN OUT VARCHAR2,
                                I_item              IN     ITEM_MASTER.ITEM%TYPE,
                                I_supplier          IN     SUPS.SUPPLIER%TYPE,
                                I_item_exp_type     IN     ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                                I_item_exp_seq      IN     ITEM_EXP_DETAIL.ITEM_EXP_SEQ%TYPE,
                                I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------
-- Function: DEFAULT_GROUP_EXP
-- Purpose : Loops through the tran level or above offspring for an item
--         : and calls default_expenses function for each item.
----------------------------------------------------------------------------------------------
FUNCTION DEFAULT_GROUP_EXP(O_error_message        IN OUT VARCHAR2,
                           I_item                 IN     ITEM_MASTER.ITEM%TYPE,
                           I_supplier             IN     SUPS.SUPPLIER%TYPE,
                           I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------
-- Function Name : COPY_DOWN_PARENT_EXP
-- Purpose       : Remove old ITEM_EXP_HEAD and ITEM_EXP_DETAIL values of all tran level
--               : or above offspring of passed the passed in item,
--               : get latest values for the item, and insert new ITEM_EXP_HEAD 
--               : ITEM_EXP_DETAIL values for the valid children and grandchildren.
-----------------------------------------------------------------------------------------
FUNCTION COPY_DOWN_PARENT_EXP(O_error_message        IN OUT VARCHAR2,
                              I_parent               IN     ITEM_MASTER.ITEM%TYPE,
                              I_supplier             IN     SUPS.SUPPLIER%TYPE,
                              I_origin_country_id    IN     COUNTRY.COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
FUNCTION GET_EXP_ORG_CTRY(O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_exp_ctry_exists   IN OUT   BOOLEAN,
                          I_exp_prof_type     IN       EXP_PROF_HEAD.EXP_PROF_TYPE%TYPE,
                          I_supplier          IN       SUPS.SUPPLIER%TYPE,
                          I_org_ctry_id       IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
-- Function Name : GET_ITEM_EXP
-- Purpose       : To check if an expense exists on a given item-supp-country combination
-----------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_EXP (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_item            IN       ITEM_MASTER.ITEM%TYPE,
                       I_supplier        IN       SUPS.SUPPLIER%TYPE,
                       I_origin_country  IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                       O_count           IN OUT   NUMBER)
   RETURN BOOLEAN;
-----------------------------------------------------------------------------------------------
END;
/
