CREATE OR REPLACE PACKAGE BODY OTB_SQL AS

   ----------------------------------------------------------------
   --- These variables were declared in the spec file and
   --- are populated in ORD_INFO.
   -- LP_order_type      ordhead.order_type%TYPE;
   -- LP_order_eow_date  DATE;
   -- LP_period_ahead    NUMBER(10);
   -- LP_week_no         otb.week_no%TYPE;
   -- LP_month_no        otb.month_no%TYPE;
   -- LP_half_no         otb.half_no%TYPE;
   -- LP_supplier        ordhead.supplier%TYPE;
   -- LP_order_no        ordhead.order_no%TYPE;
   -- LP_item            item_master.item%TYPE;
   ----------------------------------------------------------------

   --- ORDER receiving BULK
   TYPE amt_TBL       is table of otb.n_budget_amT%TYPE  INDEX BY BINARY_INTEGER;
   TYPE otb_rowid_TBL is table of ROWID                  INDEX BY BINARY_INTEGER;
   TYPE dept_TBL      is table of deps.dept%TYPE         INDEX BY BINARY_INTEGER;
   TYPE class_TBL     is table of class.class%TYPE       INDEX BY BINARY_INTEGER;
   TYPE subclass_TBL  is table of subclass.subclass%TYPE INDEX BY BINARY_INTEGER;
   TYPE eow_date_TBL  is table of otb.eow_date%TYPE      INDEX BY BINARY_INTEGER;
   ---
   --- ORDER approve BULK
   LP_a_approved_amt_tbl_1    amt_TBL;
   LP_b_approved_amt_tbl_1    amt_TBL;
   LP_n_approved_amt_tbl_1    amt_TBL;
   LP_dept_tbl_1              dept_TBL;
   LP_class_tbl_1             class_TBL;
   LP_subclass_tbl_1          subclass_TBL;
   LP_eow_date_tbl_1          eow_date_TBL;
   LP_otb_size_1              NUMBER := 0;
   ---
   --- ORDER unapprove BULK
   LP_a_approved_amt_tbl_2    amt_TBL;
   LP_b_approved_amt_tbl_2    amt_TBL;
   LP_n_approved_amt_tbl_2    amt_TBL;
   LP_dept_tbl_2              dept_TBL;
   LP_class_tbl_2             class_TBL;
   LP_subclass_tbl_2          subclass_TBL;
   LP_eow_date_tbl_2          eow_date_TBL;
   LP_otb_size_2              NUMBER := 0;
   ---
   --- ORDER cancel/reinstate BULK
   LP_a_approved_amt_tbl_3    amt_TBL;
   LP_b_approved_amt_tbl_3    amt_TBL;
   LP_n_approved_amt_tbl_3    amt_TBL;
   LP_cancel_amt_tbl_3        amt_TBL;
   LP_dept_tbl_3              dept_TBL;
   LP_class_tbl_3             class_TBL;
   LP_subclass_tbl_3          subclass_TBL;
   LP_eow_date_tbl_3          eow_date_TBL;
   LP_otb_size_3              NUMBER := 0;
   ---
   P_nb_rcv_amt     amt_TBL;
   P_nb_appv_amt    amt_TBL;
   P_arb_rcv_amt    amt_TBL;
   P_arb_appv_amt   amt_TBL;
   P_brb_rcv_amt    amt_TBL;
   P_brb_appv_amt   amt_TBL;
   P_otb_rowid      otb_rowid_TBL;
   P_otb_size NUMBER := 0;

   LP_contract_flag   VARCHAR2(1);
   LP_order_status    ORDHEAD.STATUS%TYPE;
   LP_oldpack         ITEM_SUPPLIER.ITEM%TYPE                  := 0;
   LP_sum_comp_cost   PRICE_HIST.UNIT_COST%TYPE                := 0;
   LP_comp_sum_qty    PACKITEM_BREAKOUT.ITEM_QTY%TYPE          := 0;
   LP_elc_ind         SYSTEM_OPTIONS.ELC_IND%TYPE;
   -- Record locking  variables
   LP_dept            OTB.DEPT%TYPE;
   LP_class           OTB.CLASS%TYPE;
   LP_subclass        OTB.SUBCLASS%TYPE;
   LP_eow_date        OTB.EOW_DATE%TYPE;
   LP_origin_country  ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE;
   LP_import_country  ORDHEAD.IMPORT_COUNTRY_ID%TYPE;
   LP_currency_code   ORDHEAD.CURRENCY_CODE%TYPE;
   LP_exchange_rate   ORDHEAD.EXCHANGE_RATE%TYPE;
   LP_pack_type       ITEM_MASTER.PACK_TYPE%TYPE;
   RECORD_LOCKED      EXCEPTION;
   PRAGMA             EXCEPTION_INIT(RECORD_LOCKED, -54);

   -- Record locking cursor
   cursor CP_LOCK_OTB (dept_PARAM        OTB.DEPT%TYPE,
                       class_PARAM       OTB.CLASS%TYPE,
                       subclass_PARAM    OTB.SUBCLASS%TYPE,
                       eow_date_PARAM    OTB.EOW_DATE%TYPE) is
      select rowid
        from otb
       where dept         = dept_PARAM
         and class        = class_PARAM
         and subclass     = subclass_PARAM
         and eow_date     = eow_date_PARAM
         for update nowait;

   cursor CP_SUM_COMP_COST(pack_no_PARAM ordsku.item%TYPE) is
      select sum(its.unit_cost * vpq.qty)
        from item_supp_country its,
             v_packsku_qty vpq
       where vpq.pack_no           = pack_no_PARAM
         and its.origin_country_id = LP_origin_country
         and its.supplier          = LP_supplier
         and its.item              = vpq.item;

   cursor CP_SUM_COMP_COST_ICH(pack_no_PARAM ordsku.item%TYPE) is
      select sum(decode(ca.default_po_cost, 'BC', ich.base_cost * vpq.qty,
                                                  ich.negotiated_item_cost * vpq.qty))
        from item_cost_head ich,
             v_packsku_qty vpq,
             country_attrib ca
       where vpq.pack_no           = pack_no_PARAM
         and ich.origin_country_id = LP_origin_country
         and ich.supplier          = LP_supplier
         and ich.item              = vpq.item
         and ca.country_id         = ich.origin_country_id
         and prim_dlvy_ctry_ind = 'Y';

   cursor CP_COMP_UNIT_COST(item_PARAM ordsku.item%TYPE) is
      select unit_cost
        from item_supp_country
       where supplier          = LP_supplier
         and origin_country_id = LP_origin_country
         and item              = item_PARAM;

   cursor CP_COMP_UNIT_COST_ICH(item_PARAM ordsku.item%TYPE) is
      select decode(ca.default_po_cost, 'BC', ich.base_cost, ich.negotiated_item_cost)
        from item_cost_head ich,
             country_attrib ca
       where ich.supplier           = LP_supplier
         and ich.origin_country_id  = LP_origin_country
         and ich.item               = item_PARAM
         and ca.country_id          = ich.origin_country_id
         and ich.prim_dlvy_ctry_ind = 'Y';

   cursor CP_ORDER_APPR is
      select os.item item,
             os.origin_country_id origin_country,
             to_char(NULL) comp_item,
             to_number(NULL) comp_qty,
             im.dept dept,
             im.class class,
             im.subclass subclass
        from ordsku os,
             item_master im
       where os.order_no = OTB_SQL.LP_order_no
         and os.item     = im.item
         and im.pack_ind = 'N'
      UNION ALL
      select os.item item,
             os.origin_country_id origin_country,
             ps.item comp_item,
             ps.pack_qty comp_qty,
             im.dept dept,
             im.class class,
             im.subclass subclass
        from ordsku os,
             item_master im,
             packitem ps
       where os.order_no  = OTB_SQL.LP_order_no
         and os.item      = ps.pack_no
         and (im.item     = ps.item or im.item = ps.item_parent)
    ORDER BY 5, 6, 7; --im.dept, im.class, im.subclass;

    cursor CP_OTB_CALC_TYPE is
       select otb_calc_type
         from deps
        where dept = LP_dept;

----------------------------------------------------------------------------
FUNCTION CALC_COST_RETAIL(O_error_message   IN OUT VARCHAR2,
                          I_order_no        IN     ORDSKU.ORDER_NO%TYPE,
                          I_item            IN     ORDSKU.ITEM%TYPE,
                          I_origin_country  IN     ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                          I_comp_item       IN     PACKITEM.ITEM%TYPE,
                          I_comp_qty        IN     PACKITEM_BREAKOUT.ITEM_QTY%TYPE,
                          I_otb_calc_type   IN     DEPS.OTB_CALC_TYPE%TYPE,
                          O_total           IN OUT NUMBER)
         RETURN BOOLEAN IS

   L_qty_ordered       ORDLOC.QTY_ORDERED%TYPE                 := NULL;
   L_qty_received      ORDLOC.QTY_RECEIVED%TYPE                := NULL;
   L_location          ORDLOC.LOCATION%TYPE                    := NULL;
   L_loc_type          ORDLOC.LOC_TYPE%TYPE                    := NULL;
   L_old_loc           ORDLOC.LOCATION%TYPE                    := NULL;
   L_comp_cost         PRICE_HIST.UNIT_COST%TYPE               := NULL;
   L_unit_retail       ORDLOC.UNIT_RETAIL%TYPE                 := NULL;
   L_supplier          ITEM_SUPPLIER.SUPPLIER%TYPE             := NULL;
   L_subtotal          OTB.N_BUDGET_AMT%TYPE                   := 0;
   L_pack_type         ITEM_MASTER.PACK_TYPE%TYPE              := NULL;
   L_duty_ord          ORDLOC.UNIT_COST%TYPE                   := 0;
   L_expenses_ord      ORDLOC.UNIT_COST%TYPE                   := 0;
   L_program           VARCHAR2(60) := 'OTB_SQL.CALC_COST_RETAIL';
   --- used in call to itemloc_attrib_sql.get_costs_and_retails
   L_store             STORE.STORE%TYPE                        := NULL;
   L_wh                WH.WH%TYPE                              := NULL;
   L_dummy_cost        ORDLOC.UNIT_COST%TYPE                   := NULL;
   L_pack_ind          ITEM_MASTER.PACK_IND%TYPE;
   ---
   L_comp_unit_cost       NUMBER;
   L_exp_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_dty_currency         CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp    CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_sellable_ind         ITEM_MASTER.SELLABLE_IND%TYPE        := NULL;
   L_orderable_ind        ITEM_MASTER.ORDERABLE_IND%TYPE       := NULL;
   L_avg_cost_new         ITEM_LOC_SOH.AV_COST%TYPE            := NULL;
   L_unit_cost            ITEM_LOC_SOH.UNIT_COST%TYPE          := NULL;
   L_selling_unit_retail  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE    := NULL;
   L_selling_uom          ITEM_LOC.SELLING_UOM%TYPE            := NULL;
   L_system_options_rec   SYSTEM_OPTIONS%ROWTYPE;
   ---
   cursor C_ORDLOC is
      select qty_ordered,
             NVL(qty_received,0) qty_received,
             unit_cost,
             location,
             loc_type,
             unit_retail
        from ordloc
       where item     = I_item
         and order_no = I_order_no;

BEGIN
   O_total := 0;

   -- get the system_options
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;

   -- if I_item is a pack, and isn't the same pack as before,
   -- get the sum cost of its components
   if (I_comp_item is not NULL) then
      if ITEM_ATTRIB_SQL.GET_PACK_INDS (O_error_message,
                                        L_pack_ind,
                                        L_sellable_ind,
                                        L_orderable_ind,
                                        L_pack_type,
                                        I_item) = FALSE then
         return FALSE;
      end if;

      if (L_pack_ind != 'Y') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAK', 'Pack No ='||(I_item), NULL, NULL);
         return FALSE;
      end if;

      if L_system_options_rec.default_tax_type in ('SVAT','SALES') then
         SQL_LIB.SET_MARK('OPEN',
                          'CP_SUM_COMP_COST',
                          'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                          'Pack No = '||(I_item));
         open CP_SUM_COMP_COST(I_item);
         SQL_LIB.SET_MARK('FETCH',
                          'CP_SUM_COMP_COST',
                          'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                          'Pack No = '||(I_item));
         fetch CP_SUM_COMP_COST into LP_sum_comp_cost;
         if CP_SUM_COMP_COST%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_PAK', 'Pack No ='||(I_item), NULL, NULL);
            SQL_LIB.SET_MARK('CLOSE',
                             'CP_SUM_COMP_COST',
                             'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                             'PACK_NO = '||(I_item));
            close CP_SUM_COMP_COST;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'CP_SUM_COMP_COST',
                          'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY',
                          'PACK_NO = '||(I_item));
         close CP_SUM_COMP_COST;
      elsif L_system_options_rec.default_tax_type = 'GTAX' then 
         SQL_LIB.SET_MARK('OPEN',
                          'CP_SUM_COMP_COST_ICH',
                          'ITEM_COST_HEAD, V_PACKSKU_QTY',
                          'Pack No = '||(I_item));
         open CP_SUM_COMP_COST_ICH(I_item);
         SQL_LIB.SET_MARK('FETCH',
                          'CP_SUM_COMP_COST_ICH',
                          'ITEM_COST_HEAD, V_PACKSKU_QTY',
                          'Pack No = '||(I_item));
         fetch CP_SUM_COMP_COST_ICH into LP_sum_comp_cost;
         if CP_SUM_COMP_COST_ICH%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_PAK', 'Pack No ='||(I_item), NULL, NULL);
            SQL_LIB.SET_MARK('CLOSE',
                             'CP_SUM_COMP_COST_ICH',
                             'ITEM_COST_HEAD, V_PACKSKU_QTY',
                             'PACK_NO = '||(I_item));
            close CP_SUM_COMP_COST_ICH;
            return FALSE;
         end if;
         SQL_LIB.SET_MARK('CLOSE',
                          'CP_SUM_COMP_COST',
                          'ITEM_COST_HEAD, V_PACKSKU_QTY',
                          'PACK_NO = '||(I_item));
         close CP_SUM_COMP_COST_ICH;
     end if;

      LP_oldpack := I_item;
   end if;

   FOR ordloc_rec in C_ORDLOC LOOP
      L_qty_ordered  := ordloc_rec.qty_ordered;
      L_qty_received := ordloc_rec.qty_received;
      L_unit_cost    := ordloc_rec.unit_cost;
      L_location     := ordloc_rec.location;
      L_loc_type     := ordloc_rec.loc_type;
      L_unit_retail  := ordloc_rec.unit_retail;

      if I_comp_item is not NULL then -- item is a pack component
         if I_otb_calc_type = 'C' then
            if L_system_options_rec.default_tax_type in ('SVAT', 'SALES') then
               SQL_LIB.SET_MARK('OPEN',
                                'CP_COMP_UNIT_COST',
                                'ITEM_SUPP_COUNTRY',
                                NULL);
               open CP_COMP_UNIT_COST(I_comp_item);
               SQL_LIB.SET_MARK('FETCH',
                                'CP_COMP_UNIT_COST',
                                'ITEM_SUPP_COUNTRY',
                                NULL);
               fetch CP_COMP_UNIT_COST into L_comp_cost;
               if CP_COMP_UNIT_COST%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('INV_SKU_PACKITEM',
                                                        (I_comp_item),
                                                        NULL,
                                                        NULL);
                  SQL_LIB.SET_MARK('CLOSE',
                                   'CP_COMP_UNIT_COST',
                                   'ITEM_SUPP_COUNTRY',
                                    NULL);
                  close CP_COMP_UNIT_COST;
                  return FALSE;
               end if;
               SQL_LIB.SET_MARK('CLOSE',
                                'CP_COMP_UNIT_COST',
                                'ITEM_SUPP_COUNTRY',
                                NULL);
               close CP_COMP_UNIT_COST;
            elsif L_system_options_rec.default_tax_type = 'GTAX' then
               SQL_LIB.SET_MARK('OPEN',
                                'CP_COMP_UNIT_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               open CP_COMP_UNIT_COST_ICH(I_comp_item);
               SQL_LIB.SET_MARK('FETCH',
                                'CP_COMP_UNIT_COST_ICH',
                                'ITEM_COST_HEAD',
                                NULL);
               fetch CP_COMP_UNIT_COST_ICH into L_comp_cost;
               if CP_COMP_UNIT_COST_ICH%NOTFOUND then
                  O_error_message := SQL_LIB.CREATE_MSG('ITEM_COST_HEAD',
                                                        (I_comp_item),
                                                        NULL,
                                                        NULL);
                  SQL_LIB.SET_MARK('CLOSE',
                                   'CP_COMP_UNIT_COST_ICH',
                                   'ITEM_SUPP_COUNTRY',
                                    NULL);
                  close CP_COMP_UNIT_COST_ICH;
                  return FALSE;
               end if;
               SQL_LIB.SET_MARK('CLOSE',
                                'CP_COMP_UNIT_COST_ICH',
                                'ITEM_SUPP_COUNTRY',
                                NULL);
               close CP_COMP_UNIT_COST_ICH;
            end if;

            if LP_sum_comp_cost = 0 then
               L_comp_unit_cost := 0;
            else
               L_comp_unit_cost := L_unit_cost * (L_comp_cost / LP_sum_comp_cost);
            end if;

            if LP_elc_ind  = 'Y' then
               if L_pack_type = 'B' then
                  -- retrieve the landed cost in primary currency for the component item
                  if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                                  L_subtotal,   -- total landed cost
                                                  L_expenses_ord,
                                                  L_exp_currency,
                                                  L_exchange_rate_exp,
                                                  L_duty_ord,
                                                  L_dty_currency,
                                                  I_order_no,
                                                  I_item,
                                                  I_comp_item,
                                                  NULL,            --zone_id
                                                  L_location,
                                                  LP_supplier,
                                                  I_origin_country,
                                                  LP_import_country,
                                                  L_unit_cost) then
                     return FALSE;
                  end if;
               else -- L_pack_type = 'V'
                  -- retrieve the landed cost in primary currency for the vendor pack
                  if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                                  L_subtotal,     -- total landed cost
                                                  L_expenses_ord,
                                                  L_exp_currency,
                                                  L_exchange_rate_exp,
                                                  L_duty_ord,
                                                  L_dty_currency,
                                                  I_order_no,
                                                  I_item,
                                                  NULL,            --comp_item,
                                                  NULL,            --zone_id
                                                  L_location,
                                                  LP_supplier,
                                                  I_origin_country,
                                                  LP_import_country,
                                                  L_unit_cost) then
                     return FALSE;
                  end if;
                  -- prorate landed cost of vendor pack for component item
                  if LP_sum_comp_cost = 0 then
                     L_subtotal := 0;
                  else
                     L_subtotal := L_subtotal *
                                    (L_comp_cost / LP_sum_comp_cost);
                  end if;
               end if; --- L_pack_type
            else -- LP_elc_ind = 'N'
               -- convert the order cost of the component item to primary currency
               if not CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                       I_order_no,
                                                       'O',
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       L_unit_cost,
                                                       L_subtotal,    -- order cost in primary
                                                       'N',
                                                       NULL,
                                                       NULL) then
                  return FALSE;
               end if;
            end if; -- LP_elc_ind = Y/N

            L_subtotal := L_subtotal * (I_comp_qty * (L_qty_ordered - L_qty_received));

         else -- otb_calc_type = R

            if (L_loc_type = 'S') then
               L_store := L_location;
               L_wh    := -1;
            else
               L_wh       := L_location;
               L_store    := -1;
            end if;

            if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                        I_comp_item,
                                                        L_location,
                                                        L_loc_type,
                                                        L_avg_cost_new,
                                                        L_unit_cost,
                                                        L_unit_retail,
                                                        L_selling_unit_retail,
                                                        L_selling_uom) = FALSE then
               return FALSE;
            end if;

            -- convert the unit retail of the item to primary currency
            if not CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                    L_location,
                                                    L_loc_type,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    L_unit_retail,
                                                    L_subtotal,   -- unit retail in prim. curr.
                                                    'N',
                                                    NULL,
                                                    NULL) then
               return FALSE;
            end if;
            L_subtotal := L_subtotal * (I_comp_qty * (L_qty_ordered - L_qty_received));
         end if;

      else -- I_comp_item is null, item is a staple/fashion item
         if I_otb_calc_type  = 'C' then
            if LP_elc_ind = 'Y' then
               -- retrieve the landed cost for the item in primary currency
               if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                               L_subtotal,     --item's landed cost
                                               L_expenses_ord,
                                               L_exp_currency,
                                               L_exchange_rate_exp,
                                               L_duty_ord,
                                               L_dty_currency,
                                               I_order_no,
                                               I_item,
                                               NULL,           -- comp_item
                                               NULL,            --zone_id
                                               L_location,
                                               LP_supplier,
                                               I_origin_country,
                                               LP_import_country,
                                               L_unit_cost) then
                  return FALSE;
               end if;
            else -- LP_elc_ind = 'N'
               -- convert the order_cost of the item to primary_currency
               if not CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                       I_order_no,
                                                       'O',
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       NULL,
                                                       L_unit_cost,
                                                       L_subtotal,
                                                       'N',
                                                       NULL,
                                                       NULL) then
                  return FALSE;
               end if;
            end if; -- LP_elc_ind = Y/N

            L_subtotal := L_subtotal * (L_qty_ordered - L_qty_received);

         else -- otb_calc_type = R
            if not CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                    L_location,
                                                    L_loc_type,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    L_unit_retail,
                                                    L_subtotal,   -- unit retail in prim. curr.
                                                    'N',
                                                    NULL,
                                                    NULL) then
               return FALSE;
            end if;
            L_subtotal := L_subtotal * (L_qty_ordered - L_qty_received);
         end if; -- otb_calc_type = C/R
      end if; -- I_comp_item is/not null
      O_total := O_total + L_subtotal;

   END LOOP; -- ordloc_rec in c_ordloc
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM, L_program,
                                            to_char(SQLCODE));
      return FALSE;

END; -- CALC_COST_RETAIL
-------------------------------------------------------------------------------
FUNCTION GET_PURGE_DATE (O_purge_eow          IN OUT  DATE,
                         O_error_message      IN OUT  VARCHAR2)
                         RETURN BOOLEAN IS

   L_vdate            PERIOD.VDATE%TYPE        := GET_VDATE;
   L_in_dd            NUMBER(2)                := NULL;
   L_in_mm            NUMBER(2)                := NULL;
   L_in_yyyy          NUMBER(4)                := NULL;
   L_out_dd           NUMBER(2)                := NULL;
   L_out_mm           NUMBER(2)                := NULL;
   L_out_yyyy         NUMBER(4)                := NULL;
   L_half             NUMBER(5)                := NULL;
   L_month            NUMBER(2)                := NULL;

   L_return_code      VARCHAR2(5)              := 'TRUE';
   L_error_msg        VARCHAR2(255)            := NULL;
   L_function         VARCHAR2(20)             := NULL;
   L_program          VARCHAR2(50)             := 'OTB_SQL.GET_PURGE_DATE';
   DATE_FAILED        EXCEPTION;

BEGIN

   L_in_dd   := TO_NUMBER(TO_CHAR(L_vdate,'DD'));
   L_in_mm   := TO_NUMBER(TO_CHAR(L_vdate,'MM'));
   L_in_yyyy := TO_NUMBER(TO_CHAR(L_vdate,'YYYY'));

   CAL_TO_454_HALF (L_in_dd, L_in_mm, L_in_yyyy,
                    L_half, L_month,
                    L_return_code, L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454_HALF';
      raise DATE_FAILED;
   end if;

   L_half := L_half - 10;

   HALF_TO_454_LDOH (L_half,
                     L_out_dd, L_out_mm, L_out_yyyy,
                     L_return_code, L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454_LDOH';
      raise DATE_FAILED;
   end if;

   O_purge_eow := TO_DATE(TO_CHAR(L_out_dd)||'-'||
                  TO_CHAR(L_out_mm)||'-'||
                  TO_CHAR(L_out_yyyy),'DD-MM-YYYY');

   return TRUE;

EXCEPTION

   when DATE_FAILED then
      O_error_message := SQL_LIB.CREATE_MSG('STKLEDGR_DATE',
         L_function, L_program, NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_PURGE_DATE;
-----------------------------------------------------------------------------
FUNCTION OTB_INSERTS (I_order_type      IN        VARCHAR2,
                      I_dept            IN        NUMBER,
                      I_class           IN        NUMBER,
                      I_subclass        IN        NUMBER,
                      I_eow_date        IN        DATE,
                      I_budget_amt      IN        NUMBER,
                      O_error_message   IN OUT    VARCHAR2)
                      RETURN BOOLEAN IS

   L_in_dd              NUMBER(2)               := NULL;
   L_in_mm              NUMBER(2)               := NULL;
   L_in_yyyy            NUMBER(4)               := NULL;
   L_out_dd             NUMBER(2)               := NULL;
   L_week               NUMBER(2)               := NULL;
   L_out_mm             NUMBER(2)               := NULL;
   L_out_yyyy           NUMBER(4)               := NULL;
   L_half               NUMBER(5)               := NULL;
   L_month              NUMBER(2)               := NULL;

   L_return_code        VARCHAR2(5)             := 'TRUE';
   L_error_msg          VARCHAR2(255)           := NULL;
   L_function           VARCHAR2(20)            := NULL;
   L_otb_ind            SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;
   L_program            VARCHAR2(50)            := 'OTB_SQL.OTB_INSERTS';
   DATE_FAILED          EXCEPTION;

BEGIN
     if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                              L_otb_ind) = FALSE then
        return FALSE;
     end if;

     if L_otb_ind = 'N' then
     return TRUE;
     end if;
     
   L_in_dd   := TO_NUMBER(TO_CHAR(I_eow_date,'DD'));
   L_in_mm   := TO_NUMBER(TO_CHAR(I_eow_date,'MM'));
   L_in_yyyy := TO_NUMBER(TO_CHAR(I_eow_date,'YYYY'));

   CAL_TO_454_HALF (L_in_dd,
                    L_in_mm,
                    L_in_yyyy,
                    L_half,
                    L_month,
                    L_return_code,
                    L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454_HALF';
      raise DATE_FAILED;
   end if;

   CAL_TO_454 (L_in_dd,
               L_in_mm,
               L_in_yyyy,
               L_out_dd,
               L_week,
               L_out_mm,
               L_out_yyyy,
               L_return_code,
               L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454';
      raise DATE_FAILED;
   end if;

   -- Record locking for update of otb table
   open CP_LOCK_OTB (I_dept,
                     I_class,
                     I_subclass,
                     I_eow_date);
   close CP_LOCK_OTB;
   --
   if I_order_type = 'A' then
      update otb
         set a_budget_amt = I_budget_amt
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass
         and eow_date = I_eow_date;
      if SQL%NOTFOUND then
         insert into otb values (I_dept,
                                 I_class,
                                 I_subclass,
                                 I_eow_date,
                                 L_week,
                                 L_month,
                                 L_half,
                                 0, 0, 0, 0,
                                 0, 0, 0,
                                 I_budget_amt, 0, 0);
      end if;
   elsif I_order_type = 'B' then
      update otb
         set b_budget_amt = I_budget_amt
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass
         and eow_date = I_eow_date;
      if SQL%NOTFOUND then
         insert into otb values (I_dept,
                                 I_class,
                                 I_subclass,
                                 I_eow_date,
                                 L_week,
                                 L_month,
                                 L_half,
                                 0, 0, 0, 0,
                                 I_budget_amt,
                                 0, 0,
                                 0, 0, 0);
      end if;
   elsif I_order_type = 'N' then
      update otb
         set n_budget_amt = I_budget_amt
       where dept = I_dept
         and class = I_class
         and subclass = I_subclass
         and eow_date = I_eow_date;
      if SQL%NOTFOUND then
         insert into otb values (I_dept,
                                 I_class,
                                 I_subclass,
                                 I_eow_date,
                                 L_week,
                                 L_month,
                                 L_half,
                                 0,
                                 I_budget_amt,
                                 0, 0,
                                 0, 0, 0,
                                 0, 0, 0);
      end if;
   end if;
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 'OTB',
                                            to_char(I_dept), to_char(I_class));
      return FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END OTB_INSERTS;
-----------------------------------------------------------------------------
FUNCTION ORD_INFO (I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                   O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_in_dd         NUMBER(2);
   L_in_mm         NUMBER(2);
   L_in_yyyy       NUMBER(4);
   L_out_dd        NUMBER(2);
   L_out_mm        NUMBER(2);
   L_out_yyyy      NUMBER(4);
   L_vdate         DATE          := GET_VDATE;
   L_current_eow_date      DATE;

   L_program       VARCHAR2(50)  := 'OTB_SQL.ORD_INFO';
   L_function      VARCHAR2(20)  := NULL;
   L_cursor        VARCHAR2(20)  := NULL;
   L_return_code   VARCHAR2(5)   := 'TRUE';
   L_error_msg     VARCHAR2(255) := NULL;
   DATE_FAILED     EXCEPTION;

   cursor C_variables is
      select otb_eow_date,
             order_type,
             supplier,
             status,
             import_country_id,
             currency_code,
             exchange_rate
        from ordhead
       where order_no = OTB_SQL.LP_order_no ;

BEGIN

   OTB_SQL.LP_order_no := I_order_no;
   SQL_LIB.SET_MARK('OPEN', 'C_variables', 'ordhead', NULL);
   open C_variables;
   SQL_LIB.SET_MARK('FETCH', 'C_variables', 'ordhead', NULL);
   fetch C_VARIABLES into OTB_SQL.LP_order_eow_date,
                          OTB_SQL.LP_order_type,
                          OTB_SQL.LP_supplier,
                          LP_order_status,
                          LP_import_country,
                          LP_currency_code,
                          LP_exchange_rate;
   SQL_LIB.SET_MARK('CLOSE', 'C_variables', 'ordhead', NULL);
   if C_variables%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_NO','C_VARIABLES',
                                            'ordhead',OTB_SQL.LP_order_no);
      close C_variables;
      return FALSE;
   end if;
   close C_variables;

   L_in_dd   := TO_NUMBER(TO_CHAR(OTB_SQL.LP_order_eow_date,'DD'),'09');
   L_in_mm   := TO_NUMBER(TO_CHAR(OTB_SQL.LP_order_eow_date,'MM'),'09');
   L_in_yyyy := TO_NUMBER(TO_CHAR(OTB_SQL.LP_order_eow_date,'YYYY'),'0999');

   /* calculate the week_no that the order's otb_eow_date is in */


   CAL_TO_454(L_in_dd,
              L_in_mm,
              L_in_yyyy,
              L_out_dd,
              OTB_SQL.LP_week_no,
              L_out_mm,
              L_out_yyyy,
              L_return_code,
              L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454';
      raise DATE_FAILED;
   end if;

   /* calculate the month_no and half_no that the order's otb_eow_date
      is in */

   CAL_TO_454_HALF(L_in_dd,
                   L_in_mm,
                   L_in_yyyy,
                   OTB_SQL.LP_half_no,
                   OTB_SQL.LP_month_no,
                   L_return_code,
                   L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454_HALF';
      raise DATE_FAILED;
   end if;

   /* calculate the period_ahead using the current end of week date */

   L_in_dd   := TO_NUMBER(TO_CHAR(L_vdate,'DD'),'09');
   L_in_mm   := TO_NUMBER(TO_CHAR(L_vdate,'MM'),'09');
   L_in_yyyy := TO_NUMBER(TO_CHAR(L_vdate,'YYYY'),'0999');

   CAL_TO_454_LDOW(L_in_dd,
                   L_in_mm,
                   L_in_yyyy,
                   L_out_dd,
                   L_out_mm,
                   L_out_yyyy,
                   L_return_code,
                   L_error_msg);
   if L_return_code = 'FALSE' then
      L_function := 'CAL_TO_454_LDOW';
      raise DATE_FAILED;
   end if;

   L_current_eow_date := TO_DATE(TO_CHAR(L_out_dd,'09')||TO_CHAR(L_out_mm,'09')
                                 ||TO_CHAR(L_out_yyyy,'0999'),'DDMMYYYY');

   OTB_SQL.LP_period_ahead := (OTB_SQL.LP_order_eow_date - L_current_eow_date) / 7;


   return TRUE;

EXCEPTION

   when DATE_FAILED then
      O_error_message := SQL_LIB.CREATE_MSG('STKLEDGR_DATE', L_function,
         L_program, NULL);
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_INFO;
-----------------------------------------------------------------------------
FUNCTION ORD_CHECK (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                    O_subclass_list   IN OUT   VARCHAR2,
                    O_excess_ind      IN OUT   VARCHAR2,
                    O_error_message   IN OUT   VARCHAR2)
                    RETURN BOOLEAN IS

   L_dept           DEPS.DEPT%TYPE;
   L_class          CLASS.CLASS%TYPE;
   L_subclass       SUBCLASS.SUBCLASS%TYPE;
   L_olddept        DEPS.DEPT%TYPE;
   L_oldclass       CLASS.CLASS%TYPE;
   L_oldsubclass    SUBCLASS.SUBCLASS%TYPE;
   L_subclass_ctr   NUMBER                           := 0;
   L_subclass_list  VARCHAR2(154);
   L_subtotal       OTB.N_BUDGET_AMT%TYPE            := 0;
   L_amount         OTB.N_BUDGET_AMT%TYPE            := 0;
   L_approved_amt   OTB.N_APPROVED_AMT%TYPE          := 0;
   L_fwd_limit      NUMBER(12,4) :=100;
   L_budget_amt     OTB.N_BUDGET_AMT%TYPE;
   L_purch_bud      OTB.N_BUDGET_AMT%TYPE;
   L_duty_expenses  ORDLOC.UNIT_COST%TYPE;
   L_program        VARCHAR2(50)                     := 'OTB_SQL.ORD_CHECK';
   L_cursor         VARCHAR2(20);
   L_otb_calc_type  DEPS.OTB_CALC_TYPE%TYPE;

   cursor C_ORDER is
      select decode(OTB_SQL.LP_order_type, 'ARB', a_budget_amt,
                                           'BRB', b_budget_amt,
                                           'N/B', n_budget_amt,
                                           -1),
             decode(OTB_SQL.LP_order_type, 'ARB', a_approved_amt,
                                           'BRB', b_approved_amt,
                                           'N/B', n_approved_amt,
                                           0)
        from otb
       where eow_date = OTB_SQL.LP_order_eow_date
         and dept     = L_olddept
         and class    = L_oldclass
         and subclass = L_oldsubclass;
/*
   cursor C_FWD_LIMIT is
      select o.fwd_limit_pct
        from otb_fwd_limit o
       where o.period_ahead = (select max(f.period_ahead)
                                 from otb_fwd_limit f
                                where f.period_ahead <= OTB_SQL.LP_period_ahead
                                  and f.subclass = o.subclass
                                  and f.class    = o.class
                                  and f.dept     = o.dept)
         and o.subclass     = L_oldsubclass
         and o.class        = L_oldclass
         and o.dept         = L_olddept;
*/
   ---------------------------------------------------------------------
   FUNCTION COMPARE_BUDGET
            RETURN BOOLEAN IS

   BEGIN
      /* get the subclass's budget for this order type from OTB */
      L_cursor := 'C_order';
      SQL_LIB.SET_MARK('OPEN', 'C_order', 'otb', NULL);
      open C_order;
      SQL_LIB.SET_MARK('FETCH', 'C_order', 'otb', NULL);
      fetch C_order into L_budget_amt, L_approved_amt;
      if C_order%NOTFOUND then
         L_purch_bud := 0;
      else
         if L_budget_amt = -1 then
            SQL_LIB.SET_MARK('CLOSE', 'C_order', 'otb', NULL);
            close C_order;
            O_error_message := SQL_LIB.CREATE_MSG('INV_ORDER_TYPE',
                                                  to_char(OTB_SQL.LP_order_no),
                                                  L_program, NULL);
            return FALSE;
         end if;
         /* get the subclass's fwd_limit_pct off otb_fwd_limit */
        /* L_cursor := 'C_fwd_limit';
         SQL_LIB.SET_MARK('OPEN', 'C_fwd_limit', 'otb_fwd_limit', NULL);
         open C_fwd_limit;
         SQL_LIB.SET_MARK('FETCH', 'C_fwd_limit', 'otb_fwd_limit', NULL);
         fetch C_fwd_limit into L_fwd_limit;
         if C_fwd_limit%NOTFOUND then
            L_fwd_limit := 100;
         end if;
         SQL_LIB.SET_MARK('CLOSE', 'C_fwd_limit', 'otb_fwd_limit', NULL);
         close C_fwd_limit;*/

         /* adjust the budget amt by the fwd_limit_pct to get the current
            purchase budget amount for that period */
         L_purch_bud := L_budget_amt * (L_fwd_limit / 100) - L_approved_amt;
      end if;
      SQL_LIB.SET_MARK('CLOSE', 'C_order', 'otb', NULL);
      close C_order;

      /* check to see if the current budget is less than the outstanding
         order amount for the subclass/period.  If it is, set an excess
         flag to 'TRUE' and add the subclass onto a list of subclasses
         that have outstanding order amounts greater than their purchase
         budgets.  9 is the maximum number of full dept/class/subclass
         listings that can be included in O_subclass_list without
         exceeding the error msg limit of 255 */
      if L_purch_bud < L_amount then
         O_excess_ind := 'TRUE';
         L_subclass_ctr := L_subclass_ctr + 1;
         if L_subclass_ctr < 9 then
            L_subclass_list := L_subclass_list||
                               TO_CHAR(L_oldsubclass)||'/'||
                               TO_CHAR(L_oldclass)||'/'||
                               TO_CHAR(L_olddept)||' ';
         elsif L_subclass_ctr < 10 then
            L_subclass_list := L_subclass_list||'and others . . .';
         end if;
      end if;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                               L_program, to_char(SQLCODE));
         return FALSE;

   END COMPARE_BUDGET;
   ---------------------------------------------------------------------
BEGIN -- ORD_CHECK
   /* populate spec variables */
   if OTB_SQL.ORD_INFO (I_order_no,
                        O_error_message) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.LP_order_type != 'DSD' then
      if not SYSTEM_OPTIONS_SQL.GET_ELC_IND(O_error_message,
                                            LP_elc_ind) then
         return FALSE;
      end if;

      O_excess_ind := 'FALSE';

      SQL_LIB.SET_MARK('LOOP','CP_ORDER_APPR',
                       'ordsku,desc_look,packitem',
                       'order_no = '||OTB_SQL.LP_order_no);
      FOR C_order_appr_rec IN CP_ORDER_APPR LOOP

         if CP_ORDER_APPR%ROWCOUNT = 1 then
            -- initialize L_olddept,L_oldclass,L_oldsubclass
            L_olddept     := C_order_appr_rec.dept;
            L_oldclass    := C_order_appr_rec.class;
            L_oldsubclass := C_order_appr_rec.subclass;
         end if;

         L_duty_expenses := 0;
         LP_origin_country := C_order_appr_rec.origin_country;
         LP_dept           := C_order_appr_rec.dept;
         L_class           := C_order_appr_rec.class;
         L_subclass        := C_order_appr_rec.subclass;

         SQL_LIB.SET_MARK('OPEN','CP_OTB_CALC_TYPE',NULL,NULL);
         open CP_OTB_CALC_TYPE;
         SQL_LIB.SET_MARK('FETCH','CP_OTB_CALC_TYPE',NULL,NULL);
         fetch CP_OTB_CALC_TYPE into L_otb_calc_type;
         SQL_LIB.SET_MARK('CLOSE','CP_OTB_CALC_TYPE',NULL,NULL);
         close CP_OTB_CALC_TYPE;

         if (LP_dept != L_olddept) or
            (L_class != L_oldclass) or
            (L_subclass != L_oldsubclass) then

            -- compare total to the subclass' budget and
            -- add the subclass to the error list if total is over
            if not COMPARE_BUDGET then
               return FALSE;
            end if;
            L_amount := 0;
            L_olddept := LP_dept;
            L_oldclass := L_class;
            L_oldsubclass := L_subclass;

         end if; -- dept/class/subclass change
         if not CALC_COST_RETAIL(O_error_message,
                                 I_order_no,
                                 C_order_appr_rec.item,
                                 C_order_appr_rec.origin_country,
                                 C_order_appr_rec.comp_item,
                                 C_order_appr_rec.comp_qty,
                                 L_otb_calc_type,
                                 L_subtotal) then
            return FALSE;
         end if;
         L_amount := L_amount + L_subtotal;
      END LOOP; --- c_order_appr_rec

      if not COMPARE_BUDGET then
         return FALSE;
      end if;

      O_subclass_list := SQL_LIB.CREATE_MSG('OTB_EXCEEDED',L_subclass_list,
         NULL,NULL);
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_CHECK;
-----------------------------------------------------------------------------
FUNCTION ORD_APPROVE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                      O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.ORD_APPROVE';
   L_type      VARCHAR2(30)   := 'APPROVE';
   L_otb_ind   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;

BEGIN -- ORD_APPROVE

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if OTB_SQL.INIT_ORD_BULK(O_error_message,
                            L_type,
                            0) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.BUILD_ORD_APPROVE (O_error_message,
                                 I_order_no) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.FLUSH_ORD_BULK(O_error_message,
                             L_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_APPROVE;
-----------------------------------------------------------------------------
FUNCTION ORD_UNAPPROVE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                        O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.ORD_UNAPPROVE';
   L_type      VARCHAR2(30)   := 'UNAPPROVE';
   L_otb_ind   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;

BEGIN -- ORD_UNAPPROVE

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if OTB_SQL.INIT_ORD_BULK(O_error_message,
                            L_type,
                            0) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.BUILD_ORD_UNAPPROVE (O_error_message,
                                   I_order_no) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.FLUSH_ORD_BULK(O_error_message,
                             L_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_UNAPPROVE;
-----------------------------------------------------------------------------
-- This function is called from the batch_ordcostcompupd.ksh to stage the updates
-- which needs to be done on OTB table after approving the order.
-----------------------------------------------------------------------------
FUNCTION ORD_APPROVE_CASCADE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                              O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.ORD_APPROVE_CASCADE';
   L_type      VARCHAR2(30)   := 'APPROVE';
   L_otb_ind   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;

BEGIN -- ORD_APPROVE

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if OTB_SQL.INIT_ORD_BULK(O_error_message,
                            L_type,
                            0) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.BUILD_ORD_APPROVE (O_error_message,
                                 I_order_no) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.FLUSH_ORD_BULK(O_error_message,
                             'APPROVE_CASCADE') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_APPROVE_CASCADE;
-----------------------------------------------------------------------------
-- This function is called from the batch_ordcostcompupd.ksh to stage the updates
-- which needs to be done on OTB table after unapproving the order.
------------------------------------------------------------------------------
FUNCTION ORD_UNAPPROVE_CASCADE (I_order_no        IN       ORDSKU.ORDER_NO%TYPE,
                                O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.ORD_UNAPPROVE_CASCADE';
   L_type      VARCHAR2(30)   := 'UNAPPROVE';
   L_otb_ind   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;

BEGIN -- ORD_UNAPPROVE

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if OTB_SQL.INIT_ORD_BULK(O_error_message,
                            L_type,
                            0) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.BUILD_ORD_UNAPPROVE (O_error_message,
                                   I_order_no) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.FLUSH_ORD_BULK(O_error_message,
                             'UNAPPROVE_CASCADE') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_UNAPPROVE_CASCADE;
-----------------------------------------------------------------------------
FUNCTION ORD_CANCEL_REINSTATE (O_error_message         IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_order_no              IN      ORDHEAD.ORDER_NO%TYPE,
                               I_item                  IN      ORDLOC.item%TYPE,
                               I_location              IN      ORDLOC.LOCATION%TYPE,
                               I_loc_type              IN      ORDLOC.LOC_TYPE%TYPE,
                               I_cancel_reinstate_qty  IN      ORDLOC.QTY_CANCELLED%TYPE,
                               I_cancel_reinstate_flag IN      VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.ORD_CANCEL_REINSTATE';
   L_type      VARCHAR2(30)   := 'CANCEL_REINSTATE';
   L_otb_ind   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;

BEGIN -- ORD_CANCEL_REINSTATE

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if OTB_SQL.INIT_ORD_BULK(O_error_message,
                            L_type,
                            0) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.BUILD_ORD_CANCEL_REINSTATE (O_error_message,
                                          I_order_no,
                                          I_item,
                                          I_location,
                                          I_loc_type,
                                          I_cancel_reinstate_qty,
                                          I_cancel_reinstate_flag) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.FLUSH_ORD_BULK(O_error_message,
                             L_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END ORD_CANCEL_REINSTATE;
------------------------------------------------------------------------------
FUNCTION ORD_RECEIVE(O_error_message   IN OUT VARCHAR2,
                     I_unit_retail     IN     ORDLOC.UNIT_RETAIL%TYPE,
                     I_unit_cost_duty  IN     ORDLOC.UNIT_COST%TYPE,
                     I_order_no        IN     ORDSKU.ORDER_NO%TYPE,
                     I_dept            IN     DEPS.DEPT%TYPE,
                     I_class           IN     CLASS.CLASS%TYPE,
                     I_subclass        IN     SUBCLASS.SUBCLASS%TYPE,
                     I_receipt_qty     IN     ORDLOC.QTY_RECEIVED%TYPE,
                     I_approved_qty    IN     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN IS
   L_otb_ind   SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;
BEGIN

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if INIT_ORD_RECEIVE(O_error_message) = FALSE then
      return FALSE;
   end if;
   if BUILD_ORD_RECEIVE(O_error_message,
                        I_unit_retail,
                        I_unit_cost_duty,
                        I_order_no,
                        I_dept,
                        I_class,
                        I_subclass,
                        I_receipt_qty,
                        I_approved_qty) = FALSE then
      return FALSE;
   end if;
   if FLUSH_ORD_RECEIVE(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'OTB_SQL.ORD_RECEIVE',
                                            to_char(SQLCODE));
      return FALSE;

END ORD_RECEIVE;
------------------------------------------------------------------------------
FUNCTION BUILD_ORD_RECEIVE(O_error_message   IN OUT VARCHAR2,
                           I_unit_retail     IN     ORDLOC.UNIT_RETAIL%TYPE,
                           I_unit_cost_duty  IN     ORDLOC.UNIT_COST%TYPE,
                           I_order_no        IN     ORDSKU.ORDER_NO%TYPE,
                           I_dept            IN     DEPS.DEPT%TYPE,
                           I_class           IN     CLASS.CLASS%TYPE,
                           I_subclass        IN     SUBCLASS.SUBCLASS%TYPE,
                           I_receipt_qty     IN     ORDLOC.QTY_RECEIVED%TYPE,
                           I_approved_qty    IN     ORDLOC.QTY_ORDERED%TYPE)
RETURN BOOLEAN IS

   cursor C_CALC_TYPE is
      select otb_calc_type
        from deps
       where dept = I_dept;

   L_calc_type   DEPS.OTB_CALC_TYPE%TYPE;
   L_amount      ORDLOC.UNIT_RETAIL%TYPE;
   L_rowid       ROWID := NULL;

BEGIN

   if OTB_SQL.ORD_INFO(I_order_no,
                       O_error_message) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.LP_order_type != 'DSD' then

      SQL_LIB.SET_MARK('OPEN', 'C_CALC_TYPE', 'DEPS', 'Dept: '||to_char(I_dept));
      open C_CALC_TYPE;
      SQL_LIB.SET_MARK('FETCH', 'C_CALC_TYPE', 'DEPS', 'Dept: '||to_char(I_dept));
      fetch C_CALC_TYPE into L_calc_type;
      SQL_LIB.SET_MARK('CLOSE', 'C_CALC_TYPE', 'DEPS', 'Dept: '||to_char(I_dept));
      close C_CALC_TYPE;

      if L_calc_type = 'R' then

         if ((I_unit_retail = 0) OR (I_unit_retail is NULL)) then
            return TRUE;
         end if;

         L_amount := I_unit_retail;
      else

         if ((I_unit_cost_duty = 0) OR (I_unit_cost_duty is NULL)) then
            return TRUE;
         end if;

         L_amount := I_unit_cost_duty;
      end if;

      open CP_LOCK_OTB(I_dept,
                       I_class,
                       I_subclass,
                       LP_order_eow_date);
      fetch CP_LOCK_OTB into L_rowid;
      close CP_LOCK_OTB;

      P_otb_size := P_otb_size + 1;
      P_otb_rowid(P_otb_size) := L_rowid;

      if LP_order_type = 'N/B' then

         P_nb_rcv_amt(P_otb_size)   := I_receipt_qty * L_amount;
         P_nb_appv_amt(P_otb_size)  := I_approved_qty * L_amount;
         P_arb_rcv_amt(P_otb_size)  := 0;
         P_arb_appv_amt(P_otb_size) := 0;
         P_brb_rcv_amt(P_otb_size)  := 0;
         P_brb_appv_amt(P_otb_size) := 0;

      elsif LP_order_type = 'ARB' then

         P_nb_rcv_amt(P_otb_size)   := 0;
         P_nb_appv_amt(P_otb_size)  := 0;
         P_arb_rcv_amt(P_otb_size)  := I_receipt_qty * L_amount;
         P_arb_appv_amt(P_otb_size) := I_approved_qty * L_amount;
         P_brb_rcv_amt(P_otb_size)  := 0;
         P_brb_appv_amt(P_otb_size) := 0;

      elsif LP_order_type = 'BRB' then

         P_nb_rcv_amt(P_otb_size)   := 0;
         P_nb_appv_amt(P_otb_size)  := 0;
         P_arb_rcv_amt(P_otb_size)  := 0;
         P_arb_appv_amt(P_otb_size) := 0;
         P_brb_rcv_amt(P_otb_size)  := I_receipt_qty * L_amount;
         P_brb_appv_amt(P_otb_size) := I_approved_qty * L_amount;

      elsif LP_order_type = 'CO' then

         P_nb_rcv_amt(P_otb_size)   := 0;
         P_nb_appv_amt(P_otb_size)  := 0;
         P_arb_rcv_amt(P_otb_size)  := 0;
         P_arb_appv_amt(P_otb_size) := 0;
         P_brb_rcv_amt(P_otb_size)  := 0;
         P_brb_appv_amt(P_otb_size) := 0;
      end if; -- order_type

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED','OTB',
                                            to_char(I_class),
                                            to_char(I_subclass));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'OTB_SQL.BUILD_ORD_RECEIVE',
                                            to_char(SQLCODE));
      return FALSE;

END BUILD_ORD_RECEIVE;
-----------------------------------------------------------------------------
FUNCTION FLUSH_ORD_RECEIVE(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS
  L_otb_ind            SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;
BEGIN

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if P_otb_size > 0 then
      SQL_LIB.SET_MARK('UPDATE', NULL, 'OTB', NULL);
      FORALL i IN 1..P_otb_size
         update otb
            set n_receipts_amt = n_receipts_amt + P_nb_rcv_amt(i),
                n_approved_amt = n_approved_amt + P_nb_appv_amt(i),
                a_receipts_amt = a_receipts_amt + P_arb_rcv_amt(i),
                a_approved_amt = a_approved_amt + P_arb_appv_amt(i),
                b_receipts_amt = b_receipts_amt + P_brb_rcv_amt(i),
                b_approved_amt = b_approved_amt + P_brb_appv_amt(i)
          where rowid = P_otb_rowid(i);
   end if;

   P_otb_size := 0;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'OTB_SQL.FLUSH_ORD_RECEIVE',
                                            to_char(SQLCODE));
      return FALSE;
END FLUSH_ORD_RECEIVE;
-----------------------------------------------------------------------------
FUNCTION INIT_ORD_RECEIVE(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   P_otb_size := 0;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            'OTB_SQL.INIT_ORD_RECEIVE',
                                            to_char(SQLCODE));
      return FALSE;
END INIT_ORD_RECEIVE;
-----------------------------------------------------------------------------
FUNCTION ITEM_COST(I_item              IN     ordloc.item%TYPE,
                   I_pack_no           IN     item_master.item%TYPE,
                   O_total_elc_rec     IN OUT ORDLOC.UNIT_COST%TYPE,
                   O_total_elc_can     IN OUT ORDLOC.UNIT_COST%TYPE,
                   O_total_elc_app     IN OUT ORDLOC.UNIT_COST%TYPE,
                   O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_qty_approved        ordloc.qty_ordered%TYPE        := NULL;
   L_qty_received        ordloc.qty_received%TYPE       := NULL;
   L_comp_unit_cost      ORDLOC.UNIT_COST%TYPE          := NULL;
   L_duty                ORDLOC.UNIT_COST%TYPE          := NULL;
   L_expenses            ORDLOC.UNIT_COST%TYPE          := NULL;
   L_qty_cancelled       ordloc.qty_cancelled%TYPE      := NULL;
   L_pack_sum            ORDLOC.UNIT_COST%TYPE          := NULL;
   L_total_elc_prim      NUMBER;
   L_exp_currency        elc_comp.comp_currency%TYPE;
   L_exchange_rate_exp   ordhead.exchange_rate%TYPE;
   L_duty_currency       elc_comp.comp_currency%TYPE;

   L_program             VARCHAR2(30)                   := 'OTB_SQL.ITEM_COST';

   cursor C_ORDLOC is
      select location,
             NVL(qty_cancelled,0) qty_cancelled,
             DECODE(qty_ordered, 0, NVL(qty_received,0), qty_ordered) qty_approved,
             NVL(qty_received, 0) qty_received
        from ordloc
       where order_no = OTB_SQL.LP_order_no
         and item = I_item;

   cursor C_ORDLOC_PACK is
      select (DECODE(ol.qty_ordered,0,NVL(ol.qty_received,0),
                     ol.qty_ordered) * vpq.qty) qty_approved,
             (NVL(ol.qty_received, 0) * vpq.qty) qty_received,
             (NVL(ol.qty_cancelled, 0) * vpq.qty) qty_cancelled,
             ol.location location,
             its.unit_cost
        from ordloc ol,
             ordsku os,
             item_supp_country_loc its,
             v_packsku_qty vpq,
             ordhead o,
             country_attrib ca
       where ol.order_no           = OTB_SQL.LP_order_no
         and ol.item               = I_pack_no
         and vpq.pack_no           = ol.item
         and vpq.item              = I_item
         and its.item              = vpq.item
         and its.supplier          = OTB_SQL.LP_supplier
         and os.origin_country_id  = its.origin_country_id
         and os.item               = ol.item
         and os.order_no           = ol.order_no
         and ol.location           = its.loc
         and o.order_no            = ol.order_no
         and o.import_country_id   = ca.country_id(+);
BEGIN
   O_total_elc_rec := 0;
   O_total_elc_can := 0;
   O_total_elc_app := 0;

   if I_pack_no is NULL then
      --- I_item is an individual staple/fashion item
      SQL_LIB.SET_MARK('LOOP','C_ORDLOC','ordloc, ordsku',
                       'order_no '||OTB_SQL.LP_order_no||'; item '||I_item);
      for c_ordloc_rec in C_ORDLOC LOOP
         L_qty_cancelled := c_ordloc_rec.qty_cancelled;
         L_qty_approved  := c_ordloc_rec.qty_approved;
         L_qty_received  := c_ordloc_rec.qty_received;
         ---
         if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                         L_total_elc_prim,
                                         L_expenses,
                                         L_exp_currency,
                                         L_exchange_rate_exp,
                                         L_duty,
                                         L_duty_currency,
                                         OTB_SQL.LP_order_no,
                                         I_item,
                                         NULL,
                                         NULL,
                                         c_ordloc_rec.location,
                                         OTB_SQL.LP_supplier,
                                         LP_origin_country,
                                         LP_import_country,
                                         NULL) then
            RETURN FALSE;
         end if;

         O_total_elc_rec := O_total_elc_rec + (L_total_elc_prim * L_qty_received);
         O_total_elc_can := O_total_elc_can + (L_total_elc_prim * L_qty_cancelled);
         O_total_elc_app := O_total_elc_app + (L_total_elc_prim * L_qty_approved);
      END LOOP; --- c_ordloc_rec

   else -- I_pack_no is not null
      --- I_item is a pack component

      SQL_LIB.SET_MARK('LOOP','C_ORDLOC_PACK','ordloc, v_packsku_qty',
                       'order_no '||OTB_SQL.LP_order_no||'; item '||I_item||
                       '; pack_no '||I_pack_no);
      for c_ordloc_pack_rec in C_ORDLOC_PACK LOOP
         L_qty_approved  := c_ordloc_pack_rec.qty_approved;
         L_qty_received  := c_ordloc_pack_rec.qty_received;
         L_qty_cancelled := c_ordloc_pack_rec.qty_cancelled;
         L_comp_unit_cost := c_ordloc_pack_rec.unit_cost;

         if LP_pack_type = 'B' then
            --- For a buyer pack, get landed cost of component items
            if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                            L_total_elc_prim,
                                            L_expenses,
                                            L_exp_currency,
                                            L_exchange_rate_exp,
                                            L_duty,
                                            L_duty_currency,
                                            OTB_SQL.LP_order_no,
                                            I_pack_no,
                                            I_item,
                                            NULL,
                                            c_ordloc_pack_rec.location,
                                            OTB_SQL.LP_supplier,
                                            LP_origin_country,
                                            LP_import_country,
                                            NULL) then
               RETURN FALSE;
            end if;

        else -- LP_pack_type = V
            --- prorate the landed cost of the component item
            if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                            L_total_elc_prim,
                                            L_expenses,
                                            L_exp_currency,
                                            L_exchange_rate_exp,
                                            L_duty,
                                            L_duty_currency,
                                            OTB_SQL.LP_order_no,
                                            I_pack_no,
                                            NULL,
                                            NULL,
                                            c_ordloc_pack_rec.location,
                                            OTB_SQL.LP_supplier,
                                            LP_origin_country,
                                            LP_import_country,
                                            NULL) then
               RETURN FALSE;
            end if;

            if LP_sum_comp_cost = 0 then
               L_total_elc_prim := 0;
            else
               L_total_elc_prim := L_total_elc_prim * (L_comp_unit_cost / LP_sum_comp_cost);
            end if;
         end if; -- LP_pack_type

         O_total_elc_rec := O_total_elc_rec + (L_total_elc_prim * L_qty_received);
         O_total_elc_can := O_total_elc_can + (L_total_elc_prim * L_qty_cancelled);
         O_total_elc_app := O_total_elc_app + (L_total_elc_prim * L_qty_approved);

      END LOOP; --- c_ordloc_pack

   end if; -- I_pack_no is/not null

   RETURN TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
         L_program, TO_CHAR(SQLCODE));
      return FALSE;

END ITEM_COST;
------------------------------------------------------------------------------
FUNCTION OTB_ITEM_RETAIL(I_item            IN     ORDLOC.ITEM%TYPE,
                         I_pack_no         IN     ITEM_MASTER.ITEM%TYPE,
                         O_retail_rec      IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                         O_retail_can      IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                         O_retail_app      IN OUT ORDLOC.UNIT_RETAIL%TYPE,
                         O_error_message   IN OUT VARCHAR2) RETURN BOOLEAN IS

   L_location             ORDLOC.LOCATION%TYPE;
   L_loc_code             ORDLOC.LOC_TYPE%TYPE;
   L_retail_prim          ORDLOC.UNIT_RETAIL%TYPE;
   L_unit_retail          ORDLOC.UNIT_RETAIL%TYPE;
   -- for itemloc_attrib_sql.get_costs_and_retail call
   L_dummy_cost           ORDLOC.UNIT_COST%TYPE;
   L_avg_cost_new         ITEM_LOC_SOH.AV_COST%TYPE            := NULL;
   L_unit_cost            ITEM_LOC_SOH.UNIT_COST%TYPE              := NULL;
   L_selling_unit_retail  ITEM_LOC.SELLING_UNIT_RETAIL%TYPE    := NULL;
   L_selling_uom          ITEM_LOC.SELLING_UOM%TYPE            := NULL;

   L_program          VARCHAR2(50)             := 'OTB_SQL.OTB_ITEM_RETAIL';

   cursor C_ITEM_RETAIL is
      select location,
            loc_type,
            unit_retail,
            DECODE(qty_ordered,0, NVL(qty_received,0), qty_ordered)   qty_approved,
            NVL(qty_received, 0) qty_received,
            NVL(qty_cancelled,0) qty_cancelled
        from ordloc
       where order_no   = OTB_SQL.LP_order_no
         and ordloc.item = I_item;

   cursor C_ORDLOC_PACK is
      select (DECODE(ol.qty_ordered,0,NVL(qty_received,0),
                    ol.qty_ordered) * vpq.qty) qty_approved,
             (NVL(ol.qty_received,0) * vpq.qty) qty_received,
             (NVL(ol.qty_cancelled,0) * vpq.qty) qty_cancelled,
             ol.location,
             ol.loc_type
        from ordloc ol, v_packsku_qty vpq
       where ol.order_no = OTB_SQL.LP_order_no
         and ol.item      = I_pack_no
         and vpq.pack_no = ol.item
         and vpq.item     = I_item;

BEGIN

   O_retail_rec := 0;
   O_retail_app := 0;
   O_retail_can := 0;

   if I_pack_no is NULL then
      --- I_item is an individual staple/fashion item
      SQL_LIB.SET_MARK('LOOP', 'C_item_retail', 'ordloc', NULL);
      FOR C_item_retail_rec IN C_item_retail LOOP
         L_location := C_item_retail_rec.location;
         L_loc_code := C_item_retail_rec.loc_type;

         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_location,
                                             L_loc_code,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             C_item_retail_rec.unit_retail,
                                             L_retail_prim,
                                             'N',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         O_retail_rec := O_retail_rec +
                         (L_retail_prim * C_item_retail_rec.qty_received);
         O_retail_can := O_retail_can +
                         (L_retail_prim * C_item_retail_rec.qty_cancelled);
         O_retail_app := O_retail_app +
                         (L_retail_prim * (C_item_retail_rec.qty_approved));
      END LOOP; --- c_item_retail_rec

   else --- I_pack_no is not null
      --- I_item is a pack item component
      SQL_LIB.SET_MARK('LOOP','C_ORDLOC_PACK','ordloc, v_packsku_qty',NULL);
      FOR ordloc_pack_rec in C_ORDLOC_PACK LOOP
         L_location := ordloc_pack_rec.location;
         L_loc_code := ordloc_pack_rec.loc_type;

         --- if I_item is a pack component, get its unit retail from
         --- item_master table

         if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                     I_item,
                                                     L_location,
                                                     L_loc_code,
                                                     L_avg_cost_new,
                                                     L_unit_cost,
                                                     L_unit_retail,
                                                     L_selling_unit_retail,
                                                     L_selling_uom) = FALSE then
            return FALSE;
         end if;
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             L_location,
                                             L_loc_code,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             L_unit_retail,
                                             L_retail_prim,
                                             'N',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;
         O_retail_rec := O_retail_rec +
                         (L_retail_prim * ordloc_pack_rec.qty_received);
         O_retail_can := O_retail_can +
                         (L_retail_prim * ordloc_pack_rec.qty_cancelled);
         O_retail_app := O_retail_app +
                         (L_retail_prim * (ordloc_pack_rec.qty_approved));

      END LOOP; --- ordloc_pack_rec
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END OTB_ITEM_RETAIL;
-----------------------------------------------------------------------------
FUNCTION RECLASS(O_error_message   IN OUT   VARCHAR2,
                 O_locked          IN OUT   BOOLEAN,
                 I_reclass_date    IN       DATE,
                 I_item            IN       ORDSKU.ITEM%TYPE,
                 I_old_dept        IN       OTB.DEPT%TYPE,
                 I_old_class       IN       OTB.CLASS%TYPE,
                 I_old_subclass    IN       OTB.SUBCLASS%TYPE,
                 I_new_dept        IN       OTB.DEPT%TYPE,
                 I_new_class       IN       OTB.CLASS%TYPE,
                 I_new_subclass    IN       OTB.SUBCLASS%TYPE)
   RETURN BOOLEAN IS

   L_order_no            ORDHEAD.ORDER_NO%TYPE          := 0;
   L_otb_calc_type_old   DEPS.OTB_CALC_TYPE%TYPE        := NULL;
   L_otb_calc_type_new   DEPS.OTB_CALC_TYPE%TYPE        := NULL;
   L_pack_no             ITEM_MASTER.ITEM%TYPE          := NULL;
   L_pack_cost           ORDLOC.UNIT_COST%TYPE          := NULL;
   L_comp_cost           ORDLOC.UNIT_COST%TYPE          := NULL;
   L_prorated_cost       NUMBER(24,8)                   := NULL;
   L_location            ORDLOC.LOCATION%TYPE           := NULL;
   L_loc_type            ORDLOC.LOC_TYPE%TYPE           := NULL;
   L_qty_approved        ORDLOC.QTY_ORDERED%TYPE        := 0;
   L_qty_received        ORDLOC.QTY_RECEIVED%TYPE       := 0;
   L_qty_cancelled       ORDLOC.QTY_CANCELLED%TYPE      := 0;
   L_function            VARCHAR2(20)                   := NULL;
   L_return_code         VARCHAR2(5)                    := 'TRUE';
   L_cursor              VARCHAR2(20)                   := NULL;
   L_program             VARCHAR2(50)                   := 'OTB_SQL.RECLASS';
   L_exception           VARCHAR2(3)                    := NULL;

   L_approved_cost       NUMBER                         := 0;
   L_received_cost       NUMBER                         := 0;
   L_cancelled_cost      NUMBER                         := 0;

   L_approved_retail     ORDLOC.UNIT_COST%TYPE          := 0;
   L_received_retail     ORDLOC.UNIT_COST%TYPE          := 0;
   L_cancelled_retail    ORDLOC.UNIT_COST%TYPE          := 0;
   L_sellable_ind        ITEM_MASTER.SELLABLE_IND%TYPE  := NULL;
   L_orderable_ind       ITEM_MASTER.ORDERABLE_IND%TYPE := NULL;
   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE      := NULL;
   L_system_options_rec  SYSTEM_OPTIONS%ROWTYPE;

   cursor C_OTB_CALC(L_dept otb.dept%TYPE) is
      select otb_calc_type
        from deps
       where dept = L_dept;

   cursor C_ORDER is
      select oh.order_no,
             osc.origin_country_id,
             ol.location
        from ordhead oh, ordsku osc, ordloc ol
       where osc.item     = I_item
         and ol.item  = osc.item
         and osc.order_no = oh.order_no
         and ol.order_no  = oh.order_no
         and (oh.status = 'A'
             or (oh.status = 'C'
                and otb_eow_date > I_reclass_date));

   cursor C_ORDER_PACK is
      select oh.order_no,
             ol.unit_cost,
             vpq.pack_no,
             os.origin_country_id,
             ol.location
        from ordhead oh, ordsku os, v_packsku_qty vpq, ordloc ol
       where os.item     = vpq.pack_no
         and ol.item     = os.item
         and vpq.item    = I_item
         and os.order_no = oh.order_no
         and ol.order_no = oh.order_no
         and (oh.status = 'A'
             or (oh.status = 'C'
                and otb_eow_date > I_reclass_date));

   cursor C_OUTSTAND is
      select NVL(sum(ol.unit_cost * (DECODE(ol.qty_ordered,
                                            0, NVL(ol.qty_received,0),
                                            ol.qty_ordered))), 0) approved,
             NVL(sum(ol.unit_cost * NVL(ol.qty_received,0)),0),
             NVL(sum(ol.unit_cost * NVL(ol.qty_cancelled,0)),0)
        from ordloc ol, ordsku os
       where ol.order_no  = OTB_SQL.LP_order_no
         and ol.item      = I_item
         and os.item      = ol.item
         and os.order_no  = ol.order_no
         and ol.location  = L_location;

   cursor C_PACK_LOC is
      select (SUM(DECODE(ol.qty_ordered,0,NVL(ol.qty_received,0),
                  ol.qty_ordered) * vpq.qty)) qty_approved,
             (SUM(NVL(ol.qty_received,0)* vpq.qty)) qty_received,
             (SUM(NVL(ol.qty_cancelled,0) * vpq.qty)) qty_cancelled
        from ordloc ol,
             v_packsku_qty vpq
       where ol.order_no = OTB_SQL.LP_order_no
         and ol.item     = L_pack_no
         and vpq.pack_no = ol.item
         and vpq.item    = I_item;


   --------------------------------------------------------------------------
   FUNCTION CALC_VALUES
      RETURN BOOLEAN IS

   BEGIN
      if (L_otb_calc_type_old = 'C' OR L_otb_calc_type_new = 'C') then
         if LP_elc_ind = 'Y' then
            if OTB_SQL.ITEM_COST(I_item,
                                 L_pack_no,
                                 L_received_cost,
                                 L_cancelled_cost,
                                 L_approved_cost,
                                 O_error_message) = FALSE then
               return FALSE;
            end if;
         end if; --- Calculate landed cost
      end if;

      if (L_otb_calc_type_old = 'R' OR L_otb_calc_type_new = 'R') then
         if OTB_SQL.OTB_ITEM_RETAIL(I_item,
                                   L_pack_no,
                                   L_received_retail,
                                   L_cancelled_retail,
                                   L_approved_retail,
                                   O_error_message) = FALSE then
            return FALSE;
         end if;
      end if;

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;

   END CALC_VALUES;
   --------------------------------------------------------------------------
   FUNCTION RECLASS_SUB
      RETURN BOOLEAN IS

   L_cancelled   ORDLOC.UNIT_COST%TYPE := 0;
   L_approved    ORDLOC.UNIT_COST%TYPE := 0;
   L_received    ORDLOC.UNIT_COST%TYPE := 0;

   BEGIN

      if CALC_VALUES = FALSE then
         return FALSE;
      end if;

    ------------------------------------------------------
    -- Update otb entries for old department/class/subclass

      L_exception  := 'OLD';
      open CP_LOCK_OTB (I_old_dept,
                        I_old_class,
                        I_old_subclass,
                        OTB_SQL.LP_order_eow_date);
      close CP_LOCK_OTB;
      --
      if L_otb_calc_type_old = 'C' then
         L_cancelled := L_cancelled_cost;
         L_approved  := L_approved_cost;
         L_received  := L_received_cost;
      else
         L_cancelled := L_cancelled_retail;
         L_approved  := L_approved_retail;
         L_received  := L_received_retail;
      end if;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'otb',
                       'dept '||to_char(I_old_dept)||
                       ' class '||to_char(I_old_class)||
                       ' subclass '||to_char(I_old_subclass));

      if OTB_SQL.LP_order_type = 'ARB' then
         update otb
            set cancel_amt     = cancel_amt     - L_cancelled,
                a_approved_amt = a_approved_amt - L_approved,
                a_receipts_amt = a_receipts_amt - L_received
          where dept     = I_old_dept
            and class    = I_old_class
            and subclass = I_old_subclass
            and eow_date = OTB_SQL.LP_order_eow_date;

      elsif OTB_SQL.LP_order_type = 'BRB' then
         update otb
            set cancel_amt     = cancel_amt     - L_cancelled,
                b_approved_amt = b_approved_amt - L_approved,
                b_receipts_amt = b_receipts_amt - L_received
          where dept     = I_old_dept
            and class    = I_old_class
            and subclass = I_old_subclass
            and eow_date = OTB_SQL.LP_order_eow_date;

      elsif OTB_SQL.LP_order_type = 'N/B' then
         update otb
            set cancel_amt     = cancel_amt     - L_cancelled,
                n_approved_amt = n_approved_amt - L_approved,
                n_receipts_amt = n_receipts_amt - L_received
          where dept     = I_old_dept
            and class    = I_old_class
            and subclass = I_old_subclass
            and eow_date = OTB_SQL.LP_order_eow_date;
      end if;

   --------------------------------------------------------------------
   -- Update or insert an otb entries for new department/class/subclass

      L_exception  := 'NEW';
      open CP_LOCK_OTB (I_new_dept,
                        I_new_class,
                        I_new_subclass,
                        OTB_SQL.LP_order_eow_date);
      close CP_LOCK_OTB;
      ---
      if L_otb_calc_type_new = 'C' then
         L_cancelled := L_cancelled_cost;
         L_approved  := L_approved_cost;
         L_received  := L_received_cost;
      else
         L_cancelled := L_cancelled_retail;
         L_approved  := L_approved_retail;
         L_received  := L_received_retail;
      end if;
      ---
      SQL_LIB.SET_MARK('UPDATE',
                       NULL,
                       'otb',
                       'dept '||to_char(I_new_dept)||
                       ' class '||to_char(I_new_class)||
                       ' subclass '||to_char(I_new_subclass));

      update otb
         set cancel_amt     = cancel_amt + L_cancelled,
             a_approved_amt = a_approved_amt +
                              DECODE(OTB_SQL.LP_order_type, 'ARB',
                                     L_approved, 0),
             a_receipts_amt = a_receipts_amt +
                              DECODE(OTB_SQL.LP_order_type, 'ARB',
                                     L_received, 0),
             b_approved_amt = b_approved_amt +
                              DECODE(OTB_SQL.LP_order_type, 'BRB',
                                     L_approved, 0),
             b_receipts_amt = b_receipts_amt +
                              DECODE(OTB_SQL.LP_order_type, 'BRB',
                                     L_received, 0),
             n_approved_amt = n_approved_amt +
                              DECODE(OTB_SQL.LP_order_type, 'N/B',
                                     L_approved, 0),
             n_receipts_amt = n_receipts_amt +
                              DECODE(OTB_SQL.LP_order_type, 'N/B',
                                     L_received, 0)
         where dept    = I_new_dept
          and class    = I_new_class
          and subclass = I_new_subclass
          and eow_date = OTB_SQL.LP_order_eow_date;

      if SQL%NOTFOUND then
         SQL_LIB.SET_MARK('INSERT',
                          NULL,
                          'otb',
                          'dept '||to_char(I_new_dept)||
                          ' class '||to_char(I_new_class)||
                          ' subclass '||to_char(I_new_subclass));

         insert into otb values (I_new_dept,
                                 I_new_class,
                                 I_new_subclass,
                                 OTB_SQL.LP_order_eow_date,
                                 OTB_SQL.LP_week_no,
                                 OTB_SQL.LP_month_no,
                                 OTB_SQL.LP_half_no,
                                 L_cancelled,
                                 0,
                                 DECODE(OTB_SQL.LP_order_type, 'N/B',
                                        L_approved, 0),
                                 DECODE(OTB_SQL.LP_order_type, 'N/B',
                                        L_received, 0),
                                 0,
                                 DECODE(OTB_SQL.LP_order_type, 'BRB',
                                        L_approved, 0),
                                 DECODE(OTB_SQL.LP_order_type, 'BRB',
                                        L_received, 0),
                                 0,
                                 DECODE(OTB_SQL.LP_order_type, 'ARB',
                                        L_approved, 0),
                                 DECODE(OTB_SQL.LP_order_type, 'ARB',
                                        L_received, 0));
      end if;

      return TRUE;

   EXCEPTION
      when RECORD_LOCKED then
         if L_exception = 'OLD' then
            O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                  'OTB',
                                                  to_char(I_old_class),
                                                  to_char(I_old_subclass));
         elsif L_exception = 'NEW' then
            O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                  'OTB',
                                                  to_char(I_new_class),
                                                  to_char(I_new_subclass));
         end if;
         O_locked := TRUE;
         return FALSE;

      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               L_program,
                                               TO_CHAR(SQLCODE));
         return FALSE;

   END RECLASS_SUB;
   --------------------------------------------------------------------------
BEGIN -- RECLASS

   O_locked := FALSE;
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
      return FALSE;
   end if;
   ---
   if L_system_options_rec.otb_system_ind = 'N' then
      return TRUE;
   end if;
   ---
   LP_elc_ind := L_system_options_rec.elc_ind;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_OTB_CALC',
                    'deps',
                    'dept '||to_char(I_old_dept));
   OPEN C_otb_calc(I_old_dept);
   SQL_LIB.SET_MARK('FETCH',
                    'C_OTB_CALC',
                    'deps',
                    'dept '||to_char(I_old_dept));
   FETCH C_otb_calc INTO L_otb_calc_type_old;
   if C_otb_calc%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_OTB_CALC',
                       'deps',
                       'dept '||to_char(I_old_dept));
      CLOSE C_otb_calc;
      O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT',
                                            'C_OTB_CALC',
                                            'deps',
                                            'dept '||to_char(I_old_dept));
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_OTB_CALC',
                    'deps',
                    'dept '||to_char(I_old_dept));
   CLOSE C_OTB_CALC;
   ---
   SQL_LIB.SET_MARK('OPEN',
                    'C_OTB_CALC',
                    'deps',
                    'dept '||to_char(I_new_dept));
   OPEN C_otb_calc(I_new_dept);
   SQL_LIB.SET_MARK('FETCH',
                    'C_OTB_CALC',
                    'deps',
                    'dept '||to_char(I_new_dept));
   FETCH C_otb_calc INTO L_otb_calc_type_new;
   if C_otb_calc%NOTFOUND then
      SQL_LIB.SET_MARK('CLOSE',
                       'C_OTB_CALC',
                       'deps',
                       'dept '||to_char(I_new_dept));
      CLOSE C_otb_calc;
      O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT',
                                            'C_OTB_CALC',
                                            'deps',
                                            'dept '||to_char(I_new_dept));
      return FALSE;
   end if;
   SQL_LIB.SET_MARK('CLOSE',
                    'C_OTB_CALC',
                    'deps',
                    'dept '||to_char(I_new_dept));
   CLOSE C_OTB_CALC;
   --- Process orders containing I_item
   SQL_LIB.SET_MARK('LOOP',
                    'C_ORDER',
                    'ordhead,
                    ordsku',
                    'item '||I_item||';reclass_date '||I_reclass_date);
   for c_order_rec in C_ORDER LOOP
      L_order_no        := c_order_rec.order_no;
      LP_origin_country := c_order_rec.origin_country_id;
      L_location        := c_order_rec.location;
      if OTB_SQL.ORD_INFO(L_order_no,
                          O_error_message) = FALSE then
         return FALSE;
      end if;
      ---
      if OTB_SQL.LP_order_type != 'DSD' then
         if (L_otb_calc_type_old = 'C' OR L_otb_calc_type_new = 'C') then
            if LP_elc_ind = 'N' then
               SQL_LIB.SET_MARK('OPEN',
                                'C_OUTSTAND',
                                'ordhead,
                                ordsku',
                                'order no '||to_char(OTB_SQL.LP_order_no));
               OPEN C_outstand;
               SQL_LIB.SET_MARK('FETCH',
                                'C_OUTSTAND',
                                'ordhead,
                                ordsku',
                                'order no '||to_char(OTB_SQL.LP_order_no));
               FETCH C_outstand INTO L_approved_cost,
                                     L_received_cost,
                                     L_cancelled_cost;
               SQL_LIB.SET_MARK('CLOSE',
                                'C_OUTSTAND',
                                'ordhead,
                                ordsku',
                                'order no '||to_char(OTB_SQL.LP_order_no));
               CLOSE C_outstand;
               ---
               if L_cancelled_cost != 0 then
                  if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                      OTB_SQL.LP_order_no,
                                                      'O',
                                                      NULL, NULL, NULL, NULL,
                                                      L_cancelled_cost,
                                                      L_cancelled_cost,
                                                      'N', NULL, NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               if L_approved_cost != 0 then
                  if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                      OTB_SQL.LP_order_no,
                                                      'O',
                                                      NULL, NULL, NULL, NULL,
                                                      L_approved_cost,
                                                      L_approved_cost,
                                                      'N', NULL, NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               if L_received_cost != 0 then
                  if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                      OTB_SQL.LP_order_no,
                                                      'O',
                                                      NULL, NULL, NULL, NULL,
                                                      L_received_cost,
                                                      L_received_cost,
                                                      'N', NULL, NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;  -- if elc_ind = N
         end if;  -- if either calc_type = C
         ---
         if not RECLASS_SUB then
            return FALSE;
         end if;
      end if;

   END LOOP; --- c_order_rec
   --- Process orders containing pack item for which I_item is a component
   SQL_LIB.SET_MARK('LOOP',
                    'C_ORDER_PACK',
                    'ordhead, ordsku, v_packsku_qty',
                    'item '||I_item||';reclass_date '||I_reclass_date);
   for c_order_pack_rec in C_ORDER_PACK LOOP
      L_order_no        := c_order_pack_rec.order_no;
      L_pack_no         := c_order_pack_rec.pack_no;
      L_pack_cost       := c_order_pack_rec.unit_cost;
      LP_origin_country := c_order_pack_rec.origin_country_id;
      L_location        := c_order_pack_rec.location;
      if OTB_SQL.ORD_INFO(L_order_no,
                          O_error_message) = FALSE then
         return FALSE;
      end if;
      ---
      if OTB_SQL.LP_order_type != 'DSD' then
         if (L_otb_calc_type_old = 'C' OR L_otb_calc_type_new = 'C') then
            --- get the pack type and fill global variable
            if ITEM_ATTRIB_SQL.GET_PACK_INDS (O_error_message,
                                              L_pack_ind,
                                              L_sellable_ind,
                                              L_orderable_ind,
                                              LP_pack_type,
                                              L_pack_no) = FALSE then
               return FALSE;
            end if;

            if (LP_pack_type is NULL) then
               return FALSE;
            end if;

            if L_system_options_rec.default_tax_type in ('SVAT', 'SALES') then
               SQL_LIB.SET_MARK('OPEN',
                                'CP_SUM_COMP_COST',
                                NULL,
                                NULL);
               open CP_SUM_COMP_COST(L_pack_no);
               SQL_LIB.SET_MARK('FETCH',
                                'CP_SUM_COMP_COST',
                                'item_supplier, v_packsku_qty',
                                'PACK_NO = '||L_pack_no);
               fetch CP_SUM_COMP_COST into LP_sum_comp_cost;
               SQL_LIB.SET_MARK('CLOSE',
                                'CP_SUM_COMP_COST',
                                NULL,
                                NULL);
               close CP_SUM_COMP_COST;
            elsif L_system_options_rec.default_tax_type = 'GTAX' then
               SQL_LIB.SET_MARK('OPEN',
                                'CP_SUM_COMP_COST_ICH',
                                NULL,
                                NULL);
               open CP_SUM_COMP_COST_ICH(L_pack_no);
               SQL_LIB.SET_MARK('FETCH',
                                'CP_SUM_COMP_COST_ICH',
                                'item_cost_head, v_packsku_qty',
                                'PACK_NO = '||L_pack_no);
               fetch CP_SUM_COMP_COST_ICH into LP_sum_comp_cost;
               SQL_LIB.SET_MARK('CLOSE',
                                'CP_SUM_COMP_COST_ICH',
                                NULL,
                                NULL);
               close CP_SUM_COMP_COST_ICH;
            end if;
            ---
            if LP_elc_ind = 'N' then
               SQL_LIB.SET_MARK('OPEN',
                                'C_PACK_LOC',
                                'ordloc',
                                'pack_no '||(L_pack_no));
               OPEN c_pack_loc;
               SQL_LIB.SET_MARK('FETCH',
                                'C_PACK_LOC',
                                'ordloc',
                                'pack_no '||(L_pack_no));
               FETCH c_pack_loc INTO L_qty_approved,
                                     L_qty_received,
                                     L_qty_cancelled;
               SQL_LIB.SET_MARK('CLOSE',
                                'C_PACK_LOC',
                                'ordloc',
                                'pack_no '||(L_pack_no));
               CLOSE c_pack_loc;
               ---
               if L_system_options_rec.default_tax_type in ('SVAT', 'SALES') then
                  SQL_LIB.SET_MARK('OPEN',
                                   'CP_COMP_UNIT_COST',
                                   NULL,
                                   NULL);
                  open CP_COMP_UNIT_COST(I_item);
                  SQL_LIB.SET_MARK('FETCH',
                                   'CP_COMP_UNIT_COST',
                                   'item_supplier',
                                   ' item = '||I_item);
                  fetch CP_COMP_UNIT_COST into L_comp_cost;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'CP_COMP_UNIT_COST',
                                   NULL,
                                   NULL);
                  close CP_COMP_UNIT_COST;
               elsif L_system_options_rec.default_tax_type = 'GTAX' then
                  SQL_LIB.SET_MARK('OPEN',
                                   'CP_COMP_UNIT_COST_ICH',
                                   NULL,
                                   NULL);
                  open CP_COMP_UNIT_COST_ICH(I_item);
                  SQL_LIB.SET_MARK('FETCH',
                                   'CP_COMP_UNIT_COST_ICH',
                                   'item_cost_head',
                                   ' item = '||I_item);
                  fetch CP_COMP_UNIT_COST_ICH into L_comp_cost;
                  SQL_LIB.SET_MARK('CLOSE',
                                   'CP_COMP_UNIT_COST_ICH',
                                   NULL,
                                   NULL);
                  close CP_COMP_UNIT_COST_ICH;
               end if;

               if LP_sum_comp_cost = 0 then
                  L_prorated_cost := 0;
               else
                  L_prorated_cost := L_pack_cost * (L_comp_cost / LP_sum_comp_cost);
               end if;
               L_approved_cost  := (L_prorated_cost * L_qty_approved);
               L_received_cost  := (L_prorated_cost * L_qty_received);
               L_cancelled_cost := (L_prorated_cost * L_qty_cancelled);

               if L_cancelled_cost != 0 then
                  if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                      OTB_SQL.LP_order_no,
                                                      'O',
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      L_cancelled_cost,
                                                      L_cancelled_cost,
                                                      'N',
                                                      NULL,
                                                      NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               if L_approved_cost != 0 then
                  if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                      OTB_SQL.LP_order_no,
                                                      'O',
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      L_approved_cost,
                                                      L_approved_cost,
                                                      'N',
                                                      NULL,
                                                      NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
               if L_received_cost != 0 then
                  if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                                      OTB_SQL.LP_order_no,
                                                      'O',
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      NULL,
                                                      L_received_cost,
                                                      L_received_cost,
                                                      'N',
                                                      NULL,
                                                      NULL) = FALSE then
                     return FALSE;
                  end if;
               end if;
            end if;  -- if elc_ind = N
         end if; -- new or old dept is 'C'
         ---
         if not RECLASS_SUB then
            return FALSE;
         end if;
      end if;

   END LOOP; -- c_order_pack
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END RECLASS;
-------------------------------------------------------------------------------
FUNCTION INIT_ORD_BULK(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                       I_type          IN     VARCHAR2,
                       I_otb_size      IN     NUMBER)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.INIT_ORD_BULK';

BEGIN
   if I_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if I_type = 'APPROVE' then
      LP_otb_size_1 := I_otb_size;
   ---
   elsif I_type = 'UNAPPROVE' then
      LP_otb_size_2 := I_otb_size;
   ---
   elsif I_type = 'CANCEL_REINSTATE' then
      LP_otb_size_3 := I_otb_size;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END INIT_ORD_BULK;
-------------------------------------------------------------------------------
FUNCTION FLUSH_ORD_BULK(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_type          IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program    VARCHAR2(50)             := 'OTB_SQL.FLUSH_ORD_BULK';
   L_class      class.class%TYPE         := NULL;
   L_subclass   subclass.subclass%TYPE   := NULL;
   L_otb_ind    SYSTEM_OPTIONS.OTB_SYSTEM_IND%TYPE := NULL;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_OTB_SYSTEM_IND(O_error_message,
                                            L_otb_ind) = FALSE then
      return FALSE;
   end if;

   if L_otb_ind = 'N' then
      return TRUE;
   end if;
   
   if I_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if I_type = 'APPROVE' then
      ---
      if LP_otb_size_1 > 0 then
         FOR i IN 1..LP_otb_size_1 LOOP
            L_class    := LP_class_tbl_1(i);
            L_subclass := LP_subclass_tbl_1(i);
            ---
            -- Record locking for update of otb table
            open CP_LOCK_OTB (LP_dept_tbl_1(i),
                              LP_class_tbl_1(i),
                              LP_subclass_tbl_1(i),
                              LP_eow_date_tbl_1(i));
            close CP_LOCK_OTB;
            ---
            SQL_LIB.SET_MARK('UPDATE', NULL, 'OTB', NULL);
            UPDATE OTB
               SET n_approved_amt = n_approved_amt + LP_n_approved_amt_tbl_1(i),
                   a_approved_amt = a_approved_amt + LP_a_approved_amt_tbl_1(i),
                   b_approved_amt = b_approved_amt + LP_b_approved_amt_tbl_1(i)
             WHERE dept     = LP_dept_tbl_1(i)
               AND class    = LP_class_tbl_1(i)
               AND subclass = LP_subclass_tbl_1(i)
               AND eow_date = LP_eow_date_tbl_1(i);
            ---
            if SQL%NOTFOUND then
               SQL_LIB.SET_MARK('INSERT',NULL,'OTB',NULL);
               INSERT INTO OTB (dept,
                                class,
                                subclass,
                                eow_date,
                                week_no,
                                month_no,
                                half_no,
                                cancel_amt,
                                n_budget_amt,
                                n_approved_amt,
                                n_receipts_amt,
                                b_budget_amt,
                                b_approved_amt,
                                b_receipts_amt,
                                a_budget_amt,
                                a_approved_amt,
                                a_receipts_amt)
                        values (LP_dept_tbl_1(i),
                                LP_class_tbl_1(i),
                                LP_subclass_tbl_1(i),
                                LP_eow_date_tbl_1(i),
                                OTB_SQL.LP_week_no,
                                OTB_SQL.LP_month_no,
                                OTB_SQL.LP_half_no,
                                0,                        -- cancel_amt
                                0,                        -- n_budget_amt
                                LP_n_approved_amt_tbl_1(i),
                                0,                        -- n_receipts_amt
                                0,                        -- b_budget_amt
                                LP_b_approved_amt_tbl_1(i),
                                0,                        -- b_receipts_amt
                                0,                        -- a_budget_amt
                                LP_a_approved_amt_tbl_1(i),
                                0);                       -- a_receipts_amt
            end if; -- if SQL%NOTFOUND then
         END LOOP;
      end if; -- if LP_otb_size_1 > 0 then
      ---
      LP_otb_size_1 := 0;
      ---
   elsif I_type = 'UNAPPROVE' then
      ---
      if LP_otb_size_2 > 0 then
         ---
         FOR i IN 1..LP_otb_size_2 LOOP
            L_class    := LP_class_tbl_2(i);
            L_subclass := LP_subclass_tbl_2(i);
            ---
            --- Record locking for update of otb table
            open CP_LOCK_OTB (LP_dept_tbl_2(i),
                              LP_class_tbl_2(i),
                              LP_subclass_tbl_2(i),
                              LP_eow_date_tbl_2(i));
            close CP_LOCK_OTB;
         END LOOP;
         ---
         SQL_LIB.SET_MARK('UPDATE', NULL, 'OTB', NULL);
         FORALL i IN 1..LP_otb_size_2
            UPDATE OTB
               SET n_approved_amt = n_approved_amt + LP_n_approved_amt_tbl_2(i),
                   a_approved_amt = a_approved_amt + LP_a_approved_amt_tbl_2(i),
                   b_approved_amt = b_approved_amt + LP_b_approved_amt_tbl_2(i)
             WHERE dept     = LP_dept_tbl_2(i)
               AND class    = LP_class_tbl_2(i)
               AND subclass = LP_subclass_tbl_2(i)
               AND eow_date = LP_eow_date_tbl_2(i);
      end if;
      ---
      LP_otb_size_2 := 0;
      ---
   elsif I_type = 'CANCEL_REINSTATE' then
      if LP_otb_size_3 > 0 then
         ---
         FOR i IN 1..LP_otb_size_3 LOOP
            L_class    := LP_class_tbl_3(i);
            L_subclass := LP_subclass_tbl_3(i);
            ---
            --- Record locking for update of otb table
            open CP_LOCK_OTB (LP_dept_tbl_3(i),
                              LP_class_tbl_3(i),
                              LP_subclass_tbl_3(i),
                              LP_eow_date_tbl_3(i));
            close CP_LOCK_OTB;
         END LOOP;
         ---
         SQL_LIB.SET_MARK('UPDATE', NULL, 'OTB', NULL);
         FORALL i IN 1..LP_otb_size_3
            UPDATE OTB
               SET n_approved_amt = n_approved_amt + LP_n_approved_amt_tbl_3(i),
                   a_approved_amt = a_approved_amt + LP_a_approved_amt_tbl_3(i),
                   b_approved_amt = b_approved_amt + LP_b_approved_amt_tbl_3(i),
                   cancel_amt     = cancel_amt     + LP_cancel_amt_tbl_3(i)
             WHERE dept     = LP_dept_tbl_3(i)
               AND class    = LP_class_tbl_3(i)
               AND subclass = LP_subclass_tbl_3(i)
               AND eow_date = LP_eow_date_tbl_3(i);
      end if;
      ---
      LP_otb_size_3 := 0;
   ---
   elsif I_type = 'APPROVE_CASCADE' then
      if LP_otb_size_1 > 0 then
         FORALL i IN 1..LP_otb_size_1
            INSERT INTO OTB_CASCADE_STG (dept,
                                         class,
                                         subclass,
                                         eow_date,
                                         n_approved_amt,
                                         a_approved_amt,
                                         b_approved_amt)
                                 values (LP_dept_tbl_1(i),
                                         LP_class_tbl_1(i),
                                         LP_subclass_tbl_1(i),
                                         LP_eow_date_tbl_1(i),
                                         LP_n_approved_amt_tbl_1(i),
                                         LP_a_approved_amt_tbl_1(i),
                                         LP_b_approved_amt_tbl_1(i));
      end if; -- if LP_otb_size_1 > 0 then
      ---
      LP_otb_size_1 := 0;
      ---
   elsif I_type = 'UNAPPROVE_CASCADE' then
      if LP_otb_size_2 > 0 then
         ---
         FORALL i IN 1..LP_otb_size_2
            INSERT INTO OTB_CASCADE_STG (dept,
                                         class,
                                         subclass,
                                         eow_date,
                                         n_approved_amt,
                                         a_approved_amt,
                                         b_approved_amt)
                                 values (LP_dept_tbl_2(i),
                                         LP_class_tbl_2(i),
                                         LP_subclass_tbl_2(i),
                                         LP_eow_date_tbl_2(i),
                                         LP_n_approved_amt_tbl_2(i),
                                         LP_a_approved_amt_tbl_2(i),
                                         LP_b_approved_amt_tbl_2(i));
      end if;
      ---
      LP_otb_size_2 := 0;
      ---
   end if;
   ---
   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED', 'OTB',
                                            to_char(L_class),
                                            to_char(L_subclass));
      return FALSE;

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END FLUSH_ORD_BULK;
-----------------------------------------------------------------------------
FUNCTION GET_BULK_SIZE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_otb_size      IN OUT NUMBER,
                        I_type          IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(50)   := 'OTB_SQL.GET_BULK_SIZE';

BEGIN
   if I_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INV_INPUT_GENERIC',NULL,NULL,NULL);
      return FALSE;
   end if;
   ---
   if I_type = 'APPROVE' then
      O_otb_size := LP_otb_size_1;
   ---
   elsif I_type = 'UNAPPROVE' then
      O_otb_size := LP_otb_size_2;
   ---
   elsif I_type = 'CANCEL_REINSTATE' then
      O_otb_size := LP_otb_size_3;
   end if;
   ---
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END GET_BULK_SIZE;
-----------------------------------------------------------------------------
FUNCTION BUILD_ORD_APPROVE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_order_no      IN     ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_dept            DEPS.DEPT%TYPE;
   L_class           CLASS.CLASS%TYPE;
   L_subclass        SUBCLASS.SUBCLASS%TYPE;
   L_olddept         DEPS.DEPT%TYPE;
   L_oldclass        CLASS.CLASS%TYPE;
   L_oldsubclass     SUBCLASS.SUBCLASS%TYPE;
   L_order_amt       OTB.N_BUDGET_AMT%TYPE      := 0;
   L_subtotal        OTB.N_BUDGET_AMT%TYPE      := 0;
   L_amount          OTB.N_BUDGET_AMT%TYPE      := 0;
   L_duty_expenses   ORDLOC.UNIT_COST%TYPE;
   L_program         VARCHAR2(50)               := 'OTB_SQL.BUILD_ORD_APPROVE';
   L_otb_calc_type   DEPS.OTB_CALC_TYPE%TYPE;
   L_system_options  SYSTEM_OPTIONS%ROWTYPE;

   -------------------------------------------------------------------------
   FUNCTION APPROVE_INSERT
   RETURN BOOLEAN IS

   BEGIN
      --- do not need to check/lock OTB here, since it will be done during flush
      ---
      if L_amount > 0 and OTB_SQL.LP_order_type in ('ARB', 'BRB', 'N/B') then
         LP_otb_size_1                    := LP_otb_size_1 + 1;
         ---
         LP_dept_tbl_1(LP_otb_size_1)     := L_olddept;
         LP_class_tbl_1(LP_otb_size_1)    := L_oldclass;
         LP_subclass_tbl_1(LP_otb_size_1) := L_oldsubclass;
         LP_eow_date_tbl_1(LP_otb_size_1) := OTB_SQL.LP_order_eow_date;
         ---
         if OTB_SQL.LP_order_type = 'ARB' then
            LP_a_approved_amt_tbl_1(LP_otb_size_1) := L_amount;
            LP_b_approved_amt_tbl_1(LP_otb_size_1) := 0;
            LP_n_approved_amt_tbl_1(LP_otb_size_1) := 0;
         elsif OTB_SQL.LP_order_type = 'BRB' then
            LP_a_approved_amt_tbl_1(LP_otb_size_1) := 0;
            LP_b_approved_amt_tbl_1(LP_otb_size_1) := L_amount;
            LP_n_approved_amt_tbl_1(LP_otb_size_1) := 0;
         elsif OTB_SQL.LP_order_type = 'N/B' then
            LP_a_approved_amt_tbl_1(LP_otb_size_1) := 0;
            LP_b_approved_amt_tbl_1(LP_otb_size_1) := 0;
            LP_n_approved_amt_tbl_1(LP_otb_size_1) := L_amount;
         end if; -- check LP_order_type
      end if; -- L_amount > 0
      ---
      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                               L_program, to_char(SQLCODE));
         return FALSE;

   END APPROVE_INSERT;
   ---------------------------------------------------------------------
BEGIN -- BUILD_ORD_APPROVE
   if OTB_SQL.ORD_INFO (I_order_no,
                        O_error_message) = FALSE then
      return FALSE;
   end if;
   ---
   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.LP_order_type != 'DSD' then

      LP_elc_ind := L_system_options.elc_ind;

      FOR C_order_appr_rec IN CP_ORDER_APPR LOOP
         if CP_ORDER_APPR%ROWCOUNT = 1 then
            -- initialize L_olddept,L_oldclass,L_oldsubclass
            L_olddept     := C_order_appr_rec.dept;
            L_oldclass    := C_order_appr_rec.class;
            L_oldsubclass := C_order_appr_rec.subclass;
         end if;

         L_duty_expenses := 0;
         LP_origin_country := C_order_appr_rec.origin_country;
         LP_dept           := C_order_appr_rec.dept;
         L_class           := C_order_appr_rec.class;
         L_subclass        := C_order_appr_rec.subclass;

         SQL_LIB.SET_MARK('OPEN', 'CP_OTB_CALC_TYPE', NULL, NULL);
         open CP_OTB_CALC_TYPE;

         SQL_LIB.SET_MARK('FETCH', 'CP_OTB_CALC_TYPE', NULL, NULL);
         fetch CP_OTB_CALC_TYPE into L_otb_calc_type;

         SQL_LIB.SET_MARK('CLOSE', 'CP_OTB_CALC_TYPE', NULL, NULL);
         close CP_OTB_CALC_TYPE;

         if (LP_dept != L_olddept) or
            (L_class != L_oldclass) or
            (L_subclass != L_oldsubclass) then

            if not APPROVE_INSERT then
               return FALSE;
            end if;
            L_amount      := 0;
            L_olddept     := LP_dept;
            L_oldclass    := L_class;
            L_oldsubclass := L_subclass;
         end if;

         if not CALC_COST_RETAIL(O_error_message,
                                 I_order_no,
                                 C_order_appr_rec.item,
                                 C_order_appr_rec.origin_country,
                                 C_order_appr_rec.comp_item,
                                 C_order_appr_rec.comp_qty,
                                 L_otb_calc_type,
                                 L_subtotal) then
            return FALSE;
         end if;
         L_amount := L_amount + L_subtotal;

      END LOOP; --- c_order_appr_rec

      if not APPROVE_INSERT then
         return FALSE;
      end if;
   end if;
   ---

   if L_system_options.rtm_simplified_ind = 'N' then
      if not ALC_ALLOC_SQL.INSERT_ELC_COMPS(O_error_message, I_order_no) then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END BUILD_ORD_APPROVE;
------------------------------------------------------------------------------
FUNCTION BUILD_ORD_UNAPPROVE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_order_no      IN     ORDSKU.ORDER_NO%TYPE)
   RETURN BOOLEAN IS

   L_dept           DEPS.DEPT%TYPE;
   L_class          CLASS.CLASS%TYPE;
   L_subclass       SUBCLASS.SUBCLASS%TYPE;
   L_olddept        DEPS.DEPT%TYPE;
   L_oldclass       CLASS.CLASS%TYPE;
   L_oldsubclass    SUBCLASS.SUBCLASS%TYPE;
   L_order_amt      OTB.N_BUDGET_AMT%TYPE;
   L_subtotal       OTB.N_BUDGET_AMT%TYPE      := 0;
   L_amount         OTB.N_BUDGET_AMT%TYPE      := 0;
   L_duty_expenses  ORDLOC.UNIT_COST%TYPE;
   L_otb_calc_type  DEPS.OTB_CALC_TYPE%TYPE;
   L_program        VARCHAR2(50)               := 'OTB_SQL.BUILD_ORD_UNAPPROVE';

   ----------------------------------------------------------------------
   FUNCTION UNAPPROVE_INSERT
   RETURN BOOLEAN IS

   BEGIN
      --- do not need to check/lock OTB here, since it will be done during flush
      ---
      if L_amount > 0 and OTB_SQL.LP_order_type in ('ARB', 'BRB', 'N/B') then
         LP_otb_size_2                    := LP_otb_size_2 + 1;
         ---
         LP_dept_tbl_2(LP_otb_size_2)     := L_olddept;
         LP_class_tbl_2(LP_otb_size_2)    := L_oldclass;
         LP_subclass_tbl_2(LP_otb_size_2) := L_oldsubclass;
         LP_eow_date_tbl_2(LP_otb_size_2) := OTB_SQL.LP_order_eow_date;
         ---
         if OTB_SQL.LP_order_type = 'ARB' then
            LP_a_approved_amt_tbl_2(LP_otb_size_2) := -1 * L_amount;
            LP_b_approved_amt_tbl_2(LP_otb_size_2) := 0;
            LP_n_approved_amt_tbl_2(LP_otb_size_2) := 0;
         elsif OTB_SQL.LP_order_type = 'BRB' then
            LP_a_approved_amt_tbl_2(LP_otb_size_2) := 0;
            LP_b_approved_amt_tbl_2(LP_otb_size_2) := -1 * L_amount;
            LP_n_approved_amt_tbl_2(LP_otb_size_2) := 0;
         elsif OTB_SQL.LP_order_type = 'N/B' then
            LP_a_approved_amt_tbl_2(LP_otb_size_2) := 0;
            LP_b_approved_amt_tbl_2(LP_otb_size_2) := 0;
            LP_n_approved_amt_tbl_2(LP_otb_size_2) := -1 * L_amount;
         end if; -- check LP_order_type
      end if; --if L_amount > 0 then
      ---
      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
            L_program, TO_CHAR(SQLCODE));
         return FALSE;

   END UNAPPROVE_INSERT;
   ------------------------------------------------------------------------
BEGIN -- BUILD_ORD_UNAPPROVE
   if OTB_SQL.ORD_INFO (I_order_no,
                        O_error_message) = FALSE then
      return FALSE;
   end if;
   if OTB_SQL.LP_order_type != 'DSD' then
      if not SYSTEM_OPTIONS_SQL.GET_ELC_IND(O_error_message,
                                            LP_elc_ind) then
         return FALSE;
      end if;

      FOR C_order_appr_rec IN CP_ORDER_APPR LOOP
         if CP_ORDER_APPR%ROWCOUNT = 1 then
            -- initialize L_olddept,L_oldclass,L_oldsubclass
            L_olddept     := C_order_appr_rec.dept;
            L_oldclass    := C_order_appr_rec.class;
            L_oldsubclass := C_order_appr_rec.subclass;
         end if;

         L_duty_expenses   := 0;
         LP_origin_country := C_order_appr_rec.origin_country;
         LP_dept           := C_order_appr_rec.dept;
         L_class           := C_order_appr_rec.class;
         L_subclass        := C_order_appr_rec.subclass;

         SQL_LIB.SET_MARK('OPEN', 'CP_OTB_CALC_TYPE', NULL, NULL);
         open CP_OTB_CALC_TYPE;

         SQL_LIB.SET_MARK('FETCH', 'CP_OTB_CALC_TYPE', NULL, NULL);
         fetch CP_OTB_CALC_TYPE into L_otb_calc_type;

         SQL_LIB.SET_MARK('CLOSE', 'CP_OTB_CALC_TYPE', NULL, NULL);
         close CP_OTB_CALC_TYPE;

         if (LP_dept != L_olddept) or
            (L_class != L_oldclass) or
            (L_subclass != L_oldsubclass) then

            if not UNAPPROVE_INSERT then
               return FALSE;
            end if;
            L_amount      := 0;
            L_olddept     := LP_dept;
            L_oldclass    := L_class;
            L_oldsubclass := L_subclass;
         end if; -- dept/class/subclass changed

         if not CALC_COST_RETAIL(O_error_message,
                                 I_order_no,
                                 C_order_appr_rec.item,
                                 C_order_appr_rec.origin_country,
                                 C_order_appr_rec.comp_item,
                                 C_order_appr_rec.comp_qty,
                                 L_otb_calc_type,
                                 L_subtotal) then
            return FALSE;
         end if;
         L_amount := L_amount + L_subtotal;

      END LOOP; --- c_order_appr_rec

      if not UNAPPROVE_INSERT then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END BUILD_ORD_UNAPPROVE;
------------------------------------------------------------------------------
FUNCTION BUILD_ORD_CANCEL_REINSTATE (O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_order_no              IN     ORDHEAD.ORDER_NO%TYPE,
                                     I_item                  IN     ORDLOC.item%TYPE,
                                     I_location              IN     ORDLOC.LOCATION%TYPE,
                                     I_loc_type              IN     ORDLOC.LOC_TYPE%TYPE,
                                     I_cancel_reinstate_qty  IN     ORDLOC.QTY_CANCELLED%TYPE,
                                     I_cancel_reinstate_flag IN     VARCHAR2)
   RETURN BOOLEAN IS

   L_program             VARCHAR2(50) := 'OTB_SQL.BUILD_ORD_CANCEL_REINSTATE';
   L_pack_type           ITEM_MASTER.PACK_TYPE%TYPE;
   L_comp_item           PACKITEM.ITEM%TYPE;
   L_comp_qty            PACKITEM_BREAKOUT.ITEM_QTY%TYPE;
   L_comp_sum_qty        PACKITEM_BREAKOUT.ITEM_QTY%TYPE;
   L_unit_cost           ORDLOC.UNIT_COST%TYPE;
   L_comp_cost           ORDLOC.UNIT_COST%TYPE;
   L_sum_cost            ORDLOC.UNIT_COST%TYPE;
   L_duty_ord            ORDLOC.UNIT_COST%TYPE;
   L_expenses_ord        ORDLOC.UNIT_COST%TYPE;
   L_vend_lc_ord         ORDLOC.UNIT_COST%TYPE;
   L_total_elc           ORDLOC_EXP.EST_EXP_VALUE%TYPE;
   L_exp_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   L_dty_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   L_exchange_rate_exp   CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   L_sellable_ind        ITEM_MASTER.SELLABLE_IND%TYPE;
   L_orderable_ind       ITEM_MASTER.ORDERABLE_IND%TYPE;
   L_pack_ind            ITEM_MASTER.PACK_IND%TYPE;
   L_default_po_cost     COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE;

   cursor C_packitem is
     SELECT vpq.item,
            vpq.qty,
            its.unit_cost
       FROM v_packsku_qty vpq,
            item_supp_country_loc its
      WHERE vpq.pack_no           = I_item
        AND its.origin_country_id = LP_origin_country
        AND its.supplier          = OTB_SQL.LP_supplier
        AND its.item              = vpq.item
        AND its.loc               = I_location
        AND its.loc_type          = I_loc_type;

   cursor C_ITEM_UNIT_COST is
      SELECT ol.unit_cost,
             os.origin_country_id
        FROM ordsku os,
             ordloc ol
       WHERE os.item       = I_item
         AND os.order_no   = I_order_no
         AND os.order_no   = ol.order_no
         AND os.item       = ol.item 
         AND ol.location   = I_location
         AND ol.loc_type   = I_loc_type;

   ------------------------------------------------------------------
   /* This function is internal to BUILD_ORD_CANCEL_REINSTATE. */
   FUNCTION ORD_CANC_REIN_ITEM(IF_item      IN ordloc.item%TYPE,
                               IF_pack_no   IN ITEM_MASTER.ITEM%TYPE,
                               IF_unit_cost IN ORDLOC.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   LF_dept                DEPS.DEPT%TYPE;
   LF_class               CLASS.CLASS%TYPE;
   LF_subclass            SUBCLASS.SUBCLASS%TYPE;
   LF_amount              ORDLOC.UNIT_RETAIL%TYPE       := 0;
   LF_calc_type           DEPS.OTB_CALC_TYPE%TYPE;
   LF_unit_retail         ORDLOC.UNIT_RETAIL%TYPE       := 0;
   LF_duty_ord            ORDLOC.UNIT_COST%TYPE         := 0;
   LF_expenses_ord        ORDLOC.UNIT_COST%TYPE         := 0;
   LF_lc_ord              ORDLOC.UNIT_COST%TYPE         := 0;
   LF_item                ITEM_SUPPLIER.ITEM%TYPE;
   LF_comp_item           ITEM_SUPPLIER.ITEM%TYPE;
   LF_simple_pack_ind     ITEM_MASTER.SIMPLE_PACK_IND%TYPE;

   --
   LF_exp_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   LF_dty_currency        CURRENCIES.CURRENCY_CODE%TYPE;
   LF_exchange_rate_exp   CURRENCY_RATES.EXCHANGE_RATE%TYPE;
   --- used in the itemloc_attrib_sql.get_costs_and_retails call
   LF_dummy_cost          ITEM_LOC_SOH.UNIT_COST%TYPE       := NULL;
   LF_avg_cost_new        ITEM_LOC_SOH.AV_COST%TYPE         := NULL;
   LF_unit_cost           ITEM_LOC_SOH.UNIT_COST%TYPE           := NULL;
   LF_selling_unit_retail ITEM_LOC.SELLING_UNIT_RETAIL%TYPE := NULL;
   LF_selling_uom         ITEM_LOC.SELLING_UOM%TYPE         := NULL;
   ---
   cursor C_GET_OTB_CALC_TYPE is
      select otb_calc_type
        from deps
       where dept = LF_dept;

   cursor C_item_UNIT_RETAIL is
      select unit_retail
        from ordloc
       where item     = IF_item
         and order_no = I_order_no
         and location = I_location
         and loc_type = I_loc_type;

   cursor C_CHECK_SIMPLE_PACK is
      select simple_pack_ind
        from item_master
       where item = IF_pack_no;


   BEGIN
      --call item_att to get dept/class/subclass of IF_item
      if ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                        IF_item,
                                        LF_dept,
                                        LF_class,
                                        LF_subclass) = FALSE then
         return FALSE;
      end if;

      --get otb_calc_type from deps table
      SQL_LIB.SET_MARK('OPEN', 'C_GET_OTB_CALC_TYPE', NULL, NULL);
      open C_GET_OTB_CALC_TYPE;

      SQL_LIB.SET_MARK('FETCH', 'C_GET_OTB_CALC_TYPE', NULL, NULL);
      fetch C_GET_OTB_CALC_TYPE into LF_calc_type;

      if C_GET_OTB_CALC_TYPE%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_DEPT', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE', 'C_GET_OTB_CALC_TYPE', NULL, NULL);
         close C_GET_OTB_CALC_TYPE;
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_GET_OTB_CALC_TYPE', NULL, NULL);
      close C_GET_OTB_CALC_TYPE;

      LF_amount := 0;
      ---
      if IF_pack_no is NOT NULL then
         LF_item      := IF_pack_no;
         LF_comp_item := IF_item;
      else
         LF_item      := IF_item;
         LF_comp_item := NULL;
      end if;
      ---
      if LF_calc_type = 'C' then
         if LP_elc_ind = 'Y' then

            if L_vend_lc_ord is NOT NULL then -- IF_item is a vendor pack comp
               --- prorate landed cost
               SQL_LIB.SET_MARK('OPEN', 'C_CHECK_SIMPLE_PACK', NULL, NULL);
               open C_CHECK_SIMPLE_PACK;
      
               SQL_LIB.SET_MARK('FETCH', 'C_CHECK_SIMPLE_PACK', NULL, NULL);
               fetch C_CHECK_SIMPLE_PACK into LF_simple_pack_ind;

               SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_SIMPLE_PACK', NULL, NULL);
               close C_CHECK_SIMPLE_PACK;
             
               if LF_simple_pack_ind = 'Y' then
                  LF_amount := L_vend_lc_ord * I_cancel_reinstate_qty;
               else
                  if L_sum_cost = 0 then
                     LF_amount := 0;
                  else
                     LF_amount := (L_vend_lc_ord * (L_comp_cost / L_sum_cost))
                                   * L_comp_qty * I_cancel_reinstate_qty;
                  end if;
               end if;


            else -- IF_item is either a buyer pack component or indiv. item
               -- retrieve landed cost for item in primary currency
               if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                               LF_amount,
                                               LF_expenses_ord,
                                               LF_exp_currency,
                                               LF_exchange_rate_exp,
                                               LF_duty_ord,
                                               LF_dty_currency,
                                               I_order_no,
                                               LF_item,
                                               LF_comp_item,
                                               NULL,            --zone_id
                                               I_location,
                                               LP_supplier,
                                               LP_origin_country,
                                               LP_import_country,
                                               IF_unit_cost) then
                  return FALSE;
               end if;

               LF_amount := LF_amount * L_comp_qty * I_cancel_reinstate_qty;

            end if;
         end if; -- import_ind = Y/N
      else -- LF_calc_type = R
         if IF_pack_no is NULL then -- IF_item is staple or style
            SQL_LIB.SET_MARK('OPEN', 'C_ITEM_UNIT_RETAIL', NULL, NULL);
            open C_ITEM_UNIT_RETAIL;

            SQL_LIB.SET_MARK('FETCH', 'C_ITEM_UNIT_RETAIL', NULL, NULL);
            fetch C_ITEM_UNIT_RETAIL into LF_unit_retail;

            if C_ITEM_UNIT_RETAIL%NOTFOUND then
               O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', NULL, NULL, NULL);
               SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_UNIT_RETAIL', 'ORDLOC', NULL);
               close C_ITEM_UNIT_RETAIL;
               return FALSE;
            end if;

            SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_UNIT_RETAIL', 'ORDLOC', NULL);
            close C_ITEM_UNIT_RETAIL;

         else -- IF_item is a pack component

            if ITEMLOC_ATTRIB_SQL.GET_COSTS_AND_RETAILS(O_error_message,
                                                        LF_item,
                                                        I_location,
                                                        I_loc_type,
                                                        LF_avg_cost_new,
                                                        LF_unit_cost,
                                                        LF_unit_retail,
                                                        LF_selling_unit_retail,
                                                        LF_selling_uom) = FALSE then
               return FALSE;
            end if;

         end if;
         -- convert unit retail from local to primary currency
         if CURRENCY_SQL.CONVERT_BY_LOCATION(O_error_message,
                                             I_location,
                                             I_loc_type,
                                             NULL,
                                             NULL,
                                             NULL,
                                             NULL,
                                             LF_unit_retail,
                                             LF_unit_retail,
                                             'N',
                                             NULL,
                                             NULL) = FALSE then
            return FALSE;
         end if;

         LF_amount := LF_unit_retail * (L_comp_qty * I_cancel_reinstate_qty);

      end if; -- LF_calc_type

      --- do not need to check/lock OTB here, since it will be done during flush
      ---
      if I_cancel_reinstate_flag = 'C' and LF_amount > 0 then
         if OTB_SQL.LP_order_status = 'A' then
            if OTB_SQL.LP_order_type = 'ARB' then
               ---
               LP_otb_size_3                    := LP_otb_size_3 + 1;
               ---
               LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
               LP_class_tbl_3(LP_otb_size_3)    := LF_class;
               LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
               LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
               ---
               LP_a_approved_amt_tbl_3(LP_otb_size_3) := -1 * LF_amount;
               LP_b_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_n_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_cancel_amt_tbl_3(LP_otb_size_3)     := LF_amount;
               ---
            elsif OTB_SQL.LP_order_type = 'BRB' then
               ---
               LP_otb_size_3                    := LP_otb_size_3 + 1;
               ---
               LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
               LP_class_tbl_3(LP_otb_size_3)    := LF_class;
               LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
               LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
               ---
               LP_a_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_b_approved_amt_tbl_3(LP_otb_size_3) := -1 * LF_amount;
               LP_n_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_cancel_amt_tbl_3(LP_otb_size_3)     := LF_amount;
               ---
            elsif OTB_SQL.LP_order_type = 'N/B' then
               ---
               LP_otb_size_3                    := LP_otb_size_3 + 1;
               ---
               LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
               LP_class_tbl_3(LP_otb_size_3)    := LF_class;
               LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
               LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
               ---
               LP_a_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_b_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_n_approved_amt_tbl_3(LP_otb_size_3) := -1 * LF_amount;
               LP_cancel_amt_tbl_3(LP_otb_size_3)     := LF_amount;
               ---
            end if; --- order_type
            ---
         else --- order_status != A
            ---
            LP_otb_size_3                    := LP_otb_size_3 + 1;
            ---
            LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
            LP_class_tbl_3(LP_otb_size_3)    := LF_class;
            LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
            LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
            ---
            LP_a_approved_amt_tbl_3(LP_otb_size_3) := 0;
            LP_b_approved_amt_tbl_3(LP_otb_size_3) := 0;
            LP_n_approved_amt_tbl_3(LP_otb_size_3) := 0;
            LP_cancel_amt_tbl_3(LP_otb_size_3)     := LF_amount;
            ---
         end if; --- LP_order_status

      elsif I_cancel_reinstate_flag = 'R' and LF_amount > 0 then

         if OTB_SQL.LP_order_status = 'A' then
            if OTB_SQL.LP_order_type = 'ARB' then
               ---
               LP_otb_size_3                    := LP_otb_size_3 + 1;
               ---
               LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
               LP_class_tbl_3(LP_otb_size_3)    := LF_class;
               LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
               LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
               ---
               LP_a_approved_amt_tbl_3(LP_otb_size_3) := LF_amount;
               LP_b_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_n_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_cancel_amt_tbl_3(LP_otb_size_3)     := -1 * LF_amount;
               ---
            elsif OTB_SQL.LP_order_type = 'BRB' then
               ---
               LP_otb_size_3                    := LP_otb_size_3 + 1;
               ---
               LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
               LP_class_tbl_3(LP_otb_size_3)    := LF_class;
               LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
               LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
               ---
               LP_a_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_b_approved_amt_tbl_3(LP_otb_size_3) := LF_amount;
               LP_n_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_cancel_amt_tbl_3(LP_otb_size_3)     := -1 * LF_amount;
               ---
            elsif OTB_SQL.LP_order_type = 'N/B' then
               ---
               LP_otb_size_3                    := LP_otb_size_3 + 1;
               ---
               LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
               LP_class_tbl_3(LP_otb_size_3)    := LF_class;
               LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
               LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
               ---
               LP_a_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_b_approved_amt_tbl_3(LP_otb_size_3) := 0;
               LP_n_approved_amt_tbl_3(LP_otb_size_3) := LF_amount;
               LP_cancel_amt_tbl_3(LP_otb_size_3)     := -1 * LF_amount;
               ---
            end if; --- order_type

         else --- order_status != A
            ---
            LP_otb_size_3                    := LP_otb_size_3 + 1;
            ---
            LP_dept_tbl_3(LP_otb_size_3)     := LF_dept;
            LP_class_tbl_3(LP_otb_size_3)    := LF_class;
            LP_subclass_tbl_3(LP_otb_size_3) := LF_subclass;
            LP_eow_date_tbl_3(LP_otb_size_3) := OTB_SQL.LP_order_eow_date;
            ---
            LP_a_approved_amt_tbl_3(LP_otb_size_3) := 0;
            LP_b_approved_amt_tbl_3(LP_otb_size_3) := 0;
            LP_n_approved_amt_tbl_3(LP_otb_size_3) := 0;
            LP_cancel_amt_tbl_3(LP_otb_size_3)     := -1 * LF_amount;
            ---
         end if; --- LP_order_status
         ---
      end if; -- cancel_reinstate flag

      return TRUE;

   EXCEPTION
      when OTHERS then
         O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM,
                                               L_program, to_char(SQLCODE));
         return FALSE;

   END ORD_CANC_REIN_ITEM;
   -------------------------------------------------------------------
BEGIN -- BUILD_ORD_CANCEL_REINSTATE
   --populate package variables
   if OTB_SQL.ORD_INFO(I_order_no,
                       O_error_message) = FALSE then
      return FALSE;
   end if;

   if OTB_SQL.LP_order_type != 'DSD' then
      --get landed cost indicator
      if SYSTEM_OPTIONS_SQL.GET_ELC_IND(O_error_message,
                                        LP_elc_ind) = FALSE then
         return FALSE;
      end if;
      if ITEM_ATTRIB_SQL.GET_PACK_INDS (O_error_message,
                                        L_pack_ind,
                                        L_sellable_ind,
                                        L_orderable_ind,
                                        L_pack_type,
                                        I_item) = FALSE then
         return FALSE;
      end if;
      --
      if ITEM_COST_SQL.GET_DEFAULT_PO_COST(O_error_message,
                                           L_default_po_cost,
                                           NULL,
                                           I_location) = FALSE then
         return FALSE;
      end if;
      --
      if (L_pack_type is NULL) and (L_pack_ind = 'Y') then
         O_error_message := SQL_LIB.CREATE_MSG('INV_PAK', 'Pack No ='||(I_item), NULL, NULL);
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('OPEN', 'C_ITEM_UNIT_COST', 'ORDSKU', 'item '||(I_item)||' order_no '||to_char(I_order_no));
      open C_ITEM_UNIT_COST;

      SQL_LIB.SET_MARK('FETCH', 'C_ITEM_UNIT_COST', 'ORDSKU', 'item '||(I_item)||' order_no '||to_char(I_order_no));
      fetch C_ITEM_UNIT_COST into L_unit_cost,
                                  LP_origin_country;

      if C_ITEM_UNIT_COST%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_UNIT_COST', 'ORDSKU', 'item '||(I_item)||' order_no '||to_char(I_order_no));
         close C_ITEM_UNIT_COST;
         return FALSE;
      end if;

      SQL_LIB.SET_MARK('CLOSE', 'C_ITEM_UNIT_COST', 'ORDSKU', 'item '||(I_item)||' order_no '||to_char(I_order_no));
      close C_ITEM_UNIT_COST;

      if L_pack_type is not NULL then --- I_item is a pack item

         SQL_LIB.SET_MARK('OPEN', 'CP_SUM_COMP_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY', 'Supplier: '||to_char(LP_supplier)||' Origin Country: '||LP_origin_country||' Item '||(I_item));
         open CP_SUM_COMP_COST(I_item);

         SQL_LIB.SET_MARK('FETCH', 'CP_SUM_COMP_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY', 'Supplier: '||to_char(LP_supplier)||' Origin Country: '||LP_origin_country||' Item '||(I_item));
         fetch CP_SUM_COMP_COST into L_sum_cost;

         if CP_SUM_COMP_COST%NOTFOUND then
            O_error_message := SQL_LIB.CREATE_MSG('INV_ITEM_SUP', NULL, NULL, NULL);
            SQL_LIB.SET_MARK('CLOSE', 'CP_SUM_COMP_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY', 'Supplier: '||to_char(LP_supplier)||' Origin Country: '||LP_origin_country||' Item '||(I_item));
            close CP_SUM_COMP_COST;
            return FALSE;
         end if;

         SQL_LIB.SET_MARK('CLOSE', 'CP_SUM_COMP_COST', 'ITEM_SUPP_COUNTRY, V_PACKSKU_QTY', 'Supplier: '||to_char(LP_supplier)||' Origin Country: '||LP_origin_country||' Item '||(I_item));
         close CP_SUM_COMP_COST;

         FOR packitem_rec in C_packitem LOOP
            L_comp_item := packitem_rec.item;
            L_comp_qty  := packitem_rec.qty;
            L_comp_cost := packitem_rec.unit_cost;

            --- get landed cost of whole vendor pack for use later in
            --- prorating landed cost for component items
            if L_pack_type = 'V' then
               -- retrieve the landed cost for the item in primary currency
               if not ELC_CALC_SQL.CALC_TOTALS(O_error_message,
                                               L_vend_lc_ord,      --total_elc
                                               L_expenses_ord,     --total_exp
                                               L_exp_currency,     --exp_curr
                                               L_exchange_rate_exp,--exch_rate
                                               L_duty_ord,         --total_duty
                                               L_dty_currency,     --dty_curr
                                               I_order_no,         --ord_no
                                               I_item,              --item
                                               NULL,               --comp_item
                                               NULL,               --zone_id
                                               I_location,         --loc
                                               LP_supplier,        --supp
                                               LP_origin_country,  --orig_cntry
                                               LP_import_country,  --import_cntry
                                               L_unit_cost) then   --cost
                  return FALSE;
               end if;
            end if;

            --- call ord_canc_rein_item with prorated unit_cost for component
            if ORD_CANC_REIN_ITEM(L_comp_item,
                                  I_item,
                                  L_unit_cost) = FALSE then
               return FALSE;
            end if;

         end LOOP; --- packitem_rec

      else --- I_item is not a pack
         -- L_comp_qty is used in the calculation for the total amount,
         -- so it needs to be populated even when not used
         L_comp_qty := 1;

         if ORD_CANC_REIN_ITEM(I_item,
                               NULL,
                               L_unit_cost) = FALSE then
            return FALSE;
         end if;
      end if; -- I_item is/not pack
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',SQLERRM,
                                            L_program, to_char(SQLCODE));
      return FALSE;

END BUILD_ORD_CANCEL_REINSTATE;
-----------------------------------------------------------------------------
END OTB_SQL;
/