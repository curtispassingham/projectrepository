CREATE OR REPLACE PACKAGE PROCUREMENT_SQL AUTHID CURRENT_USER IS
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrcost (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF
  -- Purpose:   Validate that the diff is a valid diff and  populate the description field
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_CNTRCOST(O_error_message         IN OUT   VARCHAR2,
							  I_TI_ITEM               IN       VARCHAR2,
							  I_TI_ITEM_GRANDPARENT   IN       VARCHAR2,
							  I_TI_ITEM_PARENT        IN       VARCHAR2,
							  P_PM_CONTRACT           IN       NUMBER,
							  O_diff_desc             IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
							  I_diff_id               IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
							  I_diff_no               IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrcost (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF_GROUP
  -- Purpose:   Validate that the diff group is a valid diff group and populate the description field
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_GROUP_CNTRCOST(O_error_message     IN OUT   VARCHAR2,
									P_PM_CONTRACT       IN       NUMBER,
									O_diff_group_desc   IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
									I_diff_group_id     IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
									I_diff_no           IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrdetl (Form Module) 
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_CNTRDETL(O_error_message         IN OUT   VARCHAR2,
							  I_TI_ITEM               IN       VARCHAR2,
							  I_TI_ITEM_GRANDPARENT   IN       VARCHAR2,
							  I_TI_ITEM_PARENT        IN       VARCHAR2,
							  P_PM_CONTRACT           IN       NUMBER,
							  O_diff_desc             IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
							  I_diff_id               IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
							  I_diff_no               IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrdetl (Form Module) 
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF_GROUP
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_GROUP_CNTRDETL(O_error_message     IN OUT   VARCHAR2,
									P_PM_CONTRACT       IN       NUMBER,
									O_diff_group_desc   IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
									I_diff_group_id     IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
									I_diff_no           IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrdetl (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_LOCATION
  -- Purpose: To ensure that location is valid
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_LOCATION_CNTRDETL(O_error_message         IN OUT   VARCHAR2,
								  I_LI_LOC_TYPE           IN  	   VARCHAR2,
								  I_TI_ITEM               IN       VARCHAR2,
								  I_TI_ITEM_GRANDPARENT   IN       VARCHAR2,
								  I_TI_ITEM_PARENT        IN       VARCHAR2,
								  I_TI_LOCATION           IN       NUMBER,
								  I_TI_LOC_DESC           IN OUT   VARCHAR2,
								  P_PM_CONTRACT           IN       NUMBER)
  return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrdist (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF
  -- Purpose:   Validate that the diff is a valid diff and populate the description field
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_CNTRDIST(O_error_message         IN OUT   VARCHAR2,
							  I_TI_ITEM               IN       VARCHAR2,
							  I_TI_ITEM_GRANDPARENT   IN       VARCHAR2,
							  I_TI_ITEM_PARENT        IN       VARCHAR2,
							  P_PM_CONTRACT           IN       NUMBER,
							  O_diff_desc             IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
							  I_diff_id               IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
							  I_diff_no               IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrdist (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_DIFF_GROUP
  -- Purpose:   Validate that the diff group is a valid diff group and populate the description field
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_DIFF_GROUP_CNTRDIST(O_error_message     IN OUT   VARCHAR2,
									P_PM_CONTRACT       IN       NUMBER,
									O_diff_group_desc   IN OUT   V_DIFF_ID_GROUP_TYPE.DESCRIPTION%TYPE,
									I_diff_group_id     IN       V_DIFF_ID_GROUP_TYPE.ID_GROUP%TYPE,
									I_diff_no           IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : cntrdist (Form Module)
  -- Source Object            : PROCEDURE  -> VALIDATE_CNTR_FILTER.FILTER_LOCATION
  -- Purpose: To ensure that location is valid
  ---------------------------------------------------------------------------------------------
FUNCTION FILTER_LOCATION_CNTRDIST(O_error_message         IN OUT   VARCHAR2,
								  I_LI_LOC_TYPE           IN       VARCHAR2,
								  I_TI_ITEM               IN       VARCHAR2,
								  I_TI_ITEM_GRANDPARENT   IN       VARCHAR2,
								  I_TI_ITEM_PARENT        IN       VARCHAR2,
								  I_TI_LOCATION           IN       NUMBER,
								  I_TI_LOC_DESC           IN OUT   VARCHAR2,
								  P_PM_CONTRACT           IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> ZN_FORM_VALIDATION.ZONE_ID
  -- Purpose: Validates the B_CZONE.ZONE_ID field (does not exist - for example purposes only).
  ---------------------------------------------------------------------------------------------
FUNCTION ZONE_ID(O_error_message      IN OUT   VARCHAR2,
				 I_ZONE_ID            IN       NUMBER,
				 I_TI_ZONE_GROUP_ID   IN       NUMBER,
				 P_ZnFrmVldtn         IN OUT   VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> P_NUMB_LOC
  ---------------------------------------------------------------------------------------------
FUNCTION NUMB_LOC(O_error_message    IN OUT   VARCHAR2,
				  I_TI_NUMB_OF_LOC   IN OUT   NUMBER,
				  I_ZONE_ID          IN       NUMBER,
				  I_ZONE_GROUP_ID    IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> P_LIKE_GROUP
  ---------------------------------------------------------------------------------------------
FUNCTION LIKE_GROUP_COSTZONE(O_error_message   IN OUT   VARCHAR2,
							 G_CURRENCY_CODE   IN       VARCHAR2,
							 I_DESCRIPTION     IN       VARCHAR2,
							 I_TI_LIKE_GROUP   IN       NUMBER,
							 I_ZONE_GROUP_ID   IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> P_INSERT_DAILY_PURGE
  ---------------------------------------------------------------------------------------------
FUNCTION INSERT_DAILY_PURGE(O_error_message   IN OUT   VARCHAR2,
                            I_ZONE_GROUP_ID   IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> LOC_FORM_VALIDATION.LOCATION
  -- Purpose: Validates the B_CZONELOC.LOCATION field (does not exist - for example purposes only).
  --------------------------------------------------------------------------------------------
FUNCTION LOCATION(O_error_message      IN OUT   VARCHAR2,
				  I_LOCATION           IN OUT   NUMBER,
				  I_LOC_TYPE           IN       VARCHAR2,
				  I_TI_LOC_NAME        IN OUT   VARCHAR2,
				  I_TI_CURRENCY_CODE   IN       VARCHAR2,
				  I_TI_ZONE_GROUP_ID   IN       NUMBER,
				  I_TI_ZONE_ID         IN       NUMBER,
				  P_LcFrmVldtn         IN OUT   VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> GRP_FORM_VALIDATION.ZONE_GROUP_ID
  -- Purpose: Validates the B_CZONEGRP.ZONE_GROUP_ID field (does not exist - for example purposes only).
  ---------------------------------------------------------------------------------------------
FUNCTION ZONE_GROUP_ID(O_error_message   IN OUT   VARCHAR2,
					   I_ZONE_GROUP_ID   IN       NUMBER,
					   I_ROWID           IN       ROWID,
					   P_GrpFrmVldtn     IN OUT   VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> GRP_FORM_VALIDATION.LIKE_GROUP
  -- Purpose: Validates the B_CZONEGRP.TI_LIKE_GROUP field
  ---------------------------------------------------------------------------------------------
FUNCTION LIKE_GROUP(O_error_message   IN OUT   VARCHAR2,
					I_TI_LIKE_GROUP   IN       NUMBER,
					I_ZONE_GROUP_ID   IN       NUMBER,
					P_GrpFrmVldtn     IN OUT   VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : costzone (Form Module)
  -- Source Object            : PROCEDURE  -> P_UPDATE_ZN_LOC
  ---------------------------------------------------------------------------------------------
FUNCTION UPDATE_ZN_LOC(O_error_message             IN OUT   VARCHAR2,
					   I_TI_PRIMARY_DISCHARGE_PO   IN       VARCHAR2,
					   I_ZONE_ID                   IN       NUMBER,
					   I_TI_ZONE_GROUP_ID          IN       NUMBER)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : ordfind (Form Module)
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.VENDOR_ORDER_NO
  ---------------------------------------------------------------------------------------------
FUNCTION VENDOR_ORDER_NO(O_error_message          IN OUT   VARCHAR2,
						 I_TI_SUPPLIER            IN       NUMBER,
						 I_TI_SUPPLIER_SITE       IN       NUMBER,
						 I_TI_VENDOR_ORDER_NO     IN       VARCHAR2,
						 P_IntrnlVrblsOptnsRcrd   IN       VARCHAR2)
  return BOOLEAN;
  ---------------------------------------------------------------------------------------------
  -- Module                   : ordhead (Form Module)
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.SHIP_METHOD
  ---------------------------------------------------------------------------------------------
FUNCTION SHIP_METHOD(O_error_message         IN OUT   VARCHAR2,
					 I_SHIP_METHOD           IN       VARCHAR2,
					 I_TI_SHIP_METHOD_DESC   IN OUT   VARCHAR2,
					 P_PM_MODE               IN       VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : ordhead (Form Module)
  -- Source Object            : PROCEDURE  -> FORM_VALIDATION.SHIP_PAY_METHOD
  ---------------------------------------------------------------------------------------------
FUNCTION SHIP_PAY_METHOD(O_error_message      IN OUT   VARCHAR2,
						 I_SHIP_PAY_METHOD    IN       VARCHAR2,
						 I_TI_SHIP_PAY_DESC   IN OUT   VARCHAR2,
						 P_PM_MODE            IN       VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : ordhead (Form Module)
  -- Source Object            : PROCEDURE  -> P_CHECK_UNRECEIVED_QTY
  ---------------------------------------------------------------------------------------------
FUNCTION CHECK_UNRECEIVED_QTY(O_error_message                IN OUT   VARCHAR2,
							  I_ORDER_NO                     IN       NUMBER,
							  P_IntrnlVrblsGvUnrcvdQtyExst   IN OUT   VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : recctadj (Form Module)
  -- Source Object            : PROCEDURE  -> P_NEW_COST
  ---------------------------------------------------------------------------------------------
FUNCTION NEW_COST(O_error_message               IN OUT   VARCHAR2,
				  I_CB_ADJ_MATCHED_RCPT         IN       VARCHAR2,
				  I_TI_COST_NEW_ORD             IN       NUMBER,
				  I_TI_COST_OLD_ORD             IN       NUMBER,
				  I_LOCATION                    IN       NUMBER,
				  I_LOC_TYPE                    IN       VARCHAR2,
				  I_TI_LOCAL_CURRENCY           IN       VARCHAR2,
				  I_TI_ITEM                     IN       VARCHAR2,
				  I_TI_ORDER_NO                 IN       NUMBER,
				  I_TI_SUPPLIER                 IN       NUMBER,
				  P_IntrnlVrblsGpAlcStts        IN       ALC_HEAD.STATUS%TYPE,
				  P_IntrnlVrblsGpElcInd         IN       VARCHAR2,
				  P_IntrnlVrblsGpImprtCntryId   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
				  P_IntrnlVrblsGpImprtInd       IN       VARCHAR2,
				  P_IntrnlVrblsGpImprtOrdrInd   IN       VARCHAR2,
				  P_IntrnlVrblsGpItmLvl         IN       ITEM_MASTER.ITEM_LEVEL%TYPE,
				  P_IntrnlVrblsGpOrgnCntryId    IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
				  P_IntrnlVrblsGpPckInd         IN       ITEM_MASTER.PACK_IND%TYPE,
				  P_IntrnlVrblsGpPckType        IN       ITEM_MASTER.PACK_TYPE%TYPE,
				  P_IntrnlVrblsGpPcDffOrd       IN OUT   ORDLOC.UNIT_COST%TYPE,
				  P_IntrnlVrblsGpPrmCrrncy      IN       SYSTEM_OPTIONS.CURRENCY_CODE%TYPE,
				  P_IntrnlVrblsGpTrnlvl         IN       ITEM_MASTER.TRAN_LEVEL%TYPE,
				  P_IntrnlVrblsGpOrdType        IN       ITEM_MASTER.ORDER_AS_TYPE%TYPE)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : suppsku (Form Module)
  -- Source Object            : FUNCTION   -> F_MIN_DATE
  ---------------------------------------------------------------------------------------------
FUNCTION MIN_DATE(O_error_message       IN OUT   VARCHAR2,
				  O_ret                 IN OUT   DATE,
				  P_IntrnlVrblsGvVdte   IN      PERIOD.VDATE%TYPE)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : timeline (Form Module)
  -- Source Object            : FUNCTION   -> FORM_VALIDATION.VALIDATE_TMLN_STEPS
  ---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TMLN_STEPS(I_timeline_no     IN       timeline_steps.timeline_no%TYPE,
							 I_timeline_type   IN       timeline_head.timeline_type%TYPE,
							 O_exists          IN OUT   VARCHAR2,
							 O_error_message   IN OUT   VARCHAR2)
  return BOOLEAN ;
  ---------------------------------------------------------------------------------------------
  -- Module                   : timeline (Form Module)
  -- Source Object            : FUNCTION   -> FORM_VALIDATION.VALIDATE_TIMELINE
  ---------------------------------------------------------------------------------------------
FUNCTION VALIDATE_TIMELINE(I_key_value_1     IN       timeline.key_value_1%TYPE,
						   I_key_value_2     IN       timeline.key_value_2%TYPE,
						   I_timeline_no     IN       timeline.timeline_no%TYPE,
						   O_exists          IN OUT   VARCHAR2,
						   O_error_message   IN OUT   VARCHAR2)
  return BOOLEAN ;
END PROCUREMENT_SQL;
/