
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_RECALC_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------
--Function Name  : RECALC_ORDER
--Purpose        : This function determines whether the order is to
--                 be recalced for replenishment, rescaled to 
--                 constraints, or both. Once it makes its 
--                 determination, it calls the appropriate internal function 
--                 to perform the actual recalculation. The recalc type
--                 input parameter will determine what type of recalculation
--                 is to be performed. Valid values for this input parameter
--                 are 'R' for recalc replenishment, 'C' for redo scaling to 
--                 constraints, and 'B' for perform both types of recalculation.
--                 In the case of both, replenishment will first be recalculated 
--                 and then the new replenishment order will be scaled.
--
--                 Recalculation of replenishment will send indicated item/locations
--                 back through the forecasting algorithms to generate new recommended 
--                 order qunatities. Whether or not an item/location is recalculated
--                 is indicated by the recalc_type column on the repl_results table.
--                 Based on the new recommended order quantities, the existing order 
--                 will be modified to reflect the new replenishment results. This means 
--                 that item/location order quantities can either be decreased or 
--                 increased and that item/locations may either be added or deleted from 
--                 the order.  Parameters to be used in the recalculation of an indicated 
--                 item/location are retrieved from the repl_results table. 
--
--                 Recalculation of Constraints will be performed by the function xxxx which
--                 is not contained within this module. That function will update the order
--                 appropriately after it has finished its scaling recalculations. If for whatever
--                 reason scaling was not performed successfully, the recalc_order function will 
--                 return a non-fatal error meaning that the function will return TRUE with an
--                 error message. The parameter O_scaled_ind will be FALSE in this case to indicate
--                 that re-scaling was not performed. However, this parameter will also be FALSE if no
--                 rescaling was attempted due to the recalc_type input parameter being 'R'. This 
--                 module will also allow the undo scaling function to undo scaling up to this this 
--                 point by copying the current order to temp tables and then scaling. If the user
--                 then wishes to undo scaling, the temp tables will be used to undo the scaling process.
--Initial Author  : Norm Long
--Initial Creation: 9/99 
-------------------------------------------------------------------
FUNCTION RECALC_ORDER(O_error_message      IN OUT VARCHAR2,
                      O_scaled_ind         IN OUT BOOLEAN,
                      I_order_no           IN     ORDHEAD.ORDER_NO%TYPE,
                      I_recalc_type        IN     VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------
--Function Name  : UNDO_SCALING
--Purpose        : This function will 'undo' any scaling logic that has been
--                 performed on an order. It will attempt to accomplish this 
--                 by deleting the order and then recreating it from already
--                 populated temp tables. These temp tables are shared by 
--                 redistribution functionality. Therefore, if an order has been
--                 redistributed, undoing the scaling is no longer possible. In
--                 that case, there will be no information on the temp tables 
--                 to use to rebuild the order. If for any reason undoing of scaling
--                 cannot or was not accomplished by this function, the function
--                 will return FALSE.
--Initial Author  : Norm Long
--Initial Creation: 9/99 
-------------------------------------------------------------------
FUNCTION UNDO_SCALING(O_error_message   IN OUT VARCHAR2,
                      I_order_no        IN     ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------
END ORDER_RECALC_SQL;
/