
CREATE OR REPLACE PACKAGE BODY FUTURE_COST_EVENT_SQL AS
-------------------------------------------------------------------------------------------------------
-- Local Package Variables
-------------------------------------------------------------------------------------------------------
LP_ind_y                  VARCHAR2(1)  := 'Y';
LP_ind_n                  VARCHAR2(1)  := 'N';
-------------------------------------------------------------------------------------------------------
   -- PRIVATE FUNCTIONS
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- Function Name:  GET_NEXT_COST_EVENT_SEQ
-- Purpose      :  This function is called by INSERT_COST_EVENT function.
--                 It gets the next available cost event sequence number.
--------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_COST_EVENT_SEQ(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  INSERT_COST_EVENT
-- Purpose      :  This function is called by Cost Events functions.
--                 It inserts cost event header information into the cost_event table.
--------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_action                 IN     COST_EVENT.ACTION%TYPE,
                           I_event_type             IN     COST_EVENT.EVENT_TYPE%TYPE,
                           I_persist_ind            IN     COST_EVENT.PERSIST_IND%TYPE,
                           I_user                   IN     COST_EVENT.USER_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  ENQUEUE_COST_EVENT
-- Purpose      :  Whenever the cost_engine is run in ASYNC mode this function
--                 enqueues the message for dequeuing it later.
-------------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name:  VALIDATE_COST_EVENT
-- Purpose      :  This function is called by ADD_COST_CHANGE_EVENT and  ADD_DEALS functions.
--                 This function validates the cost event if the current event requires processing
--                 when event run type set to 'BATCH' mode.
--                 e.g - For a cost change we have ADD REM and ADD event on the same day .
--                 So there is no need to process the initial ADD and REM events as those are not
--                 yet processed by the future cost engine.
--------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id  IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_action                 IN     COST_EVENT.ACTION%TYPE,
                             I_event_type             IN     COST_EVENT.EVENT_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             I_cost_event_process_id  IN COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_action                 IN     COST_EVENT.ACTION%TYPE,
                             I_event_type             IN     COST_EVENT.EVENT_TYPE%TYPE)

RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.VALIDATE_COST_EVENT';
   TYPE cost_event_tbl_type IS TABLE OF COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_cost_event_process_id_tbl cost_event_tbl_type := cost_event_tbl_type();

   -- select the cost events if there is a pending ADD event.
   -- MOD events does not exists cost change.
   cursor check_process_cc is
      with process_id
       as (select cec2.cost_event_process_id
             from cost_event_cost_chg cec1,
                  cost_event_cost_chg cec2
            where cec1.cost_event_process_id = I_cost_event_process_id
              and cec2.cost_change = cec1.cost_change
              and cec2.cost_event_process_id <> I_cost_event_process_id
              and cec2.cost_event_process_id < I_cost_event_process_id)
         select ce.cost_event_process_id
           from cost_event ce,
                process_id ps
          where ce.cost_event_process_id = ps.cost_event_process_id
            -- Assumption is , Pending events will not have a row in cost_event_result table
            and not exists ( select 'x'
                               from cost_event_result cer
                              where cer.cost_event_process_id = ps.cost_event_process_id)
            and ce.action = FUTURE_COST_EVENT_SQL.ADD_EVENT
         union
         -- select the currect process id
         select I_cost_event_process_id
           from dual ;

   -- For deals only select cost events if there is a pending ADD event.
   -- If there is only pending MOD events then no need to mark the REM event as complete.
   cursor check_process_deal is
      with process_id
       as (select ced2.cost_event_process_id
             from cost_event_deal ced1,
                  cost_event_deal ced2
            where ced1.cost_event_process_id = I_cost_event_process_id
              and ced2.deal_id = ced1.deal_id
              and ced2.cost_event_process_id <> I_cost_event_process_id
              and ced2.cost_event_process_id < I_cost_event_process_id)
         select cost_event_process_id
           from (select ce.cost_event_process_id,
                        first_value(ce.action) over(order by ce.action) first_value
                   from cost_event ce,
                        process_id ps
                  where ce.cost_event_process_id = ps.cost_event_process_id
                    -- Assumption is , Pending events will not have a row in cost_event_result table
                    and not exists ( select 'x'
                                       from cost_event_result cer
                                      where cer.cost_event_process_id = ps.cost_event_process_id)
                    and ce.action in ( FUTURE_COST_EVENT_SQL.ADD_EVENT,
                                       FUTURE_COST_EVENT_SQL.MODIFY_EVENT))
            where first_value = FUTURE_COST_EVENT_SQL.ADD_EVENT
         union
         -- select the currect process id
         select I_cost_event_process_id
           from dual ;

BEGIN
   case I_event_type

   when FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE then
      if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
         --
         open check_process_cc;
         fetch check_process_cc bulk collect into L_cost_event_process_id_tbl;
         close check_process_cc;
         --
      end if;
   when FUTURE_COST_EVENT_SQL.DEAL_COST_EVENT_TYPE then
      if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
         ---
         open check_process_deal;
         fetch check_process_deal bulk collect into L_cost_event_process_id_tbl;
         close check_process_deal;
         ---
      end if;
   end case;
   --- If no previous process ids found then exit
   if L_cost_event_process_id_tbl.COUNT = 1 then
      return TRUE;
   end if;
   -- Write results to mark processes are complete
   -- As no need to process consecutive ADD and REM events
   -- If ADD event is still pending.
   forall i in 1 .. L_cost_event_process_id_tbl.count
      insert into cost_event_result
         (cost_event_process_id,
          thread_id,
          attempt_num,
          status,
          retry_user_id,
          error_message,
          create_datetime)
      values
         (L_cost_event_process_id_tbl(i),
          0,
          0,
          FUTURE_COST_EVENT_SQL.COMPLETE_COST_EVENT,
          get_user,
          NULL,
          sysdate);
return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END VALIDATE_COST_EVENT;
-------------------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_COST_EVENT_SEQ(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS
   L_program               VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.GET_NEXT_COST_EVENT_SEQ';
   L_exists                VARCHAR2(1)  := 'N';
   L_cost_event_process_id COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   L_wrap_seq_no           COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
   SEQ_MAXVAL              EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(SEQ_MAXVAL,-08004);
   cursor C_NEXT_SEQ is
      select cost_event_process_id_seq.nextval
        from dual;
   cursor C_SEQ_EXISTS is
      select 'Y'
        from cost_event
       where cost_event_process_id = L_cost_event_process_id
         and rownum = 1;
BEGIN
   open C_NEXT_SEQ;
   fetch C_NEXT_SEQ into L_cost_event_process_id;
   close C_NEXT_SEQ;
-- Save the first sequence value fetched.
   L_wrap_seq_no := L_cost_event_process_id;
   LOOP
      -- See if the value is already in use.  If it is not in use then
      -- it is valid and we return.
      L_exists := 'N';
      open C_SEQ_EXISTS;
      fetch C_SEQ_EXISTS into L_exists;
      close C_SEQ_EXISTS;
      --
      if L_exists = 'N' then
         O_cost_event_process_id := L_cost_event_process_id;
         return TRUE;
      end if;
      -- The value was already in use so fetch the next sequence value.
      open C_NEXT_SEQ;
      fetch C_NEXT_SEQ into L_cost_event_process_id;
      close C_NEXT_SEQ;
      -- If the sequence cycled (looped around) and we are back to
      -- the first sequence value selected, then return with an error
      -- since all sequence values are already in use.
      if L_cost_event_process_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_COST_EVENT_ID',NULL,NULL,NULL);
         return FALSE;
      end if;
   END LOOP;
EXCEPTION
   -- If we tried to select from a sequence which has reached its
   -- maximum value then return an error.
   when SEQ_MAXVAL then
      if C_NEXT_SEQ%ISOPEN then
         close C_NEXT_SEQ;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('NO_COST_EVENT_ID',NULL,NULL,NULL);
      return FALSE;
   when OTHERS then
     if C_NEXT_SEQ%ISOPEN then
         close C_NEXT_SEQ;
      end if;
      if C_SEQ_EXISTS%ISOPEN then
         close C_SEQ_EXISTS;
      end if;
      ---
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END GET_NEXT_COST_EVENT_SEQ;
-------------------------------------------------------------------------------------------------------
FUNCTION INSERT_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_action                 IN     COST_EVENT.ACTION%TYPE,
                           I_event_type             IN     COST_EVENT.EVENT_TYPE%TYPE,
                           I_persist_ind            IN     COST_EVENT.PERSIST_IND%TYPE,
                           I_user                   IN     COST_EVENT.USER_ID%TYPE)
RETURN BOOLEAN IS
L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.INSERT_COST_EVENT';
BEGIN
   if GET_NEXT_COST_EVENT_SEQ(O_error_message,
                              O_cost_event_process_id) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event (cost_event_process_id,
                           action,
                           event_type,
                           persist_ind,
                           user_id,
                           create_datetime,
                           override_run_type)
                   values (O_cost_event_process_id,
                           I_action,
                           I_event_type,
                           I_persist_ind,
                           I_user,
                           sysdate,
                           FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE);
return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INSERT_COST_EVENT;
-------------------------------------------------------------------------------------------------------
   -- PUBLIC FUNCTIONS
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_NIL(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_cost_event_process_id      OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                 I_item_locs               IN     OBJ_ITEMLOC_TBL,
                 I_user                    IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                 I_override_event_run_type IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_NIL';
L_exists  VARCHAR2(1)  := 'N';

cursor C_ORDERABLE_ITEM is
   select 'Y'
     from table(cast(I_item_locs as OBJ_ITEMLOC_TBL)) input,
          item_master im
    where input.item = im.item
      and ( im.orderable_ind = 'Y'
           or (im.orderable_ind = 'N' and exists (select 1 from item_supplier isup where isup.item = im.item))
           )
      and rownum = 1;
      
BEGIN
   --Validation
   FOR i in I_item_locs.first..I_item_locs.last LOOP
      if I_item_locs(i).item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item_locs.Item', L_program);
         return FALSE;
      end if;
      if I_item_locs(i).loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_item_locs.Loc', L_program);
         return FALSE;
      end if;
   end LOOP;
   ---
   open C_ORDERABLE_ITEM;
   fetch C_ORDERABLE_ITEM into L_exists;
   close C_ORDERABLE_ITEM;
   
   if L_exists='N' then
      return TRUE;
   end if;
   ---
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.NEW_ITEM_LOC_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --

   --bring W/F stores into cost_event_nil, they will be skipped future_cost_thread
   --and brought back into play in future_cost_sql
   insert into cost_event_nil (cost_event_process_id,
                               item,
                               location,
                               loc_type,
                               division,
                               group_no,
                               dept,
                               class,
                               subclass,
                               item_grandparent,
                               item_parent,
                               diff_1,
                               diff_2,
                               diff_3,
                               diff_4,
                               chain,
                               area,
                               region,
                               district,
                               simple_pack_ind,
                               store_type)
      select /*+ ORDERED INDEX(im  pk_item_master) USE_NL(input, IM, D, G) */
             O_cost_event_process_id,
             input.item,
             input.loc,
             org.loc_type,
             g.division,
             g.group_no,
             im.dept,
             im.class,
             im.subclass,
             im.item_grandparent,
             im.item_parent,
             im.diff_1,
             im.diff_2,
             im.diff_3,
             im.diff_4,
             org.chain,
             org.area,
             org.region,
             org.district,
             im.simple_pack_ind,
             org.store_type
        from table(cast(I_item_locs as OBJ_ITEMLOC_TBL)) input,
             item_master im,
             deps d,
             groups g,
             (select a.chain,
                     a.area,
                     r.region,
                     d.district,
                     s.store location,
                     'S' loc_type,
                     s.store_type
                from area a,
                     region r,
                     district d,
                     store s
               where s.district = d.district
                 and d.region   = r.region
                 and r.area     = a.area
              union all
              select NULL chain,
                     NULL area,
                     NULL region,
                     NULL district,
                     wh.wh location,
                     'W' loc_type,
                     NULL store_type
                from wh wh
               where stockholding_ind = 'Y'
                 and (org_hier_type = 1 or org_hier_type = 50 or org_hier_type is null)
              union all
              select wh.org_hier_value chain,
                     NULL area,
                     NULL region,
                     NULL district,
                     wh.wh location,
                     'W' loc_type,
                     NULL store_type
                from wh wh
               where stockholding_ind = 'Y'
                 and org_hier_type = 10
              union all
              select ar.chain chain,
                     wh.org_hier_value area,
                     NULL region,
                     NULL district,
                     wh.wh location,
                     'W' loc_type,
                     NULL store_type
                from wh wh,
                     area ar
               where stockholding_ind = 'Y'
                 and org_hier_type = 20
                 and org_hier_value = ar.area
              union all
              select ar.chain chain,
                     re.area area,
                     org_hier_value region,
                     NULL district,
                     wh.wh location,
                     'W' loc_type,
                     NULL store_type
                from wh wh,
                     area ar,
                     region re
               where stockholding_ind = 'Y'
                 and org_hier_type = 30
                 and org_hier_value = re.region
                 and re.area = ar.area
              union all
              select ar.chain chain,
                     re.area area,
                     di.region region,
                     org_hier_value district,
                     wh.wh location,
                     'W' loc_type,
                     NULL store_type
                from wh wh,
                     area ar,
                     region re,
                     district di
               where stockholding_ind = 'Y'
                 and org_hier_type = 40
                 and org_hier_value = di.district
                 and di.region = re.region
                 and re.area = ar.area) org
       where input.item    = im.item
         and im.dept       = d.dept
         and g.group_no    = d.group_no
         and input.loc     = org.location;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.NEW_ITEM_LOC_COST_EVENT_TYPE,
                        I_override_event_run_type) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_NIL;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_RETAIL_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_item_locs              IN     OBJ_ITEM_LOC_RETAIL_TBL,
                           I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_RETAIL_CHANGE';
   O_excl_unit_retail      ITEM_LOC.UNIT_RETAIL%TYPE;
   L_vdate    DATE            := get_vdate;
BEGIN

   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.RETAIL_CHANGE_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
  
   FOR i in I_item_locs.first..I_item_locs.last LOOP
  
     O_excl_unit_retail := 0;
     
      if TAX_SQL.GET_TAX_EXCLUSIVE_RETAIL(O_error_message,
                                          O_excl_unit_retail,
                                          I_item_locs(i).item, 
                                          I_item_locs(i).dept, 
                                          I_item_locs(i).class,
                                          I_item_locs(i).loc, 
                                          I_item_locs(i).loc_type,
                                          L_vdate,
                                          I_item_locs(i).unit_retail,
                                          1) = FALSE then
                   return FALSE;
       end if;
      
       insert into cost_event_retail_change
                                    (cost_event_process_id,
                                     item,
                                     loc,
                                     loc_type,
                                     unit_retail,
                                     margin_pct,
                                     templ_id,
                                     start_date,
                                     end_date)
                             values (O_cost_event_process_id,
                                     I_item_locs(i).item,
                                     I_item_locs(i).loc,
                                     I_item_locs(i).loc_type,
                                     O_excl_unit_retail,
                                     I_item_locs(i).margin_pct,
                                     I_item_locs(i).templ_id,
                                     I_item_locs(i).start_date,
                                     I_item_locs(i).end_date);
                                     
      
     end LOOP;
   --
  
   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.RETAIL_CHANGE_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_RETAIL_CHANGE;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_CHANGE_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_action                 IN     COST_EVENT.ACTION%TYPE,
                               I_cost_changes           IN     OBJ_CC_COST_EVENT_TBL,
                               I_persist_ind            IN     COST_EVENT.PERSIST_IND%TYPE,
                               I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                               I_over_run_type          IN     COST_EVENT.OVERRIDE_RUN_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT';
   L_event_run_type    COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;

   cursor C_GET_RUN_TYPE is
      select NVL(FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE,event_run_type)
        from cost_event_run_type_config
       where event_type = FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE;

BEGIN
   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE :=  I_over_run_type;
   --Validation
   FOR i in I_cost_changes.first..I_cost_changes.last LOOP
      if I_cost_changes(i).cost_change is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_cost_changes.cost_changes', L_program);
         return FALSE;
      end if;
      if I_cost_changes(i).src_tmp_ind is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_cost_changes.src_tmp_ind', L_program);
         return FALSE;
      end if;
   end LOOP;
   if I_action is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action', L_program);
      return FALSE;
   end if;
   if I_persist_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_persist_ind', L_program);
      return FALSE;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        I_action,
                        FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE,
                        I_persist_ind,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_cost_chg (cost_event_process_id,
                                    cost_change,
                                    src_tmp_ind)
      select O_cost_event_process_id,
             input.cost_change,
             input.src_tmp_ind
        from table(cast(I_cost_changes as OBJ_CC_COST_EVENT_TBL)) input;

   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   if L_event_run_type = FUTURE_COST_EVENT_SQL.BATCH_COST_EVENT_RUN_TYPE and
      I_persist_ind = 'Y' then
      if VALIDATE_COST_EVENT(O_error_message,
                             O_cost_event_process_id,
                             I_action,
                             FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE)= FALSE then
         return FALSE;
      end if;
   end if;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_COST_CHANGE_EVENT;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_RECLASS_EVENT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id      OUT   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_action                  IN       COST_EVENT.ACTION%TYPE,
                           I_reclass_nos             IN       OBJ_RCLS_COST_EVENT_TBL,
                           I_user                    IN       COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61)   := 'FUTURE_COST_EVENT_SQL.ADD_RECLASS_EVENT';

BEGIN
   --Validation
   FOR i in I_reclass_nos.first..I_reclass_nos.last LOOP
      if I_reclass_nos(i) is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_reclass_nos.reclass_nos', L_program);
         return FALSE;
      end if;
   end LOOP;
   if I_action is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action', L_program);
      return FALSE;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        I_action,
                        FUTURE_COST_EVENT_SQL.RECLASS_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_reclass(cost_event_process_id,
                                  reclass_no,
                                  reclass_date,
                                  item,
                                  to_dept,
                                  to_class,
                                  to_subclass)
      select O_cost_event_process_id,
             input.reclass_no,
             input.reclass_date,
             input.item,
             input.to_dept,
             input.to_class,
             input.to_subclass
        from table(cast(I_reclass_nos as OBJ_RCLS_COST_EVENT_TBL)) input;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.RECLASS_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'FUTURE_COST_EVENT_SQL.ADD_RECLASS_EVENT',
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_RECLASS_EVENT;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_DEALS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                   I_action                 IN     COST_EVENT.ACTION%TYPE,
                   I_deal_ids               IN     OBJ_NUMERIC_ID_TABLE,
                   I_persist_ind            IN     COST_EVENT.PERSIST_IND%TYPE,
                   I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_DEALS';
   L_event_run_type    COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;

   cursor C_GET_RUN_TYPE is
      select event_run_type
        from cost_event_run_type_config
       where event_type = FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE;

BEGIN
   --Validation
   FOR i in I_deal_ids.first..I_deal_ids.last LOOP
      if I_deal_ids(i) is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_deal_ids.deal_ids', L_program);
         return FALSE;
      end if;
   END LOOP;
   if I_action is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action', L_program);
      return FALSE;
   end if;
   if I_persist_ind is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_persist_ind', L_program);
      return FALSE;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        I_action,
                        FUTURE_COST_EVENT_SQL.DEAL_COST_EVENT_TYPE,
                        I_persist_ind,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_deal (cost_event_process_id,
                                deal_id)
      select O_cost_event_process_id,
             value(ids)
        from table(cast(I_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids;

   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   if L_event_run_type = FUTURE_COST_EVENT_SQL.BATCH_COST_EVENT_RUN_TYPE  and
      I_persist_ind = 'Y' then
      if VALIDATE_COST_EVENT(O_error_message,
                             O_cost_event_process_id,
                             I_action,
                             FUTURE_COST_EVENT_SQL.DEAL_COST_EVENT_TYPE)= FALSE then
         return FALSE;
      end if;
   end if;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.DEAL_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_DEALS;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_ORG_HIER_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                             O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                             I_old_chain              IN     CHAIN.CHAIN%TYPE,
                             I_old_area               IN     AREA.AREA%TYPE,
                             I_old_region             IN     REGION.REGION%TYPE,
                             I_old_district           IN     DISTRICT.DISTRICT%TYPE,
                             I_new_chain              IN     CHAIN.CHAIN%TYPE,
                             I_new_area               IN     AREA.AREA%TYPE,
                             I_new_region             IN     REGION.REGION%TYPE,
                             I_new_district           IN     DISTRICT.DISTRICT%TYPE,
                             I_store                  IN     STORE.STORE%TYPE,
                             I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_ORG_HIER_CHANGE';
   L_old_chain      CHAIN.CHAIN%TYPE := I_old_chain;
   L_old_area       AREA.AREA%TYPE := I_old_area;
   L_old_region     REGION.REGION%TYPE := I_old_region;
   L_old_district   DISTRICT.DISTRICT%TYPE := I_old_district;
   L_new_chain      CHAIN.CHAIN%TYPE := I_new_chain;
   L_new_area       AREA.AREA%TYPE := I_new_area;
   L_new_region     REGION.REGION%TYPE := I_new_region;
   L_new_district   DISTRICT.DISTRICT%TYPE := I_new_district;

   L_store_type store.store_type%TYPE;

   cursor C_GET_REGION(I_district DISTRICT.DISTRICT%TYPE) is
      select d.region
        from district d
       where d.district = I_district;

   cursor C_GET_AREA(I_region REGION.REGION%TYPE) is
      select r.area
        from district d,
             region r
       where r.region = d.region
         and r.region = I_region;

   cursor C_GET_CHAIN(I_area AREA.AREA%TYPE) is
      select a.chain
        from region r,
             area a
       where r.area = a.area
         and a.area = I_area;

   cursor C_STORE_TYPE is
      select store_type
        from store
       where store = I_store;

BEGIN

   if I_store is NOT NULL then
      open C_STORE_TYPE;
      fetch C_STORE_TYPE into L_store_type;
      close C_STORE_TYPE;
      --
      if L_store_type != 'C' then
         return TRUE;
      end if;
   end if;
   --
   if L_old_district is NOT NULL and
      I_store is NOT NULL then
      open C_GET_REGION(L_old_district);
      fetch C_GET_REGION into L_old_region;
      close C_GET_REGION;
   end if;
   if L_new_district is NOT NULL and I_store is NOT NULL then
      open C_GET_REGION(L_new_district);
      fetch C_GET_REGION into L_new_region;
      close C_GET_REGION;
   end if;
   if L_old_region is NOT NULL and L_old_district is NOT NULL then
      open C_GET_AREA(L_old_region);
      fetch C_GET_AREA into L_old_area;
      close C_GET_AREA;
   end if;
   if L_new_region is NOT NULL and L_new_district is NOT NULL then
      open C_GET_AREA(L_new_region);
      fetch C_GET_AREA into L_new_area;
      close C_GET_AREA;
   end if;
   if L_old_area is NOT NULL and L_old_region is NOT NULL then
      open C_GET_CHAIN(L_old_area);
      fetch C_GET_CHAIN into L_old_chain;
      close C_GET_CHAIN;
   end if;
   if L_new_area is NOT NULL and L_new_region is NOT NULL then
      open C_GET_CHAIN(L_new_area);
      fetch C_GET_CHAIN into L_new_chain;
      close C_GET_CHAIN;
   end if;

   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_org_hier (cost_event_process_id,
                                    old_chain,
                                    old_area,
                                    old_region,
                                    old_district,
                                    new_chain,
                                    new_area,
                                    new_region,
                                    new_district,
                                    location)
      values (O_cost_event_process_id,
              L_old_chain,
              L_old_area,
              L_old_region,
              L_old_district,
              L_new_chain,
              L_new_area,
              L_new_region,
              L_new_district,
              I_store);

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ORG_HIER_CHANGE;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_ORG_HIER_CHANGE_WH(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_old_type          IN     WH.ORG_HIER_TYPE%TYPE,
                                I_old_value         IN     WH.ORG_HIER_VALUE%TYPE,
                                I_new_type          IN     WH.ORG_HIER_TYPE%TYPE,
                                I_new_value         IN     WH.ORG_HIER_VALUE%TYPE,
                                I_wh                IN     WH.WH%TYPE,
                                I_user              IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_ORG_HIER_CHANGE_WH';
   L_old_chain      CHAIN.CHAIN%TYPE := NULL ;
   L_old_area       AREA.AREA%TYPE := NULL;
   L_old_region     REGION.REGION%TYPE := NULL;
   L_old_district   DISTRICT.DISTRICT%TYPE := NULL;
   L_new_chain      CHAIN.CHAIN%TYPE:= NULL ;
   L_new_area       AREA.AREA%TYPE := NULL;
   L_new_region     REGION.REGION%TYPE := NULL;
   L_new_district   DISTRICT.DISTRICT%TYPE := NULL ;

   cursor C_GET_REGION(I_district DISTRICT.DISTRICT%TYPE) is
      select d.region
        from district d
       where d.district = I_district;

   cursor C_GET_AREA(I_region REGION.REGION%TYPE) is
      select r.area
        from district d,
             region r
       where r.region = d.region
         and r.region = I_region;

   cursor C_GET_CHAIN(I_area AREA.AREA%TYPE) is
      select a.chain
        from region r,
             area a
       where r.area = a.area
         and a.area = I_area;


BEGIN

   if I_old_type = 10 then
      L_old_chain := I_old_value;
   elsif I_old_type = 20 then
      L_old_area := I_old_value;
      open C_GET_CHAIN(L_old_area);
      fetch C_GET_CHAIN into L_old_chain;
      close C_GET_CHAIN;
   elsif I_old_type = 30 then
      L_old_region := I_old_value;
      open C_GET_AREA(L_old_region);
      fetch C_GET_AREA into L_old_area;
      close C_GET_AREA;
      ---
      open C_GET_CHAIN(L_old_area);
      fetch C_GET_CHAIN into L_old_chain;
      close C_GET_CHAIN;
   elsif I_old_type = 40 then
      L_old_district := I_old_value;
      open C_GET_REGION(L_old_district);
      fetch C_GET_REGION into L_old_region;
      close C_GET_REGION;
      ---
      open C_GET_AREA(L_old_region);
      fetch C_GET_AREA into L_old_area;
      close C_GET_AREA;
      ---
      open C_GET_CHAIN(L_old_area);
      fetch C_GET_CHAIN into L_old_chain;
      close C_GET_CHAIN;
   end if;

   if I_new_type = 10 then
      L_new_chain := I_new_value;
   elsif I_new_type = 20 then
      L_new_area := I_new_value;
      open C_GET_CHAIN(L_new_area);
      fetch C_GET_CHAIN into L_new_chain;
      close C_GET_CHAIN;
   elsif I_new_type = 30 then
      L_new_region := I_new_value;
      open C_GET_AREA(L_new_region);
      fetch C_GET_AREA into L_new_area;
      close C_GET_AREA;
      ---
      open C_GET_CHAIN(L_new_area);
      fetch C_GET_CHAIN into L_new_chain;
      close C_GET_CHAIN;
   elsif I_new_type = 40 then
      L_new_district := I_new_value;
      open C_GET_REGION(L_new_district);
      fetch C_GET_REGION into L_new_region;
      close C_GET_REGION;
      ---
      open C_GET_AREA(L_new_region);
      fetch C_GET_AREA into L_new_area;
      close C_GET_AREA;
      ---
      open C_GET_CHAIN(L_new_area);
      fetch C_GET_CHAIN into L_new_chain;
      close C_GET_CHAIN;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_org_hier (cost_event_process_id,
                                    old_chain,
                                    old_area,
                                    old_region,
                                    old_district,
                                    new_chain,
                                    new_area,
                                    new_region,
                                    new_district,
                                    location)
      values (O_cost_event_process_id,
              L_old_chain,
              L_old_area,
              L_old_region,
              L_old_district,
              L_new_chain,
              L_new_area,
              L_new_region,
              L_new_district,
              I_wh);

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ORG_HIER_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ORG_HIER_CHANGE_WH;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_MERCH_HIER_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_old_division           IN     DIVISION.DIVISION%TYPE,
                               I_old_group_no           IN     GROUPS.GROUP_NO%TYPE,
                               I_new_division           IN     DIVISION.DIVISION%TYPE,
                               I_new_group_no           IN     GROUPS.GROUP_NO%TYPE,
                               I_dept                   IN     DEPS.DEPT%TYPE,
                               I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_MERCH_HIER_CHANGE';
BEGIN
   -- Validation for division change
   if I_old_group_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_group_no', L_program);
      return FALSE;
   end if;
   ---
   if I_new_group_no is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_group_no', L_program);
      return FALSE;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.MERCH_HIER_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_merch_hier (cost_event_process_id,
                                      old_division,
                                      old_group_no,
                                      new_division,
                                      new_group_no,
                                      dept)
      values (O_cost_event_process_id,
              I_old_division,
              I_old_group_no,
              I_new_division,
              I_new_group_no,
              I_dept);

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.MERCH_HIER_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_MERCH_HIER_CHANGE;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_ZONE_LOC_MOVE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                I_location               IN     ITEM_LOC.LOC%TYPE,
                                I_zone_group_id          IN     COST_ZONE.ZONE_GROUP_ID%TYPE,
                                I_old_zone_id            IN     COST_ZONE.ZONE_ID%TYPE,
                                I_new_zone_id            IN     COST_ZONE.ZONE_ID%TYPE,
                                I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS

   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_COST_ZONE_LOC_MOVE';
   L_store_type store.store_type%TYPE := 'C';

   cursor C_STORE_TYPE is
      select store_type
        from store
       where store = I_location
      union all
      select 'C'
        from wh
       where wh = I_location;

BEGIN

   if I_location is not null then
      open C_STORE_TYPE;
      fetch C_STORE_TYPE into L_store_type;
      close C_STORE_TYPE;
      --
      if L_store_type != 'C' then
         return TRUE;
      end if;
   end if;

   --Validation
   if I_location is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_location', L_program);
      return FALSE;
   end if;
   if I_zone_group_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_zone_group_id', L_program);
      return FALSE;
   end if;
   if I_old_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_old_zone_id', L_program);
      return FALSE;
   end if;
   if I_new_zone_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_new_zone_id', L_program);
      return FALSE;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.COST_ZONE_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_cost_zone (cost_event_process_id,
                                     location,
                                     zone_group_id,
                                     old_zone_id,
                                     new_zone_id)
      values (O_cost_event_process_id,
              I_location,
              I_zone_group_id,
              I_old_zone_id,
              I_new_zone_id);

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.COST_ZONE_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_COST_ZONE_LOC_MOVE;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_ELC_EVENTS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                        I_elc_changes            IN     OBJ_ELC_COST_EVENT_TBL,
                        I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                        I_over_run_type          IN     COST_EVENT.OVERRIDE_RUN_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_ELC_EVENTS';
BEGIN
   FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE :=  I_over_run_type;
   --Validation
   --Null Validation added to handle delete plsql errors when elc_changes is null
   if I_elc_changes is null  or I_elc_changes.count <=0 then
     return true;
   end if;
   
   FOR i in I_elc_changes.first..I_elc_changes.last LOOP
      if I_elc_changes(i).item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_elc_changes.Item', L_program);
         return FALSE;
      end if;
      if I_elc_changes(i).Supplier is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_elc_changes.Supplier', L_program);
         return FALSE;
      end if;
   end LOOP;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.ELC_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_elc (cost_event_process_id,
                               item,
                               supplier,
                               origin_country_id,
                               cost_zone_group,
                               cost_zone)
      select O_cost_event_process_id,
             input.item,
             input.supplier,
             input.origin_country_id,
             input.cost_zone_group,
             input.cost_zone
        from table(cast(I_elc_changes as OBJ_ELC_COST_EVENT_TBL)) input;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ELC_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ELC_EVENTS;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_HIER_CHANGE(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_supp_hier_chgs         IN     OBJ_ISCL_SUPP_HIER_CHG_TBL,
                              I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_SUPP_HIER_CHANGE';

BEGIN

   --Validation
   FOR i in I_supp_hier_chgs.first..I_supp_hier_chgs.last LOOP
      if I_supp_hier_chgs(i).item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_supp_hier_chgs.Item', L_program);
         return FALSE;
      end if;
      if I_supp_hier_chgs(i).Supplier is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_supp_hier_chgs.Supplier', L_program);
         return FALSE;
      end if;
      if I_supp_hier_chgs(i).origin_country_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_supp_hier_chgs.Origin_country_id', L_program);
         return FALSE;
      end if;
      if I_supp_hier_chgs(i).location is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_supp_hier_chgs.Location', L_program);
         return FALSE;
      end if;
   end LOOP;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.SUPP_HIER_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_supp_hier (
             cost_event_process_id,
             item,
             supplier,
             origin_country_id,
             location,
             old_supp_hier_lvl_1,
             old_supp_hier_lvl_2,
             old_supp_hier_lvl_3,
             new_supp_hier_lvl_1,
             new_supp_hier_lvl_2,
             new_supp_hier_lvl_3)
      select O_cost_event_process_id,
             input.item,
             input.supplier,
             input.origin_country_id,
             input.location,
             input.old_supp_hier_lvl_1,
             input.old_supp_hier_lvl_2,
             input.old_supp_hier_lvl_3,
             input.new_supp_hier_lvl_1,
             input.new_supp_hier_lvl_2,
             input.new_supp_hier_lvl_3
        from table(cast(I_supp_hier_chgs as OBJ_ISCL_SUPP_HIER_CHG_TBL)) input,
             (select store as loc, store_type from store
              union all
              select wh as loc, 'C' as store_type from wh) loc
       where input.location = loc.loc
         and loc.store_type = 'C';

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.SUPP_HIER_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_SUPP_HIER_CHANGE;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_ITEM_COST_ZONE_GRP_CHG(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                    I_items                  IN     OBJ_VARCHAR_ID_TABLE,
                                    I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_ITEM_COST_ZONE_GRP_CHG';
BEGIN
   --Validation
   FOR i in I_items.first..I_items.last LOOP
      if I_items(i) is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_items.items', L_program);
         return FALSE;
      end if;
   end LOOP;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.ITEM_COST_ZONE_GRP_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_item_cost_zone (cost_event_process_id,
                                          item)
      select O_cost_event_process_id,
             value(ids)
        from table(cast(I_items as OBJ_VARCHAR_ID_TABLE)) ids;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ITEM_COST_ZONE_GRP_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_ITEM_COST_ZONE_GRP_CHG;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_COUNTRY(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                          I_action                 IN     COST_EVENT.ACTION%TYPE,
                          I_sc_detail_rec          IN     OBJ_SC_COST_EVENT_TBL,
                          I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY';
BEGIN
   --Validation
   if I_sc_detail_rec is NOT NULL and I_sc_detail_rec.COUNT>0 then
      FOR i in I_sc_detail_rec.first..I_sc_detail_rec.last LOOP
         if I_sc_detail_rec(i).item is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_sc_detail_rec.Item', L_program);
            return FALSE;
         end if;
         if I_sc_detail_rec(i).location is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_sc_detail_rec.Location', L_program);
            return FALSE;
         end if;
         if I_sc_detail_rec(i).Supplier is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_sc_detail_rec.Supplier', L_program);
            return FALSE;
         end if;
         if I_sc_detail_rec(i).origin_country_id is NULL then
            O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                                  'I_sc_detail_rec.Origin_country_id', L_program);
            return FALSE;
         end if;
      end LOOP;
   end if;

   if I_action is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_action', L_program);
      return FALSE;
   end if;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        I_action,
                        FUTURE_COST_EVENT_SQL.SUPP_COUNTRY_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   if I_action = FUTURE_COST_EVENT_SQL.REMOVE_EVENT then
      insert into cost_event_supp_country (cost_event_process_id,
                                            item,
                                            location,
                                            supplier,
                                            origin_country_id)
                                     select O_cost_event_process_id,
                                            input.item,
                                            input.location,
                                            input.supplier,
                                            input.origin_country_id
                                       from table(cast(I_sc_detail_rec as OBJ_SC_COST_EVENT_TBL)) input,
                                            (select store as loc, store_type from store
                                             union all
                                             select wh as loc, 'C' as store_type from wh) loc
                                      where input.location = loc.loc;
     else 
       insert into cost_event_supp_country (cost_event_process_id,
                                            item,
                                            location,
                                            supplier,
                                            origin_country_id)
                                     select O_cost_event_process_id,
                                            input.item,
                                            input.location,
                                            input.supplier,
                                            input.origin_country_id
                                       from table(cast(I_sc_detail_rec as OBJ_SC_COST_EVENT_TBL)) input;                                        
   
     end if;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.SUPP_COUNTRY_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_SUPP_COUNTRY;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_PRIMARY_PACK_COST_CHG(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                   I_pp_detail_rec          IN     OBJ_PP_COST_EVENT_TBL,
                                   I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_PRIMARY_PACK_COST_CHG';
BEGIN
   --Validation
   for i in I_pp_detail_rec.first..I_pp_detail_rec.last LOOP
      if I_pp_detail_rec(i).item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_pp_detail_rec.Item', L_program);
         return FALSE;
      end if;
      if I_pp_detail_rec(i).location is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_pp_detail_rec.Location', L_program);
         return FALSE;
      end if;
   end loop;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.PRIMARY_PACK_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_prim_pack (cost_event_process_id,
                                     item,
                                     location,
                                     pack_no)
                              select O_cost_event_process_id,
                                     input.item,
                                     input.location,
                                     input.pack_no
                                from table(cast(I_pp_detail_rec as OBJ_PP_COST_EVENT_TBL)) input,
                                     (select store as loc, store_type from store
                                      union all
                                      select wh as loc, 'C' as store_type from wh) loc
                               where input.location = loc.loc
                                 and loc.store_type = 'C';

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.PRIMARY_PACK_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_PRIMARY_PACK_COST_CHG;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_PRIMARY_PACK_COST_CHG_WRP(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_cost_event_process_id     OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                       I_pp_detail_rec          IN     OBJ_PP_COST_EVENT_TBL,
                                       I_user                   IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN NUMBER IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_PRIMARY_PACK_COST_CHG_WRP';
BEGIN
    if FUTURE_COST_EVENT_SQL.ADD_PRIMARY_PACK_COST_CHG(O_error_message,
                                                       O_cost_event_process_id,
                                                       I_pp_detail_rec,
                                                       I_user) = FALSE then
      return 0;
   end if;
   
   return 1;   
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ADD_PRIMARY_PACK_COST_CHG_WRP;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_TMPL_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_templ_ids             IN     OBJ_NUMERIC_ID_TABLE,
                           I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program           VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_COST_TMPL_CHG';
BEGIN
   --Validation
   for i in I_templ_ids.first..I_templ_ids.last LOOP
      if I_templ_ids(i) is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_ids.templ_id', L_program);
         return FALSE;
      end if;
   end loop;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.MODIFY_EVENT,
                        FUTURE_COST_EVENT_SQL.TEMPLATE_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;

   insert into cost_event_cost_tmpl (cost_event_process_id,
                                     templ_id)
                              select O_cost_event_process_id,
                                     value(ids)
                                from table(cast(I_templ_ids as OBJ_NUMERIC_ID_TABLE)) ids;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.TEMPLATE_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_COST_TMPL_CHG;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_TMPL_RELATIONSHIP_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                        I_templ_relns           IN     OBJ_TEMPL_RELN_EVENT_TBL,
                                        I_action                IN     COST_EVENT.ACTION%TYPE,
                                        I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program           VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_COST_TMPL_RELATIONSHIP_CHG';
BEGIN
   --Validation
   for i in I_templ_relns.first..I_templ_relns.last LOOP
      if I_templ_relns(i).dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.dept', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).class is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.class', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).subclass is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.subclass', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).location is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.location', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).old_start_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.old_start_date', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).old_end_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.old_end_date', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).new_start_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.new_start_date', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).new_end_date is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_relns.new_end_date', L_program);
         return FALSE;
      end if;
      if I_templ_relns(i).templ_id is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_templ_ids.I_templ_relns.templ_id', L_program);
         return FALSE;
      end if;
   end loop;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        I_action,
                        FUTURE_COST_EVENT_SQL.TEMPLATE_RELN_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_cost_relationship(cost_event_process_id,
                                            dept,
                                            class,
                                            subclass,
                                            location,
                                            old_start_date,
                                            old_end_date,
                                            new_start_date,
                                            new_end_date,
                                            templ_id,
                                            item)
                                     select O_cost_event_process_id,
                                            input.dept,
                                            input.class,
                                            input.subclass,
                                            input.location,
                                            input.old_start_date,
                                            input.old_end_date,
                                            input.new_start_date,
                                            input.new_end_date,
                                            input.templ_id,
                                            input.item
                                       from table(cast(I_templ_relns as OBJ_TEMPL_RELN_EVENT_TBL)) input;

   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.TEMPLATE_RELN_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_COST_TMPL_RELATIONSHIP_CHG;
-------------------------------------------------------------------------------------------------------
FUNCTION ADD_DEAL_PASSTHRU_CHG(O_error_message         IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id    OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_deal_pt_rec           IN     OBJ_DEAL_PASSTHRU_EVENT_TBL,
                               I_action                IN     COST_EVENT.ACTION%TYPE,
                               I_user                  IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program           VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_DEAL_PASSTHRU_CHG';
BEGIN
   for i in I_deal_pt_rec.first..I_deal_pt_rec.last LOOP
      if I_deal_pt_rec(i).dept is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_deal_pt_rec.dept', L_program);
         return FALSE;
      end if;
      if I_deal_pt_rec(i).supplier is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_deal_pt_rec.supplier', L_program);
         return FALSE;
      end if;
      if I_deal_pt_rec(i).costing_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_deal_pt_rec.wh', L_program);
         return FALSE;
      end if;
      if I_deal_pt_rec(i).location is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_deal_pt_rec.location', L_program);
         return FALSE;
      end if;
      if I_deal_pt_rec(i).loc_type is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_deal_pt_rec.loc_type', L_program);
         return FALSE;
      end if;
   end loop;
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        I_action,
                        FUTURE_COST_EVENT_SQL.DEAL_PASSTHRU_COST_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
   insert into cost_event_deal_passthru(cost_event_process_id,
                                        dept,
                                        supplier,
                                        location,
                                        loc_type,
                                        costing_loc,
                                        passthru_pct)
                                 select O_cost_event_process_id,
                                        input.dept,
                                        input.supplier,
                                        input.location,
                                        input.loc_type,
                                        input.costing_loc,
                                        decode(I_action,'REM',0,input.passthru_pct)
                                   from table(cast(I_deal_pt_rec as OBJ_DEAL_PASSTHRU_EVENT_TBL)) input;
   --
   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.DEAL_PASSTHRU_COST_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;
   --
   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_DEAL_PASSTHRU_CHG;
-------------------------------------------------------------------------------------------------------

FUNCTION MOD_FRAN_COSTING_LOC (O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id        OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_item                      IN     ITEM_LOC.ITEM%TYPE,
                               I_fran_loc                  IN     ITEM_LOC.LOC%TYPE,
                               I_costing_loc               IN     ITEM_LOC.COSTING_LOC%TYPE,
                               I_cost_loc_change_date      IN     FUTURE_COST.ACTIVE_DATE%TYPE,
                               I_user                      IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.MOD_FRAN_COSTING_LOC';
BEGIN
   --Validation
  
      if I_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_Item', L_program);
         return FALSE;
      end if;
      if I_fran_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_fran_loc', L_program);
         return FALSE;
      end if;
      if I_costing_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_costing_loc', L_program);
         return FALSE;
      end if;

   
   --
   if INSERT_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.ADD_EVENT,
                        FUTURE_COST_EVENT_SQL.FRAN_COST_LOC_CHG_EVENT_TYPE,
                        LP_ind_y,
                        I_user) = FALSE then
      return FALSE;
   end if;
   --
      --
   insert into COST_EVENT_CL (cost_event_process_id,
                                             item,
                                             franchise_loc,
                                             costing_loc,
                                             cost_loc_change_date)
                                 values     ( O_cost_event_process_id,
                                              I_item,
                                              I_fran_loc,
                                              I_costing_loc,
                                              I_cost_loc_change_date);
   --
   if HANDLE_COST_EVENT(O_error_message,
                        O_cost_event_process_id,
                        FUTURE_COST_EVENT_SQL.FRAN_COST_LOC_CHG_EVENT_TYPE) = FALSE then
      return FALSE;
   end if;
   --
   
   
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MOD_FRAN_COSTING_LOC;

-------------------------------------------------------------------------------------------------------
FUNCTION HANDLE_COST_EVENT(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_event_process_id   IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_event_type              IN     COST_EVENT.EVENT_TYPE%TYPE,
                           I_override_event_run_type IN     VARCHAR2 DEFAULT 'N')
RETURN BOOLEAN IS
   L_program           VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.HANDLE_COST_EVENT';
   L_event_run_type    COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE := NULL;
   L_return_code       NUMBER := FUTURE_COST_EVENT_SQL.SUCCESS;
   L_invalid_run_type  EXCEPTION;
   PRAGMA              EXCEPTION_INIT(L_invalid_run_type,-08004);
   L_src_tmp_ind       COST_EVENT_COST_CHG.SRC_TMP_IND%TYPE := NULL;

   cursor C_GET_RUN_TYPE is
      select NVL(FUTURE_COST_EVENT_SQL.LP_OVERRIDE_RUN_TYPE,event_run_type)
        from cost_event_run_type_config
       where event_type = I_event_type;

   cursor C_CHECK_SRC_TMP is
      select src_tmp_ind
        from cost_event_cost_chg
       where cost_event_process_id = I_cost_event_process_id
         and src_tmp_ind           = 'Y'
         and rownum                = 1;

BEGIN
   if I_cost_event_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_event_process_id', L_program);
      return FALSE;
   end if;
   if I_event_type is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_event_type', L_program);
      return FALSE;
   end if;
   open C_GET_RUN_TYPE;
   fetch C_GET_RUN_TYPE into L_event_run_type;
   close C_GET_RUN_TYPE;

   -- Override the run type from the table if the input override parameter is defined as Y
   if I_override_event_run_type = 'Y' then
      L_event_run_type := FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE;
   end if;

   -- If the cost event is of type Cost Change and src_tmp_ind is set to 'Y'
   -- then set run type to Override.
   if I_event_type = FUTURE_COST_EVENT_SQL.COST_CHANGE_COST_EVENT_TYPE then
      open C_CHECK_SRC_TMP;
      fetch C_CHECK_SRC_TMP into L_src_tmp_ind;
      close C_CHECK_SRC_TMP;
      if L_src_tmp_ind = 'Y' then
         L_event_run_type := FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE;
      end if;
   end if;

   case L_event_run_type
   --Over ride event run type
   when FUTURE_COST_EVENT_SQL.OVRID_COST_EVENT_RUN_TYPE then
      FUTURE_COST_THREAD_SQL.PROCESS_COST_EVENTS(I_cost_event_process_id,
                                                 L_return_code,
                                                 O_error_message,
                                                 I_override_event_run_type);
      if L_return_code = FUTURE_COST_EVENT_SQL.FAILURE then
         return FALSE;
      end if;
   -- batch
   when FUTURE_COST_EVENT_SQL.BATCH_COST_EVENT_RUN_TYPE then
      return TRUE;

   -- synchronous
   when FUTURE_COST_EVENT_SQL.SYNC_COST_EVENT_RUN_TYPE then
      FUTURE_COST_THREAD_SQL.PROCESS_COST_EVENTS(I_cost_event_process_id,
                                                 L_return_code,
                                                 O_error_message);
      if L_return_code = FUTURE_COST_EVENT_SQL.FAILURE then
         return FALSE;
      end if;
   -- asynchronous
   when FUTURE_COST_EVENT_SQL.ASYNC_COST_EVENT_RUN_TYPE then
     if ENQUEUE_COST_EVENT(O_error_message,
                           I_cost_event_process_id) = FALSE then
              return FALSE;
     end if;
   else
      raise L_invalid_run_type;
   end case;
   return TRUE;
EXCEPTION
   WHEN L_invalid_run_type THEN
      O_error_message := SQL_LIB.CREATE_MSG('INV_COST_EVENT_RUN_TYPE');
      return FALSE;
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END HANDLE_COST_EVENT;
-------------------------------------------------------------------------------------------------------
FUNCTION REPROCESS_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                              I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                              I_thread_id              IN     COST_EVENT_THREAD.THREAD_ID%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program           VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.REPROCESS_COST_EVENT';
   L_return_code       NUMBER       := FUTURE_COST_EVENT_SQL.SUCCESS;
   L_exists            VARCHAR2(1)  := 'N';
   cursor C_CHECK_EXIST_EVENT_THREAD is
      select 'Y'
        from cost_event_thread
       where cost_event_process_id = I_cost_event_process_id
         and thread_id = I_thread_id
         and rownum < 2;
   cursor C_CHECK_EXIST_EVENT is
      select 'Y'
        from cost_event
       where cost_event_process_id = I_cost_event_process_id;
BEGIN
   -- Check required parameter
   if I_cost_event_process_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_cost_event_process_id', L_program);
      return FALSE;
   end if;
   -- Check if cost event exists prior to reprocessing
   -- A cost event with no thread id is assummed to be a reprocess for an event that failed prior to being threaded.
   -- Check if cost event and thread exist
   if I_thread_id is not NULL then
      open C_CHECK_EXIST_EVENT_THREAD;
      fetch C_CHECK_EXIST_EVENT_THREAD into L_exists;
      ---
      if C_CHECK_EXIST_EVENT_THREAD%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_CE_THREAD_REPROCESS',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      close C_CHECK_EXIST_EVENT_THREAD;
      ---
      -- insert a row in reprocessing status into the cost_event_result table
      insert into cost_event_result(cost_event_process_id,
                                    thread_id,
                                    attempt_num,
                                    status,
                                    retry_user_id,
                                    error_message,
                                    create_datetime)
      select I_cost_event_process_id,
             I_thread_id,
             nvl(max(attempt_num)+1, 1),
             FUTURE_COST_EVENT_SQL.REPROCESSING_COST_EVENT,
             get_user,
             NULL,
             SYSDATE
        from cost_event_result
       where cost_event_process_id = I_cost_event_process_id
         and thread_id             = I_thread_id;
      --
      -- call AQ to reprocess event and thread
         if FUTURE_COST_THREAD_SQL.ENQUEUE_COST_EVENT_THREAD(O_error_message,
                                                             I_cost_event_process_id,
                                                             I_thread_id) = FALSE then
            return FALSE;
         end if;
   else -- Check if cost event exists
      open C_CHECK_EXIST_EVENT;
      fetch C_CHECK_EXIST_EVENT into L_exists;
      ---
      if C_CHECK_EXIST_EVENT%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('NO_CE_REPROCESS',NULL,NULL,NULL);
         return FALSE;
      end if;
      ---
      close C_CHECK_EXIST_EVENT;
      ---
      -- call AQ to reprocess event
      if ENQUEUE_COST_EVENT(O_error_message,
                            I_cost_event_process_id) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END REPROCESS_COST_EVENT;

------------------------------------------------------------------------------------------------------
FUNCTION ENQUEUE_COST_EVENT(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            I_cost_event_process_id  IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
RETURN BOOLEAN IS
   L_program            VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ENQUEUE_COST_EVENT';
   L_queue_name         Varchar2(200);
   L_enqueue_options    DBMS_AQ.ENQUEUE_OPTIONS_T;
   L_message_properties DBMS_AQ.MESSAGE_PROPERTIES_T;
   L_message_handle     RAW(16);
   L_message            cost_event_msg := cost_event_msg(I_cost_event_process_id);
   L_owner              VARCHAR2(30);

   cursor C_GET_OWNER is
      select table_owner
        from system_options;

BEGIN

   open  C_GET_OWNER;
   fetch C_GET_OWNER into L_owner;
   close C_GET_OWNER;

   L_queue_name                 := L_owner || '.cost_event_queue';

   DBMS_AQ.ENQUEUE(
      queue_name         => L_queue_name,
      enqueue_options    => L_enqueue_options,
      message_properties => L_message_properties,
      payload            => L_message,
      msgid              => L_message_handle);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ENQUEUE_COST_EVENT;
-------------------------------------------------------------------------------------------------------
-- Function Name:  CAPTURE_RETAIL_CHANGES
-- Purpose      :  This function will run in nightly batch after price change batch and before 
--                 date-roll. It will capture retail changes that have come for item-location combinations
--                 that are on percent off retail type of franchise cost template.
--------------------------------------------------------------------------------------------------------
FUNCTION CAPTURE_RETAIL_CHANGES(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
  RETURN BOOLEAN
IS
  L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.CAPTURE_RETAIL_CHANGES';
  CURSOR C_get_retail_changes
  IS
    SELECT OBJ_ITEM_LOC_RETAIL_REC ( inner1.item, inner1.loc, inner1.loc_type, inner1.dept,inner1.class,inner1.unit_retail,inner1.margin_pct,inner1.templ_id, inner1.start_date, inner1.end_date)
      FROM (SELECT ph.item, ph.loc, ph.loc_type, im.dept,im.class,im.subclass,ph.unit_retail,cth.margin_pct,cr.templ_id, cr.start_date, cr.end_date,ph.action_date,
                   case
                   when (cr.class = '-1' and cr.subclass = '-1' and cr.item = '-1') then 4
                   when (cr.class != '-1' and cr.subclass = '-1' and cr.item = '-1') then 3
                   when (cr.class != '-1' and cr.subclass != '-1' and cr.item = '-1') then 2
                   else 1
                   end lvl    
              FROM wf_cost_buildup_tmpl_head cth,
                   wf_cost_relationship cr,
                   item_master im,
                   price_hist ph
             WHERE cth.templ_id = cr.templ_id
               AND im.dept        = cr.dept
               AND im.class       = DECODE(cr.class,'-1',im.class,cr.class)
               AND im.subclass    = DECODE(cr.subclass,'-1',im.subclass,cr.subclass)
               AND im.item        = DECODE(cr.item,'-1',im.item,cr.item)
               AND ph.item        = im.item
               AND ph.loc         = cr.location
               AND cr.end_date   >= ph.action_date
               AND ph.action_date = get_vdate + 1
               AND ph.tran_type              IN (4,8,11)
               AND first_applied  = 'R'
           ) inner1   
     WHERE NOT EXISTS
           (SELECT 1
              FROM (SELECT case
                           when (cr1.class = '-1' and cr1.subclass = '-1' and cr1.item = '-1') then 4
                           when (cr1.class != '-1' and cr1.subclass = '-1' and cr1.item = '-1') then 3
                           when (cr1.class != '-1' and cr1.subclass != '-1' and cr1.item = '-1') then 2
                           else 1
                           end lvl1
                      FROM wf_cost_relationship cr1,
                           wf_cost_buildup_tmpl_head cth1
                     WHERE cth1.templ_id = cr1.templ_id
                       AND inner1.item = decode(cr1.item, '-1', inner1.item, cr1.item)
                       AND inner1.dept = cr1.dept
                       AND inner1.class = decode(cr1.class, '-1',inner1.class, cr1.class)
                       AND inner1.subclass = decode(cr1.subclass, '-1',inner1.subclass, cr1.subclass)
                       AND inner1.loc = cr1.location
                       AND cr1.start_date <= inner1.start_date 
                       AND cr1.end_date >= inner1.end_date
                      ) inner2
                WHERE inner2.lvl1 < inner1.lvl
           );
              
  L_fetch_tab OBJ_ITEM_LOC_RETAIL_TBL;
  L_chunk_size NUMBER:=1000;
  L_cost_event_process_id COST_EVENT.COST_EVENT_PROCESS_ID%TYPE;
BEGIN
  OPEN C_get_retail_changes;
  LOOP
    FETCH C_get_retail_changes bulk collect INTO L_fetch_tab limit L_chunk_size;
    IF L_fetch_tab.count()                                                     > 0 THEN
      IF ADD_RETAIL_CHANGE(O_error_message,L_cost_event_process_id,L_fetch_tab)=FALSE THEN
        RETURN false;
      END IF;
    END IF;
    COMMIT;
    EXIT
  WHEN C_get_retail_changes%NOTFOUND;
  END LOOP;
  RETURN true;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN FALSE;
END CAPTURE_RETAIL_CHANGES;
--------------------------------------------------------------------------------------------------------
FUNCTION MOD_FRAN_COSTING_LOC_CH(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_cost_event_process_id        OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_item                      IN     ITEM_LOC.ITEM%TYPE,
                                 I_fran_loc                  IN     ITEM_LOC.LOC%TYPE,
                                 I_costing_loc               IN     ITEM_LOC.COSTING_LOC%TYPE,
                                 I_cost_loc_change_date      IN     FUTURE_COST.ACTIVE_DATE%TYPE,
                                 I_user                      IN     COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN BOOLEAN IS
   
   L_program VARCHAR2(61)         := 'FUTURE_COST_EVENT_SQL.MOD_FRAN_COSTING_LOC_CH';
   L_ranged  VARCHAR2(1)          := NULL;
   L_cost_loc_ranged VARCHAR2(1)  := NULL; 
   
   cursor C_GET_ITEM_CHILDREN is
   select item,
          status,
          item_level,
          tran_level
     from item_master
    where ((item_parent = I_item) or
           (item_grandparent = I_item))
      and tran_level >= item_level;
      
      
   cursor C_CHECK_COST_LOC_RANGED(c_child_item ITEM_MASTER.ITEM%TYPE) is
      select 'X'
        from item_loc
       where item = c_child_item
         and loc = I_costing_loc;
         
   cursor C_CHECK_FRAN_LOC_RANGED(c_child_item ITEM_MASTER.ITEM%TYPE) is
      select 'X'
        from item_loc
       where item = c_child_item
         and loc = I_fran_loc;
BEGIN
   --Validation
  
      if I_item is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_Item', L_program);
         return FALSE;
      end if;
      if I_fran_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_fran_loc', L_program);
         return FALSE;
      end if;
      if I_costing_loc is NULL then
         O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                               'I_costing_loc', L_program);
         return FALSE;
      end if;
   
   
   FOR item_child_rec IN C_GET_ITEM_CHILDREN LOOP
      
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_FRAN_LOC_RANGED', 'STORE', NULL);
      OPEN C_CHECK_FRAN_LOC_RANGED(item_child_rec.item);
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FRAN_LOC_RANGED', 'STORE', NULL);  
      FETCH C_CHECK_FRAN_LOC_RANGED INTO L_ranged;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FRAN_LOC_RANGED', 'STORE', NULL);
      CLOSE C_CHECK_FRAN_LOC_RANGED;
      
      
      SQL_LIB.SET_MARK('OPEN', 'C_CHECK_COST_LOC_RANGED', 'STORE', NULL);
      OPEN C_CHECK_COST_LOC_RANGED(item_child_rec.item);
      SQL_LIB.SET_MARK('FETCH', 'C_CHECK_FRAN_LOC_RANGED', 'STORE', NULL);  
      FETCH C_CHECK_COST_LOC_RANGED INTO L_cost_loc_ranged;
      SQL_LIB.SET_MARK('CLOSE', 'C_CHECK_FRAN_LOC_RANGED', 'STORE', NULL);
      CLOSE C_CHECK_COST_LOC_RANGED;
      
      If L_ranged is NOT NULL and L_cost_loc_ranged is NOT NULL then
         
         if INSERT_COST_EVENT(O_error_message,
                              O_cost_event_process_id,
                              FUTURE_COST_EVENT_SQL.ADD_EVENT,
                              FUTURE_COST_EVENT_SQL.FRAN_COST_LOC_CHG_EVENT_TYPE,
                              LP_ind_y,
                              I_user) = FALSE then
            return FALSE;
         end if;
       
         insert into COST_EVENT_CL (cost_event_process_id,
                                                 item,
                                                 franchise_loc,
                                                 costing_loc,
                                                 cost_loc_change_date)
                                     values     ( O_cost_event_process_id,
                                                  item_child_rec.item,
                                                  I_fran_loc,
                                                  I_costing_loc,
                                                  I_cost_loc_change_date);
         --
         if HANDLE_COST_EVENT(O_error_message,
                              O_cost_event_process_id,
                              FUTURE_COST_EVENT_SQL.FRAN_COST_LOC_CHG_EVENT_TYPE) = FALSE then
            return FALSE;
         end if;
      end if;
   END LOOP;
   --
   
   
   return TRUE;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END MOD_FRAN_COSTING_LOC_CH;
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_HIER_CHANGE_WRP(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_cost_event_process_id OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
    I_supp_hier_chgs IN OBJ_ISCL_SUPP_HIER_CHG_TBL,
    I_user           IN COST_EVENT.USER_ID%TYPE DEFAULT get_user)
  RETURN INTEGER
IS
  L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_SUPP_HIER_CHANGE_WRP';
BEGIN
  IF ADD_SUPP_HIER_CHANGE(O_error_message, O_cost_event_process_id, I_supp_hier_chgs, I_user)=false THEN
    RETURN 0;
  END IF;
  RETURN 1;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END ADD_SUPP_HIER_CHANGE_WRP;
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_SUPP_COUNTRY_WRP(
    O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
    O_cost_event_process_id OUT COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
    I_action        IN COST_EVENT.ACTION%TYPE,
    I_sc_detail_rec IN OBJ_SC_COST_EVENT_TBL,
    I_user          IN COST_EVENT.USER_ID%TYPE DEFAULT get_user)
  RETURN INTEGER
IS
  L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_SUPP_COUNTRY_WRP';
BEGIN
  IF ADD_SUPP_COUNTRY(O_error_message, O_cost_event_process_id, I_action, I_sc_detail_rec, I_user)=false THEN
    RETURN 0;
  END IF;
  RETURN 1;
EXCEPTION
WHEN OTHERS THEN
  O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR', SQLERRM, L_program, TO_CHAR(SQLCODE));
  RETURN 0;
END ADD_SUPP_COUNTRY_WRP;
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_DEALS_WRP(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_cost_event_process_id   IN OUT   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                       I_action                  IN       COST_EVENT.ACTION%TYPE,
                       I_deal_ids                IN       OBJ_NUMERIC_ID_TABLE,
                       I_persist_ind             IN       COST_EVENT.PERSIST_IND%TYPE,
                       I_user                    IN       COST_EVENT.USER_ID%TYPE DEFAULT get_user)
RETURN INTEGER IS

   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_DEALS_WRP';

BEGIN

   if ADD_DEALS (O_error_message,
                 O_cost_event_process_id,
                 I_action,
                 I_deal_ids,
                 I_persist_ind,
                 I_user) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END ADD_DEALS_WRP;
--------------------------------------------------------------------------------------------------------
FUNCTION ADD_COST_CHANGE_EVENT(O_error_message           IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_cost_event_process_id   IN OUT   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                               I_action                  IN       COST_EVENT.ACTION%TYPE,
                               I_cost_change             IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                               I_src_tmp_ind             IN       VARCHAR2,
                               I_persist_ind             IN       COST_EVENT.PERSIST_IND%TYPE,
                               I_user                    IN       COST_EVENT.USER_ID%TYPE DEFAULT get_user,
                               I_over_run_type           IN       COST_EVENT.OVERRIDE_RUN_TYPE%TYPE DEFAULT NULL)
RETURN BOOLEAN IS
   L_program VARCHAR2(61) := 'FUTURE_COST_EVENT_SQL.ADD_COST_CHANGE_EVENT';

   L_cost_changes_rec   OBJ_CC_COST_EVENT_REC;
   L_cost_changes_tbl   OBJ_CC_COST_EVENT_TBL := OBJ_CC_COST_EVENT_TBL();
BEGIN

   L_cost_changes_rec := OBJ_CC_COST_EVENT_REC(I_cost_change,
                                               I_src_tmp_ind);
   
   L_cost_changes_tbl.EXTEND;
   L_cost_changes_tbl(L_cost_changes_tbl.COUNT) := L_cost_changes_rec;

   if ADD_COST_CHANGE_EVENT(O_error_message,
                            O_cost_event_process_id,
                            I_action,
                            L_cost_changes_tbl,
                            I_persist_ind,
                            I_user,
                            I_over_run_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END ADD_COST_CHANGE_EVENT;
--------------------------------------------------------------------------------------------------------
END FUTURE_COST_EVENT_SQL;
/
