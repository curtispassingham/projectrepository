
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ELC_ITEM_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
--Function Name: RECALC_COMP
--Purpose      : Function called by ELC_SQL.CALC_COMP to calculate all item
--               expenses and assessments.
-------------------------------------------------------------------------------
FUNCTION RECALC_COMP(O_error_message     IN OUT VARCHAR2,
                     O_est_value         IN OUT NUMBER,
                     I_dtl_flag          IN     VARCHAR2,
                     I_comp_id           IN     ELC_COMP.COMP_ID%TYPE,
                     I_calc_type         IN     VARCHAR2,
                     I_item              IN     ITEM_MASTER.ITEM%TYPE,
                     I_supplier          IN     SUPS.SUPPLIER%TYPE,
                     I_item_exp_type     IN     ITEM_EXP_HEAD.ITEM_EXP_TYPE%TYPE,
                     I_item_exp_seq      IN     ITEM_EXP_HEAD.ITEM_EXP_SEQ%TYPE,
                     I_hts               IN     HTS.HTS%TYPE,
                     I_import_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_origin_country_id IN     COUNTRY.COUNTRY_ID%TYPE,
                     I_effect_from       IN     HTS.EFFECT_FROM%TYPE,
                     I_effect_to         IN     HTS.EFFECT_TO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Function: COST_ZONE_CHANGE
--  Purpose: Deletes existing item_exp_detail and item_exp_head records for the
--           item.  If suppliers exist, returns the total elc for the 
--           primary supplier.
-------------------------------------------------------------------------------
FUNCTION COST_ZONE_CHANGE(O_error_message    IN OUT  VARCHAR2,
                          O_total_elc        IN OUT  NUMBER,
                          I_suppliers_exist  IN      VARCHAR2,
                          I_item             IN      ITEM_MASTER.ITEM%TYPE)
return BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION NEW_COST_ZONE_COMP_DETAILS(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                    I_item             IN      ITEM_MASTER.ITEM%TYPE,
                                    I_cost_zone_group  IN      ITEM_MASTER.COST_ZONE_GROUP_ID%TYPE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
END ELC_ITEM_SQL;
/
