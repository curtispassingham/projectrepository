CREATE OR REPLACE PACKAGE ORDER_CALC_SQL AUTHID CURRENT_USER AS
-- Global Data Types
-- These new global variables will be used in returning the calculated unit
-- costs of the components of a complex deposit pack in the ordskucomp.fmb

TYPE ordsku_complex_pack_rec is RECORD(item                 ITEM_MASTER.ITEM%TYPE,
                                       deposit_item_type    ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE,
                                       uop_order_cost        NUMBER(20,4),
                                       supplier_unit_cost   ORDLOC.UNIT_COST%TYPE,
                                       uop_qty_ordered      ORDLOC.QTY_ORDERED%TYPE);
                                   
TYPE ordsku_complex_pack_tbl is TABLE of ordsku_complex_pack_rec INDEX BY BINARY_INTEGER;

TYPE ordsku_rec IS RECORD(item                     ORDSKU.ITEM%TYPE,
                          ti_item_desc             ITEM_MASTER.ITEM_DESC%TYPE,
                          origin_country_id        ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                          ti_supplier_cost_ord     GTT_ORDITEM_SUM.SUPPLIER_COST%TYPE,
                          ti_cost_source           SUPS.SUP_NAME%TYPE,
                          ti_unit_elc_ord          ORDLOC.UNIT_RETAIL%TYPE,
                          ti_unit_of_purch         UOM_CLASS_TL.UOM_DESC_TRANS%TYPE,
                          non_scale_ind            ORDSKU.NON_SCALE_IND%TYPE,
                          ti_qty_received          ORDLOC.QTY_RECEIVED%TYPE,
                          ti_unit_expense_ord      ORDLOC.UNIT_RETAIL%TYPE,
                          ti_unit_expense_prim     ORDLOC.UNIT_RETAIL%TYPE,
                          ti_total_expenses_ord    ORDLOC.UNIT_RETAIL%TYPE,
                          ti_total_expenses_prim   ORDLOC.UNIT_RETAIL%TYPE,
                          ti_unit_duty_ord         ORDLOC.UNIT_RETAIL%TYPE,
                          ti_unit_duty_prim        ORDLOC.UNIT_RETAIL%TYPE,
                          ti_total_duty_ord        ORDLOC.UNIT_RETAIL%TYPE,
                          ti_total_duty_prim       ORDLOC.UNIT_RETAIL%TYPE,
                          ti_unit_elc_prim         ORDLOC.UNIT_RETAIL%TYPE,
                          ti_total_elc_ord         ORDLOC.UNIT_RETAIL%TYPE,
                          ti_total_elc_prim        ORDLOC.UNIT_RETAIL%TYPE,
                          earliest_ship_date       ORDSKU.EARLIEST_SHIP_DATE%TYPE,
                          latest_ship_date         ORDSKU.LATEST_SHIP_DATE%TYPE,
                          ref_item                 ORDSKU.REF_ITEM%TYPE,
                          ti_ref_item_desc         ITEM_MASTER.ITEM_DESC%TYPE,
                          ti_unit_cost_prim        ORDLOC.UNIT_RETAIL%TYPE,
                          ti_supplier_cost_prim    ORDLOC.UNIT_RETAIL%TYPE,
                          ti_unit_cost             ORDLOC.UNIT_COST%TYPE,
                          ti_vpn                   ITEM_SUPPLIER.VPN%TYPE,
                          ti_qty_ordered           ORDLOC.QTY_ORDERED%TYPE,
                          ti_pack_tmpl_qty         PACK_TMPL_DETAIL.QTY%TYPE,
                          supp_pack_size           ORDSKU.SUPP_PACK_SIZE%TYPE,
                          ti_standard_uom          UOM_CLASS.UOM%TYPE,
                          ti_qty_prescaled         ORDLOC.QTY_PRESCALED%TYPE,
                          pickup_loc               ORDSKU.PICKUP_LOC%TYPE,
                          pickup_no                ORDSKU.PICKUP_NO%TYPE,
                          order_no                 ORDSKU.ORDER_NO%TYPE,
                          ti_cost_source_base      ORDLOC.COST_SOURCE%TYPE,
                          ti_orig_cost             ORDLOC.UNIT_COST%TYPE,
                          ti_deposit_item_type     ITEM_MASTER.DEPOSIT_ITEM_TYPE%TYPE,
                          ti_total_order_cost_ord  NUMBER(20,4),
                          ti_total_order_cost_prim NUMBER(20,4),
                          ti_total_allocated_qty   NUMBER(20,4)
                          );
--------------------------------------------------------------------------
-- Name:    TOTAL_COST_RETAIL
-- Purpose: This function calculates the total cost, total retail,
--          total duty, total expense, oustanding cost, 
--          and the discount percent of an order in the order currency.
--          The landed cost parameters are NUMBERs for rounding accuracy
--          purposes.  Do not type-cast them.
--------------------------------------------------------------------------
FUNCTION TOTAL_COST_RETAIL
                 (O_error_message                IN OUT   VARCHAR2,
                  O_total_cost_ord               IN OUT   NUMBER,
                  O_total_cost_prim              IN OUT   NUMBER,
                  O_total_outstand_cost_ord      IN OUT   NUMBER,
                  O_total_outstand_cost_prim     IN OUT   NUMBER,
                  O_total_cancel_cost_ord        IN OUT   NUMBER,
                  O_total_cancel_cost_prim       IN OUT   NUMBER,
                  O_total_retail_incl_vat_ord    IN OUT   NUMBER,
                  O_total_retail_incl_vat_prim   IN OUT   NUMBER,
                  O_total_retail_excl_vat_ord    IN OUT   NUMBER,
                  O_total_retail_excl_vat_prim   IN OUT   NUMBER,
                  O_total_landed_cost_ord        IN OUT   NUMBER,
                  O_total_landed_cost_prim       IN OUT   NUMBER,
                  O_total_expense_ord            IN OUT   NUMBER,
                  O_total_expense_prim           IN OUT   NUMBER,
                  O_total_duty_ord               IN OUT   NUMBER,
                  O_total_duty_prim              IN OUT   NUMBER,
                  O_brkt_pct_off                 IN OUT   NUMBER,
                  O_qty                          IN OUT   NUMBER,
                  O_total_cost_incl_tax_ord      IN OUT   NUMBER,
                  O_total_cost_incl_tax_prim     IN OUT   NUMBER,                  
                  I_order_no                     IN       ORDHEAD.ORDER_NO%TYPE, 
                  I_exchange_rate                IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                  I_currency_code                IN       CURRENCIES.CURRENCY_CODE%TYPE,
                  I_import_country               IN       COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Name:        ITEM_TOTAL_RETAIL
-- Purpose:     This function calculates the total retail amount and 
--              the outstanding retail amount from the ordloc table.
--              It does this for a given item in order currency.
--------------------------------------------------------------------------
FUNCTION ITEM_TOTAL_RETAIL(I_order_no              IN       ORDHEAD.ORDER_NO%TYPE,
                           I_item                  IN       ITEM_MASTER.ITEM%TYPE,
                           O_total_retail_ord      IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_outstand_retail_ord   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                           O_error_message         IN OUT   VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Name:        TOTAL_COSTS
-- Purpose:     This function calculates the total order cost, 
--              the outstanding order cost, and total cancelled cost. Add 
--              functionallity now allows order cost at pre-scaled order
--              quantities. Also order costs at the item/order, 
--              location/order level for pre-scaled and total order costs.
--------------------------------------------------------------------------
FUNCTION TOTAL_COSTS(O_error_message       IN OUT   VARCHAR2,
                     O_total_cost_ord      IN OUT   ORDLOC.UNIT_COST%TYPE,
                     O_prescale_cost_ord   IN OUT   ORDLOC.UNIT_COST%TYPE,
                     O_outstand_cost_ord   IN OUT   ORDLOC.UNIT_COST%TYPE,
                     O_cancel_cost_ord     IN OUT   ORDLOC.UNIT_COST%TYPE,
                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                     I_item                IN       ITEM_MASTER.ITEM%TYPE,
                     I_location            IN       ORDLOC.LOCATION%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Name:    BRKT_PCT_OFF
-- Purpose: This function will calculate the total discount percent for 
--          the order.
--------------------------------------------------------------------------
FUNCTION BRKT_PCT_OFF(O_error_message   IN OUT   VARCHAR2,
                      O_brkt_pct_off    IN OUT   NUMBER,
                      I_order_no        IN       ORDHEAD.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
--- Name   : ITEM_LOC_COSTS
--- Purpose: This function will do two things:  if location is passed in,
---          then the function will find the total elc, expenses, and duty
---          for that PO/item/location combination.  If location is passed
---          in as NULL, then the function will find the aggregate total elc,
---          expenses, and duty for the PO/item combination, over all locations.
--           The landed cost parameters are NUMBERs for rounding accuracy
--           purposes.  Do not type-cast them.
--------------------------------------------------------------------------
FUNCTION ITEM_LOC_COSTS(O_error_message            IN OUT   VARCHAR2,
                        O_landed_cost_ord          IN OUT   NUMBER,
                        O_landed_cost_prim         IN OUT   NUMBER,
                        O_total_landed_cost_ord    IN OUT   NUMBER,
                        O_total_landed_cost_prim   IN OUT   NUMBER,
                        O_expense_ord              IN OUT   NUMBER,
                        O_expense_prim             IN OUT   NUMBER,
                        O_total_expense_ord        IN OUT   NUMBER,
                        O_total_expense_prim       IN OUT   NUMBER,
                        O_duty_ord                 IN OUT   NUMBER,
                        O_duty_prim                IN OUT   NUMBER,
                        O_total_duty_ord           IN OUT   NUMBER,
                        O_total_duty_prim          IN OUT   NUMBER,
                        O_qty                      IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                        I_order_no                 IN       ORDHEAD.ORDER_NO%TYPE,
                        I_exchange_rate            IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_currency_code            IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_item                     IN       ITEM_MASTER.ITEM%TYPE,
                        I_location                 IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type                 IN       ORDLOC.LOC_TYPE%TYPE,
                        I_order_qty                IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_supplier                 IN       SUPS.SUPPLIER%TYPE,
                        I_origin_country           IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_import_country           IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
--- Function: ORD_ITEM_RETAIL
--- Purpose:  This function will calculate the Order/Item's unit retail,
--            landed cost, and percent markup in primary currency.
--            If the order's currency and exchange rate
--            are not passed in, the function will retrieve them.
--------------------------------------------------------------------------
FUNCTION ORD_ITEM_RETAIL(O_error_message               IN OUT   VARCHAR2,
                         O_pct_markup                  IN OUT   NUMBER,
                         O_unit_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                         O_unit_retail_excl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                         O_unit_landed_cost_prim       IN OUT   NUMBER,
                         I_order_no                    IN       ORDHEAD.ORDER_NO%TYPE,
                         I_item                        IN       ITEM_MASTER.ITEM%TYPE,
                         I_pack_item                   IN       ITEM_MASTER.ITEM%TYPE,
                         I_exchange_rate               IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                         I_currency_code_ord           IN       CURRENCIES.CURRENCY_CODE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
--- Function: ITEM_DIFF_COSTS
--- Purpose:  This function will return the total costs of items in an
--            entered item parent(grandparent)/diff.  It will return the 
--            cost with and without pack components, and both of these 
--            values will be returned in the order and primary currencies.
--------------------------------------------------------------------------
FUNCTION ITEM_DIFF_COSTS(O_error_message      IN OUT   VARCHAR2,
                         O_nopack_ord_cost    IN OUT   ORDLOC.UNIT_COST%TYPE,
                         O_nopack_prim_cost   IN OUT   ORDLOC.UNIT_COST%TYPE,
                         O_pack_ord_cost      IN OUT   ORDLOC.UNIT_COST%TYPE,
                         O_pack_prim_cost     IN OUT   ORDLOC.UNIT_COST%TYPE,
                         I_order_no           IN       ORDHEAD.ORDER_NO%TYPE,
                         I_item               IN       ITEM_MASTER.ITEM%TYPE,
                         I_diff               IN       ITEM_MASTER.DIFF_1%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------
-- FUNCTION:    SUPPLIER_COST_UNITS
-- Purpose:     This function calculates the total supplier's gross cost 
--              (before any deals or rebates have been applied), for an 
--              order, order/item, order/location, order/item/location or
--              an order/allocation/location
-----------------------------------------------------------------------------------
FUNCTION SUPPLIER_COST_UNITS(O_error_message        IN OUT   VARCHAR2,
                             O_supp_prescale_cost   IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_supp_order_cost      IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_prescale_cost        IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_order_cost           IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_qty_prescaled        IN OUT   ORDLOC.QTY_PRESCALED%TYPE,
                             O_qty_ordered          IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                             O_supp_item_cost       IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_ord_item_cost        IN OUT   ORDLOC.UNIT_COST%TYPE,
                             I_order_no             IN       ORDHEAD.ORDER_NO%TYPE,
                             I_item                 IN       ITEM_MASTER.ITEM%TYPE,
                             I_location             IN       ORDLOC.LOCATION%TYPE,
                             I_alloc_no             IN       ALLOC_HEADER.ALLOC_NO%TYPE,
                             I_supplier             IN       SUPS.SUPPLIER%TYPE,
                             I_order_status         IN       ORDHEAD.STATUS%TYPE,
                             I_ord_currency_code    IN       CURRENCIES.CURRENCY_CODE%TYPE,
                             I_ord_exchange_rate    IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function:  CALC_HEADER_DATES
-- Purpose:   This function will calculate the Pickup, Not Before, and
--            Not After Dates of the Order.
--------------------------------------------------------------------------
FUNCTION CALC_HEADER_DATES(O_error_message     IN OUT   VARCHAR2,
                           O_pickup_date       IN OUT   ORDHEAD.PICKUP_DATE%TYPE,
                           O_not_before_date   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                           O_not_after_date    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                           I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                           I_supplier          IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
--Function: CHECK_ORDLOC_VWH_COST
--Purpose : This function will query all of the items with VWHs as locations to 
--          ensure that the unit cost is the same for each VWH associated with the same
--          PWH. If not, the item in question is returned with the error message.
-----------------------------------------------------------------------------
FUNCTION CHECK_ORDLOC_VWH_COSTS(O_error_message   IN OUT VARCHAR2,
                                I_order_no        IN     ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN;
-----------------------------------------------------------------------------
-- Function:  CALC_HEADER_DATES
-- Purpose:   This function will calculate the Pickup, Not Before, and
--            Not After Dates of the Order based on the start and end 
--            dates of a contract.
--------------------------------------------------------------------------
FUNCTION CALC_CONTRACT_ORDER_DATES(O_error_message     IN OUT   VARCHAR2,
                                   O_pickup_date       IN OUT   ORDHEAD.PICKUP_DATE%TYPE,
                                   O_not_before_date   IN OUT   ORDHEAD.NOT_BEFORE_DATE%TYPE,
                                   O_not_after_date    IN OUT   ORDHEAD.NOT_AFTER_DATE%TYPE,
                                   O_valid_ind         IN OUT   BOOLEAN,
                                   I_start_date        IN       CONTRACT_HEADER.START_DATE%TYPE,
                                   I_end_date          IN       CONTRACT_HEADER.END_DATE%TYPE,
                                   I_order_no          IN       ORDHEAD.ORDER_NO%TYPE,
                                   I_supplier          IN       SUPS.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function:  CALCULATE_DEPOSIT_COMP_COST
-- Purpose:   This function will calculate the order cost of the component
--            of a complex deposit pack based on its unit cost, the order 
--            status and the supplier pack size
--------------------------------------------------------------------------
FUNCTION CALCULATE_DEPOSIT_COMP_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,     
                                     O_item_cost           IN OUT   ORDLOC.UNIT_COST%TYPE,        
                                     I_item                IN       ITEM_MASTER.ITEM%TYPE,        
                                     I_order_status        IN       ORDHEAD.STATUS%TYPE,          
                                     I_supp_pack_size      IN       ORDSKU.SUPP_PACK_SIZE%TYPE,   
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,           
                                     I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_pack_no                      PACKITEM.PACK_NO%TYPE,
                                     I_location            IN       ORDLOC.LOCATION%TYPE,
                                     I_order_curr          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)        
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function:  CALCULATE_DEPOSIT_PACK_COMP
-- Purpose:   This function will calculate the Unit Cost UOP of the components
--            and display them in the ordskucomp form
--------------------------------------------------------------------------
FUNCTION CALCULATE_DEPOSIT_PACK_COMP(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_pack_comp_tbl       IN OUT   ORDER_CALC_SQL.ORDSKU_COMPLEX_PACK_TBL,
                                     I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                                     I_order_status        IN       ORDHEAD.STATUS%TYPE,
                                     I_supp_pack_size      IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_location            IN       ORDLOC.LOCATION%TYPE,
                                     I_order_curr          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN BOOLEAN;   
--------------------------------------------------------------------------
-- Function:  ITEMLOC_COST_RETAIL
-- Purpose:   This function will calculate the Total Cost, Retail, Landed Cost
--            for a given Order/Item/Location combination.
--------------------------------------------------------------------------
FUNCTION ITEMLOC_COST_RETAIL(O_error_message                IN OUT   VARCHAR2,
                             O_total_cost_ord               IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_total_cost_prim              IN OUT   ORDLOC.UNIT_COST%TYPE,
                             O_total_retail_incl_vat_ord    IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_retail_incl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_retail_excl_vat_ord    IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_retail_excl_vat_prim   IN OUT   ORDLOC.UNIT_RETAIL%TYPE,
                             O_total_landed_cost_ord        IN OUT   NUMBER,
                             O_total_landed_cost_prim       IN OUT   NUMBER,
                             O_total_expense_ord            IN OUT   NUMBER,
                             O_total_expense_prim           IN OUT   NUMBER,
                             O_total_duty_ord               IN OUT   NUMBER,
                             O_total_duty_prim              IN OUT   NUMBER,
                             I_order_no                     IN       ORDHEAD.ORDER_NO%TYPE,
                             I_location                     IN       ORDLOC.LOCATION%TYPE,
                             I_item                         IN       ORDLOC.ITEM%TYPE,
                             I_qty                          IN       ORDLOC.QTY_ORDERED%TYPE,
                             I_exchange_rate                IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                             I_currency_code                IN       CURRENCIES.CURRENCY_CODE%TYPE,
                             I_import_country               IN       COUNTRY.COUNTRY_ID%TYPE)
RETURN BOOLEAN;   
--------------------------------------------------------------------------
-- Name:    RECALC_EXCHANGE_RATE
-- Purpose: This function recalculates the exchange rate whenever there is
--          a change in exchange rate.
--------------------------------------------------------------------------
FUNCTION RECALC_EXCHANGE_RATE(O_error_message   IN OUT   VARCHAR2,
                              I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                              I_exchange_rate   IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                              I_currency_code   IN       CURRENCIES.CURRENCY_CODE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Name:    ROUND_ORDER
-- Purpose: This function rounds the the order qty  if pack size is 
--          modified in ordsku form
--------------------------------------------------------------------------
FUNCTION ROUND_ORDER(O_error_message   IN OUT   VARCHAR2,
                     I_order_no        IN       ORDHEAD.ORDER_NO%TYPE,
                     I_supplier        IN       ORDHEAD.SUPPLIER%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Name:    QUERY_ORDSKU
-- Purpose: This procedure will query from v_ordsku view based on values passed for
--          order_no. The returning resultset will be used by the F_ORDSKU form.
--------------------------------------------------------------------------
PROCEDURE QUERY_ORDSKU(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       O_ordsku_rec          IN OUT   ORDSKU_REC,
                       I_supplier            IN       ITEM_SUPPLIER.SUPPLIER%TYPE,
                       I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                       I_fash_prepack_ind    IN       VARCHAR2,
                       I_import_country_id   IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                       I_currency_ord        IN       CURRENCY_RATES.CURRENCY_CODE%TYPE,
                       I_currency_prim       IN       CURRENCY_RATES.CURRENCY_CODE%TYPE);
--------------------------------------------------------------------------
-- Name:    QUERY_ORDSKU
-- Purpose: This procedure will query from v_ordsku view based on values passed for
--          order_no. The returning resultset will be used by the F_ORDSKU form.
--------------------------------------------------------------------------
FUNCTION QUERY_ORDSKU(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 O_ordsku_rec        IN OUT WRP_ORDSKU_REC,
                 I_supplier          IN ITEM_SUPPLIER.SUPPLIER%TYPE,
                 I_exchange_rate     IN CURRENCY_RATES.EXCHANGE_RATE%TYPE,
                 I_fash_prepack_ind  IN VARCHAR2,
                 I_import_country_id IN ORDHEAD.IMPORT_COUNTRY_ID%TYPE,
                 I_currency_ord      IN CURRENCY_RATES.CURRENCY_CODE%TYPE,
                 I_currency_prim     IN CURRENCY_RATES.CURRENCY_CODE%TYPE)
  RETURN INTEGER;
--------------------------------------------------------------------------
-- Function: CHECK_ORDLOC_COSTS
-- Purpose : This function will check if all the locations of the item has the same unit cost,
--           for an import order and if REIM is used as an external invoice matching system.
--------------------------------------------------------------------------
FUNCTION CHECK_ORDLOC_COSTS(O_error_message   IN OUT   VARCHAR2,
                            I_order_no        IN       ORDLOC.ORDER_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------
-- Function:  CALC_DEPOSIT_PACK_COMP_WRP
-- Purpose:   This is a wrapper function for CALCULATE_DEPOSIT_PACK_COMP that returns a
--            database object type instead of a PL/SQL type to be called from Java wrappers.
--------------------------------------------------------------------------
FUNCTION CALC_DEPOSIT_PACK_COMP_WRP (O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_pack_comp_tbl       IN OUT   WRP_ORDSKU_COMPLEX_PACK_TBL,
                                     I_pack_no             IN       PACKITEM.PACK_NO%TYPE,
                                     I_order_status        IN       ORDHEAD.STATUS%TYPE,
                                     I_supp_pack_size      IN       ORDSKU.SUPP_PACK_SIZE%TYPE,
                                     I_supplier            IN       SUPS.SUPPLIER%TYPE,
                                     I_origin_country_id   IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                                     I_order_no            IN       ORDHEAD.ORDER_NO%TYPE,
                                     I_location            IN       ORDLOC.LOCATION%TYPE,
                                     I_order_curr          IN       ORDHEAD.CURRENCY_CODE%TYPE,
                                     I_exchange_rate       IN       CURRENCY_RATES.EXCHANGE_RATE%TYPE)
RETURN INTEGER;
--------------------------------------------------------------------------
--Name   : ITEM_LOC_COSTS
--- Purpose: This is overloaded function with additional local related parameters.
-------------------------------------------------------------------------------
FUNCTION ITEM_LOC_COSTS(O_error_message             IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        O_landed_cost_ord           IN OUT   NUMBER,
                        O_landed_cost_prim          IN OUT   NUMBER,
                        O_landed_cost_local         IN OUT   NUMBER,    
                        O_total_landed_cost_ord     IN OUT   NUMBER,
                        O_total_landed_cost_prim    IN OUT   NUMBER,
                        O_total_landed_cost_local   IN OUT   NUMBER,
                        O_expense_ord               IN OUT   NUMBER,
                        O_expense_prim              IN OUT   NUMBER,
                        O_expense_local             IN OUT   NUMBER,
                        O_total_expense_ord         IN OUT   NUMBER,
                        O_total_expense_prim        IN OUT   NUMBER,
                        O_total_expense_local       IN OUT   NUMBER,
                        O_duty_ord                  IN OUT   NUMBER,
                        O_duty_prim                 IN OUT   NUMBER,
                        O_duty_local                IN OUT   NUMBER,
                        O_total_duty_ord            IN OUT   NUMBER,
                        O_total_duty_prim           IN OUT   NUMBER,
                        O_total_duty_local          IN OUT   NUMBER,
                        O_qty                       IN OUT   ORDLOC.QTY_ORDERED%TYPE,
                        I_order_no                  IN       ORDHEAD.ORDER_NO%TYPE,
                        I_exchange_rate             IN       ORDHEAD.EXCHANGE_RATE%TYPE,
                        I_currency_code             IN       ORDHEAD.CURRENCY_CODE%TYPE,
                        I_item                      IN       ITEM_MASTER.ITEM%TYPE,
                        I_location                  IN       ORDLOC.LOCATION%TYPE,
                        I_loc_type                  IN       ORDLOC.LOC_TYPE%TYPE,
                        I_order_qty                 IN       ORDLOC.QTY_ORDERED%TYPE,
                        I_supplier                  IN       SUPS.SUPPLIER%TYPE,
                        I_origin_country            IN       ORDSKU.ORIGIN_COUNTRY_ID%TYPE,
                        I_import_country            IN       ORDHEAD.IMPORT_COUNTRY_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------
END;
/