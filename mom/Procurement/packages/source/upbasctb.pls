CREATE OR REPLACE PACKAGE BODY UPDATE_BASE_COST AS
--------------------------------------------------------------------------------

/* Declare system level variables here.  They will be populated only once
   per session, reducing the number of queries to the system_options table. */
LP_prim_curr       system_options.currency_code%TYPE := NULL;
LP_std_av_ind      system_options.std_av_ind%TYPE := NULL;
LP_elc_ind         system_options.elc_ind%TYPE := NULL;

--------------------------------------------------------------------------------

FUNCTION CHANGE_SUPPLIER(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item              IN     ITEM_SUPPLIER.ITEM%TYPE,
                         I_supplier          IN     ITEM_SUPPLIER.SUPPLIER%TYPE,
                         I_process_children  IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'UPDATE_BASE_COST.CHANGE_SUPPLIER';
   L_active_date       period.vdate%TYPE := DATES_SQL.GET_VDATE;

   L_item              item_master.item%TYPE := NULL;

   L_prim_cost         item_loc_soh.unit_cost%TYPE := NULL;
   L_supp_curr         sups.currency_code%TYPE := NULL;
   L_supp_cost         item_loc_soh.unit_cost%TYPE := NULL;

   L_dummy_cost        price_hist.unit_cost%TYPE := NULL;
   L_prim_unit_retail  price_hist.unit_retail%TYPE := NULL;
   L_std_uom           price_hist.selling_uom%TYPE := NULL;
   L_selling_retail    price_hist.unit_retail%TYPE := NULL;
   L_selling_uom       price_hist.selling_uom%TYPE := NULL;

   cursor C_COST is
       select im.item item,
              isc.unit_cost supp_cost
         from item_master im,
              item_supp_country isc
        where ((im.item = I_item) or
                (I_process_children = 'Y'
                 and im.item_parent = I_item
                 and im.item_level <= im.tran_level) or
                (I_process_children = 'Y'
                 and im.item_grandparent = I_item
                 and im.item_level <= im.tran_level))
          and im.status = 'A'
          and isc.item = im.item
          and isc.supplier = I_supplier
          and isc.primary_country_ind = 'Y';

BEGIN

   /* make required parameters are populated */
   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           I_item,
                                           NULL,
                                           NULL);
      RETURN FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_process_children is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_process_children',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;

   if LP_prim_curr is NULL then
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                               LP_prim_curr) then
         return FALSE;
      end if;
   end if;

   if not SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                            L_supp_curr,
                                            I_supplier) then
      return FALSE;
   end if;

   FOR c_rec in C_COST LOOP

      L_item := c_rec.item;
      L_supp_cost := c_rec.supp_cost;

      /* convert the sups unit cost to the primary currency */
      if L_supp_curr != LP_prim_curr then
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_supp_cost,
                                     L_supp_curr,
                                     LP_prim_curr,
                                     L_prim_cost,
                                     'C',
                                     NULL,
                                     NULL) then
               return FALSE;
         end if;
      else
         L_prim_cost := L_supp_cost;
      end if;

      /* get the base retail */
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                              L_dummy_cost,
                                              L_prim_unit_retail,
                                              L_std_uom,
                                              L_selling_retail,
                                              L_selling_uom,
                                              L_item,
                                              'R') = FALSE then
         return FALSE;
      end if;

      /* insert a PH 02 - no loc price history record */
      SQL_LIB.SET_MARK('INSERT',
                       NULL,
                       'PRICE_HIST',
                       'Base Cost for Item: '||L_item);
      insert into price_hist(TRAN_TYPE,
                             REASON,
                             EVENT,
                             ITEM,
                             LOC,
                             UNIT_COST,
                             UNIT_RETAIL,
                             ACTION_DATE,
                             MULTI_UNITS,
                             MULTI_UNIT_RETAIL,
                             SELLING_UNIT_RETAIL,
                             SELLING_UOM,
                             MULTI_SELLING_UOM,
                             LOC_TYPE)
                     values (02,                    -- unit cost change
                             4,                     -- prim supplier change
                             NULL,                  -- event
                             L_item,
                             0,                     -- no location = base cost
                             L_prim_cost,
                             L_prim_unit_retail,
                             L_active_date,
                             NULL,                  -- multi units
                             NULL,                  -- multi units retail
                             NULL,                  -- selling unit retail
                             NULL,                  -- selling UOM
                             NULL,                  -- multi selling UOM
                             NULL);                 -- loc_type

   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_SUPPLIER;
--------------------------------------------------------------------------------

FUNCTION CHANGE_COUNTRY(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                        I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                        I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_process_children  IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'UPDATE_BASE_COST.CHANGE_COUNTRY';
   L_active_date       period.vdate%TYPE := DATES_SQL.GET_VDATE;

   L_item              item_master.item%TYPE := NULL;

   L_prim_cost         item_loc_soh.unit_cost%TYPE := NULL;
   L_supp_curr         sups.currency_code%TYPE := NULL;
   L_supp_cost         item_loc_soh.unit_cost%TYPE := NULL;

   L_dummy_cost        price_hist.unit_cost%TYPE := NULL;
   L_prim_unit_retail  price_hist.unit_retail%TYPE := NULL;
   L_std_uom           price_hist.selling_uom%TYPE := NULL;
   L_selling_retail    price_hist.unit_retail%TYPE := NULL;
   L_selling_uom       price_hist.selling_uom%TYPE := NULL;

   cursor C_COST is
       select im.item item,
              iscl.unit_cost supp_cost
         from item_master im,
              item_supp_country_loc iscl,
              item_supp_country isc
        where ((im.item = I_item) or
                (I_process_children = 'Y'
                 and im.item_parent = I_item
                 and im.item_level <= tran_level) or
                (I_process_children = 'Y'
                 and im.item_grandparent = I_item
                 and im.item_level <= tran_level))
          and im.status = 'A'
          and iscl.item = im.item
          and iscl.item = isc.item
          and iscl.supplier = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and iscl.primary_loc_ind = 'Y'
          and iscl.supplier = isc.supplier
          and iscl.origin_country_id = isc.origin_country_id
          and primary_supp_ind = 'Y';

BEGIN

   /* make sure required parameters are populated */
   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_item',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_origin_country_id is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_origin_country_id',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_process_children is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_process_children',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;

   if LP_prim_curr is NULL then
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE (O_error_message,
                                               LP_prim_curr) then
         return FALSE;
      end if;
   end if;

   if not SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                            L_supp_curr,
                                            I_supplier) then
      return FALSE;
   end if;

   FOR c_rec in C_COST LOOP
      L_item := c_rec.item;
      L_supp_cost := c_rec.supp_cost;

      /* convert the sups unit cost to the primary currency */
      if L_supp_curr != LP_prim_curr then
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_supp_cost,
                                     L_supp_curr,
                                     LP_prim_curr,
                                     L_prim_cost,
                                     'C',
                                     NULL,
                                     NULL) then
            return FALSE;
         end if;
      else
         L_prim_cost := L_supp_cost;
      end if;

      /* get the base retail */
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                              L_dummy_cost,
                                              L_prim_unit_retail,
                                              L_std_uom,
                                              L_selling_retail,
                                              L_selling_uom,
                                              L_item,
                                              'R') = FALSE then
         return FALSE;
      end if;

      /* insert a PH 02 - no loc price history record */
      SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                       'Base Cost for Item: '||L_item);
      insert into price_hist(TRAN_TYPE,
                             REASON,
                             EVENT,
                             ITEM,
                             LOC,
                             UNIT_COST,
                             UNIT_RETAIL,
                             ACTION_DATE,
                             MULTI_UNITS,
                             MULTI_UNIT_RETAIL,
                             SELLING_UNIT_RETAIL,
                             SELLING_UOM,
                             MULTI_SELLING_UOM,
                             LOC_TYPE)
                     values (02,                    -- unit cost change
                             5,                     -- prim country changed
                             NULL,                  -- event
                             L_item,
                             0,                     -- no location = base cost
                             L_prim_cost,
                             L_prim_unit_retail,
                             L_active_date,
                             NULL,                  -- multi units
                             NULL,                  -- multi units retail
                             NULL,                  -- selling unit retail
                             NULL,                  -- selling UOM
                             NULL,                  -- multi selling UOM
                             NULL);                 -- loc_type

   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_COUNTRY;
--------------------------------------------------------------------------------
FUNCTION CHANGE_LOC(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                    I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                    I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                    I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                    I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                    I_process_children  IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program           VARCHAR2(64) := 'UPDATE_BASE_COST.CHANGE_LOC';

   L_active_date       period.vdate%TYPE := DATES_SQL.GET_VDATE;
   L_item              item_master.item%TYPE := NULL;

   L_prim_cost         item_loc_soh.unit_cost%TYPE := NULL;
   L_supp_curr         sups.currency_code%TYPE := NULL;
   L_supp_cost         item_loc_soh.unit_cost%TYPE := NULL;

   L_dummy_cost        price_hist.unit_cost%TYPE := NULL;
   L_prim_unit_retail  price_hist.unit_retail%TYPE := NULL;
   L_std_uom           price_hist.selling_uom%TYPE := NULL;
   L_selling_retail    price_hist.unit_retail%TYPE := NULL;
   L_selling_uom       price_hist.selling_uom%TYPE := NULL;

   cursor C_COST is
       select im.item item,
              iscl.unit_cost supp_cost
         from item_master im,
              item_supp_country_loc iscl,
              item_supp_country isc
        where ((im.item = I_item) or
                (I_process_children = 'Y'
                 and im.item_parent = I_item
                 and im.item_level <= tran_level) or
                (I_process_children = 'Y'
                 and im.item_grandparent = I_item
                 and im.item_level <= tran_level))
          and im.status = 'A'
          and iscl.item = im.item
          and iscl.item = isc.item
          and iscl.supplier = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and iscl.loc = I_loc
          and iscl.supplier = isc.supplier
          and iscl.origin_country_id = isc.origin_country_id
          and isc.primary_supp_ind = 'Y'
          and isc.primary_country_ind = 'Y';

BEGIN

   /* make required parameters are populated */
   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_item',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_origin_country_id is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_origin_country_id',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_loc is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM',
                                           'I_loc',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_process_children is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_process_children',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   if LP_prim_curr is NULL then
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                              LP_prim_curr) then
         return FALSE;
      end if;
   end if;

   if not SUPP_ATTRIB_SQL.GET_CURRENCY_CODE(O_error_message,
                                            L_supp_curr,
                                            I_supplier) then
      return FALSE;
   end if;

   FOR c_rec in C_COST LOOP
   
      L_item := c_rec.item;
      L_supp_cost := c_rec.supp_cost;

      /* convert the sups unit cost to the primary currency */
      if L_supp_curr != LP_prim_curr then
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_supp_cost,
                                     L_supp_curr,
                                     LP_prim_curr,
                                     L_prim_cost,
                                     'C',
                                     NULL,
                                     NULL) then
            return FALSE;
         end if;
      else
         L_prim_cost := L_supp_cost;
      end if;

      /* get the base retail */
      if ITEM_ATTRIB_SQL.GET_BASE_COST_RETAIL(O_error_message,
                                              L_dummy_cost,
                                              L_prim_unit_retail,
                                              L_std_uom,
                                              L_selling_retail,
                                              L_selling_uom,
                                              L_item,
                                              'R') = FALSE then
         return FALSE;
      end if;

      /* insert a PH 02 - no loc price history record */
      SQL_LIB.SET_MARK('INSERT',NULL,'PRICE_HIST',
                       'Base Cost for Item: '||L_item);
      insert into price_hist(TRAN_TYPE,
                             REASON,
                             EVENT,
                             ITEM,
                             LOC,
                             UNIT_COST,
                             UNIT_RETAIL,
                             ACTION_DATE,
                             MULTI_UNITS,
                             MULTI_UNIT_RETAIL,
                             SELLING_UNIT_RETAIL,
                             SELLING_UOM,
                             MULTI_SELLING_UOM,
                             LOC_TYPE)
                     values (02,                    -- unit cost change
                             6,                     -- prim loc changed
                             NULL,                  -- event
                             L_item,
                             0,                     -- no location = base cost
                             L_prim_cost,
                             L_prim_unit_retail,
                             L_active_date,
                             NULL,                  -- multi units
                             NULL,                  -- multi units retail
                             NULL,                  -- selling unit retail
                             NULL,                  -- selling UOM
                             NULL,                  -- multi selling UOM
                             NULL);                 -- loc_type

   end LOOP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_LOC;
------------------------------------------------------------------------------------
FUNCTION CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message        IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                     I_item                 IN     ITEM_LOC.ITEM%TYPE,
                                     I_loc                  IN     ITEM_LOC.LOC%TYPE,
                                     I_supplier             IN     ITEM_LOC.PRIMARY_SUPP%TYPE,
                                     I_origin_country_id    IN     ITEM_LOC.PRIMARY_CNTRY%TYPE,
                                     I_process_children     IN     VARCHAR2,
                                     I_cost_change          IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64)                    := 'UPDATE_BASE_COST.CHG_ITEMLOC_PRIM_SUPP_CNTRY';
   L_table           VARCHAR2(64)                    := NULL;
   L_active_date     PERIOD.VDATE%TYPE               := NULL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked,
                                    -54);

   L_pack_ind        ITEM_MASTER.PACK_IND%TYPE       := NULL;
   L_sellable        ITEM_MASTER.SELLABLE_IND%TYPE   := NULL;
   L_orderable       ITEM_MASTER.ORDERABLE_IND%TYPE  := NULL;
   L_pack_type       ITEM_MASTER.PACK_TYPE%TYPE      := NULL;

   L_item            ITEM_MASTER.ITEM%TYPE           := NULL;
   L_supp_curr       SUPS.CURRENCY_CODE%TYPE         := NULL;
   L_supp_cost       ITEM_LOC_SOH.UNIT_COST%TYPE     := NULL;
   L_supp_ebc_cost   ITEM_LOC_SOH.AV_COST%TYPE       := NULL;
   L_supp_nic_cost   ITEM_LOC_SOH.UNIT_COST%TYPE     := NULL;
   L_loc_ebc_cost    ITEM_LOC_SOH.AV_COST%TYPE       := NULL;
   L_loc_nic_cost    ITEM_LOC_SOH.UNIT_COST%TYPE     := NULL;
   L_local_curr      SUPS.CURRENCY_CODE%TYPE         := NULL;
   L_old_cost        ITEM_LOC_SOH.UNIT_COST%TYPE     := NULL;
   L_old_av_cost     ITEM_LOC_SOH.AV_COST%TYPE       := NULL;
   L_local_retail    ITEM_LOC.UNIT_RETAIL%TYPE       := NULL;
   L_loc_type        ITEM_LOC.LOC_TYPE%TYPE          := NULL;
   L_soh             ITEM_LOC_SOH.STOCK_ON_HAND%TYPE := NULL;
   L_status          ITEM_MASTER.STATUS%TYPE         := NULL;
   L_tran_code       TRAN_DATA.TRAN_CODE%type;
   L_total_cost      TRAN_DATA.TOTAL_COST%type;
   L_local_cost      ITEM_LOC_SOH.UNIT_COST%TYPE     := NULL;

   L_dept            SUBCLASS.DEPT%TYPE              := NULL;
   L_class           SUBCLASS.CLASS%TYPE             := NULL;
   L_subclass        SUBCLASS.SUBCLASS%TYPE          := NULL;
   --variable for Wrapper Function
   L_l10n_fin_rec    L10N_FIN_REC                    := L10N_FIN_REC();

   L_default_po_cost COUNTRY_ATTRIB.DEFAULT_PO_COST%TYPE := NULL;
   L_price_hist_ind  VARCHAR2(1) := NULL;

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   
   cursor C_COST is
       select im.item item,
              s.currency_code supp_curr,
              iscl.unit_cost supp_cost,
              iscl.extended_base_cost supp_ebc_cost,
              iscl.negotiated_item_cost supp_nic_cost,
              st.currency_code loc_curr,
              NVL(ils.unit_cost, 0) loc_cost,
              ils.av_cost av_cost,
              il.unit_retail loc_retail,
              'S' loc_type,
              nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) +
                 nvl(ils.in_transit_qty, 0) + nvl(ils.pack_comp_intran, 0) soh,
              im.status status
         from item_supp_country_loc iscl,
              item_loc il,
              item_loc_soh ils,
              sups s,
              store st,
              item_master im
        where iscl.item              = im.item
          and iscl.supplier          = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and iscl.loc               = I_loc
          and il.item                = iscl.item
          and il.loc                 = iscl.loc
          and ils.item(+)            = iscl.item
          and ils.loc(+)             = iscl.loc
          and s.supplier             = iscl.supplier
          and st.store               = iscl.loc
          and ((im.item = I_item)
                or (im.item_parent         = I_item
                    and I_process_children = 'Y'
                    and im.item_level <= im.tran_level)
                or (im.item_grandparent    = I_item
                    and I_process_children = 'Y'
                    and im.item_level <= im.tran_level))
        union all
       select im.item item,
              s.currency_code supp_curr,
              iscl.unit_cost supp_cost,
              iscl.extended_base_cost supp_ebc_cost,
              iscl.negotiated_item_cost supp_nic_cost,
              w.currency_code loc_curr,
              NVL(ils.unit_cost, 0) loc_cost,
              ils.av_cost av_cost,
              il.unit_retail loc_retail,
              'W' loc_type,
              nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) +
                 nvl(ils.in_transit_qty, 0) + nvl(ils.pack_comp_intran, 0) soh,
              im.status status
         from item_supp_country_loc iscl,
              item_loc il,
              item_loc_soh ils,
              sups s,
              wh w,
              item_master im
        where iscl.item              = im.item
          and iscl.supplier          = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and iscl.loc               = I_loc
          and il.item                = iscl.item
          and il.loc                 = iscl.loc
          and ils.item(+)            = iscl.item
          and ils.loc(+)             = iscl.loc
          and s.supplier             = iscl.supplier
          and w.wh                   = iscl.loc
          and ((im.item = I_item)
                or (im.item_parent         = I_item
                    and I_process_children = 'Y'
                    and im.item_level <= im.tran_level)
                or (im.item_grandparent    = I_item
                    and I_process_children = 'Y'
                    and im.item_level <= im.tran_level));

   cursor C_LOCK_ITEM_LOC_SOH is
       select 'x'
         from item_loc_soh
        where item = L_item
          and loc  = I_loc
       for update of unit_cost,av_cost nowait;

   cursor C_CHECK_PRICE_HIST is
      select 'x'
        from price_hist
       where item        = L_item
         and loc         = I_loc
         and action_date = L_active_date
         and tran_type   = 2
         and reason      = 7;

BEGIN
   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_item',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_loc is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_loc',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_supplier',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_origin_country_id is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_origin_country_id',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;
   if I_process_children is NULL then
      O_error_message:= SQL_LIB.CREATE_MSG('INVALID_PARM',
                                           'I_process_children',
                                           'NULL',
                                           'NOT NULL');
      RETURN FALSE;
   end if;

   if LP_std_av_ind is NULL then
      if not SYSTEM_OPTIONS_SQL.STD_AV_IND(O_error_message,
                                           LP_std_av_ind) then
         return FALSE;
      end if;
   end if;
   if LP_elc_ind is NULL then
     if not SYSTEM_OPTIONS_SQL.GET_ELC_IND(O_error_message,
                                           LP_elc_ind) then
        return FALSE;
     end if;
   end if;
   if LP_prim_curr is NULL then
      if not SYSTEM_OPTIONS_SQL.CURRENCY_CODE(O_error_message,
                                               LP_prim_curr) then
         return FALSE;
      end if;
   end if;

   ---
   -- If the I_cost_change parameter is not null, the function was called (indirectly)
   -- from the batch program sccext.pc.  The batch program picks up cost changes for
   -- tomorrow, so for these cost changes, the active date should be tomorrow.
   ---
   if I_cost_change is NOT NULL then
      L_active_date := DATES_SQL.GET_VDATE + 1;
   else
      L_active_date := DATES_SQL.GET_VDATE;
   end if;

   FOR c_rec in C_COST LOOP
      L_item               := c_rec.item;
      L_supp_curr          := c_rec.supp_curr;
      L_supp_cost          := c_rec.supp_cost;
      L_supp_ebc_cost      := c_rec.supp_ebc_cost;
      L_supp_nic_cost      := c_rec.supp_nic_cost;
      L_local_curr         := c_rec.loc_curr;
      L_old_cost           := c_rec.loc_cost;
      L_old_av_cost        := c_rec.av_cost;
      L_local_retail       := c_rec.loc_retail;
      L_loc_type           := c_rec.loc_type;
      L_soh                := c_rec.soh;
      L_status             := c_rec.status;

      if I_loc is NOT NULL then
         if ITEM_COST_SQL.GET_DEFAULT_PO_COST(O_error_message,
                                              L_default_po_cost,
                                              NULL,
                                              I_loc) = FALSE then
            return FALSE;
         end if;
      end if;

      -- Get L_local_cost (supplier unit cost), L_loc_ebc_cost, L_loc_nic_cost values
      -- (convert from supplier's currency to local currency, if necessary)
      if L_supp_curr != L_local_curr then
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_supp_cost,
                                     L_supp_curr,
                                     L_local_curr,
                                     L_local_cost,
                                     'C',
                                     NULL,
                                     NULL) then
            return FALSE;
         end if;
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_supp_ebc_cost,
                                     L_supp_curr,
                                     L_local_curr,
                                     L_loc_ebc_cost,
                                     'C',
                                     NULL,
                                     NULL) then
            return FALSE;
         end if;      
         if not CURRENCY_SQL.CONVERT(O_error_message,
                                     L_supp_nic_cost,
                                     L_supp_curr,
                                     L_local_curr,
                                     L_loc_nic_cost,
                                     'C',
                                     NULL,
                                     NULL) then
            return FALSE;
         end if;
      else
         L_local_cost   := L_supp_cost;
         L_loc_ebc_cost := L_supp_ebc_cost;
         L_loc_nic_cost := L_supp_nic_cost;
      end if;
      ---
      if L_system_options_rec.default_tax_type <> 'GTAX' then
         L_loc_ebc_cost := L_local_cost;
         L_loc_nic_cost := L_local_cost;
      end if;         
      ---     
      if L_status = 'A' then
         ---
         SQL_LIB.SET_MARK('OPEN',
                          NULL,
                          'PRICE_HIST',
                          'Item:' || L_item || 'Loc:' || I_loc || 'Action Date:' || L_active_date);
         open C_CHECK_PRICE_HIST;

         SQL_LIB.SET_MARK('FETCH',
                          NULL,
                          'PRICE_HIST',
                          'Item:' || L_item || 'Loc:' || I_loc || 'Action Date:' || L_active_date);
         fetch C_CHECK_PRICE_HIST into L_price_hist_ind;

         SQL_LIB.SET_MARK('CLOSE',
                          NULL,
                          'PRICE_HIST',
                          'Item:' || L_item || 'Loc:' || I_loc || 'Action Date:' || L_active_date);
         close C_CHECK_PRICE_HIST;

         if L_price_hist_ind is not NULL then
            SQL_LIB.SET_MARK('UPDATE',
                             NULL,
                             'PRICE_HIST',
                             'Item:' || L_item || 'Loc:' || I_loc || 'Action Date:' || L_active_date);
            update price_hist
               set unit_cost   = L_local_cost,
                   unit_retail = L_local_retail
             where item        = L_item
               and loc         = I_loc
               and action_date = L_active_date
               and tran_type   = 2
               and reason      = 7;
         else
            SQL_LIB.SET_MARK('INSERT',
                             NULL,
                             'PRICE_HIST',
                             'Item: ' || L_item || ' Loc: ' || I_loc);
            insert into price_hist(TRAN_TYPE,
                                   REASON,
                                   EVENT,
                                   ITEM,
                                   LOC,
                                   UNIT_COST,
                                   UNIT_RETAIL,
                                   ACTION_DATE,
                                   MULTI_UNITS,
                                   MULTI_UNIT_RETAIL,
                                   SELLING_UNIT_RETAIL,
                                   SELLING_UOM,
                                   MULTI_SELLING_UOM,
                                   LOC_TYPE)
                           values (2,                     -- unit cost change
                                   7,                     -- itemloc's prim_supp or prim_cntry change
                                   NULL,                  -- event
                                   L_item,
                                   I_loc,
                                   L_local_cost,
                                   L_local_retail,
                                   L_active_date,
                                   NULL,                  -- multi units
                                   NULL,                  -- multi units retail
                                   NULL,                  -- selling unit retail
                                   NULL,                  -- selling UOM
                                   NULL,                  -- multi selling UOM
                                   L_loc_type);
         end if;
      end if;

      if not ITEM_ATTRIB_SQL.GET_PACK_INDS(O_error_message,
                                           L_pack_ind,
                                           L_sellable,
                                           L_orderable,
                                           L_pack_type,
                                           L_item) then
         return FALSE;
      end if;
      
       L_table := 'ITEM_LOC_SOH';
       SQL_LIB.SET_MARK('UPDATE',
                        NULL,
                        'ITEM_LOC_SOH',
                        'Item: ' || L_item || ' Loc: ' || I_loc);
       open C_LOCK_ITEM_LOC_SOH;
       close C_LOCK_ITEM_LOC_SOH;
       -- for Approved items, average cost should not be updated when the primary supplier is changed         
       update item_loc_soh
          set unit_cost            = DECODE(L_default_po_cost,'BC',L_local_cost,'NIC',L_loc_nic_cost),
              av_cost              = DECODE(L_pack_ind,'N',DECODE(L_status,'A',av_cost,L_loc_ebc_cost),NULL),
              last_update_datetime = sysdate,
              last_update_id       = get_user
        where item                 = L_item
          and loc                  = I_loc;
      if L_pack_ind = 'N' then  
         -- only non-pack items have average cost stored in ITEM_LOC_SOH
         ---objects for L10N_FIN_REC
         L_l10n_fin_rec.procedure_key   := 'UPDATE_AV_COST';
         L_l10n_fin_rec.item            := L_item;
         L_l10n_fin_rec.source_entity   := 'LOC';
         L_l10n_fin_rec.source_id       := I_loc;
         L_l10n_fin_rec.source_type     := L_loc_type;

         ---making the EBC cost as av_cost for l10n
         if L_status = 'A' then
            L_l10n_fin_rec.av_cost      := ROUND(L_old_av_cost,4);
         else
            L_l10n_fin_rec.av_cost      := ROUND(L_loc_ebc_cost,4);
         end if;

         if L10N_SQL.EXEC_FUNCTION(O_error_message,
                                   L_l10n_fin_rec)= FALSE then
            return FALSE;
         end if;
      end if;

      if (LP_std_av_ind = 'S' and L_soh > 0 and L_pack_ind = 'N') then
         if not ITEM_ATTRIB_SQL.GET_MERCH_HIER(O_error_message,
                                               L_item,
                                               L_dept,
                                               L_class,
                                               L_subclass) then
            return FALSE;
         end if;
         ---
         L_tran_code := 70;
         ---
         L_total_cost := (L_old_cost - L_local_cost) * L_soh;
         ---
         if not STKLEDGR_SQL.TRAN_DATA_INSERT(O_error_message,
                                              L_item,
                                              L_dept,
                                              L_class,
                                              L_subclass,
                                              I_loc,
                                              L_loc_type,
                                              L_active_date,
                                              L_tran_code,
                                              NULL,
                                              L_soh,
                                              L_total_cost,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL,
                                              L_old_cost,
                                              L_local_cost,
                                              L_dept,
                                              L_class,
                                              L_subclass,
                                              L_program) then
            return FALSE;
         end if;
      end if;      
   end LOOP;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := O_error_message || SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                               L_table,
                                                               I_item,
                                                               I_loc);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END CHG_ITEMLOC_PRIM_SUPP_CNTRY;
------------------------------------------------------------------------------------
FUNCTION ELC_CALLS(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                   I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                   I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'ELC_CALLS';

BEGIN

   if LP_elc_ind is NULL then
     if not SYSTEM_OPTIONS_SQL.GET_ELC_IND(O_error_message,
                                           LP_elc_ind) then
        return FALSE;
     end if;
   end if;

   /* if elc is not being used don't bother */
   if LP_elc_ind = 'N' then
      return TRUE;
   end if;

   /*call package to update item expenses */
   if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                 'IE',        /*calc_type*/
                                 I_item,
                                 I_supplier,
                                 NULL,        /*item_exp_type*/
                                 NULL,        /*item_exp_seq*/
                                 NULL,        /*order_no*/
                                 NULL,        /*ord_seq_no*/
                                 NULL,        /*pack_item*/
                                 NULL,        /*zone_id*/
                                 NULL,        /*hts*/
                                 NULL,        /*import_origin_country*/
                                 I_origin_country_id,
                                 NULL,        /*effect_from*/
                                 NULL) then   /*effect_to*/
      return FALSE;
   end if;

   /* call package to update item assessments */
   if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                 'IA',        /*calc_type*/
                                 I_item,
                                 I_supplier,
                                 NULL,        /*item_exp_type*/
                                 NULL,        /*item_exp_seq*/
                                 NULL,        /*order_no*/
                                 NULL,        /*ord_seq_no*/
                                 NULL,        /*pack_item*/
                                 NULL,        /*zone_id*/
                                 NULL,        /*hts*/
                                 NULL,        /*import_origin_country*/
                                 I_origin_country_id,
                                 NULL,        /*effect_from*/
                                 NULL) then   /*effect_to*/
      return FALSE;
   end if;

   /* call package to update item expenses again because there may be some expenses that */
   /* are dependent on the updated assessments                                           */
   if not ELC_CALC_SQL.CALC_COMP(O_error_message,
                                 'IE',        /*calc_type*/
                                 I_item,
                                 I_supplier,
                                 NULL,        /*item_exp_type*/
                                 NULL,        /*item_exp_seq*/
                                 NULL,        /*order_no*/
                                 NULL,        /*ord_seq_no*/
                                 NULL,        /*pack_item*/
                                 NULL,        /*zone_id*/
                                 NULL,        /*hts*/
                                 NULL,        /*import_origin_country*/
                                 I_origin_country_id,
                                 NULL,        /*effect_from*/
                                 NULL) then   /*effect_to*/
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END ELC_CALLS;

------------------------------------------------------------------------------------
FUNCTION CHANGE_COST(O_error_message     IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                     I_item              IN     ITEM_SUPP_COUNTRY.ITEM%TYPE,
                     I_supplier          IN     ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                     I_origin_country_id IN     ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                     I_loc               IN     ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                     I_process_children  IN     VARCHAR2,
                     I_update_cost_ind   IN     VARCHAR2,
                     I_cost_change       IN     COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE)
RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.CHANGE_COST';
   L_table           VARCHAR2(64) := NULL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_item            item_supp_country.item%TYPE := NULL;

   L_unit_cost       item_supp_country.unit_cost%TYPE := NULL;
   L_parent_cost     item_supp_country.unit_cost%TYPE := NULL;
   L_exist           VARCHAR2(1) := 'N';

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;
   
   L_tax_calc_rec           OBJ_TAX_CALC_REC := OBJ_TAX_CALC_REC();
   L_tax_calc_tbl           OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := NULL;
   L_tax_rate               ITEM_COST_DETAIL.COMP_RATE%TYPE := 0;
   L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE;
   L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE;
   L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE;
   
   cursor C_PRIM_LOC is
       select 'Y'
         from item_loc
        where item = L_item
          and primary_supp = I_supplier
          and primary_cntry = I_origin_country_id
          and loc = I_loc;

   cursor C_LOCK_PRIM_LOC is
       select iscl.unit_cost,
              iscl.inclusive_cost
         from item_supp_country isc,
              item_supp_country_loc iscl
        where iscl.item = L_item
          and iscl.supplier = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and iscl.loc = I_loc
          and iscl.primary_loc_ind = 'Y'
          and iscl.item = isc.item
          and iscl.supplier = isc.supplier
          and iscl.origin_country_id = isc.origin_country_id
          for update of isc.unit_cost,isc.inclusive_cost,isc.extended_base_cost,isc.base_cost,
                        isc.negotiated_item_cost,isc.last_update_datetime,isc.last_update_id nowait;

   cursor C_GET_CHILDREN is
       select iscl.item
         from item_master im,
              item_supp_country_loc iscl
        where iscl.item = im.item
          and iscl.supplier = I_supplier
          and iscl.origin_country_id = I_origin_country_id
          and iscl.loc = I_loc
          and (im.item_parent = I_item or
               im.item_grandparent = I_item)
          for update of iscl.unit_cost nowait;

   cursor C_DEFAULT_PARENT_COST is
       select unit_cost,
              inclusive_cost
         from item_supp_country_loc
        where item = I_item
          and supplier = I_supplier
          and origin_country_id = I_origin_country_id
          and loc = I_loc;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_item',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_supplier',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   if I_origin_country_id is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_origin_country_id',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   if I_loc is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_loc',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   /* process the parent item first */
   L_item := I_item;

   SQL_LIB.SET_MARK('OPEN','C_PRIM_LOC','ITEM_LOC',
                    'Item: '||L_item
                    || ' Supplier: '||I_supplier
                    || ' Origin Country: '||I_origin_country_id
                    || ' Loc: '||I_loc);
   open  C_PRIM_LOC;
   SQL_LIB.SET_MARK('FETCH','C_PRIM_LOC','ITEM_LOC',
                    'Item: '||L_item
                    || ' Supplier: '||I_supplier
                    || ' Origin Country: '||I_origin_country_id
                    || ' Loc: '||I_loc);
   fetch C_PRIM_LOC into L_exist;
   SQL_LIB.SET_MARK('CLOSE','C_PRIM_LOC','ITEM_LOC',
                    'Item: '||L_item
                    || ' Supplier: '||I_supplier
                    || ' Origin Country: '||I_origin_country_id
                    || ' Loc: '||I_loc);
   close C_PRIM_LOC;

   if L_exist = 'Y' then
      if not CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                         L_item,
                                         I_loc,
                                         I_supplier,
                                         I_origin_country_id,
                                         'N',
                                         I_cost_change) then
         return FALSE;
      end if;
   end if;

   /* push the new unit_cost up to item_supp_country if the location
      is the primary location for the supplier/country */
   L_table := 'ITEM_SUPP_COUNTRY';
   SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                    'Item: '||L_item
                    || ' Supplier: ' ||I_supplier
                    || ' Origin Country: ' ||I_origin_country_id);
   open C_LOCK_PRIM_LOC;
   fetch C_LOCK_PRIM_LOC into L_unit_cost,L_inclusive_cost;
   if (C_LOCK_PRIM_LOC%FOUND) then
      close C_LOCK_PRIM_LOC;
      if L_system_options_rec.default_tax_type = 'GTAX' then
         update item_supp_country
            set unit_cost = L_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where item = L_item
            and supplier = I_supplier
            and origin_country_id = I_origin_country_id;
     else
         update item_supp_country
            set unit_cost = L_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user,
                negotiated_item_cost = NULL,
                extended_base_cost = NULL,
                inclusive_cost = NULL,
                base_cost = NULL
          where item = L_item
            and supplier = I_supplier
            and origin_country_id = I_origin_country_id;
      end if;

      /* call the elc packages */
      if not ELC_CALLS(O_error_message,
                       L_item,
                       I_supplier,
                       I_origin_country_id) then
         return FALSE;
      end if;

   else
      close C_LOCK_PRIM_LOC;
   end if;

   /* push the cost change down to children records */
   if I_process_children = 'Y' then

      /* get the parent's iscl cost to push down to the children */
      SQL_LIB.SET_MARK('OPEN','C_DEFAULT_PARENT_COST','ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item
                       || ' Supplier: '||I_supplier
                       || ' Origin Country: '||I_origin_country_id
                       || ' Loc: '||I_loc);
      open  C_DEFAULT_PARENT_COST;
      SQL_LIB.SET_MARK('FETCH','C_DEFAULT_PARENT_COST','ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item
                       || ' Supplier: '||I_supplier
                       || ' Origin Country: '||I_origin_country_id
                       || ' Loc: '||I_loc);
      fetch C_DEFAULT_PARENT_COST into L_parent_cost,L_inclusive_cost;
      SQL_LIB.SET_MARK('CLOSE','C_DEFAULT_PARENT_COST','ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item
                       || ' Supplier: '||I_supplier
                       || ' Origin Country: '||I_origin_country_id
                       || ' Loc: '||I_loc);
      close C_DEFAULT_PARENT_COST;

      FOR c_rec in C_GET_CHILDREN LOOP

         L_item := c_rec.item;

         if I_update_cost_ind = 'Y' then
            L_table := 'ITEM_SUPP_COUNTRY_LOC';
            if L_system_options_rec.default_tax_type = 'GTAX' then
               SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                                'Item: '||L_item
                                || ' Supplier: ' ||to_char(I_supplier)
                                || ' Origin Country: ' ||I_origin_country_id
                                || ' Location: ' ||to_char(I_loc));
               update item_supp_country_loc
                  set unit_cost = NVL(L_parent_cost, unit_cost),
                      last_update_datetime = sysdate,
                      last_update_id = get_user
                where item = L_item
                  and loc = I_loc
                  and supplier = I_supplier
                  and origin_country_id = I_origin_country_id;
            else
               SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                                'Item: '||L_item
                                || ' Supplier: ' ||to_char(I_supplier)
                                || ' Origin Country: ' ||I_origin_country_id
                                || ' Location: ' ||to_char(I_loc));
               update item_supp_country_loc
                  set unit_cost = NVL(L_parent_cost, unit_cost),
                      last_update_datetime = sysdate,
                      last_update_id = get_user,
                      negotiated_item_cost = NULL,
                      extended_base_cost = NULL,
                      inclusive_cost = NULL,
                      base_cost = NULL
                where item = L_item
                  and loc = I_loc
                  and supplier = I_supplier
                  and origin_country_id = I_origin_country_id;
            end if;

         end if; -- I_update_child_ind = 'Y'

         SQL_LIB.SET_MARK('OPEN','C_PRIM_LOC','ITEM_LOC',
                          'Item: '||L_item
                          || ' Supplier: '||I_supplier
                          || ' Origin Country: '||I_origin_country_id
                          || ' Loc: '||I_loc);
         open  C_PRIM_LOC;
         SQL_LIB.SET_MARK('FETCH','C_PRIM_LOC','ITEM_LOC',
                          'Item: '||L_item
                          || ' Supplier: '||I_supplier
                          || ' Origin Country: '||I_origin_country_id
                          || ' Loc: '||I_loc);
         fetch C_PRIM_LOC into L_exist;
         SQL_LIB.SET_MARK('CLOSE','C_PRIM_LOC','ITEM_LOC',
                          'Item: '||L_item
                          || ' Supplier: '||I_supplier
                          || ' Origin Country: '||I_origin_country_id
                          || ' Loc: '||I_loc);
         close C_PRIM_LOC;

         if L_exist = 'Y' then
            if not CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                               L_item,
                                               I_loc,
                                               I_supplier,
                                               I_origin_country_id,
                                               'N',
                                               I_cost_change) then
               return FALSE;
            end if;
         end if;

         /* push the new unit_cost up to item_supp_country if the location
            is the primary location for the supplier/country */
         /* this does overwrite the value in l_unit_cost that was populated
            outside of the loop, but it is selecting the value that was just
            updated using that variable so it will never be differrent.  It
            needs to work this way for the parent section to work. */
         L_table := 'ITEM_SUPP_COUNTRY';
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY',
                          'Item: '||L_item
                          || ' Supplier: ' ||I_supplier
                          || ' Origin Country: ' ||I_origin_country_id);
         open C_LOCK_PRIM_LOC;
         fetch C_LOCK_PRIM_LOC into L_unit_cost,L_inclusive_cost;
         if (C_LOCK_PRIM_LOC%FOUND) then
            close C_LOCK_PRIM_LOC;

            if L_system_options_rec.default_tax_type = 'GTAX' then
               update item_supp_country
                  set unit_cost = L_unit_cost,
                      last_update_datetime = sysdate,
                      last_update_id = get_user
                where item = L_item
                  and supplier = I_supplier
                  and origin_country_id = I_origin_country_id;
            else
               update item_supp_country
                  set unit_cost = L_unit_cost,
                      last_update_datetime = sysdate,
                      last_update_id = get_user,
                      negotiated_item_cost = NULL,
                      extended_base_cost = NULL,
                      inclusive_cost =  NULL,
                      base_cost = NULL
                where item = L_item
                  and supplier = I_supplier
                  and origin_country_id = I_origin_country_id;
            end if;

            /* call the elc packages */
            if not ELC_CALLS(O_error_message,
                             L_item,
                             I_supplier,
                             I_origin_country_id) then
               return FALSE;
            end if;

         else
            close C_LOCK_PRIM_LOC;
         end if;

      end LOOP;

   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      if C_PRIM_LOC%ISOPEN then
         close C_PRIM_LOC;
      end if;
      if C_LOCK_PRIM_LOC%ISOPEN then
         close C_LOCK_PRIM_LOC;
      end if;
      if C_GET_CHILDREN%ISOPEN then
               close C_GET_CHILDREN ;
      end if;
      if C_DEFAULT_PARENT_COST%ISOPEN then
         close C_DEFAULT_PARENT_COST;
      end if;   
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               L_table,
                                               L_item,
                                               I_loc);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_COST;

------------------------------------------------------------------------------------
FUNCTION CHANGE_ISC_COST(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item                IN       ITEM_SUPP_COUNTRY.ITEM%TYPE,
                         I_supplier            IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                         I_origin_country_id   IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                         I_all_locs            IN       VARCHAR2)
RETURN BOOLEAN IS

   L_program       VARCHAR2(64) := 'UPDATE_BASE_COST.CHANGE_ISC_COST';
   L_table         VARCHAR2(64) := NULL;
   RECORD_LOCKED   EXCEPTION;
   PRAGMA          EXCEPTION_INIT(Record_Locked, -54);

   L_loc                    ITEM_LOC.LOC%TYPE := NULL;
   L_loc_type               ITEM_LOC.LOC_TYPE%TYPE := NULL;
   L_negotiated_item_cost   ITEM_SUPP_COUNTRY.NEGOTIATED_ITEM_COST%TYPE := 0;
   L_extended_base_cost     ITEM_SUPP_COUNTRY.EXTENDED_BASE_COST%TYPE := 0;
   L_inclusive_cost         ITEM_SUPP_COUNTRY.INCLUSIVE_COST%TYPE := 0;
   L_dlvry_ctry_id          ITEM_COST_HEAD.DELIVERY_COUNTRY_ID%TYPE := NULL;
   L_nic_static_ind         ITEM_COST_HEAD.NIC_STATIC_IND%TYPE := NULL;
   L_exists                 VARCHAR2(1) := 'N';
   L_cost_change            COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE := NULL;
   L_item_record            ITEM_MASTER%ROWTYPE;
   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE := NULL;
   L_prim_loc               ITEM_SUPP_COUNTRY_LOC.LOC%TYPE := NULL;
   L_prim_loc_type          ITEM_SUPP_COUNTRY_LOC.LOC_TYPE%TYPE := NULL;
   L_base_cost              ITEM_SUPP_COUNTRY.BASE_COST%TYPE := 0;
   L_unit_cost              ITEM_SUPP_COUNTRY.UNIT_COST%TYPE := 0;
   L_physical_wh            WH.PHYSICAL_WH%TYPE := NULL;
   L_comp_item_cost_rec     OBJ_COMP_ITEM_COST_REC := OBJ_COMP_ITEM_COST_REC();
   L_comp_item_cost_tbl     OBJ_COMP_ITEM_COST_TBL := OBJ_COMP_ITEM_COST_TBL();
   L_country_attrib_row     COUNTRY_ATTRIB%ROWTYPE;
   L_vdate                  PERIOD.VDATE%TYPE := GET_VDATE;

   cursor C_PRIM_LOC_VIRTUAL is
      select w.physical_wh
        from wh w,
             item_supp_country_loc isl
       where isl.item = I_item
         and isl.supplier = I_supplier
         and isl.origin_country_id = I_origin_country_id
         and isl.primary_loc_ind = 'Y'
         and w.wh = isl.loc
         and w.stockholding_ind = 'Y';

   cursor C_GET_LOC is
      select loc,
             loc_type
        from item_loc
       where item = I_item;

   cursor C_GET_ALL_VWH is
      select loc,
             loc_type 
       from  item_supp_country_loc iscl,
             wh
       where item                 = I_item
         and supplier             = I_supplier
         and origin_country_id    = I_origin_country_id
         and iscl.loc             = wh.wh
         and wh.physical_wh       = L_physical_wh;

   cursor C_GET_DLVRY_CTRY is
      select country_id
        from mv_l10n_entity
       where (entity = 'LOC' or (entity = 'PTNR' and entity_type = 'E'))
         and entity_id = to_char(nvl(L_loc,L_prim_loc));

   cursor C_GET_UNIT_COST is
      select unit_cost,
             negotiated_item_cost,
             extended_base_cost,
             inclusive_cost,
             base_cost
        from item_supp_country
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id;

   cursor C_GET_PRIM_ISCL is
      select loc,
             loc_type
        from item_supp_country_loc
       where item = I_item
         and supplier = I_supplier
         and origin_country_id = I_origin_country_id
         and primary_loc_ind = 'Y';
         
   cursor C_PRIM_LOC is
     select 'Y'
       from item_loc_soh
      where item = I_item
        and primary_supp = I_supplier
        and primary_cntry = I_origin_country_id
     and loc = L_loc;

BEGIN
   if I_item is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_item',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;
   if I_supplier is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_supplier',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;
   if I_origin_country_id is NULL then
      O_error_message:= SQL_LIB.create_msg('INVALID_PARM', 'I_origin_country_id',
                                           'NULL', 'NOT NULL');
      RETURN FALSE;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options_rec) = FALSE then
         return FALSE;
   end if;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_record,
                                      I_item) = FALSE then
      return FALSE;
   end if;



   SQL_LIB.SET_MARK('OPEN','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',
                    'Item: ' || I_item || ' ' ||
                    'Supplier: ' || I_supplier || ' ' ||
                    'Origin Country ID: ' || I_origin_country_id);
   OPEN C_GET_UNIT_COST;
   
   SQL_LIB.SET_MARK('FETCH','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',
                    'Item: ' || I_item || ' ' ||
                    'Supplier: ' || I_supplier || ' ' ||
                    'Origin Country ID: ' || I_origin_country_id);
   FETCH C_GET_UNIT_COST INTO L_unit_cost,
                              L_negotiated_item_cost,
                              L_extended_base_cost,
                              L_inclusive_cost,
                              L_base_cost;
                              
   SQL_LIB.SET_MARK('CLOSE','C_GET_UNIT_COST','ITEM_SUPP_COUNTRY',
                    'Item: ' || I_item || ' ' ||
                    'Supplier: ' || I_supplier || ' ' ||
                    'Origin Country ID: ' || I_origin_country_id);
   CLOSE C_GET_UNIT_COST;
                       
   if (I_all_locs = 'Y') then
      SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                       'Item: '||I_item||
                       ' Supplier: ' ||to_char(I_supplier)||
                       ' Origin Country: ' ||I_origin_country_id);
      if L_system_options_rec.default_tax_type in ('SALES', 'SVAT') then

         update item_supp_country_loc
            set unit_cost            = L_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user,
                negotiated_item_cost = NULL,
                extended_base_cost = NULL,
                inclusive_cost = NULL,
                base_cost = NULL
          where item = I_item
            and supplier = I_supplier
            and origin_country_id = I_origin_country_id;

            FOR c_rec IN c_get_loc LOOP
               L_loc := c_rec.loc;
               L_exists := 'N';
               SQL_LIB.SET_MARK('OPEN',
                                'C_PRIM_LOC',
                                'ITEM_LOC_SOH',
                                NULL);
               open C_PRIM_LOC;
        
               SQL_LIB.SET_MARK('CLOSE',
                                'C_PRIM_LOC',
                                'ITEM_LOC_SOH',
                                NULL);      
               fetch C_PRIM_LOC INTO L_exists;
        
               SQL_LIB.SET_MARK('CLOSE',
                                'C_PRIM_LOC',
                                'ITEM_LOC_SOH',
                                NULL);
               close C_PRIM_LOC;
        
             --update the item_loc_soh and price_hist tables.
             if L_item_record.status != 'A'  and L_exists ='Y' then
               if not CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                                  I_item,
                                                  L_loc,
                                                  I_supplier,
                                                  I_origin_country_id,
                                                  'N',
                                                  L_cost_change) then
                  return FALSE;
               end if;
             end if;
            END LOOP;
      elsif L_system_options_rec.default_tax_type = 'GTAX' then

         L_extended_base_cost := NULL;
         L_inclusive_cost := NULL;
         FOR c_rec IN c_get_loc LOOP
            L_loc      := c_rec.loc;
            L_loc_type := c_rec.loc_type;

            SQL_LIB.SET_MARK('OPEN','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
            OPEN C_GET_DLVRY_CTRY;

            SQL_LIB.SET_MARK('FETCH','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
            FETCH C_GET_DLVRY_CTRY INTO L_dlvry_ctry_id;

            SQL_LIB.SET_MARK('CLOSE','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
            CLOSE C_GET_DLVRY_CTRY;

            if ITEM_COST_SQL.GET_NIC_STATIC_IND (O_error_message,
                                                 L_nic_static_ind,
                                                 I_item,
                                                 I_supplier,
                                                 I_origin_country_id,
                                                 L_dlvry_ctry_id) = FALSE then
               return FALSE;
            end if;
            if L_nic_static_ind is NULL then 
               if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                          L_country_attrib_row,
                                                          NULL,
                                                          L_dlvry_ctry_id) = FALSE then
                  return FALSE;
                end if;                
                L_nic_static_ind := L_country_attrib_row.item_cost_tax_incl_ind;
            end if;

            if L_nic_static_ind = 'N' then
               L_negotiated_item_cost := NULL;
               L_base_cost := L_unit_cost;
            elsif L_nic_static_ind = 'Y' then
               L_base_cost := NULL;
               L_negotiated_item_cost := L_unit_cost;
            end if;

            L_comp_item_cost_rec.I_item                       := I_item;
            L_comp_item_cost_rec.I_nic_static_ind             := L_nic_static_ind;
            L_comp_item_cost_rec.I_supplier                   := I_supplier;
            L_comp_item_cost_rec.I_location                   := L_loc;
            L_comp_item_cost_rec.I_loc_type                   := L_loc_type;
            L_comp_item_cost_rec.I_calling_form               := 'ITEMSUPPCTRYLOC';
            L_comp_item_cost_rec.I_update_itemcost_ind        := 'N';  -- Do NOT write ITEM_COST_HEAD/DETAIL
            L_comp_item_cost_rec.I_update_itemcost_child_ind  := 'N';
            L_comp_item_cost_rec.I_item_cost_tax_incl_ind     := L_nic_static_ind;
            L_comp_item_cost_rec.O_base_cost                  := L_base_cost;
            L_comp_item_cost_rec.O_extended_base_cost         := L_extended_base_cost;
            L_comp_item_cost_rec.O_inclusive_cost             := L_inclusive_cost;
            L_comp_item_cost_rec.O_negotiated_item_cost       := L_negotiated_item_cost;

            if L_system_options_rec.default_tax_type ='GTAX' then
               L_comp_item_cost_rec.I_effective_date          := L_vdate;
               L_comp_item_cost_rec.I_origin_country_id       := I_origin_country_id;
            end if;

            L_comp_item_cost_tbl.EXTEND;
            L_comp_item_cost_tbl(L_comp_item_cost_tbl.COUNT) := L_comp_item_cost_rec;
               --
         END LOOP;
         --
         if L_comp_item_cost_tbl.COUNT > 0 then
            if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK (O_error_message,
                                                     L_comp_item_cost_tbl) = FALSE then
               return FALSE;
            end if;
            --
            FOR k IN L_comp_item_cost_tbl.FIRST..L_comp_item_cost_tbl.LAST LOOP
               if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST (O_error_message,
                                                            I_item,
                                                            I_supplier,
                                                            I_origin_country_id ,
                                                            L_comp_item_cost_tbl(k).I_location,
                                                            L_comp_item_cost_tbl(k).O_base_cost,
                                                            L_comp_item_cost_tbl(k).O_extended_base_cost,
                                                            L_comp_item_cost_tbl(k).O_negotiated_item_cost,
                                                            L_comp_item_cost_tbl(k).O_inclusive_cost,
                                                            'ITEMSUPPCTRYLOC') = FALSE then
                  return FALSE;
               end if;
            END LOOP;
         end if;
      end if;
   else  /* I_all_locs = 'N' */
      ---
      SQL_LIB.SET_MARK('OPEN','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      open C_PRIM_LOC_VIRTUAL;
      SQL_LIB.SET_MARK('FETCH','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                       ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
      fetch C_PRIM_LOC_VIRTUAL into L_physical_wh;
      ---
      if C_PRIM_LOC_VIRTUAL%NOTFOUND then
         ---
         SQL_LIB.SET_MARK('CLOSE','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         close C_PRIM_LOC_VIRTUAL;
         ---
         SQL_LIB.SET_MARK('OPEN','C_GET_PRIM_ISCL','ITEM_SUPP_COUNTRY_LOC','Item: ' || I_item||' Supplier:'|| I_supplier||' Origin_country_id:'|| I_origin_country_id);
         OPEN C_GET_PRIM_ISCL;
 
         SQL_LIB.SET_MARK('FETCH','C_GET_PRIM_ISCL','ITEM_SUPP_COUNTRY_LOC','Item: ' || I_item||' Supplier:'|| I_supplier||' Origin_country_id:'|| I_origin_country_id);
         FETCH C_GET_PRIM_ISCL INTO L_loc,L_loc_type;

         SQL_LIB.SET_MARK('CLOSE','C_GET_PRIM_ISCL','ITEM_SUPP_COUNTRY_LOC','Item: ' || I_item||' Supplier:'|| I_supplier||' Origin_country_id:'|| I_origin_country_id);
         CLOSE C_GET_PRIM_ISCL;
         if L_system_options_rec.default_tax_type in ('SALES', 'SVAT') then
            update item_supp_country_loc
               set unit_cost            = L_unit_cost,
                   last_update_datetime = sysdate,
                   last_update_id = get_user,
                   negotiated_item_cost = NULL,
                   extended_base_cost = NULL,
                   inclusive_cost = NULL,
                   base_cost = NULL
             where item = I_item
               and supplier = I_supplier
               and origin_country_id = I_origin_country_id
               and primary_loc_ind = 'Y'
               and loc = L_loc;
               
               SQL_LIB.SET_MARK('OPEN',
                               'C_PRIM_LOC',
                               'ITEM_LOC_SOH',
                               NULL);
              open C_PRIM_LOC;
       
              SQL_LIB.SET_MARK('CLOSE',
                               'C_PRIM_LOC',
                               'ITEM_LOC_SOH',
                               NULL);      
              fetch C_PRIM_LOC INTO L_exists;
       
              SQL_LIB.SET_MARK('CLOSE',
                               'C_PRIM_LOC',
                               'ITEM_LOC_SOH',
                               NULL);
              close C_PRIM_LOC;

            --update the item_loc_soh and price_hist tables.
            if L_item_record.status != 'A'  and L_exists ='Y' then
               if not CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                                  I_item,
                                                  L_loc,
                                                  I_supplier,
                                                  I_origin_country_id,
                                                  'N',
                                                  L_cost_change) then
                  return FALSE;
               end if;
            end if;

         elsif L_system_options_rec.default_tax_type = 'GTAX' then

            L_extended_base_cost := NULL;
            L_inclusive_cost := NULL;

            SQL_LIB.SET_MARK('OPEN','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
            OPEN C_GET_DLVRY_CTRY;

            SQL_LIB.SET_MARK('FETCH','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
            FETCH C_GET_DLVRY_CTRY INTO L_dlvry_ctry_id;

            SQL_LIB.SET_MARK('CLOSE','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
            CLOSE C_GET_DLVRY_CTRY;

            if ITEM_COST_SQL.GET_NIC_STATIC_IND (O_error_message,
                                                 L_nic_static_ind,
                                                 I_item,
                                                 I_supplier,
                                                 I_origin_country_id,
                                                 L_dlvry_ctry_id) = FALSE then
               return FALSE;
            end if;

            if L_nic_static_ind is NULL then 
               if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                          L_country_attrib_row,
                                                          NULL,
                                                          L_dlvry_ctry_id) = FALSE then
                  return FALSE;
                end if;                
                L_nic_static_ind := L_country_attrib_row.item_cost_tax_incl_ind;
            end if;

            if L_nic_static_ind = 'N' then
               L_negotiated_item_cost := NULL;
               L_base_cost := L_unit_cost;
            elsif L_nic_static_ind = 'Y' then
               L_base_cost := NULL;
               L_negotiated_item_cost := L_unit_cost;
            end if;
         
            L_comp_item_cost_rec.I_item                       := I_item;
            L_comp_item_cost_rec.I_nic_static_ind             := L_nic_static_ind;
            L_comp_item_cost_rec.I_supplier                   := I_supplier;
            L_comp_item_cost_rec.I_location                   := L_loc;
            L_comp_item_cost_rec.I_loc_type                   := L_loc_type;
            L_comp_item_cost_rec.I_calling_form               := 'ITEMSUPPCTRYLOC';
            L_comp_item_cost_rec.I_update_itemcost_ind        := 'N';  -- Do NOT write ITEM_COST_HEAD/DETAIL
            L_comp_item_cost_rec.I_update_itemcost_child_ind  := 'N';
            L_comp_item_cost_rec.I_item_cost_tax_incl_ind     := L_nic_static_ind;
            L_comp_item_cost_rec.O_base_cost                  := L_base_cost;
            L_comp_item_cost_rec.O_extended_base_cost         := L_extended_base_cost;
            L_comp_item_cost_rec.O_inclusive_cost             := L_inclusive_cost;
            L_comp_item_cost_rec.O_negotiated_item_cost       := L_negotiated_item_cost;

            if L_system_options_rec.default_tax_type ='GTAX' then
               L_comp_item_cost_rec.I_effective_date          := L_vdate;
               L_comp_item_cost_rec.I_origin_country_id       := I_origin_country_id;
            end if;

            L_comp_item_cost_tbl.EXTEND;
            L_comp_item_cost_tbl(L_comp_item_cost_tbl.COUNT) := L_comp_item_cost_rec;
               --
            --
            if L_comp_item_cost_tbl.COUNT > 0 then
               if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK (O_error_message,
                                                        L_comp_item_cost_tbl) = FALSE then
                  return FALSE;
               end if;
               --
               FOR k IN L_comp_item_cost_tbl.FIRST..L_comp_item_cost_tbl.LAST LOOP
                  if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST (O_error_message,
                                                               I_item,
                                                               I_supplier,
                                                               I_origin_country_id,
                                                               L_comp_item_cost_tbl(k).I_location,
                                                               L_comp_item_cost_tbl(k).O_base_cost,
                                                               L_comp_item_cost_tbl(k).O_extended_base_cost,
                                                               L_comp_item_cost_tbl(k).O_negotiated_item_cost,
                                                               L_comp_item_cost_tbl(k).O_inclusive_cost,
                                                               'ITEMSUPPCTRYLOC') = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
            end if;
         end if;
      else  /* C_PRIM_LOC_VIRTUAL%FOUND */
         ---
         SQL_LIB.SET_MARK('CLOSE','C_PRIM_LOC_VIRTUAL','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||
                          ' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
         close C_PRIM_LOC_VIRTUAL;
         ---
         SQL_LIB.SET_MARK('UPDATE',NULL,'ITEM_SUPP_COUNTRY_LOC',
                          'Item: '||I_item||
                          ' Supplier: ' ||to_char(I_supplier)||
                          ' Origin Country: ' ||I_origin_country_id);
						  
         if L_system_options_rec.default_tax_type in ('SALES', 'SVAT') then

            update item_supp_country_loc iscl
               set unit_cost            = L_unit_cost,
                   last_update_datetime = sysdate,
                   last_update_id = get_user,
                   negotiated_item_cost = NULL,
                   extended_base_cost = NULL,
                   inclusive_cost = NULL,
                   base_cost = NULL
             where item = I_item
               and supplier = I_supplier
               and origin_country_id = I_origin_country_id
               and exists              (select 'x'
                                          from wh
                                         where iscl.loc = wh.wh
                                            and wh.physical_wh = L_physical_wh);

            SQL_LIB.SET_MARK('OPEN','C_GET_ALL_VWH','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
            FOR c_rec IN C_GET_ALL_VWH LOOP
               L_loc := c_rec.loc;
               L_exists := 'N';
               SQL_LIB.SET_MARK('OPEN',
                                'C_PRIM_LOC',
                                'ITEM_LOC_SOH',
                                 NULL);
               open C_PRIM_LOC;
     
               SQL_LIB.SET_MARK('CLOSE',
                                'C_PRIM_LOC',
                                'ITEM_LOC_SOH',
                                 NULL);      
               fetch C_PRIM_LOC INTO L_exists;
            
               SQL_LIB.SET_MARK('CLOSE',
                                'C_PRIM_LOC',
                                'ITEM_LOC_SOH',
                                 NULL);
                close C_PRIM_LOC;

               --update the item_loc_soh and price_hist tables.
               if L_item_record.status != 'A'  and L_exists ='Y' then
                  if not CHG_ITEMLOC_PRIM_SUPP_CNTRY(O_error_message,
                                                     I_item,
                                                     L_loc,
                                                     I_supplier,
                                                     I_origin_country_id,
                                                     'N',
                                                     L_cost_change) then
                     return FALSE;
                  end if;
               end if;
            END LOOP;
         

         elsif L_system_options_rec.default_tax_type = 'GTAX' then

            L_extended_base_cost := NULL;
            L_inclusive_cost := NULL;
            SQL_LIB.SET_MARK('OPEN','C_GET_ALL_VWH','ITEM_SUPP_COUNTRY_LOC, WH','Item: '||I_item||' Supplier: '||to_char(I_supplier)||' Country: '||I_origin_country_id);
            FOR c_rec IN C_GET_ALL_VWH LOOP
               L_loc      := c_rec.loc;
               L_loc_type := c_rec.loc_type;

               SQL_LIB.SET_MARK('OPEN','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
               OPEN C_GET_DLVRY_CTRY;

               SQL_LIB.SET_MARK('FETCH','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
               FETCH C_GET_DLVRY_CTRY INTO L_dlvry_ctry_id;

               SQL_LIB.SET_MARK('CLOSE','C_GET_DLVRY_CTRY','ITEM_COST_HEAD','Delivery Country Id: ' || L_loc);
               CLOSE C_GET_DLVRY_CTRY;

               if ITEM_COST_SQL.GET_NIC_STATIC_IND (O_error_message,
                                                    L_nic_static_ind,
                                                    I_item,
                                                    I_supplier,
                                                    I_origin_country_id,
                                                    L_dlvry_ctry_id) = FALSE then
                  return FALSE;
               end if;

               if L_nic_static_ind is NULL then 
                  if COUNTRY_VALIDATE_SQL.GET_COUNTRY_ATTRIB(O_error_message,
                                                             L_country_attrib_row,
                                                             NULL,
                                                             L_dlvry_ctry_id) = FALSE then
                     return FALSE;
                   end if;                
                   L_nic_static_ind := L_country_attrib_row.item_cost_tax_incl_ind;
               end if;

               if L_nic_static_ind = 'N' then
                  L_negotiated_item_cost := NULL;
                  L_base_cost := L_unit_cost;
               elsif L_nic_static_ind = 'Y' then
                  L_base_cost := NULL;
                  L_negotiated_item_cost := L_unit_cost;
               end if;

               L_comp_item_cost_rec.I_item                       := I_item;
               L_comp_item_cost_rec.I_nic_static_ind             := L_nic_static_ind;
               L_comp_item_cost_rec.I_supplier                   := I_supplier;
               L_comp_item_cost_rec.I_location                   := L_loc;
               L_comp_item_cost_rec.I_loc_type                   := L_loc_type;
               L_comp_item_cost_rec.I_calling_form               := 'ITEMSUPPCTRYLOC';
               L_comp_item_cost_rec.I_update_itemcost_ind        := 'N';  -- Do NOT write ITEM_COST_HEAD/DETAIL
               L_comp_item_cost_rec.I_update_itemcost_child_ind  := 'N';
               L_comp_item_cost_rec.I_item_cost_tax_incl_ind     := L_nic_static_ind;
               L_comp_item_cost_rec.O_base_cost                  := L_base_cost;
               L_comp_item_cost_rec.O_extended_base_cost         := L_extended_base_cost;
               L_comp_item_cost_rec.O_inclusive_cost             := L_inclusive_cost;
               L_comp_item_cost_rec.O_negotiated_item_cost       := L_negotiated_item_cost;

               if L_system_options_rec.default_tax_type ='GTAX' then
                  L_comp_item_cost_rec.I_effective_date          := L_vdate;
                  L_comp_item_cost_rec.I_origin_country_id       := I_origin_country_id;
               end if;

               L_comp_item_cost_tbl.EXTEND;
               L_comp_item_cost_tbl(L_comp_item_cost_tbl.COUNT) := L_comp_item_cost_rec;
                  --
            END LOOP;
            --
            if L_comp_item_cost_tbl.COUNT > 0 then
               if ITEM_COST_SQL.COMPUTE_ITEM_COST_BULK (O_error_message,
                                                        L_comp_item_cost_tbl) = FALSE then
                  return FALSE;
               end if;
               --
               FOR k IN L_comp_item_cost_tbl.FIRST..L_comp_item_cost_tbl.LAST LOOP
                  if ITEM_COST_SQL.UPD_ITEM_SUPP_COUNTRY_COST (O_error_message,
                                                               I_item,
                                                               I_supplier,
                                                               I_origin_country_id,
                                                               L_comp_item_cost_tbl(k).I_location,
                                                               L_comp_item_cost_tbl(k).O_base_cost,
                                                               L_comp_item_cost_tbl(k).O_extended_base_cost,
                                                               L_comp_item_cost_tbl(k).O_negotiated_item_cost,
                                                               L_comp_item_cost_tbl(k).O_inclusive_cost,
                                                               'ITEMSUPPCTRYLOC') = FALSE then
                     return FALSE;
                  end if;
               END LOOP;
            end if;
         end if;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message :=O_error_message|| SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                               L_table,
                                               I_item,
                                               I_supplier);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END CHANGE_ISC_COST;
------------------------------------------------------------------------------------
FUNCTION BULK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_isc_rows        IN       RMSSUB_XCOSTCHG.ROWID_TBL,
                  I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_ISC';
   L_rowid           ROWID;
   L_user            VARCHAR2(30):= get_user;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   cursor C_LOCK_ISC is
      select 'x'
        from item_supp_country isc
       where rowid = L_rowid
         for update nowait;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   FOR i in I_isc_rows.FIRST..I_isc_rows.LAST LOOP
      L_rowid := I_isc_rows(i);
      ---
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ISC','item',null);
      open  C_LOCK_ISC;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISC','item',null);
      close C_LOCK_ISC;
   end LOOP;
   if L_system_options_rec.default_tax_type = 'GTAX' then
    FORALL i in I_isc_rows.FIRST..I_isc_rows.LAST
         update item_supp_country
            set unit_cost = I_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = L_user
          where rowid = I_isc_rows(i);
   else
    FORALL i in I_isc_rows.FIRST..I_isc_rows.LAST
         update item_supp_country
            set unit_cost = I_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = L_user,
                negotiated_item_cost = NULL,
                extended_base_cost = NULL,
                inclusive_cost = NULL,
                base_cost = NULL
          where rowid = I_isc_rows(i);
   end if;
   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY',
                                             NULL,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BULK_ISC;
------------------------------------------------------------------------------------
FUNCTION BULK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_items           IN       ITEM_TBL,
                  I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE,
                  I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                  I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_ISC';

   L_user            VARCHAR2(30):= get_user;
   TAB_rowids        RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_ICH_rowids    RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_ICD_rowids    RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();   

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   cursor C_LOCK_ISC is
      select isc.rowid
        from item_supp_country isc,
             TABLE(cast(I_items as ITEM_TBL)) xitem
       where supplier = I_supplier
         and origin_country_id = I_country
         and item = value(xitem)
         for update of isc.item nowait;
         
   cursor C_GET_ISC_API_ROW is
      select isc.rowid
        from item_supp_country isc,
             api_iscl a
       where supplier = I_supplier
         and origin_country_id = I_country
         and isc.item = a.item
         and a.primary_loc_ind = 'Y'
         and a.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no
         for update nowait;
         
BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_items is NULL  or I_items.count = 0 then                 -- Multi/Single Loc
      SQL_LIB.SET_MARK('OPEN','C_GET_ISC_API_ROW','item',null);
      open C_GET_ISC_API_ROW;
      SQL_LIB.SET_MARK('FETCH','C_GET_ISC_API_ROW','item',null);
      fetch C_GET_ISC_API_ROW BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_GET_ISC_API_ROW','item',null);
      close C_GET_ISC_API_ROW;
      
   else                                                         -- No Loc

      SQL_LIB.SET_MARK('OPEN','C_LOCK_ISC','item',null);
      open  C_LOCK_ISC;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_ISC','item',null);
      fetch  C_LOCK_ISC BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISC','item',null);
      close C_LOCK_ISC;
   end if;

   if TAB_rowids is NOT NULL and TAB_rowids.count > 0 then
      if L_system_options_rec.default_tax_type = 'GTAX' then
        FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            update item_supp_country isc
               set isc.unit_cost = I_unit_cost,
                   isc.last_update_datetime = sysdate,
                   isc.last_update_id = L_user
             where isc.rowid = TAB_rowids(i);
      else
        FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            update item_supp_country isc
               set isc.unit_cost = I_unit_cost,
                   isc.last_update_datetime = sysdate,
                   isc.last_update_id = L_user,
                   isc.negotiated_item_cost = NULL,
                   isc.extended_base_cost = NULL,
                   isc.inclusive_cost = NULL,
                   isc.base_cost = NULL
             where isc.rowid = TAB_rowids(i);
      end if;
      ---
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY',
                                            NULL,
                                            NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BULK_ISC;
------------------------------------------------------------------------------------
FUNCTION BULK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_unit_cost       IN       ITEM_SUPP_COUNTRY.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_ISCL';

   L_user            VARCHAR2(30) := get_user;
   L_rowid_tbl           RMSSUB_XCOSTCHG.ROWID_TBL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   cursor C_ISCL is
      select iscl.rowid
        from item_supp_country_loc iscl,
             api_iscl a
       where iscl.rowid = a.iscl_rowid
         and a.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no
         for update of iscl.item nowait;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_ISCL','item',null);
   open  C_ISCL;
   ---
   SQL_LIB.SET_MARK('FETCH','C_ISCL','item',null);
   fetch  C_ISCL BULK COLLECT into L_rowid_tbl;
   ---
   SQL_LIB.SET_MARK('CLOSE','C_ISCL','item',null);
   close C_ISCL;
   if L_rowid_tbl is NOT NULL and L_rowid_tbl.count > 0 then
      if L_system_options_rec.default_tax_type = 'GTAX' then
         FORALL i in L_rowid_tbl.FIRST..L_rowid_tbl.LAST
            update item_supp_country_loc
               set unit_cost = I_unit_cost,
                   last_update_datetime = sysdate,
                   last_update_id = L_user
             where rowid  =  L_rowid_tbl(i);
      else
         FORALL i in L_rowid_tbl.FIRST..L_rowid_tbl.LAST
            update item_supp_country_loc
                set unit_cost = I_unit_cost,
                    last_update_datetime = sysdate,
                    last_update_id = L_user,
                    negotiated_item_cost = NULL,
                    extended_base_cost = NULL,
                    inclusive_cost = NULL,
                    base_cost = NULL
              where rowid  =  L_rowid_tbl(i);
      end if;
      ---
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY',
                                             NULL,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BULK_ISCL;
------------------------------------------------------------------------------------
FUNCTION BULK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   I_items           IN       ITEM_TBL,
                   I_unit_cost       IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE,
                   I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                   I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_ISCL';

   L_user             VARCHAR2(30):= get_user;
   TAB_rowids        RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   cursor C_LOCK_ISCL is
      select iscl.rowid
        from item_supp_country_loc iscl,
             TABLE(cast(I_items as ITEM_TBL)) xitem
       where supplier = I_supplier
         and origin_country_id = I_country
         and item = value(xitem)
         for update of iscl.item nowait;

   cursor C_LOCK_ISCL_SINGLE is
      select rowid
        from item_supp_country_loc iscl
       where supplier = I_supplier
         and origin_country_id = I_country
         and item = I_items(1)
         for update nowait;
BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_items.COUNT > 1 then
      SQL_LIB.SET_MARK('OPEN','C_LOCK_ISCL','item',null);
      open  C_LOCK_ISCL;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_ISCL','item',null);
      fetch  C_LOCK_ISCL BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISCL','item',null);
      close C_LOCK_ISCL;

   elsif I_items.COUNT = 1 then
      TAB_rowids.extend;

      SQL_LIB.SET_MARK('OPEN','C_LOCK_ISCL_SINGLE','item',null);
      open  C_LOCK_ISCL_SINGLE;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_ISCL_SINGLE','item',null);
      fetch  C_LOCK_ISCL_SINGLE BULK COLLECT into TAB_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_ISCL_SINGLE','item',null);
      close C_LOCK_ISCL_SINGLE;
   end if;

   if TAB_rowids is NOT NULL AND TAB_rowids.COUNT > 0 then
      if L_system_options_rec.default_tax_type = 'GTAX' then
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            update item_supp_country_loc
               set unit_cost = I_unit_cost,
                   last_update_datetime = sysdate,
                   last_update_id = L_user
             where rowid = TAB_rowids(i);
      else
         FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
            update item_supp_country_loc
               set unit_cost = I_unit_cost,
                   last_update_datetime = sysdate,
                   last_update_id = L_user,
                   negotiated_item_cost = NULL,
                   extended_base_cost = NULL,
                   inclusive_cost = NULL,
                   base_cost = NULL
             where rowid = TAB_rowids(i);
      end if;

      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY_LOC',
                                             NULL,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_ISCL;
-------------------------------------------------------------------------------------
FUNCTION BULK_ITEMLOC_SOH(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          I_items           IN       ITEM_TBL,
                          I_item_locs       IN       RMSSUB_XCOSTCHG.PRICE_HIST_RECTYPE)

   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_ITEMLOC_SOH';

   L_user             VARCHAR2(30) := get_user;
   TAB_rowids        RMSSUB_XCOSTCHG.ROWID_TBL;
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_ITMLOC_SOH is
      select ils.rowid
        from item_loc_soh ils,
             TABLE(cast(I_items as ITEM_TBL)) xitem,
             TABLE(cast(I_item_locs.locs as LOC_TBL)) xloc
       where ils.item = value(xitem)
         and ils.loc = value(xloc)
         for update of ils.unit_cost nowait;

BEGIN

   SQL_LIB.SET_MARK('OPEN',
                    'C_ITMLOC_SOH',
                    'item',
                    null);
   open  C_ITMLOC_SOH;

   SQL_LIB.SET_MARK('FETCH',
                    'C_ITMLOC_SOH',
                    'item',
                    null);
   fetch  C_ITMLOC_SOH BULK COLLECT into TAB_rowids;

   SQL_LIB.SET_MARK('CLOSE',
                    'C_ITMLOC_SOH',
                    'item',
                    null);
   close C_ITMLOC_SOH;

   if TAB_rowids is NOT NULL AND TAB_rowids.COUNT > 0 then
      FORALL i in I_item_locs.locs.FIRST..I_item_locs.locs.LAST
         update item_loc_soh ils
            set unit_cost = I_item_locs.unit_costs(i),
                last_update_datetime = sysdate,
                last_update_id = L_user
          where ils.item = I_item_locs.items(i)
            and ils.loc = I_item_locs.locs(i)
            and exists (select 'x'
                          from TABLE(cast(I_items as ITEM_TBL)) xitem
                         where ils.item = value(xitem));
  
      ---
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_LOC_SOH',
                                             NULL,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_ITEMLOC_SOH;
-------------------------------------------------------------------------------------
FUNCTION UPDATE_SINGLE_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                            I_supplier        IN       ITEM_SUPP_COUNTRY_LOC.SUPPLIER%TYPE,
                            I_country         IN       ITEM_SUPP_COUNTRY_LOC.ORIGIN_COUNTRY_ID%TYPE,
                            I_item            IN       ITEM_SUPP_COUNTRY_LOC.ITEM%TYPE,
                            I_parent_id       IN       VARCHAR2,
                            I_loc             IN       ITEM_SUPP_COUNTRY_LOC.LOC%TYPE,
                            I_unit_cost       IN       ITEM_SUPP_COUNTRY_LOC.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.UPDATE_SINGLE_ISCL';

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_SINGLE_ISCL is
      select 'x'
        from item_supp_country_loc
       where item              = I_item
         and supplier          = I_supplier
         and origin_country_id = I_country
         and loc               = I_loc
         for update nowait;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   SQL_LIB.SET_MARK('OPEN','C_SINGLE_ISCL','item',null);
   open  C_SINGLE_ISCL;

   SQL_LIB.SET_MARK('CLOSE','C_SINGLE_ISCL','item',null);
   close C_SINGLE_ISCL;

   if I_parent_id = 'Y' then
      if L_system_options_rec.default_tax_type = 'GTAX' then
         update item_supp_country_loc iscl
            set unit_cost         = I_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where supplier          = I_supplier
            and origin_country_id = I_country
            and loc               = I_loc
            and exists(select 'x'
                         from item_master im,
                              item_supp_country isc
                        where im.item = I_item
                          and im.item = isc.item
                          and im.item = iscl.item
                        union all
                       select 'x'
                         from item_master im,
                              item_supp_country isc
                        where im.item_parent = I_item
                          and im.item = isc.item
                          and im.item = iscl.item
                        union all
                       select 'x'
                         from item_master im,
                              item_supp_country isc
                        where im.item_grandparent = I_item
                          and im.item = isc.item
                          and im.item = iscl.item);
      else
         update item_supp_country_loc iscl
            set unit_cost         = I_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user,
                negotiated_item_cost =I_unit_cost,
                extended_base_cost = I_unit_cost,
                inclusive_cost = I_unit_cost,
                base_cost = I_unit_cost
          where supplier          = I_supplier
            and origin_country_id = I_country
            and loc               = I_loc
            and exists(select 'x'
                         from item_master im,
                              item_supp_country isc
                        where im.item = I_item
                          and im.item = isc.item
                          and im.item = iscl.item
                        union all
                       select 'x'
                         from item_master im,
                              item_supp_country isc
                        where im.item_parent = I_item
                          and im.item = isc.item
                          and im.item = iscl.item
                        union all
                       select 'x'
                         from item_master im,
                              item_supp_country isc
                        where im.item_grandparent = I_item
                          and im.item = isc.item
                          and im.item = iscl.item);
      end if;
   else
      if L_system_options_rec.default_tax_type = 'GTAX' then
         update item_supp_country_loc
            set unit_cost         = I_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user
          where item              = I_item
            and supplier          = I_supplier
            and origin_country_id = I_country
            and loc               = I_loc;
      else
         update item_supp_country_loc
            set unit_cost         = I_unit_cost,
                last_update_datetime = sysdate,
                last_update_id = get_user,
                negotiated_item_cost =I_unit_cost,
                extended_base_cost = I_unit_cost,
                inclusive_cost = I_unit_cost,
                base_cost = I_unit_cost
          where item              = I_item
            and supplier          = I_supplier
            and origin_country_id = I_country
            and loc               = I_loc;
      end if;
   end if;

   ---
   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY',
                                             NULL,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END UPDATE_SINGLE_ISCL;
-------------------------------------------------------------------------------------
FUNCTION BULK_PACK_ISC(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                       I_isc_rows        IN       RMSSUB_XCOSTCHG.ROWID_TBL,
                       I_packs           IN       ITEM_TBL,
                       I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE,
                       I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_PACK_ISC';

   L_user             VARCHAR2(30) := get_user;
   L_calc_ind        VARCHAR2(1)  := 'N';
   L_rowid           ROWID;
   TAB_packs         ITEM_TBL := ITEM_TBL();
   TAB_ICH_packs     ITEM_TBL := ITEM_TBL();
   TAB_ICD_packs     ITEM_TBL := ITEM_TBL();
   TAB_rowids        RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_iscl_rowids   RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_ICH_rowids    RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();
   TAB_ICD_rowids    RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PACK_ISC_API is
      select isc.rowid,
             isc.item,
             a.iscl_rowid
        from item_supp_country isc,
             api_iscl a
       where isc.supplier = I_supplier
         and isc.origin_country_id = I_country
         and isc.item = a.item
         and a.primary_loc_ind = 'Y'
         and a.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no
         for update of isc.item nowait;

   cursor C_LOCK_PACK_ISC is
      select isc.rowid,
             isc.item
        from item_supp_country isc,
             TABLE(CAST(I_packs AS ITEM_TBL)) xpack
       where isc.item = value(xpack)
         and supplier = I_supplier
         and origin_country_id = I_country
         for update of isc.item nowait;
         
   cursor C_LOCK_PACK_ISC_ID is
      select 'x'
        from item_supp_country isc
       where isc.rowid = L_rowid
         for update nowait;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_isc_rows is NOT NULL and I_isc_rows.count > 0 then
      FOR i in I_isc_rows.first..I_isc_rows.last LOOP
         L_rowid := I_isc_rows(i);
         SQL_LIB.SET_MARK('OPEN','C_LOCK_PACK_ISC_ID','item',null);
         open C_LOCK_PACK_ISC_ID;
         SQL_LIB.SET_MARK('CLOSE','C_LOCK_PACK_ISC_ID','item',null);
         close C_LOCK_PACK_ISC_ID;
      end LOOP;
      ---
      TAB_rowids := I_isc_rows;

   elsif I_packs is NOT NULL and I_packs.count > 0 then                        -- No Locs
      SQL_LIB.SET_MARK('OPEN','C_LOCK_PACK_ISC','item',null);
      open  C_LOCK_PACK_ISC;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_PACK_ISC','item',null);
      fetch  C_LOCK_PACK_ISC BULK COLLECT into TAB_rowids,
                                               TAB_packs;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_PACK_ISC','item',null);
      close C_LOCK_PACK_ISC;

      L_calc_ind := 'Y';
   else                                                                     -- Multi/Single Loc
      SQL_LIB.SET_MARK('OPEN','C_LOCK_PACK_ISC_API','item',null);
      open  C_LOCK_PACK_ISC_API;

      SQL_LIB.SET_MARK('FETCH','C_LOCK_PACK_ISC_API','item',null);
      fetch  C_LOCK_PACK_ISC_API BULK COLLECT into TAB_rowids,
                                                   TAB_packs,
                                                   TAB_iscl_rowids;

      SQL_LIB.SET_MARK('CLOSE','C_LOCK_PACK_ISC_API','item',null);
      close C_LOCK_PACK_ISC_API;
     
   end if;

   if TAB_rowids is NOT NULL and TAB_rowids.count > 0 then
      if L_calc_ind = 'Y' then
         if L_system_options_rec.default_tax_type = 'GTAX' then
            FORALL i in TAB_packs.FIRST..TAB_packs.LAST
               update item_supp_country isc1
                  set isc1.unit_cost = (select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                          from item_supp_country isc2,
                                               v_packsku_qty vpq
                                         where vpq.pack_no            = TAB_packs(i)
                                           and isc2.item              = vpq.item
                                           and isc2.supplier          = isc1.supplier
                                           and isc2.origin_country_id = isc1.origin_country_id),
                   isc1.last_update_id       = L_user,
                   isc1.last_update_datetime = sysdate
                where isc1.rowid = TAB_rowids(i);
         else
            FORALL i in TAB_packs.FIRST..TAB_packs.LAST
               update item_supp_country isc1
                  set isc1.unit_cost = (select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                          from item_supp_country isc2,
                                               v_packsku_qty vpq
                                         where vpq.pack_no            = TAB_packs(i)
                                           and isc2.item              = vpq.item
                                           and isc2.supplier          = isc1.supplier
                                           and isc2.origin_country_id = isc1.origin_country_id),
                   isc1.last_update_id       = L_user,
                   isc1.last_update_datetime = sysdate
                where isc1.rowid = TAB_rowids(i);
                
            FORALL i in TAB_packs.FIRST..TAB_packs.LAST
               update item_supp_country isc1
                  set isc1.negotiated_item_cost =isc1.unit_cost,
                      isc1.extended_base_cost = isc1.unit_cost,
                      isc1.inclusive_cost = isc1.unit_cost,
                      isc1.base_cost = isc1.unit_cost,                   
                      isc1.last_update_id       = L_user,
                      isc1.last_update_datetime = sysdate
                where isc1.rowid = TAB_rowids(i);
         end if;
      else
         if L_system_options_rec.default_tax_type = 'GTAX' then
            FORALL i in TAB_packs.FIRST..TAB_packs.LAST
               update item_supp_country isc1
                  set isc1.unit_cost = (select unit_cost
                                          from item_supp_country_loc
                                         where rowid = TAB_iscl_rowids(i)),
                   isc1.last_update_id       = L_user,
                   isc1.last_update_datetime = sysdate
               where isc1.rowid = TAB_rowids(i);
         else
            FORALL i in TAB_packs.FIRST..TAB_packs.LAST
               update item_supp_country isc1
                  set isc1.unit_cost = (select unit_cost
                                          from item_supp_country_loc
                                         where rowid = TAB_iscl_rowids(i)),
                      isc1.last_update_id       = L_user,
                      isc1.last_update_datetime = sysdate
                where isc1.rowid = TAB_rowids(i);
                
            FORALL i in TAB_packs.FIRST..TAB_packs.LAST
               update item_supp_country isc1
                  set isc1.negotiated_item_cost =isc1.unit_cost,
                      isc1.extended_base_cost = isc1.unit_cost,
                      isc1.inclusive_cost = isc1.unit_cost,
                      isc1.base_cost = isc1.unit_cost,                   
                      isc1.last_update_id       = L_user,
                      isc1.last_update_datetime = sysdate
                where isc1.rowid = TAB_rowids(i);
         end if;
      end if;
      ---
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC', NULL, NULL, NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            'ITEM_SUPP_COUNTRY',
                                             NULL,
                                             NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return FALSE;
END BULK_PACK_ISC;
------------------------------------------------------------------------------------
FUNCTION BULK_PACK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_packs           IN       ITEM_TBL,
                        I_country         IN       ITEM_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
                        I_supplier        IN       ITEM_SUPP_COUNTRY.SUPPLIER%TYPE)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_PACK_ISCL';

   L_user             VARCHAR2(30) := get_user;
   L_calc_ind        VARCHAR2(1)  := 'N';
   L_rowid           ROWID;

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   TAB_packs         ITEM_TBL := ITEM_TBL();
   TAB_rowids        RMSSUB_XCOSTCHG.ROWID_TBL := RMSSUB_XCOSTCHG.ROWID_TBL();

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PACK_ISCL_API is
      select iscl.rowid,
             iscl.item
        from item_supp_country_loc iscl,
             api_iscl a
       where iscl.rowid = a.iscl_rowid
         and a.seq_no = RMSSUB_XCOSTCHG.LP_api_seq_no
         for update of iscl.item nowait;

   cursor C_LOCK_PACK_ISCL is
      select iscl.rowid,
             iscl.item
        from item_supp_country_loc iscl,
             TABLE(CAST(I_packs AS ITEM_TBL)) xpack
       where iscl.origin_country_id = I_country
         and iscl.supplier = I_supplier
         and iscl.item = value(xpack)
         for update of iscl.item nowait;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   if I_packs is NULL or I_packs.count = 0 then                       -- Multi/Single locs
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_PACK_ISCL_API', 'item', NULL);
      open  C_LOCK_PACK_ISCL_API;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_LOCK_PACK_ISCL_API', 'item', NULL);
      fetch  C_LOCK_PACK_ISCL_API BULK COLLECT into TAB_rowids,
                                                    TAB_packs;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_PACK_ISCL_API', 'item', NULL);
      close C_LOCK_PACK_ISCL_API;

      -- Process unit cost calculations for ISCL
      L_calc_ind := 'Y';
   else                                                              -- No locs
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_PACK_ISCL', 'item', NULL);
      open  C_LOCK_PACK_ISCL;
      ---
      SQL_LIB.SET_MARK('FETCH', 'C_LOCK_PACK_ISCL', 'item', NULL);
      fetch  C_LOCK_PACK_ISCL BULK COLLECT into TAB_rowids,
                                                TAB_packs;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_PACK_ISCL', 'item', NULL);
      close C_LOCK_PACK_ISCL;

   end if;


   if TAB_rowids is NOT NULL AND TAB_rowids.count > 0 then
      if L_calc_ind = 'Y' then
         if L_system_options_rec.default_tax_type = 'GTAX' then
            FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
               update item_supp_country_loc isc1
                  set isc1.unit_cost = ( select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                           from item_supp_country_loc isc2,
                                                v_packsku_qty vpq
                                          where vpq.pack_no            = TAB_packs(i)
                                            and isc2.item              = vpq.item
                                            and isc2.supplier          = isc1.supplier
                                            and isc2.origin_country_id = isc1.origin_country_id
                                            and isc2.loc               = isc1.loc ),
                      isc1.last_update_id       = L_user,
                      isc1.last_update_datetime = sysdate
                where isc1.rowid = TAB_rowids(i);
             else
                FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
                   update item_supp_country_loc isc1
                      set isc1.unit_cost = ( select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                                   from item_supp_country_loc isc2,v_packsku_qty vpq
                                                  where vpq.pack_no            = TAB_packs(i)
                                                    and isc2.item              = vpq.item
                                                    and isc2.supplier          = isc1.supplier
                                                    and isc2.origin_country_id = isc1.origin_country_id
                                                    and isc2.loc               = isc1.loc ),
                          isc1.last_update_id       = L_user,
                          isc1.last_update_datetime = sysdate,
                          isc1.negotiated_item_cost = NULL,
                          isc1.extended_base_cost = NULL,
                          isc1.inclusive_cost = NULL,
                          isc1.base_cost = NULL
                     where isc1.rowid = TAB_rowids(i);
          end if;
      else
         if L_system_options_rec.default_tax_type = 'GTAX' then
            FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST
               update item_supp_country_loc isc1
                  set isc1.unit_cost = (select isc.unit_cost
                                          from item_supp_country isc
                                         where isc.origin_country_id = I_country
                                           and isc.item = TAB_packs(i)
                                           and isc.supplier = I_supplier),
                      isc1.last_update_id       = L_user,
                      isc1.last_update_datetime = sysdate,
                      isc1.negotiated_item_cost =isc1.unit_cost,
                      isc1.extended_base_cost = isc1.unit_cost,
                      isc1.inclusive_cost = isc1.unit_cost,
                      isc1.base_cost = isc1.unit_cost
                where isc1.rowid = TAB_rowids(i);
             else
                FORALL i in TAB_rowids.FIRST..TAB_rowids.LAST  
                  update item_supp_country_loc isc1
                     set isc1.unit_cost = (select isc.unit_cost
                                             from item_supp_country isc
                                            where isc.origin_country_id = I_country
                                              and isc.item = TAB_packs(i)
                                              and isc.supplier = I_supplier),
                          isc1.last_update_id       = L_user,
                          isc1.last_update_datetime = sysdate,
                          isc1.negotiated_item_cost = NULL,
                          isc1.extended_base_cost = NULL,
                          isc1.inclusive_cost = NULL,
                          isc1.base_cost = NULL
                    where isc1.rowid = TAB_rowids(i);
          end if;
      end if;
      ---    
      if SQL%NOTFOUND then
         O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                                NULL,
                                                NULL,
                                                NULL);
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_SUPP_COUNTRY_LOC',
                                                               NULL,
                                                               NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_PACK_ISCL;
------------------------------------------------------------------------------------
FUNCTION BULK_PACK_ISCL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        I_pack_rows       IN       RMSSUB_XCOSTCHG.ROWID_TBL,
                        I_packs           IN       ITEM_TBL)
   RETURN BOOLEAN IS

   L_program         VARCHAR2(64) := 'UPDATE_BASE_COST.BULK_PACK_ISCL';

   L_user            VARCHAR2(30) := get_user;
   L_rowid           ROWID;

   L_system_options_rec     SYSTEM_OPTIONS%ROWTYPE;

   TAB_packs         ITEM_TBL;
   TAB_rowids        RMSSUB_XCOSTCHG.ROWID_TBL;

   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_PACK_ISCL is
      select 'x'
        from item_supp_country_loc
       where rowid = L_rowid
         for update nowait;

BEGIN

   if not SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                               L_system_options_rec) then

      return FALSE;
   end if;

   FOR i in 1..I_pack_rows.COUNT LOOP --I_pack_rows.FIRST..I_pack_rows.LAST LOOP
      L_rowid := I_pack_rows(i);
      ---
      SQL_LIB.SET_MARK('OPEN', 'C_LOCK_PACK_ISCL', 'item', NULL);
      open  C_LOCK_PACK_ISCL;
      ---
      SQL_LIB.SET_MARK('CLOSE','C_LOCK_PACK_ISCL', 'item', NULL);
      close C_LOCK_PACK_ISCL;
   end LOOP;
   if L_system_options_rec.default_tax_type = 'GTAX' then
      FORALL i in I_pack_rows.FIRST..I_pack_rows.LAST
      
         update item_supp_country_loc isc1
            set isc1.unit_cost = ( select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                     from item_supp_country_loc isc2,
                                          v_packsku_qty vpq
                                    where vpq.pack_no            = I_packs(i)
                                      and isc2.item              = vpq.item
                                      and isc2.supplier          = isc1.supplier
                                      and isc2.origin_country_id = isc1.origin_country_id
                                      and isc2.loc               = isc1.loc ),
                isc1.last_update_id       = L_user,
                isc1.last_update_datetime = sysdate
          where isc1.rowid = I_pack_rows(i);
       else
          FORALL i in I_pack_rows.FIRST..I_pack_rows.LAST
            update item_supp_country_loc isc1
              set isc1.unit_cost = ( select NVL(sum(isc2.unit_cost * vpq.qty), isc1.unit_cost)
                                       from item_supp_country_loc isc2,v_packsku_qty vpq
                                      where vpq.pack_no            = I_packs(i)
                                        and isc2.item              = vpq.item
                                        and isc2.supplier          = isc1.supplier
                                        and isc2.origin_country_id = isc1.origin_country_id
                                        and isc2.loc               = isc1.loc ),
                    isc1.last_update_id       = L_user,
                    isc1.last_update_datetime = sysdate,
                    isc1.negotiated_item_cost = NULL,
                    isc1.extended_base_cost = NULL,
                    isc1.inclusive_cost = NULL,
                    isc1.base_cost = NULL
            where isc1.rowid = I_pack_rows(i);
    end if;

   if SQL%NOTFOUND then
      O_error_message := SQL_LIB.CREATE_MSG('COULD_NOT_UPDATE_REC',
                                             NULL,
                                             NULL,
                                             NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                                              'ITEM_SUPP_COUNTRY_LOC',
                                                               NULL,
                                                               NULL);
      RETURN FALSE;
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END BULK_PACK_ISCL;
------------------------------------------------------------------------------------

END UPDATE_BASE_COST;
/
