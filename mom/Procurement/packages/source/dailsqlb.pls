CREATE OR REPLACE PACKAGE BODY DEAL_ACTUALS_ITEM_LOC_SQL AS
--------------------------------------------------------
FUNCTION GET_NEXT_DAI_ID(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_dai_id        IN OUT DEAL_ACTUALS_ITEM_LOC.DAI_ID%TYPE)
   RETURN BOOLEAN is

   L_first_time               VARCHAR2(1) := 'Y';
   L_wrap_seq_no              DEAL_ACTUALS_ITEM_LOC.DAI_ID%TYPE;
   L_exists                   VARCHAR2(1) :=  'N' ;

   cursor C_DAI_EXISTS is
      select 'Y'
        from deal_actuals_item_loc
       where dai_id  = O_dai_id
         and rownum = 1;

   cursor C_SELECT_NEXTVAL is
      select DEAL_ACTUALS_ITEMLOC_SEQ.NEXTVAL
        from dual;
BEGIN

   SQL_LIB.SET_MARK('OPEN','C_SELECT_NEXTVAL','DUAL',NULL);
   open C_SELECT_NEXTVAL;
   ---
   LOOP
      SQL_LIB.SET_MARK('FETCH','C_SELECT_NEXTVAL','DUAL',NULL);
      fetch C_SELECT_NEXTVAL into O_dai_id;
      ---
      if L_first_time = 'Y' then
         L_wrap_seq_no   := O_dai_id;
         L_first_time    := 'N';
      elsif O_dai_id = L_wrap_seq_no then
         O_error_message := SQL_LIB.CREATE_MSG('NO_DAI_NUM', NULL, NULL, NULL);
         SQL_LIB.SET_MARK('CLOSE','C_SELECT_NEXTVAL','DUAL',NULL);
         close C_SELECT_NEXTVAL;
         return FALSE;
      end if;
      ---
      SQL_LIB.SET_MARK('OPEN','C_DAI_EXISTS','DEAL_ACTUALS_ITEM_LOC', 'dai_id: '||to_char(O_dai_id));
      open C_DAI_EXISTS;
      SQL_LIB.SET_MARK('FETCH','C_DAI_EXISTS','DEAL_ACTUALS_ITEM_LOC', 'dai_id:'||to_char(O_dai_id));
      fetch C_DAI_EXISTS into L_exists;
      SQL_LIB.SET_MARK('CLOSE','C_DAI_EXISTS','DEAL_ACTUALS_ITEM_LOC', 'dai_id:'||to_char(O_dai_id));
      close C_DAI_EXISTS;
      if L_exists = 'N'  then
         SQL_LIB.SET_MARK('CLOSE','C_SELECT_NEXTVAL','DUAL',NULL);
         close C_SELECT_NEXTVAL;
         return TRUE;
      end if;
      ---
   END LOOP;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'DEAL_ACTUALS_ITEM_LOC_SQL.GET_NEXT_DAI_ID',
                                            to_char(SQLCODE));
   return FALSE;
END GET_NEXT_DAI_ID;
--------------------------------------------------------
FUNCTION INSERT_DEAL_BB_RCPT_SALES_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_next_eom_date IN DATE)
RETURN BOOLEAN IS

   L_vdate           DATE             := get_vdate;
   L_next_eom_date   DATE             := I_next_eom_date;
   L_program         VARCHAR2(100)    := 'DEAL_ACTUALS_ITEM_LOC_SQL.INSERT_DEAL_BB_RCPT_SALES_TEMP';

BEGIN

   INSERT /*+ append parallel(tt,4) */ INTO deal_bb_receipt_sales_temp tt
            (deal_id,
             deal_detail_id,
             item,
             loc_type,
             location,
             rebate_purch_sales_ind,
             partner_type,
             partner_id,
             supplier,
             reporting_date_vdate,
             reporting_date_neom,
             month_closed_ind,
             active_date,
             close_date,
             track_pack_level_ind,
             origin_country_id)
      SELECT deal_id,
             deal_detail_id,
             item,
             loc_type,
             location,
             rebate_purch_sales_ind,
             partner_type,
             partner_id,
             supplier,
             reporting_date_vdate,
             reporting_date_neom,
             month_closed_ind,
             active_date,
             close_date,
             track_pack_level_ind,
             origin_country_id
        FROM ( SELECT distinct /*+ parallel(dh,4) */
                     dile.deal_id              as deal_id,
                     dile.deal_detail_id       as deal_detail_id,
                     dile.item                 as item,
                     dile.loc_type             as loc_type,
                     dile.LOCATION             as location,
                     dh.rebate_purch_sales_ind as rebate_purch_sales_ind,
                     dh.partner_type           as partner_type,
                     dh.partner_id             as partner_id,
                     NVL(dh.supplier,NVL(sup.supplier_parent,sup.supplier)) as supplier,
                     vrd1.reporting_date       as reporting_date_vdate,
                     vrd2.reporting_date       as reporting_date_neom,
                     /* if vdate is greater than next_eom_date then month not closed */
                     CASE WHEN L_vdate
                       > L_next_eom_date THEN 'N' ELSE 'Y' END as month_closed_ind,
                     dile.active_date          as active_date,
                     dile.close_date           as close_date,
                     dh.track_pack_level_ind   as track_pack_level_ind,
                     DECODE(dh.rebate_purch_sales_ind,'S',null,dile.origin_country_id)    as origin_country_id,
                     rank() over ( partition by dile.item, dh.supplier, dile.LOCATION
                                   order by
                                   decode(nvl(dd.deal_class,'ZZ'), 'EX', 1, 2)) as exclusive_rank
                FROM deal_head dh,
                     deal_item_loc_explode dile,
                     sups sup,
                     deal_detail dd,
                     ( /* Find MINIMUM reporting_date that is greate than or equal to VDATE */
                       SELECT /*+ parallel(daf1,4) */ daf1.deal_id,
                              daf1.deal_detail_id,
                              MIN(daf1.reporting_date) reporting_date
                         FROM deal_actuals_forecast daf1
                        WHERE daf1.actual_forecast_ind = 'F'
                          AND daf1.reporting_date >= L_vdate
                     GROUP BY daf1.deal_id, daf1.deal_detail_id) vrd1,
                    ( /* Find MAXIMUM reporting_date that is less than or equal to Next EOM */
                       SELECT /*+ parallel(daf2,4) */ daf2.deal_id,
                              daf2.deal_detail_id,
                              MAX(daf2.reporting_date) reporting_date
                         FROM deal_actuals_forecast daf2
                        WHERE daf2.actual_forecast_ind = 'F'
                          AND daf2.reporting_date <= L_next_eom_date
						  AND EXISTS( SELECT '1'   
                                      FROM deal_actuals_forecast daf3 
                                     WHERE daf3.reporting_date = L_next_eom_date 
                                        AND daf3.deal_detail_id = daf2.deal_detail_id 
                                        AND daf3.deal_id = daf2.deal_id) 

                     GROUP BY daf2.deal_id, daf2.deal_detail_id) vrd2
               WHERE dh.billing_type in ('BB','BBR')
                 AND dh.status = 'A'
                 AND (   (dh.rebate_purch_sales_ind = 'S' AND dh.deal_appl_timing IS NULL)
                      OR (dh.rebate_purch_sales_ind = 'P' AND dh.deal_appl_timing = 'R')
                     )
                 AND dh.deal_id = dile.deal_id
                 AND dile.deal_id = vrd1.deal_id(+)
                 AND dile.deal_detail_id = vrd1.deal_detail_id(+)
                 AND dile.deal_id = vrd2.deal_id(+)
                 AND dile.deal_detail_id = vrd2.deal_detail_id(+)
                 AND dile.deal_id = dd.deal_id
                 AND dile.deal_detail_id = dd.deal_detail_id
                 AND sup.supplier = dile.supplier)
       WHERE exclusive_rank = 1;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END INSERT_DEAL_BB_RCPT_SALES_TEMP;
--------------------------------------------------------
FUNCTION INSERT_DEAL_BB_NO_REBATE_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_vdate           DATE              := get_vdate;
   L_program         VARCHAR2(100)     := 'DEAL_ACTUALS_ITEM_LOC_SQL.INSERT_DEAL_BB_NO_REBATE_TEMP';

BEGIN

    
    INSERT /*+ append parallel(t,4) */ INTO DEAL_BB_NO_REBATE_TEMP t
          (order_no,
           deal_id,
           deal_detail_id,
           dai_id,
           item,
           loc_type,
           location,
           order_currency_code,
           loc_currency_code,
           total_units,
           total_revenue,
           reporting_date)
    SELECT vt.order_no,
           vt.deal_id,
           vt.deal_detail_id,
           dail.dai_id,
           vt.item,
           vt.loc_type,
           vt.location,
           vt.order_currency_code,
           vt.loc_currency_code,
           SUM(vt.qty_ordered) total_units,
           SUM(vt.qty_ordered * vt.unit_cost) total_revenue,
           vt.reporting_date
      FROM (SELECT di.deal_id,
                   di.deal_detail_id,
                   di.item,
                   di.loc_type,
                   di.LOCATION,
                   oi.order_no,
                   oi.currency_code order_currency_code,
                   oi.qty_ordered,
                   oi.unit_cost,
                   vloc.currency_code loc_currency_code,
                   vdaf.reporting_date
             FROM (SELECT partner_type,
                          supplier,
                          partner_id,
                          active_date,
                          close_date,
                          deal_id,
                          deal_detail_id,
                          item,
                          loc_type,
                          location,
			              origin_country_id
                     FROM (SELECT distinct /*+ no_merge parallel(dh,4) */
                                  dh.partner_type,
                                  dile.supplier,
                                  dh.partner_id,
                                  dile.active_date,
                                  dile.close_date,
                                  dile.deal_id,
                                  dile.deal_detail_id,
                                  dile.item,
                                  dile.loc_type,
                                  dile.location,
				                  dile.origin_country_id,
                                  rank() over(partition by dile.item, dh.supplier, dile.location
                                              order by
                                              decode(nvl(dd.deal_class,'ZZ'), 'EX', 1, 2)) as exclusive_rank
                             FROM deal_head dh,
                                  deal_item_loc_explode dile,
                                  deal_detail dd
                            WHERE dh.deal_id = dile.deal_id
                              AND dh.billing_type = 'BB'
                              AND dh.status = 'A'
                              AND dh.rebate_ind = 'N'
                              AND dh.rebate_purch_sales_ind  = 'P'
                              AND dh.deal_appl_timing = 'O'
                              AND dile.deal_id = dd.deal_id
                              AND dile.deal_detail_id = dd.deal_detail_id)
                    WHERE exclusive_rank = 1) di,
                   (SELECT /*+ no_merge parallel(oh,4) */
                           oh.order_no,
                           oh.currency_code order_currency_code,
                           oh.orig_approval_date,
                           oh.supplier,
                           i.supp_hier_type_1,
                           i.supp_hier_lvl_1,
                           i.supp_hier_type_2,
                           i.supp_hier_lvl_2,
                           i.supp_hier_type_3,
                           i.supp_hier_lvl_3,
                           oh.currency_code,
                           ol.qty_ordered,
                           ol.unit_cost,
                           ol.item,
                           ol.loc_type,
                           ol.location,
			               os.origin_country_id
                      FROM ordloc ol,
                           ordhead oh,
                           item_supp_country i,
                           sups sup,
                           ordsku os
                     where ol.order_no = oh.order_no
                       and oh.order_no = os.order_no
                       AND ol.item = os.item
                       AND oh.orig_approval_date = L_vdate
                       AND sup.supplier = oh.supplier
                       AND oh.supplier = i.supplier
                       AND os.item = ol.item
                       AND os.order_no = oh.order_no
                       AND os.origin_country_id = i.origin_country_id
                       AND ol.item = i.item) oi,
                   (SELECT /*+ parallel(daf,4) */ deal_id,
                           deal_detail_id,
                           MIN(reporting_date) reporting_date
                      FROM deal_actuals_forecast daf
                     WHERE actual_forecast_ind = 'F'
                       AND reporting_date >= L_vdate
                  GROUP BY deal_id, deal_detail_id) vdaf,
                   (SELECT /*+ parallel(s,4) */ store loc,
                           currency_code,
                           'S' loc_type
                      FROM store s
                     WHERE stockholding_ind = 'Y'
                 union ALL
                    SELECT /*+ parallel(wh,4) */ wh loc,
                           currency_code,
                           'W' loc_type
                      FROM wh
                     WHERE stockholding_ind = 'Y'
                       AND finisher_ind = 'N') vloc
             WHERE di.item = oi.item
               AND di.loc_type = oi.loc_type
               AND di.location = oi.location
               -- Note : Data is extracted from approved orders on the day that
               -- they are first approved.  if the order is subsequently unapproved
               -- or modified then income will NOT be recalculated.
               -- See Design Assumptions.
               AND oi.orig_approval_date BETWEEN (di.active_date) AND
                   DECODE(di.close_date, NULL, oi.orig_approval_date + 1, di.close_date)
               AND (   (di.partner_type = 'S' AND di.supplier = oi.supplier)
                     OR (di.partner_type = oi.supp_hier_type_1 AND di.partner_id = oi.supp_hier_lvl_1)
                     OR (di.partner_type = oi.supp_hier_type_2 AND di.partner_id = oi.supp_hier_lvl_2)
                     OR (di.partner_type = oi.supp_hier_type_3 AND di.partner_id = oi.supp_hier_lvl_3)
                   )
               AND di.origin_country_id = oi.origin_country_id    
               AND di.location = vloc.loc
               AND di.loc_type = vloc.loc_type
               AND di.deal_id = vdaf.deal_id
               AND di.deal_detail_id = vdaf.deal_detail_id) vt,
               deal_actuals_item_loc dail
         WHERE dail.deal_id (+) = vt.deal_id
           AND dail.deal_detail_id (+) = vt.deal_detail_id
           AND dail.item (+) = vt.item
           AND dail.loc_type (+) = vt.loc_type
           AND dail.location (+) = vt.location
           AND dail.reporting_date (+) = vt.reporting_date
           AND dail.order_no (+) = vt.order_no
      GROUP BY vt.deal_id,
               vt.deal_detail_id,
               vt.reporting_date,
               vt.item,
               vt.loc_type,
               vt.location,
               vt.order_no,
               vt.order_currency_code,
               vt.loc_currency_code,
               dail.dai_id;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END INSERT_DEAL_BB_NO_REBATE_TEMP;
--------------------------------------------------------
FUNCTION DEAL_BB_REBATE_PO_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   L_vdate           DATE              := get_vdate;
   L_program         VARCHAR2(100)     := 'DEAL_ACTUALS_ITEM_LOC_SQL.DEAL_BB_REBATE_PO_TEMP';

BEGIN

    
    INSERT /*+ append parallel(t,4) */INTO deal_bb_rebate_po_temp
          (deal_id,
           deal_detail_id,
           dai_id,
           item,
           loc_type,
           location,
           order_currency_code,
           loc_currency_code,
           total_units,
           total_revenue,
           reporting_date,
           bill_to_loc,
           bill_to_loc_type)
    SELECT vt.deal_id,
           vt.deal_detail_id,
           dail.dai_id,
           vt.item,
           vt.loc_type,
           vt.location,
           vt.order_currency_code,
           vt.loc_currency_code,
           SUM(vt.qty_ordered) total_units,
           SUM(vt.qty_ordered * vt.unit_cost) total_revenue,
           vt.reporting_date,
           vt.import_id,
           vt.import_type
      FROM (SELECT di.deal_id,
                   di.deal_detail_id,
                   di.item,
                   di.loc_type,
                   di.LOCATION,
                   oi.order_no,
                   oi.currency_code order_currency_code,
                   oi.qty_ordered,
                   oi.unit_cost,
                   oi.import_id,
                   oi.import_type,
                   vloc.currency_code loc_currency_code,
                   vdaf.reporting_date
              FROM (SELECT partner_type,
                           supplier,
                           partner_id,
                           active_date,
                           close_date,
                           deal_id,
                           deal_detail_id,
                           item,
                           loc_type,
                           location,
			               origin_country_id
                      FROM (SELECT distinct /*+ no_merge parallel(dh,4) */
                                  dh.partner_type,
                                  dile.supplier,
                                  dh.partner_id,
                                  dile.active_date,
                                  dile.close_date,
                                  dile.deal_id,
                                  dile.deal_detail_id,
                                  dile.item,
                                  dile.loc_type,
                                  dile.location,
				                  dile.origin_country_id,
                                  rank() over(partition by dile.item, dh.supplier, dile.location
                                              order by
                                              decode(nvl(dd.deal_class,'ZZ'), 'EX', 1, 2)) as exclusive_rank
                             FROM deal_head dh,
                                  deal_item_loc_explode dile,
                                  deal_detail dd
                            WHERE dh.deal_id = dile.deal_id
                              AND dh.billing_type in ('BB','BBR')
                              AND dh.status = 'A'
                              AND dh.rebate_ind = 'Y'
                              AND dh.rebate_purch_sales_ind  = 'P'
                              AND dh.deal_appl_timing = 'O'
                              AND dile.deal_id = dd.deal_id
                              AND dile.deal_detail_id = dd.deal_detail_id)
                     WHERE exclusive_rank = 1) di,
                   (SELECT /*+ no_merge parallel(oh,4) */
                           oh.order_no,
                           oh.currency_code order_currency_code,
                           oh.orig_approval_date,
                           oh.supplier,
                           i.supp_hier_type_1,
                           i.supp_hier_lvl_1,
                           i.supp_hier_type_2,
                           i.supp_hier_lvl_2,
                           i.supp_hier_type_3,
                           i.supp_hier_lvl_3,
                           oh.currency_code,
                           NVL(oh.import_id,ol.location) import_id,
                           NVL(oh.import_type,ol.loc_type) import_type,
                           ol.qty_ordered,
                           ol.unit_cost,
                           ol.item,
                           ol.loc_type,
                           ol.location,
			               os.origin_country_id
                      FROM ordloc ol,
                           ordhead oh,
                           item_supp_country i,
                           sups sup,
                           ordsku os
		     WHERE ol.order_no = oh.order_no
		       AND oh.order_no = os.order_no
                       AND ol.item = os.item
                       AND oh.orig_approval_date = L_vdate
                       AND sup.supplier = oh.supplier
                       AND oh.supplier = i.supplier
                       AND os.item = ol.item
                       AND os.order_no = oh.order_no
                       AND os.origin_country_id = i.origin_country_id
                       AND ol.item = i.item) oi,
                   (SELECT /*+ parallel(daf,4) */ deal_id,
                           deal_detail_id,
                           MIN(reporting_date) reporting_date
                      FROM deal_actuals_forecast daf
                     WHERE actual_forecast_ind = 'F'
                       AND reporting_date >= L_vdate
                  GROUP BY deal_id, deal_detail_id) vdaf,
                   (SELECT /*+ parallel(s,4) */ store loc,
                           currency_code,
                           'S' loc_type
                      FROM store s
                     WHERE stockholding_ind = 'Y'
                 union ALL
                    SELECT /*+ parallel(wh,4) */ wh loc,
                           currency_code,
                           'W' loc_type
                      FROM wh
                     WHERE stockholding_ind = 'Y'
                       AND finisher_ind = 'N') vloc
             WHERE di.item = oi.item
               AND di.loc_type = oi.loc_type
               AND di.location = oi.location
               -- Note : Data is extracted from approved orders on the day that
               -- they are first approved.  if the order is subsequently unapproved
               -- or modified then income will NOT be recalculated.
               -- See Design Assumptions.
               AND oi.orig_approval_date BETWEEN (di.active_date) AND
                   DECODE(di.close_date, NULL, oi.orig_approval_date + 1, di.close_date)
               AND (   (di.partner_type = 'S' AND di.supplier = oi.supplier)
                     OR (di.partner_type = oi.supp_hier_type_1 AND di.partner_id = oi.supp_hier_lvl_1)
                     OR (di.partner_type = oi.supp_hier_type_2 AND di.partner_id = oi.supp_hier_lvl_2)
                     OR (di.partner_type = oi.supp_hier_type_3 AND di.partner_id = oi.supp_hier_lvl_3)
                   )
               AND di.origin_country_id = oi.origin_country_id
               AND di.location = vloc.loc
               AND di.loc_type = vloc.loc_type
               AND di.deal_id = vdaf.deal_id
               AND di.deal_detail_id = vdaf.deal_detail_id) vt,
               deal_actuals_item_loc dail
         WHERE dail.deal_id (+) = vt.deal_id
           AND dail.deal_detail_id (+) = vt.deal_detail_id
           AND dail.item (+) = vt.item
           AND dail.loc_type (+) = vt.loc_type
           AND dail.location (+) = vt.location
           AND dail.reporting_date (+) = vt.reporting_date
      GROUP BY vt.deal_id,
               vt.deal_detail_id,
               vt.reporting_date,
               vt.item,
               vt.loc_type,
               vt.location,
               dail.dai_id,
               vt.order_currency_code,
               vt.loc_currency_code,
               vt.import_id,
               vt.import_type;
   RETURN TRUE;
EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      RETURN FALSE;

END DEAL_BB_REBATE_PO_TEMP;
--------------------------------------------------------
END;
/
