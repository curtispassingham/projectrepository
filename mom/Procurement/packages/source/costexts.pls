



CREATE OR REPLACE PACKAGE COST_EXTRACT_SQL AUTHID CURRENT_USER AS
------------------------------------------------------------------------------------------
   TYPE TYP_UNIT_COST   is TABLE OF NUMBER(20,4) INDEX BY BINARY_INTEGER;
   TYPE TYP_ITEM        is TABLE OF VARCHAR2(25) INDEX BY BINARY_INTEGER; 
------------------------------------------------------------------------------------------

--- Function Name  :  UPDATE_COSTS
--- Purpose        :  
--- This function will drive all cost changes within RMS.
--- This function will first determine if the cost change comes from 
--- COST_SUSP_SUP_DETAIL or COST_SUSP_SUP_DETAIL_LOC.
--- IF THE COST CHANGE IS FROM COST_SUSP_SUP_DETAIL_LOC (locations present):
--- 1.  Check to see if the cost change is a reason of 1 (new bracket
---     structure) or reason 2 (new bracket).  If it is, call 
---     INSERT_BRACKET_LOC to insert brackets at the location level.
--- 2.  If it is any other reason:  
---     A.  (if brackets).  If it is...
---         - process brackets at the  locations on
---           ITEM_SUPP_COUNTRY_BRACKET_COSTS.  If the
---           item is above the transaction level, update the child
---           brackets on ITEM_SUPP_COUNTRY_BRACKET_COSTS.  
---         - If the bracket is the default bracket, call ITEM_BRACKET_COST_SQL
---           to update the location costs for the default bracket and the 
---           unit cost on ITEM_SUPP_COUNTRY with the cost at the primary  
---           location.  Next, if the item is at the transaction level then call
---           CHECK_FOR_PACK_LOC to rebuild any pack costs.  
---           Then update the pack costs on ITEM_SUPP_COUNTRY_LOC
---           and ITEM_SUPP_COUNTRY.  If the pack is on an approved order, update
---           the costs for the order.  If the item is above transaction level,
---           check to see if the item's children are in any packs.  Update costs
---           and orders the same as described above.
---     B.  if no brackets 
---         - update costs on ITEM_SUPP_COUNTRY_LOC for the item and any children
---           at the virtual locations.  
---         - Call update_base_cost to update the unit cost on ITEM_SUPP_COUNTRY
---           where the location is the primary virtual location.
---         - Next, if the item is at the transaction level then call
---           CHECK_FOR_PACK_LOC to rebuild any pack costs.  
---           Then update the pack costs on ITEM_SUPP_COUNTRY_LOC
---           and ITEM_SUPP_COUNTRY.  If the pack is on an approved order, update
---           the costs for the order.  If the item is above transaction level,
---           check to see if the item's children are in any packs.  Update costs
---           and orders the same as described above.
---     C.  Check to see if the item on the cost chnage has changed, if it has
---         check to see if the item is to have an order recalculated.  If it is
---         at the transaction level, update the order with the new cost of the item.
---         If the item is above the transaction level, check to see if its children
---         are on any approved orders.  If they are,update the order with the new cost 
---         of the children.
--- IF THE COST CHANGE IS FROM COST_SUSP_SUP_DETAIL (no locations present):
--- 1.  Check to see if the cost change is a reason of 1 (new bracket
---     structure) or reason 2 (new bracket).  If it is, call 
---     INSERT_BRACKET to insert brackets at the location level.
--- 2.  If it is any other reason: 
---     A.  (if brackets).  If it is...
---         - process brackets no locations locations on
---           ITEM_SUPP_COUNTRY_BRACKET_COSTS.  If the
---           item is above the transaction level, update the child
---           brackets on ITEM_SUPP_COUNTRY_BRACKET_COSTS.  
---         - If the bracket is the default bracket, call ITEM_BRACKET_COST_SQL
---           to update the costs for the default bracket and the 
---           unit cost on ITEM_SUPP_COUNTRY. Update all location costs with this cost.
---           Next, if the item is at the transaction level then call
---           CHECK_FOR_PACK to rebuild any pack costs.  
---           Then update the pack costs on ITEM_SUPP_COUNTRY_LOC
---           and ITEM_SUPP_COUNTRY.  If the pack is on an approved order, update
---           the costs for the order.  If the item is above transaction level,
---           check to see if the item's children are in any packs.  Update costs
---           and orders the same as described above.
---     B.  if no brackets 
---         - update costs on ITEM_SUPP_COUNTRY for the item and any children.  
---         - Call update_base_cost to update the unit cost on ITEM_SUPP_COUNTRY_LOC.
---         - Next, if the item is at the transaction level then call
---           CHECK_FOR_PACK to rebuild any pack costs.  
---           Then update the pack costs on ITEM_SUPP_COUNTRY_LOC
---           and ITEM_SUPP_COUNTRY.  If the pack is on an approved order, update
---           the costs for the order.  If the item is above transaction level,
---           check to see if the item's children are in any packs.  Update costs
---           and orders the same as described above.
---     C.  Check to see if the item on the cost chnage has changed, if it has
---         check to see if the item is to have an order recalculated.  If it is
---         at the transaction level, update the order with the new cost of the item.
---         If the item is above the transaction level, check to see if its children
---         are on any approved orders.  If they are,update the order with the new cost 
---         of the children.


-----------------------------------------------------------------------------------------------------
FUNCTION UPDATE_COSTS(O_error_message   IN OUT    VARCHAR2,
                      I_cost_change     IN        COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                      I_cost_reason     IN        COST_SUSP_SUP_HEAD.REASON%TYPE) 
return BOOLEAN;
-------------------------------------------------------------------------------------------------------
-- Function Name  :  UPDATE_APPROVED_ORDERS
-- Purpose        :  This function will find all approved buyer orders for a passed in item.
--                   Next it will insert or update into deal_calc_que for the item.
--                   Then, it will update the order's unit_cost and unit_cost_init with the
--                   new unit_cost from ITEM_SUPP_COUNTRY_LOC.  Then, it will update any
--                   expenses and assessments for the item.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_APPROVED_ORDERS(O_error_message   IN OUT    VARCHAR2,
                                I_item            IN        ITEM_MASTER.ITEM%TYPE,
                                I_unit_cost       IN        COST_SUSP_SUP_DETAIL.UNIT_COST%TYPE)
return BOOLEAN; 
-------------------------------------------------------------------------------------------------------
-- Function Name  :  UPDATE_CHILD_APPROVED_ORDERS
-- Purpose        :  This function will select all transaction level children
--                   for the passed in item and call update_approved_orders to
--                   update any approved orders with the new unit costs from
--                   ITEM_SUPP_COUNTRY_LOC.
-------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_CHILD_APPROVED_ORDERS(O_error_message   IN OUT    VARCHAR2,
                                      L_item_tbl        IN        TYP_ITEM,
                                      L_unit_cost_tbl   IN        TYP_UNIT_COST) 
return BOOLEAN;
--------------------------------------------------------------------------------------------------------
-- Function Name  :  INSERT_BRACKET_LOC
-- Purpose        :  This function will insert new brackets into ITEM_SUPP_COUNTRY_LOC
--                   _BRACKET_COST for brackets and location on COST_SUSP_SUP_DETAIL_LOC.
--                   If the item is above the transaction level, insert into this table
--                   for the children.  
--                   Update the unit_costs on ITEM_SUPP_COUNTRY_LOC with the default brackets
--                   unit cost.  If the location is the primary location, update the cost
--                   on ITEM_SUPP_COUNTRY.  
--                   If the item a transaction level item in a pack, update all pack costs.  
--                   If the item is above the transaction level, check to see if any of its children
--                   are in a pack. If they are, update all pack costs.
--                   If the recalc order indicator is yes for the item, and the item is a tran level item,
--                   update orders for the item.  If the item is above the transaction level,
--                   update the orders unit costs for any of the items children. 
--------------------------------------------------------------------------------------------------------
FUNCTION INSERT_BRACKET_LOC(O_error_message   IN OUT    VARCHAR2,
                            I_cost_change     IN        COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                            I_cost_reason     IN        COST_SUSP_SUP_HEAD.REASON%TYPE,
                            O_cost_found      IN OUT    VARCHAR2)
return BOOLEAN; 
--------------------------------------------------------------------------------------------------------
-- Function Name  :  INSERT_BRACKET
-- Purpose        :  This function will insert new brackets into ITEM_SUPP_COUNTRY_LOC
--                  _BRACKET_COST for brackets on COST_SUSP_SUP_DETAIL.
--                  If the item is above the transaction level, insert into this table
--                  for the children.  Insert an item level bracket first, then insert
--                  brackets for all locations on ITEM_SUPP_COUNTRY_LOC for the item.
--                  Update the unit_costs on ITEM_SUPP_COUNTRY_LOC with the default brackets
--                  unit cost.  If the location is the primary location, update the cost
--                  on ITEM_SUPP_COUNTRY.  
--                  If the item a transaction level item in a pack, update all pack costs.  
--                  If the item is above the transaction level, check to see if any of its children
--                  are in a pack. If they are, update all pack costs.
--                  If the recalc order indicator is yes for the item, and the item is a tran level item,
--                  update orders for the item.  If the item is above the transaction level,
--                  update the orders unit costs for any of the items children.   
---------------------------------------------------------------------------------------------------------
FUNCTION INSERT_BRACKET(O_error_message   IN OUT    VARCHAR2,
                        I_cost_change     IN        COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                        I_cost_reason     IN        COST_SUSP_SUP_HEAD.REASON%TYPE,
                        I_cost_found      IN        VARCHAR2)
return BOOLEAN; 
----------------------------------------------------------------------------------------------------------
-- Function Name  :  INSERT_COUNTRY_BRACKET
-- Purpose        :  This function will insert a country level bracket(no locations) into
--                   ITEM_SUPP_COUNTRY_BRACKET_COST.
---------------------------------------------------------------------------------------------------------- 
FUNCTION INSERT_COUNTRY_BRACKET(O_error_message   IN OUT    VARCHAR2,
                                I_item            IN        COST_SUSP_SUP_DETAIL_LOC.ITEM%TYPE,
                                I_supplier        IN        COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE,
                                I_country         IN        COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE,
                                I_dept            IN        COST_SUSP_SUP_DETAIL_LOC.DEPT%TYPE,
                                I_cost_change     IN        COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                                I_seq_no          IN        COST_SUSP_SUP_DETAIL_LOC.SUP_DEPT_SEQ_NO%TYPE)
return BOOLEAN; 
----------------------------------------------------------------------------------------------------------
-- Function Name  :  INSERT_WORKSHEET_BRACKET
-- Purpose        :  This function will check to see if there are any worksheet items
--                   For a supplier that undergoes a bracket change.  If there are,
--                   Brackets will be inserted with a null cost for those items.
-----------------------------------------------------------------------------------------------------------
FUNCTION INSERT_WORKSHEET_BRACKET(O_error_message   IN OUT    VARCHAR2,
                                  I_bracket_level   IN        SUPS.INV_MGMT_LVL%TYPE,
                                  I_seq_no          IN        COST_SUSP_SUP_DETAIL_LOC.SUP_DEPT_SEQ_NO%TYPE,
                                  I_supplier        IN        COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE,
                                  I_bracket_value1  IN        COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE1%TYPE,
                                  I_bracket_value2  IN        COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE2%TYPE,
                                  I_default_ind     IN        COST_SUSP_SUP_DETAIL_LOC.DEFAULT_BRACKET_IND%TYPE,
                                  I_dept            IN        COST_SUSP_SUP_DETAIL_LOC.DEPT%TYPE,
                                  I_location        IN        COST_SUSP_SUP_DETAIL_LOC.LOC%TYPE,
                                  I_country         IN        COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE) 
return BOOLEAN; 
-------------------------------------------------------------------------------------------------------------
-- Function  :  DELETE_BRACKET
-- Purpose   :  This function will delete all records on ITEM_SUPP_COUTNRY_BRACKET_COST
--              For a passed in COST CHANGE.
--------------------------------------------------------------------------------------------------------------
FUNCTION DELETE_BRACKET(O_error_message   IN OUT    VARCHAR2,
                        I_loc_ind         IN        VARCHAR2,
                        I_cost_change     IN        COST_SUSP_SUP_DETAIL_LOC.COST_CHANGE%TYPE,
                        I_dept            IN        DEPS.DEPT%TYPE,
                        I_supplier        IN        COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE) 
return BOOLEAN; 
-------------------------------------------------------------------------------------------------------------
-- Function  :  CHANGE_WORKSHEET_DEFAULT_BRACKET
-- Purpose   :  This function will change the default bracket for all items and update any costs
--              for a worksheet item
--------------------------------------------------------------------------------------------------------------
FUNCTION CHANGE_WORKSHEET_DEFAULT(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                  I_bracket_value1 IN     SUP_BRACKET_COST.BRACKET_VALUE1%TYPE,
                                  I_supplier       IN     SUP_BRACKET_COST.SUPPLIER%TYPE,
                                  I_seq_no         IN     SUP_BRACKET_COST.SUP_DEPT_SEQ_NO%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------
-- Function  :  UDPATE_BUYER_PACK
-- Purpose   :  This function will check to see if the item or a transaction level child
--              of the passed in item is on a buyer pack.  Then it will call PACKITEM_ADD_SQL
--              to update all costs for the buyer pack.
--------------------------------------------------------------------------------------------------------------
FUNCTION UPDATE_BUYER_PACK(O_error_message   IN OUT    VARCHAR2,
                           I_item            IN        ITEM_MASTER.ITEM%TYPE) 
return BOOLEAN; 
---------------------------------------------------------------------------------------------------------------
-- Function  :INSERT_WKSHT_CNTRY_BRACKET
-- Purpose   :  This function inserts country level brackets for worksheet items.
---------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_WKSHT_CNTRY_BRACKET(O_error_message   IN OUT    VARCHAR2,
                                    I_seq_no          IN        COST_SUSP_SUP_DETAIL_LOC.SUP_DEPT_SEQ_NO%TYPE,
                                    I_supplier        IN        COST_SUSP_SUP_DETAIL_LOC.SUPPLIER%TYPE,
                                    I_bracket_value1  IN        COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE1%TYPE,
                                    I_bracket_value2  IN        COST_SUSP_SUP_DETAIL_LOC.BRACKET_VALUE2%TYPE,
                                    I_default_ind     IN        COST_SUSP_SUP_DETAIL_LOC.DEFAULT_BRACKET_IND%TYPE,
                                    I_dept            IN        COST_SUSP_SUP_DETAIL_LOC.DEPT%TYPE,
                                    I_country         IN        COST_SUSP_SUP_DETAIL_LOC.ORIGIN_COUNTRY_ID%TYPE) 
return BOOLEAN;
---------------------------------------------------------------------------------------------------------------
---FUNCTION NAME: BULK_UPDATE_COSTS
---Purpose: Called by SCCEXT batch to process cost change detail and detail locs.
---------------------------------------------------------------------------------------------------------------
FUNCTION BULK_UPDATE_COSTS(O_error_message   IN OUT    RTK_ERRORS.RTK_TEXT%TYPE,
                           I_cost_change     IN        COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                           I_cost_reason     IN        COST_SUSP_SUP_HEAD.REASON%TYPE)
return BOOLEAN;
---------------------------------------------------------------------------------------------------------------
END;
/
