
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE ORDER_NUMBER_SQL AUTHID CURRENT_USER AS
-------------------------------------------------------------------------------
-- Name:    NEXT_ORDER_NUMBER
-- Purpose: This order number sequence generator will return to the calling
--          program/procedure.  Upon success (TRUE) an order number.  Upon
--          failure (FALSE) an appropriate error message for display purposes
--          by the calling program/procedure.
-- Created By: Shane G. Kville
-------------------------------------------------------------------------------
FUNCTION NEXT_ORDER_NUMBER (O_error_message IN OUT VARCHAR2,
                            O_order_number  IN OUT ORDHEAD.ORDER_NO%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       PREISSUE
-- Purpose:    This is an overload function similar with existing PREISSUE function.
--             A second output parameter is added which is needed by the calling
--             CORESVC package for the generated pre-issued order numbers.
-- Called by:  CORESVC_ORD_PREISSUE_SQL
-- Calls:      ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER
-------------------------------------------------------------------------------
FUNCTION PREISSUE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                   O_ordnum_rec      IN OUT   "RIB_OrdNumColDesc_REC",
                   I_qty             IN       NUMBER,
                   I_expiry_date     IN       ORD_PREISSUE.EXPIRY_DATE%TYPE,
                   I_supplier        IN       ORD_PREISSUE.SUPPLIER%TYPE,
                   I_create_id       IN       ORD_PREISSUE.CREATE_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       PREISSUE
-- Purpose:    This function will preissue new order numbers by calling the
--             NEXT_ORDER_NUMBER function I_qty times and insert those new
--             order numbers, along with I_expiry_date and I_create_date, into
--             the ORD_PREISSUE table.
-- Called by:  ORDPRE.FMB
-- Calls:      ORDER_NUMBER_SQL.NEXT_ORDER_NUMBER
-- Created By: Shane G. Kville
-------------------------------------------------------------------------------
FUNCTION PREISSUE (O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   I_qty           IN     NUMBER,
                   I_expiry_date   IN     ORD_PREISSUE.EXPIRY_DATE%TYPE,
                   I_supplier      IN     ORD_PREISSUE.SUPPLIER%TYPE,
                   I_create_id     IN     ORD_PREISSUE.CREATE_ID%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       GET_EXPIRY_DELAY_PRE_ISSUE
-- Purpose:    This function will retrieve expiry_delay_pre_issue from procurement_unit_options
-------------------------------------------------------------------------------
FUNCTION GET_EXPIRY_DELAY_PRE_ISSUE(O_error_message           IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_expiry_delay_pre_issue  IN OUT PROCUREMENT_UNIT_OPTIONS.EXPIRY_DELAY_PRE_ISSUE%TYPE)
   RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Name:       CHECK_ORDER_PREISSUE
-- Purpose:    This function checks the ORD_PREISSUE table to determine if the
--             vendor order number passed in has been pre-issued for the supplier.
--             It also verifies that the pre-issued order number has not expired.
--             (O_preissue equals 0 if I_order_no is NOT a pre-issued order number,
--                                1 if I_order_no IS a pre-issued order number,
--                               -1 if pre-issued order number is expired or exists)
-------------------------------------------------------------------------------
FUNCTION CHECK_ORDER_PREISSUE (O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                               O_preissue        IN OUT   VARCHAR2,
                               I_supplier        IN       ORD_PREISSUE.SUPPLIER%TYPE,
                               I_order_no        IN       ORD_PREISSUE.ORDER_NO%TYPE,
                               I_expiry_date     IN       ORD_PREISSUE.EXPIRY_DATE%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------
END;
/
