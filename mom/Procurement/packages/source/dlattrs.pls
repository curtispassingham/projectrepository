
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE PACKAGE DEAL_ATTRIB_SQL AUTHID CURRENT_USER AS

----------------------------------------------------------------------------------------
-- Function Name:  GET_HEADER_INFO
-- Purpose:        This function will get header information for a deal from the
--                 deal_head table.
----------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message                IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_partner_type                 IN OUT DEAL_HEAD.PARTNER_TYPE%TYPE,
                         O_partner_id                   IN OUT DEAL_HEAD.PARTNER_ID%TYPE,
                         O_supplier                     IN OUT DEAL_HEAD.SUPPLIER%TYPE,
                         O_type                         IN OUT DEAL_HEAD.TYPE%TYPE,
                         O_status                       IN OUT DEAL_HEAD.STATUS%TYPE,
                         O_currency_code                IN OUT DEAL_HEAD.CURRENCY_CODE%TYPE,
                         O_active_date                  IN OUT DEAL_HEAD.ACTIVE_DATE%TYPE,
                         O_close_date                   IN OUT DEAL_HEAD.CLOSE_DATE%TYPE,
                         O_create_id                    IN OUT DEAL_HEAD.CREATE_ID%TYPE,
                         O_ext_ref_no                   IN OUT DEAL_HEAD.EXT_REF_NO%TYPE,
                         O_order_no                     IN OUT DEAL_HEAD.ORDER_NO%TYPE,
                         O_billing_type                 IN OUT DEAL_HEAD.BILLING_TYPE%TYPE,
                         O_bill_back_period             IN OUT DEAL_HEAD.BILL_BACK_PERIOD%TYPE,
                         O_deal_appl_timing             IN OUT DEAL_HEAD.DEAL_APPL_TIMING%TYPE,
                         O_threshold_limit_type         IN OUT DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                         O_threshold_limit_uom          IN OUT DEAL_HEAD.THRESHOLD_LIMIT_UOM%TYPE,
                         O_rebate_ind                   IN OUT DEAL_HEAD.REBATE_IND%TYPE,
                         O_rebate_calc_type             IN OUT DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                         O_growth_rebate_ind            IN OUT DEAL_HEAD.GROWTH_REBATE_IND%TYPE,
                         O_hist_comp_start_date         IN OUT DEAL_HEAD.HISTORICAL_COMP_START_DATE%TYPE,
                         O_hist_comp_end_date           IN OUT DEAL_HEAD.HISTORICAL_COMP_END_DATE%TYPE,
                         O_rebate_purch_sales_ind       IN OUT DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE,
                         O_deal_reporting_level         IN OUT DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE,
                         O_bill_back_method             IN OUT DEAL_HEAD.BILL_BACK_METHOD%TYPE,
                         O_deal_income_calc             IN OUT DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                         O_invoice_proc_logic           IN OUT DEAL_HEAD.INVOICE_PROCESSING_LOGIC%TYPE,
                         O_stock_ledger_ind             IN OUT DEAL_HEAD.STOCK_LEDGER_IND%TYPE,
                         O_include_vat_ind              IN OUT DEAL_HEAD.INCLUDE_VAT_IND%TYPE,
                         O_billing_partner_type         IN OUT DEAL_HEAD.BILLING_PARTNER_TYPE%TYPE,
                         O_billing_partner_id           IN OUT DEAL_HEAD.BILLING_PARTNER_ID%TYPE,
                         O_billing_supplier_id          IN OUT DEAL_HEAD.BILLING_SUPPLIER_ID%TYPE,
                         O_growth_rate_to_date          IN OUT DEAL_HEAD.GROWTH_RATE_TO_DATE%TYPE,
                         O_turnover_to_date             IN OUT DEAL_HEAD.TURNOVER_TO_DATE%TYPE,
                         O_actual_monies_earned_to_date IN OUT DEAL_HEAD.ACTUAL_MONIES_EARNED_TO_DATE%TYPE,
                         O_security_ind                 IN OUT DEAL_HEAD.SECURITY_IND%TYPE,
                         O_est_next_invoice_date        IN OUT DEAL_HEAD.EST_NEXT_INVOICE_DATE%TYPE,
                         O_last_invoice_date            IN OUT DEAL_HEAD.LAST_INVOICE_DATE%TYPE,
                         I_deal_id                      IN     DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------
-- Function Name:  GET_HEADER_INFO
-- Purpose:        This overloaded function will get header information for a deal from the
--                 deal_head table. Overloading is done since RPM doesn't require 
--                 O_track_pack_level_ind information.
----------------------------------------------------------------------------------------
FUNCTION GET_HEADER_INFO(O_error_message                IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_partner_type                 IN OUT DEAL_HEAD.PARTNER_TYPE%TYPE,
                         O_partner_id                   IN OUT DEAL_HEAD.PARTNER_ID%TYPE,
                         O_supplier                     IN OUT DEAL_HEAD.SUPPLIER%TYPE,
                         O_type                         IN OUT DEAL_HEAD.TYPE%TYPE,
                         O_status                       IN OUT DEAL_HEAD.STATUS%TYPE,
                         O_currency_code                IN OUT DEAL_HEAD.CURRENCY_CODE%TYPE,
                         O_active_date                  IN OUT DEAL_HEAD.ACTIVE_DATE%TYPE,
                         O_close_date                   IN OUT DEAL_HEAD.CLOSE_DATE%TYPE,
                         O_create_id                    IN OUT DEAL_HEAD.CREATE_ID%TYPE,
                         O_ext_ref_no                   IN OUT DEAL_HEAD.EXT_REF_NO%TYPE,
                         O_order_no                     IN OUT DEAL_HEAD.ORDER_NO%TYPE,
                         O_billing_type                 IN OUT DEAL_HEAD.BILLING_TYPE%TYPE,
                         O_bill_back_period             IN OUT DEAL_HEAD.BILL_BACK_PERIOD%TYPE,
                         O_deal_appl_timing             IN OUT DEAL_HEAD.DEAL_APPL_TIMING%TYPE,
                         O_threshold_limit_type         IN OUT DEAL_HEAD.THRESHOLD_LIMIT_TYPE%TYPE,
                         O_threshold_limit_uom          IN OUT DEAL_HEAD.THRESHOLD_LIMIT_UOM%TYPE,
                         O_rebate_ind                   IN OUT DEAL_HEAD.REBATE_IND%TYPE,
                         O_rebate_calc_type             IN OUT DEAL_HEAD.REBATE_CALC_TYPE%TYPE,
                         O_growth_rebate_ind            IN OUT DEAL_HEAD.GROWTH_REBATE_IND%TYPE,
                         O_hist_comp_start_date         IN OUT DEAL_HEAD.HISTORICAL_COMP_START_DATE%TYPE,
                         O_hist_comp_end_date           IN OUT DEAL_HEAD.HISTORICAL_COMP_END_DATE%TYPE,
                         O_rebate_purch_sales_ind       IN OUT DEAL_HEAD.REBATE_PURCH_SALES_IND%TYPE,
                         O_deal_reporting_level         IN OUT DEAL_HEAD.DEAL_REPORTING_LEVEL%TYPE,
                         O_bill_back_method             IN OUT DEAL_HEAD.BILL_BACK_METHOD%TYPE,
                         O_deal_income_calc             IN OUT DEAL_HEAD.DEAL_INCOME_CALCULATION%TYPE,
                         O_invoice_proc_logic           IN OUT DEAL_HEAD.INVOICE_PROCESSING_LOGIC%TYPE,
                         O_stock_ledger_ind             IN OUT DEAL_HEAD.STOCK_LEDGER_IND%TYPE,
                         O_include_vat_ind              IN OUT DEAL_HEAD.INCLUDE_VAT_IND%TYPE,
                         O_billing_partner_type         IN OUT DEAL_HEAD.BILLING_PARTNER_TYPE%TYPE,
                         O_billing_partner_id           IN OUT DEAL_HEAD.BILLING_PARTNER_ID%TYPE,
                         O_billing_supplier_id          IN OUT DEAL_HEAD.BILLING_SUPPLIER_ID%TYPE,
                         O_growth_rate_to_date          IN OUT DEAL_HEAD.GROWTH_RATE_TO_DATE%TYPE,
                         O_turnover_to_date             IN OUT DEAL_HEAD.TURNOVER_TO_DATE%TYPE,
                         O_actual_monies_earned_to_date IN OUT DEAL_HEAD.ACTUAL_MONIES_EARNED_TO_DATE%TYPE,
                         O_security_ind                 IN OUT DEAL_HEAD.SECURITY_IND%TYPE,
                         O_est_next_invoice_date        IN OUT DEAL_HEAD.EST_NEXT_INVOICE_DATE%TYPE,
                         O_last_invoice_date            IN OUT DEAL_HEAD.LAST_INVOICE_DATE%TYPE,
                         O_track_pack_level_ind         IN OUT DEAL_HEAD.TRACK_PACK_LEVEL_IND%TYPE,
                         I_deal_id                      IN     DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------
-- Function Name:  GET_DEAL_COMP_TYPE_DESC
-- Purpose:     This function will retrieve the translated description of the passed
--                 in deal component type.
----------------------------------------------------------------------------------------
FUNCTION GET_DEAL_COMP_TYPE_DESC(O_error_message   IN OUT VARCHAR2,
                                 O_desc            IN OUT DEAL_COMP_TYPE_TL.DEAL_COMP_TYPE_DESC%TYPE,
                                 I_deal_comp_type  IN     DEAL_COMP_TYPE.DEAL_COMP_TYPE%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- Function Name:  GET_MERCH_HIER
-- Purpose:        This function will retrieve the code and description of all merchandise
--                 hierarchies above the passed in hierarchy.
-------------------------------------------------------------------------------------------
FUNCTION GET_MERCH_HIER(O_error_message    IN OUT VARCHAR2,
                        O_division      IN OUT DIVISION.DIVISION%TYPE,
                        O_division_name    IN OUT DIVISION.DIV_NAME%TYPE,
                        O_group_no      IN OUT GROUPS.GROUP_NO%TYPE,
                        O_group_name    IN OUT GROUPS.GROUP_NAME%TYPE,
                        O_dept             IN OUT DEPS.DEPT%TYPE,
                        O_dept_name        IN OUT DEPS.DEPT_NAME%TYPE,
                        O_class            IN OUT CLASS.CLASS%TYPE,
                        O_class_name       IN OUT CLASS.CLASS_NAME%TYPE,
                        O_subclass         IN OUT SUBCLASS.SUBCLASS%TYPE,
                        O_subclass_name    IN OUT SUBCLASS.SUB_NAME%TYPE,
                        O_item_grandparent IN OUT ITEM_MASTER.ITEM_GRANDPARENT%TYPE,
                        O_item_gp_desc     IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                        O_item_parent      IN OUT ITEM_MASTER.ITEM_PARENT%TYPE,
                        O_item_p_desc      IN OUT ITEM_MASTER.ITEM_DESC%TYPE,
                        O_diff_1           IN OUT ITEM_MASTER.DIFF_1%TYPE,
                        O_diff_1_desc      IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                        O_diff_2           IN OUT ITEM_MASTER.DIFF_2%TYPE,
                        O_diff_2_desc      IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                        O_diff_3           IN OUT ITEM_MASTER.DIFF_3%TYPE,
                        O_diff_3_desc      IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                        O_diff_4           IN OUT ITEM_MASTER.DIFF_4%TYPE,
                        O_diff_4_desc      IN OUT DIFF_IDS.DIFF_DESC%TYPE,
                        I_merch_level      IN     VARCHAR2,
                        I_merch_value      IN     VARCHAR2,
                        I_get_descs        IN     BOOLEAN)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name:  GET_ORG_HIER
-- Purpose:        This function will retrieve the code and description of all organization
--                 hierarchies above the passed in hierarchy.
--------------------------------------------------------------------------------------------
FUNCTION GET_ORG_HIER(O_error_message  IN OUT VARCHAR2,
                      O_chain             IN OUT CHAIN.CHAIN%TYPE,
                      O_chain_name        IN OUT CHAIN.CHAIN_NAME%TYPE,
                      O_area              IN OUT AREA.AREA%TYPE,
                      O_area_name         IN OUT AREA.AREA_NAME%TYPE,
                      O_region            IN OUT REGION.REGION%TYPE,
                      O_region_name       IN OUT REGION.REGION_NAME%TYPE,
                      O_district          IN OUT DISTRICT.DISTRICT%TYPE,
                      O_district_name     IN OUT DISTRICT.DISTRICT_NAME%TYPE,
                      I_org_level         IN     VARCHAR2,
                      I_org_value         IN     NUMBER,
                      I_get_descs         IN     BOOLEAN)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name:  GET_NEXT_DEAL_ID
-- Purpose:        This function will retrieve the next value for the Deal ID, which is
--                 a system generated primary key.  This function will use the
--                 DEAL_SEQUENCE to keep the key unique.
--------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DEAL_ID(O_error_message  IN OUT VARCHAR2,
                          O_deal_id        IN OUT DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name:  GET_NEXT_DEAL_DETAIL_ID
-- Purpose:        This function will generate a unique deal_detail_id.
--------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DEAL_DETAIL_ID(O_error_message   IN OUT VARCHAR2,
                                 O_deal_detail_id  IN OUT DEAL_DETAIL.DEAL_DETAIL_ID%TYPE,
                                 I_deal_id         IN     DEAL_ITEMLOC.DEAL_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name:  GET_NEXT_DEALITLC_SEQ
-- Purpose:        This function will select the max sequence from the deal_itemloc table.
--------------------------------------------------------------------------------------------
FUNCTION GET_NEXT_DEALITLC_SEQ(O_error_message   IN OUT VARCHAR2,
                               O_seq_no          IN OUT DEAL_ITEMLOC.SEQ_NO%TYPE,
                               I_deal_id         IN     DEAL_ITEMLOC.DEAL_ID%TYPE,
                               I_deal_detail_id  IN     DEAL_ITEMLOC.DEAL_DETAIL_ID%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name:  GET_ROOT_ITEMLOC_LEVEL
-- Purpose:     This function will retrieve the root hierarchy level based on the
--                 deal_itemloc records for a given deal id and deal component.
------------------------------------------------------------------------------------------------
FUNCTION GET_ROOT_ITEMLOC_LEVEL(O_error_message      IN OUT VARCHAR2,
                                O_exists             IN OUT BOOLEAN,
                                O_merch_level        IN OUT DEAL_ITEMLOC.MERCH_LEVEL%TYPE,
                                O_org_level          IN OUT DEAL_ITEMLOC.ORG_LEVEL%TYPE,
                                O_origin_country_id  IN OUT DEAL_ITEMLOC.ORIGIN_COUNTRY_ID%TYPE,
                                I_deal_id            IN     DEAL_HEAD.DEAL_ID%TYPE,
                                I_deal_detail_id     IN     DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
   RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- Funtion Name:  ACTIVE_ANNUAL_DEAL_EXISTS
-- Purpose:    This function will check for the existence of an active annual deal
--                on the deal_head table for a given supplier.  This function will be called
--                online when an annual deal is being approved.
--------------------------------------------------------------------------------------------
FUNCTION ACTIVE_ANNUAL_DEAL_EXISTS(O_error_message    IN OUT VARCHAR2,
                                   O_exists           IN OUT BOOLEAN,
                                   O_close_date       IN OUT DEAL_HEAD.CLOSE_DATE%TYPE,
                                   O_deal_id          IN OUT DEAL_HEAD.DEAL_ID%TYPE,
                                   I_partner_type     IN     DEAL_HEAD.PARTNER_TYPE%TYPE,
                                   I_partner_id       IN     DEAL_HEAD.PARTNER_ID%TYPE,
                                   I_supplier         IN     DEAL_HEAD.SUPPLIER%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Purpose:    This function will check for the existence of a pending annual deal
--                on the deal_head table for a given supplier.  It will be called online
--                when the close_date of an active annual deal is being changed and when
--                a deal is being approved.
--------------------------------------------------------------------------------------------
FUNCTION PENDING_ANNUAL_DEAL_EXISTS(O_error_message    IN OUT VARCHAR2,
                                    O_exists           IN OUT BOOLEAN,
                                    O_active_date      IN OUT DEAL_HEAD.ACTIVE_DATE%TYPE,
                                    O_close_date       IN OUT DEAL_HEAD.CLOSE_DATE%TYPE,
                                    O_deal_id          IN OUT DEAL_HEAD.DEAL_ID%TYPE,
                                    I_active_date      IN     DEAL_HEAD.ACTIVE_DATE%TYPE,
                                    I_close_date       IN     DEAL_HEAD.CLOSE_DATE%TYPE,
                                    I_partner_type     IN     DEAL_HEAD.PARTNER_TYPE%TYPE,
                                    I_partner_id       IN     DEAL_HEAD.PARTNER_ID%TYPE,
                                    I_supplier         IN     DEAL_HEAD.SUPPLIER%TYPE,
                                    I_deal_id          IN     DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;


--------------------------------------------------------------------------------------------
-- Function Name:  DEAL_DETAIL_INFO
-- Purpose:        This function will get generic detail information for a component from
--                 the DEAL_DETAIL table.
--------------------------------------------------------------------------------------------
FUNCTION DEAL_DETAIL_INFO(O_error_message                  IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_deal_comp_type                 IN OUT   DEAL_DETAIL.DEAL_COMP_TYPE%TYPE,
                          O_application_order              IN OUT   DEAL_DETAIL.APPLICATION_ORDER%TYPE,
                          O_collect_start_date             IN OUT   DEAL_DETAIL.COLLECT_START_DATE%TYPE,
                          O_collect_end_date               IN OUT   DEAL_DETAIL.COLLECT_END_DATE%TYPE,
                          O_cost_appl_ind                  IN OUT   DEAL_DETAIL.COST_APPL_IND%TYPE,
                          O_deal_class                     IN OUT   DEAL_DETAIL.DEAL_CLASS%TYPE,
                          O_tran_discount_ind              IN OUT   DEAL_DETAIL.TRAN_DISCOUNT_IND%TYPE,
                          O_calc_to_zero_ind               IN OUT   DEAL_DETAIL.CALC_TO_ZERO_IND%TYPE,
                          O_tal_forecast_units             IN OUT   DEAL_DETAIL.TOTAL_FORECAST_UNITS%TYPE,
                          O_tal_forecast_revenue           IN OUT   DEAL_DETAIL.TOTAL_FORECAST_REVENUE%TYPE,
                          O_tal_budget_turnover            IN OUT   DEAL_DETAIL.TOTAL_BUDGET_TURNOVER%TYPE,
                          O_tal_actual_forecast_turnover   IN OUT   DEAL_DETAIL.TOTAL_ACTUAL_FORECAST_TURNOVER%TYPE,
                          O_tal_baseline_growth_budget     IN OUT   DEAL_DETAIL.TOTAL_BASELINE_GROWTH_BUDGET%TYPE,
                          O_tal_baseline_growth_act_for    IN OUT   DEAL_DETAIL.TOTAL_BASELINE_GROWTH_ACT_FOR%TYPE,
                          I_deal_id                        IN       DEAL_DETAIL.DEAL_ID%TYPE,
                          I_deal_detail_id                 IN       DEAL_DETAIL.DEAL_DETAIL_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function Name:  GET_DEAL_ITEM_LOC_INFO
-- Purpose:        This function will get generic detail information for a component from
--                 the DEAL_ITEMLOC table.
--------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_ITEM_LOC_INFO(O_error_message      IN OUT VARCHAR2,
                                O_merch_level        IN OUT DEAL_ITEMLOC.MERCH_LEVEL%TYPE,
                                O_merch_value_1      IN OUT DEAL_ITEMLOC.ITEM%TYPE,
                                O_merch_value_2      IN OUT DEAL_ITEMLOC.ITEM%TYPE,
                                O_merch_value_3      IN OUT DEAL_ITEMLOC.ITEM%TYPE,
                                O_org_level          IN OUT DEAL_ITEMLOC.ORG_LEVEL%TYPE,
                                O_org_value          IN OUT DEAL_ITEMLOC.LOCATION%TYPE,
                                O_loc_type           IN OUT DEAL_ITEMLOC.LOC_TYPE%TYPE,
                                I_deal_id            IN     DEAL_ITEMLOC.DEAL_ID%TYPE,
                                I_deal_detail_id     IN     DEAL_ITEMLOC.DEAL_DETAIL_ID%TYPE,
                                I_seq_no             IN     DEAL_ITEMLOC.SEQ_NO%TYPE)
   RETURN BOOLEAN;
--------------------------------------------------------------------------------------------
-- Function Name: DEAL_QUEUE_EXISTS
-- Purpose:       This function will check to see if a deal a deal_queue record exists for
--                a deal.  If a deal_queue record exists (meaning that the deal has been
--                approved, closed, or moved back to worksheet status since the last batch
--                run), the user should not be able to edit the deal.  A batch run needs
--                to occur between each edit after initial deal approval so that the records
--                on the future cost table remain in synch.
--------------------------------------------------------------------------------------------
FUNCTION DEAL_QUEUE_EXISTS(O_error_message         IN OUT VARCHAR2,
                           O_exists                IN OUT BOOLEAN,
                           I_deal_id               IN     DEAL_HEAD.DEAL_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
-- Function Name:  GET_DEAL_PROM_ID
-- Purpose:        This function will retrieve the next value for the Deal Prom ID, which is
--                 a system generated primary key.  This function will use the
--                 DEAL_PROM_ID_SEQUENCE to keep the key unique.
--------------------------------------------------------------------------------------------
FUNCTION GET_DEAL_PROM_ID(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                          O_deal_prom_id    IN OUT   DEAL_PROM.DEAL_PROM_ID%TYPE)
   RETURN BOOLEAN;

--------------------------------------------------------------------------------------------
END DEAL_ATTRIB_SQL;
/

