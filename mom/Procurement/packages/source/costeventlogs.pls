CREATE OR REPLACE PACKAGE COST_EVENT_LOG_SQL AUTHID CURRENT_USER AS
-- define global package variables and types
----------------------------------------------------------------------------------------------------------------------
TYPE cost_event_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
   event_desc              COST_EVENT_RUN_TYPE_CONFIG.EVENT_DESC%TYPE,
   event_type              COST_EVENT.EVENT_TYPE%TYPE,
   status_desc             CODE_DETAIL.CODE_DESC%TYPE,
   user_id                 COST_EVENT.USER_ID%TYPE,
   create_datetime         COST_EVENT.CREATE_DATETIME%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE cost_event_tbl_type IS TABLE OF cost_event_rec
   INDEX BY BINARY_INTEGER;

TYPE cost_event_result_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_RESULT.COST_EVENT_PROCESS_ID%TYPE,
   thread_id               COST_EVENT_RESULT.THREAD_ID%TYPE,
   create_datetime         VARCHAR2(8),
   event_desc              COST_EVENT_RUN_TYPE_CONFIG.EVENT_DESC%TYPE,
   status                  COST_EVENT_RESULT.STATUS%TYPE,
   status_desc             CODE_DETAIL.CODE_DESC%TYPE,
   cer_error_message       COST_EVENT_RESULT.ERROR_MESSAGE%TYPE, /* to be used  for cost event result error message */
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE cost_event_result_tbl_type IS TABLE OF cost_event_result_rec
   INDEX BY BINARY_INTEGER;

TYPE cost_event_thread_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_THREAD.COST_EVENT_PROCESS_ID%TYPE,
   thread_id               COST_EVENT_THREAD.THREAD_ID%TYPE,
   item                    COST_EVENT_THREAD.ITEM%TYPE,
   supplier                COST_EVENT_THREAD.SUPPLIER%TYPE,
   origin_country_id       COST_EVENT_THREAD.ORIGIN_COUNTRY_ID%TYPE,
   location                COST_EVENT_THREAD.LOCATION%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE cost_event_thread_tbl_type IS TABLE OF cost_event_thread_rec
   INDEX BY BINARY_INTEGER;
  

TYPE supp_country_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_SUPP_COUNTRY.COST_EVENT_PROCESS_ID%TYPE,   
   item                    COST_EVENT_SUPP_COUNTRY.ITEM%TYPE,
   item_desc               ITEM_MASTER.ITEM_DESC%TYPE,
   supplier                COST_EVENT_SUPP_COUNTRY.SUPPLIER%TYPE,
   sup_name                SUPS.SUP_NAME%TYPE,
   origin_country_id       COST_EVENT_SUPP_COUNTRY.ORIGIN_COUNTRY_ID%TYPE,
   country_desc            COUNTRY.COUNTRY_DESC%TYPE,
   location                COST_EVENT_SUPP_COUNTRY.LOCATION%TYPE,
   location_desc           V_LOCATION.LOCATION_NAME%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE supp_country_tbl_type IS TABLE OF supp_country_rec
   INDEX BY BINARY_INTEGER;

TYPE cost_change_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_SUPP_COUNTRY.COST_EVENT_PROCESS_ID%TYPE,
   cost_change             COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
   cost_change_desc        COST_SUSP_SUP_HEAD.COST_CHANGE_DESC%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE cost_change_tbl_type IS TABLE OF cost_change_rec
   INDEX BY BINARY_INTEGER;
  
TYPE new_item_loc_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_NIL.COST_EVENT_PROCESS_ID%TYPE,   
   item                    COST_EVENT_NIL.ITEM%TYPE,
   item_desc               ITEM_MASTER.ITEM_DESC%TYPE,
   location                COST_EVENT_NIL.LOCATION%TYPE,
   location_desc           V_LOCATION.LOCATION_NAME%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE new_item_loc_tbl_type IS TABLE OF new_item_loc_rec
   INDEX BY BINARY_INTEGER;  
  
TYPE reclass_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_RECLASS.COST_EVENT_PROCESS_ID%TYPE,   
   reclass_no              COST_EVENT_RECLASS.RECLASS_NO%TYPE,
   reclass_desc            RECLASS_HEAD.RECLASS_DESC%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE reclass_tbl_type IS TABLE OF reclass_rec
   INDEX BY BINARY_INTEGER;  

TYPE merch_hier_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_MERCH_HIER.COST_EVENT_PROCESS_ID%TYPE,
   new_division            COST_EVENT_MERCH_HIER.NEW_DIVISION%TYPE,
   new_division_name       DIVISION.DIV_NAME%TYPE,
   old_division            COST_EVENT_MERCH_HIER.OLD_DIVISION%TYPE,
   old_division_name       DIVISION.DIV_NAME%TYPE,
   new_group               COST_EVENT_MERCH_HIER.NEW_GROUP_NO%TYPE,
   new_group_name          GROUPS.GROUP_NAME%TYPE,
   old_group               COST_EVENT_MERCH_HIER.OLD_GROUP_NO%TYPE,
   old_group_name          GROUPS.GROUP_NAME%TYPE,
   dept                    COST_EVENT_MERCH_HIER.DEPT%TYPE,
   dept_name               DEPS.DEPT_NAME%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE merch_hier_tbl_type IS TABLE OF merch_hier_rec
   INDEX BY BINARY_INTEGER;    

TYPE deal_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_DEAL.COST_EVENT_PROCESS_ID%TYPE,
   deal_id                 COST_EVENT_DEAL.DEAL_ID%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE deal_tbl_type IS TABLE OF deal_rec
   INDEX BY BINARY_INTEGER;    

TYPE item_cost_zone_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_ITEM_COST_ZONE.COST_EVENT_PROCESS_ID%TYPE,
   item                    COST_EVENT_ITEM_COST_ZONE.ITEM%TYPE,
   item_desc               ITEM_MASTER.ITEM_DESC%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE item_cost_zone_tbl_type IS TABLE OF item_cost_zone_rec
   INDEX BY BINARY_INTEGER;    

TYPE prim_pack_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_PRIM_PACK.COST_EVENT_PROCESS_ID%TYPE,
   item                    COST_EVENT_PRIM_PACK.ITEM%TYPE,
   item_desc               ITEM_MASTER.ITEM_DESC%TYPE,
   location                COST_EVENT_PRIM_PACK.LOCATION%TYPE,
   location_desc           V_LOCATION.LOCATION_NAME%TYPE,
   pack_no                 COST_EVENT_PRIM_PACK.PACK_NO%TYPE,
   pack_desc               ITEM_MASTER.ITEM_DESC%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE prim_pack_tbl_type IS TABLE OF prim_pack_rec
   INDEX BY BINARY_INTEGER;    
   
TYPE org_hier_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_ORG_HIER.COST_EVENT_PROCESS_ID%TYPE,
   new_chain               COST_EVENT_ORG_HIER.NEW_CHAIN%TYPE,
   new_chain_desc          CHAIN.CHAIN_NAME%TYPE,
   old_chain               COST_EVENT_ORG_HIER.OLD_CHAIN%TYPE,
   old_chain_desc          CHAIN.CHAIN_NAME%TYPE,
   new_area                COST_EVENT_ORG_HIER.NEW_AREA%TYPE,
   new_area_desc           AREA.AREA_NAME%TYPE,
   old_area                COST_EVENT_ORG_HIER.OLD_AREA%TYPE,
   old_area_desc           AREA.AREA_NAME%TYPE,
   new_region              COST_EVENT_ORG_HIER.NEW_REGION%TYPE,
   new_region_desc         REGION.REGION_NAME%TYPE,
   old_region              COST_EVENT_ORG_HIER.OLD_REGION%TYPE,
   old_region_desc         REGION.REGION_NAME%TYPE,
   new_district            COST_EVENT_ORG_HIER.NEW_DISTRICT%TYPE,
   new_district_desc       DISTRICT.DISTRICT_NAME%TYPE,
   old_district            COST_EVENT_ORG_HIER.OLD_DISTRICT%TYPE,
   old_district_desc       DISTRICT.DISTRICT_NAME%TYPE,
   location                STORE.STORE%TYPE,
   location_name           STORE.STORE_NAME%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE org_hier_tbl_type IS TABLE OF org_hier_rec
   INDEX BY BINARY_INTEGER;    
   

TYPE location_cost_zone_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_COST_ZONE.COST_EVENT_PROCESS_ID%TYPE,
   zone_group_id           COST_EVENT_COST_ZONE.ZONE_GROUP_ID%TYPE,
   zone_group_desc         COST_ZONE_GROUP.DESCRIPTION%TYPE,
   new_zone_id             COST_EVENT_COST_ZONE.NEW_ZONE_ID%TYPE,
   new_zone_desc           COST_ZONE.DESCRIPTION%TYPE,
   old_zone_id             COST_EVENT_COST_ZONE.OLD_ZONE_ID%TYPE,
   old_zone_desc           COST_ZONE.DESCRIPTION%TYPE,
   location                COST_EVENT_COST_ZONE.LOCATION%TYPE,
   location_desc           V_LOCATION.LOCATION_NAME%TYPE,   
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE location_cost_zone_tbl_type IS TABLE OF location_cost_zone_rec
   INDEX BY BINARY_INTEGER;    

TYPE supp_hier_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_SUPP_HIER.COST_EVENT_PROCESS_ID%TYPE,
   item                    COST_EVENT_SUPP_HIER.ITEM%TYPE,
   supplier                COST_EVENT_SUPP_HIER.SUPPLIER%TYPE,
   origin_country_id       COST_EVENT_SUPP_HIER.ORIGIN_COUNTRY_ID%TYPE,
   location                COST_EVENT_SUPP_HIER.LOCATION%TYPE,
   new_supp_hier_lvl_1     COST_EVENT_SUPP_HIER.NEW_SUPP_HIER_LVL_1%TYPE,
   old_supp_hier_lvl_1     COST_EVENT_SUPP_HIER.OLD_SUPP_HIER_LVL_1%TYPE,
   new_supp_hier_lvl_2     COST_EVENT_SUPP_HIER.NEW_SUPP_HIER_LVL_2%TYPE,
   old_supp_hier_lvl_2     COST_EVENT_SUPP_HIER.OLD_SUPP_HIER_LVL_2%TYPE,
   new_supp_hier_lvl_3     COST_EVENT_SUPP_HIER.NEW_SUPP_HIER_LVL_3%TYPE,
   old_supp_hier_lvl_3     COST_EVENT_SUPP_HIER.OLD_SUPP_HIER_LVL_3%TYPE,   
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE supp_hier_tbl_type IS TABLE OF supp_hier_rec
   INDEX BY BINARY_INTEGER;    

TYPE elc_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_ELC.COST_EVENT_PROCESS_ID%TYPE,
   item                    COST_EVENT_ELC.ITEM%TYPE,
   item_desc               ITEM_MASTER.ITEM_DESC%TYPE,
   supplier                COST_EVENT_ELC.SUPPLIER%TYPE,
   sup_name                SUPS.SUP_NAME%TYPE,
   origin_country_id       COST_EVENT_ELC.ORIGIN_COUNTRY_ID%TYPE,
   country_desc            COUNTRY.COUNTRY_DESC%TYPE,
   zone_group_id           COST_EVENT_ELC.COST_ZONE_GROUP%TYPE,
   zone_group_desc         COST_ZONE_GROUP.DESCRIPTION%TYPE,
   zone_id                 COST_EVENT_ELC.COST_ZONE%TYPE,
   zone_desc               COST_ZONE.DESCRIPTION%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE elc_tbl_type IS TABLE OF elc_rec
   INDEX BY BINARY_INTEGER;
   
TYPE templ_rec IS RECORD
(
   cost_event_process_id   COST_EVENT_COST_TMPL.COST_EVENT_PROCESS_ID%TYPE,
   templ_id                COST_EVENT_COST_TMPL.TEMPL_ID%TYPE,
   templ_desc              WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE templ_tbl_type IS TABLE OF templ_rec
   INDEX BY BINARY_INTEGER;

TYPE costbldup_rec IS RECORD
(
   cost_event_process_id   COST_EVENT_COST_RELATIONSHIP.COST_EVENT_PROCESS_ID%TYPE,
   dept                    COST_EVENT_COST_RELATIONSHIP.DEPT%TYPE,
   class                   COST_EVENT_COST_RELATIONSHIP.CLASS%TYPE,     
   subclass                COST_EVENT_COST_RELATIONSHIP.SUBCLASS%TYPE,
   location                COST_EVENT_COST_RELATIONSHIP.LOCATION%TYPE,
   templ_id                COST_EVENT_COST_RELATIONSHIP.TEMPL_ID%TYPE,
   templ_desc              WF_COST_BUILDUP_TMPL_HEAD.TEMPL_DESC%TYPE,
   old_start_date          COST_EVENT_COST_RELATIONSHIP.OLD_START_DATE%TYPE,
   old_end_date            COST_EVENT_COST_RELATIONSHIP.OLD_END_DATE%TYPE,
   new_start_date          COST_EVENT_COST_RELATIONSHIP.NEW_START_DATE%TYPE,
   new_end_date            COST_EVENT_COST_RELATIONSHIP.NEW_END_DATE%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE costbldup_tbl_type IS TABLE OF costbldup_rec
   INDEX BY BINARY_INTEGER;

TYPE dealpassthru_rec IS RECORD
(
   cost_event_process_id   COST_EVENT_DEAL_PASSTHRU.COST_EVENT_PROCESS_ID%TYPE,
   dept                    COST_EVENT_DEAL_PASSTHRU.DEPT%TYPE,
   supplier                COST_EVENT_DEAL_PASSTHRU.SUPPLIER%TYPE,
   loc_type                COST_EVENT_DEAL_PASSTHRU.LOC_TYPE%TYPE,
   location                COST_EVENT_DEAL_PASSTHRU.LOCATION%TYPE,
   costing_loc             COST_EVENT_DEAL_PASSTHRU.COSTING_LOC%TYPE,
   costing_loc_name        WH.WH_NAME%TYPE,
   passthru_pct            COST_EVENT_DEAL_PASSTHRU.PASSTHRU_PCT%TYPE,
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE dealpassthru_tbl_type IS TABLE OF dealpassthru_rec
   INDEX BY BINARY_INTEGER;

TYPE audit_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_RESULT.COST_EVENT_PROCESS_ID%TYPE,
   thread_id               COST_EVENT_RESULT.THREAD_ID%TYPE,
   attempt_num             COST_EVENT_RESULT.ATTEMPT_NUM%TYPE,
   cer_error_message       COST_EVENT_RESULT.ERROR_MESSAGE%TYPE,
   retry_user_id           COST_EVENT_RESULT.RETRY_USER_ID%TYPE,
   retry_date              VARCHAR2(8),
   error_message           RTK_ERRORS.RTK_TEXT%TYPE,
   return_code             VARCHAR2(5)
);

TYPE audit_tbl_type IS TABLE OF audit_rec
   INDEX BY BINARY_INTEGER;

TYPE cost_event_det_rec IS RECORD 
(
   cost_event_process_id   COST_EVENT_THREAD.COST_EVENT_PROCESS_ID%TYPE,
   thread_id               COST_EVENT_THREAD.THREAD_ID%TYPE
);

TYPE cost_event_det_tbl  IS TABLE OF cost_event_det_rec
    INDEX BY BINARY_INTEGER;   
   
TYPE item_locs_rec IS RECORD 
(
   item         ITEM_MASTER.ITEM%TYPE,
   location     V_LOCATION.LOCATION_ID%TYPE
);

TYPE item_locs_tbl  IS TABLE OF item_locs_rec
    INDEX BY BINARY_INTEGER;   
  
----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENTS
-- Purpose      : This procedure will populate the information on the Future Cost Event Log form.
--                This will get records based on the records from the COST_EVENT table.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENTS (IO_cost_event_table IN OUT COST_EVENT_TBL_TYPE,
                           I_event_id          IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                           I_event_type        IN     COST_EVENT.EVENT_TYPE%TYPE,
                           I_status            IN     COST_EVENT_RESULT.STATUS%TYPE,
                           I_user_id           IN     COST_EVENT.USER_ID%TYPE,
                           I_event_date_from   IN     COST_EVENT.CREATE_DATETIME%TYPE,
                           I_event_date_to     IN     COST_EVENT.CREATE_DATETIME%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_RESULT
-- Purpose      : This procedure will populate the multi-record block on the Future Cost Event Log form 
--                (Cost Event Thread view) given the Cost Event ID. 
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_RESULT (IO_cost_event_result_table IN OUT COST_EVENT_RESULT_TBL_TYPE,
                                 I_event_id                 IN     COST_EVENT.COST_EVENT_PROCESS_ID%TYPE,
                                 I_event_thread_id          IN     COST_EVENT_THREAD.THREAD_ID%TYPE,
                                 I_status                   IN     COST_EVENT_RESULT.STATUS%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_THREAD
-- Purpose      : This procedure will populate the multi-record block on the Future Cost Event Log form 
--                (Item Supp Country Location Cost Event Thread  View) given the Cost Event ID.. 
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_THREAD (IO_cost_event_thread_table IN OUT COST_EVENT_THREAD_TBL_TYPE,
                                 I_event_id                 IN     COST_EVENT_THREAD.COST_EVENT_PROCESS_ID%TYPE,
                                 I_event_thread_id          IN     COST_EVENT_THREAD.THREAD_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_SUPPC_DETAIL
-- Purpose      : The procedure will populate the information on the supplier country view form. 
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_SUPPC_DETAIL (IO_supp_country_table IN OUT SUPP_COUNTRY_TBL_TYPE,
                                       I_event_id            IN     COST_EVENT_SUPP_COUNTRY.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_CC_DETAIL
-- Purpose      : The procedure will populate the information on the cost change view form.  
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_CC_DETAIL (IO_cost_change_table IN OUT COST_CHANGE_TBL_TYPE,
                                    I_event_id           IN     COST_EVENT_COST_CHG.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_NIL_DETAIL
-- Purpose      : The procedure will populate the information on the new item loc view form. .  
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_NIL_DETAIL(IO_new_itemloc_table IN OUT NEW_ITEM_LOC_TBL_TYPE,
                                    I_event_id           IN     COST_EVENT_NIL.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_RECLASS_DETAIL
-- Purpose      : The procedure will populate the information on the reclassification view form. .  
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_RECLASS_DETAIL(IO_reclass_table IN OUT RECLASS_TBL_TYPE,
                                        I_event_id       IN     COST_EVENT_RECLASS.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_MERCH_DETAIL
-- Purpose      : The procedure will populate the information on the merchandise hierarchy view form. .  
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_MERCH_DETAIL(IO_merch_hier_table IN OUT MERCH_HIER_TBL_TYPE,
                                      I_event_id          IN     COST_EVENT_MERCH_HIER.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_DEAL_DETAIL
-- Purpose      : The procedure will populate the information on the deal view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_DEAL_DETAIL(IO_deal_table       IN OUT DEAL_TBL_TYPE,
                                     I_event_id          IN     COST_EVENT_DEAL.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_ITC_ZONE_DETAIL
-- Purpose      : The procedure will populate the information on the item cost zone view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_ITC_ZONE_DETAIL(IO_item_cost_zone_table       IN OUT ITEM_COST_ZONE_TBL_TYPE,
                                          I_event_id                   IN     COST_EVENT_ITEM_COST_ZONE.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_PPACK_DETAIL
-- Purpose      : The procedure will populate the information on the primary pack view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_PPACK_DETAIL (IO_prim_pack_table            IN OUT PRIM_PACK_TBL_TYPE,
                                       I_event_id                    IN     COST_EVENT_PRIM_PACK.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_EVENT_ORG_HIER_DETAIL
-- Purpose      : The procedure will populate the information on the org hier view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_ORG_HIER_DETAIL (IO_org_hier_table             IN OUT ORG_HIER_TBL_TYPE,
                                          I_event_id                    IN     COST_EVENT_ORG_HIER.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name: GET_COST_LOC_COST_ZONE_DETAIL
-- Purpose      : The procedure will populate the information on the location cost zone view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_LOC_COST_ZONE_DETAIL (IO_location_cost_zone_table   IN OUT LOCATION_COST_ZONE_TBL_TYPE,
                                         I_event_id                    IN     COST_EVENT_COST_ZONE.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name:GET_COST_EVENT_SP_HIER_DETAIL
-- Purpose      : The procedure will populate the information on the supplier hier view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_SP_HIER_DETAIL (IO_supp_hier_table            IN OUT SUPP_HIER_TBL_TYPE,
                                         I_event_id                    IN     COST_EVENT_SUPP_HIER.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name:GET_COST_EVENT_ELC_DETAIL
-- Purpose      : The procedure will populate the information on the cost event ELC view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_ELC_DETAIL (IO_elc_table            IN OUT ELC_TBL_TYPE,
                                     I_event_id              IN     COST_EVENT_ELC.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name:GET_COST_EVENT_TEMPLATE_DETAIL
-- Purpose      : The procedure will populate the information on the template detail form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_TEMPLATE_DETAIL (IO_template_table  IN OUT TEMPL_TBL_TYPE,
                                          I_event_id         IN     COST_EVENT_COST_TMPL.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name:GET_COST_EVENT_CSTBLD_DETAIL
-- Purpose      : The procedure will populate the information on the template relationship detail form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_T_RELN_DETAIL (IO_costbldup_table IN OUT COSTBLDUP_TBL_TYPE,
                                        I_event_id         IN     COST_EVENT_COST_RELATIONSHIP.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name:GET_COST_EVENT_DP_DETAIL
-- Purpose      : The procedure will populate the information on the Deal Pass through detail form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_DP_DETAIL (IO_dealpassthru_table IN OUT DEALPASSTHRU_TBL_TYPE,
                                    I_event_id            IN     COST_EVENT_DEAL_PASSTHRU.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Procedure Name:GET_COST_EVENT_AUDIT_DETAIL
-- Purpose      : The procedure will populate the information on the audit view form.
----------------------------------------------------------------------------------------------------------------------
PROCEDURE GET_COST_EVENT_AUDIT_DETAIL (IO_audit_table          IN OUT AUDIT_TBL_TYPE,
                                       I_event_id              IN     COST_EVENT_RESULT.COST_EVENT_PROCESS_ID%TYPE);

----------------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS
-- Purpose      : The function is responsible for re-submitting a cost event for processing by the future cost engine.
----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                  I_process_tbl       IN       COST_EVENT_DET_TBL)
   RETURN BOOLEAN;

----------------------------------------------------------------------------------------------------------------------
-- Function Name: CHECK_COST_EVENT_CC
-- Purpose      : The function is responsible to check if a cost event already exists that successfully 
--                completed for the cost change that affected the future cost.
----------------------------------------------------------------------------------------------------------------------
FUNCTION CHECK_COST_EVENT_CC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                              O_cc_exists         OUT      BOOLEAN,
                              I_cost_change       IN       COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE,
                              I_status            IN       COST_SUSP_SUP_HEAD.STATUS%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------------------------------
-- Function Name: GET_COST_EVENT_TYPE_DESC
-- Purpose      : The function is responsible to check if a cost event already exists that successfully 
--                completed for the cost change that affected the future cost.
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_COST_EVENT_TYPE_DESC (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_cc_type_desc      OUT      COST_EVENT_RUN_TYPE_CONFIG.EVENT_DESC%TYPE,
                                   O_event_run_type    OUT      COST_EVENT_RUN_TYPE_CONFIG.EVENT_RUN_TYPE%TYPE,
                                   I_event_type        IN       COST_EVENT_RUN_TYPE_CONFIG.EVENT_TYPE%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_COST_EVENT_PROCESS_ID
-- Purpose      : The function is responsible for the validation of cost event process id from cost_event table.
----------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_EVENT_PROCESS_ID (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                         O_exists            OUT      BOOLEAN,                                         
                                         I_event_id          IN       COST_EVENT.COST_EVENT_PROCESS_ID%TYPE)
   RETURN BOOLEAN;

---------------------------------------------------------------------------------------------------------------------
-- Function Name: VALIDATE_COST_EVENT_THREAD
-- Purpose      : The function is responsible for the validation of thread id from the cost_event_thread table.
----------------------------------------------------------------------------------------------------------------------
FUNCTION VALIDATE_COST_EVENT_THREAD (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                                     O_exists            OUT      BOOLEAN,                                         
                                     I_event             IN       COST_EVENT_THREAD.COST_EVENT_PROCESS_ID%TYPE,
                                     I_thread_id         IN       COST_EVENT_THREAD.THREAD_ID%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_LOCS
-- Purpose      : The function will return all locations attached to the item specifed as input parameter.
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                        IO_item_loc_tbl     IN OUT   OBJ_ITEMLOC_TBL,                                         
                        I_item              IN       ITEM_MASTER.ITEM%TYPE,
                        I_default_down_ind  IN       VARCHAR2 DEFAULT 'N')
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name: GET_ITEM_LOCS_WRP
-- Purpose      : The function will return all locations attached to the item specifed as input parameter.
--                A wrapper for GET_ITEM_LOCS to allow for wrapper generation through jPublisher.
----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_ITEM_LOCS_WRP (O_error_message     IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
							IO_item_loc_tbl     IN OUT   OBJ_ITEMLOC_TBL,                                         
							I_item              IN       ITEM_MASTER.ITEM%TYPE,
							I_default_down_ind  IN       VARCHAR2 DEFAULT 'N')
   RETURN INTEGER;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_IED_TEMP(O_error_message IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                         I_item          IN     ITEM_EXP_DETAIL.ITEM%TYPE,
                         I_supplier      IN     ITEM_EXP_DETAIL.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_EXP_ROWS(O_error_message IN OUT RTK_ERRORS.RTK_KEY%TYPE,
                      O_elc_changes   IN OUT OBJ_ELC_COST_EVENT_TBL,
                      I_item          IN     ITEM_EXP_DETAIL.ITEM%TYPE,
                      I_supplier      IN     ITEM_EXP_DETAIL.SUPPLIER%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
FUNCTION GET_HTS_ROWS(O_error_message       IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                      O_elc_changes         IN OUT   OBJ_ELC_COST_EVENT_TBL,
                      I_item                IN       ITEM_HTS_ASSESS.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
FUNCTION INSERT_IHA_TEMP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_item            IN       ITEM_HTS_ASSESS.ITEM%TYPE)
   RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------------------------
-- Function Name: PROCESS_WRP
-- Purpose      : This is a wrapper function for PROCESS that passes a database type object instead of a PL/SQL type
--                as an input parameter to be called from Java wrappers.
----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_WRP(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                     I_process_tbl     IN       WRP_COST_EVENT_DET_TBL)
   RETURN INTEGER;
---------------------------------------------------------------------------------------------------------------------   
END COST_EVENT_LOG_SQL;
/