#! /bin/ksh
#-------------------------------------------------------------------------------------
# File:  poindbatch.ksh
# Desc:  This shell script will be used to load content xml data into s9t_folder table.
#        This script will be called from ld_iindfiles.ksh passing input file as parameter.
#-------------------------------------------------------------------------------------
# Common functions library and environment variables
 . ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='poindbatch.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

# File locations
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
ERRINDFILE=err.ind
FATAL=255
NON_FATAL=1
OK=0

#-------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#-------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: $pgmName <connect string> <input file> <template name> <destination>

      <connect string>       Username/password@db.
      
      <input file>           Input file to be uploaded.
      
      <template name>        Template Name.
      
      <Destination>          Optional Input Parameter. Valid Values are RMS and STG. If not Entered Will be defaulted to STG."
}

#-------------------------------------------------------------------------
# Function Name: CHECK_FILE_EXIST
# Purpose      : Used to Check if Input xml file exist in the specified location.
#-------------------------------------------------------------------------
function CHECK_FILE_EXIST
{
   # Check if Input xml file exist
   if [[ -f $inputFile && -r $inputFile ]]; then
     :
   else
      echo "ERROR : Input file does not exist" "CHECK_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Input file does not exist" "CHECK_FILE_EXIST" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   return ${OK}
}

#-------------------------------------------------------------------------
# Function Name: CHECK_FILE_EXTENSION
# Purpose      : Used to validate the Input filename. checks whether filename is having the Extension ".xml".
#-------------------------------------------------------------------------
function CHECK_FILE_EXTENSION
{
   inputFile1=$fileName
   fileType=`echo ${inputFile1} | cut -d "." -f2`
   # Check if input file name has extension ".xml" in it.
   xmlExt="XML"
   if [[ `echo $fileType | tr [a-z] [A-Z]` -ne `echo $xmlExt | tr [a-z] [A-Z]` ]]; then
      echo "ERROR : Invalid file Extension" "CHECK_FILE_EXTENSION" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Invalid file Extension" "CHECK_FILE_EXTENSION" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: CHECK_MAX_FILE_SIZE_UPLOAD
# Purpose      : Used to validate the File size.
#-------------------------------------------------------------------------
function CHECK_MAX_FILE_SIZE_UPLOAD
{
 maxFileSize=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select max_file_size_for_upld from po_induct_config;
   exit;
   EOF`
   
   if [[ $? -ne ${OK} ]]; then
      echo "ERROR : Program failed to get Max file size." "CHECK_MAX_FILE_SIZE_UPLOAD" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Program failed to get Max file size." "CHECK_MAX_FILE_SIZE_UPLOAD" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   
   fileSize=$(stat -c%s "$fileName")
   
   if [[ $fileSize -gt $maxFileSize ]]; then
      echo "ERROR : The file exceeds the maximum file size for upload." "CHECK_MAX_FILE_SIZE_UPLOAD" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "The file exceeds the maximum file size for upload." "CHECK_MAX_FILE_SIZE_UPLOAD" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

    return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: CHECK_TEMPLATE
# Purpose      : Used for calling S9T_PKG.CHECK_TEMPLATE.
#-------------------------------------------------------------------------
function CHECK_TEMPLATE
{
sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;
      WHENEVER SQLERROR EXIT ${FATAL}
      DECLARE         
         L_found BOOLEAN;
         L_error_message VARCHAR2(250);
         L_return BOOLEAN;
      BEGIN
         L_return := S9T_PKG.CHECK_TEMPLATE(L_error_message,
                                            '$templateName',
                                            L_found);
         
         if L_found then
            dbms_output.put_line('TRUE');
         else
            dbms_output.put_line('FALSE');
         end if;
      END;
      /
      "  | sqlplus -s ${connectStr}`

   if [[ ${sqlReturn} != "TRUE" ]]; then
      echo "ERROR : Invalid Template Name" "CHECK_TEMPLATE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Invalid Template Name" "CHECK_TEMPLATE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

    return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: CHECK_DESTINATION
# Purpose      : Used to validate the input value of destination.
#-------------------------------------------------------------------------
function CHECK_DESTINATION
{
   if [ -z ${destination}  ]; then
      destination='STG'
   elif [ ${destination} = "RMS" ] || [ ${destination} = "STG" ]; then
      :
   else
      echo "ERROR : Invalid input for destination" "CHECK_DESTINATION" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Invalid input for destination" "CHECK_DESTINATION" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
   return ${OK}
}

function GET_USER_LANG
{
   userLang=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select get_user_lang from dual;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      echo "ERROR : Program failed to get user language." "GET_USER_LANG" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Program failed to get user language." "GET_USER_LANG" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
  return ${OK}
}

function GET_DB_CURR_DATETIME
{
   dateTime=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select sysdate from dual;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      echo "ERROR : Program failed to get Current DB Datetime." "GET_DB_CURR_DATETIME" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Program failed to get Current DB Datetime." "GET_DB_CURR_DATETIME" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi
  return ${OK}
}

function GET_NEXT_SEQUENCE
{
   newSeq=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select s9t_folder_seq.nextval from dual;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      echo "ERROR : Program failed to get the next sequence for s9t_folder." "GET_NEXT_SEQUENCE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Program failed to get the next sequence for s9t_folder." "GET_NEXT_SEQUENCE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned sequence
   if [ -z ${newSeq} ]; then
      echo "ERROR : Unable to retrieve the sequence value from s9t_folder_seq." "GET_NEXT_SEQUENCE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Unable to retrieve the sequence value from s9t_folder_seq." "GET_NEXT_SEQUENCE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "Next sequence value retrieved." "GET_NEXT_SEQUENCE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi
  return ${OK}
}

function CREATE_CTL_FILE
{
   # Create ctl file
   cat > "${MMHOME}/log/${fileName}.ctl" <<EOF
   LOAD DATA
   infile "${MMHOME}/log/${fileName}.data"
   APPEND
   INTO TABLE S9T_FOLDER
   fields terminated by "," optionally enclosed by '"'
   TRAILING NULLCOLS
   ( file_id,
     file_name,
     template_key,
     user_lang,
     status,
     action,
     action_date,
     create_datetime,
     file_path filler char(4000),
     CONTENT_XML LOBFILE(file_path) TERMINATED BY EOF)
EOF

   if [[ $? -ne ${OK} ]]; then
      echo "ERROR : Program failed while creating control file." "CREATE_CTL_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "Program failed while creating control file." "CREATE_CTL_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

  return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: UPLOAD_PROCESS_TRACKER
# Purpose      : Used for Uploading the Process tracker.
#-------------------------------------------------------------------------
function UPLOAD_PROCESS_TRACKER
{
sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;
      WHENEVER SQLERROR EXIT ${FATAL}
      DECLARE         
         L_rms_async_id    RMS_ASYNC_STATUS.RMS_ASYNC_ID%TYPE;
         L_error_message   RTK_ERRORS.RTK_TEXT%TYPE;
         L_return          BOOLEAN;
         L_process_id      SVC_PROCESS_TRACKER.PROCESS_ID%TYPE;
         L_edits_allowed   BOOLEAN;
      BEGIN
         L_return := CORESVC_PO.CHECK_USER_PO_PRIVS(L_error_message,
                                                    L_edits_allowed,
                                                    NULL,
                                                    'POINDBATCH',
                                                    'U');
         if L_return then
            L_rms_async_id := RMS_ASYNC_ID_SEQ.nextval;
            L_process_id := CORESVC_ITEM_PSEQ.nextval();
            
            L_return := PO_INDUCT_SQL.INIT_PROCESS(L_error_message,
                                                   L_process_id,
                                                   '$fileName',
                                                   '$templateName',
                                                   'U',
                                                   'S9T',
                                                   '$destination',
                                                   L_rms_async_id,
                                                   ${newSeq},
                                                   '${inputFile}');
         end if;

         if L_return then
            L_return := RMS_ASYNC_PROCESS_SQL.ENQUEUE_PO_INDUCT(L_error_message,
                                                                L_rms_async_id);
            if L_return then
               dbms_output.put_line('TRUE');
            else
               dbms_output.put_line(L_error_message);
            end if;
         else
            dbms_output.put_line(L_error_message);
         end if;
         
         COMMIT;

      END;
      /
      "  | sqlplus -s ${connectStr}`

   if [[ ${sqlReturn} != "TRUE" ]]; then
      echo "ERROR : ${sqlReturn}" "UPLOAD_PROCESS_TRACKER" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      LOG_ERROR "${sqlReturn}" "UPLOAD_PROCESS_TRACKER" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

    return ${OK}
}
#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------
# Test for the number of input arguments
if [ $# -lt 3 ]
then
   USAGE
   exit ${NON_FATAL}
fi
#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
fileName=`basename $2`
templateName=$3
destination=$4

LOG_MESSAGE "${pgmName}.${pgmExt} started by ${USER} for input file ${inputFile} Template Name ${templateName} and destination ${destination}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}

CHECK_FILE_EXIST
CHECK_FILE_EXTENSION
CHECK_MAX_FILE_SIZE_UPLOAD
CHECK_TEMPLATE
CHECK_DESTINATION


GET_USER_LANG
GET_DB_CURR_DATETIME
GET_NEXT_SEQUENCE

echo "${newSeq},${fileName},${templateName},${userLang},"UPLOADED","UPLOAD",${dateTime},${dateTime}, \"${inputFile}\"" > "${MMHOME}/log/${fileName}.data"
CREATE_CTL_FILE
CTLFILE="${MMHOME}/log/$fileName.ctl"
DATAFILE="${MMHOME}/log/$fileName.data"

dtStamp=`date +"%G%m%d%H%M%S"`
sqlldrFile=${fileName##*/}
sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
sqlldr ${connectStr} silent=feedback,header \
   control=${CTLFILE} \
   log=${sqlldrLog} \
   data=${DATAFILE} \
   bad=${MMHOME}/log/$sqlldrFile.bad \
   discard=${MMHOME}/log/$sqlldrFile.dsc 
sqlldr_status=$?

UPLOAD_PROCESS_TRACKER

# Check execution status
if [[ $sqlldr_status != ${OK} ]]; then
   echo "ERROR : ${fileName##*/}; Error while loading file." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   LOG_MESSAGE "${fileName##*/}; Error while loading file." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
else
   echo "SUCCESS : ${fileName##*/}; Completed file loading." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   LOG_MESSAGE "${fileName##*/}; Completed file loading." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
    rm -f $CTLFILE
    rm -f $DATAFILE
    rm -f $sqlldrLog
fi

exit ${OK}