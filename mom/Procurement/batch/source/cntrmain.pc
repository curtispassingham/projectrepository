
#include "retek.h"
#include "std_rest.h"

#define  LEN_CONTRACT        6
#define  LEN_HISTORY_MONTH   2

#define  NULL_CONTRACT       7
#define  NULL_HISTORY_MONTH  3

char  ps_vdate[NULL_DATE];
char  ps_delete_months[NULL_HISTORY_MONTH];

long SQLCODE;
char   ora_restart_new_string[MAX_START_STRING_LEN+1];
char   ora_restart_start_string[MAX_START_STRING_LEN+1];
char   ora_restart_driver_name[MAX_DRIVER_NAME_LEN+1];
char   ora_restart_thread_val[NULL_THREAD];
char   or_restart_num_threads[NULL_THREAD];
int    oi_no_records;

EXEC SQL INCLUDE SQLCA.H;

char   restart_application_image_array[1][255];
char   restart_start_array[1][255];

struct fetch_contract_array
{
   char (*s_contract_no)    [NULL_CONTRACT];
   char (*s_rowid)          [NULL_ROWID];
} fetch_contract;


/***********************
***       main       ***
************************/

int main(int argc, char* argv[])
{
   char* function = "main";
   char  ls_log_message[NULL_ERROR_MESSAGE];
   int   li_init_results;
   strcpy(PROGRAM,"cntrmain");
   strcpy(g_s_restart_name,"cntrmain");

   if(argc < 2)
   {
      fprintf(stderr,"Usage: %s userid/passwd\n",argv[0]);
      return(FAILED);
   }

   if (LOGON(argc, argv) < 0)
      return(FAILED);

   if ((li_init_results = init()) < 0)
      gi_error_flag = 2;

   if (li_init_results != NO_THREAD_AVAILABLE)
   {
      if (li_init_results == OK)
      {
         if (process() < 0)
            gi_error_flag = 1;
      }

      if (final() < 0)
      {
         if (gi_error_flag == 0)
            gi_error_flag = 3;
      }
   }

   if (gi_error_flag == 2)
   {
      sprintf(ls_log_message, "Aborted in init.");
      LOG_MESSAGE(ls_log_message);
      return(FAILED);
   }
   else if (gi_error_flag == 1)
   {
      sprintf(ls_log_message, "Thread %s - Aborted in process.", g_s_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
      return(FAILED);
   }
   else if (gi_error_flag == 3)
   {
      sprintf(ls_log_message, "Thread %s - Aborted in final", g_s_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
      return(FAILED);
   }
   else if (li_init_results == NO_THREAD_AVAILABLE)
   {
      sprintf(ls_log_message, "Terminated - no threads available");
      LOG_MESSAGE(ls_log_message);
      return(NO_THREADS);
   }
   else
   {
      sprintf(ls_log_message, "Thread %s = Terminated Successfully", g_s_restart_thread_val);
      LOG_MESSAGE(ls_log_message);
   }

   return(SUCCEEDED);
}  /* End of main() */

int size_arrays()
{
   char* function="size_arrays";
   int no_mem=0;

   if ((fetch_contract.s_contract_no=
      (char(*)[NULL_CONTRACT])calloc(g_i_restart_max_counter,NULL_CONTRACT))
      ==NULL)
      no_mem=1;

   if ((fetch_contract.s_rowid=
      (char(*)[NULL_ROWID])calloc(g_i_restart_max_counter,NULL_ROWID))
      ==NULL)
      no_mem=1;

   if(no_mem)
   {
      sprintf(err_data,"Unable to allocate memory for arrays");
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(-1);
   }

   return(0);
} /* end size_arrays() */

/***********************
***  init function  ***
************************/

int init()
{
   char *function = "init";
   int restart_init_results;

   EXEC SQL DECLARE c_dates CURSOR FOR
         SELECT TO_CHAR(period.vdate,'YYYYMMDDHH24MISS'),
                purge_config_options.order_history_months
           FROM period,
                purge_config_options;

   restart_init_results=
      restart_init(restart_start_array,
                   restart_application_image_array,
                   or_restart_num_threads,
                   restart_driver_name);
   if(restart_init_results!=0)
      return(restart_init_results);

   if (g_i_restart_max_counter > MAX_ORACLE_ARRAY_SIZE)
      g_i_restart_max_counter = MAX_ORACLE_ARRAY_SIZE;

   EXEC SQL OPEN c_dates;
   if(SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Open c_dates.");
      strcpy(table, "period, purge_config_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   EXEC SQL FETCH c_dates INTO :ps_vdate,
                               :ps_delete_months;
   if(SQL_ERROR_FOUND || NO_DATA_FOUND)
   {
      sprintf(err_data, "Fetch c_dates.");
      strcpy(table, "period, purge_config_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   EXEC SQL CLOSE c_dates;
   if(SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Close c_dates.");
      strcpy(table, "period, purge_config_options");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   if(size_arrays()<0)
      return(-1);

   return(0);
} /* end init() */


/***********************
*** process function ***
************************/

int process()
{
   char *function = "process";

   if(delete_contracts()<0)
      return(-1);

   if(reset_inactive()<0)
      return(-1);

   return(0);
} /* end process() */

   EXEC SQL DECLARE c_ch CURSOR FOR
      SELECT ch.contract_no,
             ROWIDTOCHAR(ch.rowid)
        FROM contract_header ch
       WHERE MONTHS_BETWEEN(TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS'),ch.status_date)>=
                            :ps_delete_months
         AND (ch.status IN('W','S','C','D','X')
                 AND NOT EXISTS(
                    SELECT 'Y'
                      FROM ordhead oh
                     WHERE oh.contract_no = ch.contract_no));

int delete_contracts()
{
   char *function="delete_contracts";
   int li_break=0;
   unsigned int li_num_records;
   int li_total_recs_processed=0;

   EXEC SQL OPEN c_ch;
   if(SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Open c_ch.");
      strcpy(table, "contract_header");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }

   while(1)
   {
      EXEC SQL FOR :g_i_restart_max_counter
                         FETCH c_ch INTO :fetch_contract.s_contract_no,
                                         :fetch_contract.s_rowid;
      if(SQL_ERROR_FOUND)
      {
         sprintf(err_data, "Fetch c_ch.");
         strcpy(table, "contract_header");
         WRITE_ERROR(SQLCODE,function,table,err_data);
         return(-1);
      }
      else if(NO_DATA_FOUND)
      {
         li_break=1;
      }
      li_num_records=NUM_RECORDS_PROCESSED-li_total_recs_processed;
      li_total_recs_processed=NUM_RECORDS_PROCESSED;
      if(li_num_records>0)
      {
         EXEC SQL FOR :li_num_records
            DELETE FROM contract_cost cc
                  WHERE cc.contract_no=:fetch_contract.s_contract_no;

         if(SQL_ERROR_FOUND)
         {
            sprintf(err_data, "Delete.");
            strcpy(table, "contract_cost");
            WRITE_ERROR(SQLCODE,function,table,err_data);
            return(-1);
         }

         EXEC SQL FOR :li_num_records
            DELETE FROM contract_detail cd
                  WHERE cd.contract_no = :fetch_contract.s_contract_no;
         if(SQL_ERROR_FOUND)
         {
            sprintf(err_data, "Delete.");
            strcpy(table, "contract_detail");
            WRITE_ERROR(SQLCODE,function,table,err_data);
            return(-1);
         }
         EXEC SQL FOR :li_num_records
           DELETE FROM contract_header ch
                 WHERE ch.rowid = CHARTOROWID(:fetch_contract.s_rowid);
         if(SQL_ERROR_FOUND)
         {
            sprintf(err_data, "Delete contract_header.");
            strcpy(table, "contract_header");
            WRITE_ERROR(SQLCODE,function,table,err_data);
            return(-1);
         }
         /* force a commit */
         strcpy(restart_cur_string," ");
         strcpy(restart_new_string,"x");
         g_l_restart_current_count=g_i_restart_max_counter+1;
         if(restart_commit(restart_cur_string,restart_new_string,
                           restart_application_image)<0)
            return(-1);
      }

      if (li_break)
          break;
   }

   return(0);
} /* end delete_contracts() */

/**********************************
*** status_maintenance function ***
**********************************/

int reset_inactive(void)
{
   char *function = "reset_inactive";

   EXEC SQL UPDATE contract_header ch
        SET ch.status='C',
            ch.complete_date=TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS'),
            ch.status_date=TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS')
      WHERE ch.contract_type in ('A','B')
        AND ch.status='A'
        AND TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS')>=ch.end_date;

   if(SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Update contract_header (types A/B).");
      strcpy(table, "contract_header");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }
   EXEC SQL UPDATE contract_header ch
        SET ch.status='R',
            ch.status_date=TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS'),
            ch.review_date=TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS'),
            ch.review_id='AUTOMATIC'
      WHERE ch.contract_type in ('C','D')
        AND ch.status='A'
        AND TO_DATE(:ps_vdate,'YYYYMMDDHH24MISS')>=ch.end_date;

   if(SQL_ERROR_FOUND)
   {
      sprintf(err_data, "Update contract_header (types C/D).");
      strcpy(table, "contract_header");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(-1);
   }
   return(0);
} /* end reset_inactive() */

/***********************
***  final function  ***
************************/
int final()
{
   char * function="final";

   return(restart_close(restart_err_msg));
} /* end final() */
