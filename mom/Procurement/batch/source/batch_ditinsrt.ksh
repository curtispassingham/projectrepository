#! /bin/ksh
#-------------------------------------------------------------------------
#  File:  batch_ditinsrt.ksh
#
#  Desc:  UNIX shell script to run the ditinsrt program multi-threaded.
#-------------------------------------------------------------------------

# Initialize number of parallel threads
SLOTS=0

USAGE="Usage: `basename $0` [-p <# parallel threads>] <connect>\n
<# parallel threads> is the number of ditinsrt threads to run in parallel.\n
The default is the value on RESTART_CONTROL.NUM_THREADS.\n
"

# Parse the command line
while getopts :p: CMD
   do
      case $CMD in
         p)  SLOTS=$OPTARG;;
         *)  echo $0: Unknown option $OPTARG
             echo $USAGE
             exit 1;;
      esac
   done

shift $OPTIND-1

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1

# Test for the number of threads
# If the user does not designate the number of parallel threads, then default to RESTART_CONTROL.NUM_THREADS
if [ $SLOTS -eq 0 -o $SLOTS -lt 1 ]
   then
      SLOTS=`$ORACLE_HOME/bin/sqlplus -s $CONNECT  <<EOF
      set pause off
      set echo off
      set heading off
      set feedback off
      set verify off
      set pages 0
      select num_threads
        from restart_control
       where program_name = 'ditinsrt';
      EOF`
fi

# Adjust for any remaining arguments to be passed to ditinsrt
shift 1

# Set filename to contain this runs supplier list
SUPPLIERS=ditinsrt_suppliers.$$

# Set filename to flag any failed executions
failed=ditinsrt.$$
[ -f $failed ] && rm $failed

# If this script is killed, cleanup
trap "kill -15 0; rm -f $failed -f $SUPPLIERS; exit 15" 1 2 3 15

# Get the list of suppliers to be processed
[ -f $SUPPLIERS ] && rm $SUPPLIERS
$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$SUPPLIERS
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
set tab off

COLUMN reccount noprint

SELECT dh.supplier,       
       count(*) reccount  
  FROM deal_queue dq,       
       deal_head dh
 WHERE dh.supplier is NOT NULL
   AND dh.deal_id = dq.deal_id
 GROUP BY dh.supplier
 ORDER BY reccount DESC;

EOF

# Check for any Oracle errors from the SQLPLUS process
[ `grep "^ORA-" $SUPPLIERS | wc -l` -gt 0 ] && exit 1

# Run program ditinsrt in parallel -  each thread processing a supplier
cat $SUPPLIERS | while read supplier
 do
    if [ `jobs | wc -l` -lt $SLOTS ]
    then
       (
       ditinsrt $CONNECT S $supplier || touch $failed;
       ) &
    else
       # Loop until a thread becomes available
       while [ `jobs | wc -l` -ge $SLOTS ]
       do
          : # Null command
       done
       (
       ditinsrt $CONNECT S $supplier || touch $failed;
       ) &
    fi
 done

# Wait for all of the threads to complete
wait

# Remove the list of suppliers file
rm $SUPPLIERS

# Check for the existence of a failed file from any of the threads
# and determine exit status
if [ -f $failed ]
then
   rm $failed
   exit 1
fi

# Set filename to contain this runs partner list
PARTNERS=ditinsrt_partners.$$

# Reset the number of threads to 1 for partner level deals...
SLOTS=1

# Set filename to flag any failed executions
failed=ditinsrt.$$
[ -f $failed ] && rm $failed

# If this script is killed, cleanup
trap "kill -15 0; rm -f $failed -f $PARTNERS; exit 15" 1 2 3 15

# Get the list of partners to be processed
[ -f $PARTNERS ] && rm $PARTNERS
$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF >$PARTNERS
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
set tab off

SELECT distinct dh.partner_id
  FROM deal_queue dq,
       deal_head dh
 WHERE dh.partner_id is NOT NULL
   AND dh.deal_id = dq.deal_id;

EOF

# Check for any Oracle errors from the SQLPLUS process
[ `grep "^ORA-" $PARTNERS | wc -l` -gt 0 ] && exit 1

# Run program ditinsrt single threaded.
cat $PARTNERS | while read partner
 do
    if [ `jobs | wc -l` -lt $SLOTS ]
    then
       (
       ditinsrt $CONNECT P $partner || touch $failed;
       ) &
    else
       # Loop until a thread becomes available
       while [ `jobs | wc -l` -ge $SLOTS ]
       do
          : # Null command
       done
       (
       ditinsrt $CONNECT P $partner || touch $failed;
       ) &
    fi
 done

# Wait for all of the threads to complete
wait

# Remove the list of partners file
rm $PARTNERS

# Check for the existence of a failed file from any of the threads
# and determine exit status
if [ -f $failed ]
then
   rm $failed
   exit 1
else
   exit 0
fi
