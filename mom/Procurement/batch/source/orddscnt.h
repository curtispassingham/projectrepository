/* PROGRAM: orddscnt.pc */
/* Deal--cost calculations: Order Discount */

#ifndef ORDDSCNT_H
#define ORDDSCNT_H

#include "dealordlib.h"

init_parameter parameter[NUM_INIT_PARAMETERS] =
{
   "commit_max_ctr",       "uint",    "",
   "thread_val",           "string",  "",
   "num_threads",          "string",  "",
   "lock_wait_time",       "uint",    "",
   "retry_max_ctr",        "uint",    ""
};

/* commit max counter used for array fetch */
unsigned int pi_commit_max_ctr;
unsigned int pi_lock_wait_time;
unsigned int pi_retry_max_ctr;
unsigned int pi_locked_records = 0;

/* Variables used as restart string */
char ps_restart_num_threads[NULL_THREAD];
char ps_restart_thread_val[NULL_THREAD];

int main(int argc, char *argv[]);

#endif
