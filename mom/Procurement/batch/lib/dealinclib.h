/* PROGRAM: dealinclib.h */

#ifndef LIBDEALINC_H
#define LIBDEALINC_H

#include <retek_2.h>
#include <oci.h>
#include <time.h>

#ifdef DEALINCLIB_MAIN_MODULE

/* declare public variables */
char ps_primary_currency_code[NULL_CURRENCY_CODE]="";
/*****************************************************/
/*                constants                          */
/*****************************************************/
const char *DUMMY_DEAL="-1";
const short ALL_ORDERS=-1;
const char *ALL_DEAL_COMPS="-1";
const short NULL_IND_VALUE=-1;
const short SAME=0;
const char *ORDERING="O";
const char *RECEIVING="R";
const char *ACTUAL="A";
const char *FORECAST="F";
const char *QUANTITY="Q";
const char *AMOUNT="A";
const char *PERCENT="P";
const char *LINEAR="L";
const char *SCALAR="S";
const char *PRORATED="P";
const char *VEND_FND_MRKDWN="VFM";
const char *VEND_FND_PRMO="VFP";

#else
extern char ps_primary_currency_code[NULL_CURRENCY_CODE];
/*****************************************************/
/*                constants                          */
/*****************************************************/
extern const short ALL_ORDERS;
extern const short ALL_DEAL_COMPS;
extern const short NULL_IND_VALUE;
extern const short SAME;
extern const char *ORDERING;
extern const char *RECEIVING;
extern const char *ACTUAL;
extern const char *FORECAST;
extern const char *QUANTITY;
extern const char *AMOUNT;
extern const char *PERCENT;
extern const char *LINEAR;
extern const char *SCALAR;
extern const char *PRORATED;
#endif

/*****************************************************/
/*                Rounding Precision                 */
/*****************************************************/
#define PRECISION 4
/*****************************************************/
/*                    lengths                        */
/*****************************************************/
#define LEN_BILL_BACK_METHOD             6
#define LEN_BILLING_TYPE                 6
#define LEN_CALC_TYPE                    6
#define LEN_LIMIT_TYPE                   6
#define LEN_VALUE_TYPE                   6
#define LEN_DEAL_APPL_TIMING             6
#define LEN_DEAL_INCOME_CALCULATION      6
#define LEN_DEAL_DETAIL_ID              10
#define LEN_DEAL_ID                     10
#define LEN_DAI_ID                      10
#define LEN_DEAL_REPORTING_LEVEL         6
#define LEN_PERCENTAGE                   6
#define LEN_REBATE_TYPE                  6
#define LEN_INCOME_CALC                  6
#define LEN_PGM_NAME                     100

#define NULL_BILL_BACK_METHOD           (LEN_BILL_BACK_METHOD+1)
#define NULL_BILLING_TYPE               (LEN_BILLING_TYPE+1)
#define NULL_CALC_TYPE                  (LEN_CALC_TYPE+1)
#define NULL_LIMIT_TYPE                 (LEN_LIMIT_TYPE+1)
#define NULL_VALUE_TYPE                 (LEN_VALUE_TYPE+1)
#define NULL_DEAL_APPL_TIMING           (LEN_DEAL_APPL_TIMING+1)
#define NULL_DEAL_INCOME_CALCULATION    (LEN_DEAL_INCOME_CALCULATION + 1)
#define NULL_DEAL_DETAIL_ID             (LEN_DEAL_DETAIL_ID+1)
#define NULL_DEAL_ID                    (LEN_DEAL_ID+1)
#define NULL_DAI_ID                     (LEN_DAI_ID+1)
#define NULL_DEAL_REPORTING_LEVEL       (LEN_DEAL_REPORTING_LEVEL+1)
#define NULL_PERCENTAGE                 (LEN_PERCENTAGE+1)
#define NULL_REBATE_TYPE                (LEN_REBATE_TYPE+1)
#define NULL_INCOME_CALC                (LEN_INCOME_CALC+1)
#define NULL_PGM_NAME                   (LEN_PGM_NAME+1)
#define NULL_LOCATION   11
/*****************************************************/
/*             structure decs                       */
/*****************************************************/
struct deal_threshold
{
   double ad_lower_limit;
   double ad_upper_limit;
   char   as_total_ind[NULL_IND];
   double ad_value;
};

struct deal_component_actuals
{
   char   deal_id[NULL_DEAL_ID];
   char   deal_detail_id[NULL_DEAL_DETAIL_ID];
   char   reporting_date[NULL_DATE];
   double component_actuals;
   double component_actuals_unit;
   double component_actuals_revenue;
   char   order_no[NULL_ORDER_NO];
};

struct turnover_itemloc_td
{
   char   deal_id[NULL_DEAL_ID];
   char   deal_detail_id[NULL_DEAL_DETAIL_ID];
   char   item[NULL_ITEM];
   char   loc_type[NULL_LOC_TYPE];
   char   location[NULL_LOCATION];
   char   order_no[NULL_ORDER_NO];
   char   reporting_date[NULL_DATE];
   double total_actual_turnover_itemloc_td;
};

struct daf_totals
{
   double ad_baseline_turnover;
   double ad_budget_turnover;
   double ad_budget_income;
   double ad_actual_forecast_turnover;
   double ad_actual_forecast_income;
   double ad_actual_forecast_trend_turnover;
   double ad_actual_forecast_trend_income;
   double ad_actual_income;
};

struct deal_head
{
   char   as_rowid[NULL_ROWID];
   char   as_currency_code[NULL_CURRENCY_CODE];
   char   as_billing_type[NULL_BILLING_TYPE];
   char   as_deal_appl_timing[NULL_DEAL_APPL_TIMING];
   char   as_threshold_limit_type[NULL_LIMIT_TYPE];
   char   as_threshold_limit_uom[NULL_UOM];
   char   as_rebate_ind[NULL_IND];
   char   as_rebate_calc_type[NULL_CALC_TYPE];
   char   as_growth_rebate_ind[NULL_IND];
   char   as_rebate_purch_sales_ind[NULL_IND];
   char   as_bill_back_method[NULL_BILL_BACK_METHOD];
   char   as_deal_income_calculation[NULL_DEAL_INCOME_CALCULATION];
   char   as_stock_ledger_ind[NULL_IND];
   char   as_include_vat_ind[NULL_IND];
   char   as_status[NULL_STATUS];
   double ad_growth_rate_to_date;
   double ad_turnover_to_date;
   double ad_actual_monies_earned_to_date;

   /* Null Indicators */
   short i_currency_code;
   short i_deal_appl_timing;
   short i_threshold_limit_type;
   short i_threshold_limit_uom;
   short i_rebate_calc_type;
   short i_rebate_purch_sales_ind;
   short i_bill_back_method;
   short i_deal_income_calculation;
};

struct deal_detail
{
   char   as_rowid[NULL_ROWID];
   char   as_calc_to_zero_ind[NULL_ROWID];
   char   as_threshold_value_type[NULL_LIMIT_TYPE];
   double ad_total_forecast_units;
   double ad_total_forecast_revenue;
   double ad_total_budget_turnover;
   double ad_total_actual_forecast_turnover;
   double ad_total_baseline_growth_budget;
   double ad_total_baseline_growth_act_for;
   double ad_growth_rate_to_date;

   /* Null Indicators */
   short i_calc_to_zero_ind;
   short i_threshold_value_type;
};

struct all_periods_turnover
{
   double ad_actual_forecast_turnover;
   char   as_actual_forecast_ind[NULL_IND];
   char   as_reporting_date[NULL_DATE];
};

struct daf_details
{
   char   deal_id[NULL_DEAL_ID];
   char   deal_detail_id[NULL_DEAL_DETAIL_ID];
   char   reporting_date[NULL_DATE];
   double actual_turnover;
   double actual_income;
};

/* functions prototype list */
/*------------------------------------------- */
/*------------------------------------------- */
int external_apply_deal_perf(
   OCIExtProcContext *with_context,
   char *ios_error_message,
   char *is_deal_id,
   char *is_deal_detail_id,
   char *is_amt_per_unit,
   char *is_old_period_turnover,
   char *is_new_period_turnover,
   char *is_convert_amt_per_unit,
   char *is_update_forecast_unit_amt,
   char *is_update_actual_fixed_totals,
   char *is_update_deal_detail_actual_totals,
   char *is_update_budget_fixed_totals,
   char *is_update_deal_detail_budget_totals,
   char *is_update_total_baseline,
   char *is_update_turnover_trend,
   char *is_forecast_income_calc,
   char *is_deal_to_date_calcs
);
int convert_amt_per_unit(
   char *is_deal_id,
   char *is_deal_detail_id,
   double *od_amt_per_unit
);
int update_forecast_unit_amt(
   char *is_deal_id,
   char *is_deal_detail_id,
   double id_amt_per_unit
);
int actual_income_calc(
   char *is_deal_id,
   char *is_deal_detail_id,
   char *is_threshold_limit_type,
   char *is_threshold_value_type,
   char *is_rebate_calc_type,
   char *is_calc_to_zero_ind,
   char *is_total_actual_fixed_ind,
   char *is_deal_income_calculation,
   char *is_actual_turnover_units,
   char *is_actual_turnover_revenue,
   char *is_act_for_turnover_total,
   char *is_rebate_ind,
   char *is_final_period_ind,
   char *is_reporting_date,
   char *is_order_no,
   char *is_item,
   char *is_loc_type,
   char *is_location,
   double id_curr_forecast_turnover,
   double *od_actual_income
);
int update_actual_fixed_totals(
   char *is_deal_id,
   char *is_deal_detail_id,
   double id_old_period_turnover,
   double id_new_period_turnover
);
int update_deal_detail_actual_totals(
   char *is_deal_id,
   char *is_deal_detail_id
);
int update_budget_fixed_totals(
   char *is_deal_id,
   char *is_deal_detail_id,
   double id_old_period_turnover,
   double id_new_period_turnover
);
int update_deal_detail_budget_totals(
   char *is_deal_id,
   char *is_deal_detail_id
);
int update_total_baseline(
   char *is_deal_id,
   char *is_deal_detail_id
);
int update_turnover_trend(
   char *is_deal_id,
   char *is_deal_detail_id
);
int forecast_income_calc(
   char *is_deal_id,
   char *is_deal_detail_id,
   double id_amt_per_unit
);
int deal_to_date_calcs(char *is_deal_id);
int calculate_income(
   char *is_deal_id,
   char *is_deal_detail_id,
   char *is_threshold_limit_type,
   char *is_threshold_value_type,
   char *is_rebate_calc_type,
   char *is_calc_to_zero_ind,
   char *is_deal_income_calculation,
   char *is_rebate_ind,
   double id_period_threshold_value,
   double id_threshold_value,
   double id_amt_per_unit,
   double id_weighted_amt_per_unit,
   double *od_income
);
int get_primary_currency_code();
void free_deal_thresholds (
   int ii_threshold_count,
   struct deal_threshold *ia_thresholds
);
int get_deal_actuals_forecast_totals(
   const char *is_deal_id,
   const char *is_deal_detail_id,
   const char *is_forecast_ind,
   struct daf_totals *ot_daf_totals
);
int get_deal_details (
   char *is_deal_id,
   char *is_deal_detail_id,
   struct deal_head *ot_deal_head,
   struct deal_detail *ot_deal_detail
);
int get_deal_detail_row(
   char *is_deal_id,
   char *is_deal_detail_id,
   struct deal_detail *ot_deal_detail
);
int get_deal_head_row(
   char *is_deal_id,
   struct deal_head *ot_deal_head
);
int get_deal_thresholds(
   char *is_deal_id,
   char *is_deal_detail_id,
   int *oi_threshold_count,
   struct deal_threshold **oa_thresholds
);
void get_lib_error_message(char *ios_error_message);

void dround(double *id_amount);

#endif
