/* PROGRAM: dealordlib.h */
/* Deal--cost calculations: Order Discount library header */

#ifndef LIBDEALORD_H
#define LIBDEALORD_H

#include <retek_2.h>
#include <oci.h>

#define NUM_INIT_PARAMETERS          5
#define NUM_COMMIT_PARAMETERS        0
#define FATAL                       -1
#define NONFATAL                     1
#define ORA1407                  -1407
#define NO_ORDER_ITEM                2
#define NO_DISCOUNT_ORD_ITEM         3
#define NO_AVAILABLE_QTY            -2
#define NOT_PAID                    -2
#define COULD_NOT_FIND_ITEM         -3
#define NO_DEAL_APPLIED              1
#define INCLUDE_CURRENT_DEAL         0
#define EXCLUDE_CURRENT_DEAL         1
#define BUYER_PACK_DETECTED       7777
#define ORDER_LOCKED                 2

#define ORIG_CUT_OFF_COST_SIZE     100
#define CUT_OFF_COST_INCREMENT      20
#define ORIGINAL_GET_LIST_SIZE      40
#define GET_LIST_SIZE_INCREMENT     40
#define ORIG_GET_LIST_ARR_SIZE   10000
#define GET_LIST_ARR_SIZE_INCREMENT   1000 
#define ORIGINAL_DEAL_SIZE         100
#define DEAL_SIZE_INCREMENT         20
#define ORIGINAL_DISCT_BLD_SIZE  10000
#define DISCT_BLD_SIZE_INCREMENT  1000
#define ORIGINAL_ITEM_DEAL_SIZE  10000
#define ITEM_DEAL_SIZE_INCREMENT  1000
#define ORIGINAL_DISC_ITEM_SIZE  10000
#define DISC_ITEM_SIZE_INCREMENT  1000

#define NULL_DEAL_TYPE_PRIORITY      7
#define NULL_DEAL_AGE_PRIORITY       7
#define NULL_NUMERICAL_TYPE          2
#define NULL_DEAL_ID                11
#define NULL_DEAL_DETAIL_ID         11
#define NULL_NUMERICAL_TYPE          2
#define NULL_APPLICATION_ORDER      11
#define NULL_BILLING_TYPE            7
#define NULL_DEAL_APPL_TIMING        7
#define NULL_DEAL_CLASS              7
#define NULL_THRESHOLD_LIMIT_TYPE    7
#define NULL_THRESHOLD_LIMIT_UOM     5
#define NULL_THRESHOLD_VALUE_TYPE    7
#define NULL_REBATE_PURCH_SALES_IND  7
#define NULL_OTB_CALC_TYPE           2
#define NULL_COST_ZONE_GROUP_ID      5
#define NULL_ORG_LEVEL               7
#define NULL_MERCH_LEVEL             7
#define NULL_QTY_THRESH_GET_TYPE     7

#define NULL_FUNCTION_NAME         255
#define LIB_COMMIT_MAX_CTR           1

#define NULL_PO_EXCLUSIVE_IND        2

/* below number will be used to compare doubles instead of using the == operator. */
/* If the difference between the two doubles is less than below number, they will */
/* be considered equal. This is done in order to avoid situations where the program */
/* is trying to apply 0.00000000123 dollars as a cut off cost. */
#define ZERO_DIFFERENCE        0.00001

/* the datastructures below are to be used only by this library and its modules */

#ifdef DEALORDLIB_MAIN_MODULE
  #define DEALORD_GLOBAL
#else
  #define DEALORD_GLOBAL extern
#endif

DEALORD_GLOBAL char ps_vdate[NULL_DATE];
DEALORD_GLOBAL char ps_prim_currency_code[NULL_CURRENCY_CODE];
DEALORD_GLOBAL char ps_elc_ind[NULL_IND];
DEALORD_GLOBAL char ps_multichannel_ind[NULL_IND];
DEALORD_GLOBAL char ps_order_no[NULL_ORDER_NO];
DEALORD_GLOBAL char ps_order_supplier[NULL_SUPPLIER];
DEALORD_GLOBAL char ps_order_not_before_date[NULL_DATE];
DEALORD_GLOBAL char ps_order_appr_ind[NULL_IND];
DEALORD_GLOBAL char ps_order_status[NULL_STATUS];
DEALORD_GLOBAL char ps_order_currency_code[NULL_CURRENCY_CODE];
DEALORD_GLOBAL char ps_last_calc_date[NULL_DATE];
DEALORD_GLOBAL double pd_order_exchange_rate;
DEALORD_GLOBAL int  pi_num_cost_decimals;
DEALORD_GLOBAL char ps_supp_currency_code[NULL_CURRENCY_CODE];
DEALORD_GLOBAL double pd_upper_limit;
DEALORD_GLOBAL double pd_lower_limit;
DEALORD_GLOBAL double pd_deal_threshold_value;

typedef struct
{
   long size;
   char (*item)[NULL_ITEM];
   char (*location)[NULL_LOC];
   char (*virtual_loc)[NULL_LOC];
   double *unit_cost;
   double *old_unit_cost;
   char (*order_no)[NULL_ORDER_NO];
   int *num_cost_decimals;
} item_cost_struct;

/* used for holding records fetched from the driving cursor */
typedef struct
{
   char (*order_no)[NULL_ORDER_NO];
   char (*recalc_all_ind)[NULL_IND];
   char (*override_manual_ind)[NULL_IND];
   char (*order_appr_ind)[NULL_IND];
   char (*supplier)[NULL_SUPPLIER];
   char (*import_order_ind)[NULL_IND];
   char (*import_country_id)[NULL_COUNTRY_ID];
   short *import_country_id_ind;
   char (*not_before_date)[NULL_DATE];
   short *not_before_date_ind;
   char (*currency_code)[NULL_CURRENCY_CODE];
   char (*status)[NULL_STATUS];
   double *exchange_rate;
   char (*supp_currency_code)[NULL_CURRENCY_CODE];
   char (*bracket_costing_ind)[NULL_IND];
   char (*last_calc_date)[NULL_DATE];
} order_struct;

typedef struct
{
   long *rownum;
   char (*deal_id)[NULL_DEAL_ID];
   char (*deal_detail_id)[NULL_DEAL_DETAIL_ID];
   char (*type)[NULL_NUMERICAL_TYPE];
   char (*currency_code)[NULL_CURRENCY_CODE];
   char (*application_order)[NULL_APPLICATION_ORDER];
   short *application_order_ind;
   char (*billing_type)[NULL_BILLING_TYPE];
   char (*deal_appl_timing)[NULL_DEAL_APPL_TIMING];
   short *deal_appl_timing_ind;
   char (*deal_class)[NULL_DEAL_CLASS];
   short *deal_class_ind;
   char (*threshold_limit_type)[NULL_THRESHOLD_LIMIT_TYPE];
   short *threshold_limit_type_ind;
   char (*threshold_limit_uom)[NULL_THRESHOLD_LIMIT_UOM];
   short *threshold_limit_uom_ind;
   char (*threshold_value_type)[NULL_THRESHOLD_VALUE_TYPE];
   short *threshold_value_type_ind;
   char (*qty_thresh_buy_item)[NULL_ITEM];
   short *qty_thresh_buy_item_ind;
   double *qty_thresh_buy_qty;
   short *qty_thresh_buy_qty_ind;
   char (*qty_thresh_recur_ind)[NULL_IND];
   short *qty_thresh_recur_ind_ind;
   double *qty_thresh_buy_target;
   short *qty_thresh_buy_target_ind;
   char (*qty_thresh_get_item)[NULL_ITEM];
   short *qty_thresh_get_item_ind;
   double *qty_thresh_get_qty;
   short *qty_thresh_get_qty_ind;
   double *qty_thresh_free_item_unit_cost;
   short *qty_thresh_free_item_unit_cost_ind;
   char (*rebate_ind)[NULL_IND];
   char (*rebate_purch_sales_ind)[NULL_REBATE_PURCH_SALES_IND];
   short *rebate_purch_sales_ind_ind;
   char (*create_date)[NULL_DATE];
   double *get_free_discount;
   short *get_free_discount_ind;
   char (*qty_thresh_get_type)[NULL_QTY_THRESH_GET_TYPE];
   short *qty_thresh_get_type_ind;
   double *qty_thresh_get_value;
   short *qty_thresh_get_value_ind;
   char (*txn_discount_ind)[NULL_IND];
   char (*po_exclusive_ind)[NULL_PO_EXCLUSIVE_IND];
} deal_struct;

typedef struct
{
   long count;
   char (*order_no)[NULL_ORDER_NO];
   char (*item)[NULL_ITEM];
   char (*pack_no)[NULL_ITEM];
   char (*origin_country_id)[NULL_COUNTRY_ID];
   char (*division)[NULL_DIVISION];
   char (*group_no)[NULL_GROUP_NO];
   char (*dept)[NULL_DEPT];
   char (*class)[NULL_CLASS];
   char (*subclass)[NULL_SUBCLASS];
   char (*item_parent)[NULL_ITEM];
   char (*item_grandparent)[NULL_ITEM];
   char (*diff_1)[NULL_DIFF_ID];
   char (*diff_2)[NULL_DIFF_ID];
   char (*diff_3)[NULL_DIFF_ID];
   char (*diff_4)[NULL_DIFF_ID];
   char (*chain)[NULL_CHAIN];
   char (*area)[NULL_AREA];
   char (*region)[NULL_REGION];
   char (*district)[NULL_DISTRICT];
   char (*location)[NULL_LOC];
   char (*loc_type)[NULL_LOC_TYPE];
   char (*virtual_loc)[NULL_LOC];
   double *qty_ordered;
   char (*cost_zone_group_id)[NULL_COST_ZONE_GROUP_ID];
   char (*otb_calc_type)[NULL_OTB_CALC_TYPE];
   double *unit_cost;
   double *unit_cost_init;
   double *orig_otb;
   double *new_otb;
   int *exclusive_deal_applied;
   char (*merch_level)[NULL_MERCH_LEVEL];
   char (*org_level)[NULL_ORG_LEVEL];
   char (*costing_loc)[NULL_LOC];
   char (*costing_loc_type)[NULL_LOC_TYPE];
   char (*franchise_ind)[NULL_IND];
} discount_build_struct;

typedef struct
{
   char (*deal_id)[NULL_DEAL_ID];
   char (*deal_detail_id)[NULL_DEAL_DETAIL_ID];
   char (*item)[NULL_ITEM];
   char (*pack_no)[NULL_ITEM];
   char (*location)[NULL_LOC];
   char (*loc_type)[NULL_LOC_TYPE];
   double *unit_cost;
   double *qty_ordered;
   char (*origin_country_id)[NULL_COUNTRY_ID];
   char (*org_level)[NULL_ORG_LEVEL];
   char (*merch_level)[NULL_MERCH_LEVEL];
} item_deal_struct;

typedef struct
{
   long *rownum;
   char (*item)[NULL_ITEM];
   char (*pack_no)[NULL_ITEM];
   double *unit_cost;
   double *qty_ordered;
   char (*origin_country_id)[NULL_COUNTRY_ID];
   char (*location)[NULL_LOC];
   char (*loc_type)[NULL_LOC_TYPE];
   char (*deal_id)[NULL_DEAL_ID];
   char (*deal_detail_id)[NULL_DEAL_DETAIL_ID];
   char (*deal_class)[NULL_DEAL_CLASS];
   char (*qty_thresh_buy_item)[NULL_ITEM];
   double *qty_thresh_buy_qty;
   char (*qty_thresh_get_item)[NULL_ITEM];
   double *qty_thresh_get_qty;
   double *free_item_unit_cost;
   char (*qty_thresh_recur_ind)[NULL_IND];
   int *fixed_amt_flag;
   double *get_free_discount;
   char (*qty_thresh_get_type)[NULL_QTY_THRESH_GET_TYPE];
   double *qty_thresh_get_value;
} disc_item_struct;

typedef struct
{
     /* Because there will never be more than NULL_GET_LIST_SIZE deals
        applied on 1 item, count can be an int and we can statically 
        size details.  */
   long capacity;
   long count;

   long   *rownum;
   char  (*deal_id)[NULL_DEAL_ID];
   char  (*deal_detail_id)[NULL_DEAL_DETAIL_ID];
   double *invc_unit_cost;
   double *pror_unit_cost;
   double *deal_qty;
   double *loc_qty;
   double *pror_ordloc_unit_cost;
} get_list;

typedef struct
{
   long capacity;
   long list_count;

   char (*item)[NULL_ITEM];
   char (*location)[NULL_LOC];
   short *used_as_buy_item_ind;
   get_list (*get_list_vals);
} get_list_array;

typedef struct
{
   long capacity;
   long    count;

   char  (*get_item)[NULL_ITEM];
   char  (*deal_id)[NULL_DEAL_ID];
   char  (*paid_ind)[NULL_IND];
   long   *rownum;
} cut_off_node;

typedef struct 
{
   long capacity;
   long count;

   char (*buy_item)[NULL_ITEM];
   char (*location)[NULL_LOC];
   double *total_cut_off_cost;
   cut_off_node (*cut_off_nodes);
} cut_off_costs;

/* Function prototypes */
int external_share_apply_deals(char              *ios_error_message,
                               char              *is_order_no,
                               char              *is_order_appr_ind,
                               char              *is_recalc_all_ind,
                               char              *is_override_manual_ind);

int external_apply_deals(OCIExtProcContext *with_context,
                   char              *ios_error_message,
                   char              *is_order_no,
                   char              *is_order_appr_ind,
                   char              *is_recalc_all_ind,
                   char              *is_override_manual_ind);

int lib_init( unsigned int ii_lib_commit_max_counter );

int external_process(char *is_order_no, 
                     char *is_order_appr_ind,
                     char *is_recalc_all_ind,
                     char *is_override_manual_ind);

int size_order_array(order_struct *oa_order);

int size_deal_array(deal_struct *oa_deal);

int check_lock_and_validate(short ii_online_ind, /* online mode indicator */
                            order_struct   *ia_raw_order,
                            order_struct   *oa_order,
                            unsigned int   *ii_rec_to_process);

int reset_order(item_cost_struct *ia_item_cost,
                char is_recalc_all_ind[NULL_IND],
                char is_override_manual_ind[NULL_IND],
                char is_bracket_costing_ind[NULL_IND]);

int update_supplier_cost(short ii_do_update,
                         item_cost_struct *ia_item_cost,
                         char is_override_manual_ind[NULL_IND]);

int insert_rev_orders(discount_build_struct *ioa_disct_bld,
                      item_cost_struct *ia_item_cost);

int get_deals(deal_struct *oa_deal);

int open_cursor(void);

int fetch_cursor(deal_struct *oa_deal);

int resize_deal_array(deal_struct *ioa_deal);

int org_level_given (char is_deal_id[NULL_DEAL_ID],
                     char is_deal_detail_id[NULL_DEAL_DETAIL_ID]);

int make_ordloc_build(char is_override_manual_ind[NULL_IND],
                      discount_build_struct *ioa_disct_bld);

int size_disct_bld_array(discount_build_struct *oa_disct_bld);

int resize_disct_bld_array(discount_build_struct *oa_disct_bld,
                           long *iol_disct_bld_size);

int process_txn_discount(deal_struct *ia_deal,
                         discount_build_struct *ia_disct_bld,
                         long il_seq_no);

int get_org_hier(discount_build_struct *ioa_disct_bld, 
                 long ll_rec);

int get_otb_cost(discount_build_struct *ioa_disct_bld,
                 char ls_import_country_id[NULL_COUNTRY_ID], 
                 int li_otb_flag);

int update_otb(discount_build_struct *ia_disct_bld);

int find_items_for_deal(deal_struct *ia_deal,
                        disc_item_struct *oa_disc_item,
                        long *ol_disc_item_ct,
                        discount_build_struct *ioa_disct_bld,
                        int *ii_txn_discount_exists,
                        long *il_seq_no);

int get_item_index(discount_build_struct *ia_disct_bld,
                   char is_item[NULL_ITEM],
                   char is_location[NULL_LOC],
                   long *oi_item_index);

int store_disc_item(item_deal_struct *ia_item_deal,
                    long il_item_rec,
                    deal_struct *ia_deal,
                    long il_deal_rec,
                    disc_item_struct *oa_disc_item,
                    long il_disc_item_rec);

int process_exclusive_deal(char is_item[NULL_ITEM],
                           char is_location[NULL_LOC],
                           discount_build_struct *ia_disct_bld,
                           char is_merch_level[NULL_MERCH_LEVEL],
                           char is_org_level[NULL_ORG_LEVEL]);

int convert_currency(double id_in_value,
                     char is_in_currency_code[NULL_CURRENCY_CODE],
                     double *od_out_value,
                     char is_out_currency_code[NULL_CURRENCY_CODE],
                     char is_exchg_rate_ind[NULL_IND]);

int convert_uom(char is_item[NULL_ITEM],
                char is_origin_country_id[NULL_COUNTRY_ID],
                double *id_qty_ordered,
                char is_threshold_limit_uom[NULL_THRESHOLD_LIMIT_UOM]);

int convert_qty_to_amount(double id_unit_cost,
                          double id_qty_ordered,
                          char is_deal_currency_code[NULL_CURRENCY_CODE],
                          double *od_amount_ordered);

int get_threshold_value(double id_deal_thresh_val,
                        char is_deal_id[NULL_DEAL_ID],
                        char is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                        char is_rebate_ind[NULL_IND],
                        double *od_discount_value);

int get_application_order(char is_item[NULL_ITEM],
                          char is_location[NULL_LOC],
                          long *ol_application_order);

int size_item_deal_array(item_deal_struct *oa_item_deal);

int resize_item_deal_array(item_deal_struct *oa_item_deal,
                           long *iol_item_deal_size);

int free_item_deal_array(item_deal_struct *oa_item_deal);

int size_disc_item_array(disc_item_struct *oa_disc_item);

int resize_disc_item_array(disc_item_struct *ioa_disc_item,
                           long *iol_disc_item_size);

int size_get_list(get_list *ioa_get_list);

int resize_get_list(get_list *ioa_get_list);

int free_get_list(get_list *ioa_get_list);

int size_get_list_array(get_list_array *ioa_get_list_array);

int resize_get_list_array(get_list_array *ioa_get_list_array);

int free_get_list_array(get_list_array *ioa_get_list_array);

int size_cut_off_node(cut_off_node *ioa_cut_off_node);

int resize_cut_off_node(cut_off_node *ioa_cut_off_node);

int free_cut_off_node(cut_off_node *ioa_cut_off_node);

int size_cut_off_costs(cut_off_costs *ioa_cut_off_costs);

int resize_cut_off_costs(cut_off_costs *ioa_cut_off_costs);

int free_cut_off_costs(cut_off_costs *ioa_cut_off_costs);

int size_item_cost_array(item_cost_struct *oa_item_cost, 
                         long il_item_count);

int free_item_cost_array(item_cost_struct *oa_item_cost);

double get_total_qty(char                   is_item[NULL_ITEM],
                     char                   is_location[NULL_LOC],
                     double                *iod_loc_ordered_qty,
                     double                *iod_total_qty,
                     char                   is_deal_id[NULL_DEAL_ID],
                     char                   is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                     disc_item_struct      *ia_disc_item,
                     long                   il_disc_item_ct);
int calculate_cost(disc_item_struct *ia_disc_item,
                   long il_disc_item_ct,
                   char is_import_order_ind[NULL_IND],
                   discount_build_struct *ioa_disct_bld);

int apply_cut_off_costs(get_list_array *ioa_get_list_array,
                        cut_off_costs  *ia_cut_off_costs);

int get_item_unit_cost(char is_item[NULL_ITEM],
                       char is_location[NULL_LOC],
                       double *iod_unit_cost,
                       discount_build_struct *ioa_disct_bld);

int calculate_average_unit_cost(char is_item[NULL_ITEM],
                                char is_location[NULL_LOC],
                                double *iod_average_unit_cost,
                                get_list_array *ia_get_list_array);

int calculate_ordloc_unit_cost(char is_item[NULL_ITEM],
                               char is_location[NULL_LOC],
                               double *iod_average_unit_cost,
                               get_list_array *ia_get_list_array);

int calc_discount(char   is_item[NULL_ITEM],
                  char   is_pack_no[NULL_ITEM],
                  char   is_location[NULL_LOC],
                  double id_discount_value,
                  char   is_discount_type[NULL_DISCOUNT_TYPE],
                  long   il_rownum,
                  char   is_deal_id[NULL_DEAL_ID],
                  char   is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                  char   is_paid_ind[NULL_IND],
                  char   is_deal_class[NULL_DEAL_CLASS],
                  double id_unit_cost_init,
                  double id_qty_ordered,
                  double id_total_qty_ordered,
                  char   is_buy_item[NULL_ITEM],
                  double id_thresh_buy_qty,
                  char   is_get_item[NULL_ITEM],
                  double id_thresh_get_qty,
                  double id_free_item_unit_cost,
                  char   is_recur_ind[NULL_IND],
                  disc_item_struct *ia_disc_item,
                  long   il_disc_item_ct,
                  char   is_qty_thresh_get_type[NULL_QTY_THRESH_GET_TYPE],
                  double id_qty_thresh_get_value,
                  double id_get_free_discount,
                  get_list_array *ia_get_list_array,
                  discount_build_struct *ioa_disct_bld,
                  cut_off_costs* ioa_cut_off_costs);

int insert_ordloc_invc_cost(char   is_pack_no[NULL_ITEM],
                            char   is_item[NULL_ITEM],
                            char   is_location[NULL_LOC],
                            double id_invc_cost,
                            double id_invc_qty,
                            long   il_list_index);

long get_list_index(get_list_array *ia_get_list_array,
                    char is_item[NULL_ITEM],
                    char is_location[NULL_LOC]);

long get_cut_off_cost_index(cut_off_costs *ioa_cut_off_costs,
                            char is_buy_item[NULL_ITEM],
                            char is_location[NULL_LOC]);

int insert_cut_off_cost(cut_off_costs *ia_cut_off_costs,
                        char is_buy_item[NULL_ITEM],
                        char is_location[NULL_LOC],
                        double id_total_cut_off_cost);

int insert_buy_get_into_cut_off_cost(cut_off_costs *ia_cut_off_costs,
                                     char is_buy_item[NULL_ITEM],
                                     char is_get_item[NULL_ITEM],
                                     char is_location[NULL_LOC],
                                     char is_deal_id[NULL_DEAL_ID],
                                     long il_rownum,
                                     char is_paid_ind[NULL_IND],
                                     double id_total_cut_off_cost);

int store_new_discount_details(char is_item[NULL_ITEM],
                               char is_location[NULL_LOC],
                               long *il_index,
                               double id_pror_unit_cost,
                               double id_invc_unit_cost,
                               double id_get_qty,
                               double id_total_qty,
                               long il_rownum,
                               char is_deal_id[NULL_DEAL_ID],
                               char is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                               char is_paid_ind[NULL_IND],
                               get_list_array *ia_get_list_array);

int get_prorated_core_info(char is_item[NULL_ITEM],
                          char is_location[NULL_LOC],
                          double id_total_qty,
                          double *iod_pror_unit_cost,
                          double *iod_invc_unit_cost,
                          short  *ioi_used_as_buy_item_ind,
                          double *od_available_deal_qty,
                          double *od_available_loc_qty,
                          long il_rownum,
                          char is_deal_id[NULL_DEAL_ID],
                          char is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                          char is_paid_ind[NULL_IND],
                          disc_item_struct *ia_disc_item,
                          long il_disc_item_ct,
                          get_list_array *ia_get_list_array);

int set_prorated_core_info(char is_item[NULL_ITEM],
                          char is_location[NULL_LOC],
                          double id_pror_unit_cost,
                          double id_pror_ordloc_unit_cost_discount,
                          double id_invc_unit_cost,
                          double id_item_deal_qty,
                          double id_item_loc_qty,
                          short  ii_used_as_buy_item_ind,
                          char   is_paid_ind[NULL_IND],
                          get_list_array *ia_get_list_array);

int insert_new_prorated_info(char is_item[NULL_ITEM],
                             char is_location[NULL_LOC],
                             double id_total_qty,
                             long il_rownum,
                             char is_deal_id[NULL_DEAL_ID],
                             char is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                             char is_paid_ind[NULL_IND],
                             long *il_index,
                             disc_item_struct *ia_disc_item,
                             long il_disc_item_ct,
                             get_list_array *ia_get_list_array);

int update_ordloc(char is_item[NULL_ITEM],
                  char is_location[NULL_LOC],
                  char is_loc_type[NULL_LOC_TYPE],
                  double id_unit_cost);

int update_ordloc_discount(char   is_item[NULL_ITEM],
                           char   is_pack_no[NULL_ITEM],
                           char   is_location[NULL_LOC],
                           char   is_deal_id[NULL_DEAL_ID],
                           char   is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                           double id_discount,
                           double id_discount_amt_per_unit);

int add_to_ordloc_discount(char   is_item[NULL_ITEM],
                           char   is_pack_no[NULL_ITEM],
                           char   is_location[NULL_LOC],
                           char   is_deal_id[NULL_DEAL_ID],
                           char   is_deal_detail_id[NULL_DEAL_DETAIL_ID],
                           double id_discount_amt_per_unit);

int apply_elc(char is_item[NULL_ITEM],
              char is_pack_no[NULL_ITEM],
              char is_location[NULL_LOC],
              char is_import_order_ind[NULL_IND],
              char is_origin_country_id[NULL_COUNTRY_ID]);

int delete_ordloc_disc_bld(void);

int delete_order_deal_build(void); 

int cleanup_discount_tables(order_struct *ia_order,
                            unsigned int ii_rec_to_process);

int delete_dcq(order_struct* ia_order,
               unsigned int ii_rec_to_process);

int update_lc(char   *is_item,
              char   *is_loc,
              double il_avg_cost,
              short  ii_cost_changed_ind);

int insert_l10n_doc_details_gtt(item_cost_struct *ia_item_cost);

void zero_counters(void);

long get_deal_ct(void);

int buyer_packs_detected(void);

unsigned int get_lib_commit_max_ctr(void);

void get_lib_error_message(char *ios_error_message);

void set_lib_error_message(char *ios_error_message);

long get_otb_count(void);

int size_temp_deal_array(deal_struct *oa_temp_deal);

int free_temp_deal_array(deal_struct *oa_temp_deal);

int free_order_array(order_struct *oa_order);

int free_disct_bld_array(discount_build_struct *oa_disct_bld);

int free_disc_item_array(disc_item_struct *oa_disc_item);

int free_deal_array(deal_struct *oa_deal);

#endif
