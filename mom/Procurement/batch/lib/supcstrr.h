#include <retek.h>


/*#define DEBUG*/
#define FILENAME "OUT"

#define FATAL    -1
#define NONFATAL 1
#define OK       0
#define TRUE     1
#define FALSE    0

#define MAX_SCALE_CNSTR     2         /* max num of scaling constraints */
#define MAX_FETCH_SIZE      1000
#define MAX_NUM_ORDLOC      1000      /* max num of locations the order goes to */
#define MAX_NUM_RANK        12        /* for max of 2 scaling constraints */
#define MAX_NUM_EVAL        16        /* for max of 2 scaling constraints */ 
#define MAX_VALUE           999999999 /* default scale_cnstr_max_val */ 
#define MAX_SCALE_ITERATION 1000      /* default max num of scaling iterations */

#define NULL_ALLOC_METHOD 2
#define NULL_OPERATOR     2
#define NULL_MODULE       7
#define NULL_KEY_VALUE    21
#define NULL_CHAPTER      5

/* define length */
#define NULL_SCALE_CNSTR_LVL        7 
#define NULL_SCALE_CNSTR_OBJ        7 
#define NULL_SCALE_CNSTR_TYPE       7 
#define NULL_SCALE_CNSTR_FLAG       2 
#define NULL_MAX_SCALING_ITERATIONS 5
#define NULL_ROUND_LVL              6 

int scaling_cnstr(char *ios_error_message, char *is_order_no);

/* define data structures */
typedef struct ord_inv_mgmt {
  char   s_scale_cnstr_ind[NULL_IND];    
  char   s_scale_cnstr_lvl[NULL_SCALE_CNSTR_LVL];   
  short  i_scale_cnstr_lvl_ind;
  char   s_scale_cnstr_obj[NULL_SCALE_CNSTR_OBJ]; 
  short  i_scale_cnstr_obj_ind;

  char   s_scale_cnstr_type1[NULL_SCALE_CNSTR_TYPE];
  short  i_scale_cnstr_type1_ind;
  char   s_scale_cnstr_uom1[NULL_UOM];
  short  i_scale_cnstr_uom1_ind;
  char   s_scale_cnstr_curr1[NULL_CURRENCY_CODE];
  short  i_scale_cnstr_curr1_ind;
  double d_scale_cnstr_min_val1;
  double d_scale_cnstr_max_val1;
  double d_scale_cnstr_min_tol1;
  double d_scale_cnstr_max_tol1;
  char   s_scale_cnstr_type2[NULL_SCALE_CNSTR_TYPE];
  short  i_scale_cnstr_type2_ind;
  char   s_scale_cnstr_uom2[NULL_UOM];
  short  i_scale_cnstr_uom2_ind;
  char   s_scale_cnstr_curr2[NULL_CURRENCY_CODE];
  short  i_scale_cnstr_curr2_ind;
  double d_scale_cnstr_min_val2;
  double d_scale_cnstr_max_val2;
  double d_scale_cnstr_min_tol2;
  double d_scale_cnstr_max_tol2;

  long   l_max_scaling_iterations;
  short  i_max_scaling_iterations_ind;
  char   s_due_ord_process_ind[NULL_IND];   
  char   s_mult_vehicle_ind[NULL_IND];              
} ord_inv_mgmt;

typedef struct scale_cnstr {
  char   s_scale_cnstr_type[NULL_SCALE_CNSTR_TYPE];
  char   s_scale_cnstr_uom[NULL_UOM];
  char   s_scale_cnstr_curr[NULL_CURRENCY_CODE];
  double d_scale_cnstr_min_val;
  double d_scale_cnstr_max_val;
  double d_scale_cnstr_min_tol;
  double d_scale_cnstr_max_tol;
} scale_cnstr; 

#define NULL_ITEM_TYPE        2
#define NULL_STOCK_CAT        7
#define NULL_REPL_METHOD      7
#define NULL_INV_SELLING_DAYS 4
#define NULL_REVIEW_TIME      4
#define NULL_SUPP_LEAD_TIME   5
#define NULL_PICKUP_LEAD_TIME 4
#define NULL_WH_LEAD_TIME     4
#define NULL_SEASON_ID        4
#define NULL_PHASE_ID         4
#define NULL_STORE_ORD_MULT   2 
#define NULL_LEVEL            7
#define NULL_CSTLVL           4

typedef struct driv_cursor {

  /* data fetched from cursor */
  char   (*s_ordloc_exist)[NULL_IND]; /* ordloc or alloc_detail exists flag */ 
  char   (*s_item)[NULL_ITEM];
  char   (*s_source_wh)[NULL_LOC];    /* '-1' means not a xdock allocated loc */
  char   (*s_loc_type)[NULL_LOC_TYPE];
  char   (*s_location)[NULL_LOC];    
  double  *d_qty_prescaled;
  short   *i_qty_prescaled_ind;
  double  *d_orig_raw_roq_pack;
  short   *i_orig_raw_roq_pack_ind; 
  char   (*s_item_non_scale_ind)[NULL_IND];
  char   (*s_loc_non_scale_ind)[NULL_IND];
  char   (*s_pack_ind)[NULL_IND];
  char   (*s_dept)[NULL_DEPT];
  double  *d_sup_unit_cost;
  char   (*s_sup_currency_code)[NULL_CURRENCY_CODE];
  char   (*s_ord_currency_code)[NULL_CURRENCY_CODE];
  double  *d_exchange_rate;
  short   *i_exchange_rate_ind;
  double  *d_item_min_order_qty;  
  short   *i_item_min_order_qty_ind;
  double  *d_item_max_order_qty;
  short   *i_item_max_order_qty_ind;
  double  *d_ship_carton_len;
  short   *i_ship_carton_len_ind;
  double  *d_ship_carton_hgt;
  short   *i_ship_carton_hgt_ind;
  double  *d_ship_carton_wid;
  short   *i_ship_carton_wid_ind;
  char   (*s_dimension_uom)[NULL_UOM];
  short   *i_dimension_uom_ind;
  double  *d_ship_carton_wt;
  short   *i_ship_carton_wt_ind;
  char   (*s_weight_uom)[NULL_UOM];
  short   *i_weight_uom_ind;
  double  *d_stat_cube;
  short   *i_stat_cube_ind;
  double  *d_supp_pack_size;
  double  *d_pallet_size;
  double  *d_inner_pack_size;
  /* rounding, bracket costing, and multichannel-related variables */
  char   (*s_round_lvl)[NULL_LEVEL];
  double  *d_round_to_inner_pct;
  double  *d_round_to_case_pct;
  double  *d_round_to_layer_pct;
  double  *d_round_to_pallet_pct;
  double  *d_tier;
  char   (*s_physical_wh)[NULL_LOC];
  char   (*s_rounding_seq)[NULL_LOC];
  char   (*s_costing_level)[NULL_CSTLVL];

  char   (*s_master_item)[NULL_ITEM];
  short   *i_master_item_ind;
  char   (*s_item_type)[NULL_ITEM_TYPE];
  short   *i_item_type_ind;
  char   (*s_stock_cat)[NULL_STOCK_CAT];  
  short   *i_stock_cat_ind;
  char   (*s_repl_method)[NULL_REPL_METHOD];
  short   *i_repl_method_ind;
  char   (*s_due_ind)[NULL_IND];
  short   *i_due_ind_ind;
  double  *d_order_point;
  short   *i_order_point_ind;
  char   (*s_order_written_date)[NULL_DATE];
  char   (*s_order_lead_time_date)[NULL_DATE];
  char   (*s_order_upto_point_date)[NULL_DATE];
  short   *i_order_upto_point_date_ind;
  long    *l_max_scale_down_day;
  short   *i_max_scale_down_day_ind;
  char   (*s_season_id)[NULL_SEASON_ID];
  short   *i_season_id_ind;
  char   (*s_phase_id)[NULL_PHASE_ID];
  short   *i_phase_id_ind;
  double  *d_max_scale_value;

  /* fields fetched for order writeout */
  char   (*s_supplier)[NULL_SUPPLIER];
  char   (*s_origin_country_id)[NULL_COUNTRY_ID];
  char   (*s_import_order_ind)[NULL_IND];
  char   (*s_import_country_id)[NULL_COUNTRY_ID];
  short   *i_import_country_id_ind;
  char   (*s_earliest_ship_date)[NULL_DATE];
  short   *i_earliest_ship_date_ind;
  char   (*s_latest_ship_date)[NULL_DATE];
  short   *i_latest_ship_date_ind;
  char   (*s_alloc_no)[NULL_ALLOC_NO];
  double  *d_qty_ordered;  /* qty ordered or allocated */

  /* derived fields */
  char   (*s_non_scale_ind)[NULL_IND];
  char   (*s_store_close_date)[NULL_DATE]; 
  char   (*s_store_ord_mult)[NULL_STORE_ORD_MULT]; 
  char   (*s_buyer_pack_ind)[NULL_IND];
  double  *d_uom_conv_factor;
  double  *d_min_scale_value;
  double  *d_volume; 
  double  *d_ord_unit_cost;   /* unit_cost in order and scaling currency code */
  double  *d_pack_item_qty;    /* pack_item_qty for simple pack */
  char   (*s_break_pack_ind)[NULL_IND]; /* break_pack_ind of source_wh for xdock */

  /* scaling solution */
  char   (*s_scaled_ind)[NULL_IND];  /* flag for writing out scaled solutions */
  double  *d_qty_scaled_sol;         /* scaled qty for acceptable solution */
} driv_cursor;

/* data structure that holds all possible forecasting data for each item/loc */
typedef struct forecast_item_loc {
  char          s_item[NULL_ITEM];
  char          s_location[NULL_LOC];
  char          s_physical_wh[NULL_LOC];
  char          s_forecast_start_date[NULL_DATE];
  char          s_forecast_end_date[NULL_DATE];
  long          l_max_days;          
  long          l_total_eow_fetched;
  char        (*s_eow_date)[NULL_DATE];   /* size to max_days/7 */
  double       *d_sales;                  /* size to max_days/7 */
  /* daily forecasting variables */
  long          l_total_day_fetched;
  char        (*s_data_date)[NULL_DATE];  /* size to max_days */
  double       *d_day_sales;              /* size to max_days */
} forecast_item_loc;

typedef struct scale_cnstr_super {
  char   s_location[NULL_LOC];       /* "-1" if order-level scaling */
  char   s_scale_cnstr_type[NULL_SCALE_CNSTR_TYPE];
  double d_scale_cnstr_super_min;
  double d_scale_cnstr_super_min_tol;
  double d_scale_cnstr_super_max;
  double d_scale_cnstr_super_max_tol; 
} scale_cnstr_super;

#define UP             0     /* ranking direction */
#define DOWN           1     
#define STOP           2
#define NO             3 
#define DAY            0 
#define CASE           1 
#define NULL_RANK      3 
#define NULL_MSG       100

typedef struct rank_priority {
  char   s_rank_codes[MAX_SCALE_CNSTR+1];  
  int    i_rank_order;
  char   s_accept_ind[NULL_IND];
} rank_priority;

typedef struct eval_system {
  char   s_rank_1[3];     /* RL,YL,YH,RH */ 
  char   s_rank_2[3];     /* RL,YL,YH,RH */ 
  int    i_direction;     /* suggested direction: UP, DOWN, NO */
} eval_system;

/*    
 * data structure for solution array in day-of-supply and case-pallet scaling 
 * 1) for day-of-supply scaling, solution array is sized to max_scale_iteration
 *    + 1; index 0 stores the prescaled ranking; 
 * 2) for case-pallet scaling, solution array is sized to max_scale_iteration *
 *    gl_total_num_fetched + 1 since solution needs to be ranked separately for
 *    each item/loc in each iteration; index 0 stores the ranking after solution
 *    day rounding; 
 */
typedef struct solution_day {
  double d_qty_scaled[MAX_SCALE_CNSTR]; /* total scaled qty for loc in cnstr type */ 
  char   s_cnstr_rank[MAX_SCALE_CNSTR][NULL_RANK];  /* RL,YL,YH,RH,GX */
  char   s_rank_codes[MAX_SCALE_CNSTR+1];           /* GG,GY,YG,...,G,Y,R */ 
  int    i_rank_order;                              /* 0,1,2,... */
  char   s_accept_ind[NULL_IND];                    /* Y,N */
} solution_day;

typedef struct scale_loc {
  char          s_location[NULL_LOC];  /* "-1" if order-level scaling */
  char          s_loc_type[NULL_LOC_TYPE];
  char          s_physical_wh[NULL_LOC];
  char          s_rounding_seq[NULL_LOC];
  int           i_direction;           /* scaling direction: UP,DOWN,STOP */
  int           i_has_source;
  solution_day *a_solution_day;        
  long          l_last_index;  /* index of last iteration - for debugging */ 
  long          l_sol_index;   /* index of solution iteration - for debugging */
  long          l_pwh_index;   /* index of pwh in scale cnstr super struct (multichannel) */
  int           i_scaled_ind;
} scale_loc;  

/* 
 * data struct for each item-loc in day-of-supply and case-pallet scaling:
 * 1) d_qty_scaled_suom is the scaling qty in standard uom; 
 * 2) d_qty_scaled_suom_rnd is the scaling qty evaluated to pallet rounding 
 *    in standard uom; 
 * 3) d_qty_scaled_suom_rnd usually equals to d_qty_scaled_suom except during 
 *    case-pallet scaling and round_lvl = "B", in which case d_qty_scaled_suom
 *    holds the value of case scaling; d_qty_scaled_suom_rnd holds the value
 *    of case scaling after pallet evaluation; 
 * 4) d_qty_scaled_suom_rnd is used for evaluating against the scaling and 
 *    non-scaling constraints, and will be the scaled order quantity. 
 * 5) similar to scale_loc, for day-of-supply scaling, d_qty_scaled_xxx array 
 *    is sized to max_scale_iteration + 1, and index 0 stores the prescaled qty;
 *    for case-pallet scaling, it is sized to max_scale_iteration * gl_total_num
 *    _fetched + 1, and index 0 stores the solution day qty after rounding;
 * 6) index of d_qty_scaled_xxx corresponds to the index of solution array, 
 *    which means if the best solution day is day i, the order qty for item/loc
 *    will be d_qty_scaled_suom_rnd[i]. 
 */
typedef struct scale_item_loc {
  char     s_item[NULL_ITEM];
  char     s_location[NULL_LOC];
  char     s_physical_wh[NULL_LOC];
  char     s_rounding_seq[NULL_LOC];
  double  *d_qty_scaled_suom;
  double  *d_qty_scaled_suom_rnd;
  char   (*s_message)[NULL_MSG];
} scale_item_loc;


/* 
 * l_driv_cursor_index is the index for the x-docked location in the driving
 * cursor array.
 */
typedef struct xdock_to_loc {
  char                 s_xdock_loc[NULL_LOC]; 
  char                 s_item[NULL_ITEM];
  long                 l_driv_cursor_index;
  struct xdock_to_loc *next;
} xdock_to_loc;

/* 
 * l_driv_cursor_index is index for source_wh/item in the driving cursor array.
 * For repl order, if qty_prescaled is negative for all x-docked locations,
 * there is no ordloc record (therefore no entry in the driving cursor) for
 * the source_wh. In this case, l_driv_cursor_index will be -1. After scaling,
 * if any of the x-docked location scales to a positive quantity, add an entry
 * to the end of the driving cursor for the source_wh/item, so that an ordloc
 * record can be written during the writeout process. 
 *
 * d_residual_qty is the difference between the total allocation qty in store_
 * ord_mult before and after case rounding. This number plus the total allocation
 * qty plus the dir-to-wh qty is the total qty ordered to the warehouse.
 */ 
typedef struct xdock_from_loc {
  char                   s_source_wh[NULL_LOC];
  char                   s_item[NULL_ITEM];
  long                   l_driv_cursor_index;
  char                   s_break_pack_ind[NULL_IND];
  double                 d_residual_qty;
  xdock_to_loc          *a_xdock_to_loc; 
  struct xdock_from_loc *next;
} xdock_from_loc;


typedef struct item_list {
  char             s_item[NULL_ITEM];
  struct item_list *next; 
} item_list;

typedef struct alloc_list {
  char               s_alloc_no[NULL_ALLOC_NO];
  struct alloc_list *next;
} alloc_list;
