#include "retek_2.h"
#include "oci.h"

/********************\
 * Debugging tools: *
\********************/

/*
#define DEBUG
#define FILENAME ""
*/

/* #define DEBUG 1*/
/* If DEBUG is turned on, please change FILENAME to point to a directory with write access */
/* so that the debug file can be written. */
/* For example:  /projects/rmsdun12.1/dev/log/vk.txt */
/* #define FILENAME "vk.txt"*/


/* #defines */
#define NUM_SPLIT_CNST           2

#define NULL_SHIP_DAYS           4
#define NULL_FOB_TITLE_PASS      3
#define NULL_FOB_TITLE_PASS_DESC 251 /* NLS */
#define NULL_CONTRACT_TERMS      16
#define NULL_PAYMENT_METHOD      7
#define NULL_SHIP_METHOD         7
#define NULL_AGENT               11
#define NULL_PORT                6
#define NULL_SEQ_NO              5
#define NULL_PACK_SIZE           13
#define NULL_REPL_ORD_DAYS       4
#define NULL_ITERATIONS          5
#define NULL_ROUND_LVL           7
#define NULL_BILL_TO_ID          6
#define NULL_ORDER_TYPE          9
#define NULL_TSF_PO_LINK         11
#define NULL_CNSTR_LVL           7
#define NULL_FILE_ID             11
#define NULL_PURCHASE_TYPE       7
#define NULL_PICKUP_LOC          251 /* NLS */
#define NULL_COST_SOURCE         5

#define NULL_SHIP_PAY_METHOD        3
#define NULL_FOB_TRANS_RES          3
#define NULL_FOB_TRANS_RES_DESC     251 /* NLS */
#define NULL_FACTORY                11
#define NULL_BILL_TO_ID             6
#define NULL_CLEARING_ZONE_ID       6
#define NULL_FREIGHT_CONTRACT_NO    11
#define NULL_PICKUP_NO              26
/*#define NULL_COMMENT_DESC           2001*/ /* NLS */

#define NULL_PARTNER             11
#define NULL_PARTNER_TYPE        7

#define NULL_ROUTING_LOC         6

#define YES      "Y"
#define NO       "N"
#define ORDER    "O"
#define LOCATION "I"
#define ORDER_TYPE "PREDIST"

/* source types */
#define IB   "I"
#define PO   "O"
#define REPL "R"

#define FULL_TRUCK        1
#define LTL_TRUCK         2
#define UNKNOWN_TRUCK     3

/* globals */
#if defined(ADD_LINE_MODULE)
  #define ADD_LINE_GLOBAL
#else
  #define ADD_LINE_GLOBAL extern
#endif

/* global variables set in init() */
FILE *fp_outfile;
ADD_LINE_GLOBAL char ps_vdate[NULL_DATE];
ADD_LINE_GLOBAL char ps_repl_order_days[NULL_REPL_ORD_DAYS] ;
ADD_LINE_GLOBAL int  pi_elc_ind ;
ADD_LINE_GLOBAL char ps_alloc_method[NULL_IND] ;
ADD_LINE_GLOBAL char ps_base_country_id[NULL_COUNTRY_ID] ;
ADD_LINE_GLOBAL char ps_latest_ship_days[NULL_SHIP_DAYS] ;
ADD_LINE_GLOBAL char ps_fob_title_pass[NULL_FOB_TITLE_PASS] ;
ADD_LINE_GLOBAL char ps_fob_title_pass_desc[NULL_FOB_TITLE_PASS_DESC] ;
ADD_LINE_GLOBAL char ps_max_scale_iterations[NULL_ITERATIONS] ;
ADD_LINE_GLOBAL char ps_bill_to_id[NULL_BILL_TO_ID] ;
ADD_LINE_GLOBAL char ps_batch_ind[NULL_IND];
ADD_LINE_GLOBAL char ps_ord_appr_amt_code[NULL_IND];
ADD_LINE_GLOBAL char ps_import_ind[NULL_IND];
ADD_LINE_GLOBAL char ps_locked_ind[NULL_IND];

/**************************************\
|**************************************|
|**  PO LINKED LISTS STRUCTURES      **|
|**************************************|
\**************************************/

/* array struct to hold info from the driving cursor fetch */
typedef struct driv_cur_array
{
   char  (*s_source_type)[NULL_IND];
   char  (*s_supplier)[NULL_SUPPLIER];
   char  (*s_dept)[NULL_DEPT];
   char  (*s_order_status)[NULL_IND];
   char  (*s_item)[NULL_ITEM];
   char  (*s_pack_ind)[NULL_IND];
   char  (*s_loc)[NULL_LOC];
   char  (*s_phy_loc)[NULL_LOC];
   char  (*s_repl_wh_link)[NULL_LOC];
   char  (*s_loc_type)[NULL_LOC_TYPE];
   char  (*s_loc_country)[NULL_COUNTRY_ID];
   char  (*s_due_ind)[NULL_IND];
   char  (*s_xdock_ind)[NULL_IND];
   char  (*s_xdock_store)[NULL_LOC];
   char  (*s_contract)[NULL_CONTRACT_NO];
   char  (*s_contract_type)[NULL_IND];
   double *d_qty;
   double *d_last_rounded_qty;
   double *d_last_grp_rounded_qty;
   char  (*s_ctry)[NULL_COUNTRY_ID];
   char  (*s_unit_cost)[NULL_AMT];
   int    *i_supp_lead_time;
   int    *i_pickup_lead_time;
   char  (*s_non_scale_ind)[NULL_IND];
   char  (*s_pack_size)[NULL_PACK_SIZE];
   double *d_eso;
   short  *i_eso_ind;
   double *d_aso;
   short  *i_aso_ind;
   char  (*s_tsf_po_link)[NULL_TSF_PO_LINK];
   short  *i_tsf_po_link;
   long   *l_ib_days_to_event;
   char  (*s_rr_rowid)[NULL_ROWID];
   short  *i_rr_rowid_ind;
   char  (*s_ir_rowid)[NULL_ROWID];
   short  *i_ir_rowid_ind;
   char  (*s_bw_rowid)[NULL_ROWID];
   short  *i_bw_rowid_ind;
   char  (*s_costing_loc)[NULL_LOC];
   char  (*s_store_type)[NULL_GET_TYPE];
} driv_cur_fetch_array;

/* non-array struct to process info from the driving cursor, plus
   hold some additional info populted after the driving cursor
   is fetched (dealing with contracts and restart recovery) */
typedef struct driv_cur
{
   char    s_source_type[NULL_IND];
   char    s_supplier[NULL_SUPPLIER];
   char    s_dept[NULL_DEPT];
   char    s_order_status[NULL_IND];
   char    s_item[NULL_ITEM];
   char    s_pack_ind[NULL_IND];
   char    s_loc[NULL_LOC];
   char    s_phy_loc[NULL_LOC];
   char    s_repl_wh_link[NULL_LOC];
   char    s_loc_type[NULL_LOC_TYPE];
   char    s_due_ind[NULL_IND];
   char    s_xdock_ind[NULL_IND];
   char    s_xdock_store[NULL_LOC];
   char    s_contract[NULL_CONTRACT_NO];
   char    s_contract_type[NULL_IND+1];
   double  d_qty;
   double  d_last_rounded_qty;
   double  d_last_grp_rounded_qty;
   char    s_ctry[NULL_COUNTRY_ID];
   char    s_unit_cost[NULL_AMT];
   char    s_unit_cost_init[NULL_AMT];
   int     i_supp_lead_time;
   int     i_pickup_lead_time;
   char    s_non_scale_ind[NULL_IND];
   char    s_pack_size[NULL_PACK_SIZE];
   double  d_eso;
   double  d_aso;
   char    s_tsf_po_link[NULL_TSF_PO_LINK];
   char    s_rr_rowid[NULL_ROWID];
   char    s_ir_rowid[NULL_ROWID];
   char    s_bw_rowid[NULL_ROWID];

   /* these are not populated directly from the driving cursor struct*/
   char    s_pool_supp[NULL_SUPPLIER];
   char    s_file_id[NULL_FILE_ID];
   char    s_contract_terms[NULL_CONTRACT_TERMS];
   char    s_contract_approval_ind[NULL_IND];
   char    s_contract_header_rowid[NULL_ROWID];

   /* splitting specific */
   char    s_split_ref_ord_no[NULL_ORDER_NO];
   double  d_splitting_increment;
   double  d_round_pct;
   double  d_model_qty;
   double  d_cnst_qty[NUM_SPLIT_CNST];
   double  d_cnst_map[NUM_SPLIT_CNST];

   /* investment buy specific */
   long    l_ib_days_to_event;

   char    s_loc_country[NULL_COUNTRY_ID];

   /* franchise po specific */
   char    s_costing_loc[NULL_LOC];
   char    s_store_type[NULL_GET_TYPE];

} driv_cur_info_process;


/* holds information needed to create an ORDLOC or ALLOC_DETAIL record */
struct ordloc
{
   char    s_source_type[NULL_IND];
   char    s_loc[NULL_LOC];
   char    s_phy_loc[NULL_LOC];
   char    s_repl_wh_link[NULL_LOC];
   char    s_loc_type[NULL_LOC_TYPE];
   char    s_due_ind[NULL_IND];
   char    s_item[NULL_ITEM];
   char    s_pack_ind[NULL_IND];
   char    s_xdock_ind[NULL_IND];
   char    s_xdock_store[NULL_LOC];
   char    s_non_scale_ind[NULL_IND];
   double  d_qty;
   double  d_last_rounded_qty;
   double  d_last_grp_rounded_qty;

   /* splitting specific variables */
   double  d_cnst_qty[NUM_SPLIT_CNST];
   double  d_cnst_map[NUM_SPLIT_CNST];

   char    s_unit_cost[NULL_AMT];
   char    s_unit_cost_init[NULL_AMT];
   char    s_cost_source[NULL_COST_SOURCE];
   char    s_tsf_po_link[NULL_TSF_PO_LINK];
   char    s_rr_rowid[NULL_ROWID];
   char    s_ir_rowid[NULL_ROWID];
   char    s_bw_rowid[NULL_ROWID];

   /* at least one item/location must have a positive order qty or
      be due for the item to be added to the order, this will track that */
   char    s_write_ind[NULL_IND];

   struct  ordloc *next_ordloc_node;
};
typedef struct  ordloc ORDLOC;
typedef ORDLOC *ORDLOCPTR;

/* holds information needed to create an ORDSKU record */
struct ordsku
{
   char    s_item[NULL_ITEM];
   char    s_ctry[NULL_COUNTRY_ID];
   char    s_pack_size[NULL_QTY];
   char    s_non_scale_ind[NULL_IND];
   double  d_splitting_increment;
   double  d_round_pct;
   double  d_model_qty;
   double  d_prorate_qty;
   double  d_fraction;

   int     i_min_supp_lead_time;
   int     i_max_supp_lead_time;

   /* at least one item/location must have a positive order qty or
      be due for the item to be added to the order depending on
      the sup_inv_mgmt.due_ord_process_ind, this will track that */
   char    s_write_ind[NULL_IND];

   struct  ordsku *next_ordsku_node;
   struct  ordloc *first_ordloc_node;
};
typedef struct  ordsku ORDSKU;
typedef ORDSKU *ORDSKUPTR;

/* holds supplier/dept info associated with each ordhead node */
struct supplier_info
{
   /* SUPS info */
   char    s_qc_ind[NULL_IND];
   char    s_terms[NULL_TERMS];
   char    s_freight_terms[NULL_FREIGHT_TERMS];
   char    s_payment_method[NULL_PAYMENT_METHOD];
   char    s_ship_method[NULL_SHIP_METHOD];
   char    s_edi_po_ind[NULL_IND];
   char    s_currency_code[NULL_CURRENCY_CODE];
   char    s_pre_mark_ind[NULL_IND];
   char    s_inv_mgmt_lvl[NULL_IND];
   char    s_dept_level_ord[NULL_IND];

   /* SUP_IMPORT_ATTR info */
   char    s_agent[NULL_AGENT];
   char    s_discharge_port[NULL_PORT];
   char    s_lading_port[NULL_PORT];

   /* ADDR info */
   char    s_seq_no[NULL_SEQ_NO];

   /* DEPS info */
   char    s_buyer[NULL_BUYER];

   /* SUPS_INV_MGMT info */
   char    s_single_loc_ind[NULL_IND];
   char    s_due_ord_process_ind[NULL_IND];
   char    s_due_ord_ind[NULL_IND];
   char    s_due_ord_lvl[NULL_IND];
   char    s_non_due_ord_create_ind[NULL_IND];
   char    s_min_cnstr_lvl[NULL_CNSTR_LVL];
   char    s_scale_cnstr_ind[NULL_IND];
   char    s_truck_split_ind[NULL_IND];
   char    s_truck_method[NULL_IND];
   char    s_cnst_type[NUM_SPLIT_CNST][NULL_IND];
   char    s_cnst_uom[NUM_SPLIT_CNST][NULL_UOM];
   double  d_cnst_value[NUM_SPLIT_CNST];
   double  d_cnst_threshold[NUM_SPLIT_CNST];

   char    s_purchase_type[NULL_PURCHASE_TYPE];
   char    s_pickup_loc[NULL_PICKUP_LOC];

   char    s_sup_inv_mgmt_rowid[NULL_ROWID];
   char    s_ord_inv_mgmt_rowid[NULL_ROWID];

   /* only used/populated in split_po.pc */
   double  d_total_cnst_qty[NUM_SPLIT_CNST];
   double  d_cnst_value_reset[NUM_SPLIT_CNST];
   int     i_ltl_flag;
   /* determines which truck constraint is more constraining */
   int     i_cnst_index;

   /* Additional partners */
   char    s_partner_type_1[NULL_PARTNER_TYPE];
   char    s_partner_1[NULL_PARTNER];
   char    s_partner_type_2[NULL_PARTNER_TYPE];
   char    s_partner_2[NULL_PARTNER];
   char    s_partner_type_3[NULL_PARTNER_TYPE];
   char    s_partner_3[NULL_PARTNER];
   char    s_factory[NULL_PARTNER];
};
typedef struct supplier_info SUP_INFO;
typedef SUP_INFO *SUPINFOPTR;

/* holds information needed to create an ORDHEAD record */
struct ordhead
{
   char    s_order_no[NULL_ORDER_NO];
   char    s_split_ref_ord_no[NULL_ORDER_NO];
   char    s_supplier[NULL_SUPPLIER];
   char    s_pool_supp[NULL_SUPPLIER];
   char    s_file_id[NULL_FILE_ID];
   char    s_import_order[NULL_IND];
   char    s_order_status[NULL_IND];

   /* needed here for insert and single dept orders */
   char    s_dept[NULL_DEPT];

   /* needed here to deal with single location orders */
   char    s_order_loc[NULL_LOC];
   char    s_order_loc_type[NULL_LOC_TYPE];

   char    s_contract[NULL_CONTRACT_NO];
   char    s_contract_type[NULL_IND+1];
   char    s_contract_terms[NULL_CONTRACT_TERMS];
   char    s_contract_approval_ind[NULL_IND];
   char    s_contract_header_rowid[NULL_ROWID];

   /* these are updated for each item/location added to the order */
   int     i_min_supp_lead_time;
   int     i_max_supp_lead_time;
   int     i_min_pickup_lead_time;
   int     i_max_pickup_lead_time;

   /* these are used to determine if the order as a whole is due, if */
   /* due ordering is on and the due order level is 'O'rder */
   double  d_eso;
   double  d_aso;

   /* at least one item/location must have a positive order qty for
      the order to be written */
   char    s_write_ind[NULL_IND];

   /* ONLY used when due order processing is on, and the due
      order level is 'I', then it is used what to write to
      the ord_inv_mgmt.due_ind field  */
   char    s_itemloc_due_ind[NULL_IND];

   /* set when atleast one line item is sourced from investment buy */
   char    s_ib_flag[NULL_IND];

  /* used only when importer/exporter entity used for routing of orders */
   char    s_import_id[NULL_LOC];
   char    s_routing_loc_id[NULL_ROUTING_LOC];

   /* set when clearing zone is setup for the import country and the import_order_ind = �Y�*/
   char    s_clearing_zone_id[NULL_CLEARING_ZONE_ID];

   /* holds import country id for the order */
   char    s_import_country_id[NULL_COUNTRY_ID];

   /* holds supplier/dept level info concerning the order */
   struct  supplier_info t_sup_info;

   struct  ordhead *next_order_node;
   struct  ordsku  *first_ordsku_node;
};
typedef struct ordhead ORDHEAD;
typedef ORDHEAD *ORDHEADPTR;


/**************************************\
|**************************************|
|**  FUNCITON PROTOTYPES             **|
|**************************************|
\**************************************/

/* PLSQLORDCREATE.PC */
int external_manual_po_split(OCIExtProcContext *with_context,
                    char               ios_error_message[NULL_ERROR_MESSAGE],
                    char              *is_order_no);
int external_buyer_create_po(OCIExtProcContext *with_context,
                    char               ios_error_message[NULL_ERROR_MESSAGE],
                    char              *is_session_id,
                    char              *is_approval_ind);

/* ADD_LINE_ITEM.PC */
int set_globals(char  ios_error_message[NULL_ERROR_MESSAGE]);
int add_to_order(ORDHEADPTR            *in_head_ptr,
                 driv_cur_info_process  it_ot_info,
                 SUP_INFO               in_supp_str,
                 char                  *is_order_no,
                 char                   ios_error_message[NULL_ERROR_MESSAGE]);
int kill_order(ORDHEADPTR *in_head_ptr);
int get_inv_mgmt(SUPINFOPTR   in_supp_ptr,
                 char         *is_order_no,
                 char         *ios_pool_supp,
                 char         *ios_file_id,
                 char          ios_error_message[NULL_ERROR_MESSAGE]);
int get_sup_info(SUPINFOPTR   in_supp_ptr,
                 char        *is_contract,
                 char        *is_supplier,
                 char        *is_dept,
                 char        *is_loc,
                 char         ios_error_message[NULL_ERROR_MESSAGE]);

/* SPLIT_PO.PC */
int split_po(ORDHEADPTR *in_head_ptr,
             int        *ioi_split_ind,
             char        ios_error_message[NULL_ERROR_MESSAGE]);
int delete_order(char   *is_order_no,
                 char    ios_error_message[NULL_ERROR_MESSAGE]);

/* CREATE_PO.PC */
int do_inserts(ORDHEADPTR  in_head_ptr,
               long        *il_orders_processed,
               char         ios_error_message[NULL_ERROR_MESSAGE]);

int approve_submit_po(ORDHEADPTR  in_head_ptr,
                      char         ios_error_message[NULL_ERROR_MESSAGE]);
int round_po(ORDHEADPTR  in_head_ptr,
             char  ios_error_message[NULL_ERROR_MESSAGE]);
int update_oim(char *is_order_no,
               char *is_ltl_flag,
               char  is_split_message[NULL_ERROR_MESSAGE],
               char  ios_error_message[NULL_ERROR_MESSAGE]);
int update_group_rounded_qty(ORDHEADPTR in_head_ptr,
                             char       ios_error_message[NULL_ERROR_MESSAGE]);

/* ORDSPLIT.PC */
int split_wrapper(char *ls_order_no,
                  char  ios_error_message[NULL_ERROR_MESSAGE]);

/* BUYERBLD.PC */
int build_po_wrapper(char *is_session_id,
                     char *is_approval_ind,
                     char  ios_error_message[NULL_ERROR_MESSAGE]);
