-------------------------------------------------------------------------
-- Copyright (c) 2015, Oracle and/or its affiliates.
-- All rights reserved.
-- $HeadURL$
-- $Revision$
-- $Date$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--     TABLES UPDATED:			DEAL_HEAD_CFA_EXT
--					ELC_COMP_CFA_EXT
--					ORDHEAD_CFA_EXT
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ALTERING TABLE
--------------------------------------
ALTER TABLE DEAL_HEAD_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN DEAL_HEAD_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN DEAL_HEAD_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN DEAL_HEAD_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE ELC_COMP_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ELC_COMP_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ELC_COMP_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ELC_COMP_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

ALTER TABLE ORDHEAD_CFA_EXT
  ADD (DATE_23 DATE,
       DATE_24 DATE,
       DATE_25 DATE)
/

COMMENT ON COLUMN ORDHEAD_CFA_EXT.DATE_23 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_23 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ORDHEAD_CFA_EXT.DATE_24 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_24 in CFA_ATTRIB.STORAGE_COL_NAME.'
/
COMMENT ON COLUMN ORDHEAD_CFA_EXT.DATE_25 IS 'This column holds data related to the attribute defined on the CFA_ATTRIB table that references DATE_25 in CFA_ATTRIB.STORAGE_COL_NAME.'
/

