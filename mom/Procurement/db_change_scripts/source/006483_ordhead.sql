--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

-----------------------------------------------------------------------
--       Dropping the trigger which the table has dependency over
-----------------------------------------------------------------------


DECLARE
  L_trigger_exists number := 0;
BEGIN
  SELECT count(*) INTO L_trigger_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'RMS_TABLE_OHE_AIUR';

  if (L_trigger_exists != 0) then
      execute immediate 'DROP TRIGGER RMS_TABLE_OHE_AIUR';
  end if;
end;
/


PROMPT Modifying Table 'ORDHEAD'
ALTER TABLE ORDHEAD ADD LAST_UPDATE_ID VARCHAR2 (30 ) DEFAULT NVL(sys_context('RETAIL_CTX', 'APP_USER_ID'), NVL(sys_context('USERENV','CLIENT_INFO'),sys_context('USERENV','SESSION_USER'))) NOT NULL
/

COMMENT ON COLUMN ORDHEAD.LAST_UPDATE_ID is 'Holds the user-id of the user who most recently updated this record.  This field is required by the database.'
/

ALTER TABLE ORDHEAD ADD LAST_UPDATE_DATETIME DATE  DEFAULT SYSTIMESTAMP NOT NULL
/

COMMENT ON COLUMN ORDHEAD.LAST_UPDATE_DATETIME is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.'
/
