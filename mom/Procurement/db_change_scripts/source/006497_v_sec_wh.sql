--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:          V_SEC_WH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
CREATE OR REPLACE FORCE  VIEW V_SEC_WH ("WH_NAME", "WH_NAME_SECONDARY", "WH", "CURRENCY_CODE", "PHYSICAL_WH", "PRIMARY_WH", "STOCKHOLDING_IND", "REPL_IND", "IB_IND") AS 
  SELECT wh_name,
        WH_NAME_SECONDARY,
        wh,
        currency_code,
        physical_wh,
        primary_wh,
        stockholding_ind,
        repl_ind,
        ib_ind
FROM    v_wh
UNION
SELECT  distinct tl.wh_name wh_name,
        tl.WH_NAME_SECONDARY wh_name_sec,
        w.physical_wh wh,
        w.currency_code currency_code,
        w.physical_wh physical_wh,
        w.primary_vwh primary_wh,
        w.stockholding_ind stockholding_ind,
        w.repl_ind repl_ind,
        w.ib_ind ib_ind
FROM    wh w,
        v_wh vwh,
        v_wh_tl tl
WHERE   w.wh=w.physical_wh
  AND   w.stockholding_ind!='Y'
  AND   w.wh=vwh.physical_wh
  AND   tl.wh=w.wh
ORDER BY 1
/

COMMENT ON TABLE V_SEC_WH IS 'This view will be used to display the physical warehouses and their corresponding virtual warehouses using a security policy to filter user access.'
/
