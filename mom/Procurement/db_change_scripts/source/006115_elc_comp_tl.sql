--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       ELC_COMP_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE ELC_COMP_TL(
LANG NUMBER(6) NOT NULL,
COMP_ID VARCHAR2(10) NOT NULL,
COMP_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ELC_COMP_TL is 'This is the translation table for ELC_COMP table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN ELC_COMP_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN ELC_COMP_TL.COMP_ID is 'Contains a unique user specified code representing the Component.'
/

COMMENT ON COLUMN ELC_COMP_TL.COMP_DESC is 'Contains the name or description of the Component.'
/

COMMENT ON COLUMN ELC_COMP_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN ELC_COMP_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN ELC_COMP_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN ELC_COMP_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE ELC_COMP_TL ADD CONSTRAINT PK_ELC_COMP_TL PRIMARY KEY (
LANG,
COMP_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE ELC_COMP_TL
 ADD CONSTRAINT ELCCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE ELC_COMP_TL ADD CONSTRAINT ELCCT_ELCC_FK FOREIGN KEY (
COMP_ID
) REFERENCES ELC_COMP (
COMP_ID
)
/

