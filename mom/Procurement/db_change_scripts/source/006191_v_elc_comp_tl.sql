CREATE OR REPLACE FORCE VIEW V_ELC_COMP_TL (COMP_ID, COMP_DESC, LANG ) AS
SELECT  b.comp_id,
        case when tl.lang is not null then tl.comp_desc else b.comp_desc end comp_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ELC_COMP b,
        ELC_COMP_TL tl
 WHERE  b.comp_id = tl.comp_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ELC_COMP_TL is 'This is the translation view for base table ELC_COMP. This view fetches data in user langauge either from translation table ELC_COMP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ELC_COMP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ELC_COMP_TL.COMP_ID is 'Contains a unique user specified code representing the Component.'
/

COMMENT ON COLUMN V_ELC_COMP_TL.COMP_DESC is 'Contains the name or description of the Component.'
/

