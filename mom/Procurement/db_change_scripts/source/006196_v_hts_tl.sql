CREATE OR REPLACE FORCE VIEW V_HTS_TL (HTS, IMPORT_COUNTRY_ID, EFFECT_FROM, EFFECT_TO, HTS_DESC, LANG ) AS
SELECT  b.hts,
        b.import_country_id,
        b.effect_from,
        b.effect_to,
        case when tl.lang is not null then tl.hts_desc else b.hts_desc end hts_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  HTS b,
        HTS_TL tl
 WHERE  b.hts = tl.hts (+)
   AND  b.import_country_id = tl.import_country_id (+)
   AND  b.effect_from = tl.effect_from (+)
   AND  b.effect_to = tl.effect_to (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_HTS_TL is 'This is the translation view for base table HTS. This view fetches data in user langauge either from translation table HTS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_HTS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_HTS_TL.HTS is 'Contains the unique HTS item classification number.  This number is standard for the importing country.'
/

COMMENT ON COLUMN V_HTS_TL.IMPORT_COUNTRY_ID is 'Contains the country id of the importing country.'
/

COMMENT ON COLUMN V_HTS_TL.EFFECT_FROM is 'Denotes the beginning of the time period that the HTS classification is valid.'
/

COMMENT ON COLUMN V_HTS_TL.EFFECT_TO is 'Denotes the end of the time period that the HTS classification is valid.'
/

COMMENT ON COLUMN V_HTS_TL.HTS_DESC is 'The HTS description of the item classification.'
/

