--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ORDLOC'

PROMPT Creating Index 'ORDLOC_I2'
CREATE INDEX ORDLOC_I2 on ORDLOC
  (LOCATION
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

