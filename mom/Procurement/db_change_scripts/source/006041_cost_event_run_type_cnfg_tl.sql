--------------------------------------------------------
-- Copyright (c) 2011, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				COST_EVENT_RUN_TYPE_CNFG_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       ADDING TYPE
--------------------------------------
PROMPT Creating Table 'COST_EVENT_RUN_TYPE_CNFG_TL'
CREATE TABLE COST_EVENT_RUN_TYPE_CNFG_TL(
LANG NUMBER(6) NOT NULL,
EVENT_TYPE VARCHAR2(3) NOT NULL,
EVENT_DESC VARCHAR2(255) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE COST_EVENT_RUN_TYPE_CNFG_TL is 'This is the translation table for COST_EVENT_RUN_TYPE_CONFIG table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.EVENT_TYPE is 'The type of the cost event.  Options are: SC for supplier cost events, NIL for new item locations, PP for primary pack change cost events, CC for cost change cost events, R for reclassification cost events, D for deal cost events, MH for merchandise hierarchy cost events, OH for organizational hierarchy cost events, CZ for cost zone cost events, ELC for estimated cost cost events, SH for supplier hierarchy cost events, ICZ item cost zone group cost events'
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.EVENT_DESC is 'Holds the description of the cost event types.'
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN COST_EVENT_RUN_TYPE_CNFG_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE COST_EVENT_RUN_TYPE_CNFG_TL ADD CONSTRAINT PK_COST_EVENT_RUN_TYPE_CNFG_TL PRIMARY KEY (
LANG,
EVENT_TYPE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE COST_EVENT_RUN_TYPE_CNFG_TL
 ADD CONSTRAINT CERTCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE COST_EVENT_RUN_TYPE_CNFG_TL ADD CONSTRAINT CERTCT_CERTC_FK FOREIGN KEY (
EVENT_TYPE
) REFERENCES COST_EVENT_RUN_TYPE_CONFIG (
EVENT_TYPE
)
/

