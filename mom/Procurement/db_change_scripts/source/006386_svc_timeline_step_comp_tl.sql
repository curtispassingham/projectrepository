--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_TIMELINE_STEP_COMP_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT CREATING TABLE 'SVC_TIMELINE_STEP_COMP_TL'
CREATE TABLE SVC_TIMELINE_STEP_COMP_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, TIMELINE_TYPE VARCHAR2(6)
, STEP_NO NUMBER(4)
, STEP_DESC VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_TIMELINE_STEP_COMP_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in TIMELINE_STEP_COMP_TL.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.TIMELINE_TYPE is 'This field will hold the module the timeline is referring to. Valid values are: PO - Purchase Orders at the Header Level POIT - Purchase Orders at the Item Level IT - Item Maintenance CE - Customs Entry LO - Logistics at the Header Level LOBL - Logistics at the Bill of Lading Level LOCO - Logistics at the Container Level LOPI - Logistics at the Purchase Order/Item Level LOCI - Logistics at the Commercial Invoice Level'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.STEP_NO is 'This field contains the number that uniquely defines the timeline step.'
/

COMMENT ON COLUMN SVC_TIMELINE_STEP_COMP_TL.STEP_DESC is 'This field contains the description of the timeline step.'
/

PROMPT CREATING PRIMARY KEY ON 'SVC_TIMELINE_STEP_COMP_TL'
ALTER TABLE SVC_TIMELINE_STEP_COMP_TL
ADD CONSTRAINT SVC_TIMELINE_STEP_COMP_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING UNIQUE KEY ON 'SVC_TIMELINE_STEP_COMP_TL'
ALTER TABLE SVC_TIMELINE_STEP_COMP_TL
ADD CONSTRAINT SVC_TIMELINE_STEP_COMP_TL_UK UNIQUE
(LANG, TIMELINE_TYPE, STEP_NO)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

