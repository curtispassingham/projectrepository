--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       HTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE HTS_TL(
LANG NUMBER(6) NOT NULL,
HTS VARCHAR2(25) NOT NULL,
IMPORT_COUNTRY_ID VARCHAR2(3) NOT NULL,
EFFECT_FROM DATE NOT NULL,
EFFECT_TO DATE NOT NULL,
HTS_DESC VARCHAR2(2000) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE HTS_TL is 'This is the translation table for HTS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN HTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN HTS_TL.HTS is 'Contains the unique HTS item classification number.  This number is standard for the importing country.'
/

COMMENT ON COLUMN HTS_TL.IMPORT_COUNTRY_ID is 'Contains the country id of the importing country.'
/

COMMENT ON COLUMN HTS_TL.EFFECT_FROM is 'Denotes the beginning of the time period that the HTS classification is valid.'
/

COMMENT ON COLUMN HTS_TL.EFFECT_TO is 'Denotes the end of the time period that the HTS classification is valid.'
/

COMMENT ON COLUMN HTS_TL.HTS_DESC is 'The HTS description of the item classification.'
/

COMMENT ON COLUMN HTS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN HTS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN HTS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN HTS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE HTS_TL ADD CONSTRAINT PK_HTS_TL PRIMARY KEY (
LANG,
HTS,
IMPORT_COUNTRY_ID,
EFFECT_FROM,
EFFECT_TO
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE HTS_TL
 ADD CONSTRAINT HTST_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE HTS_TL ADD CONSTRAINT HTST_HTS_FK FOREIGN KEY (
HTS,
IMPORT_COUNTRY_ID,
EFFECT_FROM,
EFFECT_TO
) REFERENCES HTS (
HTS,
IMPORT_COUNTRY_ID,
EFFECT_FROM,
EFFECT_TO
)
/

