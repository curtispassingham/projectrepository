--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       SCAC_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE SCAC_TL(
LANG NUMBER(6) NOT NULL,
SCAC_CODE VARCHAR2(6) NOT NULL,
SCAC_CODE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SCAC_TL is 'This is the translation table for SCAC table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN SCAC_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SCAC_TL.SCAC_CODE is 'Contains the unique key that identifies the scac record.'
/

COMMENT ON COLUMN SCAC_TL.SCAC_CODE_DESC is 'Contains the description of the SCAC Code.'
/

COMMENT ON COLUMN SCAC_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN SCAC_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN SCAC_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN SCAC_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE SCAC_TL ADD CONSTRAINT PK_SCAC_TL PRIMARY KEY (
LANG,
SCAC_CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE SCAC_TL
 ADD CONSTRAINT SCACT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE SCAC_TL ADD CONSTRAINT SCACT_SCAC_FK FOREIGN KEY (
SCAC_CODE
) REFERENCES SCAC (
SCAC_CODE
)
/

