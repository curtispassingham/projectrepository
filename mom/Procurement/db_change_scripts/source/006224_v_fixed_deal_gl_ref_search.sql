--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_FIXED_DEAL_GL_REF_SEARCH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_FIXED_DEAL_GL_REF_SEARCH'
CREATE OR REPLACE FORCE VIEW "V_FIXED_DEAL_GL_REF_SEARCH"
("DEAL",
 "DEAL_DESC",
 "VENDOR_TYPE",
 "VENDOR",
 "VENDOR_NAME",
 "SUPPLIER",
 "SUPPLIER_NAME",
 "PARTNER",
 "PARTNER_NAME",
 "DEPT",
 "CLASS",
 "SUBCLASS",
 "CONTRIB_RATIO",
 "CONTRIB_AMOUNT",
 "FIXED_DEAL_AMT",
 "CURRENCY_CODE",
 "COLLECT_DATE",
 "REFERENCE_TRACE_ID",
 "SET_OF_BOOKS_ID",
 "LOC_TYPE",
 "LOCATION",
 "LOCATION_NAME",
 "DEPT_NAME",
 "CLASS_NAME",
 "SUBCLASS_NAME")
AS (
  select fd.deal_no,
       fix.deal_desc,
       'S' vendor_type,
       to_char(s.supplier) vendor,
       s.sup_name vendor_name,
       s.supplier supplier,
       s.sup_name supplier_name,
       null partner,
       null partner_name,
       fd.dept,
       fd.class,
       fd.subclass,
       fd.contrib_ratio,
       fd.contrib_amount,
       fd.fixed_deal_amt,
       fix.currency_code currency_code,
       fd.collect_date,
       fd.reference_trace_id,
       fd.set_of_books_id,
       fd.loc_type,
       fd.location,
       (select loc.location_name
          from v_location loc
         where loc.location_id = fd.location) location_name,
       (select dept_name
          from v_deps d
         where d.dept = fd.dept) dept_name,
       (select class_name
          from v_class c
         where c.dept  = fd.dept
           and c.class = fd.class) class_name,
       (select sub_name
          from v_subclass sub
         where sub.dept     = fd.dept
           and sub.class    = fd.class
           and sub.subclass = fd.subclass) subclass_name
  from fixed_deal_gl_ref_data fd,
       v_sups s,
       fixed_deal fix
 where fd.deal_no       = fix.deal_no
   and fix.supplier     = s.supplier
   and fix.partner_type = 'S'
union all
select fd.deal_no,
       fix.deal_desc,
       p.partner_type vendor_type,
       p.partner_id vendor,
       p.partner_desc vendor_name,
       null supplier,
       null supplier_name,
       p.partner_id partner,
       p.partner_desc partner_name,
       fd.dept,
       fd.class,
       fd.subclass,
       fd.contrib_ratio,
       fd.contrib_amount,
       fd.fixed_deal_amt,
       fix.currency_code currency_code,
       fd.collect_date,
       fd.reference_trace_id,
       fd.set_of_books_id,
       fd.loc_type,
       fd.location,
       (select loc.location_name
          from v_location loc
         where loc.location_id = fd.location) location_name,
       (select dept_name
          from v_deps d
         where d.dept = fd.dept) dept_name,
       (select class_name
          from v_class c
         where c.dept  = fd.dept
           and c.class = fd.class) class_name,
       (select sub_name
          from v_subclass sub
         where sub.dept     = fd.dept
           and sub.class    = fd.class
           and sub.subclass = fd.subclass) subclass_name
  from fixed_deal_gl_ref_data fd,
       fixed_deal fix,
       v_partner p
 where fd.deal_no       = fix.deal_no
   and fix.partner_id   = p.partner_id
   and fix.partner_type = p.partner_type
   and fix.partner_type != 'S')
/

COMMENT ON TABLE V_FIXED_DEAL_GL_REF_SEARCH IS 'This view is used for the Fixed Deal Transaction Data screen in RMS Alloy.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."DEAL" IS 'This field will hold the deal_no of the fixed deal record stored in the FIXED_DEAL table.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."DEAL_DESC" IS 'Contains the description of the fixed deal.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."VENDOR_TYPE" IS 'Holds the partner type or S for Supplier.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."VENDOR" IS 'Holds the supplier or partner for which the Fixed Deal is related to.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."VENDOR_NAME" IS 'Contains the translated supplier or partner name.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."SUPPLIER" IS 'Unique identifying number for a supplier within the system.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."SUPPLIER_NAME" IS 'Contains the suppliers translated trading name.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."PARTNER" IS 'Unique identifying number for a partner within the system.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."PARTNER_NAME" IS 'Contains the translated partner description or name.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."DEPT" IS 'This field will hold the dept number.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."CLASS" IS 'This field will hold the class number.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."SUBCLASS" IS 'This field will hold the subclass number.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."CONTRIB_RATIO" IS 'The Fixed Deal Contribution Ratio.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."CONTRIB_AMOUNT" IS 'The Fixed Deal Contribution Amount.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."FIXED_DEAL_AMT" IS 'This field will hold the amount of the fixed deal record.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."CURRENCY_CODE" IS 'This column will contain the currency code of the deal.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."COLLECT_DATE" IS 'This field will hold the particular collection day information.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."REFERENCE_TRACE_ID" IS 'This field will hold the Reference trace ID. This is used for drill back and traceability purposes.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."SET_OF_BOOKS_ID" IS 'This field will hold the MSOB ID linked to the deal data record.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."LOC_TYPE" IS 'This field will hold the type of the location (either store or warehouse).'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."LOCATION" IS 'This field will hold the location number (store or warehouse).'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."LOCATION_NAME" IS 'Contains the translated name of the store or warehouse.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."DEPT_NAME" IS 'Contains the translated name of the department.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."CLASS_NAME" IS 'Contains the translated name of the class.'
/

COMMENT ON COLUMN V_FIXED_DEAL_GL_REF_SEARCH."SUBCLASS_NAME" IS 'Contains the translated name of the subclass.'
/


