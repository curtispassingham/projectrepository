--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
-- ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- Table Modified: COST_CHANGE_TEMP2
----------------------------------------------------------------------------


whenever sqlerror exit

PROMPT Dropping Table 'COST_CHANGE_TEMP2' 
DECLARE
  L_table_exists number := 0;
BEGIN
  SELECT count(*) INTO L_table_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'COST_CHANGE_TEMP2';

  if (L_table_exists != 0) then
      execute immediate 'DROP TABLE COST_CHANGE_TEMP2';
  end if;
end;
/

--------------------------------------
--       ADDING TABLE
--------------------------------------
PROMPT Creating Table 'COST_CHANGE_TEMP2'
CREATE GLOBAL TEMPORARY TABLE COST_CHANGE_TEMP2 
(
  COST_CHANGE NUMBER(8) ,
  ITEM VARCHAR2(25) ,
  SUPPLIER NUMBER(10) ,
  ORIGIN_COUNTRY_ID VARCHAR2(3) ,
  LOC NUMBER(10) ,
  LOC_TYPE VARCHAR2(1) ,
  UNIT_COST NUMBER(20, 4) ,
  COST_CHANGE_TYPE VARCHAR2(2) ,
  COST_CHANGE_VALUE NUMBER(20, 4) ,
  RECALC_ORD_IND VARCHAR2(1) ,
  ISC_ROWID ROWID ,
  ISC_UNIT_COST NUMBER(20, 4) ,
  ISCL_ROWID ROWID ,
  ISCL_UNIT_COST NUMBER(20, 4) ,
  ISCL_PRIM_LOC_IND VARCHAR2(1) ,
  DEPT NUMBER(4) ,
  CLASS NUMBER(4) ,
  SUBCLASS NUMBER(4) ,
  STATUS VARCHAR2(1) ,
  PACK_IND VARCHAR2(1) ,
  CHILD_IND VARCHAR2(1) ,
  TRAN_LEVEL_ITEM_IND VARCHAR2(1) ,
  SUP_CURRENCY VARCHAR2(3) ,
  LOC_CURRENCY VARCHAR2(3) ,
  DELIVERY_COUNTRY_ID VARCHAR2(3) ,
  NEGOTIATED_ITEM_COST NUMBER(20, 4) ,
  EXTENDED_BASE_COST NUMBER(20, 4) ,
  INCLUSIVE_COST NUMBER(20, 4) ,
  BASE_COST NUMBER(20, 4) 
) 
ON COMMIT PRESERVE ROWS
/ 

COMMENT ON TABLE COST_CHANGE_TEMP2 IS 'Staging table for supplier cost change extract batch (sccext.pc) containing cost changes with location information.' 
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.COST_CHANGE IS 'Cost change number from the cost_susp_sup_detail or cost_susp_sup_detail_loc'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ITEM IS 'Item under cost change'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.SUPPLIER IS 'Supplier under cost change'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ORIGIN_COUNTRY_ID IS 'Country ID under cost change'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.LOC IS 'Location information'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.LOC_TYPE IS 'Location type information'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.UNIT_COST IS 'New unit cost from the cost_susp_sup_detail or cost_susp_sup_detail_loc table'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.COST_CHANGE_TYPE IS 'Cost change type from cost change tables'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.COST_CHANGE_VALUE IS 'Cost change value from the cost change tables'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.RECALC_ORD_IND IS 'Order recalculation indicator from the cost change tables'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ISC_ROWID IS 'ITEM_SUPP_COUNTRY rowid corresponding to the item under cost change'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ISC_UNIT_COST IS 'Item supplier country  unit cost'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ISCL_ROWID IS 'ITEM_SUPP_COUNTRY_LOC rowid corresponding to the item under cost change'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ISCL_UNIT_COST IS 'Item supplier country loc unit cost'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.ISCL_PRIM_LOC_IND IS 'Item supplier country loc primary location indicator'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.DEPT IS 'Item department information'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.CLASS IS 'Item class information'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.SUBCLASS IS 'Item subclass information'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.STATUS IS 'Item status information'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.PACK_IND IS 'Pack indicator'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.CHILD_IND IS 'Child item indicator'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.TRAN_LEVEL_ITEM_IND IS 'Transaction level item indicator'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.SUP_CURRENCY IS 'Supplier currency'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.LOC_CURRENCY IS 'Location currency'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.DELIVERY_COUNTRY_ID IS 'Country to which the item  will be delivered to.'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.NEGOTIATED_ITEM_COST IS 'This will hold the supplier negotiated item cost for the primary delivery country of the item. This is a column added to help bulk processing when integrated with an external tax solution.'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.EXTENDED_BASE_COST IS 'This will hold the extended base cost for the primary delivery country of the item. Extended base cost is the cost inclusive of all the taxes that affect the WAC. This is a column added to help bulk processing when integrated with an external tax solution.'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.INCLUSIVE_COST IS 'This will hold the inclusive cost for the primary delivery country of the item. This cost will have both the recoverable and non recoverable taxes included. This is a column added to help bulk processing when integrated with an external tax solution.'
/ 

COMMENT ON COLUMN COST_CHANGE_TEMP2.BASE_COST IS 'This field will hold the tax exclusive cost of the item. This is a column added to help bulk processing when integrated with an external tax solution.'
/
