CREATE OR REPLACE FORCE VIEW V_COST_EVENT_RUN_TYPE_CNFG_TL (EVENT_TYPE, EVENT_DESC, LANG ) AS
SELECT  b.event_type,
        case when tl.lang is not null then tl.event_desc else b.event_desc end event_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  COST_EVENT_RUN_TYPE_CONFIG b,
        COST_EVENT_RUN_TYPE_CNFG_TL tl
 WHERE  b.event_type = tl.event_type (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_COST_EVENT_RUN_TYPE_CNFG_TL is 'This is the translation view for base table COST_EVENT_RUN_TYPE_CONFIG. This view fetches data in user langauge either from translation table COST_EVENT_RUN_TYPE_CNFG_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_COST_EVENT_RUN_TYPE_CNFG_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_COST_EVENT_RUN_TYPE_CNFG_TL.EVENT_TYPE is 'The type of the cost event.  Options are: SC for supplier cost events, NIL for new item locations, PP for primary pack change cost events, CC for cost change cost events, R for reclassification cost events, D for deal cost events, MH for merchandise hierarchy cost events, OH for organizational hierarchy cost events, CZ for cost zone cost events, ELC for estimated cost cost events, SH for supplier hierarchy cost events, ICZ item cost zone group cost events'
/

COMMENT ON COLUMN V_COST_EVENT_RUN_TYPE_CNFG_TL.EVENT_DESC is 'Holds the description of the cost event types.'
/

