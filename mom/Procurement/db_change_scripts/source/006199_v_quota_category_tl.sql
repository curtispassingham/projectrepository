CREATE OR REPLACE FORCE VIEW V_QUOTA_CATEGORY_TL (QUOTA_CAT, IMPORT_COUNTRY_ID, CATEGORY_DESC, LANG ) AS
SELECT  b.quota_cat,
        b.import_country_id,
        case when tl.lang is not null then tl.category_desc else b.category_desc end category_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  QUOTA_CATEGORY b,
        QUOTA_CATEGORY_TL tl
 WHERE  b.quota_cat = tl.quota_cat (+)
   AND  b.import_country_id = tl.import_country_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_QUOTA_CATEGORY_TL is 'This is the translation view for base table QUOTA_CATEGORY. This view fetches data in user langauge either from translation table QUOTA_CATEGORY_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_QUOTA_CATEGORY_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_QUOTA_CATEGORY_TL.QUOTA_CAT is 'Contains the unique code identification.'
/

COMMENT ON COLUMN V_QUOTA_CATEGORY_TL.IMPORT_COUNTRY_ID is 'Contains the country identifier for the country that is receiving the product.'
/

COMMENT ON COLUMN V_QUOTA_CATEGORY_TL.CATEGORY_DESC is 'Contains the description of the quota category.'
/

