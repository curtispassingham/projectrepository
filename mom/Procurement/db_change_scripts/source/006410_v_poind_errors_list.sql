--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:          V_POIND_ERRORS_LIST
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Updating View 'V_POIND_ERRORS_LIST'
CREATE OR REPLACE FORCE VIEW V_POIND_ERRORS_LIST AS
   select ed.process_id                                      process_id,
          ed.template_key                                    template_key,
          ed.error_seq                                       error_seq,
          ed.table_name                                      table_name,
          NVL(wd.wksht_name,ed.table_name)                AS table_desc,
          ed.column_name                                     column_name,
          cd.column_name                                  AS column_desc,
          ed.row_seq                                         row_seq,
          NVL(ed.pr_error_type, 'E')                         error_type,
          ed.error_key                                       error_key,
          ITEM_INDUCT_SQL.GET_ERROR_DISPLAY(ed.error_key) AS error_msg,
          ed.order_no                                        order_no,
          ed.supplier                                        supplier,
          sups.sup_name                                      sup_name,
          vsups.sup_name                                     sup_name_trans,
          ed.location                                        location,
          loc.loc_name                                       loc_name,
          loc.loc_name_trans                                 loc_name_trans,
          ed.item                                            item,
          im.item_desc                                       item_desc,
          vim.item_desc                                      item_desc_trans
     from (
           -- Get errors that occured during spreadsheet to staging processing
           select 'S9T2STG'        AS error_type,
                  pt.process_id,
                  pt.template_key,
                  se.error_seq_no  AS error_seq,
                  se.wksht_key     AS table_name,
                  se.column_key    AS column_name,
                  se.row_seq,
                  se.error_type    AS pr_error_type,
                  se.error_key,
                  NULL             AS order_no,
                  NULL             AS supplier,
                  NULL             AS location,
                  NULL             AS item
             from svc_process_tracker pt,
                  s9t_errors se
            where pt.file_id = se.file_id
            union all
           -- Get errors that occured during staging to RMS processing
           select 'STG2RMS'                             AS error_type,
                  pt.process_id,
                  pt.template_key,
                  se.error_seq,
                  NVL(tbl_map.table_name,se.table_name) AS table_name,
                  se.column_name                        AS column_name,
                  se.row_seq,
                  se.error_type                         AS pr_error_type,
                  se.error_msg                          AS error_key,
                  se.order_no                           AS order_no,
                  se.supplier                           AS supplier,
                  se.location                           AS location,
                  se.item                               AS item
             from svc_process_tracker pt,
                  (select process_id,
                          table_name,
                          order_no,
                          supplier,
                          location,
                          item,
                          error_type,
                          error_msg,
                          row_seq,
                          column_name,
                          error_seq
                     from coresvc_po_err) se,
                  (select 'ORDHEAD'       AS table_name,
                          'SVC_ORDHEAD'   AS svc_table_name
                     from dual
                    union all
                   select 'ORDDETAIL'      AS table_name,
                          'SVC_ORDDETAIL'  AS svc_table_name
                     from dual
                    union all
                   select 'ORDHEAD_CFA_EXT'       AS table_name,
                          'SVC_ORDHEAD_CFA_EXT'   AS svc_table_name
                     from dual
                    union all
                   select 'ORDLC'       AS table_name,
                          'SVC_ORDLC'   AS svc_table_name
                     from dual
                    union all
                   select 'ORDLOC_EXP'       AS table_name,
                          'SVC_ORDLOC_EXP'   AS svc_table_name
                     from dual
                    union all
                   select 'ORDSKU_HTS'       AS table_name,
                          'SVC_ORDSKU_HTS'   AS svc_table_name
                     from dual
                    union all
                   select 'ORDSKU_HTS_ASSESS'       AS table_name,
                          'SVC_ORDSKU_HTS_ASSESS'   AS svc_table_name
                     from dual) tbl_map
            where pt.process_id = se.process_id
              and se.table_name = tbl_map.svc_table_name(+)
          ) ed,
          (
            -- Get location name
            select s.store      AS location,
                   s.store_name    loc_name,
                   v.store_name    loc_name_trans
              from store s, 
                   v_store_tl v
             where s.store = v.store
             union all 
            select w.wh         AS location,
                   w.wh_name       loc_name,
                   v.wh_name       loc_name_trans
              from wh w,
                   v_wh_tl v
             where w.wh = v.wh
          ) loc,
          v_s9t_tmpl_wksht_def wd,
          v_s9t_tmpl_cols_def cd,
          item_master im,
          v_item_master_tl vim,
          sups,
          v_sups_tl vsups
    where ed.template_key = wd.template_key (+)
      and ed.table_name   = wd.wksht_key (+)
      and ed.column_name  = cd.column_key (+)
      and ed.template_key = cd.template_key (+)
      and ed.table_name   = cd.wksht_key (+)
      and ed.item         = im.item (+)
      and ed.item         = vim.item (+)
      and ed.supplier     = sups.supplier (+)
      and ed.supplier     = vsups.supplier (+)
      and ed.location     = loc.location (+)
     /
	 