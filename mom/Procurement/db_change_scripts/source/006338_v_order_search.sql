CREATE OR REPLACE FORCE VIEW V_ORDER_SEARCH 
(ORDER_NO,
 STATUS,
 SUPPLIER_SITE,
 SUPPLIER_SITE_NAME,
 SUPPLIER_PARENT,
 SUPPLIER_PARENT_NAME,
 NOT_BEFORE_DATE,
 NOT_AFTER_DATE,
 OTB_EOW_DATE,
 WRITTEN_DATE,
 ORDER_TYPE,
 CURRENCY_CODE,
 IMPORT_ORDER_IND,
 IMPORT_COUNTRY_ID,
 CONTRACT_NO,
 ORIG_IND,              
 SPLIT_REF_ORDNO,
 VENDOR_ORDER_NO,
 PO_TYPE,
 PROMOTION, 
 BUYER,
 POOL_SUP_SITE,
 POOL_SUP_SITE_NAME,
 POOL_SUP_PARENT,
 POOL_SUP_PARENT_NAME,
 ITEM,
 ITEM_DESC,
 LOCATION,
 LOCATION_TYPE,
 PHYSICAL_LOC,
 DIVISION,
 GROUP_NO,
 DEPT,
 CLASS,
 SUBCLASS,
 CHAIN,
 AREA,
 REGION,
 DISTRICT,
 TSF_PO_LINK_NO,
 CREATE_ID,
 TERMS
 )
AS
WITH ord AS (SELECT o.order_no,
                    o.status,
                    o.supplier supplier_site,
                    (SELECT vss.sup_name
                       FROM v_sups_tl vss
                      WHERE vss.supplier = ss.supplier) supplier_site_name,
                    sp.supplier supplier_parent,
                    (SELECT vsp.sup_name
                       FROM v_sups_tl vsp
                      WHERE vsp.supplier = sp.supplier) supplier_parent_name,
                    o.NOT_BEFORE_DATE,
                    o.NOT_AFTER_DATE,
                    o.OTB_EOW_DATE,
                    o.written_date,
                    o.order_type,
                    o.currency_code,
                    o.import_order_ind,
                    o.import_country_id,
                    o.contract_no,
                    o.orig_ind ,
                    o.SPLIT_REF_ORDNO,
                    o.vendor_order_no,
                    o.po_type,
                    o.Promotion,
                    o.buyer,
                    o.item,
                    o.location,
                    o.loc_type,
                    o.dept,
                    oim.pool_sup_site,
                    oim.pool_sup_site_name,
                    oim.pool_sup_parent,
                    oim.pool_sup_parent_name,
                    o.create_id,
                    o.terms
               FROM ordhead o,
                    v_sups ss,
                    v_sups sp,
                    (SELECT oi.order_no,
                            oi.pool_supplier pool_sup_site,
                            (SELECT vss.sup_name
                               FROM v_sups_tl vss
                              WHERE vss.supplier = ss.supplier) pool_sup_site_name,
                            sp.supplier pool_sup_parent,
                            (SELECT vsp.sup_name
                               FROM v_sups_tl vsp
                              WHERE vsp.supplier = sp.supplier) pool_sup_parent_name
                       FROM ord_inv_mgmt oi,
                            v_sups ss,
                            v_sups sp
                      WHERE oi.pool_supplier  = ss.Supplier
                        AND ss.Supplier_Parent  = sp.supplier
                        AND sp.supplier_parent IS NULL) oim
              WHERE o.supplier        = ss.Supplier
                AND ss.Supplier_Parent  = sp.supplier
                AND sp.supplier_parent IS NULL
                AND o.order_no          = oim.order_no(+)),
  loc AS (SELECT vs.store location,
                 'S' location_type,
                 vs.store phy_loc,
                 vs.chain,
                 vs.area,
                 vs.region,
                 vs.district
            FROM v_store vs
          UNION ALL
          SELECT w.wh location,
                 'W' location_type,
                 w.physical_wh phy_loc,
                 -999 chain,
                 -999 area,
                 -999 region,
                 -999 district
            FROM v_wh w)
SELECT ord.order_no,
       ord.status,
       ord.supplier_site,
       ord.supplier_site_name,
       ord.supplier_parent,
       ord.supplier_parent_name,
       ord.not_before_date,
       ord.not_after_date,
       ord.OTB_EOW_DATE,
       ord.written_date,
       ord.order_type,
       ord.currency_code,
       ord.import_order_ind,
       ord.import_country_id,
       ord.contract_no,
       ord.orig_ind ,
       ord.SPLIT_REF_ORDNO,
       ord.vendor_order_no,
       ord.po_type,
       ord.Promotion,
       ord.buyer,
       ord.pool_sup_site,
       ord.pool_sup_site_name,
       ord.pool_sup_parent,
       ord.pool_sup_parent_name,
       ol.item,
       ol.item_desc,
       ol.location,
       ol.location_type,
       ol.phy_loc,
       ol.division,
       ol.group_no,
       ol.dept,
       ol.class,
       ol.subclass,
       ol.chain,
       ol.area,
       ol.region,
       ol.district,
       ol.tsf_po_link_no,
       ord.create_id,
       ord.terms
FROM ord,
    (SELECT olo.order_no,
            olo.item,
            (SELECT itl.item_desc
               FROM v_item_master_tl itl
              WHERE itl.item = olo.item) item_desc,
            im.dept,
            im.class,
            im.subclass,
            im.division,
            im.group_no,
            loc.location,
            loc.phy_loc,
            loc.location_type,
            loc.chain,
            loc.area,
            loc.region,
            loc.district,
            olo.tsf_po_link_no
       FROM Ordloc olo,
            v_item_master im,
            loc
      WHERE olo.item   = im.item
        AND olo.location = loc.location
        AND olo.loc_type = loc.location_type) ol
WHERE ord.order_no = ol.order_no
UNION ALL
SELECT ord.order_no,
       ord.status,
       ord.supplier_site,
       ord.supplier_site_name,
       ord.supplier_parent,
       ord.supplier_parent_name,
       ord.not_before_date,
       ord.not_after_date,
       ord.OTB_EOW_DATE,
       ord.written_date,
       ord.order_type,
       ord.currency_code,
       ord.import_order_ind,
       ord.import_country_id,
       ord.contract_no,
       ord.orig_ind ,
       ord.SPLIT_REF_ORDNO,
       ord.vendor_order_no,
       ord.po_type,
       ord.Promotion,
       ord.buyer,
       ord.pool_sup_site,
       ord.pool_sup_site_name,
       ord.pool_sup_parent,
       ord.pool_sup_parent_name,
       ord.item,
       (SELECT itl.item_desc
          FROM v_item_master_tl itl
         WHERE itl.item = ord.item) item_desc,
       ord.location,
       ord.loc_type,
       loc.phy_loc,
       im.division,
       im.group_no,
       im.dept,
       im.class,
       im.subclass,
       loc.chain,
       loc.area,
       loc.region,
       loc.district,
       NULL tsf_po_link_no,
       ord.create_id,
       ord.terms
  FROM ord,
       v_item_master im,
       loc
 WHERE ord.item   = im.item
   AND ord.location = loc.location
   AND ord.loc_type = loc.location_type
   AND NOT EXISTS (SELECT 'X'
                     FROM ordloc ol
                    WHERE ol.order_no = ord.order_no
                      AND rownum = 1)
UNION ALL
SELECT ord.order_no,
       ord.status,
       ord.supplier_site,
       ord.supplier_site_name,
       ord.supplier_parent,
       ord.supplier_parent_name,
       ord.not_before_date,
       ord.not_after_date,
       ord.OTB_EOW_DATE,
       ord.written_date,
       ord.order_type,
       ord.currency_code,
       ord.import_order_ind,
       ord.import_country_id,
       ord.contract_no,
       ord.orig_ind ,
       ord.SPLIT_REF_ORDNO,
       ord.vendor_order_no,
       ord.po_type,
       ord.Promotion,
       ord.buyer,
       ord.pool_sup_site,
       ord.pool_sup_site_name,
       ord.pool_sup_parent,
       ord.pool_sup_parent_name,
       ord.item,
       (SELECT itl.item_desc
          FROM v_item_master_tl itl
         WHERE itl.item = ord.item) item_desc,
       ord.location,
       ord.loc_type,
       loc.phy_loc,
       g.division,
       deps.group_no,
       ord.dept,
       NULL,
       NULL,
       loc.chain,
       loc.area,
       loc.region,
       loc.district,
       NULL tsf_po_link_no,
       ord.create_id,
       ord.terms
  FROM ord,
       loc,
       deps,
       groups g
 WHERE ord.location = loc.location(+)
   AND ord.loc_type = loc.location_type(+)
   AND ord.item is NULL
   AND ord.dept = deps.dept(+)
   AND deps.group_no = g.group_no(+)
   AND NOT EXISTS (SELECT 'X'
                     FROM ordloc ol
                    WHERE ol.order_no = ord.order_no
                    AND rownum = 1)
/

COMMENT ON TABLE V_ORDER_SEARCH IS 'This view is used for the order search screen in RMS Alloy. It includes all fields that can be used as item search criteria.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ORDER_NO IS 'Purchase Order Number.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.STATUS IS 'The current status of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_SITE IS 'The supplier site where the Purchase Order is placed.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_SITE_NAME IS 'Translated name of the supplier site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_PARENT IS 'Parent Supplier of the Supplier Site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUPPLIER_PARENT_NAME IS 'Translated name of the Parent Supplier.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.NOT_BEFORE_DATE IS 'Contains the first date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.NOT_AFTER_DATE IS 'Contains the last date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.OTB_EOW_DATE IS 'Contains the OTB budget bucket the order amount should be placed into.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.WRITTEN_DATE IS 'Contains the date the Purchase order was created within the system.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ORDER_TYPE IS 'Indicates which Open To Buy bucket would be updated.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CURRENCY_CODE IS 'Currency of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.IMPORT_ORDER_IND IS 'Indicates if the purchase order is an import order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.IMPORT_COUNTRY_ID IS 'The identifier of the country into which the items on the order are being imported.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CONTRACT_NO IS 'Contains the contract number associated with This order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ORIG_IND IS 'Indicates where the Purchase Order originated.'
/  
            
COMMENT ON COLUMN V_ORDER_SEARCH.SPLIT_REF_ORDNO IS 'This column will store the original order number from which the split orders were generated from.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.VENDOR_ORDER_NO IS 'Contains the vendor number who will provide the merchandise specified in the order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.PO_TYPE IS 'Contains the context of the Purchase Order creation.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.PROMOTION IS 'Contains the promotion number associated with the order.' 
/

COMMENT ON COLUMN V_ORDER_SEARCH.BUYER IS 'Contains the number associated with the buyer for the order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_SITE IS 'This field will contain the ID for a pooled supplier site if the order was created as a pooled order.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_SITE_NAME IS 'This field will contain the translated name of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_PARENT IS 'This field will contain the parent of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.POOL_SUP_PARENT_NAME IS 'This field will contain the translated name of the pooled supplier parent.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ITEM IS 'The item for which the Purchase Order IS created.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.ITEM_DESC IS 'Description of the Item.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.LOCATION IS 'The location for which the Purchase Order is created.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.LOCATION_TYPE IS 'This indicates the type of location.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.PHYSICAL_LOC IS 'This contains the physical location of the location.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.DIVISION IS 'This contains the division to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.GROUP_NO IS 'This contains the group to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.DEPT IS 'This contains the department to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CLASS IS 'This contains the class to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.SUBCLASS IS 'This contains the subclass to which the item belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.CHAIN IS 'This contains the chain to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.AREA IS 'This contains the area to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.REGION IS 'This contains the region to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.DISTRICT IS 'This contains the district to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDER_SEARCH.TSF_PO_LINK_NO IS 'Reference number to link the item on the purchase orders to transfer.'
/
COMMENT ON COLUMN V_ORDER_SEARCH.CREATE_ID IS 'This column holds the user ID of the user who created the record.'
/
COMMENT ON COLUMN V_ORDER_SEARCH.TERMS IS 'Indicator identifying the sales terms for the order. These terms specify when payment is due and if any discounts exist for early payment.'
/
