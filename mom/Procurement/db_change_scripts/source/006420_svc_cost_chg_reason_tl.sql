--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 SVC_COST_CHG_REASON_TL
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'SVC_COST_CHG_REASON_TL'
CREATE TABLE SVC_COST_CHG_REASON_TL
 (PROCESS_ID NUMBER(10) NOT NULL,
  CHUNK_ID NUMBER(10) NOT NULL,
  ROW_SEQ NUMBER(20) NOT NULL,
  ACTION VARCHAR2(10 ),
  PROCESS$STATUS VARCHAR2(10 ),
  LANG NUMBER(6),
  REASON NUMBER(2),
  REASON_DESC VARCHAR2(120 ),
  ORIG_LANG_IND VARCHAR2(1 ),
  REVIEWED_IND VARCHAR2(1 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_COST_CHG_REASON_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated to COST_CHG_REASON_TL.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.LANG is 'This field contains the language in which the translated text is maintained.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.REASON is 'Contains the number which uniquely identifies the reason for the cost change.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.REASON_DESC is 'Holds the description of the cost change reason code in a given language.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.ORIG_LANG_IND is 'Indicates if the description is in the original language entered for the inventory status type. It is set to ''''Y'''' when the first record is written to the table for the inventory status types.'
/

COMMENT ON COLUMN SVC_COST_CHG_REASON_TL.REVIEWED_IND is 'Indicates if the description needs to be reviewed for translation. It is set to ''''N'''' when the description in the original language is inserted or updated. We assume that clients will regularly run reports on all strings that are not reviewed (i.e. reviewed_ind = ''''N''''). When translation either provides a new string, or OKs that the existing string is correct, the reviewed_ind should be set to ''''Y''''.'
/


PROMPT Creating Primary Key on 'SVC_COST_CHG_REASON_TL'
ALTER TABLE SVC_COST_CHG_REASON_TL
 ADD CONSTRAINT SVC_COST_CHG_REASON_TL_PK PRIMARY KEY
  (PROCESS_ID,
   ROW_SEQ
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/


PROMPT Creating Unique Key on 'SVC_COST_CHG_REASON_TL'
ALTER TABLE SVC_COST_CHG_REASON_TL
 ADD CONSTRAINT SVC_COST_CHG_REASON_TL_UK UNIQUE
  (LANG,
   REASON
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

