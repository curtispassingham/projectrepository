--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'STAGE_FIXED_DEAL_DETAIL'

PROMPT Creating Index 'STAGE_FIXED_DEAL_DETAIL_I1'
DECLARE
  L_index_exists number := 0;
BEGIN
  SELECT count(*) INTO L_index_exists
    FROM USER_INDEXES
   WHERE INDEX_NAME = 'STAGE_FIXED_DEAL_DETAIL_I1';

  if (L_index_exists = 0) then
      execute immediate 'CREATE INDEX STAGE_FIXED_DEAL_DETAIL_I1 on STAGE_FIXED_DEAL_DETAIL( DEAL_NO,LOCATION,SET_OF_BOOKS_ID,TSF_ENTITY_ID ) INITRANS 12 TABLESPACE RETAIL_INDEX';
  end if;
end;
/

