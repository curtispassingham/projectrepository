CREATE OR REPLACE FORCE VIEW V_OGA_TL (OGA_CODE, OGA_DESC, LANG ) AS
SELECT  b.oga_code,
        case when tl.lang is not null then tl.oga_desc else b.oga_desc end oga_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  OGA b,
        OGA_TL tl
 WHERE  b.oga_code = tl.oga_code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_OGA_TL is 'This is the translation view for base table OGA. This view fetches data in user langauge either from translation table OGA_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_OGA_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_OGA_TL.OGA_CODE is 'Contains a unique code which will identify the government agency.  The codes are provided by customs.'
/

COMMENT ON COLUMN V_OGA_TL.OGA_DESC is 'Contains a description of the government agency.'
/

