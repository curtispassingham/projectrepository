--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 XY_DIFF_RATIO_TEMP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'XY_DIFF_RATIO_TEMP'
CREATE TABLE XY_DIFF_RATIO_TEMP
 (DIFF_TYPE VARCHAR(1 ) NOT NULL,
  DIFF_ID VARCHAR2(10 ) NOT NULL,
  VALUE NUMBER(12,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE XY_DIFF_RATIO_TEMP is 'This table is used only for Apply diff ratio functionality.It will be populated based on historical data presents in  diff_ratio_detail table.'
/

COMMENT ON COLUMN XY_DIFF_RATIO_TEMP.DIFF_TYPE is 'This field contains value  ''X'' or ''Y''.''X'' incase of row contains diff_x value and ''Y'' incase of row contains diff_y value'
/

COMMENT ON COLUMN XY_DIFF_RATIO_TEMP.DIFF_ID is 'This field contains diff_x ids  or diff_y ids'
/

COMMENT ON COLUMN XY_DIFF_RATIO_TEMP.VALUE is 'This field holds historical data on diff distribution value corresponds to diff_x ids  or diff_y ids'
/


PROMPT Creating Primary Key on 'XY_DIFF_RATIO_TEMP'
ALTER TABLE XY_DIFF_RATIO_TEMP
 ADD CONSTRAINT XY_DIFF_RATIO_TEMP_PK PRIMARY KEY
  (DIFF_TYPE,
   DIFF_ID
 )
 USING INDEX
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

