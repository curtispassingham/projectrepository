--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TIMELINE_STEP_COMP_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TIMELINE_STEP_COMP_TL(
LANG NUMBER(6) NOT NULL,
TIMELINE_TYPE VARCHAR2(6) NOT NULL,
STEP_NO NUMBER(4) NOT NULL,
STEP_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TIMELINE_STEP_COMP_TL is 'This is the translation table for TIMELINE_STEP_COMP table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.TIMELINE_TYPE is 'This field will hold the module the timeline is referring to. Valid values are: PO - Purchase Orders at the Header Level POIT - Purchase Orders at the Item Level IT - Item Maintenance CE - Customs Entry LO - Logistics at the Header Level LOBL - Logistics at the Bill of Lading Level LOCO - Logistics at the Container Level LOPI - Logistics at the Purchase Order/Item Level LOCI - Logistics at the Commercial Invoice Level'
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.STEP_NO is 'This field contains the number that uniquely defines the timeline step.'
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.STEP_DESC is 'This field contains the description of the timeline step.'
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TIMELINE_STEP_COMP_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TIMELINE_STEP_COMP_TL ADD CONSTRAINT PK_TIMELINE_STEP_COMP_TL PRIMARY KEY (
LANG,
TIMELINE_TYPE,
STEP_NO
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TIMELINE_STEP_COMP_TL
 ADD CONSTRAINT TSCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TIMELINE_STEP_COMP_TL ADD CONSTRAINT TSCT_TSC_FK FOREIGN KEY (
TIMELINE_TYPE,
STEP_NO
) REFERENCES TIMELINE_STEP_COMP (
TIMELINE_TYPE,
STEP_NO
)
/

