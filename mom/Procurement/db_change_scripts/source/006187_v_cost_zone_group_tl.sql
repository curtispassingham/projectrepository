CREATE OR REPLACE FORCE VIEW V_COST_ZONE_GROUP_TL (ZONE_GROUP_ID, DESCRIPTION, LANG ) AS
SELECT  b.zone_group_id,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  COST_ZONE_GROUP b,
        COST_ZONE_GROUP_TL tl
 WHERE  b.zone_group_id = tl.zone_group_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_COST_ZONE_GROUP_TL is 'This is the translation view for base table COST_ZONE_GROUP. This view fetches data in user langauge either from translation table COST_ZONE_GROUP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_COST_ZONE_GROUP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_COST_ZONE_GROUP_TL.ZONE_GROUP_ID is 'This field contains the number that uniquely identifies the zone group.'
/

COMMENT ON COLUMN V_COST_ZONE_GROUP_TL.DESCRIPTION is 'Contains the description of the cost zone group.'
/

