--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       QUOTA_CATEGORY_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE QUOTA_CATEGORY_TL(
LANG NUMBER(6) NOT NULL,
QUOTA_CAT VARCHAR2(6) NOT NULL,
IMPORT_COUNTRY_ID VARCHAR2(3) NOT NULL,
CATEGORY_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE QUOTA_CATEGORY_TL is 'This is the translation table for QUOTA_CATEGORY table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.QUOTA_CAT is 'Contains the unique code identification.'
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.IMPORT_COUNTRY_ID is 'Contains the country identifier for the country that is receiving the product.'
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.CATEGORY_DESC is 'Contains the description of the quota category.'
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN QUOTA_CATEGORY_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE QUOTA_CATEGORY_TL ADD CONSTRAINT PK_QUOTA_CATEGORY_TL PRIMARY KEY (
LANG,
QUOTA_CAT,
IMPORT_COUNTRY_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE QUOTA_CATEGORY_TL
 ADD CONSTRAINT QCT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE QUOTA_CATEGORY_TL ADD CONSTRAINT QCT_QC_FK FOREIGN KEY (
QUOTA_CAT,
IMPORT_COUNTRY_ID
) REFERENCES QUOTA_CATEGORY (
QUOTA_CAT,
IMPORT_COUNTRY_ID
)
/

