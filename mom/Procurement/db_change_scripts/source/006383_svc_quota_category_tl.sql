--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_QUOTA_CATEGORY_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT CREATING TABLE 'SVC_QUOTA_CATEGORY_TL'
CREATE TABLE SVC_QUOTA_CATEGORY_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, QUOTA_CAT VARCHAR2(6)
, IMPORT_COUNTRY_ID VARCHAR2(3)
, CATEGORY_DESC VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_QUOTA_CATEGORY_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in QUOTA_CATEGORY_TL.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.QUOTA_CAT is 'Contains the unique code identification.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.IMPORT_COUNTRY_ID is 'Contains the country identifier for the country that is receiving the product.'
/

COMMENT ON COLUMN SVC_QUOTA_CATEGORY_TL.CATEGORY_DESC is 'Contains the description of the quota category.'
/

PROMPT CREATING PRIMARY KEY ON 'SVC_QUOTA_CATEGORY_TL'
ALTER TABLE SVC_QUOTA_CATEGORY_TL
ADD CONSTRAINT SVC_QUOTA_CATEGORY_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING UNIQUE KEY ON 'SVC_QUOTA_CATEGORY_TL'
ALTER TABLE SVC_QUOTA_CATEGORY_TL
ADD CONSTRAINT SVC_QUOTA_CATEGORY_TL_UK UNIQUE
(LANG, QUOTA_CAT, IMPORT_COUNTRY_ID)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

