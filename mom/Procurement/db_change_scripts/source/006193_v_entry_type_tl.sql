CREATE OR REPLACE FORCE VIEW V_ENTRY_TYPE_TL (ENTRY_TYPE, IMPORT_COUNTRY_ID, ENTRY_TYPE_DESC, LANG ) AS
SELECT  b.entry_type,
        b.import_country_id,
        case when tl.lang is not null then tl.entry_type_desc else b.entry_type_desc end entry_type_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ENTRY_TYPE b,
        ENTRY_TYPE_TL tl
 WHERE  b.entry_type = tl.entry_type (+)
   AND  b.import_country_id = tl.import_country_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ENTRY_TYPE_TL is 'This is the translation view for base table ENTRY_TYPE. This view fetches data in user langauge either from translation table ENTRY_TYPE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ENTRY_TYPE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ENTRY_TYPE_TL.ENTRY_TYPE is 'This will hold the unique identifier for the custom defined entry type.'
/

COMMENT ON COLUMN V_ENTRY_TYPE_TL.IMPORT_COUNTRY_ID is 'This column will hold the valid of the import country.'
/

COMMENT ON COLUMN V_ENTRY_TYPE_TL.ENTRY_TYPE_DESC is 'This column will hold the description of the entry type.'
/

