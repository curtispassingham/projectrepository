--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ORDHEAD'
ALTER TABLE ORDHEAD MODIFY MASTER_PO_NO NUMBER (12)
/

COMMENT ON COLUMN ORDHEAD.MASTER_PO_NO is 'A number that is used to reference the master order number from which child records were created.  Orders with the same Master_PO number are grouped together using the same delivery date.'
/

