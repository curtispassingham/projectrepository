--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       OGA_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE OGA_TL(
LANG NUMBER(6) NOT NULL,
OGA_CODE VARCHAR2(3) NOT NULL,
OGA_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE OGA_TL is 'This is the translation table for OGA table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN OGA_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN OGA_TL.OGA_CODE is 'Contains a unique code which will identify the government agency.  The codes are provided by customs.'
/

COMMENT ON COLUMN OGA_TL.OGA_DESC is 'Contains a description of the government agency.'
/

COMMENT ON COLUMN OGA_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN OGA_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN OGA_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN OGA_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE OGA_TL ADD CONSTRAINT PK_OGA_TL PRIMARY KEY (
LANG,
OGA_CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE OGA_TL
 ADD CONSTRAINT OGAT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE OGA_TL ADD CONSTRAINT OGAT_OGA_FK FOREIGN KEY (
OGA_CODE
) REFERENCES OGA (
OGA_CODE
)
/

