--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       PO_TYPE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE PO_TYPE_TL(
LANG NUMBER(6) NOT NULL,
PO_TYPE VARCHAR2(4) NOT NULL,
PO_TYPE_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PO_TYPE_TL is 'This is the translation table for PO_TYPE table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN PO_TYPE_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN PO_TYPE_TL.PO_TYPE is 'This field contains the unique identifier for the order type.'
/

COMMENT ON COLUMN PO_TYPE_TL.PO_TYPE_DESC is 'This field contains a description for the specific order type.'
/

COMMENT ON COLUMN PO_TYPE_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN PO_TYPE_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN PO_TYPE_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN PO_TYPE_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE PO_TYPE_TL ADD CONSTRAINT PK_PO_TYPE_TL PRIMARY KEY (
LANG,
PO_TYPE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE PO_TYPE_TL
 ADD CONSTRAINT PTYT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE PO_TYPE_TL ADD CONSTRAINT PTYT_PTY_FK FOREIGN KEY (
PO_TYPE
) REFERENCES PO_TYPE (
PO_TYPE
)
/

