--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_ORDSEARCH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_ORDSEARCH'
CREATE OR REPLACE FORCE VIEW "V_ORDSEARCH" 
("ORDER_NO",
 "STATUS",
 "SUPPLIER_SITE",
 "SUPPLIER_SITE_NAME",
 "SUPPLIER_PARENT",
 "SUPPLIER_PARENT_NAME",
 "NOT_BEFORE_DATE",
 "NOT_AFTER_DATE",
 "WRITTEN_DATE",
 "ORDER_TYPE",
 "CURRENCY_CODE",
 "IMPORT_ORDER_IND",
 "IMPORT_COUNTRY_ID",
 "CONTRACT_NO",
 "ORIG_IND ",              
 "SPLIT_REF_ORDNO",
 "VENDOR_ORDER_NO",
 "PO_TYPE",
 "PROMOTION", 
 "BUYER",
 "POOL_SUPP_SITE",
 "POOL_SUPP_SITE_NAME",
 "POOL_SUPP_PARENT",
 "POOL_SUPP_PARENT_NAME",
 "ITEM",
 "ITEM_DESC",
 "LOCATION",
 "LOCATION_TYPE",
 "PHYSICAL_LOC",
 "DIVISION",
 "GROUP_NO",
 "DEPT",
 "DEPT_NAME",       
 "CLASS",
 "SUBCLASS",
 "CHAIN",
 "AREA",
 "REGION",
 "DISTRICT",
 "TSF_PO_LINK_NO")
AS
with a as (select o.order_no,
                  o.status,
                  o.supplier supplier_site,
                  (select vss.sup_name
                     from v_sups_tl vss
                    where vss.supplier = ss.supplier) supplier_site_name,
                  sp.supplier supplier_parent,
                  (select vsp.sup_name
                     from v_sups_tl vsp
                    where vsp.supplier = sp.supplier) supplier_parent_name,
                  o.NOT_BEFORE_DATE,
                  o.NOT_AFTER_DATE,
                  o.written_date,
                  o.order_type,
                  o.currency_code,
                  o.import_order_ind,
                  o.import_country_id,
                  o.contract_no,
                  o.orig_ind ,              
                  o.SPLIT_REF_ORDNO,
                  o.vendor_order_no,
                  o.po_type,
                  o.Promotion, 
                  o.buyer,
                  o.item,
                  o.location,
                  o.loc_type,
                  o.dept
          from ordhead o,
               v_sups ss,
               v_sups sp
         where o.supplier = ss.Supplier
           and ss.Supplier_Parent = sp.supplier
           and sp.supplier_parent is NULL)
-- normal orders which have item and location on ORDLOC
select a.order_no,
       a.status,
       a.supplier_site,
       a.supplier_site_name,
       a.supplier_parent,
       a.supplier_parent_name,
       a.not_before_date,
       a.not_after_date,
       a.written_date,
       a.order_type,
       a.currency_code,
       a.import_order_ind,
       a.import_country_id,
       a.contract_no,
       a.orig_ind ,              
       a.SPLIT_REF_ORDNO,
       a.vendor_order_no,
       a.po_type,
       a.Promotion, 
       a.buyer,
       oim.pool_sup_site,
       oim.pool_sup_site_name,
       oim.pool_sup_parent,
       oim.pool_sup_parent_name,
       ol.item,
       ol.item_desc,
       ol.location,
       ol.location_type,
       ol.phy_loc,
       ol.division,
       ol.group_no,
       ol.dept,
       ol.dept_name,       
       ol.class,
       ol.subclass,
       ol.chain,
       ol.area,
       ol.region,
       ol.district,
       ol.tsf_po_link_no
  from a,
       (select oi.order_no,
               oi.pool_supplier pool_sup_site,
               (select vss.sup_name
                  from v_sups_tl vss
                 where vss.supplier = ss.supplier) pool_sup_site_name,
               sp.supplier pool_sup_parent,
               (select vsp.sup_name
                  from v_sups_tl vsp
                 where vsp.supplier = sp.supplier)  pool_sup_parent_name
          from ord_inv_mgmt oi,
               v_sups ss,
               v_sups sp
         where oi.pool_supplier = ss.Supplier
           and ss.Supplier_Parent = sp.supplier
           and sp.supplier_parent is NULL) oim,
       (select olo.order_no,
               olo.item,
               (select itl.item_desc
                  from v_item_master_tl itl
                 where itl.item = olo.item) item_desc,
               im.dept,
               d.dept_name,
               im.class,
               im.subclass,
               im.division,
               im.group_no,
               loc.location,
               loc.phy_loc,
               loc.location_type,
               loc.location_name,
               loc.chain,
               loc.area,
               loc.region,
               loc.district,
               olo.tsf_po_link_no
          from Ordloc olo,
               v_item_master im,
               v_deps_tl d,
               (select vs.store location,
                       'S' location_type,
                       vs.store phy_loc,
                       vtl.store_name location_name, 
                       vs.chain,
                       vs.area,
                       vs.region,
                       vs.district
                  from v_store vs,
                       v_store_tl vtl
                 where vs.store = vtl.store
                union all
                select w.wh location,
                       'W' location_type,
                       w.physical_wh phy_loc,
                       wtl.wh_name location_name,
                       -999 chain,
                       -999 area,
                       -999 region,
                       -999 district
                  from v_wh w,
                       v_wh_tl wtl
                 where w.wh = wtl.wh)loc
         where olo.item = im.item
           and olo.location = loc.location
           and olo.loc_type = loc.location_type
           and im.dept = d.dept) ol           
 where a.order_no = oim.order_no(+)
   and a.order_no = ol.order_no
UNION ALL
-- Consignment orders. No ordloc entries.
select a.order_no,
       a.status,
       a.supplier_site,
       a.supplier_site_name,
       a.supplier_parent,
       a.supplier_parent_name,
       a.not_before_date,
       a.not_after_date,
       a.written_date,
       a.order_type,
       a.currency_code,
       a.import_order_ind,
       a.import_country_id,
       a.contract_no,
       a.orig_ind ,              
       a.SPLIT_REF_ORDNO,
       a.vendor_order_no,
       a.po_type,
       a.Promotion, 
       a.buyer,
       null pool_sup_site,
       null pool_sup_site_name,
       null pool_sup_parent,
       null pool_sup_parent_name,
       a.item,
       (select itl.item_desc
           from v_item_master_tl itl
          where itl.item = a.item) item_desc,
       a.location,
       a.loc_type,
       loc.phy_loc,
       im.division,
       im.group_no,
       im.dept,
       (select d.dept_name
          from v_deps_tl d
         where d.dept(+) = a.dept) dept_name,
       im.class,
       im.subclass,
       loc.chain,
       loc.area,
       loc.region,
       loc.district,
       null tsf_po_link_no
  from a,
       v_item_master im,
       (select vs.store location,
               'S' location_type,
               vs.store phy_loc,
               vtl.store_name location_name, 
               vs.chain,
               vs.area,
               vs.region,
               vs.district
          from v_store vs,
               v_store_tl vtl
         where vs.store = vtl.store
         union all
         select w.wh location,
                'W' location_type,
                w.physical_wh phy_loc,
                wtl.wh_name location_name,
                -999 chain,
                -999 area,
                -999 region,
                -999 district
           from v_wh w,
                v_wh_tl wtl
          where w.wh = wtl.wh)loc       
 where a.item = im.item
   and a.location = loc.location
   and a.loc_type = loc.location_type
   and not exists (select 'X'
                     from ordloc ol 
                    where ol.order_no = a.order_no
                      and rownum = 1)
UNION ALL
-- Worksheet orders whcih have location,dept on Ordhead but no items yet
select a.order_no,
       a.status,
       a.supplier_site,
       a.supplier_site_name,
       a.supplier_parent,
       a.supplier_parent_name,
       a.not_before_date,
       a.not_after_date,
       a.written_date,
       a.order_type,
       a.currency_code,
       a.import_order_ind,
       a.import_country_id,
       a.contract_no,
       a.orig_ind ,              
       a.SPLIT_REF_ORDNO,
       a.vendor_order_no,
       a.po_type,
       a.Promotion, 
       a.buyer,
       null pool_sup_site,
       null pool_sup_site_name,
       null pool_sup_parent,
       null pool_sup_parent_name,
       a.item,
       NULL item_desc,
       a.location ,
       a.loc_type,
       loc.phy_loc,
       null,
       null,
       a.dept,
       (select d.dept_name
          from v_deps_tl d
         where d.dept(+) = a.dept) dept_name,
       null,
       null,
       loc.chain,
       loc.area,
       loc.region,
       loc.district,
       null tsf_po_link_no
  from a,
       (select vs.store location,
               'S' location_type,
               vs.store phy_loc,
               vtl.store_name location_name, 
               vs.chain,
               vs.area,
               vs.region,
               vs.district
          from v_store vs,
               v_store_tl vtl
         where vs.store = vtl.store
         union all
         select w.wh location,
                'W' location_type,
                w.physical_wh phy_loc,
                wtl.wh_name location_name,
                -999 chain,
                -999 area,
                -999 region,
                -999 district
           from v_wh w,
                v_wh_tl wtl
          where w.wh = wtl.wh)loc       
 where a.item is NULL
   and a.location = loc.location
   and a.loc_type = loc.location_type
   and not exists (select 'X'
                     from ordloc ol 
                    where ol.order_no = a.order_no
                      and rownum = 1)
UNION ALL
-- Worksheet orders whcih have dept on Ordhead but no items-locs yet
select a.order_no,
       a.status,
       a.supplier_site,
       a.supplier_site_name,
       a.supplier_parent,
       a.supplier_parent_name,
       a.not_before_date,
       a.not_after_date,
       a.written_date,
       a.order_type,
       a.currency_code,
       a.import_order_ind,
       a.import_country_id,
       a.contract_no,
       a.orig_ind ,              
       a.SPLIT_REF_ORDNO,
       a.vendor_order_no,
       a.po_type,
       a.Promotion, 
       a.buyer,
       null pool_sup_site,
       null pool_sup_site_name,
       null pool_sup_parent,
       null pool_sup_parent_name,
       null item,
       NULL item_desc,
       null location ,
       null loc_type,
       null phy_loc,
       null,
       null,
       a.dept,
       (select d.dept_name
          from v_deps_tl d
         where d.dept(+) = a.dept) dept_name,
       null,
       null,
       null,
       null,
       null,
       null,
       null tsf_po_link_no
  from a
 where a.item is NULL
   and a.location is NULL
   and not exists (select 'X'
                     from ordloc ol 
                    where ol.order_no = a.order_no
                      and rownum = 1)  
/

COMMENT ON TABLE V_ORDSEARCH IS 'This view is used for the order search screen in RMS Alloy. It includes all fields that can be used as item search criteria.'
/

COMMENT ON COLUMN V_ORDSEARCH."ORDER_NO"  IS 'Purchase Order Number.'
/

COMMENT ON COLUMN V_ORDSEARCH."STATUS"  IS 'The current status of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDSEARCH."SUPPLIER_SITE"  IS 'The supplier site where the Purchase Order is placed.'
/

COMMENT ON COLUMN V_ORDSEARCH."SUPPLIER_SITE_NAME"  IS 'Translated name of the supplier site.'
/

COMMENT ON COLUMN V_ORDSEARCH."SUPPLIER_PARENT"  IS 'Parent Supplier of the Supplier Site.'
/

COMMENT ON COLUMN V_ORDSEARCH."SUPPLIER_PARENT_NAME"  IS 'Translated name of the Parent Supplier.'
/

COMMENT ON COLUMN V_ORDSEARCH."NOT_BEFORE_DATE"  IS 'Contains the first date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDSEARCH."NOT_AFTER_DATE"  IS 'Contains the last date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDSEARCH."WRITTEN_DATE"  IS 'Contains the date the Purchase order was created within the system.'
/

COMMENT ON COLUMN V_ORDSEARCH."ORDER_TYPE"  IS 'Indicates which Open To Buy bucket would be updated.'
/

COMMENT ON COLUMN V_ORDSEARCH."CURRENCY_CODE"  IS 'Currency of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDSEARCH."IMPORT_ORDER_IND"  IS 'Indicates if the purchase order is an import order.'
/

COMMENT ON COLUMN V_ORDSEARCH."IMPORT_COUNTRY_ID"  IS 'The identifier of the country into which the items on the order are being imported.'
/

COMMENT ON COLUMN V_ORDSEARCH."CONTRACT_NO"  IS 'Contains the contract number associated with this order.'
/

COMMENT ON COLUMN V_ORDSEARCH."ORIG_IND "  IS 'Indicates where the Purchase Order originated.'
/ 
             
COMMENT ON COLUMN V_ORDSEARCH."SPLIT_REF_ORDNO"  IS 'This column will store the original order number from which the split orders were generated from.'
/

COMMENT ON COLUMN V_ORDSEARCH."VENDOR_ORDER_NO"  IS 'Contains the vendor number who will provide the merchandise specified in the order.'
/

COMMENT ON COLUMN V_ORDSEARCH."PO_TYPE"  IS 'Contains the context of the Purchase Order creation.'
/

COMMENT ON COLUMN V_ORDSEARCH."PROMOTION"  IS 'Contains the promotion number associated with the order.' 
/

COMMENT ON COLUMN V_ORDSEARCH."BUYER"  IS 'Contains the number associated with the buyer for the order.'
/

COMMENT ON COLUMN V_ORDSEARCH."POOL_SUPP_SITE"  IS 'This field will contain the ID for a pooled supplier site if the order was created as a pooled order.'
/

COMMENT ON COLUMN V_ORDSEARCH."POOL_SUPP_SITE_NAME"  IS 'This field will contain the translated name of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDSEARCH."POOL_SUPP_PARENT"  IS 'This field will contain the parent of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDSEARCH."POOL_SUPP_PARENT_NAME"  IS 'This field will contain the translated name of the pooled supplier parent.'
/

COMMENT ON COLUMN V_ORDSEARCH."ITEM"  IS 'The item for which the Purchase Order is created.'
/

COMMENT ON COLUMN V_ORDSEARCH."ITEM_DESC"  IS 'Description of the Item.'
/

COMMENT ON COLUMN V_ORDSEARCH."LOCATION"  IS 'The location for which the Purchase Order is created.'
/

COMMENT ON COLUMN V_ORDSEARCH."LOCATION_TYPE"  IS 'This indicates the type of location.'
/

COMMENT ON COLUMN V_ORDSEARCH."PHYSICAL_LOC"  IS 'This contains the physical location of the location.'
/

COMMENT ON COLUMN V_ORDSEARCH."DIVISION"  IS 'This contains the division to which the item belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."GROUP_NO"  IS 'This contains the group to which the item belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."DEPT"  IS 'This contains the department to which the item belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."DEPT_NAME"  IS 'This contains the name of the department'
/

COMMENT ON COLUMN V_ORDSEARCH."CLASS"  IS 'This contains the class to which the item belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."SUBCLASS"  IS 'This contains the subclass to which the item belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."CHAIN"  IS 'This contains the chain to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."AREA"  IS 'This contains the area to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."REGION"  IS 'This contains the region to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."DISTRICT"  IS 'This contains the district to which the store location belongs.'
/

COMMENT ON COLUMN V_ORDSEARCH."TSF_PO_LINK_NO" IS 'Reference number to link the item on the purchase orders to transfer.'
/

 
 