CREATE OR REPLACE FORCE VIEW V_SCAC_TL (SCAC_CODE, SCAC_CODE_DESC, LANG ) AS
SELECT  b.scac_code,
        case when tl.lang is not null then tl.scac_code_desc else b.scac_code_desc end scac_code_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  SCAC b,
        SCAC_TL tl
 WHERE  b.scac_code = tl.scac_code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_SCAC_TL is 'This is the translation view for base table SCAC. This view fetches data in user langauge either from translation table SCAC_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_SCAC_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_SCAC_TL.SCAC_CODE is 'Contains the unique key that identifies the scac record.'
/

COMMENT ON COLUMN V_SCAC_TL.SCAC_CODE_DESC is 'Contains the description of the SCAC Code.'
/

