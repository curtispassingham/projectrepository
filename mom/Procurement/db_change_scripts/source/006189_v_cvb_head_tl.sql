CREATE OR REPLACE FORCE VIEW V_CVB_HEAD_TL (CVB_CODE, CVB_DESC, LANG ) AS
SELECT  b.cvb_code,
        case when tl.lang is not null then tl.cvb_desc else b.cvb_desc end cvb_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  CVB_HEAD b,
        CVB_HEAD_TL tl
 WHERE  b.cvb_code = tl.cvb_code (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_CVB_HEAD_TL is 'This is the translation view for base table CVB_HEAD. This view fetches data in user langauge either from translation table CVB_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_CVB_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_CVB_HEAD_TL.CVB_CODE is 'Contains a user specified code representing a computation base that will be used by expenses.'
/

COMMENT ON COLUMN V_CVB_HEAD_TL.CVB_DESC is 'Contains the description or name of the Computation Value Base.'
/

