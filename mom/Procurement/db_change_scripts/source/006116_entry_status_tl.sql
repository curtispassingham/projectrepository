--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       ENTRY_STATUS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE ENTRY_STATUS_TL(
LANG NUMBER(6) NOT NULL,
ENTRY_STATUS VARCHAR2(6) NOT NULL,
IMPORT_COUNTRY_ID VARCHAR2(3) NOT NULL,
ENTRY_STATUS_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE ENTRY_STATUS_TL is 'This is the translation table for ENTRY_STATUS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN ENTRY_STATUS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN ENTRY_STATUS_TL.ENTRY_STATUS is 'This will hold the unique identifier for the custom defined entry status.'
/

COMMENT ON COLUMN ENTRY_STATUS_TL.IMPORT_COUNTRY_ID is 'This column will hold the import country.'
/

COMMENT ON COLUMN ENTRY_STATUS_TL.ENTRY_STATUS_DESC is 'This column will hold the description of the entry status.'
/

COMMENT ON COLUMN ENTRY_STATUS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN ENTRY_STATUS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN ENTRY_STATUS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN ENTRY_STATUS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE ENTRY_STATUS_TL ADD CONSTRAINT PK_ENTRY_STATUS_TL PRIMARY KEY (
LANG,
ENTRY_STATUS,
IMPORT_COUNTRY_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE ENTRY_STATUS_TL
 ADD CONSTRAINT ESTT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE ENTRY_STATUS_TL ADD CONSTRAINT ESTT_EST_FK FOREIGN KEY (
ENTRY_STATUS,
IMPORT_COUNTRY_ID
) REFERENCES ENTRY_STATUS (
ENTRY_STATUS,
IMPORT_COUNTRY_ID
)
/

