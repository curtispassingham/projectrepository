CREATE OR REPLACE FORCE VIEW V_TARIFF_TREATMENT_TL (TARIFF_TREATMENT, TARIFF_TREATMENT_DESC, LANG ) AS
SELECT  b.tariff_treatment,
        case when tl.lang is not null then tl.tariff_treatment_desc else b.tariff_treatment_desc end tariff_treatment_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TARIFF_TREATMENT b,
        TARIFF_TREATMENT_TL tl
 WHERE  b.tariff_treatment = tl.tariff_treatment (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TARIFF_TREATMENT_TL is 'This is the translation view for base table TARIFF_TREATMENT. This view fetches data in user langauge either from translation table TARIFF_TREATMENT_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TARIFF_TREATMENT_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TARIFF_TREATMENT_TL.TARIFF_TREATMENT is 'Contains the customs approved code to uniquely identify a special tariff program.'
/

COMMENT ON COLUMN V_TARIFF_TREATMENT_TL.TARIFF_TREATMENT_DESC is 'Contains the description for the tariff treatment ID.  Example: NAFTA is North American Free Trade Agreement.'
/

