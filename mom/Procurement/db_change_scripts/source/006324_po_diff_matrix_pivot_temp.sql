--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 PO_DIFF_MATRIX_PIVOT_TEMP
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'PO_DIFF_MATRIX_PIVOT_TEMP'
CREATE TABLE PO_DIFF_MATRIX_PIVOT_TEMP
 (DIFF_Z VARCHAR2(10 ),
  DIFF_Y VARCHAR2(10 ) NOT NULL,
  DIFF_X VARCHAR2(10 ) NOT NULL,
  X_SEQ_NO NUMBER(10) NOT NULL,
  X_REF_COLUMN_NAME VARCHAR2(7 ),
  VALUE NUMBER(12,4)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE PO_DIFF_MATRIX_PIVOT_TEMP is 'This temporary table is used for PO distribution matrix screen to hold the data displayed in the pivot table. The data in this table is fetched from PO_DIFF_MATRIX_TEMP table and post user changes, the data is copied back to PO_DIFF_MATRIX_TEMP table.'
/

COMMENT ON COLUMN PO_DIFF_MATRIX_PIVOT_TEMP.DIFF_Z is 'This holds the diff Z id. This will be null for two dimension matrix. '
/

COMMENT ON COLUMN PO_DIFF_MATRIX_PIVOT_TEMP.DIFF_Y is 'This holds the diff Y id. '
/

COMMENT ON COLUMN PO_DIFF_MATRIX_PIVOT_TEMP.DIFF_X is 'This holds the diff X id.'
/

COMMENT ON COLUMN PO_DIFF_MATRIX_PIVOT_TEMP.X_SEQ_NO is 'This holds the sequence in which diff X will be displayed. '
/

COMMENT ON COLUMN PO_DIFF_MATRIX_PIVOT_TEMP.X_REF_COLUMN_NAME is 'This holds the column name in PO_DIFF_MATRIX_TEMP for the X diff id.'
/

COMMENT ON COLUMN PO_DIFF_MATRIX_PIVOT_TEMP.VALUE is 'This holds the value in the pivot table. '
/

