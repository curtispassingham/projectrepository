--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ORDHEAD'
ALTER TABLE SVC_ORDHEAD MODIFY MASTER_PO_NO NUMBER (12)
/

COMMENT ON COLUMN SVC_ORDHEAD.MASTER_PO_NO is 'Holds the master PO number from which child orders will be created. This will only be used to group orders together and will not be used for any purchase order processing.'
/

