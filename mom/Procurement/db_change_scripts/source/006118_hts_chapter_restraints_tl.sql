--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       HTS_CHAPTER_RESTRAINTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE HTS_CHAPTER_RESTRAINTS_TL(
LANG NUMBER(6) NOT NULL,
CHAPTER VARCHAR2(4) NOT NULL,
IMPORT_COUNTRY_ID VARCHAR2(3) NOT NULL,
ORIGIN_COUNTRY_ID VARCHAR2(3) NOT NULL,
RESTRAINT_TYPE VARCHAR2(6) NOT NULL,
RESTRAINT_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE HTS_CHAPTER_RESTRAINTS_TL is 'This is the translation table for HTS_CHAPTER_RESTRAINTS table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.CHAPTER is 'Contains the unique HTS Chapter number.  The unique chapter is defined by the HTS schedule for all GATT signatories.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.IMPORT_COUNTRY_ID is 'Contains the country id of the importing country.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.ORIGIN_COUNTRY_ID is 'Contains the country id of the country of origin.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_TYPE is 'Contains the type of restraint imposed on certain items.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_DESC is 'Contains the description of the chapter restraint.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN HTS_CHAPTER_RESTRAINTS_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE HTS_CHAPTER_RESTRAINTS_TL ADD CONSTRAINT PK_HTS_CHAPTER_RESTRAINTS_TL PRIMARY KEY (
LANG,
CHAPTER,
IMPORT_COUNTRY_ID,
ORIGIN_COUNTRY_ID,
RESTRAINT_TYPE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE HTS_CHAPTER_RESTRAINTS_TL
 ADD CONSTRAINT HTRT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE HTS_CHAPTER_RESTRAINTS_TL ADD CONSTRAINT HTRT_HTR_FK FOREIGN KEY (
CHAPTER,
IMPORT_COUNTRY_ID,
ORIGIN_COUNTRY_ID,
RESTRAINT_TYPE
) REFERENCES HTS_CHAPTER_RESTRAINTS (
CHAPTER,
IMPORT_COUNTRY_ID,
ORIGIN_COUNTRY_ID,
RESTRAINT_TYPE
)
/

