--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       COST_ZONE_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE COST_ZONE_TL(
LANG NUMBER(6) NOT NULL,
ZONE_GROUP_ID NUMBER(4) NOT NULL,
ZONE_ID NUMBER(10) NOT NULL,
DESCRIPTION VARCHAR2(150) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE COST_ZONE_TL is 'This is the translation table for COST_ZONE table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN COST_ZONE_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN COST_ZONE_TL.ZONE_GROUP_ID is 'Contains the identification number of the zone group in which the zone is located.'
/

COMMENT ON COLUMN COST_ZONE_TL.ZONE_ID is 'Contains the zone identification number which uniquely identifies the zone.  If the cost level of the zone group is store, then the zone will be the store number.'
/

COMMENT ON COLUMN COST_ZONE_TL.DESCRIPTION is 'Contains the name of the zone.  If the cost level of the zone group is Store, then this column contains the store name.'
/

COMMENT ON COLUMN COST_ZONE_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN COST_ZONE_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN COST_ZONE_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN COST_ZONE_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE COST_ZONE_TL ADD CONSTRAINT PK_COST_ZONE_TL PRIMARY KEY (
LANG,
ZONE_GROUP_ID,
ZONE_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE COST_ZONE_TL
 ADD CONSTRAINT CZT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE COST_ZONE_TL ADD CONSTRAINT CZT_CZ_FK FOREIGN KEY (
ZONE_GROUP_ID,
ZONE_ID
) REFERENCES COST_ZONE (
ZONE_GROUP_ID,
ZONE_ID
)
/

