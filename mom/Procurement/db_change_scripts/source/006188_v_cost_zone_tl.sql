CREATE OR REPLACE FORCE VIEW V_COST_ZONE_TL (ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, LANG ) AS
SELECT  b.zone_group_id,
        b.zone_id,
        case when tl.lang is not null then tl.description else b.description end description,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  COST_ZONE b,
        COST_ZONE_TL tl
 WHERE  b.zone_group_id = tl.zone_group_id (+)
   AND  b.zone_id = tl.zone_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_COST_ZONE_TL is 'This is the translation view for base table COST_ZONE. This view fetches data in user langauge either from translation table COST_ZONE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_COST_ZONE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_COST_ZONE_TL.ZONE_GROUP_ID is 'Contains the identification number of the zone group in which the zone is located.'
/

COMMENT ON COLUMN V_COST_ZONE_TL.ZONE_ID is 'Contains the zone identification number which uniquely identifies the zone.  If the cost level of the zone group is store, then the zone will be the store number.'
/

COMMENT ON COLUMN V_COST_ZONE_TL.DESCRIPTION is 'Contains the name of the zone.  If the cost level of the zone group is Store, then this column contains the store name.'
/

