--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_BUYER_WKSHT_SEARCH
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW  
--------------------------------------
CREATE OR REPLACE FORCE VIEW "V_BUYER_WKSHT_SEARCH" 
("SOURCE_TYPE",
 "SOURCE_TYPE_DESC",
 "ITEM",
 "ITEM_DESC",
 "DEPT",
 "DEPT_NAME",
 "CLASS",
 "CLASS_NAME",
 "SUBCLASS",
 "SUBCLASS_NAME",
 "BUYER",
 "BUYER_NAME",
 "DIFF_1",
 "DIFF_1_DESC",
 "DIFF_2",
 "DIFF_2_DESC",
 "DIFF_3",
 "DIFF_3_DESC",
 "DIFF_4",
 "DIFF_4_DESC",
 "PACK_IND",
 "COMP_ITEM",
 "COMP_ITEM_DESC",
 "SUPPLIER",
 "SUP_NAME",
 "ORIGIN_COUNTRY_ID",
 "POOL_SUPPLIER",
 "POOL_SUP_NAME",
 "LOC_TYPE",
 "LOC_TYPE_DESC",
 "LOCATION",
 "LOC_NAME",
 "PHYSICAL_WH",
 "PHYS_WH_NAME",
 "CURRENT_DSCNT",
 "ROI",
 "RAW_ROQ",
 "ORDER_ROQ",
 "STANDARD_UOM",
 "TI",
 "HI",
 "TERMS",
 "TERMS_CODE",
 "TOTAL_LEAD_TIME",
 "SUPP_UNIT_COST",
 "PRIM_SUPP_UNIT_COST",
 "UNIT_COST",
 "PRIM_UNIT_COST",
 "CURRENCY_CODE",
 "PRIM_CURR_CODE",
 "CREATE_DATE",
 "DAYS_TO_EVENT",
 "NEXT_EVENT_DATE",
 "TARGET_DATE",
 "STATUS",
 "TSF_PO_LINK_NO",
 "ORDER_EXIST",
 "DUE_IND",
 "AUDSID",
 "ROW_ID")
AS
      select 'R' source_type,
             (select code_desc from v_code_detail where code = 'R' and code_type = 'SRCT') AS  source_type_desc,
             rr.item,
             im.item_desc,
             rr.dept,
             (select vdt.dept_name from v_deps_tl vdt where vdt.dept = rr.dept) AS dept_name,
             rr.class,
             (select vct.class_name from v_class_tl vct where vct.dept = rr.dept and vct.class = rr.class) AS class_name,
             rr.subclass,
             (select vst.sub_name from v_subclass_tl vst where vst.dept = rr.dept and vst.class = rr.class and vst.subclass = rr.subclass) AS subclass_name,
             rr.buyer,
             (select b.buyer_name from buyer b where b.buyer = rr.buyer) buyer_name,
             im.diff_1,
             (select description from v_diff_id_group_type where id_group = im.diff_1) AS  diff_1_desc,
             im.diff_2,
             (select description from v_diff_id_group_type where id_group = im.diff_2) AS  diff_2_desc,
             im.diff_3,
             (select description from v_diff_id_group_type where id_group = im.diff_3) AS  diff_3_desc,
             im.diff_4,
             (select description from v_diff_id_group_type where id_group = im.diff_4) AS  diff_4_desc,
             decode(rr.item_type, 'P', 'Y', 'N') AS pack_ind,   --'M' master item, 'P' simple pack, 'S' substitute item
             decode(rr.item_type, 'P', rr.master_item, null) comp_item,
             decode(rr.item_type, 'P', (select vimt.item_desc from v_item_master_tl vimt where vimt.item = rr.master_item), NULL) AS comp_item_desc,
             rr.primary_repl_supplier supplier,
             s.sup_name,
             rr.origin_country_id,
             pool_supplier,
             ps.sup_name pool_sup_name,
             rr.loc_type,
             (select code_desc from v_code_detail where code = rr.loc_type and code_type = 'MCLT') AS loc_type_desc,
             rr.location,           
             loc.location_name loc_name,
             rr.physical_wh,
             (select wh_name from v_wh_tl vwt where vwt.wh = rr.physical_wh) AS phys_wh_name,
             NULL current_dscnt,
             NULL roi,
             DECODE(item_type, 'P', rr.raw_roq_pack, rr.raw_roq) raw_roq,
             NVL(rr.order_roq, 0) order_roq,
             im.standard_uom,
             rr.ti,
             rr.hi,
             s.terms,
             (select vtht.terms_code from v_terms_head_tl vtht where vtht.terms = s.terms) AS terms_code,
             nvl(rr.supp_lead_time,0) + nvl(rr.pickup_lead_time, 0) total_lead_time,
             rr.supp_unit_cost,   -- in supplier currency, not updatable
             case
                --supp_unit_cost converted to primary currency
                when s.currency_code = so.currency_code or NVL(rr.supp_unit_cost, 0) = 0 then
                   rr.supp_unit_cost
                else
                   rr.supp_unit_cost * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency, mv.exchange_type order by mv.effective_date desc)
                end prim_supp_unit_cost, 
             NVL(rr.unit_cost, 0) unit_cost, -- in supplier currency, updatable
             case
                --unit_cost converted to primary currency
                when s.currency_code = so.currency_code or NVL(rr.unit_cost, 0) = 0 then
                   NVL(rr.unit_cost, 0)
                else
                   rr.unit_cost * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency, mv.exchange_type order by mv.effective_date desc)
                end prim_unit_cost, 
             s.currency_code,
             so.currency_code prim_curr_code,
             repl_date create_date,
             NULL days_to_event,   --IB only
             NULL next_event_date, --IB only
             NULL target_date,     --IB only
             rr.status,
             rr.tsf_po_link_no,
             DECODE(rr.status, 'P', 'Y', 'N') order_exist,
             rr.due_ind, 
             rr.audsid,
             rr.rowid row_id         
        from repl_results rr, 
             v_item_master im,
             v_location loc,
             v_sups s,
             v_sups ps,
             system_config_options so,
             period p,
             mv_currency_conversion_rates mv
       where rr.repl_order_ctrl = 'B'
         and rr.delivery_slot_id is null
         and rr.need_date is NULL
         and rr.status in ('W', 'P')
         and rr.primary_repl_supplier = s.supplier 
         and rr.pool_supplier = ps.supplier(+)
         and rr.location = loc.location_id
         and rr.item = im.item
         and mv.from_currency = s.currency_code
         and mv.to_currency = so.currency_code
         and mv.exchange_type = 'C'    --cost
         and mv.effective_date <= p.vdate
   union all        
      select 'I' source_type,
             (select code_desc from v_code_detail where code = 'I' and code_type = 'SRCT') AS  source_type_desc,
             ir.item,
             im.item_desc,
             ir.dept,
             (select vdt.dept_name from v_deps_tl vdt where vdt.dept = ir.dept) AS dept_name,
             ir.class,
             (select vct.class_name from v_class_tl vct where vct.dept = ir.dept and vct.class = ir.class) AS class_name,
             ir.subclass,
             (select vst.sub_name from v_subclass_tl vst where vst.dept = ir.dept and vst.class = ir.class and vst.subclass = ir.subclass) AS subclass_name,
             ir.buyer,
             (select b.buyer_name from buyer b where b.buyer = ir.buyer) buyer_name,
             im.diff_1,
             (select description from v_diff_id_group_type where id_group = im.diff_1) AS  diff_1_desc,  
             im.diff_2,
             (select description from v_diff_id_group_type where id_group = im.diff_2) AS  diff_2_desc,  
             im.diff_3,
             (select description from v_diff_id_group_type where id_group = im.diff_3) AS  diff_3_desc,  
             im.diff_4,
             (select description from v_diff_id_group_type where id_group = im.diff_4) AS  diff_4_desc,  
             decode(ir.item_type, 'P', 'Y', 'N') AS pack_ind,   --'P' simple pack, 'M' non-simple pack
             decode(ir.item_type, 'P', ir.comp_item, null) comp_item,
             decode(ir.item_type, 'P', (select vimt.item_desc from v_item_master_tl vimt where vimt.item = ir.comp_item), NULL) AS comp_item_desc,
             ir.supplier supplier,
             s.sup_name,
             ir.origin_country_id,
             pool_supplier,
             ps.sup_name pool_sup_name,
             ir.loc_type,
             (select code_desc from v_code_detail where code = ir.loc_type and code_type = 'MCLT') AS loc_type_desc,
             ir.location,
             loc.location_name loc_name,
             ir.physical_wh,
             (select wh_name from v_wh_tl vwt where vwt.wh = ir.physical_wh) AS phys_wh_name,
             ir.future_cost - ir.current_cost current_dscnt,
             ir.roi,
             ir.raw_roq,
             NVL(ir.order_roq, 0) order_roq,
             im.standard_uom,
             ir.ti,
             ir.hi,
             s.terms,
             (select vtht.terms_code from v_terms_head_tl vtht where vtht.terms = s.terms) AS terms_code,
             nvl(ir.supp_lead_time,0) + nvl(ir.pickup_lead_time, 0) total_lead_time,
             ir.supp_unit_cost,  -- in supplier currency, not updatable
             case
                --supp_unit_cost converted to primary currency
                when s.currency_code = so.currency_code or NVL(ir.supp_unit_cost, 0) = 0 then
                   ir.supp_unit_cost
                else
                   ir.supp_unit_cost * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency, mv.exchange_type order by mv.effective_date desc)
                end prim_supp_unit_cost, 
             NVL(ir.unit_cost, 0) unit_cost,       -- in supplier currency, updatable
             case
                --unit_cost converted to primary currency
                when s.currency_code = so.currency_code or NVL(ir.unit_cost, 0) = 0 then
                   NVL(ir.unit_cost, 0)
                else
                   ir.unit_cost * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency, mv.exchange_type order by mv.effective_date desc)
                end prim_unit_cost, 
             s.currency_code,
             so.currency_code prim_curr_code,
             ir.create_date,
             ir.days_to_event,   --IB only
             ir.next_event_date, --IB only
             ir.target_date,     --IB only
             ir.status,
             NULL tsf_po_link_no,
             DECODE(ir.status, 'P', 'Y', 'N') order_exist,
             NULL due_ind,   --RR only
             ir.audsid,
             ir.rowid row_id        
        from ib_results ir, 
             v_item_master im,  
             v_location loc,    
             v_sups s,           
             v_sups ps,         
             system_config_options so,
             period p,
             mv_currency_conversion_rates mv
       where ir.ib_order_ctrl = 'B'
         and ir.status in ('W', 'P')
         and ir.supplier = s.supplier 
         and ir.pool_supplier = ps.supplier(+)
         and ir.location = loc.location_id
         and ir.item = im.item
         and mv.from_currency = s.currency_code
         and mv.to_currency = so.currency_code
         and mv.exchange_type = 'C'    --cost
         and mv.effective_date <= p.vdate
   union all
      select 'M'  source_type,
             (select code_desc from v_code_detail where code = 'M' and code_type = 'SRCT') AS  source_type_desc,
             bw.item,
             im.item_desc,
             bw.dept,
             (select vdt.dept_name from v_deps_tl vdt where vdt.dept = bw.dept) AS dept_name,
             bw.class,
             (select vct.class_name from v_class_tl vct where vct.dept = bw.dept and vct.class = bw.class) AS class_name,
             bw.subclass,
             (select vst.sub_name from v_subclass_tl vst where vst.dept = bw.dept and vst.class = bw.class and vst.subclass = bw.subclass) AS subclass_name,
             bw.buyer,
             (select b.buyer_name from buyer b where b.buyer = bw.buyer) buyer_name,
             im.diff_1,
             (select description from v_diff_id_group_type where id_group = im.diff_1) AS  diff_1_desc,  
             im.diff_2,
             (select description from v_diff_id_group_type where id_group = im.diff_2) AS  diff_2_desc,  
             im.diff_3,
             (select description from v_diff_id_group_type where id_group = im.diff_3) AS  diff_3_desc,  
             im.diff_4,
             (select description from v_diff_id_group_type where id_group = im.diff_4) AS  diff_4_desc,  
             decode(bw.item_type, 'P', 'Y', 'N'),   --'P' pack, 'N' non-pack
             decode(bw.item_type, 'P', bw.comp_item, null) comp_item,
             decode(bw.item_type, 'P', (select vimt.item_desc from v_item_master_tl vimt where vimt.item = bw.comp_item), NULL) AS comp_item_desc,
             bw.supplier supplier,
             s.sup_name,
             bw.origin_country_id,
             pool_supplier,
             ps.sup_name pool_sup_name,
             bw.loc_type,
             (select code_desc from v_code_detail where code = bw.loc_type and code_type = 'MCLT') AS loc_type_desc,
             bw.location,
             loc.location_name loc_name,
             bw.physical_wh,
             (select wh_name from v_wh_tl vwt where vwt.wh = bw.physical_wh) AS phys_wh_name,
             NULL current_dscnt,
             NULL roi,
             0 raw_roq,
             NVL(bw.order_roq, 0) order_roq,
             im.standard_uom,
             bw.ti,
             bw.hi,
             s.terms,
             (select vtht.terms_code from v_terms_head_tl vtht where vtht.terms = s.terms) AS terms_code,
             nvl(bw.supp_lead_time,0) + nvl(bw.pickup_lead_time, 0) total_lead_time,
             bw.supp_unit_cost,  -- in supplier currency, not updatable
             case
                --supp_unit_cost converted to primary currency
                when s.currency_code = so.currency_code or NVL(bw.supp_unit_cost, 0) = 0 then
                   bw.supp_unit_cost
                else
                   bw.supp_unit_cost * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency, mv.exchange_type order by mv.effective_date desc)
                end prim_supp_unit_cost, 
             NVL(bw.unit_cost, 0) unit_cost, -- in supplier currency, updatable
             case
                --unit_cost converted to primary currency
                when s.currency_code = so.currency_code or NVL(bw.unit_cost, 0) = 0 then
                   NVL(bw.unit_cost, 0)
                else
                   bw.unit_cost * first_value(mv.exchange_rate) over (partition by mv.from_currency, mv.to_currency, mv.exchange_type order by mv.effective_date desc)
                end prim_unit_cost, 
             s.currency_code,
             so.currency_code prim_curr_code,
             create_date,
             NULL days_to_event,   --IB only
             NULL next_event_date, --IB only
             NULL target_date,     --IB only
             bw.status,
             bw.tsf_po_link_no,
             DECODE(bw.status, 'P', 'Y', 'N') order_exist,
             NULL due_ind,   --RR only
             bw.audsid,
             bw.rowid row_id        
        from buyer_wksht_manual bw, 
             v_item_master im,  
             v_location loc,    
             v_sups s,           
             v_sups ps,         
             system_config_options so,
             period p,
             mv_currency_conversion_rates mv
       where bw.status in ('P', 'W')
         and bw.supplier = s.supplier 
         and bw.pool_supplier = ps.supplier(+)
         and bw.location = loc.location_id
         and bw.item = im.item
         and mv.from_currency = s.currency_code
         and mv.to_currency = so.currency_code
         and mv.exchange_type = 'C'    --cost
         and mv.effective_date <= p.vdate
/

COMMENT ON TABLE V_BUYER_WKSHT_SEARCH IS 'This view is used for the buyer worksheet search screen in RMS Alloy. It queries data from repl_results, ib_results, and buyer_wksht_manual tables and includes all fields that can be used as search criteria and search results. It only returns the data the user has access to and displays descriptions in the user language.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SOURCE_TYPE" IS 'Data source of the worksheet search result. R - repl_results, I - ib_results, M - buyer_wksht_manual.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SOURCE_TYPE_DESC" IS 'Description of the data source.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ITEM" IS 'Item on the buyer worksheet to create order for.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ITEM_DESC" IS 'Description of the item.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DEPT" IS 'Department of the item.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DEPT_NAME" IS 'Name of the department.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."CLASS" IS 'Class of the item.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."CLASS_NAME" IS 'Name of the class.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SUBCLASS" IS 'Subclass of the item.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SUBCLASS_NAME" IS 'Name of the subclass.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."BUYER" IS 'Buyer for the item department.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."BUYER_NAME" IS 'Name of the buyer.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_1" IS 'Item''s differentiator 1.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_1_DESC" IS 'Description of differentiator 1.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_2" IS 'Item''s differentiator 2.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_2_DESC" IS 'Description of differentiator 2.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_3" IS 'Item''s differentiator 3.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_3_DESC" IS 'Description of differentiator 3.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_4" IS 'Item''s differentiator 4.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DIFF_4_DESC" IS 'Description of differentiator 4.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."PACK_IND" IS 'Indicates if item is a pack.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."COMP_ITEM" IS 'Component item if item is a pack.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."COMP_ITEM_DESC" IS 'Component item description.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SUPPLIER" IS 'Supplier site.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SUP_NAME" IS 'Supplier site name.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ORIGIN_COUNTRY_ID" IS 'Country of sourcing.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."POOL_SUPPLIER" IS 'Pool supplier ID.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."POOL_SUP_NAME" IS 'Name of the pool supplier.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."LOC_TYPE" IS 'Location type - S for store, W for warehouse.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."LOC_TYPE_DESC" IS 'Decoded description of the location type - e.g. store, warehouse.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."LOCATION" IS 'Store or virtual warehouse.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."LOC_NAME" IS 'Location name.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."PHYSICAL_WH" IS 'Physical wh of the location is the location is a warehouse.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."PHYS_WH_NAME" IS 'Name of the physical warehosue.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."CURRENT_DSCNT" IS 'Difference between future cost and current cost for investment buy.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ROI" IS 'Return over investment for investment buy.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."RAW_ROQ" IS 'Raw recommended order quantity.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ORDER_ROQ" IS 'Actual order quantity.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."STANDARD_UOM" IS 'Item''s standard unit of measure.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."TI" IS 'Number of shipping units (cases) that make up one tier of a pallet.  Multiply TI x HI to get total number of units (cases) for a pallet.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."HI" IS 'Number of tiers that make up a complete pallet (height).  Multiply TI x HI to get total number of units (cases) for a pallet.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."TERMS" IS 'Supplier''s terms.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."TERMS_CODE" IS 'Supplier''s terms code.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."TOTAL_LEAD_TIME" IS 'Total expected number of days required for the item to be ready for pickup at the receiving location.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."SUPP_UNIT_COST" IS 'This field is used to store the unit cost of the item specified at the item/supplier/origin country/location level.  It will never be modified.  This field, in conjunction with the unit cost field, will determine the cost source to be used at the order/item/location level.  It is stored in the suppliers currency.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."PRIM_SUPP_UNIT_COST" IS 'Supplier unit cost converted to primary currency.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."UNIT_COST" IS 'The unit cost of the item defaulted from the item/supplier/origin country/location level.  It may be edited.  This field, in conjunction with the supplier unit cost field, will determine the cost source to be used at the order/item/location level.  It is stored in the suppliers currency.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."PRIM_UNIT_COST" IS 'Unit cost converted to primary currency.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."CURRENCY_CODE" IS 'Supplier currency code.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."PRIM_CURR_CODE" IS 'Primary currency code.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."CREATE_DATE" IS 'Replenishment date or the create date of either investment buy or manual buyer worksheet.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DAYS_TO_EVENT" IS 'Contains the number of days until the deal end or cost increase that prompted the investment buy opportunity.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."NEXT_EVENT_DATE" IS 'Contains the next upcoming cost event that occurs after the current cost event (that prompted the investment buy opportunity) and causes the items future cost to be equal to or lesser than the current cost. If no upcoming events satisfy the above criteria, the column will not be populated. This date is provided since upcoming cost events that cause the future cost to be less than or equal to the current cost can affect the recommended order quantity of the investment buy.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."TARGET_DATE" IS 'Contains the date through which the RAW_ROQ should supply the location, based on existing forecasts.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."STATUS" IS 'Contains the status associated with the line item. Valid values will include Worksheet, PO  processed, Unprocessed and Deleted.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."TSF_PO_LINK_NO" IS 'A reference number to link the item on the transfer to any purchase orders that have been created to allow the from location (i.e. warehouse) on the transfer to fulfill the transfer quantity to the to location (i.e store) on the transfer.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ORDER_EXIST" IS 'Indicates if the order exists for the item line.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."DUE_IND" IS 'Indicates if the order is due for replenishment.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."AUDSID" IS 'This column will store the auditing session ID when the user selects this record to create purchase orders with from the Buyer Worksheet form.  This value will be used to identify which records should be grouped together for the purchase order and rounding libraries per user.'
/
COMMENT ON COLUMN V_BUYER_WKSHT_SEARCH."ROW_ID" IS 'Rowid of the entity being modified - repl_results, ib_results, buyer_wksht_manual.'   
/
