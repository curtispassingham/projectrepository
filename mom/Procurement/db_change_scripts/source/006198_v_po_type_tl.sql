CREATE OR REPLACE FORCE VIEW V_PO_TYPE_TL (PO_TYPE, PO_TYPE_DESC, LANG ) AS
SELECT  b.po_type,
        case when tl.lang is not null then tl.po_type_desc else b.po_type_desc end po_type_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  PO_TYPE b,
        PO_TYPE_TL tl
 WHERE  b.po_type = tl.po_type (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_PO_TYPE_TL is 'This is the translation view for base table PO_TYPE. This view fetches data in user langauge either from translation table PO_TYPE_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_PO_TYPE_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_PO_TYPE_TL.PO_TYPE is 'This field contains the unique identifier for the order type.'
/

COMMENT ON COLUMN V_PO_TYPE_TL.PO_TYPE_DESC is 'This field contains a description for the specific order type.'
/

