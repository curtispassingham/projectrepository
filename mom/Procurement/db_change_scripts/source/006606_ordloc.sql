--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit


--------------------------------------
--       Dropping Index
--------------------------------------

PROMPT Dropping Index 'ORDLOC_I1'
DECLARE
  L_index_exists number := 0;
BEGIN
  SELECT count(*) INTO L_index_exists
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = 'ORDLOC_I1'
     AND OBJECT_TYPE = 'INDEX';

  if (L_index_exists != 0) then
      execute immediate 'DROP INDEX ORDLOC_I1';
  end if;
end;
/

--------------------------------------
--       Creating Index
--------------------------------------
CREATE INDEX ORDLOC_I1 ON ORDLOC (ITEM, LOCATION, LOC_TYPE, ORDER_NO) 
  INITRANS 24
  TABLESPACE RETAIL_INDEX 
  NOLOGGING
  PARALLEL
/

ALTER INDEX ORDLOC_I1 LOGGING NOPARALLEL
/