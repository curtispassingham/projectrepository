--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'STAGE_COMPLEX_DEAL_HEAD'
ALTER TABLE STAGE_COMPLEX_DEAL_HEAD ADD UPLOAD_IND VARCHAR2 (1 ) DEFAULT 'N' NOT NULL
/

COMMENT ON COLUMN STAGE_COMPLEX_DEAL_HEAD.UPLOAD_IND is 'Reim Upload Indicator'
/

