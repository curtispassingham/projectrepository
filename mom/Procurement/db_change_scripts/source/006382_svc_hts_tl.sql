--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_HTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT CREATING TABLE 'SVC_HTS_TL'
CREATE TABLE SVC_HTS_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, HTS VARCHAR2(25)
, IMPORT_COUNTRY_ID VARCHAR2(3)
, EFFECT_FROM DATE
, EFFECT_TO DATE
, HTS_DESC VARCHAR2(2000)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_HTS_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in HTS_TL.'
/

COMMENT ON COLUMN SVC_HTS_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_HTS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_HTS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_HTS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_HTS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_HTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_HTS_TL.HTS is 'Contains the unique HTS item classification number.  This number is standard for the importing country.'
/

COMMENT ON COLUMN SVC_HTS_TL.IMPORT_COUNTRY_ID is 'Contains the country id of the importing country.'
/

COMMENT ON COLUMN SVC_HTS_TL.EFFECT_FROM is 'Denotes the beginning of the time period that the HTS classification is valid.'
/

COMMENT ON COLUMN SVC_HTS_TL.EFFECT_TO is 'Denotes the end of the time period that the HTS classification is valid.'
/

COMMENT ON COLUMN SVC_HTS_TL.HTS_DESC is 'The HTS description of the item classification.'
/

PROMPT CREATING PRIMARY KEY ON 'SVC_HTS_TL'
ALTER TABLE SVC_HTS_TL
ADD CONSTRAINT SVC_HTS_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING UNIQUE KEY ON 'SVC_HTS_TL'
ALTER TABLE SVC_HTS_TL
ADD CONSTRAINT SVC_HTS_TL_UK UNIQUE
(LANG, HTS, IMPORT_COUNTRY_ID, EFFECT_FROM, EFFECT_TO)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

