CREATE OR REPLACE FORCE VIEW V_HTS_CHAPTER_RESTRAINTS_TL (CHAPTER, IMPORT_COUNTRY_ID, ORIGIN_COUNTRY_ID, RESTRAINT_TYPE, RESTRAINT_DESC, LANG ) AS
SELECT  b.chapter,
        b.import_country_id,
        b.origin_country_id,
        b.restraint_type,
        case when tl.lang is not null then tl.restraint_desc else b.restraint_desc end restraint_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  HTS_CHAPTER_RESTRAINTS b,
        HTS_CHAPTER_RESTRAINTS_TL tl
 WHERE  b.chapter = tl.chapter (+)
   AND  b.import_country_id = tl.import_country_id (+)
   AND  b.origin_country_id = tl.origin_country_id (+)
   AND  b.restraint_type = tl.restraint_type (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_HTS_CHAPTER_RESTRAINTS_TL is 'This is the translation view for base table HTS_CHAPTER_RESTRAINTS. This view fetches data in user langauge either from translation table HTS_CHAPTER_RESTRAINTS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_RESTRAINTS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_RESTRAINTS_TL.CHAPTER is 'Contains the unique HTS Chapter number.  The unique chapter is defined by the HTS schedule for all GATT signatories.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_RESTRAINTS_TL.IMPORT_COUNTRY_ID is 'Contains the country id of the importing country.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_RESTRAINTS_TL.ORIGIN_COUNTRY_ID is 'Contains the country id of the country of origin.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_TYPE is 'Contains the type of restraint imposed on certain items.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_DESC is 'Contains the description of the chapter restraint.'
/

