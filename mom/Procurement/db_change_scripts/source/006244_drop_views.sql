--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review This script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEWS DROPPED:               V_ORDSEARCH
--						 V_ORDRESULTS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       DROPPING VIEWS
--------------------------------------

DROP VIEW V_ORDSEARCH
/

DROP VIEW V_ORDRESULTS
/