--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       TARIFF_TREATMENT_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE TARIFF_TREATMENT_TL(
LANG NUMBER(6) NOT NULL,
TARIFF_TREATMENT VARCHAR2(10) NOT NULL,
TARIFF_TREATMENT_DESC VARCHAR2(120) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE TARIFF_TREATMENT_TL is 'This is the translation table for TARIFF_TREATMENT table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.TARIFF_TREATMENT is 'Contains the customs approved code to uniquely identify a special tariff program.'
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.TARIFF_TREATMENT_DESC is 'Contains the description for the tariff treatment ID.  Example: NAFTA is North American Free Trade Agreement.'
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN TARIFF_TREATMENT_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE TARIFF_TREATMENT_TL ADD CONSTRAINT PK_TARIFF_TREATMENT_TL PRIMARY KEY (
LANG,
TARIFF_TREATMENT
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE TARIFF_TREATMENT_TL
 ADD CONSTRAINT TTT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE TARIFF_TREATMENT_TL ADD CONSTRAINT TTT_TT_FK FOREIGN KEY (
TARIFF_TREATMENT
) REFERENCES TARIFF_TREATMENT (
TARIFF_TREATMENT
)
/

