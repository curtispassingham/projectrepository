CREATE OR REPLACE FORCE VIEW V_DEAL_ATTRIB_DEF_TL (DEAL_ATTRIB_TYPE_ID, DEAL_ATTRIB_DESC, LANG ) AS
SELECT  b.deal_attrib_type_id,
        case when tl.lang is not null then tl.deal_attrib_desc else b.deal_attrib_desc end deal_attrib_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  DEAL_ATTRIB_DEF b,
        DEAL_ATTRIB_DEF_TL tl
 WHERE  b.deal_attrib_type_id = tl.deal_attrib_type_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_DEAL_ATTRIB_DEF_TL is 'This is the translation view for base table DEAL_ATTRIB_DEF. This view fetches data in user langauge either from translation table DEAL_ATTRIB_DEF_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_DEAL_ATTRIB_DEF_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_DEAL_ATTRIB_DEF_TL.DEAL_ATTRIB_TYPE_ID is 'Deal attribute type id.  For ease of use and filtering of values, all deal attributes must be assigned a deal_attrib_type.  Valid values for this field can be found in the code_type DACT.  Additional values may be added to the code type.  This field is required by the database'
/

COMMENT ON COLUMN V_DEAL_ATTRIB_DEF_TL.DEAL_ATTRIB_DESC is 'Description of the deal attribute.'
/

