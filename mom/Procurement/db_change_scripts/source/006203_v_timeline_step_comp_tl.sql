CREATE OR REPLACE FORCE VIEW V_TIMELINE_STEP_COMP_TL (TIMELINE_TYPE, STEP_NO, STEP_DESC, LANG ) AS
SELECT  b.timeline_type,
        b.step_no,
        case when tl.lang is not null then tl.step_desc else b.step_desc end step_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TIMELINE_STEP_COMP b,
        TIMELINE_STEP_COMP_TL tl
 WHERE  b.timeline_type = tl.timeline_type (+)
   AND  b.step_no = tl.step_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TIMELINE_STEP_COMP_TL is 'This is the translation view for base table TIMELINE_STEP_COMP. This view fetches data in user langauge either from translation table TIMELINE_STEP_COMP_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TIMELINE_STEP_COMP_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TIMELINE_STEP_COMP_TL.TIMELINE_TYPE is 'This field will hold the module the timeline is referring to. Valid values are: PO - Purchase Orders at the Header Level POIT - Purchase Orders at the Item Level IT - Item Maintenance CE - Customs Entry LO - Logistics at the Header Level LOBL - Logistics at the Bill of Lading Level LOCO - Logistics at the Container Level LOPI - Logistics at the Purchase Order/Item Level LOCI - Logistics at the Commercial Invoice Level'
/

COMMENT ON COLUMN V_TIMELINE_STEP_COMP_TL.STEP_NO is 'This field contains the number that uniquely defines the timeline step.'
/

COMMENT ON COLUMN V_TIMELINE_STEP_COMP_TL.STEP_DESC is 'This field contains the description of the timeline step.'
/

