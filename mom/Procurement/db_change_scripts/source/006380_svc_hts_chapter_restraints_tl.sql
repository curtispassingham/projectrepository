--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Creating Table :                               SVC_HTS_CHAPTER_RESTRAINTS_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

PROMPT CREATING TABLE 'SVC_HTS_CHAPTER_RESTRAINTS_TL'
CREATE TABLE SVC_HTS_CHAPTER_RESTRAINTS_TL(
  PROCESS_ID NUMBER(10) NOT NULL
, CHUNK_ID NUMBER(10) NOT NULL
, ROW_SEQ NUMBER(20) NOT NULL
, ACTION VARCHAR2(10)
, PROCESS$STATUS VARCHAR2(10)
, LANG NUMBER(6)
, CHAPTER VARCHAR2(4)
, IMPORT_COUNTRY_ID VARCHAR2(3)
, ORIGIN_COUNTRY_ID VARCHAR2(3)
, RESTRAINT_TYPE VARCHAR2(6)
, RESTRAINT_DESC VARCHAR2(120)
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE SVC_HTS_CHAPTER_RESTRAINTS_TL is 'This is a staging table used for Admin API spreadsheet upload process.It is used to temporarily hold data before it is uploaded/updated in HTS_CHAPTER_RESTRAINTS_TL.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS_ID is 'Uniquely identifies a process in SVC_PROCESS_TRACKER.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.CHUNK_ID is 'Uniquely identifies a chunk. The value will always be 1.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.ROW_SEQ is 'The rows sequence. Should be unique within a Process-ID.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.ACTION is 'Describes type of action i.e. NEW, MOD or DEL.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.PROCESS$STATUS is 'Status of current row. Could be N - New, P-Processed, E-Error.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.CHAPTER is 'Contains the unique HTS Chapter number.  The unique chapter is defined by the HTS schedule for all GATT signatories.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.IMPORT_COUNTRY_ID is 'Contains the country id of the importing country.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.ORIGIN_COUNTRY_ID is 'Contains the country id of the country of origin.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_TYPE is 'Contains the type of restraint imposed on certain items.'
/

COMMENT ON COLUMN SVC_HTS_CHAPTER_RESTRAINTS_TL.RESTRAINT_DESC is 'Contains the description of the chapter restraint.'
/

PROMPT CREATING PRIMARY KEY ON 'SVC_HTS_CHAPTER_RESTRAINTS_TL'
ALTER TABLE SVC_HTS_CHAPTER_RESTRAINTS_TL
ADD CONSTRAINT SVC_HTS_CHAP_RESTRAINTS_TL_PK PRIMARY KEY ( PROCESS_ID, ROW_SEQ )
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

PROMPT CREATING UNIQUE KEY ON 'SVC_HTS_CHAPTER_RESTRAINTS_TL'
ALTER TABLE SVC_HTS_CHAPTER_RESTRAINTS_TL
ADD CONSTRAINT SVC_HTS_CHAP_RESTRAINTS_TL_UK UNIQUE
(LANG, CHAPTER, IMPORT_COUNTRY_ID, ORIGIN_COUNTRY_ID, RESTRAINT_TYPE)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

