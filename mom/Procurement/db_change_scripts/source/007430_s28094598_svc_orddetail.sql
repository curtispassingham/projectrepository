--------------------------------------------------------
-- Copyright (c) 2018, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'SVC_ORDDETAIL'
ALTER TABLE SVC_ORDDETAIL ADD IN_STORE_DATE DATE
/
COMMENT ON COLUMN SVC_ORDDETAIL.IN_STORE_DATE is 'This column contains In Store Date for Allocations that are created from the cross dock Purchase Orders via the PO induction.'
/
