--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       CVB_HEAD_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE CVB_HEAD_TL(
LANG NUMBER(6) NOT NULL,
CVB_CODE VARCHAR2(10) NOT NULL,
CVB_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE CVB_HEAD_TL is 'This is the translation table for CVB_HEAD table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN CVB_HEAD_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN CVB_HEAD_TL.CVB_CODE is 'Contains a user specified code representing a computation base that will be used by expenses.'
/

COMMENT ON COLUMN CVB_HEAD_TL.CVB_DESC is 'Contains the description or name of the Computation Value Base.'
/

COMMENT ON COLUMN CVB_HEAD_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN CVB_HEAD_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN CVB_HEAD_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN CVB_HEAD_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE CVB_HEAD_TL ADD CONSTRAINT PK_CVB_HEAD_TL PRIMARY KEY (
LANG,
CVB_CODE
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE CVB_HEAD_TL
 ADD CONSTRAINT CVBT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE CVB_HEAD_TL ADD CONSTRAINT CVBT_CVB_FK FOREIGN KEY (
CVB_CODE
) REFERENCES CVB_HEAD (
CVB_CODE
)
/

