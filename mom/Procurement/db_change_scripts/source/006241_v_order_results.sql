--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--  The customer DBA is responsible to review this script to ensure
--  data is preserved as desired.
--
----------------------------------------------------------------------------
--  VIEW UPDATED:               V_ORDRESULTS
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_ORDER_RESULTS'
CREATE OR REPLACE FORCE VIEW V_ORDER_RESULTS 
(ORDER_NO,
 STATUS,
 STATUS_DESC,
 SUPPLIER_SITE,
 SUPPLIER_SITE_NAME,
 NOT_BEFORE_DATE,
 NOT_AFTER_DATE,
 WRITTEN_DATE,
 ORDER_TYPE,
 CURRENCY_CODE,
 IMPORT_ORDER_IND,
 IMPORT_COUNTRY_ID,
 CONTRACT_NO,
 ORIG_IND,              
 SPLIT_REF_ORDNO,
 VENDOR_ORDER_NO,
 PO_TYPE,
 PROMOTION, 
 BUYER,
 BUYER_NAME,
 TERMS,
 TERMS_DESC,
 FREIGHT_TERMS,
 FREIGHT_TERM_DESC,
 OTB_EOW_DATE,
 EARLIEST_SHIP_DATE,
 LATEST_SHIP_DATE,
 ORIG_APPROVAL_DATE,
 ORIG_APPROVAL_ID,
 CLOSE_DATE,
 SHIP_METHOD,
 PURCHASE_TYPE,
 EDI_PO_IND,
 QC_IND,
 LADING_PORT,
 DISCHARGE_PORT,
 LOCATION,
 LOC_TYPE,
 LOCATION_NAME,
 DEPT,
 DEPT_NAME,
 POOL_SUP_PARENT,
 POOL_SUP_PARENT_NAME,
 DUE_ORD_IND)
 AS
 select o.order_no,
        o.status,
        (select cd.code_desc
           from v_code_detail cd
          where cd.code_type = 'ORST'
            and o.status = cd.code) status_desc,
        o.supplier supplier_site,
        (select vss.sup_name
           from v_sups_tl vss
          where vss.supplier = ss.supplier) supplier_site_name,
        o.NOT_BEFORE_DATE,
        o.NOT_AFTER_DATE,
        o.written_date,
        (select cd.code_desc
           from v_code_detail cd
          where cd.code_type = 'ORDO'
            and o.order_type = cd.code) order_type,
        o.currency_code,
        o.import_order_ind,
        o.import_country_id,
        o.contract_no,
        (select cd.code_desc
           from v_code_detail cd
          where cd.code_type = 'OROR'
            and o.orig_ind = cd.code) orig_ind ,              
        o.SPLIT_REF_ORDNO,
        o.vendor_order_no,
        (select cd.po_type_desc
           from v_po_type_tl cd
          where o.po_type = cd.po_type) po_type,
        o.Promotion, 
        o.buyer,
        (select buyer_name
           from buyer b
          where b.buyer = o.buyer) buyer_name,
        o.terms,
        (select th.terms_desc
           from V_TERMS_HEAD_TL th
          where o.terms = th.terms) terms_desc,
        o.freight_terms,
        (select ft.term_desc
           from V_FREIGHT_TERMS_TL ft
          where ft.freight_terms = o.freight_terms) freight_term_desc,
        o.otb_eow_date,
        o.earliest_ship_date,
        o.latest_ship_date,
        o.orig_approval_date,
        o.orig_approval_id,
        o.close_date,
        (select cd.code_desc
           from v_code_detail cd
          where cd.code_type = 'SHPM'
            and o.ship_method = cd.code) ship_method,
        (select cd.code_desc
           from v_code_detail cd
          where cd.code_type = 'PURT'
            and o.purchase_type = cd.code)purchase_type,
        o.edi_po_ind,
        o.qc_ind,
        (select vo.outloc_desc
           from v_outloc_tl vo
          where o.lading_port = vo.outloc_id
            and vo.outloc_type = 'LP') lading_port,
        (select vo.outloc_desc
           from v_outloc_tl vo
          where o.discharge_port = vo.outloc_id
            and vo.outloc_type = 'DP') discharge_port,
        o.location,
        o.loc_type,
        loc.location_name,
        o.dept,
        (select d.dept_name
           from v_deps_tl d
          where o.dept = d.dept) dept_name,
        oim.pool_sup_parent,
        oim.pool_sup_parent_name,
        oim.due_ord_ind                 
   from ordhead o,
        v_sups ss,
        v_sups sp,
        (select oi.order_no,
                sp.supplier pool_sup_parent,
                (select vsp.sup_name
                   from v_sups_tl vsp
                  where vsp.supplier = sp.supplier)  pool_sup_parent_name,
                oi.due_ord_ind
           from ord_inv_mgmt oi,
                v_sups ss,
                v_sups sp
          where oi.pool_supplier = ss.Supplier
            and ss.Supplier_Parent = sp.supplier
            and sp.supplier_parent is NULL) oim,
        (select vs.store location,
                'S' location_type,
                vtl.store_name location_name 
           from v_store vs,
                v_store_tl vtl
          where vs.store = vtl.store
         union all
         select w.wh location,
                'W' location_type,
                wtl.wh_name location_name
           from v_wh w,
                v_wh_tl wtl
          where w.wh = wtl.wh)loc
  where o.supplier = ss.Supplier
    and ss.Supplier_Parent = sp.supplier
    and sp.supplier_parent is NULL
    and o.order_no = oim.order_no(+)
    and o.location = loc.location(+)
    and o.loc_type = loc.location_type(+)
/

COMMENT ON TABLE V_ORDER_RESULTS IS 'This view is used for the order search screen in RMS Alloy. It includes all fields that would be shown in the results section.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.ORDER_NO IS 'Purchase Order Number.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.STATUS IS 'The current status of the Purchase Order.'
/
COMMENT ON COLUMN V_ORDER_RESULTS.STATUS_DESC IS 'The description of status of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.SUPPLIER_SITE IS 'The supplier site where the Purchase Order is placed.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.SUPPLIER_SITE_NAME IS 'Translated name of the supplier site.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.NOT_BEFORE_DATE IS 'Contains the first date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.NOT_AFTER_DATE IS 'Contains the last date that delivery of the Purchase order will be accepted.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.WRITTEN_DATE IS 'Contains the date the Purchase order was created within the system.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.ORDER_TYPE IS 'Indicates which Open To Buy bucket would be updated.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.CURRENCY_CODE IS 'Currency of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.IMPORT_ORDER_IND IS 'Indicates if the purchase order is an import order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.IMPORT_COUNTRY_ID IS 'The identifier of the country into which the items on the order are being imported.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.CONTRACT_NO IS 'Contains the contract number associated with this order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.ORIG_IND IS 'Indicates where the Purchase Order originated.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.SPLIT_REF_ORDNO IS 'This column will store the original order number from which the split orders were generated from.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.VENDOR_ORDER_NO IS 'Contains the vendor number who will provide the merchandise specified in the order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.PO_TYPE  IS 'Contains the context of the Purchase Order creation.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.PROMOTION  IS 'Contains the promotion number associated with the order.' 
/

COMMENT ON COLUMN V_ORDER_RESULTS.BUYER  IS 'Contains the number associated with the buyer for the order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.BUYER_NAME IS 'Name of the Buyer associated with the order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.TERMS IS 'This contains the sales terms for the order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.TERMS_DESC IS 'Translated description of the terms.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.FREIGHT_TERMS IS 'This contains the freight terms for the order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.FREIGHT_TERM_DESC IS 'Translated description of the freight terms.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.OTB_EOW_DATE IS 'This field contains the OTB budget bucket the order amount should be placed into.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.EARLIEST_SHIP_DATE IS 'The date before which the items on the purchase order can not be shipped by the supplier.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.LATEST_SHIP_DATE IS 'The date after which the items on the purchase order can not be shipped by the supplier.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.ORIG_APPROVAL_DATE IS 'Contains the date that the order was originally approved.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.ORIG_APPROVAL_ID IS 'Contains the date that the order was originally approved.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.CLOSE_DATE IS 'Contains the date when the order is closed.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.SHIP_METHOD IS 'The method used to ship the items on the purchase order from the country of origin to the country of import.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.PURCHASE_TYPE IS 'Indicates whats included in the suppliers cost of the item.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.EDI_PO_IND IS 'Indicates whether or not the order will be transmitted to the supplier via an Electronic Data Exchange transaction.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.QC_IND IS 'Determines whether or not quality control will be required when items for this order are received.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.LADING_PORT IS 'The port from which the items on the purchase order are shipped.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.DISCHARGE_PORT IS 'The port at which the items on the purchase order will enter the country of import.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.LOCATION IS 'The specific location for which the Purchase Order is created.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.LOC_TYPE IS 'The location type for which the Purchase Order is created.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.LOCATION_NAME IS 'The translated name of the specific location of the Purchase Order.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.DEPT IS 'This contains the specific department to which all the items on the Order belongs.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.DEPT_NAME IS 'The translated name of the specific department.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.POOL_SUP_PARENT IS 'This field will contain the parent of the pooled supplier site.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.POOL_SUP_PARENT_NAME IS 'This field will contain the translated name of the pooled supplier parent.'
/

COMMENT ON COLUMN V_ORDER_RESULTS.DUE_ORD_IND IS 'Indicator used to determine if Due Order Processing will be used when creating orders for the supplier or supplier/department.'
/


