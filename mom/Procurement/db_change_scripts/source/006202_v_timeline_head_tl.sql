CREATE OR REPLACE FORCE VIEW V_TIMELINE_HEAD_TL (TIMELINE_NO, TIMELINE_DESC, LANG ) AS
SELECT  b.timeline_no,
        case when tl.lang is not null then tl.timeline_desc else b.timeline_desc end timeline_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  TIMELINE_HEAD b,
        TIMELINE_HEAD_TL tl
 WHERE  b.timeline_no = tl.timeline_no (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_TIMELINE_HEAD_TL is 'This is the translation view for base table TIMELINE_HEAD. This view fetches data in user langauge either from translation table TIMELINE_HEAD_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_TIMELINE_HEAD_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_TIMELINE_HEAD_TL.TIMELINE_NO is 'This field contains a number that uniquely identifies the timeline.'
/

COMMENT ON COLUMN V_TIMELINE_HEAD_TL.TIMELINE_DESC is 'This field contains the description of the timeline.'
/

