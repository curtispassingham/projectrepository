--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'ORDHEAD'
ALTER TABLE ORDHEAD MODIFY LAST_UPDATE_DATETIME DEFAULT SYSDATE
/

COMMENT ON COLUMN ORDHEAD.LAST_UPDATE_DATETIME is 'Holds the date time stamp of the most recent update by the last_update_id.  This field is required by the database.'
/

