--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------
PROMPT Modifying Table
CREATE INDEX DEAL_ITEMLOC_ITEM_I2 ON DEAL_ITEMLOC_ITEM 
(
ITEM, 
ACTIVE_IND
) 
INITRANS 12 LOCAL
TABLESPACE RETAIL_INDEX 
/
 
 