--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--    ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--
----------------------------------------------------------------------------
-- VIEW UPDATED:           V_HTS_CHAPTER_TL
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       UPDATING VIEW
--------------------------------------
PROMPT Creating View 'V_HTS_CHAPTER_TL'
CREATE OR REPLACE FORCE VIEW V_HTS_CHAPTER_TL (CHAPTER, IMPORT_COUNTRY_ID, CHAPTER_DESC, LANG ) AS
SELECT  b.chapter,
        b.import_country_id,
        case when tl.lang is not null then tl.chapter_desc else b.chapter_desc end chapter_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  HTS_CHAPTER b,
        HTS_CHAPTER_TL tl
 WHERE  b.chapter = tl.chapter (+)
   AND  b.import_country_id = tl.import_country_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_HTS_CHAPTER_TL is 'This is the translation view for base table HTS_CHAPTER. This view fetches data in user langauge either from translation table HTS_CHAPTER_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_TL.CHAPTER is 'Contains the unique HTS Chapter number.  The unique number is defined by the HTS schedule for all GATT signatories.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_TL.CHAPTER_DESC is 'Contains the description of the HTS Chapter.'
/

COMMENT ON COLUMN V_HTS_CHAPTER_TL.IMPORT_COUNTRY_ID is 'This Column holds the import country'
/

