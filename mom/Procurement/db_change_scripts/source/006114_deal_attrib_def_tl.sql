--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE CREATED:			       DEAL_ATTRIB_DEF_TL
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       CREATING TABLE
--------------------------------------
CREATE TABLE DEAL_ATTRIB_DEF_TL(
LANG NUMBER(6) NOT NULL,
DEAL_ATTRIB_TYPE_ID VARCHAR2(6) NOT NULL,
DEAL_ATTRIB_DESC VARCHAR2(250) NOT NULL,
CREATE_DATETIME DATE NOT NULL,
CREATE_ID VARCHAR2(30) NOT NULL,
LAST_UPDATE_DATETIME DATE NOT NULL,
LAST_UPDATE_ID VARCHAR2(30) NOT NULL
)
INITRANS 6
TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE DEAL_ATTRIB_DEF_TL is 'This is the translation table for DEAL_ATTRIB_DEF table. The data in the base table is held in data integration langauge configured in SYSTEM_CONFIG_OPTIONS. The translated value in other languages are maintained in this table. '
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.LANG is 'This field contains the language in which the translated text is maintained. '
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.DEAL_ATTRIB_TYPE_ID is 'Deal attribute type id.  For ease of use and filtering of values, all deal attributes must be assigned a deal_attrib_type.  Valid values for this field can be found in the code_type DACT.  Additional values may be added to the code type.  This field is required by the database'
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.DEAL_ATTRIB_DESC is 'Description of the deal attribute.'
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.CREATE_DATETIME is 'This field contains the timestamp when the record is created.'
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.CREATE_ID is 'This field contains the user that created the record.'
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.LAST_UPDATE_DATETIME is 'This field contains the timestamp when the record is last updated.'
/

COMMENT ON COLUMN DEAL_ATTRIB_DEF_TL.LAST_UPDATE_ID is 'This field contains the user that last updated the record.'
/

ALTER TABLE DEAL_ATTRIB_DEF_TL ADD CONSTRAINT PK_DEAL_ATTRIB_DEF_TL PRIMARY KEY (
LANG,
DEAL_ATTRIB_TYPE_ID
)
USING INDEX
INITRANS 12
TABLESPACE RETAIL_INDEX
/

ALTER TABLE DEAL_ATTRIB_DEF_TL
 ADD CONSTRAINT DADT_LANG_FK
 FOREIGN KEY (LANG)
 REFERENCES LANG (LANG)
/

ALTER TABLE DEAL_ATTRIB_DEF_TL ADD CONSTRAINT DADT_DAD_FK FOREIGN KEY (
DEAL_ATTRIB_TYPE_ID
) REFERENCES DEAL_ATTRIB_DEF (
DEAL_ATTRIB_TYPE_ID
)
/

