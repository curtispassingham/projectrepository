CREATE OR REPLACE FORCE VIEW V_ENTRY_STATUS_TL (ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, LANG ) AS
SELECT  b.entry_status,
        b.import_country_id,
        case when tl.lang is not null then tl.entry_status_desc else b.entry_status_desc end entry_status_desc,
        NVL(tl.lang,GET_PRIMARY_LANG()) lang
  FROM  ENTRY_STATUS b,
        ENTRY_STATUS_TL tl
 WHERE  b.entry_status = tl.entry_status (+)
   AND  b.import_country_id = tl.import_country_id (+)
   AND  tl.lang (+) = LANGUAGE_SQL.GET_USER_LANGUAGE()
/

COMMENT ON TABLE V_ENTRY_STATUS_TL is 'This is the translation view for base table ENTRY_STATUS. This view fetches data in user langauge either from translation table ENTRY_STATUS_TL or from the base table. If the record in user langauge is not present, record from base table is fetched which is in the system data integration language.'
/

COMMENT ON COLUMN V_ENTRY_STATUS_TL.LANG is 'This column contains the language for this record.'
/

COMMENT ON COLUMN V_ENTRY_STATUS_TL.ENTRY_STATUS is 'This will hold the unique identifier for the custom defined entry status.'
/

COMMENT ON COLUMN V_ENTRY_STATUS_TL.IMPORT_COUNTRY_ID is 'This column will hold the import country.'
/

COMMENT ON COLUMN V_ENTRY_STATUS_TL.ENTRY_STATUS_DESC is 'This column will hold the description of the entry status.'
/

