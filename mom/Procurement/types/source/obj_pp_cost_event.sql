drop type obj_pp_cost_event_tbl FORCE
/

drop type obj_pp_cost_event_rec FORCE
/

create or replace TYPE obj_pp_cost_event_rec AS OBJECT (
ITEM          VARCHAR2(25),
LOCATION      NUMBER(10),
PACK_NO       VARCHAR2(25)
)
/

create or replace TYPE obj_pp_cost_event_tbl AS TABLE OF obj_pp_cost_event_rec
/
