----------------------------------------------------------------------------
-- OBJECT CREATED :  OBJ_ORDLOC_REC, OBJ_ORDLOC_TBL 
----------------------------------------------------------------------------

--------------------------------------
--       Dropping Types
--------------------------------------

PROMPT Dropping Object OBJ_ORDLOC_TBL

DROP TYPE OBJ_ORDLOC_TBL FORCE
/

PROMPT Dropping Object OBJ_ORDLOC_REC

DROP TYPE OBJ_ORDLOC_REC FORCE
/

--------------------------------------
--       Creating Types
--------------------------------------

PROMPT Creating Object OBJ_ORDLOC_REC

CREATE OR REPLACE TYPE OBJ_ORDLOC_REC AS OBJECT
(
   ORDER_NO               NUMBER(12),   
   ITEM                   VARCHAR2(25), 
   LOCATION               NUMBER(10),   
   LOC_TYPE               VARCHAR2(1),  
   UNIT_RETAIL            NUMBER(20,4), 
   QTY_ORDERED            NUMBER(12,4),
   QTY_PRESCALED          NUMBER(12,4), 
   QTY_RECEIVED           NUMBER(12,4), 
   QTY_CANCELLED          NUMBER(12,4), 
   CANCEL_CODE            VARCHAR2(1),  
   CANCEL_DATE            DATE,         
   CANCEL_ID              VARCHAR2(30), 
   UNIT_COST              NUMBER(20,4), 
   UNIT_COST_INIT         NUMBER(20,4), 
   COST_SOURCE            VARCHAR2(4),  
   NON_SCALE_IND          VARCHAR2(1),  
   ORIGINAL_REPL_QTY      NUMBER(12,4), 
   TSF_PO_LINK_NO         NUMBER(12),
   LAST_RECEIVED          NUMBER(12,4), 
   LAST_ROUNDED_QTY       NUMBER(12,4), 
   LAST_GRP_ROUNDED_QTY   NUMBER(12,4), 
   ESTIMATED_INSTOCK_DATE DATE
)
/ 

PROMPT Creating Object OBJ_ORDLOC_TBL

CREATE OR REPLACE TYPE OBJ_ORDLOC_TBL AS TABLE OF OBJ_ORDLOC_REC
/ 
