drop type obj_cc_cost_event_tbl FORCE
/

drop type obj_cc_cost_event_rec FORCE
/

create or replace TYPE obj_cc_cost_event_rec AS OBJECT (
COST_CHANGE  NUMBER(8,0),
SRC_TMP_IND  VARCHAR2(1)
)
/

create or replace TYPE obj_cc_cost_event_tbl AS TABLE OF obj_cc_cost_event_rec
/
