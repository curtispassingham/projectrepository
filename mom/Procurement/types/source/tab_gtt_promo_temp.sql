
-------------------------------------------------------
-- Copyright (c) 2000, Retek Inc.  All rights reserved.
-- $Workfile$
-- $Revision: 1.4 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	TABLE ADDED:				GTT_PROMO_TEMP
----------------------------------------------------------------------------

--------------------------------------
--       ADDING TABLE
--------------------------------------
PROMPT Creating Table 'GTT_PROMO_TEMP'
drop table GTT_PROMO_TEMP;
CREATE GLOBAL TEMPORARY TABLE GTT_PROMO_TEMP
OF OBJ_PROMO_REC 
ON COMMIT PRESERVE ROWS
/

PROMPT Creating Primary Key on 'GTT_PROMO_TEMP'
ALTER TABLE GTT_PROMO_TEMP
 ADD (CONSTRAINT PK_GTT_PROMO_TEMP PRIMARY KEY 
  (PROMO_ID))
/


 





