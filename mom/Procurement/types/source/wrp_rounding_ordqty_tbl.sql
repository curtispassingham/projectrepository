prompt dropping wrp_rounding_ordqty_rec AND wrp_rounding_ordqty_tbl
BEGIN
  EXECUTE immediate 'DROP type wrp_rounding_ordqty_rec force';
  EXECUTE immediate 'DROP type wrp_rounding_ordqty_tbl force';
EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/
--
prompt creating wrp_rounding_ordqty_rec
CREATE OR REPLACE type wrp_rounding_ordqty_rec
AS
  object
  (
    item            VARCHAR2(25),
    location        NUMBER(10),
    total_order_qty NUMBER(12,4) );
  /
  --
  prompt creating wrp_rounding_ordqty_tbl
CREATE type wrp_rounding_ordqty_tbl
AS
  TABLE OF wrp_rounding_ordqty_rec;
  /