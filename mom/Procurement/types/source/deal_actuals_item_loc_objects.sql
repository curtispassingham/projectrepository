DROP TYPE deal_actuals_item_loc_TBL FORCE
/
DROP TYPE deal_actuals_item_loc_REC FORCE
/

CREATE TYPE deal_actuals_item_loc_REC AS OBJECT ( 
  DEAL_ID                  NUMBER(10), 
  DEAL_DETAIL_ID           NUMBER(10), 
  REPORTING_DATE           DATE, 
  ITEM                     VARCHAR2(25), 
  LOCATION                 NUMBER(10), 
  LOC_TYPE                 VARCHAR2(1), 
  ACTUAL_TURNOVER_UNITS    NUMBER(12,4), 
  ACTUAL_TURNOVER_REVENUE  NUMBER(20,4), 
  ORDER_NO                 NUMBER(12), 
  ACTUAL_INCOME            NUMBER(20,4))
/



CREATE TYPE deal_actuals_item_loc_TBL AS TABLE OF deal_actuals_item_loc_REC
/
