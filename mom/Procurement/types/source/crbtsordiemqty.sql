DROP TYPE bts_orditem_qty_tbl FORCE
/
DROP TYPE bts_orditem_qty_rec FORCE
/
CREATE TYPE
   bts_orditem_qty_rec AS OBJECT
   (orderable_item           VARCHAR(25),
    sellable_item            VARCHAR(25),
    qty                      NUMBER(12,4),
    inv_status               NUMBER(2)
   ) final;
/

CREATE TYPE
    bts_orditem_qty_tbl
    AS TABLE OF bts_orditem_qty_rec
/
