DROP Type obj_rcls_cost_event_rec FORCE 
/

create or replace TYPE obj_rcls_cost_event_rec AS OBJECT (
RECLASS_NO            NUMBER(4)     ,                         
RECLASS_DATE          DATE         ,                        
ITEM                  VARCHAR2(25) ,                         
TO_DEPT               NUMBER(4)    ,                         
TO_CLASS              NUMBER(4)    ,                         
TO_SUBCLASS           NUMBER(4)    
)
/
--Table of reclass item detail recs
DROP Type obj_rcls_cost_event_tbl FORCE
/
create or replace TYPE obj_rcls_cost_event_tbl AS TABLE OF obj_rcls_cost_event_rec
/

