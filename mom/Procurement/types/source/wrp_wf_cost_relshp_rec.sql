DROP TYPE WRP_WF_COST_RELSHP_TBL FORCE
/
DROP TYPE WRP_WF_COST_RELSHP_REC FORCE
/
CREATE OR REPLACE TYPE WRP_WF_COST_RELSHP_REC AS OBJECT(
   DEPT            NUMBER(4),
   CLASS           NUMBER(4),
   SUBCLASS        NUMBER(4),
   ITEM            VARCHAR2(25),
   LOCATION        NUMBER(10),
   START_DATE      DATE,
   END_DATE        DATE
   )
/
CREATE OR REPLACE TYPE WRP_WF_COST_RELSHP_TBL AS TABLE OF WRP_WF_COST_RELSHP_REC
/