--WRP_DEAL_CRITERIA_REC
--------------------------------------
--       Dropping TYPE
--------------------------------------
PROMPT Dropping TYPE 'WRP_DEAL_CRITERIA_REC'

DROP TYPE WRP_DEAL_CRITERIA_REC FORCE
/
--------------------------------------
--       Creating TYPE
--------------------------------------
PROMPT Creating TYPE 'WRP_DEAL_CRITERIA_REC'
CREATE OR REPLACE TYPE WRP_DEAL_CRITERIA_REC AS OBJECT 
 (deal_id                  NUMBER(10),
  type                     VARCHAR2(6),
  status                   VARCHAR2(6),
  ext_ref_no               VARCHAR2(30),
  order_no                 NUMBER(12),
  currency                 VARCHAR2(3),
  active_date_min          DATE,
  active_date_max          DATE,
  close_date_min           DATE,
  close_date_max           DATE,
  partner_type             VARCHAR2(6),
  supplier                 VARCHAR2(10),
  sup_name                 VARCHAR2(240),
  division                 NUMBER(4),
  group_no                 NUMBER(4),
  dept                     NUMBER,
  class                    NUMBER,
  subclass                 NUMBER,
  item                     VARCHAR2(25),
  item_desc                VARCHAR2(250),
  chain                    NUMBER(10),
  area                     NUMBER(10),
  region                   NUMBER(10),
  district                 NUMBER(10),
  loc_type                 VARCHAR2(1),
  location                 NUMBER(10),
  deal_comp_type           VARCHAR2(6),
  billing_type             VARCHAR2(6),
  rebate_ind               VARCHAR2(1),
  growth_rebate_ind        VARCHAR2(1),
  rebate_purch_sales_ind   VARCHAR2(6))
/
