drop type obj_sc_cost_event_tbl FORCE
/

drop type obj_sc_cost_event_rec FORCE
/

create or replace TYPE obj_sc_cost_event_rec AS OBJECT (
ITEM                    VARCHAR2(25),
LOCATION                NUMBER(10),
SUPPLIER                NUMBER(10),
ORIGIN_COUNTRY_ID       VARCHAR2(3)
)
/

create or replace TYPE obj_sc_cost_event_tbl AS TABLE OF obj_sc_cost_event_rec
/
