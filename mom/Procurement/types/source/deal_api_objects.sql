
DROP Type deal_itemloc_TBL FORCE
/
DROP Type deal_itemloc_REC FORCE
/

CREATE TYPE deal_itemloc_REC AS OBJECT ( 
LOCATION              NUMBER(10),
MERCH_LEVEL_CODE      VARCHAR2(6),
MERCH_VALUE_1         VARCHAR2(25),
MERCH_VALUE_2         VARCHAR2(25),
MERCH_VALUE_3         VARCHAR2(25),
ORIGIN_COUNTRY_ID     VARCHAR2(3),
EXCL_IND              VARCHAR2(1)
)
/

CREATE TYPE deal_itemloc_TBL AS TABLE OF deal_itemloc_REC
/

DROP Type deal_comp_prom_tbl FORCE
/
DROP Type deal_comp_prom_REC FORCE
/

CREATE TYPE deal_comp_prom_REC AS OBJECT ( 
PROMOTION_ID          NUMBER(10),
PROMO_COMP_ID         NUMBER(10),
CONTRIBUTION_PCT      NUMBER(12,4)
)
/

CREATE TYPE deal_comp_prom_TBL AS TABLE OF deal_comp_prom_REC
/

DROP Type deal_detail_TBL FORCE
/
DROP Type deal_detail_REC FORCE
/

CREATE TYPE deal_detail_REC AS OBJECT ( 
DEAL_ID                 NUMBER(10),
DEAL_DETAIL_ID          NUMBER(10), -- if NULL need to fetch it
COLLECT_START_DATE      DATE,
COLLECT_END_DATE        DATE,
VFP_DEFAULT_CONTRIB_PCT NUMBER(12,4),
USER_ID                 VARCHAR2(30),
deal_itemloc_TABLE      deal_itemloc_TBL,
deal_comp_prom_TABLE    deal_comp_prom_TBL)
/


CREATE TYPE deal_detail_TBL AS TABLE OF deal_detail_REC
/

DROP Type deal_head_REC FORCE
/

CREATE TYPE deal_head_REC AS OBJECT ( 
PARTNER_TYPE      VARCHAR2(6),  --code type SUHL
PARTNER_ID        VARCHAR2(10), 
SUPPLIER          NUMBER(10),   
CURRENCY_CODE     VARCHAR2(3),
ACTIVE_DATE       DATE,
CLOSE_DATE        DATE,
BILL_BACK_PERIOD  VARCHAR2(6),
DEAL_REPORTING_LEVEL     VARCHAR2(6),
BILL_BACK_METHOD         VARCHAR2(6),
INVOICE_PROCESSING_LOGIC VARCHAR2(6),
STOCK_LEDGER_IND         VARCHAR2(1),
INCLUDE_VAT_IND          VARCHAR2(1),
USER_ID                  VARCHAR2(30),
deal_detail_TABLE        deal_detail_TBL)
/

DROP Type deal_ids_tbl FORCE
/
DROP Type deal_ids_rec FORCE
/

CREATE OR REPLACE TYPE deal_ids_REC AS OBJECT ( 
DEAL_ID           NUMBER(10),
DEAL_DETAIL_ID    NUMBER(10))
/


CREATE OR REPLACE TYPE deal_ids_TBL AS TABLE OF deal_ids_REC
/

