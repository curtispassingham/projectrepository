DROP TYPE PO_INDUCT_SCH_CRIT_TYP FORCE
/

CREATE OR REPLACE TYPE PO_INDUCT_SCH_CRIT_TYP AS OBJECT
(
    ORDER_NO              NUMBER(12),
    STATUS                VARCHAR2(1),
    SUPPLIER              NUMBER(10),
    BUYER                 NUMBER(4),
    DEPT                  NUMBER(4),
    CLASS                 NUMBER(4),
    SUBCLASS              NUMBER(4),
    ITEM                  VARCHAR2(25),
    ITEM_DESC             VARCHAR2(250),
    PROCESS_ID            NUMBER(10),
    ORIG_IND              NUMBER(1),
    ORDER_TYPE            VARCHAR2(3),
    IMPORT_ORDER_IND      VARCHAR2(1),
    IMPORT_COUNTRY_ID     VARCHAR2(3),
    CREATE_BEFORE_DATE    DATE,
    CREATE_AFTER_DATE     DATE,
    DELIVERY_BEFORE_DATE  DATE,
    DELIVERY_AFTER_DATE   DATE,
    LAST_UPD_ID           VARCHAR2(30),
    NEXT_UPD_ID           VARCHAR2(30),
    VENDOR_ORDER_NO       VARCHAR2(15),
    MASTER_PO_NO          VARCHAR2(25),
    LOC_TYPE              VARCHAR2(1),
    LOCATION              NUMBER(10)
)
/