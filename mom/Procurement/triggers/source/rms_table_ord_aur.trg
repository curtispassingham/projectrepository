PROMPT Creating Trigger 'RMS_TABLE_ORD_AUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_ORD_AUR
 AFTER UPDATE
 ON ORDLC
 FOR EACH ROW
DECLARE

   L_error_message      VARCHAR2(255)              := NULL;
   L_date               DATE                       := GET_VDATE;
BEGIN
   if UPDATING then
      if :new.lc_ind = 'Y' then
          if :new.transshipment_ind != :old.transshipment_ind then
            insert into ORD_LC_AMENDMENTS(order_no,
                                          item,
                                          change_type,
                                          new_value,
                                          change_date)
            values(:new.order_no,
                   NULL,
                   'TSF',
                   :new.transshipment_ind,
                   L_date);
         end if;
         ---
         if :new.partial_shipment_ind != :old.partial_shipment_ind then
            insert into ORD_LC_AMENDMENTS(order_no,
                                          item,
                                          change_type,
                                          new_value,
                                          change_date)
            values(:old.order_no,
                   NULL,
                   'PSF',
                   :new.partial_shipment_ind,
                   L_date);
         end if;
      end if;
   end if;
END;
/
