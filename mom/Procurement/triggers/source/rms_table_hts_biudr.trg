PROMPT Creating Trigger 'RMS_TABLE_HTS_BIUDR'
CREATE OR REPLACE TRIGGER RMS_TABLE_HTS_BIUDR
 BEFORE DELETE OR INSERT OR UPDATE
 ON HTS
 FOR EACH ROW
DECLARE

   L_category VARCHAR2(1);
   cursor C_CATEGORY is
      select 'x'
        from QUOTA_CATEGORY
       where quota_cat = :new.quota_cat
         and import_country_id = :new.import_country_id;
BEGIN
   if :new.quota_cat is not NULL then
      open C_CATEGORY;
      fetch C_CATEGORY into L_category;
      if C_CATEGORY%NOTFOUND then
         close C_CATEGORY;
         raise_application_error(-20502, 'Invalid Quota Category for this Import
 Country.');
      else
         close C_CATEGORY;
      end if;
   end if;
END;
/
