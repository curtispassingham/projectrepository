PROMPT Creating Trigger 'RMS_VIEW_DIL_UR'
CREATE OR REPLACE TRIGGER RMS_VIEW_DIL_UR 
   INSTEAD OF UPDATE ON deal_itemloc
   FOR EACH ROW
DECLARE
   L_error_message  VARCHAR2(1000) := NULL;
   L_table_name     VARCHAR2(1000) := NULL;
   
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   if :old.merch_level in(1,2,3) then
      L_table_name := 'DEAL_ITEMLOC_DIV_GRP';
      ---
      update deal_itemloc_div_grp dil
       set merch_level          = :new.merch_level,
           company_ind          = :new.company_ind,
           division             = :new.division,
           group_no             = :new.group_no,
           org_level            = :new.org_level,
           chain                = :new.chain,
           area                 = :new.area,
           region               = :new.region,
           district             = :new.district,
           location             = :new.location,
           origin_country_id    = :new.origin_country_id,
           loc_type             = :new.loc_type,
           excl_ind             = :new.excl_ind,
           last_update_id       = :new.last_update_id,
           last_update_datetime = :new.last_update_datetime
     where dil.deal_id          = :old.deal_id
       and dil.deal_detail_id   = :old.deal_detail_id
       and dil.seq_no           = :old.seq_no;

   elsif :old.merch_level in(4,5,6) then
      L_table_name := 'DEAL_ITEMLOC_DCS';
      ---
      update deal_itemloc_dcs dil
       set merch_level          = :new.merch_level,
           division             = :new.division,
           group_no             = :new.group_no,
           dept                 = :new.dept,
           class                = :new.class,
           subclass             = :new.subclass,
           org_level            = :new.org_level,
           chain                = :new.chain,
           area                 = :new.area,
           region               = :new.region,
           district             = :new.district,
           location             = :new.location,
           origin_country_id    = :new.origin_country_id,
           loc_type             = :new.loc_type,
           excl_ind             = :new.excl_ind,
           last_update_id       = :new.last_update_id,
           last_update_datetime = :new.last_update_datetime
     where dil.deal_id          = :old.deal_id
       and dil.deal_detail_id   = :old.deal_detail_id
       and dil.seq_no           = :old.seq_no;

   elsif :old.merch_level in(7,8,9,10,11) then
      L_table_name := 'DEAL_ITEMLOC_PARENT_DIFF';
      ---
      update deal_itemloc_parent_diff dil
       set merch_level          = :new.merch_level,
           division             = :new.division,
           group_no             = :new.group_no,
           dept                 = :new.dept,
           class                = :new.class,
           subclass             = :new.subclass,
           item_parent          = :new.item_parent,
           item_grandparent     = :new.item_grandparent,
           diff_1               = :new.diff_1,
           diff_2               = :new.diff_2,
           diff_3               = :new.diff_3,
           diff_4               = :new.diff_4,
           org_level            = :new.org_level,
           chain                = :new.chain,
           area                 = :new.area,
           region               = :new.region,
           district             = :new.district,
           location             = :new.location,
           origin_country_id    = :new.origin_country_id,
           loc_type             = :new.loc_type,
           excl_ind             = :new.excl_ind,
           last_update_id       = :new.last_update_id,
           last_update_datetime = :new.last_update_datetime
     where dil.deal_id          = :old.deal_id
       and dil.deal_detail_id   = :old.deal_detail_id
       and dil.seq_no           = :old.seq_no;

   elsif :old.merch_level = 12 then
      L_table_name := 'DEAL_ITEMLOC_ITEM';
      ---
      update deal_itemloc_item dil
       set merch_level          = :new.merch_level,
           division             = :new.division,
           group_no             = :new.group_no,
           dept                 = :new.dept,
           class                = :new.class,
           subclass             = :new.subclass,
           item_parent          = :new.item_parent,
           item_grandparent     = :new.item_grandparent,
           diff_1               = :new.diff_1,
           diff_2               = :new.diff_2,
           diff_3               = :new.diff_3,
           diff_4               = :new.diff_4,
           org_level            = :new.org_level,
           chain                = :new.chain,
           area                 = :new.area,
           region               = :new.region,
           district             = :new.district,
           location             = :new.location,
           origin_country_id    = :new.origin_country_id,
           loc_type             = :new.loc_type,
           item                 = :new.item,
           excl_ind             = :new.excl_ind,
           last_update_id       = :new.last_update_id,
           last_update_datetime = :new.last_update_datetime
     where dil.deal_id          = :old.deal_id
       and dil.deal_detail_id   = :old.deal_detail_id
       and dil.seq_no           = :old.seq_no;
   end if;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table_name,
                                            NULL,
                                            NULL);
     RAISE_APPLICATION_ERROR(-20001, L_error_message);
   WHEN OTHERS THEN
     L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'RMS_VIEW_DIL_UR',
                                           to_char(SQLCODE));
     RAISE_APPLICATION_ERROR(-20001, L_error_message);
END RMS_VIEW_DIL_UR;
/
