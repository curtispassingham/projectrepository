PROMPT Creating Trigger 'RMS_COL_OHE_STATUS_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_OHE_STATUS_AUR
 AFTER UPDATE OF STATUS
 ON ORDHEAD
 FOR EACH ROW
DECLARE

   L_error_message   VARCHAR2(255);
   L_order_no        ordhead.order_no%TYPE;
   L_status          ordhead.status%TYPE;
   L_contract_no     ordhead.contract_no%TYPE;
BEGIN

   L_order_no       := :old.order_no;
   L_status         := :new.status;
   L_contract_no    := :new.contract_no;

   if L_status = 'A' and L_contract_no IS NULL then
      ---
      if not DEAL_SQL.INSERT_DEAL_CALC_QUEUE(L_error_message,
                                             L_order_no,
                                             'N',
                                             'N',
                                             'Y') then
         RAISE_APPLICATION_ERROR(-2000,'INTERNAL ERROR: TR_ORDHEAD -
DEAL_SQL.INSERT_DEAL_CALC_QUEUE failed.');
      end if;
      ---
   end if;
END;
/
