PROMPT Creating Trigger 'RMS_TABLE_OLO_AIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_OLO_AIUR
 AFTER INSERT OR UPDATE
 ON ORDLOC
 FOR EACH ROW
DECLARE

   L_error_message      VARCHAR2(255)              := NULL;
   L_attached           BOOLEAN                    := NULL;
   L_date               DATE                       := GET_VDATE;
BEGIN

   if LC_SQL.ATTACHED_LC(L_error_message,
                         L_attached,
                         :old.order_no) = FALSE then
      RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_ORDLOC - LC_SQL.ATTACHED_LC failed.');
   end if;
   ---
   if L_attached = TRUE then
      if :new.qty_ordered != NVL(:old.qty_ordered, -1) then
         insert into ORD_LC_AMENDMENTS(order_no,
                                       item,
                                       change_type,
                                       new_value,
                                       change_date)
         values(:old.order_no,
                :old.item,
                'OIQ',
                NULL,
                L_date);
      end if;
      ---
      if UPDATING then
         if :new.unit_cost != :old.unit_cost then
            insert into ORD_LC_AMENDMENTS(order_no,
                                          item,
                                          change_type,
                                          new_value,
                                          change_date)
                                   values(:new.order_no,
                                          :new.item,
                                          'OIC',
                                          :new.unit_cost,
                                          L_date);

         end if;
      end if;
   end if;
END;
/
