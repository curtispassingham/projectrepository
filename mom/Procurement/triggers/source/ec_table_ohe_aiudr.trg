PROMPT Creating Trigger 'EC_TABLE_OHE_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_OHE_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF FOB_TITLE_PASS_DESC
, FOB_TITLE_PASS
, PAYMENT_METHOD
, SUPPLIER
, DEPT
, BUYER
, PURCHASE_TYPE
, NOT_AFTER_DATE
, CONTRACT_NO
, FACTORY
, FOB_TRANS_RES
, NOT_BEFORE_DATE
, PICKUP_NO
, EXCHANGE_RATE
, APP_DATETIME
, PICKUP_DATE
, CURRENCY_CODE
, SHIP_PAY_METHOD
, SHIP_METHOD
, TERMS
, LADING_PORT
, DISCHARGE_PORT
, CLOSE_DATE
, QC_IND
, PRE_MARK_IND
, COMMENT_DESC
, BACKHAUL_ALLOWANCE
, OTB_EOW_DATE
, FREIGHT_CONTRACT_NO
, LATEST_SHIP_DATE
, STATUS
, PICKUP_LOC
, BACKHAUL_TYPE
, AGENT
, FREIGHT_TERMS
, PO_TYPE
, ORDER_TYPE
, EARLIEST_SHIP_DATE
, VENDOR_ORDER_NO
, FOB_TRANS_RES_DESC
, PROMOTION
 ON ORDHEAD
 FOR EACH ROW
DECLARE


   L_order_no               ORDHEAD.ORDER_NO%TYPE := NULL;
   L_order_type             ORDHEAD.ORDER_TYPE%TYPE := NULL;
   L_order_header_status    ORDHEAD.STATUS%TYPE := NULL;
   L_supplier               ORDHEAD.SUPPLIER%TYPE := NULL;

   L_message_type            ORDER_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_error_message           VARCHAR2(255) := NULL;

   L_rib_poref_rec    "RIB_PORef_REC" := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN

   if DELETING then

      L_message_type := RMSMFM_ORDER.HDR_DEL;
      L_order_no := :old.order_no;

   else

      L_order_no             := :new.order_no;
      L_order_header_status  := :new.status;
      L_supplier             := :new.supplier;
      L_order_type           := :new.order_type;

      if INSERTING then
         L_message_type := RMSMFM_ORDER.HDR_ADD;
      else
         L_message_type := RMSMFM_ORDER.HDR_UPD;
      end if;

   end if;

   if RMSMFM_ORDER.ADDTOQ(L_error_message,
                          L_message_type,
                          L_order_no,
                          L_order_type,
                          L_order_header_status,
                          L_supplier,
                          NULL,                   -- item
                          NULL,                   -- location
                          NULL,                   -- loc_type
                          NULL) = FALSE then      -- physical_location
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_OHE_AIUDR',
                                               NULL);
      end if;

      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END;
/
