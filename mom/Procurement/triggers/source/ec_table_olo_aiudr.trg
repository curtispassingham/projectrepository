PROMPT Creating Trigger 'EC_TABLE_OLO_AIUDR'
CREATE OR REPLACE TRIGGER EC_TABLE_OLO_AIUDR
 AFTER DELETE OR INSERT OR UPDATE OF UNIT_COST
, ESTIMATED_INSTOCK_DATE
, QTY_ORDERED
 ON ORDLOC
 FOR EACH ROW
DECLARE


   L_order_no            ORDHEAD.ORDER_NO%TYPE := NULL;

   L_item                   ORDLOC.ITEM%TYPE := NULL;
   L_location              ORDLOC.LOCATION%TYPE := NULL;
   L_loc_type             ORDLOC.LOC_TYPE%TYPE := NULL;
   L_physical_location ORDLOC.LOCATION%TYPE := NULL;

   L_message_type    ORDER_MFQUEUE.MESSAGE_TYPE%TYPE := NULL;
   L_error_message   VARCHAR2(255) := NULL;

   PROGRAM_ERROR   EXCEPTION;
BEGIN


   if DELETING then

      L_order_no   := :old.order_no;
      L_item          := :old.item;
      L_location     := :old.location;
      L_loc_type    := :old.loc_type;

      L_message_type  := RMSMFM_ORDER.DTL_DEL;

   else

      L_order_no   := :new.order_no;
      L_item          := :new.item;
      L_location     := :new.location;
      L_loc_type    := :new.loc_type;

      if INSERTING then

         L_message_type := RMSMFM_ORDER.DTL_ADD;

      else  --UPDATING

	     if (:old.qty_ordered) != (:new.qty_ordered) OR
            (:old.unit_cost != :new.unit_cost) OR
            (:old.estimated_instock_date != :new.estimated_instock_date) then 

            L_message_type := RMSMFM_ORDER.DTL_UPD;
         else
            return;
         end if;
       end if;

   end if;

   if L_loc_type = 'W' then
      if not WH_ATTRIB_SQL.GET_PWH_FOR_VWH(L_error_message,
                                                                       L_physical_location,
                                                                       L_location) then
         raise PROGRAM_ERROR;
      end if;
   else
      L_physical_location := L_location;
   end if;

   if RMSMFM_ORDER.ADDTOQ(L_error_message,
                                             L_message_type,
                                             L_order_no,
                                             NULL,
                                             NULL,                   -- order_header_status
                                             NULL,                   -- supplier
                                             L_item,
                                             L_location,
                                             L_loc_type,
                                             L_physical_location) = FALSE then
      raise PROGRAM_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      if L_error_message is NULL then
         L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                               SQLERRM,
                                               'EC_TABLE_OLO_AIUDR',
                                               NULL);
      end if;
      
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);
END;
/
