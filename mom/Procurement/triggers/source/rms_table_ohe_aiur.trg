PROMPT Creating Trigger 'RMS_TABLE_OHE_AIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_OHE_AIUR
 AFTER INSERT OR UPDATE
 ON ORDHEAD
 FOR EACH ROW
DECLARE
                                                                         
                                                                                
   L_error_message      VARCHAR2(255)               := NULL;                    
   L_attached           BOOLEAN                     := NULL;                    
   L_date               DATE                        := GET_VDATE;               
   L_exists             VARCHAR2(1)                 := NULL;                    
   L_order_no           ORDHEAD.ORDER_NO%TYPE;                                  
   ---                                                                          
   cursor C_EXISTS is                                                           
      select 'x'                                                                
        from rev_orders                                                         
       where order_no = L_order_no;
BEGIN                                                                           
   L_order_no      := :new.order_no;                                            
   ---                                                                          
   open C_EXISTS;                                                               
   fetch C_EXISTS into L_exists;                                                
   close C_EXISTS;                                                              
   ---                                                                          
   if L_exists is NULL then                                                     
                                                                                
      if ((INSERTING and :new.status = 'A')                                     
          or                                                                    
          (:new.status not in ('W', 'S')                                        
           and ((:old.status <> :new.status)                                    
                or (:old.not_before_date <> :new.not_before_date)               
                or (:old.not_after_date <> :new.not_after_date)                 
                or (:old.pickup_date <> :new.pickup_date)                       
                or (:old.pickup_date is NULL and :new.pickup_date is not NULL)  
                or (:old.pickup_date is not NULL and :new.pickup_date is NULL)  
                or (:old.freight_terms <> :new.freight_terms)                   
                or (:old.freight_terms is NULL and :new.freight_terms is not NULL)                                                                              
                or (:old.freight_terms is not NULL and :new.freight_terms is NULL)                                                                              
                or (:old.buyer <> :new.buyer)                                   
                or (:old.buyer is NULL and :new.buyer is not NULL)              
                or (:old.buyer is not NULL and :new.buyer is NULL)))) then      
                                                                                
          insert into rev_orders(order_no)                                      
                          values(L_order_no);                                   
                                                                                
      end if;                                                                   
   end if; /* end if L_exists */                                                
---                                                                             
   if UPDATING then                                                             
      if LC_SQL.ATTACHED_LC(L_error_message,                                    
                            L_attached,                                         
                            :new.order_no) = FALSE then                         
         RAISE_APPLICATION_ERROR(-20000,'INTERNAL ERROR: TRG_ORDHEAD - LC_SQL.AT
TACHED_LC failed.');                                                            
      end if;                                                                   
      ---                                                                       
      if L_attached = TRUE then                                                 
         if :new.earliest_ship_date != :old.earliest_ship_date then             
            insert into ORD_LC_AMENDMENTS(order_no,                             
                                          item,                                 
                                          change_type,                          
                                          new_value,                            
                                          change_date)                          
            values(:old.order_no,                                               
                   NULL,                                                        
                   'ESD',                                                       
                   :new.earliest_ship_date,                                     
                   L_date);                                                     
         end if;                                                                
         ---                                                                    
         if :new.latest_ship_date != :old.latest_ship_date then                 
            insert into ORD_LC_AMENDMENTS(order_no,                             
                                          item,                                 
                                          change_type,                          
                                          new_value,                            
                                          change_date)                          
            values(:old.order_no,                                               
                   NULL,                                                        
                   'LSD',                                                       
                   :new.latest_ship_date,                                       
                   L_date);                                                     
         end if;                                                                
      end if;                                                                   
   end if;                                                                      
                                                                                
END;
/
