PROMPT Creating Trigger 'RMS_TABLE_OHE_BIR'
CREATE OR REPLACE TRIGGER RMS_TABLE_OHE_BIR
 BEFORE INSERT
 ON ORDHEAD
 FOR EACH ROW
BEGIN
   insert into ordhead_lock values (:new.order_no);
EXCEPTION
   when DUP_VAL_ON_INDEX then
      NULL;
END;
/
