PROMPT Creating Trigger 'RMS_COL_OLO_QTYORDERED_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_OLO_QTYORDERED_AUR
 AFTER UPDATE OF QTY_ORDERED, UNIT_COST
 ON ORDLOC
 FOR EACH ROW
DECLARE

   L_order_no      ORDHEAD.ORDER_NO%TYPE;
   L_status        ORDHEAD.STATUS%TYPE;
   L_error_message RTK_ERRORS.RTK_TEXT%TYPE;
   ---
   cursor C_CHECK_ORDER is
      select status
        from ordhead
       where order_no = L_order_no;
   ---
BEGIN
   L_order_no      := :new.order_no;

      open C_CHECK_ORDER;
      fetch C_CHECK_ORDER into L_status;
      close C_CHECK_ORDER;
      if L_status not in ('W', 'S') and ((:new.qty_ordered != :old.qty_ordered) or (:new.unit_cost != :old.unit_cost)) then
         insert into rev_orders(order_no)
                        select L_order_no
                          from dual
                         where not exists (select order_no
                                             from rev_orders
                                            where order_no = L_order_no);
      end if;
	  ---
EXCEPTION
   when DUP_VAL_ON_INDEX then
      NULL;
--
WHEN OTHERS then
      API_LIBRARY.HANDLE_ERRORS(L_status,
                                L_error_message,
                                API_LIBRARY.FATAL_ERROR,
                                'RMS_COL_OLO_QTYORDERED_AUR');
      raise_application_error(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

	  
END;
/