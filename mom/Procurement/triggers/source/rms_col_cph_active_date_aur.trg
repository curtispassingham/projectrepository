PROMPT Creating Trigger 'RMS_COL_CPH_ACTIVE_DATE_AUR'
CREATE OR REPLACE TRIGGER RMS_COL_CPH_ACTIVE_DATE_AUR
 AFTER UPDATE OF STATUS
 ON COST_SUSP_SUP_HEAD
 FOR EACH ROW
DECLARE



   L_new_cost_change   COST_SUSP_SUP_HEAD.COST_CHANGE%TYPE;
   L_new_active_date   COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE;
   L_old_active_date   COST_SUSP_SUP_HEAD.ACTIVE_DATE%TYPE;


BEGIN

   L_new_active_date := :NEW.active_date;
   L_old_active_date := :OLD.active_date;
   L_new_cost_change := :NEW.cost_change;

   if L_old_active_date !=  L_new_active_date then

      insert into cost_change_active_date_temp(cost_change,
                                               item,
                                               supplier,
                                               origin_country_id,
                                               loc,
                                               loc_type,
                                               old_active_date)
                                        select L_new_cost_change,
                                               cd.item,
                                               cd.supplier,
                                               cd.origin_country_id,
                                               NULL,
                                               NULL,
                                               L_old_active_date
                                          from cost_susp_sup_detail cd,
                                               sups s,
                                               future_cost fc
                                         where cd.cost_change = L_new_cost_change
                                           and s.supplier = cd.supplier
                                           and cd.default_bracket_ind =
                                                  DECODE(s.bracket_costing_ind, 'Y', 'Y', cd.default_bracket_ind)
                                           and fc.item = cd.item
                                           and fc.supplier = cd.supplier
                                           and fc.origin_country_id = cd.origin_country_id
                                           and fc.active_date = L_old_active_date
                                           and NOT EXISTS (select 'x'
                                                             from cost_change_active_date_temp ccad
                                                            where ccad.cost_change = L_new_cost_change
                                                              and ccad.item = cd.item
                                                              and ccad.supplier = cd.supplier
                                                              and ccad.origin_country_id = cd.origin_country_id)
                                         union
                                        select L_new_cost_change,
                                               cdl.item,
                                               cdl.supplier,
                                               cdl.origin_country_id,
                                               cdl.loc,
                                               cdl.loc_type,
                                               L_new_active_date
                                          from cost_susp_sup_detail_loc cdl,
                                               sups s,
                                               future_cost fc
                                         where cdl.cost_change = L_new_cost_change
                                           and s.supplier = cdl.supplier
                                           and fc.item = cdl.item
                                           and fc.supplier = cdl.supplier
                                           and fc.origin_country_id = cdl.origin_country_id
                                           and fc.location = cdl.loc
                                           and fc.loc_type = cdl.loc_type
                                           and fc.active_date = L_old_active_date
                                           and cdl.default_bracket_ind =
                                                  DECODE(s.bracket_costing_ind, 'Y', 'Y', cdl.default_bracket_ind)
                                           and NOT EXISTS (select 'x'
                                                             from cost_change_active_date_temp ccad
                                                            where ccad.cost_change = L_new_cost_change
                                                              and ccad.item = cdl.item
                                                              and ccad.supplier = cdl.supplier
                                                              and ccad.origin_country_id = cdl.origin_country_id
                                                              and ccad.loc = cdl.loc
                                                              and ccad.loc_type = cdl.loc_type)
                                           and NOT EXISTS (select 'x'
                                                             from cost_susp_sup_detail  cd
                                                            where cd.cost_change = L_new_cost_change
                                                              and cd.item = cdl.item
                                                              and cd.supplier = cdl.supplier
                                                              and cd.origin_country_id = cdl.origin_country_id);

   end if;

END RMS_COL_CPH_ACTIVE_DATE_AUR;
/
