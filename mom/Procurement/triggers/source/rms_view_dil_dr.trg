PROMPT Creating Trigger 'RMS_VIEW_DIL_DR'
CREATE OR REPLACE TRIGGER RMS_VIEW_DIL_DR 
   INSTEAD OF DELETE ON deal_itemloc
   FOR EACH ROW
DECLARE
   L_error_message  VARCHAR2(1000) := NULL;
   L_table_name     VARCHAR2(1000) := NULL;
   
   RECORD_LOCKED     EXCEPTION;
   PRAGMA            EXCEPTION_INIT(Record_Locked, -54);

BEGIN

   if :old.merch_level in(1,2,3) then
      L_table_name := 'DEAL_ITEMLOC_DIV_GRP';
      ---
      delete from deal_itemloc_div_grp dil
       where dil.deal_id        = :old.deal_id
         and dil.deal_detail_id = :old.deal_detail_id
         and dil.seq_no         = :old.seq_no;

   elsif :old.merch_level in(4,5,6) then
      L_table_name := 'DEAL_ITEMLOC_DCS';
      ---
      delete from deal_itemloc_dcs dil
       where dil.deal_id        = :old.deal_id
         and dil.deal_detail_id = :old.deal_detail_id
         and dil.seq_no         = :old.seq_no;

   elsif :old.merch_level in(7,8,9,10,11) then
      L_table_name := 'DEAL_ITEMLOC_PARENT_DIFF';
      ---
      delete from deal_itemloc_parent_diff dil
       where dil.deal_id        = :old.deal_id
         and dil.deal_detail_id = :old.deal_detail_id
         and dil.seq_no         = :old.seq_no;

   elsif :old.merch_level = 12 then
      L_table_name := 'DEAL_ITEMLOC_ITEM';
      ---
      delete from deal_itemloc_item dil
       where dil.deal_id        = :old.deal_id
         and dil.deal_detail_id = :old.deal_detail_id
         and dil.seq_no         = :old.seq_no;

   end if;

EXCEPTION
   WHEN RECORD_LOCKED THEN
      L_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table_name,
                                            NULL,
                                            NULL);
     RAISE_APPLICATION_ERROR(-20001, L_error_message);
   WHEN OTHERS THEN
     L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                           SQLERRM,
                                           'RMS_VIEW_DIL_DR',
                                           to_char(SQLCODE));
     RAISE_APPLICATION_ERROR(-20001, L_error_message);

END RMS_VIEW_DIL_DR;
/
