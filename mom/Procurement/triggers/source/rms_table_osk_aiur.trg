PROMPT Creating Trigger 'RMS_TABLE_OSK_AIUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_OSK_AIUR
 AFTER INSERT OR UPDATE
 ON ORDSKU
 FOR EACH ROW
DECLARE

   L_error_message      VARCHAR2(255)              := NULL;
   L_attached           BOOLEAN                    := NULL;
   L_sysdate            DATE;
   L_form_type          VARCHAR2(6)                := NULL;
   ---
   cursor C_GET_SYSDATE is
      select sysdate
        from dual;

   cursor C_LC_FORM_TYPE is
      select form_type
        from lc_head
       where lc_ref_id = (select distinct lc_ref_id
                            from lc_detail
                           where order_no = :new.order_no);
BEGIN
   if LC_SQL.ATTACHED_LC(L_error_message,
                         L_attached,
                         :new.order_no) = FALSE then
      RAISE_APPLICATION_ERROR(-20000,
         'INTERNAL ERROR: TRG_ORDHEAD - LC_SQL.ATTACHED_LC failed.');
   
end if;
   ---
   if L_attached = TRUE then
      open  C_GET_SYSDATE;
      fetch C_GET_SYSDATE into L_sysdate;
      close C_GET_SYSDATE;
      ---
      open  C_LC_FORM_TYPE;
      fetch C_LC_FORM_TYPE into L_form_type;
      close C_LC_FORM_TYPE;
      ---
      if INSERTING then
         insert into ORD_LC_AMENDMENTS(order_no,
                                       item,
                                       change_type,
                                       new_value,
                                       change_date)
                                values(:new.order_no,
                                       :new.item,
                                       'OIA',
                                       NULL,
                                       L_sysdate);
      ---
      elsif UPDATING then
         if L_form_type = 'L' then
            if :new.earliest_ship_date != :old.earliest_ship_date then
               insert into ORD_LC_AMENDMENTS(order_no,
                                             item,
                                             change_type,
                                             new_value,
                                             change_date)
                                      values(:old.order_no,
                                             :old.item,
                                             'ESD',
                                             :new.earliest_ship_date,
                                             L_sysdate);
            end if;
            ---
            if :new.latest_ship_date != :old.latest_ship_date then
               insert into ORD_LC_AMENDMENTS(order_no,
                                             item,
                                             change_type,
                                             new_value,
                                             change_date)
                                      values(:old.order_no,
                                             :old.item,
                                             'LSD',
                                             :new.latest_ship_date,
                                             L_sysdate);
            end if;
         end if; -- L_form_type
      end if;  -- INSERTING/UPDATING
   end if; -- L_attached
END;
/
