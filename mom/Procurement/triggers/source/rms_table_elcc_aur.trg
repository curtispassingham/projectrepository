PROMPT Creating Trigger 'RMS_TABLE_ELCC_AUR'
CREATE OR REPLACE TRIGGER RMS_TABLE_ELCC_AUR
 AFTER UPDATE OF COMP_DESC, CALC_BASIS, COMP_RATE,
    PER_COUNT, PER_COUNT_UOM, COMP_CURRENCY
 ON ELC_COMP
 FOR EACH ROW
BEGIN
   if :old.up_chrg_group = 'W' then 
      ---
      update wf_cost_buildup_tmpl_detail
         set description   = :new.comp_desc,
             calc_basis    = :new.calc_basis,
             comp_rate     = :new.comp_rate,
             per_count     = :new.per_count,
             per_count_uom = :new.per_count_uom,
             comp_currency = :new.comp_currency
       where cost_comp_id  = :new.comp_id;
      ---
   end if; 
END;
/