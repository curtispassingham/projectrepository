DROP TYPE OBJ_RPM_CC_PROMO_TBL FORCE
/
DROP TYPE OBJ_RPM_CC_PROMO_REC FORCE
/
CREATE OR REPLACE
TYPE OBJ_RPM_CC_PROMO_REC AS OBJECT
(
   promo_id                   NUMBER(10),
   promo_display_id           VARCHAR2(10),
   rp_secondary_ind           NUMBER(1),
   promo_comp_id              NUMBER(10),
   comp_display_id            VARCHAR2(10),
   rpc_secondary_ind          NUMBER(1),
   promo_comp_detail_id       NUMBER(10),
   rpcd_start_date            DATE,
   rpcd_end_date              DATE,
   old_rpcd_start_date        DATE,
   old_rpcd_end_date          DATE,
   apply_to_code              NUMBER(1),
   timebased_dtl_ind          NUMBER(1),
   zone_ids                   OBJ_NUMERIC_ID_TABLE,
   change_type                NUMBER(1),
   change_amount              NUMBER(20,4),
   change_currency            VARCHAR2(3),
   change_percent             NUMBER(20,4),
   change_selling_uom         VARCHAR2(4),
   price_guide_id             NUMBER(20),
   exception_parent_id        NUMBER(10),
   promotion_type             NUMBER(1),
   exclusion                  NUMBER(1),
   customer_type              NUMBER(25)
);
/

CREATE TYPE OBJ_RPM_CC_PROMO_TBL as TABLE of OBJ_RPM_CC_PROMO_REC;
/
