DROP TYPE OBJ_RPM_PROM_COMP_AGG_TBL FORCE
/
DROP TYPE OBJ_RPM_PROM_COMP_AGG_REC FORCE
/

CREATE TYPE OBJ_RPM_PROM_COMP_AGG_REC AS OBJECT
(
   promo_comp_id                  NUMBER(10),
   type                           VARCHAR2(2),
   pcpd_ind                       VARCHAR2(1),
   full_access_ind                VARCHAR2(1),
   detail_states                  OBJ_VARCHAR_DESC_TABLE,
   conflict_ind                   VARCHAR2(1),
   timebased_dtl_ind              VARCHAR2(1)
)
/

CREATE TYPE OBJ_RPM_PROM_COMP_AGG_TBL IS TABLE OF OBJ_RPM_PROM_COMP_AGG_REC;
/
