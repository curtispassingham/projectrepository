DROP TYPE OBJ_DEPT_CLASS_SUBCLASS_TBL FORCE
/
DROP TYPE OBJ_DEPT_CLASS_SUBCLASS_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_DEPT_CLASS_SUBCLASS_REC AS OBJECT
   (dept     NUMBER(4),
    class    NUMBER(4),
    subclass NUMBER(4))
/
CREATE OR REPLACE TYPE OBJ_DEPT_CLASS_SUBCLASS_TBL AS TABLE OF OBJ_DEPT_CLASS_SUBCLASS_REC
/

-----------------------------------

DROP TYPE  OBJ_NUMERIC_ID_TABLE FORCE
/

CREATE OR REPLACE TYPE OBJ_NUMERIC_ID_TABLE IS TABLE OF NUMBER(30)
/

-----------------------------------

DROP TYPE OBJ_VARCHAR_ID_TABLE FORCE
/

CREATE OR REPLACE TYPE OBJ_VARCHAR_ID_TABLE IS TABLE OF VARCHAR(30)
/

-----------------------------------

DROP TYPE OBJ_VARCHAR_DESC_TABLE FORCE
/

CREATE OR REPLACE TYPE OBJ_VARCHAR_DESC_TABLE IS TABLE OF VARCHAR(500)
/

-----------------------------------

DROP TYPE OBJ_PAGE_SORT_FILTER_TBL FORCE
/
DROP TYPE OBJ_PAGE_SORT_FILTER_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PAGE_SORT_FILTER_REC AS OBJECT
   (page_size      NUMBER(10),
    page_number    NUMBER(15),
    sort_clause    VARCHAR2(10000),
    filter_columns OBJ_VARCHAR_DESC_TABLE)
/
CREATE TYPE OBJ_PAGE_SORT_FILTER_TBL AS TABLE OF OBJ_PAGE_SORT_FILTER_REC
/

-----------------------------------

DROP TYPE OBJ_PC_DTL_TBL FORCE
/
DROP TYPE OBJ_PC_DTL_REC FORCE
/

CREATE OR REPLACE TYPE  OBJ_PC_DTL_REC AS OBJECT
   (rpm_price_workspace_detail_id NUMBER(15),
    rpm_price_workspace_id        NUMBER(15),
    item                          VARCHAR2(25),
    skulist                       NUMBER(15),
    zone_node_type                NUMBER(6),
    location                      NUMBER(10),
    action_date                   DATE,
    selling_retail                NUMBER(20,4),
    selling_retail_currency       VARCHAR2(3),
    selling_retail_less_vat       NUMBER(20,4),
    selling_uom                   VARCHAR2(4),
    multi_units                   NUMBER(12,4),
    multi_unit_retail             NUMBER(20,4),
    multi_unit_retail_currency    VARCHAR2(3),
    multi_selling_uom             VARCHAR2(4),
    clear_retail                  NUMBER(20,4),
    clear_retail_currency         VARCHAR2(3),
    clear_uom                     VARCHAR2(4),
    clearance_id                  NUMBER(15),
    pricing_cost                  NUMBER(20,4), -- Cost for markup calculations
    pricing_cost_currency         VARCHAR2(3))  -- Currency of the cost
/
CREATE OR REPLACE TYPE OBJ_PC_DTL_TBL AS TABLE OF OBJ_PC_DTL_REC
/

-----------------------------------

DROP TYPE OBJ_CUST_ATTR_TBL FORCE
/
DROP TYPE OBJ_CUST_ATTR_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_CUST_ATTR_REC AS OBJECT
   (cust_attr_id     NUMBER(15),
    price_event_id   NUMBER(15),
    price_event_type VARCHAR2(3),
    varchar2_1       VARCHAR2(250),
    varchar2_2       VARCHAR2(250),
    varchar2_3       VARCHAR2(250),
    varchar2_4       VARCHAR2(250),
    varchar2_5       VARCHAR2(250),
    varchar2_6       VARCHAR2(250),
    varchar2_7       VARCHAR2(250),
    varchar2_8       VARCHAR2(250),
    varchar2_9       VARCHAR2(250),
    varchar2_10      VARCHAR2(250),
    number_11        NUMBER,
    number_12        NUMBER,
    number_13        NUMBER,
    number_14        NUMBER,
    number_15        NUMBER,
    number_16        NUMBER,
    number_17        NUMBER,
    number_18        NUMBER,
    number_19        NUMBER,
    number_20        NUMBER,
    date_21          DATE,
    date_22          DATE)
/
CREATE OR REPLACE TYPE OBJ_CUST_ATTR_TBL AS TABLE OF OBJ_CUST_ATTR_REC
/

--------------------------------
DROP TYPE OBJ_PC_TBL FORCE
/
DROP TYPE OBJ_PC_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PC_REC AS OBJECT
   (rpm_price_workspace_id         NUMBER(15),
    ui_workflow_id                 VARCHAR2(100),
    price_change_id                NUMBER(15),
    price_change_display_id        VARCHAR2(15),
    display_ind                    NUMBER(1),
    clearance_ind                  NUMBER(1),
    state                          VARCHAR2(50),
    reason_code_id                 NUMBER(6),
    reason_code                    VARCHAR2(40),
    reason_code_desc               VARCHAR2(160),
    parent_exception_id            NUMBER(15),         --To hold the parents ID
    parent_display_id              VARCHAR2(15),       --To hold the parents Display ID
    exception_level                NUMBER(1),          --To hold, IS, HAS, ISandHAS
    new_modified_ind               NUMBER(1),          --To tell whether this record is new or modified
    item                           VARCHAR2(25),
    item_description               VARCHAR2(250),
    item_level                     NUMBER(1),
    item_standard_uom              VARCHAR2(4) ,
    diff_id                        VARCHAR2(10),
    diff_id_description            VARCHAR2(120),
    dept                           NUMBER(10),
    class                          NUMBER(10),
    subclass                       NUMBER(10),
    skulist                        NUMBER(15),
    skulist_desc                   VARCHAR2(120),
    zone_id                        NUMBER(10),
    zone_display_id                NUMBER(10),
    location                       NUMBER(10),
    location_description           VARCHAR2(150),
    zone_node_type                 NUMBER(6),
    link_code_id                   NUMBER(6),
    link_code                      VARCHAR2(10),
    link_code_desc                 VARCHAR2(40),
    effective_date                 DATE,
    currency_code                  VARCHAR2(3),
    out_of_stock_date              DATE,
    earliest_out_of_stock_date     DATE,
    latest_out_of_stock_date       DATE,
    reset_date                     DATE,
    earliest_reset_date            DATE,
    latest_reset_date              DATE,
    change_type                    NUMBER(1),
    change_amount                  NUMBER(20,4),
    change_currency                VARCHAR2(3),
    change_percent                 NUMBER(20,4),
    change_selling_uom             VARCHAR2(4),
    null_multi_ind                 NUMBER(1),
    multi_units                    NUMBER(12,4),
    multi_unit_retail              NUMBER(20,4),
    multi_unit_retail_currency     VARCHAR2(3),
    multi_selling_uom              VARCHAR2(4),
    price_guide_id                 NUMBER(20),
    price_guide_desc               VARCHAR2(1000),
    vendor_funded_ind              VARCHAR2(1),
    funding_type                   NUMBER(1),
    funding_amount                 NUMBER(20,4),
    funding_amount_currency        VARCHAR2(3),
    funding_percent                NUMBER(20,4),
    deal_id                        NUMBER(10),
    deal_detail_id                 NUMBER(10),
    partner_id                     VARCHAR2(10),
    partner_name                   VARCHAR2(240),
    partner_type                   VARCHAR2(6),
    create_date                    DATE,
    create_id                      VARCHAR2(30),
    approval_date                  DATE,
    approval_id                    VARCHAR2(30),
    primary_zone                   NUMBER(1),
    price_change_percent           NUMBER(20,4),
    -- Current retails
    current_unit_retail            NUMBER(20,4),
    current_average_retail         NUMBER(20,4),
    current_average_retail_cnst    NUMBER(1),
    current_uom                    VARCHAR2(4),
    current_multi_units            NUMBER(12,4),
    current_multi_units_average    NUMBER(20,4),
    current_multi_units_avg_cnst   NUMBER(1),
    current_multi_units_retail     NUMBER(20,4),
    current_multi_units_retail_avg NUMBER(20,4),
    current_mu_rtl_avg_cnst        NUMBER(1),
    current_multi_units_uom        VARCHAR2(4),
    current_cost                   NUMBER(20,4),
    current_cost_cnst              NUMBER(1),
    current_markup_percent         NUMBER(12,4),
    current_clearance_retail       NUMBER(20,4),
    current_clearance_retail_avg   NUMBER(20,4),
    -- Basis retails
    basis_unit_retail              NUMBER(20,4),
    basis_average_retail           NUMBER(20,4),
    basis_average_retail_cnst      NUMBER(1),
    basis_uom                      VARCHAR2(4),
    basis_multi_units              NUMBER(12,4),
    basis_multi_units_average      NUMBER(20,4),
    basis_multi_units_avg_cnst     NUMBER(1),
    basis_multi_units_retail       NUMBER(20,4),
    basis_multi_units_retail_avg   NUMBER(20,4),
    basis_mu_rtl_avg_cnst          NUMBER(1),
    basis_multi_units_uom          VARCHAR2(4),
    basis_clearance_retail         NUMBER(20,4),
    basis_clearance_retail_average NUMBER(20,4),
    -- New retails
    new_unit_retail                NUMBER(20,4),
    new_average_retail             NUMBER(20,4),
    new_average_retail_cnst        NUMBER(1),
    new_uom                        VARCHAR2(4),
    new_multi_units                NUMBER(12,4),
    new_multi_units_retail         NUMBER(20,4),
    new_multi_units_uom            VARCHAR2(4),
    new_cost                       NUMBER(20,4),
    new_cost_cnst                  NUMBER(1),
    new_markup_percent             NUMBER(12,4),
    -- Asynchronous Error Flag
    asyn_error_exist               NUMBER(1),
    -- Promotion Constraints
    ignore_constraints             NUMBER(1),
    pc_details                     OBJ_PC_DTL_TBL,
    markup_calculated              NUMBER(1),
    -- Stock
    store_on_hand                  NUMBER(20,4),
    store_on_order                 NUMBER(20,4),
    store_total_inv                NUMBER(20,4),
    wh_on_hand                     NUMBER(20,4),
    wh_on_order                    NUMBER(20,4),
    wh_total_inv                   NUMBER(20,4),
    store_in_transit               NUMBER(20,4),
    store_pack_comp_intran         NUMBER(20,4),
    wh_in_transit                  NUMBER(20,4),
    wh_pack_comp_intran            NUMBER(20,4),
    markup_change_date             DATE,
    sys_generated_exclusion        NUMBER(1),
    price_event_itemlist           NUMBER(15),
    price_event_itemlist_desc      VARCHAR2(120),
    total_inventory                NUMBER(20,4),
    total_on_hand                  NUMBER(20,4),
    total_on_order                 NUMBER(20,4),
    cust_attr_id                   NUMBER(15),
    cust_attributes                OBJ_CUST_ATTR_TBL)
/
CREATE OR REPLACE TYPE  OBJ_PC_TBL AS TABLE OF OBJ_PC_REC
/

-----------------------------------

DROP TYPE OBJ_PRICE_CHANGE_SEARCH_TBL FORCE
/
DROP TYPE OBJ_PRICE_CHANGE_SEARCH_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PRICE_CHANGE_SEARCH_REC AS OBJECT
   (price_change_ids               OBJ_NUMERIC_ID_TABLE,
    price_change_type              NUMBER(1),
    --SECURITY
    security_dept_class_subclasses OBJ_DEPT_CLASS_SUBCLASS_TBL,
    security_zone_groups           OBJ_NUMERIC_ID_TABLE,
    ---PRICE CHANGE ATTRIBS
    clearance_ind                  NUMBER(1),
    reason_codes                   OBJ_NUMERIC_ID_TABLE,
    price_guides                   OBJ_NUMERIC_ID_TABLE,
    states                         OBJ_VARCHAR_ID_TABLE,
    create_by_ids                  OBJ_VARCHAR_ID_TABLE,
    approved_by_ids                OBJ_VARCHAR_ID_TABLE,
    create_date_from               DATE,
    create_date_to                 DATE,
    approved_date_from             DATE,
    approved_date_to               DATE,
    effective_date_from            DATE,
    effective_date_to              DATE,
    reset_date_from                DATE,
    reset_date_to                  DATE,
    deal_ids                       OBJ_NUMERIC_ID_TABLE,
    deal_detail_ids                OBJ_NUMERIC_ID_TABLE,
    ---ITEM ATTRIBS
    item_itemlist_ind              VARCHAR2(2),
    item_level_ind                 VARCHAR2(2),
    item_list_id                   NUMBER(8),
    price_event_item_list_id       NUMBER(15),
    exclusive_item_level_ind       VARCHAR2(1),
    dept_class_subclasses          OBJ_DEPT_CLASS_SUBCLASS_TBL,
    items                          OBJ_VARCHAR_ID_TABLE,
    diff_type                      VARCHAR2(6),
    diff_ids                       OBJ_VARCHAR_ID_TABLE,
    ---LOCATION ATTRIBS
    zone_group                     NUMBER(4),
    zones                          OBJ_NUMERIC_ID_TABLE,
    locations                      OBJ_NUMERIC_ID_TABLE,
    exclusive_loc_level_ind        VARCHAR2(1),
    ---GENERAL ATTRIBS
    market_basket_codes            OBJ_VARCHAR_ID_TABLE,
    link_codes                     OBJ_NUMERIC_ID_TABLE)
/
CREATE OR REPLACE TYPE OBJ_PRICE_CHANGE_SEARCH_TBL AS TABLE OF OBJ_PRICE_CHANGE_SEARCH_REC
/

DROP TYPE OBJ_CREATE_PC_TBL FORCE
/
DROP TYPE OBJ_CREATE_PC_REC FORCE
/
DROP TYPE OBJ_MERCH_NODE_TBL FORCE
/
DROP TYPE OBJ_MERCH_NODE_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_MERCH_NODE_REC AS OBJECT
   (merch_level_type     NUMBER(1),
    dept                 NUMBER(4),
    class                NUMBER(4),
    subclass             NUMBER(4),
    item                 VARCHAR2(25),
    diff_id              VARCHAR2(10),
    link_code            NUMBER(6),
    skulist              NUMBER(8),
    merch_node_desc      VARCHAR2(250),
    price_event_itemlist NUMBER(15),
    diff_id_description  VARCHAR2(120))
/
CREATE OR REPLACE TYPE OBJ_MERCH_NODE_TBL AS TABLE OF OBJ_MERCH_NODE_REC
/
CREATE OR REPLACE TYPE OBJ_CREATE_PC_REC AS OBJECT
   (merch_nodes                OBJ_MERCH_NODE_TBL,
    zone_nodes                 OBJ_NUM_NUM_STR_TBL,
    clearance_ind              NUMBER(1),
    change_type                NUMBER(1),
    change_amount              NUMBER(20,4),
    change_currency            VARCHAR2(3),
    change_percent             NUMBER(20,4),
    change_selling_uom         VARCHAR2(4),
    null_multi_ind             NUMBER(1),
    multi_units                NUMBER(12,4),
    multi_unit_retail          NUMBER(20,4),
    multi_unit_retail_currency VARCHAR2(3),
    multi_selling_uom          VARCHAR2(4),
    cust_attributes            OBJ_CUST_ATTR_TBL)
/
CREATE OR REPLACE TYPE OBJ_CREATE_PC_TBL AS TABLE OF OBJ_CREATE_PC_REC
/