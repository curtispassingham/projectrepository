DROP TYPE OBJ_CHUNKS_FOR_PB_BATCH_REC FORCE
/
DROP TYPE OBJ_CHUNKS_FOR_PB_BATCH_TBL FORCE
/
create or replace type OBJ_CHUNKS_FOR_PB_BATCH_REC as object
(
 bulk_cc_pe_id         NUMBER(10),
 transaction_id        NUMBER(10),
 parent_thread_number  NUMBER(4),
 thread_number         NUMBER(10),
 chunk_numbers         OBJ_NUMERIC_ID_TABLE
)
/

create or replace type OBJ_CHUNKS_FOR_PB_BATCH_TBL as table of OBJ_CHUNKS_FOR_PB_BATCH_REC
/
