DROP TYPE OBJ_ROLLUP_MODS_TBL FORCE
/

DROP TYPE OBJ_ROLLUP_MODS_REC FORCE
/

CREATE OR REPLACE type OBJ_ROLLUP_MODS_REC as object 
(
   WORKSPACE_ID              NUMBER(10),
   ROLLUP_LEVEL              NUMBER(6),
   LINK_CODE                 VARCHAR2(40),
   ZONE_ID                   NUMBER(10),
   LOCATION                  NUMBER(10),
   LOC_TYPE                  NUMBER(6),
   ITEM                      VARCHAR2(25),
   ITEM_PARENT               VARCHAR2(25),
   DIFF_1                    VARCHAR2(10),
   DIFF_TYPE_1               VARCHAR2(6),
   DIFF_2                    VARCHAR2(10),
   DIFF_TYPE_2               VARCHAR2(6),
   DIFF_3                    VARCHAR2(10),
   DIFF_TYPE_3               VARCHAR2(6),
   DIFF_4                    VARCHAR2(10),
   DIFF_TYPE_4               VARCHAR2(6),
   ACTION_FLAG               NUMBER(1),
   EFFECTIVE_DATE            DATE  ,
   NEW_RETAIL                NUMBER(20,4),
   NEW_RETAIL_STD            NUMBER(20,4),
   NEW_RETAIL_UOM            VARCHAR2(4),
   NEW_MULTI_UNITS           NUMBER(12,4),
   IGNORE_CONSTRAINT_BOOLEAN NUMBER(1),
   NEW_MULTI_UNIT_UOM        VARCHAR2(4),
   NEW_MULTI_UNIT_RETAIL     NUMBER(20,4),
   NEW_CLEAR_IND             NUMBER(1),
   NEW_RESET_DATE            DATE  ,
   NEW_OUT_OF_STOCK_DATE     DATE,
   STATE                     NUMBER(4),
   NEW_CLEAR_MKDN_NBR        NUMBER(2),
   WORKSHEET_STATUS_ID       NUMBER(10),
   REVIEWED                  NUMBER(1)
)
/

CREATE OR REPLACE TYPE OBJ_ROLLUP_MODS_TBL AS TABLE OF OBJ_ROLLUP_MODS_REC
/
