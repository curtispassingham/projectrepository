-- This script will create the object
-- OBJ_PROMOTION_SEARCH_REC and OBJ_PROMOTION_REC

DROP TYPE OBJ_PROMOTION_SEARCH_TBL FORCE
/

DROP TYPE OBJ_PROMOTION_SEARCH_REC FORCE
/

CREATE TYPE OBJ_PROMOTION_SEARCH_REC AS OBJECT
(
   promotion_display_id     OBJ_VARCHAR_ID_TABLE,
   user_id                  VARCHAR2(30),
   promotion_event_id       NUMBER(6),
   currency                 VARCHAR2(3),
   states                   OBJ_NUMERIC_ID_TABLE,
   start_date_after         DATE,
   start_date_before        DATE,
   end_date_after           DATE,
   end_date_before          DATE,
   create_date_after        DATE,
   create_date_before       DATE,
   threshold_id             NUMBER(6),
   price_guide_id           NUMBER(20),
   item_itemlist_ind        VARCHAR2(2),
   item_level_ind           VARCHAR2(2),
   item_list_id             NUMBER(8),
   price_event_item_list_id NUMBER(15),
   exclusive_item_level_ind VARCHAR2(1),
   dept_class_subclasses    OBJ_DEPT_CLASS_SUBCLASS_TBL,
   items                    OBJ_VARCHAR_ID_TABLE,
   diff_type                VARCHAR2(6),
   diff_ids                 OBJ_VARCHAR_ID_TABLE,
   zone_group               NUMBER(4),
   zones                    OBJ_NUMERIC_ID_TABLE,
   locations                OBJ_NUMERIC_ID_TABLE,
   exclusive_loc_level_ind  VARCHAR2(1),
   division                 VARCHAR2(4),
   groups                   VARCHAR2(4),
   create_by_id             VARCHAR2(30),
   approved_by_id           VARCHAR2(30),
   approved_date_after      DATE,
   approved_date_before     DATE,
   vendor_funded            NUMBER(1),
   theme                    VARCHAR2(80),
   deal_id                  OBJ_NUMERIC_ID_TABLE,
   customer_types           OBJ_NUMERIC_ID_TABLE
)
/

CREATE TYPE OBJ_PROMOTION_SEARCH_TBL AS TABLE OF OBJ_PROMOTION_SEARCH_REC
/

DROP TYPE OBJ_PROMOTION_TBL FORCE
/

DROP TYPE OBJ_PROMOTION_REC FORCE
/

CREATE TYPE OBJ_PROMOTION_REC AS OBJECT
(
   promotion_id             NUMBER(10),
   promotion_display_id     VARCHAR2(10),
   promo_desc               VARCHAR2(240),
   theme                    VARCHAR2(80),
   start_date               DATE,
   end_date                 DATE,
   currency                 VARCHAR2(3),
   event_id                 NUMBER(6),
   event_desc               VARCHAR2(1000),
   event_display_id         VARCHAR2(6),
   event_start_date         DATE,
   event_end_date           DATE,
   full_access              NUMBER(1),
   hist_ind                 NUMBER(1),
   timebased_dtl_ind        NUMBER(1),
   cust_attr_id             NUMBER(15),
   cust_attr_promo_comp_ind NUMBER(1),
   cust_attr_promo_dtl_ind  NUMBER(1)
)
/

CREATE TYPE OBJ_PROMOTION_TBL AS TABLE OF OBJ_PROMOTION_REC
/
