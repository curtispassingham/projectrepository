DROP TYPE OBJ_ROLLUP_ID_TBL FORCE
/

DROP TYPE OBJ_ROLLUP_ID_REC FORCE
/

create or replace TYPE OBJ_ROLLUP_ID_REC AS OBJECT 
( 
    WORKSPACE_ID      number(10), 
    ITEM              varchar2(25), 
    ZONE_ID           number(10), 
    LINK_CODE         varchar2(10), 
    LOCATION          number(10), 
    LOCATION_TYPE     varchar2(30), 
    IS_PARENT_DIFF    number(10), 
    DIFF_ID           varchar2(10), 
    ITEM_PARENT       varchar2(25), 
    DIFF_TYPE         varchar2(6) 
)
/

create or replace TYPE OBJ_ROLLUP_ID_TBL AS TABLE OF OBJ_ROLLUP_ID_REC
/


