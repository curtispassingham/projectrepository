DROP TYPE obj_item_zone_price_TBL FORCE
/

DROP TYPE OBJ_item_zone_price_rec FORCE
/

CREATE OR REPLACE
TYPE OBJ_item_zone_price_rec AS OBJECT 
(ITEM                        VARCHAR2(25)  , 
 ZONE                        NUMBER(10)    , 
 SELLING_RETAIL              NUMBER(20,4)  , 
 SELLING_RETAIL_CURRENCY     VARCHAR2(3)   , 
 SELLING_UOM                 VARCHAR2(4)   , 
 STD_RETAIL                  NUMBER(20,4)  , 
 STD_RETAIL_CURRENCY         VARCHAR2(3)   , 
 STD_UOM                     VARCHAR2(4)   , 
 NEED_UOM_CONVERT            VARCHAR2(1)   , 
 MULTI_UNITS                 NUMBER(12,4)  , 
 MULTI_UNIT_RETAIL           NUMBER(20,4)  , 
 MULTI_UNIT_RETAIL_CURRENCY  VARCHAR2(3)   , 
 MULTI_SELLING_UOM           VARCHAR2(4)   , 
 LOCK_VERSION                NUMBER(18) 
)
/

CREATE OR REPLACE
TYPE obj_item_zone_price_TBL  IS TABLE OF  obj_item_zone_price_rec
/
