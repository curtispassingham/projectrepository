DROP TYPE OBJ_CLEARANCE_ITEM_LOC_TBL FORCE
/
DROP TYPE OBJ_CLEARANCE_ITEM_LOC_REC FORCE
/
create or replace TYPE OBJ_CLEARANCE_ITEM_LOC_REC AS OBJECT
( clearance_id    NUMBER(15),
  item            VARCHAR2(25),
  location        NUMBER(10),
  zone_node_type  NUMBER(6),
  effective_date      DATE
)
/
create or replace TYPE OBJ_CLEARANCE_ITEM_LOC_TBL AS TABLE OF OBJ_CLEARANCE_ITEM_LOC_REC;
/
