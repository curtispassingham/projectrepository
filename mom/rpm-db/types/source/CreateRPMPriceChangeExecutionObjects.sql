drop type OBJ_PRICE_CHANGE_TBL FORCE
/
drop type OBJ_PRICE_CHANGE_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PRICE_CHANGE_REC AS OBJECT
(
price_change_id     NUMBER(15),
price_change_type   VARCHAR2(2)
);
/
CREATE OR REPLACE TYPE OBJ_PRICE_CHANGE_TBL AS TABLE OF OBJ_PRICE_CHANGE_REC;
/
DROP Type PriceChange_REC FORCE
/
CREATE OR REPLACE TYPE PriceChange_REC AS OBJECT ( 
	item                     VARCHAR2(25), 
	loc                      NUMBER(10), 
	loc_type                 VARCHAR2(1), 
	selling_unit_retail      NUMBER(20,4), 
	selling_uom              VARCHAR2(4), 
	multi_units              NUMBER(12,4), 
	multi_unit_retail        NUMBER(20,4), 
	multi_selling_uom        VARCHAR2(4), 
	prom_event               VARCHAR2(6), 
	price_chg_type           VARCHAR2(2), 
	currency_code            VARCHAR2(3), 
	old_selling_unit_retail  NUMBER(20,4),
	old_selling_uom          VARCHAR2(4), 	 
	vendor_funded_ind        VARCHAR2(1 BYTE), 
    funding_type             NUMBER(1), 
    funding_amount           NUMBER(20,4), 
    funding_amount_currency  VARCHAR2(3 BYTE), 
    funding_percent          NUMBER(20,4), 
    deal_id                  NUMBER(10), 
    deal_detail_id           NUMBER(10), 
    active_date              DATE,
    is_on_clearance          VARCHAR2(1),
    pc_reason_code           NUMBER(6),                 
    change_type              NUMBER(1), 
    change_amount            NUMBER(20,4)
)
/
DROP Type PriceChange_TBL FORCE
/
CREATE TYPE PriceChange_TBL AS TABLE OF PriceChange_REC;
/
drop type PriceChangeDesc_REC FORCE
/
CREATE OR REPLACE TYPE PriceChangeDesc_REC AS OBJECT
(rib_oid NUMBER,
 PC_TBL	PriceChange_TBL)
/
