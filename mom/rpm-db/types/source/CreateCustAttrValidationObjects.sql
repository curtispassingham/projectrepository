DROP TYPE OBJ_CUST_ATTR_VALIDATION_TBL FORCE
/
DROP TYPE OBJ_CUST_ATTR_VALIDATION_REC FORCE
/
CREATE OR REPLACE TYPE OBJ_CUST_ATTR_VALIDATION_REC AS OBJECT
(
 varchar2_1       VARCHAR2(250),
 varchar2_2       VARCHAR2(250),
 varchar2_3       VARCHAR2(250),
 varchar2_4       VARCHAR2(250),
 varchar2_5       VARCHAR2(250),
 varchar2_6       VARCHAR2(250),
 varchar2_7       VARCHAR2(250),
 varchar2_8       VARCHAR2(250),
 varchar2_9       VARCHAR2(250),
 varchar2_10      VARCHAR2(250),
 number_11        NUMBER,
 number_12        NUMBER,
 number_13        NUMBER,
 number_14        NUMBER,
 number_15        NUMBER,
 number_16        NUMBER,
 number_17        NUMBER,
 number_18        NUMBER,
 number_19        NUMBER,
 number_20        NUMBER,
 date_21          DATE,
 date_22          DATE
)
/

CREATE OR REPLACE TYPE OBJ_CUST_ATTR_VALIDATION_TBL AS TABLE OF OBJ_CUST_ATTR_VALIDATION_REC
/
