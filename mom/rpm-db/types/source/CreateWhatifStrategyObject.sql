DROP TYPE OBJ_WHATIF_STRATEGY_TBL FORCE
/

DROP TYPE OBJ_WHATIF_STRATEGY_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_WHATIF_STRATEGY_REC AS OBJECT
(STRATEGY_TYPE            VARCHAR2(25),
 PRICE_GUIDE_ID           NUMBER(20),
 CLR_MARKDOWN_BASIS       NUMBER(1),
 COMPETE_TYPE             NUMBER(1),
 MAINT_MARGIN_METHOD      VARCHAR2(1),
 INCREASE                 VARCHAR2(1),
 DECREASE                 VARCHAR2(1)
);
/

CREATE OR REPLACE TYPE OBJ_WHATIF_STRATEGY_TBL AS TABLE OF OBJ_WHATIF_STRATEGY_REC
/

DROP TYPE OBJ_WHATIF_STRATEGY_CLR_TBL FORCE
/

DROP TYPE OBJ_WHATIF_STRATEGY_CLR_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_WHATIF_STRATEGY_CLR_REC AS OBJECT
(MKDN_NUMBER              NUMBER(2),
 MARKDOWN_PERCENT         NUMBER(20,4)
);
/

CREATE OR REPLACE TYPE OBJ_WHATIF_STRATEGY_CLR_TBL AS TABLE OF OBJ_WHATIF_STRATEGY_CLR_REC
/

DROP TYPE OBJ_WHATIF_STRATEGY_DTL_TBL FORCE
/

DROP TYPE OBJ_WHATIF_STRATEGY_DTL_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_WHATIF_STRATEGY_DTL_REC AS OBJECT
(COMPETE_TYPE             NUMBER(1),
 MKT_BASKET_CODE          VARCHAR2(24), 
 PERCENT                  NUMBER(20,4), 
 FROM_PERCENT             NUMBER(20,4), 
 TO_PERCENT               NUMBER(20,4)
);
/

CREATE OR REPLACE TYPE OBJ_WHATIF_STRATEGY_DTL_TBL AS TABLE OF OBJ_WHATIF_STRATEGY_DTL_REC
/

DROP TYPE OBJ_WHATIF_ROLLUP_ID_TBL FORCE
/

DROP TYPE OBJ_WHATIF_ROLLUP_ID_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_WHATIF_ROLLUP_ID_REC AS OBJECT
(ITEM                     VARCHAR2(25),
 ITEM_PARENT              VARCHAR2(25),
 LINK_CODE                VARCHAR2(40),
 DIFF_TYPE                VARCHAR2(6),
 DIFF_ID                  VARCHAR2(10),
 ZONE_ID                  NUMBER(10)
);
/

CREATE OR REPLACE TYPE OBJ_WHATIF_ROLLUP_ID_TBL AS TABLE OF OBJ_WHATIF_ROLLUP_ID_REC
/

