DROP TYPE OBJ_NUM_NUM_STR_TBL force
/

DROP TYPE obj_num_num_str_rec force
/


CREATE OR REPLACE TYPE obj_num_num_str_rec AS OBJECT(
      number_1   NUMBER(30),
      number_2   NUMBER(30),
      string     VARCHAR2(255)
   )
/

CREATE OR REPLACE
TYPE obj_num_num_str_tbl AS TABLE of obj_num_num_str_rec
/
