
DROP TYPE obj_cost_loc_tbl FORCE;
/
DROP TYPE obj_cost_loc_rec FORCE;
/
CREATE TYPE obj_cost_loc_rec AS OBJECT
(
   item                VARCHAR2(25),
   location            NUMBER(20),
   action_date         DATE,
   cost                NUMBER(20,4),
   uom                 VARCHAR2(4),
   currency_code       VARCHAR2(3),
   supplier            NUMBER(10),
   origin_country_id   VARCHAR2(3)
)
/
CREATE TYPE obj_cost_loc_tbl  IS TABLE OF obj_cost_loc_rec
/
 
-------------------------

DROP TYPE obj_rpm_promo_tbl FORCE
/
DROP TYPE obj_rpm_promo_rec FORCE
/
CREATE TYPE obj_rpm_promo_rec AS OBJECT
(
   workspace_id                VARCHAR2(100),
   rpm_promo_comp_detail_id    NUMBER(10),
   state                       VARCHAR2(50),
   start_date                  DATE,
   end_date                    DATE,
   --org node
   zone_node_type              NUMBER(6),
   location                    NUMBER(10),
   zone_id                     NUMBER(10),
   --merch node
   merch_type                  NUMBER(1),
   dept                        NUMBER(4),
   class                       NUMBER(4),
   subclass                    NUMBER(4),
   item                        VARCHAR2(25),
   diff_id                     VARCHAR2(10),
   skulist                     NUMBER(15),
   component_type              VARCHAR2(1), --1 = SIMPLE, 2 = THRESHOLD
   apply_to_code               NUMBER(1),   -- 0 = REGULAR, 1 = CLEARANCE, 2 = BOTH
   timebased_dtl_ind           NUMBER(1),
   --simple info
   change_type                 NUMBER(1),
   change_amount               NUMBER(20,4),
   change_currency             VARCHAR2(3),
   change_percent              NUMBER(20,4),
   change_selling_uom          VARCHAR2(4),
   price_guide_id              NUMBER(20),
   -- These are used in ranking
   promo_secondary_ind         NUMBER(1),
   promo_comp_secondary_ind    NUMBER(1),
   promotion_id                NUMBER(10),
   promo_start_date            DATE,
   promo_end_date              DATE,
   component_id                NUMBER(10),
   customer_type               NUMBER(10),
   sys_generated_exclusion     NUMBER(1),
   price_event_itemlist        NUMBER(15)
)
/

CREATE TYPE obj_rpm_promo_tbl AS TABLE OF obj_rpm_promo_rec
/

-------------------------

DROP TYPE obj_rpm_promo_values_tbl FORCE
/
DROP TYPE obj_rpm_promo_values_rec FORCE
/
CREATE TYPE obj_rpm_promo_values_rec AS OBJECT
(
   workspace_id                   VARCHAR2(100),
   rpm_promo_comp_detail_id       NUMBER(10),
   currency_code                  VARCHAR2(3),
   cur_rtl                        NUMBER(20,4),
   cur_rtl_unique                 NUMBER(1),
   cur_rtl_stduom_exvat           NUMBER(20,4),
   cur_rtl_stduom_exvat_unique    NUMBER(1),
   cur_cost                       NUMBER(20,4),
   cur_cost_unique                NUMBER(1),
   cur_rtl_uom                    VARCHAR2(4),  -- UOM or NULL if not all ranged have same UOM
   basis_rtl                      NUMBER(20,4),
   basis_rtl_unique               NUMBER(1),
   basis_rtl_stduom_exvat         NUMBER(20,4),
   basis_rtl_stduom_exvat_unique  NUMBER(1),
   basis_cost                     NUMBER(20,4),
   basis_cost_unique              NUMBER(1),
   basis_rtl_uom                  VARCHAR2(4), -- UOM or NULL if not all ranged have same UOM
   promo_rtl                      NUMBER(20,4),
   promo_rtl_unique               NUMBER(1),
   promo_rtl_stduom_exvat         NUMBER(20,4),
   promo_rtl_stduom_exvat_unique  NUMBER(1),
   promo_cost                     NUMBER(20,4),
   promo_rtl_uom                  VARCHAR2(4), -- UOM or NULL if not all ranged have same UOM
   non_regular_basis_ind          NUMBER(1), -- If there is a clearance, other promotion, or price change on the
                                             -- date used to calculate promotional retail this should be set
   is_multi_item_loc_ind          NUMBER(1), -- Differs from is precise, whether the retail values are averages
   clearance_overlap_ind          NUMBER(1), -- Set if during the promotion (start-end date) there is a clearance
   price_change_overlap_ind       NUMBER(1), -- Set if during the promotion (start-end date) there is a price change
   cost_change_overlap_ind        NUMBER(1),  -- Set if during the promotion (start-end date) there is a cost change
   no_item_loc_ranged_ind         NUMBER(1), -- Set when no item-locs exist for the passed merch-org node.
   async_conflict_error           NUMBER(1), -- Set when conflict exists in the rpm_con_check_err table
   conflict_during_calculation    NUMBER(1),  -- Set when package cannot calculate values due to conflict check error.
   markup_change_date             DATE
)
/
CREATE TYPE obj_rpm_promo_values_tbl AS TABLE OF obj_rpm_promo_values_rec
/
-------------------------

DROP TYPE obj_promo_ranking_tbl FORCE
/

DROP TYPE obj_promo_ranking_rec FORCE
/

CREATE TYPE obj_promo_ranking_rec AS OBJECT
(
   promotion_id            NUMBER(15),
   promo_display_id        VARCHAR2(10),
   rank                    NUMBER(3),
   secondary_ind           NUMBER(1),
   promo_start_date        DATE,
   component_id            NUMBER(15),
   comp_display_id         VARCHAR2(10),
   detail_id               NUMBER(15),
   type                    NUMBER(1),
   comp_rank               NUMBER(3),
   comp_secondary_ind      NUMBER(1),
   comp_start_date         DATE,
   start_ind               NUMBER(1),
   apply_to_code           NUMBER(1),
   change_type             NUMBER(1),
   change_amount           NUMBER(20,4),
   change_currency         VARCHAR2(3),
   change_percent          NUMBER(20,4),
   change_selling_uom      VARCHAR2(4),
   price_guide_id          NUMBER(20)
)
/
CREATE TYPE obj_promo_ranking_tbl IS TABLE OF obj_promo_ranking_rec
/
-----------------------

DROP TYPE OBJ_PROMO_DTL_REFRESH_TBL
/

DROP TYPE OBJ_PROMO_DTL_REFRESH_REC
/

CREATE TYPE OBJ_PROMO_DTL_REFRESH_REC AS OBJECT
(
   PROMO_DTL_ID          NUMBER(10),
   STATE                 NUMBER(1),
   END_DATE              VARCHAR2(20),
   DISPLAY_ID            VARCHAR2(10),
   SYS_GEN_EXCLUSION_IND NUMBER(1)
)
/

CREATE TYPE OBJ_PROMO_DTL_REFRESH_TBL IS TABLE OF OBJ_PROMO_DTL_REFRESH_REC
/
