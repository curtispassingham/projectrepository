drop type OBJ_MARGIN_VISIBILITY_TBL FORCE
/

drop type OBJ_MARGIN_VISIBILITY_REC FORCE
/

CREATE OR REPLACE
type OBJ_MARGIN_VISIBILITY_REC AS OBJECT
(
   item               VARCHAR2(25),
   location           NUMBER(10),
   price_event_id     NUMBER(15),
   markup_change_date DATE
)
/

CREATE OR REPLACE
type OBJ_MARGIN_VISIBILITY_TBL AS TABLE OF OBJ_MARGIN_VISIBILITY_REC
/

-----------------------------------

drop type OBJ_MV_MARKUP_CHG_DATES_TBL FORCE
/

drop type OBJ_MV_MARKUP_CHG_DATES_REC FORCE
/

CREATE OR REPLACE
type OBJ_MV_MARKUP_CHG_DATES_REC AS OBJECT
(
   parent_diff_id             VARCHAR2(25),
   item                       VARCHAR2(25),
   location_level             NUMBER(1),
   location_zone_id           NUMBER(10),
   cost_change_date           DATE,
   retail_change_date         DATE,
   cost                       NUMBER(20,4),
   retail                     NUMBER(20,4),
   markup_percent             NUMBER(20,4),
   clean_ind                  VARCHAR2(1),
   price_event_display_id     VARCHAR(15),
   cost_currency              VARCHAR2(3),
   retail_currency            VARCHAR2(3)
)
/

CREATE OR REPLACE
type OBJ_MV_MARKUP_CHG_DATES_TBL AS TABLE OF OBJ_MV_MARkUP_CHG_DATES_REC
/

-----------------------------------

drop type OBJ_MERCH_ND_ZONE_ND_DATE_TBL FORCE
/

drop type OBJ_MERCH_ND_ZONE_ND_DATE_REC FORCE
/

CREATE OR REPLACE
type OBJ_MERCH_ND_ZONE_ND_DATE_REC AS OBJECT
(
   zone_node_type       NUMBER(1),
   location             NUMBER(10),
   zone_id              NUMBER(10),
   merch_type           NUMBER(1),
   dept                 NUMBER(10),
   class                NUMBER(10),
   subclass             NUMBER(10),
   item                 VARCHAR2(25),
   diff_id              VARCHAR2(10),
   skulist              NUMBER(15),
   effective_date       DATE,
   price_event_itemlist NUMBER(15)
)
/

CREATE OR REPLACE
type OBJ_MERCH_ND_ZONE_ND_DATE_TBL AS TABLE OF OBJ_MERCH_ND_ZONE_ND_DATE_REC
/
