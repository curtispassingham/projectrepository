DROP TYPE OBJ_MERCH_ZONE_ND_DESCS_TBL
/

DROP TYPE OBJ_MERCH_ZONE_ND_DESCS_REC
/

CREATE TYPE OBJ_MERCH_ZONE_ND_DESCS_REC AS OBJECT
(
   ZONE_NODE_TYPE             NUMBER(1),
   LOCATION                   NUMBER(10),
   LOC_DESC                   VARCHAR2(150),
   ZONE_ID                    NUMBER(10),
   ZONE_DISPLAY_ID            NUMBER(10),
   ZONE_DESC                  VARCHAR2(120),
   MERCH_TYPE                 NUMBER(1),
   DEPT                       NUMBER(10),
   DEPT_DESC                  VARCHAR2(120),
   CLASS                      NUMBER(10),
   CLASS_DESC                 VARCHAR2(120),
   SUBCLASS                   NUMBER(10),
   SUBCLASS_DESC              VARCHAR2(120),
   ITEM                       VARCHAR2(25),
   ITEM_DESC                  VARCHAR2(250),
   DIFF_ID                    VARCHAR2(10),
   DIFF_DESC                  VARCHAR2(120),
   SKULIST                    NUMBER(15),
   SKULIST_DESC               VARCHAR2(120),
   PRICE_EVENT_ITEMLIST       NUMBER(15),
   PRICE_EVENT_ITEMLIST_DESC  VARCHAR2(120)
);
/

CREATE TYPE OBJ_MERCH_ZONE_ND_DESCS_TBL IS TABLE OF OBJ_MERCH_ZONE_ND_DESCS_REC;
/
