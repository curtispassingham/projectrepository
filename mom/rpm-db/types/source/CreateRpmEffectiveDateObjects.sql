DROP TYPE OBJ_EFF_DATES_UPD_TBL FORCE
/

DROP TYPE OBJ_EFF_DATES_UPD_REC FORCE
/

CREATE OR REPLACE type OBJ_EFF_DATES_UPD_REC as object 
(
   WORKSPACE_ID              NUMBER(10),
   LINK_CODE                 VARCHAR2(40),
   ITEM_PARENT               VARCHAR2(25),
   DIFF_ID                   VARCHAR2(10),
   ITEM                      VARCHAR2(25),
   ZONE_ID                   NUMBER(10),
   EFFECTIVE_DATE            DATE,
   DEPT                      NUMBER(4),
   PROPOSED_STRATEGY_ID      NUMBER (10)
)
/

CREATE OR REPLACE TYPE OBJ_EFF_DATES_UPD_TBL AS TABLE OF OBJ_EFF_DATES_UPD_REC
/

