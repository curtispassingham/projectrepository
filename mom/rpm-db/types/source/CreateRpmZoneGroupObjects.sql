drop type OBJ_ZONE_GROUP_SEARCH_TBL FORCE
/

drop type OBJ_ZONE_GROUP_SEARCH_REC FORCE
/

drop type OBJ_ZONE_TBL FORCE
/

drop type OBJ_ZONE_REC FORCE
/

drop type OBJ_ZONE_LOC_TBL FORCE
/

drop type OBJ_ZONE_LOC_REC FORCE
/

create or replace TYPE OBJ_ZONE_GROUP_SEARCH_REC AS OBJECT
(
    ZONE_GROUP_ID               NUMBER(4),
    CURRENCY_CODE               VARCHAR2(3),
    ZONE_ID                     NUMBER(4)
)
/

create or replace TYPE OBJ_ZONE_GROUP_SEARCH_TBL AS TABLE OF OBJ_ZONE_GROUP_SEARCH_REC
/

create or replace TYPE OBJ_ZONE_LOC_REC AS OBJECT 
( 
    ZONE_LOCATION_ID            NUMBER(10),
    LOC_NAME                    VARCHAR2(150),
    LOC_ID                      NUMBER(10),
    LOCTYPE                     NUMBER(6),
    DEF_CURRENCY_CODE           VARCHAR2(3),
    WHOLESALE                   VARCHAR2(6),
    LM_SCHEDULE_DATE            DATE
)
/

create or replace TYPE OBJ_ZONE_LOC_TBL AS TABLE OF OBJ_ZONE_LOC_REC
/

create or replace TYPE OBJ_ZONE_REC AS OBJECT
(
    ZONE_ID                     NUMBER(10),
    ZONE_DISPLAY_ID             NUMBER(10),
    ZONE_NAME                   VARCHAR2(120),
    ZONE_CURRENCY_CODE          VARCHAR2(3),
    BASE_IND                    NUMBER(6),
    ZONE_LOCATIONS              OBJ_ZONE_LOC_TBL
)
/

create or replace TYPE OBJ_ZONE_TBL AS TABLE OF OBJ_ZONE_REC
/
