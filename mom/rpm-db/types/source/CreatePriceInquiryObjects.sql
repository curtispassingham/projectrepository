DROP TYPE OBJ_PRC_INQ_TBL FORCE
/

DROP TYPE OBJ_PRC_INQ_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PRC_INQ_REC AS OBJECT
(
item                              VARCHAR2(25),
item_desc                         VARCHAR2(250),
is_parent                         NUMBER(1),
diff_Id                           VARCHAR2(10),
location                          NUMBER(10),
location_display_id               NUMBER(10),
location_name                     VARCHAR2(150),
cust_seg_promo_ind                NUMBER(1),
primary_zone                      NUMBER(1),
is_zone                           NUMBER(1),
action_date                       DATE,
selling_retail                    NUMBER(20,4),
selling_uom                       VARCHAR2(4),
selling_retail_currency           VARCHAR2(3),
multi_units                       NUMBER(12,4),
multi_unit_retail                 NUMBER(20,4),
multi_selling_uom                 VARCHAR2(4),
multi_selling_retail_currency     VARCHAR2(3),
clear_retail                      NUMBER(20,4),
clear_uom                         VARCHAR2(4),
clear_retail_currency             VARCHAR2(3),
simple_promo_retail               NUMBER(20,4),
simple_promo_uom                  VARCHAR2(4),
simple_promo_retail_currency      VARCHAR2(3),
complex_promo                     NUMBER(1),
reg_retail_avg                    NUMBER(1),
reg_multi_unit_avg                NUMBER(1),
reg_multi_unit_retail_avg         NUMBER(1),
clear_retail_avg                  NUMBER(1),
promo_retail_avg                  NUMBER(1),
cost                              NUMBER(20,4),
markup_percent                    NUMBER(12,4)
)
/

CREATE OR REPLACE TYPE OBJ_PRC_INQ_TBL AS TABLE OF OBJ_PRC_INQ_REC
/

DROP TYPE OBJ_PRICE_INQ_SEARCH_TBL FORCE
/

DROP TYPE OBJ_PRICE_INQ_SEARCH_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PRICE_INQ_SEARCH_REC AS OBJECT
(DEPT_CLASS_SUBCLASS OBJ_DEPT_CLASS_SUBCLASS_TBL,
ITEM_TYPE                VARCHAR2(2),
ITEM_LEVEL               VARCHAR2(2),
ITEM_LIST_ID             NUMBER(8,0),
ITEMS                    OBJ_VARCHAR_ID_TABLE,
DIFF_TYPE                VARCHAR2(6),
DIFF_IDS                 OBJ_VARCHAR_ID_TABLE,
ZONE_GROUP               NUMBER(4),
ZONE_IDS                 OBJ_NUMERIC_ID_TABLE,
LOCATIONS                OBJ_NUMERIC_ID_TABLE,
EFFECTIVE_DATE           DATE,
MAXIMUM_RESULT           NUMBER(20),
CUSTOMER_TYPES           OBJ_NUMERIC_ID_TABLE,
USER_ID                  VARCHAR2(30),
PRICE_EVENT_ITEM_LIST_ID NUMBER(15)
)
/

CREATE OR REPLACE TYPE OBJ_PRICE_INQ_SEARCH_TBL AS TABLE OF OBJ_PRICE_INQ_SEARCH_REC
/

DROP TYPE OBJ_CUST_SEG_PROMO_TBL FORCE
/

DROP TYPE OBJ_CUST_SEG_PROMO_REC FORCE
/

CREATE OR REPLACE type OBJ_CUST_SEG_PROMO_REC as object
 (
  CUSTOMER_TYPE                                      NUMBER(10),
  CUSTOMER_TYPE_DESC                                 VARCHAR2(120),
  ITEM                                               VARCHAR2(25),
  LOCATION                                           NUMBER(10),
  LOCATION_DISPLAY_ID                                NUMBER(10),
  PROMO_RETAIL                                       NUMBER(20,4),
  PROMO_UOM                                          VARCHAR2(4),
  COMPLEX_PROMO_IND                                  NUMBER(1),
  PROMO_RETAIL_AVG                                   NUMBER(1),
  PROMO_CURRENCY                                     VARCHAR2(3),
  PARENT_ITEM_IND                                    NUMBER(1))
/

DROP TYPE OBJ_CUST_SEG_PROMO_REQ_TBL FORCE
/

DROP TYPE OBJ_CUST_SEG_PROMO_REQ_REC FORCE
/

CREATE OR REPLACE type OBJ_CUST_SEG_PROMO_REQ_REC  as object
 (
 ITEM                                               VARCHAR2(25),
 DIFF_ID                                            VARCHAR2(10),
 LOCATION                                           NUMBER(10),
 IS_ZONE                                            NUMBER(1),
 ACTION_DATE                                        DATE,
 CUSTOMER_TYPES                                     OBJ_NUMERIC_ID_TABLE
 )
/

CREATE OR REPLACE type OBJ_CUST_SEG_PROMO_REQ_tbl  as table of OBJ_CUST_SEG_PROMO_REQ_rec
/

CREATE OR REPLACE type OBJ_CUST_SEG_PROMO_tbl  as table of OBJ_CUST_SEG_PROMO_rec
/
