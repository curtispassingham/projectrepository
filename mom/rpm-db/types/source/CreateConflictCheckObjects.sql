DROP TYPE conflict_check_error_tbl FORCE
/
DROP TYPE conflict_check_error_rec FORCE
/
CREATE OR REPLACE TYPE conflict_check_error_rec AS OBJECT(
      price_event_id              NUMBER(15),
      future_retail_id            NUMBER(15),
      error_type                  NUMBER(1),
      error_string                VARCHAR2(255),
      cs_promo_fr_id              NUMBER(15),
      ---
      CONSTRUCTOR FUNCTION conflict_check_error_rec
      RETURN SELF AS RESULT,
      ---
      CONSTRUCTOR FUNCTION conflict_check_error_rec(I_price_event_id   NUMBER,
                                                    I_future_retail_id NUMBER,
                                                    I_error_type       NUMBER,
                                                    I_error_string     VARCHAR2)
      RETURN SELF AS RESULT
   ) NOT FINAL;
/

CREATE OR REPLACE TYPE BODY conflict_check_error_rec AS

   CONSTRUCTOR FUNCTION conflict_check_error_rec
   RETURN SELF AS RESULT IS
   BEGIN
       SELF.price_event_id   := NULL;
       SELF.future_retail_id := NULL;
       SELF.error_type       := NULL;
       SELF.error_string     := NULL;
       SELF.cs_promo_fr_id   := NULL;

       return;
   END;

   CONSTRUCTOR FUNCTION conflict_check_error_rec(I_price_event_id   NUMBER,
                                                 I_future_retail_id NUMBER,
                                                 I_error_type       NUMBER,
                                                 I_error_string     VARCHAR2)
    RETURN SELF AS RESULT IS
    BEGIN
       SELF.price_event_id   := I_price_event_id;
       SELF.future_retail_id := I_future_retail_id;
       SELF.error_type       := I_error_type;
       SELF.error_string     := I_error_string;
       SELF.cs_promo_fr_id   := NULL;

       return;
   END;
END;
/

CREATE OR REPLACE TYPE conflict_check_error_tbl AS TABLE of conflict_check_error_rec
/
