DROP TYPE OBJ_LISTABLE_TBL FORCE
/
DROP TYPE OBJ_LISTABLE_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_LISTABLE_REC AS OBJECT
   (id         NUMBER(10),
    display_id VARCHAR(10),
    name       VARCHAR2(120))
/
CREATE OR REPLACE TYPE OBJ_LISTABLE_TBL AS TABLE OF OBJ_LISTABLE_REC
/

-----------------------------------

DROP TYPE OBJ_LISTABLE4_TBL FORCE
/
DROP TYPE OBJ_LISTABLE4_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_LISTABLE4_REC AS OBJECT
   (id         NUMBER(4),
    display_id VARCHAR(4),
    name       VARCHAR2(120))
/
CREATE OR REPLACE TYPE OBJ_LISTABLE4_TBL AS TABLE OF OBJ_LISTABLE4_REC
/

-----------------------------------

DROP TYPE OBJ_PRICE_STRATEGY_TBL FORCE
/
DROP TYPE OBJ_PRICE_STRATEGY_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_PRICE_STRATEGY_REC AS OBJECT
   (strategy_id               NUMBER(10),
    type_code                 VARCHAR2(1),
    state_code                VARCHAR2(50),
    dept_id                   NUMBER(4),
    dept_name                 VARCHAR2(120),
    class_id                  NUMBER(4),
    class_name                VARCHAR2(120),
    subclass_id               NUMBER(4),
    subclass_name             VARCHAR2(120),
    zone_group_id             NUMBER(4),
    zone_group_disp_id        NUMBER(4),
    zone_group_name           VARCHAR2(120),
    zone_id                   NUMBER(10),
    zone_disp_id              NUMBER(10),
    zone_name                 VARCHAR2(120),
    zone_currency_code        VARCHAR2(3),
    zone_base_ind             NUMBER(6),
    suspend                   NUMBER(1),
    current_cal_id            NUMBER(10),
    current_cal_name          VARCHAR2(100),
    current_cal_desc          VARCHAR(1000),
    current_cal_end_date      DATE,
    new_cal_id                NUMBER(10),
    new_cal_name              VARCHAR2(100),
    new_cal_desc              VARCHAR(1000),
    new_cal_end_date          DATE,
    price_guide_id            NUMBER(20),
    price_guide_name          VARCHAR2(480),
    price_guide_description   VARCHAR2(1000),
    price_guide_currency_code VARCHAR2(3),
    price_guide_corp          VARCHAR(1),
    price_guide_depts         OBJ_LISTABLE4_TBL,
    warehouses                OBJ_LISTABLE_TBL,
    zone_locations            OBJ_ZONE_LOC_TBL)
/
CREATE OR REPLACE TYPE OBJ_PRICE_STRATEGY_TBL AS TABLE OF OBJ_PRICE_STRATEGY_REC
/

-----------------------------------

DROP TYPE OBJ_AREA_DIFF_MBC_TBL FORCE
/
DROP TYPE OBJ_AREA_DIFF_MBC_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_AREA_DIFF_MBC_REC AS OBJECT
   (area_diff_mbc_id NUMBER(10),
    mkt_basket_code  VARCHAR2(6),
    mbc_name         VARCHAR2(30))
/
CREATE OR REPLACE TYPE OBJ_AREA_DIFF_MBC_TBL AS TABLE OF OBJ_AREA_DIFF_MBC_REC
/

-----------------------------------
DROP TYPE OBJ_REF_COMP_TBL FORCE
/
DROP TYPE OBJ_REF_COMP_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_REF_COMP_REC AS OBJECT
   (comp_index      NUMBER(1),
    comp_store_id   NUMBER(10),
    comp_store_name VARCHAR2(20),
    comp_id         NUMBER(10),
    comp_name       VARCHAR2(40),
    comp_type       NUMBER(1),
    comp_percent    NUMBER(20,4))
/
CREATE OR REPLACE TYPE OBJ_REF_COMP_TBL AS TABLE OF OBJ_REF_COMP_REC
/

-----------------------------------

DROP TYPE OBJ_AREA_DIFF_SEC_TBL FORCE
/
DROP TYPE OBJ_AREA_DIFF_SEC_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_AREA_DIFF_SEC_REC AS OBJECT
   (secondary_id              NUMBER(10),
    auto_approve_ind          NUMBER(1),
    pct_apply_type            NUMBER(1),
    pct                       NUMBER(12,4),
    comp_store_id             NUMBER(10),
    comp_store_name           VARCHAR2(150),
    comp_id                   NUMBER(10),
    comp_name                 VARCHAR2(160),
    comp_type                 NUMBER(1),
    comp_detail_id            NUMBER(10),
    zone_group_id             NUMBER(4),
    zone_group_disp_id        NUMBER(4),
    zone_group_name           VARCHAR2(120),
    zone_id                   NUMBER(10),
    zone_disp_id              NUMBER(10),
    zone_name                 VARCHAR2(120),
    zone_currency_code        VARCHAR2(3),
    zone_base_ind             NUMBER(6),
    range_from                NUMBER(20,4),
    range_to                  NUMBER(20,4),
    range_pct                 NUMBER(20,4),
    price_guide_id            NUMBER(20),
    price_guide_name          VARCHAR2(480),
    price_guide_description   VARCHAR2(1000),
    price_guide_currency_code VARCHAR2(3),
    price_guide_corp          VARCHAR(1),
    price_guide_depts         OBJ_LISTABLE4_TBL,
    warehouses                OBJ_LISTABLE_TBL,
    zone_locations            OBJ_ZONE_LOC_TBL,
    exclusions                OBJ_LISTABLE_TBL,
    reference_competitors     OBJ_REF_COMP_TBL,
    mbc_details               OBJ_AREA_DIFF_MBC_TBL)
/
CREATE OR REPLACE
TYPE OBJ_AREA_DIFF_SEC_TBL AS TABLE OF OBJ_AREA_DIFF_SEC_REC
/

-----------------------------------

DROP TYPE OBJ_AREA_DIFF_TBL FORCE
/
DROP TYPE OBJ_AREA_DIFF_REC FORCE
/

CREATE OR REPLACE TYPE OBJ_AREA_DIFF_REC AS OBJECT
   (strategy_id          NUMBER(10),
    dept_id              NUMBER(4),
    dept_name            VARCHAR2(120),
    markup_calc_type     VARCHAR(2),
    class_id             NUMBER(4),
    class_name           VARCHAR2(120),
    subclass_id          NUMBER(4),
    subclass_name        VARCHAR2(120),
    zone_group_id        NUMBER(4),
    zone_group_disp_id   NUMBER(4),
    zone_group_name      VARCHAR2(120),
    zone_id              NUMBER(10),
    zone_disp_id         NUMBER(10),
    zone_name            VARCHAR2(120),
    zone_currency_code   VARCHAR2(3),
    zone_base_ind        NUMBER(6),
    warehouses           OBJ_LISTABLE_TBL,
    zone_locations       OBJ_ZONE_LOC_TBL,
    secondary_area_diffs OBJ_AREA_DIFF_SEC_TBL)
/
CREATE OR REPLACE TYPE OBJ_AREA_DIFF_TBL AS TABLE OF OBJ_AREA_DIFF_REC
/