DROP TYPE OBJ_PROMO_COMP_DETAIL_TBL FORCE
/
DROP TYPE OBJ_PROMO_COMP_DETAIL_REC FORCE
/

CREATE TYPE OBJ_PROMO_COMP_DETAIL_REC AS OBJECT
(
rpcd_id              NUMBER(10),
--
item                 VARCHAR2(25),
location             NUMBER(10),
start_date           DATE,
dept                 NUMBER(4),
--
apply_to_code        NUMBER(1),
change_type          NUMBER(1),
change_amount        NUMBER(20,4),
change_currency      VARCHAR2(3),
change_percent       NUMBER(20,4),
change_selling_uom   VARCHAR2(4),
--
markup_calc_type     VARCHAR2(2),
--
cost                 NUMBER(20,4),
cost_currency        VARCHAR2(3),
--
retail               NUMBER(20,4),
retail_currency      VARCHAR2(3),
selling_uom          VARCHAR2(4),
standard_uom         VARCHAR2(4),
--
vat_rate             NUMBER(20,10),
vat_date             DATE
)
/

CREATE TYPE OBJ_PROMO_COMP_DETAIL_TBL IS TABLE OF OBJ_PROMO_COMP_DETAIL_REC;
/

--


DROP TYPE OBJ_RPM_PROM_MKUP_TBL FORCE
/
DROP TYPE OBJ_RPM_PROM_MKUP_REC FORCE
/

CREATE TYPE OBJ_RPM_PROM_MKUP_REC AS OBJECT
(
promo_id           NUMBER(10),
--
total_cost         NUMBER(20,4),
total_retail       NUMBER(20,4),
markup_calc_type   VARCHAR2(2),
currency_code      VARCHAR2(3)
)
/

CREATE TYPE OBJ_RPM_PROM_MKUP_TBL IS TABLE OF OBJ_RPM_PROM_MKUP_REC;
/
