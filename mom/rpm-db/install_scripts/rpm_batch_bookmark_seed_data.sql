SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-----------------------------------------------------
--       Inserting seed data into RPM_BATCH_BOOKMARK
-----------------------------------------------------

insert into rpm_batch_bookmark
   (batch_bookmark_id,
    batch_name,
    bookmark_value)
   (select RPM_BATCH_BOOKMARK_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PurgeFutureRetailBatch',
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_bookmark
                        where batch_name = 'com.retek.rpm.batch.PurgeFutureRetailBatch'))
/

insert into rpm_batch_bookmark
   (batch_bookmark_id,
    batch_name,
    bookmark_value)
   (select RPM_BATCH_BOOKMARK_SEQ.NEXTVAL,
           'com.retek.rpm.batch.FutureRetailRollupBatch',
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_bookmark
                        where batch_name = 'com.retek.rpm.batch.FutureRetailRollupBatch'))
/

commit;
