SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
spool rsmrpm.log;

--- Startall ---
prompt 'Starting File: RSM_RPM_hierarchy_type.sql';
@@RSM_RPM_hierarchy_type.sql;

prompt 'Starting File: RSM_RPM_SE_named_permission.sql';
@@RSM_RPM_SE_named_permission.sql;

prompt 'Starting File: RSM_RPM_SE_role.sql';
@@RSM_RPM_SE_role.sql;

prompt 'Starting File: RSM_RPM_role_named_permission.sql';
@@RSM_RPM_role_named_permission.sql;

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_en.sql';
@@RSM_RPM_SE_named_permission_dsc_en.sql;

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_de.sql';
@@RSM_RPM_SE_named_permission_dsc_de.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_el.sql';
@@RSM_RPM_SE_named_permission_dsc_el.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_en.sql';
@@RSM_RPM_SE_named_permission_dsc_en.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_es.sql';
@@RSM_RPM_SE_named_permission_dsc_es.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_fr.sql';
@@RSM_RPM_SE_named_permission_dsc_fr.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_hr.sql';
@@RSM_RPM_SE_named_permission_dsc_hr.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_hu.sql';
@@RSM_RPM_SE_named_permission_dsc_hu.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_it.sql';
@@RSM_RPM_SE_named_permission_dsc_it.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_ja.sql';
@@RSM_RPM_SE_named_permission_dsc_ja.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_ko.sql';
@@RSM_RPM_SE_named_permission_dsc_ko.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_nl.sql';
@@RSM_RPM_SE_named_permission_dsc_nl.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_pl.sql';
@@RSM_RPM_SE_named_permission_dsc_pl.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_pt.sql';
@@RSM_RPM_SE_named_permission_dsc_pt.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_ru_RU.sql';
@@RSM_RPM_SE_named_permission_dsc_ru_RU.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_sv.sql';
@@RSM_RPM_SE_named_permission_dsc_sv.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_tr.sql';
@@RSM_RPM_SE_named_permission_dsc_tr.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_zh_CN.sql';
@@RSM_RPM_SE_named_permission_dsc_zh_CN.sql

prompt 'Starting File: RSM_RPM_SE_named_permission_dsc_zh_TW.sql';
@@RSM_RPM_SE_named_permission_dsc_zh_TW.sql

spool off;