
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
spool rpmctl.log;

--- Startall ---
prompt 'Starting File: rpm_rib_properties_seed_data.sql';
@@rpm_rib_properties_seed_data.sql;
prompt 'Starting File: rpm_mbc_lov_values_seed_data.sql';
@@rpm_mbc_lov_values_seed_data.sql;
prompt 'Starting File: rpm_worksheet_column_seed_data.sql';
@@rpm_worksheet_column_seed_data.sql;
prompt 'Starting File: rpm_worksheet_view_seed_data.sql';
@@rpm_worksheet_view_seed_data.sql;
prompt 'Starting File: rpm_worksheet_view_col_seed_data.sql';
@@rpm_worksheet_view_col_seed_data.sql;
prompt 'Starting File: rpm_batch_control_seed_data.sql';
@@rpm_batch_control_seed_data.sql;
prompt 'Starting File: rpm_batch_config_seed_data.sql';
@@rpm_batch_config_seed_data.sql;
prompt 'Starting File: rpm_conflict_query_control_seed_data.sql';
@@rpm_conflict_query_control_seed_data.sql;
prompt 'Starting File: rpm_pre_process_config_seed_data.sql';
@@rpm_pre_process_config_seed_data.sql;
prompt 'Starting File: rpm_codes_seed_data.sql';
@@rpm_codes_seed_data.sql;
prompt 'Starting File: alert_status_seed_data.sql';
@@alert_status_seed_data.sql;
prompt 'Starting File: alert_status_dsc_seed_data.sql';
@@alert_status_dsc_seed_data.sql;
prompt 'Starting File: task_state_seed_data.sql';
@@task_state_seed_data.sql;
prompt 'Starting File: task_alert_mode_seed_data.sql';
@@task_alert_mode_seed_data.sql;
prompt 'Starting File: sys_proc_typ_seed_data.sql';
@@sys_proc_typ_seed_data.sql;
prompt 'Starting File: rpm_batch_trace_seed_data.sql';
@@rpm_batch_trace_seed_data.sql;
prompt 'Starting File: rpm_batch_trace_seed_data.sql';
@@rpm_custom_attribute_label_seed_data.sql;
prompt 'Starting File: rpm_config_gtt_capture_seed_data.sql';
@@rpm_config_gtt_capture_seed_data.sql;
prompt 'Starting File: rpm_batch_bookmark_seed_data.sql';
@@rpm_batch_bookmark_seed_data.sql

spool off;

exit;
