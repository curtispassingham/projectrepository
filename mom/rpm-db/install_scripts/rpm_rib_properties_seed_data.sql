
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
insert into rpm_rib_properties
(
 RIB_PROPERTIES_ID,
 FAMILY,
 MAX_DETAILS_TO_PUBLISH
) 
values 
(
 1, 
 'RegPrcChg', 
 1000
)
/

insert into rpm_rib_properties 
(
 RIB_PROPERTIES_ID,
 FAMILY,
 MAX_DETAILS_TO_PUBLISH
) 
values 
(
 2, 
 'ClrPrcChg', 
 1000
)
/

insert into rpm_rib_properties
(
 RIB_PROPERTIES_ID,
 FAMILY,
 MAX_DETAILS_TO_PUBLISH
) 
values 
(
 3, 
 'PrmPrcChg', 
 1000
)
/

commit;
