
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- INSERT rpm_worksheet_view_col

INSERT INTO rpm_worksheet_view_col (worksheet_view_id, worksheet_column_id, view_order, view_width, visible_ind, lock_version)
SELECT v.worksheet_view_id, c.worksheet_column_id, c.worksheet_column_map, c.default_width, 1, 0
FROM rpm_worksheet_view v, rpm_worksheet_column c
WHERE v.corp_ind = 1;
 
COMMIT;