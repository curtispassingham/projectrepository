
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
insert into rpm_mbc_lov_values
(mkt_basket_code, name) values ('HCOMP', 'Highly Competitive')
/

insert into rpm_mbc_lov_values
(mkt_basket_code, name) values ('NCOMP', 'Not Competitive')
/

insert into rpm_mbc_lov_values
(mkt_basket_code, name) values ('COMP', 'Competitive')
/

commit;

