SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

--An Administrator role should already exist once the RSM install is done
--An association of a single user (e.g., RETAIL.USER) with the Administrator Role should also exist in USER_ROLE
--NOTE: the user (e.g., RETAIL.USER) must be available as a valid user from LDAP


--This will associate that Role with the RPM Administrator permission to have access to all RPM permissions
insert into rsm_role_named_permission (id,
                                       role_id,
                                       permission_id,
                                       is_view,
                                       is_edit,
                                       is_submit,
                                       is_approve,
                                       is_emergency) 
select RSM_ROLE_NAMED_PERMISSION_SEQ.NEXTVAL,
       r.id,
       n.id,
       1,
       1,
       1,
       1,
       1
  from rsm_role r,
       rsm_named_permission n
 where n.name = 'rsm.*'
   and r.role_description = 'Administrator Role'
   and NOT EXISTS (select 1
                     from rsm_role_named_permission p
                    where p.role_id       = r.id
                      and p.permission_id = n.id);

insert into rsm_role_named_permission (id,
                                       role_id,
                                       permission_id,
                                       is_view,
                                       is_edit,
                                       is_submit,
                                       is_approve,
                                       is_emergency) 
select RSM_ROLE_NAMED_PERMISSION_SEQ.NEXTVAL,
       r.id,
       n.id,
       1,
       1,
       1,
       1,
       1
  from rsm_role r,
       rsm_named_permission n
 where n.name like 'rpm.%'
   and r.role_description = 'Administrator Role'
   and NOT EXISTS (select 1
                     from rsm_role_named_permission p
                    where p.role_id       = r.id
                      and p.permission_id = n.id);

WHENEVER SQLERROR CONTINUE