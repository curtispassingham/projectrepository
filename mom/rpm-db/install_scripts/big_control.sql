
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
spool rsm_stuff.log;
@RSM_RPM_SE_role.sql
@RSM_RPM_hierarchy_type.sql
@RSM_RPM_SE_named_permission.sql
@RSM_RPM_SE_named_permission_dsc_en.sql
@RSM_RPM_role_named_permission.sql
spool off;