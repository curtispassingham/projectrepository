
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- 0 - pending
-- 1 - executing
-- 2 - expired
-- 3 - unknown
insert into TASK_STATE (TASK_STATE_ID) values (0);
insert into TASK_STATE (TASK_STATE_ID) values (1);
insert into TASK_STATE (TASK_STATE_ID) values (2);
insert into TASK_STATE (TASK_STATE_ID) values (3);
 
commit;