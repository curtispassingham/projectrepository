
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- INSERT rpm_worksheet_column

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,0,100,3,'PRICE_CHANGE_PERCENT_REG_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,1,100,3,'PRICE_CHANGE_PERCENT_BAS_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,2,100,3,'BASE_COST_AVG','BASE_COST_MAX',0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,3,100,3,'BASIS_COST_AVG','BASIS_COST_MAX',0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,4,100,3,'CLASS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,5,100,3,'CLASS_NAME',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,6,100,3,'NEW_CLEAR_IND',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,7,100,3,'CLEAR_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,8,100,3,'PRIMARY_COMPETITOR_ALERT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,9,100,3,null,null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,10,100,3,'PRIMARY_COMP_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,11,100,3,'PRIMARY_COMP_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,12,100,3,'PRIMARY_MULTI_UNITS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,13,100,3,'PRIMARY_MULTI_UNIT_RETAIL',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,14,100,3,'PRIMARY_MULTI_UNIT_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,15,100,3,'A_COMP_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,16,100,3,'A_COMP_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,17,100,3,'B_COMP_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,18,100,3,'B_COMP_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,19,100,3,'C_COMP_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,20,100,3,'C_COMP_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,21,100,3,'D_COMP_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,22,100,3,'D_COMP_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,23,100,3,'E_COMP_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,24,100,3,'E_COMP_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,25,100,3,'PEND_COST_CHANGE_DATE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,26,100,3,null,null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,27,100,3,'CURRENT_CLEARANCE_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,28,100,3,'CURRENT_CLEARANCE_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,29,100,3,'BASIS_REGULAR_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,30,100,3,'BASIS_REGULAR_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,31,100,3,'BASIS_MULTI_UNITS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,32,100,3,'BASIS_MULTI_UNIT_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,33,100,3,'BASIS_MULTI_UNIT_UOM_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,34,100,3,'CURRENT_COST_AVG','CURRENT_COST_MAX',0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,35,100,3,'CURRENT_COST_MARKUP_PERCENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,36,100,3,'CURRENT_RETAIL_MARKUP_PERCENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,37,100,3,'CURRENT_MULTI_UNITS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,38,100,3,'CURRENT_MULTI_UNIT_RETAIL',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,39,100,3,'CURRENT_MULTI_UNIT_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,40,100,3,'CURRENT_REGULAR_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,41,100,3,'CURRENT_REGULAR_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,42,100,3,null,null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,43,100,3,'DIFF_1',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,44,100,3,'DIFF_2',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,45,100,3,'DIFF_3',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,46,100,3,'DIFF_4',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,47,100,3,'EFFECTIVE_DATE_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,48,100,3,'NEW_ITEM_LOC_IND',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,49,100,3,'FIRST_RECEIVED_DATE_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,50,100,3,'HISTORICAL_SALES_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,51,100,3,'HISTORICAL_SALES_UNITS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,52,100,3,'ITEM_DESC',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,53,100,3,'ITEM_ID',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,54,100,3,'LAST_RECEIVED_DATE_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,55,100,3,'LINK_CODE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,56,100,3,'PROPOSED_RETAIL_MARKDOWN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,57,100,3,'MARGIN_MKT_BASKET_CODE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,58,100,3,'NEW_RETAIL_MARKUP_PERCENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,59,100,3,'NEW_MULTI_UNITS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,60,100,3,'NEW_MULTI_UNITS_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,61,100,3,'NEW_MULTI_UNIT_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,62,100,3,'NEW_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,63,100,3,null,null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,64,100,3,'NEW_RETAIL_MARKUP_PERCENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,65,100,3,'NEW_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,66,100,3,'ORIGINAL_RETAIL',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,67,100,3,'OUT_OF_STOCK_DATE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,68,100,3,'RESET_DATE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,69,100,3,'PACKAGE_SIZE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,70,100,3,'PACKAGE_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,71,100,3,'ITEM_PARENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,72,100,3,'PARENT_DESC',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,73,100,3,'PRICE_CHANGE_AMOUNT_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,74,100,3,'ACTION_FLAG',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,75,100,3,'PRICE_CHANGE_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,76,100,3,'PRIMARY_SUPPLIER',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,77,100,3,'PROMO_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,78,100,3,'PROPOSED_COST_MARKUP_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,79,100,3,'PROPOSED_RETAIL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,80,100,3,null,null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,81,100,3,'PROPOSED_RETAIL_UOM',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,82,100,3,'PROPOSED_RETAIL_MARKUP_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,83,100,3,'REPLENISH_IND',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,84,100,3,'RETAIL_LABEL_TYPE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,85,100,3,'RETAIL_LABEL_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,86,100,3,'ZONE_GROUP_DISPLAY_ID',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,87,100,3,'ZONE_DISPLAY_ID',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,88,100,3,'RULE_FLAG',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,89,100,3,'PROJECTED_SALES',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,90,100,3,'SEASONAL_SELL_THRU_PERCENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,91,100,3,'SEASON_PHASE_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,92,100,3,'SELL_THRU_PERCENT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,93,100,3,'STORE_STOCK_ON_HAND',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,94,100,3,'STORE_STOCK_ON_ORDER',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,95,100,3,'STORE_TOTAL_INVENTORY',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,96,100,3,'SUBCLASS',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,97,100,3,'SUBCLASS_NAME',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,98,100,3,'UDA_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,99,100,3,'VPN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,100,100,3,'WKS_OF_SALES_EXP_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,101,100,3,'WKS_FIRST_SALE_VALUE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,102,100,3,'TOTAL_INVENTORY',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,103,100,3,'WAREHOUSE_STOCK_ON_HAND',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,104,100,3,'WAREHOUSE_STOCK_ON_ORDER',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,105,100,3,'WAREHOUSE_TOTAL_INVENTORY',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,106,100,3,'CONFLICT_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,107,100,3,'STATE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,108,100,3,'SALES_CHANGE_AMOUNT',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,109,100,3,'PAST_COST_CHANGE_DATE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,110,100,3,'PAST_PRICE_CHANGE_DATE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,111,100,3,'COMP_MKT_BASKET_CODE',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,112,100,3,'MARGIN_MBC_NAME',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,113,100,3,'COMP_MKT_BASKET_CODE_NAME',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,114,100,3,'PROMO_BOOLEAN',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,115,100,3,'IGNORE_CONSTRAINT_HANDLING',null,0 FROM dual;

INSERT INTO rpm_worksheet_column
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,116,100,3,'ZONE_GROUP_NAME',null,0 FROM dual;

INSERT INTO rpm_worksheet_column 
(worksheet_column_id, worksheet_column_map, default_width, corporate_display_code, column_name, max_column_name, lock_version)
SELECT rpm_worksheet_column_seq.nextval,117,100,3,'ZONE_NAME',null,0 FROM dual;

COMMIT;