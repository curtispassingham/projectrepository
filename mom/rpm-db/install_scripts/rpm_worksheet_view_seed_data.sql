
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- INSERT rpm_worksheet_view

INSERT INTO rpm_worksheet_view (worksheet_view_id, view_name, corp_ind, user_id, lock_version)
SELECT rpm_worksheet_view_seq.nextval, 'CORPORATE', 1, null, 0 FROM dual;

COMMIT;