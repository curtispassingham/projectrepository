
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (1, 'RPM_CC_PROMO_CONSTRAINT.VALIDATE',
             'Promotion Constraint check', 'Y', 'Y', 1,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (2, 'RPM_CC_NEG_RETAIL.VALIDATE',
             'negative retail check', 'Y', 'N', 2,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (3, 'RPM_CC_CLEAR_LT_REG.VALIDATE',
             'clearance retai less that regular retail', 'Y', 'N', 3,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (4, 'RPM_CC_CLEAR_MKDN.VALIDATE',
             'clearance markdowns must lower price', 'Y', 'N', 4,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (5, 'RPM_CC_CLEARUOM_SELLUOM.VALIDATE',
             'selling uom and clearance uom must be equal', 'Y', 'N', 5,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active,
             override_allowed, execution_order, base_generated,
             base_required, lock_version
            )
     VALUES (6, 'RPM_CC_FIXED_CLR_PROM_OVER.VALIDATE',
             'fixed price clearance and fixed price promotion overlap', 'Y',
             'N', 6, 'Y',
             'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed,
             execution_order, base_generated, base_required, lock_version
            )
     VALUES (7, 'RPM_CC_PROM_LT_CLEAR_REG.VALIDATE',
             'promotion retail must be less than clearance retail', 'Y', 'N',
             7, 'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active,
             override_allowed, execution_order, base_generated,
             base_required, lock_version
            )
     VALUES (8, 'RPM_CC_AMOUNT_OFF_UOM.VALIDATE',
             'amount off changes can not change the unit of measure', 'Y',
             'N', 8, 'Y',
             'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (9, 'RPM_CC_MULTI_UNIT_UOM.VALIDATE',
             'multi-unit retail checks', 'Y', 'N', 9,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (10, 'RPM_CC_PC_PROM_OV.VALIDATE',
             'price change-promotion overlap check', 'Y', 'N', 10,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active, override_allowed, execution_order,
             base_generated, base_required, lock_version
            )
     VALUES (11, 'RPM_CC_CL_PROM_OV.VALIDATE',
             'clearance-promotion overlap check', 'Y', 'N', 11,
             'Y', 'Y', 0
            );
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active,
             override_allowed, execution_order, base_generated,
             base_required, lock_version
            )
     VALUES (12, 'RPM_CC_PROM_COMP_CNT.VALIDATE',
             'two promotion and two components per promotion limit', 'Y',
             'N', 12, 'Y',
             'Y', 0
            );			
INSERT INTO rpm_conflict_query_control
            (conflict_query_control_id, conflict_query_function_name,
             conflict_query_desc, active,
             override_allowed, execution_order, base_generated,
             base_required, lock_version
            )
     VALUES (13, 'RPM_CC_PROMO_FIXED_PRICE.VALIDATE',
             'one fixed price promotion per day limit', 'Y',
             'N', 13, 'Y',
             'Y', 0
            );
COMMIT;

