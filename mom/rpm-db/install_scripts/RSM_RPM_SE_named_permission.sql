------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuidesCM.copy',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuidesCM.copy'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotions.componentMaintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotions.componentMaintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.applicationCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.applicationCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.clearances.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.clearances.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChangesCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChangesCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChanges.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChanges.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotions.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotions.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptionsCM.optionsEdit',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptionsCM.optionsEdit'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionsCM.componentMaintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionsCM.componentMaintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rsm.*',
       'app.rsm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rsm.*'
                      and application = 'app.rsm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructureCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructureCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionsCM.componentCreate',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionsCM.componentCreate'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructure.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructure.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionsCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionsCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuides.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuides.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.foundation',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.foundation'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptions.optionsEdit',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptions.optionsEdit'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.linkCodesCM.maintainLinkCode',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.linkCodesCM.maintainLinkCode'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionThresholdCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionThresholdCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionThresholdCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionThresholdCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructure.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructure.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuides.copy',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuides.copy'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChangesCM.autoApprove',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChangesCM.autoApprove'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.vendorFundingDefaultsCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.vendorFundingDefaultsCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.foundationCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.foundationCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructureCM.locationMove',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructureCM.locationMove'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.application',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.application'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotions.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotions.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChangesCM.areaDiffs',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChangesCM.areaDiffs'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionConstraintsCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionConstraintsCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptions.defaultsView',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptions.defaultsView'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.clearances',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.clearances'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptions.defaultsEdit',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptions.defaultsEdit'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionThreshold.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionThreshold.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.clearancesCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.clearancesCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructure.locationMove',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructure.locationMove'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.vendorFundingDefaultsCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.vendorFundingDefaultsCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.vendorFundingDefaults.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.vendorFundingDefaults.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuidesCM.link',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuidesCM.link'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.clearancesCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.clearancesCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionsCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionsCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptions.optionsView',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptions.optionsView'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuides.link',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuides.link'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.linkCodes',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.linkCodes'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotions.componentCreate',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotions.componentCreate'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotions',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotions'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionEvents.createMaintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionEvents.createMaintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChangesCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChangesCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructureCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructureCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuides.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuides.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionEventsCM.createMaintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionEventsCM.createMaintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.linkCodesCM.createLinkCode',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.linkCodesCM.createLinkCode'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructureCM.maintainPrimaryZoneGroup',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructureCM.maintainPrimaryZoneGroup'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChanges.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChanges.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuidesCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuidesCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptionsCM.optionsView',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptionsCM.optionsView'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptionsCM.defaultsEdit',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptionsCM.defaultsEdit'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.conflictCheckResultsCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.conflictCheckResultsCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.clearances.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.clearances.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceGuidesCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceGuidesCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.vendorFundingDefaults.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.vendorFundingDefaults.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.zoneStructure.maintainPrimaryZoneGroup',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.zoneStructure.maintainPrimaryZoneGroup'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceChanges',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceChanges'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.linkCodesCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.linkCodesCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.priceInquiryCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.priceInquiryCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.systemOptionsCM.defaultsView',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.systemOptionsCM.defaultsView'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionConstraintsCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionConstraintsCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.promotionThreshold.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.promotionThreshold.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.tasksCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.tasksCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.candidateRules.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.candidateRules.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.candidateRules.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.candidateRules.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.merchExtractItemDeletionsCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.merchExtractItemDeletionsCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.marketBasketCodesCM.maintainMarketBasketCode',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.marketBasketCodesCM.maintainMarketBasketCode'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategy.copy',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategy.copy'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.aggregationLevelCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.aggregationLevelCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.aggregationLevel',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.aggregationLevel'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategy.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategy.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.calendarCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.calendarCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.calendar.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.calendar.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.worksheetPricingCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.worksheetPricingCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.calendarCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.calendarCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategyCM.copy',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategyCM.copy'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.candidateRulesCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.candidateRulesCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.candidateRules.maintainRuleVariable',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.candidateRules.maintainRuleVariable'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.calendar.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.calendar.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.candidateRulesCM.maintainRuleVariable',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.candidateRulesCM.maintainRuleVariable'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.worksheetPricing',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.worksheetPricing'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategyCM.create',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategyCM.create'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.marketBasketCodesCM.createMarketBasketCode',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.marketBasketCodesCM.createMarketBasketCode'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategyCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategyCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.marketBasketCodesCM',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.marketBasketCodesCM'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategy.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategy.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.pricingStrategy',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.pricingStrategy'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.candidateRulesCM.maintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.candidateRulesCM.maintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.financeDetails.createMaintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.financeDetails.createMaintain'
                      and application = 'app.rpm');
------------------------------------------------------
------------------------------------------------------
insert into rsm_named_permission (id,
                                  name,
                                  application,
                                  is_view,
                                  is_edit,
                                  is_submit,
                                  is_approve,
                                  is_emergency)
select RSM_NAMED_PERMISSION_SEQ.NEXTVAL,
       'rpm.financeDetailsCM.createMaintain',
       'app.rpm',
       1,
       1,
       0,
       0,
       1
  from dual
 where NOT EXISTS (select 1
                     from rsm_named_permission
                    where name        = 'rpm.financeDetailsCM.createMaintain'
                      and application = 'app.rpm');