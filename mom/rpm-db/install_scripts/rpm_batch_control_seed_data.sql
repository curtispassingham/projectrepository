
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------
-- Copyright (c) 2000, Retek Inc.  All rights reserved.
-- $Workfile$
-- $Revision: 1.1 $
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--   ATTENTION: This script DOES NOT preserve data.
--
-- The customer DBA is responsible to review this script to ensure
-- data is preserved as desired.
--

-----------------------------------------------------
--       Inserting seed data into RPM_BATCH_CONTROL
-----------------------------------------------------

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PriceEventExecutionBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.PriceEventExecutionBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.LocationMove.LocationMoveBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.LocationMove.LocationMoveBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.LocationMove.PurgeLocationMovesBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.LocationMove.PurgeLocationMovesBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.MerchExtract.MerchExtractKickOffBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.MerchExtract.MerchExtractKickOffBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.WorksheetAutoApproveBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.WorksheetAutoApproveBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PromotionPurgeBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.PromotionPurgeBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.NewItemLocBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.NewItemLocBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.RegularPriceChangePublishBatch',
           1,
           10000
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.RegularPriceChangePublishBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.ClearancePriceChangePublishBatch',
           1,
           10000
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.ClearancePriceChangePublishBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PromotionPriceChangePublishBatch',
           1,
           10000
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.PromotionPriceChangePublishBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PriceEventExecutionRMSBatch',
           6,
           50
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.PriceEventExecutionRMSBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PriceChangeAreaDifferentialBatch',
           5,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.PriceChangeAreaDifferentialBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.app.bulkcc.service.BulkConflictCheckAppService',
           NULL,
           10000
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.app.bulkcc.service.BulkConflictCheckAppService'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.app.bulkcc.service.ChunkConflictCheckAppService',
           NULL,
           50000
      from dual
     where NOT EXISTS (select 1
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.app.bulkcc.service.ChunkConflictCheckAppService'))
/
insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.GenerateFutureRetailRollupBatch',
           10,
           1000
      from dual
     where NOT EXISTS (select 1
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.GenerateFutureRetailRollupBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count,
    batch_time_limit_hours)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.FutureRetailRollupBatch',
           10,
           1000,
           2
      from dual
     where NOT EXISTS (select 1
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.FutureRetailRollupBatch'))
/

insert into rpm_batch_control
   (batch_control_id,
    program_name,
    num_threads,
    thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.refreshPos.RefreshPosDataBatch',
           5,
           1000
      from dual
     where NOT EXISTS (select 1
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.refreshPos.RefreshPosDataBatch'))
/

insert into rpm_batch_control (batch_control_id,
                               program_name,
                               num_threads,
                               thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.InjectorPriceEventBatch',
           1,
           50000
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.InjectorPriceEventBatch'))
/

insert into rpm_batch_control (batch_control_id,
                               program_name,
                               num_threads,
                               thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.LocationMove.LocationMoveScheduleBatch',
           1,
           NULL
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.LocationMove.LocationMoveScheduleBatch'))
/

insert into rpm_batch_control (batch_control_id,
                               program_name,
                               num_threads,
                               batch_time_limit_hours)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.PurgeFutureRetailBatch',
           1,
           2
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.PurgeFutureRetailBatch'))
/

insert into rpm_batch_control (batch_control_id,
                               program_name,
                               num_threads,
                               thread_luw_count)
   (select RPM_BATCH_CONTROL_SEQ.NEXTVAL,
           'com.retek.rpm.batch.NewItemLocRollUpBatch',
           1,
           5000
      from dual
     where NOT EXISTS (select '1'
                         from rpm_batch_control
                        where program_name = 'com.retek.rpm.batch.NewItemLocRollUpBatch'))
/

commit;
