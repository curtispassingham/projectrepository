
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

insert into rpm_pre_process_config (pre_process_high_percent,
                                    pre_process_low_percent,
                                    pre_process_num_threads)
values(0.05,0.015,16);

commit;