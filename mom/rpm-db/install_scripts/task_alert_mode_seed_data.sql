
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
-- 0 - always
-- 1 - on failure only
-- 2 - on fatal error only
insert into TASK_ALERT_MODE (TASK_ALERT_MODE_ID) values (0);
insert into TASK_ALERT_MODE (TASK_ALERT_MODE_ID) values (1);
insert into TASK_ALERT_MODE (TASK_ALERT_MODE_ID) values (2);

commit;

