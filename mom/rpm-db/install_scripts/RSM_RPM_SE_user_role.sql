
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

insert into rsm_user_role
   (id,
    user_id,
    role_id,
    start_date_time,
    end_date_time)
values (RSM_USER_ROLE_SEQ.NEXTVAL,
        '&1',
        -1001,
        NVL(GET_VDATE, SYSDATE) - 365,
        NULL);
