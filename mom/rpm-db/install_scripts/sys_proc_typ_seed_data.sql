
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
insert into SYS_PROC_TYP (SYS_PROC_TYP_ID, PGM_CODE) values (1, 'com.retek.platform.app.taskengine.TaskEngine');

commit;
