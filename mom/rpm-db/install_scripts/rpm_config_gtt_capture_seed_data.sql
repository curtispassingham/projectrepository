insert into rpm_config_gtt_capture
   (config_gtt_capture_id,
    process_name,
    enable_gtt_capture,
    capture_start_point,
    capture_data_after_start_point,
    capture_rfr_gtt_data,
    capture_rpile_gtt_data,
    capture_cspfr_gtt_data,
    capture_clr_gtt_data,
    capture_rfr_il_expl_gtt_data)
   select RPM_CONFIG_GTT_CAPTURE_SEQ.NEXTVAL,
          'CONFLICT_CHECKING',
          'N',
          'POP_GTT',
          'N',
          'N',
          'N',
          'N',
          'N',
          'N'
     from dual
    where NOT EXISTS (select 'x'
                        from rpm_config_gtt_capture
                       where process_name = 'CONFLICT_CHECKING')
/