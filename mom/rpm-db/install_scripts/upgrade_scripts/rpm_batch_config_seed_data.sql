insert into rpm_batch_config (batch_config_id,
                              process_name,
                              key,
                              value)
  select RPM_BATCH_CONFIG_SEQ.NEXTVAL,
         'PURGE_TASK_PURGE',
         'PURGE_DAYS',
         '10'
    from dual
   where NOT EXISTS (select 'x'
                       from rpm_batch_config
                      where process_name = 'PURGE_TASK_PURGE'
                        and key          = 'PURGE_DAYS')
/

insert into rpm_batch_config (batch_config_id,
                              process_name,
                              key,
                              value)
  select RPM_BATCH_CONFIG_SEQ.NEXTVAL,
         'PURGE_TASK_PURGE',
         'COMPLETE_STATUS',
         'Y'
    from dual
   where NOT EXISTS (select 'x'
                       from rpm_batch_config
                      where process_name = 'PURGE_TASK_PURGE'
                        and key          = 'COMPLETE_STATUS')
/
