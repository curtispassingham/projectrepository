SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

update rpm_batch_control
   set batch_control_limit = 2
 where program_name = 'com.retek.rpm.batch.FutureRetailRollupBatch')
/

commit;
