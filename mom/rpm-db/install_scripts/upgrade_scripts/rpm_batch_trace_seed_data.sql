
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

-----------------------------------------------------
--       Inserting seed data into RPM_BATCH_TRACE
-----------------------------------------------------

insert into rpm_batch_trace (batch_name,
                             enable_trace)
   select 'Injector',
          'N'
     from dual
    where NOT EXISTS (select 1
                        from rpm_batch_trace
                       where batch_name = 'Injector');

insert into rpm_batch_trace (batch_name,
                             enable_trace)
   select 'LocMoveBatch',
          'N'
     from dual
    where NOT EXISTS (select 1
                        from rpm_batch_trace
                       where batch_name = 'LocMoveBatch');

insert into rpm_batch_trace (batch_name,
                             enable_trace)
   select 'LocMoveSch',
          'N'
     from dual
    where NOT EXISTS (select 1
                        from rpm_batch_trace
                       where batch_name = 'LocMoveSch');

commit; 

SET FEEDBACK ON
SET ECHO ON