--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_FRILE_GTT_DATA_CAPTURE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_FRILE_GTT_DATA_CAPTURE'
CREATE TABLE RPM_FRILE_GTT_DATA_CAPTURE
 (BULK_CC_PE_ID NUMBER(10) NOT NULL,
  PARENT_THREAD_NUMBER NUMBER(10) NOT NULL,
  THREAD_NUMBER NUMBER(10) NOT NULL,
  CHUNK_NUMBER NUMBER(10),
  DT_TIME TIMESTAMP NOT NULL,
  PRICE_EVENT_ID NUMBER(15,0) NOT NULL,
  FUTURE_RETAIL_ID NUMBER(15,0),
  ITEM VARCHAR2(25 ) NOT NULL,
  DEPT NUMBER(4,0) NOT NULL,
  CLASS NUMBER(4,0),
  SUBCLASS NUMBER(4,0),
  ZONE_NODE_TYPE NUMBER(6,0) NOT NULL,
  LOCATION NUMBER(10,0) NOT NULL,
  ACTION_DATE DATE NOT NULL,
  SELLING_RETAIL NUMBER(20,4),
  SELLING_RETAIL_CURRENCY VARCHAR2(3 ),
  SELLING_UOM VARCHAR2(4 ),
  MULTI_UNITS NUMBER(12,4),
  MULTI_UNIT_RETAIL NUMBER(20,4),
  MULTI_UNIT_RETAIL_CURRENCY VARCHAR2(3 ),
  MULTI_SELLING_UOM VARCHAR2(4 ),
  CLEAR_EXCEPTION_PARENT_ID NUMBER(15,0),
  CLEAR_RETAIL NUMBER(20,4),
  CLEAR_RETAIL_CURRENCY VARCHAR2(3 ),
  CLEAR_UOM VARCHAR2(4 ),
  SIMPLE_PROMO_RETAIL NUMBER(20,4),
  SIMPLE_PROMO_RETAIL_CURRENCY VARCHAR2(3 ),
  SIMPLE_PROMO_UOM VARCHAR2(4 ),
  COMPLEX_PROMO_UOM VARCHAR2(4 ),
  PROMO_COMP_MSG_TYPE VARCHAR2(30 ),
  PRICE_CHANGE_ID NUMBER(15,0),
  PRICE_CHANGE_DISPLAY_ID VARCHAR2(15 ),
  PC_EXCEPTION_PARENT_ID NUMBER(15,0),
  PC_CHANGE_TYPE NUMBER(1,0),
  PC_CHANGE_AMOUNT NUMBER(20,4),
  PC_CHANGE_CURRENCY VARCHAR2(3 ),
  PC_CHANGE_PERCENT NUMBER(20,4),
  PC_CHANGE_SELLING_UOM VARCHAR2(4 ),
  PC_NULL_MULTI_IND NUMBER(1,0),
  PC_MULTI_UNITS NUMBER(12,4),
  PC_MULTI_UNIT_RETAIL NUMBER(20,4),
  PC_MULTI_UNIT_RETAIL_CURRENCY VARCHAR2(3 ),
  PC_MULTI_SELLING_UOM VARCHAR2(4 ),
  PC_PRICE_GUIDE_ID NUMBER(20,0),
  PC_MSG_TYPE VARCHAR2(30 ),
  PC_SELLING_RETAIL_IND NUMBER(1,0),
  PC_MULTI_UNIT_IND NUMBER(1,0),
  CLEARANCE_ID NUMBER(15,0),
  CLEARANCE_DISPLAY_ID VARCHAR2(15 ),
  CLEAR_MKDN_INDEX NUMBER(2,0),
  CLEAR_START_IND NUMBER(1,0),
  CLEAR_CHANGE_TYPE NUMBER(1,0),
  CLEAR_CHANGE_AMOUNT NUMBER(20,4),
  CLEAR_CHANGE_CURRENCY VARCHAR2(3 ),
  CLEAR_CHANGE_PERCENT NUMBER(20,4),
  CLEAR_CHANGE_SELLING_UOM VARCHAR2(4 ),
  CLEAR_PRICE_GUIDE_ID NUMBER(20,0),
  CLEAR_MSG_TYPE VARCHAR2(30 ),
  LOC_MOVE_FROM_ZONE_ID NUMBER(10,0),
  LOC_MOVE_TO_ZONE_ID NUMBER(10,0),
  LOCATION_MOVE_ID NUMBER(10,0),
  PROMO_DTL_ID NUMBER(10,0),
  TYPE NUMBER(1,0),
  CUSTOMER_TYPE NUMBER(10,0),
  DETAIL_START_DATE DATE,
  DETAIL_END_DATE DATE,
  PROMO_RETAIL NUMBER(20,4),
  PROMO_UOM VARCHAR2(4 ),
  PROMO_ID NUMBER(10,0),
  EXCEPTION_PARENT_ID NUMBER(10,0),
  STEP_IDENTIFIER NUMBER(10,0)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_FRILE_GTT_DATA_CAPTURE is 'This table will hold data captured from the RPM_FR_ITEM_LOC_EXPL_GTT table.  New records will be created on this table when the system is configured to capture GTT data to assist with troubleshooting efforts.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.BULK_CC_PE_ID is 'The identifier of the group of events submitted to conflict checking together for the record.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PARENT_THREAD_NUMBER is 'The parent thread number, or sequence, under the BULK_CC_PE_ID.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.THREAD_NUMBER is 'The thread number under the PARENT_THREAD_NUMBER.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CHUNK_NUMBER is 'The chunk number under the THREAD_NUMBER when conflict check processing is being done through chunk logic.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.DT_TIME is 'The date and time that the data was captured at.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PRICE_EVENT_ID is 'Indicates the Price Event ID'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.FUTURE_RETAIL_ID is 'ID that uniquely identifies the future retail record'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.ITEM is 'ID that uniquely identifies the item on the future retail record. Items on the rpm_fr_item_loc_expl_gtt table are always transaction level items.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.DEPT is 'The department of the item'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLASS is 'The class of the item.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SUBCLASS is 'The subclass of the item.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.ZONE_NODE_TYPE is 'Indicates what location level was used for the price event.  Valid values are 0 - store, 1 - zone or 2 - warehouse'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.LOCATION is 'ID that uniquely identifies the location on the RPM_FUTURE_RETAIL record. This field will contain either a store or a warehouse.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.ACTION_DATE is 'The action date of the future retail event.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SELLING_RETAIL is 'This field contains the regular unit retail price in the selling unit of measure for the item/location combination on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SELLING_RETAIL_CURRENCY is 'The currency for which the selling retail is represented.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SELLING_UOM is 'This field contains the selling unit of measure for the item/location.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.MULTI_UNITS is 'This field contains the number of units associated with the multi-unit regular retail for the item/location combination on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.MULTI_UNIT_RETAIL is 'This field holds the multi-unit regular retail in the multi-unit selling unit of measure for the item/location combination on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.MULTI_UNIT_RETAIL_CURRENCY is 'The currency for which the multi-unit retail is represented.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.MULTI_SELLING_UOM is 'This field holds the selling unit of measure for the item/location regular multi-unit retail on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_EXCEPTION_PARENT_ID is 'ID that uniquely identifies the clearance that this clearance is an exception to. Only filled if this clearance is a clearance exception.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_RETAIL is 'This field contains the clearance unit retail price in the clearance unit of measure for the item/location combination on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_RETAIL_CURRENCY is 'The currency for which the clearance unit retail is represented'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_UOM is 'This field contains the selling unit of measure for the item/location clearance unit retail on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SIMPLE_PROMO_RETAIL is 'This field contains the simple promotion unit retail price in the promotion unit of measure for the item/location combination on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SIMPLE_PROMO_RETAIL_CURRENCY is 'The currency for which the promotional unit retail is represented.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.SIMPLE_PROMO_UOM is 'This field contains the selling unit of measure for the item/location simple promotion unit retail on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.COMPLEX_PROMO_UOM is 'This field contains the selling unit of measure for the item/location complex promotional retail on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PROMO_COMP_MSG_TYPE is 'The mesage type used by the RIB.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PRICE_CHANGE_ID is 'ID that uniquely identifies the price change. This field will be populated if there is either a price change occurring on the action date, or if there is a pricing event occurring on the action date that affects the regular retail for the price change on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PRICE_CHANGE_DISPLAY_ID is 'Display ID that uniquely identifies the price change. This field will be populated if there is either a price change occurring on the action date, or if there is a pricing event occurring on the action date that affects the regular retail for the price change on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_EXCEPTION_PARENT_ID is 'ID that uniquely identifies the price change that this price change is an exception to. Only filled if this price change is a price change exception.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_CHANGE_TYPE is 'The type of change being applied to the item/location on the price change. Valid values include: 0 Change By Percent; 1 Change By Amount; 2 Fixed Price; 4 Exclude;'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_CHANGE_AMOUNT is 'The amount of change applied to the item/location on the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_CHANGE_CURRENCY is 'When the value in the PC_CHANGE_TYPE field is either 1 Change by Amount, or 2 Fixed Price, this field holds the currency associated with the amount field.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_CHANGE_PERCENT is 'The percentage amount of change applied to the item/location on the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_CHANGE_SELLING_UOM is 'The selling UOM associated with the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_NULL_MULTI_IND is 'Indicates whether or not the price change is changing the multi-unit regular retail of the item/location. Valid values include: 1 price change is not changing the multi-unit retail, and 0 price change is  changing the multi-unit retail.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_MULTI_UNITS is 'This field contains the number of units associated with the multi-unit retail for the item/location combination on the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_MULTI_UNIT_RETAIL is 'This field holds the multi-unit retail in the multi-unit selling unit of measure for the item/location combination on the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_MULTI_UNIT_RETAIL_CURRENCY is 'The currency for which the multi-unit retail is represented.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_MULTI_SELLING_UOM is 'The multi-unit selling UOM associated with the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_PRICE_GUIDE_ID is 'ID that uniquely identifies the price guide attached to the price change.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_MSG_TYPE is 'The mesage type used by the RIB.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_SELLING_RETAIL_IND is 'PC Selling Retail Indicator'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PC_MULTI_UNIT_IND is 'PC Multi Unit Indicator'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEARANCE_ID is 'ID that uniquely identifies the clearance. This field will be populated if there is either a clearance occurring on the action date, or if there is a pricing event occurring on the action date that affects the clearance retail on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEARANCE_DISPLAY_ID is 'Display ID that uniquely identifies the clearance. This field will be populated if there is either a clearance occurring on the action date, or if there is a pricing event occurring on the action date that affects the clearance retail on the action date'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_MKDN_INDEX is 'The current count of clearances for the item/location activated since the last reset date. This field will be populated if there is either a new clearance event occurring on the action date, or if there is a pricing event occurring on the action date that affects the clearance retail on the action date.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_START_IND is 'This field indicates the status of the clearance markdown. Valid values include: 1 Clearance is Starting;  2 Clearance is in Progress; 3 Clearance is Ending;'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_CHANGE_TYPE is 'The type of change being applied to the item/location on the clearance. Valid values include: 0 Change By Percent; 1 Change By Amount; 2 Fixed Price; 4 Exclude;'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_CHANGE_AMOUNT is 'The amount of change applied to the item/location on the clearance. For clearance price changes with a change type of 1 Change By Amount, this is the amount that will be deducted from the current regular retail. For clearance price changes with a change time of 2 Fixed Price, this is the price of the item/location on clearance.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_CHANGE_CURRENCY is 'When the value in the CHANGE_TYPE field is either 1 Change by Amount, or 2 Fixed Price, this field holds the currency of the amount.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_CHANGE_PERCENT is 'The percentage amount of change applied to the item/location on the clearance. This field will only hold a value for clearances with a change type of 0.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_CHANGE_SELLING_UOM is 'The selling UOM associated with the clearance.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_PRICE_GUIDE_ID is 'ID that uniquely identifies the price guide attached to the clearance.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CLEAR_MSG_TYPE is 'The mesage type used by the RIB.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.LOC_MOVE_FROM_ZONE_ID is 'Unique ID of pricing zone this item/location is moving out of. Blank if this item/location is being added to a zone group structure.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.LOC_MOVE_TO_ZONE_ID is 'Unique ID of the pricing zone this item/location is moving into. Blank if this item/location is being removed from a zone group structure.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.LOCATION_MOVE_ID is 'Unique ID of a location move that this item/location is a part of on this day.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PROMO_DTL_ID is 'This column stores the promo comp detail id for the item/location combination.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.TYPE is 'The type of promotion component for the component in the PROM_COMP_ID field. Valid values include: 0 Complex; 1 Simple; 2 Threshold; 4 Transaction'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.CUSTOMER_TYPE is 'The customer type associated with the promotion component for this item and location and action_date if any.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.DETAIL_START_DATE is 'The start date of the promotion component.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.DETAIL_END_DATE is 'The end date of the promotion component.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PROMO_RETAIL is 'The promotional retail value for the item at this location on this date for the customer segment given.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PROMO_UOM is 'The UOM of the item at this location on this date for the customer segment given.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.PROMO_ID is 'ID that uniquely identifies the promotion'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.EXCEPTION_PARENT_ID is 'This is the ID of the parent that this item/location promotion record is an exception to.'
/

COMMENT ON COLUMN RPM_FRILE_GTT_DATA_CAPTURE.STEP_IDENTIFIER is 'Identifies the step in the overall processing that created or updated the record. Used for capturing GTT data.'
/

