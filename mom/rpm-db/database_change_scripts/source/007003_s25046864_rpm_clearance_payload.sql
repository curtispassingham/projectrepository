--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_CLEARANCE_PAYLOAD'

PROMPT Creating Index 'RPM_CLEARANCE_PAYLOAD_I2'
CREATE INDEX RPM_CLEARANCE_PAYLOAD_I2 on RPM_CLEARANCE_PAYLOAD
  (ITEM,
   LOCATION
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

