--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       DROPPING PACKAGE                
--------------------------------------
PROMPT DROPPING PACKAGE 'RPM_TO_ORPOS_PRICE_PUBLISH_SQL';
DECLARE
  L_package_exists number := 0;
BEGIN
  SELECT count(*) INTO L_package_exists
    FROM USER_OBJECTS
    WHERE OBJECT_NAME='RPM_TO_ORPOS_PRICE_PUBLISH_SQL';
 
  if (L_package_exists != 0) then
      execute immediate 'DROP PACKAGE RPM_TO_ORPOS_PRICE_PUBLISH_SQL';
  end if;
end;
/










