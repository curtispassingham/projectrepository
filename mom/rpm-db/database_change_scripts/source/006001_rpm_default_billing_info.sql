--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_DEFAULT_BILLING_INFO'
COMMENT ON COLUMN RPM_DEFAULT_BILLING_INFO.INVOICE_PROC_LOGIC_CODE IS 'The code from the ''CDIP'' code type that identifies the type of deal invoice processing for this default billing information. Valid values only include: aa automatic; ma manual; no no invoice processing - ap and mp values are removed from the list before the LOV is displayed.'
/
