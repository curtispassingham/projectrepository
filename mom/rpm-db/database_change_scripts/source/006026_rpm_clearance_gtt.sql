--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_CLEARANCE_GTT'
ALTER TABLE RPM_CLEARANCE_GTT ADD STEP_IDENTIFIER NUMBER (10) NULL
/

COMMENT ON COLUMN RPM_CLEARANCE_GTT.STEP_IDENTIFIER is 'Identifies the step in the overall processing that created or updated the record. Used for capturing GTT data.'
/

