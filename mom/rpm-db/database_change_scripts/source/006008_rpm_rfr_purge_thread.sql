--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_RFR_PURGE_THREAD
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_RFR_PURGE_THREAD'
CREATE TABLE RPM_RFR_PURGE_THREAD
 (DEPT NUMBER(4) NOT NULL,
  CLASS NUMBER(4) NOT NULL,
  SUBCLASS NUMBER(4) NOT NULL,
  THREAD_NUMBER NUMBER(10) NOT NULL,
  STATUS VARCHAR2(5) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_RFR_PURGE_THREAD is 'This table is used for thread assignments for the future retail purge batch process.  Each dept/class/subclass combination in the system will be assigned a thread value with each run of the associated batch process.'
/

COMMENT ON COLUMN RPM_RFR_PURGE_THREAD.DEPT is 'The department id for the record'
/

COMMENT ON COLUMN RPM_RFR_PURGE_THREAD.CLASS is 'The class id for the record'
/

COMMENT ON COLUMN RPM_RFR_PURGE_THREAD.SUBCLASS is 'The subclass id for the record'
/

COMMENT ON COLUMN RPM_RFR_PURGE_THREAD.THREAD_NUMBER is 'The assigned thread number for the record'
/

COMMENT ON COLUMN RPM_RFR_PURGE_THREAD.STATUS is 'The status of the thread for the record.  Valid values are ''N'' (New), ''IP'' (In Progress), ''S'' (Skip), ''C'' (Complete).'
/

