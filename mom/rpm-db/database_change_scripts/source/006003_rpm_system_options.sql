--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_SYSTEM_OPTIONS'
COMMENT ON COLUMN RPM_SYSTEM_OPTIONS.CLEARANCE_PROMO_OVERLAP_IND is 'System Option to indicate whether or not an item/location can have an overlapping promotion and clearance.  If the indicator is set to unchecked (value of 0 on the table), promotion/clearance overlap is not allowed. If the indicator is set to checked (value of 1 on the table), promotion/clearance overlap is allowed.'
/

COMMENT ON COLUMN RPM_SYSTEM_OPTIONS.COMPLEX_PROMO_ALLOWED_IND is 'System option to indicate whether or not complex promotion components can be created. If the indicator is set to checked (value of 1 on the table) then all promotion component types are available. If the indicator is set to unchecked (value of 0 on the table), then only simple promotion components can be defined; all other complex promotion component types including Threshold, Multibuy and Transaction components will not be available in the promotion component dialog.'
/

