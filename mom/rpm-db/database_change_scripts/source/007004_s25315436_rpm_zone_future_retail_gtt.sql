--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_ZONE_FUTURE_RETAIL_GTT'
ALTER TABLE RPM_ZONE_FUTURE_RETAIL_GTT MODIFY PC_CHANGE_TYPE NUMBER (2,0)
/

COMMENT ON COLUMN RPM_ZONE_FUTURE_RETAIL_GTT.PC_CHANGE_TYPE is ' pc change type'
/

