--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_MERCH_LIST_DETAIL'

PROMPT Creating Index 'RPM_MERCH_LIST_DETAIL_I3'
CREATE INDEX RPM_MERCH_LIST_DETAIL_I3 on RPM_MERCH_LIST_DETAIL
  (MERCH_LIST_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

