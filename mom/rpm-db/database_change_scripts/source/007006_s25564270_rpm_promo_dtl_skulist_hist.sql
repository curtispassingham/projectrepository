--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_PROMO_DTL_SKULIST_HIST'

PROMPT Creating Index 'RPM_PROMO_DTL_SKULIST_HIST_I1'
CREATE INDEX RPM_PROMO_DTL_SKULIST_HIST_I1 on RPM_PROMO_DTL_SKULIST_HIST
  (PRICE_EVENT_ID
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

