--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_BATCH_CONTROL'
ALTER TABLE RPM_BATCH_CONTROL ADD BATCH_TIME_LIMIT_HOURS NUMBER (4,2) NULL
/

COMMENT ON COLUMN RPM_BATCH_CONTROL.BATCH_TIME_LIMIT_HOURS is 'The maximum number of hours that the batch should run.  Only used by batch processes that are designed to run for set time periods.  Batch processes might run beyond this limitation, but new threads are not initiated once the timeframe has been exceeded.'
/

