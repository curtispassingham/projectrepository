--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_RFR_ROLLUP_THREAD
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_RFR_ROLLUP_THREAD'
CREATE TABLE RPM_RFR_ROLLUP_THREAD
 (DEPT NUMBER(4) NOT NULL,
  CLASS NUMBER(4) NOT NULL,
  SUBCLASS NUMBER(4) NOT NULL,
  ITEM_ID VARCHAR2(25 ) NOT NULL,
  ITEM_TYPE NUMBER(1) NOT NULL,
  THREAD_NUMBER NUMBER(10) NOT NULL,
  STATUS VARCHAR2(5) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_RFR_ROLLUP_THREAD is 'This table is used for thread assignments for the future retail rollup batch process.  Each parent item and staple item in the system will be assigned a thread value with each run of the associated batch process.'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.DEPT is 'The department id for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.CLASS is 'The class id for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.SUBCLASS is 'The subclass id for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.ITEM_ID is 'The item id for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.ITEM_TYPE is 'The type of item for the record.  Valid values are 1 (Parent Item) and 2 (Transaction Item).'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.THREAD_NUMBER is 'The assigned thread number for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_THREAD.STATUS is 'The status of the thread for the record.  Valid values are ''N'' (New), ''IP'' (In Progress), ''S'' (Skip), ''C'' (Complete).'
/

