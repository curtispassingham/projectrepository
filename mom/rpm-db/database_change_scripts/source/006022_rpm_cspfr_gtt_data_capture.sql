--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_CSPFR_GTT_DATA_CAPTURE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_CSPFR_GTT_DATA_CAPTURE'
CREATE TABLE RPM_CSPFR_GTT_DATA_CAPTURE
 (BULK_CC_PE_ID NUMBER(10) NOT NULL,
  PARENT_THREAD_NUMBER NUMBER(10) NOT NULL,
  THREAD_NUMBER NUMBER(10) NOT NULL,
  CHUNK_NUMBER NUMBER(10),
  DT_TIME TIMESTAMP NOT NULL,
  PRICE_EVENT_ID NUMBER(15) NOT NULL,
  CUST_SEGMENT_PROMO_ID NUMBER(15) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  ZONE_NODE_TYPE NUMBER(6) NOT NULL,
  LOCATION NUMBER(10) NOT NULL,
  ACTION_DATE DATE NOT NULL,
  CUSTOMER_TYPE NUMBER(10) NOT NULL,
  DEPT NUMBER(4) NOT NULL,
  PROMO_RETAIL NUMBER(20,4),
  PROMO_RETAIL_CURRENCY VARCHAR2(3 ),
  PROMO_UOM VARCHAR2(4 ),
  COMPLEX_PROMO_IND NUMBER(1) NOT NULL,
  PROMO_COMP_MSG_TYPE VARCHAR2(30 ),
  CSPFR_ROWID ROWID,
  ZONE_ID NUMBER(10),
  ITEM_PARENT VARCHAR2(25 ),
  DIFF_ID VARCHAR2(10 ),
  MAX_HIER_LEVEL VARCHAR2(2 ),
  CUR_HIER_LEVEL VARCHAR2(2 ),
  ORIGINAL_CS_PROMO_ID NUMBER(15),
  STEP_IDENTIFIER NUMBER(10)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_CSPFR_GTT_DATA_CAPTURE is 'This table will hold data captured from the RPM_CUST_SEGMENT_PROMO_FR_GTT table.  New records will be created on this table when the system is configured to capture GTT data to assist with troubleshooting efforts.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.BULK_CC_PE_ID is 'The identifier of the group of events submitted to conflict checking together for the record.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.PARENT_THREAD_NUMBER is 'The parent thread number, or sequence, under the BULK_CC_PE_ID.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.THREAD_NUMBER is 'The thread number under the PARENT_THREAD_NUMBER.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.CHUNK_NUMBER is 'The chunk number under the THREAD_NUMBER when conflict check processing is being done through chunk logic.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.DT_TIME is 'The date and time that the data was captured at.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.PRICE_EVENT_ID is 'the id of the price event being processed by the conflict check process.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.CUST_SEGMENT_PROMO_ID is 'the id that uniquely identifies the record on rpm_cust_segment_promo_fr'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.ITEM is 'the item of the rpm_cust_segment_promo_fr record.  these items are always transaction level items.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.ZONE_NODE_TYPE is 'indicates the type of location in the customer segment promotion future retail record.  a value of 0 indicates store, 2 indicates warehouse.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.LOCATION is 'the location of the rpm_cust_segment_promo_fr record.  this location value will be either a store or a warehouse.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.ACTION_DATE is 'action date of the customer segment promotion record.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.CUSTOMER_TYPE is 'the customer type associated with the customer segment promotion for this item/location/action date.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.DEPT is 'the dept of the item for this rpm_cust_segment_promo_fr record.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.PROMO_RETAIL is 'the promotional retail value for the item at this location on this date for the customer segment given.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.PROMO_RETAIL_CURRENCY is 'the currency of the promotional retail for the item at this location on this date for the customer segment given.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.PROMO_UOM is 'the uom of the item at this location on this date for the customer segment given.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.COMPLEX_PROMO_IND is 'indicates if this customer segment promotion is a complex promotion.  defaults to 0 based on the default value from RPM_CUST_SEGEMENT_PROMO_FR_GTT.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.CSPFR_ROWID is 'the rowid value of the corresponding record on the rpm_cust_segment_promo_fr table.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.ZONE_ID is 'The zone id of the location when the location is a store or warehouse'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.ITEM_PARENT is 'The item parent of the item when the item is a transaction level item'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.DIFF_ID is 'The diff id of the item parent that the transaction level item falls under'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.MAX_HIER_LEVEL is 'The maximum hierarchy timeline level that this record could be rolled up to.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.CUR_HIER_LEVEL is 'The current hierarchy timeline level that this record is part of.'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.ORIGINAL_CS_PROMO_ID is 'The original customer segment promo fr id that this record was generated from'
/

COMMENT ON COLUMN RPM_CSPFR_GTT_DATA_CAPTURE.STEP_IDENTIFIER is 'Identifies the step in the overall processing that created or updated the record. Used for capturing GTT data.'
/

