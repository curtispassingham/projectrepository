--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_RPILE_GTT_DATA_CAPTURE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_RPILE_GTT_DATA_CAPTURE'
CREATE TABLE RPM_RPILE_GTT_DATA_CAPTURE
 (BULK_CC_PE_ID NUMBER(10) NOT NULL,
  PARENT_THREAD_NUMBER NUMBER(10) NOT NULL,
  THREAD_NUMBER NUMBER(10) NOT NULL,
  CHUNK_NUMBER NUMBER(10),
  DT_TIME TIMESTAMP NOT NULL,
  PRICE_EVENT_ID NUMBER(15) NOT NULL,
  PROMO_ITEM_LOC_EXPL_ID NUMBER(15) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  DEPT NUMBER(4) NOT NULL,
  CLASS NUMBER(4) NOT NULL,
  SUBCLASS NUMBER(4) NOT NULL,
  LOCATION NUMBER(10) NOT NULL,
  PROMO_ID NUMBER(10),
  PROMO_DISPLAY_ID VARCHAR2(10 ),
  PROMO_SECONDARY_IND NUMBER(1),
  PROMO_COMP_ID NUMBER(10),
  COMP_DISPLAY_ID VARCHAR2(10 ),
  PROMO_DTL_ID NUMBER(10),
  TYPE NUMBER(1),
  CUSTOMER_TYPE NUMBER(10),
  DETAIL_SECONDARY_IND NUMBER(1),
  DETAIL_START_DATE DATE,
  DETAIL_END_DATE DATE,
  DETAIL_APPLY_TO_CODE NUMBER(1),
  DETAIL_CHANGE_TYPE NUMBER(1),
  DETAIL_CHANGE_AMOUNT NUMBER(20,4),
  DETAIL_CHANGE_CURRENCY VARCHAR2(3 ),
  DETAIL_CHANGE_PERCENT NUMBER(20,4),
  DETAIL_CHANGE_SELLING_UOM VARCHAR2(4 ),
  DETAIL_PRICE_GUIDE_ID NUMBER(20),
  EXCEPTION_PARENT_ID NUMBER(10),
  PROMO_COMP_MSG_TYPE VARCHAR2(30 ),
  DELETED_IND NUMBER(1),
  RPILE_ROWID ROWID,
  ZONE_NODE_TYPE NUMBER(1),
  ZONE_ID NUMBER(10),
  ITEM_PARENT VARCHAR2(25 ),
  DIFF_ID VARCHAR2(10 ),
  MAX_HIER_LEVEL VARCHAR2(2 ),
  CUR_HIER_LEVEL VARCHAR2(2 ),
  ORIGINAL_PILE_ID NUMBER(15),
  TIMEBASED_DTL_IND NUMBER(1),
  STEP_IDENTIFIER NUMBER(10)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_RPILE_GTT_DATA_CAPTURE is 'This table will hold data captured from the RPM_PROMO_ITEM_LOC_EXPL_GTT table.  New records will be created on this table when the system is configured to capture GTT data to assist with troubleshooting efforts.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.BULK_CC_PE_ID is 'The identifier of the group of events submitted to conflict checking together for the record.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PARENT_THREAD_NUMBER is 'The parent thread number, or sequence, under the BULK_CC_PE_ID.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.THREAD_NUMBER is 'The thread number under the PARENT_THREAD_NUMBER.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.CHUNK_NUMBER is 'The chunk number under the THREAD_NUMBER when conflict check processing is being done through chunk logic.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DT_TIME is 'The date and time that the data was captured at.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PRICE_EVENT_ID is 'unique id for the price event being processed.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_ITEM_LOC_EXPL_ID is 'unique id for the promotion item/location explotion record.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.ITEM is 'id that uniquely identifies the item on the future retail record. items here are always transaction level items.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DEPT is 'the department of the item.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.CLASS is 'the class of the item.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.SUBCLASS is 'the subclass of the item.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.LOCATION is 'id that uniquely identifies the location. this field will contain either a store or a warehouse.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_ID is 'id that uniquely identifies the promotion.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_DISPLAY_ID is 'id that uniquely identifies the promotion.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_SECONDARY_IND is 'boolean field that indicates if the promotion component should be considered secondary to any other promotion components for this promotion on the same day if value is true. valid values include: 0 false 1 true'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_COMP_ID is 'id that uniquely identifies the first promotion component associated with the promotion in the promo_id field.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.COMP_DISPLAY_ID is 'display id that uniquely identifies the promotion component associated with the promotion in the promo_id field.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_DTL_ID is 'id that uniquely identifies the promotion component detail associated with the promotion in the promo_id field and the component in the promo_comp_id field.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.TYPE is 'the type of promotion component for the component in the promo_comp_id field. valid values include:  0 complex;  1 simple; 2 threshold; 4 Transaction'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.CUSTOMER_TYPE is 'the customer type associated with the promotion component.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_SECONDARY_IND is 'boolean field that indicates if the promotion component in the promo_comp_id field should be considered secondary to any other promotion components for this promotion on the same day if value is true. valid values include: 0 false 1 true'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_START_DATE is 'the start date of the promotion component'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_END_DATE is 'the end date of the promotion component'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_APPLY_TO_CODE is 'field that indicates if the promotion component in the promo_comp_id field should apply to just regular retails, clearance retails, or both regular and clearance retails. valid values include:  0 regular only;  1 clearance only 2 both regular and clearance.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_CHANGE_TYPE is 'the type of change being applied to the item/location on the promotion component in the promo_comp_id field. valid values include: 0  change by percent; 1 change by amount; 2 fixed price; 4 exclude;'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_CHANGE_AMOUNT is 'the amount of change applied to the item/location on the promotion component inthe promo_comp_id field. only used if detail_change_type = 1 or 2.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_CHANGE_CURRENCY is 'when the value in the detail_change_type field is either 1 change by amount, or 2 fixed price, this field holds the currency of the amount.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_CHANGE_PERCENT is 'the percentage amount of change applied to the item/location on the promotion component in the promo_comp_id field. this field will only hold a value for promotion components with a change type of 0 change by percent.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_CHANGE_SELLING_UOM is 'the selling uom associated with the promotion component in the promo_comp_id field. only used if detail_change_type = 0'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DETAIL_PRICE_GUIDE_ID is 'id that uniquely identifies the price guide attached to the promotion component in the promo_comp_id field.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.EXCEPTION_PARENT_ID is 'this is the id of the parent that this record is an exception to.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.PROMO_COMP_MSG_TYPE is 'the mesage type used by the rib.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DELETED_IND is 'deleted indicator'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.RPILE_ROWID is 'row id of the corresponding rpm_promo_item_loc_expl record.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.ZONE_NODE_TYPE is 'The zone node type for the record.  Valid values are 0 - Store; 1 - Zone; 2- Warehouse'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.ZONE_ID is 'The zone id of the location when the location is a store or warehouse'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.ITEM_PARENT is 'The item parent of the item when the item is a transaction level item'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.DIFF_ID is 'The diff id of the item parent that the transaction level item falls under'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.MAX_HIER_LEVEL is 'he maximum hierarchy timeline level that this record could be rolled up to.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.CUR_HIER_LEVEL is 'The current hierarchy timeline level that this record is part of.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.ORIGINAL_PILE_ID is 'The original promo item loc expl id that this record was generated from'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.TIMEBASED_DTL_IND is 'If the TIMEBASED_DTL_IND is set to 0, the time portion of the START_DATE needs to be populated with 00:00:00 and the time portion of the END_DATE (if NOT NULL) needs to be populated with 23:59:00.'
/

COMMENT ON COLUMN RPM_RPILE_GTT_DATA_CAPTURE.STEP_IDENTIFIER is 'Identifies the step in the overall processing that created or updated the record. Used for capturing GTT data.'
/

