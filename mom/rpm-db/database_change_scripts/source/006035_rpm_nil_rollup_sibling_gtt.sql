--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_NIL_ROLLUP_SIBLING_GTT'
ALTER TABLE RPM_NIL_ROLLUP_SIBLING_GTT MODIFY ZONE_ID NUMBER (10) NULL
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.ZONE_ID is 'The zone id for the record if the node type is zone'
/

