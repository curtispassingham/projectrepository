--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_LOC_MOVE_PROMO_COMP_DTL_EX'

PROMPT Creating Foreign Key on 'RPM_LOC_MOVE_PROMO_COMP_DTL_EX'
ALTER TABLE RPM_LOC_MOVE_PROMO_COMP_DTL_EX
 ADD CONSTRAINT RLMPCDE_RPD_FK FOREIGN KEY (RPM_PROMO_COMP_DETAIL_ID)
 REFERENCES RPM_PROMO_DTL(PROMO_DTL_ID)
/
