--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_NIL_ROLLUP_THREAD'
ALTER TABLE RPM_NIL_ROLLUP_THREAD RENAME COLUMN THEAD_NUMBER to THREAD_NUMBER
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.THREAD_NUMBER is 'The thread number of the record'
/

