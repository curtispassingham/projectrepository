--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_BATCH_BOOKMARK
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_BATCH_BOOKMARK'
CREATE TABLE RPM_BATCH_BOOKMARK
 (BATCH_BOOKMARK_ID NUMBER(10) NOT NULL,
  BATCH_NAME VARCHAR2(300 ) NOT NULL,
  BOOKMARK_VALUE VARCHAR2(50 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_BATCH_BOOKMARK is 'This table holds bookmark data used by batch processes that run for specified time limits and process data in a round robin fashion.'
/

COMMENT ON COLUMN RPM_BATCH_BOOKMARK.BATCH_BOOKMARK_ID is 'The unique id for the record.'
/

COMMENT ON COLUMN RPM_BATCH_BOOKMARK.BATCH_NAME is 'The name of the batch process that uses this bookmark record.'
/

COMMENT ON COLUMN RPM_BATCH_BOOKMARK.BOOKMARK_VALUE is 'The bookmark value for the batch process.  This is the last value for the LUW processed.  If NULL, the batch either has not been run before or was reset to force the batch to start at the top of the round robin data processing.'
/

