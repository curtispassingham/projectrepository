--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_CLEARANCE'

PROMPT Creating Index 'RPM_CLEARANCE_I12'
CREATE INDEX RPM_CLEARANCE_I12 on RPM_CLEARANCE
  (PRICE_EVENT_ITEMLIST
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

