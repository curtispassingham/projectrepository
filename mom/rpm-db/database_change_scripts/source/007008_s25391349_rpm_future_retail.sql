--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_FUTURE_RETAIL'

PROMPT Creating Index 'RPM_FUTURE_RETAIL_I5'
CREATE INDEX RPM_FUTURE_RETAIL_I5 on RPM_FUTURE_RETAIL
  (ITEM_PARENT
 )
 LOCAL
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

