--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_PROMO_ITEM_LOC_SR_PAYLOAD'
ALTER TABLE RPM_PROMO_ITEM_LOC_SR_PAYLOAD ADD REF_PROMO_DTL_ID NUMBER (15) NULL
/

COMMENT ON COLUMN RPM_PROMO_ITEM_LOC_SR_PAYLOAD.REF_PROMO_DTL_ID is 'Referenced Promotion Detail Id'
/

