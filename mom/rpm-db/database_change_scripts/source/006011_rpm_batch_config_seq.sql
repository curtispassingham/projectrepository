--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_BATCH_CONFIG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       CREATING SEQUENCE               
--------------------------------------
PROMPT CREATING SEQUENCE 'rpm_batch_config_seq'
create sequence rpm_batch_config_seq   
   INCREMENT BY 1
   START WITH 1
   MAXVALUE 9999999999
   MINVALUE 1
   CACHE 20
   CYCLE
   NOORDER
/
