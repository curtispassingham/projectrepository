--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_BATCH_CONFIG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_BATCH_CONFIG'
CREATE TABLE RPM_BATCH_CONFIG
 (BATCH_CONFIG_ID NUMBER(10) NOT NULL,
  PROCESS_NAME VARCHAR2(300 ) NOT NULL,
  KEY VARCHAR2(250 ) NOT NULL,
  VALUE VARCHAR2(250 )
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_BATCH_CONFIG is 'This table holds configurations for specific RPM batch processes using a key-value pairing.'
/

COMMENT ON COLUMN RPM_BATCH_CONFIG.BATCH_CONFIG_ID is 'Unique ID for the record'
/

COMMENT ON COLUMN RPM_BATCH_CONFIG.PROCESS_NAME is 'String to identify the process/functionality for the record'
/

COMMENT ON COLUMN RPM_BATCH_CONFIG.KEY is 'The key for the key-value pairing'
/

COMMENT ON COLUMN RPM_BATCH_CONFIG.VALUE is 'The value for the key-value pairing'
/

