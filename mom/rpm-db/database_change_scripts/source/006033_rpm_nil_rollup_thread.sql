--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_NIL_ROLLUP_THREAD
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_NIL_ROLLUP_THREAD'
CREATE TABLE RPM_NIL_ROLLUP_THREAD
 (DEPT NUMBER(4) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  LOCATION NUMBER(10) NOT NULL,
  ITEM_LEVEL NUMBER(1) NOT NULL,
  ZONE_NODE_TYPE NUMBER(1) NOT NULL,
  THEAD_NUMBER NUMBER(10) NOT NULL,
  START_DATE DATE,
  STATUS VARCHAR2(1 ) NOT NULL,
  PROCESS_ID NUMBER(15) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_NIL_ROLLUP_THREAD is 'Staging table for ROLL UP process'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.DEPT is 'The department id for the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.ITEM is 'The item id for the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.LOCATION is 'The location id for the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.ITEM_LEVEL is 'The item level - values can be 1 for transaction level item or 2 for parente level item'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.ZONE_NODE_TYPE is 'The zone node type for the location'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.THEAD_NUMBER is 'The thread number of the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.START_DATE is 'The date when the rollup process has started for the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.STATUS is 'The rollup status of the record - can be ''N''ew, ''S''tarted, ''R''estarted, ''E''rror or ''C''ompleted'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_THREAD.PROCESS_ID is 'The process id for New Item Loc Batch'
/

