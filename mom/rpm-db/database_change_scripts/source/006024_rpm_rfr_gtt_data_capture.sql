--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_RFR_GTT_DATA_CAPTURE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_RFR_GTT_DATA_CAPTURE'
CREATE TABLE RPM_RFR_GTT_DATA_CAPTURE
 (BULK_CC_PE_ID NUMBER(10) NOT NULL,
  PARENT_THREAD_NUMBER NUMBER(10) NOT NULL,
  THREAD_NUMBER NUMBER(10) NOT NULL,
  CHUNK_NUMBER NUMBER(10),
  DT_TIME TIMESTAMP NOT NULL,
  PRICE_EVENT_ID NUMBER(15) NOT NULL,
  FUTURE_RETAIL_ID NUMBER(15) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  DEPT NUMBER(4) NOT NULL,
  CLASS NUMBER(4) NOT NULL,
  SUBCLASS NUMBER(4) NOT NULL,
  ZONE_NODE_TYPE NUMBER(6) NOT NULL,
  LOCATION NUMBER(10) NOT NULL,
  ACTION_DATE DATE NOT NULL,
  SELLING_RETAIL NUMBER(20,4) NOT NULL,
  SELLING_RETAIL_CURRENCY VARCHAR2(3 ) NOT NULL,
  SELLING_UOM VARCHAR2(4 ) NOT NULL,
  MULTI_UNITS NUMBER(12,4),
  MULTI_UNIT_RETAIL NUMBER(20,4),
  MULTI_UNIT_RETAIL_CURRENCY VARCHAR2(3 ),
  MULTI_SELLING_UOM VARCHAR2(4 ),
  CLEAR_EXCEPTION_PARENT_ID NUMBER(15),
  CLEAR_RETAIL NUMBER(20,4),
  CLEAR_RETAIL_CURRENCY VARCHAR2(3 ),
  CLEAR_UOM VARCHAR2(4 ),
  SIMPLE_PROMO_RETAIL NUMBER(20,4),
  SIMPLE_PROMO_RETAIL_CURRENCY VARCHAR2(3 ),
  SIMPLE_PROMO_UOM VARCHAR2(4 ),
  ON_SIMPLE_PROMO_IND NUMBER(1) NOT NULL,
  ON_COMPLEX_PROMO_IND NUMBER(1) NOT NULL,
  PROMO_COMP_MSG_TYPE VARCHAR2(30 ),
  PRICE_CHANGE_ID NUMBER(15),
  PRICE_CHANGE_DISPLAY_ID VARCHAR2(15 ),
  PC_EXCEPTION_PARENT_ID NUMBER(15),
  PC_CHANGE_TYPE NUMBER(1),
  PC_CHANGE_AMOUNT NUMBER(20,4),
  PC_CHANGE_CURRENCY VARCHAR2(3 ),
  PC_CHANGE_PERCENT NUMBER(20,4),
  PC_CHANGE_SELLING_UOM VARCHAR2(4 ),
  PC_NULL_MULTI_IND NUMBER(1),
  PC_MULTI_UNITS NUMBER(12,4),
  PC_MULTI_UNIT_RETAIL NUMBER(20,4),
  PC_MULTI_UNIT_RETAIL_CURRENCY VARCHAR2(3 ),
  PC_MULTI_SELLING_UOM VARCHAR2(4 ),
  PC_PRICE_GUIDE_ID NUMBER(20),
  PC_MSG_TYPE VARCHAR2(30 ),
  PC_SELLING_RETAIL_IND NUMBER(1),
  PC_MULTI_UNIT_IND NUMBER(1),
  CLEARANCE_ID NUMBER(15),
  CLEARANCE_DISPLAY_ID VARCHAR2(15 ),
  CLEAR_MKDN_INDEX NUMBER(2),
  CLEAR_START_IND NUMBER(1),
  CLEAR_CHANGE_TYPE NUMBER(1),
  CLEAR_CHANGE_AMOUNT NUMBER(20,4),
  CLEAR_CHANGE_CURRENCY VARCHAR2(3 ),
  CLEAR_CHANGE_PERCENT NUMBER(20,4),
  CLEAR_CHANGE_SELLING_UOM VARCHAR2(4 ),
  CLEAR_PRICE_GUIDE_ID NUMBER(20),
  CLEAR_MSG_TYPE VARCHAR2(30 ),
  LOC_MOVE_FROM_ZONE_ID NUMBER(10),
  LOC_MOVE_TO_ZONE_ID NUMBER(10),
  LOCATION_MOVE_ID NUMBER(10),
  LOCK_VERSION NUMBER(18),
  TIMELINE_SEQ NUMBER(15),
  RFR_ROWID ROWID,
  ITEM_PARENT VARCHAR2(25 ),
  DIFF_ID VARCHAR2(10 ),
  ZONE_ID NUMBER(10),
  MAX_HIER_LEVEL VARCHAR2(2 ),
  CUR_HIER_LEVEL VARCHAR2(2 ),
  ORIGINAL_FR_ID NUMBER(15),
  STEP_IDENTIFIER NUMBER(10)
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_RFR_GTT_DATA_CAPTURE is 'This table will hold data captured from the RPM_FUTURE_RETAIL_GTT table.  New records will be created on this table when the system is configured to capture GTT data to assist with troubleshooting efforts.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.BULK_CC_PE_ID is 'The identifier of the group of events submitted to conflict checking together for the record.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.PARENT_THREAD_NUMBER is 'The parent thread number, or sequence, under the BULK_CC_PE_ID.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.THREAD_NUMBER is 'The thread number under the PARENT_THREAD_NUMBER.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.CHUNK_NUMBER is 'The chunk number under the THREAD_NUMBER when conflict check processing is being done through chunk logic.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.DT_TIME is 'The date and time that the data was captured at.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.PROMO_COMP_MSG_TYPE is 'the mesage type used by the rib.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.ITEM_PARENT is 'The item parent of the item when the item is a transaction level item'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.DIFF_ID is 'The diff id of the item parent that the transaction level item falls under'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.ZONE_ID is 'The zone id of the location when the location is a store or warehouse'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.MAX_HIER_LEVEL is 'The maximum hierarchy timeline level that this record could be rolled up to.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.CUR_HIER_LEVEL is 'The current hierarchy timeline level that this record is part of.'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.ORIGINAL_FR_ID is 'The original future retail id that this record was generated from'
/

COMMENT ON COLUMN RPM_RFR_GTT_DATA_CAPTURE.STEP_IDENTIFIER is 'Identifies the step in the overall processing that created or updated the record. Used for capturing GTT data.'
/

