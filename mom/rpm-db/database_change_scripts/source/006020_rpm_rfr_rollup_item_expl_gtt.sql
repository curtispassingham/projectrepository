--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_RFR_ROLLUP_ITEM_EXPL_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_RFR_ROLLUP_ITEM_EXPL_GTT'
CREATE GLOBAL TEMPORARY TABLE RPM_RFR_ROLLUP_ITEM_EXPL_GTT
 (DEPT NUMBER(4) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  ITEM_LEVEL NUMBER(1) NOT NULL
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE RPM_RFR_ROLLUP_ITEM_EXPL_GTT is 'This temporary table will hold exploded item data for the future retail rollup batch process.  Each thread in the batch process will create data on this table to assist with populating the three future retail GTT tables.'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_ITEM_EXPL_GTT.DEPT is 'The department id for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_ITEM_EXPL_GTT.ITEM is 'The item id for the record'
/

COMMENT ON COLUMN RPM_RFR_ROLLUP_ITEM_EXPL_GTT.ITEM_LEVEL is 'The type of item for the record.  Valid values are 1 (Parent Item) and 2 (Transaction Item).'
/

