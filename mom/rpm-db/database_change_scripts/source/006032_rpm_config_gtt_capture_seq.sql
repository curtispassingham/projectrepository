--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	SEQUENCE ADDED:				RPM_CONFIG_GTT_CAPTURE_SEQ
----------------------------------------------------------------------------

whenever sqlerror exit failure

--------------------------------------
--       ADDING SEQUENCE
--------------------------------------
PROMPT Creating Sequence 'RPM_CONFIG_GTT_CAPTURE_SEQ'
CREATE SEQUENCE RPM_CONFIG_GTT_CAPTURE_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999
  MINVALUE 1
  CACHE 20
  CYCLE 
  NOORDER
/