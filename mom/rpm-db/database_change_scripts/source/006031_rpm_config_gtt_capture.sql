--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_CONFIG_GTT_CAPTURE
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_CONFIG_GTT_CAPTURE'
CREATE TABLE RPM_CONFIG_GTT_CAPTURE
 (CONFIG_GTT_CAPTURE_ID NUMBER(10) NOT NULL,
  PROCESS_NAME VARCHAR2(250 ) NOT NULL,
  ENABLE_GTT_CAPTURE VARCHAR2(1 ) NOT NULL,
  USER_ID VARCHAR2(30 ),
  CAPTURE_START_POINT VARCHAR2(30 ) NOT NULL,
  CAPTURE_DATA_AFTER_START_POINT VARCHAR2(1 ) NOT NULL,
  CAPTURE_RFR_GTT_DATA VARCHAR2(1 ) NOT NULL,
  CAPTURE_RPILE_GTT_DATA VARCHAR2(1 ) NOT NULL,
  CAPTURE_CSPFR_GTT_DATA VARCHAR2(1 ) NOT NULL,
  CAPTURE_CLR_GTT_DATA VARCHAR2(1 ) NOT NULL,
  CAPTURE_RFR_IL_EXPL_GTT_DATA VARCHAR2(1 ) NOT NULL
 )
 INITRANS 6
 TABLESPACE RETAIL_DATA
/

COMMENT ON TABLE RPM_CONFIG_GTT_CAPTURE is 'This table is used to determine if the system should be capturing GTT data for specific processes.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CONFIG_GTT_CAPTURE_ID is 'The unique identifier for the record.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.PROCESS_NAME is 'The name of the process for the record.  The only valid value currently is "CONFLICT_CHECKING".'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.ENABLE_GTT_CAPTURE is 'Determines if the system should capture GTT data for the process specified for the record.  Valid values are ''Y'' (Yes) and ''N'' (No).'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.USER_ID is 'If specified the system will only capture data for the user id specified by the USER_ID.  If this value is not populated, the system will capture GTT data for all users when enable_gtt_capture is set to Y.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_START_POINT is 'Specifies where in the processing the system should start to capture GTT data.  For Conflict Checking, the valid starting points are "POP_GTT", "MERGE_INTO_TIMELINE", "ROLL_FORWARD", "PAYLOAD_POP", and "RFR_PURGE".'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_DATA_AFTER_START_POINT is 'Determines if the system should just capture the GTT data at the starting point specified or if data beyond the starting point should be also be captured.  Valid values are ''Y'' (Yes, also capture data after the starting point) and ''N'' (No, do not capture data after the starting point).'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_RFR_GTT_DATA is 'Indicates if data from RPM_FUTURE_RETAIL_GTT should be captured or not.  Valid values are ''Y'' (Yes, capture the data) or ''N'' (No, do not capture the data).  If data is being captured for this table, it will be captured in the RPM_RFR_GTT_DATA_CAPTURE table.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_RPILE_GTT_DATA is 'Indicates if data from RPM_PROMO_ITEM_LOC_EXPL_GTT should be captured or not.  Valid values are ''Y'' (Yes, capture the data) or ''N'' (No, do not capture the data).  If data is being captured for this table, it will be captured in the RPM_RPILE_GTT_DATA_CAPTURE table.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_CSPFR_GTT_DATA is 'Indicates if data from RPM_CUST_SEGMENT_PROMO_FR_GTT should be captured or not.  Valid values are ''Y'' (Yes, capture the data) or ''N'' (No, do not capture the data).  If data is being captured for this table, it will be captured in the RPM_CSPFR_GTT_DATA_CAPTURE table.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_CLR_GTT_DATA is 'Indicates if data from RPM_CLEARANCE_GTT should be captured or not.  Valid values are ''Y'' (Yes, capture the data) or ''N'' (No, do not capture the data).  If data is being captured for this table, it will be captured in the RPM_CLR_GTT_DATA_CAPTURE table.'
/

COMMENT ON COLUMN RPM_CONFIG_GTT_CAPTURE.CAPTURE_RFR_IL_EXPL_GTT_DATA is 'Indicates if data from RPM_FR_ITEM_LOC_EXPL_GTT should be captured or not.  Valid values are ''Y'' (Yes, capture the data) or ''N'' (No, do not capture the data).  If data is being captured for this table, it will be captured in the RPM_FRILE_GTT_DATA_CAPTURE table.'
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_EGT CHECK (ENABLE_GTT_CAPTURE IN ('Y', 'N'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CSP CHECK (CAPTURE_START_POINT IN ('POP_GTT', 'MERGE_INTO_TIMELINE', 'ROLL_FORWARD', 'PAYLOAD_POP', 'RFR_PURGE'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CDASP CHECK (CAPTURE_DATA_AFTER_START_POINT IN ('Y', 'N'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CRFR CHECK (CAPTURE_RFR_GTT_DATA IN ('Y', 'N'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CRPILE CHECK (CAPTURE_RPILE_GTT_DATA IN ('Y', 'N'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CCSPFR CHECK (CAPTURE_CSPFR_GTT_DATA IN ('Y', 'N'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CCLR CHECK (CAPTURE_CLR_GTT_DATA IN ('Y', 'N'))
/

ALTER TABLE RPM_CONFIG_GTT_CAPTURE ADD CONSTRAINT RPM_CONFIG_GTT_CAPTURE_CRFILE CHECK (CAPTURE_RFR_IL_EXPL_GTT_DATA IN ('Y', 'N'))
/

