--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_BATCH_CONFIG
----------------------------------------------------------------------------


whenever sqlerror exit failure

----------------------------------------------------------
--       DROPPING TABLE RPM_ORPOS_CLEARANCE_PUBLISH              
----------------------------------------------------------
PROMPT DROPPING TABLE  'RPM_ORPOS_CLEARANCE_PUBLISH';
DECLARE
  l_cleareance_table_exists number := 0;
BEGIN
  SELECT count(*) INTO l_cleareance_table_exists
    FROM USER_OBJECTS
    WHERE OBJECT_NAME='RPM_ORPOS_CLEARANCE_PUBLISH';
 
  if (l_cleareance_table_exists != 0) then
      execute immediate 'DROP TABLE RPM_ORPOS_CLEARANCE_PUBLISH';
  end if;
end;
/
