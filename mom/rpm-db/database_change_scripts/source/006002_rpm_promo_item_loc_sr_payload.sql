--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_PROMO_ITEM_LOC_SR_PAYLOAD'
COMMENT ON TABLE RPM_PROMO_ITEM_LOC_SR_PAYLOAD IS 'this table contains the selling retail information at the item/loc level for promotions'
/

COMMENT ON COLUMN RPM_PROMO_ITEM_LOC_SR_PAYLOAD.SELLING_RETAIL_CURRENCY is 'selling retail currency'
/
