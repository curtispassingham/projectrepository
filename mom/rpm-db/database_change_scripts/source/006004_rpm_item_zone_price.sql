--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--             ATTENTION: This script DOES NOT preserve data.
--
--             The customer DBA is responsible to review this script to ensure
--             data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

-----------------------------------------------
--       Modifying Table  RPM_ITEM_ZONE_PRICE             
-----------------------------------------------
PROMPT MODIFYING TABLE 'RPM_ITEM_ZONE_PRICE'
COMMENT ON TABLE RPM_ITEM_ZONE_PRICE is 'This table will store the retail information for an item/zone where the zone is within the primary zone group of the item. The information in this table will be updated by executed price changes, and will also be used to determine what zone retails to assign to new child items.'
/
