--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_PROMO_ITEM_LOC_EXPL_GTT'

PROMPT Creating Index 'RPM_PROMO_ITEM_LOC_EXPL_GTT_I3'
CREATE INDEX RPM_PROMO_ITEM_LOC_EXPL_GTT_I3 on RPM_PROMO_ITEM_LOC_EXPL_GTT
  (ITEM_PARENT,
   LOCATION,
   ZONE_NODE_TYPE,
   DIFF_ID
 )
/

