--------------------------------------------------------
-- Copyright (c) 2015, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--      ATTENTION: This script DOES NOT preserve data.
--
--      The customer DBA is responsible to review this script to ensure
--      data is preserved as desired.
--
----------------------------------------------------------------------------
--      Table MODIFIED:                  RPM_BATCH_LOG
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Renaming Table
--------------------------------------
PROMPT Renaming TABLE RPM_BATCH_LOG
RENAME RPM_BATCH_LOG TO DBC_RPM_BATCH_LOG
/

--------------------------------------
--       Creating Table
--------------------------------------
PROMPT CREATING TABLE 'RPM_BATCH_LOG';
CREATE TABLE RPM_BATCH_LOG
  (
    SID				    VARCHAR2 (20),
    SESSION_ID			VARCHAR2 (20),
    PRODUCT_BU			VARCHAR2 (200),
    PRODUCT			    VARCHAR2 (200),
    BATCH_NAME			VARCHAR2 (200),
    MODULE			    VARCHAR2 (200),
    START_TIME			TIMESTAMP (6),
    END_TIME			TIMESTAMP (6),
    TRANSACTION_COUNT	NUMBER (12,0),
    LOG_TEXT			VARCHAR2 (4000),
    STATUS			    VARCHAR2 (20),
    LOG_TIME			TIMESTAMP (6) 
  )
 PARTITION BY RANGE(LOG_TIME)
 INTERVAL(NUMTOYMINTERVAL(1, 'MONTH')) 
 (
	PARTITION rpm_batch_log_p1 VALUES LESS THAN (TO_DATE('1-1-2014', 'DD-MM-YYYY')), 
	PARTITION rpm_batch_log_p2 VALUES LESS THAN (TO_DATE('1-2-2014', 'DD-MM-YYYY')) 
 )
/

COMMENT ON TABLE RPM_BATCH_LOG IS 'Hold BATCH log information'
/
COMMENT ON COLUMN RPM_BATCH_LOG.SID IS 'SID'
/
COMMENT ON COLUMN RPM_BATCH_LOG.SESSION_ID IS 'Session_id'
/
COMMENT ON COLUMN RPM_BATCH_LOG.PRODUCT_BU IS 'Bussiness Unit'
/
COMMENT ON COLUMN RPM_BATCH_LOG.PRODUCT IS 'Product name'
/
COMMENT ON COLUMN RPM_BATCH_LOG.BATCH_NAME IS 'Batch name'
/
COMMENT ON COLUMN RPM_BATCH_LOG.MODULE IS 'Module name'
/
COMMENT ON COLUMN RPM_BATCH_LOG.START_TIME IS 'Start time' 
/
COMMENT ON COLUMN RPM_BATCH_LOG.END_TIME IS 'End time'
/
COMMENT ON COLUMN RPM_BATCH_LOG.TRANSACTION_COUNT IS 'Number of transations'
/
COMMENT ON COLUMN RPM_BATCH_LOG.LOG_TEXT IS 'Log message'
/
COMMENT ON COLUMN RPM_BATCH_LOG.STATUS IS 'Status info'
/
COMMENT ON COLUMN RPM_BATCH_LOG.LOG_TIME IS 'Time info'
/


--------------------------------------
--       Inserting Data
--------------------------------------
PROMPT Inserting Data...
INSERT /*+ append */ INTO RPM_BATCH_LOG
(SID,
SESSION_ID,
PRODUCT_BU,
PRODUCT ,
BATCH_NAME,
MODULE,
START_TIME,
END_TIME,
TRANSACTION_COUNT ,
LOG_TEXT ,
STATUS ,
LOG_TIME
)
SELECT
SID,
SESSION_ID,
PRODUCT_BU,
PRODUCT ,
BATCH_NAME,
MODULE,
START_TIME,
END_TIME,
TRANSACTION_COUNT ,
LOG_TEXT ,
STATUS ,
LOG_TIME
FROM DBC_RPM_BATCH_LOG
/

COMMIT
/

--------------------------------------
--       Creating Constraints
--------------------------------------
PROMPT CREATING INDEX 'RPM_BATCH_LOG_I1'
CREATE INDEX RPM_BATCH_LOG_I1 ON RPM_BATCH_LOG
(
  LOG_TIME
) LOCAL
/

--------------------------------------
--       Dropping Backup Table
--------------------------------------
DROP TABLE DBC_RPM_BATCH_LOG;


