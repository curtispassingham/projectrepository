--------------------------------------------------------
-- Copyright (c) 2016, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------
--	Table Added: 		 RPM_NIL_ROLLUP_SIBLING_GTT
----------------------------------------------------------------------------


whenever sqlerror exit failure

--------------------------------------
--       Creating Table               
--------------------------------------
PROMPT Creating Table 'RPM_NIL_ROLLUP_SIBLING_GTT'
CREATE GLOBAL TEMPORARY TABLE RPM_NIL_ROLLUP_SIBLING_GTT
 (DEPT NUMBER(4) NOT NULL,
  ITEM VARCHAR2(25 ) NOT NULL,
  ITEM_PARENT VARCHAR2(25 ),
  DIFF_ID VARCHAR2(10 ),
  LOCATION NUMBER(10) NOT NULL,
  ZONE_NODE_TYPE NUMBER(1) NOT NULL,
  ZONE_ID NUMBER(10) NOT NULL
 )
ON COMMIT DELETE ROWS
/

COMMENT ON TABLE RPM_NIL_ROLLUP_SIBLING_GTT is 'Holds ROLL UP data temporary based on session'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.DEPT is 'The department id for the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.ITEM is 'The item id for the record'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.ITEM_PARENT is 'The item parent id of the item'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.DIFF_ID is 'The differentiator for the item'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.LOCATION is 'The location id for the record if the node type is store or warehouse'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.ZONE_NODE_TYPE is 'The zone node type for the location'
/

COMMENT ON COLUMN RPM_NIL_ROLLUP_SIBLING_GTT.ZONE_ID is 'The zone id for the record if the node type is zone'
/

