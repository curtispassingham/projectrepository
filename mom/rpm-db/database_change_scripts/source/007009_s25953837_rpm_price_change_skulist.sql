--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'RPM_PRICE_CHANGE_SKULIST'

PROMPT Creating Index 'RPM_PRICE_CHANGE_SKULIST_I2'
CREATE INDEX RPM_PRICE_CHANGE_SKULIST_I2 on RPM_PRICE_CHANGE_SKULIST
  (PRICE_EVENT_ID,
   SKULIST
 )
 INITRANS 12
 TABLESPACE RETAIL_INDEX
/

