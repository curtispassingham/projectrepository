CREATE OR REPLACE PACKAGE RPM_BULK_CC_THREADING_SQL AS
--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION INIT_BULK_CC_PE(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                         I_bulk_cc_pe_id           IN     NUMBER,
                         I_secondary_bulk_cc_pe_id IN     NUMBER,
                         I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type        IN     VARCHAR2,
                         I_persist_ind             IN     VARCHAR2,
                         I_start_state             IN     VARCHAR2,
                         I_end_state               IN     VARCHAR2,
                         I_user_name               IN     VARCHAR2,
                         I_emergency_perm          IN     NUMBER,
                         I_asynch_ind              IN     NUMBER,
                         I_cc_request_group_id     IN     NUMBER,
                         I_auto_clean_ind          IN     NUMBER,
                         I_need_il_explode         IN     NUMBER DEFAULT 1,
                         I_online_alert_ind        IN     NUMBER,
                         I_thread_processor_class  IN     VARCHAR2,
                         I_na_single_txn_ind       IN     NUMBER DEFAULT 0,
                         I_new_promo_end_date      IN     DATE DEFAULT NULL,
                         I_old_clr_oostock_date    IN     DATE DEFAULT NULL,
                         I_old_clr_reset_date      IN     DATE DEFAULT NULL,
                         I_old_promo_end_date      IN     DATE DEFAULT NULL,
                         I_new_clr_reset_date      IN     DATE DEFAULT NULL,
                         I_old_timebased_dtl_ind   IN     NUMBER DEFAULT NULL,
                         I_new_timebased_dtl_ind   IN     NUMBER DEFAULT NULL,
                         I_sys_gen_excl_reprocess  IN     NUMBER DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION THREAD_OVERLAP_PRICE_EVENTS(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                                     O_max_seq_number    OUT NUMBER,
                                     I_bulk_cc_pe_id  IN     NUMBER,
                                     I_need_explode   IN     NUMBER DEFAULT 1)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CHUNK_PRICE_EVENT_THREADS(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                                   O_max_thread_number    OUT NUMBER,
                                   I_bulk_cc_pe_id     IN     NUMBER,
                                   I_pe_seq_number     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CHECK_THREAD_SEQUENCE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                               O_sequence_complete     OUT NUMBER,
                               I_bulk_cc_pe_id      IN     NUMBER,
                               I_pe_sequence_number IN     NUMBER,
                               I_pe_thread_number   IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION UPDATE_THREAD_SEQUENCE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                I_bulk_cc_pe_id      IN     NUMBER,
                                I_pe_sequence_number IN     NUMBER,
                                I_pe_thread_number   IN     NUMBER,
                                I_thread_status      IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION UPD_AND_CHECK_THREAD_SEQUENCE(O_cc_error_tbl             OUT CONFLICT_CHECK_ERROR_TBL,
                                       O_sequence_complete        OUT NUMBER,
                                       I_bulk_cc_pe_id         IN     NUMBER,
                                       I_pe_sequence_number    IN     NUMBER,
                                       I_pe_thread_number      IN     NUMBER,
                                       I_thread_status         IN     VARCHAR2,
                                       I_post_push_back_failed IN     NUMBER DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                       I_bulk_cc_pe_id    IN     NUMBER,
                       I_pe_sequence_id   IN     NUMBER,
                       I_pe_thread_number IN     NUMBER,
                       I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PURGE_BULK_CC_TABLE(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                             I_bulk_cc_pe_id IN     NUMBER DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION COMPLETE_BULK_CC(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                          I_bulk_cc_pe_id IN     NUMBER,
                          I_set_error     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION INSERT_CC_RESULT_TABLE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_bulk_cc_pe_id    IN     NUMBER,
                                I_pe_sequence_id   IN     NUMBER,
                                I_pe_thread_number IN     NUMBER,
                                I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                                I_error_string     IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_CC_ERROR_TABLE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION NEED_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                    O_need_chunk          OUT NUMBER,
                    I_bulk_cc_pe_id    IN     NUMBER,
                    I_pe_sequence_id   IN     NUMBER,
                    I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION LOCK_PE(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                 I_bulk_cc_pe_id        IN     NUMBER,
                 I_rib_transaction_id   IN     NUMBER,
                 I_parent_thread_number IN     NUMBER,
                 I_thread_number        IN     NUMBER,
                 I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION RESET_PE_STATUS_ON_FAILURE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_bulk_cc_pe_id    IN     NUMBER,
                                    I_pe_sequence_id   IN     NUMBER,
                                    I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_BULK_CC_THREADING_SQL;
/