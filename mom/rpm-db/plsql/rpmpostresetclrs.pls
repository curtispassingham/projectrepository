CREATE OR REPLACE PACKAGE RPM_CC_POST_RESET_CLR AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

--FUNCTION VALIDATE(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
--                  I_clearance_date IN     DATE)
--RETURN NUMBER;

FUNCTION VALIDATE(IO_cc_error_tbl   IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION VALIDATE(IO_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_parent_rpcs       IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------

END RPM_CC_POST_RESET_CLR;
/

