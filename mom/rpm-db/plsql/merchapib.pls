CREATE OR REPLACE PACKAGE BODY MERCH_API_SQL IS

   LP_vdate DATE := GET_VDATE();

--------------------------------------------------------------------------------
PROCEDURE GET_RPM_SYSTEM_OPTIONS
(O_error_message           IN OUT  VARCHAR2,
 O_success                 IN OUT  VARCHAR2,
 O_rpm_system_options_rec  IN OUT  NOCOPY OBJ_RPM_SYSTEM_OPTIONS_REC
)
IS

   L_program  VARCHAR2(61) := 'MERCH_API_SQL.GET_RPM_SYSTEM_OPTIONS';

   cursor C_RPM_SYSTEM_OPTIONS is
      select OBJ_RPM_SYSTEM_OPTIONS_REC(complex_promo_allowed_ind)
        from rpm_system_options;

BEGIN

   --- Initialize output variables
   O_success                    := 'N';
   O_rpm_system_options_rec := NULL;

   open  C_RPM_SYSTEM_OPTIONS;
   fetch C_RPM_SYSTEM_OPTIONS into O_rpm_system_options_rec;
   close C_RPM_SYSTEM_OPTIONS;

   if O_rpm_system_options_rec is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('FAILED_SYSTEM_OPTIONS',NULL,NULL,NULL);
      return;
   end if;

   O_success := 'Y';

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END GET_RPM_SYSTEM_OPTIONS;

--------------------------------------------------------------------------------
PROCEDURE GET_PROMOS(O_error_message    OUT VARCHAR2,
                     O_success          OUT VARCHAR2,
                     O_promo_table      OUT NOCOPY OBJ_PROMO_TBL)
IS

   L_program VARCHAR2(25) := 'MERCH_API_SQL.GET_PROMOS';

   cursor C_PROMO is
      select OBJ_PROMO_REC(promo_id,
                           promo_display_id,
                           NULL,
                           name,
                           currency_code,
                           promo_event_id,
                           start_date,
                           end_date)
        from rpm_promo
      union all
      select OBJ_PROMO_REC(promo_id,
                           promo_display_id,
                           NULL,
                           name,
                           currency_code,
                           promo_event_id,
                           start_date,
                           end_date)
        from rpm_promo_hist;

BEGIN

   --- Initialize output variables
   O_success := 'N';

   open C_PROMO;
   fetch C_PROMO BULK COLLECT INTO O_promo_table;
   close C_PROMO;

   O_success := 'Y';

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';

END GET_PROMOS;

--------------------------------------------------------------------------------
PROCEDURE GET_VALID_PROMOS
(O_error_message  IN OUT  VARCHAR2,
 O_success        IN OUT  VARCHAR2,
 O_promo_table    IN OUT  NOCOPY OBJ_PROMO_TBL
) IS
   L_program  VARCHAR2(61) := 'MERCH_API_SQL.GET_VALID_PROMOS';

   cursor C_PROMO is
      select OBJ_PROMO_REC(promo_id,
                           promo_display_id,
                           NULL,
                           name,
                           currency_code,
                           promo_event_id,
                           start_date,
                           end_date)
        from rpm_promo
      where ((end_date >= (select vdate from period)) or (end_date is null));

BEGIN

   --- Initialize output variables
   O_success       := 'N';
   if O_promo_table is NOT NULL then
      O_promo_table.DELETE;
   end if;

   open  C_PROMO;
   fetch C_PROMO BULK COLLECT INTO O_promo_table;
   close C_PROMO;

   O_success := 'Y';

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END GET_VALID_PROMOS;
--------------------------------------------------------------------------------
PROCEDURE GET_PROMO_COMPS(O_error_message       OUT VARCHAR2,
                          O_success             OUT VARCHAR2,
                          O_promo_comp_table    OUT NOCOPY OBJ_PROMO_COMP_TBL)
IS

   L_program VARCHAR2(30) := 'MERCH_API_SQL.GET_PROMO_COMPS';

   cursor C_PROMO_COMP is
      select OBJ_PROMO_COMP_REC(promo_comp_id,
                                comp_display_id,
                                promo_id,
                                name)
        from rpm_promo_comp
      union all
      select OBJ_PROMO_COMP_REC(promo_comp_id,
                                comp_display_id,
                                promo_id,
                                name)
        from rpm_promo_comp_hist;

BEGIN

   --- Initialize output variables
   O_success := 'N';

   open C_PROMO_COMP;
   fetch C_PROMO_COMP BULK COLLECT INTO O_promo_comp_table;
   close C_PROMO_COMP;

   O_success := 'Y';

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';

END GET_PROMO_COMPS;

--------------------------------------------------------------------------------
PROCEDURE VALIDATE_PROMO (O_error_message IN OUT VARCHAR2,
                          O_success       IN OUT VARCHAR2,
                          O_valid         IN OUT VARCHAR2,
                          O_promotion_rec IN OUT NOCOPY OBJ_PROMO_REC,
                          I_check_status  IN     VARCHAR2,
                          I_promotion_id  IN     RPM_PROMO.PROMO_ID%TYPE,
                          I_need_hist     IN     NUMBER DEFAULT 0)
IS

   L_program VARCHAR2(30) := 'MERCH_API_SQL.VALIDATE_PROMO';

   L_invalid_param  VARCHAR2(30);

   cursor C_VALIDATE is
      select 'Y',
             OBJ_PROMO_REC(promo_id,
                           promo_display_id,
                           NULL,
                           name,
                           currency_code,
                           promo_event_id,
                           start_date,
                           end_date)
        from rpm_promo
       where promo_id      = I_promotion_id
         and (  (end_date >= LP_vdate)
              or(end_date is NULL));

   cursor C_VALIDATE_ALL_PROMOS is
      select 'Y',
             OBJ_PROMO_REC(promo_id,
                           promo_display_id,
                           NULL,
                           name,
                           currency_code,
                           promo_event_id,
                           start_date,
                           end_date)
        from rpm_promo_v
       where promo_id = I_promotion_id;


BEGIN

   --- Initialize output variables
   O_success       := 'N';
   O_valid         := 'N';
   O_promotion_rec := NULL;

   --- Validate parameters
   if I_promotion_id is NULL then
      L_invalid_param := 'I_promotion_id';

   elsif I_check_status is NULL then
      L_invalid_param := 'I_check_status';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      return;
   end if;

   if I_need_hist = 0 then
      open  C_VALIDATE;
      fetch C_VALIDATE into O_valid, O_promotion_rec;
      close C_VALIDATE;
   else
      open  C_VALIDATE_ALL_PROMOS;
      fetch C_VALIDATE_ALL_PROMOS into O_valid, O_promotion_rec;
      close C_VALIDATE_ALL_PROMOS;
   end if;

   O_success := 'Y';

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';

END VALIDATE_PROMO;

--------------------------------------------------------------------------------
PROCEDURE VALIDATE_PROMO_COMP (O_error_message IN OUT VARCHAR2,
                               O_success       IN OUT VARCHAR2,
                               O_valid         IN OUT VARCHAR2,
                               O_component_rec IN OUT NOCOPY OBJ_PROMO_COMP_REC,
                               I_promotion_id  IN     RPM_PROMO.PROMO_ID%TYPE,
                               I_component_id  IN     RPM_PROMO_COMP.PROMO_COMP_ID%TYPE,
                               I_need_hist     IN     NUMBER DEFAULT 0)
IS

   L_program VARCHAR2(35) := 'MERCH_API_SQL.VALIDATE_PROMO_COMP';

   L_invalid_param VARCHAR2(30);

   cursor C_VALIDATE is
      select 'Y',
             OBJ_PROMO_COMP_REC(promo_comp_id,
                                comp_display_id,
                                promo_id,
                                name)
        from rpm_promo_comp
       where promo_comp_id       = I_component_id
         and (   I_promotion_id is NULL
              or promo_id        = I_promotion_id);

   cursor C_VALIDATE_ALL_PROMOS is
      select 'Y',
             OBJ_PROMO_COMP_REC(promo_comp_id,
                                comp_display_id,
                                promo_id,
                                name)
        from rpm_promo_comp_v
       where promo_comp_id       = I_component_id
         and (   I_promotion_id is NULL
              or promo_id        = I_promotion_id);

BEGIN

   --- Initialize output variables
   O_success       := 'N';
   O_valid         := 'N';
   O_component_rec := NULL;

   --- Validate parameters
   if I_component_id is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_component_id',
                                            L_program,
                                            NULL);
      return;
   end if;

   if I_need_hist = 0 then
      open C_VALIDATE;
      fetch C_VALIDATE into O_valid,
                            O_component_rec;
      close C_VALIDATE;
   else
      open C_VALIDATE_ALL_PROMOS;
      fetch C_VALIDATE_ALL_PROMOS into O_valid,
                                       O_component_rec;
      close C_VALIDATE_ALL_PROMOS;
   end if;

   O_success := 'Y';

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';

END VALIDATE_PROMO_COMP;

--------------------------------------------------------------------------------
PROCEDURE DEAL_PROMO_EXISTS
(O_error_message  IN OUT  VARCHAR2,
 O_success        IN OUT  VARCHAR2,
 O_exists         IN OUT  VARCHAR2,
 I_deal_id        IN      DEAL_HEAD.DEAL_ID%TYPE
)
IS

   L_program  VARCHAR2(61) := 'MERCH_API_SQL.DEAL_PROMO_EXISTS';

   cursor C_EXISTS is
      select 'Y'
        from rpm_promo_deal_link
       where deal_id = I_deal_id
         and rownum  = 1
       union all
      select 'Y'
        from deal_comp_prom
       where deal_id = I_deal_id
         and rownum  = 1;

BEGIN

   --- Initialize output variables
   O_success := 'N';
   O_exists  := 'N';

   open  C_EXISTS;
   fetch C_EXISTS into O_exists;
   close C_EXISTS;

   O_success := 'Y';

EXCEPTION
   when OTHERS then
      O_success := 'N';
END DEAL_PROMO_EXISTS;

--------------------------------------------------------------------------------
/*
   This procedure is overloaded to improve the performance in RMS order entry.

   Success will be set to 'Y'es on return unless an exception has been
   encountered during execution.
 */

PROCEDURE GET_PROMOS
(O_error_message  IN OUT  VARCHAR2,
 O_success IN OUT  VARCHAR2
)
IS

   L_program  VARCHAR2(61) := 'MERCH_API_SQL.GET_PROMOS';

   L_records  INTEGER(10)      := -1;
   L_has_records  VARCHAR2(1)  := 'N';

   CURSOR c_has_records IS
      SELECT 'Y'
      FROM gtt_promo_temp
      WHERE rownum = 1 ;

BEGIN
   O_success := 'Y';

   /*
      Check whether the temporary table has records.  If it contains
      records, leave the temporary table as is and leave success set
      to 'Y'es.  Otherwise, copy records into the temporary table and
      leave success set to 'Y'es.
   */

   OPEN  c_has_records;
   FETCH c_has_records INTO L_has_records;
   CLOSE c_has_records;

   IF L_has_records = 'N' THEN
      INSERT INTO gtt_promo_temp(promo_id,
                              promo_display_id,
                              state,
                              name,
                              currency_code,
                              promo_event_id,
                              start_date,
                              end_date)
                       SELECT promo_id,
                              promo_display_id,
                              NULL,
                              name,
                              currency_code,
                              promo_event_id,
                              start_date,
                              end_date
                         FROM rpm_promo;
   END IF;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END get_promos;
--------------------------------------------------------------------------------
/*
   This procedure is overloaded to improve the performance in RMS order entry.

   Success will be set to 'Y'es on return unless an exception has been
   encountered during execution.
 */

PROCEDURE GET_PROMO_COMPS
(O_error_message     IN OUT  VARCHAR2,
 O_success        IN OUT  VARCHAR2
)
IS
   L_program  VARCHAR2(61)     := 'MERCH_API_SQL.GET_PROMO_COMPS';
   L_records  INTEGER(10)      := -1;
   L_has_records  VARCHAR2(1)  := 'N';

   cursor C_HAS_RECORDS is
      select 'Y'
         from gtt_promo_comp_temp
         where rownum = 1 ;

BEGIN

   O_success      := 'Y';

   /*
      Check whether the temporary table has records.  If it contains
      records, leave the temporary table as is and leave success set
      to 'Y'es.  Otherwise, copy records into the temporary table and
      leave success set to 'Y'es.
   */

   OPEN  c_has_records;
   FETCH c_has_records INTO L_has_records;
   CLOSE c_has_records;

   IF L_has_records = 'N' THEN
      INSERT INTO gtt_promo_comp_temp(promo_comp_id,
                                      comp_display_id,
                                      promo_id,
                                      name)
      SELECT promo_comp_id,
             comp_display_id,
             promo_id,
             name
        FROM rpm_promo_comp;
   END IF;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END get_promo_comps;

--------------------------------------------------------------------------------

FUNCTION CHECK_CUST_SEGMENT_ACTIVE (O_error_message          OUT VARCHAR2,
                                    O_cust_seg_active_ind    OUT NUMBER,
                                    I_cust_segment_type   IN     NUMBER)
RETURN NUMBER IS

   L_program          VARCHAR2(50)  := 'MERCH_API_SQL.CHECK_CUST_SEGMENT_ACTIVE';
   L_cust_seg_active  VARCHAR2(1)   := NULL;

   cursor C_CHECK_CUST_SEG is
      select 'x'
        from rpm_promo_comp
       where customer_type = I_cust_segment_type
      union all
      select 'x'
        from rpm_promo_comp_hist
       where customer_type = I_cust_segment_type;

BEGIN

   ----
   -- If the passed in customer segment type is on an active promotion,
   -- an up coming promotion, or an historical promotion set the output
   -- indicator to 1, otherwise, set it to zero.
   ---

   open C_CHECK_CUST_SEG;
   fetch C_CHECK_CUST_SEG into L_cust_seg_active;

   if C_CHECK_CUST_SEG%FOUND then
      O_cust_seg_active_ind := 1;
   else
      O_cust_seg_active_ind := 0;
   end if;

   close C_CHECK_CUST_SEG;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;
END CHECK_CUST_SEGMENT_ACTIVE;

--------------------------------------------------------------------------------

FUNCTION CHECK_ITEMLIST_ACTIVE(O_error_message          OUT VARCHAR2,
                               O_itemlist_active_ind    OUT NUMBER,
                               I_itemlist_id         IN     NUMBER)
RETURN NUMBER IS

   L_program  VARCHAR2(40)  := 'MERCH_API_SQL.CHECK_ITEMLIST_ACTIVE';

   L_ind  VARCHAR2(1) := NULL;

   cursor C_CHECK is
      select 'x'
        from rpm_price_change_skulist
       where skulist = I_itemlist_id
      union all
      select 'x'
        from rpm_clearance_skulist
       where skulist = I_itemlist_id
      union all
      select 'x'
        from rpm_promo_dtl_skulist
       where skulist = I_itemlist_id
      union all
      select 'x'
        from rpm_promo_dtl_skulist_hist
       where skulist = I_itemlist_id;

BEGIN

   O_itemlist_active_ind := 1;

   open C_CHECK;
   fetch C_CHECK into L_ind;

   if C_CHECK%FOUND then
      O_itemlist_active_ind := 1;
   else
      O_itemlist_active_ind := 0;
   end if;

   close C_CHECK;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;
END CHECK_ITEMLIST_ACTIVE;

--------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_RETAIL(O_error_message              OUT VARCHAR2,
                            O_selling_retail             OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                            O_selling_uom                OUT RPM_FUTURE_RETAIL.SELLING_UOM%TYPE,
                            O_selling_retail_currency    OUT RPM_FUTURE_RETAIL.SELLING_RETAIL_CURRENCY%TYPE,
                            I_item                    IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                            I_location                IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                            I_effective_date          IN     DATE DEFAULT NULL)
RETURN NUMBER IS

   L_program  VARCHAR2(35) := 'MERCH_API_SQL.GET_ITEMLOC_RETAIL';

   L_selling_retail  OBJ_PRC_INQ_TBL          := OBJ_PRC_INQ_TBL();
   L_search_criteria OBJ_PRICE_INQ_SEARCH_TBL := OBJ_PRICE_INQ_SEARCH_TBL();

   L_item_type VARCHAR2(1) := NULL;

   cursor C_ITEMLOC_EXIST is
      -- tran item
      select RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM
        from item_master im,
             rpm_item_loc ril
       where im.item = I_item
         and im.dept = ril.dept
         and im.item = ril.item
         and ril.loc = I_location
      union all
      -- parent item
      select RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT
        from item_master im,
             rpm_item_loc ril
       where im.item_parent = I_item
         and im.item_level  = im.tran_level
         and im.dept        = ril.dept
         and im.item        = ril.item
         and ril.loc        = I_location;

BEGIN

   O_selling_retail := NULL;

   open C_ITEMLOC_EXIST;
   fetch C_ITEMLOC_EXIST into L_item_type;

   if C_ITEMLOC_EXIST%NOTFOUND then
      close C_ITEMLOC_EXIST;
      return 1;
   end if;

   close C_ITEMLOC_EXIST;

   L_search_criteria.EXTEND;

   --Assign the input variables into the table
   L_search_criteria(L_search_criteria.COUNT) := OBJ_PRICE_INQ_SEARCH_REC(NULL,  --dept_class_subclass
                                                                          L_item_type,
                                                                          RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM,
                                                                          NULL,  --item_list_id
                                                                          OBJ_VARCHAR_ID_TABLE(I_item),
                                                                          NULL,  --diff_type
                                                                          NULL,  --diff_ids
                                                                          NULL,  --zone_group
                                                                          NULL,  --zone_ids
                                                                          OBJ_NUMERIC_ID_TABLE(I_location),
                                                                          NVL(I_effective_date, GET_VDATE),
                                                                          1,     --maximum_result
                                                                          NULL,  --customer_types
                                                                          NULL,  --user_id
                                                                          NULL); --price_event_item_list_id

   if RPM_PRICE_INQUIRY_SQL.GET_PRICE_INQ_VO(O_error_message,
                                             L_selling_retail,
                                             L_search_criteria) = 0 then
      return 0;
   end if;

   -- Set the output parameter to the unit retail.
   if L_selling_retail is NOT NULL and
      L_selling_retail.COUNT > 0 then
      ---
      O_selling_retail          := L_selling_retail(1).selling_retail;
      O_selling_uom             := L_selling_retail(1).selling_uom;
      O_selling_retail_currency := L_selling_retail(1).selling_retail_currency;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;

END GET_ITEMLOC_RETAIL;

--------------------------------------------------------------------------------

END MERCH_API_SQL;
/

