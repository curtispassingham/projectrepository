CREATE OR REPLACE PACKAGE RPM_WSFILTER_ROLLUP_QUERY AS

/* Function GET_PRIMARY_COMP_ALERT returns true if there is a primary competitor alert for an item. 
   Based on the inputs, I_proposed_strategy_id and/or I_area_diff_id, the function will query either
   the strategy tables or the secondary area differential tables to get the strategy type and range 
   percent values for the new retail.
   Based on the strategy type, it will convert and compare the competitor retail and the new retail
   for the item and raise or not raise a flag based on the comparison result.
   Note:- This function was formed to be used in the WHERE clause on the wrapper for the rollup query.
*/
FUNCTION GET_PRIMARY_COMP_ALERT (I_proposed_strategy_id    IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE,
                                 I_area_diff_id            IN RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                                 I_item_id                 IN RPM_WORKSHEET_ITEM_DATA.ITEM%TYPE,
                                 I_new_retail              IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL%TYPE,
                                 I_new_retail_uom          IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_UOM%TYPE,
                                 I_primary_comp_retail     IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL%TYPE,
                                 I_primary_comp_retail_uom IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL_UOM%TYPE)
RETURN NUMBER;

/* Function GET_COMP_A_ALERT returns true if there is a Competitor A alert for an item. 
   Based on the inputs, I_proposed_strategy_id and/or I_area_diff_id, the function will query either
   the strategy tables or the secondary area differential tables to get the strategy type
   Based on the strategy type, it will convert and compare the competitor retail and the new retail
   for the item and raise or not raise a flag based on the comparison result.
   Note:- This function was formed to be used in the WHERE clause on the wrapper for the rollup query.
*/
FUNCTION GET_COMP_A_ALERT (I_proposed_strategy_id    IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE,
                           I_area_diff_id            IN RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                           I_item_id                 IN RPM_WORKSHEET_ITEM_DATA.ITEM%TYPE,
                           I_new_retail              IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL%TYPE,
                           I_new_retail_uom          IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_UOM%TYPE,
                           I_primary_comp_retail     IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL%TYPE,
                           I_primary_comp_retail_uom IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL_UOM%TYPE)
RETURN NUMBER;

/* Function GET_PRICE_CHANGE_AMT calculates the price change amount and return the same.
   It converts the new retail to basis retai UOM and calculates the price change amount from converted new reatil
   and basis retail depending on the price change calculation type 
*/
FUNCTION GET_PRICE_CHANGE_AMT (I_worksheet_status_id IN RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                               I_basis_retail        IN RPM_WORKSHEET_ZONE_WORKSPACE.BASIS_ZL_REGULAR_RETAIL%TYPE,
                               I_basis_retail_uom    IN RPM_WORKSHEET_ZONE_WORKSPACE.BASIS_ZL_REGULAR_RETAIL_UOM%TYPE,
                               I_item_id             IN RPM_WORKSHEET_ZONE_WORKSPACE.ITEM%TYPE,
                               I_new_retail          IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL%TYPE,
                               I_new_retail_uom      IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_UOM%TYPE)
RETURN NUMBER;

/* Function GET_COSTCHNG_REVPERD return 1 if the I_pend_cost_change_date is between the strategy 
   start date and end date.Otherwise 0.
*/
FUNCTION GET_COSTCHNG_REVPERD (I_pend_cost_change_date   IN DATE,
                               I_proposed_strategy_id    IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE)
RETURN NUMBER;

/* Function UPD_SELECTED_FLAG sets the RPM_WORKSHEET_ZONE_WORKSPACE.selected column to 1 for those
   items that were selected from the Multi-record block of the worksheet filter screen search 
   result; to be edited in worksheet detail. It not only updates the column value for just 
   those items selected in the UI, but also for the items which belong to the same item_parent.
*/
FUNCTION UPD_SELECTED_FLAG (O_error_msg       OUT VARCHAR2,
                            I_workspace_id IN     RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE,
                            I_item_ids_tbl IN     OBJ_VARCHAR_ID_TABLE,
                            I_link_codes   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER;

/* Function RESET_SELECTED_FLAG resets the RPM_WORKSHEET_ZONE_WORKSPACE.selected column value to 0
   from 1.
*/
FUNCTION RESET_SELECTED_FLAG (O_error_msg       OUT VARCHAR2,
                              I_workspace_id IN     RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE)
RETURN NUMBER;

END RPM_WSFILTER_ROLLUP_QUERY;
/
