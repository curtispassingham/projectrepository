CREATE OR REPLACE PACKAGE RPM_EXT_DASHBOARD_DATA_SQL AS

----------------------------------------------------------------------

FUNCTION INITIALIZE_THREADS(O_error_message       OUT VARCHAR2,
                            O_max_thread          OUT NUMBER,
                            I_thread_luw_count IN     NUMBER)
RETURN NUMBER;

----------------------------------------------------------------------

FUNCTION STAGE_PROMO_DATA(O_error_message    OUT VARCHAR2,
                          I_thread_number IN     NUMBER)
RETURN NUMBER;

----------------------------------------------------------------------

END RPM_EXT_DASHBOARD_DATA_SQL;
/
