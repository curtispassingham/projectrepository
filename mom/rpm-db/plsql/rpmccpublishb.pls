CREATE OR REPLACE PACKAGE BODY RPM_CC_PUBLISH AS

   LP_vdate DATE := GET_VDATE;

--------------------------------------------------------

FUNCTION STAGE_PC_MESSAGES(IO_error_table    IN OUT CONFLICT_CHECK_ERROR_TBL,
                           I_transaction_id  IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                           I_bulk_cc_pe_id   IN     NUMBER,
                           I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                           I_chunk_number    IN     NUMBER DEFAULT 1,
                           I_repoppay_ind    IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_PUBLISH.STAGE_PC_MESSAGES';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_price_event_payload_seq NUMBER(10) := NULL;
   L_pe_seq                  NUMBER(10) := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

   L_parent_thread_number NUMBER(10) := NULL;
   L_thread_number        NUMBER(10) := NULL;

   cursor C_THREAD_NUMBERS is
      select /*+ CARDINALITY(ids 10) */
             distinct parent_thread_number,
             thread_number
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE;

BEGIN

   -- check if chunk_price_event_payload_id is present, else get RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL
   select NVL((select /*+ CARDINALITY(ids 10) */
                      chunk_price_event_payload_id
                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_bulk_cc_pe_thread rbcpt
                where rbcpt.price_event_id   = VALUE(ids)
                  and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
                  and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                  and rownum                 = 1), RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL)
     into L_price_event_payload_seq
     from dual;

   open C_THREAD_NUMBERS;
   fetch C_THREAD_NUMBERS into L_parent_thread_number,
                               L_thread_number;
   close C_THREAD_NUMBERS;

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 location,
                                 dept)
      with price_change as
         (select /*MATERIALIZED USE_NL(pc) LEADING(IDS) */
                 VALUE(ids) price_event_id,
                 pc.item,
                 pc.skulist,
                 pc.price_event_itemlist,
                 pc.link_code,
                 pc.location,
                 pc.zone_id,
                 pc.zone_node_type,
                 pc.sys_generated_exclusion
            from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_price_change pc
          where pc.exception_parent_id = VALUE(ids)
            and pc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                           RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                           RPM_CONSTANTS.PC_PENDING_STATE_CODE,
                                           RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
            and rownum                 > 0)
      -- parent loc
      select /*+ USE_NL(im) ORDERED */
             pc.price_event_id,
             im.item,
             pc.location,
             im.dept
        from price_change pc,
             item_master im
       where im.item_parent    = pc.item
         and im.item_level     = im.tran_level
         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and pc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
      union all
      -- item zone
      select /*+ USE_NL(im) ORDERED */
             pc.price_event_id,
             im.item,
             rzl.location,
             im.dept
        from price_change pc,
             item_master im,
             rpm_zone_location rzl
       where im.item           = pc.item
         and im.item_level     = im.tran_level
         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and pc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id       = pc.zone_id
      union all
      -- item loc
      select /*+ USE_NL(im) ORDERED */
             pc.price_event_id,
             im.item,
             pc.location,
             im.dept
        from price_change pc,
             item_master im
       where im.item           = pc.item
         and im.item_level     = im.tran_level
         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and pc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - parent items
       select /*+ USE_NL(pcs, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_price_change_skulist pcs,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 0
          and pcs.skulist                        = pc.skulist
          and pcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
          and pcs.item                           = im.item_parent
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - tran items
       select /*+ USE_NL(pcs, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_price_change_skulist pcs,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 0
          and pcs.skulist                        = pc.skulist
          and pcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
          and pcs.item                           = im.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- peil loc - parent items
       select pc.price_event_id,
              rmld.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail rmld,
              item_master im
        where pc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = pc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and rmld.item               = im.item_parent
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and im.item_level           = im.tran_level
          and pc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- peil loc - tran items
       select pc.price_event_id,
              rmld.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail rmld,
              item_master im
        where pc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = pc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and rmld.item               = im.item
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and im.item_level           = im.tran_level
          and pc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list zone - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = pc.zone_id
       union all
       -- merch_list zone - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = pc.zone_id
       union all
       -- merch_list zone - tran items
       select  /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = pc.zone_id
       union all
       -- merch_list loc - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- link code zone
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              rlca.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_link_code_attribute rlca,
              item_master im,
              rpm_zone_location rzl
        where pc.link_code      is NOT NULL
          and pc.link_code      = rlca.link_code
          and im.item           = rlca.item
          and pc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id       = pc.zone_id
       -- link code loc
       union all
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              rlca.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_link_code_attribute rlca,
              item_master im
        where pc.link_code      is NOT NULL
          and pc.link_code      = rlca.link_code
          and im.item           = rlca.item
          and pc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE);

   insert
      when rank = 1 and
         (I_chunk_number IN (0,
                             1) or
          pc_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number)
                          values (DECODE(pc_msg_type,
                                         RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD, L_pe_seq,
                                         L_price_event_payload_seq),
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                  pc_msg_type,
                                  I_bulk_cc_pe_id,
                                  L_parent_thread_number,
                                  L_thread_number)
      when 1 = 1 then
      into
         rpm_price_chg_payload (price_chg_payload_id,
                                price_event_payload_id,
                                price_change_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                selling_unit_change_ind,
                                selling_retail,
                                selling_retail_uom,
                                selling_retail_currency,
                                multi_unit_change_ind,
                                multi_units,
                                multi_units_retail,
                                multi_units_uom,
                                multi_units_currency,
                                bulk_cc_pe_id)
                       values  (RPM_PRICE_CHG_PAYLOAD_SEQ.NEXTVAL,
                                DECODE(pc_msg_type,
                                       RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD, L_pe_seq,
                                       L_price_event_payload_seq),
                                price_change_id,
                                item,
                                location,
                                loc_type,
                                action_date,
                                pc_selling_retail_ind,
                                selling_retail,
                                selling_uom,
                                selling_retail_currency,
                                pc_multi_unit_ind,
                                multi_units,
                                multi_unit_retail,
                                multi_selling_uom,
                                multi_unit_retail_currency,
                                I_bulk_cc_pe_id)
      select t2.location,
             t2.loc_type,
             t2.pc_msg_type,
             t2.item,
             t2.price_change_id,
             t2.action_date,
             t2.pc_selling_retail_ind,
             t2.selling_retail,
             t2.selling_uom,
             t2.selling_retail_currency,
             t2.pc_multi_unit_ind,
             t2.multi_units,
             t2.multi_unit_retail,
             t2.multi_selling_uom,
             t2.multi_unit_retail_currency,
             t2.rank
       from (select t1.location,
                    t1.loc_type,
                    t1.pc_msg_type,
                    t1.item,
                    t1.price_change_id,
                    t1.action_date,
                    NVL(t1.pc_selling_retail_ind, 0) pc_selling_retail_ind,
                    t1.selling_retail,
                    t1.selling_uom,
                    t1.selling_retail_currency,
                    NVL(t1.pc_multi_unit_ind, 0) pc_multi_unit_ind,
                    t1.multi_units,
                    t1.multi_unit_retail,
                    t1.multi_selling_uom,
                    t1.multi_unit_retail_currency,
                    ROW_NUMBER() OVER (PARTITION BY pc_msg_type
                                           ORDER BY item,
                                                    location,
                                                    action_date,
                                                    price_change_id,
                                                    future_retail_id) rank
               from (select location,
                            loc_type,
                            pc_msg_type,
                            item,
                            price_change_id,
                            action_date,
                            pc_selling_retail_ind,
                            selling_retail,
                            selling_uom,
                            selling_retail_currency,
                            pc_multi_unit_ind,
                            multi_units,
                            multi_unit_retail,
                            multi_selling_uom,
                            multi_unit_retail_currency,
                            future_retail_id,
                            RANK() OVER (PARTITION BY item,
                                                      location,
                                                      action_date
                                             ORDER BY promo_dtl_id) rank
                     from(select gtt.location,
                                 DECODE(zone_node_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                                 pc_msg_type,
                                 gtt.item,
                                 price_change_id,
                                 action_date,
                                 pc_selling_retail_ind,
                                 selling_retail,
                                 selling_uom,
                                 selling_retail_currency,
                                 pc_multi_unit_ind,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_units,
                                        NULL) multi_units,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_unit_retail,
                                        NULL) multi_unit_retail,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_selling_uom,
                                        NULL) multi_selling_uom,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_unit_retail_currency,
                                        NULL) multi_unit_retail_currency,
                                 future_retail_id,
                                 promo_dtl_id,
                                 ril.price_event_id ril_peid
                            from rpm_fr_item_loc_expl_gtt gtt,
                                 (select distinct
                                         price_event_id,
                                         item,
                                         dept,
                                         location
                                    from rpm_item_loc_gtt
                                   where rownum > 0) ril
                           where pc_msg_type            IN (RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE,
                                                            RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD)
                             and ril.price_event_id (+) = gtt.price_event_id
                             and ril.dept           (+) = gtt.dept
                             and ril.location       (+) = gtt.location
                             and ril.item           (+) = gtt.item) t
                    where t.ril_peid is NULL) t1
              where rank = 1) t2
         order by t2.pc_msg_type,
                  t2.rank;

      -- If we are going through the repoppay batch, this merge statement should not fire
      if I_repoppay_ind = 0 then

         merge /*+ FIRST_ROWS */
          into rpm_pc_ticket_request rptr
         using (select item,
                       location,
                       RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                       price_change_id,
                       selling_retail,
                       multi_units,
                       multi_unit_retail,
                       action_date
                  from (select item,
                               location,
                               price_change_id,
                               selling_retail,
                               multi_units,
                               multi_unit_retail,
                               action_date,
                               RANK() OVER (PARTITION BY item,
                                                         location,
                                                         price_change_id,
                                                         action_date
                                                ORDER BY promo_dtl_id) rnk
                          from (select gtt.item,
                                       gtt.location,
                                       price_change_id,
                                       DECODE(pc_selling_retail_ind,
                                              1, selling_retail,
                                              NULL) selling_retail,
                                       DECODE(pc_multi_unit_ind,
                                              1, multi_units,
                                              NULL) multi_units,
                                       DECODE(pc_multi_unit_ind,
                                              1, multi_unit_retail,
                                              NULL) multi_unit_retail,
                                       action_date,
                                       promo_dtl_id,
                                       ril.price_event_id ril_peid
                                  from rpm_fr_item_loc_expl_gtt gtt,
                                       (select distinct
                                               price_event_id,
                                               item,
                                               dept,
                                               location
                                          from rpm_item_loc_gtt
                                         where rownum > 0) ril
                                 where pc_msg_type            IN (RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE,
                                                                  RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD)
                                   and zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                   and ril.price_event_id (+) = gtt.price_event_id
                                   and ril.dept           (+) = gtt.dept
                                   and ril.location       (+) = gtt.location
                                   and ril.item           (+) = gtt.item) t
                          where t.ril_peid is NULL)
                 where rnk = 1) rfrg
            on (    rptr.item            = rfrg.item
                and rptr.loc             = rfrg.location
                and rptr.loc_type        = rfrg.loc_type
                and rptr.price_change_id = rfrg.price_change_id)
          when MATCHED then
             update
                set unit_retail       = rfrg.selling_retail,
                    multi_units       = rfrg.multi_units,
                    multi_unit_retail = rfrg.multi_unit_retail
          when NOT MATCHED then
             insert (item,
                     loc,
                     loc_type,
                     price_change_id,
                     unit_retail,
                     multi_units,
                     multi_unit_retail,
                     effective_date,
                     bulk_cc_pe_id,
                     thread_number,
                     parent_thread_number)
             values (rfrg.item,
                     rfrg.location,
                     rfrg.loc_type,
                     rfrg.price_change_id,
                     rfrg.selling_retail,
                     rfrg.multi_units,
                     rfrg.multi_unit_retail,
                     rfrg.action_date,
                     I_bulk_cc_pe_id,
                     L_thread_number,
                     L_parent_thread_number);
      end if;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_PC_MESSAGES;

--------------------------------------------------------

FUNCTION STAGE_CLR_MESSAGES(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                            I_transaction_id   IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                            I_bulk_cc_pe_id    IN     NUMBER,
                            I_price_event_type IN     VARCHAR2,
                            I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                            I_chunk_number     IN     NUMBER  DEFAULT 1)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_PUBLISH.STAGE_CLR_MESSAGES';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_price_event_payload_seq NUMBER(10) := NULL;
   L_pe_seq                  NUMBER(10) := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

   L_parent_thread_number NUMBER(10) := NULL;
   L_thread_number        NUMBER(10) := NULL;

   cursor C_THREAD_NUMBERS is
      select /*+ CARDINALITY(ids 10) */
             distinct parent_thread_number,
             thread_number
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.price_event_type = I_price_event_type;

BEGIN

   -- check if chunk_price_event_payload_id is present, else get RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL
   select NVL((select /*+ CARDINALITY(ids 10) */
                      chunk_price_event_payload_id
                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_bulk_cc_pe_thread rbcpt
                where rbcpt.price_event_id   = VALUE(ids)
                  and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
                  and rbcpt.price_event_type = I_price_event_type
                  and rownum                 = 1), RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL)
     into L_price_event_payload_seq
     from dual;

   open C_THREAD_NUMBERS;
   fetch C_THREAD_NUMBERS into L_parent_thread_number,
                               L_thread_number;
   close C_THREAD_NUMBERS;

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 location,
                                 dept)
      with clearances as
         (select /*+ MATERIALIZE USE_NL(rc) LEADING(IDS) */
                 VALUE(ids) price_event_id,
                 rc.item,
                 rc.skulist,
                 rc.price_event_itemlist,
                 rc.location,
                 rc.zone_id,
                 rc.zone_node_type,
                 rc.sys_generated_exclusion
            from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_clearance rc
           where rc.exception_parent_id = VALUE(ids)
             and rc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                            RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                            RPM_CONSTANTS.PC_PENDING_STATE_CODE,
                                            RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
             and rownum                 > 0)
       -- parent loc
       select /*+ USE_NL(im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              item_master im
        where im.item_parent    = rc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- item zone
       select /*+ USE_NL(im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              item_master im,
              rpm_zone_location rzl
        where im.item           = rc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id       = rc.zone_id
       union all
       -- item loc
       select /*+ USE_NL(im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              item_master im
        where im.item           = rc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - parent items
       select /*+ USE_NL(rcs, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_clearance_skulist rcs,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 0
          and rcs.skulist                        = rc.skulist
          and rcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
          and rcs.item                           = im.item_parent
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - tran items
       select /*+ USE_NL(rcs, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_clearance_skulist rcs,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 0
          and rcs.skulist                        = rc.skulist
          and rcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
          and rcs.item                           = im.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- peil loc - parent items
       select rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail rmld,
              item_master im
        where rc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = rc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and rmld.item               = im.item_parent
          and im.item_level           = im.tran_level
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- peil loc - tran items
       select rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail rmld,
              item_master im
        where rc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = rc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and rmld.item               = im.item
          and im.item_level           = im.tran_level
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list zone - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = rc.zone_id
       union all
       -- merch_list zone - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = rc.zone_id
       union all
       -- merch_list zone - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = rc.zone_id
       union all
       -- merch_list loc - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE);

   insert
      when rank = 1 and
         (I_chunk_number IN (0,
                             1) or
          clear_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number)
                          values (DECODE(clear_msg_type,
                                         RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD, L_pe_seq,
                                         L_price_event_payload_seq),
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                  clear_msg_type,
                                  I_bulk_cc_pe_id,
                                  L_parent_thread_number,
                                  L_thread_number)
      when 1 = 1 then
      into
         rpm_clearance_payload (clearance_payload_id,
                                price_event_payload_id,
                                clearance_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                selling_retail,
                                selling_retail_uom,
                                selling_retail_currency,
                                reset_clearance_id,
                                reset_date,
                                reset_indicator,
                                bulk_cc_pe_id)
                        values (RPM_CLEARANCE_PAYLOAD_SEQ.NEXTVAL,
                                DECODE(clear_msg_type,
                                       RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD, L_pe_seq,
                                       L_price_event_payload_seq),
                                clr_id,
                                item,
                                location,
                                loc_type,
                                action_date,
                                clear_retail,
                                clear_uom,
                                clear_retail_currency,
                                reset_clr_id,
                                reset_date,
                                reset_indicator,
                                I_bulk_cc_pe_id)
      select t2.location,
             t2.loc_type,
             t2.item,
             t2.action_date,
             DECODE(t2.clear_start_ind,
                    RPM_CONSTANTS.END_IND, t2.reset_clearance_id,
                    t2.clearance_id) clr_id,
             t2.clear_retail,
             t2.clear_uom,
             t2.clear_retail_currency,
             t2.clear_msg_type,
             DECODE(t2.reset_effective_date,
                    NULL, NULL,
                    t2.reset_clearance_id) reset_clr_id,
             t2.clear_start_ind,
             DECODE(t2.reset_indicator,
                    0, t2.reset_effective_date,
                    NULL) reset_date,
             t2.reset_indicator,
             t2.rank
        from (select t1.location,
                     t1.loc_type,
                     t1.item,
                     t1.action_date,
                     t1.clearance_id,
                     t1.clear_retail,
                     t1.clear_uom,
                     t1.clear_retail_currency,
                     t1.clear_msg_type,
                     t1.reset_clearance_id,
                     t1.clear_start_ind,
                     t1.reset_effective_date,
                     t1.reset_indicator,
                     RANK() OVER (PARTITION BY clear_msg_type
                                      ORDER BY location,
                                               clearance_id,
                                               item,
                                               action_date,
                                               future_retail_id) rank
                from (select location,
                             loc_type,
                             item,
                             action_date,
                             clearance_id,
                             clear_retail,
                             clear_uom,
                             clear_retail_currency,
                             clear_msg_type,
                             reset_clearance_id,
                             clear_start_ind,
                             future_retail_id,
                             reset_effective_date,
                             reset_indicator,
                             RANK() OVER (PARTITION BY item,
                                                       location,
                                                       action_date
                                              ORDER BY promo_dtl_id) rank
                        from (select gtt.location,
                                     DECODE(gtt.zone_node_type,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                                     gtt.item,
                                     gtt.action_date,
                                     gtt.clearance_id,
                                     gtt.clear_retail,
                                     gtt.clear_uom,
                                     gtt.clear_retail_currency,
                                     gtt.clear_msg_type,
                                     rc.clearance_id reset_clearance_id,
                                     gtt.clear_start_ind,
                                     gtt.future_retail_id,
                                     gtt.promo_dtl_id,
                                     ril.price_event_id ril_peid,
                                     rc.effective_date reset_effective_date,
                                     DECODE(gtt.clear_start_ind,
                                            RPM_CONSTANTS.END_IND, 1,
                                            0) reset_indicator
                                from rpm_fr_item_loc_expl_gtt gtt,
                                     rpm_clearance_gtt rc,
                                     (select distinct dept,
                                             item,
                                             location,
                                             price_event_id
                                        from rpm_item_loc_gtt
                                       where rownum > 0) ril
                               where gtt.clear_msg_type                   IN (RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
                                                                              RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD)
                                 and gtt.price_event_id                   = ril.price_event_id (+)
                                 and gtt.dept                             = ril.dept (+)
                                 and gtt.location                         = ril.location (+)
                                 and gtt.item                             = ril.item (+)
                                 and gtt.price_event_id                   = rc.price_event_id (+)
                                 and gtt.item                             = rc.item(+)
                                 and gtt.location                         = rc.location(+)
                                 and rc.reset_ind (+)                     = 1
                                 and NVL(rc.effective_date(+), LP_vdate) >= LP_vdate
                                 and NOT EXISTS (select 1
                                                   from rpm_clearance_payload rcp
                                                  where gtt.item            = rcp.item
                                                    and gtt.location        = rcp.location
                                                    and gtt.action_date     = rcp.effective_date
                                                    and gtt.clear_start_ind = RPM_CONSTANTS.END_IND
                                                    and rownum              = 1)) t
                       where t.ril_peid is NULL) t1
               where rank = 1) t2
       order by t2.clear_msg_type,
                t2.rank;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_CLR_MESSAGES;

--------------------------------------------------------

FUNCTION STAGE_PROM_MESSAGES(IO_error_table               IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_transaction_id             IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                             I_bulk_cc_pe_id              IN     NUMBER,
                             I_price_event_ids_to_process IN     OBJ_NUMERIC_ID_TABLE,
                             I_process_price_event_type   IN     VARCHAR2,
                             I_promo_type_to_process      IN     NUMBER,
                             I_msg_types_to_process       IN     VARCHAR2,
                             I_chunk_number               IN     NUMBER DEFAULT 1,
                             I_cancel_promo_dtl_ind       IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_PUBLISH.STAGE_PROM_MESSAGES';

   L_error_rec                  CONFLICT_CHECK_ERROR_REC := NULL;
   L_price_event_ids_to_process OBJ_NUMERIC_ID_TABLE     := NULL;
   L_price_event_payload_seq    NUMBER(10)               := NULL;
   L_parent_thread_number       NUMBER(10)               := NULL;
   L_thread_number              NUMBER(10)               := NULL;

   L_skip_cc_for_cp_approval RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_CP_APPROVAL%TYPE := NULL;
   L_simple_promo_overlap    RPM_SYSTEM_OPTIONS.SIMPLE_PROMO_OVERLAP_RULE%TYPE     :=  NULL;
   
   L_chunk_one_done       NUMBER := 0;
   L_chunk_in_progress    NUMBER := 0;    

   -- I_cancel_promo_dtl_ind is used to ensure that we do not send a mod message
   -- to the cancel item loc promo itself.  MOD message should only be sent when
   -- there is another overlapping promo that need update the selling retail value.
   cursor C_CHECK_FOR_PROMOS_TO_PUBLISH is
      select distinct rpile.promo_dtl_id
        from rpm_fr_item_loc_expl_gtt rpile
       where rpile.promo_comp_msg_type = I_msg_types_to_process
         and rpile.type                = I_promo_type_to_process
         and (   I_cancel_promo_dtl_ind      = 0
              or (    I_cancel_promo_dtl_ind = 1
                  and rpile.promo_dtl_id NOT IN (select VALUE(ids)
                                                   from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE))ids
                                                 union all
                                                 select /*+ CARDINALITY(ids 10) */ rpd.exception_parent_id
                                                   from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE))ids,
                                                        rpm_promo_dtl rpd
                                                  where rpd.promo_dtl_id = VALUE(ids))));

   cursor C_GET_THREAD_DATA is
      select /*+ CARDINALITY(ids 10) */
             parent_thread_number,
             thread_number,
             DECODE(I_msg_types_to_process,
                    RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD, RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL,
                    chunk_price_event_payload_id)
        from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_type = I_process_price_event_type
         and rownum                 = 1;

BEGIN

   select do_not_run_cc_for_cp_approval
     into L_skip_cc_for_cp_approval
     from rpm_system_options;
     
   select simple_promo_overlap_rule 
     into L_simple_promo_overlap
     from rpm_system_options;

   if I_promo_type_to_process = RPM_CONSTANTS.SIMPLE_CODE and
      I_msg_types_to_process = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD then
      ---
      open C_CHECK_FOR_PROMOS_TO_PUBLISH;
      fetch C_CHECK_FOR_PROMOS_TO_PUBLISH BULK COLLECT into L_price_event_ids_to_process;
      close C_CHECK_FOR_PROMOS_TO_PUBLISH;

   else
      L_price_event_ids_to_process := I_price_event_ids_to_process;
   end if;

   open C_GET_THREAD_DATA;
   fetch C_GET_THREAD_DATA into L_parent_thread_number,
                                L_thread_number,
                                L_price_event_payload_seq;
   close C_GET_THREAD_DATA;

   if L_price_event_payload_seq is NULL then
      L_price_event_payload_seq := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;
   end if;
   
   if I_chunk_number NOT IN (0,1) then
     while L_chunk_one_done = 0 loop
        select COUNT(1)
           into L_chunk_in_progress
         from rpm_bulk_cc_pe_chunk
        where bulk_cc_pe_id        = I_bulk_cc_pe_id
          and parent_thread_number = L_parent_thread_number
          and thread_number        = L_thread_number
          and chunk_number         = 1
          and status               = 'I';

        if L_chunk_in_progress > 0 then
           L_chunk_one_done := 0;
           dbms_lock.sleep(15);
        else 
           L_chunk_one_done := 1;
        end if;
     end loop;
   end if;    

   -- The if statement below is needed.  When this function is called to generate
   -- MOD Message, C_CHECK_FOR_PROMOS_TO_PUBLISH sometimes will return no data,
   -- this check will prevent the insertion into rpm_price_event_payload table

   if L_price_event_ids_to_process is NOT NULL and
      L_price_event_ids_to_process.COUNT > 0 then

      if I_chunk_number IN (0,
                            1) then
         insert into rpm_price_event_payload (price_event_payload_id,
                                              transaction_id,
                                              rib_family,
                                              rib_type,
                                              bulk_cc_pe_id,
                                              parent_thread_number,
                                              thread_number)
            values (L_price_event_payload_seq,
                    I_transaction_id,
                    RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                    I_msg_types_to_process,
                    I_bulk_cc_pe_id,
                    L_parent_thread_number,
                    L_thread_number);

         insert into rpm_promo_dtl_payload (promo_dtl_payload_id,
                                            price_event_payload_id,
                                            promo_id,
                                            promo_comp_id,
                                            promo_name,
                                            promo_desc,
                                            promo_comp_desc,
                                            promo_comp_type,
                                            customer_type,
                                            promo_dtl_id,
                                            start_date,
                                            end_date,
                                            apply_to_code,
                                            apply_order,
                                            exception_parent_id,
                                            threshold_id,
                                            discount_limit,
                                            thresh_qualification_type)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_PAYLOAD_SEQ.NEXTVAL,
                   L_price_event_payload_seq,
                   rp.promo_id,
                   rpc.promo_comp_id,
                   rp.name,
                   rp.description,
                   rpc.name,
                   rpc.type,
                   rpc.customer_type,
                   rpd.promo_dtl_id,
                   rpd.start_date,
                   rpd.end_date,
                   rpd.apply_to_code,
                   1, --APPLY_ORDER
                   rpd.exception_parent_id,
                   rpd.threshold_id,
                   rpd.discount_limit,
                   rt.qualification_type
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl rpd,
                   rpm_promo rp,
                   rpm_promo_comp rpc,
                   rpm_threshold rt
             where rpd.promo_dtl_id  = VALUE(ids)
               and rpd.promo_comp_id = rpc.promo_comp_id
               and rpc.promo_id      = rp.promo_id
               and rpd.threshold_id  = rt.threshold_id (+);

         insert into rpm_threshold_int_payload (threshold_int_payload_id,
                                                promo_dtl_payload_id,
                                                threshold_interval_id,
                                                threshold_id,
                                                threshold_type,
                                                threshold_amount,
                                                threshold_qty,
                                                change_type,
                                                change_amount,
                                                change_percent)
            select /*+ CARDINALITY(ids 10) */
                   RPM_THRESHOLD_INT_PAYLOAD_SEQ.NEXTVAL,
                   rpdp.promo_dtl_payload_id,
                   rti.threshold_interval_id,
                   rti.threshold_id,
                   rti.threshold_type,
                   rti.threshold_amount,
                   rti.threshold_qty,
                   rti.change_type,
                   rti.change_amount,
                   rti.change_percent
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_payload rpdp,
                   rpm_threshold_interval rti
             where rpdp.promo_dtl_id           = VALUE(ids)
               and rpdp.price_event_payload_id = L_price_event_payload_seq
               and rpdp.threshold_id           = rti.threshold_id;

         insert into rpm_promo_dtl_prc_rng_payload (promo_dtl_prc_rng_payload_id,
                                                    promo_dtl_payload_id,
                                                    buy_list_min,
                                                    buy_list_max,
                                                    get_list_min,
                                                    get_list_max)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PRC_RNG_PAYLOAD_SEQ.NEXTVAL,
                   rpdp.promo_dtl_payload_id,
                   rpdpr.buy_list_min,
                   rpdpr.buy_list_max,
                   rpdpr.get_list_min,
                   rpdpr.get_list_max
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_prc_range rpdpr,
                   rpm_promo_dtl_payload rpdp
             where rpdpr.promo_dtl_id          = VALUE(ids)
               and rpdpr.promo_dtl_id          = rpdp.promo_dtl_id
               and rpdp.price_event_payload_id = L_price_event_payload_seq;

         insert into rpm_promo_dtl_list_grp_payload (promo_dtl_list_grp_payload_id,
                                                     promo_dtl_payload_id,
                                                     promo_dtl_list_grp_id)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_LIST_GRP_PAY_SEQ.NEXTVAL,
                   pdp.promo_dtl_payload_id,
                   promo_dtl_list_grp_id
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_list_grp lgrp,
                   rpm_promo_dtl_payload pdp
             where lgrp.promo_dtl_id          = VALUE(ids)
               and pdp.promo_dtl_id           = lgrp.promo_dtl_id
               and pdp.price_event_payload_id = L_price_event_payload_seq;

         insert into rpm_promo_dtl_list_payload (promo_dtl_list_payload_id,
                                                 promo_dtl_list_grp_payload_id,
                                                 promo_dtl_list_id,
                                                 description,
                                                 reward_application)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_LIST_PAYLOAD_SEQ.NEXTVAL,
                   dlgp.promo_dtl_list_grp_payload_id,
                   promo_dtl_list_id,
                   description,
                   reward_application
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_list pdl,
                   rpm_promo_dtl_list_grp lgrp,
                   rpm_promo_dtl_list_grp_payload dlgp,
                   rpm_promo_dtl_payload pdp
             where lgrp.promo_dtl_id          = VALUE(ids)
               and pdl.promo_dtl_list_grp_id  = lgrp.promo_dtl_list_grp_id
               and dlgp.promo_dtl_list_grp_id = pdl.promo_dtl_list_grp_id
               and dlgp.promo_dtl_payload_id  = pdp.promo_dtl_payload_id
               and pdp.price_event_payload_id = L_price_event_payload_seq;

         insert into rpm_promo_dtl_mn_payload(promo_dtl_mn_payload_id,
                                              promo_dtl_payload_id,
                                              promo_dtl_list_payload_id,
                                              merch_type,
                                              dept,
                                              class,
                                              subclass,
                                              item,
                                              diff_id)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_MN_PAYLOAD_SEQ.NEXTVAL,
                   pdp.promo_dtl_payload_id,
                   pdlp.promo_dtl_list_payload_id,
                   pdmn.merch_type,
                   pdmn.dept,
                   pdmn.class,
                   pdmn.subclass,
                   pdmn.item,
                   pdmn.diff_id
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_merch_node pdmn,
                   rpm_promo_dtl_payload pdp,
                   rpm_promo_dtl_list_grp_payload pdlgp,
                   rpm_promo_dtl_list_payload pdlp
             where pdmn.promo_dtl_id                  = VALUE(ids)
               and pdmn.promo_dtl_list_id             = pdlp.promo_dtl_list_id
               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
               and pdp.price_event_payload_id         = L_price_event_payload_seq;

         insert into rpm_promo_item_payload (promo_item_payload_id,
                                             promo_dtl_list_payload_id,
                                             item)
            with txn_dtl_exclusions as
               (-- merch hierarchy
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                   RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                   RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                   and im.dept                 = rpdmn.dept
                   and im.class                = NVL(rpdmn.class, im.class)
                   and im.subclass             = NVL(rpdmn.subclass, im.subclass)
                union all
                -- skulist parent item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       rpm_promo_dtl_skulist rpds,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   and rpds.item_level         = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                   and rpds.price_event_id     = rpdmn.promo_dtl_id
                   and rpdmn.skulist           = rpds.skulist
                   and im.item_parent          = rpds.item
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                union all
                -- skulist tran item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       rpm_promo_dtl_skulist rpds,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                   and rpds.item_level         = RPM_CONSTANTS.IL_ITEM_LEVEL
                   and rpds.price_event_id     = rpdmn.promo_dtl_id
                   and rpdmn.skulist           = rpds.skulist
                   and im.item                 = rpds.item
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                union all
                -- peil parent item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       rpm_merch_list_detail rmld,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   and rmld.merch_list_id      = rpdmn.price_event_itemlist
                   and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                   and im.item_parent          = rmld.item
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                union all
                -- peil tran item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       rpm_merch_list_detail rmld,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                   and rmld.merch_list_id      = rpdmn.price_event_itemlist
                   and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                   and im.item_parent          = rmld.item
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                union all
                -- Parent item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                   and im.item_parent          = rpdmn.item
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                union all
                -- Parent Diff item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.PARENT_ITEM_DIFF
                   and im.item_parent          = rpdmn.item
                   and (   im.diff_1 = rpdmn.diff_id
                        or im.diff_2 = rpdmn.diff_id
                        or im.diff_3 = rpdmn.diff_id
                        or im.diff_4 = rpdmn.diff_id)
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                union all
                -- Tran item
                select /*+ CARDINALITY(ids 10) */
                       rpd.exception_parent_id promo_dtl_id,
                       im.item
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       item_master im
                 where I_promo_type_to_process = RPM_CONSTANTS.TRANSACTION_CODE
                   and rpd.exception_parent_id = VALUE(ids)
                   and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                   and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                   and im.item                 = rpdmn.item
                   and im.item_level           = im.tran_level
                   and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                   and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES)
            select RPM_PROMO_ITEM_PAYLOAD_SEQ.NEXTVAL,
                   promo_dtl_list_payload_id,
                   item
              from (select distinct item,
                           promo_dtl_list_payload_id
                      from (-- Transaction level items
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   item_master im
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                               and im.item                            = pdmn.item
                               and im.item_level                      = im.tran_level
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and rownum                             > 0
                            -- Item Parent Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                               and im.item_parent                     = pdmn.item
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- Parent Diff Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.PARENT_ITEM_DIFF
                               and im.item_parent                     = pdmn.item
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and (   im.diff_1 = pdmn.diff_id
                                    or im.diff_2 = pdmn.diff_id
                                    or im.diff_3 = pdmn.diff_id
                                    or im.diff_4 = pdmn.diff_id)
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- ItemList Tran Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   rpm_promo_dtl_skulist pds,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                               and pds.price_event_id                 = pdmn.promo_dtl_id
                               and pds.skulist                        = pdmn.skulist
                               and pds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                               and im.item                            = pds.item
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- ItemList Parent Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   rpm_promo_dtl_skulist pds,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                               and pds.price_event_id                 = pdmn.promo_dtl_id
                               and pds.skulist                        = pdmn.skulist
                               and pds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                               and im.item_parent                     = pds.item
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- PEIL Tran Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   rpm_merch_list_detail rmld,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                               and rmld.merch_list_id                 = pdmn.price_event_itemlist
                               and rmld.merch_level                   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                               and im.item                            = rmld.item
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- PEIL Parent Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   rpm_merch_list_detail rmld,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                               and rmld.merch_list_id                 = pdmn.price_event_itemlist
                               and rmld.merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                               and im.item_parent                     = rmld.item
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- Merch Hierarchy Level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   item_master im,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                          RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and im.dept                            = pdmn.dept
                               and im.class                           = NVL(pdmn.class, im.class)
                               and im.subclass                        = NVL(pdmn.subclass, im.subclass)
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- Storewide Level (Tran promo) - store level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   distinct im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   item_master im,
                                   item_loc il,
                                   rpm_promo_zone_location pzl,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and pdp.promo_dtl_id                   = pzl.promo_dtl_id
                               and pzl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                               and pzl.location                       = il.loc
                               and im.item                            = il.item
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0
                            -- Storewide Level (Tran promo) - zone level
                            union all
                            select /*+ CARDINALITY(ids 10) */
                                   distinct im.item,
                                   pdlp.promo_dtl_list_payload_id
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_merch_node pdmn,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_dtl_list_grp_payload pdlgp,
                                   rpm_promo_dtl_list_payload pdlp,
                                   rpm_promo_dtl_list pdl,
                                   rpm_promo_dtl_list_grp pdlg,
                                   item_master im,
                                   item_loc il,
                                   rpm_promo_zone_location pzl,
                                   rpm_zone_location rzl,
                                   txn_dtl_exclusions tde
                             where pdmn.promo_dtl_id                  = VALUE(ids)
                               and pdmn.merch_type                    = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                               and im.item_level                      = im.tran_level
                               and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and pdp.promo_dtl_id                   = pzl.promo_dtl_id
                               and rzl.loc_type                       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                               and pzl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                               and rzl.zone_id                        = pzl.zone_id
                               and im.item                            = il.item
                               and rzl.location                       = il.loc
                               and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                               and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                               and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                               and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                               and pdp.price_event_payload_id         = L_price_event_payload_seq
                               and pdmn.promo_dtl_id                  = tde.promo_dtl_id (+)
                               and im.item                            = tde.item (+)
                               and tde.item                           is NULL
                               and rownum                             > 0));

         insert into rpm_promo_disc_ldr_payload (promo_disc_ldr_payload_id,
                                                 promo_dtl_list_payload_id,
                                                 change_type,
                                                 change_amount,
                                                 change_currency,
                                                 change_percent,
                                                 change_selling_uom,
                                                 qual_type,
                                                 qual_value)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DISC_LDR_PAYLOAD_SEQ.NEXTVAL,
                   pdlp.promo_dtl_list_payload_id,
                   change_type,
                   change_amount,
                   change_currency,
                   change_percent,
                   change_selling_uom,
                   DECODE(change_type,
                          RPM_CONSTANTS.RETAIL_EXCLUDE, NULL,
                          qual_type),
                   DECODE(change_type,
                          RPM_CONSTANTS.RETAIL_EXCLUDE, NULL,
                          qual_value)
              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_disc_ladder lad,
                   rpm_promo_dtl_list pdl,
                   rpm_promo_dtl_list_payload pdlp,
                   rpm_promo_dtl_list_grp pdlg,
                   rpm_promo_dtl_payload pdp,
                   rpm_promo_dtl_list_grp_payload pdlgp
             where pdlg.promo_dtl_id                  = VALUE(ids)
               and pdlg.promo_dtl_list_grp_id         = pdl.promo_dtl_list_grp_id
               and lad.promo_dtl_list_id              = pdl.promo_dtl_list_id
               and pdl.promo_dtl_list_id              = pdlp.promo_dtl_list_id
               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
               and pdp.price_event_payload_id         = L_price_event_payload_seq;

      end if;

      if L_skip_cc_for_cp_approval = 0 then

         -- Populate rpm_item_loc_gtt table for all possible item/loc combination that is excluded from promotion.
         -- This table is then later used to join with rpm_fr_item_loc_expl_gtt
         -- in order to populate the rpm_promo_item_loc_sr_payload by checking the price_event_id column.
         -- If the price_event_id column is NULL, this means that the item/loc combination is on promotion,
         -- and this is the data that we want to insert into the payload table.
         -- Also, when populating rpm_item_loc_gtt table, we only check zone level for tran item because
         -- front end only allow the user to exclude tran level at zone level.

         delete from rpm_item_loc_gtt;

         insert into rpm_item_loc_gtt (price_event_id,
                                       item,
                                       location,
                                       dept)
            with promo_dtl as
               (select /*MATERIALIZED USE_NL(rpd, rpdmn) USE_NL(rpd, rpzl) LEADING(ids) */
                       VALUE(ids) price_event_id,
                       rpdmn.merch_type,
                       rpdmn.dept,
                       rpdmn.class,
                       rpdmn.subclass,
                       rpdmn.item,
                       rpdmn.skulist,
                       rpdmn.price_event_itemlist,
                       rpzl.location,
                       rpzl.zone_id,
                       rpzl.zone_node_type,
                       rpd.sys_generated_exclusion
                  from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_promo_dtl rpd,
                       rpm_promo_dtl_merch_node rpdmn,
                       rpm_promo_zone_location rpzl
                 where rpd.exception_parent_id = VALUE(ids)
                   and rpd.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                   RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                   RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                                   RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                                   RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                                   RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE)
                   and (   I_msg_types_to_process           != RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
                        or (    I_msg_types_to_process       = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
                            and rpd.cancel_il_promo_dtl_ind != 1))
                  and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
                  and rpd.promo_dtl_id        = rpzl.promo_dtl_id
                  and rpzl.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE))
            -- parent loc
            select /*+ USE_NL(im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   item_master im
             where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
               and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and im.item_parent     = rpd.item
               and im.item_level      = im.tran_level
               and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- item zone
            select /*+ USE_NL(im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   item_master im,
                   rpm_zone_location rzl
             where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
               and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
               and im.item            = rpd.item
               and im.item_level      = im.tran_level
               and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and rzl.zone_id        = rpd.zone_id
               and rzl.loc_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            union all
            -- item loc
            select /*+ USE_NL(im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   item_master im
             where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
               and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and im.item            = rpd.item
               and im.item_level      = im.tran_level
               and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- skulist loc - parent items
            select /*+ USE_NL(rpds, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_promo_dtl_skulist rpds,
                   item_master im
             where NVL(rpd.sys_generated_exclusion, 0) = 0
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and rpds.skulist                        = rpd.skulist
               and rpds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
               and rpds.item                           = im.item_parent
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- skulist loc - tran items
            select /*+ USE_NL(rpds, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_promo_dtl_skulist rpds,
                   item_master im
             where NVL(rpd.sys_generated_exclusion, 0) = 0
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and rpds.skulist                        = rpd.skulist
               and rpds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
               and rpds.item                           = im.item
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- peil loc - parent items
            select rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail rmld,
                   item_master im
             where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
               and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and rmld.merch_list_id = rpd.price_event_itemlist
               and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
               and rmld.item          = im.item_parent
               and im.item_level      = im.tran_level
               and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- peil loc - tran items
            select rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail rmld,
                   item_master im
             where rpd.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
               and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and rmld.merch_list_id = rpd.price_event_itemlist
               and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
               and rmld.item          = im.item
               and im.item_level      = im.tran_level
               and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- merch_list zone - parent items
            select /*+ USE_NL(mld, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rzl.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail mld,
                   item_master im,
                   rpm_zone_location rzl
             where NVL(rpd.sys_generated_exclusion, 0) = 1
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
               and mld.merch_list_id                   = rpd.skulist
               and mld.merch_level                     = RPM_CONSTANTS.PARENT_MERCH_TYPE
               and im.item_parent                      = mld.item
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and rzl.zone_id                         = rpd.zone_id
               and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            union all
            -- merch_list zone - parent/diff items
            select /*+ USE_NL(mld, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rzl.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail mld,
                   item_master im,
                   rpm_zone_location rzl
             where NVL(rpd.sys_generated_exclusion, 0) = 1
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
               and mld.merch_list_id                   = rpd.skulist
               and mld.merch_level                     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
               and im.item_parent                      = mld.item
               and im.diff_1                           = mld.diff_id
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and rzl.zone_id                         = rpd.zone_id
               and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            union all
            -- merch_list zone - tran items
            select /*+ USE_NL(mld, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rzl.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail mld,
                   item_master im,
                   rpm_zone_location rzl
             where NVL(rpd.sys_generated_exclusion, 0) = 1
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
               and mld.merch_list_id                   = rpd.skulist
               and mld.merch_level                     = RPM_CONSTANTS.ITEM_MERCH_TYPE
               and im.item                             = mld.item
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and rzl.zone_id                         = rpd.zone_id
               and rzl.loc_type                        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            union all
            -- merch_list loc - parent items
            select /*+ USE_NL(mld, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail mld,
                   item_master im
             where NVL(rpd.sys_generated_exclusion, 0) = 1
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and mld.merch_list_id                   = rpd.skulist
               and mld.merch_level                     = RPM_CONSTANTS.PARENT_MERCH_TYPE
               and im.item_parent                      = mld.item
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- merch_list loc - parent/diff items
            select /*+ USE_NL(mld, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail mld,
                   item_master im
             where NVL(rpd.sys_generated_exclusion, 0) = 1
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and mld.merch_list_id                   = rpd.skulist
               and mld.merch_level                     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
               and im.item_parent                      = mld.item
               and im.diff_1                           = mld.diff_id
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- merch_list loc - tran items
            select /*+ USE_NL(mld, im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   rpm_merch_list_detail mld,
                   item_master im
             where NVL(rpd.sys_generated_exclusion, 0) = 1
               and rpd.merch_type                      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
               and rpd.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and mld.merch_list_id                   = rpd.skulist
               and mld.merch_level                     = RPM_CONSTANTS.ITEM_MERCH_TYPE
               and im.item                             = mld.item
               and im.item_level                       = im.tran_level
               and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            union all
            -- merch hier loc
            select /*+ USE_NL(im) ORDERED */
                   rpd.price_event_id,
                   im.item,
                   rpd.location,
                   im.dept
              from promo_dtl rpd,
                   item_master im
             where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                          RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                          RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
               and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
               and im.dept            = rpd.dept
               and im.class           = NVL(rpd.class, im.class)
               and im.subclass        = NVL(rpd.subclass, im.subclass)
               and im.item_level      = im.tran_level
               and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

      insert into rpm_promo_item_loc_sr_payload (promo_item_loc_sr_payload_id,
                                                 promo_dtl_payload_id,
                                                 item,
                                                 location,
                                                 selling_retail,
                                                 selling_retail_currency,
                                                 selling_uom,
                                                 effective_date,
                                                 ref_promo_dtl_id)
         select RPM_PROMO_ITEM_LOC_SR_PAY_SEQ.NEXTVAL,
                pdp.promo_dtl_payload_id,
                gtt.item,
                gtt.location,
                gtt.simple_promo_retail,
                gtt.simple_promo_retail_currency,
                gtt.simple_promo_uom,
                gtt.action_date,
                gtt.ref_promo_dtl_id
           from rpm_promo_dtl_payload pdp,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                (select ril_peid,
                        promo_dtl_id,
                        item,
                        location,
                        action_date,
                        simple_promo_retail,
                        simple_promo_uom,
                        simple_promo_retail_currency,
                        zone_node_type,
                        persist_ind,
                        clearance_id,
                        clear_start_ind,
                        ref_promo_dtl_id
                   from (select /*+ CARDINALITY(ids 10) */
                                ROW_NUMBER() OVER (PARTITION BY fr.item,
                                                                fr.location,
                                                                fr.promo_dtl_id
                                                       ORDER BY fr.action_date) rn,
                                ril.price_event_id ril_peid,
                                fr.promo_dtl_id,
                                fr.item,
                                fr.location,
                                fr.action_date,
                                fr.simple_promo_retail,
                                fr.simple_promo_uom,
                                fr.simple_promo_retail_currency,
                                fr.zone_node_type,
                                0 persist_ind,
                                fr.clearance_id,
                                fr.clear_start_ind,
                                case 
                                   when L_simple_promo_overlap = 0 then 
                                      NVL(fr.ref_promo_dtl_id, fr.promo_dtl_id)
                                   else
                                      null
                                end ref_promo_dtl_id
                           from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_fr_item_loc_expl_gtt fr,
                                (select distinct dept,
                                        item,
                                        location,
                                        price_event_id
                                   from rpm_item_loc_gtt
                                  where rownum > 0) ril
                          where fr.promo_dtl_id   = VALUE(ids)
                            and fr.customer_type  is NULL
                            and fr.action_date    BETWEEN fr.detail_start_date and NVL(fr.detail_end_date, TO_DATE('3000', 'YYYY'))
                            and fr.price_event_id = ril.price_event_id (+)
                            and fr.dept           = ril.dept (+)
                            and fr.location       = ril.location (+)
                            and fr.item           = ril.item (+)
                            and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE  
                            and rownum            > 0
                         union all
                         select /*+ CARDINALITY(ids 10) */
                                ROW_NUMBER() OVER (PARTITION BY cspf.item,
                                                                cspf.location,
                                                                cspf.promo_dtl_id
                                                       ORDER BY cspf.action_date) rn,
                                ril.price_event_id ril_peid,
                                cspf.promo_dtl_id,
                                cspf.item,
                                cspf.location,
                                cspf.action_date,
                                cspf.promo_retail simple_promo_retail,
                                cspf.promo_uom simple_promo_uom,
                                cspf.simple_promo_retail_currency,
                                cspf.zone_node_type,
                                0 persist_ind,
                                cspf.clearance_id,
                                cspf.clear_start_ind,
                                case 
                                   when L_simple_promo_overlap = 0 then 
                                      NVL(cspf.ref_promo_dtl_id, cspf.promo_dtl_id)
                                   else
                                      null
                                end ref_promo_dtl_id
                           from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_fr_item_loc_expl_gtt cspf,
                                (select distinct dept,
                                        item,
                                        location,
                                        price_event_id,
                                        item_parent
                                   from rpm_item_loc_gtt
                                  where rownum > 0) ril
                          where cspf.promo_dtl_id   = VALUE(ids)
                            and cspf.customer_type  is NOT NULL
                            and cspf.action_date    BETWEEN cspf.detail_start_date and NVL(cspf.detail_end_date, TO_DATE('3000', 'YYYY'))
                            and cspf.price_event_id = ril.price_event_id (+)
                            and cspf.dept           = ril.dept (+)
                            and cspf.location       = ril.location (+)
                            and cspf.item           = ril.item (+)
                            and cspf.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE 
                            and rownum              > 0)
                         model
                            partition by (promo_dtl_id,
                                          item,
                                          location,
                                          zone_node_type)
                            dimension by (rn)
                            measures (simple_promo_retail,
                                      simple_promo_uom,
                                      simple_promo_retail_currency,
                                      persist_ind,
                                      action_date,
                                      ril_peid,
                                      clearance_id,
                                      clear_start_ind,
                                      ref_promo_dtl_id)
                            rules (persist_ind [rn] order by rn =
                                      case
                                         when CV(rn) = 1 then
                                            1
                                         else
                                            case
                                               when simple_promo_retail[CV(rn)] != simple_promo_retail[CV(rn) - 1] or
                                                    simple_promo_uom[CV(rn)] != simple_promo_uom[CV(rn) - 1] or
                                                    simple_promo_retail_currency[CV(rn)] != simple_promo_retail_currency[CV(rn) - 1] then
                                                  1
                                               else
                                                  0
                                         end
                                      end)) gtt
          where persist_ind                = 1
            and rpd.promo_dtl_id           = gtt.promo_dtl_id
            and rpc.promo_comp_id          = rpd.promo_comp_id
            and rpd.promo_dtl_id           = pdp.promo_dtl_id
            and pdp.price_event_payload_id = L_price_event_payload_seq
            and gtt.ril_peid               is NULL
            and (   rpd.apply_to_code                 = RPM_CONSTANTS.REGULAR_AND_CLEARANCE
                 or (    rpd.apply_to_code            = RPM_CONSTANTS.REGULAR_ONLY
                     and (   gtt.clearance_id         is NULL
                          or (    gtt.clearance_id    is NOT NULL
                              and gtt.clear_start_ind IN (RPM_CONSTANTS.END_IND,
                                                          RPM_CONSTANTS.START_END_IND)
                              and rpd.end_date        > gtt.action_date)))
                      or (    rpd.apply_to_code       = RPM_CONSTANTS.CLEARANCE_ONLY
                          and (    gtt.clearance_id   is NOT NULL
                               and gtt.clear_start_ind NOT IN (RPM_CONSTANTS.END_IND,
                                                               RPM_CONSTANTS.START_END_IND))));

         insert into rpm_promo_location_payload (promo_location_payload_id,
                                                 promo_dtl_payload_id,
                                                 location,
                                                 location_type)
            select RPM_PROMO_LOCATION_PAYLOAD_SEQ.NEXTVAL,
                   promo_dtl_payload_id,
                   location,
                   loc_type
              from (select location,
                           loc_type,
                           promo_dtl_payload_id
                      from (select /*+ CARDINALITY(ids 10) */
                                   rpilsp.location location,
                                   RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                                   pdp.promo_dtl_payload_id,
                                   ROW_NUMBER() OVER (PARTITION BY rpilsp.location,
                                                                   rpilsp.promo_dtl_payload_id
                                                          ORDER BY rpilsp.location) rnk
                              from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                                   rpm_promo_dtl_payload pdp,
                                   rpm_promo_item_loc_sr_payload rpilsp,
                                	 rpm_zone_location rzl
                             where pdp.promo_dtl_id           = VALUE(ids)
                               and pdp.price_event_payload_id = L_price_event_payload_seq
                               and pdp.promo_dtl_payload_id   = rpilsp.promo_dtl_payload_id
                               and rpilsp.location            = rzl.location
                            	 and rzl.loc_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE 
                               and rownum                     > 0)
                     where rnk = 1);

      else

         insert into rpm_promo_location_payload (promo_location_payload_id,
                                                 promo_dtl_payload_id,
                                                 location,
                                                 location_type)
            select RPM_PROMO_LOCATION_PAYLOAD_SEQ.NEXTVAL,
                   promo_dtl_payload_id,
                   location,
                   loc_type
              from (select /*+ CARDINALITY(ids 10) */
                           pzl.location location,
                           RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                           pdp.promo_dtl_payload_id
                      from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_zone_location pzl
                     where pzl.promo_dtl_id           = VALUE(ids)
                       and pzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                       and pdp.promo_dtl_id           = pzl.promo_dtl_id
                       and pdp.price_event_payload_id = L_price_event_payload_seq
                       and rownum                     > 0
                    -- Zone level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           rzl.location location,
                           RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                           pdp.promo_dtl_payload_id
                      from table(cast(L_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_zone_location pzl,
                           rpm_zone_location rzl
                     where pzl.promo_dtl_id           = VALUE(ids)
                       and pzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                       and rzl.zone_id                = pzl.zone_id
                       and pdp.promo_dtl_id           = pzl.promo_dtl_id
                       and pdp.price_event_payload_id = L_price_event_payload_seq
                       and rzl.loc_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                       and rownum                     > 0);

      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));

      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_PROM_MESSAGES;

--------------------------------------------------------
FUNCTION STAGE_PROM_CIL_MESSAGES(IO_error_table               IN OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_transaction_id             IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                 I_price_event_ids_to_process IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(61)             := 'RPM_CC_PUBLISH.STAGE_PROM_CIL_MESSAGES';
   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_curr_pe_pay_seq RPM_PRICE_EVENT_PAYLOAD.PRICE_EVENT_PAYLOAD_ID%TYPE := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

BEGIN

   insert into rpm_price_event_payload (price_event_payload_id,
                                        transaction_id,
                                        rib_family,
                                        rib_type)
      values (L_curr_pe_pay_seq,
              I_transaction_id,
              RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
              RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CIL);

   insert into rpm_promo_dtl_cil_payload (promo_dtl_cil_payload_id,
                                          price_event_payload_id,
                                          promo_id,
                                          promo_comp_id,
                                          promo_dtl_id,
                                          promo_comp_type,
                                          exception_parent_id,
                                          apply_to_code,
                                          cancellation_date)
      select /*+ CARDINALITY(ids 10) */
             PROMO_DTL_CIL_PAYLOAD_SEQ.NEXTVAL,
             L_curr_pe_pay_seq,
             rp.promo_id,
             rpc.promo_comp_id,
             rpd.promo_dtl_id,
             rpc.type,
             rpd.exception_parent_id,
             rpd.apply_to_code,
             rpd.end_date
        from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo rp,
             rpm_promo_comp rpc
       where rpd.promo_dtl_id            = VALUE(ids)
         and rpd.cancel_il_promo_dtl_ind = 1
         and rpd.promo_comp_id           = rpc.promo_comp_id
         and rpc.promo_id                = rp.promo_id;

   insert into rpm_promo_dtl_cil_item_payload (promo_dtl_cil_item_payload_id,
                                               promo_dtl_cil_payload_id,
                                               item)
      select /*+ CARDINALITY(ids 10) */
             PROMO_DTL_CIL_ITEM_PAYLOAD_SEQ.NEXTVAL,
             promo_dtl_cil_payload_id,
             item
        from ( -- Item Level
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     item_master im
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpdmn.merch_type             = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpd.promo_dtl_id             = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and im.item                      = rpdmn.item
                 and im.item_level                = im.tran_level
                 and im.status                    = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind              = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- Parent Item Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     item_master im
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpdmn.merch_type             = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpd.promo_dtl_id             = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and im.item_parent               = rpdmn.item
                 and im.item_level                = im.tran_level
                 and im.status                    = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind              = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- Parent Item Diff Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     item_master im
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpdmn.merch_type             = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and rpd.promo_dtl_id             = rpdmn.promo_dtl_id
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and im.item_parent               = rpdmn.item
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and im.item_level                = im.tran_level
                 and im.status                    = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind              = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- ItemList Tran Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     rpm_promo_dtl_skulist rpds,
                     item_master im
               where rpd.promo_dtl_id                    = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind         = 1
                 and NVL(rpd.sys_generated_exclusion, 0) = 0
                 and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id        = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id                  = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id                  = VALUE(ids)
                 and rpdmn.promo_dtl_id                  = rpds.price_event_id
                 and rpdmn.skulist                       = rpds.skulist
                 and rpds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item                             = rpds.item
                 and im.item_level                       = im.tran_level
                 and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- ItemList Parent Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     rpm_promo_dtl_skulist rpds,
                     item_master im
               where rpd.promo_dtl_id                    = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind         = 1
                 and NVL(rpd.sys_generated_exclusion, 0) = 0
                 and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id        = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id                  = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id                  = VALUE(ids)
                 and rpdmn.promo_dtl_id                  = rpds.price_event_id
                 and rpdmn.skulist                       = rpds.skulist
                 and rpds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent                      = rpds.item
                 and im.item_level                       = im.tran_level
                 and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- Price Event Item List (PEIL) - Tran Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpdmn.merch_type             = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpd.promo_dtl_id             = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and rpdmn.price_event_itemlist   = rmld.merch_list_id
                 and rmld.merch_level             = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item                      = rmld.item
                 and im.item_level                = im.tran_level
                 and im.status                    = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind              = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- Price Event Item List (PEIL) - Parent Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpdmn.merch_type             = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpd.promo_dtl_id             = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and rpdmn.price_event_itemlist   = rmld.merch_list_id
                 and rmld.merch_level             = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent               = rmld.item
                 and im.item_level                = im.tran_level
                 and im.status                    = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind              = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- Merch Hierachy Level
              union all
              select /*+ CARDINALITY (ids 10) */
                     im.item,
                     rpdcp.promo_dtl_cil_payload_id
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_cil_payload rpdcp,
                     item_master im
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpdmn.merch_type             IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                      RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                      RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and rpd.promo_dtl_id             = rpdmn.promo_dtl_id
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and im.dept                      = rpdmn.dept
                 and im.class                     = NVL(rpdmn.class, im.class)
                 and im.subclass                  = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level                = im.tran_level
                 and im.status                    = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind              = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES);

   insert into rpm_promo_dtl_cil_loc_payload (promo_dtl_cil_loc_payload_id,
                                              promo_dtl_cil_payload_id,
                                              location,
                                              location_type)
      select PROMO_DTL_CIL_LOC_PAYLOAD_SEQ.NEXTVAL,
             promo_dtl_cil_payload_id,
             location,
             location_type
        from (-- Store level
              select /*+ CARDINALITY(ids 10) */
                     rpdcp.promo_dtl_cil_payload_id,
                     rpzl.location,
                     RPM_CONSTANTS.LOCATION_TYPE_STORE location_type
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_cil_payload rpdcp
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpd.promo_dtl_id             = rpzl.promo_dtl_id
                 and rpzl.zone_node_type          = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id
              -- Zone level
              union all
              select /*+ CARDINALITY(ids 10) */
                     rpdcp.promo_dtl_cil_payload_id,
                     rzl.location,
                     RPM_CONSTANTS.LOCATION_TYPE_STORE location_type
                from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_location rzl,
                     rpm_promo_dtl_cil_payload rpdcp
               where rpd.promo_dtl_id             = VALUE(ids)
                 and rpd.cancel_il_promo_dtl_ind  = 1
                 and rpd.promo_dtl_id             = rpzl.promo_dtl_id
                 and rpzl.zone_node_type          = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpzl.zone_id                 = rzl.zone_id
                 and rzl.loc_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpdcp.price_event_payload_id = L_curr_pe_pay_seq
                 and rpdcp.promo_dtl_id           = VALUE(ids)
                 and rpdcp.promo_dtl_id           = rpd.promo_dtl_id);

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));

      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_PROM_CIL_MESSAGES;

--------------------------------------------------------
FUNCTION STAGE_FINANCE_PROM_MESSAGES(IO_error_table               IN OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_transaction_id             IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                     I_price_event_ids_to_process IN     OBJ_NUMERIC_ID_TABLE,
                                     I_price_event_type           IN     VARCHAR2 DEFAULT RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION)
RETURN NUMBER IS

   L_program   VARCHAR2(50)             := 'RPM_CC_PUBLISH.STAGE_FINANCE_PROM_MESSAGES';
   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_curr_pe_pay_seq RPM_PRICE_EVENT_PAYLOAD.PRICE_EVENT_PAYLOAD_ID%TYPE := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

BEGIN

   if I_price_event_ids_to_process is NOT NULL and
      I_price_event_ids_to_process.COUNT > 0 then

      insert into rpm_price_event_payload (price_event_payload_id,
                                           transaction_id,
                                           rib_family,
                                           rib_type)
         values(L_curr_pe_pay_seq,
                I_transaction_id,
                RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                case
                   when I_price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD then
                      RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD
                   else
                      RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE
                end);

      insert into rpm_promo_dtl_payload (promo_dtl_payload_id,
                                         price_event_payload_id,
                                         promo_id,
                                         promo_comp_id,
                                         promo_name,
                                         promo_desc,
                                         promo_comp_desc,
                                         promo_comp_type,
                                         customer_type,
                                         promo_dtl_id,
                                         start_date,
                                         end_date,
                                         apply_to_code,
                                         apply_order,
                                         exception_parent_id,
                                         threshold_id)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_DTL_PAYLOAD_SEQ.NEXTVAL,
                L_curr_pe_pay_seq,
                rp.promo_id,
                rpc.promo_comp_id,
                rp.name,
                rp.description,
                rpc.name,
                rpc.type,
                rpc.customer_type,
                rpd.promo_dtl_id,
                rpd.start_date,
                rpd.end_date,
                rpd.apply_to_code,
                1, --APPLY_ORDER
                rpd.exception_parent_id,
                rpd.threshold_id
           from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl rpd,
                rpm_promo rp,
                rpm_promo_comp rpc
          where rpd.promo_dtl_id  = VALUE(ids)
            and rpd.promo_comp_id = rpc.promo_comp_id
            and rpc.promo_id      = rp.promo_id;

      insert into rpm_fin_cred_dtl_payload (fin_cred_dtl_payload_id,
                                            promo_dtl_payload_id,
                                            financial_dtl_id,
                                            card_type,
                                            bin_from_value,
                                            bin_to_value,
                                            commission_rate,
                                            comments)
         select /*+ CARDINALITY(ids 10) */
                RPM_FIN_CRED_DTL_PAYLOAD_SEQ.NEXTVAL,
                rpdp.promo_dtl_payload_id,
                rfd.financial_dtl_id,
                rfd.card_type,
                rfd.bin_from_value,
                rfd.bin_to_value,
                rpcd.commission_rate,
                rpcd.comments
           from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl_payload rpdp,
                rpm_promo_credit_dtl rpcd,
                rpm_financial_dtl rfd
          where rpdp.promo_dtl_id           = VALUE(ids)
            and rpcd.promo_dtl_id           = VALUE(ids)
            and rpdp.promo_dtl_id           = rpcd.promo_dtl_id
            and rpdp.price_event_payload_id = L_curr_pe_pay_seq
            and rpcd.financial_dtl_id       = rfd.financial_dtl_id;

      insert into rpm_promo_dtl_list_grp_payload (promo_dtl_list_grp_payload_id,
                                                  promo_dtl_payload_id,
                                                  promo_dtl_list_grp_id)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_DTL_LIST_GRP_PAY_SEQ.NEXTVAL,
                pdp.promo_dtl_payload_id,
                promo_dtl_list_grp_id
           from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl_list_grp lgrp,
                rpm_promo_dtl_payload pdp
          where lgrp.promo_dtl_id          = VALUE(ids)
            and pdp.promo_dtl_id           = lgrp.promo_dtl_id
            and pdp.price_event_payload_id = L_curr_pe_pay_seq;

      insert into rpm_promo_dtl_list_payload (promo_dtl_list_payload_id,
                                              promo_dtl_list_grp_payload_id,
                                              promo_dtl_list_id,
                                              description,
                                              reward_application)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_DTL_LIST_PAYLOAD_SEQ.NEXTVAL,
                dlgp.promo_dtl_list_grp_payload_id,
                promo_dtl_list_id,
                description,
                reward_application
           from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl_list pdl,
                rpm_promo_dtl_list_grp lgrp,
                rpm_promo_dtl_list_grp_payload dlgp,
                rpm_promo_dtl_payload pdp
          where lgrp.promo_dtl_id          = VALUE(ids)
            and pdl.promo_dtl_list_grp_id  = lgrp.promo_dtl_list_grp_id
            and dlgp.promo_dtl_list_grp_id = pdl.promo_dtl_list_grp_id
            and dlgp.promo_dtl_payload_id  = pdp.promo_dtl_payload_id
            and pdp.price_event_payload_id = L_curr_pe_pay_seq;

      insert into gtt_num_num_str_str_date_date
         (number_1,
          varchar2_1)
         (--
          -- Tran Item Level
          --
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 item_master im
           where pdmn.promo_dtl_id = VALUE(ids)
             and pdmn.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
             and im.item           = pdmn.item
             and im.item_level     = im.tran_level
             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          --
          -- Item Parent Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 item_master im
           where pdmn.promo_dtl_id = VALUE(ids)
             and pdmn.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
             and im.item_parent    = pdmn.item
             and im.item_level     = im.tran_level
             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          --
          -- Parent Diff Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 item_master im
           where pdmn.promo_dtl_id = VALUE(ids)
             and pdmn.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
             and im.item_parent    = pdmn.item
             and im.item_level     = im.tran_level
             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and (   im.diff_1 = pdmn.diff_id
                  or im.diff_2 = pdmn.diff_id
                  or im.diff_3 = pdmn.diff_id
                  or im.diff_4 = pdmn.diff_id)
          --
          -- Skulist Tran Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 rpm_promo_dtl_skulist rpds,
                 item_master im
           where pdmn.promo_dtl_id = VALUE(ids)
             and pdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and pdmn.promo_dtl_id = rpds.price_event_id
             and pdmn.skulist      = rpds.skulist
             and rpds.item_level   = RPM_CONSTANTS.IL_ITEM_LEVEL
             and im.item           = rpds.item
             and im.item_level     = im.tran_level
             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          --
          -- Skulist Parent Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 rpm_promo_dtl_skulist rpds,
                 item_master im
           where pdmn.promo_dtl_id = VALUE(ids)
             and pdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and pdmn.promo_dtl_id = rpds.price_event_id
             and pdmn.skulist      = rpds.skulist
             and rpds.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
             and im.item_parent    = rpds.item
             and im.item_level     = im.tran_level
             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          --
          -- PEIL Tran Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 rpm_merch_list_detail rmld,
                 item_master im
           where pdmn.promo_dtl_id  = VALUE(ids)
             and pdmn.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id = pdmn.price_event_itemlist
             and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
             and rmld.item          = im.item
             and im.item_level      = im.tran_level
             and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          --
          -- PEIL Parent Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 rpm_merch_list_detail rmld,
                 item_master im
           where pdmn.promo_dtl_id  = VALUE(ids)
             and pdmn.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id = pdmn.price_event_itemlist
             and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
             and rmld.item          = im.item_parent
             and im.item_level      = im.tran_level
             and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          --
          -- Merch Hierarchy Level
          --
          union all
          select /*+ CARDINALITY(ids 10) */
                 pdmn.promo_dtl_id,
                 im.item
            from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl_merch_node pdmn,
                 item_master im
           where pdmn.promo_dtl_id = VALUE(ids)
             and pdmn.merch_type   IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                       RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                       RPM_CONSTANTS.DEPT_LEVEL_ITEM)
             and im.item_level     = im.tran_level
             and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.dept           = pdmn.dept
             and im.class          = NVL(pdmn.class, im.class)
             and im.subclass       = NVL(pdmn.subclass, im.subclass))
          minus
          (--
           -- Tran Item Level
           --
           select /*+ CARDINALITY(ids 10) */
                  rpd.exception_parent_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node rpdmn,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and im.item                 = rpdmn.item
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and rpdmn.promo_dtl_id      = rpd.promo_dtl_id
              and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
           union all
           --
           -- Parent Item Level
           --
           select /*+ CARDINALITY(ids 10) */
                  rpd.exception_parent_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node rpdmn,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and rpdmn.promo_dtl_id      = rpd.promo_dtl_id
              and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent          = rpdmn.item
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           --
           -- Parent Diff Level
           --
           select /*+ CARDINALITY(ids 10) */
                  rpd.exception_parent_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node rpdmn,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and rpdmn.promo_dtl_id      = rpd.promo_dtl_id
              and rpdmn.merch_type        = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and (   im.diff_1 = rpdmn.diff_id
                   or im.diff_2 = rpdmn.diff_id
                   or im.diff_3 = rpdmn.diff_id
                   or im.diff_4 = rpdmn.diff_id)
           union all
           --
           -- Skulist Tran Level
           --
           select /*+ CARDINALITY(ids 10) */
                  pdmn.promo_dtl_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node pdmn,
                  rpm_promo_dtl_skulist rpds,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and pdmn.promo_dtl_id       = rpd.promo_dtl_id
              and pdmn.merch_type         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and pdmn.promo_dtl_id       = rpds.price_event_id
              and pdmn.skulist            = rpds.skulist
              and rpds.item_level         = RPM_CONSTANTS.IL_ITEM_LEVEL
              and im.item                 = rpds.item
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           --
           -- Skulist Parent Level
           --
           union all
           select /*+ CARDINALITY(ids 10) */
                  pdmn.promo_dtl_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node pdmn,
                  rpm_promo_dtl_skulist rpds,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and pdmn.promo_dtl_id       = rpd.promo_dtl_id
              and pdmn.merch_type         = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and pdmn.promo_dtl_id       = rpds.price_event_id
              and pdmn.skulist            = rpds.skulist
              and rpds.item_level         = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
              and im.item_parent          = rpds.item
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           --
           -- PEIL Tran Level
           --
           union all
           select /*+ CARDINALITY(ids 10) */
                  pdmn.promo_dtl_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node pdmn,
                  rpm_merch_list_detail rmld,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and pdmn.promo_dtl_id       = rpd.promo_dtl_id
              and pdmn.merch_type         = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rmld.merch_list_id      = pdmn.price_event_itemlist
              and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
              and rmld.item               = im.item
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           --
           -- PEIL Parent Level
           --
           union all
           select /*+ CARDINALITY(ids 10) */
                  pdmn.promo_dtl_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node pdmn,
                  rpm_merch_list_detail rmld,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and pdmn.promo_dtl_id       = rpd.promo_dtl_id
              and pdmn.merch_type         = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rmld.merch_list_id      = pdmn.price_event_itemlist
              and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
              and rmld.item               = im.item_parent
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           --
           -- Merch Hierarchy Level
           --
           select /*+ CARDINALITY(ids 10) */
                  rpd.exception_parent_id,
                  im.item
             from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_merch_node rpdmn,
                  item_master im
            where rpd.exception_parent_id = VALUE(ids)
              and rpdmn.promo_dtl_id      = rpd.promo_dtl_id
              and rpdmn.merch_type        IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                              RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                              RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and im.item_parent          = rpdmn.item
              and im.item_level           = im.tran_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and im.dept                 = rpdmn.dept
              and im.class                = NVL(rpdmn.class, im.class)
              and im.subclass             = NVL(rpdmn.subclass, im.subclass));

      insert into rpm_promo_item_payload (promo_item_payload_id,
                                          promo_dtl_list_payload_id,
                                          item)
         select RPM_PROMO_ITEM_PAYLOAD_SEQ.NEXTVAL,
                pdlp.promo_dtl_list_payload_id,
                gtt.varchar2_1
           from gtt_num_num_str_str_date_date gtt,
                rpm_promo_dtl_list pdl,
                rpm_promo_dtl_list_grp pdlg,
                rpm_promo_dtl_list_payload pdlp,
                rpm_promo_dtl_payload pdp,
                rpm_promo_dtl_list_grp_payload pdlgp
          where pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
            and pdlg.promo_dtl_id                  = gtt.number_1
            and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
            and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
            and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
            and pdp.price_event_payload_id         = L_curr_pe_pay_seq;

      insert into rpm_promo_disc_ldr_payload (promo_disc_ldr_payload_id,
                                              promo_dtl_list_payload_id,
                                              change_type,
                                              change_amount,
                                              change_currency,
                                              change_percent,
                                              change_selling_uom,
                                              qual_type,
                                              qual_value,
                                              duration)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_DISC_LDR_PAYLOAD_SEQ.NEXTVAL,
                pdlp.promo_dtl_list_payload_id,
                change_type,
                change_amount,
                change_currency,
                change_percent,
                change_selling_uom,
                qual_type,
                qual_value,
                duration
           from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl_payload pdp,
                rpm_promo_dtl_list_grp_payload dlgp,
                rpm_promo_dtl_list_payload pdlp,
                rpm_promo_dtl_disc_ladder pddl
          where pdp.promo_dtl_id                   = VALUE(ids)
            and pdp.price_event_payload_id         = L_curr_pe_pay_seq
            and pdp.promo_dtl_payload_id           = dlgp.promo_dtl_payload_id
            and dlgp.promo_dtl_list_grp_payload_id = pdlp.promo_dtl_list_grp_payload_id
            and pdlp.promo_dtl_list_id             = pddl.promo_dtl_list_id;

      insert into rpm_promo_location_payload (promo_location_payload_id,
                                              promo_dtl_payload_id,
                                              location,
                                              location_type)
         select RPM_PROMO_LOCATION_PAYLOAD_SEQ.NEXTVAL,
                promo_dtl_payload_id,
                location,
                loc_type
           from (--
                 -- Location level
                 --
                 select /*+ CARDINALITY(ids 10) */
                        pzl.location,
                        DECODE(pzl.zone_node_type,
                               RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                        pdp.promo_dtl_payload_id
                   from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                        rpm_promo_zone_location pzl,
                        rpm_promo_dtl_payload pdp
                  where pzl.promo_dtl_id           = VALUE(ids)
                    and pzl.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                    and pdp.promo_dtl_id           = pzl.promo_dtl_id
                    and pdp.price_event_payload_id = L_curr_pe_pay_seq
                 --
                 -- Zone level
                 --
                 union all
                 select /*+ CARDINALITY(ids 10) */
                        rzl.location,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                        pdp.promo_dtl_payload_id
                   from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                        rpm_promo_zone_location pzl,
                        rpm_promo_dtl_payload pdp,
                        rpm_zone_location rzl
                  where pzl.promo_dtl_id           = VALUE(ids)
                    and pzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                    and rzl.zone_id                = pzl.zone_id
                    and pdp.promo_dtl_id           = pzl.promo_dtl_id
                    and pdp.price_event_payload_id = L_curr_pe_pay_seq
                    and rzl.loc_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                    and rzl.location               NOT IN (select location
                                                             from rpm_promo_dtl rpd1,
                                                                  rpm_promo_zone_location rpzl1
                                                            where rpd1.exception_parent_id = VALUE(ids)
                                                              and rpzl1.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                              and rpzl1.promo_dtl_id       = rpd1.promo_dtl_id));

      insert into rpm_promo_fin_dtl_payload(promo_fin_dtl_payload_id,
                                            promo_dtl_payload_id,
                                            finance_dtl_id,
                                            card_type,
                                            bin_from_value,
                                            bin_to_value,
                                            commission_rate,
                                            comments)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_FIN_DTL_PAYLOAD_SEQ.NEXTVAL,
                rpdp.promo_dtl_payload_id,
                rfd.financial_dtl_id,
                rfd.card_type,
                rfd.bin_from_value,
                rfd.bin_to_value,
                rpcd.commission_rate,
                rpcd.comments
           from table(cast(I_price_event_ids_to_process as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl rpd,
                rpm_promo_credit_dtl rpcd,
                rpm_financial_dtl rfd,
                rpm_promo_dtl_payload rpdp
          where rpdp.promo_dtl_id           = VALUE(ids)
            and rpd.promo_dtl_id            = VALUE(ids)
            and rpdp.price_event_payload_id = L_curr_pe_pay_seq
            and rpd.promo_dtl_id            = rpcd.promo_dtl_id
            and rpcd.financial_dtl_id       = rfd.financial_dtl_id;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));

      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_FINANCE_PROM_MESSAGES;
--------------------------------------------------------

FUNCTION STAGE_PC_REMOVE_MESSAGES(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_transaction_id   IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                  I_bulk_cc_pe_id    IN     NUMBER,
                                  I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE,
                                  I_chunk_number     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CC_PUBLISH.STAGE_PC_REMOVE_MESSAGES';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_price_event_payload_seq NUMBER(10) := NULL;
   L_pe_seq                  NUMBER(10) := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

   L_parent_thread_number NUMBER(10) := NULL;
   L_thread_number        NUMBER(10) := NULL;

   cursor C_THREAD_NUMBERS is
      select /*+ CARDINALITY(ids 10) */
             distinct parent_thread_number,
             thread_number
        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE;

BEGIN

   -- check if chunk_price_event_payload_id is present, else get RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL
   select NVL((select /*+ CARDINALITY(ids 10) */
                      chunk_price_event_payload_id
                 from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_bulk_cc_pe_thread rbcpt
                where rbcpt.price_event_id   = VALUE(ids)
                  and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
                  and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                  and rownum                 = 1), RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL)
     into L_price_event_payload_seq
     from dual;

   open C_THREAD_NUMBERS;
   fetch C_THREAD_NUMBERS into L_parent_thread_number,
                               L_thread_number;
   close C_THREAD_NUMBERS;

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 location,
                                 dept)
      with price_change as
         (select /*MATERIALIZED USE_NL(pc) LEADING(IDS) */
                 VALUE(ids) price_event_id,
                 pc.item,
                 pc.skulist,
                 pc.price_event_itemlist,
                 pc.link_code,
                 pc.location,
                 pc.zone_id,
                 pc.zone_node_type,
                 pc.sys_generated_exclusion
            from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_price_change pc
          where pc.exception_parent_id = VALUE(ids)
            and pc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                           RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                           RPM_CONSTANTS.PC_PENDING_STATE_CODE,
                                           RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
            and rownum                 > 0)
       -- parent loc
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              item_master im
        where im.item_parent    = pc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- item zone
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from price_change pc,
              item_master im,
              rpm_zone_location rzl
        where im.item           = pc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id       = pc.zone_id
       union all
       -- item loc
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              item_master im
        where im.item           = pc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - parent items
       select /*+ USE_NL(pcs, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_price_change_skulist pcs,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 0
          and pcs.skulist                        = pc.skulist
          and pcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
          and pcs.item                           = im.item_parent
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - tran items
       select /*+ USE_NL(pcs, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_price_change_skulist pcs,
              item_master im
        where NVL(pc.sys_generated_exclusion, 0) = 0
          and pcs.skulist                        = pc.skulist
          and pcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
          and pcs.item                           = im.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- peil loc - parent items
       select pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail rmld,
              item_master im
        where pc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = pc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and rmld.item               = im.item_parent
          and im.item_level           = im.tran_level
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- peil loc - tran items
       select pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail rmld,
              item_master im
        where pc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = pc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and rmld.item               = im.item
          and im.item_level           = im.tran_level
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list zone - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = pc.zone_id
       union all
       -- merch_list zone - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = pc.zone_id
       union all
       -- merch_list zone - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = pc.zone_id
       union all
       -- merch_list loc - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              pc.price_event_id,
              im.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_merch_list_detail mld,
              item_master im
        where pc.skulist                         is NOT NULL
          and NVL(pc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = pc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and pc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- link code zone
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              rlca.item,
              rzl.location,
              im.dept
         from price_change pc,
              rpm_link_code_attribute rlca,
              item_master im,
              rpm_zone_location rzl
        where pc.link_code      is NOT NULL
          and pc.link_code      = rlca.link_code
          and im.item           = rlca.item
          and pc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id       = pc.zone_id
       -- link code loc
       union all
       select /*+ USE_NL(im) ORDERED */
              pc.price_event_id,
              rlca.item,
              pc.location,
              im.dept
         from price_change pc,
              rpm_link_code_attribute rlca,
              item_master im
        where pc.link_code      is NOT NULL
          and pc.link_code      = rlca.link_code
          and im.item           = rlca.item
          and pc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE);

   insert
      when rank = 1 and
         I_chunk_number IN (0,
                            1) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number)
                          values (L_price_event_payload_seq,
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_DEL,
                                  I_bulk_cc_pe_id,
                                  L_parent_thread_number,
                                  L_thread_number)
      when 1 = 1 then
      into
         rpm_price_chg_payload (price_chg_payload_id,
                                price_event_payload_id,
                                price_change_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                selling_unit_change_ind,
                                multi_unit_change_ind,
                                bulk_cc_pe_id)
                        values (RPM_PRICE_CHG_PAYLOAD_SEQ.NEXTVAL,
                                L_price_event_payload_seq,
                                price_event_id,
                                item,
                                location,
                                loc_type,
                                effective_date,
                                0,
                                0,
                                I_bulk_cc_pe_id)
      select t2.price_event_id,
             t2.location,
             t2.loc_type,
             t2.item,
             t2.rank,
             t2.effective_date
        from (select t1.price_event_id,
                     t1.location,
                     t1.loc_type,
                     t1.item,
                     t1.effective_date,
                     RANK() OVER (ORDER BY t1.location,
                                           t1.price_event_id,
                                           t1.item) rank
                from (select /*+ CARDINALITY(ids 10) */
                             distinct gtt.price_event_id,
                             gtt.location,
                             DECODE(gtt.zone_node_type,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                             gtt.item,
                             rpc.effective_date,
                             ril.price_event_id ril_peid
                        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                             rpm_fr_item_loc_expl_gtt gtt,
                             rpm_price_change rpc,
                             (select distinct price_event_id,
                                     item,
                                     location,
                                     dept
                                from rpm_item_loc_gtt
                               where rownum > 0) ril
                       where gtt.price_event_id = VALUE(ids)
                         and gtt.price_event_id = rpc.price_change_id
                         and rpc.change_type   != RPM_CONSTANTS.RETAIL_EXCLUDE
                         and gtt.customer_type  is NULL
                         and gtt.price_event_id = ril.price_event_id (+)
                         and gtt.item           = ril.item (+)
                         and gtt.dept           = ril.dept (+)
                         and gtt.location       = ril.location (+)) t1
               where t1.ril_peid is NULL) t2
       order by t2.rank;

   insert
      when rank = 1 and
         I_chunk_number IN (0,
                            1) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number)
                          values (L_pe_seq,
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                  pc_msg_type,
                                  I_bulk_cc_pe_id,
                                  L_parent_thread_number,
                                  L_thread_number)
      when 1 = 1 then
      into
         rpm_price_chg_payload (price_chg_payload_id,
                                price_event_payload_id,
                                price_change_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                selling_unit_change_ind,
                                selling_retail,
                                selling_retail_uom,
                                selling_retail_currency,
                                multi_unit_change_ind,
                                multi_units,
                                multi_units_retail,
                                multi_units_uom,
                                multi_units_currency,
                                bulk_cc_pe_id)
                       values  (RPM_PRICE_CHG_PAYLOAD_SEQ.NEXTVAL,
                                L_pe_seq,
                                price_change_id,
                                item,
                                location,
                                loc_type,
                                action_date,
                                pc_selling_retail_ind,
                                selling_retail,
                                selling_uom,
                                selling_retail_currency,
                                pc_multi_unit_ind,
                                multi_units,
                                multi_unit_retail,
                                multi_selling_uom,
                                multi_unit_retail_currency,
                                I_bulk_cc_pe_id)
      select t2.location,
             t2.loc_type,
             t2.pc_msg_type,
             t2.item,
             t2.price_change_id,
             t2.action_date,
             t2.pc_selling_retail_ind,
             t2.selling_retail,
             t2.selling_uom,
             t2.selling_retail_currency,
             t2.pc_multi_unit_ind,
             t2.multi_units,
             t2.multi_unit_retail,
             t2.multi_selling_uom,
             t2.multi_unit_retail_currency,
             t2.rank
       from (select t1.location,
                    t1.loc_type,
                    t1.pc_msg_type,
                    t1.item,
                    t1.price_change_id,
                    t1.action_date,
                    NVL(t1.pc_selling_retail_ind, 0) pc_selling_retail_ind,
                    t1.selling_retail,
                    t1.selling_uom,
                    t1.selling_retail_currency,
                    NVL(t1.pc_multi_unit_ind, 0) pc_multi_unit_ind,
                    t1.multi_units,
                    t1.multi_unit_retail,
                    t1.multi_selling_uom,
                    t1.multi_unit_retail_currency,
                    ROW_NUMBER() OVER (PARTITION BY pc_msg_type
                                           ORDER BY item,
                                                    location,
                                                    action_date,
                                                    price_change_id,
                                                    future_retail_id) rank
               from (select location,
                            loc_type,
                            pc_msg_type,
                            item,
                            price_change_id,
                            action_date,
                            pc_selling_retail_ind,
                            selling_retail,
                            selling_uom,
                            selling_retail_currency,
                            pc_multi_unit_ind,
                            multi_units,
                            multi_unit_retail,
                            multi_selling_uom,
                            multi_unit_retail_currency,
                            future_retail_id,
                            RANK() OVER (PARTITION BY item,
                                                      location,
                                                      action_date
                                             ORDER BY promo_dtl_id) rank
                     from(select gtt.location,
                                 DECODE(zone_node_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                                 pc_msg_type,
                                 gtt.item,
                                 price_change_id,
                                 action_date,
                                 pc_selling_retail_ind,
                                 selling_retail,
                                 selling_uom,
                                 selling_retail_currency,
                                 pc_multi_unit_ind,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_units,
                                        NULL) multi_units,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_unit_retail,
                                        NULL) multi_unit_retail,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_selling_uom,
                                        NULL) multi_selling_uom,
                                 DECODE(pc_multi_unit_ind,
                                        1, multi_unit_retail_currency,
                                        NULL) multi_unit_retail_currency,
                                 future_retail_id,
                                 promo_dtl_id,
                                 ril.price_event_id ril_peid
                            from rpm_fr_item_loc_expl_gtt gtt,
                                 (select distinct
                                         price_event_id,
                                         item,
                                         dept,
                                         location
                                    from rpm_item_loc_gtt
                                   where rownum > 0) ril
                           where pc_msg_type            = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_MOD
                             and ril.price_event_id (+) = gtt.price_event_id
                             and ril.dept           (+) = gtt.dept
                             and ril.location       (+) = gtt.location
                             and ril.item           (+) = gtt.item) t
                    where t.ril_peid is NULL) t1
              where rank = 1) t2
         order by t2.pc_msg_type,
                  t2.rank;

   forall i IN 1 .. I_price_change_ids.COUNT
      delete
        from rpm_pc_ticket_request
       where price_change_id = I_price_change_ids(i);

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_PC_REMOVE_MESSAGES;

--------------------------------------------------------

FUNCTION STAGE_LM_SCRUB_MESSAGES(IO_error_table   IN OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_transaction_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                 I_old_zone_id    IN     RPM_ZONE.ZONE_ID%TYPE,
                                 I_location       IN     RPM_ZONE_LOCATION.LOCATION%TYPE,
                                 I_loc_type       IN     RPM_ZONE_LOCATION.LOC_TYPE%TYPE,
                                 I_move_date      IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CC_PUBLISH.STAGE_LM_SCRUB_MESSAGES';

   L_error_rec  CONFLICT_CHECK_ERROR_REC := NULL;
   L_start_time TIMESTAMP                := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_old_zone_id: '|| I_old_zone_id ||
                                       ' - I_location: '|| I_location ||
                                       ' - I_loc_type: '|| I_loc_type ||
                                       ' - I_move_date: '|| I_move_date);

   --price changes
   insert
      when rank = 1 then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type)
                          values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1),
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_DEL)
      when 1 = 1 then
      into
         rpm_price_chg_payload (price_chg_payload_id,
                                price_event_payload_id,
                                price_change_id,
                                item,
                                location,
                                location_type,
                                selling_unit_change_ind,
                                multi_unit_change_ind)
                        values (RPM_PRICE_CHG_PAYLOAD_SEQ.NEXTVAL,
                                RPM_CC_PUBLISH.GET_PAYLOAD_ID(0),
                                price_change_id,
                                item,
                                location,
                                loc_type,
                                0,
                                0)
      select t2.location,
             t2.loc_type,
             t2.item,
             t2.price_change_id,
             t2.rank
        from (select t1.location,
                     t1.loc_type,
                     t1.item,
                     t1.price_change_id,
                     RANK() OVER (ORDER BY t1.price_change_id,
                                           t1.item) rank
                from (select I_location location,
                             DECODE(I_loc_type,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH,
                                    RPM_CONSTANTS.LOCATION_TYPE_STORE) loc_type,
                             gtt.item,
                             pc.price_change_id
                        from rpm_fr_item_loc_expl_gtt gtt,
                             rpm_price_change pc
                       where pc.price_change_id = gtt.price_change_id
                         and pc.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and pc.zone_id         = I_old_zone_id
                         and gtt.action_date   >= I_move_date
                         and gtt.customer_type  is NULL) t1) t2
       order by t2.rank;

   --clearances
   insert
      when rank = 1 then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type)
                          values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1),
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_DEL)
      when 1 = 1 then
      into
         rpm_clearance_payload (clearance_payload_id,
                                price_event_payload_id,
                                clearance_id,
                                item,
                                location,
                                location_type,
                                reset_date,
                                reset_indicator)
                        values (RPM_CLEARANCE_PAYLOAD_SEQ.NEXTVAL,
                                RPM_CC_PUBLISH.GET_PAYLOAD_ID(0),
                                clearance_id,
                                item,
                                location,
                                loc_type,
                                reset_date,
                                0)
      select t2.location,
             t2.loc_type,
             t2.item,
             t2.clearance_id,
             t2.reset_date,
             t2.rank
        from (select t1.location,
                     t1.loc_type,
                     t1.item,
                     t1.clearance_id,
                     t1.reset_date,
                     RANK() OVER (ORDER BY t1.clearance_id,
                                           t1.item) rank
                from (select I_location location,
                             DECODE(I_loc_type,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH,
                                    RPM_CONSTANTS.LOCATION_TYPE_STORE) loc_type,
                             gtt.item,
                             cl.clearance_id,
                             cr.effective_date reset_date
                        from rpm_fr_item_loc_expl_gtt gtt,
                             rpm_clearance cl,
                             rpm_clearance_reset cr
                       where cl.clearance_id      = gtt.clearance_id
                         and cl.zone_node_type    = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and cl.zone_id           = I_old_zone_id
                         and gtt.clear_start_ind  = RPM_CONSTANTS.START_IND
                         and gtt.action_date     >= I_move_date
                         and gtt.customer_type    is NULL
                         and cr.item              = gtt.item
                         and cr.location          = I_location
                         and cr.state             = RPM_CONSTANTS.PC_APPROVED_STATE_CODE) t1) t2
       order by t2.rank;

   --promotions
   insert all
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type)
                          values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1),
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL)
      into
         rpm_promo_dtl_payload (promo_dtl_payload_id,
                                price_event_payload_id,
                                promo_id,
                                promo_comp_id,
                                promo_name,
                                promo_desc,
                                promo_comp_desc,
                                promo_comp_type,
                                customer_type,
                                promo_dtl_id,
                                start_date,
                                end_date,
                                exception_parent_id,
                                apply_to_code,
                                discount_limit,
                                thresh_qualification_type)
                        values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1, 'RPM_PROMO_DTL_PAYLOAD_SEQ'),
                                RPM_CC_PUBLISH.GET_PAYLOAD_ID(0, 'RPM_PRICE_EVENT_PAYLOAD_SEQ'),
                                promo_id,
                                promo_comp_id,
                                promo_name,
                                promo_desc,
                                promo_comp_desc,
                                type,
                                customer_type,
                                detail_id,
                                start_date,
                                end_date,
                                exception_parent_id,
                                apply_to_code,
                                discount_limit,
                                thresh_qualification_type)
      into
         rpm_promo_dtl_list_grp_payload (promo_dtl_list_grp_payload_id,
                                         promo_dtl_payload_id,
                                         promo_dtl_list_grp_id)
                                 values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1, 'RPM_PROMO_DTL_LIST_GRP_PAY_SEQ'),
                                         RPM_CC_PUBLISH.GET_PAYLOAD_ID(0, 'RPM_PROMO_DTL_PAYLOAD_SEQ'),
                                         promo_dtl_list_grp_id)
      into
         rpm_promo_dtl_list_payload (promo_dtl_list_payload_id,
                                     promo_dtl_list_grp_payload_id,
                                     promo_dtl_list_id)
                             values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1, 'RPM_PROMO_DTL_LIST_PAYLOAD_SEQ'),
                                     RPM_CC_PUBLISH.GET_PAYLOAD_ID(0, 'RPM_PROMO_DTL_LIST_GRP_PAY_SEQ'),
                                     promo_dtl_list_id)
      into
         rpm_promo_item_payload (promo_item_payload_id,
                                 promo_dtl_list_payload_id,
                                 item)
                         values (RPM_PROMO_ITEM_PAYLOAD_SEQ.NEXTVAL,
                                 RPM_CC_PUBLISH.GET_PAYLOAD_ID(0, 'RPM_PROMO_DTL_LIST_PAYLOAD_SEQ'),
                                 item)
      into
         rpm_promo_location_payload (promo_location_payload_id,
                                     promo_dtl_payload_id,
                                     location,
                                     location_type)
                             values (RPM_PROMO_LOCATION_PAYLOAD_SEQ.NEXTVAL,
                                     RPM_CC_PUBLISH.GET_PAYLOAD_ID(0, 'RPM_PROMO_DTL_PAYLOAD_SEQ'),
                                     location,
                                     loc_type)
      into
         rpm_promo_dtl_prc_rng_payload (promo_dtl_prc_rng_payload_id,
                                        promo_dtl_payload_id,
                                        buy_list_min,
                                        buy_list_max,
                                        get_list_min,
                                        get_list_max)
                                values (RPM_PRC_RNG_PAYLOAD_SEQ.NEXTVAL,
                                        RPM_CC_PUBLISH.GET_PAYLOAD_ID(0, 'RPM_PROMO_DTL_PAYLOAD_SEQ'),
                                        buy_list_min,
                                        buy_list_max,
                                        get_list_min,
                                        get_list_max)
      select /*+ LEADING(gtt) */
             I_location location,
             DECODE(I_loc_type,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH,
                    RPM_CONSTANTS.LOCATION_TYPE_STORE) loc_type,
             gtt.item,
             pdl.promo_dtl_list_id,
             pdl.promo_dtl_list_grp_id,
             gtt.exception_parent_id,
             rp.start_date start_date,
             rp.end_date end_date,
             rp.name promo_name,
             rp.description promo_desc,
             rpc.name promo_comp_desc,
             rpc.type,
             rpc.customer_type,
             rp.promo_id,
             rpc.promo_comp_id,
             rpd.promo_dtl_id detail_id,
             rpd.apply_to_code,
             rpd.discount_limit,
             rt.qualification_type thresh_qualification_type,
             rpdpr.buy_list_min,
             rpdpr.buy_list_max,
             rpdpr.get_list_min,
             rpdpr.get_list_max
        from rpm_fr_item_loc_expl_gtt gtt,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_list pdl,
             rpm_promo_dtl_list_grp pdlg,
             rpm_promo rp,
             rpm_promo_comp rpc,
             rpm_promo_dtl rpd,
             rpm_threshold rt,
             rpm_promo_dtl_prc_range rpdpr
       where gtt.type                   IN (RPM_CONSTANTS.SIMPLE_CODE,
                                            RPM_CONSTANTS.THRESHOLD_CODE,
                                            RPM_CONSTANTS.COMPLEX_CODE)
         and gtt.action_date            = gtt.detail_start_date
         and rpzl.promo_dtl_id          = gtt.promo_dtl_id
         and rpzl.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpzl.zone_id               = I_old_zone_id
         and gtt.action_date           >= I_move_date
         and gtt.promo_id               = rp.promo_id
         and rpc.promo_id               = rp.promo_id
         and rpd.promo_comp_id          = rpc.promo_comp_id
         and rpd.promo_dtl_id           = gtt.promo_dtl_id
         and rpd.promo_dtl_id           = pdlg.promo_dtl_id
         and pdlg.promo_dtl_list_grp_id = pdl.promo_dtl_list_grp_id
         and rpd.threshold_id           = rt.threshold_id (+)
         and rpd.promo_dtl_id           = rpdpr.promo_dtl_id (+);

   LOGGER.LOG_TIME(L_program ||' - I_old_zone_id: '|| I_old_zone_id ||
                               ' - I_location: '|| I_location ||
                               ' - I_loc_type: '|| I_loc_type ||
                               ' - I_move_date: '|| I_move_date,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_LM_SCRUB_MESSAGES;

--------------------------------------------------------

FUNCTION STAGE_CLR_REMOVE_MESSAGES(IO_error_table   IN OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_transaction_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                   I_bulk_cc_pe_id  IN     NUMBER,
                                   I_clearance_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_chunk_number   IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CC_PUBLISH.STAGE_CLR_REMOVE_MESSAGES';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_price_event_payload_seq NUMBER(10) := NULL;

   L_parent_thread_number NUMBER(10) := NULL;
   L_thread_number        NUMBER(10) := NULL;

   cursor C_THREAD_NUMBERS is
      select /*+ CARDINALITY(ids 10) */
             distinct parent_thread_number,
             thread_number
        from table(cast(I_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET);

BEGIN

   -- check if chunk_price_event_payload_id is present, else get RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL
   select NVL((select /*+ CARDINALITY(ids 10) */
                      chunk_price_event_payload_id
                 from table(cast(I_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_bulk_cc_pe_thread rbcpt
                where rbcpt.price_event_id   = VALUE(ids)
                  and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
                  and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                 RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                 RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET)
                  and rownum                 = 1), RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL)
     into L_price_event_payload_seq
     from dual;

   open C_THREAD_NUMBERS;
   fetch C_THREAD_NUMBERS into L_parent_thread_number,
                               L_thread_number;
   close C_THREAD_NUMBERS;

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 location,
                                 dept)
      with clearances as
         (select /*+ MATERIALIZE USE_NL(rc) LEADING(IDS) */
                 VALUE(ids) price_event_id,
                 rc.item,
                 rc.skulist,
                 rc.price_event_itemlist,
                 rc.location,
                 rc.zone_id,
                 rc.zone_node_type,
                 rc.sys_generated_exclusion
            from table(cast(I_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_clearance rc
           where rc.exception_parent_id = VALUE(ids)
             and rc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                            RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                            RPM_CONSTANTS.PC_PENDING_STATE_CODE,
                                            RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
             and rownum                 > 0)
       -- parent loc
       select /*+ USE_NL(im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              item_master im
        where im.item_parent    = rc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- item zone
       select /*+ USE_NL(im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              item_master im,
              rpm_zone_location rzl
        where im.item           = rc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id       = rc.zone_id
       union all
       -- item loc
       select /*+ USE_NL(im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              item_master im
        where im.item           = rc.item
          and im.item_level     = im.tran_level
          and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - parent items
       select /*+ USE_NL(rcs, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_clearance_skulist rcs,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 0
          and rcs.skulist                        = rc.skulist
          and rcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
          and rcs.item                           = im.item_parent
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- skulist loc - tran items
       select /*+ USE_NL(rcs, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_clearance_skulist rcs,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 0
          and rcs.skulist                        = rc.skulist
          and rcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
          and rcs.item                           = im.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- Price Event Itemlist - parent items
       select /*+ USE_NL(rcs, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail rmld,
              item_master im
        where rc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = rc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and rmld.item               = im.item_parent
          and im.item_level           = im.tran_level
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- Price Event Itemlist - tran items
       select rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail rmld,
              item_master im
        where rc.price_event_itemlist is NOT NULL
          and rmld.merch_list_id      = rc.price_event_itemlist
          and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and rmld.item               = im.item
          and im.item_level           = im.tran_level
          and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list zone - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = rc.zone_id
       union all
       -- merch_list zone - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rzl.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = rc.zone_id
       union all
       -- merch_list zone - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im,
              rpm_zone_location rzl
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rzl.zone_id                        = rc.zone_id
       union all
       -- merch_list loc - parent items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - parent/diff items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
          and im.item_parent                     = mld.item
          and im.diff_1                          = mld.diff_id
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
       union all
       -- merch_list loc - tran items
       select /*+ USE_NL(mld, im) ORDERED */
              rc.price_event_id,
              im.item,
              rc.location,
              im.dept
         from clearances rc,
              rpm_merch_list_detail mld,
              item_master im
        where rc.skulist                         is NOT NULL
          and NVL(rc.sys_generated_exclusion, 0) = 1
          and mld.merch_list_id                  = rc.skulist
          and mld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
          and im.item                            = mld.item
          and im.item_level                      = im.tran_level
          and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
          and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          and rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE);

   insert
      when rank = 1 and
         I_chunk_number IN (0,
                            1) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number)
                          values (L_price_event_payload_seq,
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_DEL,
                                  I_bulk_cc_pe_id,
                                  L_parent_thread_number,
                                  L_thread_number)
      when 1 = 1 then
      into
         rpm_clearance_payload (clearance_payload_id,
                                price_event_payload_id,
                                clearance_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                reset_date,
                                reset_indicator,
                                bulk_cc_pe_id)
                        values (RPM_CLEARANCE_PAYLOAD_SEQ.NEXTVAL,
                                L_price_event_payload_seq,
                                price_event_id,
                                item,
                                location,
                                loc_type,
                                effective_date,
                                reset_date,
                                0,
                                I_bulk_cc_pe_id)
      select t2.location,
             t2.loc_type,
             t2.item,
             t2.price_event_id,
             t2.rank,
             t2.effective_date,
             t2.reset_date
        from (select t1.location,
                     t1.loc_type,
                     t1.item,
                     t1.price_event_id,
                     t1.effective_date,
                     t1.reset_date,
                     RANK() OVER (ORDER BY t1.location,
                                           t1.price_event_id,
                                           t1.item) rank
                from (select /*+ CARDINALITY(ids 10) */
                             distinct gtt.price_event_id,
                             gtt.location,
                             DECODE(gtt.zone_node_type,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                             gtt.item,
                             rcl.effective_date,
                             rcl.reset_date,
                             ril.price_event_id ril_peid
                        from table(cast(I_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                             rpm_fr_item_loc_expl_gtt gtt,
                             rpm_clearance rcl,
                             (select distinct dept,
                                     item,
                                     location,
                                     price_event_id
                                from rpm_item_loc_gtt
                               where rownum > 0) ril
                       where gtt.price_event_id = VALUE(ids)
                         and gtt.price_event_id = rcl.clearance_id
                         and gtt.customer_type  is NULL
                         and gtt.price_event_id = ril.price_event_id (+)
                         and gtt.dept           = ril.dept (+)
                         and gtt.item           = ril.item (+)
                         and gtt.location       = ril.location(+)) t1
               where t1.ril_peid is NULL) t2
       order by t2.rank;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_CLR_REMOVE_MESSAGES;

--------------------------------------------------------

FUNCTION STAGE_PROM_REMOVE_MESSAGES(IO_error_table         IN OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_transaction_id       IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                    I_bulk_cc_pe_id        IN     NUMBER,
                                    I_price_event_type     IN     VARCHAR2,
                                    I_promotion_detail_ids IN     OBJ_NUMERIC_ID_TABLE,
                                    I_chunk_number         IN     NUMBER DEFAULT 1)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CC_PUBLISH.STAGE_PROM_REMOVE_MESSAGES';

   L_error_rec               CONFLICT_CHECK_ERROR_REC := NULL;
   L_price_event_payload_seq NUMBER(10)               := NULL;
   L_parent_thread_number    NUMBER(10)               := NULL;
   L_thread_number           NUMBER(10)               := NULL;

   cursor C_GET_THREAD_DATA is
      select /*+ CARDINALITY(ids 10) */
             parent_thread_number,
             thread_number,
             chunk_price_event_payload_id
        from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO)
         and rownum                 = 1;

BEGIN

   if I_chunk_number IN (0,
                         1) then

      open C_GET_THREAD_DATA;
      fetch C_GET_THREAD_DATA into L_parent_thread_number,
                                   L_thread_number,
                                   L_price_event_payload_seq;
      close C_GET_THREAD_DATA;

      if L_price_event_payload_seq is NULL then
         L_price_event_payload_seq := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;
      end if;

      insert into rpm_price_event_payload (price_event_payload_id,
                                           transaction_id,
                                           rib_family,
                                           rib_type,
                                           bulk_cc_pe_id,
                                           parent_thread_number,
                                           thread_number)
         values (L_price_event_payload_seq,
                 I_transaction_id,
                 RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
                 RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL,
                 I_bulk_cc_pe_id,
                 L_parent_thread_number,
                 L_thread_number);

      insert into rpm_promo_dtl_payload (promo_dtl_payload_id,
                                         price_event_payload_id,
                                         promo_id,
                                         promo_comp_id,
                                         promo_name,
                                         promo_desc,
                                         promo_comp_desc,
                                         promo_comp_type,
                                         customer_type,
                                         promo_dtl_id,
                                         start_date,
                                         end_date,
                                         exception_parent_id,
                                         threshold_id,
                                         apply_to_code,
                                         discount_limit,
                                         thresh_qualification_type)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_DTL_PAYLOAD_SEQ.NEXTVAL,
                L_price_event_payload_seq,
                rp.promo_id,
                rpc.promo_comp_id,
                rp.name,
                rp.description,
                rpc.name,
                rpc.type,
                rpc.customer_type,
                rpd.promo_dtl_id,
                rpd.start_date,
                rpd.end_date,
                rpd.exception_parent_id,
                rpd.threshold_id,
                rpd.apply_to_code,
                rpd.discount_limit,
                rt.qualification_type
           from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl rpd,
                rpm_promo rp,
                rpm_promo_comp rpc,
                rpm_threshold rt
          where rpd.promo_dtl_id  = VALUE(ids)
            and rpd.promo_comp_id = rpc.promo_comp_id
            and rpc.promo_id      = rp.promo_id
            and rpd.threshold_id  = rt.threshold_id (+);

         insert into rpm_threshold_int_payload (threshold_int_payload_id,
                                                promo_dtl_payload_id,
                                                threshold_interval_id,
                                                threshold_id,
                                                threshold_type,
                                                threshold_amount,
                                                threshold_qty,
                                                change_type,
                                                change_amount,
                                                change_percent)
            select /*+ CARDINALITY(ids 10) */
                   RPM_THRESHOLD_INT_PAYLOAD_SEQ.NEXTVAL,
                   rpdp.promo_dtl_payload_id,
                   rti.threshold_interval_id,
                   rti.threshold_id,
                   rti.threshold_type,
                   rti.threshold_amount,
                   rti.threshold_qty,
                   rti.change_type,
                   rti.change_amount,
                   rti.change_percent
              from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_payload rpdp,
                   rpm_threshold_interval rti
             where rpdp.promo_dtl_id           = VALUE(ids)
               and rpdp.price_event_payload_id = L_price_event_payload_seq
               and rpdp.threshold_id           = rti.threshold_id;

         insert into rpm_promo_dtl_prc_rng_payload (promo_dtl_prc_rng_payload_id,
                                                    promo_dtl_payload_id,
                                                    buy_list_min,
                                                    buy_list_max,
                                                    get_list_min,
                                                    get_list_max)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PRC_RNG_PAYLOAD_SEQ.NEXTVAL,
                   rpdp.promo_dtl_payload_id,
                   rpdpr.buy_list_min,
                   rpdpr.buy_list_max,
                   rpdpr.get_list_min,
                   rpdpr.get_list_max
              from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_prc_range rpdpr,
                   rpm_promo_dtl_payload rpdp
             where rpdpr.promo_dtl_id          = VALUE(ids)
               and rpdpr.promo_dtl_id          = rpdp.promo_dtl_id
               and rpdp.price_event_payload_id = L_price_event_payload_seq;

         insert into rpm_promo_dtl_list_grp_payload (promo_dtl_list_grp_payload_id,
                                                     promo_dtl_payload_id,
                                                     promo_dtl_list_grp_id)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_LIST_GRP_PAY_SEQ.NEXTVAL,
                   pdp.PROMO_DTL_PAYLOAD_ID,
                   promo_dtl_list_grp_id
              from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_list_grp lgrp,
                   rpm_promo_dtl_payload pdp
             where lgrp.promo_dtl_id          = VALUE(ids)
               and pdp.promo_dtl_id           = lgrp.promo_dtl_id
               and pdp.price_event_payload_id = L_price_event_payload_seq;

         insert into rpm_promo_dtl_list_payload (promo_dtl_list_payload_id,
                                                 promo_dtl_list_grp_payload_id,
                                                 promo_dtl_list_id,
                                                 reward_application)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_LIST_PAYLOAD_SEQ.NEXTVAL,
                   dlgp.promo_dtl_list_grp_payload_id,
                   promo_dtl_list_id,
                   reward_application
              from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_list pdl,
                   rpm_promo_dtl_list_grp lgrp,
                   rpm_promo_dtl_list_grp_payload dlgp,
                   rpm_promo_dtl_payload pdp
             where pdl.promo_dtl_list_grp_id  = lgrp.promo_dtl_list_grp_id
               and lgrp.promo_dtl_id          = VALUE(ids)
               and dlgp.promo_dtl_list_grp_id = pdl.promo_dtl_list_grp_id
               and dlgp.promo_dtl_payload_id  = pdp.promo_dtl_payload_id
               and pdp.price_event_payload_id = L_price_event_payload_seq;

         insert into rpm_promo_dtl_mn_payload (promo_dtl_mn_payload_id,
                                               promo_dtl_payload_id,
                                               promo_dtl_list_payload_id,
                                               merch_type,
                                               dept,
                                               class,
                                               subclass,
                                               item,
                                               diff_id)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DTL_MN_PAYLOAD_SEQ.NEXTVAL,
                   pdp.promo_dtl_payload_id,
                   pdlp.promo_dtl_list_payload_id,
                   pdmn.merch_type,
                   pdmn.dept,
                   pdmn.class,
                   pdmn.subclass,
                   pdmn.item,
                   pdmn.diff_id
              from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_merch_node pdmn,
                   rpm_promo_dtl_payload pdp,
                   rpm_promo_dtl_list_grp_payload pdlgp,
                   rpm_promo_dtl_list_payload pdlp
             where pdmn.promo_dtl_id                  = VALUE(ids)
               and pdmn.promo_dtl_list_id             = pdlp.promo_dtl_list_id
               and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
               and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
               and pdp.price_event_payload_id         = L_price_event_payload_seq;

         insert into rpm_promo_disc_ldr_payload (promo_disc_ldr_payload_id,
                                                 promo_dtl_list_payload_id,
                                                 change_type,
                                                 change_amount,
                                                 change_currency,
                                                 change_percent,
                                                 change_selling_uom,
                                                 qual_type,
                                                 qual_value,
                                                 duration)
            select /*+ CARDINALITY(ids 10) */
                   RPM_PROMO_DISC_LDR_PAYLOAD_SEQ.NEXTVAL,
                   pdlp.promo_dtl_list_payload_id,
                   pddl.change_type,
                   pddl.change_amount,
                   pddl.change_currency,
                   pddl.change_percent,
                   pddl.change_selling_uom,
                   DECODE(pddl.change_type,
                          RPM_CONSTANTS.RETAIL_EXCLUDE, NULL,
                          pddl.qual_type),
                   DECODE(pddl.change_type,
                          RPM_CONSTANTS.RETAIL_EXCLUDE, NULL,
                          pddl.qual_value),
                   pddl.duration
              from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                   rpm_promo_dtl_payload pdp,
                   rpm_promo_dtl_list_grp_payload dlgp,
                   rpm_promo_dtl_list_payload pdlp,
                   rpm_promo_dtl_disc_ladder pddl
             where pdp.price_event_payload_id         = L_price_event_payload_seq
               and pdp.promo_dtl_id                   = VALUE(ids)
               and pdp.promo_dtl_payload_id           = dlgp.promo_dtl_payload_id
               and dlgp.promo_dtl_list_grp_payload_id = pdlp.promo_dtl_list_grp_payload_id
               and pdlp.promo_dtl_list_id             = pddl.promo_dtl_list_id;

         insert into rpm_promo_item_payload (promo_item_payload_id,
                                             promo_dtl_list_payload_id,
                                             item)
            select RPM_PROMO_ITEM_PAYLOAD_SEQ.NEXTVAL,
                   promo_dtl_list_payload_id,
                   item
              from (-- Item Level
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                       and im.item                            = pdmn.item
                       and im.item_level                      = im.tran_level
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_Dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- Item Parent Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                       and im.item_parent                     = pdmn.item
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_Dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- Parent Diff Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.PARENT_ITEM_DIFF
                       and im.item_parent                     = pdmn.item
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and (   im.diff_1 = pdmn.diff_id
                            or im.diff_2 = pdmn.diff_id
                            or im.diff_3 = pdmn.diff_id
                            or im.diff_4 = pdmn.diff_id)
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_Dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- ItemList Tran Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           rpm_promo_dtl_skulist pds,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       and pds.price_event_id                 = pdmn.promo_dtl_id
                       and pds.skulist                        = pdmn.skulist
                       and pds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                       and im.item                            = pds.item
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_Dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- ItemList Parent Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           rpm_promo_dtl_skulist pds,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       and pds.price_event_id                 = pdmn.promo_dtl_id
                       and pds.skulist                        = pdmn.skulist
                       and pds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                       and im.item_parent                     = pds.item
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- PEIL Tran Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           rpm_merch_list_detail rmld,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                       and rmld.merch_list_id                 = pdmn.price_event_itemlist
                       and rmld.merch_level                   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                       and im.item                            = rmld.item
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_Dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- PEIL Parent Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           rpm_merch_list_detail rmld,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                       and rmld.merch_list_id                 = pdmn.price_event_itemlist
                       and rmld.merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                       and im.item_parent                     = rmld.item
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0
                    -- Merch Hierarchy Level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           im.item,
                           pdlp.promo_dtl_list_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im,
                           rpm_promo_dtl_list pdl,
                           rpm_promo_dtl_list_grp pdlg,
                           rpm_promo_dtl_list_payload pdlp,
                           rpm_promo_dtl_payload pdp,
                           rpm_promo_dtl_list_grp_payload pdlgp
                     where pdmn.promo_dtl_id                  = VALUE(ids)
                       and pdmn.merch_type                    IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                       and im.item_level                      = im.tran_level
                       and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and im.dept                            = pdmn.dept
                       and im.class                           = NVL(pdmn.class, im.class)
                       and im.subclass                        = NVL(pdmn.subclass, im.subclass)
                       and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                       and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                       and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                       and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                       and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                       and pdp.price_event_payload_id         = L_price_event_payload_seq
                       and rownum                             > 0);

         insert into rpm_promo_location_payload (promo_location_payload_id,
                                                 promo_dtl_payload_id,
                                                 location,
                                                 location_type)
            select RPM_PROMO_LOCATION_PAYLOAD_SEQ.NEXTVAL,
                   promo_dtl_payload_id,
                   location,
                   loc_type
              from (-- Location level
                    select /*+ CARDINALITY(ids 10) */
                           pzl.location location,
                           DECODE(pzl.zone_node_type,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                           pdp.promo_dtl_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_zone_location pzl,
                           rpm_promo_dtl_payload pdp
                     where pzl.promo_dtl_id           = VALUE(ids)
                       and pzl.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                       and pdp.promo_dtl_id           = pzl.promo_dtl_id
                       and pdp.price_event_payload_id = L_price_event_payload_seq
                    -- Zone level
                    union all
                    select /*+ CARDINALITY(ids 10) */
                           rzl.location location,
                           RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                           pdp.promo_dtl_payload_id
                      from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                           rpm_promo_zone_location pzl,
                           rpm_promo_dtl_payload pdp,
                           rpm_zone_location rzl
                     where pzl.promo_dtl_id           = VALUE(ids)
                       and pzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                       and rzl.zone_id                = pzl.zone_id
                       and pdp.promo_dtl_id           = pzl.promo_dtl_id
                       and pdp.price_event_payload_id = L_price_event_payload_seq
                       and rzl.loc_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE);
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_PROM_REMOVE_MESSAGES;

--------------------------------------------------------
FUNCTION STAGE_FIN_REMOVE_PROM_MESSAGES(IO_error_table         IN OUT CONFLICT_CHECK_ERROR_TBL,
                                        I_transaction_id       IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                        I_promotion_detail_ids IN     OBJ_NUMERIC_ID_TABLE,
                                        I_promotion_type       IN     VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(50)             := 'RPM_CC_PUBLISH.STAGE_FIN_REMOVE_PROM_MESSAGES';
   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_curr_pe_pay_seq RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;

BEGIN

   insert into rpm_price_event_payload (price_event_payload_id,
                                        transaction_id,
                                        rib_family,
                                        rib_type)
      values(L_curr_pe_pay_seq,
             I_transaction_id,
             RPM_CONSTANTS.RIB_MSG_FAM_PROM_PC,
             RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_DEL);

   insert into rpm_promo_dtl_payload (promo_dtl_payload_id,
                                      price_event_payload_id,
                                      promo_id,
                                      promo_comp_id,
                                      promo_name,
                                      promo_desc,
                                      promo_comp_desc,
                                      promo_comp_type,
                                      customer_type,
                                      promo_dtl_id,
                                      start_date,
                                      end_date,
                                      apply_to_code,
                                      exception_parent_id)
      select /*+ CARDINALITY(ids 10) */
             RPM_PROMO_DTL_PAYLOAD_SEQ.NEXTVAL,
             L_curr_pe_pay_seq,
             rp.promo_id,
             rpc.promo_comp_id,
             rp.name,
             rp.description,
             rpc.name,
             rpc.type,
             rpc.customer_type,
             rpd.promo_dtl_id,
             rpd.start_date,
             rpd.end_date,
             rpd.apply_to_code,
             rpd.exception_parent_id
        from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_comp rpc,
             rpm_promo rp
       where rpd.promo_dtl_id  = VALUE(ids)
         and rpd.promo_comp_id = rpc.promo_comp_id
         and rpc.promo_id      = rp.promo_id;

      insert into rpm_fin_cred_dtl_payload (fin_cred_dtl_payload_id,
                                            promo_dtl_payload_id,
                                            financial_dtl_id,
                                            card_type,
                                            bin_from_value,
                                            bin_to_value,
                                            commission_rate,
                                            comments)
         select /*+ CARDINALITY(ids 10) */
                RPM_FIN_CRED_DTL_PAYLOAD_SEQ.NEXTVAL,
                rpdp.promo_dtl_payload_id,
                rfd.financial_dtl_id,
                rfd.card_type,
                rfd.bin_from_value,
                rfd.bin_to_value,
                rpcd.commission_rate,
                rpcd.comments
           from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl_payload rpdp,
                rpm_promo_credit_dtl rpcd,
                rpm_financial_dtl rfd
          where rpdp.promo_dtl_id           = VALUE(ids)
            and rpcd.promo_dtl_id           = VALUE(ids)
            and rpdp.promo_dtl_id           = rpcd.promo_dtl_id
            and rpdp.price_event_payload_id = L_curr_pe_pay_seq
            and rpcd.financial_dtl_id       = rfd.financial_dtl_id;

   insert into rpm_promo_dtl_list_grp_payload (promo_dtl_list_grp_payload_id,
                                               promo_dtl_payload_id,
                                               promo_dtl_list_grp_id)
      select /*+ CARDINALITY(ids 10) */
             RPM_PROMO_DTL_LIST_GRP_PAY_SEQ.NEXTVAL,
             pdp.promo_dtl_payload_id,
             promo_dtl_list_grp_id
        from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list_grp lgrp,
             rpm_promo_dtl_payload pdp
       where lgrp.promo_dtl_id          = VALUE(ids)
         and pdp.promo_dtl_id           = lgrp.promo_dtl_id
         and pdp.price_event_payload_id = L_curr_pe_pay_seq;

   insert into rpm_promo_dtl_list_payload (promo_dtl_list_payload_id,
                                           promo_dtl_list_grp_payload_id,
                                           promo_dtl_list_id,
                                           description,
                                           reward_application)
      select /*+ CARDINALITY(ids 10) */
             RPM_PROMO_DTL_LIST_PAYLOAD_SEQ.NEXTVAL,
             dlgp.promo_dtl_list_grp_payload_id,
             promo_dtl_list_id,
             NULL,
             reward_application
        from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list_grp lgrp,
             rpm_promo_dtl_list pdl,
             rpm_promo_dtl_list_grp_payload dlgp,
             rpm_promo_dtl_payload pdp
       where lgrp.promo_dtl_id          = VALUE(ids)
         and pdl.promo_dtl_list_grp_id  = lgrp.promo_dtl_list_grp_id
         and dlgp.promo_dtl_list_grp_id = pdl.promo_dtl_list_grp_id
         and dlgp.promo_dtl_payload_id  = pdp.promo_dtl_payload_id
         and pdp.price_event_payload_id = L_curr_pe_pay_seq;

   insert into rpm_promo_disc_ldr_payload (promo_disc_ldr_payload_id,
                                           promo_dtl_list_payload_id,
                                           change_type,
                                           change_amount,
                                           change_currency,
                                           change_percent,
                                           change_selling_uom,
                                           qual_type,
                                           qual_value,
                                           duration)
      select /*+ CARDINALITY(ids 10) */
             RPM_PROMO_DISC_LDR_PAYLOAD_SEQ.NEXTVAL,
             pdlp.promo_dtl_list_payload_id,
             pddl.change_type,
             pddl.change_amount,
             pddl.change_currency,
             pddl.change_percent,
             pddl.change_selling_uom,
             pddl.qual_type,
             pddl.qual_value,
             pddl.duration
        from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_payload pdp,
             rpm_promo_dtl_list_grp_payload dlgp,
             rpm_promo_dtl_list_payload pdlp,
             rpm_promo_dtl_disc_ladder pddl
       where pdp.promo_dtl_id                   = VALUE(ids)
         and pdp.price_event_payload_id         = L_curr_pe_pay_seq
         and pdp.promo_dtl_payload_id           = dlgp.promo_dtl_payload_id
         and dlgp.promo_dtl_list_grp_payload_id = pdlp.promo_dtl_list_grp_payload_id
         and pdlp.promo_dtl_list_id             = pddl.promo_dtl_list_id;

   insert into rpm_promo_item_payload (promo_item_payload_id,
                                       promo_dtl_list_payload_id,
                                       item)
      select RPM_PROMO_ITEM_PAYLOAD_SEQ.NEXTVAL,
             promo_dtl_list_payload_id,
             item
        from (--
              -- Item Level
              --
              select /*+ CARDINALITY(ids 10) */
                     im.item,
                     pdlp.promo_dtl_list_payload_id
                from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_promo_dtl_list_grp pdlg,
                     rpm_promo_dtl_list pdl,
                     rpm_promo_dtl_list_payload pdlp,
                     rpm_promo_dtl_payload pdp,
                     rpm_promo_dtl_list_grp_payload pdlgp
               where pdmn.promo_dtl_id                  = VALUE(ids)
                 and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item                            = pdmn.item
                 and im.item_level                      = im.tran_level
                 and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                 and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                 and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                 and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                 and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                 and pdmn.promo_dtl_list_id             = pdl.promo_dtl_list_id
                 and pdp.price_event_payload_id         = L_curr_pe_pay_seq
              --
              -- Item Parent Level
              --
              union all
              select /*+ CARDINALITY(ids 10) */
                     im.item,
                     pdlp.promo_dtl_list_payload_id
                from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_promo_dtl_list_grp pdlg,
                     rpm_promo_dtl_list pdl,
                     rpm_promo_dtl_list_payload pdlp,
                     rpm_promo_dtl_list_grp_payload pdlgp,
                     rpm_promo_dtl_payload pdp
               where pdmn.promo_dtl_id                  = VALUE(ids)
                 and pdmn.merch_type                    = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item_parent                     = pdmn.item
                 and im.item_level                      = im.tran_level
                 and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                 and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                 and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                 and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                 and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                 and pdp.price_event_payload_id         = L_curr_pe_pay_seq
              --
              -- Parent Diff Level
              --
              union all
              select /*+ CARDINALITY(ids 10) */
                      im.item,
                     pdlp.promo_dtl_list_payload_id
                from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_promo_dtl_list_grp pdlg,
                     rpm_promo_dtl_list pdl,
                     rpm_promo_dtl_list_payload pdlp,
                     rpm_promo_dtl_list_grp_payload pdlgp,
                     rpm_promo_dtl_payload pdp
               where pdmn.promo_dtl_id                  = VALUE(ids)
                 and pdmn.merch_type                    = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item_parent                     = pdmn.item
                 and im.item_level                      = im.tran_level
                 and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and (   im.diff_1 = pdmn.diff_id
                      or im.diff_2 = pdmn.diff_id
                      or im.diff_3 = pdmn.diff_id
                      or im.diff_4 = pdmn.diff_id)
                 and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                 and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                 and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                 and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                 and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                 and pdp.price_event_payload_id         = L_curr_pe_pay_seq
              --
              -- Merch Hierarchy Level
              --
              union all
              select /*+ CARDINALITY(ids 10) */
                     im.item,
                     pdlp.promo_dtl_list_payload_id
                from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl_merch_node pdmn,
                     item_master im,
                     rpm_promo_dtl_list_grp pdlg,
                     rpm_promo_dtl_list pdl,
                     rpm_promo_dtl_list_payload pdlp,
                     rpm_promo_dtl_list_grp_payload pdlgp,
                     rpm_promo_dtl_payload pdp
               where pdmn.promo_dtl_id                  = VALUE(ids)
                 and pdmn.merch_type                    IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                            RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                            RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                 and im.item_level                      = im.tran_level
                 and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.dept                            = pdmn.dept
                 and im.class                           = NVL(pdmn.class, im.class)
                 and im.subclass                        = NVL(pdmn.subclass, im.subclass)
                 and pdlg.promo_dtl_id                  = pdmn.promo_dtl_id
                 and pdl.promo_dtl_list_grp_id          = pdlg.promo_dtl_list_grp_id
                 and pdlp.promo_dtl_list_id             = pdl.promo_dtl_list_id
                 and pdlp.promo_dtl_list_grp_payload_id = pdlgp.promo_dtl_list_grp_payload_id
                 and pdlgp.promo_dtl_payload_id         = pdp.promo_dtl_payload_id
                 and pdp.price_event_payload_id         = L_curr_pe_pay_seq);

   insert into rpm_promo_location_payload (promo_location_payload_id,
                                           promo_dtl_payload_id,
                                           location,
                                           location_type)
      select RPM_PROMO_LOCATION_PAYLOAD_SEQ.NEXTVAL,
             promo_dtl_payload_id,
             location,
             loc_type
        from (--
              -- Location level
              --
              select /*+ CARDINALITY(ids 10) */
                     pzl.location location,
                     DECODE(pzl.zone_node_type,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                     pdp.promo_dtl_payload_id
                from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_zone_location pzl,
                     rpm_promo_dtl_payload pdp
               where pzl.promo_dtl_id           = VALUE(ids)
                 and pzl.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and pdp.promo_dtl_id           = pzl.promo_dtl_id
                 and pdp.price_event_payload_id = L_curr_pe_pay_seq
              --
              -- Zone level
              --
              union all
              select /*+ CARDINALITY(ids 10) */
                     rzl.location location,
                     RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                     pdp.promo_dtl_payload_id
                from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_zone_location pzl,
                     rpm_zone_location rzl,
                     rpm_promo_dtl_payload pdp
               where pzl.promo_dtl_id           = VALUE(ids)
                 and pzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rzl.zone_id                = pzl.zone_id
                 and pdp.promo_dtl_id           = pzl.promo_dtl_id
                 and pdp.price_event_payload_id = L_curr_pe_pay_seq
                 and rzl.loc_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE);

      insert into rpm_promo_fin_dtl_payload(promo_fin_dtl_payload_id,
                                            promo_dtl_payload_id,
                                            finance_dtl_id,
                                            card_type,
                                            bin_from_value,
                                            bin_to_value,
                                            commission_rate,
                                            comments)
         select /*+ CARDINALITY(ids 10) */
                RPM_PROMO_FIN_DTL_PAYLOAD_SEQ.NEXTVAL,
                rpdp.promo_dtl_payload_id,
                rfd.financial_dtl_id,
                rfd.card_type,
                rfd.bin_from_value,
                rfd.bin_to_value,
                rpcd.commission_rate,
                rpcd.comments
           from table(cast(I_promotion_detail_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_promo_dtl_payload rpdp,
                rpm_promo_dtl rpd,
                rpm_promo_credit_dtl rpcd,
                rpm_financial_dtl rfd
          where rpdp.promo_dtl_id           = VALUE(ids)
            and rpd.promo_dtl_id            = VALUE(ids)
            and rpdp.price_event_payload_id = L_curr_pe_pay_seq
            and rpd.promo_dtl_id            = rpcd.promo_dtl_id
            and rpcd.financial_dtl_id       = rfd.financial_dtl_id;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_FIN_REMOVE_PROM_MESSAGES;
--------------------------------------------------------
FUNCTION STAGE_RESET_POS_MESSAGES(O_error_msg          OUT VARCHAR2,
                                  I_transaction_id  IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                  L_reset_pos_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
   L_function     VARCHAR2(61) := 'RPM_CC_PUBLISH.STAGE_RESET_POS_MESSAGES';
   L_pc_id        NUMBER(15)   := NULL;
   LP_vdate       DATE         := GET_VDATE;

BEGIN

   for I in 1..L_reset_pos_ids.COUNT LOOP
      L_pc_id := L_reset_pos_ids(I);

      insert
         when (rank = 1) then
         INTO
            rpm_price_event_payload (price_event_payload_id,
                                     transaction_id,
                                     rib_family,
                                     rib_type)
                             values (RPM_CC_PUBLISH.GET_PAYLOAD_ID(1),
                                     I_transaction_id,
                                     RPM_CONSTANTS.RIB_MSG_FAM_REG_PC,
                                     pc_msg_type)
         when (1 = 1) then
         INTO
            rpm_price_chg_payload (price_chg_payload_id,
                                   price_event_payload_id,
                                   price_change_id,
                                   item,
                                   location,
                                   location_type,
                                   effective_date,
                                   selling_unit_change_ind,
                                   selling_retail,
                                   selling_retail_uom,
                                   selling_retail_currency,
                                   multi_unit_change_ind,
                                   multi_units,
                                   multi_units_retail,
                                   multi_units_uom,
                                   multi_units_currency)
                           values (RPM_PRICE_CHG_PAYLOAD_SEQ.NEXTVAL,
                                   RPM_CC_PUBLISH.GET_PAYLOAD_ID(0),
                                   price_change_id,
                                   item,
                                   location,
                                   loc_type,
                                   action_date,
                                   pc_selling_retail_ind,
                                   selling_retail,
                                   selling_uom,
                                   selling_retail_currency,
                                   pc_multi_unit_ind,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_selling_uom,
                                   multi_unit_retail_currency)
         select t2.location,
                t2.loc_type,
                t2.pc_msg_type,
                t2.item,
                t2.price_change_id,
                t2.action_date,
                t2.pc_selling_retail_ind,
                t2.selling_retail,
                t2.selling_uom,
                t2.selling_retail_currency,
                t2.pc_multi_unit_ind,
                t2.multi_units,
                t2.multi_unit_retail,
                t2.multi_selling_uom,
                t2.multi_unit_retail_currency,
                t2.rank
           from (select t1.location,
                        t1.loc_type,
                        t1.pc_msg_type,
                        t1.item,
                        t1.price_change_id,
                        t1.action_date,
                        t1.pc_selling_retail_ind,
                        t1.selling_retail,
                        t1.selling_uom,
                        t1.selling_retail_currency,
                        t1.pc_multi_unit_ind,
                        t1.multi_units,
                        t1.multi_unit_retail,
                        t1.multi_selling_uom,
                        t1.multi_unit_retail_currency,
                        RANK() OVER (PARTITION BY t1.pc_msg_type
                                         ORDER BY t1.location,
                                                  t1.price_change_id,
                                                  t1.item) rank
                   from (select rfr.location,
                                DECODE(rfr.zone_node_type,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                                RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE pc_msg_type,
                                rfr.item,
                                L_pc_id price_change_id,
                                LP_vdate action_date,
                                0 AS pc_selling_retail_ind,
                                rfr.selling_retail,
                                rfr.selling_uom,
                                rfr.selling_retail_currency,
                                0 AS pc_multi_unit_ind,
                                NULL AS multi_units,
                                NULL AS multi_unit_retail,
                                NULL AS multi_selling_uom,
                                NULL AS multi_unit_retail_currency
                           from (select fr.location,
                                        fr.zone_node_type,
                                        fr.item,
                                        fr.selling_retail,
                                        fr.selling_uom,
                                        fr.selling_retail_currency,
                                        RANK() OVER (PARTITION BY fr.item,
                                                                  fr.location
                                                         ORDER BY fr.action_date desc) ranking
                                   from rpm_fr_item_loc_expl_gtt fr
                                  where (fr.dept,
                                         fr.item,
                                         fr.location) IN (
                                                         -- item/parent/diff and loc
                                                         select im.dept,
                                                                im.item,
                                                                rpc.location
                                                           from rpm_price_change rpc,
                                                                item_master im
                                                          where rpc.price_change_id = L_pc_id
                                                            and rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                            and (   im.item = rpc.item
                                                                 or im.item_parent = rpc.item)
                                                            and (   rpc.diff_id IS NULL
                                                                 or (    rpc.diff_id is NOT NULL
                                                                     and (   im.diff_1 = rpc.diff_id
                                                                          or im.diff_2 = rpc.diff_id
                                                                          or im.diff_3 = rpc.diff_id
                                                                          or im.diff_4 = rpc.diff_id)))
                                                         -- item/parent/diff and zone
                                                         union all
                                                         select im.dept,
                                                                im.item,
                                                                rzl.location
                                                           from rpm_price_change rpc,
                                                                item_master im,
                                                                rpm_zone_location rzl
                                                          where rpc.price_change_id = L_pc_id
                                                            and rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                            and (   im.item = rpc.item
                                                                 or im.item_parent = rpc.item)
                                                            and (   rpc.diff_id IS NULL
                                                                 or (    rpc.diff_id is NOT NULL
                                                                     and (   im.diff_1 = rpc.diff_id
                                                                          or im.diff_2 = rpc.diff_id
                                                                          or im.diff_3 = rpc.diff_id
                                                                          or im.diff_4 = rpc.diff_id)))
                                                            and rzl.zone_id = rpc.zone_id
                                                         -- item list and loc
                                                         union all
                                                         select im.dept,
                                                                im.item,
                                                                rpc.location
                                                           from rpm_price_change rpc,
                                                                item_master im,
                                                                rpm_price_change_skulist rpcs
                                                          where rpc.price_change_id = L_pc_id
                                                            and rpc.skulist is NOT NULL
                                                            and rpcs.skulist = rpc.skulist 
                                                            and rpcs.price_event_id = rpc.price_change_id
                                                            and rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                            and (   im.item = rpcs.item
                                                                 or im.item_parent = rpcs.item)
                                                         -- item list and zone
                                                         union all
                                                         select im.dept,
                                                                im.item,
                                                                rzl.location
                                                           from rpm_price_change rpc,
                                                                item_master im,
                                                                rpm_zone_location rzl,
                                                                rpm_price_change_skulist rpcs
                                                          where rpc.price_change_id = L_pc_id
                                                            and rpc.skulist is NOT NULL
                                                            and rpcs.skulist = rpc.skulist 
                                                            and rpcs.price_event_id = rpc.price_change_id
                                                            and rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                            and (    im.item = rpcs.item
                                                                  or im.item_parent = rpcs.item)
                                                            and rzl.zone_id = rpc.zone_id      
                                                         -- PEIL and loc
                                                         union all
                                                         select im.dept,
                                                                im.item,
                                                                rpc.location
                                                           from rpm_price_change rpc,
                                                                item_master im,
                                                                rpm_merch_list_detail rmld
                                                          where rpc.price_change_id = L_pc_id
                                                            and rpc.price_event_itemlist is NOT NULL
                                                            and rmld.merch_list_id = rpc.price_event_itemlist
                                                            and rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                            and (   im.item = rmld.item
                                                                 or im.item_parent = rmld.item)
                                                         -- PEIL and zone
                                                         union all
                                                         select im.dept,
                                                                im.item,
                                                                rzl.location
                                                           from rpm_price_change rpc,
                                                                item_master im,
                                                                rpm_zone_location rzl,
                                                                rpm_merch_list_detail rmld
                                                          where rpc.price_change_id = L_pc_id
                                                            and rpc.price_event_itemlist is NOT NULL
                                                            and rmld.merch_list_id = rpc.price_event_itemlist
                                                            and rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                            and (   im.item = rmld.item
                                                                 or im.item_parent = rmld.item)
                                                            and rzl.zone_id = rpc.zone_id)
                                    and fr.action_date <= LP_vdate) rfr
                          where rfr.ranking = 1) t1) t2
          order by t2.rank;

   end loop;

   return 1;

EXCEPTION
    when OTHERS then
        O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                SQLERRM,
                L_function,
                TO_CHAR(SQLCODE));
    return 0;

END STAGE_RESET_POS_MESSAGES;

--------------------------------------------------------

FUNCTION STAGE_CLR_RST_REMOVE_MESSAGES(IO_error_table    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_transaction_id  IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                       I_chunk_number    IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CC_PUBLISH.STAGE_CLR_RST_REMOVE_MESSAGES';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;

   L_price_event_payload_seq NUMBER(10) := NULL;

   L_parent_thread_number NUMBER(10) := NULL;
   L_thread_number        NUMBER(10) := NULL;

   cursor C_THREAD_NUMBERS is
      select /*+ CARDINALITY(ids 10) */
             distinct parent_thread_number,
             thread_number
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET);

BEGIN

   -- check if chunk_price_event_payload_id is present, else get RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL
   select NVL((select /*+ CARDINALITY(ids 10) */
                      chunk_price_event_payload_id
                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_bulk_cc_pe_thread rbcpt
                where rbcpt.price_event_id   = VALUE(ids)
                  and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
                  and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                 RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                 RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET)
                  and rownum                 = 1), RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL)
     into L_price_event_payload_seq
     from dual;

   open C_THREAD_NUMBERS;
   fetch C_THREAD_NUMBERS into L_parent_thread_number,
                               L_thread_number;
   close C_THREAD_NUMBERS;

   insert
      when rank = 1 and
         I_chunk_number IN (0,
                            1) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  transaction_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number)
                          values (L_price_event_payload_seq,
                                  I_transaction_id,
                                  RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_DEL,
                                  I_bulk_cc_pe_id,
                                  L_parent_thread_number,
                                  L_thread_number)
      when 1 = 1 then
      into
         rpm_clearance_payload (clearance_payload_id,
                                price_event_payload_id,
                                clearance_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                selling_retail,
                                selling_retail_uom,
                                selling_retail_currency,
                                reset_clearance_id,
                                reset_indicator,
                                bulk_cc_pe_id)
                        values (RPM_CLEARANCE_PAYLOAD_SEQ.NEXTVAL,
                                L_price_event_payload_seq,
                                clearance_id,
                                item,
                                location,
                                loc_type,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                reset_indicator,
                                I_bulk_cc_pe_id)
      select t2.location,
             t2.loc_type,
             t2.item,
             t2.clearance_id,
             t2.reset_indicator,
             t2.rank
        from (select t1.location,
                     t1.loc_type,
                     t1.item,
                     t1.clearance_id,
                     t1.reset_indicator,
                     RANK() OVER (ORDER BY t1.location,
                                           t1.clearance_id,
                                           t1.item) rank
                from (select distinct gtt.location,
                             DECODE(gtt.zone_node_type,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                             gtt.item,
                             rcg.clearance_id clearance_id,
                             rcg.reset_ind reset_indicator
                        from rpm_fr_item_loc_expl_gtt gtt,
                             rpm_clearance_gtt rcg
                       where gtt.item           = rcg.item
                         and gtt.location       = rcg.location
                         and rcg.reset_ind      = 1
                         and rcg.effective_date IS NULL
                         and gtt.customer_type  IS NULL) t1) t2
       order by t2.rank;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END STAGE_CLR_RST_REMOVE_MESSAGES;

--------------------------------------------------------

FUNCTION GET_PAYLOAD_ID(I_is_next   IN     NUMBER,
                        I_seq_name  IN     VARCHAR2  default NULL)
RETURN NUMBER IS

   L_payload_id           NUMBER    := NULL;

BEGIN

   if I_seq_name IS NULL or
      I_seq_name = 'RPM_PRICE_EVENT_PAYLOAD_SEQ' then

      if I_is_next = 1 then
         select RPM_PRICE_EVENT_PAYLOAD_SEQ.nextval
           into L_payload_id
           from dual;
      else
         select RPM_PRICE_EVENT_PAYLOAD_SEQ.currval
           into L_payload_id
           from dual;
      end if;

   elsif I_seq_name = 'RPM_PROMO_DTL_PAYLOAD_SEQ' then

      if I_is_next = 1 then
         select RPM_PROMO_DTL_PAYLOAD_SEQ.nextval
           into L_payload_id
           from dual;
      else
         select RPM_PROMO_DTL_PAYLOAD_SEQ.currval
           into L_payload_id
           from dual;
      end if;

   elsif I_seq_name = 'RPM_PROMO_DTL_LIST_PAYLOAD_SEQ' then

      if I_is_next = 1 then
         select RPM_PROMO_DTL_LIST_PAYLOAD_SEQ.nextval
           into L_payload_id
           from dual;
      else
         select RPM_PROMO_DTL_LIST_PAYLOAD_SEQ.currval
           into L_payload_id
           from dual;
      end if;

   elsif I_seq_name = 'RPM_PROMO_DTL_LIST_GRP_PAY_SEQ' then

      if I_is_next = 1 then
         select RPM_PROMO_DTL_LIST_GRP_PAY_SEQ.nextval
           into L_payload_id
           from dual;
      else
         select RPM_PROMO_DTL_LIST_GRP_PAY_SEQ.currval
           into L_payload_id
           from dual;
      end if;

   end if;

   return L_payload_id;

END GET_PAYLOAD_ID;
--------------------------------------------------------
FUNCTION STAGE_UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                                       I_reset_clearance_id IN     OBJ_CLEARANCE_ITEM_LOC_TBL,
                                       I_bulk_cc_pe_id      IN     NUMBER,
                                       I_chunk_number       IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CC_PUBLISH.STAGE_UPD_RESET_ON_CLR_REMOVE';

   L_price_event_payload_seq NUMBER(10) := NULL;

BEGIN

   if I_chunk_number = 1 then
      select /*+ CARDINALITY(ids 10) */
             chunk_clearance_payload_id
        into L_price_event_payload_seq
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt
       where rbcpt.price_event_id   = VALUE(ids)
         and rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
         and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                        RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                        RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET)
         and rownum                 = 1;
   else
      L_price_event_payload_seq := RPM_PRICE_EVENT_PAYLOAD_SEQ.NEXTVAL;
   end if;

   insert
      when rank = 1 and
         I_chunk_number IN (0,
                            1) then
      into
         rpm_price_event_payload (price_event_payload_id,
                                  rib_family,
                                  rib_type,
                                  bulk_cc_pe_id)
                          values (L_price_event_payload_seq,
                                  RPM_CONSTANTS.RIB_MSG_FAM_CLEAR_PC,
                                  RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_DEL,
                                  I_bulk_cc_pe_id)
      when 1 = 1 then
      into
         rpm_clearance_payload (clearance_payload_id,
                                price_event_payload_id,
                                clearance_id,
                                item,
                                location,
                                location_type,
                                effective_date,
                                selling_retail,
                                selling_retail_uom,
                                selling_retail_currency,
                                reset_clearance_id,
                                reset_indicator,
                                bulk_cc_pe_id)
                        values (RPM_CLEARANCE_PAYLOAD_SEQ.NEXTVAL,
                                L_price_event_payload_seq,
                                clearance_id,
                                item,
                                location,
                                loc_type,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                1,
                                I_bulk_cc_pe_id)
   select t2.bulk_cc_pe_id,
          t2.location,
          t2.loc_type,
          t2.item,
          t2.clearance_id,
          t2.rank
     from (select t1.bulk_cc_pe_id,
                  t1.location,
                  t1.loc_type,
                  t1.item,
                  t1.clearance_id,
                  DENSE_RANK() OVER (PARTITION BY t1.bulk_cc_pe_id
                                         ORDER BY t1.clearance_id) rank
             from (select I_bulk_cc_pe_id bulk_cc_pe_id,
                          clr.location,
                          clr.item,
                          clr.clearance_id,
                          DECODE(clr.zone_node_type,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type
                     from table(cast(I_reset_clearance_id as OBJ_CLEARANCE_ITEM_LOC_TBL)) clr
                    where clr.effective_date is NOT NULL) t1) t2
    order by t2.location,
             t2.rank;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END STAGE_UPD_RESET_ON_CLR_REMOVE;
--------------------------------------------------------
END RPM_CC_PUBLISH;
/
