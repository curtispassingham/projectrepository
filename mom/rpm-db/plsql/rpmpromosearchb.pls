CREATE OR REPLACE PACKAGE BODY RPM_PROMO_SEARCH_SQL AS
-------------------------------------------------------------------------------
-- Package Global Variables
-------------------------------------------------------------------------------
   LP_criteria OBJ_PROMOTION_SEARCH_REC;
   LP_exclude_no_comp NUMBER := 0;

   -- Promotion Search item level and type
   LP_PROMO_SEARCH_ITEM_LEVEL   CONSTANT VARCHAR2(2) := 'I';
   LP_PROMO_SEARCH_PARENT_LEVEL CONSTANT VARCHAR2(2) := 'P';
   LP_PROMO_SEARCH_DIFF_LEVEL   CONSTANT VARCHAR2(2) := 'PD';
   LP_PROMO_SEARCH_MERCH_HIER   CONSTANT VARCHAR2(2) := 'H';
   LP_PROMO_SEARCH_STORE_WIDE   CONSTANT VARCHAR2(2) := 'SW';
   LP_PROMO_SEARCH_ITEM_LIST    CONSTANT VARCHAR2(2) := 'IL';
   LP_PROMO_SEARCH_PEIL         CONSTANT VARCHAR2(2) := 'PL';

-------------------------------------------------------------------------------
-- Private Functions Definition
-------------------------------------------------------------------------------
FUNCTION SEARCH_NO_CRIT(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_PROMO_ID(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_ITEM_LIST(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_PRICE_EVENT_ITEM_LIST(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_ITEM(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_ITEM_PARENT(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_ITEM_PARENT_DIFF(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_STORE_WIDE(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_DEPT_CLASS_SUB(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_MERCH_HIERARCHY(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_OTHER_CRIT(O_error_msg    OUT VARCHAR2,
                           I_from_hist IN     NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_EVENT_ID(O_error_msg         OUT VARCHAR2,
                         I_promo_event_id IN     NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_THRESHOLD_ID(O_error_msg       OUT VARCHAR2,
                             I_threshold_id IN     NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_ZONE_IDS(O_error_msg    OUT VARCHAR2,
                         I_zone_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
FUNCTION SEARCH_PRICE_GUIDE_ID(O_error_msg         OUT VARCHAR2,
                               I_price_guide_id IN     NUMBER)
RETURN BOOLEAN;
-------------------------------------------------------------------------------
-- Public Procedure Search
-------------------------------------------------------------------------------
PROCEDURE SEARCH(O_return_code        OUT NUMBER,
                 O_error_msg          OUT VARCHAR2,
                 O_search_tbl         OUT OBJ_PROMOTION_TBL,
                 O_ps_count           OUT NUMBER,
                 I_criteria_tbl    IN     OBJ_PROMOTION_SEARCH_TBL,
                 I_exclude_no_comp IN     NUMBER DEFAULT 0)
IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SEARCH_SQL.SEARCH';

   L_search_max     NUMBER(10) := 0;
   L_no_full_access NUMBER(10) := 0;

   cursor C_PROMO_OBJ is
      select NEW OBJ_PROMOTION_REC(rp.promo_id,
                                   rp.promo_display_id,
                                   rp.name,
                                   rpe.theme,
                                   rp.start_date,
                                   rp.end_date,
                                   rp.currency_code,
                                   rpe.promo_event_id,
                                   rpe.description,
                                   rpe.promo_event_display_id,
                                   rpe.start_date,
                                   rpe.end_date,
                                   -- the value for full_access is used by the UI to enable/disable
                                   -- the delete button in the Promotion Component UI
                                   case
                                      when L_no_full_access = 1
                                        or EXISTS (select 1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd
                                                    where rpc.promo_id       = rp.promo_id
                                                      and rpc.promo_comp_id  = rpd.promo_comp_id
                                                      and rpd.state         != RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE)
                                        or EXISTS (select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd,
                                                          rpm_promo_dtl_merch_node rpdm,
                                                          rpm_promo_zone_location rpzl,
                                                          item_master im
                                                    where rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                                RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                                      and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                                                      and rpdm.item         = im.item
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd,
                                                          rpm_promo_dtl_merch_node rpdm,
                                                          rpm_promo_zone_location rpzl,
                                                          rpm_promo_dtl_skulist rpds,
                                                          item_master im
                                                    where rpc.promo_id        = rp.promo_id
                                                      and rpc.promo_comp_id   = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id    = rpdm.promo_dtl_id
                                                      and rpdm.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                                      and rpzl.promo_dtl_id   = rpdm.promo_dtl_id
                                                      and rpds.price_event_id = rpdm.promo_dtl_id
                                                      and rpds.skulist        = rpdm.skulist
                                                      and rpds.item           = im.item
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd,
                                                          rpm_promo_dtl_merch_node rpdm,
                                                          rpm_promo_zone_location rpzl,
                                                          rpm_merch_list_detail rmld,
                                                          item_master im
                                                    where rpc.promo_id       = rp.promo_id
                                                      and rpd.promo_dtl_id   = rpdm.promo_dtl_id
                                                      and rpdm.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                                      and rpzl.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rmld.merch_list_id = rpdm.price_event_itemlist
                                                      and rmld.item          = im.item
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd,
                                                          rpm_promo_dtl_merch_node rpdm,
                                                          rpm_promo_zone_location rpzl,
                                                          subclass sc
                                                    where rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rpdm.merch_type   IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                                RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                                RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                                                      and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                                                      and rpdm.dept         = sc.dept
                                                      and sc.class          = NVL(rpdm.class, sc.class)
                                                      and sc.subclass       = NVL(rpdm.subclass, sc.subclass)
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = sc.dept
                                                                         and sdcs.class         = sc.class
                                                                         and sdcs.subclass      = sc.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   -- storewide
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd,
                                                          rpm_promo_dtl_merch_node rpdm,
                                                          rpm_promo_zone_location rpzl,
                                                          rpm_zone_location rzl,
                                                          item_loc il,
                                                          item_master im
                                                    where rpc.type          = RPM_CONSTANTS.TRANSACTION_CODE
                                                      and rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rpdm.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                                      and rpzl.promo_dtl_id = rpd.promo_dtl_id
                                                      and rzl.zone_id       = NVL(rpzl.zone_id, rzl.zone_id)
                                                      and rzl.location      = NVL(rpzl.location, rzl.location)
                                                      and rzl.location      = il.loc
                                                      and il.item           = im.item
                                                      and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                      and im.tran_level     = im.item_level
                                                      and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))) then
                                         0
                                      else
                                         1
                                   end, -- full_access
                                   0, -- hist_ind
                                   case
                                      when EXISTS (select 1
                                                     from rpm_promo_comp rpc,
                                                          rpm_promo_dtl rpd
                                                    where rpc.promo_id           = rp.promo_id
                                                      and rpc.promo_comp_id      = rpd.promo_comp_id
                                                      and rpd.timebased_dtl_ind  = 1) then
                                         1
                                      else
                                         0
                                   end,  -- timebased_dtl_ind
                                   rp.cust_attr_id,
                                   rp.cust_attr_promo_comp_ind,
                                   rp.cust_attr_promo_dtl_ind)
        from gtt_num_num_str_str_date_date ids,
             rpm_promo rp,
             rpm_promo_event rpe
       where ids.number_2           = 0
         and rp.promo_id            = ids.number_1
         and rpe.promo_event_id (+) = rp.promo_event_id
      union all
      select NEW OBJ_PROMOTION_REC(rp.promo_id,
                                   rp.promo_display_id,
                                   rp.name,
                                   rpe.theme,
                                   rp.start_date,
                                   rp.end_date,
                                   rp.currency_code,
                                   rpe.promo_event_id,
                                   rpe.description,
                                   rpe.promo_event_display_id,
                                   rpe.start_date,
                                   rpe.end_date,
                                   -- the value for full_access is used by the UI to enable/disable
                                   -- the delete button in the Promotion Component UI
                                   case
                                      when L_no_full_access = 1
                                        or EXISTS (select 1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd
                                                    where rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.state         = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE)
                                        or EXISTS (select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd,
                                                          rpm_promo_dtl_merch_node_hist rpdm,
                                                          rpm_promo_zone_location_hist rpzl,
                                                          item_master im
                                                    where rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                                RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                                      and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                                                      and rpdm.item         = im.item
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd,
                                                          rpm_promo_dtl_merch_node_hist rpdm,
                                                          rpm_promo_zone_location_hist rpzl,
                                                          rpm_promo_dtl_skulist_hist rpds,
                                                          item_master im
                                                    where rpc.promo_id        = rp.promo_id
                                                      and rpc.promo_comp_id   = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id    = rpdm.promo_dtl_id
                                                      and rpdm.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                                      and rpzl.promo_dtl_id   = rpdm.promo_dtl_id
                                                      and rpds.price_event_id = rpdm.promo_dtl_id
                                                      and rpds.skulist        = rpdm.skulist
                                                      and rpds.item           = im.item
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd,
                                                          rpm_promo_dtl_merch_node_hist rpdm,
                                                          rpm_promo_zone_location_hist rpzl,
                                                          rpm_merch_list_detail rmld,
                                                          item_master im
                                                    where rpc.promo_id       = rp.promo_id
                                                      and rpd.promo_dtl_id   = rpdm.promo_dtl_id
                                                      and rpdm.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                                      and rpzl.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rmld.merch_list_id = rpdm.price_event_itemlist
                                                      and rmld.item          = im.item
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd,
                                                          rpm_promo_dtl_merch_node_hist rpdm,
                                                          rpm_promo_zone_location_hist rpzl,
                                                          subclass sc
                                                    where rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rpdm.merch_type   IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                                RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                                RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                                                      and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                                                      and rpdm.dept         = sc.dept
                                                      and sc.class          = NVL(rpdm.class, sc.class)
                                                      and sc.subclass       = NVL(rpdm.subclass, sc.subclass)
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = sc.dept
                                                                         and sdcs.class         = sc.class
                                                                         and sdcs.subclass      = sc.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))
                                                   union all
                                                   -- storewide
                                                   select /*+ ORDERED */
                                                          1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd,
                                                          rpm_promo_dtl_merch_node_hist rpdm,
                                                          rpm_promo_zone_location_hist rpzl,
                                                          rpm_zone_location rzl,
                                                          item_loc il,
                                                          item_master im
                                                    where rpc.type          = RPM_CONSTANTS.TRANSACTION_CODE
                                                      and rpc.promo_id      = rp.promo_id
                                                      and rpc.promo_comp_id = rpd.promo_comp_id
                                                      and rpd.promo_dtl_id  = rpdm.promo_dtl_id
                                                      and rpdm.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                                      and rpzl.promo_dtl_id = rpd.promo_dtl_id
                                                      and rzl.zone_id       = NVL(rpzl.zone_id, rzl.zone_id)
                                                      and rzl.location      = NVL(rpzl.location, rzl.location)
                                                      and rzl.location      = il.loc
                                                      and il.item           = im.item
                                                      and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                      and im.tran_level     = im.item_level
                                                      and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                      and NOT EXISTS (select 1
                                                                        from rpm_pc_search_merch_tbl sdcs,
                                                                             rpm_zone_locs_tbl rzlt
                                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                                         and sdcs.dept          = im.dept
                                                                         and sdcs.class         = im.class
                                                                         and sdcs.subclass      = im.subclass
                                                                         and rzlt.location_id   = NVL(rpzl.location, rzlt.location_id)
                                                                         and rzlt.zone_id       = NVL(rpzl.zone_id, rzlt.zone_id))) then
                                         0
                                      else
                                         1
                                   end,
                                   1,
                                   case
                                      when EXISTS (select 1
                                                     from rpm_promo_comp_hist rpc,
                                                          rpm_promo_dtl_hist rpd
                                                    where rpc.promo_id          = rp.promo_id
                                                      and rpc.promo_comp_id     = rpd.promo_comp_id
                                                      and rpd.timebased_dtl_ind = 1) then
                                         1
                                      else
                                         0
                                   end,
                                   rp.cust_attr_id,
                                   rp.cust_attr_promo_comp_ind,
                                   rp.cust_attr_promo_dtl_ind)
        from gtt_num_num_str_str_date_date ids,
             rpm_promo_hist rp,
             rpm_promo_event rpe
       where ids.number_2           = 1
         and rp.promo_id            = ids.number_1
         and rpe.promo_event_id (+) = rp.promo_event_id;

BEGIN

   LP_criteria := I_criteria_tbl(1);
   LP_exclude_no_comp := I_exclude_no_comp;

   select promotion_search_max into L_search_max
     from rpm_system_options;

   if L_search_max is NULL or
      L_search_max = 0 then
      ---
      L_search_max := 301;
   else
      L_search_max := L_search_max + 1;
   end if;

   -- Clear the result table
   delete
     from gtt_num_num_str_str_date_date;

   -- Clear Criteria Table
   delete
     from rpm_pc_search_char_tbl;
   delete
     from rpm_pc_search_num_tbl;
   delete
     from rpm_pc_search_merch_tbl;

   -- Check whether the USER_ID is populated
   -- If it is not populated, then it will be either
   --    search by EVENT_ID
   --    search by THRESHOLD_ID
   --    search by ZONE_ID
   --    search by PRICE_GUIDE_ID
   -- The call to this package with this condition will not be coming from the Maintain Promotion UI

   if LP_criteria.user_id is NULL then

      if LP_criteria.promotion_event_id is NOT NULL then

         if SEARCH_EVENT_ID(O_error_msg,
                            LP_criteria.promotion_event_id) = FALSE then
            O_return_code := 0;
            return;
         end if;

      elsif LP_criteria.threshold_id is NOT NULL then

         if SEARCH_THRESHOLD_ID(O_error_msg,
                                LP_criteria.threshold_id)  = FALSE then
            O_return_code := 0;
            return;
         end if;

      elsif LP_criteria.zones is NOT NULL and
            LP_criteria.zones.COUNT > 0 then
         ---
         if SEARCH_ZONE_IDS(O_error_msg,
                            LP_criteria.zones) = FALSE then
            O_return_code := 0;
            return;
         end if;

      elsif LP_criteria.price_guide_id is NOT NULL then

         if SEARCH_PRICE_GUIDE_ID(O_error_msg,
                                  LP_criteria.price_guide_id)  = FALSE then
            O_return_code := 0;
            return;
         end if;

      end if;

      L_no_full_access := 1;

      open C_PROMO_OBJ;
      fetch C_PROMO_OBJ BULK COLLECT into O_search_tbl
                        LIMIT L_search_max;
      close C_PROMO_OBJ;

      O_ps_count := O_search_tbl.COUNT;
      O_return_code := 1;

      return;

   end if;

   --Security Dept, Class, Subclass
   insert into rpm_pc_search_merch_tbl (criteria_type,
                                        dept,
                                        class,
                                        subclass)
      select 'SECURITY_DEPT_CLASS_SUB',
             v.dept,
             v.class,
             v.subclass
        from (select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_DEPT
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = LP_criteria.user_id
                 and sc.dept                = TO_NUMBER(rhp.key_value)
              union all
              select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_CLASS
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = LP_criteria.user_id
                 and rhp.key_value          = TO_CHAR(sc.dept||';'||sc.class)
              union all
              select sc.dept,
                     sc.class,
                     sc.subclass
                from rsm_hierarchy_permission rhp,
                     rsm_role_hierarchy_perm rrhp,
                     rsm_hierarchy_type rht,
                     rsm_user_role rur,
                     subclass sc
               where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS
                 and rhp.id                 = rrhp.parent_id
                 and rrhp.hierarchy_type_id = rht.id
                 and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                 and rrhp.role_id           = rur.role_id
                 and rur.user_id            = LP_criteria.user_id
                 and rhp.key_value          = TO_CHAR(sc.dept||';'||sc.class||';'||sc.subclass)) v;

   insert into rpm_zone_locs_tbl (zone_group_id,
                                  zone_id,
                                  location_id)
      select v.zone_group_id,
             v.zone_id,
             rzl.location
        from rpm_zone_location rzl,
             (select TO_NUMBER(hp.key_value) zone_group_id,
                     rz.zone_id
                from rsm_hierarchy_permission hp,
                     rsm_role_hierarchy_perm rhp,
                     rsm_hierarchy_type ht,
                     rsm_user_role rur,
                     rpm_zone rz
               where hp.reference_class = RPM_CONSTANTS.RSM_REF_ZONE_GROUP
                 and hp.child_id        is NULL
                 and ht.id              = rhp.hierarchy_type_id
                 and ht.external_id     = RPM_CONSTANTS.RSM_ORG_HIER
                 and hp.id              = rhp.parent_id
                 and rhp.role_id        = rur.role_id
                 and rur.user_id        = LP_criteria.user_id
                 and rz.zone_group_id   = TO_NUMBER(hp.key_value)
              union all
              select TO_NUMBER(hp2.key_value) zone_group_id,
                     TO_NUMBER(hp1.key_value) zone_id
                from rsm_hierarchy_permission hp1,
                     rsm_hierarchy_permission hp2,
                     rsm_role_hierarchy_perm rhp,
                     rsm_hierarchy_type ht,
                     rsm_user_role rur
               where hp1.reference_class = RPM_CONSTANTS.RSM_REF_ZONE
                 and hp1.child_id        is NULL
                 and hp2.child_id        = hp1.id
                 and ht.id               = rhp.hierarchy_type_id
                 and ht.external_id      = RPM_CONSTANTS.RSM_ORG_HIER
                 and rhp.role_id         = rur.role_id
                 and rur.user_id         = LP_criteria.user_id
                 and hp2.id              = rhp.parent_id) v
       where rzl.zone_id = v.zone_id;

   -- Promotion Display ID
   -- If this is populated, the other search criteria will not matter
   if LP_criteria.promotion_display_id is NOT NULL and
      LP_criteria.promotion_display_id.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.promotion_display_id.COUNT
         insert into rpm_pc_search_char_tbl(criteria_type,
                                            varchar_id)
            values ('PROMOTION_DISPLAY_ID',
                    LP_criteria.promotion_display_id(i));

      if SEARCH_PROMO_ID(O_error_msg) = FALSE then
         O_return_code := 0;
         return;
      end if;

      open C_PROMO_OBJ;
      fetch C_PROMO_OBJ BULK COLLECT into O_search_tbl
                        LIMIT L_search_max;
      close C_PROMO_OBJ;

      O_ps_count := O_search_tbl.COUNT;
      O_return_code := 1;

      return;

   end if;

   -- If Dept/Class/Subclass is NULL and
   --    Item is NULL and
   --    Item List is NULL and
   --    PEIL is NULL and
   --    Storewide was not searched
   -- This is coming from the Maintain Component or Create Component

   if (LP_criteria.dept_class_subclasses is NULL or
       LP_criteria.dept_class_subclasses.COUNT = 0) and
      (LP_criteria.items is NULL or
       LP_criteria.items.COUNT = 0) and
      LP_criteria.item_list_id is NULL and
      LP_criteria.price_event_item_list_id is NULL and
      NVL(LP_criteria.item_level_ind, -9999) != LP_PROMO_SEARCH_STORE_WIDE then
      ---
      if SEARCH_NO_CRIT(O_error_msg) = FALSE then
         O_return_code := 0;
         return;
      end if;

      L_no_full_access := 1;

      open C_PROMO_OBJ;
      fetch C_PROMO_OBJ BULK COLLECT into O_search_tbl;
      close C_PROMO_OBJ;

      O_ps_count := O_search_tbl.COUNT;
      O_return_code := 1;

      return;

   end if;

   -- Populate temporary tables for multiple value criteria

   if LP_criteria.states is NOT NULL and
      LP_criteria.states.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.states.COUNT
         insert into rpm_pc_search_num_tbl(criteria_type,
                                           numeric_id)
            values ('STATES',
                    LP_criteria.states(i));

   end if;

   if LP_criteria.locations is NOT NULL and
      LP_criteria.locations.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.locations.COUNT
         insert into rpm_pc_search_num_tbl(criteria_type,
                                           numeric_id)
            values('LOCATIONS',
                   LP_criteria.locations(i));

   end if;

   if LP_criteria.zones is NOT NULL and
      LP_criteria.zones.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.zones.COUNT
         insert into rpm_pc_search_num_tbl(criteria_type,
                                           numeric_id)
            values('ZONES',
                   LP_criteria.zones(i));

   end if;

   if LP_criteria.deal_id is NOT NULL and
      LP_criteria.deal_id.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.deal_id.COUNT
         insert into rpm_pc_search_num_tbl(criteria_type,
                                           numeric_id)
            values('DEAL_IDS',
                   LP_criteria.DEAL_ID(i));

   end if;

   if LP_criteria.customer_types is NOT NULL and
      LP_criteria.customer_types.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.customer_types.COUNT
         insert into rpm_pc_search_num_tbl(criteria_type,
                                           numeric_id)
            values('CUST_TYPES',
                   LP_criteria.customer_types(i));

   end if;

   if LP_criteria.items is NOT NULL and
      LP_criteria.items.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.items.COUNT
         insert into rpm_pc_search_char_tbl(criteria_type,
                                            varchar_id)
            values('ITEMS',
                   LP_criteria.items(i));

      if LP_criteria.item_level_ind = LP_PROMO_SEARCH_ITEM_LEVEL then

         -- Item
         if SEARCH_ITEM(O_error_msg) = FALSE then
            O_return_code := 0;
            return;
         end if;

      elsif LP_criteria.item_level_ind = LP_PROMO_SEARCH_PARENT_LEVEL then

         -- Parent
         if SEARCH_ITEM_PARENT(O_error_msg) = FALSE then
            O_return_code := 0;
            return;
         end if;

      elsif LP_criteria.item_level_ind = LP_PROMO_SEARCH_DIFF_LEVEL then

         -- Parent Diff
         if SEARCH_ITEM_PARENT_DIFF(O_error_msg) = FALSE then
            O_return_code := 0;
            return;
         end if;

      end if;

   elsif LP_criteria.item_level_ind = LP_PROMO_SEARCH_STORE_WIDE then

      -- Storewide
      if SEARCH_STORE_WIDE(O_error_msg) = FALSE then
         O_return_code := 0;
         return;
      end if;

   elsif LP_criteria.item_itemlist_ind = LP_PROMO_SEARCH_ITEM_LIST then

      -- Item List
      if SEARCH_ITEM_LIST(O_error_msg) = FALSE then
         O_return_code := 0;
         return;
      end if;

   elsif LP_criteria.item_itemlist_ind = LP_PROMO_SEARCH_PEIL then

      -- Price Event Item List
      if SEARCH_PRICE_EVENT_ITEM_LIST(O_error_msg) = FALSE then
         O_return_code := 0;
         return;
      end if;

   elsif LP_criteria.dept_class_subclasses is NOT NULL and
         LP_criteria.dept_class_subclasses.COUNT > 0 then
      ---
      forall i IN 1..LP_criteria.dept_class_subclasses.COUNT
         insert into rpm_pc_search_merch_tbl(criteria_type,
                                             dept,
                                             class,
                                             subclass)
            values ('DEPT_CLASS_SUBCLASSES',
                    LP_criteria.dept_class_subclasses(i).dept,
                    LP_criteria.dept_class_subclasses(i).class,
                    LP_criteria.dept_class_subclasses(i).subclass);

      --Dept, Class, Subclass
      if LP_criteria.item_level_ind = LP_PROMO_SEARCH_MERCH_HIER and
         LP_criteria.exclusive_item_level_ind = 'Y' then
         ---
         if SEARCH_MERCH_HIERARCHY(O_error_msg) = FALSE then
            O_return_code := 0;
            return;
         end if;

      else
         if SEARCH_DEPT_CLASS_SUB(O_error_msg) = FALSE then
            O_return_code := 0;
            return;
         end if;

      end if;
   end if;

   open C_PROMO_OBJ;
   fetch C_PROMO_OBJ BULK COLLECT into O_search_tbl
                     LIMIT L_search_max;
   close C_PROMO_OBJ;

   O_ps_count := O_search_tbl.COUNT;
   O_return_code := 1;

   return;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;
      return;

END SEARCH;

-----------------------------------------------------------------------------------------

PROCEDURE REFRESH_STATUS(O_return_code    OUT NUMBER,
                         O_error_msg      OUT VARCHAR2,
                         O_search_tbl     OUT OBJ_PROMO_DTL_REFRESH_TBL,
                         I_criteria    IN     OBJ_VARCHAR_ID_TABLE)
IS

   L_program     VARCHAR2(40)  := 'RPM_PROMO_SEARCH_SQL.REFRESH_STATUS';

   cursor C_REFRESH_PROMO_STATE is
      select NEW OBJ_PROMO_DTL_REFRESH_REC(promo_dtl_id,
                                           state,
                                           end_date,
                                           promo_dtl_display_id,
                                           sge_ind)

        from (select dtl.promo_dtl_id,
                     dtl.state,
                     TO_CHAR(dtl.end_date, 'YYYYMMDDHH24MISS') end_date,
                     dtl.promo_dtl_display_id,
                     NVL(dtl.sys_generated_exclusion, 0) sge_ind
                from rpm_promo_dtl dtl,
                     rpm_pc_search_char_tbl t
               where dtl.promo_dtl_display_id = t.varchar_id
                 and criteria_type            = 'PROMO_DTL_IDS'
              union
              select dtl2.promo_dtl_id,
                     dtl2.state,
                     TO_CHAR(dtl2.end_date, 'YYYYMMDDHH24MISS') end_date,
                     dtl2.promo_dtl_display_id,
                     NVL(dtl2.sys_generated_exclusion, 0) sge_ind
                from rpm_promo_dtl dtl1,
                     rpm_promo_dtl dtl2,
                     rpm_pc_search_char_tbl t
               where dtl1.promo_dtl_display_id            = t.varchar_id
                 and dtl2.exception_parent_id             = dtl1.promo_dtl_id
                 and criteria_type                        = 'PROMO_DTL_IDS'
                 and NVL(dtl2.sys_generated_exclusion, 0) = 1);

BEGIN

   delete
     from rpm_pc_search_char_tbl;

   if I_criteria is NOT NULL and
      I_criteria.COUNT() > 0 then
      ---
      for I IN 1..I_criteria.COUNT() loop
         insert into rpm_pc_search_char_tbl(criteria_type,
                                            varchar_id)
                                     values('PROMO_DTL_IDS',
                                            I_criteria(I));
      end loop;
   end if;

   open C_REFRESH_PROMO_STATE;
   fetch C_REFRESH_PROMO_STATE BULK COLLECT into O_search_tbl;
   close C_REFRESH_PROMO_STATE;

   O_return_code := 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;

END REFRESH_STATUS;

-----------------------------------------------------------------------------------------
-- Private Functions
-----------------------------------------------------------------------------------------
FUNCTION SEARCH_NO_CRIT(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SEARCH_SQL.SEARCH_NO_CRIT';

   L_count         NUMBER(10) := NULL;
   L_loc_sec_count NUMBER(10) := NULL;

BEGIN

   select COUNT(1) into L_count
     from (select dept,
                  class,
                  subclass
             from subclass
           minus
           select dept,
                  class,
                  subclass
             from rpm_pc_search_merch_tbl rpsmt
            where criteria_type = 'SECURITY_DEPT_CLASS_SUB');

   select COUNT(1) into L_loc_sec_count
     from (select zone_id
             from rpm_zone
           minus
           select zone_id
             from rpm_zone_locs_tbl);

   if L_count = 0 and
      L_loc_sec_count = 0 then

      -- The user has access to all merchandise hierarchy and org hierarchy
      -- Dont worry about checking the security
      insert into gtt_num_num_str_str_date_date(number_1,
                                                number_2)
      select rp.promo_id,
             0
        from rpm_promo rp
       where LP_exclude_no_comp      = 0
          or (    LP_exclude_no_comp = 1
              and EXISTS (select 1
                            from rpm_promo_comp rpc
                           where rpc.promo_id = rp.promo_id))
      union all
      select rp.promo_id,
             1
        from rpm_promo_hist rp
       where LP_exclude_no_comp      = 0
          or (    LP_exclude_no_comp = 1
              and EXISTS (select 1
                            from rpm_promo_comp_hist rpc
                           where rpc.promo_id = rp.promo_id));

    else
      -- The user does not have access to all merchandise hierarchy
      -- and organizational hierarchy
      -- Need to check the security

      insert into gtt_num_num_str_str_date_date(number_1,
                                                number_2)
      select rp.promo_id,
             0
        from rpm_promo rp
       where LP_exclude_no_comp = 0
         and NOT EXISTS (select 1
                           from rpm_promo_comp rpc
                          where rpc.promo_id = rp.promo_id)
      union all
      select /*+ ORDERED */
             distinct rpc.promo_id,
             0
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_zone_locs_tbl rzlt,
             rpm_promo_comp rpc
       where rpd.promo_dtl_id = rpzl.promo_dtl_id
         and rzlt.location_id = NVL(rpzl.location, rzlt.location_id)
         and rzlt.zone_id     = NVL(rpzl.zone_id, rzlt.zone_id)
         and rpd.promo_dtl_id IN (select distinct rpdm.promo_dtl_id
                                    from rpm_promo_dtl_merch_node rpdm
                                   where (    rpdm.merch_type IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                          and EXISTS (select 1
                                                        from item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where im.item         = rpdm.item
                                                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.dept         = sdcs.dept
                                                         and im.class        = sdcs.class
                                                         and im.subclass     = sdcs.subclass
                                                         and rownum          = 1))
                                      or (    rpdm.merch_type IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                                          and EXISTS (select 1
                                                        from rpm_pc_search_merch_tbl sdcs
                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                         and sdcs.dept          = rpdm.dept
                                                         and sdcs.class         = NVL(rpdm.class, sdcs.class)
                                                         and sdcs.subclass      = NVL(rpdm.subclass, sdcs.subclass)
                                                         and rownum             = 1))
                                      or (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                          and EXISTS (select 1
                                                        from rpm_promo_dtl_skulist pds,
                                                             item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where rpdm.promo_dtl_id = pds.price_event_id
                                                         and rpdm.skulist      = pds.skulist
                                                         and im.item           = pds.item
                                                         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.dept           = sdcs.dept
                                                         and im.class          = sdcs.class
                                                         and im.subclass       = sdcs.subclass
                                                         and rownum            = 1))
                                      or (    rpdm.merch_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                          and EXISTS (select 1
                                                        from rpm_merch_list_detail rmld,
                                                             item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where rpdm.price_event_itemlist = rmld.merch_list_id
                                                         and im.item                   = rmld.item
                                                         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.dept                   = sdcs.dept
                                                         and im.class                  = sdcs.class
                                                         and im.subclass               = sdcs.subclass
                                                         and rownum                    = 1))
                                      or (    rpdm.merch_type = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                          and EXISTS (select 1
                                                        from rpm_item_loc ril,
                                                             item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where ril.loc         = rzlt.location_id
                                                         and ril.item        = im.item
                                                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.dept         = sdcs.dept
                                                         and im.class        = sdcs.class
                                                         and im.subclass     = sdcs.subclass
                                                         and rownum          = 1)))
         and rpc.promo_comp_id = rpd.promo_comp_id;

      insert into gtt_num_num_str_str_date_date(number_1,
                                                number_2)
      select rp.promo_id,
             1
        from rpm_promo_hist rp
       where LP_exclude_no_comp = 0
         and NOT EXISTS (select 1
                           from rpm_promo_comp_hist rpc
                          where rpc.promo_id = rp.promo_id)
      union all
      select /*+ ORDERED */
             distinct rpc.promo_id,
             1
        from rpm_promo_dtl_hist rpd,
             rpm_promo_zone_location_hist rpzl,
             rpm_zone_locs_tbl rzlt,
             rpm_promo_comp_hist rpc
       where rpd.promo_dtl_id = rpzl.promo_dtl_id
         and rzlt.location_id = NVL(rpzl.location, rzlt.location_id)
         and rzlt.zone_id     = NVL(rpzl.zone_id, rzlt.zone_id)
         and rpd.promo_dtl_id IN (select distinct rpdm.promo_dtl_id
                                    from rpm_promo_dtl_merch_node_hist rpdm
                                   where (    rpdm.merch_type IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                          and EXISTS (select 1
                                                        from item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where im.item         = rpdm.item
                                                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.dept         = sdcs.dept
                                                         and im.class        = sdcs.class
                                                         and im.subclass     = sdcs.subclass
                                                         and rownum          = 1))
                                      or (    rpdm.merch_type IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                  RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                                          and EXISTS (select 1
                                                        from rpm_pc_search_merch_tbl sdcs
                                                       where sdcs.criteria_type = 'SECURITY_DEPT_CLASS_SUB'
                                                         and sdcs.dept          = rpdm.dept
                                                         and sdcs.class         = NVL(rpdm.class, sdcs.class)
                                                         and sdcs.subclass      = NVL(rpdm.subclass, sdcs.subclass)
                                                         and rownum             = 1))
                                      or (    rpdm.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                                          and EXISTS (select 1
                                                        from rpm_promo_dtl_skulist_hist pds,
                                                             item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where rpdm.promo_dtl_id = pds.price_event_id
                                                         and rpdm.skulist      = pds.skulist
                                                         and im.item           = pds.item
                                                         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.dept           = sdcs.dept
                                                         and im.class          = sdcs.class
                                                         and im.subclass       = sdcs.subclass
                                                         and rownum            = 1))
                                      or (    rpdm.merch_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                                          and EXISTS (select 1
                                                        from rpm_merch_list_detail rmld,
                                                             item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where rpdm.price_event_itemlist = rmld.merch_list_id
                                                         and im.item                   = rmld.item
                                                         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.dept                   = sdcs.dept
                                                         and im.class                  = sdcs.class
                                                         and im.subclass               = sdcs.subclass
                                                         and rownum                    = 1))
                                      or (    rpdm.merch_type = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                                          and EXISTS (select 1
                                                        from rpm_item_loc ril,
                                                             item_master im,
                                                             (select rpsmt.dept,
                                                                     rpsmt.class,
                                                                     rpsmt.subclass
                                                                from rpm_pc_search_merch_tbl rpsmt
                                                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                                                       where ril.loc         = rzlt.location_id
                                                         and ril.item        = im.item
                                                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                                         and im.dept         = sdcs.dept
                                                         and im.class        = sdcs.class
                                                         and im.subclass     = sdcs.subclass
                                                         and rownum          = 1)))
         and rpc.promo_comp_id = rpd.promo_comp_id;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEARCH_NO_CRIT;
-----------------------------------------------------------------------------------------
FUNCTION SEARCH_PROMO_ID(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SEARCH_SQL.SEARCH_PROMO_ID';

BEGIN

   insert into gtt_num_num_str_str_date_date
      (number_1,
       number_2)
         select rp.promo_id,
                0
           from rpm_promo rp,
                rpm_pc_search_char_tbl rpsct
          where rpsct.criteria_type = 'PROMOTION_DISPLAY_ID'
            and rp.promo_display_id = rpsct.varchar_id
            and (   NOT EXISTS (select 1
                                  from rpm_promo_comp rpc
                                 where rpc.promo_id = rp.promo_id)
                 or EXISTS (select /*+ ORDERED */ 1
                              from rpm_promo_comp rpc,
                                   rpm_promo_dtl rpd,
                                   rpm_promo_dtl_merch_node rpdm,
                                   item_master im,
                                   (select dept,
                                           class,
                                           subclass
                                      from rpm_pc_search_merch_tbl
                                     where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                                   rpm_promo_zone_location rpzl,
                                   rpm_zone_locs_tbl rzlt
                             where rpc.promo_id      = rp.promo_id
                               and rpd.promo_comp_id = rpc.promo_comp_id
                               and rpdm.promo_dtl_id = rpd.promo_dtl_id
                               and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                         RPM_CONSTANTS.PARENT_ITEM_DIFF)
                               and im.item           = rpdm.item
                               and im.dept           = sdcs.dept
                               and im.class          = sdcs.class
                               and im.subclass       = sdcs.subclass
                               and rpzl.promo_dtl_id = rpd.promo_dtl_id
                               and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                               and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
                 or EXISTS (select /*+ ORDERED */ 1
                              from rpm_promo_comp rpc,
                                   rpm_promo_dtl rpd,
                                   rpm_promo_dtl_merch_node rpdm,
                                   (select dept,
                                           class,
                                           subclass
                                      from rpm_pc_search_merch_tbl
                                     where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                                   rpm_promo_zone_location rpzl,
                                   rpm_zone_locs_tbl rzlt
                             where rpc.promo_id      = rp.promo_id
                               and rpd.promo_comp_id = rpc.promo_comp_id
                               and rpdm.promo_dtl_id = rpd.promo_dtl_id
                               and rpdm.merch_type   IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                         RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                               and sdcs.dept         = rpdm.dept
                               and sdcs.class        = NVL(rpdm.class, sdcs.class)
                               and sdcs.subclass     = NVL(rpdm.subclass, sdcs.subclass)
                               and rpzl.promo_dtl_id = rpd.promo_dtl_id
                               and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                               and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
                 or EXISTS (select /*+ ORDERED */  1
                              from rpm_promo_comp rpc,
                                   rpm_promo_dtl rpd,
                                   rpm_promo_dtl_merch_node rpdm,
                                   rpm_promo_dtl_skulist pds,
                                   item_master im,
                                   (select dept,
                                           class,
                                           subclass
                                      from rpm_pc_search_merch_tbl
                                     where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                                   rpm_promo_zone_location rpzl,
                                   rpm_zone_locs_tbl rzlt
                             where rpc.promo_id      = rp.promo_id
                               and rpd.promo_comp_id = rpc.promo_comp_id
                               and rpdm.promo_dtl_id = rpd.promo_dtl_id
                               and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                               and rpdm.promo_dtl_id = pds.price_event_id
                               and rpdm.skulist      = pds.skulist
                               and pds.item          = im.item
                               and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and sdcs.dept         = im.dept
                               and sdcs.class        = im.class
                               and sdcs.subclass     = im.subclass
                               and rpzl.promo_dtl_id = rpd.promo_dtl_id
                               and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                               and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
                 or EXISTS (select /*+ ORDERED */  1
                              from rpm_promo_comp rpc,
                                   rpm_promo_dtl rpd,
                                   rpm_promo_dtl_merch_node rpdm,
                                   rpm_merch_list_detail rmld,
                                   item_master im,
                                   (select dept,
                                           class,
                                           subclass
                                      from rpm_pc_search_merch_tbl
                                     where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                                   rpm_promo_zone_location rpzl,
                                   rpm_zone_locs_tbl rzlt
                             where rpc.promo_id              = rp.promo_id
                               and rpd.promo_comp_id         = rpc.promo_comp_id
                               and rpdm.promo_dtl_id         = rpd.promo_dtl_id
                               and rpdm.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                               and rpdm.price_event_itemlist = rmld.merch_list_id
                               and rmld.item                 = im.item
                               and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and sdcs.dept                 = im.dept
                               and sdcs.class                = im.class
                               and sdcs.subclass             = im.subclass
                               and rpzl.promo_dtl_id         = rpd.promo_dtl_id
                               and rzlt.location_id          = NVL(rpzl.location, rzlt.location_id)
                               and rzlt.zone_id              = NVL(rpzl.zone_id, rzlt.zone_id))
                 or EXISTS (select /*+ ORDERED */ 1
                              from rpm_promo_comp rpc,
                                   rpm_promo_dtl rpd,
                                   rpm_promo_dtl_merch_node rpdm,
                                   (select dept,
                                           class,
                                           subclass
                                      from rpm_pc_search_merch_tbl
                                     where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                                   rpm_promo_zone_location rpzl,
                                   rpm_zone_locs_tbl rzlt
                             where rpc.promo_id      = rp.promo_id
                               and rpd.promo_comp_id = rpc.promo_comp_id
                               and rpdm.promo_dtl_id = rpd.promo_dtl_id
                               and rpdm.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                               and rpzl.promo_dtl_id = rpd.promo_dtl_id
                               and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                               and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)));

   insert into gtt_num_num_str_str_date_date
      (number_1,
       number_2)
   select rp.promo_id,
          1
     from rpm_promo_hist rp,
          rpm_pc_search_char_tbl rpsct
    where rpsct.criteria_type = 'PROMOTION_DISPLAY_ID'
      and rp.promo_display_id = rpsct.varchar_id
      and (   NOT EXISTS (select 1
                            from rpm_promo_comp_hist rpc
                           where rpc.promo_id = rp.promo_id)
           or EXISTS (select /*+ ORDERED */ 1
                        from rpm_promo_comp_hist rpc,
                             rpm_promo_dtl_hist rpd,
                             rpm_promo_dtl_merch_node_hist rpdm,
                             item_master im,
                             (select dept,
                                     class,
                                     subclass
                                from rpm_pc_search_merch_tbl
                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                             rpm_promo_zone_location_hist rpzl,
                             rpm_zone_locs_tbl rzlt
                       where rpc.promo_id      = rp.promo_id
                         and rpd.promo_comp_id = rpc.promo_comp_id
                         and rpdm.promo_dtl_id = rpd.promo_dtl_id
                         and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                   RPM_CONSTANTS.PARENT_ITEM_DIFF)
                         and im.item           = rpdm.item
                         and im.dept           = sdcs.dept
                         and im.class          = sdcs.class
                         and im.subclass       = sdcs.subclass
                         and rpzl.promo_dtl_id = rpd.promo_dtl_id
                         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
           or EXISTS (select /*+ ORDERED */ 1
                        from rpm_promo_comp_hist rpc,
                             rpm_promo_dtl_hist rpd,
                             rpm_promo_dtl_merch_node_hist rpdm,
                             (select dept,
                                     class,
                                     subclass
                                from rpm_pc_search_merch_tbl
                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                             rpm_promo_zone_location_hist rpzl,
                             rpm_zone_locs_tbl rzlt
                       where rpc.promo_id      = rp.promo_id
                         and rpd.promo_comp_id = rpc.promo_comp_id
                         and rpdm.promo_dtl_id = rpd.promo_dtl_id
                         and rpdm.merch_type   IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                   RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                   RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                         and sdcs.dept         = rpdm.dept
                         and sdcs.class        = NVL(rpdm.class, sdcs.class)
                         and sdcs.subclass     = NVL(rpdm.subclass, sdcs.subclass)
                         and rpzl.promo_dtl_id = rpd.promo_dtl_id
                         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
           or EXISTS (select /*+ ORDERED */  1
                        from rpm_promo_comp_hist rpc,
                             rpm_promo_dtl_hist rpd,
                             rpm_promo_dtl_merch_node_hist rpdm,
                             rpm_promo_dtl_skulist_hist pds,
                             item_master im,
                             (select dept,
                                     class,
                                     subclass
                                from rpm_pc_search_merch_tbl
                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                             rpm_promo_zone_location_hist rpzl,
                             rpm_zone_locs_tbl rzlt
                       where rpc.promo_id      = rp.promo_id
                         and rpd.promo_comp_id = rpc.promo_comp_id
                         and rpdm.promo_dtl_id = rpd.promo_dtl_id
                         and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpdm.promo_dtl_id = pds.price_event_id
                         and rpdm.skulist      = pds.skulist
                         and pds.item          = im.item
                         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                         and sdcs.dept         = im.dept
                         and sdcs.class        = im.class
                         and sdcs.subclass     = im.subclass
                         and rpzl.promo_dtl_id = rpd.promo_dtl_id
                         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
           or EXISTS (select /*+ ORDERED */  1
                        from rpm_promo_comp_hist rpc,
                             rpm_promo_dtl_hist rpd,
                             rpm_promo_dtl_merch_node_hist rpdm,
                             rpm_merch_list_detail rmld,
                             item_master im,
                             (select dept,
                                     class,
                                     subclass
                                from rpm_pc_search_merch_tbl
                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                             rpm_promo_zone_location_hist rpzl,
                             rpm_zone_locs_tbl rzlt
                       where rpc.promo_id              = rp.promo_id
                         and rpd.promo_comp_id         = rpc.promo_comp_id
                         and rpdm.promo_dtl_id         = rpd.promo_dtl_id
                         and rpdm.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rpdm.price_event_itemlist = rmld.merch_list_id
                         and rmld.item                 = im.item
                         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                         and sdcs.dept                 = im.dept
                         and sdcs.class                = im.class
                         and sdcs.subclass             = im.subclass
                         and rpzl.promo_dtl_id = rpd.promo_dtl_id
                         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id))
           or EXISTS (select /*+ ORDERED */  1
                        from rpm_promo_comp_hist rpc,
                             rpm_promo_dtl_hist rpd,
                             rpm_promo_dtl_merch_node_hist rpdm,
                             (select dept,
                                     class,
                                     subclass
                                from rpm_pc_search_merch_tbl
                               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                             rpm_promo_zone_location_hist rpzl,
                             rpm_zone_locs_tbl rzlt
                       where rpc.promo_id      = rp.promo_id
                         and rpd.promo_comp_id = rpc.promo_comp_id
                         and rpdm.promo_dtl_id = rpd.promo_dtl_id
                         and rpdm.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                         and rpzl.promo_dtl_id = rpd.promo_dtl_id
                         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)));

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEARCH_PROMO_ID;

-----------------------------------------------------------------------------------------
FUNCTION SEARCH_ITEM_LIST (O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SEARCH_SQL.SEARCH_ITEM_LIST';

BEGIN

   -- From Regular Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct
          promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item                                        = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpdm.diff_id                                   IN (im.diff_1,
                                                                     im.diff_2,
                                                                     im.diff_3,
                                                                     im.diff_4)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and rpdm.skulist      = LP_criteria.item_list_id
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              and EXISTS (select 1
                            from rpm_promo_dtl_skulist rps,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rps.price_event_id = rpdm.promo_dtl_id
                             and rps.skulist        = rpdm.skulist
                             and im.item            = rps.item
                             and im.dept            = sdcs.dept
                             and im.class           = sdcs.class
                             and im.subclass        = sdcs.subclass)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rpdm.price_event_itemlist                      = LP_criteria.price_event_item_list_id
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
              and EXISTS (select 1
                            from rpm_promo_dtl_skulist rps,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rps.price_event_id = rpdm.promo_dtl_id
                             and rps.skulist        = rpdm.skulist
                             and im.item            = rps.item
                             and im.dept            = sdcs.dept
                             and im.class           = sdcs.class
                             and im.subclass        = sdcs.subclass));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct
          promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item                                        = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpdm.diff_id                                   IN (im.diff_1,
                                                                     im.diff_2,
                                                                     im.diff_3,
                                                                     im.diff_4)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  skulist_detail sd,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and sd.skulist                                     = LP_criteria.item_list_id
              and im.item                                        = sd.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and rpdm.skulist      = LP_criteria.item_list_id
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              and EXISTS (select 1
                            from rpm_promo_dtl_skulist_hist rps,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rps.price_event_id = rpdm.promo_dtl_id
                             and rps.skulist        = rpdm.skulist
                             and im.item            = rps.item
                             and im.dept            = sdcs.dept
                             and im.class           = sdcs.class
                             and im.subclass        = sdcs.subclass)

           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rpdm.price_event_itemlist                      = LP_criteria.price_event_item_list_id
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
              and EXISTS (select 1
                            from rpm_promo_dtl_skulist_hist rps,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rps.price_event_id = rpdm.promo_dtl_id
                             and rps.skulist        = rpdm.skulist
                             and im.item            = rps.item
                             and im.dept            = sdcs.dept
                             and im.class           = sdcs.class
                             and im.subclass        = sdcs.subclass));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_ITEM_LIST;
-----------------------------------------------------------------------------------------
FUNCTION SEARCH_PRICE_EVENT_ITEM_LIST(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'RPM_PROMO_SEARCH_SQL.SEARCH_PRICE_EVENT_ITEM_LIST';

BEGIN

   -- From Regular Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item                                        = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpdm.diff_id                                   IN (im.diff_1,
                                                                     im.diff_2,
                                                                     im.diff_3,
                                                                     im.diff_4)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and rpdm.skulist                                   = LP_criteria.item_list_id
              and EXISTS (select 1
                            from rpm_merch_list_detail rmld,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rmld.merch_list_id = rpdm.price_event_itemlist
                             and im.item            = rmld.item
                             and im.dept            = sdcs.dept
                             and im.class           = sdcs.class
                             and im.subclass        = sdcs.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rpdm.price_event_itemlist = LP_criteria.price_event_item_list_id
              and EXISTS (select 1
                            from rpm_merch_list_detail rmld,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rpdm.price_event_itemlist = rmld.merch_list_id
                             and rmld.item                 = im.item
                             and im.dept                   = sdcs.dept
                             and im.class                  = sdcs.class
                             and im.subclass               = sdcs.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item                                        = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.item_parent                                 = rpdm.item
              and rpdm.diff_id                                   IN (im.diff_1,
                                                                     im.diff_2,
                                                                     im.diff_3,
                                                                     im.diff_4)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  rpm_merch_list_detail rmld,
                  (select dept,
                          class,
                          subclass
                     from rpm_pc_search_merch_tbl
                    where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and rmld.merch_list_id                             = LP_criteria.price_event_item_list_id
              and im.item                                        = rmld.item
              and im.dept                                        = sdcs.dept
              and im.class                                       = sdcs.class
              and im.subclass                                    = sdcs.subclass
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and rpdm.skulist                                   = LP_criteria.item_list_id
              and EXISTS (select 1
                            from rpm_merch_list_detail rmld,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rmld.merch_list_id = rpdm.price_event_itemlist
                             and im.item            = rmld.item
                             and im.dept            = sdcs.dept
                             and im.class           = sdcs.class
                             and im.subclass        = sdcs.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rpdm.price_event_itemlist = LP_criteria.price_event_item_list_id
              and EXISTS (select 1
                            from rpm_merch_list_detail rmld,
                                 item_master im,
                                 (select dept,
                                         class,
                                         subclass
                                    from rpm_pc_search_merch_tbl
                                   where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
                           where rpdm.price_event_itemlist = rmld.merch_list_id
                             and rmld.item                 = im.item
                             and im.dept                   = sdcs.dept
                             and im.class                  = sdcs.class
                             and im.subclass               = sdcs.subclass)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_PRICE_EVENT_ITEM_LIST;

-----------------------------------------------------------------------------------------

FUNCTION SEARCH_ITEM(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SEARCH_SQL.SEARCH_ITEM';

BEGIN

   -- From Regular Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and scit.item         = rpdm.item
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rpdm.item                                      = im.item_parent
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item                                      = im.item_parent
              and (   im.diff_1 = rpdm.diff_id
                   or im.diff_2 = rpdm.diff_id
                   or im.diff_3 = rpdm.diff_id
                   or im.diff_4 = rpdm.diff_id)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and (   EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select varchar_id item
                                                                from rpm_pc_search_char_tbl
                                                               where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select im.item_parent
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im
                                                               where criteria_type = 'ITEMS'
                                                                 and im.item       = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and (   EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rmld.merch_list_id  = rpdm.price_event_itemlist
                                  and rmld.item           IN (select varchar_id item
                                                                from rpm_pc_search_char_tbl
                                                               where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rmld.merch_list_id  = rpdm.price_event_itemlist
                                  and rmld.item           IN (select im.item_parent
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im
                                                               where criteria_type = 'ITEMS'
                                                                 and im.item       = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and scit.item         = rpdm.item
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rpdm.item                                      = im.item_parent
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item                                      = im.item_parent
              and (   im.diff_1 = rpdm.diff_id
                   or im.diff_2 = rpdm.diff_id
                   or im.diff_3 = rpdm.diff_id
                   or im.diff_4 = rpdm.diff_id)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and (   EXISTS ( select 1
                                 from rpm_promo_dtl_skulist_hist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select varchar_id item
                                                                from rpm_pc_search_char_tbl
                                                               where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_promo_dtl_skulist_hist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select im.item_parent
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im
                                                               where criteria_type = 'ITEMS'
                                                                 and im.item       = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and (   EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select varchar_id item
                                                                      from rpm_pc_search_char_tbl
                                                                     where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select im.item_parent
                                                                      from rpm_pc_search_char_tbl,
                                                                           item_master im
                                                                     where criteria_type = 'ITEMS'
                                                                       and im.item       = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_ITEM;
-----------------------------------------------------------------------------------------
FUNCTION SEARCH_ITEM_PARENT(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_PROMO_SEARCH_SQL.SEARCH_ITEM_PARENT';

BEGIN

   -- From Regular Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rpdm.item         = scit.item
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent                                 = scit.item
              and rpdm.item                                      = im.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item                                      = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and (   EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select varchar_id item
                                                                from rpm_pc_search_char_tbl
                                                               where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select im.item
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im
                                                               where criteria_type = 'ITEMS'
                                                                 and im.item_parent = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and (   EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select varchar_id item
                                                                      from rpm_pc_search_char_tbl
                                                                     where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select im.item
                                                                      from rpm_pc_search_char_tbl,
                                                                           item_master im
                                                                     where criteria_type = 'ITEMS'
                                                                       and im.item_parent = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rpdm.item         = scit.item
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent                                 = scit.item
              and rpdm.item                                      = im.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item                                      = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.DEPT_LEVEL_ITEM)
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and (   EXISTS ( select 1
                                 from rpm_promo_dtl_skulist_hist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select varchar_id item
                                                                from rpm_pc_search_char_tbl
                                                               where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_promo_dtl_skulist_hist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select im.item
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im
                                                               where criteria_type  = 'ITEMS'
                                                                 and im.item_parent = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and (   EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select varchar_id item
                                                                      from rpm_pc_search_char_tbl
                                                                     where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select im.item
                                                                      from rpm_pc_search_char_tbl,
                                                                           item_master im
                                                                     where criteria_type  = 'ITEMS'
                                                                       and im.item_parent = varchar_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_ITEM_PARENT;
-----------------------------------------------------------------------------------------

FUNCTION SEARCH_ITEM_PARENT_DIFF(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_PROMO_SEARCH_SQL.SEARCH_ITEM_PARENT_DIFF';

   L_diff_type       VARCHAR2(6) := LP_criteria.diff_type;
   L_search_diff_ids NUMBER(1)   := 0;

BEGIN

   if LP_criteria.diff_ids is NOT NULL and
      LP_criteria.diff_ids.COUNT() > 0 then

      L_search_diff_ids := 1;

      forall i IN 1..LP_criteria.diff_ids.COUNT()
         insert into rpm_pc_search_char_tbl (criteria_type,
                                             varchar_id)
         values ('DIFF_IDS',
                 LP_criteria.diff_ids(i));

   else

      insert into rpm_pc_search_char_tbl (criteria_type,
                                          varchar_id)
         select 'DIFF_IDS',
                diff_id
           from diff_ids
          where diff_type = LP_criteria.diff_type;

   end if;

   -- From Regular Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where L_search_diff_ids = 1
              and rpdm.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item         = scit.item
              and rpdm.diff_id      IN (select varchar_id diff_id
                                          from rpm_pc_search_char_tbl
                                         where criteria_type = 'DIFF_IDS')
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  diff_ids diff,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where L_search_diff_ids = 0
              and rpdm.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item         = scit.item
              and rpdm.diff_id      IN (select varchar_id diff_id
                                          from rpm_pc_search_char_tbl
                                         where criteria_type = 'DIFF_IDS')
              and diff.diff_type    = L_diff_type
              and rpdm.diff_id      = diff.diff_id
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rpdm.item                                      = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  (select varchar_id diff_id
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'DIFF_IDS') scdf,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and L_search_diff_ids                              = 1
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent                                 = scit.item
              and rpdm.item                                      = im.item
              and (   im.diff_1 = scdf.diff_id
                   or im.diff_2 = scdf.diff_id
                   or im.diff_3 = scdf.diff_id
                   or im.diff_4 = scdf.diff_id)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  diff_ids diff,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and L_search_diff_ids                              = 0
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent                                 = scit.item
              and rpdm.item                                      = im.item
              and (   im.diff_1 = diff.diff_id
                   or im.diff_2 = diff.diff_id
                   or im.diff_3 = diff.diff_id
                   or im.diff_4 = diff.diff_id)
              and diff.diff_type                                 = L_diff_type
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and (   EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select varchar_id item
                                                                from rpm_pc_search_char_tbl
                                                               where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select im.item
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im,
                                                                     (select varchar_id diff_id
                                                                        from rpm_pc_search_char_tbl
                                                                       where criteria_type = 'DIFF_IDS') scdf
                                                               where criteria_type     = 'ITEMS'
                                                                 and L_search_diff_ids = 1
                                                                 and im.item_parent    = varchar_id
                                                                 and (   im.diff_1 = scdf.diff_id
                                                                      or im.diff_2 = scdf.diff_id
                                                                      or im.diff_3 = scdf.diff_id
                                                                      or im.diff_4 = scdf.diff_id)))
                   or EXISTS ( select 1
                                 from rpm_promo_dtl_skulist rpds
                                where rpds.price_event_id = rpdm.promo_dtl_id
                                  and rpds.skulist        = rpdm.skulist
                                  and rpds.item           IN (select im.item
                                                                from rpm_pc_search_char_tbl,
                                                                     item_master im,
                                                                     diff_ids diff
                                                               where criteria_type     = 'ITEMS'
                                                                 and L_search_diff_ids = 0
                                                                 and im.item_parent    = varchar_id
                                                                 and (   im.diff_1 = diff.diff_id
                                                                      or im.diff_2 = diff.diff_id
                                                                      or im.diff_3 = diff.diff_id
                                                                      or im.diff_4 = diff.diff_id)
                                                                 and diff.diff_type    = L_diff_type)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node rpdm,
                  rpm_promo_zone_location rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and (   EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select varchar_id item
                                                                      from rpm_pc_search_char_tbl
                                                                     where criteria_type = 'ITEMS'))
                   or EXISTS ( select 1
                                 from rpm_merch_list_detail rmld
                                where rpdm.price_event_itemlist = rmld.merch_list_id
                                  and rmld.item                 IN (select im.item
                                                                      from rpm_pc_search_char_tbl,
                                                                           item_master im,
                                                                           (select varchar_id diff_id
                                                                              from rpm_pc_search_char_tbl
                                                                             where criteria_type = 'DIFF_IDS') scdf
                                                                     where criteria_type     = 'ITEMS'
                                                                       and im.item_parent    = varchar_id
                                                                       and (   im.diff_1 = scdf.diff_id
                                                                            or im.diff_2 = scdf.diff_id
                                                                            or im.diff_3 = scdf.diff_id
                                                                            or im.diff_4 = scdf.diff_id)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
   select distinct promo_dtl_id
     from (select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where L_search_diff_ids = 1
              and rpdm.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item         = scit.item
              and rpdm.diff_id      IN (select varchar_id diff_id
                                          from rpm_pc_search_char_tbl
                                         where criteria_type = 'DIFF_IDS')
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                   where criteria_type = 'ITEMS') scit,
                  diff_ids diff,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where L_search_diff_ids = 0
              and rpdm.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
              and rpdm.item         = scit.item
              and rpdm.diff_id      IN (select varchar_id diff_id
                                          from rpm_pc_search_char_tbl
                                         where criteria_type = 'DIFF_IDS')
              and diff.diff_type    = L_diff_type
              and rpdm.diff_id      = diff.diff_id
              and rpzl.promo_dtl_id = rpdm.promo_dtl_id
              and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and rpdm.item                                      = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  (select varchar_id diff_id
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'DIFF_IDS') scdf,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and L_search_diff_ids                              = 1
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent                                 = scit.item
              and rpdm.item                                      = im.item
              and (   im.diff_1 = scdf.diff_id
                   or im.diff_2 = scdf.diff_id
                   or im.diff_3 = scdf.diff_id
                   or im.diff_4 = scdf.diff_id)
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  diff_ids diff,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and L_search_diff_ids                              = 0
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LEVEL_ITEM
              and im.item_parent                                 = scit.item
              and rpdm.item                                      = im.item
              and (   im.diff_1 = diff.diff_id
                   or im.diff_2 = diff.diff_id
                   or im.diff_3 = diff.diff_id
                   or im.diff_4 = diff.diff_id)
              and diff.diff_type                                 = L_diff_type
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  item_master im,
                  (select varchar_id item
                     from rpm_pc_search_char_tbl
                    where criteria_type = 'ITEMS') scit,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                     RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
              and im.dept                                        = rpdm.dept
              and im.class                                       = NVL(rpdm.class, im.class)
              and im.subclass                                    = NVL(rpdm.subclass, im.subclass)
              and im.item                                        = scit.item
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
              and (   EXISTS (select 1
                                from rpm_promo_dtl_skulist_hist rpds
                               where rpds.price_event_id = rpdm.promo_dtl_id
                                 and rpds.skulist        = rpdm.skulist
                                 and rpds.item           IN (select varchar_id item
                                                               from rpm_pc_search_char_tbl
                                                              where criteria_type = 'ITEMS'))
                   or EXISTS (select 1
                                from rpm_promo_dtl_skulist_hist rpds
                               where rpds.price_event_id = rpdm.promo_dtl_id
                                 and rpds.skulist        = rpdm.skulist
                                 and rpds.item           IN (select im.item
                                                               from rpm_pc_search_char_tbl,
                                                                    item_master im,
                                                                    (select varchar_id diff_id
                                                                       from rpm_pc_search_char_tbl
                                                                      where criteria_type = 'DIFF_IDS') scdf
                                                              where criteria_type     = 'ITEMS'
                                                                and L_search_diff_ids = 1
                                                                and im.item_parent    = varchar_id
                                                                and (   im.diff_1 = scdf.diff_id
                                                                     or im.diff_2 = scdf.diff_id
                                                                     or im.diff_3 = scdf.diff_id
                                                                     or im.diff_4 = scdf.diff_id)))
                   or EXISTS (select 1
                                from rpm_promo_dtl_skulist_hist rpds
                               where rpds.price_event_id = rpdm.promo_dtl_id
                                 and rpds.skulist        = rpdm.skulist
                                 and rpds.item           IN (select im.item
                                                               from rpm_pc_search_char_tbl,
                                                                    item_master im,
                                                                    diff_ids diff
                                                              where criteria_type     = 'ITEMS'
                                                                and L_search_diff_ids = 0
                                                                and im.item_parent    = varchar_id
                                                                and (   im.diff_1 = diff.diff_id
                                                                     or im.diff_2 = diff.diff_id
                                                                     or im.diff_3 = diff.diff_id
                                                                     or im.diff_4 = diff.diff_id)
                                                                and diff.diff_type = L_diff_type)))
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
           union all
           select rpdm.promo_dtl_id
             from rpm_promo_dtl_merch_node_hist rpdm,
                  rpm_promo_zone_location_hist rpzl,
                  rpm_zone_locs_tbl rzlt
            where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
              and rpdm.merch_type                                = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
              and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
              and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
              and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
              and (   EXISTS (select 1
                                from rpm_merch_list_detail rmld
                               where rmld.merch_list_id = rpdm.price_event_itemlist
                                 and rmld.item          IN (select varchar_id item
                                                              from rpm_pc_search_char_tbl
                                                             where criteria_type = 'ITEMS'))
                   or EXISTS (select 1
                                from rpm_merch_list_detail rmld
                               where rpdm.price_event_itemlist = rmld.merch_list_id
                                 and rmld.item                 IN (select im.item
                                                                     from rpm_pc_search_char_tbl,
                                                                          item_master im,
                                                                          (select varchar_id diff_id
                                                                             from rpm_pc_search_char_tbl
                                                                            where criteria_type = 'DIFF_IDS') scdf
                                                                    where criteria_type     = 'ITEMS'
                                                                      and im.item_parent    = varchar_id
                                                                      and (   im.diff_1 = scdf.diff_id
                                                                           or im.diff_2 = scdf.diff_id
                                                                           or im.diff_3 = scdf.diff_id
                                                                           or im.diff_4 = scdf.diff_id)))));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_ITEM_PARENT_DIFF;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_STORE_WIDE(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SEARCH_SQL.SEARCH_STORE_WIDE';

BEGIN

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select rpdm.promo_dtl_id
        from rpm_promo_dtl_merch_node rpdm,
             rpm_promo_zone_location rpzl,
             rpm_zone_locs_tbl rzlt,
             item_loc il,
             item_master im,
             (select rpsmt.dept,
                     rpsmt.class,
                     rpsmt.subclass
                from rpm_pc_search_merch_tbl rpsmt
               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
       where rpdm.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
         and rpzl.promo_dtl_id = rpdm.promo_dtl_id
         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
         and il.loc            = rzlt.location_id
         and il.item           = im.item
         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.dept           = sdcs.dept
         and im.class          = sdcs.class
         and im.subclass       = sdcs.subclass;

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select rpdm.promo_dtl_id
        from rpm_promo_dtl_merch_node_hist rpdm,
             rpm_promo_zone_location_hist rpzl,
             rpm_zone_locs_tbl rzlt,
             item_loc il,
             item_master im,
             (select rpsmt.dept,
                     rpsmt.class,
                     rpsmt.subclass
                from rpm_pc_search_merch_tbl rpsmt
               where criteria_type = 'SECURITY_DEPT_CLASS_SUB') sdcs
       where rpdm.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
         and rpzl.promo_dtl_id = rpdm.promo_dtl_id
         and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
         and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
         and il.loc            = rzlt.location_id
         and il.item           = im.item
         and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.dept           = sdcs.dept
         and im.class          = sdcs.class
         and im.subclass       = sdcs.subclass;

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_STORE_WIDE;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_DEPT_CLASS_SUB(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_PROMO_SEARCH_SQL.SEARCH_DEPT_CLASS_SUB';

BEGIN

   -- From Regular Promotion Tables
   delete
     from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select distinct promo_dtl_id
        from (select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                 and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                        RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                        RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                 and rpdm.dept                                      = dcs.dept
                 and NVL(rpdm.class, dcs.class)                     = dcs.class
                 and NVL(rpdm.subclass, dcs.subclass)               = dcs.subclass
                 and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
                 and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     item_master im,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_ITEM_LEVEL)
                 and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpdm.item         = im.item
                 and im.dept           = dcs.dept
                 and im.class          = dcs.class
                 and im.subclass       = dcs.subclass
                 and im.tran_level     = im.item_level
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     item_master im,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_PARENT_LEVEL)
                 and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpdm.item         = im.item
                 and im.dept           = dcs.dept
                 and im.class          = dcs.class
                 and im.subclass       = dcs.subclass
                 and im.tran_level     = im.item_level + 1
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     item_master im,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_DIFF_LEVEL)
                 and rpdm.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and rpdm.item         = im.item
                 and im.dept           = dcs.dept
                 and im.class          = dcs.class
                 and im.subclass       = dcs.subclass
                 and im.tran_level     = im.item_level + 1
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)

              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_ITEM_LIST)
                 and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
                 and (EXISTS (select 1
                                from rpm_promo_dtl_skulist rpds,
                                     item_master im,
                                     (select sc.dept,
                                             sc.class,
                                             sc.subclass
                                        from rpm_pc_search_merch_tbl rpsmt,
                                             subclass sc
                                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                                         and sc.dept       = rpsmt.dept
                                         and sc.class      = NVL(rpsmt.class, sc.class)
                                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs
                               where rpds.price_event_id = rpdm.promo_dtl_id
                                 and rpds.skulist        = rpdm.skulist
                                 and im.item             = rpds.item
                                 and im.dept             = dcs.dept
                                 and im.class            = dcs.class
                                 and im.subclass         = dcs.subclass))
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_PEIL)
                 and rpdm.merch_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
                 and (EXISTS (select 1
                                from rpm_merch_list_detail rmld,
                                     item_master im,
                                     (select sc.dept,
                                             sc.class,
                                             sc.subclass
                                        from rpm_pc_search_merch_tbl rpsmt,
                                             subclass sc
                                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                                         and sc.dept       = rpsmt.dept
                                         and sc.class      = NVL(rpsmt.class, sc.class)
                                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs
                               where rpdm.price_event_itemlist = rmld.merch_list_id
                                 and im.item                   = rmld.item
                                 and im.dept                   = dcs.dept
                                 and im.class                  = dcs.class
                                 and im.subclass               = dcs.subclass)));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete
     from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select distinct promo_dtl_id
        from (select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location_hist rpzl,
                     rpm_zone_locs_tbl rzlt
               where NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                 and rpdm.merch_type                                IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                                                        RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                        RPM_CONSTANTS.DEPT_LEVEL_ITEM)
                 and rpdm.dept                                      = dcs.dept
                 and NVL(rpdm.class, dcs.class)                     = dcs.class
                 and NVL(rpdm.subclass, dcs.subclass)               = dcs.subclass
                 and rpzl.promo_dtl_id                              = rpdm.promo_dtl_id
                 and rzlt.location_id                               = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                                   = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     item_master im,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location_hist rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_ITEM_LEVEL)
                 and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpdm.item         = im.item
                 and im.dept           = dcs.dept
                 and im.class          = dcs.class
                 and im.subclass       = dcs.subclass
                 and im.tran_level     = im.item_level
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     item_master im,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location_hist rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_PARENT_LEVEL)
                 and rpdm.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and rpdm.item         = im.item
                 and im.dept           = dcs.dept
                 and im.class          = dcs.class
                 and im.subclass       = dcs.subclass
                 and im.tran_level     = im.item_level + 1
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     item_master im,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location_hist rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_DIFF_LEVEL)
                 and rpdm.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and rpdm.item         = im.item
                 and im.dept           = dcs.dept
                 and im.class          = dcs.class
                 and im.subclass       = dcs.subclass
                 and im.tran_level     = im.item_level + 1
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     rpm_promo_zone_location_hist rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_ITEM_LIST)
                 and rpdm.merch_type                                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and EXISTS (select 1
                               from rpm_promo_dtl_skulist_hist rpds,
                                    item_master im,
                                    (select sc.dept,
                                            sc.class,
                                            sc.subclass
                                       from rpm_pc_search_merch_tbl rpsmt,
                                            subclass sc
                                      where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                                        and sc.dept       = rpsmt.dept
                                        and sc.class      = NVL(rpsmt.class, sc.class)
                                        and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs
                              where rpds.price_event_id = rpdm.promo_dtl_id
                                and rpds.skulist        = rpdm.skulist
                                and im.item             = rpds.item
                                and im.dept             = dcs.dept
                                and im.class            = dcs.class
                                and im.subclass         = dcs.subclass)
                 and rpzl.promo_dtl_id                                  = rpdm.promo_dtl_id
                 and rzlt.location_id                                   = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                                       = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     rpm_promo_zone_location_hist rpzl,
                     rpm_zone_locs_tbl rzlt
               where (   NVL(LP_criteria.exclusive_item_level_ind, 'N') = 'N'
                      or LP_criteria.item_level_ind                     = LP_PROMO_SEARCH_PEIL)
                 and rpdm.merch_type                                    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and EXISTS (select 1
                               from rpm_merch_list_detail rmld,
                                    item_master im,
                                    (select sc.dept,
                                            sc.class,
                                            sc.subclass
                                       from rpm_pc_search_merch_tbl rpsmt,
                                            subclass sc
                                      where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                                        and sc.dept       = rpsmt.dept
                                        and sc.class      = NVL(rpsmt.class, sc.class)
                                        and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs
                              where rpdm.price_event_itemlist = rmld.merch_list_id
                                and im.item                   = rmld.item
                                and im.dept                   = dcs.dept
                                and im.class                  = dcs.class
                                and im.subclass               = dcs.subclass)
                 and rpzl.promo_dtl_id                                  = rpdm.promo_dtl_id
                 and rzlt.location_id                                   = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                                       = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_DEPT_CLASS_SUB;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_MERCH_HIERARCHY(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_PROMO_SEARCH_SQL.SEARCH_MERCH_HIERARCHY';

BEGIN

   delete
     from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select distinct
             promo_dtl_id
        from (select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where rpdm.merch_type                   = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                 and rpdm.dept                         = dcs.dept
                 and NVL(rpdm.class, dcs.class)        = dcs.class
                 and NVL(rpdm.subclass, dcs.subclass)  = dcs.subclass
                 and rpzl.promo_dtl_id                 = rpdm.promo_dtl_id
                 and rzlt.location_id                  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = rpsmt.class
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where rpdm.merch_type                   = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                 and rpdm.dept                         = dcs.dept
                 and rpdm.class                        = dcs.class
                 and NVL(rpdm.subclass, dcs.subclass)  = dcs.subclass
                 and rpzl.promo_dtl_id                 = rpdm.promo_dtl_id
                 and rzlt.location_id                  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                      = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = rpsmt.class
                         and sc.subclass   = rpsmt.subclass) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where rpdm.merch_type   = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                 and rpdm.dept         = dcs.dept
                 and rpdm.class        = dcs.class
                 and rpdm.subclass     = dcs.subclass
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        0) = FALSE then
      return FALSE;
   end if;

   -- From History Promotion Tables

   delete
     from numeric_id_gtt;

   insert into numeric_id_gtt (numeric_id)
      select distinct
             promo_dtl_id
        from (select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = NVL(rpsmt.class, sc.class)
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where rpdm.merch_type                  = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                 and rpdm.dept                        = dcs.dept
                 and NVL(rpdm.class, dcs.class)       = dcs.class
                 and NVL(rpdm.subclass, dcs.subclass) = dcs.subclass
                 and rpzl.promo_dtl_id                = rpdm.promo_dtl_id
                 and rzlt.location_id                 = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                     = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = rpsmt.class
                         and sc.subclass   = NVL(rpsmt.subclass, sc.subclass)) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where rpdm.merch_type                  = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                 and rpdm.dept                        = dcs.dept
                 and rpdm.class                       = dcs.class
                 and NVL(rpdm.subclass, dcs.subclass) = dcs.subclass
                 and rpzl.promo_dtl_id                = rpdm.promo_dtl_id
                 and rzlt.location_id                 = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id                     = NVL(rpzl.zone_id, rzlt.zone_id)
              union all
              select rpdm.promo_dtl_id
                from rpm_promo_dtl_merch_node_hist rpdm,
                     (select sc.dept,
                             sc.class,
                             sc.subclass
                        from rpm_pc_search_merch_tbl rpsmt,
                             subclass sc
                       where criteria_type = 'DEPT_CLASS_SUBCLASSES'
                         and sc.dept       = rpsmt.dept
                         and sc.class      = rpsmt.class
                         and sc.subclass   = rpsmt.subclass) dcs,
                     rpm_promo_zone_location rpzl,
                     rpm_zone_locs_tbl rzlt
               where rpdm.merch_type   = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                 and rpdm.dept         = dcs.dept
                 and rpdm.class        = dcs.class
                 and rpdm.subclass     = dcs.subclass
                 and rpzl.promo_dtl_id = rpdm.promo_dtl_id
                 and rzlt.location_id  = NVL(rpzl.location, rzlt.location_id)
                 and rzlt.zone_id      = NVL(rpzl.zone_id, rzlt.zone_id));

   if SEARCH_OTHER_CRIT(O_error_msg,
                        1) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_MERCH_HIERARCHY;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_OTHER_CRIT(O_error_msg    OUT VARCHAR2,
                           I_from_hist IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_PROMO_SEARCH_SQL.SEARCH_OTHER_CRIT';

   L_search_states        NUMBER(1)   := 0;
   L_search_deals         NUMBER(1)   := 0;
   L_search_cust_type     NUMBER(1)   := 0;
   L_search_location      NUMBER(1)   := 0;
   L_search_zone          NUMBER(1)   := 0;
   L_division             VARCHAR2(4) := NULL;
   L_groups               VARCHAR2(4) := NULL;

BEGIN

   L_division := LP_criteria.division;
   L_groups   := LP_criteria.groups;

   if LP_criteria.states is NOT NULL and
      LP_criteria.states.COUNT() > 0 then
      L_search_states := 1;
   end if;

   if LP_criteria.deal_id is NOT NULL and
      LP_criteria.deal_id.COUNT() > 0 then
      L_search_deals := 1;
   end if;

   if LP_criteria.customer_types is NOT NULL and
      LP_criteria.customer_types.COUNT() > 0 then
      L_search_cust_type := 1;
   end if;

   if LP_criteria.zones is NOT NULL and
      LP_criteria.zones.COUNT > 0 then
      L_search_zone := 1;
   end if;

   if LP_criteria.locations is NOT NULL and
      LP_criteria.locations.COUNT > 0 then
      L_search_location := 1;
   end if;

   if NVL(I_from_hist, 0) = 0 then

   insert into gtt_num_num_str_str_date_date
         (number_1,
          number_2)
      select distinct
             rp.promo_id,
             0
        from (select rpc.promo_id
                from rpm_promo_comp rpc,
                     rpm_promo_dtl rpd,
                     numeric_id_gtt ids
               where rpd.promo_dtl_id  = ids.numeric_id
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 -- STATES
                 and (   L_search_states = 0
                      or rpd.state       IN (select numeric_id
                                               from rpm_pc_search_num_tbl
                                              where criteria_type = 'STATES'))
                 -- CREATE DATE
                 and (   LP_criteria.create_date_after is NULL
                      or rpd.create_date >= LP_criteria.create_date_after)
                 and (   LP_criteria.create_date_before is NULL
                      or rpd.create_date <= LP_criteria.create_date_before)
                 -- CREATE BY
                 and (   LP_criteria.create_by_id is NULL
                      or rpd.create_id = LP_criteria.create_by_id)
                 -- APPROVED BY
                 and (   LP_criteria.approved_by_id is NULL
                      or rpd.approval_id = LP_criteria.approved_by_id)
                 -- APPROVED DATE
                 and (   LP_criteria.approved_date_after is NULL
                      or rpd.approval_date >= LP_criteria.approved_date_after)
                 and (   LP_criteria.approved_date_before is NULL
                      or rpd.approval_date <= LP_criteria.approved_date_before)
                 -- VENDOR FUNDED
                 and (   LP_criteria.vendor_funded is NULL
                      or (    LP_criteria.vendor_funded > 0
                          and EXISTS (select 1
                                        from deal_comp_prom dcp
                                       where dcp.promo_comp_id = rpc.promo_comp_id
                                      union
                                      select 1
                                        from rpm_pending_deal_detail rpdd
                                       where rpdd.promo_comp_id = rpc.promo_comp_id))
                      or (    LP_criteria.vendor_funded = 0
                          and NOT EXISTS (select 1
                                            from deal_comp_prom dcp
                                           where dcp.promo_comp_id = rpc.promo_comp_id
                                          union
                                          select 1
                                            from rpm_pending_deal_detail rpdd
                                           where rpdd.promo_comp_id = rpc.promo_comp_id)))
                 -- DEAL ID
                 and (   L_search_deals = 0
                      or EXISTS (select 1
                                   from rpm_pending_deal_detail rpdd
                                  where rpc.promo_comp_id             = rpdd.promo_comp_id
                                    and NVL(rpdd.pending_deal_ind, 0) = 1
                                    and rpdd.deal_id                  IN (select numeric_id
                                                                             from rpm_pc_search_num_tbl
                                                                            where criteria_type = 'DEAL_IDS')
                                 union all
                                 select 1
                                   from deal_comp_prom dcp
                                  where rpc.promo_comp_id = dcp.promotion_id
                                    and dcp.deal_id       IN (select numeric_id
                                                                from rpm_pc_search_num_tbl
                                                               where criteria_type = 'DEAL_IDS')))
                 -- DIVISION AND GROUP
                 and (   (    L_division is NULL
                          and L_groups is NULL)
                      or EXISTS (select 1
                                   from rpm_promo_dtl_merch_node rpdm,
                                        item_master im,
                                        deps d,
                                        groups g
                                  where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                    and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                              RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                    and im.item           = rpdm.item
                                    and d.dept            = im.dept
                                    and g.group_no        = d.group_no
                                    and g.division        = NVL(L_division, g.division)
                                    and g.group_no        = NVL(L_groups, g.group_no))
                      or EXISTS (select 1
                                   from rpm_promo_dtl_merch_node rpdm,
                                        deps d,
                                        groups g
                                  where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                    and rpdm.merch_type IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                            RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                            RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                                    and d.dept          = rpdm.dept
                                    and g.group_no      = d.group_no
                                    and g.division      = NVL(L_division, g.division)
                                    and g.group_no      = NVL(L_groups, g.group_no)))
                 -- CUSTOMER TYPE
                 and (   L_search_cust_type = 0
                      or rpc.customer_type IN (select numeric_id
                                                 from rpm_pc_search_num_tbl
                                                where criteria_type = 'CUST_TYPES'))
                 -- Search Locations
                 and (   L_search_location = 0
                      or EXISTS (select 1
                                   from rpm_promo_zone_location rpzl
                                  where rpzl.promo_dtl_id    = rpd.promo_dtl_id
                                    and rpzl.zone_node_type != 1
                                    and rpzl.location IN (select numeric_id
                                                            from rpm_pc_search_num_tbl
                                                           where criteria_type = 'LOCATIONS'))
                      or (    NVL(LP_criteria.exclusive_loc_level_ind, 'N') = 'N'
                          and EXISTS (select 1
                                        from rpm_promo_zone_location rpzl,
                                             rpm_zone_location rzl
                                       where rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                         and rpzl.zone_node_type = 1
                                         and rzl.zone_id         = rpzl.zone_id
                                         and rzl.location IN (select numeric_id
                                                                from rpm_pc_search_num_tbl
                                                               where criteria_type = 'LOCATIONS'))))
                 -- Search Zones
                 and (   L_search_zone = 0
                      or EXISTS (select 1
                                   from rpm_promo_zone_location rpzl
                                  where rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                    and rpzl.zone_node_type = 1
                                    and rpzl.zone_id IN (select numeric_id
                                                           from rpm_pc_search_num_tbl
                                                          where criteria_type = 'ZONES'))
                      or EXISTS (select 1
                                   from rpm_promo_zone_location rpzl,
                                        rpm_zone_location rzl
                                  where NVL(LP_criteria.exclusive_loc_level_ind, 'N') = 'N'
                                    and rpzl.promo_dtl_id                             = rpd.promo_dtl_id
                                    and rpzl.zone_node_type                          != 1
                                    and rpzl.zone_id                                 IN (select numeric_id
                                                                                           from rpm_pc_search_num_tbl
                                                                                          where criteria_type = 'ZONES')
                                    and rpzl.location                                 = rzl.location))) t,
             rpm_promo rp
       where rp.promo_id = t.promo_id
         -- Check Promo Event ID
         and (   LP_criteria.promotion_event_id is NULL
              or rp.promo_event_id = LP_criteria.promotion_event_id)
         -- Check Currency
         and (   LP_criteria.currency is NULL
              or rp.currency_code = LP_criteria.currency)
         -- Check Start Date After
         and (   LP_criteria.start_date_after is NULL
              or rp.start_date >= LP_criteria.start_date_after)
         -- Check Start Date Before
         and (   LP_criteria.start_date_before is NULL
              or rp.start_date <= LP_criteria.start_date_before)
         -- Check End Date After
         and (   LP_criteria.end_date_after is NULL
              or rp.end_date >= LP_criteria.end_date_after)
         -- Check End Date Before
         and (   LP_criteria.end_date_before is NULL
              or rp.end_date <= LP_criteria.end_date_before)
         -- Check Theme
         and (   LP_criteria.theme is NULL
              or EXISTS (select 1
                           from rpm_promo_event rpe
                          where rpe.promo_event_id = rp.promo_event_id
                            and rpe.theme          = LP_criteria.theme));

   else

      insert into gtt_num_num_str_str_date_date
         (number_1,
          number_2)
      select distinct
             rp.promo_id,
             1
        from (select rpc.promo_id
                from rpm_promo_comp_hist rpc,
                     rpm_promo_dtl_hist rpd,
                     numeric_id_gtt ids
               where rpd.promo_dtl_id  = ids.numeric_id
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 -- STATES
                 and (   L_search_states = 0
                      or rpd.state IN (select numeric_id
                                         from rpm_pc_search_num_tbl
                                        where criteria_type = 'STATES'))
                 -- CREATE DATE
                 and (   LP_criteria.create_date_after is NULL
                      or rpd.create_date >= LP_criteria.create_date_after)
                 and (   LP_criteria.create_date_before is NULL
                      or rpd.create_date <= LP_criteria.create_date_before)
                 -- CREATE BY
                 and (   LP_criteria.create_by_id is NULL
                      or rpd.create_id = LP_criteria.create_by_id)
                 -- APPROVED BY
                 and (   LP_criteria.approved_by_id is NULL
                      or rpd.approval_id = LP_criteria.approved_by_id)
                 -- APPROVED DATE
                 and (   LP_criteria.approved_date_after is NULL
                      or rpd.approval_date >= LP_criteria.approved_date_after)
                 and (   LP_criteria.approved_date_before is NULL
                      or rpd.approval_date <= LP_criteria.approved_date_before)
                 -- VENDOR FUNDED
                 and (   LP_criteria.vendor_funded is NULL
                      or (    LP_criteria.vendor_funded > 0
                          and EXISTS (select 1
                                        from deal_comp_prom dcp
                                       where dcp.promo_comp_id = rpc.promo_comp_id
                                      union
                                      select 1
                                        from rpm_pending_deal_detail rpdd
                                       where rpdd.promo_comp_id = rpc.promo_comp_id))
                      or (    LP_criteria.vendor_funded = 0
                          and NOT EXISTS (select 1
                                            from deal_comp_prom dcp
                                           where dcp.promo_comp_id = rpc.promo_comp_id
                                          union
                                          select 1
                                            from rpm_pending_deal_detail rpdd
                                           where rpdd.promo_comp_id = rpc.promo_comp_id)))
                 -- DEAL ID
                 and (   L_search_deals = 0
                      or EXISTS (select 1
                                   from rpm_pending_deal_detail rpdd
                                  where rpc.promo_comp_id             = rpdd.promo_comp_id
                                    and NVL(rpdd.pending_deal_ind, 0) = 1
                                    and rpdd.deal_id                  IN (select numeric_id
                                                                            from rpm_pc_search_num_tbl
                                                                           where criteria_type = 'DEAL_IDS')
                                  union all
                                 select 1
                                   from deal_comp_prom dcp
                                  where rpc.promo_comp_id = dcp.promotion_id
                                    and dcp.deal_id       IN (select numeric_id
                                                                from rpm_pc_search_num_tbl
                                                               where criteria_type = 'DEAL_IDS')))
                 -- DIVISION AND GROUP
                 and (   (    L_division is NULL
                          and L_groups   is NULL)
                      or EXISTS (select 1
                                   from rpm_promo_dtl_merch_node_hist rpdm,
                                        item_master im,
                                        deps d,
                                        groups g
                                  where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                    and rpdm.merch_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                              RPM_CONSTANTS.PARENT_ITEM_DIFF)
                                    and im.item           = rpdm.item
                                    and d.dept            = im.dept
                                    and g.group_no        = d.group_no
                                    and g.division        = NVL(L_division, g.division)
                                    and g.group_no        = NVL(L_groups, g.group_no))
                      or EXISTS (select 1
                                   from rpm_promo_dtl_merch_node_hist rpdm,
                                        deps d,
                                        groups g
                                  where rpdm.promo_dtl_id = rpd.promo_dtl_id
                                    and rpdm.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                              RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                              RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                                    and d.dept            = rpdm.dept
                                    and g.group_no        = d.group_no
                                    and g.division        = NVL(L_division, g.division)
                                    and g.group_no        = NVL(L_groups, g.group_no)))
                 -- CUSTOMER TYPE
                 and (   L_search_cust_type = 0
                      or rpc.customer_type  IN (select numeric_id
                                                  from rpm_pc_search_num_tbl
                                                 where criteria_type = 'CUST_TYPES'))
                 -- Search Locations
                 and (   L_search_location = 0
                      or EXISTS (select 1
                                   from rpm_promo_zone_location_hist rpzl
                                  where rpzl.promo_dtl_id    = rpd.promo_dtl_id
                                    and rpzl.zone_node_type != 1
                                    and rpzl.location       IN (select numeric_id
                                                                  from rpm_pc_search_num_tbl
                                                                 where criteria_type = 'LOCATIONS'))
                      or (    NVL(LP_criteria.exclusive_loc_level_ind, 'N') = 'N'
                          and EXISTS (select 1
                                        from rpm_promo_zone_location_hist rpzl,
                                             rpm_zone_location rzl
                                       where rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                         and rpzl.zone_node_type = 1
                                         and rzl.zone_id         = rpzl.zone_id
                                         and rzl.location        IN (select numeric_id
                                                                       from rpm_pc_search_num_tbl
                                                                      where criteria_type = 'LOCATIONS'))))
                 -- Search Zones
                 and (   L_search_zone = 0
                      or EXISTS (select 1
                                   from rpm_promo_zone_location_hist rpzl
                                  where rpzl.promo_dtl_id   = rpd.promo_dtl_id
                                    and rpzl.zone_node_type = 1
                                    and rpzl.zone_id        IN (select numeric_id
                                                                  from rpm_pc_search_num_tbl
                                                                 where criteria_type = 'ZONES'))
                      or EXISTS (select 1
                                   from rpm_promo_zone_location_hist rpzl,
                                        rpm_zone_location rzl
                                  where NVL(LP_criteria.exclusive_loc_level_ind, 'N') = 'N'
                                    and rpzl.promo_dtl_id                             = rpd.promo_dtl_id
                                    and rpzl.zone_node_type                          != 1
                                    and rpzl.zone_id                                 IN (select numeric_id
                                                                                           from rpm_pc_search_num_tbl
                                                                                          where criteria_type = 'ZONES')
                                    and rpzl.location                                 = rzl.location))) t,
             rpm_promo_hist rp
       where rp.promo_id = t.promo_id
         -- Check Promo Event ID
         and (   LP_criteria.promotion_event_id is NULL
              or rp.promo_event_id = LP_criteria.promotion_event_id)
         -- Check Currency
         and (   LP_criteria.currency is NULL
              or rp.currency_code = LP_criteria.currency)
         -- Check Start Date After
         and (   LP_criteria.start_date_after is NULL
              or rp.start_date >= LP_criteria.start_date_after)
         -- Check Start Date Before
         and (   LP_criteria.start_date_before is NULL
              or rp.start_date <= LP_criteria.start_date_before)
         -- Check End Date After
         and (   LP_criteria.end_date_after is NULL
              or rp.end_date >= LP_criteria.end_date_after)
         -- Check End Date Before
         and (   LP_criteria.end_date_before is NULL
              or rp.end_date <= LP_criteria.end_date_before)
         -- Check Theme
         and (   LP_criteria.theme is NULL
              or EXISTS (select 1
                           from rpm_promo_event rpe
                          where rpe.promo_event_id = rp.promo_event_id
                            and rpe.theme          = LP_criteria.theme));
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SEARCH_OTHER_CRIT;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_EVENT_ID(O_error_msg          OUT VARCHAR2,
                         I_promo_event_id  IN     NUMBER)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(100) := 'RPM_PROMO_SEARCH_SQL.SEARCH_EVENT_ID';

BEGIN

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select promo_id,
             0
        from rpm_promo
       where promo_event_id = I_promo_event_id;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select promo_id,
             1
        from rpm_promo_hist
       where promo_event_id = I_promo_event_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEARCH_EVENT_ID;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_THRESHOLD_ID(O_error_msg        OUT VARCHAR2,
                             I_threshold_id  IN     NUMBER)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(100) := 'RPM_PROMO_SEARCH_SQL.SEARCH_THRESHOLD_ID';

BEGIN

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select rpc.promo_id,
             0
        from rpm_promo_comp rpc,
             rpm_promo_dtl rpd
       where rpd.threshold_id  = I_threshold_id
         and rpc.promo_comp_id = rpd.promo_comp_id;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select rpc.promo_id,
             1
        from rpm_promo_comp_hist rpc,
             rpm_promo_dtl_hist rpd
       where rpd.threshold_id  = I_threshold_id
         and rpc.promo_comp_id = rpd.promo_comp_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEARCH_THRESHOLD_ID;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_ZONE_IDS(O_error_msg    OUT VARCHAR2,
                         I_zone_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(100) := 'RPM_PROMO_SEARCH_SQL.SEARCH_ZONE_IDS';

BEGIN

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select /*+ CARDINALITY(ids 10) */
             rpc.promo_id,
             0
        from rpm_promo_comp rpc,
             rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             table(cast(I_zone_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where rpzl.zone_id      = value(ids)
         and rpd.promo_dtl_id  = rpzl.promo_dtl_id
         and rpc.promo_comp_id = rpd.promo_comp_id;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select /*+ CARDINALITY(ids 10) */
             rpc.promo_id,
             1
        from rpm_promo_comp_hist rpc,
             rpm_promo_dtl_hist rpd,
             rpm_promo_zone_location_hist rpzl,
             table(cast(I_zone_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where rpzl.zone_id      = value(ids)
         and rpd.promo_dtl_id  = rpzl.promo_dtl_id
         and rpc.promo_comp_id = rpd.promo_comp_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEARCH_ZONE_IDS;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SEARCH_PRICE_GUIDE_ID(O_error_msg          OUT VARCHAR2,
                               I_price_guide_id  IN     NUMBER)
RETURN BOOLEAN IS

   L_program                    VARCHAR2(100) := 'RPM_PROMO_SEARCH_SQL.SEARCH_PRICE_GUIDE_ID';

BEGIN

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select /*+ CARDINALITY(ids 10) */
             rpc.promo_id,
             0
        from rpm_promo_comp rpc,
             rpm_promo_dtl rpd
       where rpd.price_guide_id = I_price_guide_id
         and rpc.promo_comp_id  = rpd.promo_comp_id;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select /*+ CARDINALITY(ids 10) */
             rpc.promo_id,
             1
        from rpm_promo_comp_hist rpc,
             rpm_promo_dtl_hist rpd
       where rpd.price_guide_id = I_price_guide_id
         and rpc.promo_comp_id  = rpd.promo_comp_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEARCH_PRICE_GUIDE_ID;
-----------------------------------------------------------------------------------------------------------------------

END RPM_PROMO_SEARCH_SQL;
/