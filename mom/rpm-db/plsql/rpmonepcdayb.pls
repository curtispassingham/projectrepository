CREATE OR REPLACE PACKAGE BODY RPM_CC_ONE_PC_PER_DAY AS

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------
FUNCTION VALIDATE(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_parent_rpcs   IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_ONE_PC_PER_DAY.VALIDATE';

   L_vdate DATE := GET_VDATE;

   L_error_rec     CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl     CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_out_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select /*+ CARDINALITY(t 10) CARDINALITY(ccet 10)*/
             CONFLICT_CHECK_ERROR_REC(rfrg.price_event_id,
                                      rfrg.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'future_retail_price_change_rule1')
        from rpm_future_retail_gtt rfrg,
             table(cast(I_parent_rpcs as OBJ_NUM_NUM_DATE_TBL)) t,
             table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet
       where rfrg.price_event_id   = t.numeric_col1
         and rfrg.price_event_id  != ccet.price_event_id
         and rfrg.price_change_id != NVL(t.numeric_col2, -999)
         and rfrg.price_change_id  is NOT NULL
         and EXISTS (select 1
                       from rpm_price_change rpc
                      where rpc.price_change_id = t.numeric_col1
                        and rfrg.action_date    = rpc.effective_date
                        and rpc.effective_date != L_vdate
                        and rownum              = 1);

BEGIN

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then
      ---
      L_error_tbl := IO_cc_error_tbl;
   else
      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);
      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   open C_CHECK;
   fetch C_CHECK BULK COLLECT into L_out_error_tbl;
   close C_CHECK;

   if L_out_error_tbl is NOT NULL and
      L_out_error_tbl.COUNT > 0 then

      if IO_cc_error_tbl is NULL or
         IO_cc_error_tbl.COUNT = 0 then
         ---
         IO_cc_error_tbl := L_out_error_tbl;
      else
         for i IN 1..L_out_error_tbl.COUNT loop
            IO_cc_error_tbl.EXTEND;
            IO_cc_error_tbl(IO_cc_error_tbl.COUNT) := L_out_error_tbl(i);
         end loop;
      end if;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE;
--------------------------------------------------------------------------------

END RPM_CC_ONE_PC_PER_DAY;
/

