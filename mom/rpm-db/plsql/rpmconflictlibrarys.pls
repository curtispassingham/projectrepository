CREATE OR REPLACE PACKAGE RPM_CONFLICT_LIBRARY AS
--------------------------------------------------------------------------------
FUNCTION APPLY_RETAIL_CHANGE_WITH_GUIDE(IO_error_table   IN OUT OBJ_VARCHAR_DESC_TABLE,
                                        I_retail         IN     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                                        I_change_type    IN     RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE,
                                        I_change_amount  IN     RPM_PRICE_CHANGE.CHANGE_AMOUNT%TYPE,
                                        I_change_percent IN     RPM_PRICE_CHANGE.CHANGE_PERCENT%TYPE,
                                        I_price_guide_id IN     RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE,
                                        O_retail            OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION APPLY_RETAIL_CHANGE(IO_error_table   IN OUT OBJ_VARCHAR_DESC_TABLE,
                             I_retail         IN     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                             I_change_type    IN     RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE,
                             I_change_amount  IN     RPM_PRICE_CHANGE.CHANGE_AMOUNT%TYPE,
                             I_change_percent IN     RPM_PRICE_CHANGE.CHANGE_PERCENT%TYPE,
                             O_retail            OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION APPLY_PRICE_GUIDE(IO_error_table   IN OUT OBJ_VARCHAR_DESC_TABLE,
                           I_retail         IN     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                           I_price_guide_id IN     RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE,
                           O_retail            OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_CONFLICT_LIBRARY;
/

