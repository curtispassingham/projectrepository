CREATE OR REPLACE PACKAGE BODY RPM_NIGHTLY_BATCH_CLEANUP_SQL AS
--------------------------------------------------------

PROCEDURE EXEC_STMT(I_statement IN     VARCHAR2);

----------------------------------------------------------------------------

PROCEDURE PRE
IS

   L_program VARCHAR2(30) := 'RPM_NIGHTLY_BATCH_CLEANUP.PRE';

BEGIN

   -- This function should be run before the nightly batch window or after 1 large RPM Price
   -- Event (3 million or more Item/Loc) or 3 medium Price Events or 30 small Price Events.
   -- The time needed to rebuild an index depends on its size. Please make sure the time
   -- needed to rebuild all indexes is less than 3 minutes. A DDL level lock is required to
   -- rebuild an index. As this script is run during the online window, 2 kinds of issues/errors may occur:
   -- 1. Row level lock is on some rows, some individual index rebuild statement may fail. You can ignore the error.
   -- 2. When the index rebuild is in progress, the application may be a few seconds slower or fails
   --    to acquire the Lock to update a record and cause the application fail. So this script should
   --    be run in between 2 transactions.

   EXEC_STMT('alter index PK_RPM_PROMO_DISC_LDR_PAYLOAD rebuild');
   EXEC_STMT('alter index RPM_PROMO_DISC_LDR_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index PK_RPM_PRICE_EVENT_PAYLOAD rebuild');
   EXEC_STMT('alter index RPM_PRICE_EVENT_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index RPM_PROMO_LOCATION_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index RPM_PROMO_DLT_LIST_GRP_PAYL_I1 rebuild');
   EXEC_STMT('alter index RPM_PROMO_ITEM_LOC_SR_PAYL_I1 rebuild');
   EXEC_STMT('alter index PK_RPM_CLEARANCE_PAYLOAD rebuild');
   EXEC_STMT('alter index PK_RPM_PRICE_CHG_PAYLOAD rebuild');
   EXEC_STMT('alter index PK_RPM_PROMO_LOCATION_PAYLOAD rebuild');
   EXEC_STMT('alter index PK_RPM_PROMO_ITEM_LOC_SR_PAYLO rebuild');
   EXEC_STMT('alter index RPM_PROMO_ITEM_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index RPM_PROMO_DTL_LIST_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index PK_RPM_PROMO_DTL_LIST_PAYLOAD rebuild');
   EXEC_STMT('alter index PK_RPM_PROMO_DTL_LIST_GRP_PAYL rebuild');
   EXEC_STMT('alter index PK_RPM_PROMO_ITEM_PAYLOAD rebuild');
   EXEC_STMT('alter index RPM_PROMO_ITEM_LOC_SR_PAYL_I2 rebuild');
   EXEC_STMT('alter index RPM_PRICE_CHG_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index RPM_CLEARANCE_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index RPM_PROMO_DTL_PAYLOAD_I1 rebuild');
   EXEC_STMT('alter index PK_RPM_PROMO_DTL_PAYLOAD rebuild');
   EXEC_STMT('alter index PK_RPM_BULK_CC_PE_CHUNK rebuild');
   EXEC_STMT('alter index RPM_BULK_CC_PE_ITEM_I2 rebuild');
   EXEC_STMT('alter index RPM_BULK_CC_PE_ITEM_I1 rebuild');
   EXEC_STMT('alter index RPM_BULK_CC_PE_SEQUENCE_I1 rebuild');
   EXEC_STMT('alter index RPM_BULK_CC_PE_THREAD_I1 rebuild');
   EXEC_STMT('alter index RPM_BULK_CC_PE_THREAD_I3 rebuild');
   EXEC_STMT('alter index RPM_BULK_CC_PE_THREAD_I2 rebuild');
   EXEC_STMT('alter index PK_RPM_BULK_CC_TASK rebuild');
   EXEC_STMT('alter index PK_RPM_WORKSHEET_CC_TASK rebuild');

   return;

END PRE;

----------------------------------------------------------------------------

PROCEDURE POST
IS

   L_program VARCHAR2(30) := 'RPM_NIGHTLY_BATCH_CLEANUP.POST';

BEGIN

   -- Enable Row Movement
   EXEC_STMT('alter table RPM_BULK_CC_PE enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_CHUNK enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_ITEM enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_LOCATION enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_SEQUENCE enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_THREAD enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_TASK enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_WKSHT_CLR enable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_WKSHT_PC enable row movement');
   EXEC_STMT('alter table RPM_CHUNK_CC_TASK enable row movement');
   EXEC_STMT('alter table RPM_CLEARANCE_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PE_CC_LOCK enable row movement');
   EXEC_STMT('alter table RPM_PRICE_CHG_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PRICE_EVENT_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_DISC_LDR_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_DTL_LIST_GRP_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_DTL_LIST_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_DTL_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_FIN_DTL_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_ITEM_LOC_SR_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_ITEM_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_PROMO_LOCATION_PAYLOAD enable row movement');
   EXEC_STMT('alter table RPM_WORKSHEET_CC_TASK enable row movement');

   -- Shrink the Tables and their Indexes
   EXEC_STMT('alter table RPM_BULK_CC_PE shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_PE_CHUNK shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_PE_ITEM shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_PE_LOCATION shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_PE_SEQUENCE shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_PE_THREAD shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_TASK shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_WKSHT_CLR shrink space cascade');
   EXEC_STMT('alter table RPM_BULK_CC_WKSHT_PC shrink space cascade');
   EXEC_STMT('alter table RPM_CHUNK_CC_TASK shrink space cascade');
   EXEC_STMT('alter table RPM_CLEARANCE_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PE_CC_LOCK shrink space cascade');
   EXEC_STMT('alter table RPM_PRICE_CHG_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PRICE_EVENT_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_DISC_LDR_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_DTL_LIST_GRP_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_DTL_LIST_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_DTL_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_FIN_DTL_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_ITEM_LOC_SR_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_ITEM_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_PROMO_LOCATION_PAYLOAD shrink space cascade');
   EXEC_STMT('alter table RPM_WORKSHEET_CC_TASK shrink space cascade');

   -- Disable the Row Movement
   EXEC_STMT('alter table RPM_BULK_CC_PE disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_CHUNK disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_ITEM disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_LOCATION disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_SEQUENCE disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_PE_THREAD disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_TASK disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_WKSHT_CLR disable row movement');
   EXEC_STMT('alter table RPM_BULK_CC_WKSHT_PC disable row movement');
   EXEC_STMT('alter table RPM_CHUNK_CC_TASK disable row movement');
   EXEC_STMT('alter table RPM_CLEARANCE_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PE_CC_LOCK disable row movement');
   EXEC_STMT('alter table RPM_PRICE_CHG_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PRICE_EVENT_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_DISC_LDR_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_DTL_LIST_GRP_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_DTL_LIST_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_DTL_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_FIN_DTL_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_ITEM_LOC_SR_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_ITEM_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_PROMO_LOCATION_PAYLOAD disable row movement');
   EXEC_STMT('alter table RPM_WORKSHEET_CC_TASK disable row movement');

   return;

END POST;

----------------------------------------------------------------------------

PROCEDURE EXEC_STMT(I_statement IN     VARCHAR2)
IS

BEGIN

   EXECUTE IMMEDIATE I_statement;

   return;

EXCEPTION

   when OTHERS then

      dbms_output.put_line('Error when executing the statement '''||I_statement||''':');
      dbms_output.put_line(SQLCODE||':'||SQLERRM);
      dbms_output.put_line('----------------');

   return;

END EXEC_STMT;
----------------------------------------------------------------------------

END RPM_NIGHTLY_BATCH_CLEANUP_SQL;
/
