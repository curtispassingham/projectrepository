CREATE OR REPLACE PACKAGE RPM_AREA_DIFF_PRICE_CHANGE AS
-----------------------------------------------------------------------------

REASON_CODE_SYSTEM_CODE    CONSTANT RPM_PRICE_CHANGE.REASON_CODE%TYPE := 3;
REASON_CODE_WORKSHEET_CODE CONSTANT RPM_PRICE_CHANGE.REASON_CODE%TYPE := 4;

-----------------------------------------------------------------------------

/******************************************************************
 * Called when approving price changes -- expects rpm_future_retail_gtt
 * to be populated for the price changes that are being approved before
 * it is called.
 *
 * Creates the necessary secondary area differentail price changes needed
 * due to the approval of the price changes sent ino the function.
 *
 * Populates NUMERIC_ID_GTT with IDs of price changes that were
 * created due to area differentials that need to be approved.
 ******************************************************************/

FUNCTION APPLY_AREA_DIFFS_TO_PC(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                                I_primary_pc_ids IN    OBJ_NUMERIC_ID_TABLE,
                                I_user_name      IN    VARCHAR2)
RETURN BOOLEAN;

FUNCTION CHUNK_APPLY_AREA_DIFFS_TO_PC(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_primary_pc_id    IN     NUMBER,
                                      I_bulk_cc_pe_id    IN     NUMBER,
                                      I_user_name        IN     VARCHAR2,
                                      I_pe_sequence_id   IN     NUMBER,
                                      I_pe_thread_number IN     NUMBER)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------
/**********************************************************************************
 * Called when unapproving price changes. The I_primary_pc_ids is the primary price
 * changes that are successfully unparroved.
 *
 * Insert the Secondary Price Change Ids that need to be unapproved into
 * NUMERIC_ID_GTT table.
 *
 * Detach all Secondary Price Changes from the Primary Price Changes
 **********************************************************************************/

FUNCTION UNAPPROVE_AREA_DIFF_PC(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                                O_secondary_ind              OUT NUMBER,
                                I_primary_pc_ids          IN     OBJ_NUMERIC_ID_TABLE,
                                I_secondary_bulk_cc_pe_id IN     NUMBER)
RETURN BOOLEAN;

-----------------------------------------------------------------------------
/******************************************************************
 * Called with the price changes that passed unapproval.
 *
 * Set the price change status to pricechange.state.deleted
 *
 * Null out the loc_exception_parent_id and parent_exception_parent_id fields
 * of any children the price change may have.
 *
 ******************************************************************/
-----------------------------------------------------------------------------

FUNCTION DELETE_AREA_DIFF_PC(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                             I_unapproved_pc_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;

-----------------------------------------------------------------------------

END RPM_AREA_DIFF_PRICE_CHANGE;
/

