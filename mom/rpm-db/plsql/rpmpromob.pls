CREATE OR REPLACE PACKAGE BODY RPM_PROMO_SQL AS
-----------------------------------------------------------------------------------------

COST_LOOK_FORWARD_DAYS   CONSTANT NUMBER(4) := 365;
RETAIL_LOOK_FORWARD_DAYS CONSTANT NUMBER(4) := 365;

PROMO_APPROVED CONSTANT VARCHAR2(50) := 'state.approved';
PROMO_ACTIVE   CONSTANT VARCHAR2(50) := 'state.active';

LP_add_remove_promo_fail BOOLEAN := FALSE;

---types used for processing
TYPE PROMO_COMP_DETAIL_REC IS RECORD (promo_rec OBJ_RPM_PROMO_REC);

-----------------------------------------------------------------------------------------
--function prototypes
-----------------------------------------------------------------------------------------

FUNCTION GET_LOC_NODE_CURRENCY(O_error_msg      IN OUT VARCHAR2,
                               O_currency_code     OUT RPM_ZONE.CURRENCY_CODE%TYPE,
                               I_zone_node_type IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                               I_zone_id        IN     RPM_ZONE_LOCATION.LOCATION%TYPE,
                               I_location       IN     RPM_PROMO_ZONE_LOCATION.ZONE_NODE_TYPE%TYPE)
RETURN BOOLEAN;

FUNCTION GET_DATES(O_error_msg     IN OUT VARCHAR2,
                   I_obj_promo_rec IN     OBJ_RPM_PROMO_REC,
                   O_current_date     OUT DATE,
                   O_basis_date       OUT DATE,
                   O_promo_date       OUT DATE)
RETURN BOOLEAN;

FUNCTION CHECK_CONFLICT (O_error_msg                IN OUT VARCHAR2,
                         I_rpm_promo_comp_detail_id IN     RPM_PROMO_DTL.PROMO_DTL_ID%TYPE,
                         I_async_conflict_error     IN OUT NUMBER)
RETURN BOOLEAN;

FUNCTION CHECK_RANGED(O_error_msg              IN OUT VARCHAR2,
                      O_no_item_loc_ranged_ind    OUT NUMBER,
                      O_is_multi_item_loc_ind     OUT NUMBER)
RETURN BOOLEAN;

FUNCTION GET_MISC_INDS(O_error_msg                IN OUT VARCHAR2,
                       I_rpm_promo_comp_detail_id IN     RPM_PROMO_DTL.PROMO_DTL_ID%TYPE,
                       I_start_date               IN     DATE,
                       I_end_date                 IN     DATE,
                       O_non_regular_basis_ind       OUT NUMBER,
                       O_clearance_overlap_ind       OUT NUMBER,
                       O_price_change_overlap_ind    OUT NUMBER,
                       O_async_conflict_error        OUT NUMBER)
RETURN BOOLEAN;

FUNCTION POPULATE_VAT(O_error_msg IN OUT  VARCHAR2,
                      I_vat_date  IN      DATE)
RETURN BOOLEAN;

FUNCTION POPULATE_ITEM_INFO(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION CALCULATE_COST(O_error_msg               IN OUT VARCHAR2,
                        I_promo_comp_detail_rec   IN OUT NOCOPY PROMO_COMP_DETAIL_REC,
                        I_to_currency_code        IN     CURRENCIES.CURRENCY_CODE%TYPE,
                        O_cur_cost                   OUT FUTURE_COST.PRICING_COST%TYPE,
                        O_cur_cost_unique            OUT NUMBER,
                        O_basis_cost                 OUT FUTURE_COST.PRICING_COST%TYPE,
                        O_basis_cost_unique          OUT NUMBER,
                        O_cost_change_overlap_ind    OUT NUMBER)
RETURN BOOLEAN;

FUNCTION CALCULATE_RETAIL_QUERY(O_error_msg                   IN OUT VARCHAR2,
                                I_retail_date                 IN     DATE,
                                I_to_currency_code            IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                I_customer_type               IN     RPM_CUST_SEGMENT_PROMO_FR.CUSTOMER_TYPE%TYPE,
                                I_retail_type_ind             IN     NUMBER,
                                O_prom_retail                    OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                                O_prom_retail_unique             OUT NUMBER,
                                O_prom_exvatuom_retail           OUT NUMBER,
                                O_prom_exvatuom_retail_unique    OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                                O_prom_uom                       OUT RPM_FUTURE_RETAIL.SIMPLE_PROMO_UOM%TYPE,
                                O_prom_uom_unique                OUT NUMBER)
RETURN BOOLEAN;

FUNCTION IGNORE_APPROVED_EXCEPTIONS(O_error_msg      IN OUT VARCHAR2,
                                    I_component_type IN     VARCHAR2,
                                    I_price_event_id IN     NUMBER,
                                    I_customer_type  IN     NUMBER)
RETURN BOOLEAN;

FUNCTION IGNORE_EXCLUSIONS(O_error_msg      IN OUT VARCHAR2,
                           I_price_event_id IN     NUMBER,
                           I_promotion_id   IN     NUMBER,
                           I_promo_comp_id  IN     NUMBER)
RETURN BOOLEAN;

FUNCTION REMOVE_PROMO(O_error_msg     OUT VARCHAR2,
                   I_obj_promo_rec IN     OBJ_RPM_PROMO_REC)
RETURN BOOLEAN;

FUNCTION ADD_PROMO(O_error_msg        OUT VARCHAR2,
                   I_obj_promo_rec IN     OBJ_RPM_PROMO_REC)
RETURN BOOLEAN;

FUNCTION GET_PROMOTION_OBJECT(O_error_msg        OUT VARCHAR2,
                              I_obj_promo_rec IN     OBJ_RPM_PROMO_REC,
                              O_promo_rec        OUT OBJ_RPM_CC_PROMO_REC)
RETURN BOOLEAN;

FUNCTION VALIDATE_PROMOTION_FOR_MERGE(O_error_msg    OUT VARCHAR2,
                                      I_promo_rec IN     OBJ_RPM_CC_PROMO_REC)
RETURN BOOLEAN;

FUNCTION POPULATE_GTT(O_error_msg        OUT VARCHAR2,
                      I_obj_promo_rec IN     OBJ_RPM_PROMO_REC,
                      I_query_date    IN     DATE)
RETURN NUMBER;

FUNCTION POP_GTT_FOR_IL_CANCEL_APPLY(O_error_message                 OUT VARCHAR2,
                                     I_promo_dtl_id               IN     NUMBER,
                                     I_merch_nd_zone_nd_to_check  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                     I_curr_merch_nd_zone_nd_data IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER;

FUNCTION CHECK_CANCEL_INPUT_OVERLAPS(O_error_message    OUT VARCHAR2)
RETURN NUMBER;

FUNCTION CHECK_SIBLINGS_FOR_CANCEL(O_error_message             OUT VARCHAR2,
                                   O_valid_merch_nd_zone_nd    OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                                   I_promo_dtl_id           IN     NUMBER)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
--function definitions
-----------------------------------------------------------------------------------------

FUNCTION GET_LOC_NODE_CURRENCY(O_error_msg      IN OUT VARCHAR2,
                               O_currency_code     OUT RPM_ZONE.CURRENCY_CODE%TYPE,
                               I_zone_node_type IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                               I_zone_id        IN     RPM_ZONE_LOCATION.LOCATION%TYPE,
                               I_location       IN     RPM_PROMO_ZONE_LOCATION.ZONE_NODE_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SQL.GET_LOC_NODE_CURRENCY';

   cursor C_ZONE is
      select rz.currency_code
        from rpm_zone rz
       where rz.zone_id = I_zone_id;

   cursor C_LOCATION is
      select s.currency_code
        from store s
       where s.store = I_location
     union all
      select w.currency_code
        from wh w
       where w.wh = I_location;

BEGIN

   if I_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then

      open C_ZONE;
      fetch C_ZONE into O_currency_code;
      close C_ZONE;

   elsif I_zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) then
      open C_LOCATION;
      fetch C_LOCATION into O_currency_code;
      close C_LOCATION;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END GET_LOC_NODE_CURRENCY;
-----------------------------------------------------------------------------------------

FUNCTION GET_DATES(O_error_msg     IN OUT VARCHAR2,
                   I_obj_promo_rec IN     OBJ_RPM_PROMO_REC,
                   O_current_date     OUT DATE,
                   O_basis_date       OUT DATE,
                   O_promo_date       OUT DATE)
RETURN BOOLEAN IS

   L_program VARCHAR2(25) := 'RPM_PROMO_SQL.GET_DATES';

   L_vdate          DATE := DATES_SQL.GET_VDATE;
   L_pcd_start_date DATE := I_obj_promo_rec.start_date;
   L_pcd_end_date   DATE := NVL(I_obj_promo_rec.end_date, L_vdate + 1);

BEGIN

   --current
   O_current_date := L_vdate;

   --basis - promo
   if L_vdate < L_pcd_start_date or
      L_pcd_end_date < L_vdate then

      O_basis_date := I_obj_promo_rec.start_date;
      O_promo_date := I_obj_promo_rec.start_date;

   elsif L_vdate > L_pcd_start_date and
         L_vdate < L_pcd_end_date then

      if I_obj_promo_rec.state = PROMO_ACTIVE then
         O_basis_date := L_vdate;
         O_promo_date := L_vdate;
      else
         O_basis_date := NULL;
         O_promo_date := NULL;
      end if;

   elsif L_vdate = L_pcd_start_date then
        O_basis_date := I_obj_promo_rec.start_date;
        O_promo_date := I_obj_promo_rec.start_date;

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END GET_DATES;

-----------------------------------------------------------------------------------------
FUNCTION CHECK_CONFLICT (O_error_msg                IN OUT VARCHAR2,
                         I_rpm_promo_comp_detail_id IN     RPM_PROMO_DTL.PROMO_DTL_ID%TYPE,
                         I_async_conflict_error     IN OUT NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SQL.CHECK_CONFLICT_CHECK';

   cursor C_CONFLICT_CHECK is
      select 'x'
        from rpm_con_check_err
       where ref_id    = I_rpm_promo_comp_detail_id
         and ref_class = RPM_CONSTANTS.COMP_DETAIL_REF_CLASS;

   L_exists VARCHAR2(1);

BEGIN

   open C_CONFLICT_CHECK;
   fetch C_CONFLICT_CHECK INTO L_exists;

   if C_CONFLICT_CHECK%FOUND then
      I_async_conflict_error := 1;
   else
      I_async_conflict_error := 0;
   end if;

   close C_CONFLICT_CHECK;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CHECK_CONFLICT;
-----------------------------------------------------------------------------------------
FUNCTION CHECK_RANGED(O_error_msg              IN OUT VARCHAR2,
                      O_no_item_loc_ranged_ind    OUT NUMBER,
                      O_is_multi_item_loc_ind     OUT NUMBER)
RETURN BOOLEAN IS

   L_item_loc_count          NUMBER(9) := 0;
   L_distinct_item_loc       NUMBER(9) := 0;

   cursor C_IL_COUNT is
   select count(*), count(distinct item||location)
     from rpm_future_retail_gtt;

BEGIN

   open C_IL_COUNT;
   fetch C_IL_COUNT
    into L_item_loc_count,
         L_distinct_item_loc;
   close C_IL_COUNT;

   if L_item_loc_count = 0 then
      O_no_item_loc_ranged_ind := 1;
   else
      O_no_item_loc_ranged_ind := 0;
   end if;

   if L_distinct_item_loc = 0 or
      L_distinct_item_loc = 1 then
      O_is_multi_item_loc_ind := 0;
   else
      O_is_multi_item_loc_ind := 1;
   end if;


   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PROMO_SQL.CHECK_RANGED',
                                        to_char(SQLCODE));
      return FALSE;
END CHECK_RANGED;
-----------------------------------------------------------------------------------------
FUNCTION GET_MISC_INDS(O_error_msg                IN OUT VARCHAR2,
                       I_rpm_promo_comp_detail_id IN     RPM_PROMO_DTL.PROMO_DTL_ID%TYPE,
                       I_start_date               IN     DATE,
                       I_end_date                 IN     DATE,
                       O_non_regular_basis_ind       OUT NUMBER,
                       O_clearance_overlap_ind       OUT NUMBER,
                       O_price_change_overlap_ind    OUT NUMBER,
                       O_async_conflict_error        OUT NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SQL.GET_MISC_INDS';

   L_vdate             DATE      := get_vdate;
   L_non_reg_basis_ind NUMBER(1) := 0;

   cursor C_OVERLAP is
      select MAX(price_change_overlap_ind) price_change_overlap_ind,
             MAX(clearance_overlap_ind) clearance_overlap_ind
        from (select distinct
                     DECODE(NULL,
                            fr.price_change_id, 0,
                            1) price_change_overlap_ind,
                     DECODE(NULL,
                            fr.clearance_id, 0,
                            1) clearance_overlap_ind
                from rpm_future_retail_gtt fr
               where fr.action_date between I_start_date and
                                            NVL(I_end_date, L_vdate + RETAIL_LOOK_FORWARD_DAYS));

   cursor C_BASIS is
      select 1
        from rpm_future_retail_gtt fr,
             rpm_promo_item_loc_expl_gtt rpile
       where fr.action_date              = I_start_date
         and rpile.item                  = fr.item
         and rpile.location              = fr.location
         and fr.action_date              between rpile.detail_start_date and NVL(rpile.detail_end_date, to_date('3000', 'YYYY'))
         and (   fr.clearance_id         is not null
              or fr.price_change_id      is not null
              or (    rpile.promo_dtl_id is not null
                  and rpile.promo_dtl_id != I_rpm_promo_comp_detail_id));

BEGIN

   open C_OVERLAP;
   fetch C_OVERLAP into O_price_change_overlap_ind,
                        O_clearance_overlap_ind;
   close C_OVERLAP;

   open C_BASIS;
   fetch C_BASIS into L_non_reg_basis_ind;
   close C_BASIS;

   O_price_change_overlap_ind := NVL(O_price_change_overlap_ind, 0);
   O_clearance_overlap_ind    := NVL(O_clearance_overlap_ind, 0);

   if NVL(L_non_reg_basis_ind, 0) > 0 then
      O_non_regular_basis_ind := 1;
   else
      O_non_regular_basis_ind := 0;
   end if;

   ---

   if CHECK_CONFLICT(O_error_msg,
                     I_rpm_promo_comp_detail_id,
                     O_async_conflict_error) = FALSE then
      return FALSE;
   end if;


   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END GET_MISC_INDS;
-----------------------------------------------------------------------------------------
FUNCTION POPULATE_VAT(O_error_msg IN OUT VARCHAR2,
                      I_vat_date  IN     DATE)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SQL.POPULATE_VAT';

   L_default_tax_type    VARCHAR2(6)      := NULL;
   L_class_level_vat_ind VARCHAR2(1)      := NULL;
   L_custom_tax_ind      NUMBER           := 0;
   L_tax_calc_tbl        OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

   cursor C_CHECK_CUSTOM is
     select 1
       from rpm_item_loc_gtt rilg,
            store s,
            vat_region vr
      where rilg.location    = s.store
        and s.vat_region     = vr.vat_region
        and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM
        and rownum           = 1;

   cursor C_CUSTOM_TAX_DATA is
      select OBJ_TAX_CALC_REC(item,                             -- I_item
                              NULL,                             -- I_pack_ind
                              location,                         -- I_from_entity
                              RPM_CONSTANTS.TAX_LOC_TYPE_STORE, -- I_from_entity_type
                              location,                         -- I_to_entity
                              RPM_CONSTANTS.TAX_LOC_TYPE_STORE, -- I_to_entity_type
                              I_vat_date,                       -- I_effective_from_date
                              selling_retail,                   -- I_amount
                              selling_retail_currency,          -- I_amount_curr
                              NULL,                             -- I_amount_tax_incl_ind
                              NULL,                             -- I_origin_country_id
                              NULL,                             -- O_cum_tax_pct
                              NULL,                             -- O_cum_tax_value
                              NULL,                             -- O_total_tax_amount
                              NULL,                             -- O_total_tax_amount_curr
                              NULL,                             -- O_total_recover_amount
                              NULL,                             -- O_total_recover_amount_curr
                              NULL,                             -- O_tax_detail_tbl
                              'MARKUPCALC',                     -- I_tran_type
                              I_vat_date,                       -- I_tran_date
                              NULL,                             -- I_tran_id
                              'R')                              -- I_cost_retail_ind
        from (select item,
                     location,
                     selling_retail,
                     selling_retail_currency,
                     action_date,
                     RANK() OVER (PARTITION BY item,
                                               location
                                      ORDER BY action_date desc) date_rnk
                from rpm_future_retail_gtt)
       where date_rnk = 1;

BEGIN

   delete
     from rpm_me_il_vat_gtt;

   select class_level_vat_ind,
          default_tax_type
     into L_class_level_vat_ind,
          L_default_tax_type
     from system_options;

   if L_default_tax_type = RPM_CONSTANTS.SVAT_TAX_TYPE then

      open C_CHECK_CUSTOM;
      fetch C_CHECK_CUSTOM into L_custom_tax_ind;
      close C_CHECK_CUSTOM;

      if NVL(L_custom_tax_ind, 0) = 1 then

         open C_CUSTOM_TAX_DATA;
         fetch C_CUSTOM_TAX_DATA BULK COLLECT into L_tax_calc_tbl;
         close C_CUSTOM_TAX_DATA;

         if TAX_SQL.CALC_RETAIL_TAX(O_error_msg,
                                    L_tax_calc_tbl) = FALSE then
            return FALSE;
         end if;

         forall i IN 1..L_tax_calc_tbl.COUNT
            insert into rpm_me_il_vat_gtt (item,
                                           location,
                                           vat_rate)
               values (L_tax_calc_tbl(i).I_item,
                       L_tax_calc_tbl(i).I_from_entity,
                       L_tax_calc_tbl(i).O_cum_tax_pct);

      else -- SVAT, but no custom tax

         insert into rpm_me_il_vat_gtt (item,
                                        location,
                                        vat_rate)
            select item,
                   store,
                   vat_rate
              from (select /*+ LEADING(i) INDEX(v PK_VAT_ITEM) USE_NL(v1) */
                           distinct i.item,
                           s.store,
                           v.vat_rate,
                           v.active_date,
                           FIRST_VALUE(v.active_date) OVER (PARTITION BY i.item,
                                                                         s.vat_region
                                                                ORDER BY v.active_date desc) max_date
                      from vat_item v,
                           store s,
                           rpm_future_retail_gtt i,
                           class c
                     where v.item        = i.item
                       and i.location    = s.store
                       and i.dept        = c.dept
                       and i.class       = c.class
                       and s.vat_region  = v.vat_region
                       and v.vat_type    IN (RPM_CONSTANTS.VAT_TYPE_RETAIL,
                                             RPM_CONSTANTS.VAT_TYPE_BOTH)
                       and v.active_date <= I_vat_date
                       and (   L_class_level_vat_ind      = 'N'
                            or (    L_class_level_vat_ind = 'Y'
                                and c.class_vat_ind       = 'Y')))
             where active_date = max_date;

       end if;

    elsif L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then

      insert into rpm_me_il_vat_gtt (item,
                                     location,
                                     vat_rate,
                                     vat_value)
         select distinct item,
                location,
                cum_tax_pct tax_rate,
                cum_tax_value tax_amount
           from (select rfr.item,
                        rfr.location,
                        gt.effective_from_date,
                        gt.cum_tax_pct,
                        gt.cum_tax_value,
                        ROW_NUMBER() OVER (PARTITION BY gt.item,
                                                        gt.loc
                                               ORDER BY gt.effective_from_date desc) rnum,
                        FIRST_VALUE(gt.effective_from_date) OVER (PARTITION BY gt.item,
                                                                               gt.loc
                                                                      ORDER BY gt.effective_from_date desc) max_date
                   from gtax_item_rollup gt,
                        rpm_future_retail_gtt rfr
                  where gt.item (+)                 = rfr.item
                    and rfr.location                = gt.loc (+)
                    and gt.loc_type (+)             = RPM_CONSTANTS.TAX_LOC_TYPE_STORE
                    and gt.effective_from_date (+) <= I_vat_date)
          where effective_from_date = max_date
            and rnum                = 1;

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_VAT;
-----------------------------------------------------------------------------------------
FUNCTION POPULATE_ITEM_INFO(O_error_msg                IN OUT VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   insert into rpm_me_item_gtt (item,
                                standard_uom,
                                retail_include_vat_ind)
      select /*+ INDEX(im, pk_item_master) */distinct im.item,
             im.standard_uom,
             cl.class_vat_ind
        from rpm_future_retail_gtt fr,
             item_master im,
             class cl
       where fr.item  = im.item
         and im.dept  = cl.dept
         and im.class = cl.class;

   insert into rpm_item_loc_soh_gtt(price_event_id,
                                    item,
                                    location,
                                    zone_node_type,
                                    dept,
                                    stock_on_hand,
                                    pack_comp_soh,
                                    in_transit_qty,
                                    pack_comp_intran,
                                    qty_ordered,
                                    qty_received)
   select fr.price_event_id,
          fr.item,
          fr.location,
          DECODE(ils.loc_type,
                 'S', 0,
                 2) zone_node_type,
          fr.dept,
          NVL(ils.stock_on_hand,0),
          NVL(ils.pack_comp_soh,0),
          NVL(ils.in_transit_qty,0),
          NVL(ils.pack_comp_intran,0),
          0 qty_ordered,
          0 qty_received
     from (select rfr.price_event_id,
                  rfr.dept,
                  rfr.item,
                  rfr.location,
                  RANK() OVER(PARTITION BY rfr.price_event_id,
                                           rfr.item,
                                           rfr.location
                                  ORDER BY rfr.action_date desc) rank
             from rpm_future_retail_gtt rfr) fr,
          item_loc_soh ils
    where fr.rank  = 1
      and ils.item = fr.item
      and ils.loc  = fr.location;

   merge into rpm_item_loc_soh_gtt target
   using (select il.price_event_id,
                 il.item,
                 il.location,
                 sum(nvl(ol.qty_ordered,0)) qty_ordered,
                 sum(nvl(ol.qty_received,0)) qty_received
            from rpm_item_loc_soh_gtt il,
                 ordloc   ol,
                 ordhead  oh
           where ol.item = il.item
             and ol.location = il.location
             and ol.order_no = oh.order_no
             and oh.status = 'A'
           group by il.price_event_id,
                    il.item,
                    il.location) source
   on (    target.price_event_id = source.price_event_id
       and target.item = source.item
       and target.location = source.location)
   when matched then
   update
      set target.qty_ordered = nvl(source.qty_ordered,0),
          target.qty_received = nvl(source.qty_received,0);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PROMO_SQL.POPULATE_ITEM_INFO',
                                        to_char(SQLCODE));
      return FALSE;
END POPULATE_ITEM_INFO;
-----------------------------------------------------------------------------------------
FUNCTION CALCULATE_COST(O_error_msg               IN OUT VARCHAR2,
                        I_promo_comp_detail_rec   IN OUT NOCOPY PROMO_COMP_DETAIL_REC,
                        I_to_currency_code        IN     CURRENCIES.CURRENCY_CODE%TYPE,
                        O_cur_cost                   OUT FUTURE_COST.PRICING_COST%TYPE,
                        O_cur_cost_unique            OUT NUMBER,
                        O_basis_cost                 OUT FUTURE_COST.PRICING_COST%TYPE,
                        O_basis_cost_unique          OUT NUMBER,
                        O_cost_change_overlap_ind    OUT NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SQL.CALCULATE_COST';

   L_cost_calculation_method  RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE := NULL;

   L_current_date DATE := NULL;
   L_basis_date   DATE := NULL;
   L_promo_date   DATE := NULL;

   cursor C_CURRENT_COST is
      with curr_rate as
         (select currency_code,
                 exchange_rate,
                 effective_date
            from (select currency_code,
                         exchange_rate,
                         effective_date,
                         RANK() OVER (PARTITION BY currency_code
                                          ORDER BY effective_date desc) date_rank
                    from currency_rates
                   where exchange_type = 'C')
           where date_rank = 1)
      select DECODE(L_cost_calculation_method,
                    RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST, AVG(curr.curr_pricing_cost *
                                                                curr.curr_to_exchange_rate / curr.curr_from_exchange_rate),
                    MAX(curr.curr_pricing_cost *
                        curr.curr_to_exchange_rate / curr.curr_from_exchange_rate)) curr_cost,
             DECODE(COUNT(distinct curr.curr_pricing_cost),
                    1, 1,
                    0) curr_unique
        from (select /*+ ORDERED */
                     distinct fc.item,
                     fc.location,
                     FIRST_VALUE(fc.pricing_cost) OVER (PARTITION BY il.item,
                                                                     il.location
                                                            ORDER BY fc.active_date desc) curr_pricing_cost,
                     FIRST_VALUE(c_from.exchange_rate) OVER (ORDER BY c_from.effective_date desc) curr_from_exchange_rate,
                     FIRST_VALUE(c_to.exchange_rate) OVER (ORDER BY c_to.effective_date desc) curr_to_exchange_rate
                from rpm_future_retail_gtt il,
                     rpm_future_cost fc,
                     curr_rate c_from,
                     curr_rate c_to
               where fc.item            = il.item
                 and fc.location        = il.location
                 and fc.active_date    <= L_current_date
                 and fc.currency_code   = c_from.currency_code
                 and I_to_currency_code = c_to.currency_code) curr;

   cursor C_BASIS_COST is
      with curr_rate as
         (select currency_code,
                 exchange_rate,
                 effective_date
            from (select currency_code,
                         exchange_rate,
                         effective_date,
                         RANK() OVER (PARTITION BY currency_code
                                          ORDER BY effective_date desc) date_rank
                    from currency_rates
                   where exchange_type = 'C')
           where date_rank = 1)
      select DECODE(L_cost_calculation_method,
                    RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST, AVG(basis.basis_pricing_cost *
                                                                basis.basis_to_exchange_rate / basis.basis_from_exchange_rate),
                    MAX(basis.basis_pricing_cost *
                        basis.basis_to_exchange_rate / basis.basis_from_exchange_rate)) basis_cost,
             DECODE(COUNT(distinct basis.basis_pricing_cost),
                    1, 1,
                    0) basis_unique
        from (select /*+ ORDERED */
                     distinct fc.item,
                     fc.location,
                     FIRST_VALUE(fc.pricing_cost) OVER (PARTITION BY il.item,
                                                                     il.location
                                                            ORDER BY fc.active_date desc) basis_pricing_cost,
                     FIRST_VALUE(c_from.exchange_rate) OVER (ORDER BY c_from.effective_date desc) basis_from_exchange_rate,
                     FIRST_VALUE(c_to.exchange_rate) OVER (ORDER BY c_to.effective_date desc) basis_to_exchange_rate
                from rpm_future_retail_gtt il,
                     rpm_future_cost fc,
                     curr_rate c_from,
                     curr_rate c_to
               where fc.item            = il.item
                 and fc.location        = il.location
                 and fc.active_date    <= L_basis_date
                 and fc.currency_code   = c_from.currency_code
                 and I_to_currency_code = c_to.currency_code) basis;

   cursor C_COST_OVERLAP is
      select /*+ LEADING(IL) */
             1
        from future_cost f,
             rpm_future_retail_gtt il
       where f.item     = il.item
         and f.location = il.location
         and f.active_date BETWEEN I_promo_comp_detail_rec.promo_rec.start_date AND
                                   NVL(I_promo_comp_detail_rec.promo_rec.end_date, L_current_date + COST_LOOK_FORWARD_DAYS)
         and rownum     = 1;

BEGIN

   if RPM_SYSTEM_OPTIONS_SQL.GET_COST_CALCULATION_METHOD(L_cost_calculation_method,
                                                         O_error_msg) = FALSE then
      return FALSE;
   end if;

   if GET_DATES(O_error_msg,
                I_promo_comp_detail_rec.promo_rec,
                L_current_date,
                L_basis_date,
                L_promo_date) = FALSE then
      return FALSE;
   end if;

   open C_CURRENT_COST;
   fetch C_CURRENT_COST into O_cur_cost,
                             O_cur_cost_unique;
   close C_CURRENT_COST;

   open C_BASIS_COST;
   fetch C_BASIS_COST into O_basis_cost,
                           O_basis_cost_unique;
   close C_BASIS_COST;

   if CURRENCY_SQL.ROUND_CURRENCY(O_error_msg,
                                  O_cur_cost,
                                  I_to_currency_code,
                                  'C') = FALSE then
      return FALSE;
   end if;

   if CURRENCY_SQL.ROUND_CURRENCY(O_error_msg,
                                  O_basis_cost,
                                  I_to_currency_code,
                                  'C') = FALSE then
      return FALSE;
   end if;

   O_cost_change_overlap_ind := 0;

   open C_COST_OVERLAP;
   fetch C_COST_OVERLAP into O_cost_change_overlap_ind;
   close C_COST_OVERLAP;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CALCULATE_COST;
-----------------------------------------------------------------------------------------
FUNCTION CALCULATE_RETAIL_QUERY(O_error_msg                   IN OUT VARCHAR2,
                                I_retail_date                 IN     DATE,
                                I_to_currency_code            IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                I_customer_type               IN     RPM_CUST_SEGMENT_PROMO_FR.CUSTOMER_TYPE%TYPE,
                                I_retail_type_ind             IN     NUMBER,
                                O_prom_retail                    OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                                O_prom_retail_unique             OUT NUMBER,
                                O_prom_exvatuom_retail           OUT NUMBER,
                                O_prom_exvatuom_retail_unique    OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                                O_prom_uom                       OUT RPM_FUTURE_RETAIL.SIMPLE_PROMO_UOM%TYPE,
                                O_prom_uom_unique                OUT NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SQL.CALCULATE_RETAIL_QUERY';

   L_loop_flag NUMBER(1) := 0;

   L_count                    BINARY_INTEGER := 0;
   L_agg_prom_retail          RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := 0;
   L_agg_prom_exvatuom_retail RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := 0;
   L_prom_retail              RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := NULL;
   L_prom_exvatuom_retail     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := NULL;
   L_prev_retail              RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := NULL;
   L_prev_exvatuom_retail     RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := NULL;
   L_prev_simple_promo_uom    RPM_FUTURE_RETAIL.SIMPLE_PROMO_UOM%TYPE := NULL;
   L_default_tax_type         SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE    := NULL;

   cursor C_RETAIL is
      select case
                when SUM(NVL((ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran + ils.qty_ordered - ils.qty_received), 0))= 0 then
                   AVG(retl.prom_retail)
                else
                   SUM(retl.prom_retail*(ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran + ils.qty_ordered - ils.qty_received))/
                   SUM((ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran + ils.qty_ordered - ils.qty_received))
             end as prom_retail,
             ---
             DECODE(COUNT(distinct retl.prom_retail),
                    1, 1,
                    0) prom_retail_unique,
             ---
             case L_default_tax_type
                when RPM_CONSTANTS.GTAX_TAX_TYPE then
                   AVG(retl.prom_retail - ((retl.prom_retail - NVL(v.vat_value, 0)) * (NVL(v.vat_rate,0)/100)) - NVL(v.vat_value, 0))
                else
                   AVG(retl.prom_retail/(1+(NVL(v.vat_rate,0)/100)))
             end exvat_retail,
             ---
             DECODE(DECODE(L_default_tax_type,
                           RPM_CONSTANTS.GTAX_TAX_TYPE, COUNT(distinct (retl.prom_retail - ((retl.prom_retail - NVL(v.vat_value,0)) *
                                                                                            (NVL(v.vat_rate,0)/100)) - NVL(v.vat_value,0))),
                           COUNT(distinct retl.prom_retail/(1+(NVL(v.vat_rate,0)/100)))),
                    1, 1,
                    0) exvat_retail_unique,
             ---
             DECODE(COUNT(distinct retl.simple_promo_uom),
                    1, 1,
                    0) prom_uom_unique,
             ---
             MIN(retl.simple_promo_uom) simple_promo_uom,
             MAX(DECODE(i.standard_uom,
                        retl.simple_promo_uom, 0,
                        1)) loop_flag
        from rpm_me_item_gtt i,
             rpm_me_il_vat_gtt v,
             rpm_item_loc_soh_gtt ils,
             (select distinct fr.item,
                     fr.location,
                     FIRST_VALUE(case
                                    when I_customer_type is NOT NULL then
                                       fr.clear_uom
                                    else
                                       case
                                          when I_retail_type_ind = RPM_CONSTANTS.CURRENT_RETAIL then
                                             fr.clear_uom
                                          else
                                             fr.simple_promo_uom
                                       end
                                 end) OVER (PARTITION BY fr.item,
                                                         fr.location
                                                ORDER BY fr.action_date desc) simple_promo_uom,
                     FIRST_VALUE(case
                                    when I_customer_type is NOT NULL then
                                       fr.clear_retail
                                    else
                                       case
                                          when I_retail_type_ind = RPM_CONSTANTS.CURRENT_RETAIL then
                                             fr.clear_retail
                                          else
                                             fr.simple_promo_retail
                                       end
                                    end) OVER (PARTITION BY fr.item,
                                                         fr.location
                                                ORDER BY fr.action_date desc) prom_retail
                from rpm_future_retail_gtt fr
               where fr.action_date <=  I_retail_date) retl
       where i.item         = retl.item
         and ils.item       = retl.item
         and ils.location   = retl.location
         and v.item(+)      = retl.item
         and v.location(+)  = retl.location;

   cursor C_RETAIL_CS is
      select case
                when SUM(NVL((ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran + ils.qty_ordered - ils.qty_received), 0))= 0  then
                      AVG(retl.prom_retail)
                else
                   SUM(retl.prom_retail * (ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran + ils.qty_ordered - ils.qty_received))/
                   SUM((ils.stock_on_hand + ils.pack_comp_soh + ils.in_transit_qty + ils.pack_comp_intran + ils.qty_ordered - ils.qty_received))
                end as prom_retail,
             DECODE(COUNT(distinct retl.prom_retail),
                    1, 1,
                    0) prom_retail_unique,
             case L_default_tax_type
                when RPM_CONSTANTS.GTAX_TAX_TYPE then
                   AVG(retl.prom_retail - ((retl.prom_retail - NVL(v.vat_value, 0)) * (NVL(v.vat_rate,0)/100)) - NVL(v.vat_value, 0))
                else
                   AVG(retl.prom_retail/(1+(NVL(v.vat_rate,0)/100)))
             end exvat_retail,
             DECODE(DECODE(L_default_tax_type,
                           RPM_CONSTANTS.GTAX_TAX_TYPE, COUNT(distinct (retl.prom_retail - ((retl.prom_retail - NVL(v.vat_value, 0)) *
                                                                                            (NVL(v.vat_rate, 0) / 100)) - NVL(v.vat_value, 0))),
                           COUNT(distinct retl.prom_retail/(1+(NVL(v.vat_rate,0)/100)))),
                    1, 1,
                    0) exvat_retail_unique,
             DECODE(COUNT(distinct retl.simple_promo_uom),
                    1, 1,
                    0) prom_uom_unique,
             MIN(retl.simple_promo_uom) simple_promo_uom,
             MAX(DECODE(i.standard_uom,
                        retl.simple_promo_uom, 0,
                        1)) loop_flag
        from rpm_me_item_gtt i,
             rpm_me_il_vat_gtt v,
             rpm_item_loc_soh_gtt ils,
             (select distinct fr.item,
                     fr.location,
                     FIRST_VALUE(fr.promo_uom) OVER (PARTITION BY fr.item,
                                                                  fr.location
                                                         ORDER BY fr.action_date desc) simple_promo_uom,
                     FIRST_VALUE(fr.promo_retail) OVER (PARTITION BY fr.item,
                                                                     fr.location
                                                            ORDER BY fr.action_date desc) prom_retail
                from rpm_cust_segment_promo_fr_gtt fr
               where fr.action_date <=  I_retail_date) retl
       where i.item         = retl.item
         and ils.item       = retl.item
         and ils.location   = retl.location
         and v.item(+)      = retl.item
         and v.location(+)  = retl.location;

   cursor C_NEED_LOOP is
      select retl.item,
             retl.location,
             i.standard_uom,
             retl.simple_promo_uom,
             retl.prom_retail prom_retail,
             case L_default_tax_type
                when RPM_CONSTANTS.GTAX_TAX_TYPE then
                   retl.prom_retail - ((retl.prom_retail - NVL(v.vat_value, 0)) * (NVL(v.vat_rate,0)/100)) - NVL(v.vat_value, 0)
                else
                   retl.prom_retail/(1+(NVL(v.vat_rate,0)/100))
             end exvat_retail
        from rpm_me_item_gtt i,
             rpm_me_il_vat_gtt v,
             (select distinct fr.item,
                     fr.location,
                     FIRST_VALUE(fr.simple_promo_uom) OVER (PARTITION BY fr.item,
                                                                         fr.location
                                                                ORDER BY fr.action_date desc) simple_promo_uom,
                     FIRST_VALUE(fr.simple_promo_retail) OVER (PARTITION BY fr.item,
                                                                            fr.location
                                                                   ORDER BY fr.action_date desc) prom_retail
                from rpm_future_retail_gtt fr
               where fr.action_date <=  I_retail_date) retl
       where I_customer_type is NULL
         and i.item           = retl.item
         and v.item(+)        = retl.item
         and v.location(+)    = retl.location
       union all
      select retl.item,
             retl.location,
             i.standard_uom,
             retl.simple_promo_uom,
             retl.prom_retail prom_retail,
             case L_default_tax_type
                when RPM_CONSTANTS.GTAX_TAX_TYPE then
                   retl.prom_retail - ((retl.prom_retail - NVL(v.vat_value, 0)) * (NVL(v.vat_rate,0)/100)) - NVL(v.vat_value, 0)
                else
                   retl.prom_retail/(1+(NVL(v.vat_rate,0)/100))
             end exvat_retail
        from rpm_me_item_gtt i,
             rpm_me_il_vat_gtt v,
             (select distinct fr.item,
                     fr.location,
                     FIRST_VALUE(fr.promo_uom) OVER (PARTITION BY fr.item,
                                                                  fr.location
                                                         ORDER BY fr.action_date desc) simple_promo_uom,
                     FIRST_VALUE(fr.promo_retail) OVER (PARTITION BY fr.item,
                                                                     fr.location
                                                            ORDER BY fr.action_date desc) prom_retail
                from rpm_cust_segment_promo_fr_gtt fr
               where fr.action_date <=  I_retail_date) retl
       where I_customer_type is NOT NULL
         and i.item         = retl.item
         and v.item(+)      = retl.item
         and v.location(+)  = retl.location;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_msg,
                                              L_default_tax_type) = FALSE then
      return FALSE;
   end if;

   if I_retail_date is NULL then
      return TRUE;
   end if;

   if I_customer_type is NOT NULL and
      I_retail_type_ind = RPM_CONSTANTS.NEW_RETAIL then

      open C_RETAIL_CS;
      fetch C_RETAIL_CS into O_prom_retail,
                             O_prom_retail_unique,
                             O_prom_exvatuom_retail,
                             O_prom_exvatuom_retail_unique,
                             O_prom_uom_unique,
                             O_prom_uom,
                             L_loop_flag;
      close C_RETAIL_CS;

   else

      open C_RETAIL;
      fetch C_RETAIL into O_prom_retail,
                          O_prom_retail_unique,
                          O_prom_exvatuom_retail,
                          O_prom_exvatuom_retail_unique,
                          O_prom_uom_unique,
                          O_prom_uom,
                          L_loop_flag;
      close C_RETAIL;

   end if;

   --check and see if we need to do some UOM conversions
   if L_loop_flag = 1 then

      O_prom_retail_unique          := 1;
      O_prom_exvatuom_retail_unique := 1;
      O_prom_uom_unique             := 1;

      for rec IN C_NEED_LOOP LOOP

         L_count := L_count + 1;

         if rec.standard_uom != rec.simple_promo_uom then
            if UOM_SQL.CONVERT(O_error_msg,
                               L_prom_exvatuom_retail,
                               rec.simple_promo_uom,
                               rec.exvat_retail,
                               rec.standard_uom,
                               rec.item,
                               NULL,              --i_supplier
                               NULL) = FALSE then --i_origin_country
               return FALSE;
            end if;
         end if;

         L_agg_prom_retail          := L_agg_prom_retail + rec.prom_retail;
         L_agg_prom_exvatuom_retail := L_agg_prom_exvatuom_retail + L_prom_exvatuom_retail;

         if L_prom_retail != L_prev_retail then
            O_prom_retail_unique := 0;
         end if;

         if L_prom_exvatuom_retail != L_prev_exvatuom_retail then
            O_prom_exvatuom_retail_unique := 0;
         end if;

         if rec.simple_promo_uom != L_prev_simple_promo_uom then
            O_prom_uom_unique := 0;
         end if;

         L_prev_retail := L_prom_retail;
         L_prev_exvatuom_retail := L_prom_exvatuom_retail;
         L_prev_simple_promo_uom := rec.simple_promo_uom;
         O_prom_uom := rec.simple_promo_uom;

      end loop;

      O_prom_retail := L_agg_prom_retail / L_count;
      O_prom_exvatuom_retail := L_agg_prom_exvatuom_retail / L_count;

   end if;

   if CURRENCY_SQL.ROUND_CURRENCY(O_error_msg,
                                  O_prom_retail,
                                  I_to_currency_code,
                                  'C') = FALSE then
      return FALSE;
   end if;
   if CURRENCY_SQL.ROUND_CURRENCY(O_error_msg,
                                  O_prom_exvatuom_retail,
                                  I_to_currency_code,
                                  'C') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CALCULATE_RETAIL_QUERY;
-----------------------------------------------------------------------------------------

FUNCTION IGNORE_EXCLUSIONS(O_error_msg        IN OUT VARCHAR2,
                           I_price_event_id   IN     NUMBER,
                           I_promotion_id     IN     NUMBER,
                           I_promo_comp_id    IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'RPM_PROMO_SQL.IGNORE_EXCLUSIONS';

BEGIN

   delete from rpm_promo_item_loc_expl_gtt
    where (item,
           location) IN (select distinct
                                gtt.item,
                                gtt.location
                           from rpm_promo_item_loc_expl_gtt gtt
                          where gtt.type                         = RPM_CONSTANTS.RETAIL_EXCLUDE
                           and gtt.price_event_id                = I_price_event_id
                           and (   (    gtt.promo_id             = I_promotion_id
                                    and gtt.exception_parent_id is NULL)
                                or (    gtt.promo_id             = I_promotion_id
                                    and gtt.promo_comp_id        = I_promo_comp_id
                                    and gtt.exception_parent_id is NOT NULL)));

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END IGNORE_EXCLUSIONS;
-----------------------------------------------------------------------------------------
FUNCTION IGNORE_APPROVED_EXCEPTIONS(O_error_msg        IN OUT VARCHAR2,
                                    I_component_type   IN     VARCHAR2,
                                    I_price_event_id   IN     NUMBER,
                                    I_customer_type    IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_PROMO_SQL.IGNORE_APPROVED_EXCEPTIONS';

   L_price_event_ids  OBJ_NUMERIC_ID_TABLE     := NULL;
   L_child_event_ids  OBJ_NUM_NUM_DATE_TBL     := NULL;
   L_cc_error_tbl     CONFLICT_CHECK_ERROR_TBL := NULL;
   L_price_event_type VARCHAR2(3)              := NULL;

BEGIN

   if I_component_type = RPM_CONSTANTS.SIMPLE_CODE then

      if I_customer_type is NULL then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION;
      else
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO;
      end if;

   elsif I_component_type = RPM_CONSTANTS.THRESHOLD_CODE then

      if I_customer_type is NULL then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION;
      else
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO;
      end if;

   end if;

   L_price_event_ids := NEW OBJ_NUMERIC_ID_TABLE(I_price_event_id);

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_CHILDREN(L_cc_error_tbl,
                                             L_child_event_ids,
                                             L_price_event_ids,
                                             L_price_event_type) = 0 then
      ---
      O_error_msg := L_cc_error_tbl(1).error_string;
      return FALSE;
   end if;

   if L_child_event_ids is NULL or
      L_child_event_ids.COUNT = 0 then
      ---
      return TRUE;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_TIMELINES(L_cc_error_tbl,
                                                 L_price_event_type,
                                                 L_child_event_ids) = 0 then
      O_error_msg := L_cc_error_tbl(1).error_string;
      return FALSE;
   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END IGNORE_APPROVED_EXCEPTIONS;

-------------------------------------------------------------------------------------------

FUNCTION GET_BASIS_RETAIL_VALUES(O_error_msg                IN OUT VARCHAR2,
                                 I_obj_rpm_promo_tbl        IN     OBJ_RPM_PROMO_TBL,
                                 O_obj_rpm_promo_values_tbl IN OUT OBJ_RPM_PROMO_VALUES_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PROMO_SQL.GET_BASIS_RETAIL_VALUES';

   L_cur_cost                FUTURE_COST.PRICING_COST%TYPE := NULL;
   L_cur_cost_unique         NUMBER                        := NULL;
   L_basis_cost              FUTURE_COST.PRICING_COST%TYPE := NULL;
   L_basis_cost_unique       NUMBER                        := NULL;
   L_cost_change_overlap_ind NUMBER                        := NULL;

   L_prom_retail                 RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := NULL;
   L_prom_retail_unique          NUMBER                                  := NULL;
   L_prom_exvatuom_retail        RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE   := NULL;
   L_prom_exvatuom_retail_unique NUMBER                                  := NULL;
   L_prom_uom_unique             NUMBER                                  := NULL;
   L_prom_uom                    RPM_FUTURE_RETAIL.SIMPLE_PROMO_UOM%TYPE := NULL;

   L_non_regular_basis_ind    NUMBER := NULL;
   L_is_multi_item_loc_ind    NUMBER := NULL;
   L_clearance_overlap_ind    NUMBER := NULL;
   L_price_change_overlap_ind NUMBER := NULL;
   L_no_item_loc_ranged_ind   NUMBER := NULL;
   L_async_conflict_error     NUMBER := NULL;

   L_current_date DATE   := NULL;
   L_basis_date   DATE   := NULL;
   L_promo_date   DATE   := NULL;
   L_count        NUMBER := 0;
   L_vdate        DATE   := GET_VDATE;
   L_query_date   DATE   := NULL;

   L_item_from  VARCHAR2(2255) := NULL;
   L_item_where VARCHAR2(2255) := NULL;
   L_loc_from   VARCHAR2(2255) := NULL;
   L_loc_where  VARCHAR2(2255) := NULL;

   L_select        VARCHAR2(32767) := NULL;
   L_from          VARCHAR2(5000)  := NULL;
   L_where         VARCHAR2(6000)  := NULL;
   L_order         VARCHAR2(100)   := ' order by rfr.item, rfr.location, rfr.action_date';
   L_cost_insert   VARCHAR2(32767) := NULL;
   L_retail_insert VARCHAR2(32767) := NULL;

   L_obj_promo_rec OBJ_RPM_PROMO_REC  := NULL;

   L_local_currency        CURRENCIES.CURRENCY_CODE%TYPE := NULL;
   L_promo_comp_detail_rec PROMO_COMP_DETAIL_REC         := NULL;
   L_promo_values_rec      OBJ_RPM_PROMO_VALUES_REC      := NULL;
   L_promo_values_tbl      OBJ_RPM_PROMO_VALUES_TBL      := OBJ_RPM_PROMO_VALUES_TBL();
   L_marg_vis_tbl          OBJ_MARGIN_VISIBILITY_TBL     := OBJ_MARGIN_VISIBILITY_TBL();
   L_promo_ids             OBJ_NUMERIC_ID_TABLE          := OBJ_NUMERIC_ID_TABLE();

   L_dept             NUMBER(4)    := NULL;
   L_class            NUMBER(4)    := NULL;
   L_subclass         NUMBER(4)    := NULL;
   L_item             VARCHAR2(25) := NULL;
   L_diff_id          VARCHAR2(10) := NULL;
   L_merch_type       NUMBER(1)    := NULL;
   L_markup_calc_type NUMBER       := NULL;

   cursor C_GET_MARKUP_CALC_TYPE is
      select markup_calc_type
        from rpm_merch_retail_def_expl rmr
       where (    L_merch_type IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                   RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
              and rmr.dept     = L_dept
              and rmr.class    = NVL(L_class, rmr.class)
              and rmr.subclass = NVL(L_subclass, rmr.subclass))
          or (    L_merch_type = RPM_CONSTANTS.PARENT_MERCH_TYPE
              and EXISTS (select 1
                            from item_master im
                           where im.item       = L_item
                             and im.tran_level = im.item_level + 1
                             and im.dept       = rmr.dept
                             and im.class      = rmr.class
                             and im.subclass   = rmr.subclass))
          or (    L_merch_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
              and EXISTS (select 1
                            from item_master im
                           where im.item       = L_item
                             and im.tran_level = im.item_level + 1
                             and im.dept       = rmr.dept
                             and im.class      = rmr.class
                             and im.subclass   = rmr.subclass
                             and (   im.diff_1 = L_diff_id
                                  or im.diff_2 = L_diff_id
                                  or im.diff_3 = L_diff_id
                                  or im.diff_4 = L_diff_id)))
          or (    L_merch_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
              and EXISTS (select 1
                            from item_master im
                           where im.item     = L_item
                             and im.dept     = rmr.dept
                             and im.class    = rmr.class
                             and im.subclass = rmr.subclass));

   cursor C_ADD_MARKUP_CHG_DATES IS
      select new OBJ_RPM_PROMO_VALUES_REC(t1.workspace_id,
                                          t1.rpm_promo_comp_detail_id,
                                          t1.currency_code,
                                          t1.cur_rtl,
                                          t1.cur_rtl_unique,
                                          t1.cur_rtl_stduom_exvat,
                                          t1.cur_rtl_stduom_exvat_unique,
                                          t1.cur_cost,
                                          t1.cur_cost_unique,
                                          t1.cur_rtl_uom,
                                          t1.basis_rtl,
                                          t1.basis_rtl_unique,
                                          t1.basis_rtl_stduom_exvat,
                                          t1.basis_rtl_stduom_exvat_unique,
                                          t1.basis_cost,
                                          t1.basis_cost_unique,
                                          t1.basis_rtl_uom,
                                          t1.promo_rtl,
                                          t1.promo_rtl_unique,
                                          t1.promo_rtl_stduom_exvat,
                                          t1.promo_rtl_stduom_exvat_unique,
                                          t1.promo_cost,
                                          t1.promo_rtl_uom,
                                          t1.non_regular_basis_ind,
                                          t1.is_multi_item_loc_ind,
                                          t1.clearance_overlap_ind,
                                          t1.price_change_overlap_ind,
                                          t1.cost_change_overlap_ind,
                                          t1.no_item_loc_ranged_ind,
                                          t1.async_conflict_error,
                                          t1.conflict_during_calculation,
                                          t2.markup_change_date)
      from table(cast (L_promo_values_tbl as OBJ_RPM_PROMO_VALUES_TBL)) t1,
           table(cast (L_marg_vis_tbl as OBJ_MARGIN_VISIBILITY_TBL)) t2
     where t1.rpm_promo_comp_detail_id = t2.price_event_id(+);

BEGIN

   L_count := I_obj_rpm_promo_tbl.FIRST;

   while L_count is NOT NULL loop

      delete from rpm_future_retail_gtt;
      delete from rpm_me_item_gtt;

      delete from rpm_promo_item_loc_expl_gtt;
      delete from rpm_promo_fr_item_loc_gtt;

      delete from rpm_cust_segment_promo_fr_gtt;

      L_obj_promo_rec := I_obj_rpm_promo_tbl(L_count);

      L_count := I_obj_rpm_promo_tbl.NEXT(L_count);

      L_promo_comp_detail_rec.promo_rec := L_obj_promo_rec;

      --get local currecy
      if GET_LOC_NODE_CURRENCY(O_error_msg,
                               L_local_currency,
                               L_obj_promo_rec.zone_node_type,
                               L_obj_promo_rec.zone_id,
                               L_obj_promo_rec.location) = FALSE then
         return 0;
      end if;

      -- if the record is a system generated exclusion, there should be no full column detail data calculated
      if NVL(L_obj_promo_rec.sys_generated_exclusion, 0) = 0 then

         L_promo_values_rec := OBJ_RPM_PROMO_VALUES_REC(L_promo_comp_detail_rec.promo_rec.workspace_id,
                                                        L_promo_comp_detail_rec.promo_rec.rpm_promo_comp_detail_id,
                                                        L_local_currency,
                                                        NULL,                          --cur_rtl
                                                        1,                             --cur_rtl_unique
                                                        NULL,                          --cur_rtl_margin
                                                        1,                             --cur_rtl_margin_unique
                                                        NULL,                          --cur_cost
                                                        1,                             --cur_cost_unique
                                                        NULL,                          --cur_rtl_uom
                                                        NULL,                          --basis_rtl
                                                        1,                             --basis_rtl_unique
                                                        NULL,                          --basis_rtl_margin
                                                        1,                             --basis_rtl_margin_unique
                                                        NULL,                          --basis_cost
                                                        1,                             --basis_cost_unique
                                                        NULL,                          --basis_rtl_uom
                                                        NULL,                          --promo_rtl
                                                        1,                             --promo_rtl_unique
                                                        NULL,                          --promo_rtl_margin
                                                        1,                             --promo_rtl_margin_unique
                                                        NULL,                          --promo_cost
                                                        NULL,                          --promo_rtl_uom
                                                        0,                             --non_regular_basis_ind
                                                        NULL,                          --is_multi_item_loc_ind
                                                        0,                             --clearance_overlap_ind
                                                        0,                             --price_change_overlap_ind
                                                        0,                             --cost_change_overlap_ind
                                                        NULL,                          --no_item_loc_ranged_ind
                                                        NULL,                          --async_conflict_error
                                                        0,                             --conflict_during_calculation
                                                        NULL);                         --markup_change_date

      else

         L_promo_values_rec := OBJ_RPM_PROMO_VALUES_REC(L_promo_comp_detail_rec.promo_rec.workspace_id,
                                                        L_promo_comp_detail_rec.promo_rec.rpm_promo_comp_detail_id,
                                                        L_local_currency,
                                                        NULL,                          --cur_rtl
                                                        NULL,                          --cur_rtl_unique
                                                        NULL,                          --cur_rtl_margin
                                                        NULL,                          --cur_rtl_margin_unique
                                                        NULL,                          --cur_cost
                                                        NULL,                          --cur_cost_unique
                                                        NULL,                          --cur_rtl_uom
                                                        NULL,                          --basis_rtl
                                                        NULL,                          --basis_rtl_unique
                                                        NULL,                          --basis_rtl_margin
                                                        NULL,                          --basis_rtl_margin_unique
                                                        NULL,                          --basis_cost
                                                        NULL,                          --basis_cost_unique
                                                        NULL,                          --basis_rtl_uom
                                                        NULL,                          --promo_rtl
                                                        NULL,                          --promo_rtl_unique
                                                        NULL,                          --promo_rtl_margin
                                                        NULL,                          --promo_rtl_margin_unique
                                                        NULL,                          --promo_cost
                                                        NULL,                          --promo_rtl_uom
                                                        NULL,                          --non_regular_basis_ind
                                                        NULL,                          --is_multi_item_loc_ind
                                                        NULL,                          --clearance_overlap_ind
                                                        NULL,                          --price_change_overlap_ind
                                                        NULL,                          --cost_change_overlap_ind
                                                        NULL,                          --no_item_loc_ranged_ind
                                                        NULL,                          --async_conflict_error
                                                        NULL,                          --conflict_during_calculation
                                                        NULL);                         --markup_change_date

         if CHECK_CONFLICT(O_error_msg,
                           L_promo_values_rec.rpm_promo_comp_detail_id,
                           L_promo_values_rec.async_conflict_error) = FALSE then
            return 0;
         end if;

         L_promo_values_tbl.EXTEND();
         L_promo_values_tbl(L_promo_values_tbl.COUNT) := L_promo_values_rec;

         CONTINUE;
      end if;

      ----------------------------------------------------------------------------
      -- explosion and populate rpm_future_retail_gtt
      ----------------------------------------------------------------------------

      if L_obj_promo_rec.end_date is NULL then
         L_query_date := L_vdate + COST_LOOK_FORWARD_DAYS;

      elsif L_vdate > L_obj_promo_rec.end_date then
         L_query_date := L_vdate;

      else
         L_query_date := L_obj_promo_rec.end_date;
      end if;

      if POPULATE_GTT(O_error_msg,
                      L_obj_promo_rec,
                      L_query_date) = 0 then
         return 0;
      end if;

      if IGNORE_APPROVED_EXCEPTIONS(O_error_msg,
                                    L_obj_promo_rec.component_type,
                                    L_obj_promo_rec.rpm_promo_comp_detail_id,
                                    L_obj_promo_rec.customer_type) = FALSE then
         return 0;
      end if;

      if L_obj_promo_rec.change_type != RPM_CONSTANTS.RETAIL_EXCLUDE then

         if IGNORE_EXCLUSIONS(O_error_msg,
                              L_obj_promo_rec.rpm_promo_comp_detail_id,
                              L_obj_promo_rec.promotion_id,
                              L_obj_promo_rec.component_id) = FALSE then
            return 0;
         end if;
      end if;

      ----------------------------------------------------------------------------
      -- misc indicators
      ----------------------------------------------------------------------------

      if CHECK_RANGED(O_error_msg,
                      L_no_item_loc_ranged_ind,
                      L_is_multi_item_loc_ind) = FALSE then
         return 0;
      end if;

      L_promo_values_rec.no_item_loc_ranged_ind := L_no_item_loc_ranged_ind;
      L_promo_values_rec.is_multi_item_loc_ind  := L_is_multi_item_loc_ind;

      if L_no_item_loc_ranged_ind = 0 then

         if POPULATE_ITEM_INFO(O_error_msg) = FALSE then
            return 0;
         end if;

         L_dept       := L_obj_promo_rec.dept;
         L_class      := L_obj_promo_rec.class;
         L_subclass   := L_obj_promo_rec.subclass;
         L_merch_type := L_obj_promo_rec.merch_type;
         L_item       := L_obj_promo_rec.item;
         L_diff_id    := L_obj_promo_rec.diff_id;

         open C_GET_MARKUP_CALC_TYPE;
         fetch C_GET_MARKUP_CALC_TYPE into L_markup_calc_type;

         ----------------------------------------------------------------------------
         -- populate cost info
         ----------------------------------------------------------------------------

         if CALCULATE_COST(O_error_msg,
                           L_promo_comp_detail_rec,
                           L_local_currency,
                           L_cur_cost,
                           L_cur_cost_unique,
                           L_basis_cost,
                           L_basis_cost_unique,
                           L_cost_change_overlap_ind) = FALSE then
            return 0;
         end if;

         L_promo_values_rec.cur_cost                := L_cur_cost;
         L_promo_values_rec.cur_cost_unique         := L_cur_cost_unique;
         L_promo_values_rec.basis_cost              := L_basis_cost;
         L_promo_values_rec.basis_cost_unique       := L_basis_cost_unique;
         L_promo_values_rec.promo_cost              := L_basis_cost;
         L_promo_values_rec.cost_change_overlap_ind := L_cost_change_overlap_ind;

         if GET_DATES(O_error_msg,
                      L_obj_promo_rec,
                      L_current_date,
                      L_basis_date,
                      L_promo_date) = FALSE then
            return 0;
         end if;

         ----------------------------------------------------------------------------
         -- current retail
         ----------------------------------------------------------------------------

         if POPULATE_VAT(O_error_msg,
                         L_current_date) = FALSE then
            return 0;
         end if;

         if CALCULATE_RETAIL_QUERY(O_error_msg,
                                   L_current_date,
                                   L_local_currency,
                                   L_obj_promo_rec.customer_type,
                                   RPM_CONSTANTS.CURRENT_RETAIL,
                                   L_prom_retail,
                                   L_prom_retail_unique,
                                   L_prom_exvatuom_retail,
                                   L_prom_exvatuom_retail_unique,
                                   L_prom_uom,
                                   L_prom_uom_unique) = FALSE then
            return 0;
         end if;

         --rely on basis and promo date being the same.

         L_promo_values_rec.cur_rtl                     := L_prom_retail;
         L_promo_values_rec.cur_rtl_unique              := L_prom_retail_unique;
         L_promo_values_rec.cur_rtl_stduom_exvat_unique := L_prom_exvatuom_retail_unique;
         L_promo_values_rec.cur_rtl_uom                 := L_prom_uom;

         if (L_markup_calc_type = 0) then
            if (L_cur_cost > 0) then
               L_promo_values_rec.cur_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_cur_cost)/L_cur_cost;
            else
               L_promo_values_rec.cur_rtl_stduom_exvat := 0;
            end if;
         else
            if (L_prom_exvatuom_retail > 0) then
               L_promo_values_rec.cur_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_cur_cost)/L_prom_exvatuom_retail;
            else
               L_promo_values_rec.cur_rtl_stduom_exvat := 0;
            end if;
         end if;

         ----------------------------------------------------------------------------
         -- basis / new retail
         ----------------------------------------------------------------------------

         if (L_obj_promo_rec.state = PROMO_APPROVED or
             L_obj_promo_rec.state = PROMO_ACTIVE) then
            --get new retail

            if L_obj_promo_rec.component_type = RPM_CONSTANTS.SIMPLE_CODE and
               L_obj_promo_rec.change_type != RPM_CONSTANTS.RETAIL_EXCLUDE then
               --

                if POPULATE_VAT(O_error_msg,
                                L_promo_date) = FALSE then
                   return 0;
                end if;

               if CALCULATE_RETAIL_QUERY(O_error_msg,
                                         L_promo_date,
                                         L_local_currency,
                                         L_obj_promo_rec.customer_type,
                                         RPM_CONSTANTS.NEW_RETAIL,
                                         L_prom_retail,
                                         L_prom_retail_unique,
                                         L_prom_exvatuom_retail,
                                         L_prom_exvatuom_retail_unique,
                                         L_prom_uom,
                                         L_prom_uom_unique) = FALSE then
                  return 0;
               end if;

               L_promo_values_rec.promo_rtl                     := L_prom_retail;
               L_promo_values_rec.promo_rtl_unique              := L_prom_retail_unique;
               L_promo_values_rec.promo_rtl_stduom_exvat_unique := L_prom_exvatuom_retail_unique;
               L_promo_values_rec.promo_rtl_uom                 := L_prom_uom;

               if (L_markup_calc_type = 0) then
                  if (L_basis_cost > 0) then
                     L_promo_values_rec.promo_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_basis_cost;
                  else
                     L_promo_values_rec.promo_rtl_stduom_exvat := 0;
                  end if;
               else
                  if (L_prom_exvatuom_retail > 0) then
                     L_promo_values_rec.promo_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_prom_exvatuom_retail;
                  else
                     L_promo_values_rec.promo_rtl_stduom_exvat := 0;
                  end if;
               end if;

               --remove event and get basis retail

               if REMOVE_PROMO(O_error_msg,
                               L_obj_promo_rec) = FALSE then
                  return 0;
               end if;

            end if;

            if LP_add_remove_promo_fail then

                  L_promo_values_rec.conflict_during_calculation := 1;
                  LP_add_remove_promo_fail := FALSE;
            else
               --
               if POPULATE_VAT(O_error_msg,
                               L_basis_date) = FALSE then
                   return 0;
                end if;

               if CALCULATE_RETAIL_QUERY(O_error_msg,
                                         L_basis_date,
                                         L_local_currency,
                                         L_obj_promo_rec.customer_type,
                                         RPM_CONSTANTS.BASIS_RETAIL,
                                         L_prom_retail,
                                         L_prom_retail_unique,
                                         L_prom_exvatuom_retail,
                                         L_prom_exvatuom_retail_unique,
                                         L_prom_uom,
                                         L_prom_uom_unique) = FALSE then
                  return 0;
               end if;

               L_promo_values_rec.basis_rtl                     := L_prom_retail;
               L_promo_values_rec.basis_rtl_unique              := L_prom_retail_unique;
               L_promo_values_rec.basis_rtl_stduom_exvat_unique := L_prom_exvatuom_retail_unique;
               L_promo_values_rec.basis_rtl_uom                 := L_prom_uom;

               if (L_markup_calc_type = 0) then
                  if (L_basis_cost > 0) then
                     L_promo_values_rec.basis_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_basis_cost;
                  else
                     L_promo_values_rec.basis_rtl_stduom_exvat := 0;
                  end if;
               else
                  if (L_prom_exvatuom_retail > 0) then
                     L_promo_values_rec.basis_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_prom_exvatuom_retail;
                  else
                     L_promo_values_rec.basis_rtl_stduom_exvat := 0;
                  end if;
               end if;

            end if;

         else -- not on future_retail

            if POPULATE_VAT(O_error_msg,
                            L_basis_date) = FALSE then
               return 0;
            end if;

            --get basis retail
            if CALCULATE_RETAIL_QUERY(O_error_msg,
                                      L_basis_date,
                                      L_local_currency,
                                      L_obj_promo_rec.customer_type,
                                      RPM_CONSTANTS.BASIS_RETAIL,
                                      L_prom_retail,
                                      L_prom_retail_unique,
                                      L_prom_exvatuom_retail,
                                      L_prom_exvatuom_retail_unique,
                                      L_prom_uom,
                                      L_prom_uom_unique) = FALSE then
               return 0;
            end if;

            L_promo_values_rec.basis_rtl                     := L_prom_retail;
            L_promo_values_rec.basis_rtl_unique              := L_prom_retail_unique;
            L_promo_values_rec.basis_rtl_stduom_exvat_unique := L_prom_exvatuom_retail_unique;
            L_promo_values_rec.basis_rtl_uom                 := L_prom_uom;

            if (L_markup_calc_type = 0) then
               if (L_basis_cost > 0) then
                  L_promo_values_rec.basis_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_basis_cost;
               else
                  L_promo_values_rec.basis_rtl_stduom_exvat := 0;
               end if;
            else
               if (L_prom_exvatuom_retail > 0) then
                  L_promo_values_rec.basis_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_prom_exvatuom_retail;
               else
                  L_promo_values_rec.basis_rtl_stduom_exvat := 0;
               end if;
            end if;

            --add event and get new retail

            if L_obj_promo_rec.component_type = RPM_CONSTANTS.SIMPLE_CODE and
               L_obj_promo_rec.change_type != RPM_CONSTANTS.RETAIL_EXCLUDE then

               if ADD_PROMO(O_error_msg,
                            L_obj_promo_rec) = FALSE then
                  return 0;
               end if;

               if LP_add_remove_promo_fail then

                  L_promo_values_rec.conflict_during_calculation := 1;
                  LP_add_remove_promo_fail := FALSE;

               else
                  --

                  if POPULATE_VAT(O_error_msg,
                                  L_promo_date) = FALSE then
                     return 0;
                  end if;

                  if CALCULATE_RETAIL_QUERY(O_error_msg,
                                            L_promo_date,
                                            L_local_currency,
                                            L_obj_promo_rec.customer_type,
                                            RPM_CONSTANTS.NEW_RETAIL,
                                            L_prom_retail,
                                            L_prom_retail_unique,
                                            L_prom_exvatuom_retail,
                                            L_prom_exvatuom_retail_unique,
                                            L_prom_uom,
                                            L_prom_uom_unique) = FALSE then
                     return 0;
                  end if;

                  L_promo_values_rec.promo_rtl                     := L_prom_retail;
                  L_promo_values_rec.promo_rtl_unique              := L_prom_retail_unique;
                  L_promo_values_rec.promo_rtl_stduom_exvat_unique := L_prom_exvatuom_retail_unique;
                  L_promo_values_rec.promo_rtl_uom                 := L_prom_uom;

                  if (L_markup_calc_type = 0) then
                     if (L_basis_cost > 0) then
                        L_promo_values_rec.promo_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_basis_cost;
                     else
                        L_promo_values_rec.promo_rtl_stduom_exvat := 0;
                     end if;
                  else
                     if (L_prom_exvatuom_retail > 0) then
                        L_promo_values_rec.promo_rtl_stduom_exvat := (L_prom_exvatuom_retail - L_basis_cost)/L_prom_exvatuom_retail;
                     else
                        L_promo_values_rec.promo_rtl_stduom_exvat := 0;
                     end if;
                  end if;

               end if;
            end if;
         end if;

         if GET_MISC_INDS(O_error_msg,
                          L_obj_promo_rec.rpm_promo_comp_detail_id,
                          L_obj_promo_rec.start_date,
                          L_obj_promo_rec.end_date,
                          L_non_regular_basis_ind,
                          L_clearance_overlap_ind,
                          L_price_change_overlap_ind,
                          L_async_conflict_error) = FALSE then
            return 0;
         end if;

         L_promo_values_rec.non_regular_basis_ind    := L_non_regular_basis_ind;
         L_promo_values_rec.is_multi_item_loc_ind    := L_is_multi_item_loc_ind;
         L_promo_values_rec.clearance_overlap_ind    := L_clearance_overlap_ind;
         L_promo_values_rec.price_change_overlap_ind := L_price_change_overlap_ind;
         L_promo_values_rec.async_conflict_error     := L_async_conflict_error;

         close C_GET_MARKUP_CALC_TYPE;

      end if;

      L_promo_values_tbl.EXTEND();
      L_promo_values_tbl(L_promo_values_tbl.COUNT) := L_promo_values_rec;

   END LOOP;

   if RPM_MARGIN_VISIBILITY_SQL.GET_MARKUP_CHANGE_DATES(O_error_msg,
                                                        I_obj_rpm_promo_tbl,
                                                        RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                        L_marg_vis_tbl) = 0 then
      return 0;
   end if;

   open C_ADD_MARKUP_CHG_DATES;
   fetch C_ADD_MARKUP_CHG_DATES BULK COLLECT into O_obj_rpm_promo_values_tbl;
   close C_ADD_MARKUP_CHG_DATES;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return 0;

END GET_BASIS_RETAIL_VALUES;

-----------------------------------------------------------------------------------------

FUNCTION REMOVE_PROMO(O_error_msg     OUT VARCHAR2,
                      I_obj_promo_rec IN     OBJ_RPM_PROMO_REC)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SQL.REMOVE_PROMO';

   L_cc_error_tbl     CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_price_event_id   NUMBER                   := NULL;
   L_price_event_ids  OBJ_NUM_NUM_DATE_TBL     := NULL;
   L_price_event_type VARCHAR2(3)              := NULL;
   CC_ERROR           EXCEPTION;

BEGIN

   L_price_event_id  := I_obj_promo_rec.rpm_promo_comp_detail_id;
   L_price_event_ids := new OBJ_NUM_NUM_DATE_TBL(
                        new OBJ_NUM_NUM_DATE_REC(L_price_event_id,
                                                 L_price_event_id,
                                                 NULL));

   if I_obj_promo_rec.component_type = RPM_CONSTANTS.SIMPLE_CODE then
      ---
      if I_obj_promo_rec.customer_type is NULL then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION;
      else
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO;
      end if;

   elsif I_obj_promo_rec.component_type = RPM_CONSTANTS.THRESHOLD_CODE then
      ---
      if I_obj_promo_rec.customer_type is NULL then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION;
      else
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO;
      end if;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(L_cc_error_tbl,
                                             L_price_event_ids,
                                             L_price_event_type) = 0 then

      O_error_msg := L_cc_error_tbl(1).error_string;
      return FALSE;
   end if;

   if RPM_ROLL_FORWARD_SQL.EXECUTE(L_cc_error_tbl,
                                   L_price_event_type) = 0 then
      raise CC_ERROR;
   end if;

   return TRUE;

EXCEPTION

   when CC_ERROR then
      if L_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then
         LP_add_remove_promo_fail := TRUE;
         return TRUE;
      else
         O_error_msg := L_cc_error_tbl(1).error_string;
         return FALSE;
      end if;

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END REMOVE_PROMO;

--------------------------------------------------------------------------------

FUNCTION ADD_PROMO(O_error_msg        OUT VARCHAR2,
                   I_obj_promo_rec IN     OBJ_RPM_PROMO_REC)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SQL.ADD_PROMO';

   L_cc_error_tbl     CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_price_event_id   NUMBER                   := NULL;
   L_price_event_type VARCHAR2(3)              := NULL;
   L_promo_rec        OBJ_RPM_CC_PROMO_REC     := NULL;
   L_promo_tbl        OBJ_RPM_CC_PROMO_TBL     := NULL;
   CC_ERROR           EXCEPTION;

BEGIN

   L_price_event_id := I_obj_promo_rec.rpm_promo_comp_detail_id;

   if I_obj_promo_rec.component_type = RPM_CONSTANTS.SIMPLE_CODE then
      ---
      if I_obj_promo_rec.customer_type is NULL then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION;
      else
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO;
      end if;

   elsif I_obj_promo_rec.component_type = RPM_CONSTANTS.THRESHOLD_CODE then
      ---
      if I_obj_promo_rec.customer_type is NULL then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION;
      else
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO;
      end if;
   end if;

   if GET_PROMOTION_OBJECT(O_error_msg,
                           I_obj_promo_rec,
                           L_promo_rec) = FALSE then
      return FALSE;
   end if;

   if VALIDATE_PROMOTION_FOR_MERGE(O_error_msg,
                                   L_promo_rec) = FALSE then
      return FALSE;
   end if;

   L_promo_tbl := NEW OBJ_RPM_CC_PROMO_TBL(L_promo_rec);

   if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PROMOTION(L_cc_error_tbl,
                                                L_promo_tbl,
                                                NULL,
                                                L_price_event_type) = 0 then
      O_error_msg := L_cc_error_tbl(1).error_string;
      return FALSE;
   end if;

   if RPM_ROLL_FORWARD_SQL.EXECUTE(L_cc_error_tbl,
                                   L_price_event_type) = 0 then
      raise CC_ERROR;
   end if;

   return TRUE;

EXCEPTION

   when CC_ERROR then
      if L_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then
         LP_add_remove_promo_fail := TRUE;
         return TRUE;
      else
         O_error_msg := L_cc_error_tbl(1).error_string;
         return FALSE;
      end if;

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                         SQLERRM,
                                         L_program,
                                         TO_CHAR(SQLCODE));
      return FALSE;

END ADD_PROMO;
--------------------------------------------------------------------------------

FUNCTION GET_PROMOTION_OBJECT(O_error_msg        OUT VARCHAR2,
                              I_obj_promo_rec IN     OBJ_RPM_PROMO_REC,
                              O_promo_rec        OUT OBJ_RPM_CC_PROMO_REC)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RPM_PROMO_SQL.GET_PROMOTION_OBJECT';

BEGIN

   O_promo_rec := OBJ_RPM_CC_PROMO_REC(I_obj_promo_rec.promotion_id,
                                       NULL,
                                       I_obj_promo_rec.promo_secondary_ind,
                                       I_obj_promo_rec.component_id,
                                       NULL,
                                       I_obj_promo_rec.promo_comp_secondary_ind,
                                       I_obj_promo_rec.rpm_promo_comp_detail_id,
                                       I_obj_promo_rec.start_date,
                                       I_obj_promo_rec.end_date,
                                       NULL,
                                       NULL,
                                       I_obj_promo_rec.apply_to_code,
                                       I_obj_promo_rec.timebased_dtl_ind,
                                       OBJ_NUMERIC_ID_TABLE(I_obj_promo_rec.zone_id),
                                       I_obj_promo_rec.change_type,
                                       I_obj_promo_rec.change_amount,
                                       I_obj_promo_rec.change_currency,
                                       I_obj_promo_rec.change_percent,
                                       I_obj_promo_rec.change_selling_uom,
                                       I_obj_promo_rec.price_guide_id,
                                       NULL,
                                       I_obj_promo_rec.component_type,
                                       NULL,
                                       I_obj_promo_rec.customer_type);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END GET_PROMOTION_OBJECT;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_PROMOTION_FOR_MERGE(O_error_msg    OUT VARCHAR2,
                                      I_promo_rec IN     OBJ_RPM_CC_PROMO_REC)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_PROMO_SQL.VALIDATE_PROMOTION_FOR_MERGE';

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   CC_ERROR       EXCEPTION;

BEGIN

   if RPM_CC_PROM_SPAN_LM.VALIDATE(L_cc_error_tbl,
                                   I_promo_rec) = 0 then
      raise CC_ERROR;
   end if;

   return TRUE;

EXCEPTION

   when CC_ERROR then

      if L_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then
         LP_add_remove_promo_fail := TRUE;
         return TRUE;
      else
         O_error_msg := L_cc_error_tbl(1).error_string;
         return FALSE;
      end if;

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END VALIDATE_PROMOTION_FOR_MERGE;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_FP_ITEM_EXCLUSION(O_error_msg         IN OUT VARCHAR2,
                                    O_merch_nodes          OUT OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                    I_merch_nodes       IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                    I_merch_nodes_excl  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL         )
RETURN NUMBER IS

   L_program                 VARCHAR2(100) := 'RPM_PROMO_SQL.VALIDATE_FP_ITEM_EXCLUSION';

   L_merch_node_count        NUMBER;
   L_dept                    NUMBER(4);
   L_class                   NUMBER(4);
   L_subclass                NUMBER(4);
   L_item                    VARCHAR2(25);
   L_diff_id                 VARCHAR2(10);
   L_dummy                   NUMBER;
   L_tran_level              NUMBER;

   cursor C_DEPT is
      select count(1)
        from dual
       where exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and t.dept = L_dept)
         and L_merch_node_count > 1;

   cursor C_CLASS is
      select count(1)
        from dual
       where (exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                         and t.dept = L_dept
                         and t.class = L_class)
          and L_merch_node_count > 1)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and t.dept = L_dept);

   cursor C_SUBCLASS is
      select count(1)
        from dual
       where (exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                         and t.dept = L_dept
                         and t.class = L_class
                         and t.subclass = L_subclass)
          and L_merch_node_count > 1)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and t.dept = L_dept)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        and t.dept = L_dept
                        and t.class = L_class);

   cursor C_ITEM_LEVEL is
      select count(1)
        from item_master im
       where im.item = L_item
         and im.tran_level = im.item_level;

   cursor C_TRAN_ITEM is
      select count(1)
        from dual
       where (exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and t.item = L_item)
          and L_merch_node_count > 1)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and t.dept = L_dept)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        and t.dept = L_dept
                        and t.class = L_class)
          or exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                         and t.dept = L_dept
                         and t.class = L_class
                         and t.subclass = L_subclass)
          or exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
                             item_master im
                       where t.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and im.item = L_item
                         and im.item_parent = t.item)
          or exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t,
                             item_master im
                       where t.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and im.item = L_item
                         and im.item_parent = t.item
                         and (   im.diff_1 = t.diff_id
                          or im.diff_2 = t.diff_id
                          or im.diff_3 = t.diff_id
                          or im.diff_4 = t.diff_id));

   cursor C_PARENT_ITEM is
      select count(1)
        from dual
       where (exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and t.item = L_item)
          and L_merch_node_count > 1)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and t.dept = L_dept)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        and t.dept = L_dept
                        and t.class = L_class)
          or exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                         and t.dept = L_dept
                         and t.class = L_class
                         and t.subclass = L_subclass);

   cursor C_PARENT_DIFF is
      select count(1)
        from dual
       where (exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and t.item = L_item
                         and t.diff_id = L_diff_id)
          and L_merch_node_count > 1)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and t.dept = L_dept)
          or exists (select 1
                       from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                      where t.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        and t.dept = L_dept
                        and t.class = L_class)
          or exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                         and t.dept = L_dept
                         and t.class = L_class
                         and t.subclass = L_subclass)
          or exists (select 1
                        from table(cast (I_merch_nodes as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
                       where t.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and t.item = L_item);

BEGIN
   -- Since the Selected Merchandise Node and the Exclusion can be at any level, Casting the collection
   -- will nmot work very well, so we need to loop through the Exclusion Collection to validate it

   O_merch_nodes := NEW OBJ_MERCH_ND_ZONE_ND_DATE_TBL();

   if I_merch_nodes is NOT NULL and
      I_merch_nodes.count > 0 then
      --
      L_merch_node_count := I_merch_nodes.count;
      --
      if I_merch_nodes_excl is NOT NULL and
         I_merch_nodes_excl.count > 0 then
         --
         for I in 1..I_merch_nodes_excl.count loop
            if I_merch_nodes_excl(I).merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM then
               L_dept := I_merch_nodes_excl(I).dept;
               --
               open C_DEPT;
               fetch C_DEPT
                into L_dummy;
               close C_DEPT;
               --
               if L_dummy = 0 then
                  O_merch_nodes.extend();
                  O_merch_nodes(O_merch_nodes.count) := I_merch_nodes_excl(I);
               end if;
               --
            elsif I_merch_nodes_excl(I).merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM then
               L_dept  := I_merch_nodes_excl(I).dept;
               L_class := I_merch_nodes_excl(I).class;
               --
               open C_CLASS;
               fetch C_CLASS
                into L_dummy;
               close C_CLASS;
               --
               if L_dummy = 0 then
                  O_merch_nodes.extend();
                  O_merch_nodes(O_merch_nodes.count) := I_merch_nodes_excl(I);
               end if;
               --
            elsif I_merch_nodes_excl(I).merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
               L_dept  := I_merch_nodes_excl(I).dept;
               L_class := I_merch_nodes_excl(I).class;
               L_subclass := I_merch_nodes_excl(I).subclass;
               --
               open C_SUBCLASS;
               fetch C_SUBCLASS
                into L_dummy;
               close C_SUBCLASS;
               --
               if L_dummy = 0 then
                  O_merch_nodes.extend();
                  O_merch_nodes(O_merch_nodes.count) := I_merch_nodes_excl(I);
               end if;
               --
            elsif I_merch_nodes_excl(I).merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM then
               --
               L_dept  := I_merch_nodes_excl(I).dept;
               L_class := I_merch_nodes_excl(I).class;
               L_subclass := I_merch_nodes_excl(I).subclass;
               L_item := I_merch_nodes_excl(I).item;
               --
               open C_ITEM_LEVEL;
               fetch C_ITEM_LEVEL
                into L_tran_level;
               close C_ITEM_LEVEL;
               --
               if L_tran_level = 1 then
               --
                  open C_TRAN_ITEM;
                  fetch C_TRAN_ITEM
                   into L_dummy;
                  close C_TRAN_ITEM;
                  --
                  if L_dummy = 0 then
                     O_merch_nodes.extend();
                     O_merch_nodes(O_merch_nodes.count) := I_merch_nodes_excl(I);
                  end if;
               --
               else
               --
                  open C_PARENT_ITEM;
                  fetch C_PARENT_ITEM
                   into L_dummy;
                  close C_PARENT_ITEM;
                  --
                  if L_dummy = 0 then
                     O_merch_nodes.extend();
                     O_merch_nodes(O_merch_nodes.count) := I_merch_nodes_excl(I);
                  end if;
               --
               end if;
            elsif I_merch_nodes_excl(I).merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF then
               --
               L_dept  := I_merch_nodes_excl(I).dept;
               L_class := I_merch_nodes_excl(I).class;
               L_subclass := I_merch_nodes_excl(I).subclass;
               L_item := I_merch_nodes_excl(I).item;
               L_diff_id := I_merch_nodes_excl(I).diff_id;
               --
               open C_PARENT_DIFF;
               fetch C_PARENT_DIFF
                into L_dummy;
               close C_PARENT_DIFF;
               --
               if L_dummy = 0 then
                  O_merch_nodes.extend();
                  O_merch_nodes(O_merch_nodes.count) := I_merch_nodes_excl(I);
               end if;
               --
            end if;
         end loop;
         --
      end if;
      --
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;

END VALIDATE_FP_ITEM_EXCLUSION;
-----------------------------------------------------------------------------------------

FUNCTION POPULATE_GTT(O_error_msg        OUT VARCHAR2,
                      I_obj_promo_rec IN     OBJ_RPM_PROMO_REC,
                      I_query_date    IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_PROMO_SQL.POPULATE_GTT';

   L_obj_rpm_promo_tbl OBJ_RPM_PROMO_TBL := NEW OBJ_RPM_PROMO_TBL(I_obj_promo_rec);

BEGIN

   delete rpm_price_inquiry_gtt;
   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 location,
                                 dept,
                                 diff_id)
      select rpet_item.rpm_promo_comp_detail_id,
             rpet_item.item,
             rpet_loc.location,
             rpet_item.dept,
             rpet_item.diff_id
       from (select /*+ INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    item_master im
              where rpet.merch_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                and im.item         = rpet.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Item Parent Level
             select /*+ INDEX (im ITEM_MASTER_I1) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    item_master im
              where rpet.merch_type = RPM_CONSTANTS.PARENT_MERCH_TYPE
                and im.item_parent  = rpet.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Parent Diff Level
             select /*+ INDEX (im ITEM_MASTER_I1) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    rpet.diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    item_master im
              where rpet.merch_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                and im.item_parent  = rpet.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                and (   im.diff_1 = rpet.diff_id
                     or im.diff_2 = rpet.diff_id
                     or im.diff_3 = rpet.diff_id
                     or im.diff_4 = rpet.diff_id)
             union all
             -- Merch Hierarchy Level
             select /*+ INDEX (im ITEM_MASTER_I3) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    item_master im
              where rpet.merch_type IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                        RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                        RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
                and im.dept         = rpet.dept
                and im.class        = NVL(rpet.class, im.class)
                and im.subclass     = NVL(rpet.subclass, im.subclass)
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Item list - tran item level
             select /*+ INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    rpm_promo_dtl_skulist pdsl,
                    item_master im
              where rpet.merch_type               = RPM_CONSTANTS.ITEM_LIST_TYPE
                and rpet.skulist                  = pdsl.skulist
                and rpet.rpm_promo_comp_detail_id = pdsl.price_event_id
                and pdsl.item_level               = RPM_CONSTANTS.IL_ITEM_LEVEL
                and im.item                       = pdsl.item
                and im.item_level                 = im.tran_level
                and im.status                     = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind               = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Item Lists - parent item level
             select /*+ INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    rpm_promo_dtl_skulist pdsl,
                    item_master im
              where rpet.merch_type               = RPM_CONSTANTS.ITEM_LIST_TYPE
                and rpet.skulist                  = pdsl.skulist
                and rpet.rpm_promo_comp_detail_id = pdsl.price_event_id
                and pdsl.item_level               = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                and im.item_parent                = pdsl.item
                and im.item_level                 = im.tran_level
                and im.status                     = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind               = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Item list - tran item level - Get it from SKULIST_DETAIL when the promotion detail is not saved yet
             select /*+ ORDERED INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    skulist_detail sd,
                    item_master im
              where rpet.merch_type = RPM_CONSTANTS.ITEM_LIST_TYPE
                and rpet.skulist    = sd.skulist
                and sd.item_level   = sd.tran_level
                and im.item         = sd.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                and NOT EXISTS (select 1
                                  from rpm_promo_dtl_skulist pdsl
                                 where pdsl.skulist        = rpet.skulist
                                   and pdsl.price_event_id = rpet.rpm_promo_comp_detail_id)
             union all
             -- Item Lists - parent item level - Get it from SKULIST_DETAIL when the promotion detail is not saved yet
             select /*+ ORDERED INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    skulist_detail sd,
                    item_master im
              where rpet.merch_type = RPM_CONSTANTS.ITEM_LIST_TYPE
                and rpet.skulist    = sd.skulist
                and sd.item_level   = sd.tran_level - 1
                and im.item_parent  = sd.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                and NOT EXISTS (select 1
                                  from rpm_promo_dtl_skulist pdsl
                                 where pdsl.skulist        = rpet.skulist
                                   and pdsl.price_event_id = rpet.rpm_promo_comp_detail_id)
             union all
             -- Price event item list (PEIL)- tran item level
             select /*+ INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    rpm_merch_list_detail rmld,
                    item_master im
              where rpet.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                and rmld.merch_level          = RPM_CONSTANTS.ITEM_MERCH_TYPE
                and rpet.price_event_itemlist = rmld.merch_list_id
                and im.item                   = rmld.item
                and im.item_level             = im.tran_level
                and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Price event item list (PEIL) - parent item level
             select /*+ INDEX (im PK_ITEM_MASTER) CARDINALITY(rpet 10)*/
                    rpet.rpm_promo_comp_detail_id,
                    im.item,
                    im.dept,
                    NULL diff_id
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    rpm_merch_list_detail rmld,
                    item_master im
              where rpet.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                and rmld.merch_level          = RPM_CONSTANTS.PARENT_MERCH_TYPE
                and rpet.price_event_itemlist = rmld.merch_list_id
                and im.item_parent            = rmld.item
                and im.item_level             = im.tran_level
                and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) rpet_item,
            (select /*+ CARDINALITY(rpet 10) */
                    rpet.rpm_promo_comp_detail_id,
                    rpet.location
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet
              where rpet.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
             union all
             select /*+ HASH(rpet, rzl) CARDINALITY(rpet 10) */
                    rpet.rpm_promo_comp_detail_id,
                    rzl.location
               from table(cast(L_obj_rpm_promo_tbl as OBJ_RPM_PROMO_TBL)) rpet,
                    rpm_zone_location rzl
              where rpet.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                and rzl.zone_id         = rpet.zone_id
                and rzl.loc_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE) rpet_loc
      where rpet_item.rpm_promo_comp_detail_id = rpet_loc.rpm_promo_comp_detail_id;

   insert into rpm_price_inquiry_gtt (price_event_id,
                                      item,
                                      item_parent,
                                      diff_id,
                                      dept,
                                      location,
                                      fr_zone_node_type,
                                      fr_zone_id)
      select s.price_event_id,
             s.item,
             s.item_parent,
             s.diff_id,
             s.dept,
             s.location,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             s.zone_id
        from (select t.price_event_id,
                     t.item,
                     t.item_parent,
                     t.diff_id,
                     t.dept,
                     t.location,
                     rzl.zone_id,
                     ROW_NUMBER() OVER (PARTITION BY t.price_event_id,
                                                     t.item,
                                                     t.location
                                            ORDER BY NVL(rzl.zone_id, -9999999999) desc) rank
                from (select /*+ ORDERED */
                             rilg.price_event_id,
                             rilg.item,
                             im.item_parent,
                             im.dept,
                             rilg.diff_id,
                             rilg.location,
                             rz.zone_id
                        from rpm_item_loc_gtt rilg,
                             item_master im,
                             rpm_merch_retail_def_expl mer,
                             rpm_zone rz
                       where im.item          = rilg.item
                         and mer.dept         = im.dept
                         and mer.class        = im.class
                         and mer.subclass     = im.subclass
                         and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                         and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                         and rz.zone_group_id = mer.regular_zone_group
                         and rownum           > 0) t,
                     rpm_zone_location rzl
               where rzl.zone_id (+)  = t.zone_id
                 and rzl.location (+) = t.location) s
       where s.rank = 1;

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      zone_node_type,
                                      location,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      item_parent,
                                      diff_id,
                                      zone_id,
                                      cur_hier_level,
                                      max_hier_level)
      select s.price_event_id,
             s.future_retail_id,
             s.dept,
             s.class,
             s.subclass,
             s.item,
             s.zone_node_type,
             s.location,
             s.action_date,
             s.selling_retail,
             s.selling_retail_currency,
             s.selling_uom,
             s.multi_units,
             s.multi_unit_retail,
             s.multi_unit_retail_currency,
             s.multi_selling_uom,
             s.clear_retail,
             s.clear_retail_currency,
             s.clear_uom,
             s.simple_promo_retail,
             s.simple_promo_retail_currency,
             s.simple_promo_uom,
             s.price_change_id,
             s.price_change_display_id,
             s.pc_exception_parent_id,
             s.pc_change_type,
             s.pc_change_amount,
             s.pc_change_currency,
             s.pc_change_percent,
             s.pc_change_selling_uom,
             s.pc_null_multi_ind,
             s.pc_multi_units,
             s.pc_multi_unit_retail,
             s.pc_multi_unit_retail_currency,
             s.pc_multi_selling_uom,
             s.pc_price_guide_id,
             s.clearance_id,
             s.clearance_display_id,
             s.clear_mkdn_index,
             s.clear_start_ind,
             s.clear_change_type,
             s.clear_change_amount,
             s.clear_change_currency,
             s.clear_change_percent,
             s.clear_change_selling_uom,
             s.clear_price_guide_id,
             s.loc_move_from_zone_id,
             s.loc_move_to_zone_id,
             s.location_move_id,
             s.lock_version,
             s.on_simple_promo_ind,
             s.on_complex_promo_ind,
             s.item_parent,
             s.diff_id,
             s.zone_id,
             s.cur_hier_level,
             s.max_hier_level
        from (select t.price_event_id,
                     fr.future_retail_id,
                     fr.dept,
                     fr.class,
                     fr.subclass,
                     t.item,
                     t.fr_zone_node_type zone_node_type,
                     t.location,
                     fr.action_date,
                     fr.selling_retail,
                     fr.selling_retail_currency,
                     fr.selling_uom,
                     fr.multi_units,
                     fr.multi_unit_retail,
                     fr.multi_unit_retail_currency,
                     fr.multi_selling_uom,
                     fr.clear_retail,
                     fr.clear_retail_currency,
                     fr.clear_uom,
                     fr.simple_promo_retail,
                     fr.simple_promo_retail_currency,
                     fr.simple_promo_uom,
                     fr.price_change_id,
                     fr.price_change_display_id,
                     fr.pc_exception_parent_id,
                     fr.pc_change_type,
                     fr.pc_change_amount,
                     fr.pc_change_currency,
                     fr.pc_change_percent,
                     fr.pc_change_selling_uom,
                     fr.pc_null_multi_ind,
                     fr.pc_multi_units,
                     fr.pc_multi_unit_retail,
                     fr.pc_multi_unit_retail_currency,
                     fr.pc_multi_selling_uom,
                     fr.pc_price_guide_id,
                     fr.clearance_id,
                     fr.clearance_display_id,
                     fr.clear_mkdn_index,
                     fr.clear_start_ind,
                     fr.clear_change_type,
                     fr.clear_change_amount,
                     fr.clear_change_currency,
                     fr.clear_change_percent,
                     fr.clear_change_selling_uom,
                     fr.clear_price_guide_id,
                     fr.loc_move_from_zone_id,
                     fr.loc_move_to_zone_id,
                     fr.location_move_id,
                     fr.lock_version,
                     fr.on_simple_promo_ind,
                     fr.on_complex_promo_ind,
                     fr.item_parent,
                     fr.diff_id,
                     t.fr_zone_id zone_id,
                     RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                     fr.max_hier_level,
                     RANK() OVER (PARTITION BY t.price_event_id,
                                               t.item,
                                               t.location,
                                               fr.action_date
                                      ORDER BY DECODE(fr.cur_hier_level,
                                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC, 1,
                                                      RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 2,
                                                      RPM_CONSTANTS.FR_HIER_DIFF_LOC, 3,
                                                      RPM_CONSTANTS.FR_HIER_PARENT_LOC, 4,
                                                      RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 5,
                                                      RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 8)) rank
                from rpm_price_inquiry_gtt t,
                     rpm_future_retail fr
               where fr.dept         = t.dept
                 and fr.action_date <= I_query_date
                 and (   (    fr.item           = t.item
                          and fr.location       = t.location
                          and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
                      or (    fr.item           = t.item
                          and fr.location       = t.fr_zone_id
                          and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE)
                      or (    fr.item           = t.item_parent
                          and fr.diff_id        = t.diff_id
                          and fr.location       = t.location
                          and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
                      or (    fr.item           = t.item_parent
                          and fr.location       = t.location
                          and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC)
                      or (    fr.item           = t.item_parent
                          and fr.diff_id        = t.diff_id
                          and fr.location       = t.fr_zone_id
                          and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE)
                      or (    fr.item           = t.item_parent
                          and fr.location       = t.fr_zone_id
                          and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE))) s
       where s.rank = 1;

   insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                            promo_item_loc_expl_id,
                                            item,
                                            dept,
                                            class,
                                            subclass,
                                            location,
                                            promo_id,
                                            promo_display_id,
                                            promo_secondary_ind,
                                            promo_comp_id,
                                            comp_display_id,
                                            promo_dtl_id,
                                            type,
                                            customer_type,
                                            detail_secondary_ind,
                                            detail_start_date,
                                            detail_end_date,
                                            detail_apply_to_code,
                                            timebased_dtl_ind,
                                            detail_change_type,
                                            detail_change_amount,
                                            detail_change_currency,
                                            detail_change_percent,
                                            detail_change_selling_uom,
                                            detail_price_guide_id,
                                            exception_parent_id,
                                            zone_node_type,
                                            item_parent,
                                            diff_id,
                                            zone_id,
                                            max_hier_level,
                                            cur_hier_level)
      select s.price_event_id,
             s.promo_item_loc_expl_id,
             s.item,
             s.dept,
             s.class,
             s.subclass,
             s.location,
             s.promo_id,
             s.promo_display_id,
             s.promo_secondary_ind,
             s.promo_comp_id,
             s.comp_display_id,
             s.promo_dtl_id,
             s.type,
             s.customer_type,
             s.detail_secondary_ind,
             s.detail_start_date,
             s.detail_end_date,
             s.detail_apply_to_code,
             s.timebased_dtl_ind,
             s.detail_change_type,
             s.detail_change_amount,
             s.detail_change_currency,
             s.detail_change_percent,
             s.detail_change_selling_uom,
             s.detail_price_guide_id,
             s.exception_parent_id,
             s.zone_node_type,
             s.item_parent,
             s.diff_id,
             s.zone_id,
             s.max_hier_level,
             s.cur_hier_level
        from (select distinct t.price_event_id,
                     rpile.promo_item_loc_expl_id,
                     t.item,
                     rpile.dept,
                     rpile.class,
                     rpile.subclass,
                     t.location,
                     rpile.promo_id,
                     rpile.promo_display_id,
                     rpile.promo_secondary_ind,
                     rpile.promo_comp_id,
                     rpile.comp_display_id,
                     rpile.promo_dtl_id,
                     rpile.type,
                     rpile.customer_type,
                     rpile.detail_secondary_ind,
                     rpile.detail_start_date,
                     rpile.detail_end_date,
                     rpile.detail_apply_to_code,
                     rpile.timebased_dtl_ind,
                     rpile.detail_change_type,
                     rpile.detail_change_amount,
                     rpile.detail_change_currency,
                     rpile.detail_change_percent,
                     rpile.detail_change_selling_uom,
                     rpile.detail_price_guide_id,
                     rpile.exception_parent_id,
                     rpile.zone_node_type,
                     rpile.item_parent,
                     rpile.diff_id,
                     rpile.zone_id,
                     rpile.max_hier_level,
                     RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                     RANK() OVER (PARTITION BY rpile.promo_dtl_id,
                                               t.item,
                                               t.location,
                                               rpile.detail_start_date
                                      ORDER BY DECODE(rpile.cur_hier_level, RPM_CONSTANTS.FR_HIER_ITEM_LOC, 1,
                                                                            RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 2,
                                                                            RPM_CONSTANTS.FR_HIER_DIFF_LOC, 3,
                                                                            RPM_CONSTANTS.FR_HIER_PARENT_LOC, 4,
                                                                            RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 5,
                                                                            RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 8)) rank
                from rpm_price_inquiry_gtt t,
                     rpm_promo_item_loc_expl rpile
               where I_obj_promo_rec.customer_type is NULL
                 and rpile.dept                    = t.dept
                 and (   (    rpile.item           = t.item
                          and rpile.location       = t.location
                          and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC)
                      or (    rpile.item           = t.item
                          and rpile.location       = t.fr_zone_id
                          and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE)
                      or (    rpile.item           = t.item_parent
                          and rpile.diff_id        = t.diff_id
                          and rpile.location       = t.location
                          and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
                      or (    rpile.item           = t.item_parent
                          and rpile.location       = t.location
                          and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC)
                      or (    rpile.item           = t.item_parent
                          and rpile.diff_id        = t.diff_id
                          and rpile.location       = t.fr_zone_id
                          and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC)
                      or (    rpile.item           = t.item_parent
                          and rpile.location       = t.fr_zone_id
                          and rpile.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE))) s
       where rank = 1;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := O_error_msg ||SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                      SQLERRM,
                                                      L_program,
                                                      TO_CHAR(SQLCODE));
      return 0;

END POPULATE_GTT;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_IL_FOR_CANCEL(O_error_message                 OUT VARCHAR2,
                                O_display_participate_msg       OUT NUMBER,
                                O_valid_merch_nd_zone_nd        OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                                I_promo_dtl_id               IN     NUMBER,
                                I_merch_nd_zone_nd_to_check  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                I_curr_merch_nd_zone_nd_data IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER IS

   L_program  VARCHAR2(50)  := 'RPM_PROMO_SQL.VALIDATE_IL_FOR_CANCEL';

   L_participation_check  NUMBER  := 0;

   cursor C_CHECK_PARTICIPATE is
      select COUNT(*)
        from (select zone_node_type,
                     location,
                     zone_id,
                     merch_type,
                     dept,
                     class,
                     subclass,
                     item,
                     diff_id,
                     skulist
                from rpm_cancel_promo_mn_zn_gtt
               where rownum > 0
              minus
              select zone_node_type,
                     location,
                     zone_id,
                     merch_type,
                     dept,
                     class,
                     subclass,
                     item,
                     diff_id,
                     skulist
                from table(cast(O_valid_merch_nd_zone_nd AS OBJ_MERCH_ZONE_ND_DESCS_TBL)) t
               where rownum > 0);

BEGIN

   O_display_participate_msg := 0;

   if POP_GTT_FOR_IL_CANCEL_APPLY(O_error_message,
                                  I_promo_dtl_id,
                                  I_merch_nd_zone_nd_to_check,
                                  I_curr_merch_nd_zone_nd_data) = 0 then
      return 0;
   end if;

   if I_curr_merch_nd_zone_nd_data is NOT NULL and
      I_curr_merch_nd_zone_nd_data.COUNT > 0 then

      if CHECK_CANCEL_INPUT_OVERLAPS(O_error_message) = 0 then
         return 0;
      end if;

   end if;

   if CHECK_SIBLINGS_FOR_CANCEL(O_error_message,
                                O_valid_merch_nd_zone_nd,
                                I_promo_dtl_id) = 0 then
      return 0;
   end if;

   open C_CHECK_PARTICIPATE;
   fetch C_CHECK_PARTICIPATE into L_participation_check;
   close C_CHECK_PARTICIPATE;

   if L_participation_check > 0 then
      O_display_participate_msg := 1;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_IL_FOR_CANCEL;

--------------------------------------------------------------------------------

FUNCTION POP_GTT_FOR_IL_CANCEL_APPLY(O_error_message                 OUT VARCHAR2,
                                     I_promo_dtl_id               IN     NUMBER,
                                     I_merch_nd_zone_nd_to_check  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                                     I_curr_merch_nd_zone_nd_data IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_PROMO_SQL.POP_GTT_FOR_IL_CANCEL_APPLY';

BEGIN

   delete from rpm_cancel_promo_mn_zn_gtt;
   delete from rpm_cancel_promo_mnzn_expl_gtt;

   insert into rpm_cancel_promo_mn_zn_gtt (id,
                                           input_type,
                                           zone_node_type,
                                           location,
                                           zone_id,
                                           merch_type,
                                           dept,
                                           class,
                                           subclass,
                                           item,
                                           diff_id,
                                           skulist,
                                           price_event_itemlist)
      select /*+ CARDINALITY(t 10) */
             rownum,
             1,
             zone_node_type,
             location,
             zone_id,
             merch_type,
             dept,
             class,
             subclass,
             item,
             diff_id,
             skulist,
             price_event_itemlist
        from table(cast(I_merch_nd_zone_nd_to_check AS OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t
      union all
      select /*+ CARDINALITY(t 10) */
             rownum,
             2,
             zone_node_type,
             location,
             zone_id,
             merch_type,
             dept,
             class,
             subclass,
             item,
             diff_id,
             skulist,
             price_event_itemlist
        from table(cast(I_curr_merch_nd_zone_nd_data AS OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) t2;

   -- remove any "to_check" data where it is exactly the same as a record
   -- that has already been applied - "curr"

   delete
     from rpm_cancel_promo_mn_zn_gtt gtt1
    where gtt1.input_type = 1
      and EXISTS (select 'x'
                    from rpm_cancel_promo_mn_zn_gtt gtt2
                   where input_type                           = 2
                     and gtt1.merch_type                      = gtt2.merch_type
                     and gtt1.zone_node_type                  = gtt2.zone_node_type
                     and NVL(gtt1.dept, -999)                 = NVL(gtt2.dept, -999)
                     and NVL(gtt1.class, -999)                = NVL(gtt2.class, -999)
                     and NVL(gtt1.subclass, -999)             = NVL(gtt2.subclass, -999)
                     and NVL(gtt1.item, -999)                 = NVL(gtt2.item, -999)
                     and NVL(gtt1.diff_id, -999)              = NVL(gtt2.diff_id, -999)
                     and NVL(gtt1.skulist, -999)              = NVL(gtt2.skulist, -999)
                     and NVL(gtt1.price_event_itemlist, -999) = NVL(gtt2.price_event_itemlist, -999)
                     and NVL(gtt1.location, -999)             = NVL(gtt2.location, -999)
                     and NVL(gtt1.zone_id, -999)              = NVL(gtt2.zone_id, -999));

   merge into rpm_cancel_promo_mn_zn_gtt target
   using (select im.item,
                 im.dept,
                 im.class,
                 im.subclass,
                 gtt.rowid gtt_rowid
            from rpm_cancel_promo_mn_zn_gtt gtt,
                 item_master im
           where gtt.merch_type IN (RPM_CONSTANTS.PARENT_MERCH_TYPE,
                                    RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
                                    RPM_CONSTANTS.ITEM_MERCH_TYPE)
             and gtt.item       = im.item) source
   on (target.rowid = source.gtt_rowid)
   when MATCHED then
      update
         set target.dept     = source.dept,
             target.class    = source.class,
             target.subclass = source.subclass;

   insert into rpm_cancel_promo_mnzn_expl_gtt
      (id,
       expl_item,
       expl_location,
       input_type,
       zone_node_type,
       location,
       zone_id,
       merch_type,
       dept,
       class,
       subclass,
       item,
       diff_id,
       skulist,
       price_event_itemlist)
      select expl_il.id,
             expl_il.expl_item,
             expl_il.expl_location,
             expl_il.input_type,
             expl_il.zone_node_type,
             expl_il.location,
             expl_il.zone_id,
             expl_il.merch_type,
             expl_il.dept,
             expl_il.class,
             expl_il.subclass,
             expl_il.item,
             expl_il.diff_id,
             expl_il.skulist,
             expl_il.price_event_itemlist
        from (-- item loc level
              select t.id,
                     t.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     item_master im
               where t.merch_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and im.item          = t.item
              union all
              -- item zone level
              select t.id,
                     t.item expl_item,
                     rzl.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_zone_location rzl,
                     item_master im
               where t.merch_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and t.zone_id        = rzl.zone_id
                 and im.item          = t.item
              union all
              -- parent item location
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     item_master im
               where t.merch_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and t.item           = im.item_parent
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- parent item zone level
              select t.id,
                     im.item expl_item,
                     rzl.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_zone_location rzl,
                     item_master im
               where t.merch_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and t.zone_id        = rzl.zone_id
                 and t.item           = im.item_parent
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- parent-diff loc level
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     item_master im
               where t.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and t.item           = im.item_parent
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level    = im.tran_level
                 and t.diff_id        IN (im.diff_1,
                                          im.diff_2,
                                          im.diff_3,
                                          im.diff_4)
              union all
              -- parent-diff zone level
              select t.id,
                     im.item expl_item,
                     rzl.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_zone_location rzl,
                     item_master im
               where t.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and t.zone_id        = rzl.zone_id
                 and t.item           = im.item_parent
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level    = im.tran_level
                 and t.diff_id        IN (im.diff_1,
                                          im.diff_2,
                                          im.diff_3,
                                          im.diff_4)
              union all
              -- Dept/Class/subclass loc level
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     item_master im
               where t.merch_type     IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                          RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                          RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and im.dept          = t.dept
                 and im.class         = NVL(t.class, im.class)
                 and im.subclass      = NVL(t.subclass, im.subclass)
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level    = im.tran_level
              union all
              -- Class/subclass zone level - it is not possible to have a Dept/Zone combo for this functionality
              select t.id,
                     im.item expl_item,
                     rzl.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_zone_location rzl,
                     item_master im
               where t.merch_type     IN (RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                          RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
                 and t.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rzl.zone_id      = t.zone_id
                 and im.dept          = t.dept
                 and im.class         = t.class
                 and im.subclass      = NVL(t.subclass, im.subclass)
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level    = im.tran_level
              union all
              -- itemlist loc level - parent items
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_promo_dtl_skulist rpds,
                     item_master im
               where t.merch_type        = RPM_CONSTANTS.ITEM_LIST_TYPE
                 and t.zone_node_type    = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpds.price_event_id = I_promo_dtl_id
                 and rpds.skulist        = t.skulist
                 and rpds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rpds.item
                 and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level       = im.tran_level
              union all
              -- itemlist loc level - tran items
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_promo_dtl_skulist rpds,
                     item_master im
               where t.merch_type        = RPM_CONSTANTS.ITEM_LIST_TYPE
                 and t.zone_node_type    = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rpds.price_event_id = I_promo_dtl_id
                 and rpds.skulist        = t.skulist
                 and rpds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rpds.item
                 and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level       = im.tran_level
              union all
              -- Price Event Item List (PEIL) loc level - parent items
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_merch_list_detail rmld,
                     item_master im
               where t.merch_type       = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and t.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rmld.merch_list_id = t.price_event_itemlist
                 and im.item_parent     = rmld.item
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level      = im.tran_level
              union all
              -- Price Event Item List (PEIL) loc level - tran items
              select t.id,
                     im.item expl_item,
                     t.location expl_location,
                     t.input_type,
                     t.zone_node_type,
                     t.location,
                     t.zone_id,
                     t.merch_type,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.diff_id,
                     t.skulist,
                     t.price_event_itemlist,
                     im.dept expl_item_dept
                from rpm_cancel_promo_mn_zn_gtt t,
                     rpm_merch_list_detail rmld,
                     item_master im
               where t.merch_type       = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
                 and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and t.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and rmld.merch_list_id = t.price_event_itemlist
                 and im.item            = rmld.item
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level      = im.tran_level) expl_il,
              rpm_item_loc ril
       where ril.dept = expl_il.expl_item_dept
         and ril.item = expl_il.expl_item
         and ril.loc  = expl_il.expl_location;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END POP_GTT_FOR_IL_CANCEL_APPLY;

--------------------------------------------------------------------------------

FUNCTION CHECK_CANCEL_INPUT_OVERLAPS(O_error_message    OUT VARCHAR2)
RETURN NUMBER IS

   L_program  VARCHAR2(45) := 'RPM_PROMO_SQL.CHECK_CANCEL_INPUT_OVERLAPS';

   L_previous_to_check_input_type  NUMBER(1)  := 0;
   L_previous_to_check_id          NUMBER(10) := 0;

   L_previous_applied_input_type  NUMBER(1)  := 0;
   L_previous_applied_id          NUMBER(10) := 0;

   cursor C_OVERLAPS is
      with to_check as
         (select id,
                 expl_item,
                 expl_location,
                 input_type,
                 zone_node_type,
                 merch_type
            from rpm_cancel_promo_mnzn_expl_gtt
           where input_type = 1),
      applied as
         (select id,
                 expl_item,
                 expl_location,
                 input_type,
                 zone_node_type,
                 merch_type
            from rpm_cancel_promo_mnzn_expl_gtt
           where input_type = 2)
      select to_check.id                  to_check_id,
             to_check.input_type          to_check_input_type,
             to_check.zone_node_type      to_check_zone_node_type,
             to_check.merch_type          to_check_merch_type,
             applied.id                   applied_id,
             applied.input_type           applied_input_type,
             applied.zone_node_type       applied_zone_node_type,
             applied.merch_type           applied_merch_type,
             DECODE(to_check.merch_type,
                    RPM_CONSTANTS.ITEM_LIST_TYPE, 0,
                    RPM_CONSTANTS.DEPT_MERCH_TYPE, 1,
                    RPM_CONSTANTS.CLASS_MERCH_TYPE, 2,
                    RPM_CONSTANTS.SUBCLASS_MERCH_TYPE, 3,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE, 4,
                    RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE, 5,
                    RPM_CONSTANTS.ITEM_MERCH_TYPE, 6,
                    RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE, 7) to_check_merch_rank,
             DECODE(to_check.zone_node_type,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 0,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, 1) to_check_org_rank,
             DECODE(applied.merch_type,
                    RPM_CONSTANTS.ITEM_LIST_TYPE, 0,
                    RPM_CONSTANTS.DEPT_MERCH_TYPE, 1,
                    RPM_CONSTANTS.CLASS_MERCH_TYPE, 2,
                    RPM_CONSTANTS.SUBCLASS_MERCH_TYPE, 3,
                    RPM_CONSTANTS.PARENT_MERCH_TYPE, 4,
                    RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE, 5,
                    RPM_CONSTANTS.ITEM_MERCH_TYPE, 6,
                    RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE, 7) applied_merch_rank,
             DECODE(applied.zone_node_type,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 0,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, 1) applied_org_rank
        from applied,
             to_check
       where applied.expl_item     = to_check.expl_item
         and applied.expl_location = to_check.expl_location;

BEGIN

   for rec in C_OVERLAPS loop

      -- If the input_types and ids for the current record are the same as
      -- the last record, skip this one
      if L_previous_to_check_input_type = rec.to_check_input_type and
         L_previous_to_check_id         = rec.to_check_id and
         L_previous_applied_input_type  = rec.applied_input_type and
         L_previous_applied_id          = rec.applied_id then
         ---
         continue;
      end if;

      -- if the "to_check" record has a smaller merch rank than the "applied" record and
      -- "to_check" had an org rank less than or equal to the "applied", remove the "applied" data

      if rec.to_check_merch_rank < rec.applied_merch_rank and
         (rec.to_check_org_rank = rec.applied_org_rank or
          rec.to_check_org_rank < rec.applied_org_rank) then

         delete
           from rpm_cancel_promo_mn_zn_gtt
          where input_type = rec.applied_input_type
            and id         = rec.applied_id;

         delete
           from rpm_cancel_promo_mnzn_expl_gtt
          where input_type = rec.applied_input_type
            and id         = rec.applied_id;

      -- if the "to_check" record has a larger merch rank than the "applied" record and
      -- "to_check" had an org rank greater than or equal to the "applied", remove the "to_check" data
      elsif rec.to_check_merch_rank > rec.applied_merch_rank and
            (rec.to_check_org_rank = rec.applied_org_rank or
             rec.to_check_org_rank > rec.applied_org_rank) then

         delete
           from rpm_cancel_promo_mn_zn_gtt
          where input_type = rec.to_check_input_type
            and id         = rec.to_check_id;

         delete
           from rpm_cancel_promo_mnzn_expl_gtt
          where input_type = rec.to_check_input_type
            and id         = rec.to_check_id;

      end if;

      L_previous_to_check_input_type := rec.to_check_input_type;
      L_previous_to_check_id         := rec.to_check_id;
      L_previous_applied_input_type  := rec.applied_input_type;
      L_previous_applied_id          := rec.applied_id;

   end loop;

   return 1;

EXCEPTION
   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END CHECK_CANCEL_INPUT_OVERLAPS;

--------------------------------------------------------------------------------

FUNCTION CHECK_SIBLINGS_FOR_CANCEL(O_error_message             OUT VARCHAR2,
                                   O_valid_merch_nd_zone_nd    OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                                   I_promo_dtl_id           IN     NUMBER)
RETURN NUMBER IS

   L_program  VARCHAR2(50) := 'RPM_PROMO_SQL.CHECK_SIBLINGS_FOR_CANCEL';

   cursor C_CHECK_SIBLINGS is
      with clean as
         (select distinct id,
                 input_type
            from (select id,
                         expl_item,
                         expl_location,
                         input_type
                    from rpm_cancel_promo_mnzn_expl_gtt
                  minus
                  select expl.id,
                         il_gtt.item,
                         il_gtt.location,
                         expl.input_type
                    from rpm_item_loc_gtt il_gtt,
                         rpm_cancel_promo_mnzn_expl_gtt expl
                   where il_gtt.item     = expl.expl_item
                     and il_gtt.location = expl.expl_location))
      -- Item Loc and Parent Item loc level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, --zone_id
                                         NULL, --zone_display_id
                                         NULL, --zone_desc
                                         gtt.merch_type,
                                         d.dept,
                                         d.dept_name,
                                         c.class,
                                         c.class_name,
                                         sc.subclass,
                                         sc.sub_name,
                                         gtt.item,
                                         im.item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             item_master im,
             deps d,
             class c,
             subclass sc
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     IN (RPM_CONSTANTS.ITEM_MERCH_TYPE,
                                    RPM_CONSTANTS.PARENT_MERCH_TYPE)
         and s.store            = gtt.location
         and im.item            = gtt.item
         and im.dept            = d.dept
         and im.dept            = c.dept
         and im.class           = c.class
         and im.dept            = sc.dept
         and im.class           = sc.class
         and im.subclass        = sc.subclass
      union all
      -- Item Zone and Parent Item zone level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         NULL, -- store
                                         NULL, -- store name
                                         gtt.zone_id,
                                         rz.zone_display_id,
                                         rz.name,
                                         gtt.merch_type,
                                         d.dept,
                                         d.dept_name,
                                         c.class,
                                         c.class_name,
                                         sc.subclass,
                                         sc.sub_name,
                                         gtt.item,
                                         im.item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             rpm_zone rz,
             item_master im,
             deps d,
             class c,
             subclass sc
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and gtt.merch_type     IN (RPM_CONSTANTS.ITEM_MERCH_TYPE,
                                    RPM_CONSTANTS.PARENT_MERCH_TYPE)
         and rz.zone_id         = gtt.zone_id
         and im.item            = gtt.item
         and im.dept            = d.dept
         and im.dept            = c.dept
         and im.class           = c.class
         and im.dept            = sc.dept
         and im.class           = sc.class
         and im.subclass        = sc.subclass
      union all
      -- Parent-Diff Location level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, --zone_id
                                         NULL, --zone_display_id
                                         NULL, --name
                                         gtt.merch_type,
                                         d.dept,
                                         d.dept_name,
                                         c.class,
                                         c.class_name,
                                         sc.subclass,
                                         sc.sub_name,
                                         gtt.item,
                                         im.item_desc,
                                         gtt.diff_id, -- diff_id
                                         di.diff_desc, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             diff_ids di,
             item_master im,
             deps d,
             class c,
             subclass sc
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and s.store            = gtt.location
         and im.item            = gtt.item
         and di.diff_id         = gtt.diff_id
         and im.dept            = d.dept
         and im.dept            = c.dept
         and im.class           = c.class
         and im.dept            = sc.dept
         and im.class           = sc.class
         and im.subclass        = sc.subclass
      union all
      -- Parent-Diff Zone level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         NULL, --location
                                         NULL, --loc_desc
                                         gtt.zone_id,
                                         rz.zone_display_id,
                                         rz.name,
                                         gtt.merch_type,
                                         d.dept,
                                         d.dept_name,
                                         c.class,
                                         c.class_name,
                                         sc.subclass,
                                         sc.sub_name,
                                         gtt.item,
                                         im.item_desc,
                                         gtt.diff_id,
                                         di.diff_desc,
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             rpm_zone rz,
             diff_ids di,
             item_master im,
             deps d,
             class c,
             subclass sc
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and gtt.merch_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and rz.zone_id         = gtt.zone_id
         and im.item            = gtt.item
         and di.diff_id         = gtt.diff_id
         and im.dept            = d.dept
         and im.dept            = c.dept
         and im.class           = c.class
         and im.dept            = sc.dept
         and im.class           = sc.class
         and im.subclass        = sc.subclass
      union all
      -- Dept Location Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, -- zone_id
                                         NULL, -- zone_display_id
                                         NULL, -- name
                                         gtt.merch_type,
                                         gtt.dept,
                                         d.dept_name,
                                         NULL, -- class
                                         NULL, -- class_desc
                                         NULL, -- subclass
                                         NULL, -- subclass_desc
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             deps d
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     = RPM_CONSTANTS.DEPT_MERCH_TYPE
         and s.store            = gtt.location
         and d.dept             = gtt.dept
      union all
      -- Class Location Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, -- zone_id
                                         NULL, -- zone_display_id
                                         NULL, -- name
                                         gtt.merch_type,
                                         gtt.dept,
                                         d.dept_name,
                                         gtt.class,
                                         c.class_name,
                                         NULL, -- subclass
                                         NULL, -- subclass_desc
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             deps d,
             class c
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     = RPM_CONSTANTS.CLASS_MERCH_TYPE
         and s.store            = gtt.location
         and d.dept             = gtt.dept
         and c.dept             = gtt.dept
         and c.class            = gtt.class
      union all
      -- Class Zone Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         NULL, -- location
                                         NULL, -- loc_desc
                                         gtt.zone_id,
                                         rz.zone_display_id,
                                         rz.name,
                                         gtt.merch_type,
                                         gtt.dept,
                                         d.dept_name,
                                         gtt.class,
                                         c.class_name,
                                         NULL, -- subclass
                                         NULL, -- subclass_desc
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             rpm_zone rz,
             deps d,
             class c
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and gtt.merch_type     = RPM_CONSTANTS.CLASS_MERCH_TYPE
         and rz.zone_id         = gtt.zone_id
         and d.dept             = gtt.dept
         and c.dept             = gtt.dept
         and c.class            = gtt.class
      union all
      -- Subclass Location Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, -- zone_id
                                         NULL, -- zone_display_id
                                         NULL, -- name
                                         gtt.merch_type,
                                         gtt.dept,
                                         d.dept_name,
                                         gtt.class,
                                         c.class_name,
                                         gtt.subclass,
                                         s.sub_name,
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             deps d,
             class c,
             subclass s
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     = RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
         and s.store            = gtt.location
         and d.dept             = gtt.dept
         and c.dept             = gtt.dept
         and c.class            = gtt.class
         and s.dept             = gtt.dept
         and s.class            = gtt.class
         and s.subclass         = gtt.subclass
      union all
      -- Subclass Zone Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         NULL, -- location
                                         NULL, -- loc_desc
                                         gtt.zone_id,
                                         rz.zone_display_id,
                                         rz.name,
                                         gtt.merch_type,
                                         gtt.dept,
                                         d.dept_name,
                                         gtt.class,
                                         c.class_name,
                                         gtt.subclass,
                                         s.sub_name,
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, -- skulist desc
                                         NULL, -- price_event_itemlist
                                         NULL) -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             rpm_zone rz,
             deps d,
             class c,
             subclass s
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and gtt.merch_type     = RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
         and rz.zone_id         = gtt.zone_id
         and d.dept             = gtt.dept
         and c.dept             = gtt.dept
         and c.class            = gtt.class
         and s.dept             = gtt.dept
         and s.class            = gtt.class
         and s.subclass         = gtt.subclass
      union all
      -- Item List Location Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, -- zone_id
                                         NULL, -- zone_display_id
                                         NULL, -- name
                                         gtt.merch_type,
                                         NULL, -- dept
                                         NULL, -- dept_name
                                         NULL, -- class
                                         NULL, -- class_name
                                         NULL, -- subclass
                                         NULL, -- subclass_desc
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         gtt.skulist,
                                         sh.skulist_desc, --skulist desc
                                         NULL,            -- price_event_itemlist
                                         NULL)            -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             skulist_head sh
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     = RPM_CONSTANTS.ITEM_LIST_TYPE
         and s.store            = gtt.location
         and sh.skulist         = gtt.skulist
      union all
      -- Price Event Item List (PEIL) - Location Level
      select OBJ_MERCH_ZONE_ND_DESCS_REC(gtt.zone_node_type,
                                         gtt.location,
                                         s.store_name,
                                         NULL, -- zone_id
                                         NULL, -- zone_display_id
                                         NULL, -- name
                                         gtt.merch_type,
                                         NULL, -- dept
                                         NULL, -- dept_name
                                         NULL, -- class
                                         NULL, -- class_name
                                         NULL, -- subclass
                                         NULL, -- subclass_desc
                                         NULL, -- item,
                                         NULL, -- item_desc,
                                         NULL, -- diff_id
                                         NULL, -- diff_desc
                                         NULL, -- skulist
                                         NULL, --skulist desc
                                         gtt.price_event_itemlist, -- price_event_itemlist
                                         rmlh.merch_list_desc)     -- price_event_itemlist_desc
        from clean,
             rpm_cancel_promo_mn_zn_gtt gtt,
             store s,
             rpm_merch_list_head rmlh
       where clean.id           = gtt.id
         and clean.input_type   = gtt.input_type
         and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and gtt.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and s.store            = gtt.location
         and rmlh.merch_list_id = gtt.price_event_itemlist;

BEGIN

   delete from rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt
      (item,
       location)
      -- Item loc
      select rpdmn.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item                 = rpdmn.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union
      -- Item zone
      select rpdmn.item,
             rzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_zone_location rzl,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rzl.zone_id             = rpzl.zone_id
         and im.item                 = rpdmn.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union
      -- Parent Item Loc
      select im.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent          = rpdmn.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union
      -- Parent Item Zone
      select im.item,
             rzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_zone_location rzl,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and rzl.zone_id             = rpzl.zone_id
         and im.item_parent          = rpdmn.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union
      -- Parent Diff Loc
      select im.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type        = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent          = rpdmn.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rpdmn.diff_id           IN (im.diff_1,
                                         im.diff_2,
                                         im.diff_3,
                                         im.diff_4)
      union
      -- Parent Diff Zone
      select im.item,
             rzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_zone_location rzl,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpdmn.merch_type        = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and rzl.zone_id             = rpzl.zone_id
         and im.item_parent          = rpdmn.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rpdmn.diff_id           IN (im.diff_1,
                                         im.diff_2,
                                         im.diff_3,
                                         im.diff_4)
      union
      -- Dept/Class/Subclass Loc
      select im.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type        IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = rpdmn.dept
         and im.class                = NVL(rpdmn.class, im.class)
         and im.subclass             = NVL(rpdmn.subclass, im.subclass)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
      union
      -- Class/Subclass Zone
      select im.item,
             rzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_zone_location rzl,
             item_master im
       where rpd.exception_parent_id is not NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpdmn.merch_type        IN (RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and rzl.zone_id             = rpzl.zone_id
         and im.dept                 = rpdmn.dept
         and im.class                = NVL(rpdmn.class, im.class)
         and im.subclass             = NVL(rpdmn.subclass, im.subclass)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
      union
      -- Item List Loc - parent items
      select im.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_promo_dtl_skulist rpds,
             item_master im
       where rpd.exception_parent_id is not NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpds.price_event_id     = rpd.promo_dtl_id
         and rpdmn.skulist           = rpds.skulist
         and rpds.item_level         = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and rpds.item               = im.item_parent
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
      union
      -- Item List Loc - tran items
      select rpds.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_promo_dtl_skulist rpds
       where rpd.exception_parent_id is not NULL
         and rpd.exception_parent_id = I_promo_dtl_id
         and rpd.promo_dtl_id        = rpzl.promo_dtl_id
         and rpd.promo_dtl_id        = rpdmn.promo_dtl_id
         and rpzl.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpds.price_event_id     = rpd.promo_dtl_id
         and rpdmn.skulist           = rpds.skulist
         and rpds.item_level         = RPM_CONSTANTS.IL_ITEM_LEVEL
      union
      -- Price Event Item List (PEIL) Loc - parent items
      select im.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_merch_list_detail rmld,
             item_master im
       where rpd.exception_parent_id is NOT NULL
         and rpd.exception_parent_id    = I_promo_dtl_id
         and rpd.promo_dtl_id           = rpzl.promo_dtl_id
         and rpd.promo_dtl_id           = rpdmn.promo_dtl_id
         and rpzl.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rpdmn.price_event_itemlist = rmld.merch_list_id
         and rmld.item                  = im.item_parent
         and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level              = im.tran_level
      union
      -- Price Event Item List (PEIL) Loc - tran items
      select rmld.item,
             rpzl.location
        from rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdmn,
             rpm_merch_list_detail rmld,
             item_master im
       where rpd.exception_parent_id    is NOT NULL
         and rpd.exception_parent_id    = I_promo_dtl_id
         and rpd.promo_dtl_id           = rpzl.promo_dtl_id
         and rpd.promo_dtl_id           = rpdmn.promo_dtl_id
         and rpzl.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rpdmn.price_event_itemlist = rmld.merch_list_id
         and rmld.item                  = im.item
         and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level              = im.tran_level;

   open C_CHECK_SIBLINGS;
   fetch C_CHECK_SIBLINGS BULK COLLECT into O_valid_merch_nd_zone_nd;
   close C_CHECK_SIBLINGS;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END CHECK_SIBLINGS_FOR_CANCEL;

--------------------------------------------------------------------------------

FUNCTION TXN_EXCL_TRAN_FROM_PARENT_DIFF(O_error_message    OUT VARCHAR2,
                                        O_tran_items       OUT OBJ_VARCHAR_ID_TABLE,
                                        I_parent_diffs  IN     OBJ_ITEM_DIFF_LOC_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_PROMO_SQL.TXN_EXCL_TRAN_FROM_PARENT_DIFF';

   cursor C_TRAN_ITEMS is
      select /*+ CARDINALITY (pds, 10) */ im.item
        from table(cast(I_parent_diffs as OBJ_ITEM_DIFF_LOC_DATE_TBL)) pds,
             item_master im
       where pds.item        = im.item_parent
         and pds.diff        IN (im.diff_1,
                                 im.diff_2,
                                 im.diff_3,
                                 im.diff_4)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level   = im.tran_level;

BEGIN

   open C_TRAN_ITEMS;
   fetch C_TRAN_ITEMS BULK COLLECT into O_tran_items;
   close C_TRAN_ITEMS;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END TXN_EXCL_TRAN_FROM_PARENT_DIFF;

--------------------------------------------------------------------------------

FUNCTION PROM_DTL_STATUS_UPDATE(O_error_message      OUT VARCHAR2,
                                I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                I_state           IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_PROMO_SQL.PROM_DTL_STATUS_UPDATE';

BEGIN

   forall i IN 1..I_price_event_ids.COUNT
     update rpm_promo_dtl rpd
        set rpd.state = I_state
      where  rpd.promo_dtl_id = I_price_event_ids(i)
        and rpd.state <> I_state;
    
   forall i IN 1..I_price_event_ids.COUNT  
     update rpm_promo_dtl rpd
        set rpd.state = I_state
      where rpd.exception_parent_id = I_price_event_ids(i)
        and exists (select 1 
		              from rpm_promo_comp rpc 
                     where rpc.promo_comp_id = rpd.promo_comp_id 	
		               and rpc.type =RPM_CONSTANTS.FINANCE_CODE
		               and rownum=1)
        and rpd.state <> I_state;
                                                        
   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END PROM_DTL_STATUS_UPDATE;

--------------------------------------------------------------------------------

END RPM_PROMO_SQL;
/
