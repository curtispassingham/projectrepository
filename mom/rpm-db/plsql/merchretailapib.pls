CREATE OR REPLACE PACKAGE BODY MERCH_RETAIL_API_SQL IS

--------------------------------------------------------------------------------
FUNCTION GET_EXISTING(O_error_msg             OUT RTK_ERRORS.RTK_TEXT%TYPE,
                      O_item_pricing_table    OUT NOCOPY OBJ_ITEM_PRICING_TBL,
                      I_item               IN     ITEM_MASTER.ITEM%TYPE,
                      I_query_item         IN     ITEM_MASTER.ITEM%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION ADD_TAX(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item             IN     ITEM_MASTER.ITEM%TYPE,
                 I_dept             IN     ITEM_MASTER.DEPT%TYPE,
                 I_class            IN     ITEM_MASTER.CLASS%TYPE,
                 I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                 I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                 I_from_entity_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                 I_from_loc_curr    IN     CURRENCIES.CURRENCY_CODE%TYPE,
                 I_date             IN     DATE,
                 IO_retail          IN OUT ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP (O_error_msg           OUT VARCHAR2,
                                  O_unit_retail         OUT RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                                  I_markup_percent   IN     RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE,
                                  I_markup_calc_type IN     RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                                  I_unit_cost        IN     ITEM_LOC_SOH.UNIT_COST%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION GET_MERCH_RETAIL_DEFS(O_error_message           OUT VARCHAR2,
                               O_merch_retail_def_id     OUT RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE,
                               O_regular_zone_group      OUT RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE,
                               O_clearance_zone_group    OUT RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE,
                               O_markup_calc_type        OUT RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                               O_markup_percent          OUT RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE,
                               O_exists                  OUT VARCHAR2,
                               I_dept                 IN     RPM_MERCH_RETAIL_DEF.DEPT%TYPE,
                               I_class                IN     RPM_MERCH_RETAIL_DEF.CLASS%TYPE,
                               I_subclass             IN     RPM_MERCH_RETAIL_DEF.SUBCLASS%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION APPLY_PRICE_GUIDES (O_error_message          OUT VARCHAR2,
                             O_guided_retail          OUT RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                             I_unit_retail         IN     RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                             I_currency            IN     RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE,
                             I_merch_retail_def_id IN     RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE,
                             I_price_guide_id      IN     RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE)

RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION CALCULATE_NON_RANGED_RETAIL (O_error_message          OUT VARCHAR2,
                                      O_selling_retail         OUT RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                                      O_selling_uom            OUT RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                                      O_selling_retail_curr    OUT RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                                      I_item                IN     ITEM_MASTER.ITEM%TYPE,
                                      I_dept                IN     ITEM_MASTER.DEPT%TYPE,
                                      I_merch_retail_def_id IN     RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE,
                                      I_markup_calc_type    IN     RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                                      I_markup_percent      IN     RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE,
                                      I_location            IN     ITEM_LOC.LOC%TYPE,
                                      I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                                      I_loc_currency        IN     STORE.CURRENCY_CODE%TYPE,
                                      I_unit_cost           IN     ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                                      I_supp_currency       IN     SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)

RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION AREA_DIFF_EXISTS (O_error_message       OUT VARCHAR2,
                           O_area_diff_exists    OUT NUMBER,
                           I_item             IN     ITEM_MASTER.ITEM%TYPE,
                           I_zone_group_id    IN     RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE)

RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION IS_PRIMARY_AREA(O_error_message        OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_primary              OUT NUMBER,
                         O_area_diff_prim_id    OUT RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_zone_id           IN     RPM_ZONE.ZONE_ID%TYPE)

RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION SET_AREA_DIFF_ZONE_IND(O_error_msg             OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_item_pricing_table IN OUT NOCOPY OBJ_ITEM_PRICING_TBL,
                                I_item               IN     ITEM_MASTER.ITEM%TYPE)

RETURN BOOLEAN;
--------------------------------------------------------------------------------
FUNCTION GET_MERCH_PRICING_DEFS(O_error_message            OUT  VARCHAR2,
                                I_dept                  IN      RPM_MERCH_RETAIL_DEF.DEPT%TYPE,
                                I_class                 IN      RPM_MERCH_RETAIL_DEF.CLASS%TYPE,
                                I_subclass              IN      RPM_MERCH_RETAIL_DEF.SUBCLASS%TYPE,
                                O_regular_zone_group       OUT  RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE,
                                O_clearance_zone_group     OUT  RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE,
                                O_markup_calc_type         OUT  RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                                O_markup_percent           OUT  RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE)
RETURN BOOLEAN IS

   L_program               VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.GET_MERCH_PRICING_DEFS';
   L_merch_retail_def_id   RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE;
   L_exists                VARCHAR2(1) := NULL;

BEGIN

   if GET_MERCH_RETAIL_DEFS(O_error_message,
                            L_merch_retail_def_id,
                            O_regular_zone_group,
                            O_clearance_zone_group,
                            O_markup_calc_type,
                            O_markup_percent,
                            L_exists,
                            I_dept,
                            I_class,
                            I_subclass) = FALSE then
      return FALSE;
   elsif L_exists = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MERCH_RETAIL_DEF', NULL, NULL, NULL);
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_MERCH_PRICING_DEFS;

--------------------------------------------------------------------------------
PROCEDURE MERCH_PRICING_DEFS_EXIST
(O_error_message          OUT  VARCHAR2,
 O_success                OUT  VARCHAR2,
 I_dept                IN      DEPS.DEPT%TYPE,
 I_class               IN      CLASS.CLASS%TYPE,
 I_subclass            IN      SUBCLASS.SUBCLASS%TYPE
)
IS

   L_program        VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.MERCH_PRICING_DEFS_EXIST';
   L_invalid_param  VARCHAR2(30) := null;
   L_exist          VARCHAR2(1)  := 'y';

   cursor C_GET_RETAIL_DEF is
      select 'x'
        from rpm_merch_retail_def
       where dept  = I_dept
         and NVL(class,I_class) = I_class
         and NVL(subclass,I_subclass) = I_subclass;


BEGIN

   O_success := 'Y';

   if I_dept is NULL then
      L_invalid_param := 'I_dept';
   end if;
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_success := 'N';
      return;
   end if;

   open C_GET_RETAIL_DEF;
   fetch C_GET_RETAIL_DEF into L_exist;
   if C_GET_RETAIL_DEF%NOTFOUND then
          close C_GET_RETAIL_DEF;
          O_success := 'N';
          O_error_message := SQL_LIB.CREATE_MSG('NO_MERCH_RETAIL_DEF',NULL,NULL,NULL);
          return;
   end if;
   close C_GET_RETAIL_DEF;

   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END MERCH_PRICING_DEFS_EXIST;
--------------------------------------------------------------------------------
FUNCTION GET_RPM_ITEM_ZONE_PRICE(O_error_message               OUT VARCHAR2,
                                 I_item                     IN     rpm_item_zone_price.item%TYPE,
                                 I_zone_id                  IN     rpm_item_zone_price.zone_id%TYPE,
                                 O_standard_retail             OUT rpm_item_zone_price.standard_retail%TYPE,
                                 O_standard_retail_currency    OUT rpm_item_zone_price.standard_retail_currency%TYPE,
                                 O_standard_uom                OUT rpm_item_zone_price.standard_uom%TYPE,
                                 O_selling_retail              OUT rpm_item_zone_price.selling_retail%TYPE,
                                 O_selling_retail_currency     OUT rpm_item_zone_price.selling_retail_currency%TYPE,
                                 O_selling_uom                 OUT rpm_item_zone_price.selling_uom%TYPE,
                                 O_multi_units                 OUT rpm_item_zone_price.multi_units%TYPE,
                                 O_multi_unit_retail           OUT rpm_item_zone_price.multi_unit_retail%TYPE,
                                 O_multi_unit_retail_currency  OUT rpm_item_zone_price.multi_unit_retail_currency%TYPE,
                                 O_multi_selling_uom           OUT rpm_item_zone_price.multi_selling_uom%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.GET_RPM_ITEM_ZONE_PRICE';

   cursor C_RIZP is
      select rizp.standard_retail,
             rizp.standard_retail_currency,
             rizp.standard_uom,
             rizp.selling_retail,
             rizp.selling_retail_currency,
             rizp.selling_uom,
             rizp.multi_units,
             rizp.multi_unit_retail,
             rizp.multi_unit_retail_currency,
             rizp.multi_selling_uom
        from rpm_item_zone_price rizp
       where rizp.item    = I_item
         and rizp.zone_id = I_zone_id;

BEGIN

   open C_RIZP;
   fetch C_RIZP into O_standard_retail,
                     O_standard_retail_currency,
                     O_standard_uom,
                     O_selling_retail,
                     O_selling_retail_currency,
                     O_selling_uom,
                     O_multi_units,
                     O_multi_unit_retail,
                     O_multi_unit_retail_currency,
                     O_multi_selling_uom;
   close C_RIZP;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END GET_RPM_ITEM_ZONE_PRICE;
--------------------------------------------------------------------------------
PROCEDURE GET_ITEM_PRICING_INFO (O_error_message         OUT VARCHAR2,
                                 O_success               OUT VARCHAR2,
                                 O_item_pricing_table    OUT NOCOPY OBJ_ITEM_PRICING_TBL,
                                 I_item_parent        IN     RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_item               IN     RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                                 I_dept               IN     DEPS.DEPT%TYPE,
                                 I_class              IN     CLASS.CLASS%TYPE,
                                 I_subclass           IN     SUBCLASS.SUBCLASS%TYPE,
                                 I_cost_currency_code IN     RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE,
                                 I_cost               IN     NUMBER)
IS

   L_program       VARCHAR2(50) := 'MERCH_RETAIL_API_SQL.GET_ITEM_PRICING_INFO';
   L_invalid_param VARCHAR2(30) := NULL;

   L_reqular_zone_group    RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE := NULL;
   L_markup_calc_type      RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE   := NULL;
   L_markup_percent        RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE     := NULL;
   L_unit_retail           RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE     := NULL;
   L_converted_unit_retail RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE     := NULL;
   L_rounded_unit_retail   RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE     := NULL;
   L_guided_unit_retail    RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE     := NULL;

   L_retail_currency_code RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE            := NULL;
   L_merch_retail_def_id  RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE := NULL;

   L_area_diff_prim_id  RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE := NULL;
   L_zone_id            RPM_ZONE.ZONE_ID%TYPE                     := NULL;
   L_area_diff_exists   NUMBER(1)                                 := NULL;
   L_primary_zone_index NUMBER(10)                                := NULL;

   L_area_diff_zone_ind    NUMBER(1)            := NULL;
   L_primary_area          NUMBER(1)            := NULL;
   L_primary_pricing_table OBJ_ITEM_PRICING_TBL := NULL;

   cursor C_GET_RETAIL_DEF is
      select regular_zone_group,
             markup_calc_type,
             markup_percent,
             merch_retail_def_id
        from (select '1' sort_order,
                     regular_zone_group,
                     markup_calc_type,
                     markup_percent/100 markup_percent,
                     merch_retail_def_id
                from rpm_merch_retail_def
               where NVL(dept,-999)     = I_dept
                 and NVL(class,-999)    = I_class
                 and NVL(subclass,-999) = I_subclass
              union all
              select '2' sort_order,
                     regular_zone_group,
                     markup_calc_type,
                     markup_percent/100 markup_percent,
                     merch_retail_def_id
                from rpm_merch_retail_def
               where NVL(dept,-999)  = I_dept
                 and NVL(class,-999) = I_class
                 and subclass        is NULL
              union all
              select '3' sort_order,
                     regular_zone_group,
                     markup_calc_type,
                     markup_percent/100 markup_percent,
                     merch_retail_def_id
                from rpm_merch_retail_def
               where dept     = I_dept
                 and class    is NULL
                 and subclass is NULL)
        order by sort_order;

   cursor C_GET_ZONES is
      select zone_id,
             zone_display_id,
             zone_group_id,
             name,
             base_ind,
             currency_code
        from rpm_zone
       where zone_group_id = L_reqular_zone_group
         and (   L_area_diff_exists != 1
              or (    L_area_diff_exists = 1
                  and zone_id NOT IN (select zone_hier_id
                                        from rpm_area_diff rad,
                                             (select area_diff_prim_id
                                                from rpm_area_diff_prim radp,
                                                     (select zone_id
                                                        from rpm_zone
                                                       where zone_group_id = L_reqular_zone_group) rz
                                               where radp.zone_hier_id = rz.zone_id
                                                 and (   (    dept     = I_dept
                                                          and class    is NULL
                                                          and subclass is NULL)
                                                      or (    dept     = I_dept
                                                          and class    = I_class
                                                          and subclass is NULL)
                                                      or (    dept     = I_dept
                                                          and class    = I_class
                                                          and subclass = I_subclass))) radd
                                       where rad.area_diff_prim_id = radd.area_diff_prim_id )));

   cursor C_GET_SECONDARY_ZONES is
      select /*+ CARDINALITY (prim 10) */
             rz.zone_id,
             rz.zone_display_id,
             rz.zone_group_id,
             rz.name,
             rz.base_ind,
             rz.currency_code,
             rad.percent_apply_type,
             rad.percent,
             rad.price_guide_id,
             prim.unit_retail,
             rz1.currency_code prim_currency_code
        from table(cast(L_primary_pricing_table as OBJ_ITEM_PRICING_TBL)) prim,
             rpm_zone rz,
             rpm_zone rz1,
             rpm_area_diff rad,
             (select zone_hier_id,
                     area_diff_prim_id
                from (select zone_hier_id,
                             area_diff_prim_id
                        from rpm_area_diff_prim rap
                       where (   (    rap.dept     = I_dept
                                  and rap.class    is NULL
                                  and rap.subclass is NULL)
                              or (    rap.dept     = I_dept
                                  and rap.class    = I_class
                                  and rap.subclass is NULL)
                              or (    rap.dept     = I_dept
                                  and rap.class    = I_class
                                  and rap.subclass = I_subclass))
                       order by dept,
                                NVL(class, 9999999999),
                                NVL(subclass, 9999999999))
               where rownum = 1) rap
       where prim.area_diff_zone_ind = 1
         and rap.zone_hier_id        = prim.zone_id
         and rz1.zone_id             = prim.zone_id
         and rad.area_diff_prim_id   = rap.area_diff_prim_id
         and rz.zone_id              = rad.zone_hier_id;

BEGIN

   O_success := 'Y';
   --- Validate parameters
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_cost_currency_code is NULL then
      L_invalid_param := 'I_cost_currency_code';
   elsif I_cost is NULL then
      L_invalid_param := 'I_cost';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_success := 'N';
      return;
   end if;

   --- Be sure pricing table is initialized and empty
   if O_item_pricing_table is NULL then
      O_item_pricing_table := OBJ_ITEM_PRICING_TBL();
   else
      O_item_pricing_table.DELETE;
   end if;

   --check for existing records
   if GET_EXISTING(O_error_message,
                   O_item_pricing_table,
                   I_item,
                   I_item) = FALSE then
      O_success := 'N';
      return;
   end if;

   if O_item_pricing_table.COUNT > 0 then
      O_success := 'Y';
      return;
   end if;

   if I_item_parent IS NOT NULL then
      --check for existing parent records
      if GET_EXISTING(O_error_message,
                      O_item_pricing_table,
                      I_item,
                      I_item_parent) = FALSE then
         O_success := 'N';
         return;
      end if;

      if O_item_pricing_table.COUNT > 0 then
         O_success := 'Y';
         return;
      end if;
   end if;

   -- Get the markup percent and markup calc type

   open C_GET_RETAIL_DEF;
   fetch C_GET_RETAIL_DEF into L_reqular_zone_group,
                               L_markup_calc_type,
                               L_markup_percent,
                               L_merch_retail_def_id;

   if C_GET_RETAIL_DEF%NOTFOUND then
      close C_GET_RETAIL_DEF;
      O_success := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('NO_MERCH_RETAIL_DEF',
                                            NULL,
                                            NULL,
                                            NULL);
      return;
   end if;

   close C_GET_RETAIL_DEF;

   -- Check if an Area Differential is defined for the Department
   if AREA_DIFF_EXISTS (O_error_message,
                        L_area_diff_exists,
                        I_item,
                        L_reqular_zone_group) = FALSE then
      O_success := 'N';
      return;
   end if;
   -- Calculate the retail price for all ZONE_ID

   for reczone IN C_GET_ZONES loop

      -- Calculate the retail price based on the markup percent
      if I_cost = 0 then
         L_unit_retail           := 0;
         L_converted_unit_retail := 0;
         L_rounded_unit_retail   := 0;
      else
         if NOT CALC_RETAIL_FROM_MARKUP (O_error_message,
                                         L_unit_retail,
                                         L_markup_percent,
                                         L_markup_calc_type,
                                         I_cost) then
            O_success := 'N';
            return;
         end if;
         --
         -- Convert the Unit Retail to the currency of the Zone
         if reczone.currency_code != I_cost_currency_code then
            if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                                   L_converted_unit_retail,
                                                   reczone.currency_code,
                                                   L_unit_retail,
                                                   I_cost_currency_code) = 0 then
               O_success := 'N';
               return;
            end if;
         else
            L_converted_unit_retail := L_unit_retail;
         end if;
         --
         if RPM_WRAPPER.ROUND_CURRENCY(O_error_message,
                                       L_rounded_unit_retail,
                                       L_converted_unit_retail,
                                       reczone.currency_code,
                                       1) = 0 then
            O_success := 'N';
            return;
         end if;
      end if;

      -- Populate the Output Table
      if IS_PRIMARY_AREA(O_error_message,
                         L_primary_area,
                         L_area_diff_prim_id,
                         I_item,
                         reczone.zone_id) = FALSE then
        O_success := 'N';
         return;
      end if;

      if L_primary_area = 1 then
         L_area_diff_zone_ind := 1;
      else
         L_area_diff_zone_ind := 0;
      end if;

      O_item_pricing_table.EXTEND;

      O_item_pricing_table(O_item_pricing_table.COUNT) := OBJ_ITEM_PRICING_REC(I_item,
                                                                               reczone.zone_group_id,
                                                                               reczone.zone_id,
                                                                               reczone.zone_display_id,
                                                                               reczone.name,
                                                                               L_rounded_unit_retail,
                                                                               L_rounded_unit_retail,
                                                                               NULL,
                                                                               NULL,
                                                                               NULL,
                                                                               NULL,
                                                                               reczone.base_ind,
                                                                               reczone.currency_code,
                                                                               NULL,
                                                                               L_area_diff_zone_ind);
   end loop;

   -- Apply VAT

   PM_RETAIL_API_SQL.APPLY_VAT (O_error_message,
                                O_success,
                                O_item_pricing_table);

   if O_success = 'N' then
      return;
   end if;

   -- Execute Price Guide

   for i IN 1..O_item_pricing_table.COUNT loop
      L_retail_currency_code := O_item_pricing_table(i).currency_code;
      L_rounded_unit_retail  := O_item_pricing_table(i).unit_retail;

      if APPLY_PRICE_GUIDES (O_error_message,
                             L_guided_unit_retail,
                             L_rounded_unit_retail,
                             L_retail_currency_code,
                             L_merch_retail_def_id,
                             NULL) = FALSE then
         O_success := 'N';
         return;
      end if;

      O_item_pricing_table(i).unit_retail         := L_guided_unit_retail;
      O_item_pricing_table(i).selling_unit_retail := L_guided_unit_retail;

      if L_zone_id is NOT NULL and
         L_primary_zone_index is NULL then
         if O_item_pricing_table(i).zone_id = L_zone_id then
            L_primary_zone_index := i;
         end if;
      end if;
   end loop;

   if L_area_diff_exists = 1 then
      L_primary_pricing_table := O_item_pricing_table;
      for seczone IN C_GET_SECONDARY_ZONES loop
         if I_cost = 0 then
            L_unit_retail           := 0;
            L_converted_unit_retail := 0;
            L_rounded_unit_retail   := 0;

         else
            L_unit_retail := seczone.unit_retail;
            if seczone.percent_apply_type = 0 then
               L_unit_retail := L_unit_retail * (1 + seczone.percent / 100);
            elsif seczone.percent_apply_type = 1 then
               L_unit_retail := L_unit_retail * (1 - seczone.percent / 100);
            end if;
            --
            -- Convert the Unit Retail to the currency of the Zone
            if seczone.currency_code != seczone.prim_currency_code then
               if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                                      L_converted_unit_retail,
                                                      seczone.currency_code,
                                                      L_unit_retail,
                                                      seczone.prim_currency_code) = 0 then
                  O_success := 'N';
                  return;
               end if;
            else
               L_converted_unit_retail := L_unit_retail;
            end if;

            if RPM_WRAPPER.ROUND_CURRENCY(O_error_message,
                                          L_rounded_unit_retail,
                                          L_converted_unit_retail,
                                          seczone.currency_code,
                                          1) = 0 then
               O_success := 'N';
               return;
            end if;

            if seczone.price_guide_id is NOT NULL then
               if APPLY_PRICE_GUIDES (O_error_message,
                                      L_guided_unit_retail,
                                      L_rounded_unit_retail,
                                      seczone.currency_code,
                                      NULL,
                                      seczone.price_guide_id) = FALSE then
                  O_success := 'N';
                  return;
               end if;
            else
               L_guided_unit_retail := L_rounded_unit_retail;
            end if;

         end if;

         O_ITEM_PRICING_TABLE.EXTEND;

         O_ITEM_PRICING_TABLE(O_ITEM_PRICING_TABLE.COUNT) := OBJ_ITEM_PRICING_REC(I_item,
                                                                                  seczone.zone_group_id,
                                                                                  seczone.zone_id,
                                                                                  seczone.zone_display_id,
                                                                                  seczone.name,
                                                                                  L_guided_unit_retail,
                                                                                  L_guided_unit_retail,
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL,
                                                                                  NULL,
                                                                                  seczone.base_ind,
                                                                                  seczone.currency_code,
                                                                                  NULL,
                                                                                  2);
      end loop;
   end if;

   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END GET_ITEM_PRICING_INFO;

--------------------------------------------------------------------------------
FUNCTION GET_EXISTING(O_error_msg               OUT  rtk_errors.rtk_text%TYPE,
                      O_item_pricing_table      OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                      I_item                 IN      item_master.item%TYPE,
                      I_query_item           IN      item_master.item%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.GET_EXISTING';

   cursor C_EXISTING is
      select OBJ_ITEM_PRICING_REC(I_item,
                                  rz.zone_group_id,
                                  rz.zone_id,
                                  rz.zone_display_id,
                                  rz.name,
                                  rizp.standard_retail,
                                  rizp.selling_retail,
                                  rizp.selling_uom,
                                  rizp.multi_units,
                                  rizp.multi_unit_retail,
                                  rizp.multi_selling_uom,
                                  rz.base_ind,
                                  rizp.standard_retail_currency,
                                  rizp.standard_uom,
                                  0)
        from rpm_item_zone_price rizp,
             rpm_zone rz
       where rizp.item    = I_query_item
         and rizp.zone_id = rz.zone_id
       order by rz.zone_id;
   ---

BEGIN

   open C_EXISTING;

   fetch C_EXISTING BULK COLLECT into O_item_pricing_table;

   close C_EXISTING;

   if O_item_pricing_table.COUNT > 0 then
      if SET_AREA_DIFF_ZONE_IND(O_error_msg,
                                O_item_pricing_table,
                                I_item) = FALSE then
         return FALSE;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END GET_EXISTING;

--------------------------------------------------------------------------------
FUNCTION CALC_RETAIL_FROM_MARKUP (O_error_msg            OUT  VARCHAR2,
                                  O_unit_retail          OUT  RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                                  I_markup_percent    IN      RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE,
                                  I_markup_calc_type  IN      RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                                  I_unit_cost         IN      ITEM_LOC_SOH.UNIT_COST%TYPE)
   RETURN BOOLEAN IS

   L_program      VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.CALC_RETAIL_FROM_MARKUP';

BEGIN

   if I_markup_calc_type = COST_MARKUP_CALC_TYPE then
      if I_markup_percent = -(MAX_MARKUP_PERCENT) then
         O_unit_retail := 0;
      elsif I_markup_percent < -(MAX_MARKUP_PERCENT) then
         O_error_msg := SQL_LIB.CREATE_MSG('MKDN_LESS_100PCT_COST');
         return FALSE;
      else
         ---
         O_unit_retail := I_unit_cost * (1 + I_markup_percent);
      end if;
   elsif I_markup_calc_type = RETAIL_MARKUP_CALC_TYPE then

      if I_markup_percent >= MAX_MARKUP_PERCENT then
         O_error_msg := SQL_LIB.CREATE_MSG('RETAIL_MARKUP_100');
         return FALSE;
      else
         ---
         O_unit_retail := I_unit_cost / (1 - I_markup_percent);
      end if;
   else
      O_error_msg := SQL_LIB.CREATE_MSG('INV_MARKUP_TYPE_VALUE', I_markup_calc_type);
      return FALSE;
   end if;

   if O_unit_retail > MAX_UNIT_RETAIL or O_unit_retail < MIN_UNIT_RETAIL then
      O_error_msg := SQL_LIB.CREATE_MSG('CALC_RETL_OUTSIDE_RANGE');
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END CALC_RETAIL_FROM_MARKUP;

--------------------------------------------------------------------------------
PROCEDURE GET_ZONE_LOCATIONS
(O_error_message       OUT  VARCHAR2,
 O_success             OUT  VARCHAR2,
 O_zone_locs_table     OUT  NOCOPY OBJ_ZONE_LOCS_TBL,
 I_zone_group_id    IN      RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
 I_zone_id          IN      RPM_ZONE.ZONE_ID%TYPE
)
IS

   L_program  VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.GET_ZONE_LOCATIONS';

   cursor C_ZONE_LOCS is
      select OBJ_ZONE_LOCS_REC(z.zone_group_id, zl.zone_id, zl.location, null, zl.loc_type)
        from rpm_zone_location zl,
             rpm_zone z
       where z.zone_group_id = I_zone_group_id
         and z.zone_id       = zl.zone_id
         and zl.zone_id      = NVL(I_zone_id, zl.zone_id)
       order by location;

BEGIN

   -- We do not have to check for NULL value in I_zone_id here.  In the case
   -- where this parameter is null we will select all zone/locations for
   -- the zone group.
   if I_zone_group_id is NULL then
      O_success := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_zone_group_id',
                                            L_program,
                                            NULL);
      return;
   end if;

   open  C_ZONE_LOCS;
   fetch C_ZONE_LOCS BULK COLLECT into O_zone_locs_table;
   close C_ZONE_LOCS;

   O_success := 'Y';

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END GET_ZONE_LOCATIONS;

--------------------------------------------------------------------------------
PROCEDURE SET_ITEM_PRICING_INFO(O_error_message          OUT  VARCHAR2,
                                O_success                OUT  VARCHAR2,
                                I_item_pricing_table  IN      OBJ_ITEM_PRICING_TBL) IS

   L_program  VARCHAR2(61)      := 'MERCH_RETAIL_API_SQL.SET_ITEM_PRICING_INFO';
   L_vdate    PERIOD.VDATE%TYPE := DATES_SQL.GET_VDATE;

BEGIN

   --- Validate parameters
   if I_item_pricing_table       is NULL or
      I_item_pricing_table.COUNT = 0 then
      O_success := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_item_pricing_table',
                                            L_program,
                                            NULL);
      return;
   end if;

   merge /*+ FIRST_ROWS */ into rpm_item_zone_price rizp
   using (select zone_id,
                 item,
                 unit_retail,
                 standard_uom,
                 selling_unit_retail,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_selling_uom,
                 currency_code
            from table(cast(I_item_pricing_table as OBJ_ITEM_PRICING_TBL))) ipt
   on (    rizp.item    = ipt.item
       and rizp.zone_id = ipt.zone_id)
   when MATCHED then
      update
         set standard_retail            = ipt.unit_retail,
             standard_retail_currency   = ipt.currency_code,
             standard_uom               = ipt.standard_uom,
             selling_retail             = ipt.selling_unit_retail,
             selling_retail_currency    = ipt.currency_code,
             selling_uom                = ipt.selling_uom,
             multi_units                = ipt.multi_units,
             multi_unit_retail          = ipt.multi_unit_retail,
             multi_unit_retail_currency = ipt.currency_code,
             multi_selling_uom          = ipt.multi_selling_uom
    when NOT MATCHED then
       insert (item_zone_price_id,
               item,
               zone_id,
               standard_retail,
               standard_retail_currency,
               standard_uom,
               selling_retail,
               selling_retail_currency,
               selling_uom,
               multi_units,
               multi_unit_retail,
               multi_unit_retail_currency,
               multi_selling_uom)
       values (RPM_ITEM_ZONE_PRICE_SEQ.NEXTVAL,
               ipt.item,
               ipt.zone_id,
               ipt.unit_retail,
               ipt.currency_code,
               ipt.standard_uom,
               ipt.selling_unit_retail,
               ipt.currency_code,
               ipt.selling_uom,
               ipt.multi_units,
               ipt.multi_unit_retail,
               ipt.currency_code,
               ipt.multi_selling_uom);

   merge /*+ FIRST_ROWS */ into rpm_zone_future_retail rzfr
   using (select L_vdate - 1 as vdate,
                 ip.zone_id,
                 ip.item,
                 ip.unit_retail,
                 ip.standard_uom,
                 ip.selling_unit_retail,
                 ip.selling_uom,
                 ip.multi_units,
                 ip.multi_unit_retail,
                 ip.multi_selling_uom,
                 ip.currency_code
            from table(cast(I_item_pricing_table as OBJ_ITEM_PRICING_TBL)) ip,
                 item_master im
           where im.item       = ip.item
             and im.item_level = im.tran_level) ipt
      on (    rzfr.item   = ipt.item
          and rzfr.zone   = ipt.zone_id
          and action_date = ipt.vdate)
   when MATCHED then
      update
         set selling_retail             = ipt.selling_unit_retail,
             selling_retail_currency    = ipt.currency_code,
             selling_uom                = ipt.selling_uom,
             multi_units                = ipt.multi_units,
             multi_unit_retail          = ipt.multi_unit_retail,
             multi_unit_retail_currency = ipt.currency_code,
             multi_selling_uom          = ipt.multi_selling_uom
   when NOT MATCHED then
      insert (zone_future_retail_id,
              item,
              zone,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              price_change_id,
              price_change_display_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              lock_version)
      values (RPM_ZONE_FUTURE_RETAIL_SEQ.NEXTVAL,
              ipt.item,
              ipt.zone_id,
              ipt.vdate,
              ipt.selling_unit_retail,
              ipt.currency_code,
              ipt.selling_uom,
              ipt.multi_units,
              ipt.multi_unit_retail,
              ipt.currency_code,
              ipt.multi_selling_uom,
              NULL,                    --price_change_id,
              NULL,                    --price_change_display_id,
              NULL,                    --pc_change_type,
              NULL,                    --pc_change_amount,
              NULL,                    --pc_change_currency,
              NULL,                    --pc_change_percent,
              NULL,                    --pc_change_selling_uom,
              NULL,                    --pc_null_multi_ind,
              NULL,                    --pc_multi_units,
              NULL,                    --pc_multi_unit_retail,
              NULL,                    --pc_multi_unit_retail_currency,
              NULL,                    --pc_multi_selling_uom,
              NULL,                    --pc_price_guide_id,
              0);                      --loc_version)

   O_success := 'Y';

   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END SET_ITEM_PRICING_INFO;

--------------------------------------------------------------------------------
PROCEDURE SET_CHILD_ITEM_PRICING_INFO
(O_error_message                OUT  VARCHAR2,
 O_success                      OUT  VARCHAR2,
 I_parent_item               IN      ITEM_MASTER.ITEM%TYPE,
 I_zone_id                   IN      RPM_ITEM_ZONE_PRICE.ZONE_ID%TYPE,
 I_child_item_pricing_table  IN      OBJ_CHILD_ITEM_PRICING_TBL
)
IS

   TYPE ITEM_CHILD_PRICING_REC IS RECORD
   (
      ITEM                          RPM_ITEM_ZONE_PRICE.ITEM%TYPE
     ,ZONE_GROUP_ID                 RPM_ZONE.ZONE_GROUP_ID%TYPE
     ,ZONE_ID                       RPM_ITEM_ZONE_PRICE.ZONE_ID%TYPE
     ,ZONE_DISPLAY_ID               RPM_ZONE.ZONE_DISPLAY_ID%TYPE
     ,ZONE_NAME                     RPM_ZONE.NAME%TYPE
     ,STANDARD_RETAIL               RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE
     ,STANDARD_RETAIL_CURRENCY      RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL_CURRENCY%TYPE
     ,STANDARD_UOM                  RPM_ITEM_ZONE_PRICE.STANDARD_UOM%TYPE
     ,SELLING_RETAIL                RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE
     ,SELLING_RETAIL_CURRENCY       RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE
     ,SELLING_UOM                   RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE
     ,MULTI_UNITS                   RPM_ITEM_ZONE_PRICE.MULTI_UNITS%TYPE
     ,MULTI_UNIT_RETAIL             RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL%TYPE
     ,MULTI_SELLING_UOM             RPM_ITEM_ZONE_PRICE.MULTI_SELLING_UOM%TYPE
     ,BASE_IND                      RPM_ZONE.BASE_IND%TYPE
     ,NEW_UNIT_RETAIL               RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE
     ,NEW_STANDARD_UOM              RPM_ITEM_ZONE_PRICE.STANDARD_UOM%TYPE
     ,NEW_SELLING_UNIT_RETAIL       RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE
     ,NEW_SELLING_UOM               RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE
     ,NEW_CURRENCY_CODE             RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL_CURRENCY%TYPE
   );


   TYPE ITEM_CHILD_PRICING_TEMP_TABLE is TABLE OF ITEM_CHILD_PRICING_REC;

   L_CHILD_PRICING_TEMP_TABLE            ITEM_CHILD_PRICING_TEMP_TABLE;

   L_program  VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.SET_CHILD_ITEM_PRICING_INFO';
   L_item_pricing_table           OBJ_ITEM_PRICING_TBL;
   L_item_pricing_area_diff       OBJ_ITEM_PRICING_TBL;
   L_item                         rpm_item_zone_price.item%TYPE;
   L_unit_retail                  rpm_item_zone_price.standard_retail%TYPE;
   L_standard_uom                 rpm_item_zone_price.standard_uom%TYPE;
   L_selling_retail               rpm_item_zone_price.standard_retail%TYPE;
   L_selling_uom                  rpm_item_zone_price.standard_uom%TYPE;
   L_currency_code                rpm_item_zone_price.standard_retail_currency%TYPE;
   L_multi_units                  rpm_item_zone_price.multi_units%TYPE;
   L_multi_unit_retail            rpm_item_zone_price.multi_unit_retail%TYPE;
   L_multi_selling_uom            rpm_item_zone_price.multi_selling_uom%TYPE;
   L_base_ind                     rpm_zone.base_ind%TYPE;
   ---

      cursor C_PRICE is
      select L_item,
             rz.zone_group_id,
             rz.zone_id,
             rz.zone_display_id,
             rz.name,
             rizp.standard_retail,
             rizp.standard_retail_currency,
             rizp.standard_uom,
             rizp.selling_retail,
             rizp.selling_retail_currency,
             rizp.selling_uom,
             rizp.multi_units,
             rizp.multi_unit_retail,
             rizp.multi_selling_uom,
             rz.base_ind,
             L_unit_retail,
             L_standard_uom,
             L_selling_retail,
             L_selling_uom,
             L_currency_code
        from rpm_item_zone_price rizp,
             rpm_zone rz
       where rizp.item    = I_parent_item
         and rizp.zone_id = rz.zone_id
         and rizp.zone_id = NVL(I_zone_id, rizp.zone_id);

BEGIN

   if I_child_item_pricing_table is NULL or I_child_item_pricing_table.COUNT = 0 then
      O_success := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_child_item_pricing_table',
                                            L_program,
                                            NULL);
      return;
   end if;

   if I_parent_item is NULL then
      O_success := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('REQUIRED_INPUT_IS_NULL',
                                            'I_parent_item',
                                            L_program,
                                            NULL);
      return;
   end if;

   if L_item_pricing_table is NULL then
      L_item_pricing_table := OBJ_ITEM_PRICING_TBL();
   else
      L_item_pricing_table.DELETE;
   end if;

   for i in 1..I_child_item_pricing_table.COUNT LOOP
      if L_CHILD_PRICING_TEMP_TABLE is NULL then
         L_CHILD_PRICING_TEMP_TABLE := ITEM_CHILD_PRICING_TEMP_TABLE();
      else
         L_CHILD_PRICING_TEMP_TABLE.DELETE;
      end if;

      L_item        := I_child_item_pricing_table(i).child_item;
      L_unit_retail := I_child_item_pricing_table(i).unit_retail;
      L_standard_uom    := I_child_item_pricing_table(i).standard_uom;
      L_selling_retail  := I_child_item_pricing_table(i).selling_unit_retail;
      L_selling_uom     := I_child_item_pricing_table(i).selling_uom;
      L_currency_code   := I_child_item_pricing_table(i).currency_code;

      open C_PRICE;

      fetch C_PRICE BULK COLLECT into L_CHILD_PRICING_TEMP_TABLE;

      close C_PRICE;

      for i in 1..L_CHILD_PRICING_TEMP_TABLE.COUNT LOOP
         if L_CHILD_PRICING_TEMP_TABLE(i).NEW_CURRENCY_CODE is NULL then
            L_unit_retail    := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail;
            L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).standard_uom;
            L_selling_retail := L_CHILD_PRICING_TEMP_TABLE(i).selling_retail;
            L_selling_uom    := L_CHILD_PRICING_TEMP_TABLE(i).selling_uom;
            L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
            L_multi_units       := L_CHILD_PRICING_TEMP_TABLE(i).multi_units;
            L_multi_unit_retail := L_CHILD_PRICING_TEMP_TABLE(i).multi_unit_retail;
            L_multi_selling_uom := L_CHILD_PRICING_TEMP_TABLE(i).multi_selling_uom;
            L_base_ind          := L_CHILD_PRICING_TEMP_TABLE(i).base_ind;
         else
            L_multi_units       := NULL;
            L_multi_unit_retail := NULL;
            L_multi_selling_uom := NULL;
            L_base_ind          := NULL;
            if L_CHILD_PRICING_TEMP_TABLE(i).NEW_UNIT_RETAIL is NULL then
               L_unit_retail    := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail;
               L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).standard_uom;
               L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
            else
               if L_CHILD_PRICING_TEMP_TABLE(i).NEW_CURRENCY_CODE !=
                  L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency then
                  if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                                         L_unit_retail,
                                                         L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency,
                                                         L_CHILD_PRICING_TEMP_TABLE(i).NEW_UNIT_RETAIL,
                                                         L_CHILD_PRICING_TEMP_TABLE(i).NEW_CURRENCY_CODE) = 0 then
                     O_success := 'N';
                     return;
                  end if;
                  ---
                  if RPM_WRAPPER.ROUND_CURRENCY(O_error_message,
                                                L_unit_retail,
                                                L_unit_retail,
                                                L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency,
                                                1) = 0 then
                     O_success := 'N';
                     return;
                  end if;
                  L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
               else
                  L_unit_retail    := L_CHILD_PRICING_TEMP_TABLE(i).NEW_UNIT_RETAIL;
                  L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
               end if;

               if L_CHILD_PRICING_TEMP_TABLE(i).new_standard_uom is NOT NULL then
                  L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).new_standard_uom;
               else
                  L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).standard_uom;
               end if;
            end if;

            if L_CHILD_PRICING_TEMP_TABLE(i).NEW_SELLING_UNIT_RETAIL is NULL then
               L_selling_retail := L_CHILD_PRICING_TEMP_TABLE(i).selling_retail;
               L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).selling_uom;
               L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
            else
               if L_CHILD_PRICING_TEMP_TABLE(i).NEW_CURRENCY_CODE !=
                  L_CHILD_PRICING_TEMP_TABLE(i).selling_retail_currency then
                  if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                                         L_selling_retail,
                                                         L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency,
                                                         L_CHILD_PRICING_TEMP_TABLE(i).NEW_SELLING_UNIT_RETAIL,
                                                         L_CHILD_PRICING_TEMP_TABLE(i).NEW_CURRENCY_CODE) = 0 then
                     O_success := 'N';
                     return;
                  end if;
                  ---
                  if RPM_WRAPPER.ROUND_CURRENCY(O_error_message,
                                                L_selling_retail,
                                                L_selling_retail,
                                                L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency,
                                                1) = 0 then
                     O_success := 'N';
                     return;
                  end if;
                  L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
               else
                  L_selling_retail := L_CHILD_PRICING_TEMP_TABLE(i).new_selling_unit_retail;
                  L_currency_code  := L_CHILD_PRICING_TEMP_TABLE(i).standard_retail_currency;
               end if;

               if L_CHILD_PRICING_TEMP_TABLE(i).new_selling_uom is NOT NULL then
                  L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).new_selling_uom;
               else
                  L_standard_uom   := L_CHILD_PRICING_TEMP_TABLE(i).selling_uom;
               end if;
            end if;
         end if;

         L_item_pricing_table.extend;

         L_item_pricing_table(L_item_pricing_table.count) :=
                                 OBJ_ITEM_PRICING_REC(
                                    L_CHILD_PRICING_TEMP_TABLE(i).item,
                                    L_CHILD_PRICING_TEMP_TABLE(i).zone_group_id,
                                    L_CHILD_PRICING_TEMP_TABLE(i).zone_id,
                                    L_CHILD_PRICING_TEMP_TABLE(i).zone_display_id,
                                    L_CHILD_PRICING_TEMP_TABLE(i).zone_name,
                                    L_unit_retail,
                                    L_selling_retail,
                                    L_selling_uom,
                                    L_multi_units,
                                    L_multi_unit_retail,
                                    L_multi_selling_uom,
                                    L_base_ind,
                                    L_currency_code,
                                    L_standard_uom,
                                    NULL);
      end LOOP;

   end LOOP;

   if SET_AREA_DIFF_PRICE(O_error_message,
                          L_item_pricing_area_diff,
                          L_item_pricing_table) = FALSE then
      O_success := 'N';
      return;
   end if;

   SET_ITEM_PRICING_INFO(O_error_message,
                         O_success,
                         L_item_pricing_table);

   if O_success = 'N' then
      return;
   end if;

   if L_item_pricing_area_diff is NOT NULL and
      L_item_pricing_area_diff.COUNT > 0 then
      SET_ITEM_PRICING_INFO(O_error_message,
                            O_success,
                            L_item_pricing_area_diff);

      if O_success = 'N' then
         return;
      end if;
   end if;

   O_success := 'Y';
   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END SET_CHILD_ITEM_PRICING_INFO;

--------------------------------------------------------------------------------
PROCEDURE LOCK_PRICING_RECORDS
(O_error_message     OUT  VARCHAR2,
 O_success           OUT  VARCHAR2,
 I_item           IN      ITEM_MASTER.ITEM%TYPE
)
IS

   L_program  VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.LOCK_PRICING_RECORDS';

   L_table             VARCHAR2(30)  := 'RPM_ITEM_ZONE_PRICE';
   L_key1              VARCHAR2(100) := I_item;
   L_key2              VARCHAR2(100) := NULL;
   RECORD_LOCKED       EXCEPTION;
   PRAGMA              EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_RPM_ITEM_ZONE_PRICE is
      select 'x'
        from rpm_item_zone_price
       where item = I_item
       for update nowait;

BEGIN

   open C_LOCK_RPM_ITEM_ZONE_PRICE;
   close C_LOCK_RPM_ITEM_ZONE_PRICE;

   ---don't error on no data found

   O_success := 'Y';

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',
                                            L_table,
                                            L_key1,
                                            L_key2);
      O_success := 'N';
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END LOCK_PRICING_RECORDS;

--------------------------------------------------------------------------------
PROCEDURE BUILD_ITEM_PRICING_OBJECT
(O_error_message           OUT  VARCHAR2,
 O_success                 OUT  VARCHAR2,
 O_item_pricing_table      OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
 I_item                 IN      RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
 I_dept                 IN      DEPS.DEPT%TYPE,
 I_class                IN      CLASS.CLASS%TYPE,
 I_subclass             IN      SUBCLASS.SUBCLASS%TYPE,
 I_price_currency_code  IN      RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE,
 I_retail_price         IN      NUMBER
)
IS

   L_program        VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.BUILD_ITEM_PRICING_OBJECT';
   L_invalid_param  VARCHAR2(30);

   L_sort_order     VARCHAR2(1);
   L_reqular_zone_group        RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE;
   L_unit_retail               RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE;
   L_converted_unit_retail     RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE;
   L_rounded_unit_retail       RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE;
   L_guided_unit_retail        RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE;
   ---
   L_standard_uom              ITEM_MASTER.STANDARD_UOM%TYPE;
   L_conv_factor               ITEM_MASTER.UOM_CONV_FACTOR%TYPE;
   L_standard_class            VARCHAR2(30);
   L_retail_currency_code      RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE;
   L_merch_retail_def_id       RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE;

   cursor C_GET_RETAIL_DEF is
      select '1' sort_order,
             regular_zone_group
        from rpm_merch_retail_def
       where NVL(dept,-999)  = I_dept
         and NVL(class,-999) = I_class
         and NVL(subclass,-999) = I_subclass
       UNION
      select '2' sort_order,
             regular_zone_group
        from rpm_merch_retail_def
       where NVL(dept,-999)  = I_dept
         and NVL(class,-999) = I_class
         and subclass is NULL
       UNION
      select '3' sort_order,
             regular_zone_group
        from rpm_merch_retail_def
       where dept = I_dept
         and class is NULL
         and subclass is NULL
       order by sort_order;

   cursor C_GET_ZONES is
      select zone_id,
             zone_display_id,
             zone_group_id,
             name,
             base_ind,
             currency_code
        from rpm_zone
       where zone_group_id = L_reqular_zone_group;

BEGIN

   O_success := 'Y';
   --- Validate parameters
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_price_currency_code is NULL then
      L_invalid_param := 'I_price_currency_code';
   elsif I_retail_price is NULL then
      L_invalid_param := 'I_retail_price';
   end if;
   ---
   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            L_invalid_param,
                                            L_program,
                                            NULL);
      O_success := 'N';
      return;
   end if;

   --- Be sure pricing table is initialized and empty
   if O_item_pricing_table is NULL then
      O_item_pricing_table := OBJ_ITEM_PRICING_TBL();
   else
      O_item_pricing_table.DELETE;
   end if;

   open C_GET_RETAIL_DEF;

   fetch C_GET_RETAIL_DEF
    into L_sort_order,
         L_reqular_zone_group;

   if C_GET_RETAIL_DEF%NOTFOUND then
      close C_GET_RETAIL_DEF;
      O_success := 'N';
      O_error_message := SQL_LIB.CREATE_MSG('NO_MERCH_RETAIL_DEF',NULL,NULL,NULL);
      return;
   end if;

   close C_GET_RETAIL_DEF;

   -- Calculate the retail price for all ZONE_ID

   for reczone in C_GET_ZONES LOOP

      -- Calculate the retail price based on the markup percent
      if I_retail_price = 0 then
         L_unit_retail           := 0;
         L_converted_unit_retail := 0;
         L_rounded_unit_retail   := 0;
      else
         L_unit_retail := I_retail_price;
         --
         -- Convert the Unit Retail to the currency of the Zone
         if reczone.currency_code != I_price_currency_code then
            if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                                   L_converted_unit_retail,
                                                   reczone.currency_code,
                                                   L_unit_retail,
                                                   I_price_currency_code) = 0 then
               O_success := 'N';
               return;
            end if;
         else
            L_converted_unit_retail := L_unit_retail;
         end if;
         --
         if RPM_WRAPPER.ROUND_CURRENCY(O_error_message,
                                       L_rounded_unit_retail,
                                       L_converted_unit_retail,
                                       reczone.currency_code,
                                       1) = 0 then
             O_success := 'N';
             return;
         end if;
      end if;

      -- Populate the Output Table
      O_ITEM_PRICING_TABLE.EXTEND;

      O_ITEM_PRICING_TABLE(O_ITEM_PRICING_TABLE.COUNT) := OBJ_ITEM_PRICING_REC(
                                                            I_item,
                                                            reczone.zone_group_id,
                                                            reczone.zone_id,
                                                            reczone.zone_display_id,
                                                            reczone.name,
                                                            L_rounded_unit_retail,
                                                            L_rounded_unit_retail,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            NULL,
                                                            reczone.base_ind,
                                                            reczone.currency_code,
                                                            NULL,
                                                            0);
   end LOOP;
   ---
   if O_item_pricing_table.COUNT > 0 then
      if SET_AREA_DIFF_ZONE_IND(O_error_message,
                                O_item_pricing_table,
                                I_item) = FALSE then
         O_success := 'N';
         return;
      end if;
   end if;
   ---
   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END BUILD_ITEM_PRICING_OBJECT;

--------------------------------------------------------------------------------
PROCEDURE CHECK_RETAIL_EXISTS
(O_error_message     OUT  VARCHAR2,
 O_success           OUT  VARCHAR2,
 O_exists            OUT  VARCHAR2,
 I_item           IN      ITEM_MASTER.ITEM%TYPE
)
IS

   L_program        VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.CHECK_RETAIL_EXISTS';

   cursor C_RETAIL_EXISTS is
      select 'Y'
        from dual
       where exists (select r.item
                       from rpm_item_zone_price r
                      where r.item = I_item);

BEGIN

   O_exists := 'N';
   open C_RETAIL_EXISTS;
   fetch C_RETAIL_EXISTS into O_exists;
   close C_RETAIL_EXISTS;

   O_success := 'Y';
   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END CHECK_RETAIL_EXISTS;

--------------------------------------------------------------------------------
PROCEDURE COMPARE_RETAIL_DEF
(O_error_message           OUT  VARCHAR2,
 O_success                 OUT  VARCHAR2,
 O_equal                   OUT  VARCHAR2,
 I_new_dept             IN      DEPS.DEPT%TYPE,
 I_new_class            IN      CLASS.CLASS%TYPE,
 I_new_subclass         IN      SUBCLASS.SUBCLASS%TYPE,
 I_old_dept             IN      DEPS.DEPT%TYPE,
 I_old_class            IN      CLASS.CLASS%TYPE,
 I_old_subclass         IN      SUBCLASS.SUBCLASS%TYPE
)
IS

   L_program        VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.COMPARE_RETAIL_DEF';
   L_new_merch_retail_def_id       RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE;
   L_old_merch_retail_def_id       RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE;
   L_dept                          DEPS.DEPT%TYPE;
   L_class                         CLASS.CLASS%TYPE;
   L_subclass                       SUBCLASS.SUBCLASS%TYPE;
   L_sort                          VARCHAR2(1);

   cursor C_GET_RETAIL_DEF is
      select '1' sort_order,
             merch_retail_def_id
        from rpm_merch_retail_def
       where NVL(dept,-999)  = L_dept
         and NVL(class,-999) = L_class
         and NVL(subclass,-999) = L_subclass
       UNION
      select '2' sort_order,
             merch_retail_def_id
        from rpm_merch_retail_def
       where NVL(dept,-999)  = L_dept
         and NVL(class,-999) = L_class
         and subclass is NULL
       UNION
      select '3' sort_order,
             merch_retail_def_id
        from rpm_merch_retail_def
       where dept = L_dept
         and class is NULL
         and subclass is NULL
       order by sort_order;

BEGIN

   O_success := 'Y';
   ---
   L_dept     := I_new_dept;
   L_class    := I_new_class;
   L_subclass := I_new_subclass;

   open C_GET_RETAIL_DEF;

   fetch C_GET_RETAIL_DEF
    into L_sort,
         L_new_merch_retail_def_id;

   close C_GET_RETAIL_DEF;
   ---
   L_dept     := I_old_dept;
   L_class    := I_old_class;
   L_subclass := I_old_subclass;

   open C_GET_RETAIL_DEF;

   fetch C_GET_RETAIL_DEF
    into L_sort,
         L_old_merch_retail_def_id;

   close C_GET_RETAIL_DEF;
   ---
   if NVL(L_new_merch_retail_def_id, -999) = NVL(L_new_merch_retail_def_id, -888) then
      O_equal := 'Y';
   else
      O_equal := 'N';
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END COMPARE_RETAIL_DEF;
--------------------------------------------------------------------------------
PROCEDURE DELETE_RPM_ITEM_ZONE_PRICE
(O_error_message           OUT  VARCHAR2,
 O_success                 OUT  VARCHAR2,
 I_item                 IN      RPM_ITEM_ZONE_PRICE.ITEM%TYPE
)
IS

   L_program        VARCHAR2(61) := 'MERCH_RETAIL_API_SQL.DELETE_RPM_ITEM_ZONE_PRICE';
   L_table          VARCHAR2(30) := 'RPM_ITEM_ZONE_PRICE';
   L_rowid                 ROWID;
   RECORD_LOCKED           EXCEPTION;
   PRAGMA                  EXCEPTION_INIT(Record_Locked, -54);

   cursor C_LOCK_ZONE_PRICE is
   select rowid
     from RPM_ITEM_ZONE_PRICE
    where item = I_item
      for UPDATE NOWAIT;

   cursor C_LOCK_ZONE_FUTURE_RETAIL is
   select rowid
     from RPM_ZONE_FUTURE_RETAIL
    where item = I_item
      for UPDATE NOWAIT;

BEGIN

   O_success := 'Y';
   ---
   open C_LOCK_ZONE_PRICE;
   LOOP

      fetch C_LOCK_ZONE_PRICE
       into L_rowid;

      if C_LOCK_ZONE_PRICE%NOTFOUND then
         exit;
      end if;

      delete RPM_ITEM_ZONE_PRICE
       where rowid = L_rowid;

   END LOOP;
   close C_LOCK_ZONE_PRICE;
   ---
   open C_LOCK_ZONE_FUTURE_RETAIL;
   LOOP

      fetch C_LOCK_ZONE_FUTURE_RETAIL
       into L_rowid;

      if C_LOCK_ZONE_FUTURE_RETAIL%NOTFOUND then
         exit;
      end if;

      delete RPM_ZONE_FUTURE_RETAIL
       where rowid = L_rowid;

   END LOOP;
   close C_LOCK_ZONE_FUTURE_RETAIL;

EXCEPTION
   when RECORD_LOCKED then
      O_error_message := SQL_LIB.CREATE_MSG('TABLE_LOCKED',L_table,
                                            I_item);
      O_success := 'N';
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';
END DELETE_RPM_ITEM_ZONE_PRICE;

--------------------------------------------------------------------------------
FUNCTION GET_MERCH_RETAIL_DEFS(O_error_message            OUT  VARCHAR2,
                               O_merch_retail_def_id      OUT  RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE,
                               O_regular_zone_group       OUT  RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE,
                               O_clearance_zone_group     OUT  RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE,
                               O_markup_calc_type         OUT  RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                               O_markup_percent           OUT  RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE,
                               O_exists                   OUT  VARCHAR2,
                               I_dept                  IN      RPM_MERCH_RETAIL_DEF.DEPT%TYPE,
                               I_class                 IN      RPM_MERCH_RETAIL_DEF.CLASS%TYPE,
                               I_subclass              IN      RPM_MERCH_RETAIL_DEF.SUBCLASS%TYPE)
RETURN BOOLEAN IS

   L_program   VARCHAR2(61)  := 'MERCH_RETAIL_API_SQL.GET_MERCH_RETAIL_DEFS';

   cursor C_GET_RETAIL_DEF is
      select merch_retail_def_id,
             regular_zone_group,
             clearance_zone_group,
             markup_calc_type,
             markup_percent
        from rpm_merch_retail_def
       where dept                      = I_dept
         and nvl(class, I_class)       = I_class
         and nvl(subclass, I_subclass) = I_subclass
       order by dept, class, subclass;

BEGIN

   if I_subclass is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_subclass',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_class is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_class',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;
   if I_dept is NULL then
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM',
                                            'I_dept',
                                            'NULL',
                                            'NOT NULL');
      return FALSE;
   end if;

   open C_GET_RETAIL_DEF;
   fetch C_GET_RETAIL_DEF into O_merch_retail_def_id,
                               O_regular_zone_group,
                               O_clearance_zone_group,
                               O_markup_calc_type,
                               O_markup_percent;

   if C_GET_RETAIL_DEF%NOTFOUND then
      O_exists := 'N';
   else
      O_exists := 'Y';
   end if;

   close C_GET_RETAIL_DEF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MERCH_RETAIL_DEFS;

--------------------------------------------------------------------------------
FUNCTION APPLY_PRICE_GUIDES (O_error_message           OUT  VARCHAR2,
                             O_guided_retail           OUT  RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                             I_unit_retail          IN      RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                             I_currency             IN      RPM_PRICE_GUIDE.CURRENCY_CODE%TYPE,
                             I_merch_retail_def_id  IN      RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE,
                             I_price_guide_id       IN      RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE)
   RETURN BOOLEAN IS

   L_program   VARCHAR2(75)  := 'MERCH_RETAIL_API_SQL.APPLY_PRICE_GUIDES';

   cursor C_PRICE_GUIDE is
      select rpgi.new_price_val
        from rpm_price_guide rpg,
             rpm_merch_price_guide_link rml,
             rpm_price_guide_interval rpgi
       where rpg.currency_code       = I_currency
         and rml.price_guide_id      = rpg.price_guide_id
         and rml.merch_retail_def_id = I_merch_retail_def_id
         and rpgi.price_guide_id     = rpg.price_guide_id
         and I_unit_retail          >= from_price_val
         and I_unit_retail          <= to_price_val;

   cursor C_PRICE_GUIDE_TOO is
      select rpgi.new_price_val
        from rpm_price_guide rpg,
             rpm_price_guide_interval rpgi
       where rpg.currency_code       = I_currency
         and rpg.price_guide_id      = I_price_guide_id
         and rpgi.price_guide_id     = rpg.price_guide_id
         and I_unit_retail          >= from_price_val
         and I_unit_retail          <= to_price_val;


BEGIN

   if I_price_guide_id is NULL then
      open C_PRICE_GUIDE;
      fetch C_PRICE_GUIDE into O_guided_retail;

      if C_PRICE_GUIDE%NOTFOUND then
         O_guided_retail := I_unit_retail;
      end if;

      close C_PRICE_GUIDE;
   else
      open C_PRICE_GUIDE_TOO;
      fetch C_PRICE_GUIDE_TOO into O_guided_retail;

      if C_PRICE_GUIDE_TOO%NOTFOUND then
         O_guided_retail := I_unit_retail;
      end if;

      close C_PRICE_GUIDE_TOO;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END APPLY_PRICE_GUIDES;

--------------------------------------------------------------------------------
FUNCTION CALCULATE_NON_RANGED_RETAIL (O_error_message           OUT  VARCHAR2,
                                      O_selling_retail          OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                                      O_selling_uom             OUT  RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                                      O_selling_retail_curr     OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                                      I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                                      I_dept                 IN      ITEM_MASTER.DEPT%TYPE,
                                      I_merch_retail_def_id  IN      RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE,
                                      I_markup_calc_type     IN      RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE,
                                      I_markup_percent       IN      RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE,
                                      I_location             IN      ITEM_LOC.LOC%TYPE,
                                      I_loc_type             IN      ITEM_LOC.LOC_TYPE%TYPE,
                                      I_loc_currency         IN      STORE.CURRENCY_CODE%TYPE,
                                      I_unit_cost            IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                                      I_supp_currency        IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(50) := 'MERCH_RETAIL_API_SQL.CALCULATE_NON_RANGED_RETAIL';
   ---
   L_selling_retail          RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := NULL;

   L_item_record             ITEM_MASTER%ROWTYPE                     := NULL;

   L_selling_uom             RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE    := NULL;
   L_standard_class          UOM_CLASS.UOM_CLASS%TYPE                := NULL;
   L_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE        := NULL;

   L_multi_curr_exists       BOOLEAN                                 := FALSE;
   L_conv_selling_retail     RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := NULL;
   L_rounded_selling_retail  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := NULL;
   L_guided_selling_retail   RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE := NULL;

   L_class                   CLASS.CLASS%TYPE                        := NULL;
   L_vdate                   DATE                                    := get_vdate();

BEGIN

   -- Get Item's Standard UOM
   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER (O_error_message,
                                       L_item_record,
                                       I_item) = FALSE then
      return FALSE;
   end if;

   -- Check if the item has no cost or Zero cost
   if I_unit_cost = 0 or
      I_unit_cost is NULL then
      O_selling_retail      := 0;
      O_selling_uom         := L_item_record.standard_uom;
      O_selling_retail_curr := I_loc_currency;

      return TRUE;
   end if;

   -- Calculate the retail from markup
   if CALC_RETAIL_FROM_MARKUP (O_error_message,
                               L_selling_retail,
                               I_markup_percent/100,
                               I_markup_calc_type,
                               I_unit_cost) = FALSE then
      return FALSE;
   end if;

   -- If the supplier currency is different from the location currency, convert the
   -- selling retail to location currency (since Item's cost is in supplier currency)
   if I_loc_currency != I_supp_currency then
      if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                             L_conv_selling_retail,
                                             I_loc_currency,
                                             L_selling_retail,
                                             I_supp_currency) = 0 then
         return FALSE;
      end if;
   else
      L_conv_selling_retail := L_selling_retail;
   end if;


   -- Apply TAX to Item's retail
   if ADD_TAX(O_error_message,
              I_item,
              L_item_record.dept,
              L_item_record.class,
              L_item_record.pack_ind,
              I_location,
              I_loc_type,
              I_loc_currency,
              L_vdate,
              L_conv_selling_retail) = FALSE then
      return FALSE;
   end if;

   -- Get the rounded value for Selling Retail
   if RPM_WRAPPER.ROUND_CURRENCY (O_error_message,
                                  L_rounded_selling_retail,
                                  L_conv_selling_retail,
                                  I_loc_currency,
                                  1) = 0 then
      return FALSE;
   end if;

   -- Apply Price Guides to Item's retail
   if APPLY_PRICE_GUIDES (O_error_message,
                          L_guided_selling_retail,
                          L_rounded_selling_retail,
                          I_loc_currency,
                          I_merch_retail_def_id,
                          NULL) = FALSE then
      return FALSE;
   end if;

   -- Assign Output parameters with the final values
   O_selling_retail      := L_guided_selling_retail;
   O_selling_uom         := L_item_record.standard_uom;
   O_selling_retail_curr := I_loc_currency;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END CALCULATE_NON_RANGED_RETAIL;

--------------------------------------------------------------------------------
FUNCTION GET_NON_RANGED_ITEM_RETAIL (O_error_message         OUT  VARCHAR2,
                                     O_selling_retail        OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                                     O_selling_uom           OUT  RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                                     O_multi_units           OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNITS%TYPE,
                                     O_multi_unit_retail     OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL%TYPE,
                                     O_multi_selling_uom     OUT  RPM_ITEM_ZONE_PRICE.MULTI_SELLING_UOM%TYPE,
                                     O_currency_code         OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                                     I_item               IN      ITEM_MASTER.ITEM%TYPE,
                                     I_dept               IN      ITEM_MASTER.DEPT%TYPE,
                                     I_class              IN      ITEM_MASTER.CLASS%TYPE,
                                     I_subclass           IN      ITEM_MASTER.SUBCLASS%TYPE,
                                     I_location           IN      ITEM_LOC.LOC%TYPE,
                                     I_loc_type           IN      ITEM_LOC.LOC_TYPE%TYPE,
                                     I_unit_cost          IN      ITEM_SUPP_COUNTRY.UNIT_COST%TYPE  DEFAULT NULL,
                                     I_currency           IN      SUPS.CURRENCY_CODE%TYPE           DEFAULT NULL)
   RETURN NUMBER IS

   L_program                 VARCHAR2(75)                                   := 'MERCH_RETAIL_API_SQL.GET_NON_RANGED_ITEM_RETAIL';
   L_invalid_param           VARCHAR2(30)                                   := NULL;

   L_merch_retail_def_id     RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE  := NULL;
   L_regular_zone_group      RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE   := NULL;
   L_clearance_zone_group    RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE := NULL;
   L_markup_calc_type        RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE     := NULL;
   L_markup_percent          RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE       := NULL;
   L_exists                  VARCHAR2(1)                                    := NULL;
   L_dept_markup_calc_type   DEPS.MARKUP_CALC_TYPE%TYPE                     := NULL;
   L_cost_markup_pct         DEPS.BUD_MKUP%TYPE                             := NULL;
   L_retail_markup_pct       DEPS.BUD_INT%TYPE                              := NULL;
   L_zone_id                 RPM_ZONE_LOCATION.ZONE_ID%TYPE                 := NULL;
   L_conv_factor             ITEM_MASTER.UOM_CONV_FACTOR%TYPE               := NULL;
   L_multi_curr_exists       BOOLEAN                                        := FALSE;
   L_loc_currency            STORE.CURRENCY_CODE%TYPE                       := NULL;
   L_supplier                ITEM_SUPP_COUNTRY.SUPPLIER%TYPE                := NULL;
   L_item_cost               ITEM_SUPP_COUNTRY.UNIT_COST%TYPE               := NULL;
   L_supp_currency           SUPS.CURRENCY_CODE%TYPE                        := NULL;

   cursor C_LOCATION_ZONE is
      select rzl.zone_id
        from rpm_zone_location rzl,
             rpm_zone rz
       where rz.zone_id       = rzl.zone_id
         and rzl.location     = I_location
         and rz.zone_group_id = L_regular_zone_group;

   cursor C_ZONE_RETAIL is
      select selling_retail,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             selling_retail_currency
        from rpm_item_zone_price
       where item    = I_item
         and zone_id = L_zone_id;

BEGIN

   -- Validate input parameters
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_class';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_subclass';
   elsif I_location is NULL then
      L_invalid_param := 'I_location';
   elsif I_loc_type is NULL then
      L_invalid_param := 'I_loc_type';
   elsif I_unit_cost is not NULL and I_currency is NULL then
      L_invalid_param := 'I_currency';
   end if;

   if L_invalid_param is not NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             L_invalid_param,
                                             L_program,
                                             NULL);
      return 0;
   end if;

   -- Get Merch Retail Defs
   if GET_MERCH_RETAIL_DEFS (O_error_message,
                             L_merch_retail_def_id,
                             L_regular_zone_group,
                             L_clearance_zone_group,
                             L_markup_calc_type,
                             L_markup_percent,
                             L_exists,
                             I_dept,
                             I_class,
                             I_subclass) = FALSE then
      return 0;
   end if;

   if L_exists = 'Y' then
      -- Get the Zone for the input location
      open C_LOCATION_ZONE;
      fetch C_LOCATION_ZONE into L_zone_id;
      close C_LOCATION_ZONE;

      -- Get the item's retail at the input location's zone
      open C_ZONE_RETAIL;
      fetch C_ZONE_RETAIL into O_selling_retail,
                               O_selling_uom,
                               O_multi_units,
                               O_multi_unit_retail,
                               O_multi_selling_uom,
                               O_currency_code;
      if C_ZONE_RETAIL%FOUND then
         close C_ZONE_RETAIL;
         return 1;
      end if;
      close C_ZONE_RETAIL;
   end if;

   -- If Item's retail is not found in Zone_future_retail, calculate it by:
   --    a) Getting Item Cost
   --    b) Applying markup %
   --    c) Applying VAT, if required
   --    d) Applying price guides, if any exist

   -- Get Department's markup calc_type and percent, if NOT
   -- already found in the item's primary zone group
   if L_markup_percent is NULL then
      if DEPT_ATTRIB_SQL.GET_MARKUP (O_error_message,
                                     L_dept_markup_calc_type,
                                     L_retail_markup_pct,
                                     L_cost_markup_pct,
                                     I_dept) = FALSE then
         return 0;
      else
         if L_dept_markup_calc_type = 'R' then
            L_markup_calc_type := RETAIL_MARKUP_CALC_TYPE;
            L_markup_percent := L_retail_markup_pct;
         elsif L_dept_markup_calc_type = 'C' then
            L_markup_calc_type := COST_MARKUP_CALC_TYPE;
            L_markup_percent := L_cost_markup_pct;
         end if;
      end if;
   end if;

   -- Get the location's currency
   if LOCATION_ATTRIB_SQL.GET_LOCATION_CURRENCY (O_error_message,
                                                 L_multi_curr_exists,
                                                 L_loc_currency,
                                                 I_loc_type,
                                                 I_location) = FALSE then
      return 0;
   end if;

   if I_unit_cost is NULL then
      -- Get the Item cost
      if SUPP_ITEM_SQL.GET_PRI_SUP_COST (O_error_message,
                                         L_supplier,
                                         L_item_cost,
                                         I_item,
                                         I_location) = FALSE then
         return 0;
      elsif L_item_cost is NULL then
         return 1;
      end if;

      -- Get the supplier currency
      if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE (O_error_message,
                                            L_supp_currency,
                                            L_supplier) = FALSE then
         return 0;
      end if;
   else
      L_item_cost     := I_unit_cost;
      L_supp_currency := I_currency;
   end if;

   -- Call private function to calculate retail
   if CALCULATE_NON_RANGED_RETAIL (O_error_message,
                                   O_selling_retail,
                                   O_selling_uom,
                                   O_currency_code,
                                   I_item,
                                   I_dept,
                                   L_merch_retail_def_id,
                                   L_markup_calc_type,
                                   L_markup_percent,
                                   I_location,
                                   I_loc_type,
                                   L_loc_currency,
                                   L_item_cost,
                                   L_supp_currency) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return 0;

END GET_NON_RANGED_ITEM_RETAIL;

--------------------------------------------------------------------------------
FUNCTION GET_BASE_ZONE_RETAIL (O_error_message         OUT  VARCHAR2,
                               O_zone_group            OUT  RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE,
                               O_base_zone             OUT  RPM_ZONE.ZONE_ID%TYPE,
                               O_selling_retail        OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL%TYPE,
                               O_selling_uom           OUT  RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                               O_multi_units           OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNITS%TYPE,
                               O_multi_unit_retail     OUT  RPM_ITEM_ZONE_PRICE.MULTI_UNIT_RETAIL%TYPE,
                               O_multi_selling_uom     OUT  RPM_ITEM_ZONE_PRICE.MULTI_SELLING_UOM%TYPE,
                               O_currency_code         OUT  RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                               I_item               IN      ITEM_MASTER.ITEM%TYPE,
                               I_dept               IN      ITEM_MASTER.DEPT%TYPE,
                               I_class              IN      ITEM_MASTER.CLASS%TYPE,
                               I_subclass           IN      ITEM_MASTER.SUBCLASS%TYPE)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(75)                                   := 'MERCH_RETAIL_API_SQL.GET_BASE_ZONE_RETAIL';
   L_invalid_param           VARCHAR2(30)                                   := NULL;
   L_vdate                   DATE                                           := GET_VDATE;

   L_item_level              ITEM_MASTER.ITEM_LEVEL%TYPE                    := NULL;
   L_tran_level              ITEM_MASTER.TRAN_LEVEL%TYPE                    := NULL;
   L_merch_retail_def_id     RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE  := NULL;
   L_regular_zone_group      RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE   := NULL;
   L_clearance_zone_group    RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE := NULL;
   L_markup_calc_type        RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE     := NULL;
   L_markup_percent          RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE       := NULL;
   L_exists                  VARCHAR2(1)                                    := NULL;
   L_dept_markup_calc_type   DEPS.MARKUP_CALC_TYPE%TYPE                     := NULL;
   L_cost_markup_pct         DEPS.BUD_MKUP%TYPE                             := NULL;
   L_retail_markup_pct       DEPS.BUD_INT%TYPE                              := NULL;
   L_base_zone               RPM_ZONE_LOCATION.ZONE_ID%TYPE                 := NULL;
   L_zone_currency           RPM_ZONE.CURRENCY_CODE%TYPE                    := NULL;
   L_loc                     RPM_FUTURE_RETAIL.LOCATION%TYPE                := NULL;
   L_loc_type                VARCHAR2(1)                                    := NULL;
   L_loc_currency            STORE.CURRENCY_CODE%TYPE                       := NULL;
   L_supplier                ITEM_SUPP_COUNTRY.SUPPLIER%TYPE                := NULL;
   L_item_cost               ITEM_SUPP_COUNTRY.UNIT_COST%TYPE               := NULL;
   L_supp_currency           SUPS.CURRENCY_CODE%TYPE                        := NULL;

   cursor C_BASE_ZONE is
      select zone_id,
             currency_code
        from rpm_zone
       where zone_group_id = L_regular_zone_group
         and base_ind      = 1;

   cursor C_BASE_ZONE_RETAIL is
      select selling_retail,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             selling_retail_currency
        from rpm_item_zone_price
       where item    = I_item
         and zone_id = L_base_zone;

   cursor C_BASE_ZONE_LOC_RETAIL is
      select rfr.selling_retail,
             rfr.selling_uom,
             rfr.multi_units,
             rfr.multi_unit_retail,
             rfr.multi_selling_uom,
             rfr.selling_retail_currency
        from rpm_future_retail rfr,
             rpm_zone_location rzl
       where rfr.item                = I_item
         and rfr.location            = rzl.location
         and TRUNC(rfr.action_date) <= L_vdate
         and rzl.zone_id             = L_base_zone
       order by rfr.location;

   cursor C_ZONE_LOC is
      select location,
             DECODE(loc_type,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                    RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type
        from rpm_zone_location
       where zone_id = L_base_zone
       order by location;

   cursor C_GET_ANY_STORE is
      select store,
             RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
             currency_code
        from store
       order by store;

BEGIN

   -- Validate input parameters
   if I_item is NULL then
      L_invalid_param := 'I_item';
   elsif I_dept is NULL then
      L_invalid_param := 'I_dept';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_class';
   elsif I_subclass is NULL then
      L_invalid_param := 'I_subclass';
   end if;

   if L_invalid_param is NOT NULL then
      O_error_message := SQL_LIB.CREATE_MSG ('INVALID_PARM',
                                             L_invalid_param,
                                             L_program,
                                             NULL);
      return FALSE;
   end if;

   -- Get Merch Retail Defs
   if GET_MERCH_RETAIL_DEFS (O_error_message,
                             L_merch_retail_def_id,
                             L_regular_zone_group,
                             L_clearance_zone_group,
                             L_markup_calc_type,
                             L_markup_percent,
                             L_exists,
                             I_dept,
                             I_class,
                             I_subclass) = FALSE then
      return FALSE;
   end if;

   if L_exists = 'Y' then
      -- Get Base zone for Item's primary zone group
      open C_BASE_ZONE;
      fetch C_BASE_ZONE into L_base_zone,
                             L_zone_currency;
      close C_BASE_ZONE;

      -- Return Item's Primary zone group and the Base zone for the Primary Zone Group
      O_zone_group := L_regular_zone_group;
      O_base_zone  := L_base_zone;

      -- Get the Item's retail at the Base zone for the primary zone group
      open C_BASE_ZONE_RETAIL;
      fetch C_BASE_ZONE_RETAIL into O_selling_retail,
                                    O_selling_uom,
                                    O_multi_units,
                                    O_multi_unit_retail,
                                    O_multi_selling_uom,
                                    O_currency_code;
      if C_BASE_ZONE_RETAIL%FOUND then
         close C_BASE_ZONE_RETAIL;
         return TRUE;
      end if;
      close C_BASE_ZONE_RETAIL;

      -- If Item's retail at the Base zone is not found in Zone_future_retail, get the
      -- Item's retail at the first location that it is ranged at in the Base zone
      open C_BASE_ZONE_LOC_RETAIL;
      fetch C_BASE_ZONE_LOC_RETAIL into O_selling_retail,
                                        O_selling_uom,
                                        O_multi_units,
                                        O_multi_unit_retail,
                                        O_multi_selling_uom,
                                        O_currency_code;
      if C_BASE_ZONE_LOC_RETAIL%FOUND then
         close C_BASE_ZONE_LOC_RETAIL;
         return TRUE;
      end if;
      close C_BASE_ZONE_LOC_RETAIL;
   end if;

   -- Function should return if the item level is below the transaction level
   if ITEM_ATTRIB_SQL.GET_LEVELS (O_error_message,
                                  L_item_level,
                                  L_tran_level,
                                  I_item) = FALSE then
      return FALSE;
   else
      if L_item_level > L_tran_level then
         return TRUE;
      end if;
   end if;

   -- If Item is not ranged at any location in the Base zone, calculate the Item's retail by:
   --    a) Getting Item Cost
   --    b) Applying markup %
   --    c) Applying VAT, if required (use any Store in the Base zone to get the VAT region
   --       and if the base zone does not have stores, use the the VAT region for any store)
   --    d) Applying price guides, if any exist

   -- Get Department's markup calc_type and percent, if NOT
   -- already found in the item's primary zone group
   if L_markup_percent is NULL then
      if DEPT_ATTRIB_SQL.GET_MARKUP (O_error_message,
                                     L_dept_markup_calc_type,
                                     L_retail_markup_pct,
                                     L_cost_markup_pct,
                                     I_dept) = FALSE then
         return FALSE;
      else
         if L_dept_markup_calc_type = 'R' then
            L_markup_calc_type := RETAIL_MARKUP_CALC_TYPE;
            L_markup_percent := L_retail_markup_pct;
         elsif L_dept_markup_calc_type = 'C' then
            L_markup_calc_type := COST_MARKUP_CALC_TYPE;
            L_markup_percent := L_cost_markup_pct;
         end if;
      end if;
   end if;

   -- Fetch a Store for tax calcs
   open C_ZONE_LOC;
   fetch C_ZONE_LOC into L_loc,
                         L_loc_type;
   if C_ZONE_LOC%NOTFOUND then
      open C_GET_ANY_STORE;
      fetch C_GET_ANY_STORE into L_loc,
                                 L_loc_type,
                                 L_loc_currency;
      close C_GET_ANY_STORE;
   end if;
   close C_ZONE_LOC;

   -- Get the Item cost
   if SUPP_ITEM_SQL.GET_PRI_SUP_COST (O_error_message,
                                      L_supplier,
                                      L_item_cost,
                                      I_item,
                                      NULL) = FALSE then
      return FALSE;
   elsif L_item_cost is NULL then
      return TRUE;
   end if;

   -- Get the supplier currency
   if SUPP_ATTRIB_SQL.GET_CURRENCY_CODE (O_error_message,
                                         L_supp_currency,
                                         L_supplier) = FALSE then
      return FALSE;
   end if;

   -- Call private function to calculate retail
   if CALCULATE_NON_RANGED_RETAIL (O_error_message,
                                   O_selling_retail,
                                   O_selling_uom,
                                   O_currency_code,
                                   I_item,
                                   I_dept,
                                   L_merch_retail_def_id,
                                   L_markup_calc_type,
                                   L_markup_percent,
                                   L_loc,
                                   L_loc_type,
                                   NVL(L_zone_currency, L_loc_currency),
                                   L_item_cost,
                                   L_supp_currency) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END GET_BASE_ZONE_RETAIL;

--------------------------------------------------------------------------------
FUNCTION AREA_DIFF_EXISTS (O_error_message           OUT  VARCHAR2,
                           O_area_diff_exists        OUT  NUMBER,
                           I_item                 IN      ITEM_MASTER.ITEM%TYPE,
                           I_zone_group_id        IN      RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE)
   RETURN BOOLEAN IS

   L_program                 VARCHAR2(75)                                   := 'MERCH_RETAIL_API_SQL.AREA_DIFF_EXISTS';
   L_dept                    ITEM_MASTER.DEPT%TYPE;
   L_class                   ITEM_MASTER.CLASS%TYPE;
   L_subclass                ITEM_MASTER.SUBCLASS%TYPE;

   cursor C_MERCH is
   select dept,
          class,
          subclass
     from item_master
    where item = I_item;

   cursor C_AREA_DIFF is
   select 1
     from rpm_area_diff_prim
    where ZONE_HIER_ID in (select zone_id
                             from rpm_zone
                            where zone_group_id = I_zone_group_id)
      and ((dept = L_dept
        and class is null
        and subclass is null)
        or (dept = L_dept
        and class = L_class
        and subclass is null)
        or (dept = L_dept
        and class = L_class
        and subclass = L_subclass));

BEGIN

   open C_MERCH;
   fetch C_MERCH
    into L_dept,
         L_class,
         L_subclass;

   if C_MERCH%NOTFOUND then
      O_error_message := 'Invalid Item : '||I_item;
      close C_MERCH;
      return FALSE;
   end if;
   close C_MERCH;

   open C_AREA_DIFF;

   fetch C_AREA_DIFF
    into O_area_diff_exists;

   if C_AREA_DIFF%NOTFOUND then
      O_area_diff_exists := 0;
   end if;

   close C_AREA_DIFF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END AREA_DIFF_EXISTS;

--------------------------------------------------------------------------------
FUNCTION IS_PRIMARY_AREA(O_error_message        OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         O_primary              OUT NUMBER,
                         O_area_diff_prim_id    OUT RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE,
                         I_item              IN     ITEM_MASTER.ITEM%TYPE,
                         I_zone_id           IN     RPM_ZONE.ZONE_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(45) := 'MERCH_RETAIL_API_SQL.IS_PRIMARY_AREA';

   L_dept     ITEM_MASTER.DEPT%TYPE     := NULL;
   L_class    ITEM_MASTER.CLASS%TYPE    := NULL;
   L_subclass ITEM_MASTER.SUBCLASS%TYPE := NULL;

   cursor C_MERCH is
      select dept,
             class,
             subclass
        from item_master
       where item = I_item;

   cursor C_AREA_DIFF is
      select area_diff_prim_id
        from rpm_area_diff_prim
       where zone_hier_id = I_zone_id
         and (   (    dept     = L_dept
                  and class    is NULL
                  and subclass is NULL)
              or (    dept     = L_dept
                  and class    = L_class
                  and subclass is NULL)
              or (    dept     = L_dept
                  and class    = L_class
                  and subclass = L_subclass))
   order by dept,
            NVL(class, 9999999999),
            NVL(subclass, 9999999999);

BEGIN

   open C_MERCH;
   fetch C_MERCH into L_dept,
                      L_class,
                      L_subclass;

   if C_MERCH%NOTFOUND then
      O_error_message := 'Invalid Item : '||I_item;
      close C_MERCH;
      return FALSE;
   end if;

   close C_MERCH;

   open C_AREA_DIFF;
   fetch C_AREA_DIFF into O_area_diff_prim_id;

   if C_AREA_DIFF%NOTFOUND then
      O_primary := 0;
   else
      O_primary := 1;
   end if;

   close C_AREA_DIFF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END IS_PRIMARY_AREA;

--------------------------------------------------------------------------------
FUNCTION SET_AREA_DIFF_ZONE_IND(O_error_msg               OUT  rtk_errors.rtk_text%TYPE,
                                O_item_pricing_table   IN OUT  NOCOPY OBJ_ITEM_PRICING_TBL,
                                I_item                 IN      item_master.item%TYPE)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(75)                                   := 'MERCH_RETAIL_API_SQL.SET_AREA_DIFF_ZONE_IND';

   L_area_diff_prim_id         RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE;
   L_zone_id                   RPM_ZONE.ZONE_ID%TYPE;
   L_area_diff_exists          NUMBER(1);
   L_zone_group_id             RPM_ZONE.ZONE_GROUP_ID%TYPE;
   ---
   L_percent_apply_type        rpm_area_diff.PERCENT_APPLY_TYPE%TYPE;
   L_percent                   rpm_area_diff.PERCENT%TYPE;
   L_price_guide_id            rpm_area_diff.PRICE_GUIDE_ID%TYPE;
   ---
   L_secondary_area            NUMBER(1);
   L_primary                   NUMBER(1);
   ---
   L_dept                      ITEM_MASTER.DEPT%TYPE;
   L_class                     ITEM_MASTER.CLASS%TYPE;
   L_subclass                  ITEM_MASTER.SUBCLASS%TYPE;

   cursor C_MERCH is
   select dept,
          class,
          subclass
     from item_master
    where item = I_item;

   cursor C_secondary is
   select 1
     from rpm_area_diff
    where area_diff_prim_id in (select AREA_DIFF_PRIM_ID
                                  from rpm_area_diff_prim
                                 where ZONE_HIER_ID in (select zone_id
                                                          from rpm_zone
                                                         where zone_group_id = L_zone_group_id)
                                                           and ((dept = L_dept
                                                             and class is null
                                                             and subclass is null)
                                                             or (dept = L_dept
                                                             and class = L_class
                                                             and subclass is null)
                                                             or (dept = L_dept
                                                             and class = L_class
                                                             and subclass = L_subclass)))
      and zone_hier_id = L_zone_id;

BEGIN

   -- Check if an Area Differential is defined for the Item
   if AREA_DIFF_EXISTS (O_error_msg,
                        L_area_diff_exists,
                        I_item,
                        O_item_pricing_table(1).zone_group_id) = FALSE then
      return FALSE;
   end if;
   ---
   open C_MERCH;
   fetch C_MERCH
    into L_dept,
         L_class,
         L_subclass;

   if C_MERCH%NOTFOUND then
      O_error_msg := 'Invalid Item : '||I_item;
      close C_MERCH;
      return FALSE;
   end if;
   close C_MERCH;
   ---
   for i in 1..O_item_pricing_table.count LOOP

      if IS_PRIMARY_AREA(O_error_msg,
                         L_primary,
                         L_area_diff_prim_id,
                         I_item,
                         O_item_pricing_table(i).zone_id) = FALSE then
         return FALSE;
      end if;

      if L_primary = 1 then
         O_item_pricing_table(i).AREA_DIFF_ZONE_IND := 1;
      else
         L_zone_group_id := O_item_pricing_table(i).zone_group_id;
         L_zone_id := O_item_pricing_table(i).zone_id;

         open C_secondary;

       fetch C_secondary
          into L_secondary_area;

         if C_secondary%NOTFOUND then
            O_item_pricing_table(i).AREA_DIFF_ZONE_IND := 0;
         else
            O_item_pricing_table(i).AREA_DIFF_ZONE_IND := 2;
         end if;

       close C_secondary;

      end if;

   end LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;

END SET_AREA_DIFF_ZONE_IND;
--------------------------------------------------------------------------------
PROCEDURE GET_AREA_DIFF_PRICE (O_error_message         OUT VARCHAR2,
                               O_success               OUT VARCHAR2,
                               O_selling_retail        OUT RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                               I_zone_group_id      IN     RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE,
                               I_item               IN     RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                               I_sec_zone_id        IN     RPM_ZONE.ZONE_ID%TYPE,
                               I_sec_currency_code  IN     RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE,
                               I_prm_zone_id        IN     RPM_PROMO_DTL_MERCH_NODE.ITEM%TYPE,
                               I_prm_selling_retail IN     RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE,
                               I_prm_selling_uom    IN     RPM_ITEM_ZONE_PRICE.SELLING_UOM%TYPE,
                               I_prm_currency_code  IN     RPM_ITEM_ZONE_PRICE.SELLING_RETAIL_CURRENCY%TYPE)
IS

   L_program  VARCHAR2(50) := 'MERCH_RETAIL_API_SQL.GET_AREA_DIFF_PRICE';

   L_dept     ITEM_MASTER.DEPT%TYPE     := NULL;
   L_class    ITEM_MASTER.CLASS%TYPE    := NULL;
   L_subclass ITEM_MASTER.SUBCLASS%TYPE := NULL;

   L_area_diff_prim_id RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE := NULL;
   L_area_diff_exists  NUMBER(1)                                 := NULL;
   L_secondary_area    NUMBER(1)                                 := NULL;

   L_merch_retail_def_id  RPM_MERCH_RETAIL_DEF.MERCH_RETAIL_DEF_ID%TYPE  := NULL;
   L_regular_zone_group   RPM_MERCH_RETAIL_DEF.REGULAR_ZONE_GROUP%TYPE   := NULL;
   L_clearance_zone_group RPM_MERCH_RETAIL_DEF.CLEARANCE_ZONE_GROUP%TYPE := NULL;
   L_markup_calc_type     RPM_MERCH_RETAIL_DEF.MARKUP_CALC_TYPE%TYPE     := NULL;
   L_markup_percent       RPM_MERCH_RETAIL_DEF.MARKUP_PERCENT%TYPE       := NULL;
   L_exists               VARCHAR2(1)                                    := NULL;

   L_percent_apply_type RPM_AREA_DIFF.PERCENT_APPLY_TYPE%TYPE := NULL;
   L_percent            RPM_AREA_DIFF.PERCENT%TYPE            := NULL;
   L_price_guide_id     RPM_AREA_DIFF.PRICE_GUIDE_ID%TYPE     := NULL;

   L_unit_retail           RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE := NULL;
   L_converted_unit_retail RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE := NULL;
   L_rounded_unit_retail   RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE := NULL;
   L_guided_unit_retail    RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE := NULL;

   L_primary NUMBER(1) := NULL;

   cursor C_DEPT is
      select dept,
             class,
             subclass
        from item_master
       where item = I_item;

   cursor C_SECONDARY_ZONE is
      select 1,
             rad.percent_apply_type,
             rad.percent,
             rad.price_guide_id
        from rpm_area_diff rad
       where rad.area_diff_prim_id IN (select area_diff_prim_id
                                         from (select area_diff_prim_id
                                                 from rpm_area_diff_prim rap
                                                where (   (    rap.dept     = L_dept
                                                           and rap.class    is NULL
                                                           and rap.subclass is NULL)
                                                       or (    rap.dept     = L_dept
                                                           and rap.class    = L_class
                                                           and rap.subclass is NULL)
                                                       or (    rap.dept     = L_dept
                                                           and rap.class    = L_class
                                                           and rap.subclass = L_subclass))
                                                order by dept,
                                                         NVL(class, 9999999999),
                                                         NVL(subclass, 9999999999))
                                        where rownum = 1)
         and zone_hier_id = I_sec_zone_id;

BEGIN

   open C_DEPT;
   fetch C_DEPT into L_dept,
                     L_class,
                     L_subclass;
   close C_DEPT;

   -- Check if an Area Differential is defined for the Department
   if AREA_DIFF_EXISTS (O_error_message,
                        L_area_diff_exists,
                        I_item,
                        I_zone_group_id) = FALSE then
      O_success := 'N';
      return;
   end if;

   if L_area_diff_exists != 1 then
      O_success        := 'Y';
      O_selling_retail := NULL;
      return;
   end if;

   if IS_PRIMARY_AREA(O_error_message,
                      L_primary,
                      L_area_diff_prim_id,
                      I_item,
                      I_prm_zone_id) = FALSE then
      O_success := 'N';
      return;
   end if;

   if L_primary != 1 then
      O_success        := 'Y';
      O_selling_retail := NULL;
     return;
   end if;

   open C_SECONDARY_ZONE;
   fetch C_SECONDARY_ZONE into L_secondary_area,
                               L_percent_apply_type,
                               L_percent,
                               L_price_guide_id;

   if C_SECONDARY_ZONE%NOTFOUND then
      L_secondary_area := 0;
   end if;

   close C_SECONDARY_ZONE;

   if L_secondary_area != 1 then
      O_success        := 'Y';
      O_selling_retail := NULL;
      return;
   end if;

   if GET_MERCH_RETAIL_DEFS(O_error_message,
                            L_merch_retail_def_id,
                            L_regular_zone_group,
                            L_clearance_zone_group,
                            L_markup_calc_type,
                            L_markup_percent,
                            L_exists,
                            L_dept,
                            L_class,
                            L_subclass) = FALSE then
      O_success := 'N';
      return;
   end if;

   if L_exists = 'N' then
      O_error_message := SQL_LIB.CREATE_MSG('NO_MERCH_RETAIL_DEF',
                                            NULL,
                                            NULL,
                                            NULL);
      O_success := 'N';
      return;
   end if;

   L_unit_retail := I_prm_selling_retail;

   if L_percent_apply_type = 0 then
      L_unit_retail := L_unit_retail * (1 + L_percent / 100);
   elsif L_percent_apply_type = 1 then
      L_unit_retail := L_unit_retail * (1 - L_percent / 100);
   end if;

   -- Convert the Unit Retail to the currency of the Zone
   if I_sec_currency_code != I_prm_currency_code then
      if RPM_WRAPPER.CURRENCY_CONVERT_VALUE (O_error_message,
                                             L_converted_unit_retail,
                                             I_sec_currency_code,
                                             L_unit_retail,
                                             I_prm_currency_code) = 0 then
         O_success := 'N';
         return;
      end if;
   else
      L_converted_unit_retail := L_unit_retail;
   end if;

   if RPM_WRAPPER.ROUND_CURRENCY(O_error_message,
                                 L_rounded_unit_retail,
                                 L_converted_unit_retail,
                                 I_sec_currency_code,
                                 1) = 0 then
      O_success := 'N';
      return;
   end if;

   if L_price_guide_id is NOT NULL then
      if APPLY_PRICE_GUIDES (O_error_message,
                             L_guided_unit_retail,
                             L_rounded_unit_retail,
                             I_sec_currency_code,
                             NULL,
                             L_price_guide_id) = FALSE then
         O_success := 'N';
         return;
      end if;
   else
        L_guided_unit_retail := L_rounded_unit_retail;
   end if;

   O_selling_retail := L_guided_unit_retail;

   O_success := 'Y';

   return;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_success := 'N';

   return;

END GET_AREA_DIFF_PRICE;
--------------------------------------------------------------------------------
FUNCTION SET_AREA_DIFF_PRICE(O_error_msg               OUT  rtk_errors.rtk_text%TYPE,
                             O_item_pricing_table      OUT  OBJ_ITEM_PRICING_TBL,
                             I_item_pricing_table   IN      OBJ_ITEM_PRICING_TBL)
RETURN BOOLEAN IS
   L_program                 VARCHAR2(75)                                   := 'MERCH_RETAIL_API_SQL.SET_AREA_DIFF_PRICE';
   ---
   L_zone_group_id    RPM_ZONE_GROUP.ZONE_GROUP_ID%TYPE := NULL;
   ---
   L_area_diff_prim_id         RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE;
   L_zone_id                   RPM_ZONE.ZONE_ID%TYPE;
   L_area_diff_exists          NUMBER(1);
   L_primary_zone_index        NUMBER(10);
   ---
   L_query_item                ITEM_MASTER.ITEM%TYPE;
   L_ITEM_PRICING_REC          OBJ_ITEM_PRICING_REC;
   ---
   L_success                   VARCHAR2(1);
   L_selling_retail            RPM_ITEM_ZONE_PRICE.STANDARD_RETAIL%TYPE;
   L_primary                   NUMBER(1);

   cursor  C_ORIG is
   select /*+ INDEX(IM, PK_ITEM_MASTER) */ ipt.item,
          im.dept,
          ipt.zone_group_id,
          ipt.zone_id,
          ipt.selling_unit_retail,
          ipt.selling_uom,
          ipt.currency_code
     from TABLE(CAST(I_item_pricing_table as OBJ_ITEM_PRICING_TBL)) ipt,
          item_master im
    where im.item = ipt.item
    order by im.dept,
             ipt.zone_group_id;

   cursor C_SECONDARY is
   select OBJ_ITEM_PRICING_REC(L_query_item,
                               rz.zone_group_id,
                               rz.zone_id,
                               rz.zone_display_id,
                               rz.name,
                               rizp.standard_retail,
                               rizp.selling_retail,
                               rizp.selling_uom,
                               rizp.multi_units,
                               rizp.multi_unit_retail,
                               rizp.multi_selling_uom,
                               rz.base_ind,
                               rizp.standard_retail_currency,
                               rizp.standard_uom,
                               0)
     from rpm_item_zone_price rizp,
          rpm_zone rz
    where rizp.item    = L_query_item
      and rizp.zone_id = rz.zone_id
      and rizp.zone_id in (select rad.zone_hier_id
                             from rpm_area_diff rad
                            where rad.area_diff_prim_id = L_area_diff_prim_id)
      and rizp.zone_id not in (select ipt.zone_id
                                 from TABLE(CAST(I_item_pricing_table as OBJ_ITEM_PRICING_TBL)) ipt
                                where ipt.item = L_query_item);

BEGIN

   O_item_pricing_table := OBJ_ITEM_PRICING_TBL();

   for rec in C_ORIG LOOP
      if L_zone_group_id is NULL or
         rec.zone_group_id != L_zone_group_id then
         L_zone_group_id := rec.zone_group_id;
         -- Check if an Area Differential is defined for the Department
         if AREA_DIFF_EXISTS (O_error_msg,
                              L_area_diff_exists,
                              rec.item,
                              L_zone_group_id) = FALSE then
            return FALSE;
         end if;
       --
      end if;

      if L_area_diff_exists = 1 then
         if IS_PRIMARY_AREA(O_error_msg,
                            L_primary,
                            L_area_diff_prim_id,
                            rec.item,
                            rec.zone_id) = FALSE then
            return FALSE;
         end if;
      end if;

      if L_area_diff_exists = 1 and
         L_primary = 1 then

         L_query_item := rec.item;

         open C_SECONDARY;

         LOOP
            fetch C_SECONDARY
             into L_ITEM_PRICING_REC;

            if C_SECONDARY%NOTFOUND then
               EXIT;
            end if;

            GET_AREA_DIFF_PRICE(O_error_msg,
                                L_success,
                                L_selling_retail,
                                L_zone_group_id,
                                rec.item,
                                L_ITEM_PRICING_REC.zone_id,
                                L_ITEM_PRICING_REC.currency_code,
                                rec.zone_id,
                                rec.selling_unit_retail,
                                rec.selling_uom,
                                rec.currency_code);

            if L_success != 'Y' then
               return FALSE;
            end if;

            L_ITEM_PRICING_REC.unit_retail         := L_selling_retail;
            L_ITEM_PRICING_REC.selling_unit_retail := L_selling_retail;
            L_ITEM_PRICING_REC.selling_uom         := rec.selling_uom;

            O_item_pricing_table.EXTEND;
            O_item_pricing_table(O_item_pricing_table.count) := L_ITEM_PRICING_REC;

         END LOOP;

         close C_SECONDARY;

      end if;

   end LOOP;

   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;

END SET_AREA_DIFF_PRICE;
--------------------------------------------------------------------------------
FUNCTION GET_PRICING_ZONE_ID (O_error_message IN OUT VARCHAR2,
                              O_zone_ids      IN OUT OBJ_ZONE_TBL,
                              I_new_loc       IN     ITEM_LOC.LOC%TYPE,
                              I_new_loc_type  IN     ITEM_LOC.LOC_TYPE%TYPE,
                              I_zone_id       IN     ITEM_LOC.LOC%TYPE)
RETURN BOOLEAN IS

   L_program                 VARCHAR2(75)                    := 'MERCH_RETAIL_API_SQL.GET_PRICING_ZONE_ID';

   L_zone_currency VARCHAR2(10) := NULL;

   L_loc_currency VARCHAR2(10) := NULL;

   L_zone_loc_exists NUMBER := NULL;

   cursor C_ZONE_EXISTS is
      select currency_code
        from rpm_zone
       where zone_id = I_zone_id;

   cursor C_ZONE_IDS is
      select NEW OBJ_ZONE_REC(rz.zone_id,
                              rz.zone_display_id,
                              rz.name,
                              rz.currency_code,
                              rz.base_ind,
                              OBJ_ZONE_LOC_Tbl()
                          )
       from rpm_zone rz
     where rz.zone_id = NVL(I_zone_id,rz.zone_id);

   cursor C_GET_ZONE_LOCS(p_zone_id NUMBER) IS
      select NEW OBJ_zone_loc_rec(rzl.zone_location_id,
                                  s.store_name,
                                  rzl.location,
                                  rzl.loc_type,
                                  s.currency_code,
                                  DECODE(s.store_type,'W','W',NULL),
                                  rlm.effective_date)
      from rpm_zone_location rzl,
             rpm_location_move rlm,
             store s,
             wh w
       where p_zone_id = rzl.zone_id(+)
         and rzl.zone_id = rlm.old_zone_id (+)
       and rzl.location = rlm.location (+)
       and rzl.location = s.STORE (+)
       and rzl.location = w.wh (+);

   cursor C_CHECK_NEW_LOC is
      select currency_code
        from store_add
       where store = I_new_loc
         and I_new_loc_type = 'S'
      UNION ALL
      select wh_currency
        from wh_add
       where wh = I_new_loc
         and I_new_loc_type = 'W';

   cursor C_ZONE_LOC_EXISTS is
      select 1
        from rpm_zone_location
       where zone_id = I_zone_id
         and location = I_new_loc;
BEGIN
   O_zone_ids := new OBJ_ZONE_TBL();

   if I_new_loc IS NOT NULL and I_zone_id IS NOT NULL then

      open C_CHECK_NEW_LOC;
      fetch C_CHECK_NEW_LOC into L_loc_currency;
      close C_CHECK_NEW_LOC;

      if L_loc_currency is NULL then
         O_error_message := 'Invalid location';
         return FALSE;
      end if;

      open C_ZONE_EXISTS;
      fetch C_ZONE_EXISTS into L_zone_currency;
      close C_ZONE_EXISTS;

      if L_zone_currency IS NULL then
         O_error_message := 'Invalid zone';
         return FALSE;
      end if;

      if L_zone_currency != L_loc_currency then
         O_error_message := 'The currency code of input location does not match with the currency code of the Input zone';
         return FALSE;
      end if;

      open C_ZONE_LOC_EXISTS;
      fetch C_ZONE_LOC_EXISTS into L_zone_loc_exists;
      close C_ZONE_LOC_EXISTS;

      if L_zone_loc_exists = 1 then
         return TRUE;
      else
         insert into rpm_zone_location (zone_location_id,
                                        zone_id,
                                        location,
                                        loc_type
                                        )
                                values (RPM_ZONE_LOCATION_SEQ.NEXTVAL,
                                        I_zone_id,
                                        I_new_loc,
                                        DECODE(I_new_loc_type,'S',0,'W',2)
                                        );
         return TRUE;
      end if;
   elsif I_zone_id IS NOT NULL and I_new_loc IS NULL then

      open C_ZONE_EXISTS;
      fetch C_ZONE_EXISTS into L_zone_currency;
      close C_ZONE_EXISTS;

      if L_zone_currency IS NOT NULL then
         open  C_ZONE_IDS;
         fetch C_ZONE_IDS bulk collect into O_zone_ids;
         close C_ZONE_IDS;

         open C_GET_ZONE_LOCS(O_zone_ids(1).zone_id);
        fetch C_GET_ZONE_LOCS BULK COLLECT INTO O_zone_ids(1).zone_locations;
        close C_GET_ZONE_LOCS;

         return TRUE;
      else
         O_error_message := 'Invalid zone';

         return FALSE;
      end if;

  elsif I_zone_id IS NULL  and I_new_loc IS NULL then

     open  C_ZONE_IDS;
     fetch C_ZONE_IDS bulk collect into O_zone_ids;
     close C_ZONE_IDS;

     for i IN 1..O_zone_ids.COUNT loop
        open  C_GET_ZONE_LOCS(O_zone_ids(i).zone_id);
       fetch C_GET_ZONE_LOCS BULK COLLECT INTO O_zone_ids(i).zone_locations;
       close C_GET_ZONE_LOCS;
    end loop;

     return TRUE;
  end if;

  return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return FALSE;
END;

-----------------------------------------------------------------------------------------------------

FUNCTION ADD_TAX(O_error_message    IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                 I_item             IN     ITEM_MASTER.ITEM%TYPE,
                 I_dept             IN     ITEM_MASTER.DEPT%TYPE,
                 I_class            IN     ITEM_MASTER.CLASS%TYPE,
                 I_pack_ind         IN     ITEM_MASTER.PACK_IND%TYPE,
                 I_from_loc         IN     ITEM_LOC.LOC%TYPE,
                 I_from_entity_type IN     ITEM_LOC.LOC_TYPE%TYPE,
                 I_from_loc_curr    IN     CURRENCIES.CURRENCY_CODE%TYPE,
                 I_date             IN     DATE,
                 IO_retail          IN OUT ITEM_LOC.UNIT_RETAIL%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'MERCH_RETAIL_API_SQL.ADD_TAX';

   L_system_options SYSTEM_OPTIONS%ROWTYPE   := NULL;
   L_class_vat_ind  CLASS.CLASS_VAT_IND%TYPE := NULL;

   L_retail_before_tax RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE := NULL;
   L_retail_after_tax  RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE := NULL;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return FALSE;
   end if;

   if L_system_options.default_tax_type = RPM_CONSTANTS.SALES_TAX_TYPE then
      return TRUE;
   end if;

   if CLASS_ATTRIB_SQL.GET_CLASS_VAT_IND(O_error_message,
                                         L_class_vat_ind,
                                         I_dept,
                                         I_class) = FALSE then
      return FALSE;
   end if;

   if L_system_options.default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE or
      (L_system_options.default_tax_type = RPM_CONSTANTS.SVAT_TAX_TYPE and
         (L_system_options.class_level_vat_ind = 'N' or
            (L_system_options.class_level_vat_ind = 'Y' and
             L_class_vat_ind = 'Y'))) then

      L_retail_before_tax := IO_retail;

      if TAX_SQL.ADD_RETAIL_TAX(O_error_message,
                                L_retail_after_tax,
                                I_item,
                                I_pack_ind,
                                I_dept,
                                I_class,
                                I_from_loc,
                                I_from_entity_type,
                                I_date,
                                L_retail_before_tax,
                                I_from_loc_curr,
                                1, -- I_qty
                                TAX_SQL.LP_CALL_TYPE_RPM_NIL) = FALSE then
         return FALSE;
      end if;

      IO_retail := L_retail_after_tax;

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END ADD_TAX;
--------------------------------------------------------------------------------

FUNCTION GET_REASON_CODE_DESC(O_error_msg           OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                              I_reason_code_id   IN      RPM_CODES.CODE_ID%TYPE)
RETURN VARCHAR2 IS

   L_program                 VARCHAR2(75)   := 'MERCH_RETAIL_API_SQL.GET_REASON_CODE_DESC';

   L_reason_code_desc        RPM_CODES.DESCRIPTION%TYPE := NULL;

   cursor C_GET_REASON_CODE_DESC is
      select description
        from rpm_codes rc
       where rc.code_id = I_reason_code_id;

BEGIN

   open C_GET_REASON_CODE_DESC;

   fetch C_GET_REASON_CODE_DESC into L_reason_code_desc;

   if C_GET_REASON_CODE_DESC%NOTFOUND then
      L_reason_code_desc := NULL;
   end if;

   close C_GET_REASON_CODE_DESC;

   return L_reason_code_desc;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG ('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return NULL;

END GET_REASON_CODE_DESC;
--------------------------------------------------------------------------------
FUNCTION GET_REASON_CODE_DESC(I_reason_code_id   IN      RPM_CODES.CODE_ID%TYPE)
RETURN VARCHAR2 IS

   L_reason_code_desc   RPM_CODES.DESCRIPTION%TYPE := NULL;
   L_error_msg          VARCHAR2(500)              := NULL;

BEGIN

   L_reason_code_desc := GET_REASON_CODE_DESC(L_error_msg,
                                              I_reason_code_id);

   return L_reason_code_desc;

EXCEPTION
   when OTHERS then
      return NULL;

END GET_REASON_CODE_DESC;
-----------------------------------------------------------------------------------------------------

END MERCH_RETAIL_API_SQL;
/