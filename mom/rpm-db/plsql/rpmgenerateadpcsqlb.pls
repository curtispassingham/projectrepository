CREATE OR REPLACE PACKAGE BODY RPM_GENERATE_AREA_DIFF_PC AS
--------------------------------------------------------

   LP_vdate DATE           := GET_VDATE;
   LP_dept  DEPS.DEPT%TYPE := NULL;

   LP_auto_approve_ind RPM_AREA_DIFF.AUTO_APPROVE_IND%TYPE := NULL;

--------------------------------------------------------
--PROTOTYPES
--------------------------------------------------------
FUNCTION GENERATE(O_error_msg          OUT VARCHAR2,
                  I_process_id      IN     NUMBER,
                  I_area_diff_pc_id IN     RPM_AREA_DIFF_PC_CREATE.AREA_DIFF_PC_ID%TYPE,
                  I_area_diff_id    IN     RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                  I_effective_date  IN     DATE,
                  I_user_id         IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                  I_reason_code_id  IN     RPM_CODES.CODE_ID%TYPE)
RETURN NUMBER;

FUNCTION SETUP_PRIMARY(O_error_msg            OUT VARCHAR2,
                       I_area_diff_id      IN     RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                       I_area_diff_prim_id IN     RPM_AREA_DIFF.AREA_DIFF_PRIM_ID%TYPE,
                       I_effective_date    IN     DATE)
RETURN BOOLEAN;
---
FUNCTION CREATE_SECONDARY(O_error_msg            OUT VARCHAR2,
                          I_area_diff_prim_id IN     RPM_AREA_DIFF.AREA_DIFF_PRIM_ID%TYPE,
                          I_area_diff_id      IN     RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                          I_effective_date    IN     DATE,
                          I_user_id           IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                          I_reason_code_id    IN     RPM_CODES.CODE_ID%TYPE)
RETURN BOOLEAN;
---
FUNCTION CONFLICT_CHECK_PRICE_CHANGE(O_error_msg          OUT VARCHAR2,
                                     O_conflict_exists    OUT NUMBER,
                                     I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                     I_rib_trans_id    IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                     I_user_id         IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------
--INTERNAL PROCEDURES
--------------------------------------------------------
FUNCTION GENERATE(O_error_msg            OUT VARCHAR2,
                  I_process_id        IN     NUMBER,
                  I_area_diff_pc_id   IN     RPM_AREA_DIFF_PC_CREATE.AREA_DIFF_PC_ID%TYPE,
                  I_area_diff_id      IN     RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                  I_effective_date    IN     DATE,
                  I_user_id           IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                  I_reason_code_id    IN     RPM_CODES.CODE_ID%TYPE)
RETURN NUMBER IS

   L_program             VARCHAR2(40)                         := 'RPM_GENERATE_AREA_DIFF_PC.GENERATE';
   L_area_diff_id        RPM_AREA_DIFF.AREA_DIFF_ID%TYPE      := NULL;
   L_area_diff_prim_id   RPM_AREA_DIFF.AREA_DIFF_PRIM_ID%TYPE := NULL;

BEGIN

   delete from numeric_id_gtt;

   select rad.area_diff_id,
          rad.area_diff_prim_id,
          rad.auto_approve_ind into L_area_diff_id,
                                    L_area_diff_prim_id,
                                    LP_auto_approve_ind
     from rpm_area_diff rad,
          rpm_area_diff_pc_create rac
    where rac.area_diff_pc_id = I_area_diff_pc_id
      and rad.area_diff_id = rac.area_diff_id;

   if SETUP_PRIMARY(O_error_msg,
                    L_area_diff_id,
                    L_area_diff_prim_id,
                    I_effective_date) = FALSE then
      return 0;
   end if;

   if CREATE_SECONDARY(O_error_msg,
                       L_area_diff_prim_id,
                       L_area_diff_id,
                       I_effective_date,
                       I_user_id,
                       I_reason_code_id) = FALSE then
      return 0;
   end if;

   if LP_auto_approve_ind = 1 then
      insert into rpm_price_event_thread
         (pe_thread_id,
          thread_number,
          price_event_id,
          price_event_type)
      select I_process_id,
             I_area_diff_pc_id,
             numeric_id,
             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
        from numeric_id_gtt;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END GENERATE;

-----------------------------------------------------------------------------
FUNCTION SETUP_PRIMARY(O_error_msg            OUT VARCHAR2,
                       I_area_diff_id      IN     rpm_area_diff.area_diff_id%TYPE,
                       I_area_diff_prim_id IN     rpm_area_diff.area_diff_prim_id%TYPE,
                       I_effective_date    IN     DATE)
RETURN BOOLEAN IS

BEGIN
   ---------------------------------------
   -- Get all the item/zone selling retail
   ---------------------------------------

   delete from rpm_area_temp_item_loc;
   insert into rpm_area_temp_item_loc (item,
                                       loc,
                                       link_code,
                                       proposed_retail,
                                       proposed_retail_uom)
   select distinct
          u.item,
          u.zone_id,
          u.link_code,
          first_value(u.selling_retail) over (partition by item order by query_rank),
          --this will be used to throw out items that have varying UOMs across the zone
          decode(first_value(u.uom_count) over (partition by item order by query_rank),
                 1,  first_value(u.selling_uom) over (partition by item order by query_rank),
                 null)
     from (select /*+ INDEX(fr RPM_FUTURE_RETAIL_I1) */
                  f.item   item,
                  f.zone   zone_id,
                  null     link_code,
                  first_value(f.selling_retail)
                      over (partition by f.item, f.zone order by f.action_date desc) selling_retail,
                  first_value(f.selling_uom)
                      over (partition by f.item, f.zone order by f.action_date desc) selling_uom,
                  1        uom_count,
                  1        query_rank
             from rpm_area_diff_prim adp,
                  rpm_zone_future_retail f
            where adp.area_diff_prim_id = I_area_diff_prim_id
              and f.zone                = adp.zone_hier_id
              and f.action_date        <= I_effective_date
              and f.item                in (select item
                                              from item_master im
                                             where im.dept       = adp.dept
                                               and im.class      = nvl(adp.class,im.class)
                                               and im.subclass   = nvl(adp.subclass,im.subclass)
                                               and im.item_level = im.tran_level
                                               and im.status = 'A'
                                               and im.sellable_ind = 'Y')
            union all
           select distinct s.item,
                  s.zone_id,
                  s.link_code,
                  avg(s.selling_retail) selling_retail,
                  min(s.selling_uom) selling_uom,
                  count(distinct s.selling_uom) uom_count,
                  2 query_rank
             from (select /*+ INDEX(fr RPM_FUTURE_RETAIL_I1) */
                          fr.item      item,
                          zl.zone_id   zone_id,
                          null         link_code,
                          first_value(fr.selling_retail)
                              over (partition by fr.item, fr.location order by fr.action_date desc) selling_retail,
                          first_value(fr.selling_uom)
                              over (partition by fr.item, fr.location order by fr.action_date desc) selling_uom
                     from rpm_area_diff_prim adp,
                          rpm_zone_location zl,
                          rpm_future_retail fr
                    where adp.area_diff_prim_id = I_area_diff_prim_id
                      and fr.action_date       <= I_effective_date
                      and fr.dept               = adp.dept
                      and fr.class              = nvl(adp.class,fr.class)
                      and fr.subclass           = nvl(adp.subclass,fr.subclass)
                      and zl.zone_id            = adp.zone_hier_id
                      and fr.location           = zl.location) s
            group by s.item,
                     s.zone_id,
                     s.link_code) u;

   ------------------------------------------------------------------------------------
   -- Remove all items that have more than 1 LinkCode (including NULL) accross the Zone
   ------------------------------------------------------------------------------------


   delete rpm_area_temp_item_loc
    where item in (select item
                     from (select t1.item, count(distinct NVL(l.link_code,-999999))
                       from rpm_link_code_attribute l,
                          (select t.item,
                                          z.location
                                     from rpm_area_temp_item_loc t,
                                     rpm_zone_location z
                                    where t.loc  = z.zone_id) t1
                      where t1.item = l.item (+)
                        and t1.location  = l.location (+)
                      group by t1.item
                     having count(distinct NVL(l.link_code,-999999)) > 1));

   ------------------------------------------------------------------------------------
   -- Update the Link Code column in the rpm_area_temp_item_loc table
   ------------------------------------------------------------------------------------

   merge into rpm_area_temp_item_loc up
   using (select distinct
                 t.item,
                 l.link_code,
                 avg(t.proposed_retail) over (partition by l.link_code) proposed_retail
            from rpm_area_temp_item_loc  t,
                 rpm_link_code_attribute l,
                 rpm_zone_location     rzl
           where t.item = l.item
             and t.loc  = rzl.zone_id
             and rzl.location  = l.location) use_this
   on (up.item = use_this.item)
   when matched then
   update set link_code           = use_this.link_code,
              proposed_retail     = use_this.proposed_retail
   --forced fail
   when not matched then
    insert (item)
    values ('abc');

   ------------------------------------------------------------------------------------
   -- Delete all excluded items.
   ------------------------------------------------------------------------------------

   delete from rpm_area_temp_item_loc r
    where link_code is null
      and exists (select 'x'
                    from rpm_area_diff_exclude ex
                   where ex.area_diff_id  = I_area_diff_id
                     and r.item           = ex.item_id);

   ------------------------------------------------------------------------------------
   -- Delete link code if any item in the link code is excluded.
   ------------------------------------------------------------------------------------

   delete from rpm_area_temp_item_loc r
    where link_code is not null
      and exists (select 'x'
                    from rpm_area_diff_exclude ex,
                         rpm_link_code_attribute l
                   where ex.area_diff_id  = I_area_diff_id
                     and l.item           = ex.item_id
                     and l.link_code      = r.link_code);

   ------------------------------------------------------------------------------------
   -- Delete the link code that has more item than the area diff strategy item in the
   -- primary zone
   ------------------------------------------------------------------------------------

   delete from rpm_area_temp_item_loc r
    where link_code in
         (select con.link_code
            from (select r.link_code,
                         count(distinct r.item) lca_count
                    from rpm_link_code_attribute r,
                         rpm_zone_location zl,
                         rpm_area_diff_prim adp
                   where adp.area_diff_prim_id = I_area_diff_prim_id
                     and zl.zone_id = adp.zone_hier_id
                     and zl.location = zl.location
                     and r.link_code in (select distinct link_code
                                           from rpm_me_consolidate_itemloc_gtt
                                          where link_code is not null
                                            and rownum > 0)
                     group by r.link_code) lca,
                 (select link_code,
                         count(*) con_count
                    from rpm_me_consolidate_itemloc_gtt
                   where link_code is not null
                     and rownum > 0
                   group by link_code) con
           where lca.link_code  = con.link_code
             and lca.lca_count != con.con_count);

   --get rid of item/locs that have varying UOMs across the zone
   delete from rpm_area_temp_item_loc
    where proposed_retail_uom is null;

   --get rid of item/locs that have varying UOMs across the link_code/zone
   delete from rpm_area_temp_item_loc
    where link_code in (select link_code
                          from (select link_code,
                                       count(distinct proposed_retail_uom) uom_count
                                  from rpm_area_temp_item_loc
                                 where link_code is not null
                                 group by link_code)
                         where uom_count > 1);

   return TRUE;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_AREA_DIFF_PC.SETUP_PRIMARY',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_PRIMARY;

--------------------------------------------------------

FUNCTION CREATE_SECONDARY(O_error_msg            OUT VARCHAR2,
                          I_area_diff_prim_id IN     RPM_AREA_DIFF.AREA_DIFF_PRIM_ID%TYPE,
                          I_area_diff_id      IN     RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                          I_effective_date    IN     DATE,
                          I_user_id           IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                          I_reason_code_id    IN     RPM_CODES.CODE_ID%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_GENERATE_AREA_DIFF_PC.CREATE_SECONDARY';

   L_currency_rtl_fmt  CURRENCIES.CURRENCY_RTL_FMT%TYPE  := NULL;
   L_currency_rtl_dec  CURRENCIES.CURRENCY_RTL_DEC%TYPE  := NULL;
   L_currency_cost_fmt CURRENCIES.CURRENCY_COST_FMT%TYPE := NULL;
   L_currency_cost_dec CURRENCIES.CURRENCY_COST_DEC%TYPE := NULL;

   L_from_currency_code CURRENCY_RATES.CURRENCY_CODE%TYPE := NULL;
   L_from_exchange_rate CURRENCY_RATES.EXCHANGE_RATE%TYPE := NULL;
   L_to_currency_code   CURRENCY_RATES.CURRENCY_CODE%TYPE := NULL;
   L_to_exchange_rate   CURRENCY_RATES.EXCHANGE_RATE%TYPE := NULL;

   L_zone_hier_id       RPM_AREA_DIFF.ZONE_HIER_ID%TYPE       := NULL;
   L_percent            RPM_AREA_DIFF.PERCENT%TYPE            := NULL;
   L_percent_apply_type RPM_AREA_DIFF.PERCENT_APPLY_TYPE%TYPE := NULL;
   L_price_guide_id     RPM_AREA_DIFF.PRICE_GUIDE_ID%TYPE     := NULL;
   L_zone_ranging       RPM_SYSTEM_OPTIONS.ZONE_RANGING%TYPE  := NULL;

   cursor C_AREA_DIFF is
      select prim_zone.currency_code,
             sec_zone.currency_code,
             ad.zone_hier_id,
             ad.percent,
             ad.percent_apply_type,
             ad.price_guide_id
        from rpm_area_diff ad,
             rpm_area_diff_prim adp,
             rpm_zone prim_zone,
             rpm_zone sec_zone
       where adp.area_diff_prim_id = I_area_diff_prim_id
         and ad.area_diff_id       = I_area_diff_id
         and prim_zone.zone_id     = adp.zone_hier_id
         and sec_zone.zone_id      = ad.zone_hier_id;

BEGIN

   select zone_ranging
     into L_zone_ranging
     from rpm_system_options;

   open C_AREA_DIFF;
   fetch C_AREA_DIFF INTO L_from_currency_code,
                          L_to_currency_code,
                          L_zone_hier_id,
                          L_percent,
                          L_percent_apply_type,
                          L_price_guide_id;
   close C_AREA_DIFF;

   ----------------------------------------------------------------------------------------------
   -- Remove all items that is in the Link Code at the secondary zone but not in the Primary Zone
   ----------------------------------------------------------------------------------------------

   delete rpm_area_temp_item_loc
    where link_code is NULL
      and item      IN (select distinct t.item
                          from rpm_area_temp_item_loc t,
                               rpm_link_code_attribute l,
                               rpm_zone_location z
                         where t.item     = l.item
                           and z.zone_id  = L_zone_hier_id
                           and z.location = l.location);

   ----------------------------------------------------------------------------------------------
   -- Delete all Link Code that does not cover all the locations in the secondary zone
   ----------------------------------------------------------------------------------------------

   delete rpm_area_temp_item_loc
    where link_code IN (select distinct link_code
                          from (select t1.link_code,
                                       COUNT(distinct NVL(l.link_code, -1 * rownum))
                                  from rpm_link_code_attribute l,
                                       (select t.link_code,
                                               t.item,
                                               z.location
                                          from rpm_area_temp_item_loc t,
                                               rpm_zone_location z
                                         where z.zone_id   = L_zone_hier_id
                                           and t.link_code is NOT NULL) t1
                                 where t1.item     = l.item (+)
                                   and t1.location = l.location (+)
                                 group by t1.link_code
                          having COUNT(distinct NVL(l.link_code, -1 * rownum)) > 1));

   ------------------------------------------------------------------------------------
   -- Delete the link code that has more item than the area diff strategy item in the
   -- secondary zone
   ------------------------------------------------------------------------------------

   delete from rpm_area_temp_item_loc r
    where link_code IN
         (select con.link_code
            from (select r.link_code,
                         COUNT(distinct r.item) lca_count
                    from rpm_link_code_attribute r,
                         rpm_zone_location zl
                   where zl.zone_id  = L_zone_hier_id
                     and zl.location = zl.location
                     and r.link_code IN (select distinct link_code
                                           from rpm_me_consolidate_itemloc_gtt
                                          where link_code is NOT NULL
                                            and rownum    > 0)
                   group by r.link_code) lca,
                 (select link_code,
                         COUNT(1) con_count
                    from rpm_me_consolidate_itemloc_gtt
                   where link_code is NOT NULL
                     and rownum    > 0
                   group by link_code) con
           where lca.link_code  = con.link_code
             and lca.lca_count != con.con_count);

   ----------------------------------------------------------------------------------------------
   -- Delete duplicate Link Code. We just need one link code record for each Link Code
   ----------------------------------------------------------------------------------------------

   delete rpm_area_temp_item_loc
    where rowid IN (select t.row_id
                      from (select rowid row_id,
                                   RANK() OVER (PARTITION BY link_code
                                                    ORDER BY item) rank
                              from rpm_area_temp_item_loc
                             where link_code is NOT NULL) t
                      where t.rank > 1);

   -----------------------------------------------------------------------------------
   -- If Zone Ranging is checked, remove Item that is not ranged to the secondary zone
   -----------------------------------------------------------------------------------
   if L_zone_ranging = 1 then

      delete rpm_area_temp_item_loc
       where rowid IN (select t.row_id
                         from (select rowid row_id
                                 from rpm_area_temp_item_loc ral
                                where link_code is NULL
                                  and NOT EXISTS (select 1
                                                    from item_loc il,
                                                         rpm_zone_location rzl
                                                   where il.item     = ral.item
                                                     and il.loc      = rzl.location
                                                     and rzl.zone_id = L_zone_hier_id)) t);
   end if;

   ----------------------------------------
   -- Create Price Change at Secondary Zone
   ----------------------------------------

   if CURRENCY_SQL.GET_RATE(O_error_msg,
                            L_from_exchange_rate,
                            L_from_currency_code,
                            'R',
                            I_effective_date) = FALSE then
      return FALSE;
   end if;

   if CURRENCY_SQL.GET_RATE(O_error_msg,
                            L_to_exchange_rate,
                            L_to_currency_code,
                            'R',
                            I_effective_date) = FALSE then
      return FALSE;
   end if;

   if CURRENCY_SQL.GET_FORMAT(O_error_msg,
                              L_to_currency_code,
                              L_currency_rtl_fmt,
                              L_currency_rtl_dec,
                              L_currency_cost_fmt,
                              L_currency_cost_dec) = FALSE then
      return FALSE;
   end if;

   --non link codes
   insert all
      into rpm_price_change (price_change_id,
                             price_change_display_id,
                             state,
                             reason_code,
                             item,
                             zone_id,
                             zone_node_type,
                             effective_date,
                             change_type,
                             change_amount,
                             change_currency,
                             change_selling_uom,
                             null_multi_ind,
                             price_guide_id,
                             vendor_funded_ind,
                             create_date,
                             create_id,
                             ignore_constraints)
                     values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                             RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                             RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                             I_reason_code_id,
                             item,
                             L_zone_hier_id,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                             I_effective_date,
                             RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE,
                             change_amount,
                             L_to_currency_code,
                             change_selling_uom,
                             0,                    -- null_multi_ind
                             L_price_guide_id,
                             0,                    -- vendor_funded_ind
                             LP_vdate,             -- create_date
                             I_user_id,            -- create_id
                             0)                    -- ignore_constraints
      into numeric_id_gtt (numeric_id)
                   values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         select p.item,
                DECODE(L_percent_apply_type,
                       0, TRUNC(p.proposed_retail * (1 + L_percent / 100), L_currency_rtl_dec),
                       1, TRUNC(p.proposed_retail * (1 - L_percent / 100), L_currency_rtl_dec),
                       2, TRUNC(p.proposed_retail, L_currency_rtl_dec)) change_amount,
                p.proposed_retail_uom change_selling_uom
          from (select distinct item,
                       proposed_retail * L_to_exchange_rate / L_from_exchange_rate proposed_retail,
                       proposed_retail_uom
                  from rpm_area_temp_item_loc
                 where proposed_retail is NOT NULL
                   and link_code       is NULL) p;

   --link codes
   insert all
      into rpm_price_change (price_change_id,
                             price_change_display_id,
                             state,
                             reason_code,
                             zone_id,
                             zone_node_type,
                             link_code,
                             effective_date,
                             change_type,
                             change_amount,
                             change_currency,
                             change_selling_uom,
                             null_multi_ind,
                             price_guide_id,
                             vendor_funded_ind,
                             create_date,
                             create_id,
                             area_diff_parent_id,
                             ignore_constraints)
                     values (RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                             RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                             RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                             I_reason_code_id,
                             L_zone_hier_id,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                             link_code,
                             I_effective_date,
                             RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE,
                             change_amount,
                             L_to_currency_code,
                             change_selling_uom,
                             0,                    -- null_multi_ind
                             L_price_guide_id,
                             2,                    -- vendor_funded_ind
                             LP_vdate,             -- create_date
                             I_user_id,            -- create_id
                             0,                    -- area_diff_parent_id
                             0)
      into numeric_id_gtt (numeric_id)
                   values (RPM_PRICE_CHANGE_SEQ.CURRVAL)
         select p.link_code,
                DECODE(L_percent_apply_type,
                       0, TRUNC(p.proposed_retail * (1 + L_percent / 100), L_currency_rtl_dec),
                       1, TRUNC(p.proposed_retail * (1 - L_percent / 100), L_currency_rtl_dec),
                       2, TRUNC(p.proposed_retail, L_currency_rtl_dec)) change_amount,
                p.proposed_retail_uom change_selling_uom
           from (select link_code,
                        proposed_retail * L_to_exchange_rate / L_from_exchange_rate proposed_retail,
                        proposed_retail_uom
                   from rpm_area_temp_item_loc
                  where proposed_retail is NOT NULL
                    and link_code       is NOT NULL) p;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CREATE_SECONDARY;

--------------------------------------------------------

FUNCTION CONFLICT_CHECK_PRICE_CHANGE(O_error_msg            OUT VARCHAR2,
                                     O_conflict_exists      OUT NUMBER,
                                     I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE,
                                     I_rib_trans_id      IN     rpm_price_event_payload.transaction_id%TYPE,
                                     I_user_id           IN     rpm_price_change.create_id%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(60) := 'RPM_GENERATE_AREA_DIFF_PC.CONFLICT_CHECK_PRICE_CHANGE';

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_bulk_cc_pe_id           NUMBER := NULL;
   L_secondary_bulk_cc_pe_id NUMBER := NULL;
   L_pe_seq_number           NUMBER := NULL;
   L_pe_thread_number        NUMBER := NULL;
   L_max_sequence_number     NUMBER := NULL;
   L_max_thread_number       NUMBER := NULL;

BEGIN

   select RPM_BULK_CC_PE_SEQ.NEXTVAL
     into L_bulk_cc_pe_id
     from dual;

   select RPM_BULK_CC_PE_SEQ.NEXTVAL
     into L_secondary_bulk_cc_pe_id
     from dual;

   insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                               price_event_type,
                               persist_ind,
                               start_state,
                               end_state,
                               user_name,
                               emergency_perm,
                               secondary_bulk_cc_pe_id,
                               secondary_ind)
      values (L_bulk_cc_pe_id,
              RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
              'Y',
              RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
              RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
              I_user_id,
              1,
              NULL,
              1);

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id)
      select L_bulk_cc_pe_id,
             value(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids;

   if RPM_BULK_CC_THREADING_SQL.THREAD_OVERLAP_PRICE_EVENTS (L_cc_error_tbl,
                                                             L_max_sequence_number,
                                                             L_bulk_cc_pe_id) = 0 then
      O_error_msg := L_cc_error_tbl(1).error_string;
      return FALSE;
   end if;

   for i IN 1..L_max_sequence_number loop
      L_pe_seq_number := i;

      if RPM_BULK_CC_THREADING_SQL.CHUNK_PRICE_EVENT_THREADS(L_cc_error_tbl,
                                                             L_max_thread_number,
                                                             L_bulk_cc_pe_id,
                                                             L_pe_seq_number ) = 0 then
         O_error_msg := L_cc_error_tbl(1).error_string;
         return FALSE;
      end if;

      for j IN 1..L_max_thread_number loop
         L_pe_thread_number := j;

         if RPM_BULK_CC_THREADING_SQL.PROCESS_CHUNK (L_cc_error_tbl,
                                                           L_bulk_cc_pe_id,
                                                           L_pe_seq_number,
                                                           L_pe_thread_number,
                                                           I_rib_trans_id) = 0 then

            if L_cc_error_tbl(1).error_type = RPM_CONSTANTS.PLSQL_ERROR then
               O_error_msg := L_cc_error_tbl(1).error_string;
               return FALSE;
            else
               O_error_msg := SQL_LIB.CREATE_MSG('Invalid error type');
               return FALSE;
            end if;

         end if;

         if L_cc_error_tbl is NOT NULL and
          L_cc_error_tbl.COUNT > 0 then

             O_conflict_exists := 1;
         end if;

     end loop;
   end loop;

   delete from rpm_price_change
    where price_change_id = -999;

   return TRUE;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CONFLICT_CHECK_PRICE_CHANGE;
--------------------------------------------------------

--------------------------------------------------------
--PUBLIC PROCEDURES
--------------------------------------------------------
FUNCTION GENERATE(O_error_msg            OUT VARCHAR2,
                  O_process_id           OUT NUMBER,
                  O_need_cc              OUT NUMBER,
                  I_area_diff_pc_ids  IN     OBJ_NUMERIC_ID_TABLE,
                  I_user_name         IN     VARCHAR2)
RETURN NUMBER IS

   L_program       VARCHAR2(40) := 'RPM_GENERATE_AREA_DIFF_PC.GENERATE';
   L_process_id    NUMBER(10)   := RPM_PRICE_EVENT_THREAD_SEQ.NEXTVAL;
   L_count         NUMBER(10)   := NULL;

   cursor C_AREADIFF_PC is
      select /*+ CARDINALITY(ids 10) */
             rac.area_diff_pc_id,
             rac.area_diff_id,
             rac.effective_date,
             rc.code_id
        from table(cast(I_area_diff_pc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_area_diff_pc_create rac,
             rpm_area_diff rad,
             rpm_codes rc
       where rac.area_diff_pc_id = value(ids)
         and rad.area_diff_id    = rac.area_diff_id
         and rc.code_type        = 3
         and rc.code             = DECODE(rad.auto_approve_ind,
                                          1, 'AREADIFFAA',
                                          'AREADIFF');

BEGIN

   for rec IN C_AREADIFF_PC loop

      if GENERATE(O_error_msg,
                  L_process_id,
                  rec.area_diff_pc_id,
                  rec.area_diff_id,
                  rec.effective_date,
                  I_user_name,
                  rec.code_id) = 0 then
         return 0;
      end if;

      update rpm_area_diff_pc_create
         set execute_date = SYSDATE,
             state        = DECODE(LP_auto_approve_ind,
                                   1, state,
                                   'C')
       where area_diff_pc_id = rec.area_diff_pc_id;

   end loop;

   select COUNT(*) into L_count
     from rpm_price_event_thread
    where pe_thread_id = L_process_id;

   if L_count > 0 then
      O_need_cc := 1;
   else
      O_need_cc := 0;
   end if;

   O_process_id := L_process_id;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END GENERATE;
-----------------------------------------------------------------------------
FUNCTION INIT_BULK_CC_PE(O_error_msg        OUT VARCHAR2,
                         O_bulk_cc_pe_id    OUT NUMBER,
                         I_process_id    IN     NUMBER,
                         I_user_name     IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_GENERATE_AREA_DIFF_PC.INIT_BULK_CC_PE';

   L_bulk_cc_pe_id NUMBER := NULL;

BEGIN

   select RPM_BULK_CC_PE_SEQ.NEXTVAL
     into L_bulk_cc_pe_id
     from dual;

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  cc_request_group_id,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PC_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 NULL,                                  -- cc_request_group_id,
                 0,                                     -- auto_clean_ind,
                 'com.retek.rpm.app.bulkcc.service.DefaultBulkConflictCheckThreadProcessorImpl', -- thread_processor_class
                 1,                                     -- need_il_explode_ind
                 LP_vdate);

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
          select L_bulk_cc_pe_id,
                 price_event_id,
                 price_event_type
            from rpm_price_event_thread
           where pe_thread_id     = I_process_id
             and price_event_id   is NOT NULL
             and price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE;

   O_bulk_cc_pe_id := L_bulk_cc_pe_id;

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END INIT_BULK_CC_PE;
-----------------------------------------------------------------------------

FUNCTION UPDATE_STATUS(O_error_msg     OUT VARCHAR2,
                       I_process_id IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_GENERATE_AREA_DIFF_PC.UPDATE_STATUS';

   cursor C_HAS_ERROR is
      select distinct
             thread_number area_diff_pc_id
        from rpm_price_event_thread rpet,
             rpm_price_change rpc
       where rpet.pe_thread_id   = I_process_id
         and rpc.price_change_id = rpet.price_event_id
         and rpc.state           = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE;

   cursor C_HAS_NO_ERROR is
      select distinct
             thread_number area_diff_pc_id
        from rpm_price_event_thread rpet,
             rpm_price_change rpc
       where rpet.pe_thread_id    = I_process_id
         and rpc.price_change_id  = rpet.price_event_id
         and rpc.state           != RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE;

BEGIN

   for rec IN C_HAS_ERROR loop
      update rpm_area_diff_pc_create
         set state = 'CE'
       where area_diff_pc_id = rec.area_diff_pc_id;
   end loop;

   for rec IN C_HAS_NO_ERROR loop
      update rpm_area_diff_pc_create
         set state = 'C'
       where area_diff_pc_id = rec.area_diff_pc_id;
   end loop;

   delete from rpm_price_event_thread
    where pe_thread_id = I_process_id;

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END UPDATE_STATUS;
--------------------------------------------------------

FUNCTION APPROVE_SECONDARY(O_error_msg            OUT VARCHAR2,
                           O_conflict_exists      OUT NUMBER,
                           I_rib_trans_id      IN     NUMBER,
                           I_thread_id         IN     NUMBER,
                           I_thread_number     IN     NUMBER,
                           I_user_id           IN     rpm_price_change.create_id%TYPE)
RETURN NUMBER IS

   cursor c_price_change is
      select price_event_id
        from rpm_price_event_thread
       where pe_thread_id  = I_thread_id
         and thread_number = I_thread_number;

   L_conflict_exists          NUMBER(1);

   L_price_event_ids OBJ_NUMERIC_ID_TABLE;

BEGIN
   O_conflict_exists := 0;
   L_price_event_ids := OBJ_NUMERIC_ID_TABLE();

   open C_PRICE_CHANGE;
   fetch C_PRICE_CHANGE BULK COLLECT into L_price_event_ids;
   close C_PRICE_CHANGE;

   if CONFLICT_CHECK_PRICE_CHANGE(O_error_msg,
                                  L_conflict_exists,
                                  L_price_event_ids,
                                  I_rib_trans_id,
                                  I_user_id) = FALSE then
      return 0;
   end if;

   if O_conflict_exists != 1 then
      O_conflict_exists := L_conflict_exists;
   end if;

   delete from rpm_price_event_thread
    where pe_thread_id  = I_thread_id
      and thread_number = I_thread_number;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_GENERATE_AREA_DIFF_PC.APPROVE_SECONDARY',
                                        TO_CHAR(SQLCODE));
      return 0;
END APPROVE_SECONDARY;

--------------------------------------------------------
END RPM_GENERATE_AREA_DIFF_PC;
/

