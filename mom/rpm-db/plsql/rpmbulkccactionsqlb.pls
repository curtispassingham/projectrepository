CREATE OR REPLACE PACKAGE BODY RPM_BULK_CC_ACTIONS_SQL AS
--------------------------------------------------------------------------------

   LP_price_event_type        VARCHAR2(3)                               := NULL;
   LP_bulk_cc_pe_id           NUMBER                                    := NULL;
   LP_parent_thread_number    NUMBER                                    := NULL;
   LP_thread_number           NUMBER                                    := NULL;
   LP_secondary_bulk_cc_pe_id NUMBER                                    := NULL;
   LP_emergency_perm          NUMBER                                    := NULL;
   LP_vdate                   DATE                                      := GET_VDATE;
   LP_need_secondary          NUMBER                                    := NULL;
   LP_secondary_ind           NUMBER                                    := NULL;
   LP_need_il_explode_ind     RPM_BULK_CC_PE.NEED_IL_EXPLODE_IND%TYPE   := NULL;
   LP_old_clr_oostock_date    DATE                                      := NULL;
   LP_old_clr_reset_date      DATE                                      := NULL;
   LP_old_promo_end_date      DATE                                      := NULL;
   LP_old_timebased_dtl_ind   RPM_BULK_CC_PE.OLD_TIMEBASED_DTL_IND%TYPE := NULL;
   LP_new_clr_reset_date      DATE                                      := NULL;
   LP_cc_error_tbl            CONFLICT_CHECK_ERROR_TBL                  := CONFLICT_CHECK_ERROR_TBL();
   LP_reprocess_ind           NUMBER(1)                                 := NULL;
   LP_batch_name	      			VARCHAR2(200)															:= NULL;

--------------------------------------------------------------------------------
--                            PRIVATE PROTOTYPE                               --
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                      I_bulk_cc_pe_id        IN     NUMBER,
                      I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type     IN     VARCHAR2,
                      I_rib_transaction_id   IN     NUMBER,
                      I_parent_thread_number IN     NUMBER,
                      I_thread_number        IN     NUMBER)
RETURN NUMBER;

FUNCTION PROCESS_PRICE_CHANGES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id    IN     NUMBER,
                               I_persist_ind     IN     VARCHAR2,
                               I_start_state     IN     VARCHAR2,
                               I_end_state       IN     VARCHAR2,
                               I_user_name       IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_CLEARANCES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                            I_rib_trans_id    IN     NUMBER,
                            I_persist_ind     IN     VARCHAR2,
                            I_start_state     IN     VARCHAR2,
                            I_end_state       IN     VARCHAR2,
                            I_user_name       IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_CLEARANCE_RESETS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                  I_rib_trans_id    IN     NUMBER,
                                  I_persist_ind     IN     VARCHAR2,
                                  I_start_state     IN     VARCHAR2,
                                  I_end_state       IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_NEW_CLEARANCE_RESETS(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                                      I_rib_trans_id       IN     NUMBER,
                                      I_persist_ind        IN     VARCHAR2,
                                      I_start_state        IN     VARCHAR2,
                                      I_end_state          IN     VARCHAR2,
                                      I_new_clr_reset_date IN     DATE DEFAULT NULL,
                                      I_user_name          IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_FINANCE_PROMOTION(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                   I_rib_trans_id    IN     NUMBER,
                                   I_persist_ind     IN     VARCHAR2,
                                   I_start_state     IN     VARCHAR2,
                                   I_end_state       IN     VARCHAR2,
                                   I_user_name       IN     VARCHAR2)
RETURN NUMBER;

FUNCTION SKIP_CC_FOR_PROMO(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id      IN     NUMBER,
                           I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                           I_price_event_type   IN     VARCHAR2,
                           I_rib_trans_id       IN     NUMBER,
                           I_persist_ind        IN     VARCHAR2,
                           I_start_state        IN     VARCHAR2,
                           I_end_state          IN     VARCHAR2,
                           I_user_name          IN     VARCHAR2,
                           I_new_promo_end_date IN     DATE)
RETURN NUMBER;

FUNCTION PROCESS_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id    IN     NUMBER,
                               I_persist_ind     IN     VARCHAR2,
                               I_start_state     IN     VARCHAR2,
                               I_end_state       IN     VARCHAR2,
                               I_user_name       IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_UPDATE_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                      I_rib_trans_id    IN     NUMBER,
                                      I_persist_ind     IN     VARCHAR2,
                                      I_start_state     IN     VARCHAR2,
                                      I_end_state       IN     VARCHAR2,
                                      I_promo_end_date  IN     DATE)
RETURN NUMBER;

FUNCTION RESET_PE_STATUS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type IN     VARCHAR2,
                         I_start_state      IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RESET_PE_STATUS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_type IN     VARCHAR2,
                         I_start_state      IN     VARCHAR2)
RETURN NUMBER;

FUNCTION UPDATE_PROMO_DETAIL_DATES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION MERGE_IL_VALIDATION(O_error_message              OUT VARCHAR2,
                             IO_itemloc_validation_tbl IN OUT OBJ_ITEMLOC_VALIDATION_TBL,
                             I_itemloc_validation_tbl  IN     OBJ_ITEMLOC_VALIDATION_TBL)
RETURN NUMBER;

FUNCTION GENERATE_TIMELINE(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id IN     NUMBER,
                           I_pe_no_frs     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION GENERATE_TL_FROM_INTERSECTION(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION GEN_PILE_TL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                     I_bulk_cc_pe_id    IN     NUMBER,
                     I_price_event_type IN     VARCHAR2,
                     I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                     I_step_identifier  IN     NUMBER)
RETURN NUMBER;

FUNCTION GENERATE_CSPFR_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_bulk_cc_pe_id    IN     NUMBER,
                                 I_price_event_type IN     VARCHAR2,
                                 I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_step_identifier  IN     NUMBER)
RETURN NUMBER;

FUNCTION GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_bulk_cc_pe_id    IN     NUMBER,
                               I_price_event_type IN     VARCHAR2,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_step_identifier  IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------------------------------
--                            PUBLIC PROCEDURES                               --
--------------------------------------------------------------------------------

FUNCTION PROCESS_PRICE_EVENTS(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                              I_bulk_cc_pe_id           IN     NUMBER,
                              I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type        IN     VARCHAR2,
                              I_rib_trans_id            IN     NUMBER,
                              I_persist_ind             IN     VARCHAR2,
                              I_start_state             IN     VARCHAR2,
                              I_end_state               IN     VARCHAR2,
                              I_user_name               IN     VARCHAR2,
                              I_emergency_perm          IN     NUMBER DEFAULT 0,
                              I_secondary_bulk_cc_pe_id IN     NUMBER,
                              I_secondary_ind           IN     NUMBER,
                              I_parent_thread_number    IN     NUMBER,
                              I_thread_number           IN     NUMBER,
                              I_old_clr_oostock_date    IN     DATE DEFAULT NULL,
                              I_old_clr_reset_date      IN     DATE DEFAULT NULL)
RETURN NUMBER IS

   L_program    VARCHAR2(45) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_PRICE_EVENTS';

   L_asynch_ind             RPM_BULK_CC_PE.ASYNCH_IND%TYPE             := NULL;
   L_auto_clean_ind         RPM_BULK_CC_PE.AUTO_CLEAN_IND%TYPE         := NULL;
   L_thread_processor_class RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE := NULL;
   L_new_promo_end_date     RPM_BULK_CC_PE.NEW_PROMO_END_DATE%TYPE     := NULL;
   L_cc_error_tbl           CONFLICT_CHECK_ERROR_TBL                   := NULL;

   L_return_code     NUMBER(1)            := NULL;
   L_error_msg       VARCHAR2(2000)       := NULL;
   L_price_event_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_start_time      TIMESTAMP            := SYSTIMESTAMP;
   L_trace_name      VARCHAR2(100)        := 'PPE';

   L_skip_cc_for_cp_approval RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_CP_APPROVAL%TYPE := NULL;

   -- Find Price Events that do not have any item/loc conflicts on them
   cursor C_GET_NO_CON_PE_IDS is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select err.price_event_id
        from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err;

   -- Assign the SGE errors back to the main CC error collection.  To ensure that the SGE's
   -- will have binoculars in the UI.  But before that, the SGE CC errors will have to
   -- to look for real future retail record.  As well as convert the parent price event
   -- to the SGE price event id.
   cursor C_COMBINE_SGE_CC_ERROR is
      -- Get the 'Normal' conflict errors that are not SGE.
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
      union all
      -- Conflict errors that have had SGE's created for it.
      select CONFLICT_CHECK_ERROR_REC(ra.exclusion_price_event_id,
                                      ra.future_retail_id,
                                      ra.error_type,
                                      ra.error_string,
                                      ra.cs_promo_fr_id)
        from (select cc.exclusion_price_event_id,
                     fr.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id,
                     ROW_NUMBER() OVER (PARTITION BY gttd.price_event_id,
                                                     fr.item,
                                                     fr.diff_id,
                                                     fr.location,
                                                     fr.zone_node_type
                                            ORDER BY fr.action_date desc) rank
                from (select /*+ CARDINALITY (err 10) */
                             rpd.promo_dtl_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err,
                             rpm_promo_dtl rpd
                       where err.price_event_id          = rpd.exception_parent_id
                         and LP_price_event_type         IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION)
                         and err.cs_promo_fr_id          is NULL
                         and rpd.sys_generated_exclusion = 1
                      union all
                      select /*+ CARDINALITY (err 10) */
                             rpc.price_change_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err,
                             rpm_price_change rpc
                       where rpc.exception_parent_id     = err.price_event_id
                         and LP_price_event_type         = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                         and rpc.sys_generated_exclusion = 1
                      union all
                      select /*+ CARDINALITY (err 10) */
                             rc.clearance_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err,
                             rpm_clearance rc
                       where rc.exception_parent_id     = err.price_event_id
                         and LP_price_event_type        = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                         and rc.sys_generated_exclusion = 1) cc,
                     rpm_cc_sys_gen_detail_gtt gttd,
                     rpm_future_retail fr
               where cc.cs_promo_fr_id         is NULL
                 and cc.parent_price_event_id  = gttd.price_event_id
                 and gttd.dept                 = fr.dept
                 and gttd.item                 = fr.item
                 and NVL(gttd.diff_id, '-999') = NVL(fr.diff_id, '-999')
                 and gttd.cur_hier_level       = fr.cur_hier_level
                 and gttd.location             = fr.location
                 and gttd.zone_node_type       = fr.zone_node_type
                 and gttd.action_date         >= fr.action_date) ra
       where ra.rank = 1
      union all
      -- Cust Seg conflict errors that have had SGE's created for it for.
      select CONFLICT_CHECK_ERROR_REC(ra.exclusion_price_event_id,
                                      ra.future_retail_id,
                                      ra.error_type,
                                      ra.error_string,
                                      ra.cs_promo_fr_id)
        from (select cc.exclusion_price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     fr.cust_segment_promo_id cs_promo_fr_id,
                     ROW_NUMBER() OVER (PARTITION BY gttd.price_event_id,
                                                     fr.item,
                                                     fr.diff_id,
                                                     fr.location,
                                                     fr.zone_node_type
                                            ORDER BY fr.action_date desc) rank
                from (select /*+ CARDINALITY (err 10) */
                             rpd.promo_dtl_id exclusion_price_event_id,
                             err.price_event_id parent_price_event_id,
                             err.future_retail_id,
                             err.error_type,
                             err.error_string,
                             err.cs_promo_fr_id
                        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err,
                             rpm_promo_dtl rpd
                       where err.price_event_id          = rpd.exception_parent_id
                         and LP_price_event_type         IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                         and rpd.sys_generated_exclusion = 1) cc,
                     rpm_cc_sys_gen_detail_gtt gttd,
                     rpm_cust_segment_promo_fr fr
               where cc.cs_promo_fr_id         is NOT NULL
                 and cc.parent_price_event_id  = gttd.price_event_id
                 and gttd.dept                 = fr.dept
                 and gttd.item                 = fr.item
                 and NVL(gttd.diff_id, '-999') = NVL(fr.diff_id, '-999')
                 and gttd.cur_hier_level       = fr.cur_hier_level
                 and gttd.location             = fr.location
                 and gttd.zone_node_type       = fr.zone_node_type
                 and gttd.action_date         >= fr.action_date) ra
       where ra.rank = 1;

BEGIN

   LP_bulk_cc_pe_id        := I_bulk_cc_pe_id;
   LP_parent_thread_number := I_parent_thread_number;
   LP_thread_number        := I_thread_number;
   LP_price_event_type     := I_price_event_type;

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                      ' - I_thread_number: '||I_thread_number);
                                      
   --Instrumentation
   LP_batch_name  :=  case 
                        when REGEXP_LIKE (I_user_name, '[Nn]ew') then
                           'NIL'
                        when REGEXP_LIKE (I_user_name, '[Ii]njector') then
                           'INJ'
                        when REGEXP_LIKE (I_user_name, '[Ll]oc') then
                           'LM'
                      end;

   BEGIN

      if REGEXP_LIKE (I_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(L_return_code,
                                           L_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program||'-'||LP_bulk_cc_pe_id||'-'||LP_parent_thread_number||'-'||LP_thread_number,
                                           RPM_CONSTANTS.LOG_STARTED,
                                           'Start of '||L_program||' for '||I_price_event_type);

         RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(LP_batch_name||L_trace_name||'-'||LP_parent_thread_number||'-'||LP_thread_number);

      end if;

   EXCEPTION
      when OTHERS then
         goto PROCESS_PRICE_EVENTS_1;

   END;

   <<PROCESS_PRICE_EVENTS_1>>

   select asynch_ind,
          auto_clean_ind,
          thread_processor_class,
          need_il_explode_ind,
          new_promo_end_date,
          old_promo_end_date,
          NVL(sys_gen_excl_reprocess, 0),
          old_timebased_dtl_ind,
          new_clr_reset_date
     into L_asynch_ind,
          L_auto_clean_ind,
          L_thread_processor_class,
          LP_need_il_explode_ind,
          L_new_promo_end_date,
          LP_old_promo_end_date,
          LP_reprocess_ind,
          LP_old_timebased_dtl_ind,
          LP_new_clr_reset_date
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   select do_not_run_cc_for_cp_approval
     into L_skip_cc_for_cp_approval
     from rpm_system_options;

   if L_skip_cc_for_cp_approval = 1 and
      I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      if SKIP_CC_FOR_PROMO(O_cc_error_tbl,
                           I_bulk_cc_pe_id,
                           I_price_event_ids,
                           I_price_event_type,
                           I_rib_trans_id,
                           I_persist_ind,
                           I_start_state,
                           I_end_state,
                           I_user_name,
                           L_new_promo_end_date) = 0 then
         return 0;
      end if;

   else

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION then

         if PROCESS_FINANCE_PROMOTION(O_cc_error_tbl,
                                      I_price_event_ids,
                                      I_rib_trans_id,
                                      I_persist_ind,
                                      I_start_state,
                                      I_end_state,
                                      I_user_name) = 0 then
            return 0;
         end if;

      else

         LP_emergency_perm          := I_emergency_perm;
         LP_secondary_bulk_cc_pe_id := I_secondary_bulk_cc_pe_id;
         LP_need_secondary          := 0;
         LP_secondary_ind           := NVL(I_secondary_ind, 0);

         LP_old_clr_oostock_date := I_old_clr_oostock_date;
         LP_old_clr_reset_date   := I_old_clr_reset_date;

         if POPULATE_GTT(O_cc_error_tbl,
                         I_bulk_cc_pe_id,
                         I_price_event_ids,
                         I_price_event_type,
                         I_rib_trans_id,
                         I_parent_thread_number,
                         I_thread_number) = 0 then
            return 0;
         end if;

         if O_cc_error_tbl is NOT NULL and
            O_cc_error_tbl.COUNT > 0 then

            -- Store price events with conflicts
            L_cc_error_tbl := O_cc_error_tbl;

            open C_GET_NO_CON_PE_IDS;
            fetch C_GET_NO_CON_PE_IDS BULK COLLECT into L_price_event_ids;
            close C_GET_NO_CON_PE_IDS;

            if RESET_PE_STATUS(O_cc_error_tbl,
                               L_cc_error_tbl,
                               I_price_event_type,
                               I_start_state) = 0 then
               return 0;
            end if;

            if RPM_FUTURE_RETAIL_SQL.POPULATE_CC_ERROR_TABLE(L_cc_error_tbl,
                                                             I_price_event_type) = 0 then

               O_cc_error_tbl := L_cc_error_tbl;

               return 0;
            end if;

         else

            L_price_event_ids := I_price_event_ids;

         end if;

         if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

            if PROCESS_PRICE_CHANGES(O_cc_error_tbl,
                                     L_price_event_ids,
                                     I_rib_trans_id,
                                     I_persist_ind,
                                     I_start_state,
                                     I_end_state,
                                     I_user_name) = 0 then
               return 0;
            end if;

            if LP_need_secondary = 1 then

               update rpm_bulk_cc_pe
                  set secondary_ind = 1
                where bulk_cc_pe_id = LP_bulk_cc_pe_id;

               merge into rpm_bulk_cc_pe target
               using (select LP_secondary_bulk_cc_pe_id bulk_cc_pe_id
                        from dual) source
                  on (target.bulk_cc_pe_id = source.bulk_cc_pe_id)
               when MATCHED then
                  update
                     set secondary_bulk_cc_pe_id = NULL,
                         secondary_ind           = 1
               when NOT MATCHED then
                  insert (bulk_cc_pe_id,
                          price_event_type,
                          persist_ind,
                          start_state,
                          end_state,
                          user_name,
                          emergency_perm,
                          secondary_ind,
                          asynch_ind,
                          auto_clean_ind,
                          thread_processor_class,
                          need_il_explode_ind)
                  values (source.bulk_cc_pe_id,
                          I_price_event_type,
                          I_persist_ind,
                          I_start_state,
                          I_end_state,
                          I_user_name,
                          I_emergency_perm,
                          1,
                          L_asynch_ind,
                          L_auto_clean_ind,
                          L_thread_processor_class,
                          LP_need_il_explode_ind);

            end if;

         elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

            if PROCESS_CLEARANCES(O_cc_error_tbl,
                                  L_price_event_ids,
                                  I_rib_trans_id,
                                  I_persist_ind,
                                  I_start_state,
                                  I_end_state,
                                  I_user_name) = 0 then
               return 0;
            end if;

         elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET then

            if PROCESS_CLEARANCE_RESETS(O_cc_error_tbl,
                                        L_price_event_ids,
                                        I_rib_trans_id,
                                        I_persist_ind,
                                        I_start_state,
                                        I_end_state) = 0 then
               return 0;
            end if;

         elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

            if PROCESS_NEW_CLEARANCE_RESETS(O_cc_error_tbl,
                                            L_price_event_ids,
                                            I_rib_trans_id,
                                            I_persist_ind,
                                            I_start_state,
                                            I_end_state,
                                            LP_new_clr_reset_date,
                                            I_user_name) = 0 then
               return 0;
            end if;

         elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

            if PROCESS_PROMO_DETAILS(O_cc_error_tbl,
                                     L_price_event_ids,
                                     I_rib_trans_id,
                                     I_persist_ind,
                                     I_start_state,
                                     I_end_state,
                                     I_user_name) = 0 then
               return 0;
            end if;

         elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

            if PROCESS_UPDATE_PROMO_DETAILS(O_cc_error_tbl,
                                            L_price_event_ids,
                                            I_rib_trans_id,
                                            I_persist_ind,
                                            I_start_state,
                                            I_end_state,
                                            L_new_promo_end_date) = 0 then
               return 0;
            end if;

         end if;

      end if;

   end if;

   -- Assign the SGE errors back to the main CC error collection.  To ensure that the SGE's
   -- will have binoculars in the UI.  But before that, the SGE CC errors will have to
   -- be sanitized to look for real future retail record.  As well as convert the parent price event
   -- to the SGE price event id.
   open C_COMBINE_SGE_CC_ERROR;
   fetch C_COMBINE_SGE_CC_ERROR BULK COLLECT into O_cc_error_tbl;
   close C_COMBINE_SGE_CC_ERROR;

   delete
     from rpm_pe_cc_lock
    where bulk_cc_pe_id  = I_bulk_cc_pe_id
      and price_event_id IN (select VALUE(ids)
                               from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (I_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(L_return_code,
                                           L_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program||'-'||LP_bulk_cc_pe_id||'-'||LP_parent_thread_number||'-'||LP_thread_number,
                                           RPM_CONSTANTS.LOG_COMPLETED,
                                           'End of '||L_program||' for ' ||I_price_event_type);

      end if;

   EXCEPTION
      when OTHERS then
         goto PROCESS_PRICE_EVENTS_2;

   END;

   <<PROCESS_PRICE_EVENTS_2>>

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                               ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: '|| I_parent_thread_number ||
                               ' - I_thread_number: '||I_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PRICE_EVENTS;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEMLOC_LINKCODE(O_error_message              OUT VARCHAR2,
                                   O_itemloc_validation_tbl     OUT OBJ_ITEMLOC_VALIDATION_TBL,
                                   I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                                   I_price_event_type        IN     VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(60) := 'RPM_BULK_CC_ACTIONS_SQL.VALIDATE_ITEMLOC_LINKCODE';

   cursor C_IN_LINKCODE is
      -- Location Level
      -- Item
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NULL
                 and zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and diff_id             is NULL
                 and im.item             = rpc.item
                 and im.tran_level       = im.item_level) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NULL
                 and zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and diff_id             is NULL
                 and im.item_parent      = rpc.item
                 and im.tran_level       = im.item_level) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NULL
                 and zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and diff_id             is NOT NULL
                 and im.item_parent      = rpc.item
                 and (   im.diff_1       = rpc.diff_id
                      or im.diff_2       = rpc.diff_id
                      or im.diff_3       = rpc.diff_id
                      or im.diff_4       = rpc.diff_id)
                 and im.tran_level       = im.item_level) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      -- ItemList
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NOT NULL
                 and zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rps.price_event_id  = rpc.price_change_id
                 and rps.skulist         = rpc.skulist
                 and rps.item_level      = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rps.item
                 and im.tran_level       = im.item_level) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NOT NULL
                 and zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rps.price_event_id  = rpc.price_change_id
                 and rps.skulist         = rpc.skulist
                 and rps.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rps.item
                 and im.tran_level       = im.item_level) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      -- price event itemlist
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rpc.price_change_id      = VALUE(ids)
                 and link_code                is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and zone_node_type           IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item                  = rmld.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rpc.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rpc.price_change_id      = VALUE(ids)
                 and link_code                is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and zone_node_type           IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent           = rmld.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      -- Zone Level
      -- Item
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NULL
                 and zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and diff_id             is NULL
                 and im.item             = rpc.item
                 and im.tran_level       = im.item_level
                 and rzl.zone_id         = rpc.zone_id) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NULL
                 and zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and diff_id             is NULL
                 and im.item_parent      = rpc.item
                 and im.tran_level       = im.item_level
                 and rzl.zone_id         = rpc.zone_id) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NULL
                 and zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and diff_id             is NOT NULL
                 and im.item_parent      = rpc.item
                 and (   im.diff_1       = rpc.diff_id
                      or im.diff_2       = rpc.diff_id
                      or im.diff_3       = rpc.diff_id
                      or im.diff_4       = rpc.diff_id)
                 and im.tran_level       = im.item_level
                 and rzl.zone_id         = rpc.zone_id) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      -- ItemList
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NOT NULL
                 and zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rps.price_event_id  = rpc.price_change_id
                 and rps.skulist         = rpc.skulist
                 and rps.item_level      = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rps.item
                 and im.tran_level       = im.item_level
                 and rzl.zone_id         = rpc.zone_id) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id = VALUE(ids)
                 and link_code           is NULL
                 and rpc.skulist         is NOT NULL
                 and zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rps.price_event_id  = rpc.price_change_id
                 and rps.skulist         = rpc.skulist
                 and rps.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rps.item
                 and im.tran_level       = im.item_level
                 and rzl.zone_id         = rpc.zone_id) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      -- price event itemlist
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id      = VALUE(ids)
                 and link_code                is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and zone_node_type           = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item                  = rmld.item
                 and im.tran_level            = im.item_level
                 and rzl.zone_id              = rpc.zone_id
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location
      union all
      select /*+ INDEX(rlca RPM_LINK_CODE_ATTRIBUTE_I2) */
             OBJ_ITEMLOC_VALIDATION_REC(rpc1.item,
                                        rpc1.location,
                                        rlca.link_code,
                                        'item_location_attached_to_link_code')
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     rzl.location
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl
               where rpc.price_change_id      = VALUE(ids)
                 and link_code                is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and zone_node_type           = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent           = rmld.item
                 and im.tran_level            = im.item_level
                 and rzl.zone_id              = rpc.zone_id
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) rpc1,
             rpm_link_code_attribute rlca
       where rlca.item     = rpc1.item
         and rlca.location = rpc1.location;

BEGIN

   O_itemloc_validation_tbl := OBJ_ITEMLOC_VALIDATION_TBL();

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then
      open C_IN_LINKCODE;
      fetch C_IN_LINKCODE BULK COLLECT into O_itemloc_validation_tbl limit 1000;
      close C_IN_LINKCODE;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_ITEMLOC_LINKCODE;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEMLOC_RANGING(O_error_message              OUT VARCHAR2,
                                  O_itemloc_validation_tbl     OUT OBJ_ITEMLOC_VALIDATION_TBL,
                                  I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                                  I_price_event_type        IN     VARCHAR2)
RETURN NUMBER IS

   L_program   VARCHAR2(60) := 'RPM_BULK_CC_ACTIONS_SQL.VALIDATE_ITEMLOC_RANGING';

   L_zone_ranging            RPM_SYSTEM_OPTIONS.ZONE_RANGING%TYPE  := NULL;

   L_itemloc_validation_tbl  OBJ_ITEMLOC_VALIDATION_TBL  := NULL;
   L_price_event_ids         OBJ_NUMERIC_ID_TABLE        := NULL;

   cursor C_CHECK_LINK_CODE_PC is
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.link_code,
                                         NULL,
                                         NULL,
                                         'linkcode_is_invalid'),
             rpc1.price_change_id
        from (select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
                     rpc.price_change_id,
                     rpc.link_code,
                     case
                        when rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) and
                           NOT EXISTS (select 1
                                         from rpm_link_code_attribute rlca
                                        where rlca.location = rpc.location) then
                           2
                        when rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                           NOT EXISTS (select 1
                                         from rpm_link_code_attribute rlca,
                                              rpm_zone_location rzl
                                        where rzl.zone_id   = rpc.zone_id
                                          and rlca.location = rzl.location) then
                           2
                        else
                           0
                     end as invalid
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc
               where rpc.price_change_id = VALUE(ids)
                 and rpc.link_code       is NOT NULL) rpc1
       where rpc1.invalid != 0;

   cursor C_CHECK_LINK_CODE_RANGE is
      select OBJ_ITEMLOC_VALIDATION_REC (rlca1.item,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids1 10) CARDINALITY(ids2 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
                     rpc.link_code,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id
                from (select value(ids1) price_event_id
                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids1
                      minus
                      select value(ids2) price_event_id
                        from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids2) ids,
                     rpm_price_change rpc
               where rpc.price_change_id = ids.price_event_id
                 and rpc.link_code       is NOT NULL
                 and (   (    rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from rpm_link_code_attribute rlca,
                                                 item_loc il
                                           where rlca.link_code = rpc.link_code
                                             and rlca.location  = rpc.location
                                             and il.item        = rlca.item
                                             and il.loc         = rlca.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from rpm_link_code_attribute rlca,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where rlca.link_code = rpc.link_code
                                             and rzl.zone_id    = rpc.zone_id
                                             and rlca.location  = rzl.location
                                             and il.item        = rlca.item
                                             and il.loc         = rlca.location)))) rpc1,
             rpm_link_code_attribute rlca1,
             rpm_zone rz
       where rlca1.link_code = rpc1.link_code
         and rz.zone_id(+)   = rpc1.zone_id;

   cursor C_CHECK_PC_IL_RANGE is
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.item,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER)*/
                     rpc.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and rpc.link_code       is NULL
                 and rpc.skulist         is NULL
                 and diff_id             is NULL
                 and im.item             = rpc.item
                 and im.tran_level       = im.item_level
                 and (   (   rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_loc il
                                           where il.item = rpc.item
                                             and il.loc  = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from rpm_zone_location rzl,
                                                 item_loc il
                                           where rzl.zone_id = rpc.zone_id
                                             and il.item     = rpc.item
                                             and il.loc      = rzl.location)))) rpc1,
             rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.item,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and rpc.link_code       is NULL
                 and rpc.skulist         is NULL
                 and diff_id             is NULL
                 and im.item             = rpc.item
                 and im.tran_level       < im.item_level
                 and (   (    rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                              item_loc il
                                        where im1.item_parent = rpc.item
                                          and im1.tran_level  = im1.item_level
                                          and il.item         = im1.item
                                          and il.loc          = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rpc.item
                                             and im1.tran_level  = im1.item_level
                                             and rzl.zone_id     = rpc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rpc1,
          rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.item,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and rpc.link_code       is NULL
                 and rpc.skulist         is NULL
                 and diff_id             is NOT NULL
                 and im.item             = rpc.item
                 and im.tran_level       < im.item_level
                 and (   (    rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 item_loc il
                                           where im1.item_parent = rpc.item
                                             and im1.tran_level  = im1.item_level
                                             and (   im1.diff_1  = rpc.diff_id
                                                  or im1.diff_2  = rpc.diff_id
                                                  or im1.diff_3  = rpc.diff_id
                                                  or im1.diff_4  = rpc.diff_id)
                                             and il.item         = im1.item
                                             and il.loc          = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rpc.item
                                             and im1.tran_level  = im1.item_level
                                             and (   im1.diff_1  = rpc.diff_id
                                                  or im1.diff_2  = rpc.diff_id
                                                  or im1.diff_3  = rpc.diff_id
                                                  or im1.diff_4  = rpc.diff_id)
                                             and rzl.zone_id     = rpc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rpc1,
             rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.skulist,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'itemlist_zone_is_invalid',
                                                'itemlist_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER)*/
                     rpc.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id,
                     rps.skulist
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and rpc.link_code       is NULL
                 and rpc.skulist         is NOT NULL
                 and rps.price_event_id  = rpc.price_change_id
                 and rps.skulist         = rpc.skulist
                 and im.item             = rps.item
                 and im.tran_level       = im.item_level
                 and (   (   rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_loc il
                                           where il.item = rps.item
                                             and il.loc  = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from rpm_zone_location rzl,
                                                 item_loc il
                                           where rzl.zone_id = rpc.zone_id
                                             and il.item     = rps.item
                                             and il.loc      = rzl.location)))) rpc1,
             rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.skulist,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'itemlist_zone_is_invalid',
                                                'itemlist_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id,
                     rps.skulist
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im
               where rpc.price_change_id = VALUE(ids)
                 and rpc.link_code       is NULL
                 and rpc.skulist         is NOT NULL
                 and rps.price_event_id  = rpc.price_change_id
                 and rps.skulist         = rpc.skulist
                 and im.item             = rps.item
                 and im.tran_level       > im.item_level
                 and (   (    rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 item_loc il
                                           where im1.item_parent = rps.item
                                             and im1.tran_level  = im1.item_level
                                             and il.item         = im1.item
                                             and il.loc          = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rps.item
                                             and im1.tran_level  = im1.item_level
                                             and rzl.zone_id     = rpc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rpc1,
          rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.merch_list_id,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'peil_zone_is_invalid',
                                                'peil_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER)*/
                     rpc.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id,
                     rmld.merch_list_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.link_code            is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and im.item                  = rmld.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and (   (   rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_loc il
                                           where il.item = rmld.item
                                             and il.loc  = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from rpm_zone_location rzl,
                                                 item_loc il
                                           where rzl.zone_id = rpc.zone_id
                                             and il.item     = rmld.item
                                             and il.loc      = rzl.location)))) rpc1,
             rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rpc1.merch_list_id,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rpc1.location),
                                         NULL,
                                         DECODE(rpc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'itemlist_zone_is_invalid',
                                                'itemlist_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rpc.zone_node_type,
                     rpc.location,
                     rpc.zone_id,
                     rmld.merch_list_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.link_code            is NULL
                 and rpc.price_event_itemlist is NOT NULL
                 and rmld.merch_list_id       = rpc.price_event_itemlist
                 and im.item                  = rmld.item
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.tran_level            > im.item_level
                 and (   (    rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 item_loc il
                                           where im1.item_parent = rmld.item
                                             and im1.tran_level  = im1.item_level
                                             and il.item         = im1.item
                                             and il.loc          = rpc.location))
                      or (    rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging     = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rmld.item
                                             and im1.tran_level  = im1.item_level
                                             and rzl.zone_id     = rpc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rpc1,
          rpm_zone rz
       where rz.zone_id(+)   = rpc1.zone_id;

   cursor C_CHECK_CL_IL_RANGE is
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.item,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im PK_ITEM_MASTER)*/
                     rc.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     item_master im
               where rc.clearance_id = VALUE(ids)
                 and rc.skulist      is NULL
                 and diff_id is NULL
                 and im.item = rc.item
                 and im.tran_level = im.item_level
                 and (    (   rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and NOT EXISTS (select 1
                                             from item_loc il
                                            where il.item = rc.item
                                              and il.loc  = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from rpm_zone_location rzl,
                                                 item_loc il
                                           where rzl.zone_id = rc.zone_id
                                             and il.item     = rc.item
                                             and il.loc      = rzl.location)))) rc1,
             rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.item,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     item_master im
               where rc.clearance_id = VALUE(ids)
                 and rc.skulist      is NULL
                 and diff_id         is NULL
                 and im.item         = rc.item
                 and im.tran_level   < im.item_level
                 and (    (   rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and NOT EXISTS (select 1
                                             from item_master im1,
                                                  item_loc il
                                            where im1.item_parent = rc.item
                                              and im1.tran_level  = im1.item_level
                                              and il.item         = im1.item
                                              and il.loc          = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rc.item
                                             and im1.tran_level  = im1.item_level
                                             and rzl.zone_id     = rc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rc1,
                   rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.item,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'item_zone_is_invalid',
                                                'item_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     item_master im
               where rc.clearance_id = VALUE(ids)
                 and rc.skulist      is NULL
                 and diff_id         is NOT NULL
                 and im.item         = rc.item
                 and im.tran_level   < im.item_level
                 and (   (    rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 item_loc il
                                           where im1.item_parent = rc.item
                                             and im1.tran_level  = im1.item_level
                                             and (   im1.diff_1  = rc.diff_id
                                                  or im1.diff_2  = rc.diff_id
                                                  or im1.diff_3  = rc.diff_id
                                                  or im1.diff_4  = rc.diff_id)
                                             and il.item         = im1.item
                                             and il.loc          = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rc.item
                                             and im1.tran_level  = im1.item_level
                                             and (   im1.diff_1  = rc.diff_id
                                                  or im1.diff_2  = rc.diff_id
                                                  or im1.diff_3  = rc.diff_id
                                                  or im1.diff_4  = rc.diff_id)
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rc1,
             rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.skulist,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'itemlist_zone_is_invalid',
                                                'itemlist_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im PK_ITEM_MASTER)*/
                     rc.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id,
                     rcs.skulist
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     rpm_clearance_skulist rcs,
                     item_master im
               where rc.clearance_id    = VALUE(ids)
                 and rc.skulist         is NOT NULL
                 and rcs.price_event_id = rc.clearance_id
                 and rcs.skulist        = rc.skulist
                 and im.item            = rcs.item
                 and im.tran_level      = im.item_level
                 and (    (   rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and NOT EXISTS (select 1
                                             from item_loc il
                                            where il.item = rcs.item
                                              and il.loc  = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from rpm_zone_location rzl,
                                                 item_loc il
                                           where rzl.zone_id = rc.zone_id
                                             and il.item     = rcs.item
                                             and il.loc      = rzl.location)))) rc1,
             rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.skulist,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'itemlist_zone_is_invalid',
                                                'itemlist_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id,
                     rcs.skulist
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     rpm_clearance_skulist rcs,
                     item_master im
               where rc.clearance_id    = VALUE(ids)
                 and rc.skulist         is NOT NULL
                 and rcs.price_event_id = rc.clearance_id
                 and rcs.skulist        = rc.skulist
                 and im.item            = rcs.item
                 and im.tran_level      > im.item_level
                 and (    (   rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and NOT EXISTS (select 1
                                             from item_master im1,
                                                  item_loc il
                                            where im1.item_parent = rcs.item
                                              and im1.tran_level  = im1.item_level
                                              and il.item         = im1.item
                                              and il.loc          = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rcs.item
                                             and im1.tran_level  = im1.item_level
                                             and rzl.zone_id     = rc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rc1,
                   rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.merch_list_id,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'peil_zone_is_invalid',
                                                'peil_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im PK_ITEM_MASTER)*/
                     rc.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id,
                     rmld.merch_list_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rc.clearance_id         = VALUE(ids)
                 and rc.price_event_itemlist is NOT NULL
                 and rmld.merch_list_id      = rc.price_event_itemlist
                 and im.item                 = rmld.item
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.tran_level           = im.item_level
                 and (    (   rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and NOT EXISTS (select 1
                                             from item_loc il
                                            where il.item = rmld.item
                                              and il.loc  = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from rpm_zone_location rzl,
                                                 item_loc il
                                           where rzl.zone_id = rc.zone_id
                                             and il.item     = rmld.item
                                             and il.loc      = rzl.location)))) rc1,
             rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id
      union all
      select OBJ_ITEMLOC_VALIDATION_REC (rc1.merch_list_id,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rz.zone_display_id,
                                                rc1.location),
                                         NULL,
                                         DECODE(rc1.zone_node_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, 'itemlist_zone_is_invalid',
                                                'itemlist_location_is_invalid'))
        from (select /*+ CARDINALITY(ids 10) INDEX(rc PK_rpm_clearance) INDEX(im ITEM_MASTER_I1)*/
                     im.item,
                     rc.zone_node_type,
                     rc.location,
                     rc.zone_id,
                     rmld.merch_list_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_clearance rc,
                     rpm_merch_list_detail rmld,
                     item_master im
               where rc.clearance_id         = VALUE(ids)
                 and rc.price_event_itemlist is NOT NULL
                 and rmld.merch_list_id      = rc.price_event_itemlist
                 and im.item                 = rmld.item
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.tran_level           > im.item_level
                 and (    (   rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and NOT EXISTS (select 1
                                             from item_master im1,
                                                  item_loc il
                                            where im1.item_parent = rmld.item
                                              and im1.tran_level  = im1.item_level
                                              and il.item         = im1.item
                                              and il.loc          = rc.location))
                      or (    rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and L_zone_ranging    = 1
                          and NOT EXISTS (select 1
                                            from item_master im1,
                                                 rpm_zone_location rzl,
                                                 item_loc il
                                           where im1.item_parent = rmld.item
                                             and im1.tran_level  = im1.item_level
                                             and rzl.zone_id     = rc.zone_id
                                             and il.item         = im1.item
                                             and il.loc          = rzl.location)))) rc1,
                   rpm_zone rz
       where rz.zone_id(+)   = rc1.zone_id;

BEGIN

   O_itemloc_validation_tbl := OBJ_ITEMLOC_VALIDATION_TBL();

   select zone_ranging into L_zone_ranging
     from rpm_system_options;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_CHECK_LINK_CODE_PC;
      fetch C_CHECK_LINK_CODE_PC BULK COLLECT into L_itemloc_validation_tbl,
                                                   L_price_event_ids;
      close C_CHECK_LINK_CODE_PC;

      if MERGE_IL_VALIDATION(O_error_message,
                             O_itemloc_validation_tbl,
                             L_itemloc_validation_tbl) = 0 then
         return 0;
      end if;

      open C_CHECK_LINK_CODE_RANGE;
      fetch C_CHECK_LINK_CODE_RANGE BULK COLLECT into L_itemloc_validation_tbl;
      close C_CHECK_LINK_CODE_RANGE;

      if MERGE_IL_VALIDATION(O_error_message,
                             O_itemloc_validation_tbl,
                             L_itemloc_validation_tbl) = 0 then
         return 0;
      end if;

      open C_CHECK_PC_IL_RANGE;
      fetch C_CHECK_PC_IL_RANGE BULK COLLECT into L_itemloc_validation_tbl;
      close C_CHECK_PC_IL_RANGE;

      if MERGE_IL_VALIDATION(O_error_message,
                             O_itemloc_validation_tbl,
                             L_itemloc_validation_tbl) = 0 then
         return 0;
      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      open C_CHECK_CL_IL_RANGE;
      fetch C_CHECK_CL_IL_RANGE BULK COLLECT into O_itemloc_validation_tbl;
      close C_CHECK_CL_IL_RANGE;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_ITEMLOC_RANGING;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_CLEARANCE_DATES(O_error_message              OUT VARCHAR2,
                                  O_validation_tbl             OUT OBJ_ITEMLOC_VALIDATION_TBL,
                                  I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                                  I_price_event_type        IN     VARCHAR2)
RETURN NUMBER IS

   L_program          VARCHAR2(100) := 'RPM_BULK_CC_ACTIONS_SQL.VALIDATE_CLEARANCE_DATES';

   L_validation_tbl   OBJ_ITEMLOC_VALIDATION_TBL;

   cursor C_DATES is
      select /*+ CARDINALITY(ids 10) INDEX(rc PK_RPM_CLEARANCE) */
             OBJ_ITEMLOC_VALIDATION_REC (NULL,
                                         NULL,
                                         NULL,
                                         case
                                            when (rc.out_of_stock_date is NOT NULL and
                                                  rc.out_of_stock_date <= rc.effective_date) then
                                               'clearance_out_of_stock_date_before_effective_date'
                                            else
                                               'clearance_reset_date_before_effective_date'
                                         end)
     from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
          rpm_clearance rc
    where rc.clearance_id = VALUE(ids)
      and ((    rc.out_of_stock_date is NOT NULL
            and rc.out_of_stock_date <= rc.effective_date)
       or (     rc.reset_date is NOT NULL
            and rc.reset_date <= rc.effective_date)
       or (     rc.reset_date        is NOT NULL
            and rc.out_of_stock_date is NOT NULL
            and rc.out_of_stock_date > rc.reset_date))
      and rownum = 1;

BEGIN

   O_validation_tbl := OBJ_ITEMLOC_VALIDATION_TBL();

   open C_DATES;
   fetch C_DATES BULK COLLECT into L_validation_tbl;
   close C_DATES;

   if L_validation_tbl is NOT NULL and
      L_validation_tbl.count > 0 then
      O_validation_tbl := L_validation_tbl;
      return 1;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;

END VALIDATE_CLEARANCE_DATES;
--------------------------------------------------------------------------------
--                            PRIVATE PROCEDURES                              --
--------------------------------------------------------------------------------
FUNCTION MERGE_IL_VALIDATION(O_error_message                OUT VARCHAR2,
                             IO_itemloc_validation_tbl   IN OUT OBJ_ITEMLOC_VALIDATION_TBL,
                             I_itemloc_validation_tbl    IN     OBJ_ITEMLOC_VALIDATION_TBL)
RETURN NUMBER IS
--
L_program   VARCHAR2(100) := 'RPM_BULK_CC_ACTIONS_SQL.MERGE_IL_VALIDATION';
--
BEGIN
   --
   if I_itemloc_validation_tbl is NOT NULL and
      I_itemloc_validation_tbl.count > 0 then
      for I IN 1..I_itemloc_validation_tbl.count loop
         IO_itemloc_validation_tbl.extend;
         IO_itemloc_validation_tbl(IO_itemloc_validation_tbl.count) := I_itemloc_validation_tbl(i);
      end loop;
   end if;
   --
   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            to_char(SQLCODE));
      return 0;

END MERGE_IL_VALIDATION;
--------------------------------------------------------------------------------

FUNCTION POPULATE_GTT(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                      I_bulk_cc_pe_id        IN     NUMBER,
                      I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type     IN     VARCHAR2,
                      I_rib_transaction_id   IN     NUMBER,
                      I_parent_thread_number IN     NUMBER,
                      I_thread_number        IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_BULK_CC_ACTIONS_SQL.POPULATE_GTT';

   L_pe_no_frs       OBJ_NUMERIC_ID_TABLE     := NULL;
   L_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;
   L_cc_error_tbl    CONFLICT_CHECK_ERROR_TBL := NULL;
   L_start_time      TIMESTAMP                := SYSTIMESTAMP;
   L_error_message   VARCHAR2(255)            := NULL;

   cursor C_PE_IDS is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select err.price_event_id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) err;

   cursor C_NO_FR is
      select distinct price_event_id
        from ((select i.price_event_id,
                      i.item,
                      i.diff_id,
                      l.location,
                      l.zone_node_type
                 from (select it.bulk_cc_pe_id,
                              it.thread_number,
                              it.price_event_id,
                              it.item,
                              it.diff_id,
                              it.itemloc_id,
                              case
                                 when t.pe_zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) then
                                    t.pe_zone_node_type
                                 when t.pe_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                                      t.zone_group_id = rmrde.regular_zone_group then
                                    t.pe_zone_node_type
                                 else
                                    NULL
                              end pe_zone_node_type,
                              case
                                 when t.pe_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                                      t.zone_group_id = rmrde.regular_zone_group then
                                    t.location
                                 else
                                    NULL
                              end pe_zone_id,
                              RANK() OVER (PARTITION BY it.item,
                                                        it.diff_id,
                                                        t.pe_zone_node_type,
                                                        t.location
                                               ORDER BY NVL(it.txn_man_excluded_item, 0),
                                                        it.pe_merch_level desc,
                                                        it.price_event_id) rank
                         from rpm_bulk_cc_pe_item it,
                              rpm_merch_retail_def_expl rmrde,
                              (select /*+ CARDINALITY (ids 10) */
                                      rpc.price_change_id price_event_id,
                                      DECODE(rpc.zone_node_type,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpc.zone_id,
                                             rpc.location) location,
                                      rpc.zone_node_type pe_zone_node_type,
                                      rz.zone_group_id
                                 from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_price_change rpc,
                                      rpm_zone rz
                                where I_price_event_type  = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and rpc.price_change_id = VALUE(ids)
                                  and rpc.zone_id         = rz.zone_id (+)
                                  and rpc.link_code       is NULL
                               union all
                               select /*+ CARDINALITY (ids 10) */
                                      rc.clearance_id price_event_id,
                                      DECODE(rc.zone_node_type,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rc.zone_id,
                                             rc.location) location,
                                      rc.zone_node_type pe_zone_node_type,
                                      rz.zone_group_id
                                 from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_clearance rc,
                                      rpm_zone rz
                                where I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET)
                                  and rc.clearance_id    = VALUE(ids)
                                  and rc.zone_id         = rz.zone_id (+)
                               union all
                               select /*+ CARDINALITY (ids 10) */
                                      rc.clearance_id price_event_id,
                                      rc.location,
                                      rc.zone_node_type pe_zone_node_type,
                                      NULL
                                 from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_clearance_reset rc
                                where I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET
                                  and rc.clearance_id    = VALUE(ids)
                               union all
                               select /*+ CARDINALITY (ids 10) */
                                      rpzl.promo_dtl_id price_event_id,
                                      DECODE(rpzl.zone_node_type,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpzl.zone_id,
                                             rpzl.location) location,
                                      rpzl.zone_node_type pe_zone_node_type,
                                      rz.zone_group_id
                                 from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_promo_zone_location rpzl,
                                      rpm_zone rz
                                where I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
                                  and rpzl.promo_dtl_id = VALUE(ids)
                                  and rpzl.zone_id      = rz.zone_id (+)) t,
                              rpm_bulk_cc_pe bcp
                        where bcp.bulk_cc_pe_id                 = I_bulk_cc_pe_id
                          and bcp.location_move_id              is NULL
                          and it.bulk_cc_pe_id                  = bcp.bulk_cc_pe_id
                          and it.thread_number                  = I_thread_number
                          and NVL(it.txn_man_excluded_item, 0) != 1
                          and it.merch_level_type               = it.pe_merch_level
                          and rmrde.dept                        = it.dept
                          and rmrde.class                       = it.class
                          and rmrde.subclass                    = it.subclass
                          and t.price_event_id                  = it.price_event_id
                          and rownum                            > 0) i,
                      rpm_bulk_cc_pe_location l
                where i.rank           = 1
                  and l.bulk_cc_pe_id  = i.bulk_cc_pe_id
                  and l.price_event_id = i.price_event_id
                  and l.itemloc_id     = i.itemloc_id
                  and (   (    i.pe_zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and l.zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE))
                       or (    i.pe_zone_node_type is NULL
                           and l.zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE))
                       or (    i.pe_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           and l.zone_node_type    = i.pe_zone_node_type
                           and l.location          = i.pe_zone_id))
              union all
              -- Price Change is Location Level and Link Code is not NULL
              select /*+ CARDINALITY (ids 10) */
                     rpc.price_change_id price_event_id,
                     rlca.item,
                     NULL diff_id,
                     rlca.location,
                     rlca.loc_type zone_node_type
                from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_link_code_attribute rlca,
                     rpm_bulk_cc_pe bcp
               where I_price_event_type   = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and bcp.bulk_cc_pe_id    = I_bulk_cc_pe_id
                 and bcp.location_move_id is NULL
                 and rpc.price_change_id  = VALUE(ids)
                 and rpc.link_code        is NOT NULL
                 and rpc.link_code        = rlca.link_code
                 and rpc.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rpc.location         = rlca.location
              union all
              -- Price Changes with Link Code and Zone Level
              select /*+ CARDINALITY (ids 10) */
                     rpc.price_change_id price_event_id,
                     rlca.item,
                     NULL diff_id,
                     rlca.location,
                     rlca.loc_type zone_node_type
                from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_link_code_attribute rlca,
                     rpm_zone_location rzl,
                     rpm_bulk_cc_pe bcp
               where I_price_event_type   = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and bcp.bulk_cc_pe_id    = I_bulk_cc_pe_id
                 and bcp.location_move_id is NULL
                 and rpc.price_change_id  = VALUE(ids)
                 and rpc.link_code        is NOT NULL
                 and rpc.link_code        = rlca.link_code
                 and rpc.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.zone_id          = rzl.zone_id
                 and rzl.location         = rlca.location
                union all
                select /*+ CARDINALITY (ids 10) */
                       i.price_event_id,
                       i.item,
                       i.diff_id,
                       l.location,
                       l.zone_node_type
                  from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_bulk_cc_pe_item i,
                       rpm_bulk_cc_pe_location l,
                       rpm_bulk_cc_pe bcp
                 where bcp.bulk_cc_pe_id                = I_bulk_cc_pe_id
                   and bcp.location_move_id             is NOT NULL
                   and i.price_event_id                 = VALUE(ids)
                   and i.bulk_cc_pe_id                  = bcp.bulk_cc_pe_id
                   and i.thread_number                  = I_thread_number
                   and i.merch_level_type               = i.pe_merch_level
                   and NVL(i.txn_man_excluded_item, 0) != 1
                   and l.bulk_cc_pe_id                  = i.bulk_cc_pe_id
                   and l.price_event_id                 = i.price_event_id
                   and l.itemloc_id                     = i.itemloc_id)
              minus
              select price_event_id,
                     item,
                     diff_id,
                     location,
                     regular_zone_group zone_node_type
                from rpm_item_loc_gtt);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                      ' - I_thread_number: '||I_thread_number);

   if RPM_BULK_CC_THREADING_SQL.LOCK_PE(L_cc_error_tbl,
                                        I_bulk_cc_pe_id,
                                        I_rib_transaction_id,
                                        I_parent_thread_number,
                                        I_thread_number,
                                        I_price_event_ids) = 0 then
      return 0;
   end if;

   open C_PE_IDS;
   fetch C_PE_IDS BULK COLLECT into L_price_event_ids;
   close C_PE_IDS;

   delete
     from rpm_future_retail_gtt;

   delete
     from rpm_promo_item_loc_expl_gtt;

   delete
     from rpm_cust_segment_promo_fr_gtt;

   -- Populate FR GTT
   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      diff_id,
                                      item_parent,
                                      zone_node_type,
                                      location,
                                      zone_id,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      rfr_rowid,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      max_hier_level,
                                      cur_hier_level,
                                      step_identifier)
      ----
      -- Get all records on rpm_future_retail...
      ----
      with iloc as
         (select /*+ ORDERED CARDINALITY(ids, 10) */
                 distinct rpi.dept,
                 rpi.price_event_id,
                 rpi.item,
                 rpi.diff_id,
                 rpl.location,
                 rpl.zone_node_type,
                 DENSE_RANK() OVER (PARTITION BY rpi.item,
                                                 rpi.diff_id,
                                                 rpl.zone_node_type,
                                                 rpl.location
                                        ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                 rpi.pe_merch_level desc,
                                                 rpi.price_event_id) rank
            from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_bulk_cc_pe_item rpi,
                 rpm_bulk_cc_pe_location rpl
           where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
             and rpi.price_event_id = VALUE(ids)
             and rpl.price_event_id = VALUE(ids)
             and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
             and rpl.price_event_id = rpi.price_event_id
             and rpl.itemloc_id     = rpi.itemloc_id
             and rownum             > 0)
      select /*+ ORDERED INDEX(fr, RPM_FUTURE_RETAIL_I1) */
             t.price_event_id,
             future_retail_id,
             fr.dept,
             class,
             subclass,
             fr.item,
             fr.diff_id,
             fr.item_parent,
             fr.zone_node_type,
             fr.location,
             fr.zone_id,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             fr.rowid,
             fr.on_simple_promo_ind,
             fr.on_complex_promo_ind,
             fr.max_hier_level,
             fr.cur_hier_level,
             RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR
        from iloc t,
             rpm_future_retail fr
       where t.rank                  = 1
         and fr.dept                 = t.dept
         and fr.item                 = t.item
         and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
         and fr.location             = t.location
         and fr.zone_node_type       = t.zone_node_type;

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                      ' - I_thread_number: '||I_thread_number ||
                                      ' - Insert into rpm_future_retail_gtt - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   -- Use rpm_item_loc_gtt to store what timeline already exists in Future Retail
   delete rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 diff_id,
                                 location,
                                 regular_zone_group)  -- Note that this is used as zone_node_type
      select t.price_event_id,
             t.item,
             t.diff_id,
             t.location,
             t.zone_node_type
        from (select /*+ CARDINALITY (ids, 10) */
                     fr.price_event_id,
                     fr.item,
                     fr.diff_id,
                     fr.location,
                     fr.zone_node_type,
                     ROW_NUMBER() OVER (PARTITION BY fr.price_event_id,
                                                     fr.item,
                                                     fr.diff_id,
                                                     fr.location,
                                                     fr.zone_node_type
                                            ORDER BY fr.action_date desc) rank
                from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_future_retail_gtt fr
               where fr.price_event_id = VALUE(ids)) t
       where t.rank = 1;

   open C_NO_FR;
   fetch C_NO_FR BULK COLLECT into L_pe_no_frs;
   close C_NO_FR;

   if L_pe_no_frs is NOT NULL and
      L_pe_no_frs.COUNT > 0 then

      -- Populate FR GTT for Price Events that are defined at the level that there is no FR record yet.
      -- By copying existing parent level Future Retail records. Need to populate the FUTURE_RETAIL_ID
      -- column from the sequence but should leave the rfr_rowid to NULL so it will be pushed back

      if GENERATE_TIMELINE(O_cc_error_tbl,
                           I_bulk_cc_pe_id,
                           L_pe_no_frs) != 1 then
         return 0;
      end if;

      -- Need to verify if a child timeline needs to be generated due to a new intersection.
      if GENERATE_TL_FROM_INTERSECTION(O_cc_error_tbl,
                                       I_bulk_cc_pe_id,
                                       L_price_event_ids) != 1 then
         return 0;
      end if;

   end if;

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      -- get all promo_item_loc_expl data where it's not cust segment
      -- specific - i.e., all cust segment promos could be affected regardless
      -- of the customer type on the promo

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               diff_id,
                                               item_parent,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               zone_id,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               timebased_dtl_ind,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               max_hier_level,
                                               cur_hier_level,
                                               step_identifier)
         select /*+ ORDERED INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(ilex) */
                t.price_event_id,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.diff_id,
                ilex.item_parent,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.zone_node_type,
                ilex.zone_id,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.timebased_dtl_ind,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                max_hier_level,
                cur_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE_NON_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                ROW_NUMBER() OVER (PARTITION BY rpi.dept,
                                                                rpi.price_event_id,
                                                                rpi.item,
                                                                rpi.diff_id,
                                                                rpl.location,
                                                                rpl.zone_node_type
                                                       ORDER BY rpi.item) rank_value
                           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                            and rpi.price_event_id = VALUE(ids)
                            and rpl.price_event_id = VALUE(ids)
                            and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id = rpi.price_event_id
                            and rpl.itemloc_id     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_promo_item_loc_expl ilex
          where ilex.dept                 = t.dept
            and ilex.item                 = t.item
            and NVL(ilex.diff_id, '-999') = NVL(t.diff_id, '-999')
            and ilex.location             = t.location
            and ilex.zone_node_type       = t.zone_node_type;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - Insert into rpm_promo_item_loc_expl_gtt for non-promo price events - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE_NON_PR,
                                                      0, -- RFR
                                                      1, -- RPILE
                                                      0, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if GEN_PILE_TL(O_cc_error_tbl,
                     I_bulk_cc_pe_id,
                     I_price_event_type,
                     L_pe_no_frs,
                     RPM_CONSTANTS.CAPT_GTT_POP_GTT_GEN_PILE_TL) = 0 then
         return 0;
      end if;

      -- Get all records from cust segment promo fr where the customer
      -- type doesn't matter - i.e. all customer types could be affected
      -- by the price event...

      insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                                 cust_segment_promo_id,
                                                 item,
                                                 zone_node_type,
                                                 location,
                                                 action_date,
                                                 customer_type,
                                                 dept,
                                                 promo_retail,
                                                 promo_retail_currency,
                                                 promo_uom,
                                                 complex_promo_ind,
                                                 cspfr_rowid,
                                                 item_parent,
                                                 diff_id,
                                                 zone_id,
                                                 max_hier_level,
                                                 cur_hier_level,
                                                 step_identifier)
         select /*+ ORDERED */
                t.price_event_id,
                cspfr.cust_segment_promo_id,
                cspfr.item,
                cspfr.zone_node_type,
                cspfr.location,
                cspfr.action_date,
                cspfr.customer_type,
                cspfr.dept,
                cspfr.promo_retail,
                cspfr.promo_retail_currency,
                cspfr.promo_uom,
                cspfr.complex_promo_ind,
                cspfr.rowid,
                cspfr.item_parent,
                cspfr.diff_id,
                cspfr.zone_id,
                cspfr.max_hier_level,
                cspfr.cur_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_NON_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                ROW_NUMBER() OVER (PARTITION BY rpi.dept,
                                                                rpi.price_event_id,
                                                                rpi.item,
                                                                rpi.diff_id,
                                                                rpl.location,
                                                                rpl.zone_node_type
                                                       ORDER BY rpi.item) rank_value
                           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                            and rpi.price_event_id = VALUE(ids)
                            and rpl.price_event_id = VALUE(ids)
                            and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id = rpi.price_event_id
                            and rpl.itemloc_id     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_cust_segment_promo_fr cspfr
          where cspfr.dept                 = t.dept
            and cspfr.item                 = t.item
            and NVL(cspfr.diff_id, '-999') = NVL(t.diff_id, '-999')
            and cspfr.location             = t.location
            and cspfr.zone_node_type       = t.zone_node_type;

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_NON_PR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      1, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      -- Use rpm_item_loc_gtt to store what timeline already exists in Future Retail
      delete rpm_item_loc_gtt;

      insert into rpm_item_loc_gtt (price_event_id,
                                    item,
                                    diff_id,
                                    location,
                                    item_parent,         -- Note that this is used as customer_type
                                    regular_zone_group)  -- Note that this is used as zone_node_type
         select t.price_event_id,
                t.item,
                t.diff_id,
                t.location,
                t.customer_type,
                t.zone_node_type
           from (select /*+ CARDINALITY (ids, 10) */
                        fr.price_event_id,
                        fr.item,
                        fr.diff_id,
                        fr.location,
                        fr.customer_type,
                        fr.zone_node_type,
                        ROW_NUMBER() OVER (PARTITION BY fr.price_event_id,
                                                        fr.customer_type,
                                                        fr.item,
                                                        fr.diff_id,
                                                        fr.location,
                                                        fr.zone_node_type
                                               ORDER BY fr.action_date desc) rank
                   from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                        rpm_cust_segment_promo_fr_gtt fr
                  where fr.price_event_id = VALUE(ids)) t
          where t.rank = 1;

      if GENERATE_CSPFR_TIMELINE(O_cc_error_tbl,
                                 I_bulk_cc_pe_id,
                                 I_price_event_type,
                                 L_price_event_ids,
                                 RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_NPR) = 0 then
         return 0;
      end if;

      if GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl,
                               I_bulk_cc_pe_id,
                               I_price_event_type,
                               L_price_event_ids,
                               RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_INT_NON_PR) = 0 then
         return 0;
      end if;

   end if;

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE) then

      -- Get all promo item loc expl records for regular simple,
      -- threshold and complex promos and updates

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               diff_id,
                                               item_parent,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               zone_id,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               timebased_dtl_ind,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               max_hier_level,
                                               cur_hier_level,
                                               step_identifier)
         select /*+ ORDERED INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(ilex) */
                t.price_event_id,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.diff_id,
                ilex.item_parent,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.zone_node_type,
                ilex.zone_id,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.timebased_dtl_ind,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                ilex.max_hier_level,
                ilex.cur_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                RANK() OVER (PARTITION BY rpi.item,
                                                          rpi.diff_id,
                                                          rpl.zone_node_type,
                                                          rpl.location
                                                 ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                          rpi.pe_merch_level desc,
                                                          rpi.price_event_id) rank_value
                           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                            and rpi.price_event_id                 = VALUE(ids)
                            and NVL(rpi.txn_man_excluded_item, 0) != 1
                            and rpl.price_event_id                 = VALUE(ids)
                            and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id                 = rpi.price_event_id
                            and rpl.itemloc_id                     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_promo_item_loc_expl ilex
          where ilex.dept                 = t.dept
            and ilex.item                 = t.item
            and NVL(ilex.diff_id, '-999') = NVL(t.diff_id, '-999')
            and ilex.location             = t.location
            and ilex.zone_node_type       = t.zone_node_type
            and ilex.customer_type        is NULL;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - Insert into rpm_promo_item_loc_expl_gtt for non customer segment promo events - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE,
                                                      0, -- RFR
                                                      1, -- RPILE
                                                      0, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if GEN_PILE_TL(O_cc_error_tbl,
                     I_bulk_cc_pe_id,
                     I_price_event_type,
                     L_pe_no_frs,
                     RPM_CONSTANTS.CAPT_GTT_GEN_PILE_PR) = 0 then
         return 0;
      end if;

   end if;

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      -- Get all promo_item_loc_expl data for the scenario where dealing with
      -- a specific customer type...

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               timebased_dtl_ind,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               zone_id,
                                               item_parent,
                                               diff_id,
                                               max_hier_level,
                                               cur_hier_level,
                                               step_identifier)
         select /*+ ORDERED INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(ilex) */
                t.price_event_id,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.zone_node_type,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.timebased_dtl_ind,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                ilex.zone_id,
                ilex.item_parent,
                ilex.diff_id,
                ilex.max_hier_level,
                ilex.cur_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_PILE_CS_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                RANK() OVER (PARTITION BY rpi.item,
                                                          rpi.diff_id,
                                                          rpl.zone_node_type,
                                                          rpl.location
                                                 ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                          rpi.pe_merch_level desc,
                                                          rpi.price_event_id) rank_value
                           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                            and rpi.price_event_id                 = VALUE(ids)
                            and NVL(rpi.txn_man_excluded_item, 0) != 1
                            and rpl.price_event_id                 = VALUE(ids)
                            and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id                 = rpi.price_event_id
                            and rpl.itemloc_id                     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_promo_item_loc_expl ilex,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where ilex.dept                 = t.dept
            and ilex.item                 = t.item
            and NVL(ilex.diff_id, '-999') = NVL(t.diff_id, '-999')
            and ilex.location             = t.location
            and ilex.zone_node_type       = t.zone_node_type
            and ilex.customer_type        is NOT NULL
            and t.price_event_id          = rpd.promo_dtl_id
            and rpd.promo_comp_id         = rpc.promo_comp_id
            and rpc.customer_type         = ilex.customer_type;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - Insert into rpm_promo_item_loc_expl_gtt for Cust Seg Promo - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_PILE_CS_PR,
                                                      0, -- RFR
                                                      1, -- RPILE
                                                      0, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      -- Get all cust seg promo fr data where it's for a specific
      -- customer type...

      insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                                 cust_segment_promo_id,
                                                 item,
                                                 zone_node_type,
                                                 location,
                                                 action_date,
                                                 customer_type,
                                                 dept,
                                                 promo_retail,
                                                 promo_retail_currency,
                                                 promo_uom,
                                                 complex_promo_ind,
                                                 cspfr_rowid,
                                                 zone_id,
                                                 item_parent,
                                                 diff_id,
                                                 max_hier_level,
                                                 cur_hier_level,
                                                 step_identifier)
         select /*+ ORDERED */
                t.price_event_id,
                cspfr.cust_segment_promo_id,
                cspfr.item,
                cspfr.zone_node_type,
                cspfr.location,
                cspfr.action_date,
                cspfr.customer_type,
                cspfr.dept,
                cspfr.promo_retail,
                cspfr.promo_retail_currency,
                cspfr.promo_uom,
                cspfr.complex_promo_ind,
                cspfr.rowid,
                cspfr.zone_id,
                cspfr.item_parent,
                cspfr.diff_id,
                cspfr.max_hier_level,
                cspfr.cur_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_CS_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                RANK() OVER (PARTITION BY rpi.item,
                                                          rpi.diff_id,
                                                          rpl.zone_node_type,
                                                          rpl.location
                                                 ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                          rpi.pe_merch_level desc,
                                                          rpi.price_event_id) rank_value
                           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                            and rpi.price_event_id                 = VALUE(ids)
                            and NVL(rpi.txn_man_excluded_item, 0) != 1
                            and rpl.price_event_id                 = VALUE(ids)
                            and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id                 = rpi.price_event_id
                            and rpl.itemloc_id                     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_cust_segment_promo_fr cspfr,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where cspfr.dept                 = t.dept
            and cspfr.item                 = t.item
            and NVL(cspfr.diff_id, '-999') = NVL(t.diff_id, '-999')
            and cspfr.location             = t.location
            and cspfr.zone_node_type       = t.zone_node_type
            and cspfr.customer_type        is NOT NULL
            and t.price_event_id           = rpd.promo_dtl_id
            and rpd.promo_comp_id          = rpc.promo_comp_id
            and rpc.customer_type          = cspfr.customer_type;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - Insert into rpm_cust_segment_promo_fr_gtt - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_CS_PR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      1, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if GEN_PILE_TL(O_cc_error_tbl,
                     I_bulk_cc_pe_id,
                     I_price_event_type,
                     L_pe_no_frs,
                     RPM_CONSTANTS.CAPT_GTT_GEN_PILE_CSPFR) != 1 then
         return 0;
      end if;

      -- Use rpm_item_loc_gtt to store what timeline already exists in Future Retail
      delete rpm_item_loc_gtt;

      insert into rpm_item_loc_gtt (price_event_id,
                                    item,
                                    diff_id,
                                    location,
                                    item_parent,         -- Note that this is used as customer_type
                                    regular_zone_group)  -- Note that this is used as zone_node_type
         select t.price_event_id,
                t.item,
                t.diff_id,
                t.location,
                t.customer_type,
                t.zone_node_type
           from (select /*+ CARDINALITY (ids, 10) */
                        fr.price_event_id,
                        fr.item,
                        fr.diff_id,
                        fr.location,
                        fr.customer_type,
                        fr.zone_node_type,
                        ROW_NUMBER() OVER (PARTITION BY fr.price_event_id,
                                                        fr.customer_type,
                                                        fr.item,
                                                        fr.diff_id,
                                                        fr.location,
                                                        fr.zone_node_type
                                               ORDER BY fr.action_date desc) rank
                   from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                        rpm_cust_segment_promo_fr_gtt fr
                  where fr.price_event_id = VALUE(ids)) t
          where t.rank = 1;

      if GENERATE_CSPFR_TIMELINE(O_cc_error_tbl,
                                 I_bulk_cc_pe_id,
                                 I_price_event_type,
                                 L_price_event_ids,
                                 RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_PR) = 0 then
         return 0;
      end if;

      if GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl,
                               I_bulk_cc_pe_id,
                               I_price_event_type,
                               L_price_event_ids,
                               RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_INT_CS_PR) = 0 then
         return 0;
      end if;

   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      delete
        from rpm_clearance_gtt;

      insert into rpm_clearance_gtt (price_event_id,
                                     clearance_id,
                                     clearance_display_id,
                                     state,
                                     reason_code,
                                     exception_parent_id,
                                     reset_ind,
                                     item,
                                     diff_id,
                                     zone_id,
                                     location,
                                     zone_node_type,
                                     effective_date,
                                     out_of_stock_date,
                                     reset_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_percent,
                                     change_selling_uom,
                                     price_guide_id,
                                     vendor_funded_ind,
                                     funding_type,
                                     funding_amount,
                                     funding_amount_currency,
                                     funding_percent,
                                     supplier,
                                     deal_id,
                                     deal_detail_id,
                                     partner_type,
                                     partner_id,
                                     create_date,
                                     create_id,
                                     approval_date,
                                     approval_id,
                                     lock_version,
                                     rc_rowid,
                                     skulist,
                                     step_identifier)
         select /*+ CARDINALITY(ids 10) */
                VALUE(ids),
                clearance_id,
                clearance_display_id,
                state,
                reason_code,
                exception_parent_id,
                0, -- reset_ind
                item,
                diff_id,
                zone_id,
                location,
                zone_node_type,
                effective_date,
                out_of_stock_date,
                reset_date,
                change_type,
                change_amount,
                change_currency,
                change_percent,
                change_selling_uom,
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                funding_amount_currency,
                funding_percent,
                supplier,
                deal_id,
                deal_detail_id,
                partner_type,
                partner_id,
                create_date,
                create_id,
                approval_date,
                approval_id,
                lock_version,
                rc.rowid,
                rc.skulist,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR
           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_clearance rc
          where rc.clearance_id = VALUE(ids);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

     delete
       from rpm_clearance_gtt;

     insert into rpm_clearance_gtt (price_event_id,
                                    clearance_id,
                                    clearance_display_id,
                                    state,
                                    reason_code,
                                    reset_ind,
                                    item,
                                    location,
                                    zone_node_type,
                                    effective_date,
                                    out_of_stock_date,
                                    change_type,
                                    vendor_funded_ind,
                                    create_date,
                                    create_id,
                                    approval_date,
                                    approval_id,
                                    lock_version,
                                    rc_rowid,
                                    step_identifier)
        select t.price_event_id,
               rc.clearance_id,
               rc.clearance_display_id,
               rc.state,
               rc.reason_code,
               1, -- reset_ind
               t.item,
               t.location,
               rc.zone_node_type,
               rc.effective_date,
               rc.out_of_stock_date,
               -9, -- change_type
               0,  -- vendor_funded_ind
               rc.create_date,
               rc.create_id,
               rc.approval_date,
               rc.approval_id,
               rc.lock_version,
               rc.rowid,
               RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR_PC
           from (select s.price_event_id,
                        s.item,
                        s.location
                   from (select /*+ LEADING(ids) USE_NL(ids) INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                rpi.price_event_id,
                                rpi.item,
                                rpl.location,
                                ROW_NUMBER() OVER (PARTITION BY rpi.price_event_id,
                                                                rpi.item,
                                                                rpl.location
                                                       ORDER BY rpi.item) rank_value
                           from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                            and rpi.price_event_id   = VALUE(ids)
                            and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                            and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                            and rpl.price_event_id   = rpi.price_event_id
                            and rpl.itemloc_id       = rpi.itemloc_id
                            and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)) s
                  where s.rank_value = 1) t,
               rpm_clearance_reset rc
         where rc.item     = t.item
           and rc.location = t.location;

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR_PC,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '||I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: '|| I_parent_thread_number ||
                               ' - I_thread_number: '||I_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_GTT;

--------------------------------------------------------------------------------
FUNCTION PROCESS_PRICE_CHANGES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id    IN     NUMBER,
                               I_persist_ind     IN     VARCHAR2,
                               I_start_state     IN     VARCHAR2,
                               I_end_state       IN     VARCHAR2,
                               I_user_name       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_PRICE_CHANGES';

   L_reset_pos_ids      OBJ_NUMERIC_ID_TABLE     := NULL;
   L_price_event_ids    OBJ_NUMERIC_ID_TABLE     := NULL;
   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;
   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_emergency_pc       OBJ_NUMERIC_ID_TABLE     := NULL;
   L_pc_il_tbl          OBJ_PRICE_CHANGE_IL_TBL  := NULL;
   L_start_time         TIMESTAMP                := SYSTIMESTAMP;
   L_need_secondary_ids OBJ_NUMERIC_ID_TABLE     := NULL;
   L_secondary_count    NUMBER                   := NULL;

   L_error_message VARCHAR2(256) := NULL;
   L_loc_move_id   NUMBER(10)    := NULL;
   L_new_item_loc  NUMBER(1)     := 0;

   cursor C_RESET_POS is
      select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids)
         and (   rpc.change_type = RPM_CONSTANTS.RESET_POS
              or (    rpc.change_type     = RPM_CONSTANTS.RETAIL_NONE
                  and rpc.null_multi_ind != 1));

   cursor C_PC_IDS_NEED_CC is
      select price_change_id
        from (select VALUE(ids) price_change_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select VALUE(pos) price_change_id
                from table(cast(L_reset_pos_ids as OBJ_NUMERIC_ID_TABLE)) pos);

   cursor C_NO_CONFLICT_PC_IDS is
      select price_change_id
        from (select VALUE(ids) price_change_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select distinct price_event_id price_change_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids)
         and rpc.effective_date  = LP_vdate;

   cursor C_NEED_SECONDARY is
      -- Items
      select /*+ ORDERED CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc,
             rpm_codes rcd,
             item_master im,
             rpm_area_diff_prim_expl adp
       where rpc.price_change_id      = VALUE(ids)
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.link_code            is NULL
         and rcd.code_id              = rpc.reason_code
         and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                              RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE)
         and rpc.zone_id              = adp.zone_hier_id
         and rpc.item                 = im.item
         and im.dept                  = adp.dept
         and im.class                 = adp.class
         and im.subclass              = adp.subclass
      union all
      -- Item Lists
      select price_change_id
        from (select /*+ ORDERED CARDINALITY (ids 10) */
                     rpc.price_change_id,
                     RANK() OVER (PARTITION BY rpc.price_change_id
                                      ORDER BY im.item) rnk
                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rpcs,
                     rpm_codes rcd,
                     item_master im,
                     rpm_area_diff_prim_expl adp
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rpc.link_code            is NULL
                 and rcd.code_id              = rpc.reason_code
                 and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                                      RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE)
                 and rpc.zone_id              = adp.zone_hier_id
                 and rpc.skulist              = rpcs.skulist
                 and rpcs.item                = im.item
                 and im.dept                  = adp.dept
                 and im.class                 = adp.class
                 and im.subclass              = adp.subclass)
       where rnk = 1
      union all
      -- PEIL
      select price_change_id
        from (select /*+ ORDERED CARDINALITY (ids 10) */
                     rpc.price_change_id,
                     RANK() OVER (PARTITION BY rpc.price_change_id
                                      ORDER BY im.item) rnk
                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     rpm_codes rcd,
                     item_master im,
                     rpm_area_diff_prim_expl adp
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.skulist              is NULL
                 and rpc.link_code            is NULL
                 and rcd.code_id              = rpc.reason_code
                 and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                                      RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE)
                 and rpc.zone_id              = adp.zone_hier_id
                 and rpc.price_event_itemlist = rmld.merch_list_id
                 and rmld.item                = im.item
                 and im.dept                  = adp.dept
                 and im.class                 = adp.class
                 and im.subclass              = adp.subclass)
       where rnk = 1
      union all
      select /*+ ORDERED CARDINALITY (ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc,
             rpm_codes rcd
       where rpc.price_change_id      = VALUE(ids)
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.link_code            is NOT NULL
         and rcd.code_id              = rpc.reason_code
         and rcd.code_type            NOT IN (RPM_CONSTANTS.REASON_CODE_SYSTEM_CODE,
                                              RPM_CONSTANTS.REASON_CODE_WORKSHEET_CODE);

   cursor C_PC_IL_TBL(I_price_event_id IN NUMBER) is
      select /*+ USE_HASH(rpl) ORDERED */
             OBJ_PRICE_CHANGE_IL_REC(I_price_event_id,
                                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                     item,
                                     location)
        from (with price_changes as
                (select rpc.item,
                        rpc.diff_id,
                        rpc.link_code,
                        rpc.skulist,
                        rpc.price_event_itemlist,
                        rpc.location,
                        rpc.zone_id,
                        rpc.zone_node_type,
                        rpc.sys_generated_exclusion
                  from  rpm_bulk_cc_pe_location rpl,
                        rpm_bulk_cc_pe_item rpi,
                        rpm_price_change rpc
                  where rpi.bulk_cc_pe_id      = LP_bulk_cc_pe_id
                    and rpi.price_event_id     = I_price_event_id
                    and rpl.bulk_cc_pe_id (+)  = rpi.bulk_cc_pe_id
                    and rpl.price_event_id (+) = rpi.price_event_id
                    and rpl.itemloc_id (+)     = rpi.itemloc_id
                    and rpl.price_event_id     = rpc.exception_parent_id
                    and rpi.item               = rpc.item
                    and rpc.state              IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                                   RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                                   RPM_CONSTANTS.PC_PENDING_STATE_CODE)
                    and rownum                 > 0)
              select rpi.item,
                     rpl.location
                from rpm_bulk_cc_pe_item rpi,
                     rpm_bulk_cc_pe_location rpl
              where rpi.bulk_cc_pe_id      = LP_bulk_cc_pe_id
                and rpi.price_event_id     = I_price_event_id
                and rpl.bulk_cc_pe_id (+)  = rpi.bulk_cc_pe_id
                and rpl.price_event_id (+) = rpi.price_event_id
                and rpl.itemloc_id (+)     = rpi.itemloc_id
              -- Remove all the exceptions/exclusions
              minus
              (-- Item/Loc level
               select /*+ ORDERED */
                      rpc.item,
                      rpc.location
                 from price_changes rpc,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.item           = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Item/Zone level
               union all
               select /*+ ORDERED */
                      rpc.item,
                      rzl.location
                 from price_changes rpc,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id        = rzl.zone_id
                  and rpc.item           = im.item
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.item           = im.item_parent
                  and rpc.diff_id        is NULL
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- Parent-Diff/Loc level
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.diff_id        is NOT NULL
                  and rpc.item           = im.item_parent
                  and rpc.diff_id        IN (im.diff_1,
                                             im.diff_2,
                                             im.diff_3,
                                             im.diff_4)
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- skulist/loc - parent items
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_price_change_skulist rpcs,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 0
                  and rpc.skulist                         = rpcs.skulist
                  and rpcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                  and rpcs.item                           = im.item_parent
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- skulist/loc - tran level items
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_price_change_skulist rpcs,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 0
                  and rpc.skulist                         = rpcs.skulist
                  and rpcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                  and rpcs.item                           = im.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- Price Event Item List (PEIL) Item Parent /loc
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.price_event_itemlist is NOT NULL
                  and rpc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                = im.item_parent
                  and im.item_level            = im.tran_level
                  and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                   > 0
               -- Price Event Item List (PEIL) Item /loc
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.price_event_itemlist is NOT NULL
                  and rpc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                = im.item
                  and im.item_level            = im.tran_level
                  and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                   > 0
               -- Processing System Generated Exclusions Price Changes
               -- merchlist/zone - parent items
               union all
               select /*+ ORDERED */
                      im.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id                         = rzl.zone_id
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/zone - parent-diff items
               union all
               select /*+ ORDERED */
                      im.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id                         = rzl.zone_id
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.diff_1                           = rmld.diff_id
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/zone - tran level items
               union all
               select /*+ ORDERED */
                      im.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rpc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id                         = rzl.zone_id
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                           = im.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/loc - parent items
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/loc - parent-diff items
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                           = im.item_parent
                  and im.diff_1                           = rmld.diff_id
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- merchlist/loc - tran level items
               union all
               select /*+ ORDERED */
                      im.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rpc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.skulist                         is NOT NULL
                  and NVL(rpc.sys_generated_exclusion, 0) = 1
                  and rpc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                           = im.item
                  and im.item_level                       = im.tran_level
                  and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                              > 0
               -- link code/zone
               union all
               select /*+ ORDERED */
                      rlca.item,
                      rzl.location
                 from price_changes rpc,
                      rpm_link_code_attribute rlca,
                      rpm_zone_location rzl,
                      item_master im
                where rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rpc.zone_id        = rzl.zone_id
                  and rpc.link_code      is NOT NULL
                  and rpc.link_code      = rlca.link_code
                  and rlca.location      = rzl.location
                  and rlca.loc_type      = rzl.loc_type
                  and rownum             > 0
               -- link code/loc
               union all
               select /*+ ORDERED */
                      rlca.item,
                      rpc.location
                 from price_changes rpc,
                      rpm_link_code_attribute rlca,
                      item_master im
                where rpc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpc.link_code      is NOT NULL
                  and rpc.link_code      = rlca.link_code
                  and rlca.location      = rpc.location
                  and rownum             > 0));

   cursor C_GET_LOC_MOVE_ID is
      select location_move_id
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = LP_bulk_cc_pe_id
         and rownum        = 1;

BEGIN

   if NVL(I_user_name, '') = 'NewItemLocationBatch' then
      L_new_item_loc := 1;
   else
      L_new_item_loc := 0;
   end if;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                                      ' - LP_thread_number: '|| LP_thread_number);

   open C_RESET_POS;
   fetch C_RESET_POS BULK COLLECT into L_reset_pos_ids;
   close C_RESET_POS;

   open C_PC_IDS_NEED_CC;
   fetch C_PC_IDS_NEED_CC BULK COLLECT into L_price_event_ids;
   close C_PC_IDS_NEED_CC;

   open C_GET_LOC_MOVE_ID;
   fetch C_GET_LOC_MOVE_ID into L_loc_move_id;
   close C_GET_LOC_MOVE_ID;

   if L_price_event_ids is NOT NULL and
      L_price_event_ids.COUNT > 0 then

      if I_end_state IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                         RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) or
         I_persist_ind != 'Y' then

         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                     LP_cc_error_tbl,
                                                     L_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     0,               -- I_specific_item_loc
                                                     L_new_item_loc,  -- I_new_item_loc
                                                     NULL,            -- I_new_promo_end_date
                                                     LP_bulk_cc_pe_id,
                                                     LP_parent_thread_number,
                                                     LP_thread_number,
                                                     0,               -- I_chunk_number
                                                     I_end_state,
                                                     LP_reprocess_ind) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

         -- Get Price Changes that have no conflict
         open C_NO_CONFLICT_PC_IDS;
         fetch C_NO_CONFLICT_PC_IDS BULK COLLECT into L_nc_price_event_ids;
         close C_NO_CONFLICT_PC_IDS;

         -- Do the Area Differential
         if I_persist_ind = 'Y' and
            I_user_name != 'NewItemLocationBatch' and
            L_loc_move_id is NULL and
            LP_secondary_ind = 0 then

            open C_NEED_SECONDARY;
            fetch C_NEED_SECONDARY BULK COLLECT into L_need_secondary_ids;
            close C_NEED_SECONDARY;

            if L_need_secondary_ids is NOT NULL and
               L_need_secondary_ids.COUNT > 0 then

               if RPM_AREA_DIFF_PRICE_CHANGE.APPLY_AREA_DIFFS_TO_PC(O_cc_error_tbl,
                                                                    L_need_secondary_ids,
                                                                    I_user_name) = FALSE then
                  return 0;

               end if;

               select COUNT(1)
                 into L_secondary_count
                 from numeric_id_gtt;

               if L_secondary_count > 0 then

                  insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                                     price_event_id,
                                                     price_event_type)
                     select LP_secondary_bulk_cc_pe_id,
                            numeric_id,
                            RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                       from numeric_id_gtt;

                  LP_need_secondary := 1;

               end if;

            end if;

         end if;

      elsif I_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE then

         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE(O_cc_error_tbl,
                                                     L_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     LP_bulk_cc_pe_id,
                                                     NULL, --I_pe_sequence_id
                                                     NULL, --I_pe_thread_number
                                                     0,    --I_chunk_number
                                                     I_end_state) = 0 then
            return 0;

         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

         -- Get Price Changes that have no conflicts
         open C_NO_CONFLICT_PC_IDS;
         fetch C_NO_CONFLICT_PC_IDS BULK COLLECT into L_nc_price_event_ids;
         close C_NO_CONFLICT_PC_IDS;

         -- Do the Area Differential
         if I_persist_ind = 'Y' then

            if LP_secondary_ind = 0 then

               if RPM_AREA_DIFF_PRICE_CHANGE.UNAPPROVE_AREA_DIFF_PC(O_cc_error_tbl,
                                                                    LP_need_secondary,
                                                                    L_nc_price_event_ids,
                                                                    LP_secondary_bulk_cc_pe_id) = FALSE then
                  return 0;
               end if;

            else

               -- When unapproving a secondary area diff pc, delete the secondary price change
               if RPM_AREA_DIFF_PRICE_CHANGE.DELETE_AREA_DIFF_PC(O_cc_error_tbl,
                                                                 L_nc_price_event_ids) = FALSE then
                  return 0;
               end if;

            end if;

         end if;

      end if;

   end if;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      -- Update the state of the price changes that has no conflict
      update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */
             rpm_price_change target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    NULL),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_id, I_user_name),
                                    NULL)
       where price_change_id IN (select VALUE(ids)
                                   from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and state           != I_end_state;

      -- Update the state of any SGEs that might exist
      update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */
             rpm_price_change target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    NULL),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_id, I_user_name),
                                    NULL)
       where price_change_id IN (select /*+ CARDINALITY(ids 10) */
                                        rpc.price_change_id
                                   from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                        rpm_price_change rpc
                                  where rpc.exception_parent_id     = VALUE(ids)
                                    and rpc.sys_generated_exclusion = 1)
         and state           != I_end_state;

   end if;

   if I_end_state = RPM_CONSTANTS.PC_APPROVED_STATE_CODE then

      open C_EMERGENCY;
      fetch C_EMERGENCY BULK COLLECT into L_emergency_pc;
      close C_EMERGENCY;

      if L_emergency_pc is NOT NULL and
         L_emergency_pc.COUNT > 0 then

         -- No Bulk interface yet so need to loop;
         for i IN 1..L_emergency_pc.COUNT loop

            open C_PC_IL_TBL(L_emergency_pc(i));
            fetch C_PC_IL_TBL BULK COLLECT into L_pc_il_tbl;
            close C_PC_IL_TBL;

            if L_pc_il_tbl is NOT NULL and
               L_pc_il_tbl.COUNT > 0 then

               if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                                 L_pc_il_tbl,
                                                 TRUE) = 0 then
                  O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                    CONFLICT_CHECK_ERROR_REC(NULL,
                                                             NULL,
                                                             RPM_CONSTANTS.PLSQL_ERROR,
                                                             L_error_message));
                  return 0;
               end if;

            end if;

         end loop;

         delete
           from rpm_price_change
          where rowid IN (select /*+ CARDINALITY (ids 10) */
                                 rpc.rowid
                            from table(cast(L_emergency_pc as OBJ_NUMERIC_ID_TABLE)) ids,
                                 rpm_price_change rpc
                           where rpc.exception_parent_id = VALUE(ids)
                             and rpc.state               IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                                             RPM_CONSTANTS.PC_REJECTED_STATE_CODE,
                                                             RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE));

          -- Update state to EXECUTED
          update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */
                 rpm_price_change target
             set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
           where price_change_id IN (select VALUE(ids)
                                       from table(cast(L_emergency_pc as OBJ_NUMERIC_ID_TABLE)) ids)
             and state           != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

          -- Update the state of any SGEs that might exist
          update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */
                 rpm_price_change target
             set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
           where price_change_id IN (select /*+ CARDINALITY(ids 10) */
                                            rpc.price_change_id
                                       from table(cast(L_emergency_pc as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_price_change rpc
                                      where rpc.exception_parent_id     = VALUE(ids)
                                        and rpc.sys_generated_exclusion = 1)
             and state           != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

       end if;

    elsif I_end_state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE then

       -- Update RMS
       for i IN 1..L_nc_price_event_ids.COUNT loop

          open C_PC_IL_TBL(L_nc_price_event_ids(i));
          fetch C_PC_IL_TBL BULK COLLECT into L_pc_il_tbl;
          close C_PC_IL_TBL;

          if L_pc_il_tbl is NOT NULL and
             L_pc_il_tbl.COUNT > 0 then

             if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                               L_pc_il_tbl,
                                               TRUE) = 0 then
                O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                  CONFLICT_CHECK_ERROR_REC(NULL,
                                                           NULL,
                                                           RPM_CONSTANTS.PLSQL_ERROR,
                                                           L_error_message));
                return 0;
             end if;

          end if;

       end loop;

   end if;

   -- Update the state of RESET_POS
   if L_reset_pos_ids is NOT NULL and
      L_reset_pos_ids.COUNT > 0 then

      update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */
             rpm_price_change target
         set state         = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
             approval_date = SYSDATE,
             approval_id   = I_user_name
       where price_change_id IN (select VALUE(ids)
                                   from table(cast(L_reset_pos_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and state           != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

      if RPM_FUTURE_RETAIL_SQL.EXPL_TIMELINES_TO_IL(O_cc_error_tbl,
                                                    I_price_event_ids,
                                                    RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                    RPM_CONSTANTS.CAPT_GTT_EXPL_TL_TO_IL) = 0 then
         return 0;
      end if;

      if RPM_CC_PUBLISH.STAGE_RESET_POS_MESSAGES(L_error_message,
                                                 I_rib_trans_id,
                                                 L_reset_pos_ids) = 0 then
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;
      end if;

   end if;

   -- Set Back the state to the starting state in case there is a conflict
   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_conflict_error_tbl,
                      LP_price_event_type,
                      I_start_state) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_conflict_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PRICE_CHANGES;

--------------------------------------------------------------------------------

FUNCTION PROCESS_CLEARANCES(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                            I_rib_trans_id    IN     NUMBER,
                            I_persist_ind     IN     VARCHAR2,
                            I_start_state     IN     VARCHAR2,
                            I_end_state       IN     VARCHAR2,
                            I_user_name       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_CLEARANCES';

   -- Clearance Id with no conflict
   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;
   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;

   -- Emergency Clearance Id
   L_emergency_cl OBJ_NUMERIC_ID_TABLE := NULL;

   L_pc_il_tbl         OBJ_PRICE_CHANGE_IL_TBL := NULL;
   L_error_message     VARCHAR2(256)           := NULL;
   L_specific_item_loc NUMBER(1)               := 0;
   L_new_item_loc      NUMBER(1)               := 0;
   L_start_time        TIMESTAMP               := SYSTIMESTAMP;

   cursor C_NO_CONFLICT_CL_IDS is
      select clearance_id
        from (select VALUE(ids) clearance_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select distinct price_event_id clearance_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) INDEX(rc PK_RPM_CLEARANCE)*/
             rc.clearance_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc
       where rc.clearance_id   = VALUE(ids)
         and rc.effective_date = LP_vdate;

   cursor C_PC_IL_TBL(I_price_event_id IN NUMBER) is
      select /*+ USE_HASH(rpi, rpl) ORDERED */
             OBJ_PRICE_CHANGE_IL_REC(I_price_event_id,
                                     RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                     item,
                                     location)
        from (with clearances as
                (select rc.item,
                        rc.diff_id,
                        rc.skulist,
                        rc.price_event_itemlist,
                        rc.location,
                        rc.zone_id,
                        rc.zone_node_type,
                        rc.sys_generated_exclusion
                  from  rpm_bulk_cc_pe_location rpl,
                        rpm_bulk_cc_pe_item rpi,
                        rpm_clearance rc
                  where rpi.bulk_cc_pe_id      = LP_bulk_cc_pe_id
                    and rpi.price_event_id     = I_price_event_id
                    and rpl.bulk_cc_pe_id (+)  = rpi.bulk_cc_pe_id
                    and rpl.price_event_id (+) = rpi.price_event_id
                    and rpl.itemloc_id (+)     = rpi.itemloc_id
                    and rpl.price_event_id     = rc.exception_parent_id
                    and rpi.item               = rc.item
                    and rc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                                   RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                                   RPM_CONSTANTS.PC_PENDING_STATE_CODE)
                    and rownum                 > 0)
              select rpi.item,
                     rpl.location
                from rpm_bulk_cc_pe_item rpi,
                     rpm_bulk_cc_pe_location rpl
              where rpi.bulk_cc_pe_id      = LP_bulk_cc_pe_id
                and rpi.price_event_id     = I_price_event_id
                and rpl.bulk_cc_pe_id (+)  = rpi.bulk_cc_pe_id
                and rpl.price_event_id (+) = rpi.price_event_id
                and rpl.itemloc_id (+)     = rpi.itemloc_id
              -- Remove all the exceptions/exclusions
              minus
              (-- Item/Loc level
               select /*+ ORDERED */
                      rc.item,
                      rc.location
                 from clearances rc,
                      item_master im
                where rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.item           = im.item
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               -- Item/Zone level
               union all
               select /*+ ORDERED */
                      rc.item,
                      rzl.location
                 from clearances rc,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id        = rzl.zone_id
                  and rc.item           = im.item
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im
                where rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.item           = im.item_parent
                  and rc.diff_id        is NULL
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum            > 0
               -- Parent-Diff/Loc level
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      item_master im
                where rc.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.diff_id        is NOT NULL
                  and rc.item           = im.item_parent
                  and rc.diff_id        IN (im.diff_1,
                                            im.diff_2,
                                            im.diff_3,
                                            im.diff_4)
                  and im.item_level      = im.tran_level
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum             > 0
               -- skulist/loc - parent items
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_clearance_skulist rcs,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 0
                  and rc.skulist                         = rcs.skulist
                  and rcs.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                  and rcs.item                           = im.item_parent
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- skulist/loc - tran level items
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_clearance_skulist rcs,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 0
                  and rc.skulist                         = rcs.skulist
                  and rcs.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
                  and rcs.item                           = im.item
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- Price Event Item List (PEIL) Item Parent /loc
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.price_event_itemlist is NOT NULL
                  and rc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item               = im.item_parent
                  and im.item_level           = im.tran_level
                  and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                  > 0
               -- Price Event Item List (PEIL) Item /loc
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.price_event_itemlist is NOT NULL
                  and rc.price_event_itemlist = rmld.merch_list_id
                  and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item               = im.item
                  and im.item_level           = im.tran_level
                  and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                  > 0
               -- Processing System Generated Exclusions Price Changes
               -- merchlist/zone - parent items
               union all
               select /*+ ORDERED */
                      im.item,
                      rzl.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id                         = rzl.zone_id
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/zone - parent-diff items
               union all
               select /*+ ORDERED */
                      im.item,
                      rzl.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id                         = rzl.zone_id
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.diff_1                          = rmld.diff_id
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/zone - tran level items
               union all
               select /*+ ORDERED */
                      im.item,
                      rzl.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im,
                      rpm_zone_location rzl
                where rc.zone_node_type                  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and rc.zone_id                         = rzl.zone_id
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                          = im.item
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/loc - parent items
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/loc - parent-diff items
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                  and rmld.item                          = im.item_parent
                  and im.diff_1                          = rmld.diff_id
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0
               -- merchlist/loc - tran level items
               union all
               select /*+ ORDERED */
                      im.item,
                      rc.location
                 from clearances rc,
                      rpm_merch_list_detail rmld,
                      item_master im
                where rc.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rc.skulist                         is NOT NULL
                  and NVL(rc.sys_generated_exclusion, 0) = 1
                  and rc.skulist                         = rmld.merch_list_id
                  and rmld.merch_level                   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                  and rmld.item                          = im.item
                  and im.item_level                      = im.tran_level
                  and im.status                          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind                    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and rownum                             > 0));
BEGIN

   if NVL(LP_need_il_explode_ind, 0) = 1 or
      NVL(I_user_name, '') IN (RPM_CONSTANTS.RPM_LOC_MOVE_BATCH,
                               RPM_CONSTANTS.RPM_NIL_BATCH) then
      L_specific_item_loc := 0;
   else
      L_specific_item_loc := 1;
   end if;

   if NVL(I_user_name, '') = RPM_CONSTANTS.RPM_NIL_BATCH then
      L_new_item_loc := 1;
   else
      L_new_item_loc := 0;
   end if;

   if I_price_event_ids is NOT NULL and
      I_price_event_ids.COUNT > 0 then

      if I_end_state IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                         RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE,
                         RPM_CONSTANTS.PC_EXECUTED_STATE_CODE) or
         I_persist_ind != 'Y' then

         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                     LP_cc_error_tbl,
                                                     I_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     L_specific_item_loc,
                                                     L_new_item_loc,
                                                     NULL, -- new_promo_end_date
                                                     LP_bulk_cc_pe_id,
                                                     LP_parent_thread_number,
                                                     LP_thread_number,
                                                     0, --I_chunk_number
                                                     I_end_state,
                                                     LP_reprocess_ind) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

      elsif I_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE then

         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE(O_cc_error_tbl,
                                                     I_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     LP_bulk_cc_pe_id,
                                                     LP_parent_thread_number,
                                                     LP_thread_number,
                                                     0, --I_chunk_number
                                                     I_end_state) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

      end if;

   end if;

   -- Get Clearances that have no conflict
   open C_NO_CONFLICT_CL_IDS;
   fetch C_NO_CONFLICT_CL_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_CL_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      -- Update the state of the clearances that has no conflict
      update /*+ INDEX(target PK_RPM_CLEARANCE) */
             rpm_clearance target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    NULL),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_id, I_user_name),
                                    NULL)
       where clearance_id IN (select VALUE(ids)
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and state        != I_end_state;

      -- update the status of any SGEs that might exist
      update /*+ INDEX(target PK_RPM_CLEARANCE) */
             rpm_clearance target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    NULL),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(approval_id, I_user_name),
                                    NULL)
       where clearance_id IN (select /*+ CARDINALITY(ids 10) */
                                     rc.clearance_id
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_clearance rc
                               where rc.exception_parent_id     = VALUE(ids)
                                 and rc.sys_generated_exclusion = 1)
         and state        != I_end_state;

      if I_end_state = RPM_CONSTANTS.PC_APPROVED_STATE_CODE then

         open C_EMERGENCY;
         fetch C_EMERGENCY BULK COLLECT into L_emergency_cl;
         close C_EMERGENCY;

         if L_emergency_cl is NOT NULL and
            L_emergency_cl.COUNT > 0 then

            -- No Bulk interface yet so need to loop
            for i IN 1..L_emergency_cl.COUNT loop

               open C_PC_IL_TBL(L_emergency_cl(i));
               fetch C_PC_IL_TBL BULK COLLECT into L_pc_il_tbl;
               close C_PC_IL_TBL;

               if L_pc_il_tbl is NOT NULL and
                  L_pc_il_tbl.COUNT > 0 then

                  if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                                    L_pc_il_tbl,
                                                    FALSE) = 0 then
                     O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(CONFLICT_CHECK_ERROR_REC(NULL,
                                                                                         NULL,
                                                                                         RPM_CONSTANTS.PLSQL_ERROR,
                                                                                         L_error_message));
                     return 0;
                  end if;

               end if;

            end loop;

            delete
              from rpm_clearance rc
             where rc.rowid IN (select /*+ CARDINALITY (ids 10) */
                                       rc1.rowid
                                  from table(cast(L_emergency_cl as OBJ_NUMERIC_ID_TABLE)) ids,
                                       rpm_clearance rc1
                                 where rc1.exception_parent_id = VALUE(ids)
                                   and rc1.state               IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                                                   RPM_CONSTANTS.PC_REJECTED_STATE_CODE,
                                                                   RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE));

            -- Update state to EXECUTED
            update /*+ INDEX(target PK_RPM_CLEARANCE) */
                   rpm_clearance target
               set state        = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
             where clearance_id IN (select VALUE(ids)
                                      from table(cast(L_emergency_cl as OBJ_NUMERIC_ID_TABLE)) ids)
               and state        != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

            -- update the status of any SGEs that might exist
            update /*+ INDEX(target PK_RPM_CLEARANCE) */
                   rpm_clearance target
               set state        = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
             where clearance_id IN (select /*+ CARDINALITY(ids 10) */
                                           rc.clearance_id
                                      from table(cast(L_emergency_cl as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.exception_parent_id     = VALUE(ids)
                                       and rc.sys_generated_exclusion = 1)
               and state        != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

         end if;

      elsif I_end_state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE then

         -- Update RMS
         for i IN 1..L_nc_price_event_ids.COUNT loop

            open C_PC_IL_TBL(L_nc_price_event_ids(i));
            fetch C_PC_IL_TBL BULK COLLECT into L_pc_il_tbl;
            close C_PC_IL_TBL;

            if L_pc_il_tbl is NOT NULL and
               L_pc_il_tbl.COUNT > 0 then

               if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                                 L_pc_il_tbl,
                                                 FALSE) = 0 then

                  O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(CONFLICT_CHECK_ERROR_REC(NULL,
                                                                                      NULL,
                                                                                      RPM_CONSTANTS.PLSQL_ERROR,
                                                                                      L_error_message));

                  return 0;
               end if;

            end if;

         end loop;

      end if;

   end if;

   -- Set Back the state to the starting state in case there is a conflict
   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_conflict_error_tbl,
                      LP_price_event_type,
                      I_start_state) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_conflict_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CLEARANCES;
-----------------------------------------------------------------------------------------------------------------------
FUNCTION PROCESS_CLEARANCE_RESETS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                  I_rib_trans_id    IN     NUMBER,
                                  I_persist_ind     IN     VARCHAR2,
                                  I_start_state     IN     VARCHAR2,
                                  I_end_state       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_CLEARANCE_RESETS';

   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;  -- Clearance Id with no conflict
   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_emergency_cl       OBJ_NUMERIC_ID_TABLE     := NULL;

   L_pc_il_tbl         OBJ_PRICE_CHANGE_IL_TBL := NULL;
   L_error_message     VARCHAR2(256)           := NULL;
   L_specific_item_loc NUMBER(1)               := NULL;
   L_start_time        TIMESTAMP               := SYSTIMESTAMP;

   cursor C_NO_CONFLICT_CL_IDS is
      select clearance_id
        from (select VALUE(ids) clearance_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
               minus
              select distinct price_event_id clearance_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) INDEX(rc PK_RPM_CLEARANCE)*/ rc.clearance_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc
       where rc.clearance_id = VALUE(ids)
         and rc.reset_date   = LP_vdate;

   cursor C_CLR_RESET (I_price_event_id IN NUMBER) is
      select OBJ_PRICE_CHANGE_IL_REC(rc.clearance_id,
                                     RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                     rbi.item,
                                     rbl.location)
        from rpm_clearance_reset rc,
             rpm_bulk_cc_pe_item rbi,
             rpm_bulk_cc_pe_location rbl
       where rbi.bulk_cc_pe_id     = LP_bulk_cc_pe_id
         and rbl.bulk_cc_pe_id     = LP_bulk_cc_pe_id
         and rbi.price_event_id    = I_price_event_id
         and rbl.price_event_id    = I_price_event_id
         and rbi.bulk_cc_pe_id     = rbl.bulk_cc_pe_id
         and rc.item               = rbi.item
         and rc.location           = rbl.location
         and rc.state              = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
         and TRUNC(effective_date) = TRUNC(LP_vdate);

BEGIN

   -- This function will only be called when the user change the reset date of a clearance
   -- that is in either "Approved" or "Executed" status.

   if NVL(LP_need_il_explode_ind, 0) = 1 then
      L_specific_item_loc := 0;
   else
      L_specific_item_loc := 1;
   end if;

   -- Do the conflict checking
   if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                               LP_cc_error_tbl,
                                               I_price_event_ids,
                                               LP_price_event_type,
                                               I_rib_trans_id,
                                               I_persist_ind,
                                               L_specific_item_loc,
                                               0,    -- new_item_loc
                                               NULL, -- new_promo_end_date
                                               LP_bulk_cc_pe_id,
                                               LP_parent_thread_number,
                                               LP_thread_number,
                                               0, -- I_chunk_number
                                               I_end_state,
                                               LP_reprocess_ind) = 0 then
      return 0;
   end if;

   L_conflict_error_tbl := O_cc_error_tbl;

   -- Get Clearances that have no conflict
   open C_NO_CONFLICT_CL_IDS;
   fetch C_NO_CONFLICT_CL_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_CL_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      -- Update the state of the clearance that has no conflict back to its original state
      update /*+ INDEX(target PK_RPM_CLEARANCE) */
             rpm_clearance target
         set state = I_start_state
       where clearance_id IN (select VALUE(ids)
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

      open C_EMERGENCY;
      fetch C_EMERGENCY BULK COLLECT into L_emergency_cl;
      close C_EMERGENCY;

      if L_emergency_cl is NOT NULL and
         L_emergency_cl.COUNT > 0 then

         -- These actions are for emergency Clearance
         for i IN 1..L_emergency_cl.COUNT loop

            open C_CLR_RESET(L_emergency_cl(i));
            fetch C_CLR_RESET BULK COLLECT into L_pc_il_tbl;
            close C_CLR_RESET;

            if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                              L_pc_il_tbl,
                                              FALSE) = 0 then
               O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                 CONFLICT_CHECK_ERROR_REC(NULL,
                                                          NULL,
                                                          RPM_CONSTANTS.PLSQL_ERROR,
                                                          L_error_message));
               return 0;

            end if;

            update rpm_clearance_reset
               set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
             where clearance_id IN (select reset.price_change_id
                                      from table(cast(L_pc_il_tbl as OBJ_PRICE_CHANGE_IL_TBL)) reset)
               and state        != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

         end loop;

      end if;
   end if;

   -- Set Back the state to the starting state in case there is a conflict
   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_conflict_error_tbl,
                      LP_price_event_type,
                      I_start_state) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_conflict_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CLEARANCE_RESETS;

--------------------------------------------------------------------------------

FUNCTION PROCESS_NEW_CLEARANCE_RESETS(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                                      I_rib_trans_id       IN     NUMBER,
                                      I_persist_ind        IN     VARCHAR2,
                                      I_start_state        IN     VARCHAR2,
                                      I_end_state          IN     VARCHAR2,
                                      I_new_clr_reset_date IN     DATE DEFAULT NULL,
                                      I_user_name          IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_NEW_CLEARANCE_RESETS';

   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;  -- this variable holds clearance reset ids with no conflicts
   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_cc_error_tbl       CONFLICT_CHECK_ERROR_TBL := NULL;
   L_emergency_cl       OBJ_PRICE_CHANGE_IL_TBL  := NULL;
   L_error_message      VARCHAR2(256)            := NULL;
   L_start_time         TIMESTAMP                := SYSTIMESTAMP;

   cursor C_VALIDATE_DATE is
      select CONFLICT_CHECK_ERROR_REC(fr.price_event_id,
                                      fr.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'invalid_clearance_reset_date')
        from rpm_clearance_gtt clr,
             rpm_future_retail_gtt fr
       where clr.reset_ind         = 1
         and fr.price_event_id     = clr.price_event_id
         and fr.item               = clr.item
         and fr.location           = clr.location
         and TRUNC(fr.action_date) = I_new_clr_reset_date
         and fr.clearance_id       is NOT NULL
         and fr.clear_start_ind    = 1;

   cursor C_NO_CONFLICT_CL_IDS is
      select clearance_id
        from (select VALUE(ids) clearance_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select distinct price_event_id clearance_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) INDEX(rc PK_RPM_CLEARANCE_RESET)*/
             OBJ_PRICE_CHANGE_IL_REC(rc.clearance_id,
                                     RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                     rc.item,
                                     rc.location)
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance_reset rc
       where rc.clearance_id   = VALUE(ids)
         and rc.effective_date = LP_vdate;

BEGIN

   -- This function will only be called when the user change a reset date of an item/location that is on
   -- clearance using the Create Clearance Reset screen.

   -- First Populate RPM_CLEARANCE_GTT

   if RPM_CLEARANCE_GTT_SQL.POPULATE_GTT_FOR_RESET(O_cc_error_tbl,
                                                   I_price_event_ids) = 0 then
      return 0;
   end if;

   open C_VALIDATE_DATE;
   fetch C_VALIDATE_DATE BULK COLLECT into L_conflict_error_tbl;
   close C_VALIDATE_DATE;

   -- Get Clearances that have no conflict from C_VALIDATE_DATE validation

   open C_NO_CONFLICT_CL_IDS;
   fetch C_NO_CONFLICT_CL_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_CL_IDS;

   -- Do the conflict checking

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      forall i IN 1..L_nc_price_event_ids.COUNT
         update rpm_clearance_gtt
            set effective_date  = I_new_clr_reset_date,
                step_identifier = RPM_CONSTANTS.CAPT_GTT_PROCESS_NEW_CR
          where clearance_id = L_nc_price_event_ids(i)
            and reset_ind    = 1;

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_PROCESS_NEW_CR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                  LP_cc_error_tbl,
                                                  L_nc_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  0, -- specific_item_loc,
                                                  0, -- new_item_loc
                                                  NULL, -- new_promo_end_date
                                                  LP_bulk_cc_pe_id,
                                                  LP_parent_thread_number,
                                                  LP_thread_number,
                                                  0, -- I_chunk_number
                                                  I_end_state) = 0 then
         return 0;
      end if;

   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   if L_cc_error_tbl is NOT NULL and
      L_cc_error_tbl.COUNT > 0 then

      for i IN 1..L_cc_error_tbl.COUNT loop

         L_conflict_error_tbl.EXTEND();
         L_conflict_error_tbl(L_conflict_error_tbl.COUNT) := L_cc_error_tbl(i);

      end loop;

   end if;

   -- Get Clearances that have no conflict after the Conflict Checking Process

   open C_NO_CONFLICT_CL_IDS;
   fetch C_NO_CONFLICT_CL_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_CL_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      -- Update the state of the clearance_reset that has no conflict back to its original state
      forall i IN 1..L_nc_price_event_ids.COUNT
         update rpm_clearance_reset
            set state = I_start_state,
                approval_date = DECODE(I_start_state,
                                       RPM_CONSTANTS.PC_APPROVED_STATE_CODE, SYSDATE,
                                       approval_date),
                approval_id   = DECODE(I_start_state,
                                       RPM_CONSTANTS.PC_APPROVED_STATE_CODE, I_user_name,
                                       approval_id)
          where clearance_id = L_nc_price_event_ids(i);

      open C_EMERGENCY;
      fetch C_EMERGENCY BULK COLLECT into L_emergency_cl;
      close C_EMERGENCY;

      if L_emergency_cl is NOT NULL and
         L_emergency_cl.COUNT > 0 then

         forall i IN 1..L_emergency_cl.COUNT
            update rpm_clearance_reset
               set state = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
             where clearance_id = L_emergency_cl(i).price_change_id
               and state       != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

         -- These actions are for emergency Clearance
         if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                           L_emergency_cl,
                                           FALSE) = 0 then

            O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                              CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       L_error_message));
            return 0;

         end if;

      end if;
   end if;

   -- Set Back the state to the starting state in case there is a conflict

   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_conflict_error_tbl,
                      LP_price_event_type,
                      I_start_state) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_conflict_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_NEW_CLEARANCE_RESETS;

--------------------------------------------------------------------------------

FUNCTION PROCESS_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id    IN     NUMBER,
                               I_persist_ind     IN     VARCHAR2,
                               I_start_state     IN     VARCHAR2,
                               I_end_state       IN     VARCHAR2,
                               I_user_name       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_PROMO_DETAILS';

   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;
   L_emergency_pr       OBJ_NUMERIC_ID_TABLE     := NULL;
   L_price_event_type   VARCHAR(3)               := NULL;

   L_pc_il_tbl     OBJ_PRICE_CHANGE_IL_TBL := NULL;
   L_error_message VARCHAR2(256)           := NULL;
   L_new_item_loc  NUMBER(1)               := 0;
   L_start_time    TIMESTAMP               := SYSTIMESTAMP;

   cursor C_NO_CONFLICT_PCD_IDS is
      select promo_dtl_id
        from (select VALUE(ids) promo_dtl_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select distinct price_event_id promo_dtl_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) */
             rpd.promo_dtl_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where rpd.promo_dtl_id       = VALUE(ids)
         and TRUNC(rpd.start_date) <= LP_vdate;

   cursor C_PC_IL_TBL(I_price_event_id IN NUMBER) is
      select /*+ USE_HASH(rpi, rpl) ORDERED */
             OBJ_PRICE_CHANGE_IL_REC(rpd.promo_dtl_id,
                                     RPM_CONSTANTS.PE_TYPE_PROMO_START,
                                     rpi.item,
                                     rpl.location)
        from rpm_promo_dtl rpd,
             rpm_promo_comp rpc,
             rpm_bulk_cc_pe_item rpi,
             rpm_bulk_cc_pe_location rpl
       where rpd.promo_dtl_id   = I_price_event_id
         and rpc.promo_comp_id  = rpd.promo_comp_id
         and rpc.type           = RPM_CONSTANTS.SIMPLE_CODE
         and rpi.bulk_cc_pe_id  = LP_bulk_cc_pe_id
         and rpi.price_event_id = rpd.promo_dtl_id
         and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
         and rpl.price_event_id = rpi.price_event_id
         and rpl.itemloc_id     = rpi.itemloc_id;

BEGIN

   if I_price_event_ids is NOT NULL and
      I_price_event_ids.COUNT > 0 then

      if NVL(I_user_name, '') = RPM_CONSTANTS.RPM_NIL_BATCH then
         L_new_item_loc := 1;
      else
         L_new_item_loc := 0;
      end if;

      if I_end_state IN (RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE,
                         RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                         RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) or
         I_persist_ind != 'Y' then

         -- Do the conflict checking
         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                     LP_cc_error_tbl,
                                                     I_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     0, -- I_specific_item_loc
                                                     L_new_item_loc,
                                                     NULL, -- I_new_promo_end_date
                                                     LP_bulk_cc_pe_id,
                                                     LP_parent_thread_number,
                                                     LP_thread_number,
                                                     0, -- I_chunk_number
                                                     I_end_state,
                                                     LP_reprocess_ind) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

      elsif I_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE then

         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE(O_cc_error_tbl,
                                                     I_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     LP_bulk_cc_pe_id,
                                                     NULL, --I_pe_sequence_id
                                                     NULL, --I_pe_thread_number
                                                     0,    --I_chunk_number
                                                     I_end_state) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

         if I_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE then

            if UPDATE_PROMO_DETAIL_DATES(O_cc_error_tbl,
                                         I_price_event_ids) = 0 then
               return 0;
            end if;

         end if;

      elsif I_end_state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then

         if L_new_item_loc = 1 then
            L_price_event_type := LP_price_event_type;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE;
         elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO then
            L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE;
         end if;

         if L_new_item_loc != 1 then

            update rpm_promo_dtl
               set end_date = TO_DATE(CONCAT(TO_CHAR(LP_vdate, 'YYYYMMDD'), '23:59:59'), 'YYYYMMDDHH24:MI:SS')
             where promo_dtl_id IN (select VALUE(ids)
                                      from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

         end if;

         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                     LP_cc_error_tbl,
                                                     I_price_event_ids,
                                                     L_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     0, -- I_specific_item_loc
                                                     L_new_item_loc,
                                                     case L_new_item_loc
                                                        when 1 then
                                                           NULL
                                                        else
                                                           TO_DATE(CONCAT(TO_CHAR(LP_vdate, 'YYYYMMDD'), '23:59'), 'YYYYMMDDHH24:MI')
                                                     end,
                                                     LP_bulk_cc_pe_id,
                                                     LP_parent_thread_number,
                                                     LP_thread_number,
                                                     0, --I_chunk_number
                                                     I_end_state,
                                                     LP_reprocess_ind) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

      end if;

   end if;

   -- Get Promotion Component Details that have no conflict
   open C_NO_CONFLICT_PCD_IDS;
   fetch C_NO_CONFLICT_PCD_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_PCD_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      -- Update the state of the Promotion Component Details that has no conflict
      update /*+ INDEX(target PK_RPM_PROMO_DTL) */
             rpm_promo_dtl target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, NVL(approval_date, SYSDATE),
                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, NVL(approval_date, SYSDATE),
                                    NULL),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_id, I_user_name),
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, NVL(approval_id, I_user_name),
                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, NVL(approval_id, I_user_name),
                                    NULL)
       where promo_dtl_id IN (select VALUE(ids)
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and state        != I_end_state;

      -- Update the status of any SGEs that might exist
      update /*+ INDEX(target PK_RPM_PROMO_DTL) */
             rpm_promo_dtl target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_date, SYSDATE),
                                    NULL),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, NVL(approval_id, I_user_name),
                                    NULL)
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) */
                                     rpd.promo_dtl_id
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_promo_dtl rpd
                               where rpd.exception_parent_id = VALUE(ids)
                                 and sys_generated_exclusion = 1)
         and state        != I_end_state;

      if I_end_state IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                         RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) then

         -- Process Deal in RMS
         if CREATE_OR_UPDATE_PROMO_DEAL(O_cc_error_tbl,
                                        L_nc_price_event_ids,
                                        I_user_name) = 0 then
            return 0;
         end if;

         open C_EMERGENCY;
         fetch C_EMERGENCY BULK COLLECT into L_emergency_pr;
         close C_EMERGENCY;

         if L_emergency_pr is NOT NULL and
            L_emergency_pr.COUNT > 0 then

            -- No Bulk interface yet so need to loop
            if LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO) then

               for i IN 1..L_emergency_pr.COUNT loop

                  open C_PC_IL_TBL(L_emergency_pr(i));
                  fetch C_PC_IL_TBL BULK COLLECT into L_pc_il_tbl;
                  close C_PC_IL_TBL;

                  if L_pc_il_tbl is NOT NULL and
                     L_pc_il_tbl.COUNT > 0 then

                     if RPM_PRICE_EVENT_SQL.UPDATE_RMS(L_error_message,
                                                       L_pc_il_tbl,
                                                       FALSE) = 0 then
                        O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                          CONFLICT_CHECK_ERROR_REC(NULL,
                                                                   NULL,
                                                                   RPM_CONSTANTS.PLSQL_ERROR,
                                                                   L_error_message));
                        return 0;
                     end if;

                  end if;

               end loop;

            end if;

            if I_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then

               -- Update state to ACTIVE
               update /*+ INDEX(target PK_RPM_PROMO_DTL) */
                      rpm_promo_dtl target
                  set state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                where promo_dtl_id IN (select VALUE(ids)
                                         from table(cast(L_emergency_pr as OBJ_NUMERIC_ID_TABLE)) ids)
                  and state        != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE;

               -- Update the state of any SGEs that might exist
               update /*+ INDEX(target PK_RPM_PROMO_DTL) */
                      rpm_promo_dtl target
                  set state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) */
                                              rpd.promo_dtl_id
                                         from table(cast(L_emergency_pr as OBJ_NUMERIC_ID_TABLE)) ids,
                                              rpm_promo_dtl rpd
                                        where rpd.exception_parent_id = VALUE(ids)
                                          and sys_generated_exclusion = 1)
                  and state        != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE;

            end if;

         end if;

      end if;

   end if;

   -- Set Back the state to the starting state in case there is a conflict
   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_conflict_error_tbl,
                      LP_price_event_type,
                      I_start_state) = 0 then

      return 0;

   end if;

   O_cc_error_tbl := L_conflict_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));

      return 0;

END PROCESS_PROMO_DETAILS;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION PROCESS_UPDATE_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                      I_rib_trans_id    IN     NUMBER,
                                      I_persist_ind     IN     VARCHAR2,
                                      I_start_state     IN     VARCHAR2,
                                      I_end_state       IN     VARCHAR2,
                                      I_promo_end_date  IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_UPDATE_PROMO_DETAILS';

   L_start_time         TIMESTAMP                := SYSTIMESTAMP;
   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;

   -- Promotion Component Detail Id with no conflict
   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_NO_CONFLICT_PCD_IDS is
      select promo_dtl_id
        from (select VALUE(ids) promo_dtl_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select distinct price_event_id promo_dtl_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

BEGIN

   if I_price_event_ids is NOT NULL and
      I_price_event_ids.COUNT > 0 then

      -- Updating Promotion End date only allowed the promotion state is Active
      if I_end_state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE then

         -- Do the conflict checking
         if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                     LP_cc_error_tbl,
                                                     I_price_event_ids,
                                                     LP_price_event_type,
                                                     I_rib_trans_id,
                                                     I_persist_ind,
                                                     0,
                                                     0,
                                                     TRUNC(I_promo_end_date, 'MI'),
                                                     LP_bulk_cc_pe_id,
                                                     LP_parent_thread_number,
                                                     LP_thread_number,
                                                     0, --I_chunk_number
                                                     I_end_state,
                                                     LP_reprocess_ind) = 0 then
            return 0;
         end if;

         L_conflict_error_tbl := O_cc_error_tbl;

      end if;

   end if;

   -- Get Promotion Component Details that have no conflict
   open C_NO_CONFLICT_PCD_IDS;
   fetch C_NO_CONFLICT_PCD_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_PCD_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      -- Update the state and the end date of the Promotion Component Details that has no conflict
      update /*+ INDEX(target PK_RPM_PROMO_DTL) */
             rpm_promo_dtl target
         set state    = I_end_state,
             end_date = I_promo_end_date
       where promo_dtl_id IN (select VALUE(ids)
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and state        != I_end_state;

   end if;

   -- Set Back the state to the starting state in case there is a conflict
   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_conflict_error_tbl,
                      LP_price_event_type,
                      I_start_state) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_conflict_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));

      return 0;

END PROCESS_UPDATE_PROMO_DETAILS;

--------------------------------------------------------------------------------

FUNCTION RESET_PE_STATUS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type IN     VARCHAR2,
                         I_start_state      IN     VARCHAR2)
RETURN NUMBER IS

   L_program      VARCHAR2(45)         := 'RPM_BULK_CC_ACTIONS_SQL.RESET_PE_STATUS';
   L_valid_pe_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_VALID_PC is
      select /*+ CARDINALITY(ids 10) INDEX(rpc pk_rpm_price_change) */
             price_change_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids);

   cursor C_VALID_CL is
      select /*+ CARDINALITY(ids 10) INDEX(rc pk_rpm_clearance) */
             clearance_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc
       where rc.clearance_id = VALUE(ids);

   cursor C_VALID_CR is
      select /*+ CARDINALITY(ids 10) INDEX(rc pk_rpm_clearance) */
             clearance_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance_reset rc
       where rc.clearance_id = VALUE(ids);

   cursor C_VALID_PR is
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where rpd.promo_dtl_id = VALUE(ids);

BEGIN
   --
   -- Set Back the state to the starting state
   --
   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_VALID_PC;
      fetch C_VALID_PC BULK COLLECT into L_valid_pe_ids;
      close C_VALID_PC;

      update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */ rpm_price_change
         set state = I_start_state
       where price_change_id IN (select VALUE(ids)
                                   from table(cast(L_valid_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      open C_VALID_CL;
      fetch C_VALID_CL BULK COLLECT into L_valid_pe_ids;
      close C_VALID_CL;

      update /*+ INDEX(target PK_RPM_CLEARANCE) */ rpm_clearance
         set state = I_start_state
       where clearance_id IN (select VALUE(ids)
                                     from table(cast(L_valid_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET then

      open C_VALID_CL;
      fetch C_VALID_CL BULK COLLECT into L_valid_pe_ids;
      close C_VALID_CL;

      update /*+ INDEX(target PK_RPM_CLEARANCE) */ rpm_clearance
         set state             = I_start_state,
             out_of_stock_date = LP_old_clr_oostock_date,
             reset_date        = LP_old_clr_reset_date
       where clearance_id IN (select VALUE(ids)
                                     from table(cast(L_valid_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

      open C_VALID_CR;
      fetch C_VALID_CR BULK COLLECT into L_valid_pe_ids;
      close C_VALID_CR;

      update rpm_clearance_reset
         set state             = I_start_state
       where clearance_id IN (select VALUE(ids)
                                     from table(cast(L_valid_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO)  then

      open C_VALID_PR;
      fetch C_VALID_PR BULK COLLECT into L_valid_pe_ids;
      close C_VALID_PR;

      update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl
         set state = I_start_state
       where promo_dtl_id IN (select VALUE(ids)
                                     from table(cast(L_valid_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      open C_VALID_PR;
      fetch C_VALID_PR BULK COLLECT into L_valid_pe_ids;
      close C_VALID_PR;

      update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl
         set state             = I_start_state,
             end_date          = LP_old_promo_end_date,
             timebased_dtl_ind = NVL(LP_old_timebased_dtl_ind, timebased_dtl_ind)
       where promo_dtl_id IN (select VALUE(ids)
                                     from table(cast(L_valid_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RESET_PE_STATUS;

--------------------------------------------------------------------------------

FUNCTION RESET_PE_STATUS(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                         I_cc_error_tbl      IN     CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_type  IN     VARCHAR2,
                         I_start_state       IN     VARCHAR2)
RETURN NUMBER IS

   L_program          VARCHAR2(45)          := 'RPM_BULK_CC_ACTIONS_SQL.RESET_PE_STATUS';
   L_price_event_ids  OBJ_NUMERIC_ID_TABLE  := NULL;

   cursor C_PE_IDS is
      select price_event_id
        from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL));

BEGIN

   open C_PE_IDS;
   fetch C_PE_IDS BULK COLLECT into L_price_event_ids;
   close C_PE_IDS;

   if RESET_PE_STATUS(O_cc_error_tbl,
                      L_price_event_ids,
                      I_price_event_type,
                      I_start_state) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RESET_PE_STATUS;

--------------------------------------------------------------------------------

FUNCTION UPDATE_PROMO_DETAIL_DATES(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS
--
L_program   VARCHAR2(100) := 'RPM_BULK_CC_ACTIONS_SQL.UPDATE_PROMO_DETAIL_DATES';
--
BEGIN
   --
   merge into rpm_promo_dtl target
   using (select /*+ CARDINALITY (ids 10) */
                 rpd.promo_dtl_id,
                 case
                 when trunc(rpd.start_date) < startDateThreshold then
                    startDateThreshold
                 else
                    rpd.start_date
                 end as start_date,
                 case
                 when rpd.end_date is NULL then
                    rpd.end_date
                 when trunc(rpd.end_date) <= startDateThreshold then
                    startDateThreshold
                 else
                    rpd.end_date
                 end as end_date
            from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_promo_dtl rpd,
                 (select case
                         when LP_emergency_perm = 1 then
                            LP_vdate
                         else
                            LP_vdate + price_change_processing_days
                         end as startDateThreshold
                    from rpm_system_options) rs
           where rpd.promo_dtl_id = VALUE(ids)) source
      on (target.promo_dtl_id = source.promo_dtl_id)
   when matched then
      update
         set target.start_date = source.start_date,
             target.end_date = source.end_date
   -- Error out
   when not matched then
      insert (promo_dtl_id)
      values (-999);
   --
   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL, NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_PROMO_DETAIL_DATES;

--------------------------------------------------------------------------------

FUNCTION CREATE_OR_UPDATE_PROMO_DEAL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                     I_user_name        IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_ACTIONS_SQL.CREATE_OR_UPDATE_PROMO_DEAL';

   L_return_code        VARCHAR2(1)                            := NULL;
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE               := NULL;
   L_deal_ids_table     DEAL_IDS_TBL                           := NULL;
   L_deal_head_rec      DEAL_HEAD_REC                          := NULL;
   L_deal_detail_rec    DEAL_DETAIL_REC                        := NULL;
   L_deal_detail_tbl    DEAL_DETAIL_TBL                        := NULL;
   L_deal_comp_prom_rec DEAL_COMP_PROM_REC                     := NULL;
   L_deal_comp_prom_tbl DEAL_COMP_PROM_TBL                     := NULL;

   cursor C_COMPONENT is
      select rpdd.rpm_pending_deal_detail_id,
             rpdd.promo_id,
             t.promo_start_date,
             t.promo_end_date,
             rpdd.promo_comp_id,
             rpdd.deal_id,
             NULL deal_detail_id,
             rpdd.contribution_percent,
             rpd.rpm_pending_deal_id,
             rpd.partner_type,
             rpd.partner_id,
             rpd.supplier,
             DECODE(rpd.include_vat_ind,
                    1, 'Y',
                    'N') include_vat_ind,
             DECODE(rpd.stock_ledger_ind,
                    1, 'Y',
                    'N') stock_ledger_ind,
             rpd.invoice_proc_logic_type,
             rpd.invoice_proc_logic_code,
             rpd.deal_report_level_type,
             rpd.deal_report_level_code,
             rpd.bill_back_period_type,
             rpd.bill_back_period_code,
             rpd.bill_back_method_type,
             rpd.bill_back_method_code,
             rpdd.pending_deal_ind
        from (select /*+ ORDERED CARDINALITY(ids 10) INDEX(rpc PK_RPM_PROMO_COMP)*/
                     distinct rpc.promo_id,
                     rpd.promo_comp_id,
                     rpc.type,
                     rp.start_date promo_start_date,
                     rp.end_date promo_end_date
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp
               where rpd.promo_dtl_id  = VALUE(ids)
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 and rp.promo_id       = rpc.promo_id) t,
             rpm_pending_deal_detail rpdd,
             rpm_pending_deal rpd
       where rpdd.promo_id               = t.promo_id
         and rpdd.promo_comp_id          = t.promo_comp_id
         and rpd.rpm_pending_deal_id (+) = rpdd.deal_id
      union all
      select NULL rpm_pending_deal_detail_id,
             t.promo_id,
             t.promo_start_date,
             t.promo_end_date,
             t.promo_comp_id,
             dd.deal_id,
             dd.deal_detail_id,
             dcp.contribution_pct contribution_percent,
             NULL rpm_pending_deal_id,
             dh.partner_type,
             dh.partner_id,
             dh.supplier,
             include_vat_ind,
             stock_ledger_ind,
             NULL invoice_proc_logic_type,
             dh.invoice_processing_logic invoice_proc_logic_code,
             NULL deal_report_level_type,
             dh.deal_reporting_level deal_report_level_code,
             NULL bill_back_period_type,
             dh.bill_back_period bill_back_period_code,
             NULL bill_back_method_type,
             dh.bill_back_method bill_back_method_code,
             0 pending_deal_ind
        from (select /*+ ORDERED CARDINALITY(ids 10) INDEX(rpc PK_RPM_PROMO_COMP)*/
                     distinct
                     rpc.promo_id,
                     rpd.promo_comp_id,
                     rpc.type,
                     rp.start_date promo_start_date,
                     rp.end_date promo_end_date
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp
               where rpd.promo_dtl_id  = VALUE(ids)
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 and rp.promo_id       = rpc.promo_id) t,
             deal_comp_prom dcp,
             deal_head dh,
             deal_detail dd
       where dcp.promotion_id  = t.promo_id
         and dcp.promo_comp_id = t.promo_comp_id
         and dh.deal_id        = dcp.deal_id
         and dd.deal_id        = dcp.deal_id
         and dd.deal_detail_id = dcp.deal_detail_id;

BEGIN

   for rec IN C_COMPONENT loop

      L_deal_comp_prom_rec := NEW DEAL_COMP_PROM_REC(rec.promo_id,
                                                     rec.promo_comp_id,
                                                     rec.contribution_percent);

      L_deal_comp_prom_tbl := NEW DEAL_COMP_PROM_TBL(L_deal_comp_prom_rec);

      L_deal_detail_rec := NEW DEAL_DETAIL_REC(rec.deal_id,
                                               rec.deal_detail_id,
                                               rec.promo_start_date,
                                               rec.promo_end_date,
                                               rec.contribution_percent,
                                               I_user_name,
                                               NULL,
                                               L_deal_comp_prom_tbl);

      L_deal_detail_tbl := NEW DEAL_DETAIL_TBL(L_deal_detail_rec);

      if rec.pending_deal_ind = 1 then

         -- Create Deal in RMS if this is a pending deal
         L_deal_head_rec := NEW DEAL_HEAD_REC(NVL(rec.partner_type, 'S'),
                                              rec.partner_id,
                                              rec.supplier,
                                              NULL, -- CURRENCY_CODE - RPM will retrieve Supplier Curency Code,
                                              rec.promo_start_date,
                                              rec.promo_end_date,
                                              rec.bill_back_period_code,
                                              rec.deal_report_level_code,
                                              rec.bill_back_method_code,
                                              rec.invoice_proc_logic_code,
                                              rec.stock_ledger_ind,
                                              rec.include_vat_ind,
                                              I_user_name,
                                              L_deal_detail_tbl);

         MERCH_DEALS_API_SQL.CREATE_DEAL(L_return_code,
                                         L_error_message,
                                         L_deal_ids_table,
                                         L_deal_head_rec);

         if L_return_code != API_CODES.SUCCESS then

            O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                              CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                          L_error_message,
                                                                          L_program,
                                                                          TO_CHAR(SQLCODE))));
            return 0;
         end if;

      else

         -- Create Deal Promo Component
         MERCH_DEALS_API_SQL.NEW_DEAL_COMP(L_return_code,
                                           L_error_message,
                                           L_deal_ids_table,
                                           L_deal_detail_tbl);

         if L_return_code != API_CODES.SUCCESS then
            O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                              CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                          L_error_message,
                                                                          L_program,
                                                                          TO_CHAR(SQLCODE))));
            return 0;
         end if;

      end if;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CREATE_OR_UPDATE_PROMO_DEAL;

--------------------------------------------------------------------------------
FUNCTION PROCESS_FINANCE_PROMOTION(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                   I_rib_trans_id    IN     NUMBER,
                                   I_persist_ind     IN     VARCHAR2,
                                   I_start_state     IN     VARCHAR2,
                                   I_end_state       IN     VARCHAR2,
                                   I_user_name       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.PROCESS_FINANCE_PROMOTION';

   L_conflict_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_nc_price_event_ids OBJ_NUMERIC_ID_TABLE     := NULL;
   L_nc_child_ids       OBJ_NUMERIC_ID_TABLE     := NULL;
   L_emergency_pr       OBJ_NUMERIC_ID_TABLE     := NULL;
   L_start_time         TIMESTAMP                := SYSTIMESTAMP;

   cursor C_NO_CONFLICT_PC_IDS is
      select price_change_id
        from (select VALUE(ids) price_change_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select distinct price_event_id price_change_id
                from table(cast(L_conflict_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con);

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) */ rpd.promo_dtl_id
        from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where rpd.promo_dtl_id      = VALUE(ids)
         and TRUNC(rpd.start_date) = LP_vdate;

   cursor C_GET_CHILD_IDS is
      select promo_dtl_id
        from rpm_promo_dtl
       where exception_parent_id IN (select VALUE(ids)
                                       from (table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids));

BEGIN

   if I_end_state IN (RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE,
                      RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                      RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) then

      if RPM_FINANCE_PROMO_SQL.PROCESS_PRICE_EVENTS(O_cc_error_tbl,
                                                    I_price_event_ids) = 0 then
         return 0;
      end if;

      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 then

         L_conflict_error_tbl := O_cc_error_tbl;

         if RESET_PE_STATUS(O_cc_error_tbl,
                            I_price_event_ids,
                            LP_price_event_type,
                            I_start_state) = 0 then
            return 0;
         end if;

         O_cc_error_tbl := L_conflict_error_tbl;

         return 1;

      end if;

   end if;

   L_conflict_error_tbl := O_cc_error_tbl;

   open C_NO_CONFLICT_PC_IDS;
   fetch C_NO_CONFLICT_PC_IDS BULK COLLECT into L_nc_price_event_ids;
   close C_NO_CONFLICT_PC_IDS;

   open C_GET_CHILD_IDS;
   fetch C_GET_CHILD_IDS BULK COLLECT into L_nc_child_ids;
   close C_GET_CHILD_IDS;

   if L_nc_price_event_ids is NOT NULL and
      L_nc_price_event_ids.COUNT > 0 then

      update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, SYSDATE,
                                    approval_date),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, I_user_name,
                                    approval_id)
       where promo_dtl_id IN (select VALUE(ids)
                                from table(cast(L_nc_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

      update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl target
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, SYSDATE,
                                    approval_date),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, I_user_name,
                                    approval_id)
       where promo_dtl_id IN (select VALUE(ids)
                                from table(cast(L_nc_child_ids as OBJ_NUMERIC_ID_TABLE)) ids);

      if I_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE and
         I_persist_ind = 'Y' then
         ---
         if RPM_CC_PUBLISH.STAGE_FINANCE_PROM_MESSAGES(O_cc_error_tbl,
                                                       I_rib_trans_id,
                                                       L_nc_price_event_ids,
                                                       RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION) = 0 then
            return 0;
         end if;

      elsif I_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE and
            I_persist_ind = 'Y' then
         ---
         if RPM_CC_PUBLISH.STAGE_FIN_REMOVE_PROM_MESSAGES(O_cc_error_tbl,
                                                          I_rib_trans_id,
                                                          L_nc_price_event_ids,
                                                          RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION) = 0 then
            return 0;
         end if;

      elsif I_end_state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
         ---

         update rpm_promo_dtl
            set end_date = TO_DATE(CONCAT(TO_CHAR(LP_vdate, 'YYYYMMDD'), '23:59:59'), 'YYYYMMDDHH24:MI:SS')
          where promo_dtl_id IN (select VALUE(ids)
                                   from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

         if RPM_CC_PUBLISH.STAGE_FINANCE_PROM_MESSAGES(O_cc_error_tbl,
                                                       I_rib_trans_id,
                                                       L_nc_price_event_ids,
                                                       RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD) = 0 then
            return 0;

         end if;

      end if;

   end if;

   if I_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then

      open C_EMERGENCY;
      fetch C_EMERGENCY BULK COLLECT into L_emergency_pr;
      close C_EMERGENCY;

      if L_emergency_pr is NOT NULL and
         L_emergency_pr.COUNT > 0 then
         -- Update state to ACTIVE
         --
         update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl target
            set state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
          where promo_dtl_id IN (select VALUE(ids)
                                   from table(cast(L_emergency_pr as OBJ_NUMERIC_ID_TABLE)) ids);

         update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl target
            set state         = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
          where promo_dtl_id IN (select rpd.promo_dtl_id
                                   from table(cast(L_emergency_pr as OBJ_NUMERIC_ID_TABLE)) ids,
                                        rpm_promo_dtl rpd
                                  where rpd.exception_parent_id = VALUE(ids));
      end if;
   end if;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_parent_thread_number: '|| LP_parent_thread_number ||
                               ' - LP_thread_number: '|| LP_thread_number ,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_FINANCE_PROMOTION;
--------------------------------------------------------------------------------
FUNCTION SKIP_CC_FOR_PROMO(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id      IN     NUMBER,
                           I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                           I_price_event_type   IN     VARCHAR2,
                           I_rib_trans_id       IN     NUMBER,
                           I_persist_ind        IN     VARCHAR2,
                           I_start_state        IN     VARCHAR2,
                           I_end_state          IN     VARCHAR2,
                           I_user_name          IN     VARCHAR2,
                           I_new_promo_end_date IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BULK_CC_ACTIONS_SQL.SKIP_CC_FOR_PROMO';

   L_emergency_pr OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_EMERGENCY is
      select /*+ CARDINALITY (ids 10) */ rpd.promo_dtl_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where rpd.promo_dtl_id      = VALUE(ids)
         and TRUNC(rpd.start_date) = LP_vdate;

BEGIN

   if I_price_event_ids is NOT NULL and
      I_price_event_ids.COUNT > 0 then
      
      if I_end_state IN (RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                         RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) then

         update rpm_promo_dtl
            set end_date = DECODE(I_end_state,
                                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, I_new_promo_end_date,
                                  TO_DATE(CONCAT(TO_CHAR(LP_vdate, 'YYYYMMDD'), '23:59:59'), 'YYYYMMDDHH24:MI:SS'))
          where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) */ VALUE(ids)
                                   from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);
      end if;

      if RPM_FUTURE_RETAIL_SQL.SKIP_CC_PROCESS(O_cc_error_tbl,
                                               I_price_event_ids,
                                               I_price_event_type,
                                               I_bulk_cc_pe_id,
                                               I_rib_trans_id,
                                               I_end_state,
                                               I_persist_ind) = 0 then
         return 0;
      end if;

      update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl
         set state         = I_end_state,
             approval_date = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, SYSDATE,
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, NVL(approval_date, SYSDATE),
                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, NVL(approval_date, SYSDATE),                                     
                                    RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, NULL,
                                    approval_date),
             approval_id   = DECODE(I_end_state,
                                    RPM_CONSTANTS.PR_APPROVED_STATE_CODE, I_user_name,
                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE, NVL(approval_id, I_user_name),
                                    RPM_CONSTANTS.PR_ACTIVE_STATE_CODE, NVL(approval_id, I_user_name),                                    
                                    RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, NULL,
                                    approval_id)
       where promo_dtl_id IN (select VALUE(ids)
                                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids)
         and state        != I_end_state;

      if I_end_state IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                         RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) then

         -- Process Deal in RMS
         if CREATE_OR_UPDATE_PROMO_DEAL(O_cc_error_tbl,
                                        I_price_event_ids,
                                        I_user_name) = 0 then
            return 0;
         end if;

         open C_EMERGENCY;
         fetch C_EMERGENCY BULK COLLECT into L_emergency_pr;
         close C_EMERGENCY;

         if L_emergency_pr is NOT NULL and
            L_emergency_pr.COUNT > 0 then

            -- Update state to active
            update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl
               set state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             where promo_dtl_id IN (select VALUE(ids)
                                      from table(cast(L_emergency_pr as OBJ_NUMERIC_ID_TABLE)) ids);

         end if;

      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END SKIP_CC_FOR_PROMO;

--------------------------------------------------------------------------------
FUNCTION GENERATE_TIMELINE(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id IN     NUMBER,
                           I_pe_no_frs     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BULK_CC_ACTIONS_SQL.GENERATE_TIMELINE';

   L_count         NUMBER := 0;
   L_not_tran_item NUMBER := 0;
   L_not_location  NUMBER := 0;

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_pe_no_frs.COUNT: '|| I_pe_no_frs.COUNT);

   -- This function will generate a new timeline (if it does not exist yet)
   -- at the level of the price event being approved

   -- For Item/Location Level - Populate the RPM_PRICE_INQUIRY_GTT to prevent some performance issue
   delete rpm_price_inquiry_gtt;

   insert into rpm_price_inquiry_gtt
      (dept,
       price_event_id,
       item,
       diff_id,
       item_parent,
       location,
       loc_type,
       fr_zone_id)
   select t.dept,
          t.price_event_id,
          t.item,
          t.diff_id,
          t.item_parent,
          t.location,
          t.zone_node_type,
          t.zone_id
     from (select distinct
                  il.dept,
                  il.price_event_id,
                  il.item,
                  il.diff_id,
                  il.item_parent,
                  il.location,
                  il.zone_node_type,
                  zl.zone_id
             from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                          rpi.price_event_id,
                          rpi.dept,
                          rpi.item,
                          case
                             when im.item_parent is NULL and
                                  im.item_level = im.tran_level then
                                NULL
                             else
                                im.diff_1
                          end diff_id,
                          im.item_parent,
                          rpl.location,
                          rpl.zone_node_type,
                          rmrde.regular_zone_group,
                          RANK() OVER (PARTITION BY rpi.item,
                                                    rpi.diff_id,
                                                    rpl.zone_node_type,
                                                    rpl.location
                                           ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                    rpi.pe_merch_level desc,
                                                    rpi.price_event_id) rank
                     from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_bulk_cc_pe_item rpi,
                          rpm_bulk_cc_pe_location rpl,
                          item_master im,
                          rpm_merch_retail_def_expl rmrde,
                          rpm_item_loc ril
                    where rpi.price_event_id                 = VALUE(ids)
                      and rpl.price_event_id                 = VALUE(ids)
                      and rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                      and NVL(rpi.txn_man_excluded_item, 0) != 1
                      and rpi.item_parent                   is NULL
                      and rpl.zone_id                       is NULL
                      and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                      and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                      and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                      and rpl.price_event_id                 = rpi.price_event_id
                      and rpl.itemloc_id                     = rpi.itemloc_id
                      and rpl.zone_node_type                IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                      and im.item                            = rpi.item
                      and rmrde.dept                         = im.dept
                      and rmrde.class                        = im.class
                      and rmrde.subclass                     = im.subclass
                      and ril.dept                           = im.dept
                      and ril.item                           = im.item
                      and ril.loc                            = rpl.location
                      and rownum                             > 0) il,
                  (select rz.zone_group_id,
                          rz.zone_id,
                          rzl.location
                     from rpm_zone rz,
                          rpm_zone_location rzl
                    where rzl.zone_id = rz.zone_id
                      and rownum      > 0) zl
            where il.rank               = 1
              and il.regular_zone_group = zl.zone_group_id (+)
              and il.location           = zl.location (+)
              and rownum               >= 1) t
    where NOT EXISTS (select 1
                        from rpm_item_loc_gtt iloc
                       where iloc.price_event_id          = t.price_event_id
                         and iloc.item                    = t.item
                         and NVL(iloc.diff_id, '-999999') = NVL(t.diff_id, '-999999')
                         and iloc.location                = t.location
                         and iloc.regular_zone_group      = t.zone_node_type);

   select COUNT(1)
     into L_count
     from rpm_price_inquiry_gtt;

   if L_count = 0 then

      select /*+ CARDINALITY(ids 10) */
             COUNT(1)
        into L_not_tran_item
        from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_item rpi
       where rpi.price_event_id                 = VALUE(ids)
         and rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
         and rpi.pe_merch_level                != RPM_CONSTANTS.ITEM_MERCH_TYPE
         and NVL(rpi.txn_man_excluded_item, 0) != 1;

      select /*+ CARDINALITY(ids 10) */
             COUNT(1)
        into L_not_location
        from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_location rpl
       where rpl.price_event_id = VALUE(ids)
         and rpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
         and rpl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE;

      if L_not_tran_item = 0 and
         L_not_location  = 0 then
         --
         return 1;
      end if;

   end if;

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 dept,
                 class,
                 subclass,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 price_change_id,
                 price_change_display_id,
                 pc_exception_parent_id,
                 pc_change_type,
                 pc_change_amount,
                 pc_change_currency,
                 pc_change_percent,
                 pc_change_selling_uom,
                 pc_null_multi_ind,
                 pc_multi_units,
                 pc_multi_unit_retail,
                 pc_multi_unit_retail_currency,
                 pc_multi_selling_uom,
                 pc_price_guide_id,
                 clearance_id,
                 clearance_display_id,
                 clear_mkdn_index,
                 clear_start_ind,
                 clear_change_type,
                 clear_change_amount,
                 clear_change_currency,
                 clear_change_percent,
                 clear_change_selling_uom,
                 clear_price_guide_id,
                 loc_move_from_zone_id,
                 loc_move_to_zone_id,
                 location_move_id,
                 lock_version,
                 on_simple_promo_ind,
                 on_complex_promo_ind,
                 max_hier_level,
                 cur_hier_level,
                 future_retail_id
            from (-- price events at the Parent/Diff-Zone level
                  select t.price_event_id,
                         fr.dept,
                         class,
                         subclass,
                         t.item,
                         t.diff_id,
                         NULL item_parent,
                         t.zone_node_type,
                         fr.location,
                         NULL zone_id,
                         action_date,
                         selling_retail,
                         selling_retail_currency,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_unit_retail_currency,
                         multi_selling_uom,
                         clear_retail,
                         clear_retail_currency,
                         clear_uom,
                         simple_promo_retail,
                         simple_promo_retail_currency,
                         simple_promo_uom,
                         price_change_id,
                         price_change_display_id,
                         pc_exception_parent_id,
                         pc_change_type,
                         pc_change_amount,
                         pc_change_currency,
                         pc_change_percent,
                         pc_change_selling_uom,
                         pc_null_multi_ind,
                         pc_multi_units,
                         pc_multi_unit_retail,
                         pc_multi_unit_retail_currency,
                         pc_multi_selling_uom,
                         pc_price_guide_id,
                         clearance_id,
                         clearance_display_id,
                         clear_mkdn_index,
                         clear_start_ind,
                         clear_change_type,
                         clear_change_amount,
                         clear_change_currency,
                         clear_change_percent,
                         clear_change_selling_uom,
                         clear_price_guide_id,
                         loc_move_from_zone_id,
                         loc_move_to_zone_id,
                         location_move_id,
                         lock_version,
                         fr.on_simple_promo_ind,
                         fr.on_complex_promo_ind,
                         fr.max_hier_level,
                         RPM_CONSTANTS.FR_HIER_DIFF_ZONE cur_hier_level,
                         fr.future_retail_id
                    from (select s.dept,
                                 s.price_event_id,
                                 s.item,
                                 s.diff_id,
                                 s.location,
                                 s.zone_node_type
                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                         distinct
                                         rpi.dept,
                                         rpi.price_event_id,
                                         rpi.item,
                                         rpi.diff_id,
                                         rpl.location,
                                         rpl.zone_node_type,
                                         RANK() OVER (PARTITION BY rpi.item,
                                                                   rpi.diff_id,
                                                                   rpl.zone_node_type,
                                                                   rpl.location
                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                   rpi.pe_merch_level desc,
                                                                   rpi.price_event_id) rank
                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                         rpm_bulk_cc_pe_item rpi,
                                         rpm_bulk_cc_pe_location rpl,
                                         item_master im,
                                         rpm_zone_location rzl,
                                         rpm_item_loc ril
                                   where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                     and rpi.price_event_id                 = VALUE(ids)
                                     and rpl.price_event_id                 = VALUE(ids)
                                     and rpi.item_parent                   is NULL
                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                     and rpl.zone_id                       is NULL
                                     and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                     and rpl.price_event_id                 = rpi.price_event_id
                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                     and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                     and im.item_parent                     = rpi.item
                                     and im.diff_1                          = rpi.diff_id
                                     and ril.dept                           = im.dept
                                     and ril.item                           = im.item
                                     and ril.loc                            = rzl.location
                                     and rzl.zone_id                        = rpl.location) s
                           where rank = 1
                             and NOT EXISTS (select 1
                                               from rpm_item_loc_gtt iloc
                                              where iloc.price_event_id       = s.price_event_id
                                                and iloc.item                 = s.item
                                                and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                and iloc.location             = s.location
                                                and iloc.regular_zone_group   = s.zone_node_type)
                             and rownum >= 1) t,
                         rpm_future_retail fr
                   where fr.dept           = t.dept
                     and fr.item           = t.item
                     and fr.diff_id        is NULL
                     and fr.location       = t.location
                     and fr.zone_node_type = t.zone_node_type
                  union all
                  -- price events at the parent and location level
                  -- For location that is part of the primary zone group - get it from Parent/Zone Level
                  -- For location that is not part of the primary zone group - it should be available already
                  select t.price_event_id,
                         fr.dept,
                         class,
                         subclass,
                         fr.item,
                         NULL diff_id,
                         NULL item_parent,
                         t.zone_node_type,
                         t.location,
                         t.zone_id,
                         action_date,
                         selling_retail,
                         selling_retail_currency,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_unit_retail_currency,
                         multi_selling_uom,
                         clear_retail,
                         clear_retail_currency,
                         clear_uom,
                         simple_promo_retail,
                         simple_promo_retail_currency,
                         simple_promo_uom,
                         price_change_id,
                         price_change_display_id,
                         pc_exception_parent_id,
                         pc_change_type,
                         pc_change_amount,
                         pc_change_currency,
                         pc_change_percent,
                         pc_change_selling_uom,
                         pc_null_multi_ind,
                         pc_multi_units,
                         pc_multi_unit_retail,
                         pc_multi_unit_retail_currency,
                         pc_multi_selling_uom,
                         pc_price_guide_id,
                         clearance_id,
                         clearance_display_id,
                         clear_mkdn_index,
                         clear_start_ind,
                         clear_change_type,
                         clear_change_amount,
                         clear_change_currency,
                         clear_change_percent,
                         clear_change_selling_uom,
                         clear_price_guide_id,
                         loc_move_from_zone_id,
                         loc_move_to_zone_id,
                         location_move_id,
                         lock_version,
                         fr.on_simple_promo_ind,
                         fr.on_complex_promo_ind,
                         fr.max_hier_level,
                         RPM_CONSTANTS.FR_HIER_PARENT_LOC cur_hier_level,
                         fr.future_retail_id
                    from (select s.dept,
                                 s.price_event_id,
                                 s.item,
                                 s.location,
                                 s.zone_node_type,
                                 s.zone_id
                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                         distinct
                                         rpi.dept,
                                         rpi.price_event_id,
                                         rpi.item,
                                         rpl.location,
                                         rpl.zone_node_type,
                                         rz.zone_id,
                                         RANK() OVER (PARTITION BY rpi.item,
                                                                   rpi.diff_id,
                                                                   rpl.zone_node_type,
                                                                   rpl.location
                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                   rpi.pe_merch_level desc,
                                                                   rpi.price_event_id) rank
                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                         rpm_bulk_cc_pe_item rpi,
                                         rpm_bulk_cc_pe_location rpl,
                                         item_master im,
                                         rpm_merch_retail_def_expl rmrde,
                                         rpm_zone rz,
                                         rpm_zone_location rzl,
                                         rpm_item_loc ril
                                   where rpl.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                     and rpl.price_event_id                 = VALUE(ids)
                                     and rpi.price_event_id                 = VALUE(ids)
                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                     and rpl.zone_node_type                IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                     and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                     and rpl.zone_id                       is NULL
                                     and rpi.item_parent                   is NULL
                                     and im.item_parent                     = rpi.item
                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                     and rpl.price_event_id                 = rpi.price_event_id
                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                     and rmrde.dept                         = im.dept
                                     and rmrde.class                        = im.class
                                     and rmrde.subclass                     = im.subclass
                                     and rmrde.regular_zone_group           = rz.zone_group_id
                                     and rz.zone_id                         = rzl.zone_id
                                     and rzl.location                       = rpl.location
                                     and ril.dept                           = im.dept
                                     and ril.item                           = im.item
                                     and ril.loc                            = rpl.location) s
                           where rank = 1
                            and NOT EXISTS (select 1
                                               from rpm_item_loc_gtt iloc
                                              where iloc.price_event_id     = s.price_event_id
                                                and iloc.item               = s.item
                                                and iloc.diff_id            is NULL
                                                and iloc.location           = s.location
                                                and iloc.regular_zone_group = s.zone_node_type)
                             and rownum >= 1) t,
                         rpm_future_retail fr
                   where fr.dept           = t.dept
                     and fr.item           = t.item
                     and fr.diff_id        is NULL
                     and fr.location       = t.zone_id
                     and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  union all
                  -- price events at the tran item and zone level
                  -- Get it from the two possible levels (either PD/Z or P/Z) and choose the lowest level available
                  select price_event_id,
                         dept,
                         class,
                         subclass,
                         item,
                         diff_id,
                         item_parent,
                         zone_node_type,
                         location,
                         NULL zone_id,
                         action_date,
                         selling_retail,
                         selling_retail_currency,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_unit_retail_currency,
                         multi_selling_uom,
                         clear_retail,
                         clear_retail_currency,
                         clear_uom,
                         simple_promo_retail,
                         simple_promo_retail_currency,
                         simple_promo_uom,
                         price_change_id,
                         price_change_display_id,
                         pc_exception_parent_id,
                         pc_change_type,
                         pc_change_amount,
                         pc_change_currency,
                         pc_change_percent,
                         pc_change_selling_uom,
                         pc_null_multi_ind,
                         pc_multi_units,
                         pc_multi_unit_retail,
                         pc_multi_unit_retail_currency,
                         pc_multi_selling_uom,
                         pc_price_guide_id,
                         clearance_id,
                         clearance_display_id,
                         clear_mkdn_index,
                         clear_start_ind,
                         clear_change_type,
                         clear_change_amount,
                         clear_change_currency,
                         clear_change_percent,
                         clear_change_selling_uom,
                         clear_price_guide_id,
                         loc_move_from_zone_id,
                         loc_move_to_zone_id,
                         location_move_id,
                         lock_version,
                         on_simple_promo_ind,
                         on_complex_promo_ind,
                         max_hier_level,
                         RPM_CONSTANTS.FR_HIER_ITEM_ZONE cur_hier_level,
                         future_retail_id
                    from (-- Get the two possible levels (either PD/Z or P/Z) and choose the one with max rank
                          select fr_level_rank,
                                 price_event_id,
                                 dept,
                                 class,
                                 subclass,
                                 item,
                                 diff_id,
                                 item_parent,
                                 zone_node_type,
                                 location,
                                 action_date,
                                 selling_retail,
                                 selling_retail_currency,
                                 selling_uom,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_unit_retail_currency,
                                 multi_selling_uom,
                                 clear_retail,
                                 clear_retail_currency,
                                 clear_uom,
                                 simple_promo_retail,
                                 simple_promo_retail_currency,
                                 simple_promo_uom,
                                 price_change_id,
                                 price_change_display_id,
                                 pc_exception_parent_id,
                                 pc_change_type,
                                 pc_change_amount,
                                 pc_change_currency,
                                 pc_change_percent,
                                 pc_change_selling_uom,
                                 pc_null_multi_ind,
                                 pc_multi_units,
                                 pc_multi_unit_retail,
                                 pc_multi_unit_retail_currency,
                                 pc_multi_selling_uom,
                                 pc_price_guide_id,
                                 clearance_id,
                                 clearance_display_id,
                                 clear_mkdn_index,
                                 clear_start_ind,
                                 clear_change_type,
                                 clear_change_amount,
                                 clear_change_currency,
                                 clear_change_percent,
                                 clear_change_selling_uom,
                                 clear_price_guide_id,
                                 loc_move_from_zone_id,
                                 loc_move_to_zone_id,
                                 location_move_id,
                                 lock_version,
                                 on_simple_promo_ind,
                                 on_complex_promo_ind,
                                 max_hier_level,
                                 future_retail_id,
                                 MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                       item,
                                                                       diff_id,
                                                                       location,
                                                                       zone_node_type) max_fr_lvl_rank
                            from (-- Look at the Parent/Zone level - rank 0
                                  select 0 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         class,
                                         subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.zone_node_type,
                                         fr.location,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from (select s.dept,
                                                 s.price_event_id,
                                                 s.item,
                                                 s.diff_id,
                                                 s.item_parent,
                                                 s.location,
                                                 s.zone_node_type
                                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                         distinct
                                                         rpi.dept,
                                                         rpi.price_event_id,
                                                         rpi.item,
                                                         im.diff_1 diff_id,
                                                         im.item_parent,
                                                         rpl.location,
                                                         rpl.zone_node_type,
                                                         RANK() OVER (PARTITION BY rpi.item,
                                                                                   rpi.diff_id,
                                                                                   rpl.zone_node_type,
                                                                                   rpl.location
                                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                                   rpi.pe_merch_level desc,
                                                                                   rpi.price_event_id) rank
                                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                         rpm_bulk_cc_pe_item rpi,
                                                         rpm_bulk_cc_pe_location rpl,
                                                         item_master im,
                                                         rpm_zone_location rzl,
                                                         rpm_item_loc ril
                                                   where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                                     and rpi.price_event_id                 = VALUE(ids)
                                                     and rpl.price_event_id                 = VALUE(ids)
                                                     and rpi.item_parent                   is NULL
                                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                                     and rpl.zone_id                       is NULL
                                                     and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                                     and im.item                            = rpi.item
                                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                                     and rpl.price_event_id                 = rpi.price_event_id
                                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                                     and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                     and rzl.zone_id                        = rpl.location
                                                     and ril.dept                           = im.dept
                                                     and ril.item                           = im.item
                                                     and ril.loc                            = rzl.location) s
                                           where rank = 1
                                             and NOT EXISTS (select 1
                                                               from rpm_item_loc_gtt iloc
                                                              where iloc.price_event_id       = s.price_event_id
                                                                and iloc.item                 = s.item
                                                                and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                                and iloc.location             = s.location
                                                                and iloc.regular_zone_group   = s.zone_node_type)
                                             and rownum >= 1) t,
                                         rpm_future_retail fr
                                   where fr.dept           = t.dept
                                     and fr.item           = t.item_parent
                                     and fr.diff_id        is NULL
                                     and fr.location       = t.location
                                     and fr.zone_node_type = t.zone_node_type
                                  union all
                                  -- Look at the Parent-Diff/Zone level - rank 1
                                  select 1 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         class,
                                         subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.zone_node_type,
                                         fr.location,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from (select s.dept,
                                                 s.price_event_id,
                                                 s.item,
                                                 s.diff_id,
                                                 s.item_parent,
                                                 s.location,
                                                 s.zone_node_type
                                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                         distinct
                                                         rpi.dept,
                                                         rpi.price_event_id,
                                                         rpi.item,
                                                         im.diff_1 diff_id,
                                                         im.item_parent,
                                                         rpl.location,
                                                         rpl.zone_node_type,
                                                         RANK() OVER (PARTITION BY rpi.item,
                                                                                   rpi.diff_id,
                                                                                   rpl.zone_node_type,
                                                                                   rpl.location
                                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                                   rpi.pe_merch_level desc,
                                                                                   rpi.price_event_id) rank
                                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                         rpm_bulk_cc_pe_item rpi,
                                                         rpm_bulk_cc_pe_location rpl,
                                                         item_master im,
                                                         rpm_zone_location rzl,
                                                         rpm_item_loc ril
                                                   where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                                     and rpi.price_event_id                 = VALUE(ids)
                                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                                     and rpl.price_event_id                 = VALUE(ids)
                                                     and rpi.item_parent                    is NULL
                                                     and rpl.zone_id                        is NULL
                                                     and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                                     and im.item                            = rpi.item
                                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                                     and rpl.price_event_id                 = rpi.price_event_id
                                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                                     and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                                     and rzl.zone_id                        = rpl.location
                                                     and ril.dept                           = im.dept
                                                     and ril.item                           = im.item
                                                     and ril.loc                            = rzl.location) s
                                           where rank = 1
                                             and NOT EXISTS (select 1
                                                               from rpm_item_loc_gtt iloc
                                                              where iloc.price_event_id       = s.price_event_id
                                                                and iloc.item                 = s.item
                                                                and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                                and iloc.location             = s.location
                                                                and iloc.regular_zone_group   = s.zone_node_type)
                                             and rownum >= 1) t,
                                         rpm_future_retail fr
                                   where fr.dept                 = t.dept
                                     and fr.item                 = t.item_parent
                                     and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                     and fr.location             = t.location
                                     and fr.zone_node_type       = t.zone_node_type))
                  where fr_level_rank = max_fr_lvl_rank
                  union all
                  -- price events at the Parent-Diff and Loc level
                  -- Get the three possible levels (PD/Z, P/L or P/Z) and choose the lowest level available
                  select price_event_id,
                         dept,
                         class,
                         subclass,
                         item,
                         diff_id,
                         NULL item_parent,
                         zone_node_type,
                         location,
                         zone_id,
                         action_date,
                         selling_retail,
                         selling_retail_currency,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_unit_retail_currency,
                         multi_selling_uom,
                         clear_retail,
                         clear_retail_currency,
                         clear_uom,
                         simple_promo_retail,
                         simple_promo_retail_currency,
                         simple_promo_uom,
                         price_change_id,
                         price_change_display_id,
                         pc_exception_parent_id,
                         pc_change_type,
                         pc_change_amount,
                         pc_change_currency,
                         pc_change_percent,
                         pc_change_selling_uom,
                         pc_null_multi_ind,
                         pc_multi_units,
                         pc_multi_unit_retail,
                         pc_multi_unit_retail_currency,
                         pc_multi_selling_uom,
                         pc_price_guide_id,
                         clearance_id,
                         clearance_display_id,
                         clear_mkdn_index,
                         clear_start_ind,
                         clear_change_type,
                         clear_change_amount,
                         clear_change_currency,
                         clear_change_percent,
                         clear_change_selling_uom,
                         clear_price_guide_id,
                         loc_move_from_zone_id,
                         loc_move_to_zone_id,
                         location_move_id,
                         lock_version,
                         on_simple_promo_ind,
                         on_complex_promo_ind,
                         max_hier_level,
                         RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level,
                         future_retail_id
                    from (-- Get the three possible levels (PD/Z, P/L or P/Z) and choose the one with max rank
                          select fr_level_rank,
                                 price_event_id,
                                 dept,
                                 class,
                                 subclass,
                                 item,
                                 diff_id,
                                 zone_node_type,
                                 location,
                                 zone_id,
                                 action_date,
                                 selling_retail,
                                 selling_retail_currency,
                                 selling_uom,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_unit_retail_currency,
                                 multi_selling_uom,
                                 clear_retail,
                                 clear_retail_currency,
                                 clear_uom,
                                 simple_promo_retail,
                                 simple_promo_retail_currency,
                                 simple_promo_uom,
                                 price_change_id,
                                 price_change_display_id,
                                 pc_exception_parent_id,
                                 pc_change_type,
                                 pc_change_amount,
                                 pc_change_currency,
                                 pc_change_percent,
                                 pc_change_selling_uom,
                                 pc_null_multi_ind,
                                 pc_multi_units,
                                 pc_multi_unit_retail,
                                 pc_multi_unit_retail_currency,
                                 pc_multi_selling_uom,
                                 pc_price_guide_id,
                                 clearance_id,
                                 clearance_display_id,
                                 clear_mkdn_index,
                                 clear_start_ind,
                                 clear_change_type,
                                 clear_change_amount,
                                 clear_change_currency,
                                 clear_change_percent,
                                 clear_change_selling_uom,
                                 clear_price_guide_id,
                                 loc_move_from_zone_id,
                                 loc_move_to_zone_id,
                                 location_move_id,
                                 lock_version,
                                 on_simple_promo_ind,
                                 on_complex_promo_ind,
                                 max_hier_level,
                                 future_retail_id,
                                 MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                       item,
                                                                       diff_id,
                                                                       location,
                                                                       zone_node_type) max_fr_lvl_rank
                            from (-- Look at the Parent/Zone level - rank 0
                                  -- Only available when Location is part of the Primary Zone Group
                                  select 0 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         class,
                                         subclass,
                                         t.item,
                                         t.diff_id,
                                         t.zone_node_type,
                                         t.location,
                                         t.zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from (select s.dept,
                                                 s.price_event_id,
                                                 s.item,
                                                 s.diff_id,
                                                 s.location,
                                                 s.zone_node_type,
                                                 s.zone_id
                                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                         distinct
                                                         rpi.dept,
                                                         rpi.price_event_id,
                                                         rpi.item,
                                                         rpi.diff_id,
                                                         rpl.location,
                                                         rpl.zone_node_type,
                                                         rz.zone_id,
                                                         RANK() OVER (PARTITION BY rpi.item,
                                                                                   rpi.diff_id,
                                                                                   rpl.zone_node_type,
                                                                                   rpl.location
                                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                                   rpi.pe_merch_level desc,
                                                                                   rpi.price_event_id) rank
                                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                         rpm_bulk_cc_pe_item rpi,
                                                         rpm_bulk_cc_pe_location rpl,
                                                         item_master im,
                                                         rpm_merch_retail_def_expl rmrde,
                                                         rpm_zone rz,
                                                         rpm_zone_location rzl,
                                                         rpm_item_loc ril
                                                   where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                                     and rpi.price_event_id                 = VALUE(ids)
                                                     and rpl.price_event_id                 = VALUE(ids)
                                                     and rpi.item_parent                   is NULL
                                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                                     and rpl.zone_id                       is NULL
                                                     and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                                     and rpl.price_event_id                 = rpi.price_event_id
                                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                                     and rpl.zone_node_type                IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                     and im.item_parent                     = rpi.item
                                                     and im.diff_1                          = rpi.diff_id
                                                     and rmrde.dept                         = im.dept
                                                     and rmrde.class                        = im.class
                                                     and rmrde.subclass                     = im.subclass
                                                     and rmrde.regular_zone_group           = rz.zone_group_id
                                                     and rz.zone_id                         = rzl.zone_id
                                                     and rzl.location                       = rpl.location
                                                     and ril.dept                           = im.dept
                                                     and ril.item                           = im.item
                                                     and ril.loc                            = rpl.location) s
                                           where rank = 1
                                             and NOT EXISTS (select 1
                                                               from rpm_item_loc_gtt iloc
                                                              where iloc.price_event_id       = s.price_event_id
                                                                and iloc.item                 = s.item
                                                                and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                                and iloc.location             = s.location
                                                                and iloc.regular_zone_group   = s.zone_node_type)
                                             and rownum >= 1) t,
                                         rpm_future_retail fr
                                   where fr.dept           = t.dept
                                     and fr.item           = t.item
                                     and fr.diff_id        is NULL
                                     and fr.location       = t.zone_id
                                     and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  union all
                                  -- Look at the Parent-Diff/Zone level - rank 1
                                  -- Only available when Location is part of the Primary Zone Group
                                  select 1 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         class,
                                         subclass,
                                         t.item,
                                         t.diff_id,
                                         t.zone_node_type,
                                         t.location,
                                         t.zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from (select s.dept,
                                                 s.price_event_id,
                                                 s.item,
                                                 s.diff_id,
                                                 s.location,
                                                 s.zone_node_type,
                                                 s.zone_id
                                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                         distinct
                                                         rpi.dept,
                                                         rpi.price_event_id,
                                                         rpi.item,
                                                         rpi.diff_id,
                                                         rpl.location,
                                                         rpl.zone_node_type,
                                                         rz.zone_id,
                                                         RANK() OVER (PARTITION BY rpi.item,
                                                                                   rpi.diff_id,
                                                                                   rpl.zone_node_type,
                                                                                   rpl.location
                                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                                   rpi.pe_merch_level desc,
                                                                                   rpi.price_event_id) rank
                                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                         rpm_bulk_cc_pe_item rpi,
                                                         rpm_bulk_cc_pe_location rpl,
                                                         item_master im,
                                                         rpm_merch_retail_def_expl rmrde,
                                                         rpm_zone rz,
                                                         rpm_zone_location rzl,
                                                         rpm_item_loc ril
                                                   where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                                     and rpi.price_event_id                 = VALUE(ids)
                                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                                     and rpl.price_event_id                 = VALUE(ids)
                                                     and rpi.item_parent                   is NULL
                                                     and rpl.zone_id                       is NULL
                                                     and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                                     and im.item_parent                     = rpi.item
                                                     and im.diff_1                          = rpi.diff_id
                                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                                     and rpl.price_event_id                 = rpi.price_event_id
                                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                                     and rpl.zone_node_type                IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                     and rmrde.dept                         = im.dept
                                                     and rmrde.class                        = im.class
                                                     and rmrde.subclass                     = im.subclass
                                                     and rmrde.regular_zone_group           = rz.zone_group_id
                                                     and rz.zone_id                         = rzl.zone_id
                                                     and rzl.location                       = rpl.location
                                                     and ril.dept                           = im.dept
                                                     and ril.item                           = im.item
                                                     and ril.loc                            = rpl.location) s
                                           where rank = 1
                                             and NOT EXISTS (select 1
                                                               from rpm_item_loc_gtt iloc
                                                              where iloc.price_event_id       = s.price_event_id
                                                                and iloc.item                 = s.item
                                                                and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                                and iloc.location             = s.location
                                                                and iloc.regular_zone_group   = s.zone_node_type)
                                             and rownum >= 1) t,
                                         rpm_future_retail fr
                                   where fr.dept                 = t.dept
                                     and fr.item                 = t.item
                                     and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                     and fr.location             = t.zone_id
                                     and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  union all
                                  -- Look at the Parent/Loc level - rank 1 (same rank as PD/Z - can't have both and be here)
                                  -- Available for both Location is part of the PZG and Location is NOT part of PZG
                                  select 1 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         class,
                                         subclass,
                                         t.item,
                                         t.diff_id,
                                         t.zone_node_type,
                                         t.location,
                                         fr.zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from (select s.dept,
                                                 s.price_event_id,
                                                 s.item,
                                                 s.diff_id,
                                                 s.location,
                                                 s.zone_node_type
                                            from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                         distinct
                                                         rpi.dept,
                                                         rpi.price_event_id,
                                                         rpi.item,
                                                         rpi.diff_id,
                                                         rpl.location,
                                                         rpl.zone_node_type,
                                                         RANK() OVER (PARTITION BY rpi.item,
                                                                                   rpi.diff_id,
                                                                                   rpl.zone_node_type,
                                                                                   rpl.location
                                                                          ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                                   rpi.pe_merch_level desc,
                                                                                   rpi.price_event_id) rank
                                                    from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                         rpm_bulk_cc_pe_item rpi,
                                                         rpm_bulk_cc_pe_location rpl,
                                                         item_master im,
                                                         rpm_item_loc ril
                                                   where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                                     and rpi.price_event_id                 = VALUE(ids)
                                                     and NVL(rpi.txn_man_excluded_item, 0) != 1
                                                     and rpl.price_event_id                 = VALUE(ids)
                                                     and rpi.item_parent                   is NULL
                                                     and rpl.zone_id                       is NULL
                                                     and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                                     and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                                     and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                                     and rpl.price_event_id                 = rpi.price_event_id
                                                     and rpl.itemloc_id                     = rpi.itemloc_id
                                                     and rpl.zone_node_type                IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                               RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                                     and im.item_parent                     = rpi.item
                                                     and im.diff_1                          = rpi.diff_id
                                                     and ril.dept                           = im.dept
                                                     and ril.item                           = im.item
                                                     and ril.loc                            = rpl.location) s
                                                   where rank = 1
                                                   and NOT EXISTS (select 1
                                                               from rpm_item_loc_gtt iloc
                                                              where iloc.price_event_id       = s.price_event_id
                                                                and iloc.item                 = s.item
                                                                and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                                and iloc.location             = s.location
                                                                and iloc.regular_zone_group   = s.zone_node_type)
                                             and rownum >= 1) t,
                                         rpm_future_retail fr
                                   where fr.dept           = t.dept
                                     and fr.item           = t.item
                                     and fr.diff_id        is NULL
                                     and fr.location       = t.location
                                     and fr.zone_node_type = t.zone_node_type))
                   where fr_level_rank = max_fr_lvl_rank
                  union all
                  -- price events at the item and location level
                  -- Get all levels above I/L (I/Z, PD/L, PD/Z, P/L or P/Z) and choose the lowest level available
                  select price_event_id,
                         dept,
                         class,
                         subclass,
                         item,
                         diff_id,
                         item_parent,
                         zone_node_type,
                         location,
                         zone_id,
                         action_date,
                         selling_retail,
                         selling_retail_currency,
                         selling_uom,
                         multi_units,
                         multi_unit_retail,
                         multi_unit_retail_currency,
                         multi_selling_uom,
                         clear_retail,
                         clear_retail_currency,
                         clear_uom,
                         simple_promo_retail,
                         simple_promo_retail_currency,
                         simple_promo_uom,
                         price_change_id,
                         price_change_display_id,
                         pc_exception_parent_id,
                         pc_change_type,
                         pc_change_amount,
                         pc_change_currency,
                         pc_change_percent,
                         pc_change_selling_uom,
                         pc_null_multi_ind,
                         pc_multi_units,
                         pc_multi_unit_retail,
                         pc_multi_unit_retail_currency,
                         pc_multi_selling_uom,
                         pc_price_guide_id,
                         clearance_id,
                         clearance_display_id,
                         clear_mkdn_index,
                         clear_start_ind,
                         clear_change_type,
                         clear_change_amount,
                         clear_change_currency,
                         clear_change_percent,
                         clear_change_selling_uom,
                         clear_price_guide_id,
                         loc_move_from_zone_id,
                         loc_move_to_zone_id,
                         location_move_id,
                         lock_version,
                         on_simple_promo_ind,
                         on_complex_promo_ind,
                         max_hier_level,
                         RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                         future_retail_id
                    from (-- Get all levels above I/L (I/Z, PD/L, PD/Z, P/L or P/Z) and choose the one with max rank
                          select fr_level_rank,
                                 price_event_id,
                                 dept,
                                 class,
                                 subclass,
                                 item,
                                 diff_id,
                                 item_parent,
                                 zone_node_type,
                                 location,
                                 zone_id,
                                 action_date,
                                 selling_retail,
                                 selling_retail_currency,
                                 selling_uom,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_unit_retail_currency,
                                 multi_selling_uom,
                                 clear_retail,
                                 clear_retail_currency,
                                 clear_uom,
                                 simple_promo_retail,
                                 simple_promo_retail_currency,
                                 simple_promo_uom,
                                 price_change_id,
                                 price_change_display_id,
                                 pc_exception_parent_id,
                                 pc_change_type,
                                 pc_change_amount,
                                 pc_change_currency,
                                 pc_change_percent,
                                 pc_change_selling_uom,
                                 pc_null_multi_ind,
                                 pc_multi_units,
                                 pc_multi_unit_retail,
                                 pc_multi_unit_retail_currency,
                                 pc_multi_selling_uom,
                                 pc_price_guide_id,
                                 clearance_id,
                                 clearance_display_id,
                                 clear_mkdn_index,
                                 clear_start_ind,
                                 clear_change_type,
                                 clear_change_amount,
                                 clear_change_currency,
                                 clear_change_percent,
                                 clear_change_selling_uom,
                                 clear_price_guide_id,
                                 loc_move_from_zone_id,
                                 loc_move_to_zone_id,
                                 location_move_id,
                                 lock_version,
                                 on_simple_promo_ind,
                                 on_complex_promo_ind,
                                 max_hier_level,
                                 future_retail_id,
                                 MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                       item,
                                                                       diff_id,
                                                                       location,
                                                                       zone_node_type) max_fr_lvl_rank
                            from (-- Look at the Parent/Zone level - rank 0
                                  -- Only possible if Location is part of PZG
                                  select 0 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         fr.class,
                                         fr.subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.loc_type zone_node_type,
                                         t.location,
                                         t.fr_zone_id zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from rpm_price_inquiry_gtt t,
                                         rpm_future_retail fr
                                   where fr.dept           = t.dept
                                     and fr.item           = t.item_parent
                                     and fr.diff_id        is NULL
                                     and fr.location       = t.fr_zone_id
                                     and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  union all
                                  -- Look at the Parent-Diff/Zone level - rank 1
                                  -- Only possible if Location is part of PZG
                                  select 1 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         fr.class,
                                         fr.subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.loc_type zone_node_type,
                                         t.location,
                                         t.fr_zone_id zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from rpm_price_inquiry_gtt t,
                                         rpm_future_retail fr
                                   where fr.dept                 = t.dept
                                     and fr.item                 = t.item_parent
                                     and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                     and fr.location             = t.fr_zone_id
                                     and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  union all
                                  -- Look at the Parent/Loc level - rank 1 (same rank as PD/Z - can't have both and be here)
                                  -- Location can be either part of the PZG or NOT
                                  select 1 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         fr.class,
                                         fr.subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.loc_type zone_node_type,
                                         t.location,
                                         t.fr_zone_id zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from rpm_price_inquiry_gtt t,
                                         rpm_future_retail fr
                                   where fr.dept           = t.dept
                                     and fr.item           = t.item_parent
                                     and fr.diff_id        is NULL
                                     and fr.location       = t.location
                                     and fr.zone_node_type = t.loc_type
                                  union all
                                  -- Look at the Item/Zone level - rank 2
                                  -- Only possible if Location is part of PZG
                                  select 2 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         fr.class,
                                         fr.subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.loc_type zone_node_type,
                                         t.location,
                                         t.fr_zone_id zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from rpm_price_inquiry_gtt t,
                                         rpm_future_retail fr
                                   where fr.dept                 = t.dept
                                     and fr.item                 = t.item
                                     and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                     and fr.location             = t.fr_zone_id
                                     and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                  union all
                                  -- Look at the Parent-Diff/Loc level - rank 2 (same rank as I/Z - can't have both and be here)
                                  -- Location can be either part of the PZG or NOT
                                  select 2 fr_level_rank,
                                         t.price_event_id,
                                         fr.dept,
                                         fr.class,
                                         fr.subclass,
                                         t.item,
                                         t.diff_id,
                                         t.item_parent,
                                         t.loc_type zone_node_type,
                                         t.location,
                                         t.fr_zone_id zone_id,
                                         action_date,
                                         selling_retail,
                                         selling_retail_currency,
                                         selling_uom,
                                         multi_units,
                                         multi_unit_retail,
                                         multi_unit_retail_currency,
                                         multi_selling_uom,
                                         clear_retail,
                                         clear_retail_currency,
                                         clear_uom,
                                         simple_promo_retail,
                                         simple_promo_retail_currency,
                                         simple_promo_uom,
                                         price_change_id,
                                         price_change_display_id,
                                         pc_exception_parent_id,
                                         pc_change_type,
                                         pc_change_amount,
                                         pc_change_currency,
                                         pc_change_percent,
                                         pc_change_selling_uom,
                                         pc_null_multi_ind,
                                         pc_multi_units,
                                         pc_multi_unit_retail,
                                         pc_multi_unit_retail_currency,
                                         pc_multi_selling_uom,
                                         pc_price_guide_id,
                                         clearance_id,
                                         clearance_display_id,
                                         clear_mkdn_index,
                                         clear_start_ind,
                                         clear_change_type,
                                         clear_change_amount,
                                         clear_change_currency,
                                         clear_change_percent,
                                         clear_change_selling_uom,
                                         clear_price_guide_id,
                                         loc_move_from_zone_id,
                                         loc_move_to_zone_id,
                                         location_move_id,
                                         lock_version,
                                         fr.on_simple_promo_ind,
                                         fr.on_complex_promo_ind,
                                         fr.max_hier_level,
                                         fr.future_retail_id
                                    from rpm_price_inquiry_gtt t,
                                         rpm_future_retail fr
                                   where fr.dept                 = t.dept
                                     and fr.item                 = t.item_parent
                                     and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                     and fr.location             = t.location
                                     and fr.zone_node_type       = t.loc_type))
                           where fr_level_rank = max_fr_lvl_rank)) source
   on (    target.price_event_id         = source.price_event_id
       and target.item                   = source.item
       and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.action_date            = source.action_date)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              diff_id,
              item_parent,
              zone_node_type,
              location,
              zone_id,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              on_simple_promo_ind,
              on_complex_promo_ind,
              max_hier_level,
              cur_hier_level,
              original_fr_id,
              step_identifier)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.diff_id,
              source.item_parent,
              source.zone_node_type,
              source.location,
              source.zone_id,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.max_hier_level,
              source.cur_hier_level,
              source.future_retail_id,
              RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_TIMELINE;
--------------------------------------------------------------------------------
FUNCTION GENERATE_TL_FROM_INTERSECTION(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_ACTIONS_SQL.GENERATE_TL_FROM_INTERSECTION';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_price_event_ids.COUNT: '|| I_price_event_ids.COUNT);

   merge into rpm_future_retail_gtt target
   using (select /*+ ORDERED */
                 t.price_event_id,
                 fr.dept,
                 class,
                 subclass,
                 t.to_item item,
                 t.to_diff_id diff_id,
                 t.to_item_parent item_parent,
                 t.to_zone_node_type zone_node_type,
                 t.to_location location,
                 t.to_zone_id zone_id,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 price_change_id,
                 price_change_display_id,
                 pc_exception_parent_id,
                 pc_change_type,
                 pc_change_amount,
                 pc_change_currency,
                 pc_change_percent,
                 pc_change_selling_uom,
                 pc_null_multi_ind,
                 pc_multi_units,
                 pc_multi_unit_retail,
                 pc_multi_unit_retail_currency,
                 pc_multi_selling_uom,
                 pc_price_guide_id,
                 clearance_id,
                 clearance_display_id,
                 clear_mkdn_index,
                 clear_start_ind,
                 clear_change_type,
                 clear_change_amount,
                 clear_change_currency,
                 clear_change_percent,
                 clear_change_selling_uom,
                 clear_price_guide_id,
                 loc_move_from_zone_id,
                 loc_move_to_zone_id,
                 location_move_id,
                 lock_version,
                 fr.on_simple_promo_ind,
                 fr.on_complex_promo_ind,
                 fr.max_hier_level,
                 t.to_cur_hier_level cur_hier_level,
                 fr.future_retail_id
            from (select price_event_id,
                         dept,
                         from_item,
                         from_diff_id,
                         from_location,
                         from_zone_node_type,
                         to_item,
                         to_diff_id,
                         to_item_parent,
                         to_location,
                         to_zone_node_type,
                         to_zone_id,
                         to_cur_hier_level,
                         fr_rank,
                         MAX(fr_rank) OVER (PARTITION BY price_event_id,
                                                         to_item,
                                                         to_diff_id,
                                                         to_location,
                                                         to_zone_node_type) max_fr_rank
                    from (-- PD/Z and P/L intersection - Generate PD/L timeline:
                          -- working with PD/Z and P/L timeline exists
                          with item_locs as
                             (select i.price_event_id,
                                     i.item,
                                     i.dept,
                                     i.diff_id,
                                     i.pe_merch_level,
                                     i.item_parent,
                                     i.merch_level_type,
                                     i.subclass,
                                     i.class,
                                     l.location,
                                     l.zone_id,
                                     l.zone_node_type
                                from (select /*+ CARDINALITY(ids 10) */
                                             bulk_cc_pe_id,
                                             price_event_id,
                                             item,
                                             dept,
                                             diff_id,
                                             pe_merch_level,
                                             item_parent,
                                             merch_level_type,
                                             subclass,
                                             class,
                                             itemloc_id,
                                             RANK() OVER (PARTITION BY item,
                                                                       diff_id
                                                              ORDER BY NVL(txn_man_excluded_item, 0),
                                                                       pe_merch_level desc,
                                                                       price_event_id) rank
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_bulk_cc_pe_item
                                       where bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                         and price_event_id                 = VALUE(ids)
                                         and NVL(txn_man_excluded_item, 0) != 1) i,
                                     rpm_bulk_cc_pe_location l
                               where i.rank           = 1
                                 and i.bulk_cc_pe_id  = l.bulk_cc_pe_id
                                 and i.price_event_id = l.price_event_id
                                 and i.itemloc_id     = l.itemloc_id)
                          select /*+ ORDERED */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 NULL                           from_diff_id,
                                 rzl.location                   from_location,
                                 rzl.loc_type                   from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 NULL                           to_item_parent,
                                 rzl.location                   to_location,
                                 rzl.loc_type                   to_zone_node_type,
                                 rzl.zone_id                    to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.item_parent      is NULL
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and il.zone_id          is NULL
                             and rzl.zone_id         = il.location
                             and im.item_parent      = il.item
                             and im.diff_1           = il.diff_id
                             and ril.dept            = im.dept
                             and ril.item            = im.item
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.loc             = rzl.location
                             -- look for an existing timeline at the P/L level
                             and rfr.dept            = il.dept
                             and rfr.item            = il.item
                             and rfr.diff_id         is NULL
                             and rfr.location        = rzl.location
                             and rfr.zone_node_type  = rzl.loc_type
                             and rownum              > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = rzl.location
                                                and gtt.zone_node_type       = rzl.loc_type)
                          union all
                          -- working with P/L and PD/Z timeline exists
                          select /*+ ORDERED */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 il.diff_id                     from_diff_id,
                                 rzl.zone_id                    from_location,
                                 rfr.zone_node_type             from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 NULL                           to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 rzl.zone_id                    to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_merch_retail_def_expl rmrde,
                                 rpm_zone rz,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.item_parent           is NULL
                             and il.zone_node_type        IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id               is NULL
                             and il.dept                  = rmrde.dept
                             and il.class                 = rmrde.class
                             and il.subclass              = rmrde.subclass
                             and rmrde.regular_zone_group = rz.zone_group_id
                             and rz.zone_id               = rzl.zone_id
                             and rzl.location             = il.location
                             and im.item_parent           = il.item
                             and im.diff_1                = il.diff_id
                             and ril.dept                 = im.dept
                             and ril.item                 = im.item
                             and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.loc                  = il.location
                             -- Look for an existing PD/Z timeline
                             and rfr.dept                 = il.dept
                             and rfr.item                 = il.item
                             and rfr.diff_id              is NOT NULL
                             and rfr.diff_id              = il.diff_id
                             and rfr.location             = rzl.zone_id
                             and rfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and rownum                   > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- P/L and I/Z intersection - Generate I/L timeline:
                          -- working with P/L and I/Z exists
                          select /*+ ORDERED */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 il.diff_id                     from_diff_id,
                                 rz.zone_id                     from_location,
                                 rfr.zone_node_type             from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 rz.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 rpm_merch_retail_def_expl rmrde,
                                 rpm_zone rz,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.item_parent           is NOT NULL
                             and il.zone_node_type        IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id               is NULL
                             and il.dept                  = rmrde.dept
                             and il.class                 = rmrde.class
                             and il.subclass              = rmrde.subclass
                             and rmrde.regular_zone_group = rz.zone_group_id
                             and rz.zone_id               = rzl.zone_id
                             and rzl.location             = il.location
                             -- Look for an existing timeline at the I/Z level
                             and rfr.dept                 = il.dept
                             and rfr.item                 = il.item
                             and NVL(rfr.diff_id, '-999') = NVL(il.diff_id, '-999')
                             and rfr.location             = rz.zone_id
                             and rfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept                 = il.dept
                             and ril.item                 = il.item
                             and ril.loc                  = il.location
                             and rownum                   > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- working with I/Z and P/L exists
                          select /*+ ORDERED USE_NL(ril) */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 im.dept,
                                 im.item_parent                 from_item,
                                 NULL                           from_diff_id,
                                 il.location                    from_loc,
                                 il.zone_node_type              from_zone_node_type,
                                 im.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 im.item_parent                 to_item_parent,
                                 il.location                    to_loc,
                                 il.zone_node_type              to_zone_node_type,
                                 il.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.item_parent      is NULL
                             and il.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id          is NOT NULL
                             and im.item             = il.item
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             -- Look for an existing P/L timeline
                             and rfr.dept            = im.dept
                             and rfr.item            = im.item_parent
                             and rfr.diff_id         is NULL
                             and rfr.location        = il.location
                             and rfr.zone_node_type  = il.zone_node_type
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept            = im.dept
                             and ril.item            = im.item
                             and ril.loc             = il.location
                             and rownum              > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = im.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- PD/L and I/Z intersection - Generate I/L timeline:
                          -- working with PD/L and I/Z exists
                          select /*+ ORDERED */
                                 distinct 1 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 il.diff_id                     from_diff_id,
                                 rz.zone_id                     from_location,
                                 rfr.zone_node_type             from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 rz.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 rpm_merch_retail_def_expl rmrde,
                                 rpm_zone rz,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.item_parent           is NOT NULL
                             and il.zone_node_type        IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id               is NULL
                             and il.dept                  = rmrde.dept
                             and il.class                 = rmrde.class
                             and il.subclass              = rmrde.subclass
                             and rmrde.regular_zone_group = rz.zone_group_id
                             and rz.zone_id               = rzl.zone_id
                             and rzl.location             = il.location
                             -- Look for an existing timeline at the I/Z level
                             and rfr.dept                 = il.dept
                             and rfr.item                 = il.item
                             and NVL(rfr.diff_id, '-999') = NVL(il.diff_id, '-999')
                             and rfr.location             = rz.zone_id
                             and rfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept                 = il.dept
                             and ril.item                 = il.item
                             and ril.loc                  = il.location
                             and rownum                   > 0
                             -- Make sure that there is not a timeline at the I/L level already
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- working with I/Z and PD/L exists
                          select /*+ ORDERED USE_NL(ril) */
                                 distinct 1 fr_rank,
                                 il.price_event_id,
                                 im.dept,
                                 im.item_parent                 from_item,
                                 il.diff_id                     from_diff_id,
                                 il.location                    from_loc,
                                 il.zone_node_type              from_zone_node_type,
                                 im.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 im.item_parent                 to_item_parent,
                                 il.location                    to_loc,
                                 il.zone_node_type              to_zone_node_type,
                                 il.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.item_parent      is NULL
                             and il.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id          is NOT NULL
                             and im.item             = il.item
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             -- Look for an existing PD/L timeline
                             and rfr.dept            = im.dept
                             and rfr.item            = im.item_parent
                             and rfr.diff_id         is NOT NULL
                             and rfr.diff_id         = il.diff_id
                             and rfr.location        = il.location
                             and rfr.zone_node_type  = il.zone_node_type
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept            = im.dept
                             and ril.item            = im.item
                             and ril.loc             = il.location
                             and rownum              > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = im.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type))) t,
                    rpm_future_retail fr
              where t.fr_rank               = t.max_fr_rank
                and fr.dept                 = t.dept
                and fr.item                 = t.from_item
                and NVL(fr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and fr.location             = t.from_location
                and fr.zone_node_type       = t.from_zone_node_type) source
   on (    target.price_event_id         = source.price_event_id
       and target.item                   = source.item
       and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.action_date            = source.action_date)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              diff_id,
              item_parent,
              zone_node_type,
              location,
              zone_id,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              on_simple_promo_ind,
              on_complex_promo_ind,
              max_hier_level,
              cur_hier_level,
              original_fr_id,
              step_identifier)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.diff_id,
              source.item_parent,
              source.zone_node_type,
              source.location,
              source.zone_id,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.max_hier_level,
              source.cur_hier_level,
              source.future_retail_id,
              RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL_FROM_INT);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL_FROM_INT,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_TL_FROM_INTERSECTION;

--------------------------------------------------------------------------------

FUNCTION GEN_PILE_TL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                     I_bulk_cc_pe_id    IN     NUMBER,
                     I_price_event_type IN     VARCHAR2,
                     I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                     I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_BULK_CC_ACTIONS_SQL.GEN_PILE_TL';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type ' || I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id);

   -- Generate RPM_PROMO_ITEM_LOC_EXPL records for the price events being processed when there is already
   -- an existing "parent" timeline.  The new records need to be at the level of the new promotion and for
   -- the exist promotion.  Need to populate the PROMO_ITEM_LOC_EXPL_ID column from the sequence but should
   -- leave the rpile_rowid to NULL so it will be pushed back.

   merge into rpm_promo_item_loc_expl_gtt target
   using (select price_event_id,
                 to_item item,
                 to_diff_id diff_id,
                 to_item_parent item_parent,
                 dept,
                 class,
                 subclass,
                 to_location location,
                 to_zone_node_type zone_node_type,
                 to_zone_id zone_id,
                 promo_id,
                 promo_display_id,
                 promo_secondary_ind,
                 promo_comp_id,
                 comp_display_id,
                 promo_dtl_id,
                 type,
                 customer_type,
                 detail_secondary_ind,
                 detail_start_date,
                 detail_end_date,
                 detail_apply_to_code,
                 detail_change_type,
                 detail_change_amount,
                 detail_change_currency,
                 detail_change_percent,
                 detail_change_selling_uom,
                 detail_price_guide_id,
                 exception_parent_id,
                 promo_item_loc_expl_id,
                 max_hier_level,
                 to_cur_hier_level cur_hier_level,
                 timebased_dtl_ind
            from (select /*+ ORDERED */
                         t.price_event_id,
                         t.to_item,
                         t.to_diff_id,
                         t.to_item_parent,
                         ilex.dept,
                         ilex.class,
                         ilex.subclass,
                         t.to_location,
                         t.to_zone_node_type,
                         t.to_zone_id,
                         ilex.promo_id,
                         ilex.promo_display_id,
                         ilex.promo_secondary_ind,
                         ilex.promo_comp_id,
                         ilex.comp_display_id,
                         ilex.promo_dtl_id,
                         ilex.type,
                         ilex.customer_type,
                         ilex.detail_secondary_ind,
                         ilex.detail_start_date,
                         ilex.detail_end_date,
                         ilex.detail_apply_to_code,
                         ilex.detail_change_type,
                         ilex.detail_change_amount,
                         ilex.detail_change_currency,
                         ilex.detail_change_percent,
                         ilex.detail_change_selling_uom,
                         ilex.detail_price_guide_id,
                         ilex.exception_parent_id,
                         ilex.promo_item_loc_expl_id,
                         ilex.max_hier_level,
                         t.to_cur_hier_level,
                         ilex.timebased_dtl_ind,
                         t.rank,
                         MAX(rank) OVER (PARTITION BY t.price_event_id,
                                                      ilex.promo_dtl_id,
                                                      t.to_item,
                                                      t.to_diff_id,
                                                      t.to_location,
                                                      t.to_zone_node_type) max_rank
                    from (with item_locs as
                             (select /*+ MATERIALIZE ORDERED */
                                     rpi.price_event_id,
                                     rpi.item,
                                     rpl.location,
                                     rpl.zone_node_type,
                                     rpl.zone_id,
                                     rpi.diff_id,
                                     rpi.pe_merch_level,
                                     rpi.merch_level_type,
                                     im.subclass,
                                     im.class,
                                     im.dept,
                                     im.item_parent
                                from (select bulk_cc_pe_id,
                                             price_event_id,
                                             item,
                                             diff_id,
                                             pe_merch_level,
                                             merch_level_type,
                                             itemloc_id,
                                             RANK() OVER (PARTITION BY item,
                                                                       diff_id
                                                              ORDER BY NVL(txn_man_excluded_item, 0),
                                                                       pe_merch_level desc,
                                                                       price_event_id) rank
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_bulk_cc_pe_item
                                       where bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                         and price_event_id                 = VALUE(ids)
                                         and NVL(txn_man_excluded_item, 0) != 1) rpi,
                                     item_master im,
                                     rpm_bulk_cc_pe_location rpl
                               where rpi.rank           = 1
                                 and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                                 and rpl.price_event_id = rpi.price_event_id
                                 and rpl.itemloc_id     = rpi.itemloc_id
                                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and im.item            = rpi.item),
                          zone_locations as
                             (select /*+ MATERIALIZE */ rz.zone_group_id,
                                     rz.zone_id,
                                     rzl.location,
                                     rzl.loc_type
                                from rpm_zone rz,
                                     rpm_zone_location rzl
                               where rzl.zone_id = rz.zone_id),
                          merch_retail_def_expl as
                             (select /*+ MATERIALIZE */
                                     dept,
                                     class,
                                     subclass,
                                     regular_zone_group
                                from rpm_merch_retail_def_expl)
                          -- Parent Diff Zone price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                         from_item,
                                 NULL                            from_diff_id,
                                 il.location                     from_location,
                                 il.zone_node_type               from_zone_node_type,
                                 il.item                         to_item,
                                 il.diff_id                      to_diff_id,
                                 NULL                            to_item_parent,
                                 il.location                     to_location,
                                 il.zone_node_type               to_zone_node_type,
                                 NULL                            to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level
                            from item_locs il
                           where il.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and rownum             >= 1
                          union all
                          -- Parent Loc price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 NULL                              from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 NULL                              to_diff_id,
                                 NULL                              to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.location              = zl.location
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.zone_id               = il.zone_id
                             and rownum                  >= 1
                          union all
                          -- Item Zone price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                  from_item,
                                 NULL                            from_diff_id,
                                 il.location                     from_location,
                                 il.zone_node_type               from_zone_node_type,
                                 il.item                         to_item,
                                 il.diff_id                      to_diff_id,
                                 il.item_parent                  to_item_parent,
                                 il.location                     to_location,
                                 il.zone_node_type               to_zone_node_type,
                                 NULL                            to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                            from item_locs il
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and il.zone_id          is NULL
                             and rownum             >= 1
                          union all
                          -- Look at the PDZ level
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                  from_item,
                                 il.diff_id                      from_diff_id,
                                 il.location                     from_location,
                                 il.zone_node_type               from_zone_node_type,
                                 il.item                         to_item,
                                 il.diff_id                      to_diff_id,
                                 il.item_parent                  to_item_parent,
                                 il.location                     to_location,
                                 il.zone_node_type               to_zone_node_type,
                                 NULL                            to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                            from item_locs il
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and il.zone_id          is NULL
                             and rownum             >= 1
                          union all
                          -- Parent Diff Loc price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 NULL                              from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 NULL                              to_item_parent,
                                 zl.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and il.zone_id               = zl.zone_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and rownum                  >= 1
                          union all
                          -- Look at the PDZ level
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 il.diff_id                        from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 NULL                              to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and rownum                  >= 1
                          union all
                          -- Look at the PL level
                          select distinct
                                 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                        from_item,
                                 NULL                           from_diff_id,
                                 il.location                    from_location,
                                 il.zone_node_type              from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 NULL                           to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 zl.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level
                            from (select il.price_event_id,
                                         il.dept,
                                         il.item,
                                         il.location,
                                         il.zone_node_type,
                                         il.diff_id,
                                         rmrde.regular_zone_group
                                    from item_locs il,
                                         merch_retail_def_expl rmrde
                                   where il.merch_level_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                     and il.pe_merch_level       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                     and il.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                     and il.zone_id              is NULL
                                     and rmrde.dept              = il.dept
                                     and rmrde.class             = il.class
                                     and rmrde.subclass          = il.subclass) il,
                                 zone_locations zl
                           where il.regular_zone_group = zl.zone_group_id (+)
                             and il.location           = zl.location (+)
                             and rownum               >= 1
                          union all
                          -- Item Location Level Price Events
                          -- Look at all levels above and rank, then use the data with the highest rank
                          -- Look at P/Z level first and rank that data 0
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                    from_item,
                                 NULL                              from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 il.item_parent                    to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rownum                  >= 1
                          union all
                          -- Look at PD/Z level and rank that data 1
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                    from_item,
                                 il.diff_id                        from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 il.item_parent                    to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rownum                  >= 1
                          union all
                          -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                 from_item,
                                 NULL                           from_diff_id,
                                 il.location                    from_location,
                                 il.zone_node_type              from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 zl.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from (select il.price_event_id,
                                         il.dept,
                                         il.diff_id,
                                         il.item_parent,
                                         il.location,
                                         il.zone_node_type,
                                         il.item,
                                         rmrde.regular_zone_group
                                    from item_locs il,
                                         merch_retail_def_expl rmrde
                                   where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                     and il.zone_id          is NULL
                                     and rmrde.dept          = il.dept
                                     and rmrde.class         = il.class
                                     and rmrde.subclass      = il.subclass
                                     and rownum              > 0) il,
                                 zone_locations zl
                           where il.regular_zone_group = zl.zone_group_id (+)
                             and il.location           = zl.location (+)
                             and rownum               >= 1
                          union all
                          -- Look at I/Z level and rank that data 2
                          select distinct 2 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 il.diff_id                        from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 il.item_parent                    to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and il.zone_id               is NULL
                             and rownum                  >= 1
                          union all
                          -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                          select distinct 2 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                 from_item,
                                 il.diff_id                     from_diff_id,
                                 il.location                    from_location,
                                 il.zone_node_type              from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 zl.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from (select il.price_event_id,
                                         il.dept,
                                         il.item_parent,
                                         il.diff_id,
                                         il.location,
                                         il.zone_node_type,
                                         il.item,
                                         rmrde.regular_zone_group
                                    from item_locs il,
                                         merch_retail_def_expl rmrde
                                   where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                     and il.zone_id          is NULL
                                     and rmrde.dept          = il.dept
                                     and rmrde.class         = il.class
                                     and rmrde.subclass      = il.subclass) il,
                                 zone_locations zl
                           where il.regular_zone_group = zl.zone_group_id (+)
                             and il.location           = zl.location (+)
                             and rownum               >= 1) t,
                         rpm_promo_item_loc_expl ilex,
                         rpm_promo_dtl rpd,
                         rpm_promo_comp rpc
                   where ilex.dept                 = t.dept
                     and ilex.item                 = t.from_item
                     and NVL(ilex.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                     and ilex.location             = t.from_location
                     and ilex.zone_node_type       = t.from_zone_node_type
                     and rpd.promo_dtl_id          = ilex.promo_dtl_id
                     and rpc.promo_comp_id         = rpd.promo_comp_id
                     -- if dealing with PC, CL, CR, NR or LM, we don't care about the customer type - we want
                     -- to process promotions that are both customer specific and those that are not.
                     and (   I_price_event_type      IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                                                         RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                         RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET)
                     -- If dealing with a promotion (CS or not), we need to make sure that we only pull back
                     -- the appropriate data - if CS, need to get the righ customer type, if not CS, we need to
                     -- make sure that there is no customer type in the "parent" promo record
                          or (    I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE)
                              and rpc.customer_type  is NULL
                              and ilex.customer_type is NULL)
                          or (    I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
                              and rpc.customer_type  = ilex.customer_type)))
           where rank = max_rank) source
   on (    target.price_event_id         = source.price_event_id
       and target.item                   = source.item
       and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.promo_dtl_id           = source.promo_dtl_id)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              diff_id,
              item_parent,
              dept,
              class,
              subclass,
              location,
              zone_node_type,
              zone_id,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              timebased_dtl_ind,
              original_pile_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.diff_id,
              source.item_parent,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.zone_node_type,
              source.zone_id,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.timebased_dtl_ind,
              source.promo_item_loc_expl_id,
              source.max_hier_level,
              source.cur_hier_level,
              I_step_identifier);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   1, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type ' || I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GEN_PILE_TL;

--------------------------------------------------------------------------------

FUNCTION GENERATE_CSPFR_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_bulk_cc_pe_id    IN     NUMBER,
                                 I_price_event_type IN     VARCHAR2,
                                 I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.GENERATE_CSPFR_TIMELINE';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id);

   -- if dealing with PC, CL, CR, NR or LM, we don't care about the customer type - we want
   -- to process promotions that are both customer specific and those that are not.
   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    action_date,
                    customer_type,
                    dept,
                    promo_retail,
                    promo_retail_currency,
                    promo_uom,
                    complex_promo_ind,
                    max_hier_level,
                    to_cur_hier_level cur_hier_level,
                    cust_segment_promo_id
               from (select /*+ ORDERED */
                            t.price_event_id,
                            t.to_item,
                            t.to_diff_id,
                            t.to_item_parent,
                            t.to_zone_node_type,
                            t.to_location,
                            t.to_zone_id,
                            cspfr.action_date,
                            cspfr.customer_type,
                            cspfr.dept,
                            cspfr.promo_retail,
                            cspfr.promo_retail_currency,
                            cspfr.promo_uom,
                            cspfr.complex_promo_ind,
                            NULL,
                            cspfr.max_hier_level,
                            t.to_cur_hier_level,
                            cspfr.cust_segment_promo_id,
                            t.rank,
                            MAX(t.rank) OVER (PARTITION BY t.price_event_id,
                                                           cspfr.customer_type,
                                                           t.to_item,
                                                           t.to_diff_id,
                                                           t.to_location,
                                                           t.to_zone_node_type) max_rank
                       from (-- Parent Diff Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                        from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    NULL                            to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                and rpi.price_event_id   = VALUE(ids)
                                and rpl.price_event_id   = VALUE(ids)
                                and rpi.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                and rpl.price_event_id   = rpi.price_event_id
                                and rpl.itemloc_id       = rpi.itemloc_id
                                and rpl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id          is NULL
                                and im.item_parent       = rpi.item
                                and im.diff_1            = rpi.diff_id
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = rzl.location
                                and rzl.zone_id          = rpl.location
                                and rownum              >= 1
                             union all
                             -- Parent Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                   distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    NULL                              to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    NULL                              to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rzl.location             = rpl.location
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and im.item_parent           = rpi.item
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Item Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                and rpi.price_event_id   = VALUE(ids)
                                and rpl.price_event_id   = VALUE(ids)
                                and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                and rpl.price_event_id   = rpi.price_event_id
                                and rpl.itemloc_id       = rpi.itemloc_id
                                and rpl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id          is NULL
                                and im.item              = rpi.item
                                and rzl.zone_id          = rpl.location
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = rzl.location
                                and rownum              >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    rpi.diff_id                     from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                and rpi.price_event_id   = VALUE(ids)
                                and rpl.price_event_id   = VALUE(ids)
                                and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                and rpl.price_event_id   = rpi.price_event_id
                                and rpl.itemloc_id       = rpi.itemloc_id
                                and rpl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id          is NULL
                                and im.item              = rpi.item
                                and rzl.zone_id          = rpl.location
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = rzl.location
                                and rownum              >= 1
                             union all
                             -- Parent Diff Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and im.item_parent           = rpi.item
                                and im.diff_1                = rpi.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and im.item_parent           = rpi.item
                                and im.diff_1                = rpi.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at the PL level
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item                           from_item,
                                    NULL                              from_diff_id,
                                    il.location                       from_location,
                                    il.zone_node_type                 from_zone_node_type,
                                    il.item                           to_item,
                                    il.diff_id                        to_diff_id,
                                    NULL                              to_item_parent,
                                    il.location                       to_location,
                                    il.zone_node_type                 to_zone_node_type,
                                    zl.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.item,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.diff_id,
                                            rmrde.regular_zone_group
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                        and rpi.price_event_id   = VALUE(ids)
                                        and rpl.price_event_id   = VALUE(ids)
                                        and rpi.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpi.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id   = rpi.price_event_id
                                        and rpl.itemloc_id       = rpi.itemloc_id
                                        and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id          is NULL
                                        and rmrde.dept           = rpi.dept
                                        and rmrde.class          = rpi.class
                                        and rmrde.subclass       = rpi.subclass
                                        and im.item_parent       = rpi.item
                                        and im.diff_1            = rpi.diff_id
                                        and ril.dept             = im.dept
                                        and ril.item             = im.item
                                        and ril.loc              = rpl.location
                                        and rownum               > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Item Location Level Price Events
                             -- Look at all levels above and rank, then use the data with the highest rank
                             -- Look at P/Z level first and rank that data 0
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                  = rpi.item
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at PD/Z level and rank that data 1
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                  = rpi.item
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.diff_id,
                                            im.item_parent,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                        and rpi.price_event_id   = VALUE(ids)
                                        and rpl.price_event_id   = VALUE(ids)
                                        and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item              = rpi.item
                                        and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id   = rpi.price_event_id
                                        and rpl.itemloc_id       = rpi.itemloc_id
                                        and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id          is NULL
                                        and rmrde.dept           = rpi.dept
                                        and rmrde.class          = rpi.class
                                        and rmrde.subclass       = rpi.subclass
                                        and ril.dept             = im.dept
                                        and ril.item             = im.item
                                        and ril.loc              = rpl.location
                                        and rownum               > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum > 0) zl
                              where il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Look at I/Z level and rank that data 2
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    2 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and rpl.zone_id              is NULL
                                and rpi.item                 = im.item
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                              union all
                              -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                              select distinct
                                     2 rank,
                                     il.dept,
                                     il.price_event_id,
                                     il.item_parent                 from_item,
                                     il.diff_id                     from_diff_id,
                                     il.location                    from_location,
                                     il.zone_node_type              from_zone_node_type,
                                     il.item                        to_item,
                                     il.diff_id                     to_diff_id,
                                     il.item_parent                 to_item_parent,
                                     il.location                    to_location,
                                     il.zone_node_type              to_zone_node_type,
                                     zl.zone_id                     to_zone_id,
                                     RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                                from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                             rpi.price_event_id,
                                             rpi.dept,
                                             im.item_parent,
                                             rpi.diff_id,
                                             rpl.location,
                                             rpl.zone_node_type,
                                             rpi.item,
                                             rmrde.regular_zone_group
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_bulk_cc_pe_item rpi,
                                             rpm_bulk_cc_pe_location rpl,
                                             item_master im,
                                             rpm_merch_retail_def_expl rmrde,
                                             rpm_item_loc ril
                                       where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                         and rpi.price_event_id   = VALUE(ids)
                                         and rpl.price_event_id   = VALUE(ids)
                                         and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                         and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                         and im.item              = rpi.item
                                         and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                         and rpl.price_event_id   = rpi.price_event_id
                                         and rpl.itemloc_id       = rpi.itemloc_id
                                         and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                         and rpl.zone_id          is NULL
                                         and rmrde.dept           = rpi.dept
                                         and rmrde.class          = rpi.class
                                         and rmrde.subclass       = rpi.subclass
                                         and ril.dept             = im.dept
                                         and ril.item             = im.item
                                         and ril.loc              = rpl.location
                                         and rownum               > 0) il,
                                     (select rz.zone_group_id,
                                             rz.zone_id,
                                             rzl.location
                                        from rpm_zone rz,
                                             rpm_zone_location rzl
                                       where rzl.zone_id = rz.zone_id
                                         and rownum      > 0) zl
                               where il.regular_zone_group = zl.zone_group_id (+)
                                 and il.location           = zl.location (+)
                                 and rownum               >= 1) t,
                             rpm_cust_segment_promo_fr cspfr
                       where cspfr.dept                 = t.dept
                         and cspfr.item                 = t.from_item
                         and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                         and cspfr.location             = t.from_location
                         and cspfr.zone_node_type       = t.from_zone_node_type
                         and cspfr.customer_type        is NOT NULL) s
       where rank = max_rank
         and NOT EXISTS (select 1
                           from rpm_item_loc_gtt iloc
                          where iloc.price_event_id       = s.price_event_id
                            and iloc.item                 = s.to_item
                            and NVL(iloc.diff_id, '-999') = NVL(s.to_diff_id, '-999')
                            and iloc.location             = s.to_location
                            and iloc.regular_zone_group   = s.to_zone_node_type
                            and iloc.item_parent          = s.customer_type)
         and EXISTS (select 1
                       from rpm_promo_item_loc_expl_gtt ilex
                      where ilex.price_event_id       = s.price_event_id
                        and ilex.item                 = s.to_item
                        and NVL(ilex.diff_id, '-999') = NVL(s.to_diff_id, '-999')
                        and ilex.location             = s.to_location
                        and ilex.zone_node_type       = s.to_zone_node_type
                        and ilex.customer_type        = s.customer_type)) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 max_hier_level,
                 cur_hier_level,
                 original_cs_promo_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.diff_id,
                 source.item_parent,
                 source.zone_node_type,
                 source.location,
                 source.zone_id,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 source.promo_retail,
                 source.promo_retail_currency,
                 source.promo_uom,
                 source.complex_promo_ind,
                 source.max_hier_level,
                 source.cur_hier_level,
                 source.cust_segment_promo_id,
                 I_step_identifier);

   else

      -- If dealing with a promotion (CS or not), we need to make sure that we only pull back
      -- the appropriate data - if CS, need to get the right customer type, if not CS, we need to
      -- make sure that there is no customer type in the "parent" promo record
      merge into rpm_cust_segment_promo_fr_gtt target
      using (select price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    action_date,
                    customer_type,
                    dept,
                    promo_retail,
                    promo_retail_currency,
                    promo_uom,
                    complex_promo_ind,
                    max_hier_level,
                    to_cur_hier_level cur_hier_level,
                    cust_segment_promo_id
               from (select /*+ ORDERED */
                            t.price_event_id,
                            t.to_item,
                            t.to_diff_id,
                            t.to_item_parent,
                            t.to_zone_node_type,
                            t.to_location,
                            t.to_zone_id,
                            cspfr.action_date,
                            cspfr.customer_type,
                            cspfr.dept,
                            cspfr.promo_retail,
                            cspfr.promo_retail_currency,
                            cspfr.promo_uom,
                            cspfr.complex_promo_ind,
                            cspfr.max_hier_level,
                            t.to_cur_hier_level,
                            cspfr.cust_segment_promo_id,
                            t.rank,
                            MAX(t.rank) OVER (PARTITION BY t.price_event_id,
                                                           cspfr.customer_type,
                                                           t.to_item,
                                                           t.to_diff_id,
                                                           t.to_location,
                                                           t.to_zone_node_type) max_rank
                       from (-- Parent Diff Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                        from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    NULL                            to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id                        is NULL
                                and im.item_parent                     = rpi.item
                                and im.diff_1                          = rpi.diff_id
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rzl.location
                                and rzl.zone_id                        = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Parent Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    NULL                              to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rzl.location                       = rpl.location
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and im.item_parent                     = rpi.item
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Item Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id                        is NULL
                                and im.item                            = rpi.item
                                and rzl.zone_id                        = rpl.location
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rzl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    rpi.diff_id                     from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id                        is NULL
                                and im.item                            = rpi.item
                                and rzl.zone_id                        = rpl.location
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rzl.location
                                and rownum                            >= 1
                             union all
                             -- Parent Diff Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and im.item_parent                     = rpi.item
                                and im.diff_1                          = rpi.diff_id
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and im.item_parent                     = rpi.item
                                and im.diff_1                          = rpi.diff_id
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PL level
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item                        from_item,
                                    NULL                           from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    NULL                           to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    promo_excl_rank
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.item,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.diff_id,
                                            rmrde.regular_zone_group,
                                            RANK() OVER (PARTITION BY rpi.item,
                                                                      rpi.diff_id,
                                                                      rpl.zone_node_type,
                                                                      rpl.location
                                                             ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                      rpi.pe_merch_level desc,
                                                                      rpi.price_event_id) promo_excl_rank
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                        and rpi.price_event_id                 = VALUE(ids)
                                        and rpl.price_event_id                 = VALUE(ids)
                                        and NVL(rpi.txn_man_excluded_item, 0) != 1
                                        and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id                 = rpi.price_event_id
                                        and rpl.itemloc_id                     = rpi.itemloc_id
                                        and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id                        is NULL
                                        and rmrde.dept                         = rpi.dept
                                        and rmrde.class                        = rpi.class
                                        and rmrde.subclass                     = rpi.subclass
                                        and im.item_parent                     = rpi.item
                                        and im.diff_1                          = rpi.diff_id
                                        and ril.dept                           = im.dept
                                        and ril.item                           = im.item
                                        and ril.loc                            = rpl.location
                                        and rownum                             > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.promo_excl_rank    = 1
                                and il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Item Location Level Price Events
                             -- Look at all levels above and rank, then use the data with the highest rank
                             -- Look at P/Z level first and rank that data 0
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                            = rpi.item
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at PD/Z level and rank that data 1
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                              from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                            = rpi.item
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    promo_excl_rank
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.diff_id,
                                            im.item_parent,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group,
                                            RANK() OVER (PARTITION BY rpi.item,
                                                                      rpi.diff_id,
                                                                      rpl.zone_node_type,
                                                                      rpl.location
                                                             ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                      rpi.pe_merch_level desc,
                                                                      rpi.price_event_id) promo_excl_rank
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                        and rpi.price_event_id                 = VALUE(ids)
                                        and rpl.price_event_id                 = VALUE(ids)
                                        and NVL(rpi.txn_man_excluded_item, 0) != 1
                                        and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item                            = rpi.item
                                        and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id                 = rpi.price_event_id
                                        and rpl.itemloc_id                     = rpi.itemloc_id
                                        and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id                        is NULL
                                        and rmrde.dept                         = rpi.dept
                                        and rmrde.class                        = rpi.class
                                        and rmrde.subclass                     = rpi.subclass
                                        and ril.dept                           = im.dept
                                        and ril.item                           = im.item
                                        and ril.loc                            = rpl.location
                                        and rownum                             > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.promo_excl_rank    = 1
                                and il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Look at I/Z level and rank that data 2
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    2 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and rpl.zone_id                        is NULL
                                and rpi.item                           = im.item
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                             select distinct
                                    2 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    il.diff_id                     from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    promo_excl_rank
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            im.item_parent,
                                            rpi.diff_id,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group,
                                            RANK() OVER (PARTITION BY rpi.item,
                                                                      rpi.diff_id,
                                                                      rpl.zone_node_type,
                                                                      rpl.location
                                                             ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                      rpi.pe_merch_level desc,
                                                                      rpi.price_event_id) promo_excl_rank
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                        and rpi.price_event_id                 = VALUE(ids)
                                        and rpl.price_event_id                 = VALUE(ids)
                                        and NVL(rpi.txn_man_excluded_item, 0) != 1
                                        and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item                            = rpi.item
                                        and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id                 = rpi.price_event_id
                                        and rpl.itemloc_id                     = rpi.itemloc_id
                                        and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id                        is NULL
                                        and rmrde.dept                         = rpi.dept
                                        and rmrde.class                        = rpi.class
                                        and rmrde.subclass                     = rpi.subclass
                                        and ril.dept                           = im.dept
                                        and ril.item                           = im.item
                                        and ril.loc                            = rpl.location
                                        and rownum                             > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.promo_excl_rank    = 1
                                and il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1) t,
                            rpm_cust_segment_promo_fr cspfr,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc
                      where t.promo_excl_rank          = 1
                        and cspfr.dept                 = t.dept
                        and cspfr.item                 = t.from_item
                        and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                        and cspfr.location             = t.from_location
                        and cspfr.zone_node_type       = t.from_zone_node_type
                        and cspfr.customer_type        is NOT NULL
                        and rpd.promo_dtl_id           = t.price_event_id
                        and rpc.promo_comp_id          = rpd.promo_comp_id
                        and rpc.customer_type          = cspfr.customer_type)
       where rank = max_rank) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 max_hier_level,
                 cur_hier_level,
                 original_cs_promo_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.diff_id,
                 source.item_parent,
                 source.zone_node_type,
                 source.location,
                 source.zone_id,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 source.promo_retail,
                 source.promo_retail_currency,
                 source.promo_uom,
                 source.complex_promo_ind,
                 source.max_hier_level,
                 source.cur_hier_level,
                 source.cust_segment_promo_id,
                 I_step_identifier);

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type ' || I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_CSPFR_TIMELINE;

--------------------------------------------------------------------------------

FUNCTION GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_bulk_cc_pe_id    IN     NUMBER,
                               I_price_event_type IN     VARCHAR2,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.GEN_CSPFR_TL_FROM_INT';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id);

   -- if dealing with PC, CL, CR, NR or LM, we don't care about the customer type - we want
   -- to process promotions that are both customer specific and those that are not.
   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select t.price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    cspfr.action_date,
                    cspfr.customer_type,
                    cspfr.dept,
                    cspfr.promo_retail,
                    cspfr.promo_retail_currency,
                    cspfr.promo_uom,
                    cspfr.complex_promo_ind,
                    cspfr.max_hier_level,
                    t.to_cur_hier_level cur_hier_level,
                    cspfr.cust_segment_promo_id
               from (select price_event_id,
                            dept,
                            from_item,
                            from_diff_id,
                            from_location,
                            from_zone_node_type,
                            to_item,
                            to_diff_id,
                            to_item_parent,
                            to_location,
                            to_zone_node_type,
                            to_zone_id,
                            to_cur_hier_level,
                            cspfr_rank,
                            customer_type,
                            MAX(cspfr_rank) OVER (PARTITION BY price_event_id,
                                                               customer_type,
                                                               to_item,
                                                               to_diff_id,
                                                               to_location,
                                                               to_zone_node_type) max_cspfr_rank
                       from (-- PD/Z and P/L intersection - Generate PD/L timeline:
                             -- working with PD/Z and P/L timeline exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    NULL                           from_diff_id,
                                    rzl.location                   from_location,
                                    rzl.loc_type                   from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    rzl.location                   to_location,
                                    rzl.loc_type                   to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.pe_merch_level         = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.merch_level_type       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent            is NULL
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and l.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and l.zone_id                is NULL
                                and rzl.zone_id              = l.location
                                and im.item_parent           = i.item
                                and im.diff_1                = i.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = rzl.location
                                                   and gtt.zone_node_type       = rzl.loc_type)
                                -- look for an existing timeline at the P/L level
                                and cspfr.dept               = i.dept
                                and cspfr.item               = i.item
                                and cspfr.diff_id            is NULL
                                and cspfr.location           = rzl.location
                                and cspfr.zone_node_type     = rzl.loc_type
                                and rownum                   > 0
                             union all
                             -- working with P/L and PD/Z timeline exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rzl.zone_id                    from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.merch_level_type       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent            is NULL
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NULL
                                and i.dept                   = rmrde.dept
                                and i.class                  = rmrde.class
                                and i.subclass               = rmrde.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = l.location
                                and im.item_parent           = i.item
                                and im.diff_1                = i.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/Z timeline
                                and cspfr.dept               = i.dept
                                and cspfr.item               = i.item
                                and cspfr.diff_id            is NOT NULL
                                and cspfr.diff_id            = i.diff_id
                                and cspfr.location           = rzl.zone_id
                                and cspfr.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rownum                   > 0
                             union all
                             -- P/L and I/Z intersection - Generate I/L timeline:
                             -- working with P/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.merch_level_type       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent            is NOT NULL
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NULL
                                and i.dept                   = rmrde.dept
                                and i.class                  = rmrde.class
                                and i.subclass               = rmrde.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                 = i.dept
                                and cspfr.item                 = i.item
                                and NVL(cspfr.diff_id, '-999') = NVL(i.diff_id, '-999')
                                and cspfr.location             = rz.zone_id
                                and cspfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                    = i.dept
                                and il.item                    = i.item
                                and il.loc                     = l.location
                                and rownum                     > 0
                             union all
                             -- working with I/Z and P/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and l.price_event_id         = VALUE(ids)
                                and i.price_event_id         = VALUE(ids)
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and i.merch_level_type       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent            is NULL
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NOT NULL
                                and im.item                  = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing P/L timeline
                                and cspfr.dept               = im.dept
                                and cspfr.item               = im.item_parent
                                and cspfr.diff_id            is NULL
                                and cspfr.location           = l.location
                                and cspfr.zone_node_type     = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                  = im.dept
                                and il.item                  = im.item
                                and il.loc                   = l.location
                                and rownum                   > 0
                             union all
                             -- PD/L and I/Z intersection - Generate I/L timeline:
                             -- working with PD/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.merch_level_type       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent            is NOT NULL
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NULL
                                and i.dept                   = rmrde.dept
                                and i.class                  = rmrde.class
                                and i.subclass               = rmrde.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = l.location
                                -- Make sure that there is not a timeline at the I/L level already
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                 = i.dept
                                and cspfr.item                 = i.item
                                and NVL(cspfr.diff_id, '-999') = NVL(i.diff_id, '-999')
                                and cspfr.location             = rz.zone_id
                                and cspfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                    = i.dept
                                and il.item                    = i.item
                                and il.loc                     = l.location
                                and rownum                     > 0
                             union all
                             -- working with I/Z and PD/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    i.diff_id                      from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and i.merch_level_type       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent            is NULL
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NOT NULL
                                and im.item                  = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/L timeline
                                and cspfr.dept               = im.dept
                                and cspfr.item               = im.item_parent
                                and cspfr.diff_id            is NOT NULL
                                and cspfr.diff_id            = i.diff_id
                                and cspfr.location           = l.location
                                and cspfr.zone_node_type     = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                  = im.dept
                                and il.item                  = im.item
                                and il.loc                   = l.location
                                and rownum                   > 0)) t,
                    rpm_cust_segment_promo_fr cspfr
              where t.cspfr_rank               = t.max_cspfr_rank
                and cspfr.dept                 = t.dept
                and cspfr.item                 = t.from_item
                and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and cspfr.location             = t.from_location
                and cspfr.zone_node_type       = t.from_zone_node_type
                and cspfr.customer_type        = t.customer_type) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 max_hier_level,
                 cur_hier_level,
                 original_cs_promo_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.diff_id,
                 source.item_parent,
                 source.zone_node_type,
                 source.location,
                 source.zone_id,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 source.promo_retail,
                 source.promo_retail_currency,
                 source.promo_uom,
                 source.complex_promo_ind,
                 source.max_hier_level,
                 source.cur_hier_level,
                 source.cust_segment_promo_id,
                 I_step_identifier);

   else

      -- If dealing with a promotion (CS or not), we need to make sure that we only pull back
      -- the appropriate data - if CS, need to get the right customer type, if not CS, we need to
      -- make sure that there is no customer type in the "parent" promo record
      merge into rpm_cust_segment_promo_fr_gtt target
      using (select t.price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    cspfr.action_date,
                    cspfr.customer_type,
                    cspfr.dept,
                    cspfr.promo_retail,
                    cspfr.promo_retail_currency,
                    cspfr.promo_uom,
                    cspfr.complex_promo_ind,
                    cspfr.max_hier_level,
                    t.to_cur_hier_level cur_hier_level,
                    cspfr.cust_segment_promo_id
               from (select price_event_id,
                            dept,
                            from_item,
                            from_diff_id,
                            from_location,
                            from_zone_node_type,
                            to_item,
                            to_diff_id,
                            to_item_parent,
                            to_location,
                            to_zone_node_type,
                            to_zone_id,
                            to_cur_hier_level,
                            cspfr_rank,
                            customer_type,
                            promo_excl_rank,
                            MAX(cspfr_rank) OVER (PARTITION BY price_event_id,
                                                               customer_type,
                                                               to_item,
                                                               to_diff_id,
                                                               to_location,
                                                               to_zone_node_type) max_cspfr_rank
                       from (-- PD/Z and P/L intersection - Generate PD/L timeline:
                             -- working with PD/Z and P/L timeline exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    NULL                           from_diff_id,
                                    rzl.location                   from_location,
                                    rzl.loc_type                   from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    rzl.location                   to_location,
                                    rzl.loc_type                   to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and l.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and l.zone_id                        is NULL
                                and rzl.zone_id                      = l.location
                                and im.item_parent                   = i.item
                                and im.diff_1                        = i.diff_id
                                and ril.dept                         = im.dept
                                and ril.item                         = im.item
                                and ril.loc                          = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = rzl.location
                                                   and gtt.zone_node_type       = rzl.loc_type)
                                -- look for an existing timeline at the P/L level
                                and cspfr.dept                       = i.dept
                                and cspfr.item                       = i.item
                                and cspfr.diff_id                    is NULL
                                and cspfr.location                   = rzl.location
                                and cspfr.zone_node_type             = rzl.loc_type
                                and rownum                           > 0
                             union all
                             -- working with P/L and PD/Z timeline exists
                            select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rzl.zone_id                    from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NULL
                                and i.dept                           = rmrde.dept
                                and i.class                          = rmrde.class
                                and i.subclass                       = rmrde.subclass
                                and rmrde.regular_zone_group         = rz.zone_group_id
                                and rz.zone_id                       = rzl.zone_id
                                and rzl.location                     = l.location
                                and im.item_parent                   = i.item
                                and im.diff_1                        = i.diff_id
                                and ril.dept                         = im.dept
                                and ril.item                         = im.item
                                and ril.loc                          = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/Z timeline
                                and cspfr.dept                       = i.dept
                                and cspfr.item                       = i.item
                                and cspfr.diff_id                    is NOT NULL
                                and cspfr.diff_id                    = i.diff_id
                                and cspfr.location                   = rzl.zone_id
                                and cspfr.zone_node_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rownum                           > 0
                             union all
                             -- P/L and I/Z intersection - Generate I/L timeline:
                             -- working with P/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                    = I_bulk_cc_pe_id
                                and i.price_event_id                   = VALUE(ids)
                                and l.price_event_id                   = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0)   != 1
                                and i.merch_level_type                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent                      is NOT NULL
                                and i.bulk_cc_pe_id                    = l.bulk_cc_pe_id
                                and i.itemloc_id                       = l.itemloc_id
                                and i.price_event_id                   = l.price_event_id
                                and l.zone_node_type                   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                          is NULL
                                and i.dept                             = rmrde.dept
                                and i.class                            = rmrde.class
                                and i.subclass                         = rmrde.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                         = i.dept
                                and cspfr.item                         = i.item
                                and NVL(cspfr.diff_id, '-999')         = NVL(i.diff_id, '-999')
                                and cspfr.location                     = rz.zone_id
                                and cspfr.zone_node_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L time        lines are generated when the there is an I/L relationship
                                and il.dept                            = i.dept
                                and il.item                            = i.item
                                and il.loc                             = l.location
                                and rownum                             > 0
                             union all
                             -- working with I/Z and P/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and i.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NOT NULL
                                and im.item                          = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing P/L timeline
                                and cspfr.dept                       = im.dept
                                and cspfr.item                       = im.item_parent
                                and cspfr.diff_id                    is NULL
                                and cspfr.location                   = l.location
                                and cspfr.zone_node_type             = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                          = im.dept
                                and il.item                          = im.item
                                and il.loc                           = l.location
                                and rownum                           > 0
                             union all
                             -- PD/L and I/Z intersection - Generate I/L timeline:
                             -- working with PD/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                     distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                    = I_bulk_cc_pe_id
                                and i.price_event_id                   = VALUE(ids)
                                and l.price_event_id                   = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0)   != 1
                                and i.merch_level_type                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent                      is NOT NULL
                                and i.bulk_cc_pe_id                    = l.bulk_cc_pe_id
                                and i.itemloc_id                       = l.itemloc_id
                                and i.price_event_id                   = l.price_event_id
                                and l.zone_node_type                   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                          is NULL
                                and i.dept                             = rmrde.dept
                                and i.class                            = rmrde.class
                                and i.subclass                         = rmrde.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = l.location
                                -- Make sure that there is not a timeline at the I/L level already
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                         = i.dept
                                and cspfr.item                         = i.item
                                and NVL(cspfr.diff_id, '-999')         = NVL(i.diff_id, '-999')
                                and cspfr.location                     = rz.zone_id
                                and cspfr.zone_node_type               = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                            = i.dept
                                and il.item                            = i.item
                                and il.loc                             = l.location
                                and rownum                             > 0
                             union all
                             -- working with I/Z and PD/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    i.diff_id                      from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and i.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NOT NULL
                                and im.item                          = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/L timeline
                                and cspfr.dept                       = im.dept
                                and cspfr.item                       = im.item_parent
                                and cspfr.diff_id                    is NOT NULL
                                and cspfr.diff_id                    = i.diff_id
                                and cspfr.location                   = l.location
                                and cspfr.zone_node_type             = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                          = im.dept
                                and il.item                          = im.item
                                and il.loc                           = l.location
                                and rownum                           > 0)) t,
                    rpm_cust_segment_promo_fr cspfr,
                    rpm_promo_dtl rpd,
                    rpm_promo_comp rpc
              where t.promo_excl_rank          = 1
                and t.cspfr_rank               = t.max_cspfr_rank
                and cspfr.customer_type        = t.customer_type
                and cspfr.dept                 = t.dept
                and cspfr.item                 = t.from_item
                and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and cspfr.location             = t.from_location
                and cspfr.zone_node_type       = t.from_zone_node_type
                and rpd.promo_dtl_id           = t.price_event_id
                and rpc.promo_comp_id          = rpd.promo_comp_id
                and rpc.customer_type          = cspfr.customer_type) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 max_hier_level,
                 cur_hier_level,
                 original_cs_promo_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.diff_id,
                 source.item_parent,
                 source.zone_node_type,
                 source.location,
                 source.zone_id,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 source.promo_retail,
                 source.promo_retail_currency,
                 source.promo_uom,
                 source.complex_promo_ind,
                 source.max_hier_level,
                 source.cur_hier_level,
                 source.cust_segment_promo_id,
                 I_step_identifier);

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type ' || I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;
END GEN_CSPFR_TL_FROM_INT;
--------------------------------------------------------------------------------

FUNCTION GET_PE_CC_ERR_ITEM_LOC(O_error_msg           OUT  VARCHAR2,
                                O_price_event_type    OUT  RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                                O_item_locs           OUT  OBJ_NUM_NUM_STR_TBL,
                                I_cc_error_tbl     IN      CONFLICT_CHECK_ERROR_TBL,
                                I_bulk_cc_pe_id    IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE)
RETURN NUMBER IS

   L_program  VARCHAR2(50) := 'RPM_BULK_CC_ACTIONS_SQL.GET_PE_CC_ERR_ITEM_LOC';

   cursor C_PRICE_EVENT_TYPE is
      select price_event_type
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_PE_CC_ERR_ITEM_LOCS is
      select OBJ_NUM_NUM_STR_REC(itl.location,
                                 itl.loc_type,
                                 itl.item)
        from (select /*+ CARDINALITY(ccet 10) */
                     rfr.location,
                     rfr.zone_node_type loc_type,
                     rfr.item
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
                     rpm_cce_fr_itemloc_gtt rfr
               where ccet.cs_promo_fr_id is NULL
                 and ccet.future_retail_id = rfr.future_retail_id
              union all
              select /*+ CARDINALITY(ccet 10) */
                     cfr.location,
                     cfr.zone_node_type loc_type,
                     cfr.item
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
                     rpm_cce_fr_itemloc_gtt cfr
               where ccet.cs_promo_fr_id = cfr.cust_segment_promo_id) itl;

BEGIN

   open C_PRICE_EVENT_TYPE;
   fetch C_PRICE_EVENT_TYPE into O_price_event_type;
   close C_PRICE_EVENT_TYPE;

   open C_PE_CC_ERR_ITEM_LOCS;
   fetch C_PE_CC_ERR_ITEM_LOCS BULK COLLECT into O_item_locs;
   close C_PE_CC_ERR_ITEM_LOCS;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END GET_PE_CC_ERR_ITEM_LOC;
--------------------------------------------------------

END RPM_BULK_CC_ACTIONS_SQL;
/
