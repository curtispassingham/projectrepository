CREATE OR REPLACE PACKAGE BODY RPM_CC_AMOUNT_OFF_UOM AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_error_key    VARCHAR2(255) := NULL;
   L_error_type   VARCHAR2(255) := NULL;
   L_program      VARCHAR2(61)  := 'RPM_CC_AMOUNT_OFF_UOM.VALIDATE';

   L_future_retail_id  RPM_FUTURE_RETAIL_GTT.FUTURE_RETAIL_ID%TYPE := NULL;
   L_error_rec         CONFLICT_CHECK_ERROR_REC                    := NULL;
   L_error_tbl         CONFLICT_CHECK_ERROR_TBL                    := CONFLICT_CHECK_ERROR_TBL();
   
   L_gtt_count         NUMBER  := 0;

   cursor C_CHECK is
      select gtt.price_event_id,
             gtt.future_retail_id
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id       NOT IN (select ccet.price_event_id
                                                from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet)
         and (   (    price_change_id is NOT NULL 
                  and pc_change_type  = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE 
                  and selling_uom     != pc_change_selling_uom)
              or (    clearance_id      is NOT NULL 
                  and clear_change_type = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE 
                  and clear_uom         != clear_change_selling_uom))
      union 
      select gtt.price_event_id,
             gtt.future_retail_id
        from rpm_future_retail_gtt gtt,
             rpm_promo_item_loc_expl_gtt ilexgtt
       where ilexgtt.price_event_id                        = gtt.price_event_id
         and ilexgtt.item                                  = gtt.item
         and ilexgtt.location                              = gtt.location
         and ilexgtt.detail_start_date                     <= gtt.action_date
         and NVL(ilexgtt.detail_end_date, gtt.action_date) >= gtt.action_date
         and ilexgtt.detail_change_type                    = RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE 
         and ilexgtt.type                                  = RPM_CONSTANTS.SIMPLE_CODE 
         and gtt.simple_promo_uom                          != ilexgtt.detail_change_selling_uom
         and NOT EXISTS (select 1
                           from rpm_promo_item_loc_expl_gtt ilex
                          where ilex.price_event_id                        = gtt.price_event_id
                            and ilex.item                                  = gtt.item
                            and ilex.location                              = gtt.location
                            and ilex.detail_start_date                     <= gtt.action_date
                            and NVL(ilex.detail_end_date, gtt.action_date) >= gtt.action_date
                            and (   (    ilex.customer_type      is NULL
                                     and ilexgtt.customer_type   is NULL)
                                 or ilex.customer_type           = ilexgtt.customer_type)
                            and ilex.detail_change_type                    = RPM_CONSTANTS.RETAIL_EXCLUDE
                            and (   (    ilex.exception_parent_id is NOT NULL 
                                     and ilexgtt.promo_id         = ilex.promo_id
                                     and ilexgtt.promo_comp_id    = ilex.promo_comp_id) 
                                 or (   ilex.exception_parent_id is NULL 
                                     and ilexgtt.promo_id        = ilex.promo_id)));
BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,                             
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,                             
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE) then
      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999, NULL, NULL, NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      for rec in C_CHECK loop
         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'event_causes_fixed_price_uom_amount_off_uom_conflict');
         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;
         end if;

      end loop;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL, 
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 to_char(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

