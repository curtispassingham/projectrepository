CREATE OR REPLACE PACKAGE RPM_PROM_COMP_AGG_SQL AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION GET_VALUES(O_error_message         OUT VARCHAR2,
                    I_pc_processing_days IN     NUMBER,
                    I_user_id            IN     VARCHAR2,
                    --
                    I_pca_tbl            IN OUT OBJ_RPM_PROM_COMP_AGG_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------

END RPM_PROM_COMP_AGG_SQL;
/

