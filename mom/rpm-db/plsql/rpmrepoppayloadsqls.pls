CREATE OR REPLACE PACKAGE RPM_REPOP_PAYLOAD_DATA AS
----------------------------------------------------------------------
FUNCTION REPOPULATE_PAYLOAD_TABLES (O_error_msg      OUT VARCHAR2,
                                    I_location    IN     NUMBER,
                                    I_action_date IN     DATE,
                                    I_thread      IN     NUMBER,
                                    I_luw_cnt     IN     NUMBER)
RETURN NUMBER;
----------------------------------------------------------------------

FUNCTION VALIDATE_GET_THREADS (O_error_msg      OUT VARCHAR2,
                               O_threads        OUT NUMBER,
                               O_luw_cnt        OUT NUMBER,
                               I_location    IN     NUMBER,
                               I_action_date IN     DATE)
RETURN NUMBER;
----------------------------------------------------------------------
END RPM_REPOP_PAYLOAD_DATA;
/