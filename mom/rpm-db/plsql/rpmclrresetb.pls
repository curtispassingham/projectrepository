CREATE OR REPLACE PACKAGE BODY RPM_CLR_RESET AS

--------------------------------------------------------------------------------

FUNCTION VALIDATE(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(25) := 'RPM_CLR_RESET.VALIDATE';

   L_vdate DATE := GET_VDATE;

   L_error_rec  CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl  CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LO_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select CONFLICT_CHECK_ERROR_REC(t.price_event_id,
                                      t.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'future_retail_price_change_rule24')
        from (select /*+ CARDINALITY (ccet 10) */
                     rfrg.price_event_id,
                     rfrg.future_retail_id
                from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet,
                     rpm_future_retail_gtt rfrg,
                     rpm_clearance_gtt rc
               where rfrg.price_event_id     != ccet.price_event_id
                 and rc.price_event_id       = rfrg.price_event_id
                 and rc.reset_ind            = 0
                 and rfrg.clear_start_ind    = RPM_CONSTANTS.END_IND
                 and rfrg.clearance_id       is NOT NULL
                 and TRUNC(rfrg.action_date) between L_vdate and rc.effective_date
              union all
              select /*+ CARDINALITY (ccet 10) */
                     rfrg.price_event_id,
                     rfrg.future_retail_id
                from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet,
                     rpm_future_retail_gtt rfrg,
                     rpm_clearance_gtt rc
               where rfrg.price_event_id     != ccet.price_event_id
                 and rc.price_event_id       = rfrg.price_event_id
                 and rc.reset_ind            = 0
                 and rfrg.clear_start_ind    = RPM_CONSTANTS.START_IND
                 and rfrg.clearance_id       is NOT NULL
                 and TRUNC(rfrg.action_date) >= L_vdate
                 and rc.reset_date           between L_vdate and rfrg.action_date) t;

BEGIN

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then

      L_error_tbl := IO_cc_error_tbl;

   else

      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);
      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

   end if;

   open C_CHECK;
   fetch C_CHECK BULK COLLECT into LO_error_tbl;
   close C_CHECK;

   if LO_error_tbl is NOT NULL and
      LO_error_tbl.COUNT > 0 then

      if IO_cc_error_tbl is NULL or
         IO_cc_error_tbl.COUNT = 0 then

         IO_cc_error_tbl := LO_error_tbl;

      else

         for i IN 1..LO_error_tbl.COUNT loop
            IO_cc_error_tbl.EXTEND;
            IO_cc_error_tbl(IO_cc_error_tbl.COUNT) := LO_error_tbl(i);
         end loop;

      end if;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE;
--------------------------------------------------------------------------------
END RPM_CLR_RESET;
/