CREATE OR REPLACE PACKAGE RPM_GENERATE_WS_PRICE_EVENTS AS
-----------------------------------------------------------------------------

-- Compete Types
COMP_MATCH       CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 0;
COMP_PRICE_ABOVE CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 1;
COMP_PRICE_BELOW CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 2;

WS_STATE_INT_PENDING         CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 0;
WS_STATE_INT_NEW             CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 1;
WS_STATE_INT_UPDATED         CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 2;
WS_STATE_INT_IN_PROGRESS     CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 3;
WS_STATE_INT_SUBMITTED       CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 4;
WS_STATE_INT_SUBMIT_REJECTED CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 5;
WS_STATE_INT_APPROVED        CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 6;
WS_STATE_INT_DELETE_PENDING  CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 7;
WS_STATE_INT_DELETE_REJECTED CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 8;
WS_STATE_INT_DELETE_APPROVED CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 9;
WS_STATE_INT_CONFLICT_CHECK  CONSTANT RPM_WORKSHEET_DATA.STATE%TYPE := 10;

UNDECIDED CONSTANT RPM_WORKSHEET_DATA.ACTION_FLAG%TYPE := 0;
TAKE      CONSTANT RPM_WORKSHEET_DATA.ACTION_FLAG%TYPE := 1;
DONT_TAKE CONSTANT RPM_WORKSHEET_DATA.ACTION_FLAG%TYPE := 2;

-----------------------------------------------------------------------------

FUNCTION GET_WORKSHEETS_TO_APPROVE(O_error_msg            OUT VARCHAR2,
                                   O_worksheet_status_ids OUT OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION GENERATE(O_error_msg                 OUT VARCHAR2,
                  O_pc_bulk_cc_pe_id          OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                  O_clr_bulk_cc_pe_id         OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                  I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                  I_cc_request_group_id    IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE,
                  I_worksheet_status_ids   IN     OBJ_NUMERIC_ID_TABLE,
                  I_action                 IN     VARCHAR2,
                  I_user_id                IN     RPM_PRICE_CHANGE.CREATE_ID%TYPE,
                  I_auto_approve_ind       IN     NUMBER)
RETURN NUMBER;

FUNCTION BEGIN_THREAD_PROCESS(O_error_msg               OUT VARCHAR2,
                              I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                              I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                              I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE)
RETURN NUMBER;

FUNCTION END_THREAD_PROCESS(O_error_msg               OUT VARCHAR2,
                            I_cc_error_tbl         IN     CONFLICT_CHECK_ERROR_TBL,
                            I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                            I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                            I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                            I_auto_approve_ind     IN     NUMBER)
RETURN NUMBER;

FUNCTION CLEAN_UP_WORKSHEET_BULK_CC(O_error_msg              OUT VARCHAR2,
                                    O_group_complete_ind     OUT NUMBER,
                                    I_bulk_cc_pe_id      IN      RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE)
RETURN NUMBER;

FUNCTION CLEAN_UP_WKSHT_BULK_CC_BATCH(O_error_msg              OUT VARCHAR2,
                                      O_group_complete_ind     OUT NUMBER,
                                      O_conflicts_exist        OUT NUMBER,
                                      O_errors_exist           OUT NUMBER,
                                      I_cc_request_group_id IN     RPM_BULK_CC_PE.CC_REQUEST_GROUP_ID%TYPE)
RETURN NUMBER;

FUNCTION FINISH_WORKSHEETS(O_error_msg               OUT VARCHAR2,
                           I_worksheet_status_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN BOOLEAN;

END RPM_GENERATE_WS_PRICE_EVENTS;
/
