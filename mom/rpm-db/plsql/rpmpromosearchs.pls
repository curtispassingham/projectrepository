CREATE OR REPLACE PACKAGE RPM_PROMO_SEARCH_SQL AS
-------------------------------------------------------------------------------
PROCEDURE SEARCH(O_return_code        OUT  NUMBER,
                 O_error_msg          OUT  VARCHAR2,
                 O_search_tbl         OUT  OBJ_PROMOTION_TBL,
                 O_ps_count           OUT  NUMBER,
                 I_criteria_tbl    IN      OBJ_PROMOTION_SEARCH_TBL,
                 I_exclude_no_comp IN      NUMBER default 0);
-------------------------------------------------------------------------------
PROCEDURE REFRESH_STATUS(O_return_code    OUT NUMBER,
                         O_error_msg      OUT VARCHAR2,
                         O_search_tbl     OUT OBJ_PROMO_DTL_REFRESH_TBL,
                         I_criteria    IN     OBJ_VARCHAR_ID_TABLE);
-------------------------------------------------------------------------------
END RPM_PROMO_SEARCH_SQL;
/