CREATE OR REPLACE PACKAGE BODY RPM_EXT_CAND_RULE_SQL AS

----------------------------------------------------------------------------------------
---LOCAL PROTOTYPES 
----------------------------------------------------------------------------------------
---
FUNCTION EXECUTE_CONDITION(O_error_msg         OUT  VARCHAR2,
                           I_field          IN      rpm_condition.field%TYPE,
                           I_condition_id   IN      rpm_condition.condition_id%TYPE,
                           I_operator       IN      rpm_condition.operator%TYPE,
                           I_value          IN      rpm_condition.string_value%TYPE)
RETURN BOOLEAN;
---
FUNCTION PURGE_MULTI_SELECT_NOTEQUALS(O_error_msg         OUT VARCHAR2,
                                      I_dept          IN      rpm_strategy.dept%type)
RETURN BOOLEAN;
---
FUNCTION LINK_ITEM_LOC_CAND_RULES(O_error_msg        OUT VARCHAR2,
                                  I_strategy_type IN     VARCHAR2)
RETURN BOOLEAN;
---
----------------------------------------------------------------------------------------
---PUBLIC MODULES
----------------------------------------------------------------------------------------


FUNCTION CAND_RULES_DEL_EXCL (O_error_msg      OUT  VARCHAR2,
                              I_strategy_id IN      rpm_strategy.strategy_id%TYPE)
RETURN BOOLEAN IS

   L_vdate DATE := get_vdate;

BEGIN 

   insert into rpm_merch_extract_deletions
            (strategy_id,
             delete_date,
             item,
             location,
             reason_code,
             candidate_rule_id)
      select I_strategy_id,
             L_vdate,
             cr.item,
             cr.location,
             RPM_EXT_SQL.CAND_RULE_EXCLUSION,
             cr.candidate_rule_id
        from rpm_me_agg_cand_rule_link_gtt cr
       where cr.type              = RPM_EXT_SQL.EXCLUSION_TYPE;

   delete from rpm_me_consolidate_itemloc_gtt con
    where (con.worksheet_item_data_id, con.worksheet_item_loc_data_id)
       in (select cr.worksheet_item_data_id,
                  cr.worksheet_item_loc_data_id	   
             from rpm_me_agg_cand_rule_link_gtt cr
            where cr.type = RPM_EXT_SQL.EXCLUSION_TYPE);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_CAND_RULE_SQL',
                                        to_char(SQLCODE));
      return FALSE;
END CAND_RULES_DEL_EXCL;

----------------------------------------------------------------------------------------

FUNCTION CAND_RULES_LINK_INCL (O_error_msg         OUT  VARCHAR2,
                               I_strategy_id    IN      rpm_strategy.strategy_id%TYPE,
                               O_constraint_ind    OUT  rpm_worksheet_status.constraint_ind%TYPE)
RETURN BOOLEAN IS

   L_constraint_ind    rpm_worksheet_status.constraint_ind%TYPE := 0;

   cursor C_CONSTAINT_ID is
       select 1
         from rpm_me_il_cand_rule_link_gtt 
        where type   = RPM_EXT_SQL.INCLUSION_TYPE
          and rownum < 2;

BEGIN 

   open C_CONSTAINT_ID;
   fetch C_CONSTAINT_ID into L_constraint_ind;
   close C_CONSTAINT_ID;

   if L_constraint_ind != 0 then
      O_constraint_ind := 1;
   else
      O_constraint_ind := 0;
   end if;

   insert into rpm_worksheet_cand_link
            (worksheet_item_data_id,
             worksheet_item_loc_data_id,
             candidate_rule_id,
             lock_version)
      select cr.worksheet_item_data_id,
             cr.worksheet_item_loc_data_id,
             cr.candidate_rule_id,
             null
        from rpm_me_il_cand_rule_link_gtt cr
       where cr.type = RPM_EXT_SQL.INCLUSION_TYPE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_CAND_RULE_SQL.CAND_RULES_LINK_INCL',
                                        to_char(SQLCODE));
      return FALSE;
END CAND_RULES_LINK_INCL;

----------------------------------------------------------------------------------------

/******************************************************************************************
 *
 *   1) find all conditions that need to be run on this worksheet.
 *      i) for each condition evaluate it against the item/locs and put those
 *         that meet the condition in RPM_ME_IL_CAND_COND_GTT.
 *   2) for conditions with a OR operator, each value must be meet for the condition
 *      to be meet.  purge those that did not meet each value from RPM_ME_IL_CAND_COND_GTT.
 *   3) determine what rules are meet per item/loc
 *      i)   count the number of condition under the rule
 *      ii)  count the number of conditions the item/loc meet under the rule
 *      iii) for these item/loc/rule combinations where the conditions meet is >=
 *           the number of conditions for the rule populate RPM_ME_IL_CAND_RULE_LINK_GTT
 *
 *****************************************************************************************/

FUNCTION EXECUTE_CAND_RULES (O_error_msg        OUT  VARCHAR2,
                             I_strategy_type IN      VARCHAR2,
                             I_dept          IN      rpm_strategy.dept%type,
                             I_exclusion_ind IN      rpm_calendar_period.exclusion_ind%TYPE,
                             I_exception_ind IN      rpm_calendar_period.exception_ind%TYPE)
RETURN BOOLEAN IS

   cursor C_GET_WORK is
     --string value
      select rc.candidate_rule_id,
             rc.condition_id,
             rc.operator, 
             rc.string_value value,
             rc.field
        from (select top_level.condition_id,
                     rcr.candidate_rule_id,
                     top_level.child_condition_id,
                     top_level.operator,
                     top_level.field,
                     top_level.variable_id,
                     top_level.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
             union
              select one_below.condition_id,
                     rcr.candidate_rule_id,
                     one_below.child_condition_id,
                     one_below.operator,
                     one_below.field,
                     one_below.variable_id,
                     one_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.candidate_rule_id    is null
             union
              select two_below.condition_id,
                     rcr.candidate_rule_id,
                     two_below.child_condition_id,
                     two_below.operator,
                     two_below.field,
                     two_below.variable_id,
                     two_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below,
                     rpm_condition two_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.child_condition_id   = two_below.condition_id
                 and one_below.candidate_rule_id    is null
                 and two_below.candidate_rule_id    is null
             union
              select three_below.condition_id,
                     rcr.candidate_rule_id,
                     three_below.child_condition_id,
                     three_below.operator,
                     three_below.field,
                     three_below.variable_id,
                     three_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below,
                     rpm_condition two_below,
                     rpm_condition three_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.child_condition_id   = two_below.condition_id
                 and two_below.child_condition_id   = three_below.condition_id
                 and one_below.candidate_rule_id    is null
                 and two_below.candidate_rule_id    is null
                 and three_below.candidate_rule_id  is null) rc,
             rpm_candidate_rule rcr
       where rc.variable_id        is null 
         and rc.string_value       is not null
         and rcr.candidate_rule_id = rc.candidate_rule_id
         and rcr.state             = 'activeState'
         and ((rcr.type = 0 and I_exclusion_ind in(0,1))
               or
              (rcr.type = 1 and I_exclusion_ind in(0,2)))
     union all
     --listable value (can return multiple rows per condition)
      select rc.candidate_rule_id,
             rc.condition_id,
             rc.operator,
             rlv.listable_key value,
             rc.field
        from (select top_level.condition_id,
                     rcr.candidate_rule_id,
                     top_level.child_condition_id,
                     top_level.operator,
                     top_level.field,
                     top_level.variable_id,
                     top_level.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
             union
              select one_below.condition_id,
                     rcr.candidate_rule_id,
                     one_below.child_condition_id,
                     one_below.operator,
                     one_below.field,
                     one_below.variable_id,
                     one_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.candidate_rule_id    is null
             union
              select two_below.condition_id,
                     rcr.candidate_rule_id,
                     two_below.child_condition_id,
                     two_below.operator,
                     two_below.field,
                     two_below.variable_id,
                     two_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below,
                     rpm_condition two_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.child_condition_id   = two_below.condition_id
                 and one_below.candidate_rule_id    is null
                 and two_below.candidate_rule_id    is null
             union
              select three_below.condition_id,
                     rcr.candidate_rule_id,
                     three_below.child_condition_id,
                     three_below.operator,
                     three_below.field,
                     three_below.variable_id,
                     three_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below,
                     rpm_condition two_below,
                     rpm_condition three_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.child_condition_id   = two_below.condition_id
                 and two_below.child_condition_id   = three_below.condition_id
                 and one_below.candidate_rule_id    is null
                 and two_below.candidate_rule_id    is null
                 and three_below.candidate_rule_id  is null) rc,
             rpm_list_value rlv,
             rpm_candidate_rule rcr
       where rc.variable_id  is null
         and rc.string_value is null
         and rc.condition_id = rlv.condition_id
         and rcr.candidate_rule_id = rc.candidate_rule_id
         and rcr.state       = 'activeState'
         and ((rcr.type = 0 and I_exclusion_ind in(0,1))
               or
              (rcr.type = 1 and I_exclusion_ind in(0,2)))
     union all
     --variable value (can return multiple rows per condition)
      select rc.candidate_rule_id,
             rc.condition_id,
             rc.operator, 
             rvdl.string_value value,
             rc.field
        from (select top_level.condition_id,
                     rcr.candidate_rule_id,
                     top_level.child_condition_id,
                     top_level.operator,
                     top_level.field,
                     top_level.variable_id,
                     top_level.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
             union
              select one_below.condition_id,
                     rcr.candidate_rule_id,
                     one_below.child_condition_id,
                     one_below.operator,
                     one_below.field,
                     one_below.variable_id,
                     one_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.candidate_rule_id    is null
             union
              select two_below.condition_id,
                     rcr.candidate_rule_id,
                     two_below.child_condition_id,
                     two_below.operator,
                     two_below.field,
                     two_below.variable_id,
                     two_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below,
                     rpm_condition two_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.child_condition_id   = two_below.condition_id
                 and one_below.candidate_rule_id    is null
                 and two_below.candidate_rule_id    is null
             union
              select three_below.condition_id,
                     rcr.candidate_rule_id,
                     three_below.child_condition_id,
                     three_below.operator,
                     three_below.field,
                     three_below.variable_id,
                     three_below.string_value
                from rpm_candidate_rule rcr,
                     rpm_condition top_level,
                     rpm_condition one_below,
                     rpm_condition two_below,
                     rpm_condition three_below
               where rcr.candidate_rule_id          = top_level.candidate_rule_id
                 and top_level.child_condition_id   = one_below.condition_id
                 and one_below.child_condition_id   = two_below.condition_id
                 and two_below.child_condition_id   = three_below.condition_id
                 and one_below.candidate_rule_id    is null
                 and two_below.candidate_rule_id    is null
                 and three_below.candidate_rule_id  is null) rc,
             rpm_variable_dept_link rvdl,
             rpm_candidate_rule rcr
       where rc.variable_id  is not null
         and rc.string_value is null
         and rc.variable_id  = rvdl.variable_id
         and rvdl.dept       = I_dept
         and rcr.candidate_rule_id = rc.candidate_rule_id
         and rcr.state       = 'activeState'
         and ((rcr.type = 0 and I_exclusion_ind in(0,1))
               or
              (rcr.type = 1 and I_exclusion_ind in(0,2)));

BEGIN

   FOR rec in C_GET_WORK LOOP

      if EXECUTE_CONDITION(O_error_msg,
                           rec.field,
                           rec.condition_id,
                           rec.operator,
                           rec.value) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   if PURGE_MULTI_SELECT_NOTEQUALS(O_error_msg,
                                   I_dept) = FALSE then
      return FALSE;
   end if;

   if LINK_ITEM_LOC_CAND_RULES(O_error_msg,
                               I_strategy_type) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_CAND_RULE_SQL.EXECUTE_CAND_RULES',
                                        to_char(SQLCODE));
      return FALSE;

END EXECUTE_CAND_RULES;

----------------------------------------------------------------------------------------
---LOCAL MODULES
----------------------------------------------------------------------------------------
FUNCTION PURGE_MULTI_SELECT_NOTEQUALS(O_error_msg         OUT VARCHAR2,
                                      I_dept          IN      rpm_strategy.dept%type)
RETURN BOOLEAN IS

BEGIN

   delete from rpm_me_il_cand_cond_gtt gtt
    where (gtt.item, gtt.location) in (
              select il.item,
                     il.location
                from ( select gtt.item,
                              gtt.location,
                              gtt.condition_id,
                              count(*) item_loc_cnd_cnt
                         from rpm_me_il_cand_cond_gtt gtt,
                              rpm_condition cond
                        where cond.operator = RPM_EXT_CAND_RULE_SQL.NOT_EQUAL
                          and cond.condition_id = gtt.condition_id
                          group by gtt.item,
                                   gtt.location,
                                   gtt.condition_id) il,
                     ( select rc.condition_id,
                              count(rlv.listable_key) cond_count
                         from rpm_condition rc,
                              rpm_list_value rlv
                        where rc.operator     = RPM_EXT_CAND_RULE_SQL.NOT_EQUAL
                          and rc.variable_id  is null
                          and rc.string_value is null
                          and rc.condition_id = rlv.condition_id
                          group by rc.condition_id
                    union all
                       select rc.condition_id,
                              count(rvdl.string_value) cond_count
                         from rpm_condition rc,
                              rpm_variable_dept_link rvdl,
                              rpm_candidate_rule rcr
                        where rc.operator     = RPM_EXT_CAND_RULE_SQL.NOT_EQUAL
                          and rc.variable_id  is not null
                          and rc.string_value is null
                          and rc.variable_id  = rvdl.variable_id
                          and rvdl.dept       = I_dept
                          group by rc.condition_id) cond
               where il.condition_id = cond.condition_id
                 and il.item_loc_cnd_cnt < cond.cond_count);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_CAND_RULE_SQL.PURGE_MULTI_SELECT_NOTEQUALS',
                                        to_char(SQLCODE));
      return FALSE;

END PURGE_MULTI_SELECT_NOTEQUALS;

----------------------------------------------------------------------------------------
FUNCTION EXECUTE_CONDITION(O_error_msg         OUT  VARCHAR2,
                           I_field          IN      rpm_condition.field%TYPE,
                           I_condition_id   IN      rpm_condition.condition_id%TYPE,
                           I_operator       IN      rpm_condition.operator%TYPE,
                           I_value          IN      rpm_condition.string_value%TYPE)
RETURN BOOLEAN IS

   L_call_string  VARCHAR2(255) := NULL;
   L_return       NUMBER        := 1;

BEGIN

   L_call_string := 'BEGIN :return := RPM_CAND_RULE_SQL.CHECK_CONDITION_'||I_field||
                                     '(:error_msg, :condition_id, :operator, :value); END;';
   EXECUTE IMMEDIATE L_call_string USING OUT L_return,  
                                         OUT O_error_msg,
                                          IN I_condition_id,
                                          IN I_operator,
                                          IN I_value;
   if L_return = 1 then
      return TRUE;
   else 
      return FALSE;
   end if;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_CAND_RULE_SQL.EXECUTE_CONDITION',
                                        to_char(SQLCODE));
      return FALSE;
END EXECUTE_CONDITION;

----------------------------------------------------------------------------------------

FUNCTION LINK_ITEM_LOC_CAND_RULES(O_error_msg        OUT VARCHAR2,
                                  I_strategy_type IN     VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   insert into rpm_me_il_cand_rule_link_gtt 
       (item,
        location,
        link_code,
        margin_mkt_basket_code,
        comp_mkt_basket_code,
        candidate_rule_id,
        type,
        worksheet_item_data_id,
        worksheet_item_loc_data_id)
      select distinct il_cand_count.item,
                      il_cand_count.location,
                      il_cand_count.link_code,
                      il_cand_count.margin_mkt_basket_code,
                      il_cand_count.comp_mkt_basket_code,
                      cr_count.candidate_rule_id,
                      cr_count.type,
                      il_cand_count.worksheet_item_data_id,
                      il_cand_count.worksheet_item_loc_data_id
        from (select gtt.item,
                     gtt.location,
                     con.link_code,
                     con.margin_mkt_basket_code,
                     con.comp_mkt_basket_code,
                     cr_cond.candidate_rule_id,
                     count(distinct gtt.condition_id) cnd_count,
                     con.worksheet_item_data_id,
                     con.worksheet_item_loc_data_id
                from rpm_me_il_cand_cond_gtt gtt,
                     rpm_me_consolidate_itemloc_gtt con,
                   (select rcr.candidate_rule_id,
                           top_level.condition_id
                      from rpm_candidate_rule rcr,
                           rpm_condition top_level
                     where rcr.candidate_rule_id          = top_level.candidate_rule_id
                   union
                    select rcr.candidate_rule_id,
                           one_below.condition_id
                      from rpm_candidate_rule rcr,
                           rpm_condition top_level,
                           rpm_condition one_below
                     where rcr.candidate_rule_id          = top_level.candidate_rule_id
                       and top_level.child_condition_id   = one_below.condition_id
                       and one_below.candidate_rule_id    is null
                   union
                    select rcr.candidate_rule_id,
                           two_below.condition_id
                      from rpm_candidate_rule rcr,
                           rpm_condition top_level,
                           rpm_condition one_below,
                           rpm_condition two_below
                     where rcr.candidate_rule_id          = top_level.candidate_rule_id
                       and top_level.child_condition_id   = one_below.condition_id
                       and one_below.child_condition_id   = two_below.condition_id
                       and one_below.candidate_rule_id    is null
                       and two_below.candidate_rule_id    is null
                   union
                    select rcr.candidate_rule_id,
                           three_below.condition_id
                      from rpm_candidate_rule rcr,
                           rpm_condition top_level,
                           rpm_condition one_below,
                           rpm_condition two_below,
                           rpm_condition three_below
                     where rcr.candidate_rule_id          = top_level.candidate_rule_id
                       and top_level.child_condition_id   = one_below.condition_id
                       and one_below.child_condition_id   = two_below.condition_id
                       and two_below.child_condition_id   = three_below.condition_id
                       and one_below.candidate_rule_id    is null
                       and two_below.candidate_rule_id    is null
                       and three_below.candidate_rule_id  is null) cr_cond
               where gtt.condition_id = cr_cond.condition_id
                 and gtt.item         = con.item
                 and gtt.location     = con.location
               group by gtt.item,
                        gtt.location,
                        con.link_code,
                        con.margin_mkt_basket_code,
                        con.comp_mkt_basket_code,
                        cr_cond.candidate_rule_id,
                        con.worksheet_item_data_id,
                        con.worksheet_item_loc_data_id) il_cand_count,
             (select candidate_rule_id,
                     type,
                     count(condition_id) cnd_count
               from (select rcr.candidate_rule_id,
                            rcr.type,
                            top_level.condition_id
                       from rpm_candidate_rule rcr,
                            rpm_condition top_level
                      where rcr.candidate_rule_id          = top_level.candidate_rule_id
                   union
                     select rcr.candidate_rule_id,
                            rcr.type,
                            one_below.condition_id
                       from rpm_candidate_rule rcr,
                            rpm_condition top_level,
                            rpm_condition one_below
                      where rcr.candidate_rule_id          = top_level.candidate_rule_id
                        and top_level.child_condition_id   = one_below.condition_id
                        and one_below.candidate_rule_id    is null
                   union
                     select rcr.candidate_rule_id,
                            rcr.type,
                            two_below.condition_id
                       from rpm_candidate_rule rcr,
                            rpm_condition top_level,
                            rpm_condition one_below,
                            rpm_condition two_below
                      where rcr.candidate_rule_id          = top_level.candidate_rule_id
                        and top_level.child_condition_id   = one_below.condition_id
                        and one_below.child_condition_id   = two_below.condition_id
                        and one_below.candidate_rule_id    is null
                        and two_below.candidate_rule_id    is null
                   union
                     select rcr.candidate_rule_id,
                            rcr.type,
                            three_below.condition_id
                       from rpm_candidate_rule rcr,
                            rpm_condition top_level,
                            rpm_condition one_below,
                            rpm_condition two_below,
                            rpm_condition three_below
                      where rcr.candidate_rule_id          = top_level.candidate_rule_id
                        and top_level.child_condition_id   = one_below.condition_id
                        and one_below.child_condition_id   = two_below.condition_id
                        and two_below.child_condition_id   = three_below.condition_id
                        and one_below.candidate_rule_id    is null
                        and two_below.candidate_rule_id    is null
                        and three_below.candidate_rule_id  is null) inner
               group by inner.candidate_rule_id, 
                        inner.type) cr_count
       where cr_count.candidate_rule_id  = il_cand_count.candidate_rule_id
         and cr_count.cnd_count         = il_cand_count.cnd_count;

   --explode out to link code level for exclusions
   insert into rpm_me_agg_cand_rule_link_gtt
          (worksheet_item_data_id,
           worksheet_item_loc_data_id,
           candidate_rule_id,
           type,
           item,
           location)
      select distinct il.worksheet_item_data_id,
             il.worksheet_item_loc_data_id,
             ilrt.candidate_rule_id,
             ilrt.type,
             il.item,
             il.location
        from rpm_me_consolidate_itemloc_gtt il,
             (select gtt.item,
                     gtt.location,
                     gtt.candidate_rule_id,
                     gtt.type
                from rpm_me_il_cand_rule_link_gtt gtt
               where exists (select 'x'
                               from rpm_me_consolidate_itemloc_gtt con
                              where gtt.item      = con.item
                                and con.link_code is null
                                and rownum        > 0)
            union
              select gtt.item,
                     gtt.location,
                     gtt.candidate_rule_id,
                     gtt.type
                from rpm_me_il_cand_rule_link_gtt gtt
               where exists (select 'x'
                               from rpm_me_consolidate_itemloc_gtt con
                              where gtt.link_code = con.link_code
                                and con.link_code is not null
                                and rownum        > 0)) ilrt
       where il.item     = ilrt.item
         and il.location = ilrt.location;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_CAND_RULE_SQL',
                                        to_char(SQLCODE));
      return FALSE;
END LINK_ITEM_LOC_CAND_RULES;

----------------------------------------------------------------------------------------

END RPM_EXT_CAND_RULE_SQL;
/

