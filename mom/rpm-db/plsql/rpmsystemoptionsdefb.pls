CREATE OR REPLACE PACKAGE BODY RPM_SYSTEM_OPTIONS_DEF_SQL AS

-----------------------------------------------------------------------------------------
--globals
-----------------------------------------------------------------------------------------

LP_system_options_def_id        rpm_system_options_def.system_options_def_id%TYPE := NULL;
LP_def_currency                 rpm_system_options_def.def_currency%TYPE := NULL;
LP_def_price_change_diff_type   rpm_system_options_def.def_price_change_diff_type%TYPE := NULL;
LP_def_price_change_item_level  rpm_system_options_def.def_price_change_item_level%TYPE := NULL;
LP_def_price_change_type        rpm_system_options_def.def_price_change_type%TYPE := NULL;
LP_def_pricing_strategy         rpm_system_options_def.def_pricing_strategy%TYPE := NULL;
LP_def_maint_margin_method      rpm_system_options_def.def_maint_margin_method%TYPE := NULL;
LP_def_wksht_promo_const_ind    rpm_system_options_def.def_wksht_promo_const_ind%TYPE := NULL;
LP_lock_version                 rpm_system_options_def.lock_version%TYPE := NULL;

------------------------------------------------------------------------                                                

FUNCTION RESET_GLOBALS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_DEF_SQL.RESET_GLOBALS';

BEGIN

   LP_system_options_def_id        := NULL;
   LP_def_currency                 := NULL;
   LP_def_price_change_diff_type   := NULL;
   LP_def_price_change_item_level  := NULL;
   LP_def_price_change_type        := NULL;
   LP_def_pricing_strategy         := NULL;
   LP_def_maint_margin_method      := NULL;
   LP_def_wksht_promo_const_ind    := NULL;
   LP_lock_version                 := NULL;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END RESET_GLOBALS;
------------------------------------------------------------------------                                                
FUNCTION SET_GLOBALS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

    L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_DEF_SQL.SET_GLOBALS';

    cursor C_SYS_OPT is
       select system_options_def_id,
              def_currency,
              def_price_change_diff_type,
              def_price_change_item_level,
              def_price_change_type,
              def_pricing_strategy,
              def_maint_margin_method,
              def_wksht_promo_const_ind,
              lock_version
     from rpm_system_options_def;

BEGIN

   open C_SYS_OPT;
   fetch C_SYS_OPT into LP_system_options_def_id,
                        LP_def_currency,
                        LP_def_price_change_diff_type,
                        LP_def_price_change_item_level,
                        LP_def_price_change_type,
                        LP_def_pricing_strategy,
                        LP_def_maint_margin_method,
                        LP_def_wksht_promo_const_ind,
                        LP_lock_version;
   close C_SYS_OPT;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             to_char(SQLCODE));
      return FALSE;
END SET_GLOBALS;
------------------------------------------------------------------------                                                
FUNCTION GET_SYSTEM_OPTIONS_DEF_ID(O_system_options_def_id     IN OUT rpm_system_options_def.system_options_def_id%TYPE,
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS_DEF_ID';                                      
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_system_options_def_id IS NULL then                                                                            
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_system_options_def_id := LP_system_options_def_id;                                                                
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_SYSTEM_OPTIONS_DEF_ID;                                                                                          
------------------------------------------------------------------------                                                
FUNCTION GET_DEF_CURRENCY(O_def_currency     IN OUT rpm_system_options_def.def_currency%TYPE,                           
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_CURRENCY';                                               
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_currency IS NULL then                                                                                     
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_currency := LP_def_currency;                                                                                  
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_CURRENCY;                                                                                                   
------------------------------------------------------------------------
FUNCTION GET_DEF_PRICE_CHANGE_DIFF_TYPE(O_def_price_change_diff_type     IN OUT                                         
rpm_system_options_def.def_price_change_diff_type%TYPE,                                                                 
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_PRICE_CHANGE_DIFF_TYPE';                                 
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_price_change_diff_type IS NULL then                                                                       
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_price_change_diff_type := LP_def_price_change_diff_type;                                                      
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_PRICE_CHANGE_DIFF_TYPE;                                                                                     
------------------------------------------------------------------------                                                
FUNCTION GET_DEF_PC_ITEM_LEVEL(O_def_price_change_item_level     IN OUT                                       
rpm_system_options_def.def_price_change_item_level%TYPE,                                                                
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_PC_ITEM_LEVEL';                                
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_price_change_item_level IS NULL then                                                                      
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_price_change_item_level := LP_def_price_change_item_level;                                                    
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_PC_ITEM_LEVEL;                                                                                    
------------------------------------------------------------------------                                                
FUNCTION GET_DEF_PRICE_CHANGE_TYPE(O_def_price_change_type     IN OUT rpm_system_options_def.def_price_change_type%TYPE,
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_PRICE_CHANGE_TYPE';                                      
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_price_change_type IS NULL then                                                                            
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_price_change_type := LP_def_price_change_type;                                                                
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_PRICE_CHANGE_TYPE;                                                                                          
------------------------------------------------------------------------                                                
FUNCTION GET_DEF_PRICING_STRATEGY(O_def_pricing_strategy     IN OUT rpm_system_options_def.def_pricing_strategy%TYPE,   
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_PRICING_STRATEGY';                                       
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_pricing_strategy IS NULL then                                                                             
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_pricing_strategy := LP_def_pricing_strategy;                                                                  
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_PRICING_STRATEGY;                                                                                           
------------------------------------------------------------------------                                                
FUNCTION GET_DEF_MAINT_MARGIN_METHOD(O_def_maint_margin_method     IN OUT                                               
rpm_system_options_def.def_maint_margin_method%TYPE,                                                                    
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_MAINT_MARGIN_METHOD';                                    
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_maint_margin_method IS NULL then                                                                          
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_maint_margin_method := LP_def_maint_margin_method;                                                            
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_MAINT_MARGIN_METHOD;                                                                                        
------------------------------------------------------------------------                                                
FUNCTION GET_DEF_WKSHT_PROMO_CONST_IND(O_def_wksht_promo_const_ind     IN OUT                                           
rpm_system_options_def.def_wksht_promo_const_ind%TYPE,                                                                  
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEF_WKSHT_PROMO_CONST_IND';                                  
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_def_wksht_promo_const_ind IS NULL then                                                                        
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_def_wksht_promo_const_ind := LP_def_wksht_promo_const_ind;                                                        
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_DEF_WKSHT_PROMO_CONST_IND;                                                                                      
------------------------------------------------------------------------                                                
FUNCTION GET_LOCK_VERSION(O_lock_version     IN OUT rpm_system_options_def.lock_version%TYPE,                           
                   O_error_message      IN OUT VARCHAR2)                                                               
RETURN BOOLEAN IS                                                                                                       
                                                                                                                       
   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_LOCK_VERSION';                                               
                                                                                                                       
BEGIN                                                                                                                   
                                                                                                                       
   if LP_lock_version IS NULL then                                                                                     
      if SET_GLOBALS(O_error_message) = FALSE then                                                                     
         return FALSE;                                                                                                 
      end if;                                                                                                          
   end if;                                                                                                             
                                                                                                                       
   O_lock_version := LP_lock_version;                                                                                  
                                                                                                                       
   return TRUE;                                                                                                        
                                                                                                                       
EXCEPTION                                                                                                               
  when OTHERS then                                                                                                     
     O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',                                                            
                                            SQLERRM,                                                                   
                                            L_program,                                                                 
                                            to_char(SQLCODE));                                                         
     return FALSE;                                                                                                     
END GET_LOCK_VERSION;                                                                                                   
------------------------------------------------------------------------                                                
END;
/

