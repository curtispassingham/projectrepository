CREATE OR REPLACE PACKAGE RPM_CLEARANCE_GTT_SQL AS
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_bulk_cc_pe_id    IN     NUMBER,
                      I_pe_sequence_id   IN     NUMBER,
                      I_thread_number    IN     NUMBER,
                      I_chunk_number     IN     NUMBER  DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SAVE_CLEARANCE_RESET(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE,
                              I_bulk_cc_pe_id     IN     NUMBER,
                              I_thread_number     IN     NUMBER,
                              I_chunk_number      IN     NUMBER    DEFAULT 0,
                              I_specific_item_loc IN     NUMBER    DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SAVE_CLEARANCE_RESET(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                              I_parent_recs       IN     OBJ_NUM_NUM_DATE_TBL,
                              I_bulk_cc_pe_id     IN     NUMBER,
                              I_thread_number     IN     NUMBER,
                              I_chunk_number      IN     NUMBER    DEFAULT 0,
                              I_specific_item_loc IN     NUMBER    DEFAULT 0)					  
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REMOVE_CONFLICTS(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                          I_conflict_ids  IN     CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK_WS(O_cc_error_tbl             OUT CONFLICT_CHECK_ERROR_TBL,
                      I_bulk_cc_pe_id         IN    NUMBER,
                      I_parent_thread_number  IN    NUMBER,
                      I_thread_number         IN    NUMBER,
                      I_chunk_number          IN    NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION REMOVE_CLEARANCE_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT_FOR_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_CLEARANCE_GTT_SQL;
/

