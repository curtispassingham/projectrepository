CREATE OR REPLACE PACKAGE BODY RPM_ZONE_GROUP_SQL AS
--------------------------------------------------------

PROCEDURE SEARCH(O_return_code      OUT NUMBER,
                 O_error_msg        OUT VARCHAR2,
                 O_z_tbl            OUT OBJ_ZONE_TBL,
                 O_z_count          OUT NUMBER,
                 I_zg_criterias     IN  OBJ_ZONE_GROUP_SEARCH_TBL)
IS

  cursor C_ZONE_LOC (L_zone_id NUMBER) is
  select new OBJ_ZONE_LOC_REC(
         rzl.zone_location_id,
         loc.loc_name,
         loc.loc_id,
         loc.loctype,
         loc.def_currency_code,
         loc.store_type,
         null)
  from rpm_zone_location rzl,
       (select store loc_id,
               0 loctype,
               store_name loc_name,
               currency_code def_currency_code,
               store_type store_type
               from store
        union
        select wh loc_id,
               2 loctype,
               wh_name loc_name,
               currency_code def_currency_code,
               null store_type
        from wh) loc
  where rzl.zone_id = L_zone_id
        and loc.loc_id = rzl.location;

  L_criterias       OBJ_ZONE_GROUP_SEARCH_TBL;
  L_criteria        OBJ_ZONE_GROUP_SEARCH_REC;
  L_select          VARCHAR2(4000) := NULL;
  L_from            VARCHAR2(4000) := NULL;
  L_where           VARCHAR2(4000) := NULL;
  L_query           VARCHAR2(4000) := NULL;
  L_iter            NUMBER;
  L_zone_rec        OBJ_ZONE_REC;
  L_zone_loc_tbl    OBJ_ZONE_LOC_TBL;

  TYPE zoneCursorType IS REF CURSOR;        -- define weak REF CURSOR type
  il_zc             zoneCursorType;         -- declare cursor variable

BEGIN

  L_criterias   :=  I_zg_criterias ;
  L_criteria    :=  L_criterias(1);

  L_select := 'SELECT NEW OBJ_ZONE_REC(' ||
                 'rz.zone_id,' ||
                 'rz.zone_display_id,' ||
                 'rz.name,' ||
                 'rz.currency_code,' ||
                 'rz.base_ind,' ||
                 'NULL)';

  L_from := 'FROM ' ||
               'rpm_zone rz,' ||
               '(SELECT * FROM TABLE(CAST(:criterias AS obj_zone_group_search_tbl))) sc';

  L_where := 'WHERE rz.zone_group_id = sc.zone_group_id ';

  IF L_criteria.currency_code IS NOT NULL THEN
      L_where := L_where || 'AND rz.currency_code = sc.currency_code ';
  END IF;

  L_query := L_select || ' ' || L_from || ' ' || L_where;

  OPEN il_zc FOR L_query USING L_criterias;
  FETCH il_zc BULK COLLECT INTO O_z_tbl;
  CLOSE il_zc;

  FOR L_iter in 1..O_z_tbl.count LOOP

    L_zone_rec := O_z_tbl(L_iter);
    L_zone_loc_tbl := OBJ_ZONE_LOC_TBL();

    OPEN C_ZONE_LOC (L_zone_rec.zone_id);
    FETCH C_ZONE_LOC BULK COLLECT INTO L_zone_loc_tbl;
    CLOSE C_ZONE_LOC;

    O_z_tbl(L_iter).ZONE_LOCATIONS := L_zone_loc_tbl;

  END LOOP;

  O_z_count := O_z_tbl.COUNT;

  O_return_code := 1;

EXCEPTION

  WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_ZONE_GROUP_SQL.SEARCH',
                                        to_char(SQLCODE));
      O_return_code := 0;

END SEARCH;

--------------------------------------------------------
PROCEDURE PROMO_ZONE_SEARCH_LM(O_return_code     OUT NUMBER,
                               O_error_msg       OUT VARCHAR2,
                               O_z_tbl           OUT OBJ_ZONE_TBL,
                               O_z_count         OUT NUMBER,
                               I_zg_criterias IN     OBJ_ZONE_GROUP_SEARCH_TBL) IS

   L_program VARCHAR2(40) := 'RPM_ZONE_GROUP_SQL.PROMO_ZONE_SEARCH_LM';

   cursor C_ZONE_LOC (L_zone_id NUMBER) is
      select new OBJ_ZONE_LOC_REC (zone_loc_id,
                                   loc_name,
                                   loc_id,
                                   loctype,
                                   def_currency_code,
                                   store_type,
                                   effective_date)
        from (select -9999 zone_loc_id,
                     loc.loc_name,
                     loc.loc_id,
                     loc.loctype,
                     loc.def_currency_code,
                     loc.store_type,
                     rlm.effective_date,
                     RANK() OVER (PARTITION BY rlm.location
                                      ORDER BY rlm.effective_date) rank
                from rpm_location_move rlm,
                     (select store loc_id,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loctype,
                             store_name loc_name,
                             currency_code def_currency_code,
                             store_type store_type
                        from store
                      union
                      select wh loc_id,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loctype,
                             wh_name loc_name,
                             currency_code def_currency_code,
                             NULL store_type
                        from wh) loc
               where rlm.new_zone_id = L_zone_id
                 and loc.loc_id      = rlm.location
                 and rlm.state       = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE) t
       where t.rank = 1;


   cursor C_ZONE (L_zone_id NUMBER) is
     select new OBJ_ZONE_REC (rz.zone_id,
                              rz.zone_display_id,
                              rz.name,
                              rz.currency_code,
                              rz.base_ind,
                              NULL)
       from rpm_zone rz
      where rz.zone_id = L_zone_id;

   cursor C_GET_ZONE_CURR_LOC (L_zone_id NUMBER) is
     select new OBJ_ZONE_LOC_REC (rzl.zone_location_id,
                                  loc.loc_name,
                                  loc.loc_id,
                                  loc.loctype,
                                  loc.def_currency_code,
                                  loc.store_type,
                                  NULL)
       from rpm_zone_location rzl,
            (select store loc_id,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loctype,
                    store_name loc_name,
                    currency_code def_currency_code,
                    store_type store_type
               from store
             union
             select wh loc_id,
                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loctype,
                    wh_name loc_name,
                    currency_code def_currency_code,
                    NULL store_type
               from wh) loc
      where rzl.zone_id = L_zone_id
        and loc.loc_id  = rzl.location;

   L_criterias       OBJ_ZONE_GROUP_SEARCH_TBL;
   L_criteria        OBJ_ZONE_GROUP_SEARCH_REC;
   L_iter            NUMBER;
   L_zone_rec        OBJ_ZONE_REC;
   L_zone_loc_tbl    OBJ_ZONE_LOC_TBL;
   L_zone_curr_loc_tbl OBJ_ZONE_LOC_TBL;

BEGIN

   L_criterias := I_zg_criterias ;
   L_criteria  := L_criterias(1);

   if L_criteria.zone_id is NULL then
      RPM_ZONE_GROUP_SQL.SEARCH(O_return_code,
                                O_error_msg,
                                O_z_tbl,
                                O_z_count,
                                I_zg_criterias);

   if O_return_code = 1 then
      for L_iter IN 1..O_z_tbl.COUNT LOOP
         L_zone_rec := O_z_tbl(L_iter);
         L_zone_loc_tbl := OBJ_ZONE_LOC_TBL();

         open C_ZONE_LOC (L_zone_rec.zone_id);
         fetch C_ZONE_LOC BULK COLLECT INTO L_zone_loc_tbl;
         close C_ZONE_LOC;

         for i IN 1..L_zone_loc_tbl.COUNT loop
            O_z_tbl(L_iter).zone_locations.EXTEND();
            O_z_tbl(L_iter).zone_locations(O_z_tbl(L_iter).zone_locations.COUNT) := L_zone_loc_tbl(i);
         end loop;

        END LOOP;
   else
      O_return_code := 0;
   end if;

  else

    open C_ZONE (L_criteria.zone_id);
    fetch C_ZONE BULK COLLECT INTO o_z_tbl;
    close C_ZONE;

    open C_GET_ZONE_CURR_LOC (L_criteria.zone_id);
    fetch C_GET_ZONE_CURR_LOC BULK COLLECT INTO L_zone_loc_tbl;
    close C_GET_ZONE_CURR_LOC;

    O_z_tbl(1).ZONE_LOCATIONS := L_zone_loc_tbl;
    L_zone_loc_tbl.delete();

    open C_ZONE_LOC (L_criteria.zone_id);
    fetch C_ZONE_LOC BULK COLLECT INTO L_zone_loc_tbl;
    close C_ZONE_LOC;

     for i IN 1..L_zone_loc_tbl.COUNT loop
        O_z_tbl(1).zone_locations.EXTEND();
        O_z_tbl(1).zone_locations(O_z_tbl(1).zone_locations.COUNT) := L_zone_loc_tbl(i);
     end loop;

    O_z_count := O_z_tbl.COUNT;
    O_return_code := 1;

  end if;

EXCEPTION

  WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;

END PROMO_ZONE_SEARCH_LM;

--------------------------------------------------------
END RPM_ZONE_GROUP_SQL;
/

