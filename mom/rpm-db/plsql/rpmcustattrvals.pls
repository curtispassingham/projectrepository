CREATE OR REPLACE PACKAGE RPM_CUST_ATTR_VALIDATION_SQL AS
--------------------------------------------------------

FUNCTION EXECUTE(O_error_msg           OUT VARCHAR2,
                 O_val_error_table     OUT OBJ_VARCHAR_DESC_TABLE,
                 I_price_event_type IN     VARCHAR2,
                 I_cust_attr_tbl    IN     OBJ_CUST_ATTR_VALIDATION_TBL)
RETURN NUMBER;
--------------------------------------------------------
END RPM_CUST_ATTR_VALIDATION_SQL;
/

