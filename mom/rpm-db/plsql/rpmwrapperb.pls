CREATE OR REPLACE PACKAGE BODY RPM_WRAPPER AS
--------------------------------------------------------

--------------------------------------------------------
---PROTOTYPES-------------------------------------------
--------------------------------------------------------
FUNCTION PARSE_ERROR(I_procedure_name  IN VARCHAR2,
                     I_merged_txt      IN VARCHAR2)
RETURN VARCHAR2;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION VALID_UOM_FOR_ITEMS (O_error_message           OUT VARCHAR2,
                              O_valid                   OUT NUMBER,
                              I_from_uom             IN     RPM_FUTURE_RETAIL.SELLING_UOM%TYPE,
                              I_item                 IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                              I_diff_id              IN     RPM_PRICE_CHANGE.DIFF_ID%TYPE,
                              I_dept                 IN     RPM_FUTURE_RETAIL.DEPT%TYPE,
                              I_class                IN     RPM_FUTURE_RETAIL.CLASS%TYPE,
                              I_subclass             IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE,
                              I_skulist              IN     RPM_PRICE_CHANGE.SKULIST%TYPE,
                              I_price_event_itemlist IN     RPM_PRICE_CHANGE.PRICE_EVENT_ITEMLIST%TYPE,
                              I_price_event_type     IN     VARCHAR2,
                              I_price_event_id       IN     NUMBER)
RETURN NUMBER IS

   L_program   VARCHAR2(35)  := 'RPM_WRAPPER.VALID_UOM_FOR_ITEMS';
   L_valid     BOOLEAN       := NULL;

   L_list_items  OBJ_VARCHAR_ID_TABLE  := NULL;

   cursor C_price_change_items is
      select item
        from rpm_price_change_skulist
       where price_event_id = I_price_event_id
      union all
      select rmld.item
        from rpm_merch_list_detail rmld
       where rmld.merch_list_id = I_price_event_itemlist;

   cursor C_clearance_items is
      select item
        from rpm_clearance_skulist
       where price_event_id = I_price_event_id
      union all
      select rmld.item
        from rpm_merch_list_detail rmld
       where rmld.merch_list_id = I_price_event_itemlist;

   cursor C_promo_dtl_items is
      select item
        from rpm_promo_dtl_skulist
       where price_event_id = I_price_event_id
      union all
      select rmld.item
        from rpm_merch_list_detail rmld
       where rmld.merch_list_id = I_price_event_itemlist;

BEGIN

   if I_skulist is NULL and
      I_price_event_itemlist is NULL then

      if UOM_SQL.VALID_UOM_FOR_ITEMS (O_error_message,
                                      L_valid,
                                      I_from_uom,
                                      I_item,
                                      I_diff_id,
                                      I_dept,
                                      I_class,
                                      I_subclass,
                                      I_skulist) = FALSE then

           O_error_message := PARSE_ERROR(L_program,
                                          O_error_message);
           return 0;

      end if;

   else

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         open C_price_change_items;
         fetch C_price_change_items BULK COLLECT into L_list_items;
         close C_price_change_items;

      elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

         open C_clearance_items;
         fetch C_clearance_items BULK COLLECT into L_list_items;
         close C_clearance_items;

      elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION) then

         open C_promo_dtl_items;
         fetch C_promo_dtl_items BULK COLLECT into L_list_items;
         close C_promo_dtl_items;

      end if;

      if L_list_items is NULL or
         L_list_items.COUNT = 0 then

         if UOM_SQL.VALID_UOM_FOR_ITEMS (O_error_message,
                                         L_valid,
                                         I_from_uom,
                                         I_item,
                                         I_diff_id,
                                         I_dept,
                                         I_class,
                                         I_subclass,
                                         I_skulist) = FALSE then

              O_error_message := PARSE_ERROR(L_program,
                                             O_error_message);
              return 0;

         end if;

      else  -- we have data on the snapshot table for the input price event

         for I in 1..L_list_items.COUNT loop

            if UOM_SQL.VALID_UOM_FOR_ITEMS (O_error_message,
                                            L_valid,
                                            I_from_uom,
                                            L_list_items(I),
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL) = FALSE then

                 O_error_message := PARSE_ERROR(L_program,
                                                O_error_message);
                 return 0;

            end if;

         end loop;

      end if;

   end if;

   if L_valid IS NULL then
      O_valid := 1;
   elsif L_valid = TRUE then
      O_valid := 1;
   elsif L_valid = FALSE then
      O_valid := 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALID_UOM_FOR_ITEMS;

--------------------------------------------------------

FUNCTION UOM_CONVERT_VALUE (O_error_message             OUT rtk_errors.rtk_text%TYPE,
                            O_converted_retail_value    OUT item_loc.unit_retail%TYPE,
                            I_item                   IN     item_master.item%TYPE,
                            I_input_retail_value     IN     item_loc.unit_retail%TYPE,
                            I_from_uom               IN     item_master.standard_uom%TYPE,
                            I_to_uom                 IN     item_master.standard_uom%TYPE)
RETURN NUMBER IS

   L_prim_supp     item_supp_country.supplier%TYPE := NULL;
   L_prim_cntry    item_supp_country.origin_country_id%TYPE := NULL;

BEGIN

   if UOM_SQL.CONVERT(O_error_message,
                      O_converted_retail_value,
                      I_to_uom,
                      I_input_retail_value,
                      I_from_uom,
                      I_item,
                      NULL,
                      NULL) = FALSE then

        O_error_message := PARSE_ERROR('RPM_WRAPPER.UOM_CONVERT_VALUE', O_error_message);
        return 0;

   end if;

   if O_converted_retail_value = 0 and I_input_retail_value != 0 then

      if ITEM_SUPP_COUNTRY_SQL.GET_PRIM_SUPP_CNTRY(O_error_message,
                                                   L_prim_supp,
                                                   L_prim_cntry,
                                                   I_item) = FALSE then
         O_error_message := PARSE_ERROR('RPM_WRAPPER.UOM_CONVERT_VALUE', O_error_message);
         return 0;

      end if;

      O_error_message := SQL_LIB.CREATE_MSG('NO_CROSS_CONV_INFO', I_item, L_prim_supp,L_prim_cntry);
      O_error_message := PARSE_ERROR('RPM_WRAPPER.UOM_CONVERT_VALUE', O_error_message);
      return 0;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RPM_WRAPPER.UOM_CONVERT_VALUE',
                                            to_char(SQLCODE));
      return 0;

END UOM_CONVERT_VALUE;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION CURRENCY_CONVERT_VALUE (O_error_message             OUT rtk_errors.rtk_text%TYPE,
                                 O_converted_value           OUT item_loc.unit_retail%TYPE,
                                 I_output_currency        IN     currencies.currency_code%TYPE,
                                 I_input_value            IN     item_loc.unit_retail%TYPE,
                                 I_input_currency         IN     currencies.currency_code%TYPE)
RETURN NUMBER IS

BEGIN

   if CURRENCY_SQL.CONVERT (O_error_message,
                            I_input_value,
                            I_input_currency,
                            I_output_currency,
                            O_converted_value,
                            null,
                            null,
                            null) = FALSE then

        O_error_message := PARSE_ERROR('RPM_WRAPPER.CURRENCY_CONVERT_VALUE', O_error_message);
        return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RPM_WRAPPER.CURRENCY_CONVERT_VALUE',
                                            to_char(SQLCODE));
      return 0;

END CURRENCY_CONVERT_VALUE;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION ROUND_CURRENCY (O_error_message             OUT rtk_errors.rtk_text%TYPE,
                         O_rounded_value             OUT item_loc.unit_retail%TYPE,
                         I_input_value            IN     item_loc.unit_retail%TYPE,
                         I_input_currency         IN     currencies.currency_code%TYPE,
                         I_markup_calc_type       IN     NUMBER)
RETURN NUMBER IS

   L_markup_calc_type  deps.markup_calc_type%TYPE := null;

BEGIN

   O_rounded_value := I_input_value;

   if I_markup_calc_type = COST_MARKUP_CALC_TYPE then
      L_markup_calc_type := 'C';
   elsif I_markup_calc_type = RETAIL_MARKUP_CALC_TYPE then
      L_markup_calc_type := 'R';
   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_markup_calc_type',
                                            TO_CHAR(I_markup_calc_type),
                                            COST_MARKUP_CALC_TYPE || ' or ' || RETAIL_MARKUP_CALC_TYPE);
      O_error_message := PARSE_ERROR('RPM_WRAPPER.ROUND_CURRENCY', O_error_message);
      return 0;
   end if;

   if CURRENCY_SQL.ROUND_CURRENCY (O_error_message,
                                   O_rounded_value,
                                   I_input_currency,
                                   L_markup_calc_type) = FALSE then

      O_error_message := PARSE_ERROR('RPM_WRAPPER.ROUND_CURRENCY', O_error_message);
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RPM_WRAPPER.ROUND_CURRENCY',
                                            to_char(SQLCODE));
      return 0;

END ROUND_CURRENCY;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION GET_VDATE (O_error_message   OUT rtk_errors.rtk_text%TYPE,
                    O_vdate           OUT DATE)
RETURN NUMBER IS

BEGIN

   if DATES_SQL.RESET_GLOBALS(O_error_message) = FALSE then
        O_error_message := PARSE_ERROR('RPM_WRAPPER.GET_VDATE', O_error_message);
        return 0;
   end if;
   if DATES_SQL.GET_VDATE(O_error_message,
                          O_vdate) = FALSE then
        O_error_message := PARSE_ERROR('RPM_WRAPPER.GET_VDATE', O_error_message);
        return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := sql_lib.create_msg('PACKAGE_ERROR',
                                            SQLERRM,
                                            'RPM_WRAPPER.GET_VDATE',
                                            to_char(SQLCODE));
      return 0;

END GET_VDATE;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION GET_VAT_RATE_INCLUDE_IND(O_error_message      OUT  VARCHAR2,
                                  O_vat_rate           OUT  VAT_CODE_RATES.VAT_RATE%TYPE,
                                  O_include_vat_ind    OUT  CLASS.CLASS_VAT_IND%TYPE,
                                  O_vat_value          OUT  GTAX_ITEM_ROLLUP.CUM_TAX_VALUE%TYPE,
                                  O_tax_type           OUT  SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE,
                                  I_item            IN      ITEM_MASTER.ITEM%TYPE,
                                  I_loc_type        IN      ITEM_LOC.LOC_TYPE%TYPE,
                                  I_location        IN      ITEM_LOC.LOC%TYPE,
                                  I_active_date     IN      DATE)
RETURN NUMBER IS

   L_program VARCHAR2(50)  := 'RPM_WRAPPER.GET_VAT_RATE_INCLUDE_IND';

   L_system_options SYSTEM_OPTIONS%ROWTYPE;

   L_loc_type ITEM_LOC.LOC_TYPE%TYPE := NULL;
   L_item_rec ITEM_MASTER%ROWTYPE    := NULL;

   L_obj_tax_calc_rec OBJ_TAX_CALC_REC;
   L_obj_tax_calc_tbl OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();

BEGIN

   if I_loc_type = RPM_STORE_LOC_TYPE then
      L_loc_type := RPM_CONSTANTS.TAX_LOC_TYPE_STORE;

   elsif I_loc_type = RPM_WH_LOC_TYPE then
      L_loc_type := RPM_CONSTANTS.TAX_LOC_TYPE_WH;

   else
      O_error_message := SQL_LIB.CREATE_MSG('INVALID_PARM','I_loc_type',
                                            TO_CHAR(I_loc_type),
                                            RPM_STORE_LOC_TYPE || ' or ' || RPM_WH_LOC_TYPE);
      O_error_message := PARSE_ERROR('RPM_WRAPPER.GET_VAT_RATE_INCLUDE_IND', O_error_message);

      return 0;
   end if;

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(O_error_message,
                                            L_system_options) = FALSE then
      return 0;
   end if;

   O_tax_type := L_system_options.default_tax_type;

   if ITEM_ATTRIB_SQL.GET_ITEM_MASTER(O_error_message,
                                      L_item_rec,
                                      I_item) = FALSE then
      return 0;
   end if;

   if L_system_options.default_tax_type = RPM_CONSTANTS.SVAT_TAX_TYPE then

      if NOT CLASS_ATTRIB_SQL.GET_CLASS_VAT_IND(O_error_message,
                                                O_include_vat_ind,
                                                L_item_rec.dept,
                                                L_item_rec.class) then
         O_error_message := PARSE_ERROR(L_program,
                                        O_error_message);
         return 0;
      end if;

      if L_system_options.class_level_vat_ind = 'N' then
         O_include_vat_ind := 'Y';
      end if;

   elsif L_system_options.default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then
      O_include_vat_ind := 'Y';

   else
      O_include_vat_ind := 'N';
   end if;

   if O_include_vat_ind = 'N' then
      O_vat_rate := NULL;
      O_vat_value := NULL;
      return 1;
   end if;

   L_obj_tax_calc_rec := OBJ_TAX_CALC_REC(I_item,               --I_ITEM
                                          L_item_rec.pack_ind,  --I_PACK_IND
                                          I_location,           --I_FROM_ENTITY
                                          L_loc_type,           --I_FROM_ENTITY_TYPE
                                          NULL,                 --I_TO_ENTITY
                                          NULL,                 --I_TO_ENTITY_TYPE
                                          I_active_date,        --I_EFFECTIVE_FROM_DATE
                                          NULL,                 --I_AMOUNT
                                          NULL,                 --I_AMOUNT_CURR
                                          NULL,                 --O_CUM_TAX_PCT
                                          NULL,                 --O_CUM_TAX_VALUE
                                          NULL,                 --O_TOTAL_TAX_AMOUNT
                                          NULL,                 --O_TOTAL_TAX_AMOUNT_CURR
                                          NULL,                 --O_TOTAL_RECOVER_AMOUNT
                                          NULL,                 --O_TOTAL_RECOVER_AMOUNT_CURR
                                          NULL);                --O_TAX_DETAIL_TBL


   L_obj_tax_calc_tbl.EXTEND;
   L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT) := L_obj_tax_calc_rec;

   if TAX_SQL.CALC_RETAIL_TAX(O_error_message,
                              L_obj_tax_calc_tbl) = FALSE then
      return 0;
   end if;

   O_vat_rate := L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_cum_tax_pct;
   O_vat_value := L_obj_tax_calc_tbl(L_obj_tax_calc_tbl.COUNT).O_cum_tax_value;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END GET_VAT_RATE_INCLUDE_IND;
--------------------------------------------------------

--------------------------------------------------------
FUNCTION PARSE_ERROR(I_procedure_name  IN VARCHAR2,
                     I_merged_txt  IN  VARCHAR2)

   RETURN VARCHAR2 IS

   L_key    VARCHAR2 (255) := NULL;
   L_txt_1  VARCHAR2 (255) := NULL;
   L_txt_2  VARCHAR2 (255) := NULL;
   L_txt_3  VARCHAR2 (255) := NULL;

   L_start_mark  INTEGER;
   L_txt_1_mark  INTEGER;
   L_txt_2_mark  INTEGER;
   L_txt_3_mark  INTEGER;

BEGIN
   L_start_mark := INSTR(I_merged_txt, '@0');
   L_txt_1_mark := INSTR(I_merged_txt, '@1');
   L_txt_2_mark := INSTR(I_merged_txt, '@2');
   L_txt_3_mark := INSTR(I_merged_txt, '@3');

   if L_start_mark > 0 then
      if L_txt_1_mark > 0 then
         L_key := SUBSTR(I_merged_txt, 3, L_txt_1_mark - 3);
      else
         L_key := SUBSTR(I_merged_txt, 3, 255);
      end if;
   elsif L_txt_1_mark > 0 then
      L_key := SUBSTR(I_merged_txt, 1, L_txt_1_mark - 3);
   else
      L_key := I_merged_txt;
   end if;

   if L_txt_1_mark > 0 then
      if L_txt_2_mark > 0 then
         L_txt_1 := SUBSTR(I_merged_txt, L_txt_1_mark + 2, L_txt_2_mark -  L_txt_1_mark - 2);
      else
         L_txt_1 := SUBSTR(I_merged_txt, L_txt_1_mark + 2, 255);
      end if;
   end if;

   if L_txt_2_mark > 0 then
      if L_txt_3_mark > 0 then
         L_txt_2 := SUBSTR(I_merged_txt, L_txt_2_mark + 2, L_txt_3_mark -  L_txt_2_mark - 2);
      else
         L_txt_2 := SUBSTR(I_merged_txt, L_txt_2_mark + 2, 255);
      end if;
   end if;

   if L_txt_3_mark > 0 then
      L_txt_3 := SUBSTR(I_merged_txt, L_txt_3_mark + 2, 255);
   end if;


   return 'RMS PL/SQL ERROR: ' || I_procedure_name || ' > ' || SQL_LIB.GET_MESSAGE_TEXT(L_key,
                                   L_txt_1,
                                   L_txt_2,
                                   L_txt_3);
END PARSE_ERROR;
--------------------------------------------------------
FUNCTION GET_SUPPLIER_SITES_IND(O_error_message       OUT  VARCHAR2,
                                O_supplier_sites_ind  OUT  SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE)
RETURN NUMBER IS

   CURSOR c_get_supplier_sites_ind is
      select supplier_sites_ind
       from system_options;

BEGIN

   OPEN c_get_supplier_sites_ind;
   FETCH c_get_supplier_sites_ind into O_SUPPLIER_SITES_IND;
   CLOSE c_get_supplier_sites_ind;

  return 1;

END GET_SUPPLIER_SITES_IND;
--------------------------------------------------------
END RPM_WRAPPER;
/