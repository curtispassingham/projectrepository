CREATE OR REPLACE PACKAGE RPM_BULK_CC_ACTIONS_SQL AS
--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION PROCESS_PRICE_EVENTS(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                              I_bulk_cc_pe_id           IN     NUMBER,
                              I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type        IN     VARCHAR2,
                              I_rib_trans_id            IN     NUMBER,
                              I_persist_ind             IN     VARCHAR2,
                              I_start_state             IN     VARCHAR2,
                              I_end_state               IN     VARCHAR2,
                              I_user_name               IN     VARCHAR2,
                              I_emergency_perm          IN     NUMBER DEFAULT 0,
                              I_secondary_bulk_cc_pe_id IN     NUMBER,
                              I_secondary_ind           IN     NUMBER,
                              I_parent_thread_number    IN     NUMBER,
                              I_thread_number           IN     NUMBER,
                              I_old_clr_oostock_date    IN     DATE DEFAULT NULL,
                              I_old_clr_reset_date      IN     DATE DEFAULT NULL)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEMLOC_LINKCODE(O_error_message             OUT VARCHAR2,
                                   O_itemloc_validation_tbl    OUT OBJ_ITEMLOC_VALIDATION_TBL,
                                   I_price_event_ids        IN     OBJ_NUMERIC_ID_TABLE,
                                   I_price_event_type       IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_ITEMLOC_RANGING(O_error_message             OUT VARCHAR2,
                                  O_itemloc_validation_tbl    OUT OBJ_ITEMLOC_VALIDATION_TBL,
                                  I_price_event_ids        IN     OBJ_NUMERIC_ID_TABLE,
                                  I_price_event_type       IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_CLEARANCE_DATES(O_error_message       OUT VARCHAR2,
                                  O_validation_tbl      OUT OBJ_ITEMLOC_VALIDATION_TBL,
                                  I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------------------

FUNCTION CREATE_OR_UPDATE_PROMO_DEAL(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                     I_user_name       IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------------------

FUNCTION GET_PE_CC_ERR_ITEM_LOC(O_error_msg           OUT  VARCHAR2,
                                O_price_event_type    OUT  RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                                O_item_locs           OUT  OBJ_NUM_NUM_STR_TBL,
                                I_cc_error_tbl     IN      CONFLICT_CHECK_ERROR_TBL,
                                I_bulk_cc_pe_id    IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE)
RETURN NUMBER;

--------------------------------------------------------------------------------------------

END RPM_BULK_CC_ACTIONS_SQL;
/