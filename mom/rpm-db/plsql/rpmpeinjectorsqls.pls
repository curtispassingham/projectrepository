CREATE OR REPLACE PACKAGE RPM_PRICE_EVENT_INJECTOR_SQL AS
--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------
FUNCTION GET_PROCESS_ID(O_error_message        OUT VARCHAR2,
                        O_process_id           OUT NUMBER,
                        I_status_to_process IN     VARCHAR2,
                        I_event_type        IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION PRE_INIT_THREADS(O_error_message        OUT VARCHAR2,
                          I_process_id        IN     NUMBER,
                          I_event_type        IN     VARCHAR2,
                          I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION INIT_THREADS(O_error_message        OUT VARCHAR2,
                      I_process_id        IN     NUMBER,
                      I_status_to_process IN     VARCHAR2,
                      I_event_type        IN     VARCHAR2,
                      I_luw               IN     NUMBER)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION POST_INIT_THREADS(O_error_message        OUT VARCHAR2,
                           O_max_thread           OUT NUMBER,
                           I_process_id        IN     NUMBER,
                           I_event_type        IN     VARCHAR2,
                           I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_PRICE_CHANGE(O_error_message        OUT VARCHAR2,
                               I_thread_num        IN     NUMBER,
                               I_process_id        IN     NUMBER,
                               I_user_name         IN     VARCHAR2,
                               I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION GENERATE_CLEARANCE(O_error_message        OUT VARCHAR2,
                            I_thread_num        IN     NUMBER,
                            I_process_id        IN     NUMBER,
                            I_user_name         IN     VARCHAR2,
                            I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION GENERATE_CLEARANCE_RESET(O_error_message        OUT VARCHAR2,
                                  I_thread_num        IN     NUMBER,
                                  I_process_id        IN     NUMBER,
                                  I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------

FUNCTION GENERATE_SIMPLE_PROMOTION(O_error_message        OUT VARCHAR2,
                                   I_thread_num        IN     NUMBER,
                                   I_process_id        IN     NUMBER,
                                   I_user_name         IN     VARCHAR2,
                                   I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION GENERATE_MULTIBUY_PROMOTION(O_error_message        OUT VARCHAR2,
                                     I_thread_num        IN     NUMBER,
                                     I_process_id        IN     NUMBER,
                                     I_user_name         IN     VARCHAR2,
                                     I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION GENERATE_THRESHOLD_PROMOTION(O_error_message        OUT VARCHAR2,
                                      I_thread_num        IN     NUMBER,
                                      I_process_id        IN     NUMBER,
                                      I_user_name         IN     VARCHAR2,
                                      I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION GENERATE_FINANCE_PROMOTION(O_error_message        OUT VARCHAR2,
                                    I_thread_num        IN     NUMBER,
                                    I_process_id        IN     NUMBER,
                                    I_user_name         IN     VARCHAR2,
                                    I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION GENERATE_TRANSACTION_PROMOTION(O_error_message        OUT VARCHAR2,
                                        I_thread_num        IN     NUMBER,
                                        I_process_id        IN     NUMBER,
                                        I_user_name         IN     VARCHAR2,
                                        I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
------------------------------------------------------------------------------------
FUNCTION INIT_BULK_CC_PE(O_error_message        OUT VARCHAR2,
                         O_bulk_cc_pe_ids       OUT OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type  IN     VARCHAR2,
                         I_process_id        IN     NUMBER,
                         I_user_name         IN     VARCHAR2,
                         I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_message         OUT VARCHAR2,
                       I_cc_error_tbl       IN     CONFLICT_CHECK_ERROR_TBL,
                       I_price_event_type   IN     VARCHAR2,
                       I_bulk_cc_pe_id      IN     NUMBER,
                       I_pe_sequence_number IN     NUMBER,
                       I_pe_thread_number   IN     NUMBER,
                       I_set_error          IN     NUMBER   DEFAULT 0,
                       I_error_string       IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION POST_VALIDATE(O_error_message         OUT VARCHAR2,
                       O_total_count           OUT NUMBER,
                       O_error_count           OUT NUMBER,
                       O_pe_for_apprv_count    OUT NUMBER,
                       O_pe_generated_count    OUT NUMBER,
                       I_process_id         IN     NUMBER,
                       I_event_type         IN     VARCHAR2,
                       I_status_to_process  IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_CONFLICT_COUNT(O_error_message        OUT VARCHAR2,
                            O_conflict_count       OUT NUMBER,
                            I_process_id        IN     NUMBER,
                            I_event_type        IN     VARCHAR2,
                            I_status_to_process IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION ROLL_BACK_VALIDATION_THREAD(O_error_message        OUT VARCHAR2,
                                     I_event_type        IN     VARCHAR2,
                                     I_process_id        IN     NUMBER,
                                     I_thread_num        IN     NUMBER,
                                     I_status_to_process IN     VARCHAR2,
                                     I_error_string      IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_SEQ_ID(I_is_next  IN     NUMBER,
                    I_seq_name IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_PRICE_EVENT_INJECTOR_SQL;
/