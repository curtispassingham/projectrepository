CREATE OR REPLACE PACKAGE RPM_NO_RIB_PRICE_PUBLISH_SQL AS
--------------------------------------------------------

FUNCTION SETUP_MESSAGES_THREAD(O_error_msg       OUT VARCHAR2,
                               O_thread_num      OUT NUMBER,
                               I_event_family IN     VARCHAR2,
                               I_thread_luw   IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION CREATE_MESSAGES(O_error_msg       OUT VARCHAR2,
                         I_event_family IN     VARCHAR2,
                         I_thread_num   IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION CLEANUP_ALL_PAYLOADS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION UPDATE_PUBLISH_STATUS(O_error_msg       OUT VARCHAR2,
                               I_event_family IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------

END RPM_NO_RIB_PRICE_PUBLISH_SQL;
/
