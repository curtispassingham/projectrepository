CREATE OR REPLACE PACKAGE RPM_GENERATE_AREA_DIFF_PC AS
-----------------------------------------------------------------------------
FUNCTION GENERATE(O_error_msg            OUT VARCHAR2,
                  O_process_id           OUT NUMBER,
                  O_need_cc              OUT NUMBER,				  
                  I_area_diff_pc_ids  IN     OBJ_NUMERIC_ID_TABLE,
                  I_user_name         IN     VARCHAR2)
RETURN NUMBER;

-----------------------------------------------------------------------------
FUNCTION INIT_BULK_CC_PE(O_error_msg           OUT VARCHAR2,
                         O_bulk_cc_pe_id       OUT NUMBER,
                         I_process_id       IN     NUMBER,
                         I_user_name        IN     VARCHAR2)
RETURN NUMBER;

-----------------------------------------------------------------------------
FUNCTION UPDATE_STATUS(O_error_msg           OUT VARCHAR2,
                       I_process_id       IN     NUMBER)
RETURN NUMBER;

-----------------------------------------------------------------------------
FUNCTION APPROVE_SECONDARY(O_error_msg            OUT VARCHAR2,
                           O_conflict_exists      OUT NUMBER,
                           I_rib_trans_id      IN     NUMBER,         
                           I_thread_id         IN     NUMBER,
                           I_thread_number     IN     NUMBER,                    
                           I_user_id           IN     rpm_price_change.create_id%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
END RPM_GENERATE_AREA_DIFF_PC;
/

