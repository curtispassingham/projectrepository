CREATE OR REPLACE PACKAGE RPM_CLR_RES_INQUIRY_SQL AS
--------------------------------------------------------

LOCATION_LEVEL                      CONSTANT NUMBER(1) := 0;
ZONE_LEVEL                          CONSTANT NUMBER(1) := 1;

--------------------------------------------------------

FUNCTION GET_CLR_RESET_INQ_VO (O_error_msg          OUT VARCHAR2,
                               O_clear_resets       OUT OBJ_CLR_RES_INQ_TBL,
                               I_search_criteria IN     OBJ_PRICE_INQ_SEARCH_TBL) 
RETURN NUMBER;

--------------------------------------------------------
FUNCTION GET_CLEARANCE_RESET (O_error_msg        OUT VARCHAR2,
                              O_clear_resets     OUT OBJ_CLR_RES_INQ_TBL,
                              I_item_loc_recs IN     OBJ_CREATE_PC_TBL,
                              I_user_id       IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------
END RPM_CLR_RES_INQUIRY_SQL;
/
