CREATE OR REPLACE PACKAGE RPM_APP_COMM_UTILITY_SQL AS
--------------------------------------------------------------------------------
PROCEDURE LOG_THIS (O_return_code     OUT NUMBER,
                    O_error_msg       OUT VARCHAR2,
                    I_in_bu        IN     VARCHAR2,
                    I_in_prod      IN     VARCHAR2,
                    I_in_batch     IN     VARCHAR2,
                    I_in_module    IN     VARCHAR2,
                    I_in_log_type  IN     VARCHAR2,
                    I_in_log_text  IN     VARCHAR2 DEFAULT NULL,
                    I_in_nbr_trans IN     NUMBER   DEFAULT NULL);
-----------------------------------------------------------------------------
PROCEDURE ENABLE_TRACE (I_in_batch       IN     VARCHAR2,
                        I_in_trace_level IN     NUMBER DEFAULT 8);
-----------------------------------------------------------------------------
END RPM_APP_COMM_UTILITY_SQL;
/
