CREATE OR REPLACE PACKAGE RPM_CREATE_PC_VO_SQL AS
-------------------------------------------------------------------------------
FUNCTION VALIDATE_AND_GENERATE_PC(O_error_tbl                OUT OBJ_STR_STR_STR_STR_STR_TBL,
                                  O_pc_rec_tbl               OUT OBJ_PC_TBL,
                                  I_create_pc_tbl         IN     OBJ_CREATE_PC_TBL,
                                  I_user_id               IN     VARCHAR2)
RETURN NUMBER;
-------------------------------------------------------------------------------
END RPM_CREATE_PC_VO_SQL;
/