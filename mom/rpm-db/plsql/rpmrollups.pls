CREATE OR REPLACE PACKAGE RPM_ROLLUP AS

--------------------------------------------------------

ALL_TO_ALL CONSTANT NUMBER(2) := 0;
IL_TO_IZ   CONSTANT NUMBER(2) := 1;
IL_TO_DL   CONSTANT NUMBER(2) := 2;
IZ_TO_DZ   CONSTANT NUMBER(2) := 3;
DL_TO_DZ   CONSTANT NUMBER(2) := 4;
DL_TO_PL   CONSTANT NUMBER(2) := 5;
DZ_TO_PZ   CONSTANT NUMBER(2) := 6;
PL_TO_PZ   CONSTANT NUMBER(2) := 7;
IZ_TO_PZ   CONSTANT NUMBER(2) := 8;
DL_TO_PZ   CONSTANT NUMBER(2) := 9;
IL_TO_DZ   CONSTANT NUMBER(2) := 10;
IL_TO_PL   CONSTANT NUMBER(2) := 11;
IL_TO_PZ   CONSTANT NUMBER(2) := 12;

--------------------------------------------------------
FUNCTION ROLL_FUTURE_RETAIL(O_error_msg     IN OUT VARCHAR2,
                            I_thread_number IN     NUMBER,
                            I_rollup_type   IN     RPM_FUTURE_RETAIL.DEPT%TYPE DEFAULT RPM_ROLLUP.ALL_TO_ALL)
RETURN NUMBER;

--------------------------------------------------------
FUNCTION ROLL_FUTURE_RETAIL_BY_ITEMS(O_error_msg   IN OUT VARCHAR2,
                                     I_items       IN     OBJ_VARCHAR_ID_TABLE,
                                     I_rollup_type IN     RPM_FUTURE_RETAIL.DEPT%TYPE DEFAULT RPM_ROLLUP.ALL_TO_ALL)
RETURN NUMBER;

--------------------------------------------------------
FUNCTION THREAD_ROLLUP_FR(O_error_msg    OUT VARCHAR2,
                          I_luw       IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------

FUNCTION CLEAN_UP(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------
FUNCTION ROLL_FUTURE_RETAIL_FOR_NIL(O_error_msg        OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------
FUNCTION ROLL_FUTURE_RETAIL_FOR_PZ_UPD(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------
FUNCTION GET_MAX_THREAD_NUMBER(O_error_msg            OUT VARCHAR2,
                               O_max_thread_number    OUT NUMBER)
RETURN NUMBER;

--------------------------------------------------------
END RPM_ROLLUP;
/