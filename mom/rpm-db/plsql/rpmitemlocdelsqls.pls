CREATE OR REPLACE PACKAGE RPM_ITEM_LOC_DELETION_SQL AS
--------------------------------------------------------------------------------
PROCEDURE POPULATE_STAGING_TABLE(O_return_code          OUT NUMBER,
                                 O_error_msg            OUT VARCHAR2,
                                 I_item              IN     VARCHAR2,
                                 I_location          IN     NUMBER);

--------------------------------------------------------------------------------
PROCEDURE DELETE_FUTURE_RETAIL(O_return_code          OUT NUMBER,
                               O_error_msg            OUT VARCHAR2);
--------------------------------------------------------------------------------
END RPM_ITEM_LOC_DELETION_SQL;
/