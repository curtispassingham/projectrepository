CREATE OR REPLACE PACKAGE RPM_PROMO_CONSTRAINT_SQL AS
--------------------------------------------------------
PROCEDURE SEARCH(O_return_code              OUT NUMBER,
                 O_error_msg                OUT VARCHAR2,
                 O_lc_tbl                   OUT OBJ_PROMO_CONSTRAINT_TBL,
                 O_lc_count                 OUT NUMBER,
                 I_lc_criterias             IN  OBJ_PROMO_CONST_SEARCH_TBL);
--------------------------------------------------------

END RPM_PROMO_CONSTRAINT_SQL;
/