CREATE OR REPLACE PACKAGE BODY RPM_ZONE_FUTURE_RETAIL_SQL AS

LP_vdate DATE := GET_VDATE;

-------------------------------------------------------
FUNCTION CHECK_PRIMARY_ZONE(O_cc_error_tbl       OUT  CONFLICT_CHECK_ERROR_TBL,
                            O_price_change_ids   OUT  OBJ_NUMERIC_ID_TABLE,
                            I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
FUNCTION MERGE_INTO_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------
FUNCTION COPY_RZFR(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                   I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)

RETURN NUMBER;
-------------------------------------------------------------------------------------
FUNCTION INSERT_SEED_DATA(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
-------------------------------------------------------------------------------------

FUNCTION IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

-------------------------------------------------------------------------------------
FUNCTION ROLL_FORWARD(IO_cc_error_tbl   IN OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_change_id IN     NUMBER,
                      I_start_date      IN     DATE,
                      I_end_date        IN     DATE)
RETURN NUMBER;
-------------------------------------------------------------------------------------

FUNCTION PUSH_BACK(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

-------------------------------------------------------------------------------------
FUNCTION REMOVE_FROM_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
---------------------------------------------------------------------------------------
FUNCTION REMOVE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
----------------------------------------------------------------------------------------
FUNCTION ADD_PARENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                    I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE,
                    I_skip_validators  IN     BOOLEAN)
RETURN NUMBER;
------------------------------------------------------------------------------------------------
-- PUBLIC FUNCTIONS
FUNCTION PRICE_CHANGE_INSERT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT';

   L_price_change_ids OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

BEGIN

   if CHECK_PRIMARY_ZONE(O_cc_error_tbl,
                         L_price_change_ids,
                         I_price_change_ids) = 0 then
      return 0;
   end if;

   if L_price_change_ids is NOT NULL and
      L_price_change_ids.COUNT > 0 then
      --
      if MERGE_INTO_TIMELINE(O_cc_error_tbl,
                             L_price_change_ids) = 0 then
         return 0;
      end if;

      if PUSH_BACK(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   --
   end if;

   return 1;

EXCEPTION

   WHEN OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PRICE_CHANGE_INSERT;
--------------------------------------------------------------------------------------
FUNCTION PRICE_CHANGE_REMOVE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE';

   L_price_change_ids OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

BEGIN

   if CHECK_PRIMARY_ZONE(O_cc_error_tbl,
                         L_price_change_ids,
                         I_price_change_ids) = 0 then
      return 0;
   end if;
   --
   if L_price_change_ids is NOT NULL and
      L_price_change_ids.COUNT > 0 then

      if REMOVE_FROM_TIMELINE(O_cc_error_tbl,
                              L_price_change_ids) = 0 then
         return 0;
      end if;
      --
      if PUSH_BACK(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   end if;
   --
   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PRICE_CHANGE_REMOVE;
---------------------------------------------------------------------------------
-- PRIVATE FUNCTIONS
FUNCTION CHECK_PRIMARY_ZONE(O_cc_error_tbl        OUT  CONFLICT_CHECK_ERROR_TBL,
                            O_price_change_ids    OUT  OBJ_NUMERIC_ID_TABLE,
                            I_price_change_ids IN      OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ZONE_FUTURE_RETAIL_SQL.CHECK_PRIMARY_ZONE';

   L_error_msg VARCHAR2(255) := NULL;

   cursor C_ZG is
      select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rde
       where rpc.price_change_id      = VALUE(ids)
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and im.item                  = rpc.item
         and rz.zone_id               = rpc.zone_id
         and rde.dept                 = im.dept
         and rde.class                = im.class
         and rde.subclass             = im.subclass
         and rde.regular_zone_group   = rz.zone_group_id
      union all
      select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id      = VALUE(ids)
         and rpc.skulist              is NOT NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and EXISTS (select 1
                       from rpm_price_change_skulist rps,
                            item_master im,
                            rpm_zone rz,
                            rpm_merch_retail_def_expl rde
                      where rps.price_event_id     = rpc.price_change_id
                        and im.item                = rps.item
                        and rz.zone_id             = rpc.zone_id
                        and rde.dept               = im.dept
                        and rde.class              = im.class
                        and rde.subclass           = im.subclass
                        and rde.regular_zone_group = rz.zone_group_id)
      union all
      select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id
        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id      = VALUE(ids)
         and rpc.price_event_itemlist is NOT NULL
         and rpc.skulist              is NULL
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and EXISTS (select 1
                       from rpm_merch_list_detail rmld,
                            item_master im,
                            rpm_zone rz,
                            rpm_merch_retail_def_expl rde
                      where rmld.merch_list_id     = rpc.price_event_itemlist
                        and rz.zone_id             = rpc.zone_id
                        and im.item                = rmld.item
                        and rde.dept               = im.dept
                        and rde.class              = im.class
                        and rde.subclass           = im.subclass
                        and rde.regular_zone_group = rz.zone_group_id);

BEGIN

   open C_ZG;
   fetch C_ZG BULK COLLECT INTO O_price_change_ids;
   close C_ZG;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_msg));
      return 0;

END CHECK_PRIMARY_ZONE;
------------------------------------------------------------------------------------------------
FUNCTION MERGE_INTO_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program          VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_SQL.MERGE_INTO_TIMELINE';

BEGIN

   if INSERT_PRICE_CHANGE(o_cc_error_tbl,
                          I_price_change_ids) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      o_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_INTO_TIMELINE;
------------------------------------------------------------------------------
FUNCTION INSERT_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program                   VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.INSERT_PRICE_CHANGE';
   L_affected_parent_ids  OBJ_NUM_NUM_DATE_TBL := OBJ_NUM_NUM_DATE_TBL();

   cursor C_ROLL_DATE is
      select /*+ CARDINALITY(parents 10) */
             rpc.price_change_id,
             case
                when parents.date_col < rpc.effective_date then
                   parents.date_col
                else
                   rpc.effective_date
             end roll_date
        from table(cast(L_affected_parent_ids as OBJ_NUM_NUM_DATE_TBL)) parents,
             rpm_price_change rpc
       where rpc.price_change_id = parents.numeric_col1
       union all
      select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id,
             rpc.effective_date roll_date
        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = value(ids)
         and NOT EXISTS (select 1
                           from table(cast(L_affected_parent_ids as OBJ_NUM_NUM_DATE_TBL)) parents
                          where parents.numeric_col1 = rpc.price_change_id);
BEGIN

   if COPY_RZFR(O_cc_error_tbl,
                I_price_change_ids) = 0 then
      return 0;
   end if;

   if INSERT_SEED_DATA(o_cc_error_tbl,
                       I_price_change_ids) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                 I_price_change_ids) = 0 then
      return 0;
   end if;

   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.MERGE_PRICE_CHANGE(O_cc_error_tbl,
                                                        I_price_change_ids) = 0 then
      return 0;
   end if;

   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                         L_affected_parent_ids,
                                                         I_price_change_ids) = 0 then
      return 0;
   end if;

   if L_affected_parent_ids is NOT NULL and
      L_affected_parent_ids.COUNT > 0 then
      --
      if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                     L_affected_parent_ids) = 0 then
         return 0;
      end if;
      --
   end if;

   for rec IN C_ROLL_DATE loop
      if ROLL_FORWARD(O_cc_error_tbl,
                      rec.price_change_id,
                      rec.roll_date,
                      NULL) = 0 then
         return 0;
      end if;
   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INSERT_PRICE_CHANGE;
------------------------------------------------------------------------

FUNCTION COPY_RZFR(O_cc_error_tbl     OUT    conflict_check_error_tbl,
                   I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)

RETURN NUMBER IS

   L_program   VARCHAR2(40) := 'RPM_ZONE_FUTURE_RETAIL_SQL.COPY_RZFR';

BEGIN

   delete from rpm_zone_future_retail_gtt;

   insert into rpm_zone_future_retail_gtt rzfrg
      (price_event_id,
       zone_future_retail_id,
       item,
       zone,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       price_change_id,
       price_change_display_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       rzfr_rowid)
   select price_event_id,
          zone_future_retail_id,
          rzfr.item,
          zone,
          action_date,
          selling_retail,
          selling_retail_currency,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_unit_retail_currency,
          multi_selling_uom,
          price_change_id,
          price_change_display_id,
          pc_change_type,
          pc_change_amount,
          pc_change_currency,
          pc_change_percent,
          pc_change_selling_uom,
          pc_null_multi_ind,
          pc_multi_units,
          pc_multi_unit_retail,
          pc_multi_unit_retail_currency,
          pc_multi_selling_uom,
          pc_price_guide_id,
          rzfr.rowid
     from rpm_zone_future_retail rzfr,
          (select pcrec.price_change_id price_event_id,
                  im.item,
                  pcrec.zone_id
             from (select /*+ CARDINALITY (ids 10) INDEX (rpc PK_RPM_PRICE_CHANGE) */
                          rpc.price_change_id,
                          rpc.item,
                          rpc.diff_id,
                          rpc.zone_id
                     from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_price_change rpc
                    where rpc.price_change_id      = value(ids)
                      and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rpc.skulist              is NULL
                      and rpc.price_event_itemlist is NULL) pcrec,
                  item_master im
            where (   im.item             = pcrec.item
                   or (    pcrec.diff_id  is NULL
                       and im.item_parent = pcrec.item)
                   or (    pcrec.diff_id  is NOT NULL
                       and im.item_parent = pcrec.item
                       and (   im.diff_1  = pcrec.diff_id
                            or im.diff_2  = pcrec.diff_id
                            or im.diff_3  = pcrec.diff_id
                            or im.diff_4  = pcrec.diff_id)))
              and im.tran_level           = im.item_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           select pcrec.price_change_id price_event_id,
                  im.item,
                  pcrec.zone_id
             from (select /*+ CARDINALITY (ids 10) INDEX (rpc PK_RPM_PRICE_CHANGE) */
                          rpc.price_change_id,
                          rpc.item,
                          rpc.diff_id,
                          rpc.skulist,
                          rpc.zone_id
                     from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_price_change rpc
                    where rpc.price_change_id      = value(ids)
                      and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rpc.skulist              is NOT NULL
                      and rpc.price_event_itemlist is NULL
                      and rownum                   > 0) pcrec,
                  rpm_price_change_skulist rps,
                  item_master im
            where rps.price_event_id = pcrec.price_change_id
              and rps.skulist        = pcrec.skulist
              and (   im.item        = rps.item
                   or im.item_parent = rps.item)
              and im.tran_level      = im.item_level
              and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           -- Price Event ItemList level - Parent Item level
           select /*+ ORDERED CARDINALITY (ids 10) INDEX (rpc PK_RPM_PRICE_CHANGE) INDEX (IM ITEM_MASTER_I1)*/
                  rpc.price_change_id price_event_id,
                  im.item,
                  rpc.zone_id
             from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_price_change rpc,
                  rpm_merch_list_detail rmld,
                  item_master im
            where rpc.price_change_id      = value(ids)
              and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
              and rpc.price_event_itemlist is NOT NULL
              and rpc.skulist              is NULL
              and rmld.merch_list_id       = rpc.price_event_itemlist
              and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
              and rmld.item                = im.item_parent
              and im.tran_level            = im.item_level
              and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
           union all
           -- Price Event ItemList level - Tran Item level
           select /*+ ORDERED CARDINALITY (ids 10) INDEX (rpc PK_RPM_PRICE_CHANGE) */
                  rpc.price_change_id price_event_id,
                  rmld.item,
                  rpc.zone_id
             from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_price_change rpc,
                  item_master im,
                  rpm_merch_list_detail rmld
            where rpc.price_change_id      = value(ids)
              and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
              and rpc.price_event_itemlist is NOT NULL
              and rpc.skulist              is NULL
              and rmld.merch_list_id       = rpc.price_event_itemlist
              and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
              and rmld.item                = im.item
              and im.tran_level            = im.item_level
              and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES 
           minus
           --- Remove all the exceptions 
           select pcrec.price_change_id price_event_id,
                  im.item,
                  pcrec.zone_id
             from (select /*+ CARDINALITY (ids 10) INDEX (rpc PK_RPM_PRICE_CHANGE) */
                          rpc.price_change_id,
                          rpc.item,
                          rpc.diff_id,
                          rpc.zone_id
                     from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_price_change rpc
                    where rpc.price_change_id      = value(ids)
                      and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                      and rpc.skulist              is NULL
                      and rpc.price_event_itemlist is NULL) pcrec,
                      rpm_price_change rpc,
                  item_master im
            where (   im.item             = pcrec.item
                   or (    pcrec.diff_id  is NULL
                       and im.item_parent = pcrec.item)
                   or (    pcrec.diff_id  is NOT NULL
                       and im.item_parent = pcrec.item
                       and (   im.diff_1  = pcrec.diff_id
                            or im.diff_2  = pcrec.diff_id
                            or im.diff_3  = pcrec.diff_id
                            or im.diff_4  = pcrec.diff_id)))
              and im.tran_level           = im.item_level
              and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and pcrec.price_change_id   = rpc.exception_parent_id
              and im.item                 = rpc.item) t    
    where rzfr.item = t.item
      and rzfr.zone = t.zone_id;

   return 1;

EXCEPTION

   when OTHERS then

      o_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END COPY_RZFR;
-------------------------------------------------------------------------
FUNCTION INSERT_SEED_DATA(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ZONE_FUTURE_RETAIL_SQL.INSERT_SEED_DATA';

   TYPE T_CONVERT_ID_TBL IS TABLE OF RPM_ZONE_FUTURE_RETAIL.ZONE_FUTURE_RETAIL_ID%TYPE;
   TYPE T_CONVERT_SELL_RTL_TBL IS TABLE OF RPM_ZONE_FUTURE_RETAIL.SELLING_RETAIL%TYPE;
   TYPE T_CONVERT_COUNT_TBL IS TABLE OF INTEGER;

   L_convert_id_tbl       T_CONVERT_ID_TBL       := T_CONVERT_ID_TBL();
   L_convert_sell_rtl_tbl T_CONVERT_SELL_RTL_TBL := T_CONVERT_SELL_RTL_TBL();
   L_convert_count_tbl    T_CONVERT_COUNT_TBL    := T_CONVERT_COUNT_TBL();

   L_error_message VARCHAR2(1000) := NULL;

   i NUMBER := 1;

   L_converted_retail_value     RPM_ZONE_FUTURE_RETAIL.SELLING_RETAIL%TYPE        := NULL;
   L_prev_zone_future_retail_id RPM_ZONE_FUTURE_RETAIL.ZONE_FUTURE_RETAIL_ID%TYPE := NULL;

   cursor C_CONVERT_RECS is
      select zone_future_retail_id,
             item,
             zone,
             location,
             selling_retail,
             to_selling_uom,
             from_selling_uom
        from (select t.zone_future_retail_id,
                     t.item,
                     t.zone,
                     t.location,
                     t.selling_retail,
                     t.to_selling_uom,
                     t.from_selling_uom,
                     ROW_NUMBER() OVER (PARTITION BY t.zone_future_retail_id,
                                                     t.item,
                                                     t.location
                                            ORDER BY DECODE(t.cur_hier_level,
                                                            RPM_CONSTANTS.FR_HIER_ITEM_LOC, 1,
                                                            RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 2,
                                                            RPM_CONSTANTS.FR_HIER_DIFF_LOC, 3,
                                                            RPM_CONSTANTS.FR_HIER_PARENT_LOC, 4,
                                                            RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 5,
                                                            RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 8)) rank
                from (select z.zone_future_retail_id,
                             z.item,
                             z.zone,
                             z.to_selling_uom,
                             rfr.location,
                             rfr.selling_retail,
                             rfr.selling_uom from_selling_uom,
                             rfr.cur_hier_level,
                             ROW_NUMBER() OVER (PARTITION BY z.zone_future_retail_id,
                                                             rfr.item,
                                                             rfr.diff_id,
                                                             rfr.location,
                                                             rfr.zone_node_type
                                                    ORDER BY rfr.action_date desc) rank
                        from (select rzfrg.zone_future_retail_id,
                                     rzfrg.item,
                                     rzfrg.zone,
                                     rzfrg.selling_uom to_selling_uom,
                                     rzl.location,
                                     im.dept,
                                     im.item_parent,
                                     case
                                        when im.item_parent is NULL and
                                             im.item_level = im.tran_level then
                                           NULL
                                        else
                                           im.diff_1
                                     end diff_id
                                from rpm_zone_future_retail_gtt rzfrg,
                                     rpm_zone_location rzl,
                                     item_master im
                               where rzfrg.selling_retail is NULL
                                 and rzl.zone_id          = rzfrg.zone
                                 and im.item              = rzfrg.item
                                 and rownum               > 0) z,
                             rpm_future_retail rfr
                       where rfr.dept           = z.dept
                         and rfr.item           = z.item
                         and rfr.location       = z.location
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                      union all
                      select z.zone_future_retail_id,
                             z.item,
                             z.zone,
                             z.to_selling_uom,
                             rfr.location,
                             rfr.selling_retail,
                             rfr.selling_uom from_selling_uom,
                             rfr.cur_hier_level,
                             ROW_NUMBER() OVER (PARTITION BY z.zone_future_retail_id,
                                                             rfr.item,
                                                             rfr.diff_id,
                                                             rfr.location,
                                                             rfr.zone_node_type
                                                    ORDER BY rfr.action_date desc) rank
                        from (select rzfrg.zone_future_retail_id,
                                     rzfrg.item,
                                     rzfrg.zone,
                                     rzfrg.selling_uom to_selling_uom,
                                     rzl.location,
                                     im.dept,
                                     im.item_parent,
                                     case
                                        when im.item_parent is NULL and
                                             im.item_level = im.tran_level then
                                           NULL
                                        else
                                           im.diff_1
                                     end diff_id
                                from rpm_zone_future_retail_gtt rzfrg,
                                     rpm_zone_location rzl,
                                     item_master im
                               where rzfrg.selling_retail is NULL
                                 and rzl.zone_id          = rzfrg.zone
                                 and im.item              = rzfrg.item
                                 and rownum               > 0) z,
                             rpm_future_retail rfr
                       where rfr.dept           = z.dept
                         and rfr.item           = z.item
                         and rfr.location       = z.zone
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      union all
                      select z.zone_future_retail_id,
                             z.item,
                             z.zone,
                             z.to_selling_uom,
                             rfr.location,
                             rfr.selling_retail,
                             rfr.selling_uom from_selling_uom,
                             rfr.cur_hier_level,
                             ROW_NUMBER() OVER (PARTITION BY z.zone_future_retail_id,
                                                             rfr.item,
                                                             rfr.diff_id,
                                                             rfr.location,
                                                             rfr.zone_node_type
                                                    ORDER BY rfr.action_date desc) rank
                        from (select rzfrg.zone_future_retail_id,
                                     rzfrg.item,
                                     rzfrg.zone,
                                     rzfrg.selling_uom to_selling_uom,
                                     rzl.location,
                                     im.dept,
                                     im.item_parent,
                                     im.diff_1 diff_id
                                from rpm_zone_future_retail_gtt rzfrg,
                                     rpm_zone_location rzl,
                                     item_master im
                               where rzfrg.selling_retail is NULL
                                 and rzl.zone_id          = rzfrg.zone
                                 and im.item              = rzfrg.item
                                 and rownum               > 0) z,
                             rpm_future_retail rfr
                       where rfr.dept           = z.dept
                         and rfr.item           = z.item_parent
                         and rfr.diff_id        = z.diff_id
                         and rfr.location       = z.location
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                      union all
                      select z.zone_future_retail_id,
                             z.item,
                             z.zone,
                             z.to_selling_uom,
                             rfr.location,
                             rfr.selling_retail,
                             rfr.selling_uom from_selling_uom,
                             rfr.cur_hier_level,
                             ROW_NUMBER() OVER (PARTITION BY z.zone_future_retail_id,
                                                             rfr.item,
                                                             rfr.diff_id,
                                                             rfr.location,
                                                             rfr.zone_node_type
                                                    ORDER BY rfr.action_date desc) rank
                        from (select rzfrg.zone_future_retail_id,
                                     rzfrg.item,
                                     rzfrg.zone,
                                     rzfrg.selling_uom to_selling_uom,
                                     rzl.location,
                                     im.dept,
                                     im.item_parent,
                                     im.diff_1 diff_id
                                from rpm_zone_future_retail_gtt rzfrg,
                                     rpm_zone_location rzl,
                                     item_master im
                               where rzfrg.selling_retail is NULL
                                 and rzl.zone_id          = rzfrg.zone
                                 and im.item              = rzfrg.item
                                 and rownum               > 0) z,
                             rpm_future_retail rfr
                       where rfr.dept           = z.dept
                         and rfr.item           = z.item_parent
                         and rfr.location       = z.location
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      union all
                      select z.zone_future_retail_id,
                             z.item,
                             z.zone,
                             z.to_selling_uom,
                             rfr.location,
                             rfr.selling_retail,
                             rfr.selling_uom from_selling_uom,
                             rfr.cur_hier_level,
                             ROW_NUMBER() OVER (PARTITION BY z.zone_future_retail_id,
                                                             rfr.item,
                                                             rfr.diff_id,
                                                             rfr.location,
                                                             rfr.zone_node_type
                                                    ORDER BY rfr.action_date desc) rank
                        from (select rzfrg.zone_future_retail_id,
                                     rzfrg.item,
                                     rzfrg.zone,
                                     rzfrg.selling_uom to_selling_uom,
                                     rzl.location,
                                     im.dept,
                                     im.item_parent,
                                     im.diff_1 diff_id
                                from rpm_zone_future_retail_gtt rzfrg,
                                     rpm_zone_location rzl,
                                     item_master im
                               where rzfrg.selling_retail is NULL
                                 and rzl.zone_id          = rzfrg.zone
                                 and im.item              = rzfrg.item
                                 and rownum               > 0) z,
                             rpm_future_retail rfr
                       where rfr.dept           = z.dept
                         and rfr.item           = z.item_parent
                         and rfr.diff_id        = z.diff_id
                         and rfr.location       = z.zone
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                      union all
                      select z.zone_future_retail_id,
                             z.item,
                             z.zone,
                             z.to_selling_uom,
                             rfr.location,
                             rfr.selling_retail,
                             rfr.selling_uom from_selling_uom,
                             rfr.cur_hier_level,
                             ROW_NUMBER() OVER (PARTITION BY z.zone_future_retail_id,
                                                             rfr.item,
                                                             rfr.diff_id,
                                                             rfr.location,
                                                             rfr.zone_node_type
                                                    ORDER BY rfr.action_date desc) rank
                        from (select rzfrg.zone_future_retail_id,
                                     rzfrg.item,
                                     rzfrg.zone,
                                     rzfrg.selling_uom to_selling_uom,
                                     rzl.location,
                                     im.dept,
                                     im.item_parent,
                                     im.diff_1 diff_id
                                from rpm_zone_future_retail_gtt rzfrg,
                                     rpm_zone_location rzl,
                                     item_master im
                               where rzfrg.selling_retail is NULL
                                 and rzl.zone_id          = rzfrg.zone
                                 and im.item              = rzfrg.item
                                 and rownum               > 0) z,
                             rpm_future_retail rfr
                       where rfr.dept           = z.dept
                         and rfr.item           = z.item_parent
                         and rfr.location       = z.zone
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                         and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC) t
               where t.rank = 1) s
       where s.rank = 1
       order by s.zone_future_retail_id;

BEGIN

   delete rpm_pc_rec_gtt;

   insert into rpm_pc_rec_gtt (rpm_price_workspace_id,
                               ui_workflow_id,
                               price_change_id,
                               item,
                               item_description, -- In this function, this will represent item_parent
                               diff_id,
                               dept,
                               location,
                               zone_node_type,
                               zone_id,
                               effective_date)
      select pcrec.price_change_id,
             pcrec.price_change_id,
             pcrec.price_change_id,
             pcrec.item,
             pcrec.item_parent,
             pcrec.diff_1,
             pcrec.dept,
             rzl.location,
             rzl.loc_type,
             pcrec.zone_id,
             pcrec.effective_date
        from (-- Tran Item level
              select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     im.dept,
                     im.item_parent,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     rpc.zone_id,
                     rpc.price_change_id,
                     rpc.effective_date
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NULL
                 and im.item                  = rpc.item
                 and im.tran_level            = im.item_level
                 and NOT EXISTS (select 'x'
                                   from rpm_zone_future_retail_gtt rzfrg
                                  where rzfrg.item           = im.item
                                    and rzfrg.zone           = rpc.zone_id
                                    and rzfrg.action_date    < rpc.effective_date
                                    and rzfrg.price_event_id = rpc.price_change_id)
              union all
              -- Parent Diff Level
              select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     im.dept,
                     im.item_parent,
                     im.diff_1,
                     rpc.zone_id,
                     rpc.price_change_id,
                     rpc.effective_date
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     item_master im
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist is NULL
                 and im.item_parent           = rpc.item
                 and (   rpc.diff_id        is NULL
                      or (    rpc.diff_id   is NOT NULL
                          and (   im.diff_1 = rpc.diff_id
                               or im.diff_2 = rpc.diff_id
                               or im.diff_3 = rpc.diff_id
                               or im.diff_4 = rpc.diff_id)))
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and NOT EXISTS (select 'x'
                                   from rpm_zone_future_retail_gtt rzfrg
                                  where rzfrg.item           = im.item
                                    and rzfrg.zone           = rpc.zone_id
                                    and rzfrg.action_date    < rpc.effective_date
                                    and rzfrg.price_event_id = rpc.price_change_id)
            minus
            -- Remove all the exceptions in case of parent/diff item level
            select /*+ CARDINALITY(ids 10) INDEX(rpc pk_rpm_price_change) INDEX(im ITEM_MASTER_I1) */
                  im.item,
                  im.dept,
                  im.item_parent,
                  im.diff_1,
                  rpc.zone_id,
                  rpc.price_change_id,
                  rpc.effective_date
             from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_price_change rpc,
                  (select exception_parent_id,item
                     from rpm_price_change) rpc1,
                  item_master im
            where rpc.price_change_id         = value(ids)
              and rpc.zone_node_type          = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
              and rpc.skulist                 is NULL
              and rpc.price_event_itemlist    is NULL
              and im.item_parent              = rpc.item
              and (   rpc.diff_id             is NULL
                   or (    rpc.diff_id        is NOT NULL
                       and (   im.diff_1      = rpc.diff_id
                            or im.diff_2      = rpc.diff_id
                            or im.diff_3      = rpc.diff_id
                            or im.diff_4      = rpc.diff_id)))
              and im.tran_level               = im.item_level
              and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              and rpc1.exception_parent_id    = rpc.price_change_id
              and rpc1.item                   = im.item                        
             union all
              -- Item List Tran Item Level
              select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     im.dept,
                     im.item_parent,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     rpc.zone_id,
                     rpc.price_change_id,
                     rpc.effective_date
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im,
                     rpm_zone rz,
                     rpm_merch_retail_def_expl rde
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rps.price_event_id       = rpc.price_change_id
                 and rps.skulist              = rpc.skulist
                 and im.item                  = rps.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rz.zone_id               = rpc.zone_id
                 and rde.dept                 = im.dept
                 and rde.class                = im.class
                 and rde.subclass             = im.subclass
                 and rde.regular_zone_group   = rz.zone_group_id
                 and NOT EXISTS (select 'x'
                                   from rpm_zone_future_retail_gtt rzfrg
                                  where rzfrg.item           = im.item
                                    and rzfrg.zone           = rpc.zone_id
                                    and rzfrg.action_date    < rpc.effective_date
                                    and rzfrg.price_event_id = rpc.price_change_id)
              union all
              -- Item List Parent Level
              select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                     im.item,
                     im.dept,
                     im.item_parent,
                     im.diff_1,
                     rpc.zone_id,
                     rpc.price_change_id,
                     rpc.effective_date
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_price_change_skulist rps,
                     item_master im,
                     rpm_zone rz,
                     rpm_merch_retail_def_expl rde
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.skulist              is NOT NULL
                 and rpc.price_event_itemlist is NULL
                 and rps.price_event_id       = rpc.price_change_id
                 and rps.skulist              = rpc.skulist
                 and im.item_parent           = rps.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rz.zone_id               = rpc.zone_id
                 and rde.dept                 = im.dept
                 and rde.class                = im.class
                 and rde.subclass             = im.subclass
                 and rde.regular_zone_group   = rz.zone_group_id
                 and NOT EXISTS(select 'x'
                                   from rpm_zone_future_retail_gtt rzfrg
                                  where rzfrg.item           = im.item
                                    and rzfrg.zone           = rpc.zone_id
                                    and rzfrg.action_date    < rpc.effective_date
                                    and rzfrg.price_event_id = rpc.price_change_id)
              union all
              -- Price Event ItemList Tran Item Level
              select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     im.dept,
                     im.item_parent,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     rpc.zone_id,
                     rpc.price_change_id,
                     rpc.effective_date
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone rz,
                     rpm_merch_retail_def_expl rde
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and im.item                  = rmld.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rz.zone_id               = rpc.zone_id
                 and rde.dept                 = im.dept
                 and rde.class                = im.class
                 and rde.subclass             = im.subclass
                 and rde.regular_zone_group   = rz.zone_group_id
                 and NOT EXISTS (select 'x'
                                   from rpm_zone_future_retail_gtt rzfrg
                                  where rzfrg.item           = im.item
                                    and rzfrg.zone           = rpc.zone_id
                                    and rzfrg.action_date    < rpc.effective_date
                                    and rzfrg.price_event_id = rpc.price_change_id)
              union all
              -- Price Event ItemList Parent Item Level
              select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                     im.item,
                     im.dept,
                     im.item_parent,
                     im.diff_1,
                     rpc.zone_id,
                     rpc.price_change_id,
                     rpc.effective_date
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone rz,
                     rpm_merch_retail_def_expl rde
               where rpc.price_change_id      = VALUE(ids)
                 and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.price_event_itemlist is NOT NULL
                 and rpc.skulist              is NULL
                 and rpc.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and im.item_parent           = rmld.item
                 and im.tran_level            = im.item_level
                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rz.zone_id               = rpc.zone_id
                 and rde.dept                 = im.dept
                 and rde.class                = im.class
                 and rde.subclass             = im.subclass
                 and rde.regular_zone_group   = rz.zone_group_id
                 and NOT EXISTS (select 'x'
                                   from rpm_zone_future_retail_gtt rzfrg
                                  where rzfrg.item           = im.item
                                    and rzfrg.zone           = rpc.zone_id
                                    and rzfrg.action_date    < rpc.effective_date
                                    and rzfrg.price_event_id = rpc.price_change_id)) pcrec,
             rpm_zone_location rzl,
             item_loc il
       where rzl.zone_id = pcrec.zone_id
         and il.loc      = rzl.location
         and il.item     = pcrec.item;

   insert into rpm_zone_future_retail_gtt rzfrg (price_event_id,
                                                 zone_future_retail_id,
                                                 item,
                                                 zone,
                                                 action_date,
                                                 selling_retail,
                                                 selling_retail_currency,
                                                 selling_uom)
      select price_event_id,
             RPM_ZONE_FUTURE_RETAIL_SEQ.NEXTVAL,
             gt2.item,
             gt2.zone,
             gt2.action_date,
             gt2.selling_retail,
             gt2.selling_retail_currency,
             gt2.selling_uom
        from (select distinct
                     item,
                     zone_id zone,
                     effective_date - 1 action_date,
                     case MAX(selling_uom) OVER (PARTITION BY price_change_id,
                                                              item,
                                                              zone_id)
                        when MIN(selling_uom) OVER (PARTITION BY price_change_id,
                                                                 item,
                                                                 zone_id) then
                           AVG(selling_retail) OVER (PARTITION BY price_change_id,
                                                                  item,
                                                                  zone_id)
                        else
                           NULL
                     end selling_retail,
                     selling_retail_currency,
                     case MAX(selling_uom) OVER (PARTITION BY price_change_id,
                                                              item,
                                                              zone_id)
                        when MIN(selling_uom) OVER (PARTITION BY price_change_id,
                                                                 item,
                                                                 zone_id) then
                           MIN(selling_uom) OVER (PARTITION BY price_change_id,
                                                               item,
                                                               zone_id)
                        else
                           FIRST_VALUE(selling_uom) OVER (PARTITION BY price_change_id,
                                                                       item,
                                                                       zone_id)
                     end selling_uom,
                     price_change_id price_event_id
                from (select t1.item,
                             t1.location,
                             ROW_NUMBER() OVER (PARTITION BY t1.price_change_id,
                                                             t1.item,
                                                             t1.location
                                                    ORDER BY DECODE(t1.cur_hier_level,
                                                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC, 1,
                                                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 2,
                                                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC, 3,
                                                                    RPM_CONSTANTS.FR_HIER_PARENT_LOC, 4,
                                                                    RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 5,
                                                                    RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 8)) rank,
                             t1.selling_retail,
                             t1.selling_retail_currency,
                             t1.selling_uom,
                             t1.zone_id,
                             t1.effective_date,
                             t1.price_change_id
                        from (select /*+ LEADING(pcrec) USE_NL(rfr) */
                                     pcrec.item,
                                     pcrec.location,
                                     pcrec.zone_id,
                                     pcrec.effective_date,
                                     pcrec.price_change_id,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.cur_hier_level,
                                     ROW_NUMBER() OVER (PARTITION BY pcrec.price_change_id,
                                                                     rfr.item,
                                                                     rfr.diff_id,
                                                                     rfr.location,
                                                                     rfr.zone_node_type
                                                            ORDER BY rfr.action_date desc) rank
                                from rpm_pc_rec_gtt pcrec,
                                     rpm_future_retail rfr
                               where rfr.dept           = pcrec.dept
                                 and rfr.item           = pcrec.item
                                 and rfr.location       = pcrec.location
                                 and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                 and rfr.action_date    < pcrec.effective_date
                              union all
                              select /*+ LEADING(pcrec) USE_NL(rfr) */
                                     pcrec.item,
                                     pcrec.location,
                                     pcrec.zone_id,
                                     pcrec.effective_date,
                                     pcrec.price_change_id,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.cur_hier_level,
                                     ROW_NUMBER() OVER (PARTITION BY pcrec.price_change_id,
                                                                     rfr.item,
                                                                     rfr.diff_id,
                                                                     rfr.location,
                                                                     rfr.zone_node_type
                                                            ORDER BY rfr.action_date desc) rank
                                from rpm_pc_rec_gtt pcrec,
                                     rpm_future_retail rfr
                               where rfr.dept           = pcrec.dept
                                 and rfr.item           = pcrec.item
                                 and rfr.location       = pcrec.zone_id
                                 and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                 and rfr.action_date    < pcrec.effective_date
                              union all
                              select /*+ LEADING(pcrec) USE_NL(rfr) */
                                     pcrec.item,
                                     pcrec.location,
                                     pcrec.zone_id,
                                     pcrec.effective_date,
                                     pcrec.price_change_id,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.cur_hier_level,
                                     ROW_NUMBER() OVER (PARTITION BY pcrec.price_change_id,
                                                                     pcrec.item,
                                                                     rfr.diff_id,
                                                                     rfr.location,
                                                                     rfr.zone_node_type
                                                            ORDER BY rfr.action_date desc) rank
                                from rpm_pc_rec_gtt pcrec,
                                     rpm_future_retail rfr
                               where rfr.dept           = pcrec.dept
                                 and rfr.item           = pcrec.item_description        -- In this function, this will represent item_parent
                                 and rfr.diff_id        = pcrec.diff_id
                                 and rfr.location       = pcrec.location
                                 and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                                 and rfr.action_date    < pcrec.effective_date
                              union all
                              select /*+ LEADING(pcrec) USE_NL(rfr) */
                                     pcrec.item,
                                     pcrec.location,
                                     pcrec.zone_id,
                                     pcrec.effective_date,
                                     pcrec.price_change_id,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.cur_hier_level,
                                     ROW_NUMBER() OVER (PARTITION BY pcrec.price_change_id,
                                                                     pcrec.item,
                                                                     rfr.diff_id,
                                                                     rfr.location,
                                                                     rfr.zone_node_type
                                                            ORDER BY rfr.action_date desc) rank
                                from rpm_pc_rec_gtt pcrec,
                                     rpm_future_retail rfr
                               where rfr.dept           = pcrec.dept
                                 and rfr.item           = pcrec.item_description        -- In this function, this will represent item_parent
                                 and rfr.location       = pcrec.location
                                 and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                 and rfr.action_date    < pcrec.effective_date
                              union all
                              select /*+ LEADING(pcrec) USE_NL(rfr) */
                                     pcrec.item,
                                     pcrec.location,
                                     pcrec.zone_id,
                                     pcrec.effective_date,
                                     pcrec.price_change_id,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.cur_hier_level,
                                     ROW_NUMBER() OVER (PARTITION BY pcrec.price_change_id,
                                                                     pcrec.item,
                                                                     rfr.diff_id,
                                                                     rfr.location,
                                                                     rfr.zone_node_type
                                                            ORDER BY rfr.action_date desc) rank
                                from rpm_pc_rec_gtt pcrec,
                                     rpm_future_retail rfr
                               where rfr.dept           = pcrec.dept
                                 and rfr.item           = pcrec.item_description        -- In this function, this will represent item_parent
                                 and rfr.diff_id        = pcrec.diff_id
                                 and rfr.location       = pcrec.zone_id
                                 and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                                 and rfr.action_date    < pcrec.effective_date
                              union all
                              select /*+ LEADING(pcrec) USE_NL(rfr) */
                                     pcrec.item,
                                     pcrec.location,
                                     pcrec.zone_id,
                                     pcrec.effective_date,
                                     pcrec.price_change_id,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.cur_hier_level,
                                     ROW_NUMBER() OVER (PARTITION BY pcrec.price_change_id,
                                                                     pcrec.item,
                                                                     rfr.diff_id,
                                                                     rfr.location,
                                                                     rfr.zone_node_type
                                                            ORDER BY rfr.action_date desc) rank
                                from rpm_pc_rec_gtt pcrec,
                                     rpm_future_retail rfr
                               where rfr.dept           = pcrec.dept
                                 and rfr.item           = pcrec.item_description        -- In this function, this will represent item_parent
                                 and rfr.location       = pcrec.zone_id
                                 and rfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                 and rfr.action_date    < pcrec.effective_date) t1
                       where t1.rank = 1)
               where rank = 1
              union all
              select distinct
                     item,
                     zone_id zone,
                     (effective_date - 1) action_date,
                     case MAX(selling_uom) OVER (PARTITION BY price_change_id,
                                                              item,
                                                              zone_id)
                        when MIN(selling_uom) OVER (PARTITION BY price_change_id,
                                                                 item,
                                                                 zone_id) then
                           AVG(selling_retail) OVER (PARTITION BY price_change_id,
                                                                  item,
                                                                  zone_id)
                        else
                           NULL
                     end selling_retail,
                     selling_retail_currency,
                     case MAX(selling_uom) OVER (PARTITION BY price_change_id,
                                                              item,
                                                              zone_id)
                        when MIN(selling_uom) OVER (PARTITION BY price_change_id,
                                                                 item,
                                                                 zone_id) then
                           MIN(selling_uom) OVER (PARTITION BY price_change_id,
                                                               item,
                                                               zone_id)
                        else
                           FIRST_VALUE(selling_uom) OVER (PARTITION BY price_change_id,
                                                                       item,
                                                                       zone_id)
                     end selling_uom,
                     price_change_id price_event_id
               from (select /*+ ORDERED INDEX(rz RPM_ZONE_I1) */
                            pcrec.item,
                            pcrec.selling_retail,
                            rz.currency_code selling_retail_currency,
                            pcrec.selling_uom,
                            pcrec.zone_id,
                            pcrec.effective_date,
                            pcrec.price_change_id
                       from (select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                                    im.item,
                                    im.dept,
                                    im.original_retail selling_retail,
                                    im.standard_uom selling_uom,
                                    rpc.zone_id,
                                    rpc.effective_date,
                                    rpc.price_change_id
                               from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_price_change rpc,
                                    item_master im
                              where rpc.price_change_id      = VALUE(ids)
                                and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpc.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                and rpc.skulist              is NULL
                                and rpc.price_event_itemlist is NULL
                                and im.item                  = rpc.item
                                and im.tran_level            = im.item_level
                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                and NOT EXISTS (select 'x'
                                                  from rpm_zone_future_retail_gtt rzfrg
                                                 where rzfrg.item           = im.item
                                                   and rzfrg.zone           = rpc.zone_id
                                                   and rzfrg.action_date    < rpc.effective_date
                                                   and rzfrg.price_event_id = rpc.price_change_id)
                                and NOT EXISTS (select 1
                                                   from rpm_zone_location rzl,
                                                        rpm_item_loc ril
                                                  where rzl.zone_id = rpc.zone_id
                                                    and ril.dept    = im.dept
                                                    and ril.item    = im.item
                                                    and ril.loc     = rzl.location)
                             union all
                             select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                                    im.item,
                                    im.dept,
                                    im.original_retail selling_retail,
                                    im.standard_uom selling_uom,
                                    rpc.zone_id,
                                    rpc.effective_date,
                                    rpc.price_change_id
                               from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_price_change rpc,
                                    item_master im
                              where rpc.price_change_id      = VALUE(ids)
                                and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpc.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                and rpc.skulist              is NULL
                                and rpc.price_event_itemlist is NULL
                                and im.item_parent           = rpc.item
                                and (   rpc.diff_id        is NULL
                                     or (    rpc.diff_id   is NOT NULL
                                         and (   im.diff_1 = rpc.diff_id
                                              or im.diff_2 = rpc.diff_id
                                              or im.diff_3 = rpc.diff_id
                                              or im.diff_4 = rpc.diff_id)))
                                and im.tran_level            = im.item_level
                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                and NOT EXISTS (select 'x'
                                                  from rpm_zone_future_retail_gtt rzfrg
                                                 where rzfrg.item           = im.item
                                                   and rzfrg.zone           = rpc.zone_id
                                                   and rzfrg.action_date    < rpc.effective_date
                                                   and rzfrg.price_event_id = rpc.price_change_id)
                                and NOT EXISTS (select 1
                                                   from rpm_zone_location rzl,
                                                        rpm_item_loc ril
                                                  where rzl.zone_id = rpc.zone_id
                                                    and ril.dept    = im.dept
                                                    and ril.item    = im.item
                                                    and ril.loc     = rzl.location)
                             union all
                             -- Item List Tran Item Level
                             select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                                    im.item,
                                    im.dept,
                                    im.original_retail selling_retail,
                                    im.standard_uom selling_uom,
                                    rpc.zone_id,
                                    rpc.effective_date,
                                    rpc.price_change_id
                               from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_price_change rpc,
                                    rpm_price_change_skulist rps,
                                    item_master im
                              where rpc.price_change_id      = VALUE(ids)
                                and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpc.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                and rpc.skulist              is NOT NULL
                                and rpc.price_event_itemlist is NULL
                                and rpc.price_change_id      = rps.price_event_id
                                and rpc.skulist              = rps.skulist
                                and rps.item_level           = RPM_CONSTANTS.IL_ITEM_LEVEL
                                and rps.item                 = im.item
                                and im.tran_level            = im.item_level
                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                and NOT EXISTS (select 'x'
                                                  from rpm_zone_future_retail_gtt rzfrg
                                                 where rzfrg.item           = im.item
                                                   and rzfrg.zone           = rpc.zone_id
                                                   and rzfrg.action_date    < rpc.effective_date
                                                   and rzfrg.price_event_id = rpc.price_change_id)
                                and NOT EXISTS (select 1
                                                   from rpm_zone_location rzl,
                                                        rpm_item_loc ril
                                                  where rzl.zone_id = rpc.zone_id
                                                    and ril.dept    = im.dept
                                                    and ril.item    = im.item
                                                    and ril.loc     = rzl.location)
                             union all
                             -- Item List Parent Item Level
                             select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                                    im.item,
                                    im.dept,
                                    im.original_retail selling_retail,
                                    im.standard_uom selling_uom,
                                    rpc.zone_id,
                                    rpc.effective_date,
                                    rpc.price_change_id
                               from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_price_change rpc,
                                    rpm_price_change_skulist rps,
                                    item_master im
                              where rpc.price_change_id      = VALUE(ids)
                                and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpc.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                and rpc.skulist              is NOT NULL
                                and rpc.price_event_itemlist is NULL
                                and rpc.price_change_id      = rps.price_event_id
                                and rpc.skulist              = rps.skulist
                                and rps.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                and rps.item                 = im.item_parent
                                and im.tran_level            = im.item_level
                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                and NOT EXISTS (select 'x'
                                                  from rpm_zone_future_retail_gtt rzfrg
                                                 where rzfrg.item           = im.item
                                                   and rzfrg.zone           = rpc.zone_id
                                                   and rzfrg.action_date    < rpc.effective_date
                                                   and rzfrg.price_event_id = rpc.price_change_id)
                                and NOT EXISTS (select 1
                                                   from rpm_zone_location rzl,
                                                        rpm_item_loc ril
                                                  where rzl.zone_id = rpc.zone_id
                                                    and ril.dept    = im.dept
                                                    and ril.item    = im.item
                                                    and ril.loc     = rzl.location)
                             union all
                             -- Price Event ItemList Tran Item Level
                             select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im PK_ITEM_MASTER) */
                                    im.item,
                                    im.dept,
                                    im.original_retail selling_retail,
                                    im.standard_uom selling_uom,
                                    rpc.zone_id,
                                    rpc.effective_date,
                                    rpc.price_change_id
                               from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_price_change rpc,
                                    rpm_merch_list_detail rmld,
                                    item_master im
                              where rpc.price_change_id      = VALUE(ids)
                                and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpc.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                and rpc.price_event_itemlist is NOT NULL
                                and rpc.skulist              is NULL
                                and rpc.price_event_itemlist = rmld.merch_list_id
                                and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rmld.item                = im.item
                                and im.tran_level            = im.item_level
                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                and NOT EXISTS (select 'x'
                                                  from rpm_zone_future_retail_gtt rzfrg
                                                 where rzfrg.item           = im.item
                                                   and rzfrg.zone           = rpc.zone_id
                                                   and rzfrg.action_date    < rpc.effective_date
                                                   and rzfrg.price_event_id = rpc.price_change_id)
                                and NOT EXISTS (select 1
                                                   from rpm_zone_location rzl,
                                                        rpm_item_loc ril
                                                  where rzl.zone_id = rpc.zone_id
                                                    and ril.dept    = im.dept
                                                    and ril.item    = im.item
                                                    and ril.loc     = rzl.location)
                             union all
                             -- Price Event ItemList Parent Item Level
                             select /*+ CARDINALITY(ids 10) INDEX(rpc PK_RPM_PRICE_CHANGE) INDEX(im ITEM_MASTER_I1) */
                                    im.item,
                                    im.dept,
                                    im.original_retail selling_retail,
                                    im.standard_uom selling_uom,
                                    rpc.zone_id,
                                    rpc.effective_date,
                                    rpc.price_change_id
                               from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_price_change rpc,
                                    rpm_merch_list_detail rmld,
                                    item_master im
                              where rpc.price_change_id      = VALUE(ids)
                                and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpc.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
                                and rpc.price_event_itemlist is NOT NULL
                                and rpc.skulist              is NULL
                                and rpc.price_event_itemlist = rmld.merch_list_id
                                and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rmld.item                = im.item_parent
                                and im.tran_level            = im.item_level
                                and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                and NOT EXISTS (select 'x'
                                                  from rpm_zone_future_retail_gtt rzfrg
                                                 where rzfrg.item           = im.item
                                                   and rzfrg.zone           = rpc.zone_id
                                                   and rzfrg.action_date    < rpc.effective_date
                                                   and rzfrg.price_event_id = rpc.price_change_id)
                                and NOT EXISTS (select 1
                                                   from rpm_zone_location rzl,
                                                        rpm_item_loc ril
                                                  where rzl.zone_id = rpc.zone_id
                                                    and ril.dept    = im.dept
                                                    and ril.item    = im.item
                                                    and ril.loc     = rzl.location)) pcrec,
                             rpm_zone rz
                       where rz.zone_id  = pcrec.zone_id)) gt2;

   L_prev_zone_future_retail_id := NULL;

   for convert_rec IN C_CONVERT_RECS loop

      if convert_rec.from_selling_uom != convert_rec.to_selling_uom then

         if RPM_WRAPPER.UOM_CONVERT_VALUE(L_error_message,
                                          L_converted_retail_value,
                                          convert_rec.item,
                                          convert_rec.selling_retail,
                                          convert_rec.from_selling_uom,
                                          convert_rec.to_selling_uom) = 0 then

            o_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                              CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                          L_error_message,
                                                                          L_program,
                                                                          TO_CHAR(SQLCODE))));

            return 0;

         end if;

      else

         L_converted_retail_value := convert_rec.selling_retail;

      end if;

      L_convert_id_tbl.EXTEND;
      L_convert_sell_rtl_tbl.EXTEND;
      L_convert_count_tbl.EXTEND;

      if L_prev_zone_future_retail_id is NOT NULL and
         convert_rec.zone_future_retail_id != L_prev_zone_future_retail_id then

         i := i + 1;

         L_convert_id_tbl(i)       := convert_rec.zone_future_retail_id;
         L_convert_sell_rtl_tbl(i) := L_converted_retail_value;
         L_convert_count_tbl(i)    := 1;

      else

         L_convert_id_tbl(i)       := convert_rec.zone_future_retail_id;
         L_convert_sell_rtl_tbl(i) := NVL(L_convert_sell_rtl_tbl(i), 0) + L_converted_retail_value;
         L_convert_count_tbl(i)    := NVL(L_convert_count_tbl(i), 0) + 1;

      end if;

      L_prev_zone_future_retail_id := convert_rec.zone_future_retail_id;

   end loop;

   if L_convert_id_tbl.COUNT != 0 then

      forall j IN L_convert_id_tbl.FIRST..L_convert_id_tbl.LAST

         update rpm_zone_future_retail_gtt rzfrg
            set rzfrg.selling_retail = L_convert_sell_rtl_tbl(j) / L_convert_count_tbl(j)
          where rzfrg.zone_future_retail_id = L_convert_id_tbl(j)
            and rzfrg.selling_retail        is NULL;

   end if;

   return 1;

EXCEPTION
   when OTHERS then

      o_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INSERT_SEED_DATA;
-------------------------------------------------------------------------
FUNCTION IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program           VARCHAR2(60) := 'RPM_ZONE_FUTURE_RETAIL_SQL.IGNORE_APPROVED_EXCEPTIONS';
   L_child_event_ids   OBJ_NUM_NUM_DATE_TBL := OBJ_NUM_NUM_DATE_TBL();

BEGIN

   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.GET_CHILDREN(O_cc_error_tbl,
                                                  L_child_event_ids,
                                                  I_price_change_ids) = 0 then
      return 0;
   end if;

   if L_child_event_ids is NULL or
      L_child_event_ids.COUNT = 0 then
      ---
      return 1;
   end if;

   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.REMOVE_TIMELINES(O_cc_error_tbl,
                                                      L_child_event_ids) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END IGNORE_APPROVED_EXCEPTIONS;
-----------------------------------------------------------------------------

FUNCTION ROLL_FORWARD(IO_cc_error_tbl   IN OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_change_id IN     NUMBER,
                      I_start_date      IN     DATE,
                      I_end_date        IN     DATE)
RETURN NUMBER IS

   L_program   VARCHAR2(45)  := 'RPM_ZONE_FUTURE_RETAIL_SQL.ROLL_FORWARD';
   L_error_msg VARCHAR2(255) := NULL;

   L_end_date DATE := NVL(I_end_date, TO_DATE('3000', 'YYYY'));

   L_retail RPM_FUTURE_RETAIL_GTT.SELLING_RETAIL%TYPE := NULL;

   L_currency_decimal_precision NUMBER(1) := NULL;

   L_retail_temp    RPM_ZONE_FUTURE_RETAIL.SELLING_RETAIL%TYPE := NULL;
   L_change_type    RPM_PRICE_CHANGE.CHANGE_TYPE%TYPE          := NULL;
   L_change_amount  RPM_PRICE_CHANGE.CHANGE_AMOUNT%TYPE        := NULL;
   L_change_percent RPM_PRICE_CHANGE.CHANGE_PERCENT%TYPE       := NULL;
   L_price_guide_id RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE        := NULL;

   TYPE gtt_tbl IS TABLE OF RPM_ZONE_FUTURE_RETAIL_GTT%ROWTYPE INDEX BY BINARY_INTEGER;
   L_gtt_tbl GTT_TBL;

   cursor C_GTT is
      select rzfrg.price_event_id,
             rzfrg.zone_future_retail_id,
             rzfrg.item,
             rzfrg.zone,
             rzfrg.action_date,
             rzfrg.selling_retail,
             rzfrg.selling_retail_currency,
             rzfrg.selling_uom,
             rzfrg.multi_units,
             rzfrg.multi_unit_retail,
             rzfrg.multi_unit_retail_currency,
             rzfrg.multi_selling_uom,
             rzfrg.price_change_id,
             rzfrg.price_change_display_id,
             rzfrg.pc_change_type,
             rzfrg.pc_change_amount,
             rzfrg.pc_change_currency,
             rzfrg.pc_change_percent,
             rzfrg.pc_change_selling_uom,
             rzfrg.pc_nulL_multi_ind,
             rzfrg.pc_multi_units,
             rzfrg.pc_multi_unit_retail,
             rzfrg.pc_multi_unit_retail_currency,
             rzfrg.pc_multi_selling_uom,
             rzfrg.pc_price_guide_id,
             rzfrg.lock_version,
             rzfrg.rzfr_rowid,
             rzfrg.rank_value
        from (select rzfrg1.price_event_id,
                     rzfrg1.zone_future_retail_id,
                     rzfrg1.item,
                     rzfrg1.zone,
                     rzfrg1.action_date,
                     rzfrg1.selling_retail,
                     rzfrg1.selling_retail_currency,
                     rzfrg1.selling_uom,
                     rzfrg1.multi_units,
                     rzfrg1.multi_unit_retail,
                     rzfrg1.multi_unit_retail_currency,
                     rzfrg1.multi_selling_uom,
                     rzfrg1.price_change_id,
                     rzfrg1.price_change_display_id,
                     rzfrg1.pc_change_type,
                     rzfrg1.pc_change_amount,
                     rzfrg1.pc_change_currency,
                     rzfrg1.pc_change_percent,
                     rzfrg1.pc_change_selling_uom,
                     rzfrg1.pc_nulL_multi_ind,
                     rzfrg1.pc_multi_units,
                     rzfrg1.pc_multi_unit_retail,
                     rzfrg1.pc_multi_unit_retail_currency,
                     rzfrg1.pc_multi_selling_uom,
                     rzfrg1.pc_price_guide_id,
                     rzfrg1.lock_version,
                     rzfrg1.rzfr_rowid,
                     ROW_NUMBER() OVER (PARTITION BY rzfrg1.item,
                                                     rzfrg1.zone
                                            ORDER BY rzfrg1.action_date) rank_value
                from rpm_zone_future_retail_gtt rzfrg1
               where rzfrg1.action_date    < L_end_date
                 and rzfrg1.action_date   >= I_start_date
                 and rzfrg1.price_event_id = I_price_change_id
              union all
              select zero_date.price_event_id,
                     zero_date.zone_future_retail_id,
                     zero_date.item,
                     zero_date.zone,
                     zero_date.action_date,
                     zero_date.selling_retail,
                     zero_date.selling_retail_currency,
                     zero_date.selling_uom,
                     zero_date.multi_units,
                     zero_date.multi_unit_retail,
                     zero_date.multi_unit_retail_currency,
                     zero_date.multi_selling_uom,
                     zero_date.price_change_id,
                     zero_date.price_change_display_id,
                     zero_date.pc_change_type,
                     zero_date.pc_change_amount,
                     zero_date.pc_change_currency,
                     zero_date.pc_change_percent,
                     zero_date.pc_change_selling_uom,
                     zero_date.pc_nulL_multi_ind,
                     zero_date.pc_multi_units,
                     zero_date.pc_multi_unit_retail,
                     zero_date.pc_multi_unit_retail_currency,
                     zero_date.pc_multi_selling_uom,
                     zero_date.pc_price_guide_id,
                     zero_date.lock_version,
                     zero_date.rzfr_rowid,
                     zero_date.rank_value
                from (select rzfrg2.price_event_id,
                             rzfrg2.zone_future_retail_id,
                             rzfrg2.item,
                             rzfrg2.zone,
                             rzfrg2.action_date,
                             rzfrg2.selling_retail,
                             rzfrg2.selling_retail_currency,
                             rzfrg2.selling_uom,
                             rzfrg2.multi_units,
                             rzfrg2.multi_unit_retail,
                             rzfrg2.multi_unit_retail_currency,
                             rzfrg2.multi_selling_uom,
                             rzfrg2.price_change_id,
                             rzfrg2.price_change_display_id,
                             rzfrg2.pc_change_type,
                             rzfrg2.pc_change_amount,
                             rzfrg2.pc_change_currency,
                             rzfrg2.pc_change_percent,
                             rzfrg2.pc_change_selling_uom,
                             rzfrg2.pc_nulL_multi_ind,
                             rzfrg2.pc_multi_units,
                             rzfrg2.pc_multi_unit_retail,
                             rzfrg2.pc_multi_unit_retail_currency,
                             rzfrg2.pc_multi_selling_uom,
                             rzfrg2.pc_price_guide_id,
                             rzfrg2.lock_version,
                             rzfrg2.rzfr_rowid,
                             ROW_NUMBER() OVER (PARTITION BY rzfrg2.item,
                                                             rzfrg2.zone
                                                    ORDER BY rzfrg2.action_date desc) - 1 rank_value
                        from rpm_zone_future_retail_gtt rzfrg2
                       where rzfrg2.action_date    < I_start_date
                         and rzfrg2.price_event_id = I_price_change_id) zero_date
               where zero_date.rank_value = 0) rzfrg
       order by rzfrg.item,
                rzfrg.zone,
                rzfrg.rank_value;

   cursor C_GUIDE(I_item_id VARCHAR2) is
      select new_price_val
        from rpm_price_guide_interval rpgi,
             rpm_price_guide rpg
       where rpgi.price_guide_id = L_price_guide_id
         and rpgi.price_guide_id = rpg.price_guide_id
         and rpg.corp_ind        = 1
         and L_retail            BETWEEN rpgi.from_price_val and rpgi.to_price_val
      union all
      select new_price_val
        from rpm_price_guide_interval rpgi,
             rpm_price_guide rpg,
             rpm_price_guide_dept rpgd,
             item_master im
       where rpgi.price_guide_id = L_price_guide_id
         and rpgi.price_guide_id = rpg.price_guide_id
         and rpg.price_guide_id  = rpgd.price_guide_id
         and im.item             = I_item_id
         and rpg.corp_ind        = 0
         and rpgd.dept           = im.dept
         and L_retail            BETWEEN rpgi.from_price_val and rpgi.to_price_val;

   cursor C_CURRENCY_RTL_DEC(I_currency_code VARCHAR2) is
      select currency_rtl_dec
        from currencies
       where currency_code = I_currency_code;

BEGIN

   update rpm_zone_future_retail_gtt
      set timeline_seq = NULL;

   open C_GTT;
   fetch C_GTT BULK COLLECT into L_gtt_tbl;
   close C_GTT;

   delete
     from rpm_zone_future_retail_gtt gtt
    where zone_future_retail_id IN (select zone_future_retail_id
                                      from (select rfrg1.zone_future_retail_id,
                                                   ROW_NUMBER() OVER (PARTITION BY rfrg1.item,
                                                                                   rfrg1.zone
                                                                          ORDER BY rfrg1.action_date) rank_value
                                              from rpm_zone_future_retail_gtt rfrg1
                                             where rfrg1.action_date    < L_end_date
                                               and rfrg1.action_date   >= I_start_date
                                               and rfrg1.price_event_id = I_price_change_id
                                            union all
                                            select zero_date.zone_future_retail_id,
                                                   zero_date.rank_value
                                              from (select rfrg2.zone_future_retail_id,
                                                           ROW_NUMBER() OVER (PARTITION BY rfrg2.item,
                                                                                           rfrg2.zone
                                                                                  ORDER BY rfrg2.action_date desc) - 1 rank_value
                                                      from rpm_zone_future_retail_gtt rfrg2
                                                     where rfrg2.action_date    < I_start_date
                                                       and rfrg2.price_event_id = I_price_change_id) zero_date
                                     where zero_date.rank_value = 0));

   for i IN 1..L_gtt_tbl.COUNT loop

      if L_gtt_tbl(i).timeline_seq != 0 then

         -- multi unit retail
         if L_gtt_tbl(i).price_change_id is NOT NULL then
            L_gtt_tbl(i).multi_units                := L_gtt_tbl(i).pc_multi_units;
            L_gtt_tbl(i).multi_unit_retail          := L_gtt_tbl(i).pc_multi_unit_retail;
            L_gtt_tbl(i).multi_unit_retail_currency := L_gtt_tbl(i).pc_multi_unit_retail_currency;
            L_gtt_tbl(i).multi_selling_uom          := L_gtt_tbl(i).pc_multi_selling_uom;
         else
            L_gtt_tbl(i).multi_units                := L_gtt_tbl(i-1).multi_units;
            L_gtt_tbl(i).multi_unit_retail          := L_gtt_tbl(i-1).multi_unit_retail;
            L_gtt_tbl(i).multi_unit_retail_currency := L_gtt_tbl(i-1).multi_unit_retail_currency;
            L_gtt_tbl(i).multi_selling_uom          := L_gtt_tbl(i-1).multi_selling_uom;
         end if;

         -- selling uom
         if L_gtt_tbl(i).price_change_id is NOT NULL and
            L_gtt_tbl(i).pc_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
            ---
            L_gtt_tbl(i).selling_uom := L_gtt_tbl(i).pc_change_selling_uom;
         else
            L_gtt_tbl(i).selling_uom := L_gtt_tbl(i-1).selling_uom;
         end if;

         -- selling retail
         if L_gtt_tbl(i).price_change_id is NULL then
            L_gtt_tbl(i).selling_retail          := L_gtt_tbl(i-1).selling_retail;
            L_gtt_tbl(i).selling_retail_currency := L_gtt_tbl(i-1).selling_retail_currency;
            L_gtt_tbl(i).selling_uom             := L_gtt_tbl(i-1).selling_uom;
         elsif L_gtt_tbl(i-1).selling_retail is NULL and
            L_gtt_tbl(i).pc_change_type != RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
            ---
            L_gtt_tbl(i).selling_retail          := NULL;
            L_gtt_tbl(i).selling_retail_currency := NULL;
            L_gtt_tbl(i).selling_uom             := NULL;
         else
            if L_gtt_tbl(i).action_date = LP_vdate then
               -- apply_retail_change_with_guide
               L_retail_temp    := L_gtt_tbl(i).selling_retail;
               L_change_type    := L_gtt_tbl(i).pc_change_type;
               L_change_amount  := L_gtt_tbl(i).pc_change_amount;
               L_change_percent := L_gtt_tbl(i).pc_change_percent;
               L_price_guide_id := L_gtt_tbl(i).pc_price_guide_id;

               if L_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                  L_retail := L_change_amount;

               elsif L_retail_temp is NULL then

                  L_error_msg     := 'ORIGINAL VALUE CANNOT BE NULL';
                  IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                     CONFLICT_CHECK_ERROR_REC(NULL,
                                                              NULL,
                                                              RPM_CONSTANTS.CONFLICT_ERROR,
                                                              L_error_msg));
                  return 0;

               elsif L_change_type is NULL or
                     L_change_type IN (RPM_CONSTANTS.RETAIL_NONE,
                                       RPM_CONSTANTS.RETAIL_EXCLUDE) then
                  L_retail := L_retail_temp;

               elsif L_change_type IN (RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE,
                                       RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE) then
                  L_retail := L_retail_temp + L_change_amount;

               elsif L_change_type = RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE then

                  open C_CURRENCY_RTL_DEC(L_gtt_tbl(i).selling_retail_currency);
                  fetch C_CURRENCY_RTL_DEC into L_currency_decimal_precision;
                  close C_CURRENCY_RTL_DEC;

                  L_retail := ROUND(L_retail_temp * (1 + L_change_percent/100), L_currency_decimal_precision);

               else

                  L_error_msg     := 'DO NOT KNOW HOW TO HANDLE RETAILCHANGEVALUE: ' || + L_change_type;
                  IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                     CONFLICT_CHECK_ERROR_REC(NULL,
                                                              NULL,
                                                              RPM_CONSTANTS.CONFLICT_ERROR,
                                                              L_error_msg));
                  return 0;
               end if;

               if L_price_guide_id is NOT NULL then
                  L_retail_temp := L_retail;

                  open C_GUIDE(L_gtt_tbl(i).item);
                  fetch C_GUIDE into L_retail;

                  if C_GUIDE%NOTFOUND then
                     L_retail := L_retail_temp;
                  end if;

                  close C_GUIDE;

               end if;

               L_gtt_tbl(i).selling_retail_currency := L_gtt_tbl(i-1).selling_retail_currency;
               L_gtt_tbl(i).selling_retail          := L_retail;

            else
               -- apply_retail_change_with_guide
               L_retail_temp    := L_gtt_tbl(i-1).selling_retail;
               L_change_type    := L_gtt_tbl(i).pc_change_type;
               L_change_amount  := L_gtt_tbl(i).pc_change_amount;
               L_change_percent := L_gtt_tbl(i).pc_change_percent;
               L_price_guide_id := L_gtt_tbl(i).pc_price_guide_id;

               if L_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
                  L_retail := L_change_amount;

               elsif L_retail_temp is NULL then

                  L_error_msg     := 'ORIGINAL VALUE CANNOT BE NULL';
                  IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                     CONFLICT_CHECK_ERROR_REC(NULL,
                                                              NULL,
                                                              RPM_CONSTANTS.CONFLICT_ERROR,
                                                              L_error_msg));
                  return 0;
               elsif L_change_type is NULL or
                     L_change_type IN (RPM_CONSTANTS.RETAIL_NONE,
                                       RPM_CONSTANTS.RETAIL_EXCLUDE) then
                  L_retail := L_retail_temp;

               elsif L_change_type IN (RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE,
                                       RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE) then
                  L_retail := L_retail_temp + L_change_amount;

               elsif L_change_type = RPM_CONSTANTS.RETAIL_PERCENT_OFF_VALUE then

                  open C_CURRENCY_RTL_DEC(L_gtt_tbl(i).selling_retail_currency);
                  fetch C_CURRENCY_RTL_DEC into L_currency_decimal_precision;
                  close C_CURRENCY_RTL_DEC;

                  L_retail := TRUNC(L_retail_temp * (1 + L_change_percent/100), L_currency_decimal_precision);

               else

                  L_error_msg     := 'DO NOT KNOW HOW TO HANDLE RETAILCHANGEVALUE: ' || + L_change_type;
                  IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                                     CONFLICT_CHECK_ERROR_REC(NULL,
                                                              NULL,
                                                              RPM_CONSTANTS.CONFLICT_ERROR,
                                                              L_error_msg));
                  return 0;
               end if;

               if L_price_guide_id is NOT NULL then

                  L_retail_temp := L_retail;

                  open C_GUIDE(L_gtt_tbl(i).item);
                  fetch C_GUIDE into L_retail;

                  if C_GUIDE%NOTFOUND then
                     L_retail := L_retail_temp;
                  end if;

                  close C_GUIDE;

               end if;

               L_gtt_tbl(i).selling_retail_currency := L_gtt_tbl(i-1).selling_retail_currency;
               L_gtt_tbl(i).selling_retail          := L_retail;

            end if;

            if L_gtt_tbl(i).pc_change_type = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE then
               L_gtt_tbl(i).selling_uom := L_gtt_tbl(i).pc_change_selling_uom;
            else
               L_gtt_tbl(i).selling_uom := L_gtt_tbl(i-1).selling_uom;
            end if;

         end if;

      end if; -- L_gtt_tbl(i).timeline_seq != 0

   end loop;

   -- Insert that back to rpm_zone_future_retail_gtt
   forall j IN 1..L_gtt_tbl.COUNT
      insert into rpm_zone_future_retail_gtt
         values L_gtt_tbl(j);

   return 1;

EXCEPTION
   when OTHERS then
      L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  L_error_msg));
      return 0;

END ROLL_FORWARD;

---------------------------------------------------------------------------
FUNCTION PUSH_BACK(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_ZONE_FUTURE_RETAIL_SQL.PUSH_BACK';

BEGIN

   merge into rpm_zone_future_retail rzfr
   using (select zone_future_retail_id,
                 item,
                 zone,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 price_change_id,
                 price_change_display_id,
                 pc_change_type,
                 pc_change_amount,
                 pc_change_currency,
                 pc_change_percent,
                 pc_change_selling_uom,
                 pc_null_multi_ind,
                 pc_multi_units,
                 pc_multi_unit_retail,
                 pc_multi_unit_retail_currency,
                 pc_multi_selling_uom,
                 pc_price_guide_id,
                 rzfr_rowid
            from rpm_zone_future_retail_gtt) rzfrg
   on (rzfr.rowid = rzfrg.rzfr_rowid)
   when MATCHED then
      update
         set rzfr.selling_retail                = rzfrg.selling_retail,
             rzfr.selling_retail_currency       = rzfrg.selling_retail_currency,
             rzfr.selling_uom                   = rzfrg.selling_uom,
             rzfr.multi_units                   = rzfrg.multi_units,
             rzfr.multi_unit_retail             = rzfrg.multi_unit_retail,
             rzfr.multi_unit_retail_currency    = rzfrg.multi_unit_retail_currency,
             rzfr.multi_selling_uom             = rzfrg.multi_selling_uom,
             rzfr.price_change_id               = rzfrg.price_change_id,
             rzfr.price_change_display_id       = rzfrg.price_change_display_id,
             rzfr.pc_change_type                = rzfrg.pc_change_type,
             rzfr.pc_change_amount              = rzfrg.pc_change_amount,
             rzfr.pc_change_currency            = rzfrg.pc_change_currency,
             rzfr.pc_change_percent             = rzfrg.pc_change_percent,
             rzfr.pc_change_selling_uom         = rzfrg.pc_change_selling_uom,
             rzfr.pc_null_multi_ind             = rzfrg.pc_null_multi_ind,
             rzfr.pc_multi_units                = rzfrg.pc_multi_units,
             rzfr.pc_multi_unit_retail          = rzfrg.pc_multi_unit_retail,
             rzfr.pc_multi_unit_retail_currency = rzfrg.pc_multi_unit_retail_currency,
             rzfr.pc_multi_selling_uom          = rzfrg.pc_multi_selling_uom,
             rzfr.pc_price_guide_id             = rzfrg.pc_price_guide_id
   when NOT MATCHED then
      insert (zone_future_retail_id,
              item,
              zone,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              price_change_id,
              price_change_display_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id)
      values (rzfrg.zone_future_retail_id,
              rzfrg.item,
              rzfrg.zone,
              rzfrg.action_date,
              rzfrg.selling_retail,
              rzfrg.selling_retail_currency,
              rzfrg.selling_uom,
              rzfrg.multi_units,
              rzfrg.multi_unit_retail,
              rzfrg.multi_unit_retail_currency,
              rzfrg.multi_selling_uom,
              rzfrg.price_change_id,
              rzfrg.price_change_display_id,
              rzfrg.pc_change_type,
              rzfrg.pc_change_amount,
              rzfrg.pc_change_currency,
              rzfrg.pc_change_percent,
              rzfrg.pc_change_selling_uom,
              rzfrg.pc_null_multi_ind,
              rzfrg.pc_multi_units,
              rzfrg.pc_multi_unit_retail,
              rzfrg.pc_multi_unit_retail_currency,
              rzfrg.pc_multi_selling_uom,
              rzfrg.pc_price_guide_id);

   return 1;

EXCEPTION

   WHEN OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK;
--------------------------------------------------------------------------------
FUNCTION REMOVE_FROM_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_SQL.REMOVE_FROM_TIMELINE';

BEGIN

   if REMOVE_PRICE_CHANGE(O_cc_error_tbl,
                          I_price_change_ids) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   WHEN OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_FROM_TIMELINE;
--------------------------------------------------------------------------------------------------------
FUNCTION REMOVE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program                   VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_SQL.REMOVE_PRICE_CHANGE';
   --
   L_price_change_ids          OBJ_NUM_NUM_DATE_TBL := OBJ_NUM_NUM_DATE_TBL();
   L_parent_event_ids          OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_affected_parent_ids       OBJ_NUM_NUM_DATE_TBL := OBJ_NUM_NUM_DATE_TBL();
   --
   cursor C_REMOVE_IDS is
      select OBJ_NUM_NUM_DATE_REC(value(ids),
                                  value(ids),
                                  NULL)
        from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids;

   cursor C_GET_AFFECTED_PAR_ID is
      select numeric_col2 price_change_id
        from table(cast(L_affected_parent_ids as OBJ_NUM_NUM_DATE_TBL));

   cursor C_ROLL_DATE is
      select /*+ CARDINALITY(parents 10) INDEX(rpc PK_RPM_PRICE_CHANGE) */
             rpc.price_change_id,
             case
                when parents.date_col < rpc.effective_date then
                   parents.date_col
                else
                   rpc.effective_date
             end roll_date
        from table(cast(L_affected_parent_ids as OBJ_NUM_NUM_DATE_TBL)) parents,
             rpm_price_change rpc
       where rpc.price_change_id = parents.numeric_col1
       union
      select rpc.price_change_id,
             rpc.effective_date
        from rpm_price_change rpc,
             (select /*+ CARDINALITY(ids 10)*/
                     value(ids) price_event_id
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids
               minus
              select /*+ CARDINALITY(parents 10)*/
                     parents.numeric_col1 price_event_id
                from table(cast(L_affected_parent_ids as OBJ_NUM_NUM_DATE_TBL)) parents) pcids
       where rpc.price_change_id = pcids.price_event_id;

BEGIN

   if COPY_RZFR(O_cc_error_tbl,
                I_price_change_ids) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_EXCEPTIONS(o_cc_error_tbl,
                                I_price_change_ids) = 0 then
      return 0;
   end if;

   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                         L_affected_parent_ids,
                                                         I_price_change_ids) = 0 then
      return 0;
   end if;
   --
   -- Remove Price Changes from RPM_ZONE_FUTURE_RETAIL
   --
   open C_REMOVE_IDS;
   fetch C_REMOVE_IDS BULK COLLECT into L_price_change_ids;
   close C_REMOVE_IDS;
   --
   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(o_cc_error_tbl,
                                                  L_price_change_ids) = 0 then
      return 0;
   end if;
   --
   open C_GET_AFFECTED_PAR_ID;
   fetch C_GET_AFFECTED_PAR_ID BULK COLLECT into L_parent_event_ids;
   close C_GET_AFFECTED_PAR_ID;
   --
   if L_parent_event_ids is NOT NULL and
      L_parent_event_ids.COUNT > 0 then

      if ADD_PARENT(O_cc_error_tbl,
                    L_parent_event_ids,
                    NULL) = 0 then
         return 0;
      end if;
   end if;
   --
   for rec in C_ROLL_DATE loop
      if ROLL_FORWARD(O_cc_error_tbl,
                      rec.price_change_id,
                      rec.roll_date,
                      NULL) = 0 then
         return 0;
      end if;
   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_PRICE_CHANGE;

--------------------------------------------------------------------------------------------------------
FUNCTION ADD_PARENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                    I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE,
                    I_skip_validators  IN     BOOLEAN)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_ZONE_FUTURE_RETAIL_SQL.ADD_PARENT';

BEGIN

   if RPM_ZONE_FUTURE_RETAIL_GTT_SQL.MERGE_PRICE_CHANGE(O_cc_error_tbl,
                                                        I_price_change_ids) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END ADD_PARENT;
------------------------------------------------------------------------
END RPM_ZONE_FUTURE_RETAIL_SQL;
/
