CREATE OR REPLACE PACKAGE BODY RPM_CLEARANCE_GTT_SQL AS
--------------------------------------------------------------------------------
   LP_vdate DATE := GET_VDATE;

--------------------------------------------------------------------------------
-- This function will only handle populating clearance reset
-- records in the CLEARANCE_GTT table.  Regular, parent clearances
-- will not be handled by this function.

FUNCTION POPULATE_GTT(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                      I_bulk_cc_pe_id   IN     NUMBER,
                      I_pe_sequence_id  IN     NUMBER,
                      I_thread_number   IN     NUMBER,
                      I_chunk_number    IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CLEARANCE_GTT_SQL.POPULATE_GTT';

   L_clr_resets OBJ_NUM_STR_STR_TBL := NULL;

   L_error_message VARCHAR2(255) := NULL;

   cursor C_LOCK_CLR_RESETS is
      select /*+ USE_NL (rc, il) */
             OBJ_NUM_STR_STR_REC(il.price_event_id,
                                 ROWIDTOCHAR(rc.rowid),
                                 NULL)
        from (select /*+ CARDINALITY(ids, 10) */
                     rbcpi.price_event_id,
                     rbcpi.item,
                     rbcpl.location,
                     rbcpl.zone_node_type
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_bulk_cc_pe_thread rbcpt,
                     rpm_bulk_cc_pe_item rbcpi,
                     rpm_bulk_cc_pe_location rbcpl
               where rbcpt.price_event_id       = VALUE(ids)
                 and rbcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rbcpt.parent_thread_number = I_pe_sequence_id
                 and rbcpt.thread_number        = I_thread_number
                 and rbcpi.bulk_cc_pe_id        = rbcpt.bulk_cc_pe_id
                 and rbcpi.price_event_id       = rbcpt.price_event_id
                 and rbcpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rbcpl.itemloc_id           = rbcpi.itemloc_id
                 and rbcpl.price_event_id       = rbcpi.price_event_id
                 and rbcpl.bulk_cc_pe_id        = rbcpi.bulk_cc_pe_id
                 and rbcpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and NVL(rbcpi.chunk_number, 0) = I_chunk_number) il,
             rpm_clearance_reset rc
       where rc.item      = il.item
         and rc.location  = il.location
         and rc.state    != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
         and (   rc.effective_date is NULL
              or rc.effective_date >= LP_vdate)
         for update of rc.clearance_id nowait;

BEGIN

   open C_LOCK_CLR_RESETS;
   fetch C_LOCK_CLR_RESETS BULK COLLECT into L_clr_resets;
   close C_LOCK_CLR_RESETS;

   if L_clr_resets is NOT NULL and
      L_clr_resets.COUNT > 0 then

      insert into rpm_clearance_gtt
         (price_event_id,
          clearance_id,
          clearance_display_id,
          state,
          reason_code,
          reset_ind,
          item,
          location,
          zone_node_type,
          effective_date,
          out_of_stock_date,
          change_type,
          vendor_funded_ind,
          create_date,
          create_id,
          approval_date,
          approval_id,
          lock_version,
          rc_rowid,
          step_identifier)
      select /*+ CARDINALITY(clr_rec 10)*/ clr_rec.number_1,
             rc.clearance_id,
             rc.clearance_display_id,
             rc.state,
             rc.reason_code,
             1 reset_ind,
             rc.item,
             rc.location,
             rc.zone_node_type,
             rc.effective_date,
             rc.out_of_stock_date,
             -9 change_type,
             0 vendor_funded_ind,
             rc.create_date,
             rc.create_id,
             rc.approval_date,
             rc.approval_id,
             rc.lock_version,
             rc.rowid,
             RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR_GTT
        from table(cast(L_clr_resets as OBJ_NUM_STR_STR_TBL)) clr_rec,
             rpm_clearance_reset rc
       where rc.rowid = CHARTOROWID(clr_rec.string_1);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR_GTT,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      if SQLCODE = -54 then
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(I_price_event_ids(1),
                                                    NULL,
                                                    RPM_CONSTANTS.CONFLICT_ERROR,
                                                    'clearance_reset_used_by_other_process'));
      else
          O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                            CONFLICT_CHECK_ERROR_REC(NULL,
                                                     NULL,
                                                     RPM_CONSTANTS.PLSQL_ERROR,
                                                     SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                        SQLERRM,
                                                                        L_program,
                                                                        TO_CHAR(SQLCODE))));
      end if;

      return 0;

END POPULATE_GTT;

---------------------------------------------------------------------------------
-- This function requires that the CLEARANCE_GTT table have both
-- the parent clearance record and the clearance reset records
-- populated.  Without both, reset records will not be generated/
-- updated correctly.

FUNCTION SAVE_CLEARANCE_RESET(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE,
                              I_bulk_cc_pe_id     IN     NUMBER,
                              I_thread_number     IN     NUMBER,
                              I_chunk_number      IN     NUMBER DEFAULT 0,
                              I_specific_item_loc IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CLEARANCE_GTT_SQL.SAVE_CLEARANCE_RESET';

   L_user_name     VARCHAR2(30) := NULL;
   L_error_message VARCHAR2(255) := NULL;

   cursor C_DEPT is
      select /*+ ORDERED CARDINALITY(ids, 10) */
             distinct rbcpi.dept
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rbcpt,
             rpm_bulk_cc_pe_item rbcpi
       where rbcpt.price_event_id       = VALUE(ids)
         and rbcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
         and rbcpi.bulk_cc_pe_id        = rbcpt.bulk_cc_pe_id
         and rbcpi.thread_number        = I_thread_number
         and rbcpt.thread_number        = I_thread_number
         and NVL(rbcpi.chunk_number, 0) = I_chunk_number
         and rbcpi.price_event_id       = rbcpt.price_event_id
         and rbcpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE;

BEGIN

   select user_name
     into L_user_name
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   for rec IN C_DEPT loop

      merge into rpm_clearance_gtt target
      using (with clr_item_loc as
               (select /*+ ORDERED LEADING(ids) */
                       rbcpi.price_event_id,
                       rbcpi.item,
                       rbcpl.location,
                       rbcpl.zone_node_type
                  from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_bulk_cc_pe_thread rbcpt,
                       rpm_bulk_cc_pe_item rbcpi,
                       rpm_bulk_cc_pe_location rbcpl,
                       rpm_item_loc ril
                 where rbcpt.price_event_id       = VALUE(ids)
                   and rbcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                   and rbcpi.bulk_cc_pe_id        = rbcpt.bulk_cc_pe_id
                   and rbcpi.thread_number        = I_thread_number
                   and rbcpt.thread_number        = I_thread_number
                   and NVL(rbcpi.chunk_number, 0) = I_chunk_number
                   and rbcpi.price_event_id       = rbcpt.price_event_id
                   and rbcpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                   and rbcpi.dept                 = rec.dept
                   and rbcpl.itemloc_id           = rbcpi.itemloc_id
                   and rbcpl.price_event_id       = rbcpi.price_event_id
                   and rbcpl.bulk_cc_pe_id        = rbcpi.bulk_cc_pe_id
                   and rbcpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                   and ril.dept                   = rec.dept
                   and ril.item                   = rbcpi.item
                   and ril.loc                    = rbcpl.location
                   and rownum                     > 0),
             clr_exclusion as
                (-- item loc level
                 select clr.clearance_id,
                        clr.exception_parent_id,
                        il.item,
                        il.location
                   from rpm_clearance clr,
                        clr_item_loc il
                  where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                    and clr.exception_parent_id = il.price_event_id
                    and clr.item                = il.item
                    and clr.location            = il.location
                    and clr.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 union
                 -- item zone level
                 select clr.clearance_id,
                        clr.exception_parent_id,
                        il.item,
                        il.location
                   from rpm_clearance clr,
                        clr_item_loc il,
                        rpm_zone_location rzl
                  where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                    and clr.exception_parent_id = il.price_event_id
                    and clr.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                    and clr.zone_id             = rzl.zone_id
                    and rzl.location            = il.location
                    and clr.item                = il.item
                 union
                 -- parent loc level
                 select clr.clearance_id,
                        clr.exception_parent_id,
                        il.item,
                        il.location
                   from rpm_clearance clr,
                        clr_item_loc il,
                        item_master im
                  where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                    and clr.exception_parent_id = il.price_event_id
                    and clr.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                    and clr.item                = im.item_parent
                    and il.item                 = im.item
                    and clr.location            = il.location
                    and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                    and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 union
                 -- parent zone level
                 select clr.clearance_id,
                        clr.exception_parent_id,
                        il.item,
                        il.location
                   from rpm_clearance clr,
                        clr_item_loc il,
                        rpm_zone_location rzl,
                        item_master im
                  where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                    and clr.exception_parent_id = il.price_event_id
                    and clr.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                    and clr.zone_id             = rzl.zone_id
                    and rzl.location            = il.location
                    and clr.item                = im.item_parent
                    and il.item                 = im.item
                    and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                    and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES)
             select price_event_id,
                    clearance_id,
                    clearance_display_id,
                    state,
                    reason_code,
                    exception_parent_id,
                    reset_ind,
                    item,
                    diff_id,
                    zone_id,
                    location,
                    zone_node_type,
                    effective_date,
                    out_of_stock_date,
                    reset_date,
                    change_type,
                    change_amount,
                    change_currency,
                    change_percent,
                    change_selling_uom,
                    price_guide_id,
                    vendor_funded_ind,
                    funding_type,
                    funding_amount,
                    funding_amount_currency,
                    funding_percent,
                    supplier,
                    deal_id,
                    deal_detail_id,
                    partner_type,
                    partner_id,
                    create_date,
                    create_id,
                    approval_date,
                    approval_id,
                    lock_version,
                    item_explode,
                    loc_explode,
                    zone_node_type_explode
               from (select clr.price_event_id,
                            clr.clearance_id,
                            clr.clearance_display_id,
                            clr.state,
                            clr.reason_code,
                            clr.exception_parent_id,
                            clr.reset_ind,
                            il.item,
                            clr.diff_id,
                            clr.zone_id,
                            il.location,
                            il.zone_node_type,
                            clr.effective_date,
                            clr.out_of_stock_date,
                            clr.reset_date,
                            clr.change_type,
                            clr.change_amount,
                            clr.change_currency,
                            clr.change_percent,
                            clr.change_selling_uom,
                            clr.price_guide_id,
                            clr.vendor_funded_ind,
                            clr.funding_type,
                            clr.funding_amount,
                            clr.funding_amount_currency,
                            clr.funding_percent,
                            clr.supplier,
                            clr.deal_id,
                            clr.deal_detail_id,
                            clr.partner_type,
                            clr.partner_id,
                            clr.create_date,
                            clr.create_id,
                            clr.approval_date,
                            clr.approval_id,
                            clr.lock_version,
                            il.item item_explode,
                            il.location loc_explode,
                            il.zone_node_type zone_node_type_explode
                       from rpm_clearance_gtt clr,
                            clr_item_loc il,
                            clr_exclusion clr_excl
                      where clr.price_event_id    = il.price_event_id
                        and clr.clearance_id      = il.price_event_id
                        and clr.clearance_id      = clr_excl.exception_parent_id (+)
                        and il.item               = clr_excl.item (+)
                        and il.location           = clr_excl.location (+)
                        and clr_excl.clearance_id is NULL
                        --loc move
                        and il.location           NOT IN (select location
                                                            from rpm_location_move lm
                                                           where lm.old_zone_id     = clr.zone_id
                                                             and lm.effective_date <= clr.effective_date
                                                             and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE))) source
      on (    target.price_event_id = source.price_event_id
          and target.item           = source.item_explode
          and target.location       = source.loc_explode
          and target.reset_ind      = 1)
      when MATCHED then
         update
            set target.state             = RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                target.approval_id       = L_user_name,
                target.effective_date    = case
                                              when I_specific_item_loc = 0 then
                                                 case
                                                    when source.reset_ind = 1 then
                                                       target.effective_date
                                                    when NVL(target.effective_date, LP_vdate + 1) > LP_vdate then
                                                       source.reset_date
                                                    else
                                                       target.effective_date
                                                 end
                                              else
                                                 (select MAX(gtt.effective_date)
                                                    from rpm_clearance_gtt gtt
                                                   where gtt.price_event_id = source.price_event_id
                                                     and gtt.reset_ind      = 1
                                                     and gtt.item          != target.item
                                                     and gtt.location      != target.location)
                                           end,
                target.out_of_stock_date = case
                                              when I_specific_item_loc = 0 then
                                                 case
                                                    when source.reset_ind = 1 then
                                                       target.out_of_stock_date
                                                    when NVL(target.out_of_stock_date, LP_vdate + 1) > LP_vdate then
                                                       source.out_of_stock_date
                                                    else
                                                       target.out_of_stock_date
                                                 end
                                              else
                                                 (select MAX(gtt.out_of_stock_date)
                                                    from rpm_clearance_gtt gtt
                                                   where gtt.price_event_id = source.price_event_id
                                                     and gtt.reset_ind      = 1
                                                     and gtt.item          != target.item
                                                     and gtt.location      != target.location)
                                           end,
                target.step_identifier   = RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET
      when NOT MATCHED then
         insert (price_event_id,
                 clearance_id,
                 clearance_display_id,
                 state,
                 reason_code,
                 reset_ind,
                 item,
                 location,
                 zone_node_type,
                 effective_date,
                 out_of_stock_date,
                 change_type,
                 change_amount,
                 change_currency,
                 change_percent,
                 change_selling_uom,
                 price_guide_id,
                 vendor_funded_ind,
                 create_date,
                 create_id,
                 approval_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CLEARANCE_SEQ.NEXTVAL,
                 'reset:'||source.clearance_display_id,
                 RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 source.reason_code,
                 1,
                 source.item_explode,
                 source.loc_explode,
                 source.zone_node_type_explode,
                 case
                    when I_specific_item_loc = 0 then
                       source.reset_date
                    else
                       (select MAX(gtt.reset_date)
                          from rpm_clearance_gtt gtt
                         where gtt.price_event_id = source.price_event_id
                           and gtt.reset_ind      = 0)
                 end, -- effective_date,
                 case
                    when I_specific_item_loc = 0 then
                       source.out_of_stock_date
                    else
                       (select MAX(gtt.out_of_stock_date)
                          from rpm_clearance_gtt gtt
                         where gtt.price_event_id = source.price_event_id
                           and gtt.reset_ind      = 0)
                 end, -- out_of_stock_date,
                 source.change_type,
                 source.change_amount,
                 source.change_currency,
                 source.change_percent,
                 source.change_selling_uom,
                 source.price_guide_id,
                 0,            -- vendor_funded_ind,
                 LP_vdate,     -- create_date,
                 L_user_name,  -- create_id
                 L_user_name,  -- approval_id
                 RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET);

   end loop;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   1, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END SAVE_CLEARANCE_RESET;
--------------------------------------------------------------------------------

FUNCTION SAVE_CLEARANCE_RESET(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                              I_parent_recs       IN     OBJ_NUM_NUM_DATE_TBL,
                              I_bulk_cc_pe_id     IN     NUMBER,
                              I_thread_number     IN     NUMBER,
                              I_chunk_number      IN     NUMBER DEFAULT 0,
                              I_specific_item_loc IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CLEARANCE_GTT_SQL.SAVE_CLEARANCE_RESET';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge into rpm_clearance_gtt target
   using (select clearance_id,
                 clearance_display_id,
                 state,
                 reason_code,
                 exception_parent_id,
                 item,
                 diff_id,
                 zone_id,
                 location,
                 zone_node_type,
                 effective_date,
                 out_of_stock_date,
                 reset_date,
                 change_type,
                 change_amount,
                 change_currency,
                 change_percent,
                 change_selling_uom,
                 price_guide_id,
                 vendor_funded_ind,
                 funding_type,
                 funding_amount,
                 funding_amount_currency,
                 funding_percent,
                 supplier,
                 deal_id,
                 deal_detail_id,
                 partner_type,
                 partner_id,
                 create_date,
                 create_id,
                 approval_date,
                 approval_id,
                 lock_version,
                 price_event_id,
                 rank_value,
                 item_explode,
                 loc_explode,
                 zone_node_type_explode
            from (select clr.clearance_id,
                         clr.clearance_display_id,
                         clr.state,
                         clr.reason_code,
                         clr.exception_parent_id,
                         clr.item,
                         clr.diff_id,
                         clr.zone_id,
                         clr.location,
                         clr.zone_node_type,
                         clr.effective_date,
                         clr.out_of_stock_date,
                         clr.reset_date,
                         clr.change_type,
                         clr.change_amount,
                         clr.change_currency,
                         clr.change_percent,
                         clr.change_selling_uom,
                         clr.price_guide_id,
                         clr.vendor_funded_ind,
                         clr.funding_type,
                         clr.funding_amount,
                         clr.funding_amount_currency,
                         clr.funding_percent,
                         clr.supplier,
                         clr.deal_id,
                         clr.deal_detail_id,
                         clr.partner_type,
                         clr.partner_id,
                         clr.create_date,
                         clr.create_id,
                         clr.approval_date,
                         clr.approval_id,
                         clr.lock_version,
                         il.price_event_id,
                         RANK() OVER(PARTITION BY clr.clearance_id
                                         ORDER BY effective_date desc) as rank_value,
                         il.item item_explode,
                         il.location loc_explode,
                         il.zone_node_type zone_node_type_explode
                    from rpm_clearance clr,
                         (select /*+ ORDERED LEADING(parents) */
                                 rbcpi.price_event_id,
                                 rbcpi.item,
                                 rbcpl.location,
                                 rbcpl.zone_node_type,
                                 parents.numeric_col2 parent_clear_id
                            from table(cast(I_parent_recs as OBJ_NUM_NUM_DATE_TBL)) parents,
                                 rpm_bulk_cc_pe_thread rbcpt,
                                 rpm_bulk_cc_pe_item rbcpi,
                                 rpm_bulk_cc_pe_location rbcpl,
                                 rpm_item_loc ril
                           where rbcpt.price_event_id       = parents.numeric_col1
                             and rbcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                             and rbcpi.bulk_cc_pe_id        = rbcpt.bulk_cc_pe_id
                             and rbcpi.thread_number        = I_thread_number
                             and NVL(rbcpi.chunk_number, 0) = I_chunk_number
                             and rbcpi.price_event_id       = rbcpt.price_event_id
                             and rbcpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and rbcpl.itemloc_id           = rbcpi.itemloc_id
                             and rbcpl.price_event_id       = rbcpi.price_event_id
                             and rbcpl.bulk_cc_pe_id        = rbcpi.bulk_cc_pe_id
                             and rbcpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and ril.dept                   = rbcpi.dept
                             and ril.item                   = rbcpi.item
                             and ril.loc                    = rbcpl.location
                             and rownum                     > 0) il
                   where clr.clearance_id = il.parent_clear_id
                     --loc move
                     and clr.location NOT IN (select location
                                                from rpm_location_move lm
                                               where lm.old_zone_id     = clr.zone_id
                                                 and lm.effective_date <= clr.effective_date
                                                 and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE)) t
           where t.rank_value = 1) source
   on (    target.price_event_id = source.price_event_id
       and target.item           = source.item_explode
       and target.location       = source.loc_explode
       and target.reset_ind      = 1)
   when NOT MATCHED then
      insert (price_event_id,
              clearance_id,
              clearance_display_id,
              state,
              reason_code,
              reset_ind,
              item,
              location,
              zone_node_type,
              effective_date,
              out_of_stock_date,
              change_type,
              change_amount,
              change_currency,
              change_percent,
              change_selling_uom,
              price_guide_id,
              vendor_funded_ind,
              create_date,
              create_id,
              step_identifier)
      values (source.price_event_id,
              RPM_CLEARANCE_SEQ.NEXTVAL,
              'reset:'||source.clearance_display_id,
              RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
              source.reason_code,
              1,
              source.item_explode,
              source.loc_explode,
              source.zone_node_type_explode,
              case -- effective_date,
                 when I_specific_item_loc = 0 then
                    source.reset_date
                 else
                    (select MAX(gtt.effective_date)
                       from rpm_clearance_gtt gtt
                      where gtt.price_event_id = source.price_event_id
                        and gtt.reset_ind      = 1)
              end,
              case -- out_of_stock_date,
                 when I_specific_item_loc = 0 then
                    source.out_of_stock_date
                 else
                    (select MAX(gtt.out_of_stock_date)
                       from rpm_clearance_gtt gtt
                      where gtt.price_event_id = source.price_event_id
                        and gtt.reset_ind      = 1)
              end,
              source.change_type,
              source.change_amount,
              source.change_currency,
              source.change_percent,
              source.change_selling_uom,
              source.price_guide_id,
              0,        -- vendor_funded_ind,
              LP_vdate, -- create_date,
              USER,     -- create_id
              RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET_PARENT);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_SAVE_CLR_RESET_PARENT,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   1, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END SAVE_CLEARANCE_RESET;

----------------------------------------------------------------------------------

FUNCTION REMOVE_CONFLICTS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                          I_conflict_ids IN     CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CLEARANCE_GTT_SQL.REMOVE_CONFLICTS';

BEGIN

   if I_conflict_ids is NOT NULL and
      I_conflict_ids.COUNT > 0 then
      ---
      delete from rpm_clearance_gtt
       where price_event_id IN (select conflict.price_event_id
                                  from table(cast(I_conflict_ids as CONFLICT_CHECK_ERROR_TBL)) conflict);

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_CONFLICTS;

--------------------------------------------------------------------------------

FUNCTION PUSH_BACK(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CLEARANCE_GTT_SQL.PUSH_BACK';

BEGIN

   merge into rpm_clearance_reset rc
   using (select clearance_id,
                 clearance_display_id,
                 state,
                 reason_code,
                 reset_ind,
                 item,
                 location,
                 zone_node_type,
                 effective_date,
                 out_of_stock_date,
                 create_date,
                 create_id,
                 approval_date,
                 approval_id,
                 lock_version,
                 rc_rowid
            from rpm_clearance_gtt
           where reset_ind = 1) rcg
   on (rc.rowid = rcg.rc_rowid)
   when MATCHED then
      update
         set rc.state             = rcg.state,
             rc.approval_date     = DECODE(rcg.state,
                                           RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_date, SYSDATE),
                                           rc.approval_date),
             rc.approval_id       = DECODE(rcg.state,
                                           RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_id, USER),
                                           rc.approval_id),
             rc.effective_date    = rcg.effective_date,
             rc.out_of_stock_date = rcg.out_of_stock_date
   when NOT MATCHED then
      insert (clearance_id,
              clearance_display_id,
              state,
              reason_code,
              item,
              location,
              zone_node_type,
              effective_date,
              out_of_stock_date,
              create_date,
              create_id,
              approval_date,
              approval_id,
              lock_version)
      values (rcg.clearance_id,
              rcg.clearance_display_id,
              rcg.state,
              rcg.reason_code,
              rcg.item,
              rcg.location,
              rcg.zone_node_type,
              rcg.effective_date,
              rcg.out_of_stock_date,
              rcg.create_date,
              rcg.create_id,
              DECODE(rcg.state,
                     RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_date, SYSDATE),
                     NULL),
              DECODE(rcg.state,
                     RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_id, USER),
                     NULL),
              rcg.lock_version);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK;

--------------------------------------------------------------------------------

FUNCTION PUSH_BACK_WS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                      I_bulk_cc_pe_id        IN     NUMBER,
                      I_parent_thread_number IN     NUMBER,
                      I_thread_number        IN     NUMBER,
                      I_chunk_number         IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CLEARANCE_GTT_SQL.PUSH_BACK_WS';

BEGIN

   merge into rpm_clearance_ws rc
   using (select clearance_id,
                 clearance_display_id,
                 state,
                 reason_code,
                 exception_parent_id,
                 reset_ind,
                 item,
                 diff_id,
                 zone_id,
                 location,
                 zone_node_type,
                 effective_date,
                 out_of_stock_date,
                 reset_date,
                 change_type,
                 change_amount,
                 change_currency,
                 change_percent,
                 change_selling_uom,
                 price_guide_id,
                 vendor_funded_ind,
                 funding_type,
                 funding_amount,
                 funding_amount_currency,
                 funding_percent,
                 supplier,
                 deal_id,
                 deal_detail_id,
                 partner_type,
                 partner_id,
                 create_date,
                 create_id,
                 approval_date,
                 approval_id,
                 rc_rowid
            from rpm_clearance_gtt
           where reset_ind = 1) rcg
   on (rc.rc_rowid = rcg.rc_rowid)
   when MATCHED then
      update
         set rc.state             = rcg.state,
             rc.approval_date     = DECODE(rcg.state,
                                           RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_date, SYSDATE),
                                           rc.approval_date),
             rc.approval_id       = DECODE(rcg.state,
                                           RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_id, USER),
                                           rc.approval_id),
             rc.effective_date    = rcg.effective_date,
             rc.reset_date        = rcg.reset_date,
             rc.out_of_stock_date = rcg.out_of_stock_date
   when NOT MATCHED then
      insert (bulk_cc_pe_id,
              parent_thread_number,
              thread_number,
              chunk_number,
              clearance_id,
              clearance_display_id,
              state,
              reason_code,
              exception_parent_id,
              reset_ind,
              item,
              diff_id,
              zone_id,
              location,
              zone_node_type,
              effective_date,
              out_of_stock_date,
              reset_date,
              change_type,
              change_amount,
              change_currency,
              change_percent,
              change_selling_uom,
              price_guide_id,
              vendor_funded_ind,
              funding_type,
              funding_amount,
              funding_amount_currency,
              funding_percent,
              supplier,
              deal_id,
              deal_detail_id,
              partner_type,
              partner_id,
              create_date,
              create_id,
              approval_date,
              approval_id,
              rc_rowid)
      values (I_bulk_cc_pe_id,
              I_parent_thread_number,
              I_thread_number,
              I_chunk_number,
              rcg.clearance_id,
              rcg.clearance_display_id,
              rcg.state,
              rcg.reason_code,
              rcg.exception_parent_id,
              rcg.reset_ind,
              rcg.item,
              rcg.diff_id,
              rcg.zone_id,
              rcg.location,
              rcg.zone_node_type,
              rcg.effective_date,
              rcg.out_of_stock_date,
              rcg.reset_date,
              rcg.change_type,
              rcg.change_amount,
              rcg.change_currency,
              rcg.change_percent,
              rcg.change_selling_uom,
              rcg.price_guide_id,
              rcg.vendor_funded_ind,
              rcg.funding_type,
              rcg.funding_amount,
              rcg.funding_amount_currency,
              rcg.funding_percent,
              rcg.supplier,
              rcg.deal_id,
              rcg.deal_detail_id,
              rcg.partner_type,
              rcg.partner_id,
              rcg.create_date,
              rcg.create_id,
              DECODE(rcg.state,
                     RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_date, SYSDATE),
                     NULL),
              DECODE(rcg.state,
                     RPM_CONSTANTS.PC_APPROVED_STATE_CODE, NVL(rcg.approval_id, USER),
                     NULL),
              rcg.rc_rowid);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK_WS;

 --------------------------------------------------------------------------------

FUNCTION REMOVE_CLEARANCE_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER  IS

   L_program VARCHAR2(50) := 'RPM_CLEARANCE_GTT_SQL.REMOVE_CLEARANCE_RESET';

BEGIN

   merge into rpm_clearance mergeinto
   using (select /*+ ORDERED CARDINALITY(ids 1000)
                     INDEX(clr1 PK_RPM_CLEARANCE)
                     INDEX(im PK_ITEM_MASTER) */
                 clr1.clearance_id,
                 clr2.effective_date reset_date,
                 clr2.out_of_stock_date
            from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_clearance clr1,
                 item_master im,
                 rpm_clearance_reset clr2
           where clr1.clearance_id    = value(ids)
             and clr1.zone_node_type != 1
             and im.item              = clr1.item
             and im.tran_level        = im.item_level
             and clr2.item            = clr1.item
             and clr2.location        = clr1.location
             and clr2.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE) source
   on (mergeinto.clearance_id = source.clearance_id)
   when MATCHED then
      update
         set reset_date        = source.reset_date,
             out_of_stock_date = source.out_of_stock_date;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_CLEARANCE_RESET;

--------------------------------------------------------------------------------

FUNCTION POPULATE_GTT_FOR_RESET(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CLEARANCE_GTT_SQL.POPULATE_GTT_FOR_RESET';

   L_error_message VARCHAR2(255) := NULL;

   cursor C_LOCK is
      select /*+ CARDINALITY(ids 10) */
             rc.rowid
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance_reset rc
       where rc.clearance_id = VALUE(ids)
         and (   rc.effective_date > LP_vdate
              or rc.effective_date is NULL)
         and state           = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
         for update of rc.clearance_id nowait;

BEGIN

   delete rpm_clearance_gtt;

   open C_LOCK;
   close C_LOCK;

   insert into rpm_clearance_gtt (price_event_id,
                                  clearance_id,
                                  clearance_display_id,
                                  state,
                                  reason_code,
                                  reset_ind,
                                  item,
                                  location,
                                  zone_node_type,
                                  effective_date,
                                  out_of_stock_date,
                                  change_type,
                                  vendor_funded_ind,
                                  create_date,
                                  create_id,
                                  approval_date,
                                  approval_id,
                                  lock_version,
                                  rc_rowid,
                                  step_identifier)
      select /*+ CARDINALITY(ids 10) */
             VALUE(ids),
             rc.clearance_id,
             rc.clearance_display_id,
             rc.state,
             rc.reason_code,
             1, -- reset_ind
             rc.item,
             rc.location,
             rc.zone_node_type,
             rc.effective_date,
             rc.out_of_stock_date,
             -9 change_type,
             0 vendor_funded_ind,
             rc.create_date,
             rc.create_id,
             rc.approval_date,
             rc.approval_id,
             rc.lock_version,
             rc.rowid,
             RPM_CONSTANTS.CAPT_GTT_POP_GTT_FOR_RESET
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance_reset rc
       where rc.clearance_id = VALUE(ids)
         and (   rc.effective_date > LP_vdate
              or rc.effective_date is NULL)
         and state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

   insert into rpm_clearance_gtt (price_event_id,
                                  clearance_id,
                                  clearance_display_id,
                                  state,
                                  reason_code,
                                  exception_parent_id,
                                  reset_ind,
                                  item,
                                  diff_id,
                                  zone_id,
                                  location,
                                  zone_node_type,
                                  effective_date,
                                  out_of_stock_date,
                                  reset_date,
                                  change_type,
                                  change_amount,
                                  change_currency,
                                  change_percent,
                                  change_selling_uom,
                                  price_guide_id,
                                  vendor_funded_ind,
                                  funding_type,
                                  funding_amount,
                                  funding_amount_currency,
                                  funding_percent,
                                  supplier,
                                  deal_id,
                                  deal_detail_id,
                                  partner_type,
                                  partner_id,
                                  create_date,
                                  create_id,
                                  approval_date,
                                  approval_id,
                                  lock_version,
                                  rc_rowid,
                                  step_identifier)
      select /*+ INDEX(rc PK_RPM_CLEARANCE)*/
             t.price_event_id,
             rc.clearance_id,
             rc.clearance_display_id,
             state,
             reason_code,
             exception_parent_id,
             0, -- reset_ind
             item,
             diff_id,
             zone_id,
             location,
             zone_node_type,
             effective_date,
             out_of_stock_date,
             reset_date,
             change_type,
             change_amount,
             change_currency,
             change_percent,
             change_selling_uom,
             price_guide_id,
             vendor_funded_ind,
             funding_type,
             funding_amount,
             funding_amount_currency,
             funding_percent,
             supplier,
             deal_id,
             deal_detail_id,
             partner_type,
             partner_id,
             create_date,
             create_id,
             approval_date,
             approval_id,
             lock_version,
             rc.rowid,
             RPM_CONSTANTS.CAPT_GTT_POP_GTT_FOR_RESET
        from rpm_clearance rc,
             (select /*+ CARDINALITY(ids 10) */
                     distinct VALUE(ids) price_event_id,
                     clearance_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_future_retail_gtt gtt
               where gtt.price_event_id   = VALUE(ids)
                 and gtt.clearance_id     is NOT NULL
                 and gtt.clear_start_ind  = RPM_CONSTANTS.START_IND
                 and gtt.action_date     >= LP_vdate) t
       where rc.clearance_id = t.clearance_id;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_POP_GTT_FOR_RESET,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   1, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION
   when OTHERS then

      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_GTT_FOR_RESET;

---------------------------------------------------------------------------------

END RPM_CLEARANCE_GTT_SQL;
/

