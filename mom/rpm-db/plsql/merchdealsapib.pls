CREATE OR REPLACE PACKAGE BODY MERCH_DEALS_API_SQL IS

--------------------------------------------------------------------------------
PROCEDURE CREATE_DEAL(O_return_code    IN OUT  VARCHAR2,
                      O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                      O_deal_ids_table    OUT  DEAL_IDS_TBL,
                      I_deal_head_rec  IN      DEAL_HEAD_REC) IS

   L_program  VARCHAR2(61) := 'MERCH_DEALS_API_SQL.CREATE_DEAL';
   L_boolean_return BOOLEAN := TRUE;
   L_deal_head_rec DEAL_HEAD_REC := I_deal_head_rec;

BEGIN

   PM_DEALS_API_SQL.CREATE_DEAL(O_error_message,
                                L_boolean_return,
                        O_deal_ids_table,
                        L_deal_head_rec);

   if L_boolean_return then
      O_return_code := API_CODES.SUCCESS;
   else
      O_return_code := API_CODES.UNHANDLED_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_return_code := API_CODES.UNHANDLED_ERROR;
END CREATE_DEAL;

--------------------------------------------------------------------------------
PROCEDURE NEW_DEAL_COMP(O_return_code        IN OUT  VARCHAR2,
                        O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                        O_deal_ids_table        OUT  DEAL_IDS_TBL,
                        I_deal_detail_table  IN      DEAL_DETAIL_TBL) IS

   L_program  VARCHAR2(61) := 'MERCH_DEALS_API_SQL.NEW_DEAL_COMP';
   L_boolean_return BOOLEAN := TRUE;
   L_deal_detail_table DEAL_DETAIL_TBL := I_deal_detail_table;

BEGIN

   PM_DEALS_API_SQL.NEW_DEAL_COMP(O_error_message,
                                  L_boolean_return,
                          O_deal_ids_table,
                          L_deal_detail_table);

   if L_boolean_return then
      O_return_code := API_CODES.SUCCESS;
   else
      O_return_code := API_CODES.UNHANDLED_ERROR;
   end if;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_return_code := API_CODES.UNHANDLED_ERROR;
END NEW_DEAL_COMP;

--------------------------------------------------------------------------------
END MERCH_DEALS_API_SQL;
/

