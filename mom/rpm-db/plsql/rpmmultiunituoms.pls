CREATE OR REPLACE PACKAGE RPM_CC_MULTI_UNIT_UOM AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
END;
/

