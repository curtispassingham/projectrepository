CREATE OR REPLACE PACKAGE RPM_ITEM_RECLASS_SQL AS

PROCEDURE POPULATE_RPM_ITEM_MOD(O_return_code          OUT NUMBER,
                                O_error_msg            OUT VARCHAR2,
                                I_item              IN     VARCHAR2,
                                I_message_date      IN     DATE,
                                I_dept              IN     NUMBER,
                                I_class             IN     NUMBER,
                                I_subclass          IN     NUMBER);
-----------------------------------------------------------------------------------------------------------------------

PROCEDURE RECLASS_FUTURE_RETAIL(O_return_code    OUT NUMBER,
                                O_error_msg      OUT VARCHAR2);
-----------------------------------------------------------------------------------------------------------------------

END RPM_ITEM_RECLASS_SQL;
/