CREATE OR REPLACE PACKAGE BODY RPM_CC_PC_PROM_OV AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_CC_PC_PROM_OV.VALIDATE';

   L_error_key VARCHAR2(255)             := NULL;
   L_error_rec CONFLICT_CHECK_ERROR_REC  := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL  := CONFLICT_CHECK_ERROR_TBL();
   L_vdate     DATE                      := GET_VDATE;

   L_pc_promo_overlap_ind RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROMO_OVERLAP_IND%TYPE := NULL;

   cursor C_CHECK is
      select NEW CONFLICT_CHECK_ERROR_REC(gtt.price_event_id,
                                          gtt.future_retail_id,
                                          RPM_CONSTANTS.CONFLICT_ERROR,
                                          'event_causes_price_promotion_overlap',
                                          NULL)
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id NOT IN (select ccet.price_event_id
                                          from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and gtt.action_date                      >= L_vdate
         and price_change_id                      is NOT NULL
         and (   NVL(gtt.on_simple_promo_ind, 0)  = 1
              or NVL(gtt.on_complex_promo_ind, 0) = 1)
      union all
      select NEW CONFLICT_CHECK_ERROR_REC(cspfrg.price_event_id,
                                          gtt.future_retail_id,
                                          RPM_CONSTANTS.CONFLICT_ERROR,
                                          'event_causes_price_promotion_overlap',
                                          cspfrg.cust_segment_promo_id)
        from rpm_future_retail_gtt gtt,
             rpm_cust_segment_promo_fr_gtt cspfrg,
             rpm_promo_fr_item_loc_gtt rpfilg
       where cspfrg.price_event_id NOT IN (select ccet.price_event_id
                                             from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and gtt.price_change_id   is NOT NULL
         and gtt.action_date       >= L_vdate
         and rpfilg.customer_type  is NOT NULL
         and cspfrg.price_event_id = gtt.price_event_id
         and cspfrg.item           = gtt.item
         and cspfrg.location       = gtt.location
         and cspfrg.dept           = gtt.dept
         and cspfrg.action_date    = gtt.action_date
         and cspfrg.price_event_id = rpfilg.price_event_id
         and cspfrg.item           = rpfilg.item
         and cspfrg.location       = rpfilg.location
         and cspfrg.dept           = rpfilg.dept
         and cspfrg.action_date    = rpfilg.action_date
         and cspfrg.customer_type  = rpfilg.customer_type
         and gtt.action_date       <= NVL(rpfilg.detail_end_date, gtt.action_date);

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         ---
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      if RPM_SYSTEM_OPTIONS_SQL.GET_PC_PROMO_OVERLAP_IND(L_pc_promo_overlap_ind,
                                                         L_error_key) = FALSE then

         IO_error_table := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_key));
         return 0;
      end if;

      if L_pc_promo_overlap_ind = RPM_CONSTANTS.BOOLEAN_TRUE then
         return 1;
      end if;

      open C_CHECK;
      fetch C_CHECK BULK COLLECT into L_error_tbl;
      close C_CHECK;

      if L_error_tbl is NOT NULL and
         L_error_tbl.COUNT > 0 then
         ---
         if IO_error_table is NULL or
            IO_error_table.COUNT = 0 then
            ---
            IO_error_table := L_error_tbl;
         else
            for i IN 1..L_error_tbl.COUNT loop
               IO_error_table.EXTEND;
               IO_error_table(IO_error_table.COUNT) := L_error_tbl(i);
            end loop;
         end if;
      end if;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

