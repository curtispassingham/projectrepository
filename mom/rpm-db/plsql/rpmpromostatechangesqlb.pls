CREATE OR REPLACE PACKAGE BODY RPM_PROMO_STATE_CHANGE_SQL AS

FUNCTION UPD_CANCELLED_PROMO_DTL_END_DT(O_error_msg        OUT VARCHAR2,
                                        I_promo_level   IN     NUMBER,
                                        I_child_ids_tbl IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program           VARCHAR2(100) := 'RPM_PROMO_STATE_CHANGE_SQL.UPD_CANCELLED_PROMO_DTL_END_DT';
   L_vdate             DATE          := GET_VDATE();
   L_promo_comp_level  NUMBER(1)     := 1;

BEGIN

   if I_promo_level = L_promo_comp_level then

      update rpm_promo_dtl
         set end_date = L_vdate
       where promo_dtl_id in (select /*+ FIRST_ROWS CARDINALITY (ids 1000) */
                                     rpd.promo_dtl_id
                                from table(cast(I_child_ids_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_promo_dtl rpd
                               where rpd.promo_comp_id = value(ids)
                                 and rpd.state         = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE);

   else

      update rpm_promo_dtl
         set end_date = L_vdate
       where promo_dtl_id in (select /*+ FIRST_ROWS CARDINALITY (ids 1000) */
                                     rpd.promo_dtl_id
                                from table(cast(I_child_ids_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_promo_dtl rpd
                               where rpd.promo_dtl_id = value(ids)
                                 and rpd.state        = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE);
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END UPD_CANCELLED_PROMO_DTL_END_DT;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION CHK_ACTIVE_EXCP_EXCL(O_error_msg             OUT VARCHAR2,
                              O_exception_details     OUT OBJ_NUM_STR_STR_TBL,
                              I_dtl_ids_tbl        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(100) := 'RPM_PROMO_STATE_CHANGE_SQL.CHK_ACTIVE_EXCP_EXCL';

   cursor C_GET_PROMO_EXCP_EXCL is
      select /*+ CARDINALITY(ids 1000) */
             OBJ_NUM_STR_STR_REC(dtl_parent.promo_dtl_id,
                                 dtl_parent.promo_dtl_display_id,
                                 dtl_child.promo_dtl_display_id)
        from table(cast(I_dtl_ids_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl dtl_child,
             rpm_promo_dtl dtl_parent
       where dtl_child.promo_dtl_id         = value(ids)
         and dtl_child.state               != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
         and dtl_child.exception_parent_id  is NOT NULL
         and dtl_child.exception_parent_id  = dtl_parent.promo_dtl_id
         and dtl_parent.exception_parent_id is NULL
         and dtl_parent.state               = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
      union all
      select /*+ CARDINALITY(ids 1000) */
             OBJ_NUM_STR_STR_REC(dtl_grandparent.promo_dtl_id,
                                 dtl_grandparent.promo_dtl_display_id,
                                 dtl_child.promo_dtl_display_id)
        from table(cast(I_dtl_ids_tbl as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl dtl_child,
             rpm_promo_dtl dtl_parent,
             rpm_promo_dtl dtl_grandparent
       where dtl_child.promo_dtl_id         = value(ids)
         and dtl_child.state               != RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
         and dtl_child.exception_parent_id  is NOT NULL
         and dtl_child.exception_parent_id  = dtl_parent.promo_dtl_id
         and dtl_parent.exception_parent_id is NOT NULL
         and dtl_parent.exception_parent_id = dtl_grandparent.promo_dtl_id
         and dtl_grandparent.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE; 

BEGIN

   open  C_GET_PROMO_EXCP_EXCL;
   fetch C_GET_PROMO_EXCP_EXCL BULK COLLECT into O_exception_details;
   close C_GET_PROMO_EXCP_EXCL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CHK_ACTIVE_EXCP_EXCL;
-----------------------------------------------------------------------------------------------------------------------

END RPM_PROMO_STATE_CHANGE_SQL;
/
