CREATE OR REPLACE PACKAGE RPM_PRICE_CHANGE_UPD_RMS_SQL AS
---------------------------------------------------------

PROCEDURE UPDATE_RMS(O_return_code          OUT NUMBER,
                     O_error_message       OUT VARCHAR2,
                     I_pc_id               IN  NUMBER);               
-----------------------------------------------------------------------------
END RPM_PRICE_CHANGE_UPD_RMS_SQL;
/

