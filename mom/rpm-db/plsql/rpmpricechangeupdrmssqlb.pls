CREATE OR REPLACE PACKAGE BODY RPM_PRICE_CHANGE_UPD_RMS_SQL AS
--------------------------------------------------------------
 
-----------------------------------------------------------------------------------------
-- procedure definitions
-----------------------------------------------------------------------------------------
/*Call from UI and send PC_ID PC_TYPE.*/
PROCEDURE UPDATE_RMS(O_return_code      OUT NUMBER,
                     O_error_message    OUT VARCHAR2,
                     I_pc_id            IN  NUMBER )
AS
     L_pc_rec        OBJ_PRICE_CHANGE_REC;
     L_pc_tbl OBJ_PRICE_CHANGE_TBL;
BEGIN

     L_pc_rec := new OBJ_PRICE_CHANGE_REC('PC', I_pc_id);

     L_pc_tbl := new OBJ_PRICE_CHANGE_TBL(L_pc_rec);


-- TODO chengan

EXCEPTION
   WHEN OTHERS THEN
      O_error_message := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_CHANGE_UPD_RMS_SQL.UPDATE_RMS',
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;


END UPDATE_RMS;
---------------------------------------------------------------------------
--------------------------------------------------------------------------------
END RPM_PRICE_CHANGE_UPD_RMS_SQL;
/

