CREATE OR REPLACE PACKAGE RPM_MARGIN_VISIBILITY_SQL AS

FUNCTION GET_MARKUP_CHANGE_DATES(O_error_msg              OUT  VARCHAR2,
                                 I_obj_rpm_promo_tbl      IN     OBJ_RPM_PROMO_TBL,
                                 I_event_type             IN      VARCHAR2,
                                 O_marg_vis_tbl           OUT  OBJ_MARGIN_VISIBILITY_TBL)
RETURN NUMBER;

FUNCTION GET_MARKUP_DETAILS(O_error_msg              OUT VARCHAR2,
                            I_mv_criteria_tbl     IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                            O_mv_markup_chg_dates    OUT OBJ_MV_MARKUP_CHG_DATES_TBL)
RETURN NUMBER;

END RPM_MARGIN_VISIBILITY_SQL;
/