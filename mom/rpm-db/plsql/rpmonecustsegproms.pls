CREATE OR REPLACE PACKAGE RPM_CC_ONE_CUST_SEG_PROMO_MAX  AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
END RPM_CC_ONE_CUST_SEG_PROMO_MAX ;
/

