CREATE OR REPLACE PACKAGE RPM_CLR_RESET AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

FUNCTION VALIDATE(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

--------------------------------------------------------------------------------

END RPM_CLR_RESET;
/