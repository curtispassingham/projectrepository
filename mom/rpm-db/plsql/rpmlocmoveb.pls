CREATE OR REPLACE PACKAGE BODY RPM_LOC_MOVE_SQL AS
--------------------------------------------------------
   LP_vdate DATE := GET_VDATE;

   LP_lm_prom_overlap_behav RPM_SYSTEM_OPTIONS.LOC_MOVE_PROM_OVERLAP_BEHAVIOR%TYPE := NULL;

   LP_batch_name        VARCHAR2(15)  := 'LocMove';
   LP_schedule_batch    VARCHAR2(2)   := 'SC';
   LP_locmove_batch     VARCHAR2(2)   := 'EX';
   LP_schedule_thread   VARCHAR2(95)  := 'com.retek.rpm.domain.locationmove.service.LocationMoveScheduleBulkConflictCheckThreadProcessor';
   LP_preprocess_thread VARCHAR2(100) := 'com.retek.rpm.domain.locationmove.service.LocationMovePreProcessBulkConflictCheckThreadProcessor';

   LP_lm_promo_child_desc VARCHAR2(40) := 'LOC_MOVE_EXCEPTION_EXCLUSION';

   --Instrumentation
   LP_return_code NUMBER         := NULL;
   LP_error_msg   VARCHAR2(2000) := NULL;

   LP_EXTEND_EXISTING       CONSTANT NUMBER(1) := 1;
   LP_END_EXISTING          CONSTANT NUMBER(1) := 2;
   LP_DO_NOT_START_EXISTING CONSTANT NUMBER(1) := 3;

--------------------------------------------------------
--PROTOTYPES
--------------------------------------------------------

FUNCTION SETUP_BULK_HELPER_GTT(O_error_msg           OUT VARCHAR2,
                               I_price_event_type IN     RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------

FUNCTION SETUP_BULK_HELPER_GTT(O_error_msg OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------

FUNCTION POP_ITEM_LOC(O_error_msg OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------

FUNCTION POPULATE_BULK_FROM_IL(O_error_msg                 OUT  VARCHAR2,
                               I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------

FUNCTION SETUP_PRICE_EVENT_INFO(O_error_msg                 OUT  VARCHAR2,
                                I_bulk_cc_pe_id          IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                                I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                                I_price_event_type       IN      RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------

FUNCTION SETUP_PRICE_EVENTS(O_error_msg                 OUT  VARCHAR2,
                            O_data_created              OUT  BOOLEAN,
                            I_price_event_type       IN      RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                            I_max_retry_number       IN      NUMBER,
                            I_bulk_cc_pe_id          IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                            I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION POPULATE_GTT(O_error_msg      OUT VARCHAR2,
                      I_loc_move_id IN     NUMBER,
                      I_dept        IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------
FUNCTION SCRUB_ERROR_TABLE(O_error_msg  OUT VARCHAR2)
RETURN BOOLEAN;

--------------------------------------------------------

FUNCTION SETUP_BULK_HELPER_GTT(O_error_msg           OUT VARCHAR2,
                               I_price_event_type IN     RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_LOC_MOVE_SQL.SETUP_BULK_HELPER_GTT';

BEGIN

/*****************************************************
 *
 * GTT_NUM_NUM_STR_STR_DATE_DATE use in this package
 *
 * number_1    - location_move_id
 * number_2    - bulk_cc_pe_id
 * varchar2_1  - price_event_type
 * varchar2_2  - not used
 * date_1      - not used
 * date_2      - not used
 *
 *****************************************************/

   insert into gtt_num_num_str_str_date_date
      (number_1,
       number_2,
       varchar2_1)
   select lm.location_move_id,
          RPM_BULK_CC_PE_SEQ.NEXTVAL,
          I_price_event_type
     from rpm_location_move lm
    where lm.effective_date <= GET_VDATE + 1
      and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_BULK_HELPER_GTT;
--------------------------------------------------------

FUNCTION SETUP_BULK_HELPER_GTT(O_error_msg   OUT VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   delete
     from gtt_num_num_str_str_date_date;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CLEARANCE) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_LOC_MOVE_SQL.SETUP_BULK_HELPER_GTT',
                                        TO_CHAR(SQLCODE));
      return FALSE;
END SETUP_BULK_HELPER_GTT;
--------------------------------------------------------

FUNCTION POP_ITEM_LOC(O_error_msg    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_LOC_MOVE_SQL.POP_ITEM_LOC';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                merch_level_type,
                                pe_merch_level,
                                thread_number)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                merch_level_type,
                                pe_merch_level,
                                thread_number)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location,
                                    zone_node_type)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    location,
                                    loc_type)
      select distinct inner.bulk_cc_pe_id,
             inner.price_event price_event_id,
             inner.dept,
             inner.class,
             inner.subclass,
             inner.item,
             inner.diff_1 diff_id,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
             inner.location,
             inner.loc_type,
             1 thread_number
        from rpm_item_loc ril,
             (-- price change at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     pc.price_change_id price_event,
                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_price_change pc,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = pc.zone_id
                 and pc.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and pc.diff_id          is NULL
                 and im.item             = pc.item
                 and im.item_level       = im.tran_level
              union all
              -- price change at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     pc.price_change_id price_event,
                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_price_change pc,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = pc.zone_id
                 and pc.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and pc.diff_id          is NULL
                 and im.item_parent      = pc.item
                 and im.item_level       = im.tran_level
              union all
              -- price change at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     pc.price_change_id price_event,
                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_price_change pc,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = pc.zone_id
                 and pc.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and pc.diff_id          is NOT NULL
                 and im.item_parent      = pc.item
                 and im.item_parent      = pc.item
                 and im.item_level       = im.tran_level
                 and (   im.diff_1 = pc.diff_id
                      or im.diff_2 = pc.diff_id
                      or im.diff_3 = pc.diff_id
                      or im.diff_4 = pc.diff_id)
              union all
              -- for items in skulist level price change
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     pc.price_change_id price_event,
                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_price_change pc,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_price_change_skulist rpcs
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = pc.zone_id
                 and pc.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and pc.diff_id          is NULL
                 and pc.skulist          = rpcs.skulist
                 and rpcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rpcs.item
                 and im.item_level       = im.tran_level
              union all
              -- for parent items in a skulist level price change
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     pc.price_change_id price_event,
                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_price_change pc,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_price_change_skulist rpcs
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = pc.zone_id
                 and pc.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and pc.diff_id          is NULL
                 and pc.skulist          = rpcs.skulist
                 and rpcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rpcs.item
                 and im.item_level       = im.tran_level
              union all
              -- clearance at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     cl.clearance_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_clearance cl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = cl.zone_id
                 and cl.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and cl.diff_id          is NULL
                 and im.item             = cl.item
                 and im.item_level       = im.tran_level
              union all
              -- clearance at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     cl.clearance_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_clearance cl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = cl.zone_id
                 and cl.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and cl.diff_id          is NULL
                 and im.item_parent      = cl.item
                 and im.item_level       = im.tran_level
              union all
              -- clearance at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     cl.clearance_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_clearance cl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = cl.zone_id
                 and cl.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and cl.diff_id          is NOT NULL
                 and im.item_parent      = cl.item
                 and im.item_level       = im.tran_level
                 and (   im.diff_1 = cl.diff_id
                      or im.diff_2 = cl.diff_id
                      or im.diff_3 = cl.diff_id
                      or im.diff_4 = cl.diff_id)
              union all
              -- for items in skulist level clearance
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     cl.clearance_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_clearance cl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_clearance_skulist rcs
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = cl.zone_id
                 and cl.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and cl.diff_id          is NULL
                 and cl.skulist          = rcs.skulist
                 and rcs.item_level      = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rcs.item
                 and im.item_level       = im.tran_level
              union all
              -- for parent items in skulist level clearance
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     cl.clearance_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CLEARANCE price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_clearance cl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_clearance_skulist rcs
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = cl.zone_id
                 and cl.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and cl.diff_id          is NULL
                 and cl.skulist          = rcs.skulist
                 and rcs.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rcs.item
                 and im.item_level       = im.tran_level
              union all
              -- simple promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpdmn.item          is NULL
                 and im.dept             = rpdmn.dept
                 and im.class            = NVL(rpdmn.class, im.class)
                 and im.subclass         = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type   is NULL
              union all
              -- simple promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and im.item             = rpdmn.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type   is NULL
              union all
              -- simple promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and im.item_parent      = rpdmn.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type   is NULL
              union all
              -- simple promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpdmn.diff_id       is NOT NULL
                 and im.item_parent      = rpdmn.item
                 and im.item_parent      = rpdmn.item
                 and im.item_level       = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type   is NULL
              union all
              -- for items in skulist level simple promotion
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and rpds.price_event_id = rpd.promo_dtl_id
                 and rpdmn.skulist       = rpds.skulist
                 and rpds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rpds.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type   is NULL
              union all
              -- for parent items in skulist level simple promotion
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and rpds.price_event_id = rpd.promo_dtl_id
                 and rpdmn.skulist       = rpds.skulist
                 and rpds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rpds.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type   is NULL
              union all
              -- threshold promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpdmn.item          is NULL
                 and im.dept             = rpdmn.dept
                 and im.class            = NVL(rpdmn.class, im.class)
                 and im.subclass         = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type   is NULL
              union all
              -- threshold promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and im.item             = rpdmn.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type   is NULL
              union all
              -- threshold promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and im.item_parent      = rpdmn.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type   is NULL
              union all
              -- threshold promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date   <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpdmn.diff_id       is NOT NULL
                 and im.item_parent      = rpdmn.item
                 and im.item_parent      = rpdmn.item
                 and im.item_level       = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type   is NULL
              union all
              -- for items in skulist level threshold promo
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and rpds.price_event_id = rpd.promo_dtl_id
                 and rpdmn.skulist       = rpds.skulist
                 and rpds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item             = rpds.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type   is NULL
              union all
              -- for parent items in skulist level threshold promo
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date  <= LP_vdate + 1
                 and lm.state            = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id      = rpzl.zone_id
                 and rpd.promo_comp_id   = rpc.promo_comp_id
                 and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                 and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id = gtt.number_1
                 and gtt.varchar2_1      = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpdmn.diff_id       is NULL
                 and rpds.price_event_id = rpd.promo_dtl_id
                 and rpdmn.skulist       = rpds.skulist
                 and rpds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent      = rpds.item
                 and im.item_level       = im.tran_level
                 and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type   is NULL
              union all
              -- complex promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
                 and rpdmn.item                  is NULL
                 and im.dept                     = rpdmn.dept
                 and im.class                    = NVL(rpdmn.class, im.class)
                 and im.subclass                 = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NULL
              union all
              -- complex promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
                 and rpdmn.diff_id               is NULL
                 and im.item                     = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NULL
              union all
              -- complex promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
                 and rpdmn.diff_id               is NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NULL
              union all
              -- complex promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
                 and rpdmn.diff_id               is NOT NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NULL
              union all
              -- transaction promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION
                 and rpdmn.item                  is NULL
                 and im.dept                     = rpdmn.dept
                 and im.class                    = NVL(rpdmn.class, im.class)
                 and im.subclass                 = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NULL
              union all
              -- transaction promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION
                 and rpdmn.diff_id               is NULL
                 and im.item                     = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.customer_type           is NULL
              union all
              -- transaction promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION
                 and rpdmn.diff_id               is NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NULL
              union all
              -- transaction promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION
                 and rpdmn.diff_id               is NOT NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NULL
              union all
              -- transaction promo Storewide
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION
                 and rpdmn.merch_type            = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NULL
                 and rpzl.zone_id                = rzl.zone_id
                 and rzl.location                = il.loc
                 and im.item                     = il.item
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Customer segment simple promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                 and rpdmn.item                  is NULL
                 and im.dept                     = rpdmn.dept
                 and im.class                    = NVL(rpdmn.class, im.class)
                 and im.subclass                 = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment simple promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item                     = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment simple promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment simple promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                 and rpdmn.diff_id               is NOT NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- for items in skulist level customer segment simple promo
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                 and rpdmn.diff_id               is NULL
                 and rpds.price_event_id         = rpd.promo_dtl_id
                 and rpdmn.skulist               = rpds.skulist
                 and rpds.item_level             = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item                     = rpds.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- for parent items in skulist level Customer segment simple promo
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                 and rpdmn.diff_id               is NULL
                 and rpds.price_event_id         = rpd.promo_dtl_id
                 and rpdmn.skulist               = rpds.skulist
                 and rpds.item_level             = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent              = rpds.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment threshold promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                 and rpdmn.item                  is NULL
                 and im.dept                     = rpdmn.dept
                 and im.class                    = NVL(rpdmn.class, im.class)
                 and im.subclass                 = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment threshold promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item                     = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment threshold promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment threshold promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                 and rpdmn.diff_id               is NOT NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- for items in skulist level customer segment threshold promo
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                 and rpdmn.diff_id               is NULL
                 and rpds.price_event_id         = rpd.promo_dtl_id
                 and rpdmn.skulist               = rpds.skulist
                 and rpds.item_level             = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item                     = rpds.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- for parent items in skulist level Customer segment threshold promo
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im,
                     rpm_promo_dtl_skulist rpds
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                 and rpdmn.diff_id               is NULL
                 and rpds.price_event_id         = rpd.promo_dtl_id
                 and rpdmn.skulist               = rpds.skulist
                 and rpds.item_level             = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent              = rpds.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment complex promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO
                 and rpdmn.item                  is NULL
                 and im.dept                     = rpdmn.dept
                 and im.class                    = NVL(rpdmn.class, im.class)
                 and im.subclass                 = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment complex promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item                     = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment complex promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment complex promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO
                 and rpdmn.diff_id               is NOT NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment transaction promo at merch hier level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO
                 and rpdmn.item                  is NULL
                 and im.dept                     = rpdmn.dept
                 and im.class                    = NVL(rpdmn.class, im.class)
                 and im.subclass                 = NVL(rpdmn.subclass, im.subclass)
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment transaction promo at tran level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item                     = rpdmn.item
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- Customer segment transaction promo at parent level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO
                 and rpdmn.diff_id               is NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- customer segment transaction promo at parent-diff level
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO price_event_type,
                     im.item,
                     im.diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO
                 and rpdmn.diff_id               is NOT NULL
                 and im.item_parent              = rpdmn.item
                 and im.item_level               = im.tran_level
                 and (   im.diff_1 = rpdmn.diff_id
                      or im.diff_2 = rpdmn.diff_id
                      or im.diff_3 = rpdmn.diff_id
                      or im.diff_4 = rpdmn.diff_id)
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NOT NULL
              union all
              -- transaction promo Storewide
              select lm.location_move_id,
                     lm.location,
                     lm.loc_type,
                     rpd.promo_dtl_id price_event,
                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION price_event_type,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_1,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.number_2 bulk_cc_pe_id
                from rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl,
                     rpm_promo_dtl_merch_node rpdmn,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_location_move lm,
                     gtt_num_num_str_str_date_date gtt,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im
               where lm.effective_date          <= LP_vdate + 1
                 and lm.state                    = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and lm.new_zone_id              = rpzl.zone_id
                 and rpd.promo_comp_id           = rpc.promo_comp_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                 and rpddl.change_type           is NOT NULL
                 and rpd.state                   = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                 and rpd.man_txn_exclusion       = 0
                 and lm.location_move_id         = gtt.number_1
                 and gtt.varchar2_1              = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO
                 and rpdmn.merch_type            = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                 and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                 and rpc.customer_type           is NOT NULL
                 and rpzl.zone_id                = rzl.zone_id
                 and rzl.location                = il.loc
                 and im.item                     = il.item
                 and im.item_level               = im.tran_level
                 and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) inner
      where inner.dept     = ril.dept
        and inner.item     = ril.item
        and inner.location = ril.loc;

   delete
     from gtt_num_num_str_str_date_date gtt
    where NOT EXISTS (select 'x'
                        from rpm_bulk_cc_pe_item il
                       where gtt.number_2 = il.bulk_cc_pe_id);

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POP_ITEM_LOC;

--------------------------------------------------------

FUNCTION POPULATE_BULK_FROM_IL(O_error_msg                 OUT  VARCHAR2,
                               I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_LOC_MOVE_SQL.POPULATE_BULK_FROM_IL';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                               price_event_type,
                               persist_ind,
                               start_state,
                               end_state,
                               user_name,
                               emergency_perm,
                               secondary_ind,
                               asynch_ind,
                               cc_request_group_id,
                               auto_clean_ind,
                               thread_processor_class,
                               status,
                               need_il_explode_ind,
                               location_move_id)
   select distinct gtt.number_2, --bulk_cc_pe_id
          gtt.varchar2_1,        --price_event_type,
          'Y',                   --persist_ind,
          DECODE(gtt.varchar2_1,
                 RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,            RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CLEARANCE,               RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,        RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,       RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,  RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE),   --start_state
          DECODE(gtt.varchar2_1,
                 RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,            RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CLEARANCE,               RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,        RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,       RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,  RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE),   --end_state
          'LocationMoveBatch',      --user_name,
          1,                        --emergency_perm,
          0,                        --secondary_ind,
          0,                        --asynch_ind,
          0,                        --cc_request_group_id,
          0,                        --auto_clean_ind,
          I_thread_processor_class, --thread_processor_class,
          'I',                      --status,
          0,                        --need_il_explode_ind,
          gtt.number_1              --location_move_dd
     from gtt_num_num_str_str_date_date gtt;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type,
                                      rank)
   select distinct gtt.number_2, --bulk_cc_pe_id
          il.price_event_id,     --price_event_id
          gtt.varchar2_1,        --price_event_type
          0                      --rank
     from rpm_bulk_cc_pe_item il,
          gtt_num_num_str_str_date_date gtt
    where il.bulk_cc_pe_id = gtt.number_2;

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END POPULATE_BULK_FROM_IL;

--------------------------------------------------------
--PUBLIC PROCEDURES
--------------------------------------------------------

FUNCTION INITIALIZE_PROCESS(O_error_msg                 OUT VARCHAR2,
                            O_lm_bulkid_petype_obj      OUT OBJ_NUM_NUM_STR_TBL,
                            I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_LOC_MOVE_SQL.INITIALIZE_PROCESS';

   L_trace_name VARCHAR2(15) := 'INP';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

   cursor C_OUTPUT is
      select OBJ_NUM_NUM_STR_REC(lm.location_move_id,
                                 number_2,
                                 varchar2_1)
        from gtt_num_num_str_str_date_date gtt,
             rpm_location_move lm
       where gtt.number_1 (+)   = lm.location_move_id
         and lm.effective_date <= LP_VDATE + 1
         and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LM,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program);

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_LM||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto INITIALIZE_PROCESS_1;

   END;

   <<INITIALIZE_PROCESS_1>>

   if SETUP_BULK_HELPER_GTT(O_error_msg) = FALSE then
      return 0;
   end if;

   if POP_ITEM_LOC(O_error_msg) = FALSE then
      return 0;
   end if;

   if POPULATE_BULK_FROM_IL(O_error_msg,
                            I_thread_processor_class) = FALSE then
      return 0;
   end if;

   open C_OUTPUT;
   fetch C_OUTPUT BULK COLLECT into O_lm_bulkid_petype_obj;
   close C_OUTPUT;

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LM,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program);

   EXCEPTION

      when OTHERS then
         goto INITIALIZE_PROCESS_2;

   END;

   <<INITIALIZE_PROCESS_2>>

   LOGGER.LOG_TIME(L_program,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END INITIALIZE_PROCESS;

--------------------------------------------------------

FUNCTION RECORD_ERROR(O_error_msg               OUT VARCHAR2,
                      I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                      I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                      I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                      I_price_event_id       IN     RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE,
                      I_item                 IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                      I_location             IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                      I_fail_string          IN     RPM_STAGE_ITEM_LOC.ERROR_MSG%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_LOC_MOVE_SQL.RECORD_ERROR';

BEGIN

   insert into rpm_location_move_error  (rpm_location_move_error_id,
                                         location_move_id,
                                         price_event_type,
                                         price_event_id,
                                         dept,
                                         item,
                                         location,
                                         error_msg,
                                         error_date,
                                         retry_number)
      select RPM_LOCATION_MOVE_ERROR_SEQ.NEXTVAL,
             rbcp.location_move_id,
             rbcp.price_event_type,
             rbcpt.price_event_id,
             rpi.dept,
             rpi.item,
             rpl.location,
             I_fail_string,
             LP_vdate,
             0
        from rpm_bulk_cc_pe rbcp,
             rpm_bulk_cc_pe_thread rbcpt,
             rpm_bulk_cc_pe_item rpi,
             rpm_bulk_cc_pe_location rpl
       where rbcp.bulk_cc_pe_id         = I_bulk_cc_pe_id
         and rbcpt.bulk_cc_pe_id        = rbcp.bulk_cc_pe_id
         and rbcpt.parent_thread_number = I_parent_thread_number
         and rbcpt.thread_number        = I_thread_number
         and rbcpt.price_event_id       = NVL(I_price_event_id, rbcpt.price_event_id)
         and rpi.bulk_cc_pe_id          = rbcp.bulk_cc_pe_id
         and rpi.price_event_id         = rbcpt.price_event_id
         and rpi.item                   = NVL(I_item, rpi.item)
         and rpl.bulk_cc_pe_id          = rpi.bulk_cc_pe_id
         and rpl.price_event_id         = rpi.price_event_id
         and rpl.itemloc_id             = rpi.itemloc_id
         and rpl.location               = NVL(I_location, rpl.location);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END RECORD_ERROR;
--------------------------------------------------------

FUNCTION SCRUB_ERROR_TABLE(O_error_msg  OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_LOC_MOVE_SQL.SCRUB_ERROR_TABLE';

BEGIN

   delete from rpm_location_move_error
    where rowid IN (select err.rowid
                      from rpm_price_change pc,
                           rpm_location_move_error err
                     where err.price_event_type   = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                       and pc.price_change_id     = err.price_event_id
                       and (   pc.effective_date  < LP_vdate
                            or pc.state          != RPM_CONSTANTS.PC_APPROVED_STATE_CODE)
                    union all
                    select err.rowid
                      from rpm_clearance cl,
                           rpm_location_move_error err
                     where err.price_event_type   = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                       and cl.clearance_id        = err.price_event_id
                       and (   cl.effective_date  < LP_vdate
                            or cl.state          != RPM_CONSTANTS.PC_APPROVED_STATE_CODE)
                    union all
                    select err.rowid
                      from rpm_promo_dtl rpd,
                           rpm_location_move_error err
                     where err.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO)
                      and rpd.promo_dtl_id      = err.price_event_id
                      and (   rpd.end_date      < LP_vdate
                           or rpd.state         NOT IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                        RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)));

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SCRUB_ERROR_TABLE;

--------------------------------------------------------

FUNCTION SETUP_PRICE_EVENT_INFO(O_error_msg                 OUT  VARCHAR2,
                                I_bulk_cc_pe_id          IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                                I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                                I_price_event_type       IN      RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_LOC_MOVE_SQL.SETUP_PRICE_EVENTS';

BEGIN

   insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                               price_event_type,
                               persist_ind,
                               start_state,
                               end_state,
                               user_name,
                               emergency_perm,
                               secondary_ind,
                               asynch_ind,
                               cc_request_group_id,
                               auto_clean_ind,
                               thread_processor_class,
                               status,
                               need_il_explode_ind,
                               non_asynch_single_txn_ind)
   select distinct I_bulk_cc_pe_id, --bulk_cc_pe_id
          I_price_event_type,       --price_event_type,
          'Y',                      --persist_ind,
          DECODE(I_price_event_type,
                 RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,            RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CLEARANCE,               RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,        RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,       RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,  RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE),   --start_state
          DECODE(I_price_event_type,
                 RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,            RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CLEARANCE,               RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,        RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,       RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,   RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,  RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,     RPM_CONSTANTS.PR_APPROVED_STATE_CODE),   --end_state
          'LocationMoveBatch',      --user_name,
          1,                        --emergency_perm,
          0,                        --secondary_ind,
          0,                        --asynch_ind,
          0,                        --cc_request_group_id,
          0,                        --auto_clean_ind,
          I_thread_processor_class, --thread_processor_class,
          'I',                      --status,
          0,                        --need_il_explode_ind,
          0                         --non_asynch_single_txn_ind
     from dual;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
   select distinct bulk_cc_pe_id,
          price_event_id,
          I_price_event_type
     from rpm_bulk_cc_pe_item
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END SETUP_PRICE_EVENT_INFO;

--------------------------------------------------------

FUNCTION SETUP_PRICE_EVENTS(O_error_msg                 OUT  VARCHAR2,
                            O_data_created              OUT  BOOLEAN,
                            I_price_event_type       IN      RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                            I_max_retry_number       IN      NUMBER,
                            I_bulk_cc_pe_id          IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                            I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'RPM_LOC_MOVE_SQL.SETUP_PRICE_EVENTS';

   L_count NUMBER := 0;

   cursor C_ROWS is
      select COUNT(1)
        from rpm_bulk_cc_pe_item rpi,
             rpm_bulk_cc_pe_location rpl
       where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
         and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
         and rpl.price_event_id = rpi.price_event_id
         and rpl.itemloc_id     = rpi.itemloc_id;

BEGIN

   O_data_created := FALSE;

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                item,
                                thread_number)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                item,
                                thread_number)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    location)
      select I_bulk_cc_pe_id  bulk_cc_pe_id,
             s.price_event_id price_event_id,
             s.dept,
             s.item,
             s.location,
             1 thread_number
        from rpm_location_move_error s
       where s.retry_number     < I_max_retry_number
         and s.price_event_type = I_price_event_type;

   open C_ROWS;
   fetch C_ROWS into L_count;
   close C_ROWS;

   if L_count > 0 then

      O_data_created := TRUE;

      if SETUP_PRICE_EVENT_INFO(O_error_msg,
                                I_bulk_cc_pe_id,
                                I_thread_processor_class,
                                I_price_event_type) = FALSE then
         return FALSE;
      end if;

   end if;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_PRICE_EVENTS;
--------------------------------------------------------

FUNCTION RESTART_PROCESS(O_error_msg                 OUT VARCHAR2,
                         O_bulk_cc_pe_ids            OUT OBJ_NUMERIC_ID_TABLE,
                         I_max_retry_number       IN     NUMBER,
                         I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_LOC_MOVE_SQL.RESTART_PROCESS';

   L_bulk_cc_pe_id RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE := NULL;

   L_bulk_cc_pe_ids OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_data_created   BOOLEAN;

BEGIN

   if SCRUB_ERROR_TABLE(O_error_msg) = FALSE then
      return 0;
   end if;

   L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   if SETUP_PRICE_EVENTS(O_error_msg,
                         L_data_created,
                         RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                         I_max_retry_number,
                         L_bulk_cc_pe_id,
                         I_thread_processor_class) = FALSE then
      return 0;
   end if;

   if L_data_created then

      L_bulk_cc_pe_ids.EXTEND;
      L_bulk_cc_pe_ids(L_bulk_cc_pe_ids.COUNT) := L_bulk_cc_pe_id;
      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   end if;

   if SETUP_PRICE_EVENTS(O_error_msg,
                         L_data_created,
                         RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                         I_max_retry_number,
                         L_bulk_cc_pe_id,
                         I_thread_processor_class) = FALSE then
      return 0;
   end if;

   if L_data_created then

      L_bulk_cc_pe_ids.EXTEND;
      L_bulk_cc_pe_ids(L_bulk_cc_pe_ids.COUNT) := L_bulk_cc_pe_id;
      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   end if;

   if SETUP_PRICE_EVENTS(O_error_msg,
                         L_data_created,
                         RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                         I_max_retry_number,
                         L_bulk_cc_pe_id,
                         I_thread_processor_class) = FALSE then
      return 0;
   end if;

   if L_data_created then

      L_bulk_cc_pe_ids.EXTEND;
      L_bulk_cc_pe_ids(L_bulk_cc_pe_ids.COUNT) := L_bulk_cc_pe_id;
      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   end if;

   if SETUP_PRICE_EVENTS(O_error_msg,
                         L_data_created,
                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                         I_max_retry_number,
                         L_bulk_cc_pe_id,
                         I_thread_processor_class) = FALSE then
      return 0;
   end if;

   if L_data_created then

      L_bulk_cc_pe_ids.EXTEND;
      L_bulk_cc_pe_ids(L_bulk_cc_pe_ids.COUNT) := L_bulk_cc_pe_id;
      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   end if;

   if SETUP_PRICE_EVENTS(O_error_msg,
                         L_data_created,
                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                         I_max_retry_number,
                         L_bulk_cc_pe_id,
                         I_thread_processor_class) = FALSE then
      return 0;
   end if;

   if L_data_created then

      L_bulk_cc_pe_ids.EXTEND;
      L_bulk_cc_pe_ids(L_bulk_cc_pe_ids.COUNT) := L_bulk_cc_pe_id;
      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   end if;

   if SETUP_PRICE_EVENTS(O_error_msg,
                         L_data_created,
                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                         I_max_retry_number,
                         L_bulk_cc_pe_id,
                         I_thread_processor_class) = FALSE then
      return 0;
   end if;

   if L_data_created then

      L_bulk_cc_pe_ids.EXTEND;
      L_bulk_cc_pe_ids(L_bulk_cc_pe_ids.COUNT) := L_bulk_cc_pe_id;
      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   end if;

   O_bulk_cc_pe_ids := L_bulk_cc_pe_ids;

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END RESTART_PROCESS;

--------------------------------------------------------

FUNCTION INCREMENT_ERROR(O_error_msg                OUT VARCHAR2,
                         I_bulk_cc_pe_id        IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                         I_thread_number        IN      RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                         I_parent_thread_number IN      RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_price_event_id       IN      RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE,
                         I_item                 IN      RPM_FUTURE_RETAIL.ITEM%TYPE,
                         I_location             IN      RPM_FUTURE_RETAIL.LOCATION%TYPE,
                         I_fail_string          IN      RPM_STAGE_ITEM_LOC.ERROR_MSG%TYPE)
RETURN NUMBER IS

BEGIN

   merge into rpm_location_move_error err
   using (select thread.price_event_type,
                 thread.price_event_id,
                 rpi.dept,
                 rpi.item,
                 rpl.location
             from rpm_bulk_cc_pe bulk,
                  rpm_bulk_cc_pe_thread thread,
                  rpm_bulk_cc_pe_item rpi,
                  rpm_bulk_cc_pe_location rpl
            where bulk.bulk_cc_pe_id          = I_bulk_cc_pe_id
              --
              and thread.bulk_cc_pe_id        = bulk.bulk_cc_pe_id
              and thread.parent_thread_number = I_parent_thread_number
              and thread.thread_number        = I_thread_number
              and thread.price_event_id       = NVL(I_price_event_id, thread.price_event_id)
              --
              and rpi.bulk_cc_pe_id           = bulk.bulk_cc_pe_id
              and rpi.price_event_id          = thread.price_event_id
              and rpi.item                    = NVL(I_item, rpi.item)
              and rpl.bulk_cc_pe_id           = rpi.bulk_cc_pe_id
              and rpl.price_event_id          = rpi.price_event_id
              and rpl.itemloc_id              = rpi.itemloc_id
              and rpl.location                = NVL(I_location, rpl.location)) use_this
   on (    err.price_event_type = use_this.price_event_type
       and err.price_event_id   = use_this.price_event_id
       and err.item             = use_this.item
       and err.location         = use_this.location)
   when MATCHED then
      update
         set err.retry_number    = err.retry_number + 1,
             err.error_msg       = I_fail_string,
             err.error_date      = get_vdate
   --force fail
   when NOT MATCHED then
      insert(rpm_location_move_error_id)
      values('abc');

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_LOC_MOVE_SQL.INCREMENT_ERROR',
                                        TO_CHAR(SQLCODE));
      return 0;
END INCREMENT_ERROR;

--------------------------------------------------------

FUNCTION REMOVE_ERROR(O_error_msg                OUT VARCHAR2,
                      I_bulk_cc_pe_id        IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                      I_thread_number        IN      RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                      I_parent_thread_number IN      RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                      I_price_event_id       IN      RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE,
                      I_item                 IN      RPM_FUTURE_RETAIL.ITEM%TYPE,
                      I_location             IN      RPM_FUTURE_RETAIL.LOCATION%TYPE)
RETURN NUMBER IS

BEGIN

   delete
     from rpm_location_move_error
    where rpm_location_move_error_id in (select err.rpm_location_move_error_id
                                            from rpm_location_move_error err,
                                                 rpm_bulk_cc_pe_thread thread,
                                                 rpm_bulk_cc_pe_item rpi,
                                                 rpm_bulk_cc_pe_location rpl
                                           where thread.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                             and thread.parent_thread_number = I_parent_thread_number
                                             and thread.thread_number        = I_thread_number
                                             and rpi.bulk_cc_pe_id           = thread.bulk_cc_pe_id
                                             and rpi.price_event_id          = thread.price_event_id
                                             and rpi.item                    = NVL(I_item, rpi.item)
                                             and rpl.bulk_cc_pe_id           = rpi.bulk_cc_pe_id
                                             and rpl.price_event_id          = rpi.price_event_id
                                             and rpl.itemloc_id              = rpi.itemloc_id
                                             and rpl.location                = NVL(I_location, rpl.location)
                                             and err.price_event_type        = thread.price_event_type
                                             and err.price_event_id          = thread.price_event_id
                                             and err.item                    = rpi.item
                                             and err.location                = rpl.location
                                             and (   thread.has_conflict = 0
                                                  or I_item is NOT NULL));

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_LOC_MOVE_SQL.REMOVE_ERROR',
                                        TO_CHAR(SQLCODE));
      return 0;
END REMOVE_ERROR;
--------------------------------------------------------------------------------------------------
FUNCTION RESOLVE_ACTIVE_PROMOTIONS(O_error_msg             OUT VARCHAR2,
                                   O_bulk_su_cc_pe_id      OUT NUMBER,
                                   O_bulk_tu_cc_pe_id      OUT NUMBER,
                                   O_bulk_bu_cc_pe_id      OUT NUMBER,
                                   O_bulk_txp_cc_pe_id     OUT NUMBER,
                                   O_bulk_csu_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctu_cc_pe_id     OUT NUMBER,
                                   O_bulk_ccu_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctxp_cc_pe_id    OUT NUMBER,
                                   I_user_name          IN     VARCHAR2,
                                   I_location_move_id   IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE)
RETURN NUMBER IS

   --Instrumentation
   L_program VARCHAR2(45) := 'RPM_LOC_MOVE_SQL.RESOLVE_ACTIVE_PROMOTIONS';

   L_trace_name VARCHAR2(15) := 'RAP';

   L_start_time     TIMESTAMP := SYSTIMESTAMP;
   L_effective_date DATE      := NULL;

   L_zone_id     RPM_ZONE.ZONE_ID%TYPE           := NULL;
   L_location    RPM_LOCATION_MOVE.LOCATION%TYPE := NULL;
   L_new_zone_id RPM_ZONE.ZONE_ID%TYPE           := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_location_move_id);

   select old_zone_id,
          new_zone_id,
          effective_date,
          location
     into L_zone_id,
          L_new_zone_id,
          L_effective_date,
          L_location
     from rpm_location_move
    where location_move_id = I_location_move_id;

   --Start Simple Promotions
   O_bulk_su_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_location_move_id,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program||' for loc_move_id'|| I_location_move_id);

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_LMS||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto RESOLVE_ACTIVE_PROMOTIONS_1;

   END;

   <<RESOLVE_ACTIVE_PROMOTIONS_1>>

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number,
                                pe_merch_level)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                1, -- thread_number
                                pe_merch_level)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    L_location)
      with simple_promo as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.merch_type
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
             and rpc.customer_type   is NULL),
      simple_promo_skulist as
         (select rpd.promo_dtl_id,
                        rpdmn.dept,
                        rpdmn.class,
                        rpdmn.subclass,
                        rpds.item,
                        rpdmn.diff_id,
                        rpdmn.merch_type,
                        rpds.item_level
                   from rpm_promo_dtl rpd,
                        rpm_promo_comp rpc,
                        rpm_promo_zone_location rpzl,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_promo_dtl_skulist rpds
                  where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                    and rpzl.zone_id        = L_zone_id
                    and rpd.promo_comp_id   = rpc.promo_comp_id
                    and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                    and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                    and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
                    and L_effective_date    > rpd.start_date
                    and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                    and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
                    and rpc.customer_type   is NULL
                    and rpds.price_event_id = rpd.promo_dtl_id
                    and rpdmn.skulist       = rpds.skulist)
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end diff_id,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end item_parent,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end merch_level_type,
             case sp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type           IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = sp.dept
         and im.class                = NVL(sp.class, im.class)
         and im.subclass             = NVL(sp.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1 diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             case sp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = sp.dept
         and im.class        = NVL(sp.class, im.class)
         and im.subclass     = NVL(sp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NOT NULL
         and im.diff_1       is NOT NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             case sp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = sp.dept
         and im.class        = NVL(sp.class, im.class)
         and im.subclass     = NVL(sp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Tran Level for Item Level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = sp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Parent Level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             sp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent  = sp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             sp.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             simple_promo sp
       where sp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = sp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and diffs.id        = im.diff_1
      union all
      -- Parent Level for Parent Level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = sp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             sp.diff_id,
             sp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo sp
       where sp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent  = sp.item
         and (   im.diff_1   = sp.diff_id
              or im.diff_2   = sp.diff_id
              or im.diff_3   = sp.diff_id
              or im.diff_4   = sp.diff_id)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             sp.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
         from item_master im,
              (select diff_group_id id,
                      diff_id
                 from diff_group_detail
               union all
               select diff_id id,
                      diff_id
                 from diff_ids) diffs,
             simple_promo sp
       where sp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item         = sp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and sp.diff_id      = diffs.diff_id
         and diffs.id        = im.diff_1
      union all
      -- following two queris are for skulist level simple promo (items and parent_items in skulist)
      -- Tran Level for Item list - tran item level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sps.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo_skulist sps
       where sps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and sps.item_level  = RPM_CONSTANTS.IL_ITEM_LEVEL
         and im.item         = sps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sps.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo_skulist sps
      where sps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and sps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item_parent  = sps.item
        and im.item_level   = im.tran_level
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level for Item Lists - parent item level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sps.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             simple_promo_skulist sps
      where sps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and sps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = sps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
        and im.diff_1       = diffs.id
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sps.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            simple_promo_skulist sps
      where sps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and sps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = sps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
      union all
      -- Price Event ItemList level Promotion - Parent Item
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo sp,
             rpm_merch_list_detail rmld
       where sp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and sp.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Price Event ItemList level Promotion - Tran Item
      select O_bulk_su_cc_pe_id bulk_cc_pe_id,
             sp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             simple_promo sp,
             rpm_merch_list_detail rmld
       where sp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and sp.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item               = im.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
      select distinct bulk_cc_pe_id,
             price_event_id,
             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id = O_bulk_su_cc_pe_id;

   if SQL%FOUND then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time,
                                  new_promo_end_date)
         values (O_bulk_su_cc_pe_id,                     --BULK_CC_PE_ID
                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION, --PRICE_EVENT_TYPE
                 'Y',                                    --PERSIST_IND
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,     --START_STATE
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,     --END_STATE
                 I_user_name,                            --USER_NAME
                 1,                                      --EMERGENCY_PERM
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,             --SECONDARY_BULK_CC_PE_ID
                 0,                                      --SECONDARY_IND
                 1,                                      --ASYNCH_IND
                 0,                                      --AUTO_CLEAN_IND,
                 LP_schedule_thread,                     --THREAD_PROCESSOR_CLASS
                 0,                                      --NEED_IL_EXPLODE_IND
                 I_location_move_id,
                 LP_vdate,
                 LP_vdate);                               --END_DATE

   else

      O_bulk_su_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Simple Promo - I_loc_move_id: '|| I_location_move_id);

   --Start Threshold Promotions
   O_bulk_tu_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number,
                                pe_merch_level)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                1, -- thread_number
                                pe_merch_level)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    L_location)
      with threshold_promo as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.merch_type
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
             and rpc.customer_type   is NULL),
      threshold_promo_skulist as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpds.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpds.item_level
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
             and rpc.customer_type   is NULL
             and rpds.price_event_id = rpd.promo_dtl_id
             and rpdmn.skulist       = rpds.skulist)
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end diff_id,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end item_parent,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end merch_level_type,
             case tp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             threshold_promo tp
       where tp.merch_type           IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = tp.dept
         and im.class                = NVL(tp.class, im.class)
         and im.subclass             = NVL(tp.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1 diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             case tp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             threshold_promo tp
       where tp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = tp.dept
         and im.class        = NVL(tp.class, im.class)
         and im.subclass     = NVL(tp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.diff_1       is NOT NULL
         and im.item_parent  is NOT NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             case tp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             threshold_promo tp
       where tp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = tp.dept
         and im.class        = NVL(tp.class, im.class)
         and im.subclass     = NVL(tp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Tran Level for Item Level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             threshold_promo tp
       where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = tp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Parent Level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             tp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             threshold_promo tp
       where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent  = tp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             tp.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
         from item_master im,
              (select diff_group_id id,
                      diff_id
                 from diff_group_detail
               union all
               select diff_id id,
                      diff_id
                 from diff_ids) diffs,
             threshold_promo tp
       where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = tp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and diffs.id        = im.diff_1
      union all
      -- Parent Level for Parent Level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             threshold_promo tp
       where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = tp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             tp.diff_id,
             tp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             threshold_promo tp
       where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent  = tp.item
         and (   im.diff_1   = tp.diff_id
              or im.diff_2   = tp.diff_id
              or im.diff_3   = tp.diff_id
              or im.diff_4   = tp.diff_id)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             tp.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            threshold_promo tp
       where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item         = tp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and tp.diff_id      = diffs.diff_id
         and diffs.id        = im.diff_1
      union all
      -- Tran Level for Item list - tran item level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             threshold_promo_skulist tps
       where tps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and tps.item_level  = RPM_CONSTANTS.IL_ITEM_LEVEL
         and im.item         = tps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             threshold_promo_skulist tps
      where tps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and tps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item_parent  = tps.item
        and im.item_level   = im.tran_level
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Parent Diff Level for Item Lists - parent item level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            threshold_promo_skulist tps
      where tps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and tps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = tps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
        and im.diff_1       = diffs.id
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            threshold_promo_skulist tps
      where tps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and tps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = tps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
      union all
      -- Price Event ItemList (PEIL) Promotion - Parent Item
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from threshold_promo tp,
             rpm_merch_list_detail rmld,
             item_master im
       where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and tp.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Price Event ItemList (PEIL) Promotion - Tran Item
      select O_bulk_tu_cc_pe_id bulk_cc_pe_id,
             tp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from threshold_promo tp,
             rpm_merch_list_detail rmld,
             item_master im
       where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and tp.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item               = im.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
      select distinct bulk_cc_pe_id,
             price_event_id,
             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id = O_bulk_tu_cc_pe_id;

   if SQL%FOUND then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time,
                                  new_promo_end_date)
         values (O_bulk_tu_cc_pe_id,                         --BULK_CC_PE_ID
                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,  --PRICE_EVENT_TYPE
                 'Y',                                        --PERSIST_IND
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,         --START_STATE
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,         --END_STATE
                 I_user_name,                                --USER_NAME
                 1,                                          --EMERGENCY_PERM
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,                 --SECONDARY_BULK_CC_PE_ID
                 0,                                          --SECONDARY_IND
                 1,                                          --ASYNCH_IND
                 0,                                          --AUTO_CLEAN_IND,
                 LP_schedule_thread,                         --THREAD_PROCESSOR_CLASS
                 0,                                          --NEED_IL_EXPLODE_IND
                 I_location_move_id,
                 LP_vdate,
                 LP_vdate);                                   --END_DATE

   else

      O_bulk_tu_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Threshold Promo - I_loc_move_id: '|| I_location_move_id);

   --Start BuyGet Promotions

   O_bulk_bu_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number,
                                pe_merch_level)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                1, -- thread_number,
                                pe_merch_level)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    L_location)
      with buyget_promo as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.merch_type
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.COMPLEX_CODE
             and rpc.customer_type   is NULL
             and NOT EXISTS (select 1
                               from rpm_promo_zone_location rpzl1
                              where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                and rpzl1.zone_id      = L_new_zone_id)),
      buyget_promo_skulist as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpds.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpds.item_level
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date ,L_effective_date)
             and rpc.type            = RPM_CONSTANTS.COMPLEX_CODE
             and rpc.customer_type   is NULL
             and rpds.price_event_id = rpd.promo_dtl_id
             and rpdmn.skulist       = rpds.skulist
             and NOT EXISTS (select 1
                               from rpm_promo_zone_location rpzl1
                              where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                and rpzl1.zone_id      = L_new_zone_id))
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end diff_id,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end item_parent,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end merch_level_type,
             case bp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             buyget_promo bp
       where bp.merch_type           IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = bp.dept
         and im.class                = NVL(bp.class, im.class)
         and im.subclass             = NVL(bp.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1 diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             case bp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             buyget_promo bp
       where bp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = bp.dept
         and im.class        = NVL(bp.class, im.class)
         and im.subclass     = NVL(bp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.diff_1       is NOT NULL
         and im.item_parent  is NOT NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             case bp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             buyget_promo bp
       where bp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = bp.dept
         and im.class        = NVL(bp.class, im.class)
         and im.subclass     = NVL(bp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Tran Level for Item Level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             buyget_promo bp
       where bp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = bp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Parent Level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             bp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             buyget_promo bp
       where bp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent  = bp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             bp.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
         from item_master im,
              (select diff_group_id id,
                      diff_id
                 from diff_group_detail
               union all
               select diff_id id,
                      diff_id
                 from diff_ids) diffs,
             buyget_promo bp
       where bp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = bp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and diffs.id        = im.diff_1
      union all
      -- Parent Level for Parent Level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             buyget_promo bp
       where bp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = bp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             bp.diff_id,
             bp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             buyget_promo bp
       where bp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent  = bp.item
         and (   im.diff_1   = bp.diff_id
              or im.diff_2   = bp.diff_id
              or im.diff_3   = bp.diff_id
              or im.diff_4   = bp.diff_id)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             bp.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             buyget_promo bp
       where bp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item         = bp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and bp.diff_id      = diffs.diff_id
         and diffs.id        = im.diff_1
      union all
      -- Tran Level for Item list - tran item level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             buyget_promo_skulist bps
       where bps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and bps.item_level  = RPM_CONSTANTS.IL_ITEM_LEVEL
         and im.item         = bps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             buyget_promo_skulist bps
       where bps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and bps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and im.item_parent  = bps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level for Item Lists - parent item level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            buyget_promo_skulist bps
      where bps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and bps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = bps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
        and im.diff_1       = diffs.id
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            buyget_promo_skulist bps
      where bps.merch_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and bps.item_level  = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = bps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
      union all
      -- Price Event ItemList (PEIL) level Promotion - Parent Item
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from buyget_promo bp,
             rpm_merch_list_detail rmld,
             item_master im
       where bp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and bp.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Price Event ItemList (PEIL) level Promotion - Tran Item
      select O_bulk_bu_cc_pe_id bulk_cc_pe_id,
             bp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from buyget_promo bp,
             rpm_merch_list_detail rmld,
             item_master im
       where bp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and bp.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item               = im.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
      select distinct bulk_cc_pe_id,
             price_event_id,
             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id = O_bulk_bu_cc_pe_id;

   if SQL%FOUND then

      insert into rpm_bulk_cc_pe(bulk_cc_pe_id,
                                 price_event_type,
                                 persist_ind,
                                 start_state,
                                 end_state,
                                 user_name,
                                 emergency_perm,
                                 secondary_bulk_cc_pe_id,
                                 secondary_ind,
                                 asynch_ind,
                                 auto_clean_ind,
                                 thread_processor_class,
                                 need_il_explode_ind,
                                 location_move_id,
                                 create_date_time,
                                 new_promo_end_date)
         values (O_bulk_bu_cc_pe_id,                      --BULK_CC_PE_ID
                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION, --PRICE_EVENT_TYPE
                 'Y',                                     --PERSIST_IND
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,      --START_STATE
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,      --END_STATE
                 I_user_name,                             --USER_NAME
                 1,                                       --EMERGENCY_PERM
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,              --SECONDARY_BULK_CC_PE_ID
                 0,                                       --SECONDARY_IND
                 1,                                       --ASYNCH_IND
                 0,                                       --AUTO_CLEAN_IND,
                 LP_schedule_thread,                      --THREAD_PROCESSOR_CLASS
                 0,                                       --NEED_IL_EXPLODE_IND
                 I_location_move_id,
                 LP_vdate,
                 LP_vdate);                                --END_DATE

   else

      O_bulk_bu_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Complex Promo - I_loc_move_id: '|| I_location_move_id);

   --Start Transaction Promotions

   O_bulk_txp_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      when head_rnk = 1 then
         into rpm_bulk_cc_pe (bulk_cc_pe_id,
                              price_event_type,
                              persist_ind,
                              start_state,
                              end_state,
                              user_name,
                              emergency_perm,
                              secondary_bulk_cc_pe_id,
                              secondary_ind,
                              asynch_ind,
                              auto_clean_ind,
                              thread_processor_class,
                              need_il_explode_ind,
                              location_move_id,
                              create_date_time,
                              new_promo_end_date)
                      values (O_bulk_txp_cc_pe_id,
                              RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION, --price_event_type
                              'Y',                                         --persist_ind
                              RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,          --start_state
                              RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,          --end_state
                              I_user_name,
                              1,                                           --emergency_perm
                              RPM_BULK_CC_PE_SEQ.NEXTVAL,                  --secondary_bulk_cc_pe_id
                              0,                                           --secondary_ind
                              1,                                           --asynch_ind
                              0,                                           --auto_clean_ind,
                              LP_schedule_thread,                          --thread_processor_class
                              0,                                           --need_il_explode_ind
                              I_location_move_id,
                              LP_vdate,                                    --create_date_time
                              LP_vdate)                                    --end_date
      when thread_rnk = 1 then
         into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                     price_event_id,
                                     price_event_display_id,
                                     price_event_type,
                                     rank,
                                     man_txn_excl_exists,
                                     man_txn_exclusion_parent_id,
                                     elig_for_sys_gen_exclusions)
                             values (O_bulk_txp_cc_pe_id,
                                     promo_dtl_id,
                                     promo_dtl_display_id,
                                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                     0, -- rank
                                     man_txn_excl_exists,
                                     exception_parent_id,
                                     0) -- elig_for_sys_gen_exclusions
      when 1 = 1 then
         into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                   price_event_id,
                                   itemloc_id,
                                   dept,
                                   class,
                                   subclass,
                                   item,
                                   diff_id,
                                   item_parent,
                                   merch_level_type,
                                   thread_number,
                                   pe_merch_level,
                                   txn_man_excluded_item)
                           values (O_bulk_txp_cc_pe_id,
                                   promo_dtl_id,
                                   RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                   dept,
                                   class,
                                   subclass,
                                   item,
                                   diff_id,
                                   item_parent,
                                   merch_level_type,
                                   1, -- thread_number
                                   pe_merch_level,
                                   txn_man_excluded_item)
      when 1 = 1 then
         into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                       price_event_id,
                                       itemloc_id,
                                       location,
                                       zone_node_type)
                               values (O_bulk_txp_cc_pe_id,
                                       promo_dtl_id,
                                       RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                       L_location,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_STORE)
      with txp_promo as
         (select rpd.promo_dtl_id,
                 rpd.promo_dtl_display_id,
                 rpd.exception_parent_id,
                 rpd.man_txn_exclusion,
                 rpd.man_txn_excl_exists,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.skulist,
                 rpdmn.merch_type
            from rpm_promo_zone_location rpzl,
                 rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id                = L_zone_id
             and rpd.state                   = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and L_effective_date            > rpd.start_date
             and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
             and rpd.promo_dtl_id            = rpzl.promo_dtl_id
             and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
             and rpd.promo_comp_id           = rpc.promo_comp_id
             and rpc.customer_type           is NULL
             and (   rpdmn.merch_type           != RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  or rpd.sys_generated_exclusion = 1)
             and rpdmn.skulist               is NULL
             and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
             and NOT EXISTS (select 1
                               from rpm_promo_zone_location rpzl1
                              where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                and rpzl1.zone_id      = L_new_zone_id)),  -- end of txp_promo
      txp_promo_skulist as
         (select rpd.promo_dtl_id,
                 rpd.promo_dtl_display_id,
                 rpd.exception_parent_id,
                 rpd.man_txn_exclusion,
                 rpd.man_txn_excl_exists,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.diff_id,
                 rpdmn.skulist,
                 rpdmn.merch_type,
                 rpds.item,
                 rpds.item_level
            from rpm_promo_zone_location rpzl,
                 rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id                = L_zone_id
             and rpd.sys_generated_exclusion = 0
             and rpd.state                   = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and L_effective_date            > rpd.start_date
             and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
             and rpd.promo_dtl_id            = rpzl.promo_dtl_id
             and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
             and rpc.customer_type           is NULL
             and rpd.promo_comp_id           = rpc.promo_comp_id
             and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
             and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpds.price_event_id         = rpd.promo_dtl_id
             and rpdmn.skulist               = rpds.skulist
             and NOT EXISTS (select 1
                               from rpm_promo_zone_location rpzl1
                              where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                and rpzl1.zone_id      = L_new_zone_id)),  -- end of txp_promo_skulist
      txn_dtl_exclusions as
         (-- merch hierarchy
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 tp.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 subclass sc
           where tp.man_txn_exclusion = 1
             and tp.merch_type IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                   RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                   RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
             and sc.dept       = tp.dept
             and sc.class      = NVL(tp.class, sc.class)
             and sc.subclass   = NVL(tp.subclass, sc.subclass)
          -- Explode to tran item for exclusion at skulist parent item
          union all
          select tps.dept,
                 tps.class,
                 tps.subclass,
                 im.item,
                 tps.diff_id,
                 tps.merch_type,
                 tps.promo_dtl_id,
                 tps.exception_parent_id
            from txp_promo_skulist tps,
                 item_master im
           where tps.man_txn_exclusion = 1
             and tps.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and tps.item_level        = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
             and im.status             = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind       = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.item_parent        = tps.item
             and im.item_level         = im.tran_level
          union all
          -- exclusion at skulist level for item or parent item
          select tps.dept,
                 tps.class,
                 tps.subclass,
                 tps.item,
                 tps.diff_id,
                 tps.merch_type,
                 tps.promo_dtl_id,
                 tps.exception_parent_id
            from txp_promo_skulist tps
           where tps.man_txn_exclusion = 1
             and tps.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
          -- Explode to tran item for exclusion at peil parent item level
          union all
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 im.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 rpm_merch_list_detail rmld,
                 item_master im
           where tp.man_txn_exclusion = 1
             and tp.merch_type        = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id   = tp.price_event_itemlist
             and rmld.merch_level     = RPM_CONSTANTS.PARENT_MERCH_TYPE
             and im.item_parent       = rmld.item
             and im.item_level        = im.tran_level
             and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          union all
          -- exclusion at peil level for item or parent item level
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 rmld.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 rpm_merch_list_detail rmld
           where tp.man_txn_exclusion = 1
             and tp.merch_type        = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id   = tp.price_event_itemlist
          union all
          -- explode to tran level for exclusion at parent and parent diff item
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 im.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 item_master im
           where tp.man_txn_exclusion = 1
             and tp.merch_type        IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                          RPM_CONSTANTS.PARENT_ITEM_DIFF)
             and im.item_parent       = tp.item
             and (   tp.diff_id       is NULL
                  or (    tp.diff_id  is NOT NULL
                      and (   im.diff_1 = tp.diff_id
                           or im.diff_2 = tp.diff_id
                           or im.diff_3 = tp.diff_id
                           or im.diff_4 = tp.diff_id)))
             and im.item_level        = im.tran_level
             and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          union all
          -- exclusion at tran item or parent diff item
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 tp.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp
           where tp.man_txn_exclusion = 1
             and tp.merch_type        IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                          RPM_CONSTANTS.PARENT_ITEM_DIFF)) -- end of txn_dtl_exclusions
      select promo_dtl_id,
             promo_dtl_display_id,
             dept,
             class,
             subclass,
             item,
             diff_id,
             item_parent,
             merch_level_type,
             pe_merch_level,
             txn_man_excluded_item,
             exception_parent_id,
             man_txn_excl_exists,
             ROW_NUMBER() OVER (PARTITION BY O_bulk_txp_cc_pe_id
                                    ORDER BY NULL) head_rnk,
             ROW_NUMBER() OVER (PARTITION BY O_bulk_txp_cc_pe_id,
                                             promo_dtl_id
                                    ORDER BY NULL) thread_rnk
        from (-- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     case im.item_level
                        when im.tran_level then
                           im.diff_1
                        else
                           NULL
                     end diff_id,
                     case im.item_level
                        when im.tran_level then
                           im.item_parent
                        else
                           NULL
                     end item_parent,
                     case im.item_level
                        when im.tran_level then
                           RPM_CONSTANTS.ITEM_MERCH_TYPE
                        else
                           RPM_CONSTANTS.PARENT_MERCH_TYPE
                     end merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.dept                = tp.dept
                 and im.class               = NVL(tp.class, im.class)
                 and im.subclass            = NVL(tp.subclass, im.subclass)
                 and (   (    im.item_level  = im.tran_level
                          and im.item_parent is NOT NULL)
                      or im.item_level       = im.tran_level - 1)
                 and im.status              = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind        = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id        = tde.exception_parent_id (+)
                 and im.item                = NVL(tde.item (+), im.item)
                 and NVL(im.diff_1, '-999') = NVL(tde.diff_id (+), NVL(im.diff_1, '-999'))
                 and im.dept                = NVL(tde.dept (+), im.dept)
                 and im.class               = NVL(tde.class (+), im.class)
                 and im.subclass            = NVL(tde.subclass (+), im.subclass)
              union all
               -- Parent-Diff Level items for Merch Hierarchy Level Promotion
              select distinct
                     tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item_parent item,
                     im.diff_1 diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.dept         = tp.dept
                 and im.class        = NVL(tp.class, im.class)
                 and im.subclass     = NVL(tp.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.diff_1       is NOT NULL
                 and im.item_parent  is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item_parent  = NVL(tde.item (+), im.item_parent)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Tran level items with no parents for Merch Hierarchy Level Promotion
              select distinct
                     tp.promo_dtl_id price_event_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.dept         = tp.dept
                 and im.class        = NVL(tp.class, im.class)
                 and im.subclass     = NVL(tp.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.item_parent  is NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
               -- Tran Level for Item Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0)
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = tde.item (+)
              union all
              -- Tran Level for Parent Level Promotion
              select distinct
                     tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     im.diff_1 diff_id,
                     tp.item item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item_parent  = tp.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = tde.item (+)
              union all
              -- Parent/Diff level data for price events at the Parent level
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     tp.item,
                     diffs.diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                 from item_master im,
                      (select diff_group_id id,
                              diff_id
                         from diff_group_detail
                       union all
                       select diff_id id,
                              diff_id
                         from diff_ids) diffs,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level - 1
                 and im.diff_1       is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and diffs.id        = im.diff_1
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and diffs.diff_id   = NVL(tde.diff_id (+), diffs.diff_id)
              union all
              -- Parent Level for Parent Level Promotion
              select tp.promo_dtl_id ,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level - 1
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = tde.item (+)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
              union all
              -- Tran Level Item for Price Event at Parent Diff Level Item and DIFF_ID is part of DIFF_1
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     tp.diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item_parent  = tp.item
                 and im.diff_1       = tp.diff_id
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item_parent  = NVL(tde.item (+), im.item_parent)
              union all
              -- Tran Level Item for Price Event at Parent Diff Level Item
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     tp.diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item_parent  = tp.item
                 and (   im.diff_2   = tp.diff_id
                      or im.diff_3   = tp.diff_id
                      or im.diff_4   = tp.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
              union all
              -- Parent Diff data for price event at Parent Diff Level
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     tp.diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level - 1
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.diff_id      = diffs.diff_id
                 and diffs.id        = im.diff_1
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
              union all
              -- Tran Level for Item list - tran item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     txn_dtl_exclusions tde
               where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and tps.item_level   = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item          = tps.item
                 and im.item_level    = im.tran_level
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tps.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item          = tde.item (+)
              union all
              -- Tran Level for Item Lists - parent item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     im.diff_1 diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     txn_dtl_exclusions tde
               where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and tps.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent   = tps.item
                 and im.item_level    = im.tran_level
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tps.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item          = NVL(tde.item (+), im.item)
              union all
              -- Parent Diff Level for Item Lists - parent item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     diffs.diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                    txn_dtl_exclusions tde
              where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                and tps.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                and im.item          = tps.item
                and im.diff_1        is NOT NULL
                and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                and im.item_level    = im.tran_level - 1
                and im.diff_1        = diffs.id
                and tps.promo_dtl_id = tde.exception_parent_id (+)
                and tps.item         = NVL(tde.item (+), tps.item)
                and diffs.diff_id    = NVL(tde.diff_id (+), diffs.diff_id)
              union all
              -- Parent Level for Item Lists - parent item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     txn_dtl_exclusions tde
               where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and tps.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item          = tps.item
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level    = im.tran_level - 1
                 and tps.promo_dtl_id = tde.exception_parent_id (+)
                 and tps.item         = NVL(tde.item (+), tps.item)
                 and im.diff_1        = NVL(tde.diff_id (+), im.diff_1)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Tran Item
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item               = im.item
                 and im.item_level           = im.tran_level
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and im.item                 = tde.item (+)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Parent Item
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     im.diff_1 diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item               = im.item_parent
                 and im.item_level           = im.tran_level
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and im.item                 = NVL(tde.item (+), im.item)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Parent Diff Item Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     diffs.diff_id diff_id,
                     NULL,  -- item_parent
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item               = im.item
                 and im.diff_1               is NOT NULL
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level           = im.tran_level -1
                 and im.diff_1               = diffs.id
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and rmld.item               = NVL(tde.item (+), rmld.item)
                 and diffs.diff_id           = NVL(tde.diff_id (+), diffs.diff_id)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Parent Item Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL, -- diff_id,
                     NULL, -- item_parent
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item               = im.item
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level           = im.tran_level - 1
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and rmld.item               = NVL(tde.item (+), rmld.item)
                 and im.diff_1               = NVL(tde.diff_id (+), im.diff_1)
              union all
              -- parent level for Storewide Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     case im.item_level
                        when im.tran_level then
                           im.diff_1
                        else
                           NULL
                     end diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level
                 and im.item_parent  is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- tran level item with parents for Storewide Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     case im.item_level
                        when im.tran_level then
                           im.diff_1
                        else
                           NULL
                     end diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level - 1
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Parent-Diff Level items for Storewide Level Promotion
              select distinct tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     diffs.diff_id,
                     NULL, -- item_parent
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level - 1
                 and im.diff_1       is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.diff_1       = diffs.id
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and diffs.diff_id   = NVL(tde.diff_id (+), diffs.diff_id)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Tran level items with no parents for Storewide Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL,
                     NULL, -- item_parent
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level
                 and im.item_parent  is NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass));

   if SQL%NOTFOUND then

      O_bulk_txp_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Transaction Promo - I_loc_move_id: '|| I_location_move_id);

   --start customer segment simple promotions
   O_bulk_csu_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number,
                                pe_merch_level)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                1, -- thread_number
                                pe_merch_level)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    L_location)
      with cs_simple_promo as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.merch_type
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
             and rpc.customer_type   is NOT NULL),
      cs_simple_promo_skulist as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpds.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpds.item_level
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.SIMPLE_CODE
             and rpc.customer_type   is  NOT NULL
             and rpds.price_event_id = rpd.promo_dtl_id
             and rpdmn.skulist       = rpds.skulist)
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end diff_id,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end item_parent,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end merch_level_type,
             case csp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             cs_simple_promo csp
       where csp.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = csp.dept
         and im.class                = NVL(csp.class, im.class)
         and im.subclass             = NVL(csp.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
       -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1 diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             case csp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             cs_simple_promo csp
       where csp.merch_type  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = csp.dept
         and im.class        = NVL(csp.class, im.class)
         and im.subclass     = NVL(csp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.diff_1       is NOT NULL
         and im.item_parent  is NOT NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             case csp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_simple_promo csp
       where csp.merch_type  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = csp.dept
         and im.class        = NVL(csp.class, im.class)
         and im.subclass     = NVL(csp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_simple_promo csp
       where csp.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item        = csp.item
         and im.item_level  = im.tran_level
      union all
      -- Tran Level for Parent Level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             csp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_simple_promo csp
       where csp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent  = csp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             csp.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
         from item_master im,
              (select diff_group_id id,
                      diff_id
                 from diff_group_detail
               union all
               select diff_id id,
                      diff_id
                 from diff_ids) diffs,
             cs_simple_promo csp
       where csp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = csp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and diffs.id        = im.diff_1
      union all
      -- Parent Level for Parent Level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_simple_promo csp
       where csp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = csp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
       union all
       -- Tran Level Item for Price Event at Parent Diff Level Item
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             csp.diff_id,
             csp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_simple_promo csp
       where csp.merch_type  = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent  = csp.item
         and (   im.diff_1   = csp.diff_id
              or im.diff_2   = csp.diff_id
              or im.diff_3   = csp.diff_id
              or im.diff_4   = csp.diff_id)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             csp.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_simple_promo csp
       where csp.merch_type  = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item         = csp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and csp.diff_id     = diffs.diff_id
         and diffs.id        = im.diff_1
      union all
      -- Tran Level for Item list - tran item level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_simple_promo_skulist csps
       where csps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and csps.item_level = RPM_CONSTANTS.IL_ITEM_LEVEL
         and im.item         = csps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_simple_promo_skulist csps
      where csps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and csps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item_parent  = csps.item
        and im.item_level   = im.tran_level
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level for Item Lists - parent item level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            cs_simple_promo_skulist csps
      where csps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and csps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = csps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
        and im.diff_1       = diffs.id
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            cs_simple_promo_skulist csps
      where csps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and csps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = csps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
      union all
      -- Price Event ItemList level Promotion - Parent Item Level
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from cs_simple_promo csp,
             rpm_merch_list_detail rmld,
             item_master im
       where csp.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id = csp.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item          = im.item_parent
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Price Event ItemList level Promotion - Tran Item Level
      select O_bulk_csu_cc_pe_id bulk_cc_pe_id,
             csp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from cs_simple_promo csp,
             rpm_merch_list_detail rmld,
             item_master im
       where csp.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id = csp.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item          = im.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
      select distinct bulk_cc_pe_id,
             price_event_id,
             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id = O_bulk_csu_cc_pe_id;

   if SQL%FOUND then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time,
                                  new_promo_end_date)
          values (O_bulk_csu_cc_pe_id,                         --BULK_CC_PE_ID
                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO, --PRICE_EVENT_TYPE
                  'Y',                                         --PERSIST_IND
                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,          --START_STATE
                  RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,          --END_STATE
                  I_user_name,                                 --USER_NAME
                  1,                                           --EMERGENCY_PERM
                  RPM_BULK_CC_PE_SEQ.NEXTVAL,                  --SECONDARY_BULK_CC_PE_ID
                  0,                                           --SECONDARY_IND
                  1,                                           --ASYNCH_IND
                  0,                                           --AUTO_CLEAN_IND,
                  LP_schedule_thread,                          --THREAD_PROCESSOR_CLASS
                  0,                                           --NEED_IL_EXPLODE_IND
                  I_location_move_id,
                  LP_vdate,
                  LP_vdate);                                    --END_DATE

   else

      O_bulk_csu_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Cust Seg Simple Promo - I_loc_move_id: '|| I_location_move_id);

   --start customer segment threshold promotion

   O_bulk_ctu_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number,
                                pe_merch_level)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                1, -- thread_number
                                pe_merch_level)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    L_location)
      with cs_threshold_promo as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.merch_type
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
             and rpc.customer_type   is NOT NULL),
      cs_threshold_promo_skulist as
         (select rpd.promo_dtl_id,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpds.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpds.item_level
            from rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id        = L_zone_id
             and rpd.promo_comp_id   = rpc.promo_comp_id
             and rpd.promo_dtl_id    = rpzl.promo_dtl_id
             and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
             and L_effective_date    > rpd.start_date
             and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
             and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
             and rpc.customer_type   is  NOT NULL
             and rpds.price_event_id = rpd.promo_dtl_id
             and rpdmn.skulist       = rpds.skulist)
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end diff_id,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end item_parent,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end merch_level_type,
             case ctp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             cs_threshold_promo ctp
       where ctp.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = ctp.dept
         and im.class                = NVL(ctp.class, im.class)
         and im.subclass             = NVL(ctp.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1 diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             case ctp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             cs_threshold_promo ctp
       where ctp.merch_type  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = ctp.dept
         and im.class        = NVL(ctp.class, im.class)
         and im.subclass     = NVL(ctp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.diff_1       is NOT NULL
         and im.item_parent  is NOT NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             case ctp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_threshold_promo ctp
       where ctp.merch_type  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = ctp.dept
         and im.class        = NVL(ctp.class, im.class)
         and im.subclass     = NVL(ctp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_threshold_promo ctp
       where ctp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = ctp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Parent Level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             ctp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_threshold_promo ctp
       where ctp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent  = ctp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
       union all
       -- Parent/Diff level data for price events at the Parent level
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             ctp.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
         from item_master im,
              (select diff_group_id id,
                      diff_id
                 from diff_group_detail
               union all
               select diff_id id,
                      diff_id
                 from diff_ids) diffs,
             cs_threshold_promo ctp
       where ctp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = ctp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and diffs.id        = im.diff_1
      union all
      -- Parent Level for Parent Level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_threshold_promo ctp
       where ctp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = ctp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             ctp.diff_id,
             ctp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_threshold_promo ctp
       where ctp.merch_type  = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent  = ctp.item
         and (   im.diff_1   = ctp.diff_id
              or im.diff_2   = ctp.diff_id
              or im.diff_3   = ctp.diff_id
              or im.diff_4   = ctp.diff_id)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             ctp.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_threshold_promo ctp
       where ctp.merch_type  = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item         = ctp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and ctp.diff_id     = diffs.diff_id
         and diffs.id        = im.diff_1
      union all
      -- Tran Level for Item list - tran item level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_threshold_promo_skulist ctps
       where ctps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and ctps.item_level = RPM_CONSTANTS.IL_ITEM_LEVEL
         and im.item         = ctps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_threshold_promo_skulist ctps
      where ctps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and ctps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item_parent  = ctps.item
        and im.item_level   = im.tran_level
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level for Item Lists - parent item level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            cs_threshold_promo_skulist ctps
      where ctps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and ctps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = ctps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
        and im.diff_1       = diffs.id
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            cs_threshold_promo_skulist ctps
      where ctps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and ctps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = ctps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
      union all
      -- Price Event ItemList - Parent Item Level
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from cs_threshold_promo ctp,
             rpm_merch_list_detail rmld,
             item_master im
       where ctp.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id = ctp.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item          = im.item_parent
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Price Event ItemList - Tran Item Level
      select O_bulk_ctu_cc_pe_id bulk_cc_pe_id,
             ctp.promo_dtl_id    price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from cs_threshold_promo ctp,
             rpm_merch_list_detail rmld,
             item_master im
       where ctp.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id = ctp.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item          = im.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
      select distinct bulk_cc_pe_id,
             price_event_id,
             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id = O_bulk_ctu_cc_pe_id;

   if SQL%FOUND then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time,
                                  new_promo_end_date)
         values (O_bulk_ctu_cc_pe_id,                         --BULK_CC_PE_ID
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO, --PRICE_EVENT_TYPE
                 'Y',                                         --PERSIST_IND
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,          --START_STATE
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,          --END_STATE
                 I_user_name,                                 --USER_NAME
                 1,                                           --EMERGENCY_PERM
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,                  --SECONDARY_BULK_CC_PE_ID
                 0,                                           --SECONDARY_IND
                 1,                                           --ASYNCH_IND
                 0,                                           --AUTO_CLEAN_IND,
                 LP_schedule_thread,                          --THREAD_PROCESSOR_CLASS
                 0,                                           --NEED_IL_EXPLODE_IND
                 I_location_move_id,
                 LP_vdate,
                 LP_vdate);                                    --END_DATE

   else

      O_bulk_ctu_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Cust Seg Threshold Promo - I_loc_move_id: '|| I_location_move_id);

   --start customer segment complex promotion

   O_bulk_ccu_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number,
                                pe_merch_level)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                1, -- thread_number
                                pe_merch_level)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                    L_location)
      with cs_complex_promo as
      (select rpd.promo_dtl_id,
              rpdmn.dept,
              rpdmn.class,
              rpdmn.subclass,
              rpdmn.item,
              rpdmn.diff_id,
              rpdmn.price_event_itemlist,
              rpdmn.merch_type
         from rpm_promo_dtl rpd,
              rpm_promo_comp rpc,
              rpm_promo_zone_location rpzl,
              rpm_promo_dtl_merch_node rpdmn
        where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rpzl.zone_id        = L_zone_id
          and rpd.promo_comp_id   = rpc.promo_comp_id
          and rpd.promo_dtl_id    = rpzl.promo_dtl_id
          and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
          and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
          and L_effective_date    > rpd.start_date
          and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
          and rpc.type            = RPM_CONSTANTS.COMPLEX_CODE
          and rpc.customer_type   is NOT NULL
          and NOT EXISTS (select 1
                            from rpm_promo_zone_location rpzl1
                           where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                             and rpzl1.zone_id      = L_new_zone_id)),
      cs_complex_promo_skulist as
      (select rpd.promo_dtl_id,
              rpdmn.dept,
              rpdmn.class,
              rpdmn.subclass,
              rpds.item,
              rpdmn.diff_id,
              rpdmn.merch_type,
              rpds.item_level
         from rpm_promo_dtl rpd,
              rpm_promo_comp rpc,
              rpm_promo_zone_location rpzl,
              rpm_promo_dtl_merch_node rpdmn,
              rpm_promo_dtl_skulist rpds
        where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
          and rpzl.zone_id        = L_zone_id
          and rpd.promo_comp_id   = rpc.promo_comp_id
          and rpd.promo_dtl_id    = rpzl.promo_dtl_id
          and rpd.state           = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
          and rpdmn.promo_dtl_id  = rpd.promo_dtl_id
          and L_effective_date    > rpd.start_date
          and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
          and rpc.type            = RPM_CONSTANTS.COMPLEX_CODE
          and rpc.customer_type   is  NOT NULL
          and rpds.price_event_id = rpd.promo_dtl_id
          and rpdmn.skulist       = rpds.skulist
          and NOT EXISTS (select 1
                            from rpm_promo_zone_location rpzl1
                           where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                             and rpzl1.zone_id      = L_new_zone_id))
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end diff_id,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end item_parent,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end merch_level_type,
             case ccp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             cs_complex_promo ccp
       where ccp.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = ccp.dept
         and im.class                = NVL(ccp.class, im.class)
         and im.subclass             = NVL(ccp.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1 diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             case ccp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             cs_complex_promo ccp
       where ccp.merch_type  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = ccp.dept
         and im.class        = NVL(ccp.class, im.class)
         and im.subclass     = NVL(ccp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.diff_1       is NOT NULL
         and im.item_parent  is NOT NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             case ccp.merch_type
                when RPM_CONSTANTS.DEPT_LEVEL_ITEM then
                   RPM_CONSTANTS.DEPT_MERCH_TYPE
                when RPM_CONSTANTS.CLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.CLASS_MERCH_TYPE
                when RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM then
                   RPM_CONSTANTS.SUBCLASS_MERCH_TYPE
                else
                   NULL
             end pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_complex_promo ccp
       where ccp.merch_type  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                 RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                 RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept         = ccp.dept
         and im.class        = NVL(ccp.class, im.class)
         and im.subclass     = NVL(ccp.subclass, im.subclass)
         and im.item_level   = im.tran_level
         and im.item_parent  is NULL
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             NULL item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_complex_promo ccp
       where ccp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = ccp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Parent Level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             ccp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_complex_promo ccp
       where ccp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent  = ccp.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             ccp.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_complex_promo ccp
       where ccp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = ccp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and diffs.id        = im.diff_1
      union all
      -- Parent Level for Parent Level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_complex_promo ccp
       where ccp.merch_type  = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item         = ccp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             ccp.diff_id,
             ccp.item item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_complex_promo ccp
       where ccp.merch_type  = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent  = ccp.item
         and (   im.diff_1   = ccp.diff_id
              or im.diff_2   = ccp.diff_id
              or im.diff_3   = ccp.diff_id
              or im.diff_4   = ccp.diff_id)
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             ccp.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
             cs_complex_promo ccp
       where ccp.merch_type  = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item         = ccp.item
         and im.item_level   = im.tran_level - 1
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and ccp.diff_id     = diffs.diff_id
         and diffs.id        = im.diff_1
      union all
      -- Tran Level for Item list - tran item level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_complex_promo_skulist ccps
       where ccps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and ccps.item_level = RPM_CONSTANTS.IL_ITEM_LEVEL
         and im.item         = ccps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             cs_complex_promo_skulist ccps
       where ccps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and ccps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and im.item_parent  = ccps.item
         and im.item_level   = im.tran_level
         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level for Item Lists - parent item level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diffs.diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            cs_complex_promo_skulist ccps
      where ccps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and ccps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = ccps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
        and im.diff_1       = diffs.id
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccps.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL diff_id,
             NULL item_parent,
             RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from item_master im,
             (select diff_group_id id,
                     diff_id
                from diff_group_detail
              union all
              select diff_id id,
                     diff_id
                from diff_ids) diffs,
            cs_complex_promo_skulist ccps
      where ccps.merch_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
        and ccps.item_level = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
        and im.item         = ccps.item
        and im.diff_1       is NOT NULL
        and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
        and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
        and im.item_level   = im.tran_level - 1
      union all
      -- Price Event Itemlist level Promotion - Parent Item
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1 diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level
        from cs_complex_promo ccp,
             rpm_merch_list_detail rmld,
             item_master im
       where ccp.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id = ccp.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item          = im.item_parent
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Price Event Itemlist level Promotion - Tran Item
      select O_bulk_ccu_cc_pe_id bulk_cc_pe_id,
             ccp.promo_dtl_id price_event_id,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1) diff_id,
             im.item_parent item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level
        from cs_complex_promo ccp,
             rpm_merch_list_detail rmld,
             item_master im
       where ccp.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rmld.merch_list_id = ccp.price_event_itemlist
         and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item          = im.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type)
      select distinct bulk_cc_pe_id,
             price_event_id,
             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO
        from rpm_bulk_cc_pe_item
       where bulk_cc_pe_id = O_bulk_ccu_cc_pe_id;

   if SQL%FOUND then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time,
                                  new_promo_end_date)
         values (O_bulk_ccu_cc_pe_id,                           --BULK_CC_PE_ID
                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,  --PRICE_EVENT_TYPE
                 'Y',                                           --PERSIST_IND
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,            --START_STATE
                 RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,            --END_STATE
                 I_user_name,                                   --USER_NAME
                 1,                                             --EMERGENCY_PERM
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,                    --SECONDARY_BULK_CC_PE_ID
                 0,                                             --SECONDARY_IND
                 1,                                             --ASYNCH_IND
                 0,                                             --AUTO_CLEAN_IND,
                 LP_schedule_thread,                            --THREAD_PROCESSOR_CLASS
                 0,                                             --NEED_IL_EXPLODE_IND
                 I_location_move_id,
                 LP_vdate,
                 LP_vdate);                                     --END_DATE

   else

      O_bulk_ccu_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Cust Seg Complex Promo - I_loc_move_id: '|| I_location_move_id);

   --start customer segment transaction promotion

   O_bulk_ctxp_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   insert all
      when head_rnk = 1 then
         into rpm_bulk_cc_pe (bulk_cc_pe_id,
                              price_event_type,
                              persist_ind,
                              start_state,
                              end_state,
                              user_name,
                              emergency_perm,
                              secondary_bulk_cc_pe_id,
                              secondary_ind,
                              asynch_ind,
                              auto_clean_ind,
                              thread_processor_class,
                              need_il_explode_ind,
                              location_move_id,
                              create_date_time,
                              new_promo_end_date)
                      values (O_bulk_ctxp_cc_pe_id,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO, -- price_event_type
                              'Y',                                       -- persist_ind
                              RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,        -- start_state
                              RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,        -- end_state
                              I_user_name,
                              1,                                         -- emergency_perm
                              RPM_BULK_CC_PE_SEQ.NEXTVAL,                -- secondary_bulk_cc_pe_id
                              0,                                         -- secondary_ind
                              1,                                         -- asynch_ind
                              0,                                         -- auto_clean_ind,
                              LP_schedule_thread,                        -- thread_processor_class
                              0,                                         -- need_il_explode_ind
                              I_location_move_id,
                              LP_vdate,                                  -- create_date_time
                              LP_vdate)                                  -- end_date
      when thread_rnk = 1 then
         into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                     price_event_id,
                                     price_event_display_id,
                                     price_event_type,
                                     rank,
                                     man_txn_excl_exists,
                                     man_txn_exclusion_parent_id,
                                     elig_for_sys_gen_exclusions)
                             values (O_bulk_ctxp_cc_pe_id,
                                     promo_dtl_id,
                                     promo_dtl_display_id,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                     0, -- rank
                                     man_txn_excl_exists,
                                     exception_parent_id,
                                     0) -- elig_for_sys_gen_exclusions
      when 1 = 1 then
         into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                   price_event_id,
                                   itemloc_id,
                                   dept,
                                   class,
                                   subclass,
                                   item,
                                   diff_id,
                                   item_parent,
                                   merch_level_type,
                                   thread_number,
                                   pe_merch_level,
                                   txn_man_excluded_item)
                           values (O_bulk_ctxp_cc_pe_id,
                                   promo_dtl_id,
                                   RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(1),
                                   dept,
                                   class,
                                   subclass,
                                   item,
                                   diff_id,
                                   item_parent,
                                   merch_level_type,
                                   1, -- thread_number
                                   pe_merch_level,
                                   txn_man_excluded_item)
      when 1 = 1 then
         into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                       price_event_id,
                                       itemloc_id,
                                       location,
                                       zone_node_type)
                               values (O_bulk_ctxp_cc_pe_id,
                                       promo_dtl_id,
                                       RPM_LOC_MOVE_SQL.GET_ITEMLOC_ID(0),
                                       L_location,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_STORE)
      with txp_promo as
         (select rpd.promo_dtl_id,
                 rpd.promo_dtl_display_id,
                 rpd.exception_parent_id,
                 rpd.man_txn_exclusion,
                 rpd.man_txn_excl_exists,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.price_event_itemlist,
                 rpdmn.skulist,
                 rpdmn.merch_type
            from rpm_promo_zone_location rpzl,
                 rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl_merch_node rpdmn
           where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id                = L_zone_id
             and rpd.state                   = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and L_effective_date            > rpd.start_date
             and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
             and rpd.promo_dtl_id            = rpzl.promo_dtl_id
             and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
             and rpd.promo_comp_id           = rpc.promo_comp_id
             and rpc.customer_type           is NULL
             and (   rpdmn.merch_type           != RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  or rpd.sys_generated_exclusion = 1)
             and rpdmn.skulist               is NOT NULL
             and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
             and NOT EXISTS (select 1
                               from rpm_promo_zone_location rpzl1
                              where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                and rpzl1.zone_id      = L_new_zone_id)),  -- end of txp_promo
      txp_promo_skulist as
         (select rpd.promo_dtl_id,
                 rpd.promo_dtl_display_id,
                 rpd.exception_parent_id,
                 rpd.man_txn_exclusion,
                 rpd.man_txn_excl_exists,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.diff_id,
                 rpdmn.skulist,
                 rpdmn.merch_type,
                 rpds.item,
                 rpds.item_level
            from rpm_promo_zone_location rpzl,
                 rpm_promo_dtl rpd,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rpzl.zone_id                = L_zone_id
             and rpd.sys_generated_exclusion = 0
             and rpd.state                   = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
             and L_effective_date            > rpd.start_date
             and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
             and rpd.promo_dtl_id            = rpzl.promo_dtl_id
             and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
             and rpc.customer_type           is NOT NULL
             and rpd.promo_comp_id           = rpc.promo_comp_id
             and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
             and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpds.price_event_id         = rpd.promo_dtl_id
             and rpdmn.skulist               = rpds.skulist
             and NOT EXISTS (select 1
                               from rpm_promo_zone_location rpzl1
                              where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                and rpzl1.zone_id      = L_new_zone_id)),  -- end of txp_promo_skulist
      txn_dtl_exclusions as
         (-- merch hierarchy
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 tp.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 subclass sc
           where tp.man_txn_exclusion = 1
             and tp.merch_type        IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                          RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                          RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
             and sc.dept              = tp.dept
             and sc.class             = NVL(tp.class, sc.class)
             and sc.subclass          = NVL(tp.subclass, sc.subclass)
          union all
          -- Explode to tran item for exclusion at skulist parent item
          select tps.dept,
                 tps.class,
                 tps.subclass,
                 im.item,
                 tps.diff_id,
                 tps.merch_type,
                 tps.promo_dtl_id,
                 tps.exception_parent_id
            from txp_promo_skulist tps,
                 item_master im
           where tps.man_txn_exclusion = 1
             and tps.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and tps.item_level        = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
             and im.status             = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind       = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.item_parent        = tps.item
             and im.item_level         = im.tran_level
          union all
          -- exclusion at skulist level for item or parent item
          select tps.dept,
                 tps.class,
                 tps.subclass,
                 tps.item,
                 tps.diff_id,
                 tps.merch_type,
                 tps.promo_dtl_id,
                 tps.exception_parent_id
            from txp_promo_skulist tps
           where tps.man_txn_exclusion = 1
             and tps.merch_type        = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
          -- Explode to tran item for exclusion at peil parent item level
          union all
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 im.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 rpm_merch_list_detail rmld,
                 item_master im
           where tp.man_txn_exclusion = 1
             and tp.merch_type        = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id   = tp.price_event_itemlist
             and rmld.merch_level     = RPM_CONSTANTS.PARENT_MERCH_TYPE
             and im.item_parent       = rmld.item
             and im.item_level        = im.tran_level
             and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          union all
          -- exclusion at peil level for item or parent item level
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 rmld.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 rpm_merch_list_detail rmld
           where tp.man_txn_exclusion = 1
             and tp.merch_type        = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id   = tp.price_event_itemlist
          union all
          -- explode to tran level for exclusion at parent and parent diff item
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 im.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp,
                 item_master im
           where tp.man_txn_exclusion = 1
             and tp.merch_type        IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                          RPM_CONSTANTS.PARENT_ITEM_DIFF)
             and im.item_parent       = tp.item
             and (   tp.diff_id       is NULL
                  or (    tp.diff_id  is NOT NULL
                      and (   im.diff_1 = tp.diff_id
                           or im.diff_2 = tp.diff_id
                           or im.diff_3 = tp.diff_id
                           or im.diff_4 = tp.diff_id)))
             and im.item_level        = im.tran_level
             and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          union all
          -- exclusion at tran item or parent diff item
          select tp.dept,
                 tp.class,
                 tp.subclass,
                 tp.item,
                 tp.diff_id,
                 tp.merch_type,
                 tp.promo_dtl_id,
                 tp.exception_parent_id
            from txp_promo tp
           where tp.man_txn_exclusion = 1
             and tp.merch_type        IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                          RPM_CONSTANTS.PARENT_ITEM_DIFF))  -- end of txn_dtl_exclusions
      select promo_dtl_id,
             promo_dtl_display_id,
             dept,
             class,
             subclass,
             item,
             diff_id,
             item_parent,
             merch_level_type,
             pe_merch_level,
             txn_man_excluded_item,
             exception_parent_id,
             man_txn_excl_exists,
             ROW_NUMBER() OVER (PARTITION BY O_bulk_ctxp_cc_pe_id
                                    ORDER BY NULL) head_rnk,
             ROW_NUMBER() OVER (PARTITION BY O_bulk_ctxp_cc_pe_id,
                                             promo_dtl_id
                                    ORDER BY NULL) thread_rnk
        from (-- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     case im.item_level
                        when im.tran_level then
                           im.diff_1
                        else
                           NULL
                     end diff_id,
                     case im.item_level
                        when im.tran_level then
                           im.item_parent
                        else
                           NULL
                     end item_parent,
                     case im.item_level
                        when im.tran_level then
                           RPM_CONSTANTS.ITEM_MERCH_TYPE
                        else
                           RPM_CONSTANTS.PARENT_MERCH_TYPE
                     end merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.dept                = tp.dept
                 and im.class               = NVL(tp.class, im.class)
                 and im.subclass            = NVL(tp.subclass, im.subclass)
                 and (   (    im.item_level  = im.tran_level
                          and im.item_parent is NOT NULL)
                      or im.item_level       = im.tran_level - 1)
                 and im.status              = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind        = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id        = tde.exception_parent_id (+)
                 and im.item                = NVL(tde.item (+), im.item)
                 and NVL(im.diff_1, '-999') = NVL(tde.diff_id (+), NVL(im.diff_1, '-999'))
                 and im.dept                = NVL(tde.dept (+), im.dept)
                 and im.class               = NVL(tde.class (+), im.class)
                 and im.subclass            = NVL(tde.subclass (+), im.subclass)
              union all
               -- Parent-Diff Level items for Merch Hierarchy Level Promotion
              select distinct
                     tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item_parent,
                     im.diff_1 diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.dept         = tp.dept
                 and im.class        = NVL(tp.class, im.class)
                 and im.subclass     = NVL(tp.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.diff_1       is NOT NULL
                 and im.item_parent  is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item_parent  = NVL(tde.item (+), im.item_parent)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Tran level items with no parents for Merch Hierarchy Level Promotion
              select distinct
                     tp.promo_dtl_id price_event_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 and im.dept         = tp.dept
                 and im.class        = NVL(tp.class, im.class)
                 and im.subclass     = NVL(tp.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.item_parent  is NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Tran Level for Item Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = tde.item (+)
              union all
              -- Tran Level for Parent Level Promotion
              select distinct
                     tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     im.diff_1 diff_id,
                     tp.item item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item_parent  = tp.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = tde.item (+)
              union all
              -- Parent/Diff level data for price events at the Parent level
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     tp.item,
                     diffs.diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                 from item_master im,
                      (select diff_group_id id,
                              diff_id
                         from diff_group_detail
                       union all
                       select diff_id id,
                              diff_id
                         from diff_ids) diffs,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level - 1
                 and im.diff_1       is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and diffs.id        = im.diff_1
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and diffs.diff_id   = NVL(tde.diff_id (+), diffs.diff_id)
              union all
              -- Parent Level for Parent Level Promotion
              select tp.promo_dtl_id ,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level - 1
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = tde.item (+)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
              union all
              -- Tran Level Item for Price Event at Parent Diff Level Item and DIFF_ID is part of DIFF_1
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     tp.diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item_parent  = tp.item
                 and im.diff_1       = tp.diff_id
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item_parent  = NVL(tde.item (+), im.item_parent)
              union all
              -- Tran Level Item for Price Event at Parent Diff Level Item
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     tp.diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from item_master im,
                     txp_promo tp,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item_parent  = tp.item
                 and (   im.diff_2   = tp.diff_id
                      or im.diff_3   = tp.diff_id
                      or im.diff_4   = tp.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
              union all
              -- Parent Diff data for price event at Parent Diff Level
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     tp.diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.PARENT_ITEM_DIFF
                 and im.item         = tp.item
                 and im.item_level   = im.tran_level - 1
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.diff_id      = diffs.diff_id
                 and diffs.id        = im.diff_1
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
              union all
              -- Tran Level for Item list - tran item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     txn_dtl_exclusions tde
               where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and tps.item_level   = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item          = tps.item
                 and im.item_level    = im.tran_level
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tps.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item          = tde.item (+)
              union all
              -- Tran Level for Item Lists - parent item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     im.diff_1 diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     txn_dtl_exclusions tde
               where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and tps.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item_parent   = tps.item
                 and im.item_level    = im.tran_level
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tps.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item          = NVL(tde.item (+), im.item)
              union all
              -- Parent Diff Level for Item Lists - parent item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     diffs.diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                    txn_dtl_exclusions tde
              where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                and tps.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                and im.item          = tps.item
                and im.diff_1        is NOT NULL
                and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                and im.item_level    = im.tran_level - 1
                and im.diff_1        = diffs.id
                and tps.promo_dtl_id = tde.exception_parent_id (+)
                and tps.item         = NVL(tde.item (+), tps.item)
                and diffs.diff_id    = NVL(tde.diff_id (+), diffs.diff_id)
              union all
              -- Parent Level for Item Lists - parent item level Promotion
              select tps.promo_dtl_id,
                     tps.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL diff_id,
                     NULL item_parent,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tps.exception_parent_id,
                     tps.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo_skulist tps,
                     item_master im,
                     txn_dtl_exclusions tde
               where tps.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 and tps.item_level   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item          = tps.item
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level    = im.tran_level - 1
                 and tps.promo_dtl_id = tde.exception_parent_id (+)
                 and tps.item         = NVL(tde.item (+), tps.item)
                 and im.diff_1        = NVL(tde.diff_id (+), im.diff_1)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Tran Item
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item               = im.item
                 and im.item_level           = im.tran_level
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and im.item                 = tde.item (+)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Parent Item
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     im.diff_1 diff_id,
                     im.item_parent item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item               = im.item_parent
                 and im.item_level           = im.tran_level
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and im.item                 = NVL(tde.item (+), im.item)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Parent Diff Item Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     diffs.diff_id diff_id,
                     NULL, -- item_parent
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item               = im.item
                 and im.diff_1               is NOT NULL
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level           = im.tran_level -1
                 and im.diff_1               = diffs.id
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and rmld.item               = NVL(tde.item (+), rmld.item)
                 and diffs.diff_id           = NVL(tde.diff_id (+), diffs.diff_id)
              union all
              -- Price Event ItemList (PEIL) level Promotion - Parent Item Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL, -- diff_id,
                     NULL, -- item_parent
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     NVL2(tde.item, 1, 0) txn_man_excluded_item
                from txp_promo tp,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 and tp.price_event_itemlist = rmld.merch_list_id
                 and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item               = im.item
                 and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level           = im.tran_level - 1
                 and tp.promo_dtl_id         = tde.exception_parent_id (+)
                 and rmld.item               = NVL(tde.item (+), rmld.item)
                 and im.diff_1               = NVL(tde.diff_id (+), im.diff_1)
              union all
              -- parent level for Storewide Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     case im.item_level
                        when im.tran_level then
                           im.diff_1
                        else
                           NULL
                     end diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level
                 and im.item_parent  is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- tran level item with parents for Storewide Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     case im.item_level
                        when im.tran_level then
                           im.diff_1
                        else
                           NULL
                     end diff_id,
                     im.item_parent,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level - 1
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.diff_1       = NVL(tde.diff_id (+), im.diff_1)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Parent-Diff Level items for Storewide Level Promotion
              select distinct tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     diffs.diff_id,
                     NULL, -- item_parent
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     (select diff_group_id id,
                             diff_id
                        from diff_group_detail
                      union all
                      select diff_id id,
                             diff_id
                        from diff_ids) diffs,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level - 1
                 and im.diff_1       is NOT NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.diff_1       = diffs.id
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and diffs.diff_id   = NVL(tde.diff_id (+), diffs.diff_id)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass)
              union all
              -- Tran level items with no parents for Storewide Level Promotion
              select tp.promo_dtl_id,
                     tp.promo_dtl_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     NULL,
                     NULL, -- item_parent
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE pe_merch_level,
                     tp.exception_parent_id,
                     tp.man_txn_excl_exists,
                     case
                        when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                             tde.dept is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                             tde.class is NOT NULL then
                           1
                        when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                             tde.subclass is NOT NULL then
                           1
                        when tde.item is NULL then
                           0
                        else
                           1
                     end txn_man_excluded_item
                from txp_promo tp,
                     rpm_zone_location rzl,
                     item_loc il,
                     item_master im,
                     txn_dtl_exclusions tde
               where tp.merch_type   = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                 and rzl.zone_id     = L_zone_id
                 and rzl.location    = L_location
                 and rzl.location    = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level
                 and im.item_parent  is NULL
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and tp.promo_dtl_id = tde.exception_parent_id (+)
                 and im.item         = NVL(tde.item (+), im.item)
                 and im.dept         = NVL(tde.dept (+), im.dept)
                 and im.class        = NVL(tde.class (+), im.class)
                 and im.subclass     = NVL(tde.subclass (+), im.subclass));

   if SQL%NOTFOUND then

      O_bulk_ctxp_cc_pe_id := NULL;

   end if;

   LOGGER.LOG_INFORMATION(L_program || ' - Done Setup CC tables for Cust Seg Transaction Promo - I_loc_move_id: '|| I_location_move_id);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_location_move_id,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program||' for loc_move_id'|| I_location_move_id);

   EXCEPTION

      when OTHERS then
         goto RESOLVE_ACTIVE_PROMOTIONS_2;

   END;

   <<RESOLVE_ACTIVE_PROMOTIONS_2>>

   LOGGER.LOG_TIME(L_program || ' - I_loc_move_id: '|| I_location_move_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END RESOLVE_ACTIVE_PROMOTIONS;
---------------------------------------------------------------------------------------------------

FUNCTION RESOLVE_FROM_ZONE_OVERLAPS (O_error_msg             OUT VARCHAR2,
                                     O_bulk_sp_cc_pe_id      OUT NUMBER,
                                     O_bulk_tp_cc_pe_id      OUT NUMBER,
                                     O_bulk_bg_cc_pe_id      OUT NUMBER,
                                     O_bulk_txp_cc_pe_id     OUT NUMBER,
                                     O_bulk_csp_cc_pe_id     OUT NUMBER,
                                     O_bulk_ctp_cc_pe_id     OUT NUMBER,
                                     O_bulk_cbg_cc_pe_id     OUT NUMBER,
                                     O_bulk_ctxp_cc_pe_id    OUT NUMBER,
                                     I_user_name          IN     VARCHAR2,
                                     I_location_move_id   IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_LOC_MOVE_SQL.RESOLVE_FROM_ZONE_OVERLAPS';

   L_trace_name VARCHAR2(15) := 'RFZ';

   L_zone_id        RPM_ZONE.ZONE_ID%TYPE           := NULL;
   L_effective_date DATE                            := NULL;
   L_location       RPM_LOCATION_MOVE.LOCATION%TYPE := NULL;
   L_new_zone_id    RPM_ZONE.ZONE_ID%TYPE           := NULL;
   L_start_time     TIMESTAMP                       := SYSTIMESTAMP;

   L_sp_cust_seg_count     NUMBER := 0;
   L_sp_no_cust_seg_count  NUMBER := 0;
   L_tp_cust_seg_count     NUMBER := 0;
   L_tp_no_cust_seg_count  NUMBER := 0;
   L_bg_cust_seg_count     NUMBER := 0;
   L_bg_no_cust_seg_count  NUMBER := 0;
   L_txp_cust_seg_count    NUMBER := 0;
   L_txp_no_cust_seg_count NUMBER := 0;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_location_move_id);

   delete from rpm_lm_pr_overlap_simple_gtt;
   delete from rpm_lm_pr_overlap_thres_gtt;
   delete from rpm_lm_pr_overlap_bg_gtt;
   delete from rpm_lm_pr_overlap_txp_gtt;

   select old_zone_id,
          new_zone_id,
          effective_date,
          location into L_zone_id,
                        L_new_zone_id,
                        L_effective_date,
                        L_location
     from rpm_location_move
    where location_move_id = I_location_move_id;

   select loc_move_prom_overlap_behavior into LP_lm_prom_overlap_behav
     from rpm_system_options;

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_location_move_id,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program||' for loc_move_id' ||I_location_move_id);

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_LMS||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto RESOLVE_FROM_ZONE_OVERLAPS_1;

   END;

   <<RESOLVE_FROM_ZONE_OVERLAPS_1>>

   --insert all pending/active zone level promotions for the from zone into temp table

   -- start Simple Promo
   insert into rpm_lm_pr_overlap_simple_gtt(promo_dtl_id,
                                            promo_dtl_display_id,
                                            promo_comp_id,
                                            ignore_constraints,
                                            apply_to_code,
                                            create_date,
                                            create_id,
                                            state,
                                            attribute_1,
                                            attribute_2,
                                            attribute_3,
                                            exception_parent_id,
                                            from_location_move,
                                            price_guide_id,
                                            threshold_id,
                                            zone_node_type,
                                            location,
                                            merch_type,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            diff_id,
                                            change_type,
                                            change_amount,
                                            change_currency,
                                            change_percent,
                                            change_selling_uom,
                                            promo_dtl_row_id,
                                            cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1,
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM merch_type,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.change_type,
             t.change_amount,
             t.change_currency,
             t.change_percent,
             t.change_selling_uom,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merchandise hierarchy level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date            <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date            <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Diff Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item_parent  = prm.item
                 and (   im.diff_1   = prm.diff_id
                      or im.diff_2   = prm.diff_id
                      or im.diff_3   = prm.diff_id
                      or im.diff_4   = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              --Skulist Tran Level Items
              select /*+ NO_MERGE(prm)*/
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpds.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_skulist rpds,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpds.price_event_id         = rpd.promo_dtl_id
                         and rpdmn.skulist               = rpds.skulist
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              --Skulist Parent Level Items
              select /*+ NO_MERGE(prm)*/
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpds.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_skulist rpds,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpds.price_event_id         = rpd.promo_dtl_id
                         and rpdmn.skulist               = rpds.skulist
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList (PEIL) - Parent Item Level
              select --Parent Level Items
                     /*+ NO_MERGE(prm)*/
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                               when rpc.customer_type is NOT NULL then
                                   1
                               else
                                  0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where prm.item        = im.item_parent
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList (PEIL) - Tran Item Level
              select /*+ NO_MERGE(prm)*/
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                               when rpc.customer_type is NOT NULL then
                                   1
                               else
                                  0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where prm.item        = im.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1,
                                rpm_promo_zone_location rpzl1,
                                rpm_promo_dtl_merch_node rpdmn
                          where rpd1.exception_parent_id     = t.exception_parent_id
                            and rpd1.promo_dtl_id            = rpzl1.promo_dtl_id
                            and rpzl1.location               = L_location
                            and rpd1.promo_dtl_id            = rpdmn.promo_dtl_id
                            and rpdmn.item                   = t.item
                            and rpd1.sys_generated_exclusion = 0)
         and EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location)
         and NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1
                          where rpd1.state                = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                            and rpd1.promo_dtl_id         = t.exception_parent_id
                            and rownum                    > 0
                            and LP_lm_prom_overlap_behav  = LP_END_EXISTING
                            and L_effective_date         <= LP_vdate + 1
                            and rpd1.end_date             = LP_vdate + 1);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_simple_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1) into L_sp_cust_seg_count
     from rpm_lm_pr_overlap_simple_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1) into L_sp_no_cust_seg_count
     from rpm_lm_pr_overlap_simple_gtt
    where cust_seg_comp_ind = 0;

   if (L_sp_cust_seg_count + L_sp_no_cust_seg_count) > 0 then
      --for the rest of the promo comp details create new exceptions

      insert into rpm_promo_dtl(promo_dtl_id,
                                promo_comp_id,
                                promo_dtl_display_id,
                                ignore_constraints,
                                apply_to_code,
                                start_date,
                                end_date,
                                approval_date,
                                create_date,
                                create_id,
                                approval_id,
                                state,
                                attribute_1,
                                attribute_2,
                                attribute_3,
                                exception_parent_id,
                                from_location_move,
                                price_guide_id,
                                threshold_id,
                                timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      rpd.start_date
                   when L_effective_date = LP_vdate + 1 then
                      case
                         when LP_vdate = TRUNC(rpd.start_date) then
                            rpd.start_date
                         when gtt.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                            LP_vdate + 1
                         else
                            LP_vdate
                         end
                   else
                      LP_vdate + 1
                   end,
                case
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date - RPM_CONSTANTS.ONE_SECOND
                   else
                      rpd.end_date
                   end,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_simple_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_simple_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_simple_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_simple_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert all
        into rpm_promo_dtl_merch_node(promo_dtl_merch_node_id,
                                      promo_dtl_list_id,
                                      promo_dtl_id,
                                      merch_type,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      diff_id)
                               values (RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
        into rpm_promo_dtl_disc_ladder(promo_dtl_disc_ladder_id,
                                       promo_dtl_list_id,
                                       change_type,
                                       change_amount,
                                       change_currency,
                                       change_percent,
                                       change_selling_uom,
                                       qual_type,
                                       qual_value)
                               values (RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                                       promo_dtl_list_id,
                                       change_type,
                                       change_amount,
                                       change_currency,
                                       change_percent,
                                       change_selling_uom,
                                       RPM_CONSTANTS.QUAL_QUANTITY,
                                       1)
         select list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id,
                gtt.change_type,
                gtt.change_amount,
                gtt.change_currency,
                gtt.change_percent,
                gtt.change_selling_uom
           from rpm_lm_pr_overlap_simple_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Simple Promotion Creation ',
                      L_start_time);

   end if;

   L_start_time := SYSTIMESTAMP;
   -- start threshold promotions
   insert into rpm_lm_pr_overlap_thres_gtt (promo_dtl_id,
                                            promo_dtl_display_id,
                                            promo_comp_id,
                                            ignore_constraints,
                                            apply_to_code,
                                            create_date,
                                            create_id,
                                            state,
                                            attribute_1,
                                            attribute_2,
                                            attribute_3,
                                            exception_parent_id,
                                            from_location_move,
                                            price_guide_id,
                                            threshold_id,
                                            zone_node_type,
                                            location,
                                            merch_type,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            diff_id,
                                            promo_dtl_row_id,
                                            cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, -- from_location_move
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM merch_type,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                     RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                     RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LEVEL_ITEM) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_zone_location rpzl,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LEVEL_ITEM) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.PARENT_ITEM_DIFF) prm
               where im.item_parent  = prm.item
                 and (   im.diff_1   = prm.diff_id
                      or im.diff_2   = prm.diff_id
                      or im.diff_3   = prm.diff_id
                      or im.diff_4   = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              --Skulist Tran Level Items
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpds.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_skulist rpds
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpds.price_event_id = rpd.promo_dtl_id
                         and rpdmn.skulist       = rpds.skulist) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              --Skulist Parent Level Items
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpds.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_skulist rpds
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpds.price_event_id = rpd.promo_dtl_id
                         and rpdmn.skulist       = rpds.skulist) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id  = rpdmn.price_event_itemlist
                         and rmld.merch_level    = RPM_CONSTANTS.PARENT_MERCH_TYPE) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              -- Price Event ItemList - Tran Item Level
              union all
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id  = rpdmn.price_event_itemlist
                         and rmld.merch_level    = RPM_CONSTANTS.ITEM_MERCH_TYPE) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1,
                                rpm_promo_zone_location rpzl1,
                                rpm_promo_dtl_merch_node rpdmn1
                          where rpd1.exception_parent_id     = t.exception_parent_id
                            and rpd1.promo_dtl_id            = rpzl1.promo_dtl_id
                            and rpzl1.location               = L_location
                            and rpd1.promo_dtl_id            = rpdmn1.promo_dtl_id
                            and rpdmn1.item                  = t.item
                            and rpd1.sys_generated_exclusion = 0)
         and EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location)
         and NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1
                          where rpd1.state                = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                            and rpd1.promo_dtl_id         = t.exception_parent_id
                            and rownum                    > 0
                            and LP_lm_prom_overlap_behav  = LP_END_EXISTING
                            and L_effective_date         <= LP_vdate + 1
                            and rpd1.end_date             = LP_vdate + 1);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_thres_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1) into L_tp_cust_seg_count
     from rpm_lm_pr_overlap_thres_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1) into L_tp_no_cust_seg_count
     from rpm_lm_pr_overlap_thres_gtt
    where cust_seg_comp_ind = 0;

   if (L_tp_cust_seg_count + L_tp_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl(promo_dtl_id,
                                promo_comp_id,
                                promo_dtl_display_id,
                                ignore_constraints,
                                apply_to_code,
                                start_date,
                                end_date,
                                approval_date,
                                create_date,
                                create_id,
                                approval_id,
                                state,
                                attribute_1,
                                attribute_2,
                                attribute_3,
                                exception_parent_id,
                                from_location_move,
                                price_guide_id,
                                threshold_id,
                                timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      rpd.start_date
                   when L_effective_date = LP_vdate + 1 then
                      case
                         when LP_vdate = TRUNC(rpd.start_date) then
                            rpd.start_date
                         when gtt.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                            LP_vdate + 1
                         else
                            LP_vdate
                      end
                   else
                      LP_vdate + 1
                end,
                case
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date - RPM_CONSTANTS.ONE_SECOND
                   else
                      rpd.end_date
                end,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                case
                   when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                      NULL
                   else
                      gtt.threshold_id
                end threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_thres_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_thres_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_thres_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_thres_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert into rpm_promo_dtl_merch_node (promo_dtl_merch_node_id,
                                            promo_dtl_list_id,
                                            promo_dtl_id,
                                            merch_type,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            diff_id)
         select RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id
           from rpm_lm_pr_overlap_thres_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      insert into rpm_promo_dtl_disc_ladder (promo_dtl_disc_ladder_id,
                                             promo_dtl_list_id,
                                             change_type,
                                             change_amount,
                                             change_currency,
                                             change_percent,
                                             change_selling_uom,
                                             qual_type,
                                             qual_value)
         select RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                promo_dtl_list_id,
                change_type,
                change_amount,
                change_currency,
                change_percent,
                change_selling_uom,
                RPM_CONSTANTS.QUAL_QUANTITY,
                1
           from (select distinct
                        rpdl.promo_dtl_list_id,
                        case when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                           RPM_CONSTANTS.RETAIL_EXCLUDE
                        else
                           rpddl.change_type
                        end change_type,
                        case when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                           NULL
                        else
                           rpddl.change_amount
                        end change_amount,
                        case when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                           NULL
                        else
                           rpddl.change_currency
                        end change_currency,
                        case when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                           NULL
                        else
                           rpddl.change_percent
                        end change_percent,
                        case when LP_lm_prom_overlap_behav = LP_DO_NOT_START_EXISTING then
                           NULL
                        else
                           rpddl.change_selling_uom
                        end change_selling_uom
                   from rpm_lm_pr_overlap_thres_gtt gtt,
                        rpm_promo_dtl_list_grp rpdlg,
                        rpm_promo_dtl_list rpdl,
                        rpm_promo_dtl_list_grp rpdlg_parent,
                        rpm_promo_dtl_list rpdl_parent,
                        rpm_promo_dtl_disc_ladder rpddl
                  where gtt.promo_dtl_id                   = rpdlg.promo_dtl_id
                    and rpdlg.promo_dtl_list_grp_id        = rpdl.promo_dtl_list_grp_id
                    and gtt.exception_parent_id            = rpdlg_parent.promo_dtl_id
                    and rpdlg_parent.promo_dtl_list_grp_id = rpdl_parent.promo_dtl_list_grp_id
                    and rpdl_parent.promo_dtl_list_id      = rpddl.promo_dtl_list_id);

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Threshold Promotion Creation ',
                      L_start_time);

   end if;
   -- end threshold

   L_start_time := SYSTIMESTAMP;

   -- start buyget
   insert into rpm_lm_pr_overlap_bg_gtt (promo_dtl_id,
                                         promo_dtl_display_id,
                                         promo_comp_id,
                                         ignore_constraints,
                                         apply_to_code,
                                         create_date,
                                         create_id,
                                         state,
                                         attribute_1,
                                         attribute_2,
                                         attribute_3,
                                         exception_parent_id,
                                         from_location_move,
                                         price_guide_id,
                                         threshold_id,
                                         zone_node_type,
                                         location,
                                         merch_type,
                                         dept,
                                         class,
                                         subclass,
                                         item,
                                         diff_id,
                                         change_type,
                                         promo_dtl_row_id,
                                         cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, -- from_location_move
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM merch_type,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.change_type,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Diff
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date,L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and (   im.diff_1  = prm.diff_id
                      or im.diff_2  = prm.diff_id
                      or im.diff_3  = prm.diff_id
                      or im.diff_4  = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event Itemlist - Parent Item
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event Itemlist - Tran Item
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1,
                                rpm_promo_zone_location rpzl1,
                                rpm_promo_dtl_merch_node rpdmn1
                          where rpd1.exception_parent_id     = t.exception_parent_id
                            and rpd1.promo_dtl_id            = rpzl1.promo_dtl_id
                            and rpzl1.location               = L_location
                            and rpd1.promo_dtl_id            = rpdmn1.promo_dtl_id
                            and rpdmn1.item                  = t.item
                            and rpd1.sys_generated_exclusion = 0)
         and EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location)
         AND NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1
                          where rpd1.state                = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                            and rpd1.promo_dtl_id         = t.exception_parent_id
                            and rownum                    > 0
                            and LP_lm_prom_overlap_behav  = LP_END_EXISTING
                            and L_effective_date         <= LP_vdate + 1
                            and rpd1.end_date             = LP_vdate + 1);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_bg_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1) into L_bg_cust_seg_count
     from rpm_lm_pr_overlap_bg_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1) into L_bg_no_cust_seg_count
     from rpm_lm_pr_overlap_bg_gtt
    where cust_seg_comp_ind = 0;

   if (L_bg_cust_seg_count + L_bg_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl(promo_dtl_id,
                                promo_comp_id,
                                promo_dtl_display_id,
                                ignore_constraints,
                                apply_to_code,
                                start_date,
                                end_date,
                                approval_date,
                                create_date,
                                create_id,
                                approval_id,
                                state,
                                attribute_1,
                                attribute_2,
                                attribute_3,
                                exception_parent_id,
                                from_location_move,
                                price_guide_id,
                                threshold_id,
                                timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      rpd.start_date
                   when L_effective_date = LP_vdate + 1 then
                      case
                         when LP_vdate = TRUNC(rpd.start_date) then
                            rpd.start_date
                         when gtt.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                            LP_vdate + 1
                         else
                            LP_vdate
                         end
                   else
                      LP_vdate + 1
                end,
                case
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date - RPM_CONSTANTS.ONE_SECOND
                   else
                      rpd.end_date
                end,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_bg_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_bg_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_bg_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_bg_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert all
         into rpm_promo_dtl_merch_node(promo_dtl_merch_node_id,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
                               values (RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
         into rpm_promo_dtl_disc_ladder(promo_dtl_disc_ladder_id,
                                        promo_dtl_list_id,
                                        change_type,
                                        change_amount,
                                        change_currency,
                                        change_percent,
                                        change_selling_uom,
                                        qual_type,
                                        qual_value)
                                values (RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                                        promo_dtl_list_id,
                                        change_type,
                                        change_amount,
                                        change_currency,
                                        change_percent,
                                        change_selling_uom,
                                        RPM_CONSTANTS.QUAL_QUANTITY,
                                        1)
         select list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id,
                gtt.change_type,
                gtt.change_amount,
                gtt.change_currency,
                gtt.change_percent,
                gtt.change_selling_uom
           from rpm_lm_pr_overlap_bg_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Complex Promotion Creation ',
                      L_start_time);

   end if;
   -- end buyget

   -- start transaction

   L_start_time := SYSTIMESTAMP;

   insert into rpm_lm_pr_overlap_txp_gtt (promo_dtl_id,
                                          promo_dtl_display_id,
                                          promo_comp_id,
                                          ignore_constraints,
                                          apply_to_code,
                                          create_date,
                                          create_id,
                                          state,
                                          attribute_1,
                                          attribute_2,
                                          attribute_3,
                                          exception_parent_id,
                                          from_location_move,
                                          price_guide_id,
                                          threshold_id,
                                          zone_node_type,
                                          location,
                                          merch_type,
                                          dept,
                                          class,
                                          subclass,
                                          item,
                                          diff_id,
                                          change_type,
                                          promo_dtl_row_id,
                                          cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, --from_location_move
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM merch_type,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.change_type,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Diff
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and (   im.diff_1  = prm.diff_id
                      or im.diff_2  = prm.diff_id
                      or im.diff_3  = prm.diff_id
                      or im.diff_4  = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event Itemlist - Parent Item
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event Itemlist - Tran Item
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Storewide
              select /*+ NO_MERGE (prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_loc il,
                     item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_new_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM) prm
               where L_location      = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1,
                                rpm_promo_zone_location rpzl1,
                                rpm_promo_dtl_merch_node rpdmn1
                          where rpd1.exception_parent_id     = t.exception_parent_id
                            and rpd1.promo_dtl_id            = rpzl1.promo_dtl_id
                            and rpzl1.location               = L_location
                            and rpd1.promo_dtl_id            = rpdmn1.promo_dtl_id
                            and rpdmn1.item                  = t.item
                            and rpd1.sys_generated_exclusion = 0)
         and EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location)
         AND NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1
                          where rpd1.state                = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                            and rpd1.promo_dtl_id         = t.exception_parent_id
                            and rownum                    > 0
                            and LP_lm_prom_overlap_behav  = LP_END_EXISTING
                            and L_effective_date         <= LP_vdate + 1
                            and rpd1.end_date             = LP_vdate + 1);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_txp_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1) into L_txp_cust_seg_count
     from rpm_lm_pr_overlap_txp_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1) into L_txp_no_cust_seg_count
     from rpm_lm_pr_overlap_txp_gtt
    where cust_seg_comp_ind = 0;

   if (L_txp_cust_seg_count + L_txp_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl(promo_dtl_id,
                                promo_comp_id,
                                promo_dtl_display_id,
                                ignore_constraints,
                                apply_to_code,
                                start_date,
                                end_date,
                                approval_date,
                                create_date,
                                create_id,
                                approval_id,
                                state,
                                attribute_1,
                                attribute_2,
                                attribute_3,
                                exception_parent_id,
                                from_location_move,
                                price_guide_id,
                                threshold_id,
                                timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                      rpd.start_date
                   when L_effective_date = LP_vdate + 1 then
                      case
                         when LP_vdate = TRUNC(rpd.start_date) then
                            rpd.start_date
                         when gtt.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                            LP_vdate + 1
                         else
                            LP_vdate
                         end
                   else
                      LP_vdate + 1
                end,
                case
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date - RPM_CONSTANTS.ONE_SECOND
                   else
                      rpd.end_date
                end,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_txp_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_txp_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_txp_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_txp_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert all
         into rpm_promo_dtl_merch_node(promo_dtl_merch_node_id,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
                               values (RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
         into rpm_promo_dtl_disc_ladder(promo_dtl_disc_ladder_id,
                                        promo_dtl_list_id,
                                        change_type,
                                        change_amount,
                                        change_currency,
                                        change_percent,
                                        change_selling_uom,
                                        qual_type,
                                        qual_value)
                                values (RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                                        promo_dtl_list_id,
                                        change_type,
                                        change_amount,
                                        change_currency,
                                        change_percent,
                                        change_selling_uom,
                                        RPM_CONSTANTS.QUAL_QUANTITY,
                                        1)
         select list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id,
                gtt.change_type,
                gtt.change_amount,
                gtt.change_currency,
                gtt.change_percent,
                gtt.change_selling_uom
           from rpm_lm_pr_overlap_txp_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Transaction Promotion Creation ',
                      L_start_time);

   end if;
   -- end transaction promotion

   if L_sp_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_sp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_tp_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_tp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_bg_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_bg_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_txp_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_txp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_sp_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_csp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_tp_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_ctp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_bg_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_cbg_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   if L_txp_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_ctxp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_schedule_batch) = 0 then
         return 0;
      end if;
   end if;

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_location_move_id,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program||' for loc_move_id' ||I_location_move_id);

   EXCEPTION

      when OTHERS then
         goto RESOLVE_FROM_ZONE_OVERLAPS_2;

   END;

   <<RESOLVE_FROM_ZONE_OVERLAPS_2>>

   LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END RESOLVE_FROM_ZONE_OVERLAPS;
---------------------------------------------------------------------------------

FUNCTION RESOLVE_TO_ZONE_OVERLAPS (O_error_msg             OUT VARCHAR2,
                                   O_bulk_sp_cc_pe_id      OUT NUMBER,
                                   O_bulk_tp_cc_pe_id      OUT NUMBER,
                                   O_bulk_bg_cc_pe_id      OUT NUMBER,
                                   O_bulk_txp_cc_pe_id     OUT NUMBER,
                                   O_bulk_csp_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctp_cc_pe_id     OUT NUMBER,
                                   O_bulk_cbg_cc_pe_id     OUT NUMBER,
                                   O_bulk_ctxp_cc_pe_id    OUT NUMBER,
                                   I_user_name          IN     VARCHAR2,
                                   I_location_move_id   IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_LOC_MOVE_SQL.RESOLVE_TO_ZONE_OVERLAPS';

   L_sp_cust_seg_count     NUMBER := 0;
   L_sp_no_cust_seg_count  NUMBER := 0;
   L_tp_cust_seg_count     NUMBER := 0;
   L_tp_no_cust_seg_count  NUMBER := 0;
   L_bg_cust_seg_count     NUMBER := 0;
   L_bg_no_cust_seg_count  NUMBER := 0;
   L_txp_cust_seg_count    NUMBER := 0;
   L_txp_no_cust_seg_count NUMBER := 0;

   L_zone_id        RPM_ZONE.ZONE_ID%TYPE := NULL;
   L_old_zone_id    RPM_ZONE.ZONE_ID%TYPE := NULL;
   L_effective_date DATE                  := NULL;
   L_start_time     TIMESTAMP             := SYSTIMESTAMP;

   L_location RPM_LOCATION_MOVE.LOCATION%TYPE := NULL;

BEGIN

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LM,
                                        L_program||'-'||I_location_move_id,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program||' for loc_move_id' ||I_location_move_id);

   EXCEPTION

      when OTHERS then
         goto RESOLVE_TO_ZONE_OVERLAPS_1;

   END;

   <<RESOLVE_TO_ZONE_OVERLAPS_1>>

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_location_move_id);

   delete from rpm_lm_pr_overlap_simple_gtt;
   delete from rpm_lm_pr_overlap_thres_gtt;
   delete from rpm_lm_pr_overlap_bg_gtt;
   delete from rpm_lm_pr_overlap_txp_gtt;

   select new_zone_id,
          old_zone_id,
          effective_date,
          location
     into L_zone_id,
          L_old_zone_id,
          L_effective_date,
          L_location
     from rpm_location_move
    where location_move_id = I_location_move_id;

   select loc_move_prom_overlap_behavior
     into LP_lm_prom_overlap_behav
     from rpm_system_options;

   insert into rpm_lm_pr_overlap_simple_gtt (promo_dtl_id,
                                             promo_dtl_display_id,
                                             promo_comp_id,
                                             ignore_constraints,
                                             apply_to_code,
                                             create_date,
                                             create_id,
                                             state,
                                             attribute_1,
                                             attribute_2,
                                             attribute_3,
                                             exception_parent_id,
                                             from_location_move,
                                             price_guide_id,
                                             threshold_id,
                                             zone_node_type,
                                             location,
                                             merch_type,
                                             dept,
                                             class,
                                             subclass,
                                             item,
                                             diff_id,
                                             change_type,
                                             change_amount,
                                             change_currency,
                                             change_percent,
                                             change_selling_uom,
                                             promo_dtl_row_id,
                                             cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, -- from_location_move
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.change_type,
             t.change_amount,
             t.change_currency,
             t.change_percent,
             t.change_selling_uom,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.promo_dtl_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item NOT IN (select rpdm1.item
                                       from rpm_promo_dtl rpd1,
                                            rpm_promo_dtl_merch_node rpdm1,
                                            rpm_promo_zone_location rpzl1
                                      where prm.promo_dtl_id     = rpd1.exception_parent_id
                                        and rpd1.promo_dtl_id    = rpdm1.promo_dtl_id
                                        and rpzl1.promo_dtl_id   = rpdm1.promo_dtl_id
                                        and rpzl1.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE)
              union all
              -- Parent Item Diff Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
                where im.item_parent = prm.item
                  and (   im.diff_1  = prm.diff_id
                       or im.diff_2  = prm.diff_id
                       or im.diff_3  = prm.diff_id
                       or im.diff_4  = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              --Skulist Tran Level Items
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpds.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_skulist rpds,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpds.price_event_id         = rpd.promo_dtl_id
                         and rpdmn.skulist               = rpds.skulist
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              --Skulist Parent Level Items
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpds.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_skulist rpds,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                         and rpds.price_event_id         = rpd.promo_dtl_id
                         and rpdmn.skulist               = rpds.skulist
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Parent Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.change_amount,
                     prm.change_currency,
                     prm.change_percent,
                     prm.change_selling_uom,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rmld.item,
                             rpdmn.diff_id,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   RPM_CONSTANTS.RETAIL_EXCLUDE
                                else
                                   rpddl.change_type
                             end change_type,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_amount
                             end change_amount,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_currency
                             end change_currency,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_percent
                             end change_percent,
                             case
                                when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                                   NULL
                                else
                                   rpddl.change_selling_uom
                             end change_selling_uom,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_simple_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1)
     into L_sp_cust_seg_count
     from rpm_lm_pr_overlap_simple_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1)
     into L_sp_no_cust_seg_count
     from rpm_lm_pr_overlap_simple_gtt
    where cust_seg_comp_ind = 0;

   if (L_sp_cust_seg_count + L_sp_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl (promo_dtl_id,
                                 promo_comp_id,
                                 promo_dtl_display_id,
                                 ignore_constraints,
                                 apply_to_code,
                                 start_date,
                                 end_date,
                                 approval_date,
                                 create_date,
                                 create_id,
                                 approval_id,
                                 state,
                                 attribute_1,
                                 attribute_2,
                                 attribute_3,
                                 exception_parent_id,
                                 from_location_move,
                                 price_guide_id,
                                 threshold_id,
                                 timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when LP_lm_prom_overlap_behav IN (LP_EXTEND_EXISTING,
                                                     LP_DO_NOT_START_EXISTING) then
                      case
                         when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                            rpd.start_date
                         else
                            LP_vdate+1
                      end
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date
                end,
                rpd.end_date,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_simple_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_simple_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_simple_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_simple_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert all
         into rpm_promo_dtl_merch_node(promo_dtl_merch_node_id,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
                               values (RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                                       promo_dtl_list_id,
                                       promo_dtl_id,
                                       merch_type,
                                       dept,
                                       class,
                                       subclass,
                                       item,
                                       diff_id)
         into rpm_promo_dtl_disc_ladder(promo_dtl_disc_ladder_id,
                                        promo_dtl_list_id,
                                        change_type,
                                        change_amount,
                                        change_currency,
                                        change_percent,
                                        change_selling_uom,
                                        qual_type,
                                        qual_value)
                                values (RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                                        promo_dtl_list_id,
                                        change_type,
                                        change_amount,
                                        change_currency,
                                        change_percent,
                                        change_selling_uom,
                                        RPM_CONSTANTS.QUAL_QUANTITY,
                                        1)
         select list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id,
                gtt.change_type,
                gtt.change_amount,
                gtt.change_currency,
                gtt.change_percent,
                gtt.change_selling_uom
           from rpm_lm_pr_overlap_simple_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Simple Promotion Creation ',
                      L_start_time);

   end if;

   -- start threshold promotions

   L_start_time := SYSTIMESTAMP;

   insert into rpm_lm_pr_overlap_thres_gtt (promo_dtl_id,
                                            promo_dtl_display_id,
                                            promo_comp_id,
                                            ignore_constraints,
                                            apply_to_code,
                                            create_date,
                                            create_id,
                                            state,
                                            attribute_1,
                                            attribute_2,
                                            attribute_3,
                                            exception_parent_id,
                                            from_location_move,
                                            price_guide_id,
                                            threshold_id,
                                            zone_node_type,
                                            location,
                                            merch_type,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            diff_id,
                                            promo_dtl_row_id,
                                            cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, -- from_location_move,
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
               from item_master im,
                    (select /*+ ORDERED */
                            rpd.promo_comp_id,
                            rpd.ignore_constraints,
                            rpd.apply_to_code,
                            rpd.attribute_1,
                            rpd.attribute_2,
                            rpd.attribute_3,
                            rpd.promo_dtl_id exception_parent_id,
                            rpd.price_guide_id,
                            rpd.threshold_id,
                            rpdmn.dept,
                            rpdmn.class,
                            rpdmn.subclass,
                            rpdmn.item,
                            rpdmn.diff_id,
                            rpd.rowid row_id,
                            case
                               when rpc.customer_type is NOT NULL then
                                  1
                               else
                                  0
                            end cust_seg_comp_ind
                       from rpm_promo_zone_location rpzl,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc,
                            rpm_promo_dtl_merch_node rpdmn
                      where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and rpzl.zone_id        = L_zone_id
                        and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state     = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                        and L_effective_date    > rpd.start_date
                        and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                        and rpc.promo_comp_id   = rpd.promo_comp_id
                        and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                        and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                        and rpdmn.merch_type    IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                    RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                    RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)) prm
              where im.dept         = prm.dept
                and im.class        = NVL(prm.class, im.class)
                and im.subclass     = NVL(prm.subclass, im.subclass)
                and im.item_level   = im.tran_level
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state  = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state      = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LEVEL_ITEM) prm
              where im.item         = prm.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id        = L_zone_id
                         and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state  = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state      = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date    > rpd.start_date
                         and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id   = rpd.promo_comp_id
                         and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                         and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                         and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LEVEL_ITEM) prm
              where im.item_parent  = prm.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Parent Item Diff
             select /*+ NO_MERGE(prm) */
                    prm.promo_comp_id,
                    prm.ignore_constraints,
                    prm.apply_to_code,
                    prm.attribute_1,
                    prm.attribute_2,
                    prm.attribute_3,
                    prm.exception_parent_id,
                    prm.price_guide_id,
                    prm.threshold_id,
                    im.dept,
                    im.class,
                    im.subclass,
                    im.item,
                    prm.diff_id,
                    prm.row_id,
                    prm.cust_seg_comp_ind
               from item_master im,
                    (select /*+ ORDERED */
                            rpd.promo_comp_id,
                            rpd.ignore_constraints,
                            rpd.apply_to_code,
                            rpd.attribute_1,
                            rpd.attribute_2,
                            rpd.attribute_3,
                            rpd.promo_dtl_id exception_parent_id,
                            rpd.price_guide_id,
                            rpd.threshold_id,
                            rpdmn.dept,
                            rpdmn.class,
                            rpdmn.subclass,
                            rpdmn.item,
                            rpdmn.diff_id,
                            rpd.rowid row_id,
                            case
                               when rpc.customer_type is NOT NULL then
                                  1
                               else
                                  0
                            end cust_seg_comp_ind
                       from rpm_promo_zone_location rpzl,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc,
                            rpm_promo_dtl_merch_node rpdmn
                      where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and rpzl.zone_id        = L_zone_id
                        and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state     = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                        and L_effective_date    > rpd.start_date
                        and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                        and rpc.promo_comp_id   = rpd.promo_comp_id
                        and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                        and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                        and rpdmn.merch_type    = RPM_CONSTANTS.PARENT_ITEM_DIFF) prm
              where im.item_parent  = prm.item
                and (   im.diff_1   = prm.diff_id
                     or im.diff_2   = prm.diff_id
                     or im.diff_3   = prm.diff_id
                     or im.diff_4   = prm.diff_id)
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             --Skulist Tran Level Items
             select /*+ NO_MERGE(prm) */
                    prm.promo_comp_id,
                    prm.ignore_constraints,
                    prm.apply_to_code,
                    prm.attribute_1,
                    prm.attribute_2,
                    prm.attribute_3,
                    prm.exception_parent_id,
                    prm.price_guide_id,
                    prm.threshold_id,
                    im.dept,
                    im.class,
                    im.subclass,
                    im.item,
                    prm.diff_id,
                    prm.row_id,
                    prm.cust_seg_comp_ind
               from item_master im,
                    (select /*+ ORDERED */
                            rpd.promo_comp_id,
                            rpd.ignore_constraints,
                            rpd.apply_to_code,
                            rpd.attribute_1,
                            rpd.attribute_2,
                            rpd.attribute_3,
                            rpd.promo_dtl_id exception_parent_id,
                            rpd.price_guide_id,
                            rpd.threshold_id,
                            rpdmn.dept,
                            rpdmn.class,
                            rpdmn.subclass,
                            rpds.item,
                            rpdmn.diff_id,
                            rpd.rowid row_id,
                            case
                               when rpc.customer_type is NOT NULL then
                                  1
                               else
                                  0
                            end cust_seg_comp_ind
                       from rpm_promo_zone_location rpzl,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc,
                            rpm_promo_dtl_merch_node rpdmn,
                            rpm_promo_dtl_skulist rpds
                      where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and rpzl.zone_id        = L_zone_id
                        and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state     = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                        and L_effective_date    > rpd.start_date
                        and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                        and rpc.promo_comp_id   = rpd.promo_comp_id
                        and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                        and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                        and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        and rpds.price_event_id = rpd.promo_dtl_id
                        and rpdmn.skulist       = rpds.skulist) prm
              where im.item         = prm.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             --Skulist Parent Level Items
             select /*+ NO_MERGE(prm) */
                    prm.promo_comp_id,
                    prm.ignore_constraints,
                    prm.apply_to_code,
                    prm.attribute_1,
                    prm.attribute_2,
                    prm.attribute_3,
                    prm.exception_parent_id,
                    prm.price_guide_id,
                    prm.threshold_id,
                    im.dept,
                    im.class,
                    im.subclass,
                    im.item,
                    prm.diff_id,
                    prm.row_id,
                    prm.cust_seg_comp_ind
               from item_master im,
                    (select /*+ ORDERED */
                            rpd.promo_comp_id,
                            rpd.ignore_constraints,
                            rpd.apply_to_code,
                            rpd.attribute_1,
                            rpd.attribute_2,
                            rpd.attribute_3,
                            rpd.promo_dtl_id exception_parent_id,
                            rpd.price_guide_id,
                            rpd.threshold_id,
                            rpdmn.dept,
                            rpdmn.class,
                            rpdmn.subclass,
                            rpds.item,
                            rpdmn.diff_id,
                            rpd.rowid row_id,
                            case
                               when rpc.customer_type is NOT NULL then
                                  1
                               else
                                  0
                            end cust_seg_comp_ind
                       from rpm_promo_zone_location rpzl,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc,
                            rpm_promo_dtl_merch_node rpdmn,
                            rpm_promo_dtl_skulist rpds
                      where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and rpzl.zone_id        = L_zone_id
                        and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state     = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                        and L_effective_date    > rpd.start_date
                        and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                        and rpc.promo_comp_id   = rpd.promo_comp_id
                        and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                        and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                        and rpdmn.merch_type    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        and rpds.price_event_id = rpd.promo_dtl_id
                        and rpdmn.skulist       = rpds.skulist) prm
              where im.item_parent  = prm.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Price Event ItemList - Parent Item Level
             select /*+ NO_MERGE(prm) */
                    prm.promo_comp_id,
                    prm.ignore_constraints,
                    prm.apply_to_code,
                    prm.attribute_1,
                    prm.attribute_2,
                    prm.attribute_3,
                    prm.exception_parent_id,
                    prm.price_guide_id,
                    prm.threshold_id,
                    im.dept,
                    im.class,
                    im.subclass,
                    im.item,
                    prm.diff_id,
                    prm.row_id,
                    prm.cust_seg_comp_ind
               from item_master im,
                    (select /*+ ORDERED */
                            rpd.promo_comp_id,
                            rpd.ignore_constraints,
                            rpd.apply_to_code,
                            rpd.attribute_1,
                            rpd.attribute_2,
                            rpd.attribute_3,
                            rpd.promo_dtl_id exception_parent_id,
                            rpd.price_guide_id,
                            rpd.threshold_id,
                            rpdmn.dept,
                            rpdmn.class,
                            rpdmn.subclass,
                            rmld.item,
                            rpdmn.diff_id,
                            rpd.rowid row_id,
                            case
                               when rpc.customer_type is NOT NULL then
                                  1
                               else
                                  0
                            end cust_seg_comp_ind
                       from rpm_promo_zone_location rpzl,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc,
                            rpm_promo_dtl_merch_node rpdmn,
                            rpm_merch_list_detail rmld
                      where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and rpzl.zone_id        = L_zone_id
                        and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state     = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                        and L_effective_date    > rpd.start_date
                        and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                        and rpc.promo_comp_id   = rpd.promo_comp_id
                        and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                        and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                        and rpdmn.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                        and rmld.merch_list_id  = rpdmn.price_event_itemlist
                        and rmld.merch_level    = RPM_CONSTANTS.PARENT_MERCH_TYPE) prm
              where im.item_parent  = prm.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             union all
             -- Price Event ItemList - Tran Item Level
             select /*+ NO_MERGE(prm) */
                    prm.promo_comp_id,
                    prm.ignore_constraints,
                    prm.apply_to_code,
                    prm.attribute_1,
                    prm.attribute_2,
                    prm.attribute_3,
                    prm.exception_parent_id,
                    prm.price_guide_id,
                    prm.threshold_id,
                    im.dept,
                    im.class,
                    im.subclass,
                    im.item,
                    prm.diff_id,
                    prm.row_id,
                    prm.cust_seg_comp_ind
               from item_master im,
                    (select /*+ ORDERED */
                            rpd.promo_comp_id,
                            rpd.ignore_constraints,
                            rpd.apply_to_code,
                            rpd.attribute_1,
                            rpd.attribute_2,
                            rpd.attribute_3,
                            rpd.promo_dtl_id exception_parent_id,
                            rpd.price_guide_id,
                            rpd.threshold_id,
                            rpdmn.dept,
                            rpdmn.class,
                            rpdmn.subclass,
                            rmld.item,
                            rpdmn.diff_id,
                            rpd.rowid row_id,
                            case
                               when rpc.customer_type is NOT NULL then
                                  1
                               else
                                  0
                            end cust_seg_comp_ind
                       from rpm_promo_zone_location rpzl,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc,
                            rpm_promo_dtl_merch_node rpdmn,
                            rpm_merch_list_detail rmld
                      where rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and rpzl.zone_id        = L_zone_id
                        and rpd.promo_dtl_id    = rpzl.promo_dtl_id
                         and (   (    rpd.state = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state     = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                        and L_effective_date    > rpd.start_date
                        and L_effective_date   <= NVL(rpd.end_date, L_effective_date)
                        and rpc.promo_comp_id   = rpd.promo_comp_id
                        and rpc.type            = RPM_CONSTANTS.THRESHOLD_CODE
                        and rpd.promo_dtl_id    = rpdmn.promo_dtl_id
                        and rpdmn.merch_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                        and rmld.merch_list_id  = rpdmn.price_event_itemlist
                        and rmld.merch_level    = RPM_CONSTANTS.ITEM_MERCH_TYPE) prm
              where im.item         = prm.item
                and im.item_level   = im.tran_level
                and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
      where EXISTS (select 1
                      from item_loc il
                     where il.item = t.item
                       and il.loc  = L_location);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_thres_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1)
     into L_tp_cust_seg_count
     from rpm_lm_pr_overlap_thres_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1)
     into L_tp_no_cust_seg_count
     from rpm_lm_pr_overlap_thres_gtt
    where cust_seg_comp_ind = 0;

   if (L_tp_cust_seg_count + L_tp_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl (promo_dtl_id,
                                 promo_comp_id,
                                 promo_dtl_display_id,
                                 ignore_constraints,
                                 apply_to_code,
                                 start_date,
                                 end_date,
                                 approval_date,
                                 create_date,
                                 create_id,
                                 approval_id,
                                 state,
                                 attribute_1,
                                 attribute_2,
                                 attribute_3,
                                 exception_parent_id,
                                 from_location_move,
                                 price_guide_id,
                                 threshold_id,
                                 timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when LP_lm_prom_overlap_behav IN (LP_EXTEND_EXISTING,
                                                     LP_DO_NOT_START_EXISTING) then
                      case
                         when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                            rpd.start_date
                         else
                            LP_vdate+1
                      end
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date
                end,
                rpd.end_date,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_thres_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_thres_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_thres_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_thres_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert into rpm_promo_dtl_merch_node (promo_dtl_merch_node_id,
                                            promo_dtl_list_id,
                                            promo_dtl_id,
                                            merch_type,
                                            dept,
                                            class,
                                            subclass,
                                            item,
                                            diff_id)
         select RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id
           from rpm_lm_pr_overlap_thres_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      insert into rpm_promo_dtl_disc_ladder (promo_dtl_disc_ladder_id,
                                             promo_dtl_list_id,
                                             change_type,
                                             change_amount,
                                             change_currency,
                                             change_percent,
                                             change_selling_uom,
                                             qual_type,
                                             qual_value)
         select RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                promo_dtl_list_id,
                change_type,
                change_amount,
                change_currency,
                change_percent,
                change_selling_uom,
                RPM_CONSTANTS.QUAL_QUANTITY,
                1
           from (select rpdl.promo_dtl_list_id,
                    case
                       when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                          RPM_CONSTANTS.RETAIL_EXCLUDE
                       else
                          rpddl.change_type
                    end change_type,
                    case
                       when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                          NULL
                       else
                          rpddl.change_amount
                    end change_amount,
                    case
                       when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                          NULL
                       else
                          rpddl.change_currency
                    end change_currency,
                    case
                       when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                          NULL
                       else
                          rpddl.change_percent
                    end change_percent,
                    case
                       when LP_lm_prom_overlap_behav = LP_EXTEND_EXISTING then
                          NULL
                       else
                          rpddl.change_selling_uom
                    end change_selling_uom
               from rpm_lm_pr_overlap_thres_gtt gtt,
                    rpm_promo_dtl_list_grp rpdlg,
                    rpm_promo_dtl_list rpdl,
                    rpm_promo_dtl_list_grp rpdlg_parent,
                    rpm_promo_dtl_list rpdl_parent,
                    rpm_promo_dtl_disc_ladder rpddl
              where gtt.promo_dtl_id                   = rpdlg.promo_dtl_id
                and rpdlg.promo_dtl_list_grp_id        = rpdl.promo_dtl_list_grp_id
                and rpdl.promo_dtl_list_id             = rpdl.promo_dtl_list_id
                and gtt.exception_parent_id            = rpdlg_parent.promo_dtl_id
                and rpdlg_parent.promo_dtl_list_grp_id = rpdl_parent.promo_dtl_list_grp_id
                and rpdl_parent.promo_dtl_list_id      = rpddl.promo_dtl_list_id);

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Threshold Promotion Creation ',
                      L_start_time);

   end if;
   -- end threshold

   -- start buyget

   L_start_time := SYSTIMESTAMP;

   insert into rpm_lm_pr_overlap_bg_gtt (promo_dtl_id,
                                         promo_dtl_display_id,
                                         promo_comp_id,
                                         ignore_constraints,
                                         apply_to_code,
                                         create_date,
                                         create_id,
                                         state,
                                         attribute_1,
                                         attribute_2,
                                         attribute_3,
                                         exception_parent_id,
                                         from_location_move,
                                         price_guide_id,
                                         threshold_id,
                                         zone_node_type,
                                         location,
                                         merch_type,
                                         dept,
                                         class,
                                         subclass,
                                         item,
                                         diff_id,
                                         change_type,
                                         promo_dtl_row_id,
                                         cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, -- from_location_move
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.change_type,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Diff Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and (   im.diff_1   = prm.diff_id
                      or im.diff_2   = prm.diff_id
                      or im.diff_3   = prm.diff_id
                      or im.diff_4   = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1,
                                rpm_promo_zone_location rpzl1,
                                rpm_promo_dtl_merch_node rpdmn1
                          where rpd1.exception_parent_id = t.exception_parent_id
                            and rpd1.promo_dtl_id        = rpzl1.promo_dtl_id
                            and rpzl1.location           = L_location
                            and rpd1.promo_dtl_id        = rpdmn1.promo_dtl_id
                            and rpdmn1.item              = t.item
                            and rpd1.sys_generated_exclusion = 0)
         and EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location)
         and NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1
                          where rpd1.state                = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                            and rpd1.promo_dtl_id         = t.exception_parent_id
                            and rownum                    > 0
                            and LP_lm_prom_overlap_behav  = LP_END_EXISTING
                            and L_effective_date         <= LP_vdate + 1
                            and rpd1.end_date             = LP_vdate + 1);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                       '-Insert into rpm_lm_pr_overlap_bg_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1)
     into L_bg_cust_seg_count
     from rpm_lm_pr_overlap_bg_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1)
     into L_bg_no_cust_seg_count
     from rpm_lm_pr_overlap_bg_gtt
    where cust_seg_comp_ind = 0;

   if (L_bg_cust_seg_count + L_bg_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl (promo_dtl_id,
                                 promo_comp_id,
                                 promo_dtl_display_id,
                                 ignore_constraints,
                                 apply_to_code,
                                 start_date,
                                 end_date,
                                 approval_date,
                                 create_date,
                                 create_id,
                                 approval_id,
                                 state,
                                 attribute_1,
                                 attribute_2,
                                 attribute_3,
                                 exception_parent_id,
                                 from_location_move,
                                 price_guide_id,
                                 threshold_id,
                                 timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when LP_lm_prom_overlap_behav IN (LP_EXTEND_EXISTING,
                                                     LP_DO_NOT_START_EXISTING) then
                      case
                         when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                            rpd.start_date
                         else
                            LP_vdate+1
                      end
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date
                end,
                rpd.end_date,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_bg_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_bg_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_bg_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_bg_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert all
         into rpm_promo_dtl_merch_node (promo_dtl_merch_node_id,
                                        promo_dtl_list_id,
                                        promo_dtl_id,
                                        merch_type,
                                        dept,
                                        class,
                                        subclass,
                                        item,
                                        diff_id)
                                values (RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                                        promo_dtl_list_id,
                                        promo_dtl_id,
                                        merch_type,
                                        dept,
                                        class,
                                        subclass,
                                        item,
                                        diff_id)
         into rpm_promo_dtl_disc_ladder (promo_dtl_disc_ladder_id,
                                         promo_dtl_list_id,
                                         change_type,
                                         change_amount,
                                         change_currency,
                                         change_percent,
                                         change_selling_uom,
                                         qual_type,
                                         qual_value)
                                 values (RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                                         promo_dtl_list_id,
                                         change_type,
                                         change_amount,
                                         change_currency,
                                         change_percent,
                                         change_selling_uom,
                                         RPM_CONSTANTS.QUAL_QUANTITY,
                                         1)
         select list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id,
                gtt.change_type,
                gtt.change_amount,
                gtt.change_currency,
                gtt.change_percent,
                gtt.change_selling_uom
           from rpm_lm_pr_overlap_bg_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Complex Promotion Creation ',
                      L_start_time);

   end if;

   -- start transaction

   L_start_time := SYSTIMESTAMP;

   insert into rpm_lm_pr_overlap_txp_gtt (promo_dtl_id,
                                          promo_dtl_display_id,
                                          promo_comp_id,
                                          ignore_constraints,
                                          apply_to_code,
                                          create_date,
                                          create_id,
                                          state,
                                          attribute_1,
                                          attribute_2,
                                          attribute_3,
                                          exception_parent_id,
                                          from_location_move,
                                          price_guide_id,
                                          threshold_id,
                                          zone_node_type,
                                          location,
                                          merch_type,
                                          dept,
                                          class,
                                          subclass,
                                          item,
                                          diff_id,
                                          change_type,
                                          promo_dtl_row_id,
                                          cust_seg_comp_ind)
      select RPM_PROMO_DTL_SEQ.NEXTVAL,
             RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,
             t.promo_comp_id,
             t.ignore_constraints,
             t.apply_to_code,
             LP_vdate,
             I_user_name,
             RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
             t.attribute_1,
             t.attribute_2,
             t.attribute_3,
             t.exception_parent_id,
             1, -- from_location_move
             t.price_guide_id,
             t.threshold_id,
             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
             L_location location,
             RPM_CONSTANTS.ITEM_LEVEL_ITEM,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.diff_id,
             t.change_type,
             t.row_id,
             t.cust_seg_comp_ind
        from (-- Merch Hierarchy Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.dept         = prm.dept
                 and im.class        = NVL(prm.class, im.class)
                 and im.subclass     = NVL(prm.subclass, im.subclass)
                 and im.item_level   = im.tran_level
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Parent Item Diff Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PARENT_ITEM_DIFF
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and (   im.diff_1   = prm.diff_id
                      or im.diff_2   = prm.diff_id
                      or im.diff_3   = prm.diff_id
                      or im.diff_4   = prm.diff_id)
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Parent Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item_parent  = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Price Event ItemList - Tran Item Level
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     prm.diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             rpdmn.dept,
                             rpdmn.class,
                             rpdmn.subclass,
                             rpdmn.item,
                             rpdmn.diff_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn,
                             rpm_merch_list_detail rmld,
                             rpm_promo_dtl_list_grp rpdlg,
                             rpm_promo_dtl_list rpdl,
                             rpm_promo_dtl_disc_ladder rpddl
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                         and rmld.merch_list_id          = rpdmn.price_event_itemlist
                         and rmld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                         and rpdl.promo_dtl_list_id      = rpdmn.promo_dtl_list_id
                         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                         and rpddl.change_type           is NOT NULL) prm
               where im.item         = prm.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
              union all
              -- Storewide
              select /*+ NO_MERGE(prm) */
                     prm.promo_comp_id,
                     prm.ignore_constraints,
                     prm.apply_to_code,
                     prm.attribute_1,
                     prm.attribute_2,
                     prm.attribute_3,
                     prm.exception_parent_id,
                     prm.price_guide_id,
                     prm.threshold_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     im.item,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     prm.change_type,
                     prm.row_id,
                     prm.cust_seg_comp_ind
                from item_loc il,
                     item_master im,
                     (select /*+ ORDERED */
                             rpd.promo_comp_id,
                             rpd.ignore_constraints,
                             rpd.apply_to_code,
                             rpd.attribute_1,
                             rpd.attribute_2,
                             rpd.attribute_3,
                             rpd.promo_dtl_id exception_parent_id,
                             rpd.price_guide_id,
                             rpd.threshold_id,
                             RPM_CONSTANTS.RETAIL_EXCLUDE change_type,
                             rpd.rowid row_id,
                             case
                                when rpc.customer_type is NOT NULL then
                                   1
                                else
                                   0
                             end cust_seg_comp_ind
                        from rpm_promo_zone_location rpzl,
                             rpm_promo_dtl rpd,
                             rpm_promo_comp rpc,
                             rpm_promo_dtl_merch_node rpdmn
                       where rpzl.zone_node_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and rpzl.zone_id                = L_zone_id
                         and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                         and rpd.man_txn_exclusion       = 0
                         and (   (    rpd.state          = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                                  and NOT EXISTS (select 1
                                                    from rpm_promo_zone_location rpzl1
                                                   where rpzl1.promo_dtl_id = rpzl.promo_dtl_id
                                                     and rpzl1.zone_id      = L_old_zone_id))
                              or (rpd.state              = RPM_CONSTANTS.PR_APPROVED_STATE_CODE))
                         and L_effective_date            > rpd.start_date
                         and L_effective_date           <= NVL(rpd.end_date, L_effective_date)
                         and rpc.promo_comp_id           = rpd.promo_comp_id
                         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
                         and rpdmn.promo_dtl_id          = rpd.promo_dtl_id
                         and rpdmn.merch_type            = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM) prm
               where L_location      = il.loc
                 and im.item         = il.item
                 and im.item_level   = im.tran_level
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) t
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1,
                                rpm_promo_zone_location rpzl1,
                                rpm_promo_dtl_merch_node rpdmn1
                          where rpd1.exception_parent_id = t.exception_parent_id
                            and rpd1.promo_dtl_id        = rpzl1.promo_dtl_id
                            and rpzl1.location           = L_location
                            and rpd1.promo_dtl_id        = rpdmn1.promo_dtl_id
                            and rpdmn1.item              = t.item
                            and rpd1.sys_generated_exclusion = 0)
         and EXISTS (select 1
                       from item_loc il
                      where il.item = t.item
                        and il.loc  = L_location)
         and NOT EXISTS (select 1
                           from rpm_promo_dtl rpd1
                          where rpd1.state                = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                            and rpd1.promo_dtl_id         = t.exception_parent_id
                            and rownum                    > 0
                            and LP_lm_prom_overlap_behav  = LP_END_EXISTING
                            and L_effective_date         <= LP_vdate + 1
                            and rpd1.end_date             = LP_vdate + 1);

   LOGGER.LOG_INFORMATION(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                          '-Insert into rpm_lm_pr_overlap_txp_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select COUNT(1)
     into L_txp_cust_seg_count
     from rpm_lm_pr_overlap_txp_gtt
    where cust_seg_comp_ind = 1;

   select COUNT(1)
     into L_txp_no_cust_seg_count
     from rpm_lm_pr_overlap_txp_gtt
    where cust_seg_comp_ind = 0;

   if (L_txp_cust_seg_count + L_txp_no_cust_seg_count) > 0 then

      insert into rpm_promo_dtl (promo_dtl_id,
                                 promo_comp_id,
                                 promo_dtl_display_id,
                                 ignore_constraints,
                                 apply_to_code,
                                 start_date,
                                 end_date,
                                 approval_date,
                                 create_date,
                                 create_id,
                                 approval_id,
                                 state,
                                 attribute_1,
                                 attribute_2,
                                 attribute_3,
                                 exception_parent_id,
                                 from_location_move,
                                 price_guide_id,
                                 threshold_id,
                                 timebased_dtl_ind)
         select gtt.promo_dtl_id,
                gtt.promo_comp_id,
                gtt.promo_dtl_display_id,
                gtt.ignore_constraints,
                gtt.apply_to_code,
                case
                   when LP_lm_prom_overlap_behav IN (LP_EXTEND_EXISTING,
                                                     LP_DO_NOT_START_EXISTING) then
                      case
                         when rpd.state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then
                            rpd.start_date
                         else
                            LP_vdate+1
                      end
                   when LP_lm_prom_overlap_behav = LP_END_EXISTING then
                      L_effective_date
                end,
                rpd.end_date,
                gtt.approval_date,
                gtt.create_date,
                gtt.create_id,
                gtt.approval_id,
                gtt.state,
                gtt.attribute_1,
                gtt.attribute_2,
                gtt.attribute_3,
                gtt.exception_parent_id,
                gtt.from_location_move,
                gtt.price_guide_id,
                gtt.threshold_id,
                rpd.timebased_dtl_ind
           from rpm_lm_pr_overlap_txp_gtt gtt,
                rpm_promo_dtl rpd
          where gtt.promo_dtl_row_id = rpd.rowid;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                location
           from rpm_lm_pr_overlap_txp_gtt;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                promo_dtl_id
           from rpm_lm_pr_overlap_txp_gtt;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id,
                                      description)
         select RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                grp.promo_dtl_list_grp_id,
                LP_lm_promo_child_desc
           from rpm_lm_pr_overlap_txp_gtt gtt,
                rpm_promo_dtl_list_grp grp
          where gtt.promo_dtl_id = grp.promo_dtl_id;

      insert all
         into rpm_promo_dtl_merch_node (promo_dtl_merch_node_id,
                                        promo_dtl_list_id,
                                        promo_dtl_id,
                                        merch_type,
                                        dept,
                                        class,
                                        subclass,
                                        item,
                                        diff_id)
                                values (RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                                        promo_dtl_list_id,
                                        promo_dtl_id,
                                        merch_type,
                                        dept,
                                        class,
                                        subclass,
                                        item,
                                        diff_id)
         into rpm_promo_dtl_disc_ladder (promo_dtl_disc_ladder_id,
                                         promo_dtl_list_id,
                                         change_type,
                                         change_amount,
                                         change_currency,
                                         change_percent,
                                         change_selling_uom,
                                         qual_type,
                                         qual_value)
                                 values (RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                                         promo_dtl_list_id,
                                         change_type,
                                         change_amount,
                                         change_currency,
                                         change_percent,
                                         change_selling_uom,
                                         RPM_CONSTANTS.QUAL_QUANTITY,
                                         1)
         select list.promo_dtl_list_id,
                gtt.promo_dtl_id,
                gtt.merch_type,
                gtt.dept,
                gtt.class,
                gtt.subclass,
                gtt.item,
                gtt.diff_id,
                gtt.change_type,
                gtt.change_amount,
                gtt.change_currency,
                gtt.change_percent,
                gtt.change_selling_uom
           from rpm_lm_pr_overlap_txp_gtt gtt,
                rpm_promo_dtl_list_grp grp,
                rpm_promo_dtl_list list
          where gtt.promo_dtl_id           = grp.promo_dtl_id
            and list.promo_dtl_list_grp_id = grp.promo_dtl_list_grp_id;

      LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id ||
                                   '-Transaction Promotion Creation ',
                      L_start_time);

   end if;

   if L_sp_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_sp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

   if L_tp_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_tp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

   if L_bg_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_bg_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

   if L_txp_no_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_txp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

    if L_sp_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_csp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

   if L_tp_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_ctp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

   if L_bg_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_cbg_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;


   if L_txp_cust_seg_count > 0 then

      if INIT_BULK_CC_PE(O_error_msg,
                         O_bulk_ctxp_cc_pe_id,
                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                         I_user_name,
                         I_location_move_id,
                         LP_locmove_batch) = 0 then
         return 0;
      end if;

   end if;

   LOGGER.LOG_TIME(L_program || '-I_loc_move_id: '|| I_location_move_id,
                   L_start_time);
                   
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LM,
                                        L_program||'-'||I_location_move_id,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program||' for loc_move_id' ||I_location_move_id);

   EXCEPTION

      when OTHERS then
         goto RESOLVE_TO_ZONE_OVERLAPS_2;

   END;

   <<RESOLVE_TO_ZONE_OVERLAPS_2>>

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END RESOLVE_TO_ZONE_OVERLAPS;
----------------------------------------------------------------------------------------------------

FUNCTION INIT_BULK_CC_PE(O_error_msg           OUT VARCHAR2,
                         O_bulk_cc_pe_id       OUT NUMBER,
                         I_price_event_type IN     VARCHAR2,
                         I_user_name        IN     VARCHAR2,
                         I_location_move_id IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                         I_process          IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_LOC_MOVE_SQL.INIT_BULK_CC_PE';

   L_bulk_cc_pe_id NUMBER := NULL;

BEGIN

   L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                                   -- thread_processor_class
                 1,                                     -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);                             -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_simple_gtt rspc
          where cust_seg_comp_ind = 0;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,                           -- user_name
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                                   -- thread_processor_class
                 1,                                     -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);                             -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
      select L_bulk_cc_pe_id,
             promo_dtl_id,
             I_price_event_type
        from rpm_lm_pr_overlap_simple_gtt rspc
       where cust_seg_comp_ind = 1;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,                           -- user_name
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                                   -- thread_processor_class
                 1,                                     -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_thres_gtt rspc
          where cust_seg_comp_ind = 0;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                -- thread_processor_class
                 1,                  -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);           -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_thres_gtt rspc
           where cust_seg_comp_ind = 1;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                -- thread_processor_class
                 1,                  -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);          -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_bg_gtt rspc
          where cust_seg_comp_ind = 0;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                -- thread_processor_class
                 1,                  -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);          -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_bg_gtt rspc
          where cust_seg_comp_ind = 1;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                -- thread_processor_class
                 1,                  -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);          -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_txp_gtt
          where cust_seg_comp_ind = 0;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO then

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  location_move_id,
                                  create_date_time)
         values (L_bulk_cc_pe_id,
                 I_price_event_type,
                 'Y',                                   -- persist_ind
                 RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE, -- start_state
                 RPM_CONSTANTS.PR_APPROVED_STATE_CODE,  -- end_state
                 I_user_name,
                 1,                                     -- emergency_perm
                 RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                 0,                                     -- secondary_ind
                 1,                                     -- asynch_ind
                 0,                                     -- auto_clean_ind,
                 case
                    when I_process = LP_schedule_batch then
                       LP_schedule_thread
                    else
                       LP_preprocess_thread
                 end,                -- thread_processor_class
                 1,                  -- need_il_explode_ind
                 I_location_move_id,
                 LP_vdate);          -- create_date_time

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
         select L_bulk_cc_pe_id,
                promo_dtl_id,
                I_price_event_type
           from rpm_lm_pr_overlap_txp_gtt
          where cust_seg_comp_ind = 1;

   end if;

   O_bulk_cc_pe_id  := L_bulk_cc_pe_id;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END INIT_BULK_CC_PE;
-------------------------------------------------------------------------------------------------------------------

FUNCTION GENERATE_PRICE_CHANGE(O_error_msg                 OUT VARCHAR2,
                               O_bulk_cc_pe_id             OUT RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE,
                               I_loc_move_id            IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                               I_user_name              IN     RPM_BULK_CC_PE.USER_NAME%TYPE,
                               I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_LOC_MOVE_SQL.GENERATE_PRICE_CHANGE';

   L_loc_move_state  RPM_LOCATION_MOVE.STATE%TYPE      := NULL;
   L_bulk_cc_pe_id   RPM_BULK_CC_PE.BULK_CC_PE_ID%TYPE := NULL;
   L_reason_code     RPM_CODES.CODE_ID%TYPE            := NULL;
   L_start_time      TIMESTAMP                         := SYSTIMESTAMP;

   cursor C_GET_LOC_MOVE_DATA is
      select rlm.state
        from rpm_location_move rlm
       where rlm.location_move_id = I_loc_move_id;

   cursor C_GET_LOC_MOVE_REASON_CODE is
      select code_id
        from rpm_codes
       where code = 'LOCMOVE';

BEGIN

   LOGGER.LOG_INFORMATION(L_program|| ' - I_loc_move_id: '|| I_loc_move_id);
   
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LM,
                                        L_program||'-'||I_loc_move_id,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program||' for loc_move_id' ||I_loc_move_id);

   EXCEPTION

      when OTHERS then
         goto GENERATE_PRICE_CHANGE_1;

   END;

   <<GENERATE_PRICE_CHANGE_1>>

   -- get the current status of the location move for validation.
   open C_GET_LOC_MOVE_DATA;
   fetch C_GET_LOC_MOVE_DATA into L_loc_move_state;

   -- If nothing is returned or the status is not scheduled, return an error.
   if C_GET_LOC_MOVE_DATA%NOTFOUND or
      L_loc_move_state != RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE then
       --
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        'Status not Scheduled',
                                         L_program,
                                         NULL);

      close C_GET_LOC_MOVE_DATA;

      return 0;

   end if;

   close C_GET_LOC_MOVE_DATA;

   -- Get the reason code to use for a location move associated price change.
   open C_GET_LOC_MOVE_REASON_CODE;
   fetch C_GET_LOC_MOVE_REASON_CODE into L_reason_code;

   -- If the RPM_CODES table doesn't have a record for location moves,
   -- add one to the table for future use and set the local var accordingly.
   if C_GET_LOC_MOVE_REASON_CODE%NOTFOUND then

      insert into rpm_codes (code_id,
                             code_type,
                             code,
                             description,
                             sim_ind,
                             lock_version)
         select RPM_CODES_SEQ.NEXTVAL,
                3,
                'LOCMOVE',
                'PRICE CHANGE CREATED BY LOCATION MOVE TO ALIGN LOC WITH NEW ZONE',
                0,
                NULL
           from dual;

      close C_GET_LOC_MOVE_REASON_CODE;

      open C_GET_LOC_MOVE_REASON_CODE;
      fetch C_GET_LOC_MOVE_REASON_CODE into L_reason_code;

   end if;

   close C_GET_LOC_MOVE_REASON_CODE;

   -- Clear out the staging table prior to inserting values based
   -- on the location move data.
   delete from rpm_stage_price_change
    where stage_price_change_id < 0;

   insert into rpm_stage_price_change (stage_price_change_id,
                                       reason_code,
                                       item,
                                       location,
                                       zone_node_type,
                                       effective_date,
                                       change_type,
                                       change_amount,
                                       change_currency,
                                       change_selling_uom,
                                       null_multi_ind,
                                       multi_units,
                                       multi_unit_retail,
                                       multi_selling_uom,
                                       ignore_constraints,
                                       auto_approve_ind,
                                       status,
                                       price_change_id,
                                       price_change_display_id)
      select RPM_STAGE_PRICE_CHANGE_SEQ.NEXTVAL * -1,
             L_reason_code,
             item,
             location,
             zone_node_type,
             LP_vdate + 1,                            -- effective_date,
             RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE, -- change_type,
             selling_retail,                          -- change_amount,
             selling_retail_currency,                 -- change_currency,
             selling_uom,
             null_multi_ind,
             multi_units,
             multi_unit_retail,
             multi_selling_uom,
             1,   -- ignore_constraints,
             1,   -- auto_approve_ind,
             'W', -- status,
             RPM_PRICE_CHANGE_SEQ.NEXTVAL,
             RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL
        from (select il.item,
                     lm.new_zone_id,
                     DECODE(il.loc_type,
                            RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE,
                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE) zone_node_type,
                     lm.location,
                     zfr.selling_retail,
                     zfr.selling_retail_currency,
                     DECODE(zfr.pc_multi_units,
                            NULL, 0,
                            1) null_multi_ind,
                     zfr.multi_units,
                     zfr.multi_unit_retail,
                     zfr.multi_selling_uom,
                     zfr.selling_uom,
                     RANK() OVER (PARTITION BY zfr.item,
                                               zfr.zone
                                      ORDER BY zfr.action_date desc) rank_value
                from rpm_location_move lm,
                     rpm_zone_future_retail zfr,
                     item_loc il,
                     rpm_zone rz,
                     rpm_merch_retail_def mrd,
                     item_master im
               where lm.location_move_id             = I_loc_move_id
                 and zfr.zone                        = lm.new_zone_id
                 and rz.zone_id                      = zfr.zone
                 and rz.zone_group_id                = mrd.regular_zone_group
                 and il.loc                          = lm.location
                 and im.status                       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind                 = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item                         = il.item
                 and mrd.dept                        = im.dept
                 and NVL(mrd.class, im.class)        = im.class
                 and NVL(mrd.subclass, im.subclass)  = im.subclass
                 and zfr.action_date                <= lm.effective_date
                 and zfr.item                        = im.item)
       where rank_value = 1;

   -- Create new price change records for all remaining, eligible price changes.

   insert into rpm_price_change (price_change_id,
                                 price_change_display_id,
                                 state,
                                 reason_code,
                                 item,
                                 diff_id,
                                 location,
                                 zone_node_type,
                                 link_code,
                                 effective_date,
                                 change_type,
                                 change_amount,
                                 change_currency,
                                 change_percent,
                                 change_selling_uom,
                                 null_multi_ind,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_unit_retail_currency,
                                 multi_selling_uom,
                                 price_guide_id,
                                 vendor_funded_ind,
                                 create_date,
                                 create_id,
                                 ignore_constraints,
                                 lock_version)
      select price_change_id,
             price_change_display_id,
             RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
             reason_code,
             item,
             diff_id,
             location,
             zone_node_type,
             link_code,
             effective_date,
             change_type,
             change_amount,
             change_currency,
             change_percent,
             change_selling_uom,
             null_multi_ind,
             multi_units,
             multi_unit_retail,
             locs.currency_code,
             multi_selling_uom,
             price_guide_id,
             0, -- vendor_funded_ind
             SYSDATE,
             I_user_name,
             ignore_constraints,
             0 lock_version
        from rpm_stage_price_change,
             (select store loc,
                     currency_code,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loc_type
                from store) locs
       where stage_price_change_id  < 0
         and zone_node_type         = locs.loc_type
         and NVL(location, zone_id) = locs.loc;

   -- Create bulk conflict checking data if price changes were created.

   if SQL%ROWCOUNT != 0 then

      L_bulk_cc_pe_id := RPM_BULK_CC_PE_SEQ.NEXTVAL;

      insert into rpm_bulk_cc_pe (bulk_cc_pe_id,
                                  price_event_type,
                                  persist_ind,
                                  start_state,
                                  end_state,
                                  user_name,
                                  emergency_perm,
                                  secondary_bulk_cc_pe_id,
                                  secondary_ind,
                                  asynch_ind,
                                  auto_clean_ind,
                                  thread_processor_class,
                                  need_il_explode_ind,
                                  online_alert_ind,
                                  location_move_id,
                                  create_date_time,
                                  non_asynch_single_txn_ind)
          values (L_bulk_cc_pe_id,
                  RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                  'Y',                                   -- persist_ind
                  RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE, -- start_state
                  RPM_CONSTANTS.PC_APPROVED_STATE_CODE,  -- end_state
                  I_user_name,
                  1,                                     -- emergency_perm
                  RPM_BULK_CC_PE_SEQ.NEXTVAL,            -- secondary_bulk_cc_pe_id
                  0,                                     -- secondary_ind
                  1,                                     -- asynch_ind
                  0,                                     -- auto_clean_ind
                  I_thread_processor_class,
                  1,                                     -- need_il_explode_ind
                  0,                                     -- online_alert_ind
                  I_loc_move_id,
                  LP_vdate,                              -- create_date_time
                  0);                                    -- non_asynch_single_txn_ind

      insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                         price_event_id,
                                         price_event_type)
        select L_bulk_cc_pe_id,
               price_change_id,
               RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
          from rpm_stage_price_change spc;

   end if;

   -- Clean up the staging and gtt tables.
   delete from rpm_stage_price_change
    where stage_price_change_id < 0;

   delete from rpm_pc_rec_gtt;

   O_bulk_cc_pe_id := L_bulk_cc_pe_id;

   LOGGER.LOG_TIME(L_program|| ' - I_loc_move_id: '|| I_loc_move_id,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LM,
                                        L_program||'-'||I_loc_move_id,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program||' for loc_move_id' ||I_loc_move_id);

   EXCEPTION

      when OTHERS then
         goto GENERATE_PRICE_CHANGE_2;

   END;

   <<GENERATE_PRICE_CHANGE_2>>

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END GENERATE_PRICE_CHANGE;
--------------------------------------------------------

FUNCTION GET_ITEMLOC_ID(I_is_next IN     NUMBER)
RETURN NUMBER IS

   L_itemloc_id           NUMBER    := NULL;

BEGIN

   if I_is_next = 1 then
      select RPM_BULK_CC_PE_ITEMLOC_SEQ.nextval
        into L_itemloc_id
        from dual;
   else
      select RPM_BULK_CC_PE_ITEMLOC_SEQ.currval
        into L_itemloc_id
        from dual;
   end if;

   return L_itemloc_id;

END GET_ITEMLOC_ID;
-------------------------------------------------------------------------------------------------

FUNCTION POPULATE_GTT(O_error_msg      OUT VARCHAR2,
                      I_loc_move_id IN     NUMBER,
                      I_dept        IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) 		:= 'RPM_LOC_MOVE_SQL.POPULATE_GTT';
   L_trace_name VARCHAR2(15) 	:= 'PGT';
   L_start_time TIMESTAMP 		:= SYSTIMESTAMP;
   L_location   NUMBER    		:= NULL;
   L_loc_type   NUMBER    		:= NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_loc_move_id ||
                                       ' - I_dept: '|| I_dept);
                  
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_loc_move_id,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program||' POPULATE_GTT for loc_move_id' ||I_loc_move_id);

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_LMS||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto POPULATE_GTT_1;

   END;

   <<POPULATE_GTT_1>>

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;
   delete rpm_merch_node_zone_node_gtt;

   select location,
          loc_type into L_location,
                        L_loc_type
     from rpm_location_move rlm
    where location_move_id = I_loc_move_id;

   insert into rpm_merch_node_zone_node_gtt (dept,
                                             class,
                                             subclass,
                                             item_parent,
                                             item,
                                             diff_id,
                                             location,
                                             zone_node_type,
                                             zone_id)
      select t.dept,
             t.class,
             t.subclass,
             t.item_parent,
             t.item,
             t.diff_id,
             t.location,
             t.zone_node_type,
             z.zone_id
        from (select im.dept,
                     im.class,
                     im.subclass,
                     il.item,
                     im.item_parent,
                     DECODE(im.item_parent,
                            NULL, NULL,
                            im.diff_1) diff_id,
                     il.loc location,
                     L_loc_type zone_node_type,
                     mrde.regular_zone_group
                from item_loc il,
                     item_master im,
                     rpm_merch_retail_def_expl mrde
               where im.dept         = I_dept
                 and il.loc          = L_location
                 and il.item         = im.item
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level   = im.tran_level
                 and mrde.dept       = im.dept
                 and mrde.class      = im.class
                 and mrde.subclass   = im.subclass) t,
             (select rz.zone_group_id,
                     rz.zone_id
                from rpm_zone_location rzl,
                     rpm_zone rz
               where rzl.location = L_location
                 and rz.zone_id   = rzl.zone_id
                 and rownum       > 0) z
       where t.regular_zone_group = z.zone_group_id (+);

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_loc_move_id ||
                                       ' - I_dept: '|| I_dept ||
                                       ' - Insert into rpm_merch_node_zone_node_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      diff_id,
                                      item_parent,
                                      zone_node_type,
                                      location,
                                      zone_id,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      rfr_rowid,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      max_hier_level,
                                      cur_hier_level)
   select I_loc_move_id,
          DECODE(fr_rank,
                 3, fr_il.future_retail_id,
                 RPM_FUTURE_RETAIL_SEQ.NEXTVAL) future_retail_id,
          fr_il.dept,
          fr_il.class,
          fr_il.subclass,
          fr_il.item,
          fr_il.diff_id,
          fr_il.item_parent,
          fr_il.zone_node_type,
          fr_il.location,
          fr_il.zone_id,
          fr_il.action_date,
          fr_il.selling_retail,
          fr_il.selling_retail_currency,
          fr_il.selling_uom,
          fr_il.multi_units,
          fr_il.multi_unit_retail,
          fr_il.multi_unit_retail_currency,
          fr_il.multi_selling_uom,
          fr_il.clear_retail,
          fr_il.clear_retail_currency,
          fr_il.clear_uom,
          fr_il.simple_promo_retail,
          fr_il.simple_promo_retail_currency,
          fr_il.simple_promo_uom,
          fr_il.price_change_id,
          fr_il.price_change_display_id,
          fr_il.pc_exception_parent_id,
          fr_il.pc_change_type,
          fr_il.pc_change_amount,
          fr_il.pc_change_currency,
          fr_il.pc_change_percent,
          fr_il.pc_change_selling_uom,
          fr_il.pc_null_multi_ind,
          fr_il.pc_multi_units,
          fr_il.pc_multi_unit_retail,
          fr_il.pc_multi_unit_retail_currency,
          fr_il.pc_multi_selling_uom,
          fr_il.pc_price_guide_id,
          fr_il.clearance_id,
          fr_il.clearance_display_id,
          fr_il.clear_mkdn_index,
          fr_il.clear_start_ind,
          fr_il.clear_change_type,
          fr_il.clear_change_amount,
          fr_il.clear_change_currency,
          fr_il.clear_change_percent,
          fr_il.clear_change_selling_uom,
          fr_il.clear_price_guide_id,
          fr_il.loc_move_from_zone_id,
          fr_il.loc_move_to_zone_id,
          fr_il.location_move_id,
          fr_il.lock_version,
          DECODE(fr_rank,
                 3, fr_il.rfr_rowid,
                 NULL) rfr_rowid,
          fr_il.on_simple_promo_ind,
          fr_il.on_complex_promo_ind,
          fr_il.max_hier_level,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC
     from (select rfr.future_retail_id,
                  rfr.dept,
                  rfr.class,
                  rfr.subclass,
                  t.to_item item,
                  t.to_diff_id diff_id,
                  t.to_item_parent item_parent,
                  t.to_zone_node_type zone_node_type,
                  t.to_location location,
                  t.to_zone_id zone_id,
                  rfr.action_date,
                  rfr.selling_retail,
                  rfr.selling_retail_currency,
                  rfr.selling_uom,
                  rfr.multi_units,
                  rfr.multi_unit_retail,
                  rfr.multi_unit_retail_currency,
                  rfr.multi_selling_uom,
                  rfr.clear_retail,
                  rfr.clear_retail_currency,
                  rfr.clear_uom,
                  rfr.simple_promo_retail,
                  rfr.simple_promo_retail_currency,
                  rfr.simple_promo_uom,
                  rfr.price_change_id,
                  rfr.price_change_display_id,
                  rfr.pc_exception_parent_id,
                  rfr.pc_change_type,
                  rfr.pc_change_amount,
                  rfr.pc_change_currency,
                  rfr.pc_change_percent,
                  rfr.pc_change_selling_uom,
                  rfr.pc_null_multi_ind,
                  rfr.pc_multi_units,
                  rfr.pc_multi_unit_retail,
                  rfr.pc_multi_unit_retail_currency,
                  rfr.pc_multi_selling_uom,
                  rfr.pc_price_guide_id,
                  rfr.clearance_id,
                  rfr.clearance_display_id,
                  rfr.clear_mkdn_index,
                  rfr.clear_start_ind,
                  rfr.clear_change_type,
                  rfr.clear_change_amount,
                  rfr.clear_change_currency,
                  rfr.clear_change_percent,
                  rfr.clear_change_selling_uom,
                  rfr.clear_price_guide_id,
                  rfr.loc_move_from_zone_id,
                  rfr.loc_move_to_zone_id,
                  rfr.location_move_id,
                  rfr.lock_version,
                  rfr.rowid rfr_rowid,
                  rfr.on_simple_promo_ind,
                  rfr.on_complex_promo_ind,
                  rfr.max_hier_level,
                  t.fr_rank,
                  MAX(t.fr_rank) OVER (PARTITION BY t.to_item,
                                                    t.to_location,
                                                    t.to_zone_node_type) max_fr_rank
             from (-- get IL data from PZ timelines
                   select 0                                 fr_rank,
                          gtt.dept,
                          gtt.item_parent                   from_item,
                          NULL                              from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type,
                          gtt.zone_id                       to_zone_id
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.zone_id     is NOT NULL
                   union all
                   -- get IL data from PDZ timelines
                   select distinct
                          1                                 fr_rank,
                          gtt.dept,
                          gtt.item_parent                   from_item,
                          gtt.diff_id                       from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type,
                          gtt.zone_id                       to_zone_id
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.diff_id     is NOT NULL
                      and gtt.zone_id     is NOT NULL
                   union all
                   -- get IL data from PL timelines
                   select distinct
                          1                  fr_rank,
                          gtt.dept,
                          gtt.item_parent    from_item,
                          NULL               from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type,
                          gtt.zone_id        to_zone_id
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                   union all
                   -- get IL data from IZ timelines
                   select distinct
                          2                                 fr_rank,
                          gtt.dept,
                          gtt.item                          from_item,
                          gtt.diff_id                       from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type,
                          gtt.zone_id                       to_zone_id
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.zone_id is NOT NULL
                   union all
                   -- get IL data from PDL timelines
                   select distinct
                          2                  fr_rank,
                          gtt.dept,
                          gtt.item_parent    from_item,
                          gtt.diff_id        from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type,
                          gtt.zone_id        to_zone_id
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.diff_id     is NOT NULL
                   union all
                   -- get IL data from IL timelines
                   select distinct
                          3                  fr_rank,
                          gtt.dept,
                          gtt.item           from_item,
                          gtt.diff_id        from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type,
                          gtt.zone_id        to_zone_id
                     from rpm_merch_node_zone_node_gtt gtt) t,
                  rpm_future_retail rfr
            where rfr.dept                 = I_dept
              and rfr.dept                 = t.dept
              and rfr.item                 = t.from_item
              and NVL(rfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
              and rfr.location             = t.from_location
              and rfr.zone_node_type       = t.from_zone_node_type) fr_il
    where fr_rank = max_fr_rank;

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_loc_move_id ||
                                       ' - I_dept: '|| I_dept ||
                                       ' - Insert into rpm_future_retail_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                            promo_item_loc_expl_id,
                                            item,
                                            diff_id,
                                            item_parent,
                                            dept,
                                            class,
                                            subclass,
                                            location,
                                            zone_node_type,
                                            zone_id,
                                            promo_id,
                                            promo_display_id,
                                            promo_secondary_ind,
                                            promo_comp_id,
                                            comp_display_id,
                                            promo_dtl_id,
                                            type,
                                            customer_type,
                                            detail_secondary_ind,
                                            detail_start_date,
                                            detail_end_date,
                                            detail_apply_to_code,
                                            detail_change_type,
                                            detail_change_amount,
                                            detail_change_currency,
                                            detail_change_percent,
                                            detail_change_selling_uom,
                                            detail_price_guide_id,
                                            exception_parent_id,
                                            rpile_rowid,
                                            max_hier_level,
                                            cur_hier_level,
                                            timebased_dtl_ind)
   select I_loc_move_id,
          DECODE(rpile_rank,
                 3, rpile_il.promo_item_loc_expl_id,
                 RPM_PROM_IL_EXPL_SEQ.NEXTVAL) promo_item_loc_expl_id,
          rpile_il.item,
          rpile_il.diff_id,
          rpile_il.item_parent,
          rpile_il.dept,
          rpile_il.class,
          rpile_il.subclass,
          rpile_il.location,
          rpile_il.zone_node_type,
          rpile_il.zone_id,
          rpile_il.promo_id,
          rpile_il.promo_display_id,
          rpile_il.promo_secondary_ind,
          rpile_il.promo_comp_id,
          rpile_il.comp_display_id,
          rpile_il.promo_dtl_id,
          rpile_il.type,
          rpile_il.customer_type,
          rpile_il.detail_secondary_ind,
          rpile_il.detail_start_date,
          rpile_il.detail_end_date,
          rpile_il.detail_apply_to_code,
          rpile_il.detail_change_type,
          rpile_il.detail_change_amount,
          rpile_il.detail_change_currency,
          rpile_il.detail_change_percent,
          rpile_il.detail_change_selling_uom,
          rpile_il.detail_price_guide_id,
          rpile_il.exception_parent_id,
          DECODE(rpile_rank,
                 3, rpile_il.rpile_rowid,
                 NULL) rpile_rowid,
          rpile_il.max_hier_level,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC,
          rpile_il.timebased_dtl_ind
     from (select rpile.promo_item_loc_expl_id,
                  t.to_item item,
                  t.to_diff_id diff_id,
                  t.to_item_parent item_parent,
                  rpile.zone_id,
                  rpile.dept,
                  rpile.class,
                  rpile.subclass,
                  t.to_location location,
                  t.to_zone_node_type zone_node_type,
                  rpile.promo_id,
                  rpile.promo_display_id,
                  rpile.promo_secondary_ind,
                  rpile.promo_comp_id,
                  rpile.comp_display_id,
                  rpile.promo_dtl_id,
                  rpile.type,
                  rpile.customer_type,
                  rpile.detail_secondary_ind,
                  rpile.detail_start_date,
                  rpile.detail_end_date,
                  rpile.detail_apply_to_code,
                  rpile.detail_change_type,
                  rpile.detail_change_amount,
                  rpile.detail_change_currency,
                  rpile.detail_change_percent,
                  rpile.detail_change_selling_uom,
                  rpile.detail_price_guide_id,
                  rpile.exception_parent_id,
                  rpile.rowid rpile_rowid,
                  rpile.max_hier_level,
                  t.rpile_rank,
                  MAX(t.rpile_rank) OVER (PARTITION BY t.to_item,
                                                       t.to_location,
                                                       t.to_zone_node_type) max_rpile_rank,
                  rpile.timebased_dtl_ind
             from (-- get IL data from PZ timelines
                   select 0                                 rpile_rank,
                          gtt.dept,
                          gtt.item_parent                   from_item,
                          NULL                              from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.zone_id     is NOT NULL
                   union all
                   -- get IL data from PDZ timelines
                   select distinct
                          1                                 rpile_rank,
                          gtt.dept,
                          gtt.item_parent                   from_item,
                          gtt.diff_id                       from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.diff_id     is NOT NULL
                      and gtt.zone_id     is NOT NULL
                   union all
                   -- get IL data from PL timelines
                   select distinct
                          1                  rpile_rank,
                          gtt.dept,
                          gtt.item_parent    from_item,
                          NULL               from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                   union all
                   -- get IL data from IZ timelines
                   select distinct
                          2                                 rpile_rank,
                          gtt.dept,
                          gtt.item                          from_item,
                          gtt.diff_id                       from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.zone_id is NOT NULL
                   union all
                   -- get IL data from PDL timelines
                   select distinct
                          2                  rpile_rank,
                          gtt.dept,
                          gtt.item_parent    from_item,
                          gtt.diff_id        from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.diff_id     is NOT NULL
                   union all
                   -- get IL data from IL timelines
                   select distinct
                          3                  rpile_rank,
                          gtt.dept,
                          gtt.item           from_item,
                          gtt.diff_id        from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt) t,
                  rpm_promo_item_loc_expl rpile
            where rpile.dept                 = I_dept
              and rpile.dept                 = t.dept
              and rpile.item                 = t.from_item
              and NVL(rpile.diff_id, '-999') = NVL(t.from_diff_id, '-999')
              and rpile.location             = t.from_location
              and rpile.zone_node_type       = t.from_zone_node_type) rpile_il
    where rpile_rank = max_rpile_rank;

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_loc_move_id ||
                                       ' - I_dept: '|| I_dept ||
                                       ' - Insert into rpm_promo_item_loc_expl_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                              cust_segment_promo_id,
                                              item,
                                              zone_node_type,
                                              location,
                                              action_date,
                                              customer_type,
                                              dept,
                                              promo_retail,
                                              promo_retail_currency,
                                              promo_uom,
                                              complex_promo_ind,
                                              cspfr_rowid,
                                              item_parent,
                                              diff_id,
                                              zone_id,
                                              max_hier_level,
                                              cur_hier_level)
   select I_loc_move_id,
          DECODE(cspfr_rank,
                 3, cspfr_il.cust_segment_promo_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL) cust_segment_promo_id,
          cspfr_il.item,
          cspfr_il.zone_node_type,
          cspfr_il.location,
          cspfr_il.action_date,
          cspfr_il.customer_type,
          cspfr_il.dept,
          cspfr_il.promo_retail,
          cspfr_il.promo_retail_currency,
          cspfr_il.promo_uom,
          cspfr_il.complex_promo_ind,
          DECODE(cspfr_rank,
                 3, cspfr_il.cspfr_rowid,
                 NULL) cspfr_rowid,
          cspfr_il.item_parent,
          cspfr_il.diff_id,
          cspfr_il.zone_id,
          cspfr_il.max_hier_level,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC
     from (select cspfr.cust_segment_promo_id,
                  t.to_item item,
                  t.to_zone_node_type zone_node_type,
                  t.to_location location,
                  cspfr.action_date,
                  cspfr.customer_type,
                  cspfr.dept,
                  cspfr.promo_retail,
                  cspfr.promo_retail_currency,
                  cspfr.promo_uom,
                  cspfr.complex_promo_ind,
                  cspfr.rowid cspfr_rowid,
                  t.to_item_parent item_parent,
                  t.to_diff_id diff_id,
                  cspfr.zone_id,
                  cspfr.max_hier_level,
                  cspfr_rank,
                  MAX(cspfr_rank) OVER (PARTITION BY t.to_item,
                                                     t.to_location,
                                                     t.to_zone_node_type) max_cspfr_rank
             from (-- get IL data from PZ timelines
                   select 0                                 cspfr_rank,
                          gtt.dept,
                          gtt.item_parent                   from_item,
                          NULL                              from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.zone_id     is NOT NULL
                   union all
                   -- get IL data from PDZ timelines
                   select distinct
                          1                                 cspfr_rank,
                          gtt.dept,
                          gtt.item_parent                   from_item,
                          gtt.diff_id                       from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.diff_id     is NOT NULL
                      and gtt.zone_id     is NOT NULL
                   union all
                   -- get IL data from PL timelines
                   select distinct
                          1                  cspfr_rank,
                          gtt.dept,
                          gtt.item_parent    from_item,
                          NULL               from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                   union all
                   -- get IL data from IZ timelines
                   select distinct
                          2                                 cspfr_rank,
                          gtt.dept,
                          gtt.item                          from_item,
                          gtt.diff_id                       from_diff_id,
                          gtt.zone_id                       from_location,
                          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                          gtt.item                          to_item,
                          gtt.item_parent                   to_item_parent,
                          gtt.diff_id                       to_diff_id,
                          gtt.location                      to_location,
                          gtt.zone_node_type                to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.zone_id is NOT NULL
                   union all
                   -- get IL data from PDL timelines
                   select distinct
                          2                  cspfr_rank,
                          gtt.dept,
                          gtt.item_parent    from_item,
                          gtt.diff_id        from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt
                    where gtt.item_parent is NOT NULL
                      and gtt.diff_id     is NOT NULL
                   union all
                   -- get IL data from IL timelines
                   select distinct
                          3                  cspfr_rank,
                          gtt.dept,
                          gtt.item           from_item,
                          gtt.diff_id        from_diff_id,
                          gtt.location       from_location,
                          gtt.zone_node_type from_zone_node_type,
                          gtt.item           to_item,
                          gtt.item_parent    to_item_parent,
                          gtt.diff_id        to_diff_id,
                          gtt.location       to_location,
                          gtt.zone_node_type to_zone_node_type
                     from rpm_merch_node_zone_node_gtt gtt) t,
                 rpm_cust_segment_promo_fr cspfr
           where cspfr.dept                 = I_dept
             and cspfr.dept                 = t.dept
             and cspfr.item                 = t.from_item
             and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
             and cspfr.location             = t.from_location
             and cspfr.zone_node_type       = t.from_zone_node_type) cspfr_il
    where cspfr_rank = max_cspfr_rank
      and EXISTS (select 1
                    from rpm_promo_item_loc_expl_gtt ilex
                   where ilex.dept                 = cspfr_il.dept
                     and ilex.item                 = cspfr_il.item
                     and NVL(ilex.diff_id, '-999') = NVL(cspfr_il.diff_id, '-999')
                     and ilex.location             = cspfr_il.location
                     and ilex.zone_node_type       = cspfr_il.zone_node_type
                     and ilex.customer_type        = cspfr_il.customer_type);

   LOGGER.LOG_INFORMATION(L_program || ' - I_loc_move_id: '|| I_loc_move_id ||
                                       ' - I_dept: '|| I_dept ||
                                       ' - Insert into rpm_cust_segment_promo_fr_gtt - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program || ' - I_loc_move_id: '|| I_loc_move_id || ' - I_dept: '|| I_dept,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_loc_move_id,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program||' POPULATE_GTT for loc_move_id' ||I_loc_move_id);


   EXCEPTION

      when OTHERS then
         goto POPULATE_GTT_2;

   END;

   <<POPULATE_GTT_2>>

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END POPULATE_GTT;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION SCHEDULE_LOCATION_MOVE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                                I_loc_move_id  IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                                I_rib_trans_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                I_persist_ind  IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_LOC_MOVE_SQL.SCHEDULE_LOCATION_MOVE';

   L_trace_name VARCHAR2(15)  := 'SLM';
   L_error_msg  VARCHAR2(500) := NULL;
   L_start_time TIMESTAMP     := SYSTIMESTAMP;

   cursor C_DEPTS is
      select distinct dept
        from deps d
       where EXISTS (select 'x'
                       from rpm_item_loc il,
                            rpm_location_move lm
                      where il.dept             = d.dept
                        and lm.location         = il.loc
                        and lm.location_move_id = I_loc_move_id
                        and rownum              = 1)
       order by dept;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_loc_move_id: '|| I_loc_move_id);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_loc_move_id,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Start of '||L_program||' for loc_move_id' ||I_loc_move_id);

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_LMS||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto SCHEDULE_LOCATION_MOVE_1;

   END;

   <<SCHEDULE_LOCATION_MOVE_1>>


   for rec IN C_DEPTS loop

      if RPM_LOC_MOVE_SQL.POPULATE_GTT(L_error_msg,
                                       I_loc_move_id,
                                       rec.dept) = 0 then

         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_msg));
         return 0;
      end if;

      if RPM_FUTURE_RETAIL_SQL.SCHEDULE_LOCATION_MOVE(O_cc_error_tbl,
                                                      I_loc_move_id,
                                                      I_rib_trans_id,
                                                      I_persist_ind) = 0 then
         return 0;
      end if;

   end loop;

   LOGGER.LOG_TIME(L_program ||' - I_loc_move_id: '|| I_loc_move_id,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_LMS,
                                        L_program||'-'||I_loc_move_id,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'End of '||L_program||' for loc_move_id' ||I_loc_move_id);

   EXCEPTION

      when OTHERS then
         goto SCHEDULE_LOCATION_MOVE_2;

   END;

   <<SCHEDULE_LOCATION_MOVE_2>>
   
   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));

      return 0;

END SCHEDULE_LOCATION_MOVE;
--------------------------------------------------------

FUNCTION COMPLETE_LOCATION_MOVE(O_error_msg      OUT VARCHAR2,
                                I_loc_move_id IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_LOC_MOVE_SQL.COMPLETE_LOCATION_MOVE';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   L_location NUMBER := NULL;
   L_loc_type NUMBER := NULL;
   L_new_zone NUMBER := NULL;
   L_old_zone NUMBER := NULL;

   cursor C_LM_DATA is
      select location,
             loc_type,
             new_zone_id,
             old_zone_id
        from rpm_location_move
       where location_move_id = I_loc_move_id;

   -- This cursor need to get depts by joining item_loc to item_master for performance reasons...
   -- The item_loc table is partioned by loc and then joining to item_master by the item id is
   -- going to be quick and efficient.

   cursor C_DEPS is
      select distinct
             im.dept dept
        from item_loc il,
             item_master im
       where il.loc  = L_location
         and il.item = im.item;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_loc_move_id: '|| I_loc_move_id);

   open C_LM_DATA;
   fetch C_LM_DATA into L_location,
                        L_loc_type,
                        L_new_zone,
                        L_old_zone;
   close C_LM_DATA;

   for rec IN C_DEPS loop

      update rpm_future_retail
         set zone_id        = L_new_zone,
             max_hier_level = NVL2 (L_new_zone, max_hier_level, SUBSTR(max_hier_level,1,1)||'L')
       where dept           = rec.dept
         and location       = L_location
         and zone_node_type = L_loc_type
         and zone_id        is NOT NULL
         and zone_id        = L_old_zone;

      update /*+ INDEX(RPM_PROMO_ITEM_LOC_EXPL, RPM_PROMO_ITEM_LOC_EXPL_I5) */
             rpm_promo_item_loc_expl
         set zone_id        = L_new_zone,
             max_hier_level = NVL2 (L_new_zone, max_hier_level, SUBSTR(max_hier_level,1,1)||'L')
       where dept           = rec.dept
         and location       = L_location
         and zone_node_type = L_loc_type
         and zone_id        is NOT NULL
         and zone_id        = L_old_zone;

      update rpm_cust_segment_promo_fr
         set zone_id        = L_new_zone,
             max_hier_level = NVL2 (L_new_zone, max_hier_level, SUBSTR(max_hier_level,1,1)||'L')
       where dept           = rec.dept
         and location       = L_location
         and zone_node_type = L_loc_type
         and zone_id        is NOT NULL
         and zone_id        = L_old_zone;

   end loop;

   update rpm_zone_location
      set zone_id    = L_new_zone
    where zone_id    = L_old_zone
      and location   = L_location
      and loc_type   = L_loc_type
      and L_new_zone is NOT NULL;

   LOGGER.LOG_TIME(L_program ||' - I_loc_move_id: '|| I_loc_move_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END COMPLETE_LOCATION_MOVE;

--------------------------------------------------------

END RPM_LOC_MOVE_SQL;
/
