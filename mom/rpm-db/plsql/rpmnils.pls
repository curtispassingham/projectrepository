CREATE OR REPLACE PACKAGE RPM_NEW_ITEM_LOC_SQL AS
-----------------------------------------------------------------------------
FUNCTION INITIALIZE_PROCESS(O_error_msg            OUT VARCHAR2,
                            O_max_thread_number    OUT NUMBER,
                            O_process_id           OUT NUMBER,
                            I_luw               IN     NUMBER,
                            I_stage_status      IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION SETUP_PRICE_EVENTS(O_error_msg                 OUT VARCHAR2,
                            O_bulk_cc_pe_ids            OUT OBJ_NUM_NUM_STR_TBL,
                            I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                            I_thread_number          IN     NUMBER,
                            I_process_id             IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION UPDATE_NIL_FAIL(O_error_msg               OUT VARCHAR2,
                         I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                         I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                         I_item                 IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                         I_location             IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                         I_fail_string          IN     RPM_STAGE_ITEM_LOC.ERROR_MSG%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION GET_PE_ITEM_LOC(O_error_msg               OUT VARCHAR2,
                         O_price_event_type        OUT RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                         O_item_locs               OUT OBJ_NUM_NUM_STR_TBL,
                         I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                         I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                         I_price_event_id       IN     RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION STAGE_ITEM_LOC(O_error_msg              OUT VARCHAR2,
                        I_item                IN     ITEM_LOC.ITEM%TYPE,
                        I_loc                 IN     ITEM_LOC.LOC%TYPE,
                        I_loc_type            IN     ITEM_LOC.LOC_TYPE%TYPE,
                        I_selling_unit_retail IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                        I_selling_uom         IN     ITEM_LOC.SELLING_UOM%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION GET_ITEMLOC_ID(I_is_next IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION CLEAN_UP_STAGE(O_error_msg     OUT VARCHAR2,
                        I_process_id IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION THREAD_ROLLUP_FOR_NIL(O_error_msg            OUT VARCHAR2,
                               O_max_thread_number    OUT NUMBER,
                               I_process_id        IN     NUMBER,
                               I_luw               IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION ROLLUP_NIL_DATA(O_error_msg        OUT VARCHAR2,
                         I_process_id    IN     NUMBER,
                         I_thread_number IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION INITIALIZE_RESTART(O_error_msg         OUT VARCHAR2,
                            O_thread_numbers    OUT OBJ_NUMERIC_ID_TABLE,
                            I_process_id     IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION UPDATE_NIL_THREAD_STATUS(O_error_msg        OUT VARCHAR2,
                                  I_process_id    IN     NUMBER,
                                  I_thread_number IN     NUMBER,
                                  I_status        IN     VARCHAR2)
RETURN NUMBER;
-----------------------------------------------------------------------------
PROCEDURE ANALYZE_STAGE_CLEAN_TABLE(O_return_code      OUT NUMBER,
                                    O_error_message    OUT VARCHAR2,
                                    I_schema_owner  IN     VARCHAR2);
-----------------------------------------------------------------------------
END RPM_NEW_ITEM_LOC_SQL;
/

