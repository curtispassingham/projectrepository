CREATE OR REPLACE PACKAGE RPM_WRAPPER AS
-----------------------------------------------------------------------------

COST_MARKUP_CALC_TYPE     CONSTANT  NUMBER(1) := 0;
RETAIL_MARKUP_CALC_TYPE   CONSTANT  NUMBER(1) := 1;

RPM_STORE_LOC_TYPE        CONSTANT  NUMBER(1) := 0;
RPM_WH_LOC_TYPE           CONSTANT  NUMBER(1) := 2;

-----------------------------------------------------------------------------
FUNCTION VALID_UOM_FOR_ITEMS (O_error_message           OUT VARCHAR2,
                              O_valid                   OUT NUMBER,
                              I_from_uom             IN     RPM_FUTURE_RETAIL.SELLING_UOM%TYPE,
                              I_item                 IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                              I_diff_id              IN     RPM_PRICE_CHANGE.DIFF_ID%TYPE,
                              I_dept                 IN     RPM_FUTURE_RETAIL.DEPT%TYPE,
                              I_class                IN     RPM_FUTURE_RETAIL.CLASS%TYPE,
                              I_subclass             IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE,
                              I_skulist              IN     RPM_PRICE_CHANGE.SKULIST%TYPE,
                              I_price_event_itemlist IN     RPM_PRICE_CHANGE.PRICE_EVENT_ITEMLIST%TYPE,
                              I_price_event_type     IN     VARCHAR2,
                              I_price_event_id       IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION UOM_CONVERT_VALUE (O_error_message             OUT rtk_errors.rtk_text%TYPE,
                            O_converted_retail_value    OUT item_loc.unit_retail%TYPE,
                            I_item                   IN     item_master.item%TYPE,
                            I_input_retail_value     IN     item_loc.unit_retail%TYPE,
                            I_from_uom               IN     item_master.standard_uom%TYPE,
                            I_to_uom                 IN     item_master.standard_uom%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION CURRENCY_CONVERT_VALUE (O_error_message             OUT rtk_errors.rtk_text%TYPE,
                                 O_converted_value           OUT item_loc.unit_retail%TYPE,
                                 I_output_currency        IN     currencies.currency_code%TYPE,
                                 I_input_value            IN     item_loc.unit_retail%TYPE,
                                 I_input_currency         IN     currencies.currency_code%TYPE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION ROUND_CURRENCY (O_error_message             OUT rtk_errors.rtk_text%TYPE,
                         O_rounded_value             OUT item_loc.unit_retail%TYPE,
                         I_input_value            IN     item_loc.unit_retail%TYPE,
                         I_input_currency         IN     currencies.currency_code%TYPE,
                         I_markup_calc_type       IN     NUMBER)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION GET_VDATE (O_error_message   OUT rtk_errors.rtk_text%TYPE,
                    O_vdate           OUT DATE)
RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION GET_VAT_RATE_INCLUDE_IND(O_error_message      OUT  VARCHAR2,
                                  O_vat_rate           OUT  vat_code_rates.vat_rate%TYPE,
                                  O_include_vat_ind    OUT  class.class_vat_ind%TYPE,
                                  O_vat_value          OUT  gtax_item_rollup.cum_tax_value%TYPE,
                                  O_tax_type           OUT  system_options.default_tax_type%TYPE,
                                  I_item            IN      item_master.item%TYPE,
                                  I_loc_type        IN      item_loc.loc_type%TYPE,
                                  I_location        IN      item_loc.loc%TYPE,
                                  I_active_date     IN      DATE)
RETURN NUMBER;
-----------------------------------------------------------------------------

FUNCTION GET_SUPPLIER_SITES_IND(O_error_message       OUT  VARCHAR2,
                                O_supplier_sites_ind  OUT  SYSTEM_OPTIONS.SUPPLIER_SITES_IND%TYPE)
RETURN NUMBER;

-----------------------------------------------------------------------------
END;
/