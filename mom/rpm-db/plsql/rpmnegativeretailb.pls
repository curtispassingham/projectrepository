CREATE OR REPLACE PACKAGE BODY RPM_CC_NEG_RETAIL AS
--------------------------------------------------------

FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_CC_NEG_RETAIL.VALIDATE';

   L_error_key  VARCHAR2(255) := NULL;
   L_error_type VARCHAR2(255) := NULL;

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select /*+ CARDINALITY (ccet, 10) */
             price_event_id,
             future_retail_id,
             NULL cs_promo_fr_id
        from rpm_future_retail_gtt
       where price_event_id NOT IN (select ccet.price_event_id
                                      from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and (    selling_retail       < 0
               or clear_retail         < 0
               or simple_promo_retail  < 0)
      union all
      select /*+ CARDINALITY (ccet, 10) */
              cspfg.price_event_id,
              fr.future_retail_id,
              cspfg.cust_segment_promo_id cs_promo_fr_id
         from rpm_cust_segment_promo_fr_gtt cspfg,
              rpm_future_retail_gtt fr
        where cspfg.price_event_id NOT IN (select ccet.price_event_id
                                             from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
          and cspfg.promo_retail   < 0
          and cspfg.price_event_id = fr.price_event_id
          and cspfg.dept           = fr.dept
          and cspfg.item           = fr.item
          and cspfg.location       = fr.location
          and cspfg.action_date    = fr.action_date;

   cursor C_CHECK_CSFR_NO_FR is
      select price_event_id,
             future_retail_id,
             cust_segment_promo_id cs_promo_fr_id
        from (select /*+ NO MERGE */
                     cspfg.price_event_id,
                     fr.future_retail_id,
                     cspfg.cust_segment_promo_id,
                     rank() OVER (PARTITION BY cspfg.price_event_id,
                                               cspfg.item,
                                               cspfg.location,
                                               cspfg.action_date
                                      ORDER BY fr.action_date desc) rank
                from (select pfg.price_event_id,
                             pfg.item,
                             pfg.dept,
                             pfg.location,
                             pfg.action_date,
                             pfg.promo_retail,
                             pfg.cust_segment_promo_id
                        from rpm_cust_segment_promo_fr_gtt pfg,
                             gtt_num_num_str_str_date_date gtt
                       where pfg.price_event_id = gtt.number_1
                         and pfg.item           = gtt.varchar2_1
                         and pfg.location       = gtt.number_2
                         and pfg.action_date    = gtt.date_1
                         and pfg.promo_retail   < 0) cspfg,
                     rpm_future_retail_gtt fr
               where cspfg.price_event_id = fr.price_event_id
                 and cspfg.dept           = fr.dept
                 and cspfg.item           = fr.item
                 and cspfg.location       = fr.location
                 and fr.action_date      <= cspfg.action_date)
       where rank = 1;

BEGIN

   if IO_error_table is NOT NULL and
      IO_error_table.COUNT > 0 then
      L_error_tbl := IO_error_table;
   else
      L_error_rec := NEW CONFLICT_CHECK_ERROR_REC(-99999,
                                                  NULL,
                                                  NULL,
                                                  NULL);
      L_error_tbl := NEW CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   for rec IN C_CHECK loop

      L_error_rec := NEW CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                  rec.future_retail_id,
                                                  RPM_CONSTANTS.CONFLICT_ERROR,
                                                  'event_causes_approved_to_go_negative',
                                                  rec.cs_promo_fr_id);

      if IO_error_table is NULL then
         IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      else
         IO_error_table.EXTEND;
         IO_error_table(IO_error_table.COUNT) := L_error_rec;
      end if;

   end loop;

   -- Get all Customer Segment FR records that have no matching record in Future Retail

   delete from gtt_num_num_str_str_date_date;

   if IO_error_table is NOT NULL and
      IO_error_table.COUNT > 0 then
      L_error_tbl := IO_error_table;
   else
      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);

      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   insert into gtt_num_num_str_str_date_date
      (number_1,   -- price event id
       varchar2_1, -- item
       number_2,   -- location
       date_1)     -- action_date
      select /*+ CARDINALITY (ccet, 10) */
             cspfg.price_event_id,
             cspfg.item,
             cspfg.location,
             cspfg.action_date
        from rpm_cust_segment_promo_fr_gtt cspfg
       where cspfg.price_event_id NOT IN (select ccet.price_event_id
                                            from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
       minus
      select /*+ CARDINALITY (ccet, 10) */
             fr.price_event_id,
             fr.item,
             fr.location,
             fr.action_date
        from rpm_future_retail_gtt fr
       where fr.price_event_id NOT IN (select ccet.price_event_id
                                         from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet);

   for rec IN C_CHECK_CSFR_NO_FR loop

      L_error_rec := NEW CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                  rec.future_retail_id,
                                                  RPM_CONSTANTS.CONFLICT_ERROR,
                                                  'event_causes_approved_to_go_negative',
                                                  rec.cs_promo_fr_id);

      if IO_error_table is NULL then
         IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      else
         IO_error_table.EXTEND;
         IO_error_table(IO_error_table.COUNT) := L_error_rec;
      end if;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

