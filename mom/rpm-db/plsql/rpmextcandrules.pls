CREATE OR REPLACE PACKAGE RPM_EXT_CAND_RULE_SQL AS
----------------------------------------------------------------------------------------

/* NOTE: THIS MUST BE KEEP IN SYNC WITH OperatorCodeConverter.java */
EQUAL                        CONSTANT NUMBER := 0;
GREATER_THAN                 CONSTANT NUMBER := 1;
GREATER_THAN_OR_EQUAL        CONSTANT NUMBER := 2;
LESS_THAN                    CONSTANT NUMBER := 3;
LESS_THAN_OR_EQUAL           CONSTANT NUMBER := 4;
NOT_EQUAL                    CONSTANT NUMBER := 5;
/* NOTE: THIS MUST BE KEEP IN SYNC WITH OperatorCodeConverter.java */

----------------------------------------------------------------------------------------

FUNCTION CAND_RULES_DEL_EXCL (O_error_msg      OUT  VARCHAR2,
                              I_strategy_id IN      rpm_strategy.strategy_id%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------------------

FUNCTION CAND_RULES_LINK_INCL (O_error_msg         OUT  VARCHAR2,
                               I_strategy_id    IN      rpm_strategy.strategy_id%TYPE,
                               O_constraint_ind    OUT  rpm_worksheet_status.constraint_ind%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------------------

FUNCTION EXECUTE_CAND_RULES (O_error_msg        OUT  VARCHAR2,
                             I_strategy_type IN      VARCHAR2,
                             I_dept          IN      rpm_strategy.dept%type,
                             I_exclusion_ind IN      rpm_calendar_period.exclusion_ind%TYPE,
                             I_exception_ind IN      rpm_calendar_period.exception_ind%TYPE)
RETURN BOOLEAN;

----------------------------------------------------------------------------------------

END RPM_EXT_CAND_RULE_SQL;
/

