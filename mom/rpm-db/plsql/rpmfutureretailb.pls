CREATE OR REPLACE PACKAGE BODY RPM_FUTURE_RETAIL_SQL AS

--------------------------------------------------------------------------------
--                            PRIVATE GLOBALS                                 --
--------------------------------------------------------------------------------

   type RF_DATES is table of DATE index by binary_integer;

   LP_vdate                DATE         := GET_VDATE;
   LP_persist_ind          VARCHAR2(30) := NULL;
   LP_push_back_start_date DATE         := NULL;
   LP_specific_item_loc    NUMBER       := 0;
   LP_rib_trans_id         NUMBER       := NULL;
   LP_price_event_type     VARCHAR2(3)  := NULL;
   LP_bulk_cc_pe_id        NUMBER       := NULL;
   LP_pe_sequence_id       NUMBER       := NULL;
   LP_pe_thread_number     NUMBER       := NULL;
   LP_chunk_number         NUMBER       := NULL;
   LP_end_state            VARCHAR2(30) := NULL;
   LP_reprocess_ind        NUMBER       := NULL;
   LP_cancel_il_promo_dtl  NUMBER       := 0;
   LP_nil_ind              NUMBER       := 0;

   LP_cc_error_tbl         CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LP_non_sge_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   LP_from_loc_move_ind           NUMBER := 0;
   LP_elig_for_sys_gen_exclusions NUMBER := 1;

   LP_sge_chunking_pe_data OBJ_NUMERIC_ID_TABLE := NULL;

--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------

FUNCTION INIT_GLOBALS(O_cc_error_tbl OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION MERGE_INTO_TIMELINE(O_cc_error_tbl       IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type   IN     VARCHAR2,
                             I_new_promo_end_date IN     DATE DEFAULT NULL)
RETURN NUMBER;

FUNCTION INSERT_PRICE_CHANGE(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION INSERT_CLEARANCE(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                          I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION UPDATE_CLEARANCE_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION INSERT_PROMO(O_cc_error_tbl       IN OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type   IN     VARCHAR2)
RETURN NUMBER;

FUNCTION UPDATE_PROMO_END_DATE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                               I_price_event_type   IN     VARCHAR2,
                               I_new_promo_end_date IN     DATE DEFAULT NULL)
RETURN NUMBER;

FUNCTION REMOVE_FROM_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type IN     VARCHAR2,
                              I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;

FUNCTION REMOVE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2,
                             I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;

FUNCTION REMOVE_CLEARANCE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                          I_price_event_type IN     VARCHAR2,
                          I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;

FUNCTION VALIDATE_CLEARANCE_DATES(O_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION REMOVE_PROMOTION(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                          I_price_event_type IN     VARCHAR2,
                          I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;

FUNCTION GET_PROMOTION_OBJECT(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                              O_promo_recs            OUT OBJ_RPM_CC_PROMO_TBL,
                              I_promo_ids          IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type   IN     VARCHAR2,
                              I_new_promo_end_date IN     DATE DEFAULT NULL)
RETURN NUMBER;

FUNCTION IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                    I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_EXCLUSION_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION ROLL_FORWARD(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2,
                      I_nil_ind          IN     NUMBER DEFAULT 0)
RETURN NUMBER;

FUNCTION PUSH_BACK_ERROR_ROWS(IO_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                              I_populate_cc_error IN     NUMBER DEFAULT 0)
RETURN NUMBER;

FUNCTION CONSOLIDATE_CC_ERROR_TBL(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_cc_error_tbl1 IN     CONFLICT_CHECK_ERROR_TBL,
                                  I_cc_error_tbl2 IN     CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION VALIDATE_PROMO_FOR_MERGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION VALIDATE_FP_UOM(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION IGNORE_PROMO_SIBLINGS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION PROCESS_CONFLICTS(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;


FUNCTION GENERATE_EXCLUSIONS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                             I_cc_error_tbl IN     CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION UPDATE_SGE_DATA(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                         I_cc_error_tbl    IN     CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

FUNCTION POPULATE_CC_ERROR_PE_ITEM_LOC(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION PRICE_EVENT_INSERT(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                            O_cc_error_gen_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                            I_price_event_type   IN     VARCHAR2,
                            I_rib_trans_id       IN     NUMBER,
                            I_persist_ind        IN     VARCHAR2,
                            I_specific_item_loc  IN     NUMBER   DEFAULT 0,
                            I_new_item_loc       IN     NUMBER   DEFAULT 0,
                            I_new_promo_end_date IN     DATE     DEFAULT NULL,
                            I_bulk_cc_pe_id      IN     NUMBER   DEFAULT NULL,
                            I_pe_sequence_id     IN     NUMBER   DEFAULT NULL,
                            I_pe_thread_number   IN     NUMBER   DEFAULT NULL,
                            I_chunk_number       IN     NUMBER   DEFAULT 0,
                            I_end_state          IN     VARCHAR2 DEFAULT 0,
                            I_reprocess_ind      IN     NUMBER   DEFAULT 0,
                            I_loc_move_ind       IN     NUMBER   DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT';

   L_price_event_ids       OBJ_NUMERIC_ID_TABLE := NULL;
   L_nc_price_event_ids    OBJ_NUMERIC_ID_TABLE := NULL;
   L_no_fr_price_event_ids OBJ_NUMERIC_ID_TABLE := NULL;

   L_start_time       TIMESTAMP                := SYSTIMESTAMP;
   L_cc_error_tbl     CONFLICT_CHECK_ERROR_TBL := NULL;
   L_cc_uom_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_old_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_gtt_count   NUMBER     := NULL;
   L_loc_move_id NUMBER(10) := NULL;

   cursor C_IDS is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where EXISTS (select 1
                       from rpm_future_retail_gtt
                      where price_event_id = VALUE(ids));

   cursor C_NC is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select ccet.price_event_id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet;

   cursor C_GET_NO_FR_PE_IDS is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select VALUE(ids)
        from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids;

   cursor C_GET_VALID_UOM_PE_IDS is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select ccet.price_event_id
        from table(cast(L_cc_uom_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet;

   cursor C_GET_LOC_MOVE_ID is
      select location_move_id
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_SGE_CHUNKING_PE_DATA is
      select price_event_id
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL));

BEGIN

   LP_cc_error_tbl.DELETE;
   LP_non_sge_cc_error_tbl.DELETE;

   O_cc_error_tbl         := CONFLICT_CHECK_ERROR_TBL();
   LP_persist_ind         := I_persist_ind;
   LP_specific_item_loc   := I_specific_item_loc;
   LP_rib_trans_id        := I_rib_trans_id;
   LP_price_event_type    := SUBSTR(I_price_event_type, 0, 3);
   LP_bulk_cc_pe_id       := I_bulk_cc_pe_id;
   LP_pe_sequence_id      := I_pe_sequence_id;
   LP_pe_thread_number    := I_pe_thread_number;
   LP_chunk_number        := I_chunk_number;
   LP_end_state           := I_end_state;
   LP_reprocess_ind       := I_reprocess_ind;
   LP_nil_ind             := I_new_item_loc;
   LP_cancel_il_promo_dtl := 0;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_price_event_type: '|| LP_price_event_type ||
                                      ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   -- I_loc_move_ind is set to 1 only when it is coming from Loc Move synch cc
   if NVL(I_loc_move_ind, 0) = 1 then

      LP_from_loc_move_ind           := 1;
      LP_elig_for_sys_gen_exclusions := 0;

   end if;

   if I_bulk_cc_pe_id is NOT NULL then

      open C_GET_LOC_MOVE_ID;
      fetch C_GET_LOC_MOVE_ID into L_loc_move_id;
      close C_GET_LOC_MOVE_ID;

      if L_loc_move_id is NOT NULL then

         LP_from_loc_move_ind := 1;

      end if;

      select DECODE(elig_for_sys_gen_exclusions,
                    0, 0,
                    1)
        into LP_elig_for_sys_gen_exclusions
        from (select SUM(NVL(elig_for_sys_gen_exclusions, 0)) elig_for_sys_gen_exclusions
                from rpm_bulk_cc_pe_thread
               where bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and parent_thread_number = I_pe_sequence_id
                 and thread_number        = I_pe_thread_number);

   end if;

   open C_IDS;
   fetch C_IDS BULK COLLECT into L_price_event_ids;
   close C_IDS;

   open C_GET_NO_FR_PE_IDS;
   fetch C_GET_NO_FR_PE_IDS BULK COLLECT into L_no_fr_price_event_ids;
   close C_GET_NO_FR_PE_IDS;

   if L_price_event_ids is NOT NULL and
      L_price_event_ids.COUNT > 0 then

      if LP_chunk_number = 0 then

         if VALIDATE_FP_UOM(L_cc_uom_error_tbl,
                            L_price_event_ids,
                            I_price_event_type) = 0 then

            O_cc_error_tbl := L_cc_uom_error_tbl;
            return 0;

         end if;

         if L_cc_uom_error_tbl is NOT NULL and
            L_cc_uom_error_tbl.COUNT > 0 then

            open C_GET_VALID_UOM_PE_IDS;
            fetch C_GET_VALID_UOM_PE_IDS BULK COLLECT into L_price_event_ids;
            close C_GET_VALID_UOM_PE_IDS;

            if LP_price_event_type is NOT NULL and
               L_cc_uom_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then
               ---
               if POPULATE_CC_ERROR_TABLE(L_cc_uom_error_tbl,
                                          LP_price_event_type) = 0 then
                  return 0;
               end if;
            end if;

            forall i IN 1..L_cc_uom_error_tbl.COUNT
               delete
                 from rpm_cust_segment_promo_fr_gtt
                where price_event_id = L_cc_uom_error_tbl(i).price_event_id;

            forall i IN 1..L_cc_uom_error_tbl.COUNT
               delete
                 from rpm_promo_item_loc_expl_gtt
                where price_event_id = L_cc_uom_error_tbl(i).price_event_id;

            forall i IN 1..L_cc_uom_error_tbl.COUNT
               delete
                 from rpm_future_retail_gtt
                where price_event_id = L_cc_uom_error_tbl(i).price_event_id;

         end if;
      end if;

      if MERGE_INTO_TIMELINE(O_cc_error_tbl,
                             L_price_event_ids,
                             I_price_event_type,
                             I_new_promo_end_date) = 0 then
         return 0;
      end if;

      if RPM_EXE_CC_QUERY_RULES.EXECUTE(O_cc_error_tbl,
                                        I_price_event_type) = 0 then

         if PUSH_BACK_ERROR_ROWS(O_cc_error_tbl) = 0 then
            return 0;
         end if;

         return 0;
      end if;

      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 then

         if PROCESS_CONFLICTS(O_cc_error_tbl)= 0 then
            return 0;
         end if;

      end if;

      if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

         if RPM_CLR_RESET.VALIDATE(O_cc_error_tbl) = 0 then
            return 0;
         end if;

         if O_cc_error_tbl is NOT NULL and
            O_cc_error_tbl.COUNT > 0 then
            ---
            if PROCESS_CONFLICTS(O_cc_error_tbl)= 0 then
               return 0;
            end if;
         end if;

      end if;

      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 then
         ---
         L_cc_error_tbl := O_cc_error_tbl;
      end if;

      -- If RPM_FUTURE_RETAIL_GTT is empty, it means all price events have conflicts
      -- and there are no price events required to be created for items not ranged to any locations.
      --    Stop the process.

      select COUNT(1)
        into L_gtt_count
        from rpm_future_retail_gtt;

      if L_gtt_count = 0 and
         (L_no_fr_price_event_ids is NULL or
          L_no_fr_price_event_ids.COUNT = 0) then

         if L_cc_uom_error_tbl is NOT NULL and
            L_cc_uom_error_tbl.COUNT > 0 then

            if O_cc_error_tbl is NULL then
               O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
            end if;

            for i IN 1..L_cc_uom_error_tbl.COUNT loop
               O_cc_error_tbl.EXTEND;
               O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_cc_uom_error_tbl(i);
            end loop;

         end if;

         --assign the global variable for SGE to its output parameter
         O_cc_error_gen_tbl := CONFLICT_CHECK_ERROR_TBL();

         return 1;
      end if;

      if L_gtt_count > 0 and
         I_persist_ind = 'Y' then

         --Store the old CC error records for the gtt delete statements below
         L_old_cc_error_tbl := LP_cc_error_tbl;

         if LP_chunk_number = 0 and
            LP_from_loc_move_ind != 1 and
            LP_elig_for_sys_gen_exclusions = 1 then

            if LP_reprocess_ind = 0 then

               if LP_cc_error_tbl is NOT NULL and
                  LP_cc_error_tbl.COUNT > 0 then

                  if GENERATE_EXCLUSIONS(O_cc_error_tbl,
                                         LP_cc_error_tbl) = 0 then
                     return 0;
                  end if;
               end if;

            --if price event is a reprocess of an exclusion.
            else
               if UPDATE_SGE_DATA(O_cc_error_tbl,
                                  LP_cc_error_tbl,
                                  I_price_event_ids) = 0 then
                  return 0;
               end if;
            end if;

         end if;

         if LP_price_event_type is NOT NULL and
            LP_cc_error_tbl is NOT NULL and
            LP_cc_error_tbl.COUNT > 0 and
            LP_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then

            if POPULATE_CC_ERROR_TABLE(LP_cc_error_tbl,
                                       LP_price_event_type) = 0 then
               return 0;
            end if;
         end if;

         --Delete the Future Retail GTT records for the timelines that SGEs will be created for.
         delete
           from rpm_future_retail_gtt gtt
          where EXISTS (select /*+ CARDINALITY(cc 10) */ 1
                          from table(cast(L_old_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                               rpm_cc_sys_gen_detail_gtt gttd
                         where cc.price_event_id         = gttd.price_event_id
                           and gttd.item                 = gtt.item
                           and NVL(gttd.diff_id, '-999') = NVL(gttd.diff_id, '-999'));

         delete
           from rpm_promo_item_loc_expl_gtt gtt
          where EXISTS (select /*+ CARDINALITY(cc 10) */ 1
                          from table(cast(L_old_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                               rpm_cc_sys_gen_detail_gtt gttd
                         where cc.price_event_id         = gttd.price_event_id
                           and gttd.item                 = gtt.item
                           and NVL(gttd.diff_id, '-999') = NVL(gttd.diff_id, '-999'));

         delete
           from rpm_cust_segment_promo_fr_gtt gtt
          where EXISTS (select /*+ CARDINALITY(cc 10) */ 1
                          from table(cast(L_old_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                               rpm_cc_sys_gen_detail_gtt gttd
                         where cc.price_event_id         = gttd.price_event_id
                           and gttd.item                 = gtt.item
                           and NVL(gttd.diff_id, '-999') = NVL(gttd.diff_id, '-999'));

         if LP_chunk_number = 0 and
            LP_from_loc_move_ind != 1 and
            LP_elig_for_sys_gen_exclusions = 1 then

            if GENERATE_TIMELINE(O_cc_error_tbl,
                                 LP_bulk_cc_pe_id,
                                 LP_pe_sequence_id,
                                 LP_pe_thread_number) = 0 then
               return 0;
            end if;

         elsif LP_chunk_number > 0 and
               LP_from_loc_move_ind != 1 and
               LP_elig_for_sys_gen_exclusions = 1 then

            open C_SGE_CHUNKING_PE_DATA;
            fetch C_SGE_CHUNKING_PE_DATA BULK COLLECT into LP_sge_chunking_pe_data;
            close C_SGE_CHUNKING_PE_DATA;

         end if;

         -- Approve Clearance Reset and Push Back Clearance
         if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                   RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then
            ---
            if RPM_CLEARANCE_GTT_SQL.REMOVE_CONFLICTS(O_cc_error_tbl,
                                                      L_cc_error_tbl) = 0 then
               return 0;
            end if;

            if LP_chunk_number = 0 then
               ---
               if RPM_CLEARANCE_GTT_SQL.PUSH_BACK(O_cc_error_tbl) = 0 then
                  return 0;
               end if;
            end if;

         end if;

         O_cc_error_tbl := L_cc_error_tbl;

         if O_cc_error_tbl is NULL or
            (O_cc_error_tbl is NOT NULL and
             O_cc_error_tbl.COUNT = 0) then

            if I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then
               ---
               if RPM_CLEARANCE_GTT_SQL.PUSH_BACK(O_cc_error_tbl) = 0 then
                  return 0;
               end if;
            end if;

         end if;

         open C_NC;
         fetch C_NC BULK COLLECT into L_nc_price_event_ids;
         close C_NC;

         if L_nc_price_event_ids is NULL or
            L_nc_price_event_ids.COUNT = 0 then
            ---
            return 1;
         end if;

         if PUBLISH_CHANGES(O_cc_error_tbl,
                            I_rib_trans_id,
                            LP_bulk_cc_pe_id,
                            L_nc_price_event_ids,
                            I_price_event_type,
                            NULL,
                            NULL,
                            LP_cancel_il_promo_dtl) = 0 then
            return 0;
         end if;

         if RPM_PURGE_FUTURE_RETAIL_SQL.PURGE(O_cc_error_tbl) = 0 then
            return 0;
         end if;

         LP_push_back_start_date := LP_vdate;

         if LP_chunk_number = 0 then

            if PUSH_BACK(O_cc_error_tbl,
                         I_price_event_type,
                         I_new_item_loc) = 0 then
               return 0;
            end if;

            -- Update RPM_ZONE_FUTURE_RETAIL for Primary Zone Level Price change
            if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

               if L_cc_error_tbl is NOT NULL and
                  L_cc_error_tbl.COUNT > 0 then

                  open C_NC;
                  fetch C_NC BULK COLLECT into L_nc_price_event_ids;
                  close C_NC;

               else

                  L_nc_price_event_ids := I_price_event_ids;

               end if;

               if RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_CHANGE_INSERT(O_cc_error_tbl,
                                                                 L_nc_price_event_ids) = 0 then
                  return 0;
               end if;
            end if;
         end if;
      end if;
   end if;

   if L_no_fr_price_event_ids is NOT NULL and
      L_no_fr_price_event_ids.COUNT > 0 and
      I_persist_ind = 'Y' and
      I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_CHANGE_INSERT(O_cc_error_tbl,
                                                        L_no_fr_price_event_ids) = 0 then
         return 0;
      end if;

   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   if L_cc_uom_error_tbl is NOT NULL and
      L_cc_uom_error_tbl.COUNT > 0 then

      if O_cc_error_tbl is NULL then
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_cc_uom_error_tbl.COUNT loop
         O_cc_error_tbl.EXTEND;
         O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_cc_uom_error_tbl(i);
      end loop;

   end if;

   --Output the SGE conflict errors into its own collection.
   O_cc_error_gen_tbl := LP_cc_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PRICE_EVENT_INSERT;

--------------------------------------------------------------------------------

FUNCTION PRICE_EVENT_REMOVE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                            I_price_event_type IN     VARCHAR2,
                            I_rib_trans_id     IN     NUMBER,
                            I_persist_ind      IN     VARCHAR2,
                            I_bulk_cc_pe_id    IN     NUMBER   DEFAULT NULL,
                            I_pe_sequence_id   IN     NUMBER   DEFAULT NULL,
                            I_pe_thread_number IN     NUMBER   DEFAULT NULL,
                            I_chunk_number     IN     NUMBER   DEFAULT 0,
                            I_end_state        IN     VARCHAR2 DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(42) := 'RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE';

   L_remove_price_event_ids  OBJ_NUMERIC_ID_TABLE     := I_price_event_ids;
   L_remove_price_event_type VARCHAR2(3)              := SUBSTR(I_price_event_type, 0, 3);
   L_price_event_ids         OBJ_NUMERIC_ID_TABLE     := NULL;
   L_no_fr_price_event_ids   OBJ_NUMERIC_ID_TABLE     := NULL;
   L_cc_error_tbl            CONFLICT_CHECK_ERROR_TBL := NULL;
   L_start_time              TIMESTAMP                := SYSTIMESTAMP;

   L_gtt_count NUMBER := NULL;

   cursor C_IDS is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where EXISTS (select 1
                       from rpm_future_retail_gtt
                      where price_event_id = VALUE(ids));

   cursor C_GET_NO_FR_PE_IDS is
      select VALUE(ids) price_event_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select VALUE(ids) price_event_id
        from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: ' || I_price_event_type ||
                                      ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   O_cc_error_tbl      := CONFLICT_CHECK_ERROR_TBL();
   LP_rib_trans_id     := I_rib_trans_id;
   LP_price_event_type := SUBSTR(I_price_event_type, 0, 3);
   LP_bulk_cc_pe_id    := I_bulk_cc_pe_id;
   LP_pe_sequence_id   := I_pe_sequence_id;
   LP_pe_thread_number := I_pe_thread_number;
   LP_chunk_number     := I_chunk_number;
   LP_persist_ind      := I_persist_ind;
   LP_end_state        := I_end_state;

   open C_IDS;
   fetch C_IDS BULK COLLECT into L_price_event_ids;
   close C_IDS;

   open C_GET_NO_FR_PE_IDS;
   fetch C_GET_NO_FR_PE_IDS BULK COLLECT into L_no_fr_price_event_ids;
   close C_GET_NO_FR_PE_IDS;

   if L_price_event_ids is NOT NULL and
      L_price_event_ids.COUNT > 0 then

      if INIT_GLOBALS(O_cc_error_tbl) = 0 then
         return 0;
      end if;

      if REMOVE_FROM_TIMELINE(O_cc_error_tbl,
                              L_price_event_ids,
                              I_price_event_type,
                              I_rib_trans_id) = 0 then
         return 0;
      end if;

      if RPM_EXE_CC_QUERY_RULES.EXECUTE(O_cc_error_tbl,
                                        I_price_event_type) = 0 then

         if PUSH_BACK_ERROR_ROWS(O_cc_error_tbl,
                                 0) = 0 then
            return 0;
         end if;

         return 0;
      end if;

      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 then

         if PUSH_BACK_ERROR_ROWS(O_cc_error_tbl,
                                 1) = 0 then
            return 0;
         end if;

         L_cc_error_tbl := O_cc_error_tbl;

      end if;

      -- If RPM_FUTURE_RETAIL_GTT is empty, it means all price events have conflicts.
      -- Stop the process.

      select COUNT(1)
        into L_gtt_count
        from rpm_future_retail_gtt;

      if L_gtt_count = 0 and
         (L_no_fr_price_event_ids is NULL or
          L_no_fr_price_event_ids.COUNT = 0) then

         return 1;
      end if;

      if L_gtt_count > 0 and
         I_persist_ind = 'Y' then

         if PUBLISH_CHANGES(O_cc_error_tbl,
                            I_rib_trans_id,
                            LP_bulk_cc_pe_id,
                            NULL,
                            NULL,
                            L_remove_price_event_ids,
                            L_remove_price_event_type) = 0 then
            return 0;
         end if;

         if RPM_PURGE_FUTURE_RETAIL_SQL.PURGE(O_cc_error_tbl) = 0 then
            return 0;
         end if;

         LP_push_back_start_date := LP_vdate;

         if LP_chunk_number = 0 then
            if PUSH_BACK(O_cc_error_tbl,
                         I_price_event_type) = 0 then
               return 0;
            end if;
         end if;

         -- Push Back Clearance
         if I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

            if LP_chunk_number = 0 then

               if RPM_CLEARANCE_GTT_SQL.PUSH_BACK(O_cc_error_tbl) = 0 then
                  return 0;
               end if;
            end if;
         end if;

         if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

            if LP_chunk_number = 0 then

               if RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_CHANGE_REMOVE(O_cc_error_tbl,
                                                                 I_price_event_ids) = 0 then
                  return 0;
               end if;
            end if;
         end if;
      end if;
   end if;

   if L_no_fr_price_event_ids is NOT NULL and
      L_no_fr_price_event_ids.COUNT > 0 and
      I_persist_ind = 'Y' and
      I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if RPM_ZONE_FUTURE_RETAIL_SQL.PRICE_CHANGE_REMOVE(O_cc_error_tbl,
                                                        I_price_event_ids) = 0 then
         return 0;
      end if;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PRICE_EVENT_REMOVE;
--------------------------------------------------------------------------------

FUNCTION SCHEDULE_LOCATION_MOVE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_location_move_id IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                                I_rib_trans_id     IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                I_persist_ind      IN     VARCHAR2)
RETURN NUMBER IS

    --Instrumentation
    L_program 		VARCHAR2(47) := 'RPM_FUTURE_RETAIL_SQL.SCHEDULE_LOCATION_MOVE';
    L_trace_name	VARCHAR2(10) := 'SLM';

    L_lm_rec RPM_LOCATION_MOVE%ROWTYPE := NULL;

    L_gtt_count        NUMBER     := NULL;
    L_con_check_err_id NUMBER(15) := NULL;
    L_days_before      NUMBER(3)  := NULL;
    L_days_after       NUMBER(3)  := NULL;
    L_fr_start_date    DATE       := NULL;
    L_fr_end_date      DATE       := NULL;
    L_start_time       TIMESTAMP  := SYSTIMESTAMP;

    cursor C_LOCATION_MOVE is
       select location_move_id,
              location_move_display_id,
              state,
              zone_location_id,
              location,
              loc_type,
              old_zone_id,
              new_zone_id,
              effective_date,
              existing_loc_move_id,
              lock_version,
              conflict_type
         from rpm_location_move
        where location_move_id = I_location_move_id;

   cursor C_TIMELINE(I_fr_start_date IN DATE,
                     I_fr_end_date   IN DATE) is
      select a.price_change_id,
             a.price_change_display_id,
             a.action_date,
             a.selling_retail,
             a.selling_retail_currency,
             a.selling_uom,
             a.multi_units,
             a.multi_unit_retail,
             a.multi_unit_retail_currency,
             a.multi_selling_uom,
             a.clearance_id,
             a.clearance_display_id,
             a.clear_retail,
             a.clear_retail_currency,
             a.clear_uom
        from (select /*+ CARDINALITY (cc 10) */
                     fr.item,
                     fr.location,
                     fr.price_change_id,
                     fr.price_change_display_id,
                     fr.action_date,
                     fr.selling_retail,
                     fr.selling_retail_currency,
                     fr.selling_uom,
                     fr.multi_units,
                     fr.multi_unit_retail,
                     fr.multi_unit_retail_currency,
                     fr.multi_selling_uom,
                     fr.clearance_id,
                     fr.clearance_display_id,
                     fr.clear_retail,
                     fr.clear_retail_currency,
                     fr.clear_uom
                from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     rpm_future_retail fr
               where fr.future_retail_id = cc.future_retail_id
              union all
              select t.item,
                     t.location,
                     t.price_change_id,
                     t.price_change_display_id,
                     t.action_date,
                     t.selling_retail,
                     t.selling_retail_currency,
                     t.selling_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_unit_retail_currency,
                     t.multi_selling_uom,
                     t.clearance_id,
                     t.clearance_display_id,
                     t.clear_retail,
                     t.clear_retail_currency,
                     t.clear_uom
                from (select cs.item,
                             cs.location,
                             fr.price_change_id,
                             fr.price_change_display_id,
                             cs.action_date,
                             fr.selling_retail,
                             fr.selling_retail_currency,
                             fr.selling_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             fr.clearance_id,
                             fr.clearance_display_id,
                             fr.clear_retail,
                             fr.clear_retail_currency,
                             fr.clear_uom,
                             RANK() OVER (PARTITION BY fr.item,
                                                       fr.location,
                                                       fr.action_date
                                              ORDER BY fr.action_date desc) rank
                        from (select /*+ CARDINALITY (cc 10) */
                                     cspfr.dept,
                                     cspfr.item,
                                     cspfr.location,
                                     cspfr.action_date
                                from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                     rpm_cust_segment_promo_fr cspfr
                               where cspfr.cust_segment_promo_id = cc.future_retail_id) cs,
                             rpm_future_retail fr
                       where fr.item     = cs.item
                         and fr.location = cs.location
                         and fr.dept     = cs.dept) t
               where t.rank = 1) a
       order by a.item,
                a.location,
                a.action_date;

   cursor C_PROMO_TIMELINE(I_fr_start_date IN DATE,
                           I_fr_end_date   IN DATE) is
      select a.action_date,
             a.simple_promo_retail,
             a.simple_promo_retail_currency,
             a.simple_promo_uom,
             a.multi_units,
             a.multi_unit_retail,
             a.multi_unit_retail_currency,
             a.multi_selling_uom,
             a.promo_dtl_id,
             a.promo_display_id,
             a.comp_display_id
        from (select /*+ CARDINALITY (cc 10) */
                     fr.item,
                     fr.location,
                     fr.action_date,
                     fr.simple_promo_retail,
                     fr.simple_promo_retail_currency,
                     fr.simple_promo_uom,
                     fr.multi_units,
                     fr.multi_unit_retail,
                     fr.multi_unit_retail_currency,
                     fr.multi_selling_uom,
                     rpile.promo_dtl_id,
                     rpile.promo_display_id,
                     rpile.comp_display_id
                from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     rpm_future_retail fr,
                     rpm_promo_item_loc_expl rpile
               where cc.cs_promo_fr_id   is NULL
                 and fr.future_retail_id = cc.future_retail_id
                 and fr.item             = rpile.item
                 and fr.location         = rpile.location
                 and fr.zone_node_type   = rpile.zone_node_type
                 and fr.dept             = rpile.dept
                 and fr.action_date      BETWEEN I_fr_start_date and I_fr_end_date
                 and fr.action_date      BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, fr.action_date)
              union all
              select t.item,
                     t.location,
                     t.action_date,
                     t.simple_promo_retail,
                     t.simple_promo_retail_currency,
                     t.simple_promo_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_unit_retail_currency,
                     t.multi_selling_uom,
                     t.promo_dtl_id,
                     t.promo_display_id,
                     t.comp_display_id
                from (select cs.item,
                             cs.location,
                             cs.action_date,
                             cs.simple_promo_retail,
                             cs.simple_promo_retail_currency,
                             cs.simple_promo_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             cs.promo_dtl_id,
                             cs.promo_display_id,
                             cs.comp_display_id,
                             RANK() OVER (PARTITION BY fr.item,
                                                       fr.location,
                                                       fr.action_date
                                              ORDER BY fr.action_date desc) rank
                        from (select /*+ CARDINALITY(cc 10) */
                                     cspfr.dept,
                                     cspfr.item,
                                     cspfr.location,
                                     cspfr.action_date,
                                     cspfr.promo_retail simple_promo_retail,
                                     cspfr.promo_retail_currency simple_promo_retail_currency,
                                     cspfr.promo_uom simple_promo_uom,
                                     rpile.promo_dtl_id,
                                     rpile.promo_display_id,
                                     rpile.comp_display_id
                                from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                     rpm_cust_segment_promo_fr cspfr,
                                     rpm_promo_item_loc_expl rpile
                               where cc.cs_promo_fr_id           is NOT NULL
                                 and cspfr.cust_segment_promo_id = cc.cs_promo_fr_id
                                 and cspfr.item                  = rpile.item
                                 and cspfr.location              = rpile.location
                                 and cspfr.zone_node_type        = rpile.zone_node_type
                                 and cspfr.dept                  = rpile.dept
                                 and cspfr.action_date           BETWEEN I_fr_start_date and I_fr_end_date
                                 and cspfr.action_date           BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, cspfr.action_date)) cs,
                             rpm_future_retail fr
                       where fr.item     = cs.item
                         and fr.location = cs.location
                         and fr.dept     = cs.dept) t
               where t.rank = 1) a
       order by a.item,
                a.location,
                a.action_date;

BEGIN

   LP_rib_trans_id := I_rib_trans_id;

   LOGGER.LOG_INFORMATION(L_program || ' - I_location_move_id: '|| I_location_move_id);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_LMS||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto SCHEDULE_LOCATION_MOVE;

   END;

   <<SCHEDULE_LOCATION_MOVE>>

   if INIT_GLOBALS(O_cc_error_tbl) = 0 then
      return 0;
   end if;

   open C_LOCATION_MOVE;
   fetch C_LOCATION_MOVE into L_lm_rec;
   close C_LOCATION_MOVE;

   if L_lm_rec.location_move_id is NULL then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('NO_DATA_FOUND',
                                                                    NULL,
                                                                    NULL,
                                                                    NULL)));
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_LOCATION_MOVE(O_cc_error_tbl,
                                                    L_lm_rec) = 0 then
      return 0;
   end if;

   if EXPL_TIMELINES_TO_IL(O_cc_error_tbl,
                           NULL,
                           RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                           NULL) = 0 then
      return 0;
   end if;

   if RPM_CC_PUBLISH.STAGE_LM_SCRUB_MESSAGES(O_cc_error_tbl,
                                             I_rib_trans_id,
                                             L_lm_rec.old_zone_id,
                                             L_lm_rec.location,
                                             L_lm_rec.loc_type,
                                             L_lm_rec.effective_date) = 0 then
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.LOCATION_MOVE_SCRUB(O_cc_error_tbl,
                                                    L_lm_rec) = 0 then
      return 0;
   end if;

   select COUNT(1)
     into L_gtt_count
     from rpm_future_retail_gtt;

   if L_gtt_count > 0 then

      if RPM_ROLL_FORWARD_SQL.EXECUTE(O_cc_error_tbl,
                                      RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE) = 0 then

         if PUSH_BACK_ERROR_ROWS(O_cc_error_tbl,
                                 0) = 0 then
            return 0;
         end if;

         return 0;
      end if;
   end if;

   if RPM_EXE_CC_QUERY_RULES.EXECUTE(O_cc_error_tbl,
                                     RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE) = 0 then

      if PUSH_BACK_ERROR_ROWS(O_cc_error_tbl,
                              0) = 0 then
         return 0;
      end if;

      return 0;
   end if;

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then

      L_con_check_err_id := RPM_CON_CHECK_ERR_SEQ.NEXTVAL;

      insert into rpm_con_check_err (con_check_err_id,
                                     ref_class,
                                     ref_id,
                                     ref_display_id,
                                     location,
                                     effective_date,
                                     error_date,
                                     message_key)
           values(L_con_check_err_id,
                  RPM_CONSTANTS.LM_REF_CLASS,
                  L_lm_rec.location_move_id,
                  L_lm_rec.location_move_display_id,
                  L_lm_rec.location,
                  L_lm_rec.effective_date,
                  LP_vdate,
                  O_cc_error_tbl(1).error_string);

      select conflict_history_days_before,
             conflict_history_days_after
        into L_days_before,
             L_days_after
        from rpm_system_options;

      L_fr_start_date := LP_vdate - L_days_before;
      L_fr_end_date   := LP_vdate + L_days_after;

      for rec IN C_TIMELINE(L_fr_start_date,
                            L_fr_end_date) loop

         if rec.price_change_id is NOT NULL then
            insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                                  con_check_err_id,
                                                  ref_class,
                                                  ref_id,
                                                  ref_display_id,
                                                  effective_date,
                                                  selling_retail,
                                                  selling_retail_currency,
                                                  selling_uom,
                                                  multi_units,
                                                  multi_unit_retail,
                                                  multi_unit_retail_currency,
                                                  multi_unit_uom)
               values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                      L_con_check_err_id,
                      RPM_CONSTANTS.PC_REF_CLASS,
                      rec.price_change_id,
                      rec.price_change_display_id,
                      rec.action_date,
                      rec.selling_retail,
                      rec.selling_retail_currency,
                      rec.selling_uom,
                      rec.multi_units,
                      rec.multi_unit_retail,
                      rec.multi_unit_retail_currency,
                      rec.multi_selling_uom);
         end if;

         if rec.clearance_id is NOT NULL then
            insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                                  con_check_err_id,
                                                  ref_class,
                                                  ref_id,
                                                  ref_display_id,
                                                  effective_date,
                                                  selling_retail,
                                                  selling_retail_currency,
                                                  selling_uom,
                                                  multi_units,
                                                  multi_unit_retail,
                                                  multi_unit_retail_currency,
                                                  multi_unit_uom)
              values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                     L_con_check_err_id,
                     RPM_CONSTANTS.CL_REF_CLASS,
                     rec.clearance_id,
                     rec.clearance_display_id,
                     rec.action_date,
                     rec.clear_retail,
                     rec.clear_retail_currency,
                     rec.clear_uom,
                     rec.multi_units,
                     rec.multi_unit_retail,
                     rec.multi_unit_retail_currency,
                     rec.multi_selling_uom);
         end if;
      end loop;

      for rec IN C_PROMO_TIMELINE(L_fr_start_date,
                                  L_fr_end_date) loop

         insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                               con_check_err_id,
                                               ref_class,
                                               ref_id,
                                               ref_display_id,
                                               ref_secondary_display_id,
                                               effective_date,
                                               selling_retail,
                                               selling_retail_currency,
                                               selling_uom,
                                               multi_units,
                                               multi_unit_retail,
                                               multi_unit_retail_currency,
                                               multi_unit_uom)
            values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                   L_con_check_err_id,
                   RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                   rec.promo_dtl_id,
                   rec.promo_display_id,
                   rec.comp_display_id,
                   rec.action_date,
                   rec.simple_promo_retail,
                   rec.simple_promo_retail_currency,
                   rec.simple_promo_uom,
                   rec.multi_units,
                   rec.multi_unit_retail,
                   rec.multi_unit_retail_currency,
                   rec.multi_selling_uom);
      end loop;

      return 1;
   end if;

   if I_persist_ind = 'Y' then
      if RPM_PURGE_FUTURE_RETAIL_SQL.PURGE(O_cc_error_tbl) = 0 then
         return 0;
      end if;

      LP_push_back_start_date := L_lm_rec.effective_date;

      if PUSH_BACK(O_cc_error_tbl,
                   RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE) = 0 then
         return 0;
      end if;
   end if;

   LOGGER.LOG_TIME(L_program || ' - I_location_move_id: '|| I_location_move_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END SCHEDULE_LOCATION_MOVE;

--------------------------------------------------------------------------------

FUNCTION EXPL_TIMELINES_TO_IL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type IN     VARCHAR2,
                              I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.EXPL_TIMELINES_TO_IL';

   L_dept               NUMBER(4)            := NULL;
   L_start_time         TIMESTAMP            := SYSTIMESTAMP;
   L_child_event_ids    OBJ_NUM_NUM_DATE_TBL := NULL;
   L_chunk_item_loc_sge OBJ_NUM_NUM_STR_TBL  := NULL;
   L_error_message      VARCHAR2(255)        := NULL;

   cursor C_FR_DEPT is
      select distinct dept
        from rpm_future_retail_gtt;

   cursor C_CSFR_DEPT is
      select distinct dept
        from rpm_cust_segment_promo_fr_gtt;

   cursor C_CHUNK_ITEM_LOC_SGE is
      select OBJ_NUM_NUM_STR_REC(price_event_id,
                                 location,
                                 item)
        from (-- Tran Item Zone
              select /*+ CARDINALITY (ids, 10) */
                     gtt.price_event_id,
                     rzl.location,
                     gtt.item
                from table(cast(LP_sge_chunking_pe_data as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     rpm_zone_location rzl
               where gtt.price_event_id  = VALUE(ids)
                 and gtth.price_event_id = gtt.price_event_id
                 and gtth.tolerance_ind  = 1
                 and gtt.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                 and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and gtt.location        = rzl.zone_id
              union all
              -- Parent Item Zone
              select /*+ CARDINALITY (ids, 10) */
                     gtt.price_event_id,
                     rzl.location,
                     im.item
                from table(cast(LP_sge_chunking_pe_data as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     rpm_zone_location rzl,
                     item_master im
               where gtt.price_event_id  = VALUE(ids)
                 and gtth.price_event_id = gtt.price_event_id
                 and gtth.tolerance_ind  = 1
                 and gtt.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                 and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and gtt.location        = rzl.zone_id
                 and gtt.item            = im.item_parent
              union all
              -- Parent Diff Item Zone
              select /*+ CARDINALITY (ids, 10) */
                     gtt.price_event_id,
                     rzl.location,
                     gtt.item
                from table(cast(LP_sge_chunking_pe_data as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     rpm_zone_location rzl,
                     item_master im
               where gtt.price_event_id  = VALUE(ids)
                 and gtth.price_event_id = gtt.price_event_id
                 and gtth.tolerance_ind  = 1
                 and gtt.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                 and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and gtt.location        = rzl.zone_id
                 and gtt.item            = im.item_parent
                 and gtt.diff_id         = im.diff_1
              union all
              -- Tran Item Loc
              select /*+ CARDINALITY (ids, 10) */
                     gtt.price_event_id,
                     gtt.location,
                     gtt.item
                from table(cast(LP_sge_chunking_pe_data as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth
               where gtt.price_event_id  = VALUE(ids)
                 and gtth.price_event_id = gtt.price_event_id
                 and gtth.tolerance_ind  = 1
                 and gtt.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                 and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
              union all
              -- Parent Item Loc
              select /*+ CARDINALITY (ids, 10) */
                     gtt.price_event_id,
                     gtt.location,
                     im.item
                from table(cast(LP_sge_chunking_pe_data as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     item_master im
               where gtt.price_event_id  = VALUE(ids)
                 and gtth.price_event_id = gtt.price_event_id
                 and gtth.tolerance_ind  = 1
                 and gtt.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                 and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and gtt.item            = im.item_parent
              union all
              -- Parent Diff Item Loc
              select /*+ CARDINALITY (ids, 10) */
                     gtt.price_event_id,
                     gtt.location,
                     im.item
                from table(cast(LP_sge_chunking_pe_data as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     item_master im
               where gtt.price_event_id  = VALUE(ids)
                 and gtth.price_event_id = gtt.price_event_id
                 and gtth.tolerance_ind  = 1
                 and gtt.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                 and gtt.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                 and gtt.item            = im.item_parent
                 and gtt.diff_id         = im.diff_1);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number ||
                                      ' - LP_chunk_number: '|| LP_chunk_number);

   delete rpm_fr_item_loc_expl_gtt;

   -- Explode all timelines on FR_GTT to the IL level making sure that only those ranged are retrieved
   -- Loop through each dept (most of the time will be just one) so it can acces RPM_ITEM_LOC more effectively

   for rec IN C_FR_DEPT loop
      L_dept := rec.dept;

      insert into rpm_fr_item_loc_expl_gtt
         (price_event_id,
          future_retail_id,
          dept,
          class,
          subclass,
          item,
          zone_node_type,
          location,
          action_date,
          selling_retail,
          selling_retail_currency,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_unit_retail_currency,
          multi_selling_uom,
          clear_retail,
          clear_retail_currency,
          clear_uom,
          simple_promo_retail,
          simple_promo_retail_currency,
          simple_promo_uom,
          promo_comp_msg_type,
          price_change_id,
          pc_msg_type,
          clear_msg_type,
          pc_selling_retail_ind,
          price_change_display_id,
          pc_exception_parent_id,
          pc_change_type,
          pc_change_amount,
          pc_change_currency,
          pc_change_percent,
          pc_change_selling_uom,
          pc_null_multi_ind,
          pc_multi_units,
          pc_multi_unit_retail,
          pc_multi_unit_retail_currency,
          pc_multi_selling_uom,
          pc_multi_unit_ind,
          pc_price_guide_id,
          clearance_id,
          clearance_display_id,
          clear_mkdn_index,
          clear_start_ind,
          clear_change_type,
          clear_change_amount,
          clear_change_currency,
          clear_change_percent,
          clear_change_selling_uom,
          clear_price_guide_id,
          loc_move_from_zone_id,
          loc_move_to_zone_id,
          location_move_id,
          promo_dtl_id,
          detail_start_date,
          detail_end_date,
          type,
          promo_id,
          exception_parent_id,
          step_identifier,
          ref_promo_dtl_id)
      select price_event_id,
             future_retail_id,
             dept,
             class,
             subclass,
             item,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             promo_comp_msg_type,
             price_change_id,
             pc_msg_type,
             clear_msg_type,
             pc_selling_retail_ind,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_multi_unit_ind,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             promo_dtl_id,
             detail_start_date,
             detail_end_date,
             type,
             promo_id,
             exception_parent_id,
             I_step_identifier,
             ref_promo_dtl_id
        from (select t.price_event_id,
                     t.future_retail_id,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.item,
                     t.zone_node_type,
                     t.location,
                     t.action_date,
                     t.selling_retail,
                     t.selling_retail_currency,
                     t.selling_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_unit_retail_currency,
                     t.multi_selling_uom,
                     t.clear_retail,
                     t.clear_retail_currency,
                     t.clear_uom,
                     t.simple_promo_retail,
                     t.simple_promo_retail_currency,
                     t.simple_promo_uom,
                     t.promo_comp_msg_type,
                     t.price_change_id,
                     t.pc_msg_type,
                     t.clear_msg_type,
                     t.pc_selling_retail_ind,
                     t.price_change_display_id,
                     t.pc_exception_parent_id,
                     t.pc_change_type,
                     t.pc_change_amount,
                     t.pc_change_currency,
                     t.pc_change_percent,
                     t.pc_change_selling_uom,
                     t.pc_null_multi_ind,
                     t.pc_multi_units,
                     t.pc_multi_unit_retail,
                     t.pc_multi_unit_retail_currency,
                     t.pc_multi_selling_uom,
                     t.pc_multi_unit_ind,
                     t.pc_price_guide_id,
                     t.clearance_id,
                     t.clearance_display_id,
                     t.clear_mkdn_index,
                     t.clear_start_ind,
                     t.clear_change_type,
                     t.clear_change_amount,
                     t.clear_change_currency,
                     t.clear_change_percent,
                     t.clear_change_selling_uom,
                     t.clear_price_guide_id,
                     t.loc_move_from_zone_id,
                     t.loc_move_to_zone_id,
                     t.location_move_id,
                     t.promo_dtl_id,
                     t.detail_start_date,
                     t.detail_end_date,
                     t.type,
                     t.promo_id,
                     t.exception_parent_id,
                     t.ref_promo_dtl_id,
                     RANK() OVER (PARTITION BY t.dept,
                                               t.item,
                                               t.location,
                                               t.action_date
                                      ORDER BY fr_level desc) rank
                from (select /*+ ORDERED */
                             rfr.price_event_id,
                             rfr.future_retail_id,
                             rfr.dept,
                             rfr.class,
                             rfr.subclass,
                             rfr.item,
                             rfr.zone_node_type,
                             rfr.location,
                             rfr.action_date,
                             rfr.selling_retail,
                             rfr.selling_retail_currency,
                             rfr.selling_uom,
                             rfr.multi_units,
                             rfr.multi_unit_retail,
                             rfr.multi_unit_retail_currency,
                             rfr.multi_selling_uom,
                             rfr.clear_retail,
                             rfr.clear_retail_currency,
                             rfr.clear_uom,
                             rfr.simple_promo_retail,
                             rfr.simple_promo_retail_currency,
                             rfr.simple_promo_uom,
                             rfr.price_change_id,
                             rfr.pc_msg_type,
                             rfr.clear_msg_type,
                             rfr.pc_selling_retail_ind,
                             rfr.price_change_display_id,
                             rfr.pc_exception_parent_id,
                             rfr.pc_change_type,
                             rfr.pc_change_amount,
                             rfr.pc_change_currency,
                             rfr.pc_change_percent,
                             rfr.pc_change_selling_uom,
                             rfr.pc_null_multi_ind,
                             rfr.pc_multi_units,
                             rfr.pc_multi_unit_retail,
                             rfr.pc_multi_unit_retail_currency,
                             rfr.pc_multi_selling_uom,
                             rfr.pc_multi_unit_ind,
                             rfr.pc_price_guide_id,
                             rfr.clearance_id,
                             rfr.clearance_display_id,
                             rfr.clear_mkdn_index,
                             rfr.clear_start_ind,
                             rfr.clear_change_type,
                             rfr.clear_change_amount,
                             rfr.clear_change_currency,
                             rfr.clear_change_percent,
                             rfr.clear_change_selling_uom,
                             rfr.clear_price_guide_id,
                             rfr.loc_move_from_zone_id,
                             rfr.loc_move_to_zone_id,
                             rfr.location_move_id,
                             rpile.promo_dtl_id,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.type,
                             rpile.promo_id,
                             rpile.exception_parent_id,
                             rpile.promo_comp_msg_type,
                             3 fr_level,
                             rfr.ref_promo_dtl_id
                        from rpm_future_retail_gtt rfr,
                             rpm_promo_item_loc_expl_gtt rpile
                       where rfr.cur_hier_level       = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                         and rfr.dept                 = L_dept
                         and rpile.customer_type (+)  is NULL
                         and rpile.price_event_id (+) = rfr.price_event_id
                         and rpile.cur_hier_level (+) = rfr.cur_hier_level
                         and rpile.dept (+)           = rfr.dept
                         and rpile.item (+)           = rfr.item
                         and rpile.location (+)       = rfr.location
                         and rpile.zone_node_type (+) = rfr.zone_node_type
                         and rfr.action_date          BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                      union all
                      select price_event_id,
                             future_retail_id,
                             il.dept,
                             class,
                             subclass,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             clear_retail,
                             clear_retail_currency,
                             clear_uom,
                             simple_promo_retail,
                             simple_promo_retail_currency,
                             simple_promo_uom,
                             price_change_id,
                             pc_msg_type,
                             clear_msg_type,
                             pc_selling_retail_ind,
                             price_change_display_id,
                             pc_exception_parent_id,
                             pc_change_type,
                             pc_change_amount,
                             pc_change_currency,
                             pc_change_percent,
                             pc_change_selling_uom,
                             pc_null_multi_ind,
                             pc_multi_units,
                             pc_multi_unit_retail,
                             pc_multi_unit_retail_currency,
                             pc_multi_selling_uom,
                             pc_multi_unit_ind,
                             pc_price_guide_id,
                             clearance_id,
                             clearance_display_id,
                             clear_mkdn_index,
                             clear_start_ind,
                             clear_change_type,
                             clear_change_amount,
                             clear_change_currency,
                             clear_change_percent,
                             clear_change_selling_uom,
                             clear_price_guide_id,
                             loc_move_from_zone_id,
                             loc_move_to_zone_id,
                             location_move_id,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_id,
                             exception_parent_id,
                             promo_comp_msg_type,
                             2 fr_level,
                             ref_promo_dtl_id
                        from (select /*+ ORDERED */
                                     rfr.price_event_id,
                                     rfr.future_retail_id,
                                     rfr.dept,
                                     rfr.class,
                                     rfr.subclass,
                                     rfr.item,
                                     rzl.loc_type zone_node_type,
                                     rzl.location,
                                     rfr.action_date,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.multi_units,
                                     rfr.multi_unit_retail,
                                     rfr.multi_unit_retail_currency,
                                     rfr.multi_selling_uom,
                                     rfr.clear_retail,
                                     rfr.clear_retail_currency,
                                     rfr.clear_uom,
                                     rfr.simple_promo_retail,
                                     rfr.simple_promo_retail_currency,
                                     rfr.simple_promo_uom,
                                     rfr.price_change_id,
                                     rfr.pc_msg_type,
                                     rfr.clear_msg_type,
                                     rfr.pc_selling_retail_ind,
                                     rfr.price_change_display_id,
                                     rfr.pc_exception_parent_id,
                                     rfr.pc_change_type,
                                     rfr.pc_change_amount,
                                     rfr.pc_change_currency,
                                     rfr.pc_change_percent,
                                     rfr.pc_change_selling_uom,
                                     rfr.pc_null_multi_ind,
                                     rfr.pc_multi_units,
                                     rfr.pc_multi_unit_retail,
                                     rfr.pc_multi_unit_retail_currency,
                                     rfr.pc_multi_selling_uom,
                                     rfr.pc_multi_unit_ind,
                                     rfr.pc_price_guide_id,
                                     rfr.clearance_id,
                                     rfr.clearance_display_id,
                                     rfr.clear_mkdn_index,
                                     rfr.clear_start_ind,
                                     rfr.clear_change_type,
                                     rfr.clear_change_amount,
                                     rfr.clear_change_currency,
                                     rfr.clear_change_percent,
                                     rfr.clear_change_selling_uom,
                                     rfr.clear_price_guide_id,
                                     rfr.loc_move_from_zone_id,
                                     rfr.loc_move_to_zone_id,
                                     rfr.location_move_id,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id,
                                     rpile.promo_comp_msg_type,
                                     rfr.ref_promo_dtl_id
                                from rpm_future_retail_gtt rfr,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     rpm_zone_location rzl
                               where rfr.cur_hier_level       = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                 and rfr.dept                 = L_dept
                                 and rpile.customer_type (+)  is NULL
                                 and rpile.price_event_id (+) = rfr.price_event_id
                                 and rpile.cur_hier_level (+) = rfr.cur_hier_level
                                 and rpile.dept (+)           = rfr.dept
                                 and rpile.item (+)           = rfr.item
                                 and rpile.location (+)       = rfr.location
                                 and rpile.zone_node_type (+) = rfr.zone_node_type
                                 and rfr.action_date          BETWEEN rpile.detail_start_date(+) and NVL(rpile.detail_end_date(+), TO_DATE('3000', 'YYYY'))
                                 and rzl.zone_id              = rfr.location
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             future_retail_id,
                             il.dept,
                             class,
                             subclass,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             clear_retail,
                             clear_retail_currency,
                             clear_uom,
                             simple_promo_retail,
                             simple_promo_retail_currency,
                             simple_promo_uom,
                             price_change_id,
                             pc_msg_type,
                             clear_msg_type,
                             pc_selling_retail_ind,
                             price_change_display_id,
                             pc_exception_parent_id,
                             pc_change_type,
                             pc_change_amount,
                             pc_change_currency,
                             pc_change_percent,
                             pc_change_selling_uom,
                             pc_null_multi_ind,
                             pc_multi_units,
                             pc_multi_unit_retail,
                             pc_multi_unit_retail_currency,
                             pc_multi_selling_uom,
                             pc_multi_unit_ind,
                             pc_price_guide_id,
                             clearance_id,
                             clearance_display_id,
                             clear_mkdn_index,
                             clear_start_ind,
                             clear_change_type,
                             clear_change_amount,
                             clear_change_currency,
                             clear_change_percent,
                             clear_change_selling_uom,
                             clear_price_guide_id,
                             loc_move_from_zone_id,
                             loc_move_to_zone_id,
                             location_move_id,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_id,
                             exception_parent_id,
                             promo_comp_msg_type,
                             2 fr_level,
                             ref_promo_dtl_id
                        from (select /*+ ORDERED */
                                     rfr.price_event_id,
                                     rfr.future_retail_id,
                                     rfr.dept,
                                     rfr.class,
                                     rfr.subclass,
                                     im.item,
                                     rfr.zone_node_type,
                                     rfr.location,
                                     rfr.action_date,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.multi_units,
                                     rfr.multi_unit_retail,
                                     rfr.multi_unit_retail_currency,
                                     rfr.multi_selling_uom,
                                     rfr.clear_retail,
                                     rfr.clear_retail_currency,
                                     rfr.clear_uom,
                                     rfr.simple_promo_retail,
                                     rfr.simple_promo_retail_currency,
                                     rfr.simple_promo_uom,
                                     rfr.price_change_id,
                                     rfr.pc_msg_type,
                                     rfr.clear_msg_type,
                                     rfr.pc_selling_retail_ind,
                                     rfr.price_change_display_id,
                                     rfr.pc_exception_parent_id,
                                     rfr.pc_change_type,
                                     rfr.pc_change_amount,
                                     rfr.pc_change_currency,
                                     rfr.pc_change_percent,
                                     rfr.pc_change_selling_uom,
                                     rfr.pc_null_multi_ind,
                                     rfr.pc_multi_units,
                                     rfr.pc_multi_unit_retail,
                                     rfr.pc_multi_unit_retail_currency,
                                     rfr.pc_multi_selling_uom,
                                     rfr.pc_multi_unit_ind,
                                     rfr.pc_price_guide_id,
                                     rfr.clearance_id,
                                     rfr.clearance_display_id,
                                     rfr.clear_mkdn_index,
                                     rfr.clear_start_ind,
                                     rfr.clear_change_type,
                                     rfr.clear_change_amount,
                                     rfr.clear_change_currency,
                                     rfr.clear_change_percent,
                                     rfr.clear_change_selling_uom,
                                     rfr.clear_price_guide_id,
                                     rfr.loc_move_from_zone_id,
                                     rfr.loc_move_to_zone_id,
                                     rfr.location_move_id,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id,
                                     rpile.promo_comp_msg_type,
                                     rfr.ref_promo_dtl_id
                                from rpm_future_retail_gtt rfr,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im
                               where rfr.cur_hier_level       = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                                 and rfr.dept                 = L_dept
                                 and rpile.customer_type (+)  is NULL
                                 and rpile.price_event_id (+) = rfr.price_event_id
                                 and rpile.cur_hier_level (+) = rfr.cur_hier_level
                                 and rpile.dept (+)           = rfr.dept
                                 and rpile.item (+)           = rfr.item
                                 and rpile.diff_id (+)        = rfr.diff_id
                                 and rpile.location (+)       = rfr.location
                                 and rpile.zone_node_type (+) = rfr.zone_node_type
                                 and rfr.action_date          BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = rfr.item
                                 and im.diff_1                = rfr.diff_id
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             future_retail_id,
                             il.dept,
                             class,
                             subclass,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             clear_retail,
                             clear_retail_currency,
                             clear_uom,
                             simple_promo_retail,
                             simple_promo_retail_currency,
                             simple_promo_uom,
                             price_change_id,
                             pc_msg_type,
                             clear_msg_type,
                             pc_selling_retail_ind,
                             price_change_display_id,
                             pc_exception_parent_id,
                             pc_change_type,
                             pc_change_amount,
                             pc_change_currency,
                             pc_change_percent,
                             pc_change_selling_uom,
                             pc_null_multi_ind,
                             pc_multi_units,
                             pc_multi_unit_retail,
                             pc_multi_unit_retail_currency,
                             pc_multi_selling_uom,
                             pc_multi_unit_ind,
                             pc_price_guide_id,
                             clearance_id,
                             clearance_display_id,
                             clear_mkdn_index,
                             clear_start_ind,
                             clear_change_type,
                             clear_change_amount,
                             clear_change_currency,
                             clear_change_percent,
                             clear_change_selling_uom,
                             clear_price_guide_id,
                             loc_move_from_zone_id,
                             loc_move_to_zone_id,
                             location_move_id,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_id,
                             exception_parent_id,
                             promo_comp_msg_type,
                             1 fr_level,
                             ref_promo_dtl_id
                        from (select /*+ ORDERED */
                                     rfr.price_event_id,
                                     rfr.future_retail_id,
                                     rfr.dept,
                                     rfr.class,
                                     rfr.subclass,
                                     im.item,
                                     rfr.zone_node_type,
                                     rfr.location,
                                     rfr.action_date,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.multi_units,
                                     rfr.multi_unit_retail,
                                     rfr.multi_unit_retail_currency,
                                     rfr.multi_selling_uom,
                                     rfr.clear_retail,
                                     rfr.clear_retail_currency,
                                     rfr.clear_uom,
                                     rfr.simple_promo_retail,
                                     rfr.simple_promo_retail_currency,
                                     rfr.simple_promo_uom,
                                     rfr.price_change_id,
                                     rfr.pc_msg_type,
                                     rfr.clear_msg_type,
                                     rfr.pc_selling_retail_ind,
                                     rfr.price_change_display_id,
                                     rfr.pc_exception_parent_id,
                                     rfr.pc_change_type,
                                     rfr.pc_change_amount,
                                     rfr.pc_change_currency,
                                     rfr.pc_change_percent,
                                     rfr.pc_change_selling_uom,
                                     rfr.pc_null_multi_ind,
                                     rfr.pc_multi_units,
                                     rfr.pc_multi_unit_retail,
                                     rfr.pc_multi_unit_retail_currency,
                                     rfr.pc_multi_selling_uom,
                                     rfr.pc_multi_unit_ind,
                                     rfr.pc_price_guide_id,
                                     rfr.clearance_id,
                                     rfr.clearance_display_id,
                                     rfr.clear_mkdn_index,
                                     rfr.clear_start_ind,
                                     rfr.clear_change_type,
                                     rfr.clear_change_amount,
                                     rfr.clear_change_currency,
                                     rfr.clear_change_percent,
                                     rfr.clear_change_selling_uom,
                                     rfr.clear_price_guide_id,
                                     rfr.loc_move_from_zone_id,
                                     rfr.loc_move_to_zone_id,
                                     rfr.location_move_id,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id,
                                     rpile.promo_comp_msg_type,
                                     rfr.ref_promo_dtl_id
                                from rpm_future_retail_gtt rfr,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im
                               where rfr.cur_hier_level       = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                 and rfr.dept                 = L_dept
                                 and rpile.customer_type (+)  is NULL
                                 and rpile.price_event_id (+) = rfr.price_event_id
                                 and rpile.cur_hier_level (+) = rfr.cur_hier_level
                                 and rpile.dept (+)           = rfr.dept
                                 and rpile.item (+)           = rfr.item
                                 and rpile.location (+)       = rfr.location
                                 and rpile.zone_node_type (+) = rfr.zone_node_type
                                 and rfr.action_date          BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = rfr.item
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             future_retail_id,
                             il.dept,
                             class,
                             subclass,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             clear_retail,
                             clear_retail_currency,
                             clear_uom,
                             simple_promo_retail,
                             simple_promo_retail_currency,
                             simple_promo_uom,
                             price_change_id,
                             pc_msg_type,
                             clear_msg_type,
                             pc_selling_retail_ind,
                             price_change_display_id,
                             pc_exception_parent_id,
                             pc_change_type,
                             pc_change_amount,
                             pc_change_currency,
                             pc_change_percent,
                             pc_change_selling_uom,
                             pc_null_multi_ind,
                             pc_multi_units,
                             pc_multi_unit_retail,
                             pc_multi_unit_retail_currency,
                             pc_multi_selling_uom,
                             pc_multi_unit_ind,
                             pc_price_guide_id,
                             clearance_id,
                             clearance_display_id,
                             clear_mkdn_index,
                             clear_start_ind,
                             clear_change_type,
                             clear_change_amount,
                             clear_change_currency,
                             clear_change_percent,
                             clear_change_selling_uom,
                             clear_price_guide_id,
                             loc_move_from_zone_id,
                             loc_move_to_zone_id,
                             location_move_id,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_id,
                             exception_parent_id,
                             promo_comp_msg_type,
                             1 fr_level,
                             ref_promo_dtl_id
                        from (select /*+ ORDERED */
                                     rfr.price_event_id,
                                     rfr.future_retail_id,
                                     rfr.dept,
                                     rfr.class,
                                     rfr.subclass,
                                     im.item,
                                     rzl.loc_type zone_node_type,
                                     rzl.location,
                                     rfr.action_date,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.multi_units,
                                     rfr.multi_unit_retail,
                                     rfr.multi_unit_retail_currency,
                                     rfr.multi_selling_uom,
                                     rfr.clear_retail,
                                     rfr.clear_retail_currency,
                                     rfr.clear_uom,
                                     rfr.simple_promo_retail,
                                     rfr.simple_promo_retail_currency,
                                     rfr.simple_promo_uom,
                                     rfr.price_change_id,
                                     rfr.pc_msg_type,
                                     rfr.clear_msg_type,
                                     rfr.pc_selling_retail_ind,
                                     rfr.price_change_display_id,
                                     rfr.pc_exception_parent_id,
                                     rfr.pc_change_type,
                                     rfr.pc_change_amount,
                                     rfr.pc_change_currency,
                                     rfr.pc_change_percent,
                                     rfr.pc_change_selling_uom,
                                     rfr.pc_null_multi_ind,
                                     rfr.pc_multi_units,
                                     rfr.pc_multi_unit_retail,
                                     rfr.pc_multi_unit_retail_currency,
                                     rfr.pc_multi_selling_uom,
                                     rfr.pc_multi_unit_ind,
                                     rfr.pc_price_guide_id,
                                     rfr.clearance_id,
                                     rfr.clearance_display_id,
                                     rfr.clear_mkdn_index,
                                     rfr.clear_start_ind,
                                     rfr.clear_change_type,
                                     rfr.clear_change_amount,
                                     rfr.clear_change_currency,
                                     rfr.clear_change_percent,
                                     rfr.clear_change_selling_uom,
                                     rfr.clear_price_guide_id,
                                     rfr.loc_move_from_zone_id,
                                     rfr.loc_move_to_zone_id,
                                     rfr.location_move_id,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id,
                                     rpile.promo_comp_msg_type,
                                     rfr.ref_promo_dtl_id
                                from rpm_future_retail_gtt rfr,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im,
                                     rpm_zone_location rzl
                               where rfr.cur_hier_level       = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                                 and rfr.dept                 = L_dept
                                 and rpile.customer_type (+)  is NULL
                                 and rpile.price_event_id (+) = rfr.price_event_id
                                 and rpile.cur_hier_level (+) = rfr.cur_hier_level
                                 and rpile.dept (+)           = rfr.dept
                                 and rpile.item (+)           = rfr.item
                                 and rpile.diff_id (+)        = rfr.diff_id
                                 and rpile.location (+)       = rfr.location
                                 and rpile.zone_node_type (+) = rfr.zone_node_type
                                 and rfr.action_date          BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = rfr.item
                                 and im.diff_1                = rfr.diff_id
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rzl.zone_id              = rfr.location
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             future_retail_id,
                             il.dept,
                             class,
                             subclass,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             selling_retail,
                             selling_retail_currency,
                             selling_uom,
                             multi_units,
                             multi_unit_retail,
                             multi_unit_retail_currency,
                             multi_selling_uom,
                             clear_retail,
                             clear_retail_currency,
                             clear_uom,
                             simple_promo_retail,
                             simple_promo_retail_currency,
                             simple_promo_uom,
                             price_change_id,
                             pc_msg_type,
                             clear_msg_type,
                             pc_selling_retail_ind,
                             price_change_display_id,
                             pc_exception_parent_id,
                             pc_change_type,
                             pc_change_amount,
                             pc_change_currency,
                             pc_change_percent,
                             pc_change_selling_uom,
                             pc_null_multi_ind,
                             pc_multi_units,
                             pc_multi_unit_retail,
                             pc_multi_unit_retail_currency,
                             pc_multi_selling_uom,
                             pc_multi_unit_ind,
                             pc_price_guide_id,
                             clearance_id,
                             clearance_display_id,
                             clear_mkdn_index,
                             clear_start_ind,
                             clear_change_type,
                             clear_change_amount,
                             clear_change_currency,
                             clear_change_percent,
                             clear_change_selling_uom,
                             clear_price_guide_id,
                             loc_move_from_zone_id,
                             loc_move_to_zone_id,
                             location_move_id,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_id,
                             exception_parent_id,
                             promo_comp_msg_type,
                             0 fr_level,
                             ref_promo_dtl_id
                        from (select /*+ ORDERED */
                                     rfr.price_event_id,
                                     rfr.future_retail_id,
                                     rfr.dept,
                                     rfr.class,
                                     rfr.subclass,
                                     im.item,
                                     rzl.loc_type zone_node_type,
                                     rzl.location,
                                     rfr.action_date,
                                     rfr.selling_retail,
                                     rfr.selling_retail_currency,
                                     rfr.selling_uom,
                                     rfr.multi_units,
                                     rfr.multi_unit_retail,
                                     rfr.multi_unit_retail_currency,
                                     rfr.multi_selling_uom,
                                     rfr.clear_retail,
                                     rfr.clear_retail_currency,
                                     rfr.clear_uom,
                                     rfr.simple_promo_retail,
                                     rfr.simple_promo_retail_currency,
                                     rfr.simple_promo_uom,
                                     rfr.price_change_id,
                                     rfr.pc_msg_type,
                                     rfr.clear_msg_type,
                                     rfr.pc_selling_retail_ind,
                                     rfr.price_change_display_id,
                                     rfr.pc_exception_parent_id,
                                     rfr.pc_change_type,
                                     rfr.pc_change_amount,
                                     rfr.pc_change_currency,
                                     rfr.pc_change_percent,
                                     rfr.pc_change_selling_uom,
                                     rfr.pc_null_multi_ind,
                                     rfr.pc_multi_units,
                                     rfr.pc_multi_unit_retail,
                                     rfr.pc_multi_unit_retail_currency,
                                     rfr.pc_multi_selling_uom,
                                     rfr.pc_multi_unit_ind,
                                     rfr.pc_price_guide_id,
                                     rfr.clearance_id,
                                     rfr.clearance_display_id,
                                     rfr.clear_mkdn_index,
                                     rfr.clear_start_ind,
                                     rfr.clear_change_type,
                                     rfr.clear_change_amount,
                                     rfr.clear_change_currency,
                                     rfr.clear_change_percent,
                                     rfr.clear_change_selling_uom,
                                     rfr.clear_price_guide_id,
                                     rfr.loc_move_from_zone_id,
                                     rfr.loc_move_to_zone_id,
                                     rfr.location_move_id,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id,
                                     rpile.promo_comp_msg_type,
                                     rfr.ref_promo_dtl_id
                                from rpm_future_retail_gtt rfr,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im,
                                     rpm_zone_location rzl
                               where rfr.cur_hier_level       = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                 and rfr.dept                 = L_dept
                                 and rpile.customer_type (+)  is NULL
                                 and rpile.price_event_id (+) = rfr.price_event_id
                                 and rpile.cur_hier_level (+) = rfr.cur_hier_level
                                 and rpile.dept (+)           = rfr.dept
                                 and rpile.item (+)           = rfr.item
                                 and rpile.location (+)       = rfr.location
                                 and rpile.zone_node_type (+) = rfr.zone_node_type
                                 and rfr.action_date          BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = rfr.item
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rzl.zone_id              = rfr.location
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location) t)
        where rank = 1;

   end loop;

   if LP_chunk_number > 0 and
      LP_from_loc_move_ind != 1 and
      LP_elig_for_sys_gen_exclusions = 1 then

      open C_CHUNK_ITEM_LOC_SGE;
      fetch C_CHUNK_ITEM_LOC_SGE BULK COLLECT into L_chunk_item_loc_sge;
      close C_CHUNK_ITEM_LOC_SGE;

      forall i IN 1..L_chunk_item_loc_sge.COUNT
         delete
           from rpm_fr_item_loc_expl_gtt
          where price_event_id = L_chunk_item_loc_sge(i).number_1
            and location       = L_chunk_item_loc_sge(i).number_2
            and item           = L_chunk_item_loc_sge(i).string;

   end if;

   for rec IN C_CSFR_DEPT loop
      L_dept := rec.dept;

      insert into rpm_fr_item_loc_expl_gtt
         (price_event_id,
          dept,
          item,
          zone_node_type,
          location,
          action_date,
          promo_retail,
          simple_promo_retail_currency,
          promo_uom,
          customer_type,
          promo_dtl_id,
          detail_start_date,
          detail_end_date,
          type,
          promo_comp_msg_type,
          promo_id,
          exception_parent_id,
          step_identifier)
      select price_event_id,
             dept,
             item,
             zone_node_type,
             location,
             action_date,
             promo_retail,
             promo_retail_currency,
             promo_uom,
             customer_type,
             promo_dtl_id,
             detail_start_date,
             detail_end_date,
             type,
             promo_comp_msg_type,
             promo_id,
             exception_parent_id,
             I_step_identifier
        from (select t.price_event_id,
                     t.dept,
                     t.item,
                     t.zone_node_type,
                     t.location,
                     t.action_date,
                     t.promo_retail,
                     t.promo_retail_currency,
                     t.promo_uom,
                     t.customer_type,
                     t.promo_dtl_id,
                     t.detail_start_date,
                     t.detail_end_date,
                     t.type,
                     t.promo_comp_msg_type,
                     t.promo_id,
                     t.exception_parent_id,
                     RANK() OVER (PARTITION BY t.dept,
                                               t.item,
                                               t.location,
                                               t.action_date
                                      ORDER BY fr_level desc) rank
                from (select /*+ ORDERED */
                             cust.price_event_id,
                             cust.dept,
                             cust.item,
                             cust.zone_node_type,
                             cust.location,
                             cust.action_date,
                             cust.promo_retail,
                             cust.promo_retail_currency,
                             cust.promo_uom,
                             cust.customer_type,
                             rpile.promo_dtl_id,
                             rpile.detail_start_date,
                             rpile.detail_end_date,
                             rpile.type,
                             rpile.promo_comp_msg_type,
                             rpile.promo_id,
                             rpile.exception_parent_id,
                             3 fr_level
                        from rpm_cust_segment_promo_fr_gtt cust,
                             rpm_promo_item_loc_expl_gtt rpile
                       where cust.cur_hier_level      = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                         and cust.dept                = L_dept
                         and rpile.price_event_id (+) = cust.price_event_id
                         and rpile.cur_hier_level (+) = cust.cur_hier_level
                         and rpile.dept (+)           = cust.dept
                         and rpile.item (+)           = cust.item
                         and rpile.location (+)       = cust.location
                         and rpile.zone_node_type (+) = cust.zone_node_type
                         and rpile.customer_type (+)  = cust.customer_type
                         and cust.action_date         BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                      union all
                      select price_event_id,
                             il.dept,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             promo_retail,
                             promo_retail_currency,
                             promo_uom,
                             customer_type,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_comp_msg_type,
                             promo_id,
                             exception_parent_id,
                             2 fr_level
                        from (select /*+ ORDERED */
                                     cust.price_event_id,
                                     cust.dept,
                                     cust.item,
                                     rzl.loc_type zone_node_type,
                                     rzl.location,
                                     cust.action_date,
                                     cust.promo_retail,
                                     cust.promo_retail_currency,
                                     cust.promo_uom,
                                     cust.customer_type,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_comp_msg_type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id
                                from rpm_cust_segment_promo_fr_gtt cust,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     rpm_zone_location rzl
                               where cust.cur_hier_level      = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                 and cust.dept                = L_dept
                                 and rpile.price_event_id (+) = cust.price_event_id
                                 and rpile.cur_hier_level (+) = cust.cur_hier_level
                                 and rpile.dept (+)           = cust.dept
                                 and rpile.item (+)           = cust.item
                                 and rpile.location (+)       = cust.location
                                 and rpile.zone_node_type (+) = cust.zone_node_type
                                 and rpile.customer_type (+)  = cust.customer_type
                                 and cust.action_date         BETWEEN rpile.detail_start_date(+) and NVL(rpile.detail_end_date(+), TO_DATE('3000', 'YYYY'))
                                 and rzl.zone_id              = cust.location
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             il.dept,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             promo_retail,
                             promo_retail_currency,
                             promo_uom,
                             customer_type,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_comp_msg_type,
                             promo_id,
                             exception_parent_id,
                             2 fr_level
                        from (select /*+ ORDERED */
                                     cust.price_event_id,
                                     cust.dept,
                                     im.item,
                                     cust.zone_node_type,
                                     cust.location,
                                     cust.action_date,
                                     cust.promo_retail,
                                     cust.promo_retail_currency,
                                     cust.promo_uom,
                                     cust.customer_type,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_comp_msg_type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id
                                from rpm_cust_segment_promo_fr_gtt cust,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im
                               where cust.cur_hier_level      = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                                 and cust.dept                = L_dept
                                 and rpile.price_event_id (+) = cust.price_event_id
                                 and rpile.cur_hier_level (+) = cust.cur_hier_level
                                 and rpile.dept (+)           = cust.dept
                                 and rpile.item (+)           = cust.item
                                 and rpile.diff_id (+)        = cust.diff_id
                                 and rpile.location (+)       = cust.location
                                 and rpile.zone_node_type (+) = cust.zone_node_type
                                 and rpile.customer_type (+)  = cust.customer_type
                                 and cust.action_date         BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = cust.item
                                 and im.diff_1                = cust.diff_id
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             il.dept,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             promo_retail,
                             promo_retail_currency,
                             promo_uom,
                             customer_type,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_comp_msg_type,
                             promo_id,
                             exception_parent_id,
                             1 fr_level
                        from (select /*+ ORDERED */
                                     cust.price_event_id,
                                     cust.dept,
                                     im.item,
                                     cust.zone_node_type,
                                     cust.location,
                                     cust.action_date,
                                     cust.promo_retail,
                                     cust.promo_retail_currency,
                                     cust.promo_uom,
                                     cust.customer_type,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_comp_msg_type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id
                                from rpm_cust_segment_promo_fr_gtt cust,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im
                               where cust.cur_hier_level      = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                 and cust.dept                = L_dept
                                 and rpile.price_event_id (+) = cust.price_event_id
                                 and rpile.cur_hier_level (+) = cust.cur_hier_level
                                 and rpile.dept (+)           = cust.dept
                                 and rpile.item (+)           = cust.item
                                 and rpile.location (+)       = cust.location
                                 and rpile.zone_node_type (+) = cust.zone_node_type
                                 and rpile.customer_type (+)  = cust.customer_type
                                 and cust.action_date         BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = cust.item
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             il.dept,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             promo_retail,
                             promo_retail_currency,
                             promo_uom,
                             customer_type,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_comp_msg_type,
                             promo_id,
                             exception_parent_id,
                             1 fr_level
                        from (select /*+ ORDERED */
                                     cust.price_event_id,
                                     cust.dept,
                                     im.item,
                                     rzl.loc_type zone_node_type,
                                     rzl.location,
                                     cust.action_date,
                                     cust.promo_retail,
                                     cust.promo_retail_currency,
                                     cust.promo_uom,
                                     cust.customer_type,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_comp_msg_type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id
                                from rpm_cust_segment_promo_fr_gtt cust,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im,
                                     rpm_zone_location rzl
                               where cust.cur_hier_level      = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                                 and cust.dept                = L_dept
                                 and rpile.price_event_id (+) = cust.price_event_id
                                 and rpile.cur_hier_level (+) = cust.cur_hier_level
                                 and rpile.dept (+)           = cust.dept
                                 and rpile.item (+)           = cust.item
                                 and rpile.diff_id (+)        = cust.diff_id
                                 and rpile.location (+)       = cust.location
                                 and rpile.zone_node_type (+) = cust.zone_node_type
                                 and rpile.customer_type (+)  = cust.customer_type
                                 and cust.action_date         BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = cust.item
                                 and im.diff_1                = cust.diff_id
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rzl.zone_id              = cust.location
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location
                      union all
                      select price_event_id,
                             il.dept,
                             il.item,
                             zone_node_type,
                             il.location,
                             action_date,
                             promo_retail,
                             promo_retail_currency,
                             promo_uom,
                             customer_type,
                             promo_dtl_id,
                             detail_start_date,
                             detail_end_date,
                             type,
                             promo_comp_msg_type,
                             promo_id,
                             exception_parent_id,
                             0 fr_level
                        from (select /*+ ORDERED */
                                     cust.price_event_id,
                                     cust.dept,
                                     im.item,
                                     rzl.loc_type zone_node_type,
                                     rzl.location,
                                     cust.action_date,
                                     cust.promo_retail,
                                     cust.promo_retail_currency,
                                     cust.promo_uom,
                                     cust.customer_type,
                                     rpile.promo_dtl_id,
                                     rpile.detail_start_date,
                                     rpile.detail_end_date,
                                     rpile.type,
                                     rpile.promo_comp_msg_type,
                                     rpile.promo_id,
                                     rpile.exception_parent_id
                                from rpm_cust_segment_promo_fr_gtt cust,
                                     rpm_promo_item_loc_expl_gtt rpile,
                                     item_master im,
                                     rpm_zone_location rzl
                               where cust.cur_hier_level      = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                 and cust.dept                = L_dept
                                 and rpile.price_event_id (+) = cust.price_event_id
                                 and rpile.cur_hier_level (+) = cust.cur_hier_level
                                 and rpile.dept (+)           = cust.dept
                                 and rpile.item (+)           = cust.item
                                 and rpile.location (+)       = cust.location
                                 and rpile.zone_node_type (+) = cust.zone_node_type
                                 and rpile.customer_type (+)  = cust.customer_type
                                 and cust.action_date         BETWEEN rpile.detail_start_date (+) and NVL(rpile.detail_end_date (+), TO_DATE('3000', 'YYYY'))
                                 and im.item_parent           = cust.item
                                 and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and rzl.zone_id              = cust.location
                                 and rownum                   > 0) il,
                             rpm_item_loc ril
                       where ril.dept = L_dept
                         and ril.item = il.item
                         and ril.loc  = il.location) t)
        where rank = 1;

   end loop;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   1) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   if I_price_event_type != RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE then

      if RPM_FUTURE_RETAIL_GTT_SQL.GET_CHILDREN(O_cc_error_tbl,
                                                L_child_event_ids,
                                                I_price_event_ids,
                                                I_price_event_type) = 0 then
         return 0;
      end if;

      if L_child_event_ids is NULL or
         L_child_event_ids.COUNT < 1 then
         ---
         return 1;
      end if;

      if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_FR_IL_TIMELINES(O_cc_error_tbl,
                                                          L_child_event_ids,
                                                          I_price_event_type) = 0 then
         return 0;
      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number ||
                               ' - LP_chunk_number: '|| LP_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END EXPL_TIMELINES_TO_IL;

--------------------------------------------------------------------------------
--                         PRIVATE PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION INIT_GLOBALS(O_cc_error_tbl OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(40)  := 'RPM_FUTURE_RETAIL_SQL.INIT_GLOBALS';
   L_error_msg VARCHAR2(255) := NULL;

BEGIN

   if RPM_SYSTEM_OPTIONS_SQL.RESET_GLOBALS(L_error_msg) = FALSE then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_msg));
      return 0;
   end if;

   if RPM_SYSTEM_OPTIONS_DEF_SQL.RESET_GLOBALS(L_error_msg) = FALSE then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_msg));
      return 0;
   end if;

   if DATES_SQL.RESET_GLOBALS(L_error_msg) = FALSE then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_msg));
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INIT_GLOBALS;
--------------------------------------------------------------------------------

FUNCTION MERGE_INTO_TIMELINE(O_cc_error_tbl       IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type   IN     VARCHAR2,
                             I_new_promo_end_date IN     DATE    DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.MERGE_INTO_TIMELINE';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if INSERT_PRICE_CHANGE(O_cc_error_tbl,
                             I_price_event_ids,
                             I_price_event_type) = 0 then
         return 0;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then
         if INSERT_CLEARANCE(O_cc_error_tbl,
                             I_price_event_ids,
                             I_price_event_type) = 0 then
            return 0;
         end if;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                   RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then
         if UPDATE_CLEARANCE_RESET(O_cc_error_tbl,
                                   I_price_event_ids,
                                   I_price_event_type) = 0 then
            return 0;
         end if;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      if INSERT_PROMO(O_cc_error_tbl,
                      I_price_event_ids,
                      I_price_event_type) = 0 then
         return 0;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      if UPDATE_PROMO_END_DATE(O_cc_error_tbl,
                               I_price_event_ids,
                               I_price_event_type,
                               I_new_promo_end_date) = 0 then
         return 0;
      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                               ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_INTO_TIMELINE;
--------------------------------------------------------------------------------

FUNCTION INSERT_PRICE_CHANGE(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.INSERT_PRICE_CHANGE';

   L_exclusion_ids     OBJ_NUMERIC_ID_TABLE  := NULL;
   L_regular_ids       OBJ_NUMERIC_ID_TABLE  := NULL;
   L_parent_rpcs       OBJ_NUM_NUM_DATE_TBL  := NULL;
   L_removable_parents OBJ_NUM_NUM_DATE_TBL  := NULL;
   L_invalid_pc        BOOLEAN               := FALSE;
   L_pc_ids_need_merge OBJ_NUMERIC_ID_TABLE  := NULL;

   L_cc_error_tbl      CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_rf_cc_error_tbl   CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK_PC is
      select /*+ CARDINALITY(ids 10) */
             VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where NOT EXISTS (select 1
                           from rpm_price_change rpc
                          where rpc.price_change_id = VALUE(ids));

   cursor C_EXCLUSION is
      select /*+ CARDINALITY(ids 10) */
             rpc.price_change_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids)
         and rpc.change_type     = RPM_CONSTANTS.RETAIL_EXCLUDE;

   cursor C_REGULAR is
      select VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select VALUE(exc) id
        from table(cast(L_exclusion_ids as OBJ_NUMERIC_ID_TABLE)) exc;

   cursor C_NEED_MERGE is
      select VALUE(ids) id
        from table(cast(L_regular_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select con.price_event_id id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con;

   cursor C_REMOVABLE_PARENTS is
      select /*+ CARDINALITY(parents 10) CARDINALITY(ids 10)*/
             OBJ_NUM_NUM_DATE_REC(parents.numeric_col1,
                                  parents.numeric_col2,
                                  NULL)
        from table(cast(L_parent_rpcs as OBJ_NUM_NUM_DATE_TBL)) parents,
             table(cast(L_pc_ids_need_merge as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where parents.numeric_col1 = VALUE(ids)
         and rpc.price_change_id  = parents.numeric_col1
         and rpc.effective_date  != NVL(parents.date_col, rpc.effective_date);

BEGIN
   --
   -- Stop the whole process if any of the price change id is not valid
   --
   for rec IN C_CHECK_PC loop
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('INVALID_PRICE_EVENT - '||rec.id,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL)));
      L_invalid_pc := TRUE;
   end loop;

   if L_invalid_pc then
      return 0;
   end if;

   --
   -- Get Exclusion Price Changes
   --
   open C_EXCLUSION;
   fetch C_EXCLUSION BULK COLLECT into L_exclusion_ids;
   close C_EXCLUSION;

   --
   -- Get Regular Price Changes
   --
   if L_exclusion_ids is NOT NULL and
      L_exclusion_ids.COUNT != 0 then
      ---
      open C_REGULAR;
      fetch C_REGULAR BULK COLLECT into L_regular_ids;
      close C_REGULAR;

      --
      -- Process the Exclusion
      --
      if PROCESS_EXCLUSION_EVENT(O_cc_error_tbl,
                                 L_exclusion_ids,
                                 I_price_event_type) = 0 then
         return 0;
      end if;

   else
      L_regular_ids := I_price_event_ids;
   end if;

   --
   -- Process the Regular
   --
   if L_regular_ids is NULL or
      L_regular_ids.COUNT = 0 then

      return 1;
   end if;

   if LP_reprocess_ind = 0 then
      if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                    L_regular_ids,
                                    I_price_event_type) = 0 then
         return 0;
      end if;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                L_regular_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                    L_parent_rpcs,
                                                    I_price_event_ids,
                                                    I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_CC_ONE_PC_PER_DAY.VALIDATE(O_cc_error_tbl,
                                     L_parent_rpcs) = 0 then
      return 0;
   end if;

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   open C_NEED_MERGE;
   fetch C_NEED_MERGE BULK COLLECT into L_pc_ids_need_merge;
   close C_NEED_MERGE;

   --
   -- If there is nothing to be merge then EXIT
   --
   if L_pc_ids_need_merge is NULL or
      L_pc_ids_need_merge.COUNT = 0 then
      ---
      O_cc_error_tbl := L_cc_error_tbl;
      return 1;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PRICE_CHANGE(O_cc_error_tbl,
                                                   L_pc_ids_need_merge) = 0 then
      return 0;
   end if;

   if L_parent_rpcs is NOT NULL and
      L_parent_rpcs.COUNT > 0 then

      open C_REMOVABLE_PARENTS;
      fetch C_REMOVABLE_PARENTS BULK COLLECT into L_removable_parents;
      close C_REMOVABLE_PARENTS;

      if L_removable_parents is NOT NULL and
         L_removable_parents.COUNT != 0 then
         ---
         if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                   L_removable_parents,
                                                   RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE) = 0 then
            return 0;
         end if;
      end if;
   end if;

   if ROLL_FORWARD(L_rf_cc_error_tbl,
                   L_pc_ids_need_merge,
                   I_price_event_type,
                   LP_nil_ind) = 0 then
      ---
      O_cc_error_tbl := L_rf_cc_error_tbl;
      return 0;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(L_rf_cc_error_tbl)= 0 then
         O_cc_error_tbl := L_rf_cc_error_tbl;
         return 0;
      end if;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if L_cc_error_tbl is NULL then
         L_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_rf_cc_error_tbl.COUNT loop
         L_cc_error_tbl.EXTEND;
         L_cc_error_tbl(L_cc_error_tbl.COUNT) := L_rf_cc_error_tbl(i);
      end loop;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INSERT_PRICE_CHANGE;

--------------------------------------------------------------------------------

FUNCTION INSERT_CLEARANCE(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                          I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.INSERT_CLEARANCE';

   L_exclusion_ids      OBJ_NUMERIC_ID_TABLE  := NULL;
   L_regular_ids        OBJ_NUMERIC_ID_TABLE  := NULL;
   L_parent_rcs         OBJ_NUM_NUM_DATE_TBL  := NULL;
   L_removable_parents  OBJ_NUM_NUM_DATE_TBL  := NULL;
   L_invalid_cl         BOOLEAN               := FALSE;
   L_clr_ids_need_merge OBJ_NUMERIC_ID_TABLE  := NULL;

   L_cc_error_tbl    CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_rf_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK_CL is
      select VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where NOT EXISTS (select 1
                           from rpm_clearance rc
                          where rc.clearance_id = VALUE(ids));

   cursor C_EXCLUSION is
      select /*+ CARDINALITY(ids 10) */
             rc.clearance_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance_gtt rc
       where rc.price_event_id = VALUE(ids)
         and rc.clearance_id   = VALUE(ids)
         and rc.change_type    = RPM_CONSTANTS.RETAIL_EXCLUDE;

   cursor C_REGULAR is
      select VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select VALUE(exc) id
        from table(cast(L_exclusion_ids as OBJ_NUMERIC_ID_TABLE)) exc;

   cursor C_REMOVABLE_PARENTS is
      select OBJ_NUM_NUM_DATE_REC (parents.numeric_col1,
                                   parents.numeric_col2,
                                   NULL)
        from table(cast(L_parent_rcs as OBJ_NUM_NUM_DATE_TBL)) parents
       where parents.numeric_col2 is NOT NULL;

   cursor C_NEED_MERGE is
      select VALUE(ids) id
        from table(cast(L_regular_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select con.price_event_id id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con;

BEGIN

   --
   -- Stop the whole process if any of the clearance ids are not valid
   --
   for rec IN C_CHECK_CL loop
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('INVALID_PRICE_EVENT - '||rec.id,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL)));
      L_invalid_cl := TRUE;
   end loop;

   if L_invalid_cl then
      return 0;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   --
   -- Get Exclusion Clearances
   --
   open C_EXCLUSION;
   fetch C_EXCLUSION BULK COLLECT into L_exclusion_ids;
   close C_EXCLUSION;

   --
   -- Get Regular Clearances
   --
   if L_exclusion_ids is NOT NULL and
      L_exclusion_ids.COUNT != 0 then
      ---
      open C_REGULAR;
      fetch C_REGULAR BULK COLLECT into L_regular_ids;
      close C_REGULAR;

      --
      -- Process the Exclusion
      --
      if PROCESS_EXCLUSION_EVENT(O_cc_error_tbl,
                                 L_exclusion_ids,
                                 I_price_event_type) = 0 then
         return 0;
      end if;

   else
      L_regular_ids := I_price_event_ids;
   end if;

   open C_NEED_MERGE;
   fetch C_NEED_MERGE BULK COLLECT into L_clr_ids_need_merge;
   close C_NEED_MERGE;

   --
   -- If there is nothing to be merge then EXIT
   --
   if L_clr_ids_need_merge is NULL or
      L_clr_ids_need_merge.COUNT = 0 then
      ---
      O_cc_error_tbl := L_cc_error_tbl;
      return 1;
   end if;

   --
   -- Process the Regular
   --
   if L_regular_ids is NULL or
      L_regular_ids.COUNT = 0 then
      ---
      return 1;
   end if;

   if LP_reprocess_ind = 0 then
      if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                    L_regular_ids,
                                    I_price_event_type) = 0 then
         return 0;
      end if;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                L_regular_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                    L_parent_rcs,
                                                    I_price_event_ids,
                                                    I_price_event_type) = 0 then
      return 0;
   end if;

   if L_parent_rcs is NOT NULL and
      L_parent_rcs.COUNT > 0 then
      --
      open C_REMOVABLE_PARENTS;
      fetch C_REMOVABLE_PARENTS BULK COLLECT into L_removable_parents;
      close C_REMOVABLE_PARENTS;

      if L_removable_parents is NOT NULL and
         L_removable_parents.COUNT != 0 then
         ---
         if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                   L_removable_parents,
                                                   RPM_CONSTANTS.PE_TYPE_CLEARANCE) = 0 then
            return 0;
         end if;
      end if;
   end if;

   if RPM_CLEARANCE_GTT_SQL.POPULATE_GTT(O_cc_error_tbl,
                                         L_clr_ids_need_merge,
                                         LP_bulk_cc_pe_id,
                                         LP_pe_sequence_id,
                                         LP_pe_thread_number,
                                         LP_chunk_number) = 0 then
      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 and
         O_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then
         return 1;
      else
         return 0;
      end if;
   end if;

   if LP_persist_ind = 'Y' then

      if RPM_CLEARANCE_GTT_SQL.SAVE_CLEARANCE_RESET(O_cc_error_tbl,
                                                    L_clr_ids_need_merge,
                                                    LP_bulk_cc_pe_id,
                                                    LP_pe_thread_number,
                                                    LP_chunk_number,
                                                    LP_specific_item_loc) = 0 then
         return 0;
      end if;
   end if;

   if VALIDATE_CLEARANCE_DATES(O_cc_error_tbl,
                               I_price_event_ids) = 0 then
      return 0;
   end if;

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then
      if PROCESS_CONFLICTS(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   end if;

   if RPM_CLR_RESET.VALIDATE(O_cc_error_tbl) = 0 then
      return 0;
   end if;

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then
      if PROCESS_CONFLICTS(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   open C_NEED_MERGE;
   fetch C_NEED_MERGE BULK COLLECT into L_clr_ids_need_merge;
   close C_NEED_MERGE;

   --
   -- If there is nothing to be merge then EXIT
   --
   if L_clr_ids_need_merge is NULL or
      L_clr_ids_need_merge.COUNT = 0 then
      ---
      O_cc_error_tbl := L_cc_error_tbl;
      return 1;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.UPD_CLEARANCE_RESET(O_cc_error_tbl,
                                                    L_clr_ids_need_merge,
                                                    LP_bulk_cc_pe_id,
                                                    LP_pe_sequence_id,
                                                    LP_pe_thread_number,
                                                    LP_chunk_number) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   if RPM_CC_POST_RESET_CLR.VALIDATE(O_cc_error_tbl,
                                     I_price_event_ids) = 0 then
      return 0;
   end if;

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   open C_NEED_MERGE;
   fetch C_NEED_MERGE BULK COLLECT into L_clr_ids_need_merge;
   close C_NEED_MERGE;

   --
   -- If there is nothing to be merge then EXIT
   --
   if L_clr_ids_need_merge is NULL or
      L_clr_ids_need_merge.COUNT = 0 then
      ---
      O_cc_error_tbl := L_cc_error_tbl;
      return 1;
   end if;

   if RPM_CC_ONE_CLR_PER_DAY.VALIDATE(O_cc_error_tbl,
                                      I_price_event_ids) = 0 then
      return 0;
   end if;

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(O_cc_error_tbl) = 0 then
         return 0;
      end if;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   open C_NEED_MERGE;
   fetch C_NEED_MERGE BULK COLLECT into L_clr_ids_need_merge;
   close C_NEED_MERGE;

   --
   -- If there is nothing to be merge then EXIT
   --
   if L_clr_ids_need_merge is NULL or
      L_clr_ids_need_merge.COUNT = 0 then
      ---
      O_cc_error_tbl := L_cc_error_tbl;

      return 1;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_CLEARANCE(O_cc_error_tbl,
                                                L_clr_ids_need_merge,
                                                RPM_CONSTANTS.START_IND) = 0 then
      return 0;
   end if;

   if ROLL_FORWARD(L_rf_cc_error_tbl,
                   L_clr_ids_need_merge,
                   I_price_event_type,
                   LP_nil_ind) = 0 then
      ---
      O_cc_error_tbl := L_rf_cc_error_tbl;
      return 0;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(L_rf_cc_error_tbl) = 0 then
         O_cc_error_tbl := L_rf_cc_error_tbl;
         return 0;
      end if;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if L_cc_error_tbl is NULL then
         L_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_rf_cc_error_tbl.COUNT loop
         L_cc_error_tbl.EXTEND;
         L_cc_error_tbl(L_cc_error_tbl.COUNT) := L_rf_cc_error_tbl(i);
      end loop;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INSERT_CLEARANCE;

--------------------------------------------------------------------------------

FUNCTION UPDATE_CLEARANCE_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.UPDATE_CLEARANCE_RESET';

   L_cc_error_tbl      CONFLICT_CHECK_ERROR_TBL := NULL;
   L_error_message     VARCHAR2(255)            := NULL;
   L_cl_ids_to_process OBJ_NUMERIC_ID_TABLE     := NULL;

   cursor C_CHECK_CL is
      select CONFLICT_CHECK_ERROR_REC (VALUE(ids),
                                       NULL,
                                       RPM_CONSTANTS.PLSQL_ERROR,
                                       'NO_DATA_FOUND')
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where NOT EXISTS (select 1
                           from rpm_clearance rc
                          where rc.clearance_id = VALUE(ids));

   cursor C_IDS_TO_PROCESS is
      select VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select ccet.price_event_id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet;

BEGIN

   -- Stop the whole process if any of the Clearance id is not valid
   -- and we're not dealing with a new clearance reset...
   if I_price_event_type != RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

      open C_CHECK_CL;
      fetch C_CHECK_CL BULK COLLECT into L_cc_error_tbl;
      close C_CHECK_CL;

      for i IN 1..L_cc_error_tbl.COUNT loop
         O_cc_error_tbl.EXTEND;
         O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_cc_error_tbl(i);
      end loop;

   end if;

   if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                 I_price_event_ids,
                                 I_price_event_type) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                I_price_event_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   -- Populate RPM_CLEARANCE_GTT only when not dealing with a new clearance reset

   if I_price_event_type != RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

      delete
        from rpm_clearance_gtt;

      insert into rpm_clearance_gtt (price_event_id,
                                     clearance_id,
                                     clearance_display_id,
                                     state,
                                     reason_code,
                                     exception_parent_id,
                                     reset_ind,
                                     item,
                                     diff_id,
                                     zone_id,
                                     location,
                                     zone_node_type,
                                     effective_date,
                                     out_of_stock_date,
                                     reset_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_percent,
                                     change_selling_uom,
                                     price_guide_id,
                                     vendor_funded_ind,
                                     funding_type,
                                     funding_amount,
                                     funding_amount_currency,
                                     funding_percent,
                                     supplier,
                                     deal_id,
                                     deal_detail_id,
                                     partner_type,
                                     partner_id,
                                     create_date,
                                     create_id,
                                     approval_date,
                                     approval_id,
                                     lock_version,
                                     rc_rowid,
                                     step_identifier)
         select /*+ CARDINALITY(ids 10) INDEX(rc PK_RPM_CLEARANCE)*/
                VALUE(ids),
                clearance_id,
                clearance_display_id,
                state,
                reason_code,
                exception_parent_id,
                0, -- reset_ind
                item,
                diff_id,
                zone_id,
                location,
                zone_node_type,
                effective_date,
                out_of_stock_date,
                reset_date,
                change_type,
                change_amount,
                change_currency,
                change_percent,
                change_selling_uom,
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                funding_amount_currency,
                funding_percent,
                supplier,
                deal_id,
                deal_detail_id,
                partner_type,
                partner_id,
                create_date,
                create_id,
                approval_date,
                approval_id,
                lock_version,
                rc.rowid,
                RPM_CONSTANTS.CAPT_GTT_RFR_SQL_UPD_CLR_RST
           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_clearance rc
          where rc.clearance_id = VALUE(ids);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_RFR_SQL_UPD_CLR_RST,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if RPM_CLEARANCE_GTT_SQL.POPULATE_GTT(O_cc_error_tbl,
                                            I_price_event_ids,
                                            LP_bulk_cc_pe_id,
                                            LP_pe_sequence_id,
                                            LP_pe_thread_number,
                                            LP_chunk_number) = 0 then
         return 0;
      end if;

      if RPM_CLEARANCE_GTT_SQL.SAVE_CLEARANCE_RESET(O_cc_error_tbl,
                                                    I_price_event_ids,
                                                    LP_bulk_cc_pe_id,
                                                    LP_pe_thread_number,
                                                    LP_chunk_number,
                                                    LP_specific_item_loc) = 0 then
         return 0;
      end if;
   end if;

   if RPM_CLR_RESET.VALIDATE(O_cc_error_tbl) = 0 then
      return 0;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   open C_IDS_TO_PROCESS;
   fetch C_IDS_TO_PROCESS BULK COLLECT into L_cl_ids_to_process;
   close C_IDS_TO_PROCESS;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

      if RPM_FUTURE_RETAIL_GTT_SQL.UPD_NEW_CLEARANCE_RESET(O_cc_error_tbl,
                                                           L_cl_ids_to_process) = 0 then
         return 0;
      end if;

   else

      if RPM_FUTURE_RETAIL_GTT_SQL.UPD_CLEARANCE_RESET(O_cc_error_tbl,
                                                       I_price_event_ids,
                                                       LP_bulk_cc_pe_id,
                                                       LP_pe_sequence_id,
                                                       LP_pe_thread_number,
                                                       LP_chunk_number) = 0 then
         return 0;
      end if;

   end if;

   if ROLL_FORWARD(O_cc_error_tbl,
                   L_cl_ids_to_process,
                   I_price_event_type) = 0 then
      return 0;
   end if;

   if EXPL_TIMELINES_TO_IL(O_cc_error_tbl,
                           I_price_event_ids,
                           I_price_event_type,
                           RPM_CONSTANTS.CAPT_GTT_EXPL_TL_TO_IL) = 0 then
      return 0;
   end if;

   if RPM_CC_PUBLISH.STAGE_CLR_RST_REMOVE_MESSAGES(O_cc_error_tbl,
                                                   LP_rib_trans_id,
                                                   LP_bulk_cc_pe_id,
                                                   I_price_event_ids,
                                                   LP_chunk_number) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_CLEARANCE_RESET;
--------------------------------------------------------------------------------

FUNCTION INSERT_PROMO(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.INSERT_PROMO';

   L_invalid_pr BOOLEAN := FALSE;

   L_parent_rpcs       OBJ_NUM_NUM_DATE_TBL := NULL;
   L_removable_parents OBJ_NUM_NUM_DATE_TBL := NULL;
   L_pc_ids_need_merge OBJ_NUMERIC_ID_TABLE := NULL;
   L_removable_sge     OBJ_NUM_NUM_DATE_TBL := NULL;
   L_promo_recs        OBJ_RPM_CC_PROMO_TBL := NULL;

   L_cc_error_tbl    CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_rf_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK_PROMO is
      select /*+ CARDINALITY(ids 10) */
             VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where NOT EXISTS (select 1
                           from rpm_promo_dtl rpd
                          where rpd.promo_dtl_id = VALUE(ids));

   cursor C_REMOVABLE_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_DATE_REC(parents.numeric_col1,
                                  parents.numeric_col2,
                                  NULL)
        from table(cast(L_parent_rpcs as OBJ_NUM_NUM_DATE_TBL)) parents
       where parents.numeric_col2 is NOT NULL;

   cursor C_REMOVABLE_SGE is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_DATE_REC(rpd.exception_parent_id,
                                  rpd.promo_dtl_id,
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where rpd.exception_parent_id              = VALUE(ids)
         and NVL(rpd.sys_generated_exclusion, 0)  = 1;

   cursor C_NEED_MERGE is
      select /*+ CARDINALITY(ids 10) */
             VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select /*+ CARDINALITY(con 10) */
             con.price_event_id id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con;

BEGIN

   -- Stop the whole process if any of the price change id is not valid
   for rec IN C_CHECK_PROMO loop
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('INVALID_PRICE_EVENT - '||rec.id,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL)));
      L_invalid_pr := TRUE;
   end loop;

   if L_invalid_pr then
      return 0;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                I_price_event_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   if LP_reprocess_ind = 0 then

      if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                    I_price_event_ids,
                                    I_price_event_type) = 0 then
         return 0;
      end if;

   elsif LP_reprocess_ind = 1 then

      -- Remove old SGEs
      open C_REMOVABLE_SGE;
      fetch C_REMOVABLE_SGE BULK COLLECT into L_removable_sge;
      close C_REMOVABLE_SGE;

      if L_removable_sge is NOT NULL and
         L_removable_sge.COUNT != 0 then

         if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                   L_removable_sge,
                                                   I_price_event_type) = 0 then
            return 0;
         end if;
      end if;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                    L_parent_rpcs,
                                                    I_price_event_ids,
                                                    I_price_event_type) = 0 then
      return 0;
   end if;

   if L_parent_rpcs is NOT NULL and
      L_parent_rpcs.COUNT > 0 then

      open C_REMOVABLE_PARENTS;
      fetch C_REMOVABLE_PARENTS BULK COLLECT into L_removable_parents;
      close C_REMOVABLE_PARENTS;

      if L_removable_parents is NOT NULL and
         L_removable_parents.COUNT != 0 then
         --
         if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                   L_removable_parents,
                                                   I_price_event_type) = 0 then
            return 0;
         end if;
      end if;
   end if;

   if VALIDATE_PROMO_FOR_MERGE(L_cc_error_tbl,
                               I_price_event_ids,
                               I_price_event_type) = 0 then
      return 0;
   end if;

   if L_cc_error_tbl is NOT NULL and
      L_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(L_cc_error_tbl)= 0 then
         O_cc_error_tbl := L_cc_error_tbl;
         return 0;
      end if;
   end if;

   open C_NEED_MERGE;
   fetch C_NEED_MERGE BULK COLLECT into L_pc_ids_need_merge;
   close C_NEED_MERGE;

   --
   -- If there is nothing to be merge then EXIT
   --
   if L_pc_ids_need_merge is NULL or
      L_pc_ids_need_merge.COUNT = 0 then
      ---
      O_cc_error_tbl := L_cc_error_tbl;

      return 1;
   end if;

   if GET_PROMOTION_OBJECT(O_cc_error_tbl,
                           L_promo_recs,
                           L_pc_ids_need_merge,
                           I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PROMOTION(O_cc_error_tbl,
                                                L_promo_recs,
                                                NULL,
                                                I_price_event_type,
                                                LP_nil_ind,
                                                LP_bulk_cc_pe_id) = 0 then
      return 0;
   end if;

   if ROLL_FORWARD(L_rf_cc_error_tbl,
                   L_pc_ids_need_merge,
                   I_price_event_type,
                   LP_nil_ind) = 0 then

      O_cc_error_tbl := L_rf_cc_error_tbl;

      return 0;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if PROCESS_CONFLICTS(L_rf_cc_error_tbl)= 0 then
         O_cc_error_tbl := L_rf_cc_error_tbl;
         return 0;
      end if;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then

      if L_cc_error_tbl is NULL then
         L_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_rf_cc_error_tbl.COUNT loop
         L_cc_error_tbl.EXTEND;
         L_cc_error_tbl(L_cc_error_tbl.COUNT) := L_rf_cc_error_tbl(i);
      end loop;

   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INSERT_PROMO;
--------------------------------------------------------------------------------

FUNCTION UPDATE_PROMO_END_DATE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                               I_price_event_type   IN     VARCHAR2,
                               I_new_promo_end_date IN     DATE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.UPDATE_PROMO_END_DATE';

   L_promo_recs OBJ_RPM_CC_PROMO_TBL  := NULL;

   L_cc_error_tbl        CONFLICT_CHECK_ERROR_TBL := NULL;
   L_pe_ids_to_be_merged OBJ_NUMERIC_ID_TABLE     := NULL;

   L_cancel_il_off_active_dtl NUMBER  := 0;

   L_removable_parents  OBJ_NUM_NUM_DATE_TBL     := NULL;
   L_parent_rpcs        OBJ_NUM_NUM_DATE_TBL     := NULL;

   cursor C_REMOVABLE_PARENTS is
      select OBJ_NUM_NUM_DATE_REC(parents.numeric_col1,
                                  parents.numeric_col2,
                                  NULL)
        from table(cast(L_parent_rpcs as OBJ_NUM_NUM_DATE_TBL)) parents
       where parents.numeric_col2 is NOT NULL;

   cursor C_NEED_MERGE is
      select VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select con.price_event_id id
        from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) con;

   cursor C_CANCEL_IL_OFF_ACTIVE_DTL is
      select /*+ CARDINALITY(ids 10) */
             DECODE(SUM(NVL(cancel_il_promo_dtl_ind, 0)),
                    0, 0,
                    1)
        from table(cast(L_pe_ids_to_be_merged as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where promo_dtl_id = VALUE(ids);

BEGIN

   if GET_PROMOTION_OBJECT(O_cc_error_tbl,
                           L_promo_recs,
                           I_price_event_ids,
                           I_price_event_type,
                           I_new_promo_end_date) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                I_price_event_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   if IGNORE_PROMO_SIBLINGS(O_cc_error_tbl,
                            I_price_event_ids,
                            I_price_event_type) = 0 then
      return 0;
   end if;

   if I_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then
      ---
      if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                    I_price_event_ids,
                                    I_price_event_type) = 0 then
         return 0;
      end if;

   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   if VALIDATE_PROMO_FOR_MERGE(L_cc_error_tbl,
                               I_price_event_ids,
                               I_price_event_type) = 0 then
      return 0;
   end if;

   if L_cc_error_tbl is NOT NULL and
      L_cc_error_tbl.COUNT > 0 then
      ---
      open C_NEED_MERGE;
      fetch C_NEED_MERGE BULK COLLECT into L_pe_ids_to_be_merged;
      close C_NEED_MERGE;
   else
      L_pe_ids_to_be_merged := I_price_event_ids;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   if L_pe_ids_to_be_merged is NULL or
      L_pe_ids_to_be_merged.COUNT = 0 then
      ---
      return 1;
   end if;

   -- If dealing with a cancellation, need to verify if this is coming from a
   -- user selecting specific item/locs to cancel off an active promo dtl.  If
   -- so, the parent level promotion information needs to be cleaned up for the
   -- item/locs being cancelled and the new detail needs to be merged into the
   -- timelines before the "new end date" processing can happen.

   if LP_end_state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then

      open C_CANCEL_IL_OFF_ACTIVE_DTL;
      fetch C_CANCEL_IL_OFF_ACTIVE_DTL into L_cancel_il_off_active_dtl;
      close C_CANCEL_IL_OFF_ACTIVE_DTL;

      if L_cancel_il_off_active_dtl = 1 then

         LP_cancel_il_promo_dtl := 1;

         if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                          L_parent_rpcs,
                                                          I_price_event_ids,
                                                          I_price_event_type) = 0 then
            return 0;
         end if;

         if L_parent_rpcs is NOT NULL and
            L_parent_rpcs.COUNT > 0 then

            open C_REMOVABLE_PARENTS;
            fetch C_REMOVABLE_PARENTS BULK COLLECT into L_removable_parents;
            close C_REMOVABLE_PARENTS;

            if L_removable_parents is NOT NULL and
               L_removable_parents.COUNT != 0 then
               --
               if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                         L_removable_parents,
                                                         I_price_event_type) = 0 then
                  return 0;
               end if;
            end if;
         end if;

         if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PROMOTION(O_cc_error_tbl,
                                                      L_promo_recs,
                                                      NULL,
                                                      I_price_event_type,
                                                      LP_nil_ind,
                                                      LP_bulk_cc_pe_id) = 0 then

            return 0;
         end if;

      end if;

   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_NEW_PROMO_END_DATE(O_cc_error_tbl,
                                                         L_promo_recs,
                                                         I_price_event_type,
                                                         I_new_promo_end_date) = 0 then
      return 0;
   end if;

   if ROLL_FORWARD(O_cc_error_tbl,
                   L_pe_ids_to_be_merged,
                   I_price_event_type) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_PROMO_END_DATE;

--------------------------------------------------------------------------------

FUNCTION REMOVE_FROM_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type IN     VARCHAR2,
                              I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.REMOVE_FROM_TIMELINE';

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if REMOVE_PRICE_CHANGE(O_cc_error_tbl,
                             I_price_event_ids,
                             I_price_event_type,
                             I_rib_trans_id) = 0 then
         return 0;
      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      if REMOVE_CLEARANCE(O_cc_error_tbl,
                          I_price_event_ids,
                          I_price_event_type,
                          I_rib_trans_id) = 0 then
         return 0;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      if REMOVE_PROMOTION(O_cc_error_tbl,
                          I_price_event_ids,
                          I_price_event_type,
                          I_rib_trans_id) = 0 then
         return 0;
      end if;
   end if;

   if LP_chunk_number = 0 then

      if CLEAN_SGE_ON_REMOVE(O_cc_error_tbl,
                             I_price_event_ids,
                             I_price_event_type) = 0 then
         return 0;
      end if;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_FROM_TIMELINE;

--------------------------------------------------------------------------------

FUNCTION REMOVE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2,
                             I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.REMOVE_PRICE_CHANGE';

   L_parent_rpcs     OBJ_NUM_NUM_DATE_TBL     := NULL;
   L_cc_error_tbl    CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_rf_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_error_message   VARCHAR2(255)            := NULL;

   cursor C_CHECK_PC is
      select VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where NOT EXISTS (select 1
                           from rpm_price_change rpc
                          where rpc.price_change_id = VALUE(ids));

BEGIN

   -- Stop the whole process if any of the price change id is not valid
   for rec IN C_CHECK_PC loop
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('NO_DATA_FOUND - '||rec.id,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL)));
      return 0;
   end loop;

   if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                 I_price_event_ids,
                                 I_price_event_type) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                I_price_event_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                    L_parent_rpcs,
                                                    I_price_event_ids,
                                                    I_price_event_type) = 0 then
      return 0;
   end if;

   forall i IN 1..I_price_event_ids.COUNT
      update rpm_future_retail_gtt
         set price_change_id               = NULL,
             price_change_display_id       = NULL,
             pc_exception_parent_id        = NULL,
             pc_change_type                = NULL,
             pc_change_amount              = NULL,
             pc_change_currency            = NULL,
             pc_change_percent             = NULL,
             pc_change_selling_uom         = NULL,
             pc_null_multi_ind             = NULL,
             pc_multi_units                = NULL,
             pc_multi_unit_retail          = NULL,
             pc_multi_unit_retail_currency = NULL,
             pc_multi_selling_uom          = NULL,
             pc_price_guide_id             = NULL,
             step_identifier               = RPM_CONSTANTS.CAPT_GTT_REMOVE_PC
       where price_event_id  = I_price_event_ids(i)
         and price_change_id = I_price_event_ids(i);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_REMOVE_PC,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   -- ADD_PARENT
   if L_parent_rpcs is NOT NULL and
      L_parent_rpcs.COUNT > 0 then
      ---
      if L_parent_rpcs.COUNT = 1 and
         L_parent_rpcs(1).numeric_col2 is NULL then
         ---
         NULL;
      else
         ---
         if RPM_CC_ONE_PC_PER_DAY.VALIDATE(O_cc_error_tbl,
                                           L_parent_rpcs) = 0 then
            return 0;
         end if;

         L_cc_error_tbl := O_cc_error_tbl;

         if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PARENT_PRICE_CHANGE(O_cc_error_tbl,
                                                                L_parent_rpcs) = 0 then
            return 0;
         end if;
      end if;
   end if;
   -- END ADD_PARENT

   if ROLL_FORWARD(L_rf_cc_error_tbl,
                   I_price_event_ids,
                   I_price_event_type) = 0 then
      O_cc_error_tbl := L_rf_cc_error_tbl;

      return 0;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if L_cc_error_tbl is NULL then
         L_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_rf_cc_error_tbl.COUNT loop
         L_cc_error_tbl.EXTEND;
         L_cc_error_tbl(L_cc_error_tbl.COUNT) := L_rf_cc_error_tbl(i);
      end loop;
   end if;

   O_cc_error_tbl   := L_cc_error_tbl;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_PRICE_CHANGE;
--------------------------------------------------------------------------------

FUNCTION REMOVE_CLEARANCE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                          I_price_event_type IN     VARCHAR2,
                          I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.REMOVE_CLEARANCE';

   L_parent_rcs       OBJ_NUM_NUM_DATE_TBL := NULL;
   L_pe_ids_to_remove OBJ_NUM_NUM_DATE_TBL := NULL;

   L_cc_error_tbl    CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_rf_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK_CL is
      select /*+ CARDINALITY(ids 10) */
             VALUE(ids) id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
       where NOT EXISTS (select 1
                           from rpm_clearance rc
                          where rc.clearance_id = VALUE(ids));

   cursor C_REMOVE_IDS is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                  VALUE(ids),
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids;

BEGIN

   -- Stop the whole process if any of the clearance id is not valid
   for rec IN C_CHECK_CL loop
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('NO_DATA_FOUND - '||rec.id,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL)));
      return 0;
   end loop;

   if LP_persist_ind = 'Y' then

      if RPM_CLEARANCE_GTT_SQL.REMOVE_CLEARANCE_RESET(O_cc_error_tbl,
                                                      I_price_event_ids) = 0 then
         return 0;
      end if;
   end if;

   if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                 I_price_event_ids,
                                 I_price_event_type) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                I_price_event_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   open C_REMOVE_IDS;
   fetch C_REMOVE_IDS BULK COLLECT into L_pe_ids_to_remove;
   close C_REMOVE_IDS;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                             L_pe_ids_to_remove,
                                             I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_CLEARANCE_GTT_SQL.POPULATE_GTT(O_cc_error_tbl,
                                         I_price_event_ids,
                                         LP_bulk_cc_pe_id,
                                         LP_pe_sequence_id,
                                         LP_pe_thread_number,
                                         LP_chunk_number) = 0 then
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                    L_parent_rcs,
                                                    I_price_event_ids,
                                                    I_price_event_type) = 0 then
      return 0;
   end if;

   if L_parent_rcs is NOT NULL and
      L_parent_rcs.COUNT > 0 then
      --
      if L_parent_rcs.COUNT = 1 and
         L_parent_rcs(1).numeric_col2 is NULL then
         NULL;
      else
         if RPM_CLEARANCE_GTT_SQL.SAVE_CLEARANCE_RESET(O_cc_error_tbl,
                                                       L_parent_rcs,
                                                       LP_bulk_cc_pe_id,
                                                       LP_pe_thread_number,
                                                       LP_chunk_number,
                                                       0) = 0 then
            return 0;
         end if;

         if RPM_CLR_RESET.VALIDATE(O_cc_error_tbl) = 0 then
            return 0;
         end if;

         if RPM_CC_POST_RESET_CLR.VALIDATE(O_cc_error_tbl,
                                           L_parent_rcs) = 0 then
            return 0;
         end if;

         if RPM_CC_ONE_CLR_PER_DAY.VALIDATE(O_cc_error_tbl,
                                            L_parent_rcs) = 0 then
            return 0;
         end if;

         L_cc_error_tbl := O_cc_error_tbl;

         if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_CLEARANCE(O_cc_error_tbl,
                                                      L_parent_rcs,
                                                      RPM_CONSTANTS.START_IND) = 0 then
            return 0;
         end if;
      end if;
   end if;

   if ROLL_FORWARD(L_rf_cc_error_tbl,
                   I_price_event_ids,
                   I_price_event_type) = 0 then
      O_cc_error_tbl := L_rf_cc_error_tbl;
      return 0;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then
      ---
      if L_cc_error_tbl is NULL then
         L_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_rf_cc_error_tbl.COUNT loop
         L_cc_error_tbl.EXTEND;
         L_cc_error_tbl(L_cc_error_tbl.COUNT) := L_rf_cc_error_tbl(i);
      end loop;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl,
                                                        LP_rib_trans_id,
                                                        LP_bulk_cc_pe_id,
                                                        I_price_event_ids,
                                                        LP_chunk_number) = 0 then
      return 0;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));

      return 0;

END REMOVE_CLEARANCE;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_CLEARANCE_DATES(O_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.VALIDATE_CLEARANCE_DATES';

   L_error_rec  CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl  CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LO_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_EXISTING_MARKDOWN is
      select CONFLICT_CHECK_ERROR_REC(rc1.price_event_id,
                                      rc1.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      case
                                         when rc1.out_of_stock_date is NOT NULL and
                                              rc1.out_of_stock_date <= rc2.effective_date then
                                            'clearance_out_of_stock_date_before_last_effective_date'
                                         else
                                            'clearance_reset_date_before_last_effective_date'
                                      end)
        from (select /*+ ORDERED USE_NL(rc) */
                     distinct rc.price_event_id,
                     rc.reset_date,
                     rc.out_of_stock_date,
                     rfrg.future_retail_id,
                     rfrg.clearance_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
                     rpm_clearance_gtt rc,
                     rpm_future_retail_gtt rfrg
               where rc.clearance_id      = VALUE(ids)
                 and rc.clearance_id     != ccet.price_event_id
                 and (   rc.out_of_stock_date is NOT NULL
                      or rc.reset_date        is NOT NULL)
                 and rfrg.price_event_id  = rc.price_event_id
                 and rfrg.clearance_id    is NOT NULL
                 and rfrg.clear_start_ind IN (RPM_CONSTANTS.START_IND,
                                              RPM_CONSTANTS.START_END_IND)) rc1,
             rpm_clearance rc2
       where rc2.clearance_id = rc1.clearance_id
         and rc2.state        = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
         and (   (    rc1.out_of_stock_date is NOT NULL
                  and rc1.out_of_stock_date <= rc2.effective_date)
              or (    rc1.reset_date is NOT NULL
                  and rc1.reset_date <= rc2.effective_date));

   cursor C_EXECUTED_RESET is
      select CONFLICT_CHECK_ERROR_REC (rfrg.PRICE_EVENT_ID,
                                       rfrg.FUTURE_RETAIL_ID,
                                       RPM_CONSTANTS.CONFLICT_ERROR,
                                       'future_retail_price_change_rule24')
        from (select /*+ CARDINALITY(ids 10) CARDINALITY(ccet 10)*/
                     rc.price_event_id,
                     rc.effective_date
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
                     rpm_clearance_gtt rc
               where rc.clearance_id   = VALUE(ids)
                 and rc.clearance_id  != ccet.price_event_id
                 and rc.effective_date = LP_vdate
                 and rownum            > 0) rc1,
             rpm_future_retail_gtt rfrg,
             rpm_clearance_reset rc2
       where rfrg.price_event_id = rc1.price_event_id
         and rfrg.action_date    = rc1.effective_date
         and rc2.item            = rfrg.item
         and rc2.location        = rfrg.location
         and rc2.state           = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
         and rc2.effective_date  = rc1.effective_date;

BEGIN

   if O_cc_error_tbl is NOT NULL and
      O_cc_error_tbl.COUNT > 0 then
      ---
      L_error_tbl := O_cc_error_tbl;
   else
      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);
      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   open C_EXISTING_MARKDOWN;
   fetch C_EXISTING_MARKDOWN BULK COLLECT into LO_error_tbl;
   close C_EXISTING_MARKDOWN;

   if LO_error_tbl is NOT NULL and
      LO_error_tbl.COUNT > 0 then
      ---
      if O_cc_error_tbl is NULL or
         O_cc_error_tbl.COUNT = 0 then
         ---
         O_cc_error_tbl := LO_error_tbl;
      else
         for i IN 1..LO_error_tbl.COUNT loop
            O_cc_error_tbl.EXTEND;
            O_cc_error_tbl(O_cc_error_tbl.COUNT) := LO_error_tbl(i);
         end loop;
      end if;
   end if;

   open C_EXECUTED_RESET;
   fetch C_EXECUTED_RESET BULK COLLECT into LO_error_tbl;
   close C_EXECUTED_RESET;

   if LO_error_tbl is NOT NULL and
      LO_error_tbl.COUNT > 0 then

      if O_cc_error_tbl is NULL or
         O_cc_error_tbl.COUNT = 0 then
         ---
         O_cc_error_tbl := LO_error_tbl;
      else
         for i IN 1..LO_error_tbl.COUNT loop
            O_cc_error_tbl.EXTEND;
            O_cc_error_tbl(O_cc_error_tbl.COUNT) := LO_error_tbl(i);
         end loop;
      end if;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                       SQLERRM,
                                                                       L_program,
                                                                       TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_CLEARANCE_DATES;
--------------------------------------------------------------------

FUNCTION REMOVE_PROMOTION(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                          I_price_event_type IN     VARCHAR2,
                          I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.REMOVE_PROMOTION';

   L_parent_rpcs       OBJ_NUM_NUM_DATE_TBL     := NULL;
   L_parent_promo_recs OBJ_RPM_CC_PROMO_TBL     := NULL;
   L_pe_ids_to_publish OBJ_NUMERIC_ID_TABLE     := I_price_event_ids;
   L_pe_ids_to_remove  OBJ_NUM_NUM_DATE_TBL     := NULL;
   L_cc_error_tbl      CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_rf_cc_error_tbl   CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_NON_SIMPLE_PROMO_EXCLUSIONS is
      select /*+ CARDINALITY (ids, 10) */
             VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl
       where rpd.promo_dtl_id             = VALUE(ids)
         and rpd.promo_dtl_id             = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id  = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id       = rpddl.promo_dtl_list_id
         and rpddl.change_type           != RPM_CONSTANTS.RETAIL_EXCLUDE;


   cursor C_REMOVE_IDS is
      select OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                  VALUE(ids),
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids;

BEGIN

   if IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl,
                                 I_price_event_ids,
                                 I_price_event_type) = 0 then
      return 0;
   end if;

   if IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl,
                                I_price_event_ids,
                                I_price_event_type) = 0 then
      return 0;
   end if;

   -- Need to strip out simple promo exclusions so that they don't end up on the payload tables
   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then

      open C_NON_SIMPLE_PROMO_EXCLUSIONS;
      fetch C_NON_SIMPLE_PROMO_EXCLUSIONS BULK COLLECT into L_pe_ids_to_publish;
      close C_NON_SIMPLE_PROMO_EXCLUSIONS;

   end if;

   if L_pe_ids_to_publish is NOT NULL and
      L_pe_ids_to_publish.COUNT > 0 then
      ---
      if RPM_CC_PUBLISH.STAGE_PROM_REMOVE_MESSAGES(O_cc_error_tbl,
                                                   I_rib_trans_id,
                                                   LP_bulk_cc_pe_id,
                                                   I_price_event_type,
                                                   L_pe_ids_to_publish,
                                                   LP_chunk_number) = 0 then
         return 0;
      end if;

   end if;

   open C_REMOVE_IDS;
   fetch C_REMOVE_IDS BULK COLLECT into L_pe_ids_to_remove;
   close C_REMOVE_IDS;

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT(O_cc_error_tbl,
                                                    L_parent_rpcs,
                                                    I_price_event_ids,
                                                    I_price_event_type) = 0 then
      return 0;
   end if;

   if L_parent_rpcs is NOT NULL and
      L_parent_rpcs.COUNT > 0 then

      for i IN 1..L_parent_rpcs.COUNT loop
         if L_parent_rpcs(i).numeric_col2 is NOT NULL then

            if GET_PROMOTION_OBJECT(O_cc_error_tbl,
                                    L_parent_promo_recs,
                                    OBJ_NUMERIC_ID_TABLE(L_parent_rpcs(i).numeric_col2),
                                    I_price_event_type) = 0 then
               return 0;
            end if;

            if VALIDATE_PROMO_FOR_MERGE(O_cc_error_tbl,
                                        OBJ_NUMERIC_ID_TABLE(L_parent_rpcs(i).numeric_col2),
                                        I_price_event_type) = 0 then
               return 0;
            end if;

            --merge parent
            if RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PROMOTION(O_cc_error_tbl,
                                                         L_parent_promo_recs,
                                                         L_parent_rpcs(i).numeric_col1,
                                                         I_price_event_type) = 0 then
               return 0;
            end if;

         end if; -- if numeric_col2 is NOT NULL

      end loop;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                             L_pe_ids_to_remove,
                                             I_price_event_type) = 0 then
      return 0;
   end if;

   if ROLL_FORWARD(L_rf_cc_error_tbl,
                   I_price_event_ids,
                   I_price_event_type) = 0 then
      O_cc_error_tbl := L_rf_cc_error_tbl;

      return 0;
   end if;

   if L_rf_cc_error_tbl is NOT NULL and
      L_rf_cc_error_tbl.COUNT > 0 then

      if L_cc_error_tbl is NULL then
         L_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
      end if;

      for i IN 1..L_rf_cc_error_tbl.COUNT loop
         L_cc_error_tbl.EXTEND;
         L_cc_error_tbl(L_cc_error_tbl.COUNT) := L_rf_cc_error_tbl(I);
      end loop;
   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_PROMOTION;

--------------------------------------------------------------------------------

FUNCTION GET_PROMOTION_OBJECT(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                              O_promo_recs            OUT OBJ_RPM_CC_PROMO_TBL,
                              I_promo_ids          IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type   IN     VARCHAR2,
                              I_new_promo_end_date IN     DATE  DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.GET_PROMOTION_OBJECT';

   L_promo_recs    OBJ_RPM_CC_PROMO_TBL := NULL;
   L_old_end_dates OBJ_NUMERIC_DATE_TBL := NULL;

   cursor C_OLD_END_DATE is
      select NEW OBJ_NUMERIC_DATE_REC(price_event_id,
                                      action_date)
        from (select /*+ CARDINALITY(ids 10) */
                     frgtt.price_event_id,
                     frgtt.action_date,
                     RANK() OVER (PARTITION BY frgtt.price_event_id
                                      ORDER BY frgtt.action_date) rank_value
                from table(cast(I_promo_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_future_retail_gtt frgtt,
                     rpm_promo_item_loc_expl_gtt prgtt
               where frgtt.price_event_id = VALUE(ids)
                 and prgtt.promo_dtl_id   = VALUE(ids)
                 and frgtt.dept           = prgtt.dept
                 and frgtt.price_event_id = prgtt.price_event_id
                 and frgtt.item           = prgtt.item
                 and frgtt.location       = prgtt.location
                 and frgtt.zone_node_type = prgtt.zone_node_type
                 and frgtt.action_date    BETWEEN prgtt.detail_start_date and NVL(prgtt.detail_end_date, frgtt.action_date))
       where rank_value = 1;

   cursor C_PROMO_OBJ is
      select /*+ CARDINALITY(pr 10, od 10) */
             OBJ_RPM_CC_PROMO_REC(promo_id,
                                  promo_display_id,
                                  rp_secondary_ind,
                                  promo_comp_id,
                                  comp_display_id,
                                  rpc_secondary_ind,
                                  promo_comp_detail_id,
                                  rpcd_start_date,
                                  NVL(I_new_promo_end_date, rpcd_end_date),
                                  old_rpcd_start_date,
                                  date_col,
                                  apply_to_code,
                                  timebased_dtl_ind,
                                  zone_ids,
                                  change_type,
                                  change_amount,
                                  change_currency,
                                  change_percent,
                                  change_selling_uom,
                                  price_guide_id,
                                  exception_parent_id,
                                  promotion_type,
                                  exclusion,
                                  customer_type)
        from table(cast(L_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr,
             table(cast(L_old_end_dates as OBJ_NUMERIC_DATE_TBL)) od
       where pr.promo_comp_detail_id = od.numeric_col(+);

   cursor C_SIMPLE_PROMO is
      select OBJ_RPM_CC_PROMO_REC(t.promo_id,
                                  t.promo_display_id,
                                  t.rp_secondary_ind,
                                  t.promo_comp_id,
                                  t.comp_display_id,
                                  t.rpc_secondary_ind,
                                  t.promo_dtl_id,
                                  t.rpd_start_date,
                                  t.rpd_end_date,
                                  t.old_rpd_start_date,
                                  t.old_rpd_end_date,
                                  t.apply_to_code,
                                  t.timebased_dtl_ind,
                                  OBJ_NUMERIC_ID_TABLE(t.zone_id),
                                  t.change_type,
                                  t.change_amount,
                                  t.change_currency,
                                  t.change_percent,
                                  t.change_selling_uom,
                                  t.price_guide_id,
                                  t.exception_parent_id,
                                  t.promo_type,
                                  exclusion,
                                  t.customer_type)
        from (select /*+ ORDERED CARDINALITY(ids 10) USE_NL(ids, rpd, rpc, rp, rpdlg, rpzl, rpdl, rpddl) */
                     distinct
                     rp.promo_id,
                     rp.promo_display_id,
                     NVL(rp.secondary_ind, 0) rp_secondary_ind,
                     rpc.promo_comp_id,
                     rpc.comp_display_id,
                     NVL(rpc.secondary_ind, 0) rpc_secondary_ind,
                     rpd.promo_dtl_id,
                     rpd.start_date rpd_start_date,
                     TO_DATE(TO_CHAR(rpd.end_date, 'YYYYMMDDHH24MI'), 'YYYYMMDDHH24MI') rpd_end_date, -- strip out the seconds value
                     rpd.start_date old_rpd_start_date,
                     NULL old_rpd_end_date,
                     rpd.apply_to_code,
                     rpd.timebased_dtl_ind,
                     rpzl.zone_id,
                     rpddl.change_type,
                     rpddl.change_amount,
                     rpddl.change_currency,
                     rpddl.change_percent,
                     rpddl.change_selling_uom,
                     rpd.price_guide_id price_guide_id,
                     rpd.exception_parent_id,
                     RPM_CONSTANTS.SIMPLE_CODE promo_type,
                     DECODE(rpddl.change_type,
                            RPM_CONSTANTS.RETAIL_EXCLUDE, 1,
                            0) exclusion,
                     rpc.customer_type
                from table(cast(I_promo_ids AS OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_promo_zone_location rpzl
               where rpd.promo_dtl_id            = VALUE(ids)
                 and rp.promo_id                 = rpc.promo_id
                 and rpc.promo_comp_id           = rpd.promo_comp_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpd.promo_dtl_id            = rpzl.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE) t;

   cursor C_THRESHOLD_PROMO is
      select OBJ_RPM_CC_PROMO_REC(promo_id,
                                  promo_display_id,
                                  rp_secondary_ind,
                                  promo_comp_id,
                                  comp_display_id,
                                  rpc_secondary_ind,
                                  promo_dtl_id,
                                  rpcd_start_date,
                                  rpcd_end_date,
                                  rpcd_start_date,
                                  NULL,
                                  apply_to_code,
                                  timebased_dtl_ind,
                                  OBJ_NUMERIC_ID_TABLE(zone_id),
                                  change_type,
                                  change_amount,
                                  change_currency,
                                  change_percent,
                                  change_selling_uom,
                                  price_guide_id,
                                  exception_parent_id,
                                  RPM_CONSTANTS.THRESHOLD_CODE,
                                  NULL,
                                  customer_type)
     from (select /*+ CARDINALITY(ids 10) */
                  distinct
                  rp.promo_id,
                  rp.promo_display_id,
                  NVL(rp.secondary_ind, 0) rp_secondary_ind,
                  rpc.promo_comp_id,
                  rpc.comp_display_id,
                  NVL(rpc.secondary_ind, 0) rpc_secondary_ind,
                  rpc.type,
                  rpd.promo_dtl_id,
                  rpd.start_date rpcd_start_date,
                  TO_DATE(TO_CHAR(rpd.end_date, 'YYYYMMDDHH24MI'), 'YYYYMMDDHH24MI') rpcd_end_date, -- strip out the seconds value
                  rpzl.zone_id,
                  rpd.apply_to_code,
                  rpd.timebased_dtl_ind,
                  rpddl.change_type,
                  rpddl.change_amount,
                  rpddl.change_currency,
                  rpddl.change_percent,
                  rpddl.change_selling_uom,
                  rpd.price_guide_id,
                  rpd.exception_parent_id,
                  rpc.customer_type,
                  RANK() OVER (PARTITION BY VALUE(ids)
                                   ORDER BY rpddl.qual_value desc,
                                            NVL(rpddl.change_amount, rpddl.change_percent) desc) rank
             from table(cast(I_promo_ids AS OBJ_NUMERIC_ID_TABLE)) ids,
                  rpm_promo_zone_location rpzl,
                  rpm_promo_dtl rpd,
                  rpm_promo_dtl_list_grp rpdlg,
                  rpm_promo_dtl_list rpdl,
                  rpm_promo_dtl_disc_ladder rpddl,
                  rpm_promo_comp rpc,
                  rpm_promo rp
            where rpd.promo_dtl_id           = VALUE(ids)
              and rp.promo_id                = rpc.promo_id
              and rpc.promo_comp_id          = rpd.promo_comp_id
              and rpd.promo_dtl_id           = rpzl.promo_dtl_id
              and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
              and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
              and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
              and rpc.type                   = RPM_CONSTANTS.THRESHOLD_CODE)
      where rank = 1;

   cursor C_COMPLEX_PROMO is
      select OBJ_RPM_CC_PROMO_REC(t.promo_id,
                                  t.promo_display_id,
                                  t.rp_secondary_ind,
                                  t.promo_comp_id,
                                  t.comp_display_id,
                                  t.rpc_secondary_ind,
                                  t.promo_dtl_id,
                                  t.rpd_start_date,
                                  t.rpd_end_date,
                                  t.old_rpd_start_date,
                                  t.old_rpd_end_date,
                                  t.apply_to_code,
                                  t.timebased_dtl_ind,
                                  OBJ_NUMERIC_ID_TABLE(t.zone_id),
                                  t.change_type,
                                  t.change_amount,
                                  t.change_currency,
                                  t.change_percent,
                                  t.change_selling_uom,
                                  t.price_guide_id,
                                  t.exception_parent_id,
                                  t.promo_type,
                                  exclusion,
                                  t.customer_type)
        from (select /*+ CARDINALITY(ids 10) */
                     distinct
                     rp.promo_id,
                     rp.promo_display_id,
                     NVL(rp.secondary_ind, 0) rp_secondary_ind,
                     rpc.promo_comp_id,
                     rpc.comp_display_id,
                     NVL(rpc.secondary_ind, 0) rpc_secondary_ind,
                     rpd.promo_dtl_id,
                     rpd.start_date rpd_start_date,
                     TO_DATE(TO_CHAR(rpd.end_date, 'YYYYMMDDHH24MI'), 'YYYYMMDDHH24MI') rpd_end_date, -- strip out the seconds value
                     rpd.start_date old_rpd_start_date,
                     NULL old_rpd_end_date,
                     rpd.apply_to_code,
                     rpd.timebased_dtl_ind,
                     NULL zone_id,
                     case rpddl.change_type
                        when RPM_CONSTANTS.RETAIL_EXCLUDE then
                           RPM_CONSTANTS.RETAIL_EXCLUDE
                        else
                           NULL
                     end change_type,
                     NULL change_amount,
                     NULL change_currency,
                     NULL change_percent,
                     NULL change_selling_uom,
                     rpd.price_guide_id price_guide_id,
                     rpd.exception_parent_id,
                     rpc.type promo_type,
                     DECODE(rpddl.change_type,
                            RPM_CONSTANTS.RETAIL_EXCLUDE, 1,
                            0) exclusion,
                     rpc.customer_type
                from table(cast(I_promo_ids AS OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_list_grp rpdlg
               where rpd.promo_dtl_id            = VALUE(ids)
                 and rp.promo_id                 = rpc.promo_id
                 and rpc.promo_comp_id           = rpd.promo_comp_id
                 and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                 and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                 and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                 and rpc.type                    IN (RPM_CONSTANTS.COMPLEX_CODE,
                                                     RPM_CONSTANTS.TRANSACTION_CODE)
                 and rpddl.change_type           is NOT NULL) t;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then

      open C_SIMPLE_PROMO;
      fetch C_SIMPLE_PROMO BULK COLLECT into L_promo_recs;
      close C_SIMPLE_PROMO;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD) then

      open C_THRESHOLD_PROMO;
      fetch C_THRESHOLD_PROMO BULK COLLECT into L_promo_recs;
      close C_THRESHOLD_PROMO;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then
      open C_COMPLEX_PROMO;
      fetch C_COMPLEX_PROMO BULK COLLECT into L_promo_recs;
      close C_COMPLEX_PROMO;

   end if;

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      open C_OLD_END_DATE;
      fetch C_OLD_END_DATE BULK COLLECT into L_old_end_dates;
      close C_OLD_END_DATE;

      open C_PROMO_OBJ;
      fetch C_PROMO_OBJ BULK COLLECT into O_promo_recs;
      close C_PROMO_OBJ;

   else
      O_promo_recs := L_promo_recs;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_PROMOTION_OBJECT;
--------------------------------------------------------------------------------

FUNCTION IGNORE_APPROVED_EXCEPTIONS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                    I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.IGNORE_APPROVED_EXCEPTIONS';

   L_child_event_ids OBJ_NUM_NUM_DATE_TBL := null;

BEGIN

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_CHILDREN(O_cc_error_tbl,
                                             L_child_event_ids,
                                             I_price_event_ids,
                                             I_price_event_type) = 0 then
      return 0;
   end if;

   if L_child_event_ids is NULL or
      L_child_event_ids.COUNT = 0 then

      return 1;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_TIMELINES(O_cc_error_tbl,
                                                 I_price_event_type,
                                                 L_child_event_ids) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END IGNORE_APPROVED_EXCEPTIONS;
--------------------------------------------------------------------------------

FUNCTION IGNORE_APPROVED_LOC_MOVES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.IGNORE_APPROVED_LOC_MOVES';

   L_zone_level_pes OBJ_NUM_NUM_DATE_TBL;

   cursor C_ZONE_PC is
      select /*+ CARDINALITY(ids 10) */
             new OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                      rpc.zone_id,
                                      rpc.effective_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids)
         and rpc.zone_id         is NOT NULL;

   cursor C_ZONE_CL is
      select /*+ CARDINALITY(ids 10) */
             new OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                      rc.zone_id,
                                      rc.effective_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance_gtt rc
       where rc.price_event_id = VALUE(ids)
         and rc.clearance_id   = VALUE(ids)
         and rc.zone_id        is NOT NULL;

   cursor C_ZONE_PROMO is
      select /*+ CARDINALITY(ids 10) */
             new OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                      rpzl.zone_id,
                                      rpd.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_comp rpc,
             rpm_promo_zone_location rpzl,
             rpm_location_move rlm
       where rpd.promo_dtl_id  = VALUE(ids)
         and rpd.promo_dtl_id  = rpzl.promo_dtl_id
         and rpd.promo_comp_id = rpc.promo_comp_id
         and rpzl.zone_id      is NOT NULL
         and rpzl.zone_id      = rlm.old_zone_id
         and rlm.state         = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
         and NOT EXISTS (select 1
                           from rpm_promo_zone_location rpzl2
                          where rpzl2.promo_dtl_id = rpd.promo_dtl_id
                            and rpzl2.zone_id      = rlm.new_zone_id);

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_ZONE_PC;
      fetch C_ZONE_PC BULK COLLECT into L_zone_level_pes;
      close C_ZONE_PC;

      if L_zone_level_pes is NULL or
         L_zone_level_pes.COUNT = 0 then

         return 1;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then
      open C_ZONE_CL;
      fetch C_ZONE_CL BULK COLLECT into L_zone_level_pes;
      close C_ZONE_CL;

      if L_zone_level_pes is NULL or
         L_zone_level_pes.COUNT = 0 then

         return 1;
      end if;

   else
      open C_ZONE_PROMO;
      fetch C_ZONE_PROMO BULK COLLECT into L_zone_level_pes;
      close C_ZONE_PROMO;

      if L_zone_level_pes is NULL or
         L_zone_level_pes.COUNT = 0 then

         return 1;
      end if;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_LOC_MOVE_TIMELINES(O_cc_error_tbl,
                                                          L_zone_level_pes) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END IGNORE_APPROVED_LOC_MOVES;
--------------------------------------------------------------------------------

FUNCTION PROCESS_EXCLUSION_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.PROCESS_EXCLUSION_EVENT';

   L_rpc_ids        OBJ_NUMERIC_ID_TABLE := NULL;
   L_parent_rpc_ids OBJ_NUM_NUM_DATE_TBL := NULL;

   L_rc_ids        OBJ_NUMERIC_ID_TABLE := NULL;
   L_parent_rc_ids OBJ_NUM_NUM_DATE_TBL := NULL;

   cursor C_PARENT_RPC is
      select OBJ_NUM_NUM_DATE_REC(price_event_id,
                                  price_change_id,
                                  NULL)
        from (select /*+ CARDINALITY(ids 10) */
                     distinct
                     rfrg.price_event_id,
                     rfrg.price_change_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_future_retail_gtt rfrg,
                     rpm_price_change rpc
               where rfrg.price_event_id  = VALUE(ids)
                 and rpc.price_change_id  = VALUE(ids)
                 and rfrg.price_change_id = rpc.exception_parent_id);

   cursor C_PARENT_RC is
      select OBJ_NUM_NUM_DATE_REC(price_event_id,
                                  clearance_id,
                                  NULL)
        from (select /*+ CARDINALITY(ids 10) */
                     distinct
                     rfrg.price_event_id,
                     rfrg.clearance_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_future_retail_gtt rfrg,
                     rpm_clearance rc
               where rfrg.price_event_id = VALUE(ids)
                 and rc.clearance_id     = VALUE(ids)
                 and rfrg.clearance_id   = rc.exception_parent_id);

   cursor C_PC_PE is
      select /*+ CARDINALITY(ids 10) */
             distinct
             rpc2.price_change_id
        from table(cast(L_parent_rpc_ids as OBJ_NUM_NUM_DATE_TBL)) ids,
             rpm_price_change rpc1,
             rpm_price_change rpc2
       where rpc1.price_change_id = ids.numeric_col2
         and rpc1.price_change_id = rpc2.exception_parent_id
         and rpc2.price_change_id = ids.numeric_col1;

   cursor C_CL_PE is
      select /*+ CARDINALITY(ids 10) */
             rc2.clearance_id
        from table(cast(L_parent_rc_ids as OBJ_NUM_NUM_DATE_TBL)) ids,
             rpm_clearance rc1,
             rpm_clearance rc2
       where rc1.clearance_id = ids.numeric_col2
         and rc1.clearance_id = rc2.exception_parent_id
         and rc2.clearance_id = ids.numeric_col1;

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_PARENT_RPC;
      fetch C_PARENT_RPC BULK COLLECT into L_parent_rpc_ids;
      close C_PARENT_RPC;

      if L_parent_rpc_ids is NULL or
         L_parent_rpc_ids.COUNT = 0 then
         return 1;
      end if;

      if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                L_parent_rpc_ids,
                                                I_price_event_type) = 0 then
         return 0;
      end if;

      open C_PC_PE;
      fetch C_PC_PE BULK COLLECT into L_rpc_ids;
      close C_PC_PE;

      if ROLL_FORWARD(O_cc_error_tbl,
                      L_rpc_ids,
                      I_price_event_type) = 0 then
         return 0;
      end if;

      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 then

         if PROCESS_CONFLICTS(O_cc_error_tbl)= 0 then
            return 0;
         end if;
      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      open C_PARENT_RC;
      fetch C_PARENT_RC BULK COLLECT into L_parent_rc_ids;
      close C_PARENT_RC;

      if L_parent_rc_ids is NULL or
         L_parent_rc_ids.COUNT = 0 then
         return 1;
      end if;

      if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT(O_cc_error_tbl,
                                                L_parent_rc_ids,
                                                I_price_event_type) = 0 then
         return 0;
      end if;

      open C_CL_PE;
      fetch C_CL_PE BULK COLLECT into L_rc_ids;
      close C_CL_PE;

      if ROLL_FORWARD(O_cc_error_tbl,
                      L_rc_ids,
                      I_price_event_type) = 0 then
         return 0;
      end if;

      if O_cc_error_tbl is NOT NULL and
         O_cc_error_tbl.COUNT > 0 then

         if PROCESS_CONFLICTS(O_cc_error_tbl)= 0 then
            return 0;
         end if;
      end if;

      if RPM_FUTURE_RETAIL_GTT_SQL.UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl,
                                                           LP_rib_trans_id,
                                                           LP_bulk_cc_pe_id,
                                                           I_price_event_ids,
                                                           LP_chunk_number) = 0 then
         return 0;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_EXCLUSION_EVENT;

--------------------------------------------------------------------------------

FUNCTION ROLL_FORWARD(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2,
                      I_nil_ind          IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.ROLL_FORWARD';

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_fr_conflict  NUMBER := 0;
   L_pl_sql_error NUMBER := 0;
   L_start_time   TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   if RPM_ROLL_FORWARD_SQL.EXECUTE(L_cc_error_tbl,
                                   I_price_event_type,
                                   I_nil_ind) = 0 then
      --
      if L_cc_error_tbl is NOT NULL and
         L_cc_error_tbl.COUNT > 0 then

         for i IN 1..L_cc_error_tbl.COUNT loop
            if L_cc_error_tbl(i).error_type = RPM_CONSTANTS.PLSQL_ERROR then
               L_pl_sql_error := 1;
               EXIT;
            end if;
         end loop;
      end if;

      if L_pl_sql_error = 1 then
         O_cc_error_tbl := L_cc_error_tbl;
         return 0;
      else
         L_fr_conflict := 1;
      end if;

      -- Populate push back table
      if L_fr_conflict = 1 then
         if O_cc_error_tbl is NULL then
            O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();
         end if;

         if L_cc_error_tbl is NOT NULL and
            L_cc_error_tbl.COUNT > 0 then

            for i IN 1..L_cc_error_tbl.COUNT loop
               O_cc_error_tbl.EXTEND;
               O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_cc_error_tbl(i);
            end loop;
         end if;
      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                               ' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END ROLL_FORWARD;
--------------------------------------------------------------------------------

FUNCTION PUBLISH_CHANGES(IO_cc_error_tbl             IN OUT CONFLICT_CHECK_ERROR_TBL,
                         I_rib_trans_id              IN     NUMBER,
                         I_bulk_cc_pe_id             IN     NUMBER,
                         I_process_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE,
                         I_process_price_event_type  IN     VARCHAR2,
                         I_remove_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                         I_remove_price_event_type   IN     VARCHAR2,
                         I_cancel_item_loc_promo_dtl IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.PUBLISH_CHANGES';

   L_pe_type       VARCHAR2(3)          := NULL;
   L_pe_ids        OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_type    NUMBER               := NULL;
   L_start_time    TIMESTAMP            := SYSTIMESTAMP;
   L_error_message VARCHAR2(255)        := NULL;

   cursor C_NON_SIMPLE_PROMO_EXCLUSIONS is
      select /*+ CARDINALITY (ids, 10) */
             VALUE(ids)
        from table(cast(L_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl
       where rpd.promo_dtl_id             = VALUE(ids)
         and rpd.promo_dtl_id             = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id  = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id       = rpddl.promo_dtl_list_id
         and rpddl.change_type           != RPM_CONSTANTS.RETAIL_EXCLUDE;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   if I_process_price_event_ids is NOT NULL then
      L_pe_ids := I_process_price_event_ids;
      L_pe_type := I_process_price_event_type;
   else
      L_pe_ids := I_remove_price_event_ids;
      L_pe_type := I_remove_price_event_type;
   end if;

   IO_cc_error_tbl := NULL;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_PAYLOAD_POPULATION,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   1, -- CSPFR
                                                   1, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  L_error_message));
      return 0;

   end if;

   if LP_from_loc_move_ind != 1 then
      if EXPL_TIMELINES_TO_IL(IO_cc_error_tbl,
                              L_pe_ids,
                              L_pe_type,
                              RPM_CONSTANTS.CAPT_GTT_EXPL_TL_TO_IL) = 0 then
         return 0;
      end if;
   end if;

   -- Need to strip out the approval of simple promo exclusions so that they don't
   -- end up on the payload tables.  Unapproval messaging is handled elsewhere.
   if I_process_price_event_type is NOT NULL and
      L_pe_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                    RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then

      open C_NON_SIMPLE_PROMO_EXCLUSIONS;
      fetch C_NON_SIMPLE_PROMO_EXCLUSIONS BULK COLLECT into L_pe_ids;
      close C_NON_SIMPLE_PROMO_EXCLUSIONS;

   end if;

   if I_remove_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if RPM_CC_PUBLISH.STAGE_PC_REMOVE_MESSAGES(IO_cc_error_tbl,
                                                 I_rib_trans_id,
                                                 I_bulk_cc_pe_id,
                                                 L_pe_ids,
                                                 LP_chunk_number) = 0 then
         return 0;
      end if;

      if RPM_CC_PUBLISH.STAGE_CLR_MESSAGES(IO_cc_error_tbl,
                                           I_rib_trans_id,
                                           I_bulk_cc_pe_id,
                                           L_pe_type,
                                           L_pe_ids,
                                           LP_chunk_number) = 0 then
         return 0;
      end if;
   end if;

   if I_remove_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      if RPM_CC_PUBLISH.STAGE_CLR_REMOVE_MESSAGES(IO_cc_error_tbl,
                                                  I_rib_trans_id,
                                                  I_bulk_cc_pe_id,
                                                  L_pe_ids,
                                                  LP_chunk_number) = 0 then
         return 0;
      end if;
   end if;

   if I_process_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      if RPM_CC_PUBLISH.STAGE_PC_MESSAGES(IO_cc_error_tbl,
                                          I_rib_trans_id,
                                          I_bulk_cc_pe_id,
                                          L_pe_ids,
                                          LP_chunk_number) = 0 then
         return 0;
      end if;
   end if;

   if I_process_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                     RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                     RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                     RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      if RPM_CC_PUBLISH.STAGE_CLR_MESSAGES(IO_cc_error_tbl,
                                           I_rib_trans_id,
                                           I_bulk_cc_pe_id,
                                           L_pe_type,
                                           L_pe_ids,
                                           LP_chunk_number) = 0 then
         return 0;
      end if;
   end if;

   if I_cancel_item_loc_promo_dtl = 1 and
      LP_chunk_number IN (0,
                          1) then

      if RPM_CC_PUBLISH.STAGE_PROM_CIL_MESSAGES(IO_cc_error_tbl,
                                                I_rib_trans_id,
                                                I_process_price_event_ids) = 0 then
         return 0;
      end if;
   end if;

   -- First, publish any simple promo create messages if dealing with a simple promo create...
   if I_process_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO) and
      I_cancel_item_loc_promo_dtl = 0 then

      if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(IO_cc_error_tbl,
                                            I_rib_trans_id,
                                            I_bulk_cc_pe_id,
                                            L_pe_ids,
                                            L_pe_type,
                                            RPM_CONSTANTS.SIMPLE_CODE,
                                            RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE,
                                            LP_chunk_number) = 0 then
         return 0;
      end if;

   end if;

   -- Second, publish any simple promo mod messages...
   if I_process_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) or
      I_remove_price_event_type IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE)then

      if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(IO_cc_error_tbl,
                                            I_rib_trans_id,
                                            I_bulk_cc_pe_id,
                                            L_pe_ids,
                                            L_pe_type,
                                            RPM_CONSTANTS.SIMPLE_CODE,
                                            RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD,
                                            1,  -- chunk_number
                                            I_cancel_item_loc_promo_dtl) = 0 then
         return 0;
      end if;

   end if;

   if I_process_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                     RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then
      L_promo_type := RPM_CONSTANTS.TRANSACTION_CODE;
   else
      L_promo_type := RPM_CONSTANTS.COMPLEX_CODE;
   end if;

   if I_cancel_item_loc_promo_dtl = 0 then

      -- Lastly, if dealing with the right price event type, publish any complex promo create or mod messages...
      if I_process_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                        RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                        RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

         if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(IO_cc_error_tbl,
                                               I_rib_trans_id,
                                               I_bulk_cc_pe_id,
                                               L_pe_ids,
                                               L_pe_type,
                                               L_promo_type,
                                               RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE,
                                               LP_chunk_number) = 0 then
            return 0;
         end if;

      elsif I_process_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                           RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                           RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(IO_cc_error_tbl,
                                               I_rib_trans_id,
                                               I_bulk_cc_pe_id,
                                               L_pe_ids,
                                               L_pe_type,
                                               L_promo_type,
                                               RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD,
                                               1) = 0 then  -- chunk_number
            return 0;
         end if;
      end if;
   end if;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END PUBLISH_CHANGES;

--------------------------------------------------------------------------------

FUNCTION PUSH_BACK_ERROR_ROWS(IO_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                              I_populate_cc_error IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.PUSH_BACK_ERROR_ROWS';

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;

BEGIN

   if I_populate_cc_error = 1 and
      LP_price_event_type IS NOT NULL then

      if (LP_nil_ind  = 0 and
          LP_from_loc_move_ind = 0) and
         IO_cc_error_tbl is NOT NULL and
         IO_cc_error_tbl.COUNT > 0 and
         IO_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR then

         if POPULATE_CC_ERROR_TABLE(IO_cc_error_tbl,
                                    LP_price_event_type) = 0 then
            return 0;
         end if;

      end if;

   end if;

   if (LP_nil_ind = 1 or
       LP_from_loc_move_ind = 1) and
      (IO_cc_error_tbl is NOT NULL and
       IO_cc_error_tbl.COUNT > 0 and
       IO_cc_error_tbl(1).error_type = RPM_CONSTANTS.CONFLICT_ERROR) then

       if POPULATE_CC_ERROR_PE_ITEM_LOC(IO_cc_error_tbl) = 0 then
          return 0;
       end if;
   end if;

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then

      forall i IN 1..IO_cc_error_tbl.COUNT
         delete
           from rpm_cust_segment_promo_fr_gtt
          where price_event_id  = IO_cc_error_tbl(i).price_event_id;

      forall i IN 1..IO_cc_error_tbl.COUNT
         delete
           from rpm_promo_item_loc_expl_gtt
          where price_event_id  = IO_cc_error_tbl(i).price_event_id;

      forall i IN 1..IO_cc_error_tbl.COUNT
         delete
           from rpm_future_retail_gtt
          where price_event_id  = IO_cc_error_tbl(i).price_event_id;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK_ERROR_ROWS;

--------------------------------------------------------------------------------

FUNCTION PUSH_BACK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                   I_price_event_type IN     VARCHAR2,
                   I_new_item_loc     IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_FUTURE_RETAIL_SQL.PUSH_BACK';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   merge /*+ USE_NL(rpile) */
         into rpm_promo_item_loc_expl rpile
   using (select price_event_id,
                 promo_item_loc_expl_id,
                 item,
                 dept,
                 class,
                 subclass,
                 location,
                 zone_node_type,
                 promo_id,
                 promo_display_id,
                 promo_secondary_ind,
                 promo_comp_id,
                 comp_display_id,
                 promo_dtl_id,
                 type,
                 customer_type,
                 detail_secondary_ind,
                 detail_start_date,
                 detail_end_date,
                 detail_apply_to_code,
                 timebased_dtl_ind,
                 detail_change_type,
                 detail_change_amount,
                 detail_change_currency,
                 detail_change_percent,
                 detail_change_selling_uom,
                 detail_price_guide_id,
                 exception_parent_id,
                 promo_comp_msg_type,
                 deleted_ind,
                 rpile_rowid,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from rpm_promo_item_loc_expl_gtt
           where NVL(deleted_ind, 0) != 1
             and (   TRUNC(detail_start_date) >= TRUNC(LP_push_back_start_date)
                  or rpile_rowid              is NULL
                  or I_price_event_type       IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                  RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                  RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE))) rpileg
      on (rpile.rowid = rpileg.rpile_rowid)
   when MATCHED then
      update
         set promo_id                  = rpileg.promo_id,
             promo_display_id          = rpileg.promo_display_id,
             promo_secondary_ind       = rpileg.promo_secondary_ind,
             promo_comp_id             = rpileg.promo_comp_id,
             comp_display_id           = rpileg.comp_display_id,
             promo_dtl_id              = rpileg.promo_dtl_id,
             type                      = rpileg.type,
             detail_secondary_ind      = rpileg.detail_secondary_ind,
             detail_start_date         = rpileg.detail_start_date,
             detail_end_date           = rpileg.detail_end_date,
             timebased_dtl_ind         = rpileg.timebased_dtl_ind,
             detail_apply_to_code      = rpileg.detail_apply_to_code,
             detail_change_type        = rpileg.detail_change_type,
             detail_change_amount      = rpileg.detail_change_amount,
             detail_change_currency    = rpileg.detail_change_currency,
             detail_change_percent     = rpileg.detail_change_percent,
             detail_change_selling_uom = rpileg.detail_change_selling_uom,
             detail_price_guide_id     = rpileg.detail_price_guide_id,
             exception_parent_id       = rpileg.exception_parent_id
   when NOT MATCHED then
      insert (promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              zone_node_type,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              timebased_dtl_ind,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              rpileg.item,
              rpileg.dept,
              rpileg.class,
              rpileg.subclass,
              rpileg.location,
              rpileg.zone_node_type,
              rpileg.promo_id,
              rpileg.promo_display_id,
              rpileg.promo_secondary_ind,
              rpileg.promo_comp_id,
              rpileg.comp_display_id,
              rpileg.promo_dtl_id,
              rpileg.type,
              rpileg.customer_type,
              rpileg.detail_secondary_ind,
              rpileg.detail_start_date,
              rpileg.detail_end_date,
              rpileg.detail_apply_to_code,
              rpileg.timebased_dtl_ind,
              rpileg.detail_change_type,
              rpileg.detail_change_amount,
              rpileg.detail_change_currency,
              rpileg.detail_change_percent,
              rpileg.detail_change_selling_uom,
              rpileg.detail_price_guide_id,
              rpileg.exception_parent_id,
              rpileg.item_parent,
              rpileg.diff_id,
              rpileg.zone_id,
              rpileg.max_hier_level,
              rpileg.cur_hier_level);

   merge into rpm_cust_segment_promo_fr target
   using (select price_event_id,
                 cust_segment_promo_id,
                 item,
                 zone_node_type,
                 location,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 cspfr_rowid,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from rpm_cust_segment_promo_fr_gtt
           where (   TRUNC(action_date) >= TRUNC(LP_push_back_start_date)
                  or cspfr_rowid        is NULL)
             and promo_retail is NOT NULL) source
   on (target.rowid = source.cspfr_rowid)
      when MATCHED then
      update
         set target.customer_type         = source.customer_type,
             target.promo_retail          = source.promo_retail,
             target.promo_retail_currency = source.promo_retail_currency,
             target.promo_uom             = source.promo_uom,
             target.complex_promo_ind     = source.complex_promo_ind
   when NOT MATCHED then
      insert (cust_segment_promo_id,
              item,
              zone_node_type,
              location,
              action_date,
              customer_type,
              dept,
              promo_retail,
              promo_retail_currency,
              promo_uom,
              complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level)
      values (source.cust_segment_promo_id,
              source.item,
              source.zone_node_type,
              source.location,
              source.action_date,
              source.customer_type,
              source.dept,
              source.promo_retail,
              source.promo_retail_currency,
              source.promo_uom,
              source.complex_promo_ind,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level);

   merge into rpm_future_retail rfr
   using (select price_event_id,
                 future_retail_id,
                 item,
                 dept,
                 class,
                 subclass,
                 zone_node_type,
                 location,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_exception_parent_id,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 on_simple_promo_ind,
                 on_complex_promo_ind,
                 promo_comp_msg_type,
                 price_change_id,
                 price_change_display_id,
                 pc_exception_parent_id,
                 pc_change_type,
                 pc_change_amount,
                 pc_change_currency,
                 pc_change_percent,
                 pc_change_selling_uom,
                 pc_null_multi_ind,
                 pc_multi_units,
                 pc_multi_unit_retail,
                 pc_multi_unit_retail_currency,
                 pc_multi_selling_uom,
                 pc_price_guide_id,
                 pc_msg_type,
                 pc_selling_retail_ind,
                 pc_multi_unit_ind,
                 clearance_id,
                 clearance_display_id,
                 clear_mkdn_index,
                 clear_start_ind,
                 clear_change_type,
                 clear_change_amount,
                 clear_change_currency,
                 clear_change_percent,
                 clear_change_selling_uom,
                 clear_price_guide_id,
                 clear_msg_type,
                 loc_move_from_zone_id,
                 loc_move_to_zone_id,
                 location_move_id,
                 lock_version,
                 timeline_seq,
                 rfr_rowid,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from rpm_future_retail_gtt
           where (    I_new_item_loc     != 1
                  and TRUNC(action_date) >= TRUNC(LP_push_back_start_date))
              or I_new_item_loc = 1
              or rfr_rowid      is NULL) rfrg
   on (rfr.rowid = rfrg.rfr_rowid)
   when MATCHED then
      update
         set rfr.selling_retail                = rfrg.selling_retail,
             rfr.selling_retail_currency       = rfrg.selling_retail_currency,
             rfr.selling_uom                   = rfrg.selling_uom,
             rfr.multi_units                   = rfrg.multi_units,
             rfr.multi_unit_retail             = rfrg.multi_unit_retail,
             rfr.multi_unit_retail_currency    = rfrg.multi_unit_retail_currency,
             rfr.multi_selling_uom             = rfrg.multi_selling_uom,
             rfr.clear_exception_parent_id     = rfrg.clear_exception_parent_id,
             rfr.clear_retail                  = rfrg.clear_retail,
             rfr.clear_retail_currency         = rfrg.clear_retail_currency,
             rfr.clear_uom                     = rfrg.clear_uom,
             rfr.simple_promo_retail           = NVL(rfrg.simple_promo_retail, rfrg.clear_retail),
             rfr.simple_promo_retail_currency  = NVL(rfrg.simple_promo_retail_currency, rfrg.clear_retail_currency),
             rfr.simple_promo_uom              = NVL(rfrg.simple_promo_uom, rfrg.clear_uom),
             rfr.on_simple_promo_ind           = rfrg.on_simple_promo_ind,
             rfr.on_complex_promo_ind          = rfrg.on_complex_promo_ind,
             rfr.price_change_id               = rfrg.price_change_id,
             rfr.price_change_display_id       = rfrg.price_change_display_id,
             rfr.pc_exception_parent_id        = rfrg.pc_exception_parent_id,
             rfr.pc_change_type                = rfrg.pc_change_type,
             rfr.pc_change_amount              = rfrg.pc_change_amount,
             rfr.pc_change_currency            = rfrg.pc_change_currency,
             rfr.pc_change_percent             = rfrg.pc_change_percent,
             rfr.pc_change_selling_uom         = rfrg.pc_change_selling_uom,
             rfr.pc_null_multi_ind             = rfrg.pc_null_multi_ind,
             rfr.pc_multi_units                = rfrg.pc_multi_units,
             rfr.pc_multi_unit_retail          = rfrg.pc_multi_unit_retail,
             rfr.pc_multi_unit_retail_currency = rfrg.pc_multi_unit_retail_currency,
             rfr.pc_multi_selling_uom          = rfrg.pc_multi_selling_uom,
             rfr.pc_price_guide_id             = rfrg.pc_price_guide_id,
             rfr.clearance_id                  = rfrg.clearance_id,
             rfr.clearance_display_id          = rfrg.clearance_display_id,
             rfr.clear_mkdn_index              = rfrg.clear_mkdn_index,
             rfr.clear_start_ind               = rfrg.clear_start_ind,
             rfr.clear_change_type             = rfrg.clear_change_type,
             rfr.clear_change_amount           = rfrg.clear_change_amount,
             rfr.clear_change_currency         = rfrg.clear_change_currency,
             rfr.clear_change_percent          = rfrg.clear_change_percent,
             rfr.clear_change_selling_uom      = rfrg.clear_change_selling_uom,
             rfr.clear_price_guide_id          = rfrg.clear_price_guide_id,
             rfr.loc_move_from_zone_id         = rfrg.loc_move_from_zone_id,
             rfr.loc_move_to_zone_id           = rfrg.loc_move_to_zone_id,
             rfr.location_move_id              = rfrg.location_move_id
   when NOT MATCHED then
      insert(future_retail_id,
             dept,
             class,
             subclass,
             item,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_exception_parent_id,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             on_simple_promo_ind,
             on_complex_promo_ind,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level)
      values(rfrg.future_retail_id,
             rfrg.dept,
             rfrg.class,
             rfrg.subclass,
             rfrg.item,
             rfrg.zone_node_type,
             rfrg.location,
             rfrg.action_date,
             rfrg.selling_retail,
             rfrg.selling_retail_currency,
             rfrg.selling_uom,
             rfrg.multi_units,
             rfrg.multi_unit_retail,
             rfrg.multi_unit_retail_currency,
             rfrg.multi_selling_uom,
             rfrg.clear_exception_parent_id,
             rfrg.clear_retail,
             rfrg.clear_retail_currency,
             rfrg.clear_uom,
             NVL(rfrg.simple_promo_retail, rfrg.clear_retail),
             NVL(rfrg.simple_promo_retail_currency, rfrg.clear_retail_currency),
             NVL(rfrg.simple_promo_uom, rfrg.clear_uom),
             rfrg.price_change_id,
             rfrg.price_change_display_id,
             rfrg.pc_exception_parent_id,
             rfrg.pc_change_type,
             rfrg.pc_change_amount,
             rfrg.pc_change_currency,
             rfrg.pc_change_percent,
             rfrg.pc_change_selling_uom,
             rfrg.pc_null_multi_ind,
             rfrg.pc_multi_units,
             rfrg.pc_multi_unit_retail,
             rfrg.pc_multi_unit_retail_currency,
             rfrg.pc_multi_selling_uom,
             rfrg.pc_price_guide_id,
             rfrg.clearance_id,
             rfrg.clearance_display_id,
             rfrg.clear_mkdn_index,
             rfrg.clear_start_ind,
             rfrg.clear_change_type,
             rfrg.clear_change_amount,
             rfrg.clear_change_currency,
             rfrg.clear_change_percent,
             rfrg.clear_change_selling_uom,
             rfrg.clear_price_guide_id,
             rfrg.loc_move_from_zone_id,
             rfrg.loc_move_to_zone_id,
             rfrg.location_move_id,
             rfrg.on_simple_promo_ind,
             rfrg.on_complex_promo_ind,
             rfrg.item_parent,
             rfrg.diff_id,
             rfrg.zone_id,
             rfrg.max_hier_level,
             rfrg.cur_hier_level);

   if PUSH_BACK_PURGE(O_cc_error_tbl) = 0 then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK;

--------------------------------------------------------------------------------

FUNCTION CONSOLIDATE_CC_ERROR_TBL(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_cc_error_tbl1 IN     CONFLICT_CHECK_ERROR_TBL,
                                  I_cc_error_tbl2 IN     CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.CONSOLIDATE_CC_ERROR_TBL';

   L_cc_error_tbl1 CONFLICT_CHECK_ERROR_TBL := I_cc_error_tbl1;
   L_cc_error_tbl2 CONFLICT_CHECK_ERROR_TBL := I_cc_error_tbl2;

BEGIN

   O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL();

   if L_cc_error_tbl1 is NOT NULL and
      L_cc_error_tbl1.COUNT > 0 then

      for i IN 1..L_cc_error_tbl1.COUNT loop
         O_cc_error_tbl.EXTEND;
         O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_cc_error_tbl1(i);
      end loop;
   end if;
   --
   if L_cc_error_tbl2 is NOT NULL and
      L_cc_error_tbl2.COUNT > 0 then

      for i IN 1..L_cc_error_tbl1.COUNT loop
         O_cc_error_tbl.EXTEND;
         O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_cc_error_tbl2(i);
      end loop;
   end if;
   --
   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        conflict_check_error_rec(NULL, NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CONSOLIDATE_CC_ERROR_TBL;

--------------------------------------------------------------------------------

FUNCTION VALIDATE_PROMO_FOR_MERGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.VALIDATE_PROMO_FOR_MERGE';

BEGIN

   if RPM_CC_ONE_CUST_SEG_PROMO_MAX.VALIDATE(O_cc_error_tbl,
                                             I_price_event_ids,
                                             I_price_event_type) = 0 then
      return 0;
   end if;

   if RPM_CC_PROMO_LVL_EXCLUSION.VALIDATE(O_cc_error_tbl,
                                          I_price_event_ids,
                                          I_price_event_type) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_PROMO_FOR_MERGE;
--------------------------------------------------------------------------------

FUNCTION VALIDATE_FP_UOM(O_cc_error_tbl       IN OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type   IN     VARCHAR2)
RETURN NUMBER IS

   L_program                    VARCHAR2(50)                       := 'RPM_FUTURE_RETAIL_SQL.VALIDATE_FP_UOM';
   L_error_rec                  CONFLICT_CHECK_ERROR_REC           := NULL;
   L_merch_nd_zone_nd_date_tbl  OBJ_MERCH_ND_ZONE_ND_DATE_TBL      := OBJ_MERCH_ND_ZONE_ND_DATE_TBL();
   L_uom                        RPM_FUTURE_RETAIL.SELLING_UOM%TYPE := NULL;
   L_curr_promo_dtl_id          NUMBER                             := 0;
   L_prev_promo_dtl_id          NUMBER                             := 0;

   cursor C_FP_PC is
      select /*+ CARDINALITY(ids 10) */
             rpc.price_change_id,
             OBJ_MERCH_ND_ZONE_ND_DATE_REC(rpc.zone_node_type,
                                           rpc.location,
                                           rpc.zone_id,
                                           NULL, --merch_type
                                           NULL, --dept
                                           NULL, --class
                                           NULL, --subclass
                                           rpc.item,
                                           rpc.diff_id,
                                           rpc.skulist,
                                           rpc.effective_date,
                                           rpc.price_event_itemlist) rec_uom
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids)
         and rpc.LINK_CODE       is NULL
         and change_selling_uom  is NULL
         and rpc.change_type     = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
      union all
      select /*+ CARDINALITY(ids 10) */
             rpc.price_change_id,
             OBJ_MERCH_ND_ZONE_ND_DATE_REC(rpc.zone_node_type,
                                           rpc.location,
                                           rpc.zone_id,
                                           NULL, --merch_type
                                           NULL, --dept
                                           NULL, --class
                                           NULL, --subclass
                                           rpc.link_code,
                                           rpc.diff_id,
                                           rpc.skulist,
                                           rpc.effective_date,
                                           rpc.price_event_itemlist) rec_uom
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_link_code_attribute rlca,
             rpm_price_change rpc
       where rpc.price_change_id = VALUE(ids)
         and change_selling_uom  is NULL
         and rpc.LINK_CODE       is NOT NULL
         and rlca.LINK_CODE      = rpc.LINK_CODE
         and rpc.change_type     = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE;

   cursor C_FP_PROMO is
      select /*+ CARDINALITY(ids 10) */
             rpd.promo_dtl_id,
             rpddl.promo_dtl_disc_ladder_id,
             OBJ_MERCH_ND_ZONE_ND_DATE_REC(rpzl.zone_node_type,
                                           rpzl.location,
                                           rpzl.zone_id,
                                           rpdm.merch_type,
                                           rpdm.dept,
                                           rpdm.class,
                                           rpdm.subclass,
                                           rpdm.item,
                                           rpdm.diff_id,
                                           rpdm.skulist,
                                           rpd.start_date,
                                           rpdm.price_event_itemlist) rec_uom
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_zone_location rpzl,
             rpm_promo_dtl_merch_node rpdm,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl
       where rpd.promo_dtl_id           = VALUE(ids)
         and rpzl.promo_dtl_id          = rpd.promo_dtl_id
         and rpdm.promo_dtl_id          = rpd.promo_dtl_id
         and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
         and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
         and rpddl.change_selling_uom   is NULL
         and rpddl.change_type          = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE;

BEGIN

   if EXPL_TIMELINES_TO_IL(O_cc_error_tbl,
                           I_price_event_ids,
                           I_price_event_type,
                           RPM_CONSTANTS.CAPT_GTT_EXPL_TL_TO_IL_FP_UOM) = 0 then
      return 0;
   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      for rec IN C_FP_PC loop

         L_merch_nd_zone_nd_date_tbl := OBJ_MERCH_ND_ZONE_ND_DATE_TBL(rec.rec_uom);

         if RPM_UOM_VALIDATION_SQL.SAME_SELLING_UOM(O_cc_error_tbl,
                                                    L_uom,
                                                    L_merch_nd_zone_nd_date_tbl,
                                                    I_price_event_type,
                                                    rec.price_change_id) = 0 then

             return 0;
         end if;

         if L_uom is NOT NULL then

            update rpm_price_change
               set change_selling_uom = L_uom
             where price_change_id = rec.price_change_id
               and (   change_selling_uom != L_uom
                    or change_selling_uom is NULL);

         else

            L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_change_id,
                                                    NULL,
                                                    RPM_CONSTANTS.CONFLICT_ERROR,
                                                    'selling_uom_not_unique');
            if O_cc_error_tbl is NULL then

                O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

            else

                O_cc_error_tbl.EXTEND;
                O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_error_rec;

            end if;

         end if;

      end loop;

   end if;

   -- threshold is ignored as uom is mandatory in case of threshold fixed price promos

   if I_price_event_type in (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO) then
      for rec in C_FP_PROMO loop

         L_merch_nd_zone_nd_date_tbl := OBJ_MERCH_ND_ZONE_ND_DATE_TBL(rec.rec_uom);

         L_curr_promo_dtl_id := rec.promo_dtl_id;

         if L_curr_promo_dtl_id = L_prev_promo_dtl_id then
            NULL;
         else

            if RPM_UOM_VALIDATION_SQL.SAME_SELLING_UOM(O_cc_error_tbl,
                                                       L_uom,
                                                       L_merch_nd_zone_nd_date_tbl,
                                                       I_price_event_type,
                                                       rec.promo_dtl_id) = 0 then
               return 0;
            end if;

            if L_uom is NOT NULL then

               update rpm_promo_dtl_disc_ladder
                  set change_selling_uom = L_uom
                where promo_dtl_disc_ladder_id = rec.promo_dtl_disc_ladder_id
                  and (   change_selling_uom != L_uom
                       or change_selling_uom is NULL);

            else
               L_prev_promo_dtl_id := rec.promo_dtl_id;

               L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.promo_dtl_id,
                                                       NULL,
                                                       RPM_CONSTANTS.CONFLICT_ERROR,
                                                       'selling_uom_not_unique');

               if O_cc_error_tbl is NULL then

                   O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

               else

                   O_cc_error_tbl.EXTEND;

                   O_cc_error_tbl(O_cc_error_tbl.COUNT) := L_error_rec;

               end if;

            end if;

         end if;

      end loop;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE_FP_UOM;

-------------------------------------------------------------------------------
FUNCTION IGNORE_PROMO_SIBLINGS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.IGNORE_PROMO_SIBLINGS';

   L_sibling_promo_ids OBJ_NUM_NUM_DATE_TBL := NULL;

BEGIN

   if RPM_FUTURE_RETAIL_GTT_SQL.GET_PROMO_SIBLINGS(O_cc_error_tbl,
                                                   L_sibling_promo_ids,
                                                   I_price_event_ids)= 0 then
      return 0;
   end if;

   if L_sibling_promo_ids is NULL or
      L_sibling_promo_ids.COUNT = 0 then

      return 1;
   end if;

   if RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_TIMELINES(O_cc_error_tbl,
                                                 I_price_event_type,
                                                 L_sibling_promo_ids) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END IGNORE_PROMO_SIBLINGS;
-------------------------------------------------------------------------------

FUNCTION PROCESS_CONFLICTS(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL)

RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.PROCESS_CONFLICTS';

   L_new_cc_error_tbl         CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_cc_error_elg_in_gtt_tbl  CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_cc_error_elg_non_gtt_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_cc_error_non_elg_tbl     CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_sys_option_tolerance RPM_SYSTEM_OPTIONS.SYS_GEN_EXCLUSION_TOLERANCE%TYPE := NULL;
   L_exceed_pe_id         OBJ_NUMERIC_ID_TABLE                                := OBJ_NUMERIC_ID_TABLE();
   L_exception_exist      VARCHAR2(1)                                         := NULL;

   cursor C_FIND_EXCEPTION is
      select 'Y'
        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
       where cc.error_type = RPM_CONSTANTS.PLSQL_ERROR;

   cursor C_ELG_PRICE_EVENT_NOT_IN_GTT is
      --verify if the price event is eligible for sys gen exclusions.
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_bulk_cc_pe_thread pe
       where cc.price_event_id              = pe.price_event_id
         and pe.bulk_cc_pe_id               = LP_bulk_cc_pe_id
         and pe.price_event_type            = LP_price_event_type
         and pe.elig_for_sys_gen_exclusions = 1
         and NOT EXISTS (select 1
                           from rpm_cc_sys_gen_head_gtt gtt
                          where gtt.price_event_id = cc.price_event_id)
      union all
      --if the price event is a reprocess, it is assumed that is it eligible for sys gen exclusions.
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_bulk_cc_pe_thread pe,
             rpm_bulk_cc_pe bcp
       where cc.price_event_id          = pe.price_event_id
         and pe.bulk_cc_pe_id           = LP_bulk_cc_pe_id
         and pe.price_event_type        = LP_price_event_type
         and pe.bulk_cc_pe_id           = bcp.bulk_cc_pe_id
         and bcp.sys_gen_excl_reprocess = 1
         and NOT EXISTS (select 1
                           from rpm_cc_sys_gen_head_gtt gtt
                          where gtt.price_event_id = cc.price_event_id);

   cursor C_ELG_PRICE_EVENT_IN_GTT is
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_bulk_cc_pe_thread pe,
             rpm_cc_sys_gen_head_gtt gtt
       where cc.price_event_id              = pe.price_event_id
         and pe.bulk_cc_pe_id               = LP_bulk_cc_pe_id
         and pe.price_event_type            = LP_price_event_type
         and pe.elig_for_sys_gen_exclusions = 1
         and cc.price_event_id              = gtt.price_event_id
         and gtt.tolerance_ind              = 1
      union all
      --if the price event is a reprocess, it is assumed that is it eligible for sys gen exclusions.
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_bulk_cc_pe_thread pe,
             rpm_cc_sys_gen_head_gtt gtt,
             rpm_bulk_cc_pe bcp
       where cc.price_event_id          = pe.price_event_id
         and pe.bulk_cc_pe_id           = LP_bulk_cc_pe_id
         and pe.price_event_type        = LP_price_event_type
         and cc.price_event_id          = gtt.price_event_id
         and gtt.tolerance_ind          = 1
         and bcp.bulk_cc_pe_id          = pe.bulk_cc_pe_id
         and bcp.sys_gen_excl_reprocess = 1;

   cursor C_TOLERANCE_EXCEED is
      select price_event_id
        from (select ((dg.cc_count / hg.price_event_total) * 100) percent,
                     hg.price_event_id
                from rpm_cc_sys_gen_head_gtt hg,
                     (select SUM(tran_item_count) cc_count,
                             price_event_id
                        from (select /*+ LEADING(cc1) */
                                     distinct gtt.price_event_id,
                                     tran_item_count,
                                     future_retail_id,
                                     MIN(future_retail_id) OVER (PARTITION BY item,
                                                                              NVL(diff_id, -999)) min_fr_id
                                from rpm_cc_sys_gen_detail_gtt gtt,
                                     (select /*+ CARDINALITY(cc, 1) */
                                             distinct cc.price_event_id
                                        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
                                       where rownum > 0) cc1
                               where future_retail_id   is NOT NULL
                                 and gtt.price_event_id = cc1.price_event_id)
                       where future_retail_id = min_fr_id
                       group by price_event_id
                      union all
                      select SUM(tran_item_count) cc_count,
                             price_event_id
                        from (select /*+ LEADING(cc2) */
                                     distinct gtt.price_event_id,
                                     tran_item_count,
                                     cs_promo_fr_id,
                                     MIN(cs_promo_fr_id) OVER (PARTITION BY item,
                                                                            NVL(diff_id, -999)) min_fr_id
                                from rpm_cc_sys_gen_detail_gtt gtt,
                                     (select /*+ CARDINALITY(cc, 1) */
                                             distinct cc.price_event_id
                                        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
                                       where rownum > 0) cc2
                               where future_retail_id   is NULL
                                 and gtt.price_event_id = cc2.price_event_id)
                       where cs_promo_fr_id = min_fr_id
                       group by price_event_id) dg
               where hg.price_event_id = dg.price_event_id) tol
       where tol.percent > L_sys_option_tolerance;

   cursor C_PE_EXCEED_TOL is
      select CONFLICT_CHECK_ERROR_REC(ab.price_event_id,
                                      ab.future_retail_id,
                                      ab.error_type,
                                      ab.error_string,
                                      ab.cs_promo_fr_id)
        from (--price events that exceeded tolerances
              select cc.price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id
                from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     table(cast(L_exceed_pe_id as OBJ_NUMERIC_ID_TABLE)) exc
               where cc.price_event_id = value(exc)
              union
              --Fetch records backed up in the global variable for price event that exceeded
              select cc.price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id
                from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     table(cast(L_exceed_pe_id as OBJ_NUMERIC_ID_TABLE)) exc
               where cc.price_event_id = value(exc)
              union
              --Errors that are not eligible for SGE - ineligible Bulk_cc_pe_id
              select cc.price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id
               from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                    rpm_bulk_cc_pe_thread pe
              where cc.price_event_id              = pe.price_event_id
                and pe.bulk_cc_pe_id               = LP_bulk_cc_pe_id
                and pe.price_event_type            = LP_price_event_type
                and pe.elig_for_sys_gen_exclusions = 0
                and LP_elig_for_sys_gen_exclusions = 1
              union
              --Errors that are not eligible for SGE - (From Loc Move/Nil)
              select cc.price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id
               from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
              where LP_elig_for_sys_gen_exclusions = 0
              union
              --Errors that are eligible but have already exceeded tolerances in the past.
              select cc.price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id
                from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     rpm_bulk_cc_pe_thread pe,
                     rpm_cc_sys_gen_head_gtt gtt
               where cc.price_event_id              = pe.price_event_id
                 and pe.bulk_cc_pe_id               = LP_bulk_cc_pe_id
                 and pe.price_event_type            = LP_price_event_type
                 and pe.elig_for_sys_gen_exclusions = 1
                 and cc.price_event_id              = gtt.price_event_id
                 and gtt.tolerance_ind              = 0) ab;

   cursor C_PE_NOT_EXCEEED_TOL is
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(L_new_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
       where cc.price_event_id NOT IN (select exc.price_event_id
                                         from table(cast(L_cc_error_non_elg_tbl as CONFLICT_CHECK_ERROR_TBL)) exc)
      union all
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc;

   cursor C_NEW_CC_ERRORS is
      select CONFLICT_CHECK_ERROR_REC(err.price_event_id,
                                      err.future_retail_id,
                                      err.error_type,
                                      err.error_string,
                                      err.cs_promo_fr_id)
        from (select cc.price_event_id,
                     cc.future_retail_id,
                     cc.error_type,
                     cc.error_string,
                     cc.cs_promo_fr_id
                from table(cast(IO_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
              minus
              select sge.price_event_id,
                     sge.future_retail_id,
                     sge.error_type,
                     sge.error_string,
                     sge.cs_promo_fr_id
                from table(cast(LP_non_sge_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) sge) err;

   --Cursor that will get CC records from the previous runs where SGE failed tolerance/eligibility.
   --Plus the new CC records from this run where SGE failed tolerance/eligibility.
   cursor C_TOTAL_CC_OUTPUT is
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(LP_non_sge_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
      union all
      select CONFLICT_CHECK_ERROR_REC(cc.price_event_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string,
                                      cc.cs_promo_fr_id)
        from table(cast(L_cc_error_non_elg_tbl as CONFLICT_CHECK_ERROR_TBL)) cc;

BEGIN

   --The PROCESS_CONFLICTS() function is called in multiple spots within the package.
   --Make sure to only process "NEW" cc records every time this function is called.
   open C_NEW_CC_ERRORS;
   fetch C_NEW_CC_ERRORS BULK COLLECT into L_new_cc_error_tbl;
   close C_NEW_CC_ERRORS;

   --If no new CC errors passed in for this execution, then exit.
   if L_new_cc_error_tbl is NULL or
      L_new_cc_error_tbl.COUNT = 0 then
      return 1;
   end if;

   open C_FIND_EXCEPTION;
   fetch C_FIND_EXCEPTION into L_exception_exist;
   close C_FIND_EXCEPTION;

   if L_exception_exist = 'Y' then
      return 0;
   end if;

   --check if it the process is eligible for SGE
   if LP_elig_for_sys_gen_exclusions = 1 then

      select sys_gen_exclusion_tolerance
        into L_sys_option_tolerance
        from rpm_system_options;

      /*Get distinct price events that are eligible for SG exclusions but are not in the *GTT tables yet.*/
      open C_ELG_PRICE_EVENT_NOT_IN_GTT;
      fetch C_ELG_PRICE_EVENT_NOT_IN_GTT BULK COLLECT into L_cc_error_elg_non_gtt_tbl;
      close C_ELG_PRICE_EVENT_NOT_IN_GTT;

      if L_cc_error_elg_non_gtt_tbl is NOT NULL and
         L_cc_error_elg_non_gtt_tbl.COUNT > 0 then

         --insert price event to RPM_CC_SYS_GEN_HEAD_GTT if the price event does not exist yet. Default tolerance_ind = Pass(1)
         insert into rpm_cc_sys_gen_head_gtt(price_event_id,
                                             price_event_total,
                                             tolerance_ind)
            select pe.price_event_id price_event_id,
                   pe.price_event_total,
                   1
              from (--PROMO item level
                    select /*+ CARDINALITY (cc 10) */
                           1 price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_promo_dtl pd,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im
                     where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                       and pd.promo_dtl_id     = cc.price_event_id
                       and pd.promo_dtl_id     = pdmn.promo_dtl_id
                       and pdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                       and pdmn.item           = im.item
                       and im.tran_level       = im.item_level
                    group by cc.price_event_id
                    union all
                    -- PROMO item parent level
                    select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_promo_dtl pd,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im
                     where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                       and pd.promo_dtl_id     = cc.price_event_id
                       and pd.promo_dtl_id     = pdmn.promo_dtl_id
                       and pdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                       and pdmn.item           = im.item_parent
                       and im.tran_level       = im.item_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    -- PROMO item parent diff level
                    select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_promo_dtl pd,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im
                     where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                       and pd.promo_dtl_id     = cc.price_event_id
                       and pd.promo_dtl_id     = pdmn.promo_dtl_id
                       and pdmn.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
                       and pdmn.item           = im.item_parent
                       and (   pdmn.diff_id = im.diff_1
                            or pdmn.diff_id = im.diff_2
                            or pdmn.diff_id = im.diff_3
                            or pdmn.diff_id = im.diff_4)
                       and im.tran_level       = im.item_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    -- PROMO dept/class/subclass level
                    select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_promo_dtl pd,
                           rpm_promo_dtl_merch_node pdmn,
                           item_master im
                     where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                       and pd.promo_dtl_id     = cc.price_event_id
                       and pd.promo_dtl_id     = pdmn.promo_dtl_id
                       and pdmn.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                   RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                   RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                       and pdmn.dept           = im.dept
                       and im.class            = NVL(pdmn.class, im.class)
                       and im.subclass         = NVL(pdmn.subclass, im.subclass)
                       and im.tran_level       = im.item_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    -- PROMO item list level
                    select COUNT(distinct tran.item) price_event_total,
                           tran.price_event_id
                      from (--tran level items
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_promo_dtl pd,
                                   rpm_promo_dtl_merch_node pdmn,
                                   item_master im,
                                   rpm_promo_dtl_skulist pds
                             where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                               and pd.promo_dtl_id     = cc.price_event_id
                               and pd.promo_dtl_id     = pdmn.promo_dtl_id
                               and pdmn.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                               and pds.price_event_id  = pdmn.promo_dtl_id
                               and pds.skulist         = pdmn.skulist
                               and pds.item_level      = RPM_CONSTANTS.IL_ITEM_LEVEL
                               and im.item             = pds.item
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            --Parent level items
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_promo_dtl pd,
                                   rpm_promo_dtl_merch_node pdmn,
                                   item_master im,
                                   rpm_promo_dtl_skulist pds
                             where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                               and pd.promo_dtl_id     = cc.price_event_id
                               and pd.promo_dtl_id     = pdmn.promo_dtl_id
                               and pdmn.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                               and pds.price_event_id  = pdmn.promo_dtl_id
                               and pds.skulist         = pdmn.skulist
                               and pds.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                               and im.item_parent      = pds.item
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) tran
                    group by tran.price_event_id
                    union all
                    -- PROMO price event item list level
                    select COUNT(distinct tran.item) price_event_total,
                           tran.price_event_id
                      from (--peil tran level items
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_promo_dtl pd,
                                   rpm_promo_dtl_merch_node pdmn,
                                   item_master im,
                                   rpm_merch_list_detail rmld
                             where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                               and pd.promo_dtl_id     = cc.price_event_id
                               and pd.promo_dtl_id     = pdmn.promo_dtl_id
                               and pdmn.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                               and rmld.merch_list_id  = pdmn.price_event_itemlist
                               and im.item             = rmld.item
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            --peil Parent level items
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_promo_dtl pd,
                                   rpm_promo_dtl_merch_node pdmn,
                                   item_master im,
                                   rpm_merch_list_detail rmld
                             where LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                           RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                               and pd.promo_dtl_id     = cc.price_event_id
                               and pd.promo_dtl_id     = pdmn.promo_dtl_id
                               and pdmn.merch_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                               and rmld.merch_list_id  = pdmn.price_event_itemlist
                               and im.item_parent      = rmld.item
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) tran
                    group by tran.price_event_id
                    union all
                    --CLEARANCE item parent level
                    select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_clearance cl,
                           item_master im
                     where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                       and cl.clearance_id     = cc.price_event_id
                       and cl.item             = im.item_parent
                       and im.item_level       = im.tran_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    --CLEARANCE tran item level
                    select /*+ CARDINALITY (cc 10) */
                           1 price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_clearance cl,
                           item_master im
                     where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                       and cl.clearance_id     = cc.price_event_id
                       and cl.item             = im.item
                       and im.item_level       = im.tran_level
                    group by cc.price_event_id
                    union all
                    --CLEARANCE parent diff level
                     select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_clearance cl,
                           item_master im
                     where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                       and cl.clearance_id     = cc.price_event_id
                       and cl.item             = im.item_parent
                       and (   cl.diff_id = im.diff_1
                            or cl.diff_id = im.diff_2
                            or cl.diff_id = im.diff_3
                            or cl.diff_id = im.diff_4)
                       and im.tran_level       = im.item_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    --CLEARANCE item list level
                    select COUNT(distinct tran.item) price_event_total,
                           tran.price_event_id
                      from (--skulist tran level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_clearance cl,
                                   rpm_clearance_skulist sk,
                                   item_master im
                             where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                               and cl.clearance_id     = cc.price_event_id
                               and cl.skulist          is NOT NULL
                               and cl.clearance_id     = sk.price_event_id
                               and cl.skulist          = sk.skulist
                               and sk.item_level       = RPM_CONSTANTS.IL_ITEM_LEVEL
                               and sk.item             = im.item
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            --skulist parent level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_clearance cl,
                                   rpm_clearance_skulist sk,
                                   item_master im
                             where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                               and cl.clearance_id     = cc.price_event_id
                               and cl.skulist          is NOT NULL
                               and cl.clearance_id     = sk.price_event_id
                               and cl.skulist          = sk.skulist
                               and sk.item_level       = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                               and sk.item             = im.item_parent
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) tran
                    group by tran.price_event_id
                    union all
                    --CLEARANCE price event item list level
                    select COUNT(distinct tran.item) price_event_total,
                           tran.price_event_id
                      from (--peil tran level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_clearance cl,
                                   rpm_merch_list_detail rmld,
                                   item_master im
                             where LP_price_event_type     = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                               and cl.clearance_id         = cc.price_event_id
                               and cl.price_event_itemlist is NOT NULL
                               and cl.price_event_itemlist = rmld.merch_list_id
                               and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                               and rmld.item               = im.item
                               and im.item_level           = im.tran_level
                               and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            --peil parent level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_clearance cl,
                                   rpm_merch_list_detail rmld,
                                   item_master im
                             where LP_price_event_type     = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                               and cl.clearance_id         = cc.price_event_id
                               and cl.price_event_itemlist is NOT NULL
                               and cl.price_event_itemlist = rmld.merch_list_id
                               and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                               and rmld.item               = im.item_parent
                               and im.item_level           = im.tran_level
                               and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) tran
                    group by tran.price_event_id
                    union all
                    --PRICE CHANGE item parent level
                    select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_price_change pc,
                           item_master im
                     where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                       and pc.price_change_id  = cc.price_event_id
                       and pc.item             = im.item_parent
                       and im.item_level       = im.tran_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    --PRICE CHANGE tran item level
                    select /*+ CARDINALITY (cc 10) */
                           1 price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_price_change pc,
                           item_master im
                     where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                       and pc.price_change_id  = cc.price_event_id
                       and pc.item             = im.item
                       and im.item_level       = im.tran_level
                    group by cc.price_event_id
                    union all
                    --PRICE CHANGE Parent Diff level
                    select /*+ CARDINALITY (cc 10) */
                           COUNT(distinct im.item) price_event_total,
                           cc.price_event_id
                      from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                           rpm_price_change pc,
                           item_master im
                     where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                       and pc.price_change_id  = cc.price_event_id
                       and pc.item             = im.item_parent
                       and (   pc.diff_id = im.diff_1
                            or pc.diff_id = im.diff_2
                            or pc.diff_id = im.diff_3
                            or pc.diff_id = im.diff_4)
                       and im.tran_level       = im.item_level
                       and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    group by cc.price_event_id
                    union all
                    --PRICE CHANGE item list level
                    select COUNT(distinct tran.item) price_event_total,
                           tran.price_event_id
                      from (--skulist tran level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_price_change pc,
                                   rpm_price_change_skulist sk,
                                   item_master im
                             where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                               and pc.price_change_id  = cc.price_event_id
                               and pc.skulist          is NOT NULL
                               and pc.price_change_id  = sk.price_event_id
                               and pc.skulist          = sk.skulist
                               and sk.item_level       = RPM_CONSTANTS.IL_ITEM_LEVEL
                               and sk.item             = im.item
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            --skulist parent level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_price_change pc,
                                   rpm_clearance_skulist sk,
                                   item_master im
                             where LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                               and pc.price_change_id  = cc.price_event_id
                               and pc.skulist          is NOT NULL
                               and pc.price_change_id  = sk.price_event_id
                               and pc.skulist          = sk.skulist
                               and sk.item_level       = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                               and sk.item             = im.item_parent
                               and im.item_level       = im.tran_level
                               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) tran
                    group by tran.price_event_id
                    union all
                    --PRICE CHANGE price event item list level
                    select COUNT(distinct tran.item) price_event_total,
                           tran.price_event_id
                      from (--peil tran level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_price_change pc,
                                   rpm_merch_list_detail rmld,
                                   item_master im
                             where LP_price_event_type     = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                               and pc.price_change_id      = cc.price_event_id
                               and pc.price_event_itemlist is NOT NULL
                               and pc.price_event_itemlist = rmld.merch_list_id
                               and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                               and rmld.item               = im.item
                               and im.item_level           = im.tran_level
                               and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            --peil parent level item
                            select /*+ CARDINALITY (cc 10) */
                                   im.item,
                                   cc.price_event_id
                              from table(cast(L_cc_error_elg_non_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                                   rpm_price_change pc,
                                   rpm_merch_list_detail rmld,
                                   item_master im
                             where LP_price_event_type     = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                               and pc.price_change_id      = cc.price_event_id
                               and pc.price_event_itemlist is NOT NULL
                               and pc.price_event_itemlist = rmld.merch_list_id
                               and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                               and rmld.item               = im.item_parent
                               and im.item_level           = im.tran_level
                               and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) tran
                    group by tran.price_event_id) pe;

      end if;

      -- Get conflict errors that are 1) eligible for SG exclusions,
      --                              2) exists in the *GTT tables,
      --                              3) and have not failed tolerance yet.

      open C_ELG_PRICE_EVENT_IN_GTT;
      fetch C_ELG_PRICE_EVENT_IN_GTT BULK COLLECT into L_cc_error_elg_in_gtt_tbl;
      close C_ELG_PRICE_EVENT_IN_GTT;

      --insert price event to RPM_CC_SYS_GEN_DETAIL_GTT, including tran item count.
      insert into rpm_cc_sys_gen_detail_gtt(price_event_id,
                                            dept,
                                            item,
                                            diff_id,
                                            cur_hier_level,
                                            max_hier_level,
                                            tran_item_count,
                                            location,
                                            zone_node_type,
                                            future_retail_id,
                                            cs_promo_fr_id,
                                            error_type,
                                            error_string,
                                            action_date)
         select total.price_event_id,
                total.dept,
                total.item,
                total.diff_id,
                total.cur_hier_level,
                total.max_hier_level,
                total.tran_item_count,
                total.location,
                total.zone_node_type,
                total.future_retail_id,
                total.cs_promo_fr_id,
                total.error_type,
                total.error_string,
                total.action_date
           from (--tran level item count non customer segment
                 select /*+ CARDINALITY (cc 10) */
                        1 tran_item_count,
                        cc.price_event_id,
                        rfr.dept,
                        rfr.item,
                        rfr.diff_id,
                        rfr.cur_hier_level,
                        rfr.max_hier_level,
                        rfr.location,
                        rfr.zone_node_type,
                        rfr.future_retail_id,
                        cc.cs_promo_fr_id,
                        cc.error_type,
                        cc.error_string,
                        rfr.action_date
                   from table(cast(L_cc_error_elg_in_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                        rpm_future_retail_gtt rfr,
                        item_master im
                  where cc.cs_promo_fr_id    is NULL
                    and rfr.future_retail_id = cc.future_retail_id
                    and rfr.item             = im.item
                    and rfr.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC)
                  group by cc.price_event_id,
                           rfr.dept,
                           rfr.item,
                           rfr.diff_id,
                           rfr.cur_hier_level,
                           rfr.max_hier_level,
                           rfr.location,
                           rfr.zone_node_type,
                           rfr.future_retail_id,
                           cc.cs_promo_fr_id,
                           cc.error_type,
                           cc.error_string,
                           rfr.action_date
                 union all
                 --non customer segment parent level item count
                 select /*+ CARDINALITY (cc 10) */
                        COUNT(im.item) tran_item_count,
                        cc.price_event_id,
                        rfr.dept,
                        rfr.item,
                        rfr.diff_id,
                        rfr.cur_hier_level,
                        rfr.max_hier_level,
                        rfr.location,
                        rfr.zone_node_type,
                        rfr.future_retail_id,
                        cc.cs_promo_fr_id,
                        cc.error_type,
                        cc.error_string,
                        rfr.action_date
                   from table(cast(L_cc_error_elg_in_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                        rpm_future_retail_gtt rfr,
                        item_master im
                  where cc.cs_promo_fr_id    is NULL
                    and rfr.future_retail_id = cc.future_retail_id
                    and rfr.item             = im.item_parent
                    and rfr.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                  RPM_CONSTANTS.FR_HIER_PARENT_LOC)
                    and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  group by cc.price_event_id,
                           rfr.dept,
                           rfr.item,
                           rfr.diff_id,
                           rfr.cur_hier_level,
                           rfr.max_hier_level,
                           rfr.location,
                           rfr.zone_node_type,
                           rfr.future_retail_id,
                           cc.cs_promo_fr_id,
                           cc.error_type,
                           cc.error_string,
                           rfr.action_date
                 union all
                 --non customer segment parent diff level item count
                 select /*+ CARDINALITY (cc 10) */
                        COUNT(im.item) tran_item_count,
                        cc.price_event_id,
                        rfr.dept,
                        rfr.item,
                        rfr.diff_id,
                        rfr.cur_hier_level,
                        rfr.max_hier_level,
                        rfr.location,
                        rfr.zone_node_type,
                        rfr.future_retail_id,
                        cc.cs_promo_fr_id,
                        cc.error_type,
                        cc.error_string,
                        rfr.action_date
                   from table(cast(L_cc_error_elg_in_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                        rpm_future_retail_gtt rfr,
                        item_master im
                  where cc.cs_promo_fr_id    is NULL
                    and rfr.future_retail_id = cc.future_retail_id
                    and rfr.item             = im.item_parent
                    and rfr.diff_id          = im.diff_1
                    and rfr.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_DIFF_ZONE,
                                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC)
                    and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  group by cc.price_event_id,
                           rfr.dept,
                           rfr.item,
                           rfr.diff_id,
                           rfr.cur_hier_level,
                           rfr.max_hier_level,
                           rfr.location,
                           rfr.zone_node_type,
                           rfr.future_retail_id,
                           cc.cs_promo_fr_id,
                           cc.error_type,
                           cc.error_string,
                           rfr.action_date
                 union all
                 --customer segment tran level item count
                 select /*+ CARDINALITY (cc 10) */
                        1 tran_item_count,
                        cc.price_event_id,
                        rfr.dept,
                        rfr.item,
                        rfr.diff_id,
                        rfr.cur_hier_level,
                        rfr.max_hier_level,
                        rfr.location,
                        rfr.zone_node_type,
                        cc.future_retail_id,
                        cc.cs_promo_fr_id,
                        cc.error_type,
                        cc.error_string,
                        rfr.action_date
                   from table(cast(L_cc_error_elg_in_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                        rpm_cust_segment_promo_fr_gtt rfr,
                        item_master im
                  where cc.cs_promo_fr_id         is NOT NULL
                    and rfr.cust_segment_promo_id = cc.cs_promo_fr_id
                    and rfr.item                  = im.item
                    and rfr.cur_hier_level        IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC)
                  group by cc.price_event_id,
                           rfr.dept,
                           rfr.item,
                           rfr.diff_id,
                           rfr.cur_hier_level,
                           rfr.max_hier_level,
                           rfr.location,
                           rfr.zone_node_type,
                           cc.future_retail_id,
                           cc.cs_promo_fr_id,
                           cc.error_type,
                           cc.error_string,
                           rfr.action_date
                 union all
                 --customer segment parent level item count
                 select /*+ CARDINALITY (cc 10) */
                        COUNT(im.item) tran_item_count,
                        cc.price_event_id,
                        rfr.dept,
                        rfr.item,
                        rfr.diff_id,
                        rfr.cur_hier_level,
                        rfr.max_hier_level,
                        rfr.location,
                        rfr.zone_node_type,
                        cc.future_retail_id,
                        cc.cs_promo_fr_id,
                        cc.error_type,
                        cc.error_string,
                        rfr.action_date
                   from table(cast(L_cc_error_elg_in_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                        rpm_cust_segment_promo_fr_gtt rfr,
                        item_master im
                  where cc.cs_promo_fr_id         is NOT NULL
                    and rfr.cust_segment_promo_id = cc.cs_promo_fr_id
                    and rfr.item                  = im.item_parent
                    and rfr.cur_hier_level        IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                      RPM_CONSTANTS.FR_HIER_PARENT_LOC)
                    and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  group by cc.price_event_id,
                           rfr.dept,
                           rfr.item,
                           rfr.diff_id,
                           rfr.cur_hier_level,
                           rfr.max_hier_level,
                           rfr.location,
                           rfr.zone_node_type,
                           cc.future_retail_id,
                           cc.cs_promo_fr_id,
                           cc.error_type,
                           cc.error_string,
                           rfr.action_date
                 union all
                 --customer segment parent diff level item count
                 select /*+ CARDINALITY (cc 10) */
                        COUNT(im.item) tran_item_count,
                        cc.price_event_id,
                        rfr.dept,
                        rfr.item,
                        rfr.diff_id,
                        rfr.cur_hier_level,
                        rfr.max_hier_level,
                        rfr.location,
                        rfr.zone_node_type,
                        cc.future_retail_id,
                        cc.cs_promo_fr_id,
                        cc.error_type,
                        cc.error_string,
                        rfr.action_date
                   from table(cast(L_cc_error_elg_in_gtt_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                        rpm_cust_segment_promo_fr_gtt rfr,
                        item_master im
                  where cc.cs_promo_fr_id         is NOT NULL
                    and rfr.cust_segment_promo_id = cc.cs_promo_fr_id
                    and rfr.item                  = im.item_parent
                    and rfr.diff_id               = im.diff_1
                    and rfr.cur_hier_level        IN (RPM_CONSTANTS.FR_HIER_DIFF_ZONE,
                                                      RPM_CONSTANTS.FR_HIER_DIFF_LOC)
                    and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  group by cc.price_event_id,
                           rfr.dept,
                           rfr.item,
                           rfr.diff_id,
                           rfr.cur_hier_level,
                           rfr.max_hier_level,
                           rfr.location,
                           rfr.zone_node_type,
                           cc.future_retail_id,
                           cc.cs_promo_fr_id,
                           cc.error_type,
                           cc.error_string,
                           rfr.action_date) total;

      --Cursor to compare, if tolerances have been reached and collect them in to the L_exceed_pe_id.
      if LP_reprocess_ind = 0 then

         open C_TOLERANCE_EXCEED;
         fetch C_TOLERANCE_EXCEED BULK COLLECT into L_exceed_pe_id;
         close C_TOLERANCE_EXCEED;

      end if;

      --update the GTT head to failed for price_events in L_exceed_pe_id.
      update rpm_cc_sys_gen_head_gtt
         set tolerance_ind = 0
       where price_event_id IN (select value(cc)
                                  from table(cast(L_exceed_pe_id as OBJ_NUMERIC_ID_TABLE)) cc);

   end if; -- end of check for SGE eligibility

   -- Cursor to back up price events that have exceeded tolerance.
   -- 1) Get the conflict errors that are not eligible for SG exclusions
   -- 2) Get conflict errors that are eligible for SG exclusions but are already marked failed tolerance in the *GTT tables.
   -- 3) Get conflict errors that have just failed tolerances in the current processing.
   -- 4) Get conflict errors that are backed up in the global variable, for the price event in #3.
   open C_PE_EXCEED_TOL;
   fetch C_PE_EXCEED_TOL BULK COLLECT into L_cc_error_non_elg_tbl;
   close C_PE_EXCEED_TOL;

   --Cursor to backup price events that have NOT exceeded tolerance.
   open C_PE_NOT_EXCEEED_TOL;
   fetch C_PE_NOT_EXCEEED_TOL BULK COLLECT into LP_cc_error_tbl;
   close C_PE_NOT_EXCEEED_TOL;

   --Purge non-eligible price events from the GTT tables.
   if PUSH_BACK_ERROR_ROWS(L_cc_error_non_elg_tbl,
                           1) = 0 then
      return 0;
   end if;

   -- Prepare to output the failed CC errors to the output parameter.
   -- 1) clean up the output parameter
   IO_cc_error_tbl.DELETE;

   -- 2) Fill it with CC records from the previous runs where SGE failed tolerance/eligibility.
   -- 3) Add the new CC records from this run where SGE failed tolerance/eligibility.
   open C_TOTAL_CC_OUTPUT;
   fetch C_TOTAL_CC_OUTPUT BULK COLLECT into IO_cc_error_tbl;
   close C_TOTAL_CC_OUTPUT;

   -- 4) Back up the new total CC records into the Global Collection Variable.
   LP_non_sge_cc_error_tbl := IO_cc_error_tbl;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CONFLICTS;
--------------------------------------------------------------------------------

FUNCTION GENERATE_TIMELINE(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id    IN     NUMBER,
                           I_pe_sequence_id   IN     NUMBER,
                           I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.GENERATE_TIMELINE';

   L_dept          NUMBER(4)            := NULL;
   L_pe_ids        OBJ_NUMERIC_ID_TABLE := NULL;
   L_start_time    TIMESTAMP            := SYSTIMESTAMP;
   L_error_message VARCHAR2(255)        := NULL;

   cursor C_DEPT is
      select distinct dept
        from rpm_cc_sys_gen_detail_gtt;

   cursor C_CHECK_CSPFR_TL is
      select sge_dtl.price_event_id
        from rpm_cc_sys_gen_detail_gtt sge_dtl
       where NOT EXISTS (select 'x'
                           from rpm_cust_segment_promo_fr_gtt gtt,
                                rpm_promo_dtl rpd,
                                rpm_promo_comp rpc
                          where gtt.dept                 = L_dept
                            and sge_dtl.dept             = L_dept
                            and gtt.dept                 = sge_dtl.dept
                            and gtt.item                 = sge_dtl.item
                            and NVL(gtt.diff_id, '-999') = NVL(sge_dtl.diff_id, '-999')
                            and gtt.location             = sge_dtl.location
                            and gtt.zone_node_type       = sge_dtl.zone_node_type
                            and rpd.promo_dtl_id         = sge_dtl.price_event_id
                            and rpc.promo_comp_id        = rpd.promo_comp_id
                            and gtt.customer_type        = rpc.customer_type);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - parent_thread_number: '|| LP_pe_sequence_id ||
                                      ' - thread_number: '|| LP_pe_thread_number);

   for rec IN C_DEPT loop

      L_dept := rec.dept;

      -- This function will generate a new timeline (if it does not exist yet)
      -- at the level of the price event exclusion being approved

      -- For Item/Location Level - Populate the RPM_PRICE_INQUIRY_GTT to prevent some performance issue

      delete rpm_price_inquiry_gtt;

      insert into rpm_price_inquiry_gtt(dept,
                                        class,
                                        subclass,
                                        price_event_id,
                                        item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        loc_type,
                                        fr_zone_id)
      select distinct
             il.dept,
             il.class,
             il.subclass,
             il.price_event_id,
             il.item,
             il.diff_id,
             il.item_parent,
             il.location,
             il.zone_node_type,
             zl.zone_id
        from (select gtt.price_event_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     gtt.item,
                     gtt.diff_id,
                     im.item_parent,
                     rpt.zone_node_id location,
                     rpt.zone_node_type,
                     rmrde.regular_zone_group
                from rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     rpm_bulk_cc_pe_thread rpt,
                     item_master im,
                     rpm_merch_retail_def_expl rmrde,
                     rpm_item_loc ril
               where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpt.parent_thread_number = I_pe_sequence_id
                 and rpt.thread_number        = I_pe_thread_number
                 and gtt.dept                 = L_dept
                 and gtt.price_event_id       = rpt.price_event_id
                 and gtt.price_event_id       = gtth.price_event_id
                 and gtth.tolerance_ind       = 1
                 and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and gtt.item                 = im.item
                 and im.item_level            = im.tran_level
                 and rmrde.dept               = im.dept
                 and rmrde.class              = im.class
                 and rmrde.subclass           = im.subclass
                 and ril.dept                 = im.dept
                 and ril.item                 = im.item
                 and ril.loc                  = rpt.zone_node_id
                 and rownum                   > 0) il,
             (select rz.zone_group_id,
                     rz.zone_id,
                     rzl.location
                from rpm_zone rz,
                     rpm_zone_location rzl
               where rzl.zone_id = rz.zone_id
                 and rownum      > 0) zl
       where il.regular_zone_group = zl.zone_group_id (+)
         and il.location           = zl.location (+)
         and rownum               >= 1;

      merge into rpm_future_retail_gtt target
      using (
         ----
         -- Get all records on rpm_future_retail...
         ----
         select price_event_id,
                dept,
                class,
                subclass,
                item,
                diff_id,
                item_parent,
                zone_node_type,
                location,
                zone_id,
                action_date,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                clear_retail,
                clear_retail_currency,
                clear_uom,
                simple_promo_retail,
                simple_promo_retail_currency,
                simple_promo_uom,
                price_change_id,
                price_change_display_id,
                pc_exception_parent_id,
                pc_change_type,
                pc_change_amount,
                pc_change_currency,
                pc_change_percent,
                pc_change_selling_uom,
                pc_null_multi_ind,
                pc_multi_units,
                pc_multi_unit_retail,
                pc_multi_unit_retail_currency,
                pc_multi_selling_uom,
                pc_price_guide_id,
                clearance_id,
                clearance_display_id,
                clear_mkdn_index,
                clear_start_ind,
                clear_change_type,
                clear_change_amount,
                clear_change_currency,
                clear_change_percent,
                clear_change_selling_uom,
                clear_price_guide_id,
                loc_move_from_zone_id,
                loc_move_to_zone_id,
                location_move_id,
                lock_version,
                on_simple_promo_ind,
                on_complex_promo_ind,
                max_hier_level,
                cur_hier_level,
                future_retail_id
           from (-- price events at the Parent/Diff-Zone level
                 select t.price_event_id,
                        fr.dept,
                        class,
                        subclass,
                        t.item,
                        t.diff_id,
                        NULL item_parent,
                        t.zone_node_type,
                        fr.location,
                        NULL zone_id,
                        action_date,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        multi_units,
                        multi_unit_retail,
                        multi_unit_retail_currency,
                        multi_selling_uom,
                        clear_retail,
                        clear_retail_currency,
                        clear_uom,
                        simple_promo_retail,
                        simple_promo_retail_currency,
                        simple_promo_uom,
                        price_change_id,
                        price_change_display_id,
                        pc_exception_parent_id,
                        pc_change_type,
                        pc_change_amount,
                        pc_change_currency,
                        pc_change_percent,
                        pc_change_selling_uom,
                        pc_null_multi_ind,
                        pc_multi_units,
                        pc_multi_unit_retail,
                        pc_multi_unit_retail_currency,
                        pc_multi_selling_uom,
                        pc_price_guide_id,
                        clearance_id,
                        clearance_display_id,
                        clear_mkdn_index,
                        clear_start_ind,
                        clear_change_type,
                        clear_change_amount,
                        clear_change_currency,
                        clear_change_percent,
                        clear_change_selling_uom,
                        clear_price_guide_id,
                        loc_move_from_zone_id,
                        loc_move_to_zone_id,
                        location_move_id,
                        lock_version,
                        fr.on_simple_promo_ind,
                        fr.on_complex_promo_ind,
                        fr.max_hier_level,
                        RPM_CONSTANTS.FR_HIER_DIFF_ZONE cur_hier_level,
                        fr.future_retail_id
                   from (select distinct
                                im.dept,
                                gtt.price_event_id,
                                gtt.item,
                                gtt.diff_id,
                                rpt.zone_node_id location,
                                rpt.zone_node_type
                           from rpm_cc_sys_gen_detail_gtt gtt,
                                rpm_cc_sys_gen_head_gtt gtth,
                                rpm_bulk_cc_pe_thread rpt,
                                item_master im,
                                rpm_zone_location rzl,
                                rpm_item_loc ril
                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                            and rpt.parent_thread_number = I_pe_sequence_id
                            and rpt.thread_number        = I_pe_thread_number
                            and gtt.dept                 = L_dept
                            and gtt.price_event_id       = rpt.price_event_id
                            and gtt.price_event_id       = gtth.price_event_id
                            and gtth.tolerance_ind       = 1
                            and rpt.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                            and gtt.item                 = im.item_parent
                            and gtt.diff_id              = im.diff_1
                            and ril.dept                 = im.dept
                            and ril.item                 = im.item
                            and ril.loc                  = rzl.location
                            and rzl.zone_id              = rpt.zone_node_id
                            and rownum                  >= 1) t,
                        rpm_future_retail fr
                  where fr.dept           = L_dept
                    and fr.item           = t.item
                    and fr.diff_id        is NULL
                    and fr.location       = t.location
                    and fr.zone_node_type = t.zone_node_type
                    and NOT EXISTS (select 'X'
                                      from rpm_future_retail fr2
                                     where fr2.dept                 = L_dept
                                       and fr2.item                 = t.item
                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                       and fr2.location             = fr.location
                                       and fr2.zone_node_type       = t.zone_node_type)
                 union all
                 -- price events at the parent and location level
                 -- For location that is part of the primary zone group - get it from Parent/Zone Level
                 -- For location that is not part of the primary zone group - it should be available already
                 select t.price_event_id,
                        fr.dept,
                        class,
                        subclass,
                        fr.item,
                        NULL diff_id,
                        NULL item_parent,
                        t.zone_node_type,
                        t.location,
                        t.zone_id,
                        action_date,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        multi_units,
                        multi_unit_retail,
                        multi_unit_retail_currency,
                        multi_selling_uom,
                        clear_retail,
                        clear_retail_currency,
                        clear_uom,
                        simple_promo_retail,
                        simple_promo_retail_currency,
                        simple_promo_uom,
                        price_change_id,
                        price_change_display_id,
                        pc_exception_parent_id,
                        pc_change_type,
                        pc_change_amount,
                        pc_change_currency,
                        pc_change_percent,
                        pc_change_selling_uom,
                        pc_null_multi_ind,
                        pc_multi_units,
                        pc_multi_unit_retail,
                        pc_multi_unit_retail_currency,
                        pc_multi_selling_uom,
                        pc_price_guide_id,
                        clearance_id,
                        clearance_display_id,
                        clear_mkdn_index,
                        clear_start_ind,
                        clear_change_type,
                        clear_change_amount,
                        clear_change_currency,
                        clear_change_percent,
                        clear_change_selling_uom,
                        clear_price_guide_id,
                        loc_move_from_zone_id,
                        loc_move_to_zone_id,
                        location_move_id,
                        lock_version,
                        fr.on_simple_promo_ind,
                        fr.on_complex_promo_ind,
                        fr.max_hier_level,
                        RPM_CONSTANTS.FR_HIER_PARENT_LOC cur_hier_level,
                        fr.future_retail_id
                   from (select distinct
                                im.dept,
                                gtt.price_event_id,
                                im.item,
                                rpt.zone_node_id location,
                                rpt.zone_node_type,
                                rz.zone_id
                           from rpm_cc_sys_gen_detail_gtt gtt,
                                rpm_cc_sys_gen_head_gtt gtth,
                                rpm_bulk_cc_pe_thread rpt,
                                item_master im,
                                rpm_merch_retail_def_expl rmrde,
                                rpm_zone rz,
                                rpm_zone_location rzl,
                                item_master im2,
                                rpm_item_loc ril
                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                            and rpt.parent_thread_number = I_pe_sequence_id
                            and rpt.thread_number        = I_pe_thread_number
                            and gtt.dept                 = L_dept
                            and gtt.price_event_id       = rpt.price_event_id
                            and gtt.price_event_id       = gtth.price_event_id
                            and gtth.tolerance_ind       = 1
                            and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                            and gtt.item                 = im.item
                            and gtt.diff_id              is NULL
                            and im.tran_level            > im.item_level --parent item
                            and rmrde.dept               = im.dept
                            and rmrde.class              = im.class
                            and rmrde.subclass           = im.subclass
                            and rmrde.regular_zone_group = rz.zone_group_id
                            and rz.zone_id               = rzl.zone_id
                            and rzl.location             = rpt.zone_node_id
                            and im2.item_parent          = gtt.item
                            and ril.dept                 = im2.dept
                            and ril.item                 = im2.item
                            and ril.loc                  = rpt.zone_node_id
                            and rownum                  >= 1) t,
                        rpm_future_retail fr
                  where fr.dept           = L_dept
                    and fr.item           = t.item
                    and fr.diff_id        is NULL
                    and fr.location       = t.zone_id
                    and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                    and NOT EXISTS (select 'X'
                                      from rpm_future_retail fr2
                                     where fr2.dept           = L_dept
                                       and fr2.item           = fr.item
                                       and fr2.diff_id        is NULL
                                       and fr2.location       = t.location
                                       and fr2.zone_node_type = t.zone_node_type)
                 union all
                 -- price events at the tran item and zone level
                 -- Get it from the two possible levels (either PD/Z or P/Z) and choose the lowest level available
                 select price_event_id,
                        dept,
                        class,
                        subclass,
                        item,
                        diff_id,
                        item_parent,
                        zone_node_type,
                        location,
                        NULL zone_id,
                        action_date,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        multi_units,
                        multi_unit_retail,
                        multi_unit_retail_currency,
                        multi_selling_uom,
                        clear_retail,
                        clear_retail_currency,
                        clear_uom,
                        simple_promo_retail,
                        simple_promo_retail_currency,
                        simple_promo_uom,
                        price_change_id,
                        price_change_display_id,
                        pc_exception_parent_id,
                        pc_change_type,
                        pc_change_amount,
                        pc_change_currency,
                        pc_change_percent,
                        pc_change_selling_uom,
                        pc_null_multi_ind,
                        pc_multi_units,
                        pc_multi_unit_retail,
                        pc_multi_unit_retail_currency,
                        pc_multi_selling_uom,
                        pc_price_guide_id,
                        clearance_id,
                        clearance_display_id,
                        clear_mkdn_index,
                        clear_start_ind,
                        clear_change_type,
                        clear_change_amount,
                        clear_change_currency,
                        clear_change_percent,
                        clear_change_selling_uom,
                        clear_price_guide_id,
                        loc_move_from_zone_id,
                        loc_move_to_zone_id,
                        location_move_id,
                        lock_version,
                        on_simple_promo_ind,
                        on_complex_promo_ind,
                        max_hier_level,
                        RPM_CONSTANTS.FR_HIER_ITEM_ZONE cur_hier_level,
                        future_retail_id
                   from (-- Get the two possible levels (either PD/Z or P/Z) and choose the one with max rank
                         select fr_level_rank,
                                price_event_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                zone_node_type,
                                location,
                                action_date,
                                selling_retail,
                                selling_retail_currency,
                                selling_uom,
                                multi_units,
                                multi_unit_retail,
                                multi_unit_retail_currency,
                                multi_selling_uom,
                                clear_retail,
                                clear_retail_currency,
                                clear_uom,
                                simple_promo_retail,
                                simple_promo_retail_currency,
                                simple_promo_uom,
                                price_change_id,
                                price_change_display_id,
                                pc_exception_parent_id,
                                pc_change_type,
                                pc_change_amount,
                                pc_change_currency,
                                pc_change_percent,
                                pc_change_selling_uom,
                                pc_null_multi_ind,
                                pc_multi_units,
                                pc_multi_unit_retail,
                                pc_multi_unit_retail_currency,
                                pc_multi_selling_uom,
                                pc_price_guide_id,
                                clearance_id,
                                clearance_display_id,
                                clear_mkdn_index,
                                clear_start_ind,
                                clear_change_type,
                                clear_change_amount,
                                clear_change_currency,
                                clear_change_percent,
                                clear_change_selling_uom,
                                clear_price_guide_id,
                                loc_move_from_zone_id,
                                loc_move_to_zone_id,
                                location_move_id,
                                lock_version,
                                on_simple_promo_ind,
                                on_complex_promo_ind,
                                max_hier_level,
                                future_retail_id,
                                MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                      item,
                                                                      diff_id,
                                                                      location,
                                                                      zone_node_type) max_fr_lvl_rank
                           from (-- Look at the Parent/Zone level - rank 0
                                 select 0 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        class,
                                        subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.zone_node_type,
                                        fr.location,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from (select distinct
                                                im.dept,
                                                gtt.price_event_id,
                                                im.item,
                                                im.diff_1 diff_id,
                                                im.item_parent,
                                                rpt.zone_node_id location,
                                                rpt.zone_node_type
                                           from rpm_cc_sys_gen_detail_gtt gtt,
                                                rpm_cc_sys_gen_head_gtt gtth,
                                                rpm_bulk_cc_pe_thread rpt,
                                                item_master im,
                                                rpm_zone_location rzl,
                                                rpm_item_loc ril
                                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                            and rpt.parent_thread_number = I_pe_sequence_id
                                            and rpt.thread_number        = I_pe_thread_number
                                            and gtt.dept                 = L_dept
                                            and gtt.price_event_id       = rpt.price_event_id
                                            and gtt.price_event_id       = gtth.price_event_id
                                            and gtth.tolerance_ind       = 1
                                            and im.item                  = gtt.item
                                            and im.tran_level            = im.item_level --tran item
                                            and rpt.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                            and rzl.zone_id              = rpt.zone_node_id
                                            and ril.dept                 = im.dept
                                            and ril.item                 = im.item
                                            and ril.loc                  = rzl.location
                                            and rownum                  >= 1) t,
                                        rpm_future_retail fr
                                  where fr.dept           = L_dept
                                    and fr.item           = t.item_parent
                                    and fr.diff_id        is NULL
                                    and fr.location       = t.location
                                    and fr.zone_node_type = t.zone_node_type
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = fr.location
                                                       and fr2.zone_node_type       = t.zone_node_type)
                                 union all
                                 -- Look at the Parent-Diff/Zone level - rank 1
                                 select 1 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        class,
                                        subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.zone_node_type,
                                        fr.location,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from (select distinct
                                                im.dept,
                                                gtt.price_event_id,
                                                gtt.item,
                                                im.diff_1 diff_id,
                                                im.item_parent,
                                                rpt.zone_node_id location,
                                                rpt.zone_node_type
                                           from rpm_cc_sys_gen_detail_gtt gtt,
                                                rpm_cc_sys_gen_head_gtt gtth,
                                                rpm_bulk_cc_pe_thread rpt,
                                                item_master im,
                                                rpm_zone_location rzl,
                                                rpm_item_loc ril
                                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                            and rpt.parent_thread_number = I_pe_sequence_id
                                            and rpt.thread_number        = I_pe_thread_number
                                            and gtt.dept                 = L_dept
                                            and gtt.price_event_id       = rpt.price_event_id
                                            and gtt.price_event_id       = gtth.price_event_id
                                            and gtth.tolerance_ind       = 1
                                            and im.item                  = gtt.item
                                            and im.tran_level            = im.item_level --tran item
                                            and rpt.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                            and rzl.zone_id              = rpt.zone_node_id
                                            and ril.dept                 = im.dept
                                            and ril.item                 = im.item
                                            and ril.loc                  = rzl.location
                                            and rownum                  >= 1) t,
                                        rpm_future_retail fr
                                  where fr.dept                 = L_dept
                                    and fr.item                 = t.item_parent
                                    and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                    and fr.location             = t.location
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = fr.location
                                                       and fr2.zone_node_type       = t.zone_node_type)))
                  where fr_level_rank = max_fr_lvl_rank
                 union all
                 -- price events at the Parent-Diff and Loc level
                 -- Get the three possible levels (PD/Z, P/L or P/Z) and choose the lowest level available
                 select price_event_id,
                        dept,
                        class,
                        subclass,
                        item,
                        diff_id,
                        NULL item_parent,
                        zone_node_type,
                        location,
                        zone_id,
                        action_date,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        multi_units,
                        multi_unit_retail,
                        multi_unit_retail_currency,
                        multi_selling_uom,
                        clear_retail,
                        clear_retail_currency,
                        clear_uom,
                        simple_promo_retail,
                        simple_promo_retail_currency,
                        simple_promo_uom,
                        price_change_id,
                        price_change_display_id,
                        pc_exception_parent_id,
                        pc_change_type,
                        pc_change_amount,
                        pc_change_currency,
                        pc_change_percent,
                        pc_change_selling_uom,
                        pc_null_multi_ind,
                        pc_multi_units,
                        pc_multi_unit_retail,
                        pc_multi_unit_retail_currency,
                        pc_multi_selling_uom,
                        pc_price_guide_id,
                        clearance_id,
                        clearance_display_id,
                        clear_mkdn_index,
                        clear_start_ind,
                        clear_change_type,
                        clear_change_amount,
                        clear_change_currency,
                        clear_change_percent,
                        clear_change_selling_uom,
                        clear_price_guide_id,
                        loc_move_from_zone_id,
                        loc_move_to_zone_id,
                        location_move_id,
                        lock_version,
                        on_simple_promo_ind,
                        on_complex_promo_ind,
                        max_hier_level,
                        RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level,
                        future_retail_id
                   from (-- Get the three possible levels (PD/Z, P/L or P/Z) and choose the one with max rank
                         select fr_level_rank,
                                price_event_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                zone_node_type,
                                location,
                                zone_id,
                                action_date,
                                selling_retail,
                                selling_retail_currency,
                                selling_uom,
                                multi_units,
                                multi_unit_retail,
                                multi_unit_retail_currency,
                                multi_selling_uom,
                                clear_retail,
                                clear_retail_currency,
                                clear_uom,
                                simple_promo_retail,
                                simple_promo_retail_currency,
                                simple_promo_uom,
                                price_change_id,
                                price_change_display_id,
                                pc_exception_parent_id,
                                pc_change_type,
                                pc_change_amount,
                                pc_change_currency,
                                pc_change_percent,
                                pc_change_selling_uom,
                                pc_null_multi_ind,
                                pc_multi_units,
                                pc_multi_unit_retail,
                                pc_multi_unit_retail_currency,
                                pc_multi_selling_uom,
                                pc_price_guide_id,
                                clearance_id,
                                clearance_display_id,
                                clear_mkdn_index,
                                clear_start_ind,
                                clear_change_type,
                                clear_change_amount,
                                clear_change_currency,
                                clear_change_percent,
                                clear_change_selling_uom,
                                clear_price_guide_id,
                                loc_move_from_zone_id,
                                loc_move_to_zone_id,
                                location_move_id,
                                lock_version,
                                on_simple_promo_ind,
                                on_complex_promo_ind,
                                max_hier_level,
                                future_retail_id,
                                MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                      item,
                                                                      diff_id,
                                                                      location,
                                                                      zone_node_type) max_fr_lvl_rank
                           from (-- Look at the Parent/Zone level - rank 0
                                 -- Only available when Location is part of the Primary Zone Group
                                 select 0 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        class,
                                        subclass,
                                        t.item,
                                        t.diff_id,
                                        t.zone_node_type,
                                        t.location,
                                        t.zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from (select distinct
                                                im.dept,
                                                gtt.price_event_id,
                                                im.item,
                                                gtt.diff_id,
                                                rpt.zone_node_id location,
                                                rpt.zone_node_type,
                                                rz.zone_id
                                           from rpm_cc_sys_gen_detail_gtt gtt,
                                                rpm_cc_sys_gen_head_gtt gtth,
                                                rpm_bulk_cc_pe_thread rpt,
                                                item_master im,
                                                rpm_merch_retail_def_expl rmrde,
                                                rpm_zone rz,
                                                rpm_zone_location rzl,
                                                item_master im2,
                                                rpm_item_loc ril
                                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                            and rpt.parent_thread_number = I_pe_sequence_id
                                            and rpt.thread_number        = I_pe_thread_number
                                            and gtt.dept                 = L_dept
                                            and gtt.price_event_id       = rpt.price_event_id
                                            and gtt.price_event_id       = gtth.price_event_id
                                            and gtth.tolerance_ind       = 1
                                            and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                            and im.item                  = gtt.item
                                            and im.tran_level            > im.item_level --parent diff item
                                            and gtt.diff_id              is NOT NULL
                                            and rmrde.dept               = im.dept
                                            and rmrde.class              = im.class
                                            and rmrde.subclass           = im.subclass
                                            and rmrde.regular_zone_group = rz.zone_group_id
                                            and rz.zone_id               = rzl.zone_id
                                            and rzl.location             = rpt.zone_node_id
                                            and im2.item_parent          = gtt.item
                                            and im2.diff_1               = gtt.diff_id
                                            and ril.dept                 = im2.dept
                                            and ril.item                 = im2.item
                                            and ril.loc                  = rpt.zone_node_id
                                            and rownum                  >= 1) t,
                                        rpm_future_retail fr
                                  where fr.dept           = L_dept
                                    and fr.item           = t.item
                                    and fr.diff_id        is NULL
                                    and fr.location       = t.zone_id
                                    and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.zone_node_type)
                                 union all
                                 -- Look at the Parent-Diff/Zone level - rank 1
                                 -- Only available when Location is part of the Primary Zone Group
                                 select 1 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        class,
                                        subclass,
                                        t.item,
                                        t.diff_id,
                                        t.zone_node_type,
                                        t.location,
                                        t.zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from (select distinct
                                                im.dept,
                                                gtt.price_event_id,
                                                gtt.item,
                                                gtt.diff_id,
                                                rpt.zone_node_id location,
                                                rpt.zone_node_type,
                                                rz.zone_id
                                           from rpm_cc_sys_gen_detail_gtt gtt,
                                                rpm_cc_sys_gen_head_gtt gtth,
                                                rpm_bulk_cc_pe_thread rpt,
                                                item_master im,
                                                rpm_merch_retail_def_expl rmrde,
                                                rpm_zone rz,
                                                rpm_zone_location rzl,
                                                rpm_item_loc ril,
                                                item_master im2
                                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                            and rpt.parent_thread_number = I_pe_sequence_id
                                            and rpt.thread_number        = I_pe_thread_number
                                            and gtt.dept                 = L_dept
                                            and gtt.price_event_id       = rpt.price_event_id
                                            and gtt.price_event_id       = gtth.price_event_id
                                            and gtth.tolerance_ind       = 1
                                            and gtt.item                 = im.item
                                            and im.tran_level            > im.item_level --parent diff item
                                            and gtt.diff_id              is NOT NULL
                                            and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                            and rmrde.dept               = im.dept
                                            and rmrde.class              = im.class
                                            and rmrde.subclass           = im.subclass
                                            and rmrde.regular_zone_group = rz.zone_group_id
                                            and rz.zone_id               = rzl.zone_id
                                            and rzl.location             = rpt.zone_node_id
                                            and im2.item_parent          = gtt.item
                                            and im2.diff_1               = gtt.diff_id
                                            and ril.dept                 = im2.dept
                                            and ril.item                 = im2.item
                                            and ril.loc                  = rpt.zone_node_id
                                            and rownum                  >= 1) t,
                                        rpm_future_retail fr
                                  where fr.dept                 = L_dept
                                    and fr.item                 = t.item
                                    and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                    and fr.location             = t.zone_id
                                    and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.zone_node_type)
                                 union all
                                 -- Look at the Parent/Loc level - rank 1 (same rank as PD/Z - can't have both and be here)
                                 -- Available for both Location is part of the PZG and Location is NOT part of PZG
                                 select 1 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        class,
                                        subclass,
                                        t.item,
                                        t.diff_id,
                                        t.zone_node_type,
                                        t.location,
                                        fr.zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from (select distinct
                                                im.dept,
                                                rpt.price_event_id,
                                                gtt.item,
                                                gtt.diff_id,
                                                rpt.zone_node_id location,
                                                rpt.zone_node_type
                                           from rpm_cc_sys_gen_detail_gtt gtt,
                                                rpm_cc_sys_gen_head_gtt gtth,
                                                rpm_bulk_cc_pe_thread rpt,
                                                item_master im,
                                                rpm_item_loc ril
                                          where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                            and rpt.parent_thread_number = I_pe_sequence_id
                                            and rpt.thread_number        = I_pe_thread_number
                                            and gtt.dept                 = L_dept
                                            and gtt.price_event_id       = rpt.price_event_id
                                            and gtt.price_event_id       = gtth.price_event_id
                                            and gtth.tolerance_ind       = 1
                                            and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                            and im.item_parent           = gtt.item
                                            and im.diff_1                = gtt.diff_id
                                            and ril.dept                 = im.dept
                                            and ril.item                 = im.item
                                            and ril.loc                  = rpt.zone_node_id
                                            and rownum                  >= 1) t,
                                        rpm_future_retail fr
                                  where fr.dept           = L_dept
                                    and fr.item           = t.item
                                    and fr.diff_id        is NULL
                                    and fr.location       = t.location
                                    and fr.zone_node_type = t.zone_node_type
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.zone_node_type)))
                  where fr_level_rank = max_fr_lvl_rank
                 union all
                 -- price events at the item and location level
                 -- Get all levels above I/L (I/Z, PD/L, PD/Z, P/L or P/Z) and choose the lowest level available
                 select price_event_id,
                        dept,
                        class,
                        subclass,
                        item,
                        diff_id,
                        item_parent,
                        zone_node_type,
                        location,
                        zone_id,
                        action_date,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        multi_units,
                        multi_unit_retail,
                        multi_unit_retail_currency,
                        multi_selling_uom,
                        clear_retail,
                        clear_retail_currency,
                        clear_uom,
                        simple_promo_retail,
                        simple_promo_retail_currency,
                        simple_promo_uom,
                        price_change_id,
                        price_change_display_id,
                        pc_exception_parent_id,
                        pc_change_type,
                        pc_change_amount,
                        pc_change_currency,
                        pc_change_percent,
                        pc_change_selling_uom,
                        pc_null_multi_ind,
                        pc_multi_units,
                        pc_multi_unit_retail,
                        pc_multi_unit_retail_currency,
                        pc_multi_selling_uom,
                        pc_price_guide_id,
                        clearance_id,
                        clearance_display_id,
                        clear_mkdn_index,
                        clear_start_ind,
                        clear_change_type,
                        clear_change_amount,
                        clear_change_currency,
                        clear_change_percent,
                        clear_change_selling_uom,
                        clear_price_guide_id,
                        loc_move_from_zone_id,
                        loc_move_to_zone_id,
                        location_move_id,
                        lock_version,
                        on_simple_promo_ind,
                        on_complex_promo_ind,
                        max_hier_level,
                        RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                        future_retail_id
                   from (-- Get all levels above I/L (I/Z, PD/L, PD/Z, P/L or P/Z) and choose the one with max rank
                         select fr_level_rank,
                                price_event_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                zone_node_type,
                                location,
                                zone_id,
                                action_date,
                                selling_retail,
                                selling_retail_currency,
                                selling_uom,
                                multi_units,
                                multi_unit_retail,
                                multi_unit_retail_currency,
                                multi_selling_uom,
                                clear_retail,
                                clear_retail_currency,
                                clear_uom,
                                simple_promo_retail,
                                simple_promo_retail_currency,
                                simple_promo_uom,
                                price_change_id,
                                price_change_display_id,
                                pc_exception_parent_id,
                                pc_change_type,
                                pc_change_amount,
                                pc_change_currency,
                                pc_change_percent,
                                pc_change_selling_uom,
                                pc_null_multi_ind,
                                pc_multi_units,
                                pc_multi_unit_retail,
                                pc_multi_unit_retail_currency,
                                pc_multi_selling_uom,
                                pc_price_guide_id,
                                clearance_id,
                                clearance_display_id,
                                clear_mkdn_index,
                                clear_start_ind,
                                clear_change_type,
                                clear_change_amount,
                                clear_change_currency,
                                clear_change_percent,
                                clear_change_selling_uom,
                                clear_price_guide_id,
                                loc_move_from_zone_id,
                                loc_move_to_zone_id,
                                location_move_id,
                                lock_version,
                                on_simple_promo_ind,
                                on_complex_promo_ind,
                                max_hier_level,
                                future_retail_id,
                                MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                      item,
                                                                      diff_id,
                                                                      location,
                                                                      zone_node_type) max_fr_lvl_rank
                           from (-- Look at the Parent/Zone level - rank 0
                                 -- Only possible if Location is part of PZG
                                 select 0 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        fr.class,
                                        fr.subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.loc_type zone_node_type,
                                        t.location,
                                        t.fr_zone_id zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from rpm_price_inquiry_gtt t,
                                        rpm_future_retail fr
                                  where fr.dept           = L_dept
                                    and fr.item           = t.item_parent
                                    and fr.diff_id        is NULL
                                    and fr.location       = t.fr_zone_id
                                    and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.loc_type)
                                 union all
                                 -- Look at the Parent-Diff/Zone level - rank 1
                                 -- Only possible if Location is part of PZG
                                 select 1 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        fr.class,
                                        fr.subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.loc_type zone_node_type,
                                        t.location,
                                        t.fr_zone_id zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from rpm_price_inquiry_gtt t,
                                        rpm_future_retail fr
                                  where fr.dept                 = L_dept
                                    and fr.item                 = t.item_parent
                                    and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                    and fr.location             = t.fr_zone_id
                                    and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.loc_type)
                                 union all
                                 -- Look at the Parent/Loc level - rank 1 (same rank as PD/Z - can't have both and be here)
                                 -- Location can be either part of the PZG or NOT
                                 select 1 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        fr.class,
                                        fr.subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.loc_type zone_node_type,
                                        t.location,
                                        t.fr_zone_id zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from rpm_price_inquiry_gtt t,
                                        rpm_future_retail fr
                                  where fr.dept           = L_dept
                                    and fr.item           = t.item_parent
                                    and fr.diff_id        is NULL
                                    and fr.location       = t.location
                                    and fr.zone_node_type = t.loc_type
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.loc_type)
                                 union all
                                 -- Look at the Item/Zone level - rank 2
                                 -- Only possible if Location is part of PZG
                                 select 2 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        fr.class,
                                        fr.subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.loc_type zone_node_type,
                                        t.location,
                                        t.fr_zone_id zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from rpm_price_inquiry_gtt t,
                                        rpm_future_retail fr
                                  where fr.dept                 = L_dept
                                    and fr.item                 = t.item
                                    and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                    and fr.location             = t.fr_zone_id
                                    and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.loc_type)
                                 union all
                                 -- Look at the Parent-Diff/Loc level - rank 2 (same rank as I/Z - can't have both and be here)
                                 -- Location can be either part of the PZG or NOT
                                 select 2 fr_level_rank,
                                        t.price_event_id,
                                        fr.dept,
                                        fr.class,
                                        fr.subclass,
                                        t.item,
                                        t.diff_id,
                                        t.item_parent,
                                        t.loc_type zone_node_type,
                                        t.location,
                                        t.fr_zone_id zone_id,
                                        action_date,
                                        selling_retail,
                                        selling_retail_currency,
                                        selling_uom,
                                        multi_units,
                                        multi_unit_retail,
                                        multi_unit_retail_currency,
                                        multi_selling_uom,
                                        clear_retail,
                                        clear_retail_currency,
                                        clear_uom,
                                        simple_promo_retail,
                                        simple_promo_retail_currency,
                                        simple_promo_uom,
                                        price_change_id,
                                        price_change_display_id,
                                        pc_exception_parent_id,
                                        pc_change_type,
                                        pc_change_amount,
                                        pc_change_currency,
                                        pc_change_percent,
                                        pc_change_selling_uom,
                                        pc_null_multi_ind,
                                        pc_multi_units,
                                        pc_multi_unit_retail,
                                        pc_multi_unit_retail_currency,
                                        pc_multi_selling_uom,
                                        pc_price_guide_id,
                                        clearance_id,
                                        clearance_display_id,
                                        clear_mkdn_index,
                                        clear_start_ind,
                                        clear_change_type,
                                        clear_change_amount,
                                        clear_change_currency,
                                        clear_change_percent,
                                        clear_change_selling_uom,
                                        clear_price_guide_id,
                                        loc_move_from_zone_id,
                                        loc_move_to_zone_id,
                                        location_move_id,
                                        lock_version,
                                        fr.on_simple_promo_ind,
                                        fr.on_complex_promo_ind,
                                        fr.max_hier_level,
                                        fr.future_retail_id
                                   from rpm_price_inquiry_gtt t,
                                        rpm_future_retail fr
                                  where fr.dept                 = L_dept
                                    and fr.item                 = t.item_parent
                                    and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                                    and fr.location             = t.location
                                    and fr.zone_node_type       = t.loc_type
                                    and NOT EXISTS (select 'X'
                                                      from rpm_future_retail fr2
                                                     where fr2.dept                 = L_dept
                                                       and fr2.item                 = t.item
                                                       and NVL(fr2.diff_id, '-999') = NVL(t.diff_id, '-999')
                                                       and fr2.location             = t.location
                                                       and fr2.zone_node_type       = t.loc_type)))
                          where fr_level_rank = max_fr_lvl_rank)) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date)
      when NOT MATCHED then
         insert(price_event_id,
                future_retail_id,
                dept,
                class,
                subclass,
                item,
                diff_id,
                item_parent,
                zone_node_type,
                location,
                zone_id,
                action_date,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                clear_retail,
                clear_retail_currency,
                clear_uom,
                simple_promo_retail,
                simple_promo_retail_currency,
                simple_promo_uom,
                price_change_id,
                price_change_display_id,
                pc_exception_parent_id,
                pc_change_type,
                pc_change_amount,
                pc_change_currency,
                pc_change_percent,
                pc_change_selling_uom,
                pc_null_multi_ind,
                pc_multi_units,
                pc_multi_unit_retail,
                pc_multi_unit_retail_currency,
                pc_multi_selling_uom,
                pc_price_guide_id,
                clearance_id,
                clearance_display_id,
                clear_mkdn_index,
                clear_start_ind,
                clear_change_type,
                clear_change_amount,
                clear_change_currency,
                clear_change_percent,
                clear_change_selling_uom,
                clear_price_guide_id,
                loc_move_from_zone_id,
                loc_move_to_zone_id,
                location_move_id,
                lock_version,
                on_simple_promo_ind,
                on_complex_promo_ind,
                max_hier_level,
                cur_hier_level,
                original_fr_id,
                step_identifier)
         values(source.price_event_id,
                RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
                source.dept,
                source.class,
                source.subclass,
                source.item,
                source.diff_id,
                source.item_parent,
                source.zone_node_type,
                source.location,
                source.zone_id,
                source.action_date,
                source.selling_retail,
                source.selling_retail_currency,
                source.selling_uom,
                source.multi_units,
                source.multi_unit_retail,
                source.multi_unit_retail_currency,
                source.multi_selling_uom,
                source.clear_retail,
                source.clear_retail_currency,
                source.clear_uom,
                source.simple_promo_retail,
                source.simple_promo_retail_currency,
                source.simple_promo_uom,
                source.price_change_id,
                source.price_change_display_id,
                source.pc_exception_parent_id,
                source.pc_change_type,
                source.pc_change_amount,
                source.pc_change_currency,
                source.pc_change_percent,
                source.pc_change_selling_uom,
                source.pc_null_multi_ind,
                source.pc_multi_units,
                source.pc_multi_unit_retail,
                source.pc_multi_unit_retail_currency,
                source.pc_multi_selling_uom,
                source.pc_price_guide_id,
                source.clearance_id,
                source.clearance_display_id,
                source.clear_mkdn_index,
                source.clear_start_ind,
                source.clear_change_type,
                source.clear_change_amount,
                source.clear_change_currency,
                source.clear_change_percent,
                source.clear_change_selling_uom,
                source.clear_price_guide_id,
                source.loc_move_from_zone_id,
                source.loc_move_to_zone_id,
                source.location_move_id,
                source.lock_version,
                source.on_simple_promo_ind,
                source.on_complex_promo_ind,
                source.max_hier_level,
                source.cur_hier_level,
                source.future_retail_id,
                RPM_CONSTANTS.CAPT_GTT_RFR_SQL_GEN_TL);

         merge into rpm_cust_segment_promo_fr_gtt target
         using (select price_event_id,
                       to_item item,
                       to_diff_id diff_id,
                       to_item_parent item_parent,
                       to_zone_node_type zone_node_type,
                       to_location location,
                       to_zone_id zone_id,
                       action_date,
                       customer_type,
                       dept,
                       promo_retail,
                       promo_retail_currency,
                       promo_uom,
                       complex_promo_ind,
                       max_hier_level,
                       to_cur_hier_level cur_hier_level,
                       cust_segment_promo_id
                  from (select /*+ ORDERED */
                               t.price_event_id,
                               t.to_item,
                               t.to_diff_id,
                               t.to_item_parent,
                               t.to_zone_node_type,
                               t.to_location,
                               t.to_zone_id,
                               cspfr.action_date,
                               cspfr.customer_type,
                               cspfr.dept,
                               cspfr.promo_retail,
                               cspfr.promo_retail_currency,
                               cspfr.promo_uom,
                               cspfr.complex_promo_ind,
                               NULL,
                               cspfr.max_hier_level,
                               t.to_cur_hier_level,
                               cspfr.cust_segment_promo_id,
                               t.rank,
                               MAX(t.rank) OVER (PARTITION BY t.price_event_id,
                                                              cspfr.customer_type,
                                                              t.to_item,
                                                              NVL(t.to_diff_id, '-999'),
                                                              t.to_location,
                                                              t.to_zone_node_type) max_rank
                          from (-- Parent Diff Zone price Events
                                -- Look at the PZ level
                                select distinct
                                       0 rank,
                                       im.dept,
                                       gtt.price_event_id,
                                       gtt.item                        from_item,
                                       NULL                            from_diff_id,
                                       rpt.zone_node_id                from_location,
                                       rpt.zone_node_type              from_zone_node_type,
                                       gtt.item                        to_item,
                                       gtt.diff_id                     to_diff_id,
                                       NULL                            to_item_parent,
                                       rpt.zone_node_id                to_location,
                                       rpt.zone_node_type              to_zone_node_type,
                                       NULL                            to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level
                                  from rpm_cc_sys_gen_detail_gtt gtt,
                                       rpm_cc_sys_gen_head_gtt gtth,
                                       rpm_bulk_cc_pe_thread rpt,
                                       item_master im,
                                       rpm_zone_location rzl,
                                       rpm_item_loc ril
                                 where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                   and rpt.parent_thread_number = I_pe_sequence_id
                                   and rpt.thread_number        = I_pe_thread_number
                                   and gtt.dept                 = L_dept
                                   and gtt.price_event_id       = rpt.price_event_id
                                   and gtt.price_event_id       = gtth.price_event_id
                                   and gtth.tolerance_ind       = 1
                                   and rpt.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                   and gtt.item                 = im.item_parent
                                   and gtt.diff_id              = im.diff_1
                                   and ril.dept                 = im.dept
                                   and ril.item                 = im.item
                                   and ril.loc                  = rzl.location
                                   and rzl.zone_id              = rpt.zone_node_id
                                   and rownum                  >= 1
                                union all
                                -- Parent Loc price Events
                                -- Look at the PZ level
                                select distinct
                                       0 rank,
                                       im.dept,
                                       gtt.price_event_id,
                                       gtt.item                          from_item,
                                       NULL                              from_diff_id,
                                       rpt.zone_node_id                  from_location,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                       gtt.item                          to_item,
                                       NULL                              to_diff_id,
                                       NULL                              to_item_parent,
                                       rpt.zone_node_id                  to_location,
                                       rpt.zone_node_type                to_zone_node_type,
                                       NULL                              to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level
                                  from rpm_cc_sys_gen_detail_gtt gtt,
                                       rpm_cc_sys_gen_head_gtt gtth,
                                       rpm_bulk_cc_pe_thread rpt,
                                       item_master im,
                                       rpm_merch_retail_def_expl rmrde,
                                       rpm_zone rz,
                                       rpm_zone_location rzl,
                                       item_master im2,
                                       rpm_item_loc ril
                                 where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                   and rpt.parent_thread_number = I_pe_sequence_id
                                   and rpt.thread_number        = I_pe_thread_number
                                   and gtt.dept                 = L_dept
                                   and gtt.price_event_id       = rpt.price_event_id
                                   and gtt.price_event_id       = gtth.price_event_id
                                   and gtth.tolerance_ind       = 1
                                   and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                   and gtt.item                 = im.item
                                   and gtt.diff_id              is NULL
                                   and im.tran_level            > im.item_level --parent item
                                   and rmrde.dept               = im.dept
                                   and rmrde.class              = im.class
                                   and rmrde.subclass           = im.subclass
                                   and rmrde.regular_zone_group = rz.zone_group_id
                                   and rz.zone_id               = rzl.zone_id
                                   and rzl.location             = rpt.zone_node_id
                                   and im2.item_parent          = gtt.item
                                   and ril.dept                 = im2.dept
                                   and ril.item                 = im2.item
                                   and ril.loc                  = rpt.zone_node_id
                                   and rownum                  >= 1
                                union all
                                -- Item Zone price Events
                                -- Look at the PZ level
                                select distinct
                                       0 rank,
                                       im.dept,
                                       gtt.price_event_id,
                                       im.item_parent                  from_item,
                                       NULL                            from_diff_id,
                                       rpt.zone_node_id                from_location,
                                       rpt.zone_node_type              from_zone_node_type,
                                       im.item                         to_item,
                                       im.diff_1                       to_diff_id,
                                       im.item_parent                  to_item_parent,
                                       rpt.zone_node_id                to_location,
                                       rpt.zone_node_type              to_zone_node_type,
                                       NULL                            to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                                  from rpm_cc_sys_gen_detail_gtt gtt,
                                       rpm_cc_sys_gen_head_gtt gtth,
                                       rpm_bulk_cc_pe_thread rpt,
                                       item_master im,
                                       rpm_zone_location rzl,
                                       rpm_item_loc ril
                                 where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                   and rpt.parent_thread_number = I_pe_sequence_id
                                   and rpt.thread_number        = I_pe_thread_number
                                   and gtt.dept                 = L_dept
                                   and gtt.price_event_id       = rpt.price_event_id
                                   and gtt.price_event_id       = gtth.price_event_id
                                   and gtth.tolerance_ind       = 1
                                   and im.item                  = gtt.item
                                   and im.tran_level            = im.item_level --tran item
                                   and rpt.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                   and rzl.zone_id              = rpt.zone_node_id
                                   and ril.dept                 = im.dept
                                   and ril.item                 = im.item
                                   and ril.loc                  = rzl.location
                                   and rownum                  >= 1
                                union all
                                -- Look at the PDZ level
                                select distinct
                                       1 rank,
                                       im.dept,
                                       gtt.price_event_id,
                                       im.item_parent                  from_item,
                                       im.diff_1                       from_diff_id,
                                       rpt.zone_node_id                from_location,
                                       rpt.zone_node_type              from_zone_node_type,
                                       gtt.item                        to_item,
                                       im.diff_1                       to_diff_id,
                                       im.item_parent                  to_item_parent,
                                       rpt.zone_node_id                to_location,
                                       rpt.zone_node_type              to_zone_node_type,
                                       NULL                            to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                                  from rpm_cc_sys_gen_detail_gtt gtt,
                                       rpm_cc_sys_gen_head_gtt gtth,
                                       rpm_bulk_cc_pe_thread rpt,
                                       item_master im,
                                       rpm_zone_location rzl,
                                       rpm_item_loc ril
                                 where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                   and rpt.parent_thread_number = I_pe_sequence_id
                                   and rpt.thread_number        = I_pe_thread_number
                                   and gtt.dept                 = L_dept
                                   and gtt.price_event_id       = rpt.price_event_id
                                   and gtt.price_event_id       = gtth.price_event_id
                                   and gtth.tolerance_ind       = 1
                                   and im.item                  = gtt.item
                                   and im.tran_level            = im.item_level --tran item
                                   and rpt.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                   and rzl.zone_id              = rpt.zone_node_id
                                   and ril.dept                 = im.dept
                                   and ril.item                 = im.item
                                   and ril.loc                  = rzl.location
                                   and rownum                  >= 1
                                union all
                                -- Parent Diff Loc price Events
                                -- Look at the PZ level
                                select distinct
                                       0 rank,
                                       im.dept,
                                       gtt.price_event_id,
                                       im.item                           from_item,
                                       NULL                              from_diff_id,
                                       rz.zone_id                        from_location,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                       im.item                           to_item,
                                       gtt.diff_id                       to_diff_id,
                                       NULL                              to_item_parent,
                                       rpt.zone_node_id                  to_location,
                                       rpt.zone_node_type                to_zone_node_type,
                                       rz.zone_id                        to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                                  from rpm_cc_sys_gen_detail_gtt gtt,
                                       rpm_cc_sys_gen_head_gtt gtth,
                                       rpm_bulk_cc_pe_thread rpt,
                                       item_master im,
                                       rpm_merch_retail_def_expl rmrde,
                                       rpm_zone rz,
                                       rpm_zone_location rzl,
                                       item_master im2,
                                       rpm_item_loc ril
                                 where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                   and rpt.parent_thread_number = I_pe_sequence_id
                                   and rpt.thread_number        = I_pe_thread_number
                                   and gtt.dept                 = L_dept
                                   and gtt.price_event_id       = rpt.price_event_id
                                   and gtt.price_event_id       = gtth.price_event_id
                                   and gtth.tolerance_ind       = 1
                                   and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                   and im.item                  = gtt.item
                                   and im.tran_level            > im.item_level --parent diff item
                                   and gtt.diff_id              is NOT NULL
                                   and rmrde.dept               = im.dept
                                   and rmrde.class              = im.class
                                   and rmrde.subclass           = im.subclass
                                   and rmrde.regular_zone_group = rz.zone_group_id
                                   and rz.zone_id               = rzl.zone_id
                                   and rzl.location             = rpt.zone_node_id
                                   and im2.item_parent          = gtt.item
                                   and im2.diff_1               = gtt.diff_id
                                   and ril.dept                 = im2.dept
                                   and ril.item                 = im2.item
                                   and ril.loc                  = rpt.zone_node_id
                                   and rownum                  >= 1
                                union all
                                -- Look at the PDZ level
                                select distinct
                                       1 rank,
                                       im.dept,
                                       gtt.price_event_id,
                                       im.item                           from_item,
                                       gtt.diff_id                       from_diff_id,
                                       rz.zone_id                        from_location,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                       im.item                           to_item,
                                       gtt.diff_id                       to_diff_id,
                                       NULL                              to_item_parent,
                                       rpt.zone_node_id                  to_location,
                                       rpt.zone_node_type                to_zone_node_type,
                                       rz.zone_id                        to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                                  from rpm_cc_sys_gen_detail_gtt gtt,
                                       rpm_cc_sys_gen_head_gtt gtth,
                                       rpm_bulk_cc_pe_thread rpt,
                                       item_master im,
                                       rpm_merch_retail_def_expl rmrde,
                                       rpm_zone rz,
                                       rpm_zone_location rzl,
                                       rpm_item_loc ril,
                                       item_master im2
                                 where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                   and rpt.parent_thread_number = I_pe_sequence_id
                                   and rpt.thread_number        = I_pe_thread_number
                                   and gtt.dept                 = L_dept
                                   and gtt.price_event_id       = rpt.price_event_id
                                   and gtt.price_event_id       = gtth.price_event_id
                                   and gtth.tolerance_ind       = 1
                                   and gtt.item                 = im.item
                                   and im.tran_level            > im.item_level --parent diff item
                                   and gtt.diff_id              is NOT NULL
                                   and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                   and rmrde.dept               = im.dept
                                   and rmrde.class              = im.class
                                   and rmrde.subclass           = im.subclass
                                   and rmrde.regular_zone_group = rz.zone_group_id
                                   and rz.zone_id               = rzl.zone_id
                                   and rzl.location             = rpt.zone_node_id
                                   and im2.item_parent          = gtt.item
                                   and im2.diff_1               = gtt.diff_id
                                   and ril.dept                 = im2.dept
                                   and ril.item                 = im2.item
                                   and ril.loc                  = rpt.zone_node_id
                                   and rownum                  >= 1
                                union all
                                -- Look at the PL level
                                select distinct
                                       1 rank,
                                       il.dept,
                                       il.price_event_id,
                                       il.item                           from_item,
                                       NULL                              from_diff_id,
                                       il.location                       from_location,
                                       il.zone_node_type                 from_zone_node_type,
                                       il.item                           to_item,
                                       il.diff_id                        to_diff_id,
                                       NULL                              to_item_parent,
                                       il.location                       to_location,
                                       il.zone_node_type                 to_zone_node_type,
                                       zl.zone_id                        to_zone_id,
                                       RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                                  from (select DISTINCT
                                               im.dept,
                                               rpt.price_event_id,
                                               gtt.item,
                                               gtt.diff_id,
                                               rpt.zone_node_id location,
                                               rpt.zone_node_type,
                                               rmrde.regular_zone_group
                                          from rpm_cc_sys_gen_detail_gtt gtt,
                                               rpm_cc_sys_gen_head_gtt gtth,
                                               rpm_bulk_cc_pe_thread rpt,
                                               rpm_merch_retail_def_expl rmrde,
                                               item_master im,
                                               rpm_item_loc ril
                                         where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                           and rpt.parent_thread_number = I_pe_sequence_id
                                           and rpt.thread_number        = I_pe_thread_number
                                           and gtt.dept                 = L_dept
                                           and gtt.price_event_id       = rpt.price_event_id
                                           and gtt.price_event_id       = gtth.price_event_id
                                           and gtth.tolerance_ind       = 1
                                           and rpt.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                           and rmrde.dept               = im.dept
                                           and rmrde.class              = im.class
                                           and rmrde.subclass           = im.subclass
                                           and im.item_parent           = gtt.item
                                           and im.diff_1                = gtt.diff_id
                                           and ril.dept                 = im.dept
                                           and ril.item                 = im.item
                                           and ril.loc                  = rpt.zone_node_id
                                           and rownum                  >= 1) il,
                                       (select rz.zone_group_id,
                                               rz.zone_id,
                                               rzl.location
                                          from rpm_zone rz,
                                               rpm_zone_location rzl
                                         where rzl.zone_id = rz.zone_id
                                           and rownum      > 0) zl
                                 where il.regular_zone_group = zl.zone_group_id (+)
                                   and il.location           = zl.location (+)
                                   and rownum               >= 1
                                union all
                                -- Item Location Level Price Events
                                -- Look at all levels above and rank, then use the data with the highest rank
                                -- Look at P/Z level first and rank that data 0
                               select 0 rank,
                                      dept,
                                      price_event_id,
                                      item_parent                       from_item,
                                      NULL                              from_diff_id,
                                      fr_zone_id                        from_location,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                      item                              to_item,
                                      diff_id                           to_diff_id,
                                      item_parent                       to_item_parent,
                                      location                          to_location,
                                      loc_type                          to_zone_node_type,
                                      fr_zone_id                        to_zone_id,
                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                                 from rpm_price_inquiry_gtt gtt
                                where rownum > 0
                               union all
                               -- Look at PD/Z level and rank that data 1
                               select 1 rank,
                                      dept,
                                      price_event_id,
                                      item_parent                       from_item,
                                      diff_id                           from_diff_id,
                                      fr_zone_id                        from_location,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                      item                              to_item,
                                      diff_id                           to_diff_id,
                                      item_parent                       to_item_parent,
                                      location                          to_location,
                                      loc_type                          to_zone_node_type,
                                      fr_zone_id                        to_zone_id,
                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                                 from rpm_price_inquiry_gtt gtt
                                where diff_id is NOT NULL
                                  and rownum  > 0
                               union all
                               -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                               select 1 rank,
                                      dept,
                                      price_event_id,
                                      item_parent                       from_item,
                                      diff_id                           from_diff_id,
                                      location                          from_location,
                                      loc_type                          from_zone_node_type,
                                      item                              to_item,
                                      diff_id                           to_diff_id,
                                      item_parent                       to_item_parent,
                                      location                          to_location,
                                      loc_type                          to_zone_node_type,
                                      fr_zone_id                        to_zone_id,
                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                                 from rpm_price_inquiry_gtt gtt
                                where rownum  > 0
                               union all
                               -- Look at I/Z level and rank that data 2
                               select 2 rank,
                                      dept,
                                      price_event_id,
                                      item                              from_item,
                                      diff_id                           from_diff_id,
                                      fr_zone_id                        from_location,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                      item                              to_item,
                                      diff_id                           to_diff_id,
                                      item_parent                       to_item_parent,
                                      location                          to_location,
                                      loc_type                          to_zone_node_type,
                                      fr_zone_id                        to_zone_id,
                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                                 from rpm_price_inquiry_gtt gtt
                                where rownum > 0
                               union all
                               -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                               select 2 rank,
                                      dept,
                                      price_event_id,
                                      item_parent                       from_item,
                                      diff_id                           from_diff_id,
                                      location                          from_location,
                                      loc_type                          from_zone_node_type,
                                      item                              to_item,
                                      diff_id                           to_diff_id,
                                      item_parent                       to_item_parent,
                                      location                          to_location,
                                      loc_type                          to_zone_node_type,
                                      fr_zone_id                        to_zone_id,
                                      RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                                 from rpm_price_inquiry_gtt gtt
                                where diff_id is NOT NULL
                                  and rownum  >= 1) t,
                               rpm_cust_segment_promo_fr cspfr,
                               rpm_promo_dtl rpd,
                               rpm_promo_comp rpc
                         where cspfr.dept                 = L_dept
                           and cspfr.item                 = t.from_item
                           and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                           and cspfr.location             = t.from_location
                           and cspfr.zone_node_type       = t.from_zone_node_type
                           and cspfr.customer_type        is NOT NULL
                           and rpd.promo_dtl_id           = t.price_event_id
                           and rpc.promo_comp_id          = rpd.promo_comp_id
                           and rpc.customer_type          = cspfr.customer_type
                           and NOT EXISTS (select 'x'
                                             from rpm_cust_segment_promo_fr fr
                                            where L_dept                    = fr.dept
                                              and t.to_item                 = fr.item
                                              and NVL(t.to_diff_id, '-999') = NVL(fr.diff_id, '-999')
                                              and t.to_location             = fr.location
                                              and t.to_zone_node_type       = fr.zone_node_type))
                 where rank = max_rank) source
                on (    target.price_event_id         = source.price_event_id
                    and target.item                   = source.item
                    and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
                    and target.location               = source.location
                    and target.zone_node_type         = source.zone_node_type
                    and target.action_date            = source.action_date
                    and target.customer_type          = source.customer_type)
                when NOT MATCHED then
                   insert
                      (price_event_id,
                       cust_segment_promo_id,
                       item,
                       diff_id,
                       item_parent,
                       zone_node_type,
                       location,
                       zone_id,
                       action_date,
                       customer_type,
                       dept,
                       promo_retail,
                       promo_retail_currency,
                       promo_uom,
                       complex_promo_ind,
                       max_hier_level,
                       cur_hier_level,
                       original_cs_promo_id,
                       step_identifier)
                   values
                      (source.price_event_id,
                       RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                       source.item,
                       source.diff_id,
                       source.item_parent,
                       source.zone_node_type,
                       source.location,
                       source.zone_id,
                       source.action_date,
                       source.customer_type,
                       source.dept,
                       source.promo_retail,
                       source.promo_retail_currency,
                       source.promo_uom,
                       source.complex_promo_ind,
                       source.max_hier_level,
                       source.cur_hier_level,
                       source.cust_segment_promo_id,
                       RPM_CONSTANTS.CAPT_GTT_RFR_SQL_GEN_TL);

      open C_CHECK_CSPFR_TL;
      fetch C_CHECK_CSPFR_TL BULK COLLECT into L_pe_ids;
      close C_CHECK_CSPFR_TL;

      -- If there is no "parent" data on CSPFR and dealing with a CS promo, need to create the data based on FR data...
      if L_pe_ids is NOT NULL and
         L_pe_ids.COUNT > 0 and

         LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

         merge into rpm_cust_segment_promo_fr_gtt target
         using (select price_event_id,
                       item,
                       zone_node_type,
                       location,
                       action_date,
                       customer_type,
                       dept,
                       simple_promo_retail,
                       simple_promo_retail_currency,
                       simple_promo_uom,
                       item_parent,
                       diff_id,
                       zone_id,
                       max_hier_level,
                       cur_hier_level
                  from (select distinct sge_dtl.price_event_id,
                               fr.item,
                               fr.zone_node_type,
                               fr.location,
                               fr.action_date,
                               rpc.customer_type,
                               fr.dept,
                               fr.simple_promo_retail,
                               fr.simple_promo_retail_currency,
                               fr.simple_promo_uom,
                               fr.item_parent,
                               fr.diff_id,
                               fr.zone_id,
                               fr.max_hier_level,
                               fr.cur_hier_level
                          from table(cast(L_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                               rpm_cc_sys_gen_detail_gtt sge_dtl,
                               rpm_future_retail fr,
                               rpm_promo_dtl rpd,
                               rpm_promo_comp rpc
                         where sge_dtl.price_event_id   = VALUE(ids)
                           and fr.dept                  = L_dept
                           and sge_dtl.dept             = L_dept
                           and fr.item                  = sge_dtl.item
                           and NVL(fr.diff_id, '-999')  = NVL(sge_dtl.diff_id, '-999')
                           and fr.location              = sge_dtl.location
                           and fr.zone_node_type        = sge_dtl.zone_node_type
                           and rpd.promo_dtl_id         = sge_dtl.price_event_id
                           and rpd.promo_comp_id        = rpc.promo_comp_id
                           and fr.action_date          >= rpd.start_date
                           and fr.action_date          <= NVL(rpd.end_date + 1, TO_DATE('3000', 'YYYY'))
                           and NOT EXISTS (select 'x'
                                             from rpm_cust_segment_promo_fr fr
                                            where L_dept                       = fr.dept
                                              and sge_dtl.item                 = fr.item
                                              and NVL(sge_dtl.diff_id, '-999') = NVL(fr.diff_id, '-999')
                                              and sge_dtl.location             = fr.location
                                              and sge_dtl.zone_node_type       = fr.zone_node_type))) source
         on (    target.price_event_id       = source.price_event_id
             and target.dept                 = source.dept
             and target.location             = source.location
             and target.zone_node_type       = source.zone_node_type
             and target.item                 = source.item
             and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
             and target.customer_type        = source.customer_type
             and target.action_date          = source.action_date)
         when NOT MATCHED then
            insert
               (price_event_id,
                cust_segment_promo_id,
                item,
                zone_node_type,
                location,
                action_date,
                customer_type,
                dept,
                promo_retail,
                promo_retail_currency,
                promo_uom,
                complex_promo_ind,
                item_parent,
                diff_id,
                zone_id,
                max_hier_level,
                cur_hier_level,
                step_identifier)
            values (source.price_event_id,
                    RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                    source.item,
                    source.zone_node_type,
                    source.location,
                    source.action_date,
                    source.customer_type,
                    source.dept,
                    source.simple_promo_retail,
                    source.simple_promo_retail_currency,
                    source.simple_promo_uom,
                    0,      -- complex_promo_ind
                    source.item_parent,
                    source.diff_id,
                    source.zone_id,
                    source.max_hier_level,
                    source.cur_hier_level,
                    RPM_CONSTANTS.CAPT_GTT_RFR_SQL_GEN_TL);

      end if;

      merge into rpm_promo_item_loc_expl_gtt target
      using (select distinct
                    sge_dtl.price_event_id,
                    sge_dtl.item,
                    sge_dtl.dept,
                    pi.class,
                    pi.subclass,
                    sge_dtl.location,
                    sge_dtl.zone_node_type,
                    rpile.promo_id,
                    rpile.promo_display_id,
                    rpile.promo_secondary_ind,
                    rpile.promo_comp_id,
                    rpile.comp_display_id,
                    rpd.promo_dtl_id,
                    rpile.type,
                    rpile.customer_type,
                    rpile.detail_secondary_ind,
                    rpile.detail_start_date,
                    rpile.detail_end_date,
                    rpile.detail_apply_to_code,
                    rpd.exception_parent_id,
                    pi.item_parent,
                    sge_dtl.diff_id,
                    pi.fr_zone_id,
                    sge_dtl.max_hier_level,
                    sge_dtl.cur_hier_level
               from rpm_cc_sys_gen_detail_gtt sge_dtl,
                    rpm_price_inquiry_gtt pi,
                    rpm_promo_item_loc_expl_gtt rpile,
                    rpm_promo_dtl rpd
              where sge_dtl.dept           = L_dept
                and sge_dtl.price_event_id = rpile.promo_dtl_id
                and sge_dtl.price_event_id = rpd.exception_parent_id
                and sge_dtl.price_event_id = pi.price_event_id
                and sge_dtl.item           = pi.item) source
      on (source.promo_dtl_id = target.promo_dtl_id)
      when NOT MATCHED then
         insert (price_event_id,
                 promo_item_loc_expl_id,
                 item,
                 dept,
                 class,
                 subclass,
                 location,
                 zone_node_type,
                 promo_id,
                 promo_display_id,
                 promo_secondary_ind,
                 promo_comp_id,
                 comp_display_id,
                 promo_dtl_id,
                 type,
                 customer_type,
                 detail_secondary_ind,
                 detail_start_date,
                 detail_end_date,
                 detail_apply_to_code,
                 detail_change_type,
                 exception_parent_id,
                 promo_comp_msg_type,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level,
                 step_identifier)
         values (source.price_event_id,
                 RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
                 source.item,
                 source.dept,
                 source.class,
                 source.subclass,
                 source.location,
                 source.zone_node_type,
                 source.promo_id,
                 source.promo_display_id,
                 source.promo_secondary_ind,
                 source.promo_comp_id,
                 source.comp_display_id,
                 source.promo_dtl_id,
                 source.type,
                 source.customer_type,
                 source.detail_secondary_ind,
                 source.detail_start_date,
                 source.detail_end_date,
                 source.detail_apply_to_code,
                 RPM_CONSTANTS.RETAIL_EXCLUDE,
                 source.exception_parent_id,
                 RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE,
                 source.item_parent,
                 source.diff_id,
                 source.fr_zone_id,
                 source.max_hier_level,
                 source.cur_hier_level,
                 RPM_CONSTANTS.CAPT_GTT_RFR_SQL_GEN_TL);

   end loop;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_RFR_SQL_GEN_TL,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - parent_thread_number: '|| LP_pe_sequence_id ||
                               ' - thread_number: '|| LP_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_TIMELINE;
--------------------------------------------------------------------------------

FUNCTION CLEAN_SGE_ON_REMOVE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.CLEAN_SGE_ON_REMOVE';

   L_cc_err_tbl CONFLICT_CHECK_ERROR_TBL := NULL;

BEGIN

   if IO_cc_error_tbl is NULL or
      IO_cc_error_tbl.COUNT = 0 then
      ---
      L_cc_err_tbl  := CONFLICT_CHECK_ERROR_TBL(CONFLICT_CHECK_ERROR_REC(-999,
                                                                         NULL,
                                                                         NULL,
                                                                         NULL,
                                                                         NULL));

   else
      L_cc_err_tbl  := IO_cc_error_tbl;
   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      delete
        from rpm_con_check_err_detail ccd
       where ccd.con_check_err_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ cc.con_check_err_id
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                             rpm_price_change rpc,
                                             rpm_con_check_err cc
                                       where ccer.price_event_id                != VALUE(ids)
                                         and rpc.exception_parent_id             = VALUE(ids)
                                         and NVL(rpc.sys_generated_exclusion, 0) = 1
                                         and cc.ref_id                           = rpc.price_change_id);

      delete
        from rpm_con_check_err cc
       where cc.ref_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpc.price_change_id
                             from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                  table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                  rpm_price_change rpc
                            where ccer.price_event_id                != VALUE(ids)
                              and rpc.exception_parent_id             = VALUE(ids)
                              and NVL(rpc.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_merch_list_detail mld
       where mld.merch_list_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ mlh.merch_list_id
                                     from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                          table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                          rpm_price_change rpc,
                                          rpm_merch_list_head mlh
                                    where ccer.price_event_id                != VALUE(ids)
                                      and rpc.exception_parent_id             = VALUE(ids)
                                      and NVL(rpc.sys_generated_exclusion, 0) = 1
                                      and rpc.price_change_id                 = mlh.price_event_id
                                      and mlh.price_event_type                = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                      and mlh.merch_list_type                 = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE);

      delete
        from rpm_merch_list_head mlh
       where mlh.price_event_id   IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpc.price_change_id
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                             rpm_price_change rpc
                                       where ccer.price_event_id                != VALUE(ids)
                                         and rpc.exception_parent_id             = VALUE(ids)
                                         and NVL(rpc.sys_generated_exclusion, 0) = 1)
         and mlh.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE;

      delete
        from rpm_price_change rpc
       where rpc.exception_parent_id             IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ VALUE(ids)
                                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                            table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer
                                                      where ccer.price_event_id != VALUE(ids))
         and NVL(rpc.sys_generated_exclusion, 0) = 1;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      delete
        from rpm_con_check_err_detail ccd
       where ccd.con_check_err_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ cc.con_check_err_id
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                             rpm_clearance rcl,
                                             rpm_con_check_err cc
                                       where ccer.price_event_id                != VALUE(ids)
                                         and rcl.exception_parent_id             = VALUE(ids)
                                         and NVL(rcl.sys_generated_exclusion, 0) = 1
                                         and cc.ref_id                           = rcl.clearance_id);

      delete
        from rpm_con_check_err cc
       where cc.ref_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rcl.clearance_id
                             from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                  table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                  rpm_clearance rcl
                            where ccer.price_event_id                != VALUE(ids)
                              and rcl.exception_parent_id             = VALUE(ids)
                              and NVL(rcl.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_merch_list_detail mld
       where mld.merch_list_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ mlh.merch_list_id
                                     from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                          table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                          rpm_clearance rcl,
                                          rpm_merch_list_head mlh
                                    where ccer.price_event_id                != VALUE(ids)
                                      and rcl.exception_parent_id             = VALUE(ids)
                                      and NVL(rcl.sys_generated_exclusion, 0) = 1
                                      and rcl.clearance_id                    = mlh.price_event_id
                                      and mlh.price_event_type                = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                      and mlh.merch_list_type                 = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE);

      delete
        from rpm_merch_list_head mlh
       where mlh.price_event_id   IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rcl.clearance_id
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                             rpm_clearance rcl
                                       where ccer.price_event_id                != VALUE(ids)
                                         and rcl.exception_parent_id             = VALUE(ids)
                                         and NVL(rcl.sys_generated_exclusion, 0) = 1)
         and mlh.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE;

      delete
        from rpm_clearance rc
       where rc.exception_parent_id             IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ VALUE(ids)
                                                      from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                           table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer
                                                     where ccer.price_event_id != VALUE(ids))
         and NVL(rc.sys_generated_exclusion, 0) = 1;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

      delete
        from rpm_con_check_err_detail ccd
       where ccd.con_check_err_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ cc.con_check_err_id
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                             rpm_promo_dtl rpd,
                                             rpm_con_check_err cc
                                       where ccer.price_event_id                != VALUE(ids)
                                         and rpd.exception_parent_id             = VALUE(ids)
                                         and NVL(rpd.sys_generated_exclusion, 0) = 1
                                         and cc.ref_id                           = rpd.promo_dtl_id);

      delete
        from rpm_con_check_err cc
       where cc.ref_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                             from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                  table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                  rpm_promo_dtl rpd
                            where ccer.price_event_id                != VALUE(ids)
                              and rpd.exception_parent_id             = VALUE(ids)
                              and NVL(rpd.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_merch_list_detail mld
       where mld.merch_list_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ mlh.merch_list_id
                                     from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                          table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                          rpm_promo_dtl rpd,
                                          rpm_merch_list_head mlh
                                    where ccer.price_event_id                != VALUE(ids)
                                      and rpd.exception_parent_id             = VALUE(ids)
                                      and NVL(rpd.sys_generated_exclusion, 0) = 1
                                      and rpd.promo_dtl_id                    = mlh.price_event_id
                                      and mlh.merch_list_type                 = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                                      and mlh.price_event_type  IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                                    RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO));

      delete
        from rpm_merch_list_head mlh
       where mlh.price_event_id   IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                             rpm_promo_dtl rpd
                                       where ccer.price_event_id                != VALUE(ids)
                                         and rpd.exception_parent_id             = VALUE(ids)
                                         and NVL(rpd.sys_generated_exclusion, 0) = 1)
         and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO);

      delete
        from rpm_promo_zone_location
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                     rpm_promo_dtl rpd
                               where ccer.price_event_id                != VALUE(ids)
                                 and rpd.exception_parent_id             = VALUE(ids)
                                 and NVL(rpd.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_promo_dtl_merch_node
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                     rpm_promo_dtl rpd
                               where ccer.price_event_id                != VALUE(ids)
                                 and rpd.exception_parent_id             = VALUE(ids)
                                 and NVL(rpd.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_promo_dtl_disc_ladder
       where promo_dtl_list_id  IN (select promo_dtl_list_id
                                      from rpm_promo_dtl_list
                                     where promo_dtl_list_grp_id IN (select promo_dtl_list_grp_id
                                                                       from rpm_promo_dtl_list_grp
                                                                      where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                                                                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                                                                    table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                                                                                    rpm_promo_dtl rpd
                                                                                              where ccer.price_event_id                != VALUE(ids)
                                                                                                and rpd.exception_parent_id             = VALUE(ids)
                                                                                                and NVL(rpd.sys_generated_exclusion, 0) = 1)));

      delete
        from rpm_promo_dtl_list
        where promo_dtl_list_grp_id IN (select promo_dtl_list_grp_id
                                          from rpm_promo_dtl_list_grp
                                         where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                                                  from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                                                       table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                                                       rpm_promo_dtl rpd
                                                                 where ccer.price_event_id                != VALUE(ids)
                                                                   and rpd.exception_parent_id             = VALUE(ids)
                                                                   and NVL(rpd.sys_generated_exclusion, 0) = 1));

      delete
        from rpm_promo_dtl_list_grp
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                     rpm_promo_dtl rpd
                               where ccer.price_event_id                != VALUE(ids)
                                 and rpd.exception_parent_id             = VALUE(ids)
                                 and NVL(rpd.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_promo_item_loc_expl
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                     rpm_promo_dtl rpd
                               where ccer.price_event_id                != VALUE(ids)
                                 and rpd.exception_parent_id             = VALUE(ids)
                                 and NVL(rpd.sys_generated_exclusion, 0) = 1);

      delete
        from rpm_promo_dtl
       where promo_dtl_id IN (select /*+ CARDINALITY(ids 10) CARDINALITY(ccer 10) */ rpd.promo_dtl_id
                                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     table(cast(L_cc_err_tbl as CONFLICT_CHECK_ERROR_TBL)) ccer,
                                     rpm_promo_dtl rpd
                               where ccer.price_event_id                != VALUE(ids)
                                 and rpd.exception_parent_id             = VALUE(ids)
                                 and NVL(rpd.sys_generated_exclusion, 0) = 1);

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END CLEAN_SGE_ON_REMOVE;

--------------------------------------------------------------------------------

FUNCTION GENERATE_EXCLUSIONS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                             I_cc_error_tbl IN     CONFLICT_CHECK_ERROR_TBL)

RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_SQL.GENERATE_EXCLUSIONS';

   cursor C_PC_ERROR_UPDATE is
      select /*+ CARDINALITY(cc, 10) */
             CONFLICT_CHECK_ERROR_REC(pc.price_change_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_price_change pc
       where cc.price_event_id          = pc.exception_parent_id
         and pc.sys_generated_exclusion = 1;

   cursor C_CL_ERROR_UPDATE is
      select /*+ CARDINALITY(cc, 10) */
             CONFLICT_CHECK_ERROR_REC(rc.clearance_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_clearance rc
       where cc.price_event_id          = rc.exception_parent_id
         and rc.sys_generated_exclusion = 1;

   cursor C_PROMO_ERROR_UPDATE is
      select /*+ CARDINALITY(cc, 10) */
             CONFLICT_CHECK_ERROR_REC(rpd.promo_dtl_id,
                                      cc.future_retail_id,
                                      cc.error_type,
                                      cc.error_string)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_promo_dtl rpd
       where cc.price_event_id           = rpd.exception_parent_id
         and rpd.sys_generated_exclusion = 1;

BEGIN

   insert into rpm_merch_list_head (merch_list_id,
                                    price_event_id,
                                    price_event_type,
                                    merch_list_desc,
                                    merch_list_type)
      select /*+ CARDINALITY(pe 10) */
             RPM_MERCH_LIST_ID_SEQ.NEXTVAL,
             pe.price_event_id,
             LP_price_event_type,
             NVL(so.def_merch_list_desc, 'SYSTEM GENERATED LIST')||' '||RPM_MERCH_LIST_ID_SEQ.NEXTVAL,
             RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
        from (select distinct cc.price_event_id
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
             rpm_system_options_def so;

   insert into rpm_merch_list_detail (merch_list_id,
                                      merch_level,
                                      item,
                                      item_desc,
                                      diff_id,
                                      diff_desc)
      select merch_list_id,
             merch_level,
             item,
             item_desc,
             diff_id,
             diff_desc
        from (-- tran level items
              select /*+ CARDINALITY(cc, 1) */
                     distinct mlh.merch_list_id,
                     RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level,
                     im.item,
                     im.item_desc,
                     NULL diff_id,
                     NULL diff_desc
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     item_master im,
                     rpm_merch_list_head mlh
               where cc.price_event_id    = gtt.price_event_id
                 and gtt.price_event_id   = mlh.price_event_id
                 and gtth.price_event_id  = gtt.price_event_id
                 and gtth.tolerance_ind   = 1
                 and mlh.price_event_type = LP_price_event_type
                 and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                 and gtt.item             = im.item
                 and gtt.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                              RPM_CONSTANTS.FR_HIER_ITEM_LOC)
              union
              -- parent level items
              select /*+ CARDINALITY(cc, 1) */
                     distinct mlh.merch_list_id,
                     RPM_CONSTANTS.PARENT_MERCH_TYPE merch_level,
                     im.item,
                     im.item_desc,
                     NULL diff_id,
                     NULL diff_desc
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     item_master im,
                     rpm_merch_list_head mlh
               where cc.price_event_id    = gtt.price_event_id
                 and gtt.price_event_id   = mlh.price_event_id
                 and gtth.price_event_id  = gtt.price_event_id
                 and gtth.tolerance_ind   = 1
                 and mlh.price_event_type = LP_price_event_type
                 and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                 and gtt.item             = im.item
                 and gtt.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                              RPM_CONSTANTS.FR_HIER_PARENT_LOC)
              union
              -- parent diff level items
              select /*+ CARDINALITY(cc, 1) */
                     distinct mlh.merch_list_id,
                     RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE merch_level,
                     im.item,
                     im.item_desc,
                     im.diff_1 diff_id,
                     d.diff_desc
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_cc_sys_gen_head_gtt gtth,
                     item_master im,
                     diff_ids d,
                     rpm_merch_list_head mlh
               where cc.price_event_id    = gtt.price_event_id
                 and gtt.price_event_id   = mlh.price_event_id
                 and gtth.price_event_id  = gtt.price_event_id
                 and gtth.tolerance_ind   = 1
                 and mlh.price_event_type = LP_price_event_type
                 and mlh.merch_list_type  = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                 and gtt.item             = im.item
                 and gtt.diff_id          = im.diff_1
                 and im.diff_1            = d.diff_id
                 and gtt.cur_hier_level   IN (RPM_CONSTANTS.FR_HIER_DIFF_ZONE,
                                              RPM_CONSTANTS.FR_HIER_DIFF_LOC));

   -- Create the exclusions based on the price event and the merch_list created
   if LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                              RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

      insert into rpm_promo_dtl (promo_dtl_id,
                                 promo_comp_id,
                                 promo_dtl_display_id,
                                 ignore_constraints,
                                 apply_to_code,
                                 start_date,
                                 end_date,
                                 approval_date,
                                 create_date,
                                 create_id,
                                 approval_id,
                                 state,
                                 attribute_1,
                                 attribute_2,
                                 attribute_3,
                                 exception_parent_id,
                                 from_location_move,
                                 price_guide_id,
                                 threshold_id,
                                 sys_generated_exclusion,
                                 timebased_dtl_ind,
                                 cancel_il_promo_dtl_ind)
         select /*+ CARDINALITY(pe 10) */
                RPM_PROMO_DTL_SEQ.NEXTVAL,             --promo_dtl_id
                rpd.promo_comp_id,
                RPM_PROMO_DETAIL_DISPLAY_SEQ.NEXTVAL,  --promo_dtl_display_id
                rpd.ignore_constraints,
                rpd.apply_to_code,
                rpd.start_date,
                rpd.end_date,
                rpd.approval_date,
                rpd.create_date,
                rpd.create_id,
                rpd.approval_id,
                rpd.state,
                rpd.attribute_1,
                rpd.attribute_2,
                rpd.attribute_3,
                pe.price_event_id,             --exception_parent_id
                rpd.from_location_move,
                rpd.price_guide_id,
                rpd.threshold_id,
                1,                             --sys_generated_exclusion
                rpd.timebased_dtl_ind,
                rpd.cancel_il_promo_dtl_ind
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_promo_dtl rpd
          where rpd.promo_dtl_id = pe.price_event_id;

      insert into rpm_promo_zone_location (promo_zone_id,
                                           promo_dtl_id,
                                           zone_node_type,
                                           zone_id,
                                           location)
         select /*+ CARDINALITY(pe 10) */
                RPM_PROMO_ZONE_LOCATION_SEQ.NEXTVAL,
                rpd2.promo_dtl_id,
                pzl.zone_node_type,
                pzl.zone_id,
                pzl.location
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_promo_zone_location pzl,
                rpm_promo_dtl rpd1,
                rpm_promo_dtl rpd2
          where pe.price_event_id            = rpd1.promo_dtl_id
            and rpd1.promo_dtl_id            = rpd2.exception_parent_id
            and rpd2.sys_generated_exclusion = 1
            and rpd1.promo_dtl_id            = pzl.promo_dtl_id;

      insert into rpm_promo_dtl_list_grp (promo_dtl_list_grp_id,
                                          promo_dtl_id)
         select /*+ CARDINALITY(pe 10) */
                RPM_PROMO_DTL_LIST_GRP_SEQ.NEXTVAL,
                rpd2.promo_dtl_id
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_promo_dtl rpd2
          where pe.price_event_id            = rpd2.exception_parent_id
            and rpd2.sys_generated_exclusion = 1;

      insert into rpm_promo_dtl_list (promo_dtl_list_id,
                                      promo_dtl_list_grp_id)
         select /*+ CARDINALITY(pe 10) */
                RPM_PROMO_DTL_LIST_SEQ.NEXTVAL,
                pdlg.promo_dtl_list_grp_id
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_promo_dtl rpd2,
                rpm_promo_dtl_list_grp pdlg
          where pe.price_event_id            = rpd2.exception_parent_id
            and rpd2.sys_generated_exclusion = 1
            and rpd2.promo_dtl_id            = pdlg.promo_dtl_id;

      insert into rpm_promo_dtl_merch_node (promo_dtl_merch_node_id,
                                            promo_dtl_list_id,
                                            promo_dtl_id,
                                            merch_type,
                                            skulist)
         select /*+ CARDINALITY(pe 10) */
                RPM_PROMO_DTL_MERCH_NODE_SEQ.NEXTVAL,
                pdl.promo_dtl_list_id,
                rpd2.promo_dtl_id,
                RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM, --merch_type
                mlh.merch_list_id
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_promo_dtl rpd2,
                rpm_promo_dtl_list_grp pdlg,
                rpm_promo_dtl_list pdl,
                rpm_merch_list_head mlh
          where pe.price_event_id            = rpd2.exception_parent_id
            and rpd2.sys_generated_exclusion = 1
            and rpd2.promo_dtl_id            = pdlg.promo_dtl_id
            and pdlg.promo_dtl_list_grp_id   = pdl.promo_dtl_list_grp_id
            and pe.price_event_id            = mlh.price_event_id
            and mlh.price_event_type         = LP_price_event_type
            and mlh.merch_list_type          = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE;

      insert into rpm_promo_dtl_disc_ladder (promo_dtl_disc_ladder_id,
                                             promo_dtl_list_id,
                                             change_type,
                                             qual_type,
                                             qual_value)
         select /*+ CARDINALITY(pe 10) */
                RPM_PROMO_DTL_DISC_LADDER_SEQ.NEXTVAL,
                pdl.promo_dtl_list_id,
                RPM_CONSTANTS.RETAIL_EXCLUDE,
                orig.qual_type,
                orig.qual_value
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_promo_dtl rpd2,
                rpm_promo_dtl_list_grp pdlg,
                rpm_promo_dtl_list pdl,
                (select /*+ CARDINALITY(pe2 10) */
                        pddl.qual_type,
                        pddl.qual_value,
                        pe2.price_event_id
                   from (select distinct cc.price_event_id
                           from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe2,
                        rpm_promo_dtl rpd,
                        rpm_promo_dtl_list_grp pdlg2,
                        rpm_promo_dtl_list pdl2,
                        rpm_promo_dtl_disc_ladder pddl
                  where pe2.price_event_id          = rpd.promo_dtl_id
                    and rpd.promo_dtl_id            = pdlg2.promo_dtl_id
                    and pdlg2.promo_dtl_list_grp_id = pdl2.promo_dtl_list_grp_id
                    and pdl2.promo_dtl_list_id      = pddl.promo_dtl_list_id
                    and rownum                      > 0) orig
          where pe.price_event_id            = rpd2.exception_parent_id
            and rpd2.sys_generated_exclusion = 1
            and rpd2.promo_dtl_id            = pdlg.promo_dtl_id
            and pdlg.promo_dtl_list_grp_id   = pdl.promo_dtl_list_grp_id
            and orig.price_event_id          = pe.price_event_id;

      -- Update the merch list head price event id now that it's available...
      merge /*+ INDEX(target, RPM_MERCH_LIST_HEAD_I2)*/
            into rpm_merch_list_head target
      using (select /*+ CARDINALITY(pe, 10) */
                    rpd.promo_dtl_id,
                    pdmn.skulist
               from (select distinct cc.price_event_id
                       from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                    rpm_promo_dtl rpd,
                    rpm_promo_dtl_merch_node pdmn
              where pe.price_event_id           = rpd.exception_parent_id
                and rpd.promo_dtl_id            = pdmn.promo_dtl_id
                and pdmn.price_event_itemlist   is NULL
                and pdmn.skulist                is NOT NULL
                and rpd.sys_generated_exclusion = 1) source
      on (source.skulist = target.merch_list_id)
      when MATCHED then
         update
            set target.price_event_id = source.promo_dtl_id;

      open C_PROMO_ERROR_UPDATE;
      fetch C_PROMO_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
      close C_PROMO_ERROR_UPDATE;

   elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      insert into rpm_clearance (clearance_id,
                                 clearance_display_id,
                                 state,
                                 reason_code,
                                 exception_parent_id,
                                 zone_id,
                                 location,
                                 zone_node_type,
                                 effective_date,
                                 out_of_stock_date,
                                 reset_date,
                                 change_type,
                                 price_guide_id,
                                 vendor_funded_ind,
                                 funding_type,
                                 funding_amount,
                                 funding_amount_currency,
                                 funding_percent,
                                 supplier,
                                 deal_id,
                                 deal_detail_id,
                                 partner_type,
                                 partner_id,
                                 create_date,
                                 create_id,
                                 approval_date,
                                 approval_id,
                                 lock_version,
                                 skulist,
                                 sys_generated_exclusion)
         select /*+ CARDINALITY(pe 10) */
                RPM_CLEARANCE_SEQ.NEXTVAL,
                RPM_CLEARANCE_DISPLAY_SEQ.NEXTVAL,
                state,
                reason_code,
                pe.price_event_id, --exception_parent_id
                zone_id,
                location,
                zone_node_type,
                effective_date,
                out_of_stock_date,
                reset_date,
                RPM_CONSTANTS.RETAIL_EXCLUDE, --change_type
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                funding_amount_currency,
                funding_percent,
                supplier,
                deal_id,
                deal_detail_id,
                partner_type,
                partner_id,
                create_date,
                create_id,
                approval_date,
                approval_id,
                lock_version,
                mlh.merch_list_id,  --skulist,
                1                   --sys_generated_exclusion
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_clearance cl,
                rpm_merch_list_head mlh
          where cl.clearance_id     = pe.price_event_id
            and pe.price_event_id   = mlh.price_event_id
            and mlh.merch_list_type = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE;

      -- Update the merch list head price event id now that it's available...
      merge /*+ INDEX(target, RPM_MERCH_LIST_HEAD_I2)*/
            into rpm_merch_list_head target
      using (select /*+ CARDINALITY(pe, 10) */
                    rc.clearance_id,
                    rc.skulist
               from (select distinct cc.price_event_id
                       from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                    rpm_clearance rc
              where pe.price_event_id          = rc.exception_parent_id
                and rc.price_event_itemlist    is NULL
                and rc.skulist                 is NOT NULL
                and rc.sys_generated_exclusion = 1) source
      on (target.merch_list_id = source.skulist)
      when MATCHED then
         update
            set target.price_event_id = source.clearance_id;

      open C_CL_ERROR_UPDATE;
      fetch C_CL_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
      close C_CL_ERROR_UPDATE;

   elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      insert into rpm_price_change (price_change_id,
                                    price_change_display_id,
                                    state,
                                    reason_code,
                                    exception_parent_id,
                                    zone_id,
                                    location,
                                    zone_node_type,
                                    link_code,
                                    effective_date,
                                    change_type,
                                    null_multi_ind,
                                    multi_units,
                                    multi_unit_retail,
                                    multi_unit_retail_currency,
                                    multi_selling_uom,
                                    price_guide_id,
                                    vendor_funded_ind,
                                    funding_type,
                                    funding_amount,
                                    funding_amount_currency,
                                    funding_percent,
                                    deal_id,
                                    deal_detail_id,
                                    create_date,
                                    create_id,
                                    approval_date,
                                    approval_id,
                                    area_diff_parent_id,
                                    ignore_constraints,
                                    lock_version,
                                    skulist,
                                    sys_generated_exclusion)
         select /*+ CARDINALITY(pe 10) */
                RPM_PRICE_CHANGE_SEQ.NEXTVAL,
                RPM_PRICE_CHANGE_DISPLAY_SEQ.NEXTVAL,
                state,
                reason_code,
                pe.price_event_id,    --exception_parent_id
                zone_id,
                location,
                zone_node_type,
                link_code,
                effective_date,
                RPM_CONSTANTS.RETAIL_EXCLUDE, --change_type
                null_multi_ind,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                price_guide_id,
                vendor_funded_ind,
                funding_type,
                funding_amount,
                funding_amount_currency,
                funding_percent,
                deal_id,
                deal_detail_id,
                create_date,
                create_id,
                approval_date,
                approval_id,
                area_diff_parent_id,
                ignore_constraints,
                lock_version,
                mlh.merch_list_id,     --skulist,
                1                      --sys_generated_exclusion
           from (select distinct cc.price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                rpm_price_change pc,
                rpm_merch_list_head mlh
          where pc.price_change_id  = pe.price_event_id
            and pe.price_event_id   = mlh.price_event_id
            and mlh.merch_list_type = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE;

      -- Update the merch list head price event id now that it's available...
      merge /*+ INDEX(target, RPM_MERCH_LIST_HEAD_I2)*/
            into rpm_merch_list_head target
      using (select /*+ CARDINALITY(pe, 10) */
                    rpc.price_change_id,
                    rpc.skulist
               from (select distinct cc.price_event_id
                       from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc) pe,
                    rpm_price_change rpc
              where pe.price_event_id           = rpc.exception_parent_id
                and rpc.price_event_itemlist    is NULL
                and rpc.skulist                 is NOT NULL
                and rpc.sys_generated_exclusion = 1) source
      on (target.merch_list_id = source.skulist)
      when MATCHED then
         update
            set target.price_event_id = source.price_change_id;

      open C_PC_ERROR_UPDATE;
      fetch C_PC_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
      close C_PC_ERROR_UPDATE;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_EXCLUSIONS;

--------------------------------------------------------------------------------

FUNCTION UPDATE_SGE_DATA(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE)

RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.UPDATE_SGE_DATA';

   L_merch_list_head_deleted OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_MERCH_HEAD_DEL is
      select price_event_id
        from rpm_merch_list_head
       where merch_list_type = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and merch_list_id   NOT IN (select merch_list_id
                                       from rpm_merch_list_detail);

   cursor C_PC_ERROR_UPDATE is
      select /*+ CARDINALITY(cc, 10) */
             CONFLICT_CHECK_ERROR_REC (pc.price_change_id,
                                       cc.future_retail_id,
                                       cc.error_type,
                                       cc.error_string,
                                       NULL)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_price_change pc
       where cc.price_event_id          = pc.exception_parent_id
         and pc.sys_generated_exclusion = 1;

   cursor C_CL_ERROR_UPDATE is
      select /*+ CARDINALITY(cc, 10) */
             CONFLICT_CHECK_ERROR_REC (rc.clearance_id,
                                       cc.future_retail_id,
                                       cc.error_type,
                                       cc.error_string,
                                       NULL)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_clearance rc
       where cc.price_event_id          = rc.exception_parent_id
         and rc.sys_generated_exclusion = 1;

   cursor C_PROMO_ERROR_UPDATE is
      select /*+ CARDINALITY(cc, 10) */
             CONFLICT_CHECK_ERROR_REC (rpd.promo_dtl_id,
                                       cc.future_retail_id,
                                       cc.error_type,
                                       cc.error_string,
                                       NULL)
        from table(cast(LP_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_promo_dtl rpd
       where cc.price_event_id           = rpd.exception_parent_id
         and rpd.sys_generated_exclusion = 1;

BEGIN

   -- Clean up the RPM_MERCH_LIST_DETAIL
   delete
     from rpm_merch_list_detail mld
    where mld.merch_list_id IN (select mlh.merch_list_id
                                  from (select /*+ CARDINALITY(ids, 10) */
                                               rpd.promo_dtl_id price_event_id
                                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                               rpm_promo_dtl rpd
                                         where rpd.exception_parent_id     = VALUE(ids)
                                           and LP_price_event_type         IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                                               RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                                           and rpd.sys_generated_exclusion = 1
                                        union all
                                        select /*+ CARDINALITY(ids, 10) */
                                               rpc.price_change_id price_event_id
                                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                               rpm_price_change rpc
                                         where rpc.exception_parent_id     = VALUE(ids)
                                           and LP_price_event_type         = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                           and rpc.sys_generated_exclusion = 1
                                       union all
                                        select /*+ CARDINALITY(ids, 10) */
                                               rc.clearance_id price_event_id
                                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                               rpm_clearance rc
                                         where rc.exception_parent_id     = VALUE(ids)
                                           and LP_price_event_type        = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                           and rc.sys_generated_exclusion = 1) cc,
                                       rpm_merch_list_head mlh
                                 where cc.price_event_id   = mlh.price_event_id
                                   and mlh.merch_list_type = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE);

   if I_cc_error_tbl is NOT NULL and
      I_cc_error_tbl.COUNT > 0 then

      -- Insert the items from the new Conflict Check collection.
      -- This together with the delete above, will effectively clear out all items that were previously in conflict,
      -- and just leave the items that are currently in conflict.
      insert into rpm_merch_list_detail(merch_list_id,
                                        merch_level,
                                        item,
                                        item_desc,
                                        diff_id,
                                        diff_desc)
      select mlh.merch_list_id,
             case
                when pe.cur_hier_level IN (SUBSTR(RPM_CONSTANTS.FR_HIER_ITEM_ZONE, 0, 1),
                                           SUBSTR(RPM_CONSTANTS.FR_HIER_ITEM_LOC, 0, 1)) then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                when pe.cur_hier_level IN (SUBSTR(RPM_CONSTANTS.FR_HIER_PARENT_ZONE, 0, 1),
                                           SUBSTR(RPM_CONSTANTS.FR_HIER_PARENT_LOC, 0, 1)) then
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
                when pe.cur_hier_level IN (SUBSTR(RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 0, 1),
                                           SUBSTR(RPM_CONSTANTS.FR_HIER_DIFF_LOC, 0, 1)) then
                   RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
             end,
             im.item,
             im.item_desc,
             case
                when pe.cur_hier_level IN (SUBSTR(RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 0, 1),
                                           SUBSTR(RPM_CONSTANTS.FR_HIER_DIFF_LOC, 0, 1)) then
                   im.diff_1
                else
                   NULL
             end, --diff_id
             case
                when pe.cur_hier_level IN (SUBSTR(RPM_CONSTANTS.FR_HIER_DIFF_ZONE, 0, 1),
                                           SUBSTR(RPM_CONSTANTS.FR_HIER_DIFF_LOC, 0, 1)) then
                   d.diff_desc
                else
                   NULL
             end --im.diff_desc
        from -- get the exclusion Id for the parent price event passed in
             (select /*+ CARDINALITY(ids, 10) */
                     distinct rpd.promo_dtl_id price_event_id,
                     gtt.item,
                     SUBSTR(gtt.cur_hier_level, 0, 1) cur_hier_level
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_promo_dtl rpd
               where gtt.price_event_id          = VALUE(ids)
                 and rpd.exception_parent_id     = gtt.price_event_id
                 and LP_price_event_type         IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                 and rpd.sys_generated_exclusion = 1
              union all
              select /*+ CARDINALITY(ids, 10) */
                     distinct rpc.price_change_id price_event_id,
                     gtt.item,
                     SUBSTR(gtt.cur_hier_level, 0, 1) cur_hier_level
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_price_change rpc
               where gtt.price_event_id          = VALUE(ids)
                 and rpc.exception_parent_id     = gtt.price_event_id
                 and LP_price_event_type         = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and rpc.sys_generated_exclusion = 1
              union all
              select /*+ CARDINALITY(ids, 10) */
                     distinct rc.clearance_id price_event_id,
                     gtt.item,
                     SUBSTR(gtt.cur_hier_level, 0, 1) cur_hier_level
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_cc_sys_gen_detail_gtt gtt,
                     rpm_clearance rc
               where gtt.price_event_id         = VALUE(ids)
                 and rc.exception_parent_id     = gtt.price_event_id
                 and LP_price_event_type        = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and rc.sys_generated_exclusion = 1) pe,
             rpm_merch_list_head mlh,
             item_master im,
             diff_ids d
       where pe.price_event_id   = mlh.price_event_id
         and mlh.merch_list_type = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and pe.item             = im.item
         and im.diff_1           = d.diff_id (+);

   end if;

   open C_MERCH_HEAD_DEL;
   fetch C_MERCH_HEAD_DEL BULK COLLECT into L_merch_list_head_deleted;
   close C_MERCH_HEAD_DEL;

   if L_merch_list_head_deleted is NOT NULL and
      L_merch_list_head_deleted.COUNT > 0 then

      forall i IN 1..L_merch_list_head_deleted.COUNT
         delete
           from rpm_merch_list_head
          where price_event_id  = L_merch_list_head_deleted(i)
            and merch_list_type = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE;

      -- delete from the price event tables if the RPM_MECH_LIST_HEAD is deleted.
      if LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

         forall i IN 1..L_merch_list_head_deleted.COUNT
            delete
              from rpm_promo_dtl_merch_node
             where promo_dtl_id = L_merch_list_head_deleted(i);

         forall i IN 1..L_merch_list_head_deleted.COUNT
            delete
              from rpm_promo_zone_location
             where promo_dtl_id = L_merch_list_head_deleted(i);

         delete
           from rpm_promo_dtl_disc_ladder pddl
          where pddl.promo_dtl_list_id IN (select /*+ CARDINALITY(ids, 10) */
                                                  pdl.promo_dtl_list_id
                                             from table(cast(L_merch_list_head_deleted as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_promo_dtl_list pdl,
                                                  rpm_promo_dtl_list_grp pdlg
                                            where pdlg.promo_dtl_id         = VALUE(ids)
                                              and pdl.promo_dtl_list_grp_id = pdlg.promo_dtl_list_grp_id);

         delete
           from rpm_promo_dtl_list pdl
          where pdl.promo_dtl_list_grp_id IN (select /*+ CARDINALITY(ids, 10) */
                                                     pdlg.promo_dtl_list_grp_id
                                                from table(cast(L_merch_list_head_deleted as OBJ_NUMERIC_ID_TABLE)) ids,
                                                     rpm_promo_dtl_list_grp pdlg
                                               where pdlg.promo_dtl_id = VALUE(ids));

         forall i IN 1..L_merch_list_head_deleted.COUNT
            delete
              from rpm_promo_dtl_list_grp
             where promo_dtl_id = L_merch_list_head_deleted(i);

         forall i IN 1..L_merch_list_head_deleted.COUNT
            delete
              from rpm_promo_dtl
             where promo_dtl_id = L_merch_list_head_deleted(i);

        open C_PROMO_ERROR_UPDATE;
        fetch C_PROMO_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
        close C_PROMO_ERROR_UPDATE;

      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

         forall i IN 1..L_merch_list_head_deleted.COUNT
            delete
              from rpm_clearance
             where clearance_id = L_merch_list_head_deleted(i);

         open C_CL_ERROR_UPDATE;
         fetch C_CL_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
         close C_CL_ERROR_UPDATE;

      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         forall i IN 1..L_merch_list_head_deleted.COUNT
            delete
              from rpm_price_change
             where price_change_id = L_merch_list_head_deleted(i);

         open C_PC_ERROR_UPDATE;
         fetch C_PC_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
         close C_PC_ERROR_UPDATE;

      end if;

   end if;

   -- update the LP_cc_error_tbl to save the SGE price event ID to be inserted
   -- to conflict check error table
   if LP_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                              RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

      open C_PROMO_ERROR_UPDATE;
      fetch C_PROMO_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
      close C_PROMO_ERROR_UPDATE;

   elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      open C_CL_ERROR_UPDATE;
      fetch C_CL_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
      close C_CL_ERROR_UPDATE;

   elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_PC_ERROR_UPDATE;
      fetch C_PC_ERROR_UPDATE BULK COLLECT into LP_cc_error_tbl;
      close C_PC_ERROR_UPDATE;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_SGE_DATA;

--------------------------------------------------------------------------------
FUNCTION SKIP_CC_PROCESS(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type IN     VARCHAR2,
                         I_bulk_cc_pe_id    IN     NUMBER,
                         I_rib_trans_id     IN     NUMBER,
                         I_end_state        IN     VARCHAR2,
                         I_persist_ind      IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_SQL.SKIP_CC_PROCESS';

   L_promo_type NUMBER := NULL;
   
   cursor C_CANCEL_IL_OF_ACTIVE_DTL is
      select /*+ CARDINALITY(ids 10) */
             DECODE(SUM(NVL(cancel_il_promo_dtl_ind, 0)),
                    0, 0,
                    1)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where promo_dtl_id = value(ids);

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then
      L_promo_type := RPM_CONSTANTS.TRANSACTION_CODE;
   else
      L_promo_type := RPM_CONSTANTS.COMPLEX_CODE;
   end if;

   if I_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE and
      I_persist_ind = 'Y' then

      if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(IO_cc_error_tbl,
                                            I_rib_trans_id,
                                            I_bulk_cc_pe_id,
                                            I_price_event_ids,
                                            I_price_event_type,
                                            L_promo_type,
                                            RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE) = 0 then
         return 0;
      end if;

   elsif I_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE and
         I_persist_ind = 'Y' then

      if RPM_CC_PUBLISH.STAGE_PROM_REMOVE_MESSAGES(IO_cc_error_tbl,
                                                   I_rib_trans_id,
                                                   I_bulk_cc_pe_id,
                                                   I_price_event_type,
                                                   I_price_event_ids) = 0 then -- chunk_number
         return 0;
      end if;

   elsif I_end_state IN (RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                         RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) then

       open C_CANCEL_IL_OF_ACTIVE_DTL;
      fetch C_CANCEL_IL_OF_ACTIVE_DTL into LP_cancel_il_promo_dtl;
      close C_CANCEL_IL_OF_ACTIVE_DTL;
      
      if LP_cancel_il_promo_dtl = 1 then 
          if RPM_CC_PUBLISH.STAGE_PROM_CIL_MESSAGES(IO_cc_error_tbl,
                                                    I_rib_trans_id,
                                                    I_price_event_ids) = 0 then
             return 0;
          end if;
         
      else
          if RPM_CC_PUBLISH.STAGE_PROM_MESSAGES(IO_cc_error_tbl,
                                                I_rib_trans_id,
                                                I_bulk_cc_pe_id,
                                                I_price_event_ids,
                                                I_price_event_type,
                                                RPM_CONSTANTS.COMPLEX_CODE,
                                                RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD) = 0 then
               return 0;
          end if;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END SKIP_CC_PROCESS;

--------------------------------------------------------------------------------

FUNCTION POPULATE_CC_ERROR_TABLE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_SQL.POPULATE_CC_ERROR_TABLE';

   L_display_conflicts_only RPM_SYSTEM_OPTIONS.DISPLAY_CONFLICTS_ONLY%TYPE := NULL;

   L_con_check_err_id NUMBER(15) := NULL;
   L_days_before      NUMBER(3)  := NULL;
   L_days_after       NUMBER(3)  := NULL;
   L_effective_date   DATE       := NULL;
   L_fr_start_date    DATE       := NULL;
   L_fr_end_date      DATE       := NULL;

   cursor C_CONFLICT_PE is
      select price_event_id,
             error_string,
             item,
             location,
             dept,
             customer_type,
             cust_seg_ind,
             cc_fr_id,
             cs_promo_fr_id,
             diff_id,
             merch_hier_level,
             zone_node_type
        from (select distinct t2.price_event_id,
                     t2.error_string,
                     fr.item,
                     fr.location,
                     fr.dept,
                     NULL customer_type,
                     case
                        when cs_promo_fr_id is NULL then
                           0
                        else
                           1
                     end cust_seg_ind,
                     cc_fr_id,
                     cs_promo_fr_id,
                     fr.diff_id,
                     case
                        when cur_hier_level IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                RPM_CONSTANTS.FR_HIER_PARENT_LOC) then
                           1
                        when cur_hier_level IN (RPM_CONSTANTS.FR_HIER_DIFF_ZONE,
                                                RPM_CONSTANTS.FR_HIER_DIFF_LOC) then
                           2
                        when cur_hier_level IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                                RPM_CONSTANTS.FR_HIER_ITEM_LOC) then
                           3
                     end merch_hier_level,
                     fr.zone_node_type,
                     ROW_NUMBER() OVER (PARTITION BY t2.price_event_id,
                                                     fr.item,
                                                     fr.location
                                            ORDER BY fr.future_retail_id) pe_rownum
                from (select price_event_id,
                             cc_fr_id,
                             error_string,
                             cs_promo_fr_id
                        from (select /*+ CARDINALITY (cc 10) */
                                     cc.price_event_id,
                                     cc.future_retail_id cc_fr_id,
                                     cc.error_string,
                                     cc.cs_promo_fr_id
                                from table(cast(IO_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
                               where cc.error_type = RPM_CONSTANTS.CONFLICT_ERROR) t1) t2,
                     rpm_future_retail_gtt fr
               where fr.future_retail_id (+)    = t2.cc_fr_id
                 and (   t2.cs_promo_fr_id      is NULL
                      or (    t2.cs_promo_fr_id is NOT NULL
                          and NOT EXISTS (select 'x'
                                            from rpm_cust_segment_promo_fr_gtt cspf
                                           where cspf.cust_segment_promo_id = t2.cs_promo_fr_id)))
              union all
              select distinct t2.price_event_id,
                     t2.error_string,
                     cspf.item,
                     cspf.location,
                     cspf.dept,
                     cspf.customer_type,
                     1 cust_seg_ind,
                     cc_fr_id,
                     cs_promo_fr_id,
                     cspf.diff_id,
                     case
                        when cur_hier_level IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                RPM_CONSTANTS.FR_HIER_PARENT_LOC) then
                           1
                        when cur_hier_level IN (RPM_CONSTANTS.FR_HIER_DIFF_ZONE,
                                                RPM_CONSTANTS.FR_HIER_DIFF_LOC) then
                           2
                        when cur_hier_level IN (RPM_CONSTANTS.FR_HIER_ITEM_ZONE,
                                                RPM_CONSTANTS.FR_HIER_ITEM_LOC) then
                           3
                     end merch_hier_level,
                     cspf.zone_node_type,
                     ROW_NUMBER() OVER (PARTITION BY t2.price_event_id,
                                                     cspf.item,
                                                     cspf.location
                                            ORDER BY cspf.cust_segment_promo_id) pe_rownum
                from (select price_event_id,
                             cc_fr_id,
                             error_string,
                             cs_promo_fr_id
                        from (select /*+ CARDINALITY (cc 10) */
                                     cc.price_event_id,
                                     cc.future_retail_id cc_fr_id,
                                     cc.error_string,
                                     cc.cs_promo_fr_id
                                from table(cast(IO_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc
                               where cc.error_type     = RPM_CONSTANTS.CONFLICT_ERROR
                                 and cc.cs_promo_fr_id is NOT NULL) t1) t2,
                     rpm_cust_segment_promo_fr_gtt cspf
               where cspf.cust_segment_promo_id = t2.cs_promo_fr_id)
       where pe_rownum = 1;

   cursor C_PC(I_price_change_id IN NUMBER) is
      select pc.price_change_id,
             pc.price_change_display_id,
             pc.change_type,
             pc.change_amount,
             pc.change_currency,
             pc.change_percent,
             pc.change_selling_uom,
             pc.effective_date
        from rpm_price_change pc
       where pc.price_change_id = I_price_change_id;

   cursor C_CL(I_clearance_id IN NUMBER) is
      select cl.clearance_id,
             cl.clearance_display_id,
             cl.change_type,
             cl.change_amount,
             cl.change_currency,
             cl.change_percent,
             cl.change_selling_uom,
             cl.effective_date
        from rpm_clearance cl
       where cl.clearance_id = I_clearance_id;

   cursor C_CR(I_clearance_id IN NUMBER) is
      select cr.clearance_id,
             cr.clearance_display_id,
             cr.effective_date
        from rpm_clearance_reset cr
       where cr.clearance_id = I_clearance_id;

   cursor C_SP(I_promo_detail_id IN NUMBER) is
      select rpd.promo_dtl_id,
             rpd.promo_dtl_display_id,
             rpc.comp_display_id,
             rpddl.change_type,
             rpddl.change_amount,
             rpddl.change_currency,
             rpddl.change_percent,
             rpddl.change_selling_uom,
             rpd.start_date
        from rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd.promo_dtl_id           = I_promo_detail_id
         and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
         and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
         and rpc.promo_comp_id          = rpd.promo_comp_id;

   cursor C_TH(I_promo_detail_id IN NUMBER) is
      select t.promo_dtl_id,
             t.promo_dtl_display_id,
             t.comp_display_id,
             t.change_type,
             t.change_amount,
             t.change_currency,
             t.change_percent,
             t.change_uom change_selling_uom,
             t.start_date
        from (select rpd.promo_dtl_id,
                     rpd.promo_dtl_display_id,
                     rpc.comp_display_id,
                     rti.change_type,
                     rti.change_amount,
                     rti.change_currency,
                     rti.change_percent,
                     rti.change_uom,
                     rpd.start_date,
                     RANK() OVER (ORDER BY rti.threshold_interval_id desc) rank
                from rpm_promo_dtl rpd,
                     rpm_threshold_interval rti,
                     rpm_promo_comp rpc
               where rpd.promo_dtl_id  = I_promo_detail_id
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 and rti.threshold_id  = rpd.threshold_id) t
       where t.rank = 1;

   cursor C_COMPLEX(I_promo_detail_id IN NUMBER) is
      select t.promo_dtl_id,
             t.promo_dtl_display_id,
             t.comp_display_id,
             t.start_date
        from (select rpd.promo_dtl_id,
                     rpd.promo_dtl_display_id,
                     rpc.comp_display_id,
                     rpd.start_date
                from rpm_promo_dtl rpd,
                     rpm_promo_dtl_list_grp rpdlg,
                     rpm_promo_dtl_list rpdl,
                     rpm_promo_dtl_disc_ladder rpddl,
                     rpm_promo_comp rpc
               where rpd.promo_dtl_id           = I_promo_detail_id
                 and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                 and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                 and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                 and rpc.promo_comp_id          = rpd.promo_comp_id
                 and rpddl.change_type          is NOT NULL
                 and rownum                     = 1) t;

   cursor C_TIMELINE(I_dept          IN NUMBER,
                     I_item          IN VARCHAR2,
                     I_location      IN NUMBER,
                     I_fr_start_date IN DATE,
                     I_fr_end_date   IN DATE,
                     I_fr_id         IN NUMBER) is
      select price_change_id,
             price_change_display_id,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clearance_id,
             clearance_display_id,
             clear_retail,
             clear_retail_currency,
             clear_uom
        from rpm_future_retail_gtt
       where L_display_conflicts_only = 1
         and future_retail_id         = I_fr_id
         and dept                     = I_dept
      union
      select price_change_id,
             price_change_display_id,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clearance_id,
             clearance_display_id,
             clear_retail,
             clear_retail_currency,
             clear_uom
        from rpm_future_retail_gtt
       where dept                     = I_dept
         and L_display_conflicts_only = 0
         and action_date              between I_fr_start_date and I_fr_end_date
         and item                     = I_item
         and location                 = I_location;

   cursor C_PROMO_TIMELINE(I_dept          IN NUMBER,
                           I_item          IN VARCHAR2,
                           I_location      IN NUMBER,
                           I_customer_type IN NUMBER,
                           I_cust_seg_ind  IN NUMBER,
                           I_fr_start_date IN DATE,
                           I_fr_end_date   IN DATE,
                           I_fr_id         IN NUMBER,
                           I_cs_fr_id      IN NUMBER) is
      select a.simple_promo_retail,
             a.simple_promo_retail_currency,
             a.simple_promo_uom,
             a.action_date,
             a.multi_units,
             a.multi_unit_retail,
             a.multi_unit_retail_currency,
             a.multi_selling_uom,
             a.promo_dtl_id,
             a.promo_display_id,
             a.comp_display_id
        from (-- Non-customer segment, display conflicts only
              select fr.item,
                     fr.location,
                     fr.action_date,
                     fr.simple_promo_retail,
                     fr.simple_promo_retail_currency,
                     fr.simple_promo_uom,
                     fr.multi_units,
                     fr.multi_unit_retail,
                     fr.multi_unit_retail_currency,
                     fr.multi_selling_uom,
                     rpile.promo_dtl_id,
                     rpile.promo_display_id,
                     rpile.comp_display_id
                from rpm_future_retail_gtt fr,
                     rpm_promo_item_loc_expl_gtt rpile
               where I_cust_seg_ind           = 0
                 and L_display_conflicts_only = 1
                 and fr.dept                  = I_dept
                 and fr.future_retail_id      = I_fr_id
                 and fr.dept                  = rpile.dept
                 and fr.item                  = rpile.item
                 and fr.location              = rpile.location
                 and rpile.price_event_id     <> rpile.promo_dtl_id
                 and fr.action_date           between rpile.detail_start_date and NVL(rpile.detail_end_date, fr.action_date)
              union all
              -- Non-customer segment, display all data in the window
              select fr.item,
                     fr.location,
                     fr.action_date,
                     fr.simple_promo_retail,
                     fr.simple_promo_retail_currency,
                     fr.simple_promo_uom,
                     fr.multi_units,
                     fr.multi_unit_retail,
                     fr.multi_unit_retail_currency,
                     fr.multi_selling_uom,
                     rpile.promo_dtl_id,
                     rpile.promo_display_id,
                     rpile.comp_display_id
                from rpm_future_retail_gtt fr,
                     rpm_promo_item_loc_expl_gtt rpile
               where L_display_conflicts_only = 0
                 and I_cust_seg_ind           = 0
                 and fr.dept                  = I_dept
                 and fr.item                  = I_item
                 and fr.location              = I_location
                 and fr.dept                  = rpile.dept
                 and fr.item                  = rpile.item
                 and fr.location              = rpile.location
                 and rpile.price_event_id     <> rpile.promo_dtl_id
                 and fr.action_date           between I_fr_start_date and I_fr_end_date
                 and fr.action_date           between rpile.detail_start_date and NVL(rpile.detail_end_date, fr.action_date)
              union all
              select t.item,
                     t.location,
                     t.action_date,
                     t.simple_promo_retail,
                     t.simple_promo_retail_currency,
                     t.simple_promo_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_unit_retail_currency,
                     t.multi_selling_uom,
                     t.promo_dtl_id,
                     t.promo_display_id,
                     t.comp_display_id
                from (-- customer segment specific, display conflicts only
                      select cs.item,
                             cs.location,
                             cs.action_date,
                             cs.simple_promo_retail,
                             cs.simple_promo_retail_currency,
                             cs.simple_promo_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             cs.promo_dtl_id,
                             cs.promo_display_id,
                             cs.comp_display_id,
                             RANK() OVER (PARTITION BY fr.item,
                                                       fr.location
                                              ORDER BY fr.action_date desc) rank
                        from (select cspfr.dept,
                                     cspfr.item,
                                     cspfr.location,
                                     cspfr.action_date,
                                     cspfr.promo_retail simple_promo_retail,
                                     cspfr.promo_retail_currency simple_promo_retail_currency,
                                     cspfr.promo_uom simple_promo_uom,
                                     rpile.promo_dtl_id,
                                     rpile.promo_display_id,
                                     rpile.comp_display_id
                                from rpm_cust_segment_promo_fr_gtt cspfr,
                                     rpm_promo_item_loc_expl_gtt rpile
                               where I_cust_seg_ind              = 1
                                 and L_display_conflicts_only    = 1
                                 and cspfr.cust_segment_promo_id = I_cs_fr_id
                                 and cspfr.dept                  = I_dept
                                 and I_customer_type             is NOT NULL
                                 and cspfr.customer_type         = I_customer_type
                                 and rpile.item                  = cspfr.item
                                 and rpile.location              = cspfr.location
                                 and rpile.dept                  = cspfr.dept
                                 and rpile.customer_type         = cspfr.customer_type
                                 and rpile.price_event_id        <> rpile.promo_dtl_id
                                 and cspfr.action_date           between rpile.detail_start_date and NVL(rpile.detail_end_date, cspfr.action_date)) cs,
                             rpm_future_retail_gtt fr
                       where fr.item         = cs.item
                         and fr.location     = cs.location
                         and fr.dept         = cs.dept
                         and fr.action_date <= cs.action_date
                      union all
                      -- customer segment specific, display all data in the window
                      select cs.item,
                             cs.location,
                             cs.action_date,
                             cs.simple_promo_retail,
                             cs.simple_promo_retail_currency,
                             cs.simple_promo_uom,
                             fr.multi_units,
                             fr.multi_unit_retail,
                             fr.multi_unit_retail_currency,
                             fr.multi_selling_uom,
                             cs.promo_dtl_id,
                             cs.promo_display_id,
                             cs.comp_display_id,
                             RANK() OVER (PARTITION BY fr.item,
                                                       fr.location
                                              ORDER BY fr.action_date desc) rank
                        from (select cspfr.dept,
                                     cspfr.item,
                                     cspfr.location,
                                     cspfr.action_date,
                                     cspfr.promo_retail simple_promo_retail,
                                     cspfr.promo_retail_currency simple_promo_retail_currency,
                                     cspfr.promo_uom simple_promo_uom,
                                     rpile.promo_dtl_id,
                                     rpile.promo_display_id,
                                     rpile.comp_display_id
                                from rpm_cust_segment_promo_fr_gtt cspfr,
                                     rpm_promo_item_loc_expl_gtt rpile
                               where I_cust_seg_ind           = 1
                                 and L_display_conflicts_only = 0
                                 and cspfr.dept               = I_dept
                                 and cspfr.item               = I_item
                                 and cspfr.location           = I_location
                                 and I_customer_type          is NOT NULL
                                 and cspfr.customer_type      = I_customer_type
                                 and cspfr.action_date        between I_fr_start_date and I_fr_end_date
                                 and rpile.item               = cspfr.item
                                 and rpile.location           = cspfr.location
                                 and rpile.dept               = cspfr.dept
                                 and rpile.customer_type      = cspfr.customer_type
                                 and rpile.price_event_id     <> rpile.promo_dtl_id
                                 and cspfr.action_date        between rpile.detail_start_date and NVL(rpile.detail_end_date, cspfr.action_date)) cs,
                             rpm_future_retail_gtt fr
                       where fr.item         = cs.item
                         and fr.location     = cs.location
                         and fr.dept         = cs.dept
                         and fr.action_date <= cs.action_date) t
               where t.rank = 1) a
       order by a.item,
                a.location,
                a.action_date;

BEGIN

   select NVL(display_conflicts_only, 0),
          conflict_history_days_before,
          conflict_history_days_after
     into L_display_conflicts_only,
          L_days_before,
          L_days_after
     from rpm_system_options;

   for rec IN C_CONFLICT_PE loop

      L_con_check_err_id := RPM_CON_CHECK_ERR_SEQ.NEXTVAL;

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         for rec1 IN C_PC(rec.price_event_id) loop

            insert into rpm_con_check_err (con_check_err_id,
                                           ref_class,
                                           ref_id,
                                           ref_display_id,
                                           item,
                                           location,
                                           change_type,
                                           change_amount,
                                           change_currency,
                                           change_percent,
                                           change_selling_uom,
                                           effective_date,
                                           error_date,
                                           message_key,
                                           diff_id,
                                           merch_hier_level,
                                           zone_node_type)
               values(L_con_check_err_id,
                      RPM_CONSTANTS.PC_REF_CLASS,
                      rec1.price_change_id,
                      rec1.price_change_display_id,
                      rec.item,
                      rec.location,
                      rec1.change_type,
                      rec1.change_amount,
                      rec1.change_currency,
                      rec1.change_percent,
                      rec1.change_selling_uom,
                      rec1.effective_date,
                      LP_vdate,
                      rec.error_string,
                      rec.diff_id,
                      rec.merch_hier_level,
                      rec.zone_node_type);

             L_effective_date := rec1.effective_date;

         end loop;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                   RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

         for rec1 IN C_CL(rec.price_event_id) loop

            insert into rpm_con_check_err (con_check_err_id,
                                           ref_class,
                                           ref_id,
                                           ref_display_id,
                                           item,
                                           location,
                                           change_type,
                                           change_amount,
                                           change_currency,
                                           change_percent,
                                           change_selling_uom,
                                           effective_date,
                                           error_date,
                                           message_key,
                                           diff_id,
                                           merch_hier_level,
                                           zone_node_type)
               values(L_con_check_err_id,
                      RPM_CONSTANTS.CL_REF_CLASS,
                      rec1.clearance_id,
                      rec1.clearance_display_id,
                      rec.item,
                      rec.location,
                      rec1.change_type,
                      rec1.change_amount,
                      rec1.change_currency,
                      rec1.change_percent,
                      rec1.change_selling_uom,
                      rec1.effective_date,
                      LP_vdate,
                      rec.error_string,
                      rec.diff_id,
                      rec.merch_hier_level,
                      rec.zone_node_type);

            L_effective_date := rec1.effective_date;

         end loop;

      elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

         for rec1 IN C_CR(rec.price_event_id) loop

            insert into rpm_con_check_err (con_check_err_id,
                                           ref_class,
                                           ref_id,
                                           ref_display_id,
                                           item,
                                           location,
                                           effective_date,
                                           error_date,
                                           message_key)
               values(L_con_check_err_id,
                      RPM_CONSTANTS.CR_REF_CLASS,
                      rec1.clearance_id,
                      rec1.clearance_display_id,
                      rec.item,
                      rec.location,
                      rec1.effective_date,
                      LP_vdate,
                      rec.error_string);

            L_effective_date := rec1.effective_date;

         end loop;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then

         for rec1 IN C_SP(rec.price_event_id) loop

            insert into rpm_con_check_err (con_check_err_id,
                                           ref_class,
                                           ref_id,
                                           ref_display_id,
                                           ref_secondary_display_id,
                                           item,
                                           location,
                                           change_type,
                                           change_amount,
                                           change_currency,
                                           change_percent,
                                           change_selling_uom,
                                           effective_date,
                                           error_date,
                                           message_key,
                                           diff_id,
                                           merch_hier_level,
                                           zone_node_type)
               values(L_con_check_err_id,
                      RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                      rec1.promo_dtl_id,
                      rec1.promo_dtl_display_id,
                      rec1.comp_display_id,
                      rec.item,
                      rec.location,
                      rec1.change_type,
                      rec1.change_amount,
                      rec1.change_currency,
                      rec1.change_percent,
                      rec1.change_selling_uom,
                      rec1.start_date,
                      LP_vdate,
                      rec.error_string,
                      rec.diff_id,
                      rec.merch_hier_level,
                      rec.zone_node_type);

            L_effective_date := rec1.start_date;

         end loop;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD) then

         for rec1 IN C_TH(rec.price_event_id) loop

            insert into rpm_con_check_err (con_check_err_id,
                                           ref_class,
                                           ref_id,
                                           ref_display_id,
                                           ref_secondary_display_id,
                                           item,
                                           location,
                                           change_type,
                                           change_amount,
                                           change_currency,
                                           change_percent,
                                           change_selling_uom,
                                           effective_date,
                                           error_date,
                                           message_key,
                                           diff_id,
                                           merch_hier_level,
                                           zone_node_type)
               values(L_con_check_err_id,
                      RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                      rec1.promo_dtl_id,
                      rec1.promo_dtl_display_id,
                      rec1.comp_display_id,
                      rec.item,
                      rec.location,
                      rec1.change_type,
                      rec1.change_amount,
                      rec1.change_currency,
                      rec1.change_percent,
                      rec1.change_selling_uom,
                      rec1.start_date,
                      LP_vdate,
                      rec.error_string,
                      rec.diff_id,
                      rec.merch_hier_level,
                      rec.zone_node_type);

            L_effective_date := rec1.start_date;

         end loop;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                   RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         for rec1 IN C_COMPLEX(rec.price_event_id) loop

            insert into rpm_con_check_err (con_check_err_id,
                                           ref_class,
                                           ref_id,
                                           ref_display_id,
                                           ref_secondary_display_id,
                                           item,
                                           location,
                                           effective_date,
                                           error_date,
                                           message_key,
                                           diff_id,
                                           merch_hier_level,
                                           zone_node_type)
               values(L_con_check_err_id,
                      RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                      rec1.promo_dtl_id,
                      rec1.promo_dtl_display_id,
                      rec1.comp_display_id,
                      rec.item,
                      rec.location,
                      rec1.start_date,
                      LP_vdate,
                      rec.error_string,
                      rec.diff_id,
                      rec.merch_hier_level,
                      rec.zone_node_type);

            L_effective_date := rec1.start_date;

         end loop;
      end if;

      if L_display_conflicts_only = 0 then

         L_fr_start_date := L_effective_date - L_days_before;
         L_fr_end_date   := L_effective_date + L_days_after;

      end if;

      for rec2 IN C_TIMELINE(rec.dept,
                             rec.item,
                             rec.location,
                             L_fr_start_date,
                             L_fr_end_date,
                             rec.cc_fr_id) loop

         if rec2.price_change_id is NOT NULL then

            insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                                  con_check_err_id,
                                                  ref_class,
                                                  ref_id,
                                                  ref_display_id,
                                                  ref_secondary_display_id,
                                                  effective_date,
                                                  selling_retail,
                                                  selling_retail_currency,
                                                  selling_uom,
                                                  multi_units,
                                                  multi_unit_retail,
                                                  multi_unit_retail_currency,
                                                  multi_unit_uom)
               values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                      L_con_check_err_id,
                      RPM_CONSTANTS.PC_REF_CLASS,
                      rec2.price_change_id,
                      rec2.price_change_display_id,
                      NULL,
                      rec2.action_date,
                      rec2.selling_retail,
                      rec2.selling_retail_currency,
                      rec2.selling_uom,
                      rec2.multi_units,
                      rec2.multi_unit_retail,
                      rec2.multi_unit_retail_currency,
                      rec2.multi_selling_uom);

         end if;

         if rec2.clearance_id is NOT NULL then

            insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                                  con_check_err_id,
                                                  ref_class,
                                                  ref_id,
                                                  ref_display_id,
                                                  effective_date,
                                                  selling_retail,
                                                  selling_retail_currency,
                                                  selling_uom,
                                                  multi_units,
                                                  multi_unit_retail,
                                                  multi_unit_retail_currency,
                                                  multi_unit_uom)
              values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                     L_con_check_err_id,
                     RPM_CONSTANTS.CL_REF_CLASS,
                     rec2.clearance_id,
                     rec2.clearance_display_id,
                     rec2.action_date,
                     rec2.clear_retail,
                     rec2.clear_retail_currency,
                     rec2.clear_uom,
                     rec2.multi_units,
                     rec2.multi_unit_retail,
                     rec2.multi_unit_retail_currency,
                     rec2.multi_selling_uom);

         end if;

      end loop;  -- end loop on C_TIMELINE

      for rec2 IN C_PROMO_TIMELINE(rec.dept,
                                   rec.item,
                                   rec.location,
                                   rec.customer_type,
                                   rec.cust_seg_ind,
                                   L_fr_start_date,
                                   L_fr_end_date,
                                   rec.cc_fr_id,
                                   rec.cs_promo_fr_id) loop

         insert into rpm_con_check_err_detail (con_check_err_detail_id,
                                               con_check_err_id,
                                               ref_class,
                                               ref_id,
                                               ref_display_id,
                                               ref_secondary_display_id,
                                               effective_date,
                                               selling_retail,
                                               selling_retail_currency,
                                               selling_uom,
                                               multi_units,
                                               multi_unit_retail,
                                               multi_unit_retail_currency,
                                               multi_unit_uom)
            values(RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                   L_con_check_err_id,
                   RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                   rec2.promo_dtl_id,
                   rec2.promo_display_id,
                   rec2.comp_display_id,
                   rec2.action_date,
                   rec2.simple_promo_retail,
                   rec2.simple_promo_retail_currency,
                   rec2.simple_promo_uom,
                   rec2.multi_units,
                   rec2.multi_unit_retail,
                   rec2.multi_unit_retail_currency,
                   rec2.multi_selling_uom);

      end loop;

   end loop; -- end loop on C_CONFLICT_PE

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_CC_ERROR_TABLE;

--------------------------------------------------------------------------------

FUNCTION POPULATE_CC_ERROR_PE_ITEM_LOC(IO_cc_error_tbl IN OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_FUTURE_RETAIL_SQL.POPULATE_CC_ERROR_PE_ITEM_LOC';

BEGIN

   insert into rpm_cce_fr_itemloc_gtt(future_retail_id,
                                      item,
                                      zone_node_type,
                                      location,
                                      cust_segment_promo_id)
      select /*+ CARDINALITY (ccet 10) */
             rfr.future_retail_id,
             rfr.item,
             rfr.zone_node_type,
             rfr.location,
             NULL
        from table(cast(IO_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
             rpm_future_retail_gtt rfr
       where ccet.price_event_id   = rfr.price_event_id
         and ccet.future_retail_id = rfr.future_retail_id
     union all
      select /*+ CARDINALITY (ccet 10) */
             NULL,
             csfr.item,
             csfr.zone_node_type,
             csfr.location,
             csfr.cust_segment_promo_id
        from table(cast(IO_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
             rpm_cust_segment_promo_fr_gtt csfr
       where ccet.price_event_id = csfr.price_event_id
         and ccet.cs_promo_fr_id = csfr.cust_segment_promo_id;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_CC_ERROR_PE_ITEM_LOC;

--------------------------------------------------------------------------------

FUNCTION PUSH_BACK_PURGE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(45)  := 'RPM_PURGE_FUTURE_RETAIL_SQL.PUSH_BACK_PURGE';
   L_end_date  DATE          := TO_DATE('3000', 'YYYY');

BEGIN

   delete
     from rpm_future_retail rfr
    where rfr.rowid IN (select rfrg.rfr_rowid
                          from rpm_future_retail_gtt rfrg
                         where rfrg.timeline_seq = -999);

   delete
     from rpm_cust_segment_promo_fr cspf
    where cspf.rowid IN (select cspfg.cspfr_rowid
                           from rpm_cust_segment_promo_fr_gtt cspfg,
                                rpm_promo_item_loc_expl_gtt rpileg
                          where rpileg.deleted_ind            = 1
                            and rpileg.dept                   = cspfg.dept
                            and rpileg.item                   = cspfg.item
                            and NVL(rpileg.diff_id, '-99999') = NVL(cspfg.diff_id, '-99999')
                            and rpileg.location               = cspfg.location
                            and rpileg.zone_node_type         = cspfg.zone_node_type
                            and rpileg.customer_type          is NOT NULL
                            and rpileg.customer_type          = cspfg.customer_type
                            and cspfg.action_date             BETWEEN rpileg.detail_start_date and
                                                                      NVL(rpileg.detail_end_date + RPM_CONSTANTS.ONE_MINUTE, L_end_date)
                         union
                         select cspfg1.cspfr_rowid
                           from rpm_cust_segment_promo_fr_gtt cspfg1
                          where NOT EXISTS (select 'x'
                                              from rpm_promo_item_loc_expl_gtt rpileg3
                                             where rpileg3.dept                   = cspfg1.dept
                                               and rpileg3.item                   = cspfg1.item
                                               and NVL(rpileg3.diff_id, '-99999') = NVL(cspfg1.diff_id, '-99999')
                                               and rpileg3.location               = cspfg1.location
                                               and rpileg3.zone_node_type         = cspfg1.zone_node_type
                                               and rpileg3.customer_type          is NOT NULL
                                               and rpileg3.customer_type          = cspfg1.customer_type
                                               and NVL(rpileg3.deleted_ind, 0)    = 0));

   delete
     from rpm_promo_item_loc_expl rpile
    where rpile.rowid IN (select rpileg.rpile_rowid
                            from rpm_promo_item_loc_expl_gtt rpileg
                           where rpileg.deleted_ind = 1);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));

      return 0;

END PUSH_BACK_PURGE;
--------------------------------------------------------------------------------

END RPM_FUTURE_RETAIL_SQL;
/
