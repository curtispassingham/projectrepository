CREATE OR REPLACE PACKAGE RPM_CHUNK_CC_PUSH_BACK_SQL AS
--------------------------------------------------------------------------------
FUNCTION GET_CHUNKS_FOR_PB(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                           O_chunk_info              OUT OBJ_CHUNKS_FOR_PB_BATCH_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_CHUNK_CC_PUSH_BACK_SQL;
/