CREATE OR REPLACE PACKAGE RPM_DATA_VALIDATION_SQL AS
--------------------------------------------------------------------------------
FUNCTION FIND_DUPLICATE_FR_DATA (O_error_message    OUT VARCHAR2,
                                 I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                                 I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                                 I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER;
---------------------------------------------------------------------------------
FUNCTION FIND_MISSING_FR_DATA (O_error_message    OUT VARCHAR2,
                               I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                               I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                               I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER;
---------------------------------------------------------------------------------
END RPM_DATA_VALIDATION_SQL;
/