CREATE OR REPLACE PACKAGE BODY RPM_BULK_CC_THREADING_SQL AS

   LP_bulk_cc_pe_id NUMBER(10)   := NULL;
   LP_user_name     VARCHAR2(60) := NULL;
   LP_batch_name    VARCHAR2(60):= NULL;
   LP_vdate         DATE         := GET_VDATE;

   LP_chunk_cc_factor        NUMBER := 0;
   LP_bulk_thread_il_count   NUMBER := 0;
   LP_chunk_thread_il_count  NUMBER := 0;
   LP_sys_gen_excl_reprocess NUMBER := 0;

   LP_skip_cc_for_cp_approval RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_CP_APPROVAL%TYPE := NULL;

   LP_return_code NUMBER         := NULL;
   LP_error_msg   VARCHAR2(2000) := NULL;

--------------------------------------------------------------------------------
--                            PRIVATE PROTOTYPE                               --
--------------------------------------------------------------------------------

FUNCTION CHECK_OVERLAP(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                       O_max_seq_number       OUT NUMBER,
                       I_price_event_type  IN     VARCHAR2,
                       I_need_explode      IN     NUMBER,
                       I_price_event_count IN     NUMBER)
RETURN NUMBER;

FUNCTION CHECK_FINANCE_PROMO_OVERLAP(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                     O_max_seq_number      OUT NUMBER,
                                     I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION RANK_PRICE_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_bulk_cc_pe_id    IN     NUMBER,
                          I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

FUNCTION EXPLODE_PRICE_CHANGES(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION EXPLODE_CLEARANCES(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION EXPLODE_CLEARANCE_RESETS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION EXPLODE_PROMOS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

FUNCTION POPULATE_CC_RESULT_TABLE(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                                  I_bulk_cc_pe_id    IN     NUMBER,
                                  I_pe_sequence_id   IN     NUMBER,
                                  I_pe_thread_number IN     NUMBER,
                                  I_price_event_type IN     VARCHAR2,
                                  I_error_string     IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;

FUNCTION CHECK_SYS_GEN_EXCL_ELIG(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type   IN     VARCHAR2,
                                 I_bulk_cc_pe_id      IN     NUMBER,
                                 I_pe_sequence_number IN     NUMBER)
RETURN NUMBER;

FUNCTION BULK_CREATE_UPDATE_PROMO_DEAL(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_user_name       IN     VARCHAR2)
RETURN NUMBER;

FUNCTION DEAL_DELETE_CC(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                        I_bulk_cc_pe_id IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------------------------------
--                            PUBLIC PROCEDURES                               --
--------------------------------------------------------------------------------
FUNCTION INIT_BULK_CC_PE(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                         I_bulk_cc_pe_id           IN     NUMBER,
                         I_secondary_bulk_cc_pe_id IN     NUMBER,
                         I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type        IN     VARCHAR2,
                         I_persist_ind             IN     VARCHAR2,
                         I_start_state             IN     VARCHAR2,
                         I_end_state               IN     VARCHAR2,
                         I_user_name               IN     VARCHAR2,
                         I_emergency_perm          IN     NUMBER,
                         I_asynch_ind              IN     NUMBER,
                         I_cc_request_group_id     IN     NUMBER,
                         I_auto_clean_ind          IN     NUMBER,
                         I_need_il_explode         IN     NUMBER DEFAULT 1,
                         I_online_alert_ind        IN     NUMBER,
                         I_thread_processor_class  IN     VARCHAR2,
                         I_na_single_txn_ind       IN     NUMBER DEFAULT 0,
                         I_new_promo_end_date      IN     DATE DEFAULT NULL,
                         I_old_clr_oostock_date    IN     DATE DEFAULT NULL,
                         I_old_clr_reset_date      IN     DATE DEFAULT NULL,
                         I_old_promo_end_date      IN     DATE DEFAULT NULL,
                         I_new_clr_reset_date      IN     DATE DEFAULT NULL,
                         I_old_timebased_dtl_ind   IN     NUMBER DEFAULT NULL,
                         I_new_timebased_dtl_ind   IN     NUMBER DEFAULT NULL,
                         I_sys_gen_excl_reprocess  IN     NUMBER DEFAULT 0)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BULK_CC_THREADING_SQL.INIT_BULK_CC_PE';

   L_con_check_err_ids OBJ_NUMERIC_ID_TABLE := NULL;

BEGIN

   insert into rpm_bulk_cc_pe
      (bulk_cc_pe_id,
       price_event_type,
       persist_ind,
       start_state,
       end_state,
       user_name,
       emergency_perm,
       secondary_bulk_cc_pe_id,
       secondary_ind,
       asynch_ind,
       cc_request_group_id,
       auto_clean_ind,
       need_il_explode_ind,
       online_alert_ind,
       thread_processor_class,
       non_asynch_single_txn_ind,
       new_promo_end_date,
       old_clr_out_of_stock_date,
       old_clr_reset_date,
       old_promo_end_date,
       new_clr_reset_date,
       old_timebased_dtl_ind,
       new_timebased_dtl_ind,
       sys_gen_excl_reprocess)
   values
      (I_bulk_cc_pe_id,
       I_price_event_type,
       I_persist_ind,
       I_start_state,
       I_end_state,
       I_user_name,
       I_emergency_perm,
       I_secondary_bulk_cc_pe_id,
       0,
       NVL(I_asynch_ind, 0),
       I_cc_request_group_id,
       I_auto_clean_ind,
       I_need_il_explode,
       I_online_alert_ind,
       I_thread_processor_class,
       I_na_single_txn_ind,
       I_new_promo_end_date,
       I_old_clr_oostock_date,
       I_old_clr_reset_date,
       I_old_promo_end_date,
       I_new_clr_reset_date,
       I_old_timebased_dtl_ind,
       I_new_timebased_dtl_ind,
       I_sys_gen_excl_reprocess);

   insert into rpm_bulk_cc_pe_thread
      (bulk_cc_pe_id,
       price_event_id,
       price_event_type,
       elig_for_sys_gen_exclusions)
      select distinct
             I_bulk_cc_pe_id,
             VALUE(ids),
             I_price_event_type,
             0
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids;

   -- Clean up the conflict check error table
   delete rpm_con_check_err err
    where ref_class = DECODE(I_price_event_type,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,           RPM_CONSTANTS.PC_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,              RPM_CONSTANTS.CL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,        RPM_CONSTANTS.CL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,    RPM_CONSTANTS.CR_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,       RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,    RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,      RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,  RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,  RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,  RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO, RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,    RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION,      RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,          RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,       RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,         RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,     RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,    RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,    RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,   RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE,   RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                             RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD,  RPM_CONSTANTS.COMP_DETAIL_REF_CLASS)
      and ref_id    IN (select VALUE(ids)
                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
                        union all
                        select /*+ CARDINALITY(ids 10)*/ rpd.promo_dtl_id
                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                               rpm_promo_dtl rpd
                         where rpd.exception_parent_id     = VALUE(ids)
                           and I_price_event_type          IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                               RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                               RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                           and rpd.sys_generated_exclusion = 1
                        union all
                        select /*+ CARDINALITY(ids 10)*/ rpc.price_change_id
                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                               rpm_price_change rpc
                         where rpc.exception_parent_id     = VALUE(ids)
                           and I_price_event_type          = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                           and rpc.sys_generated_exclusion = 1
                        union all
                        select /*+ CARDINALITY(ids 10)*/ rc.clearance_id
                          from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                               rpm_clearance rc
                         where rc.exception_parent_id     = VALUE(ids)
                           and I_price_event_type         = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                           and rc.sys_generated_exclusion = 1)
   returning err.con_check_err_id BULK COLLECT into L_con_check_err_ids;

   forall i IN 1 .. L_con_check_err_ids.COUNT
      delete
        from rpm_con_check_err_detail
       where con_check_err_id    = L_con_check_err_ids(i);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INIT_BULK_CC_PE;

--------------------------------------------------------------------------------
FUNCTION THREAD_OVERLAP_PRICE_EVENTS(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                                     O_max_seq_number    OUT NUMBER,
                                     I_bulk_cc_pe_id  IN     NUMBER,
                                     I_need_explode   IN     NUMBER DEFAULT 1)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_THREADING_SQL.THREAD_OVERLAP_PRICE_EVENTS';

   L_price_event_type VARCHAR2(3)          := NULL;
   L_end_state        VARCHAR2(60)         := NULL;
   L_persist_ind      VARCHAR2(1)          := NULL;
   L_price_event_ids  OBJ_NUMERIC_ID_TABLE := NULL;
   L_user_name        VARCHAR2(30)         := NULL;
   L_batch_name	      VARCHAR2(30)	       := NULL;
   L_start_time       TIMESTAMP            := SYSTIMESTAMP;

   --Instrumentation
   L_trace_name VARCHAR2(10)	 						 := 'TOPE';
   
   cursor C_PE_IDS is
      select price_event_id
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id               = I_bulk_cc_pe_id
         and man_txn_exclusion_parent_id is NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id);

   -- Get Price Event Type
   select end_state,
          price_event_type,
          persist_ind,
          user_name,
          case
             when REGEXP_LIKE (user_name, '[Nn]ew') then
                'NIL'
             when REGEXP_LIKE (user_name, '[Ii]njector') then
                'INJ'
             when REGEXP_LIKE (user_name, '[Ll]oc') then
                'LM'
          end batch_name,
          NVL(sys_gen_excl_reprocess, 0) into L_end_state,
                                              L_price_event_type,
                                              L_persist_ind,
                                              L_user_name,
                                              L_batch_name,
                                              LP_sys_gen_excl_reprocess
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (L_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           L_batch_name,
                                           L_program||' FOR BULK_CC_PE_ID '||I_bulk_cc_pe_id,
                                           RPM_CONSTANTS.LOG_STARTED,
                                           'Start of '||L_program||' for ' ||L_price_event_type);

         RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(L_batch_name||L_trace_name);

      end if;

   EXCEPTION

      when OTHERS then
         goto THREAD_OVERLAP_PRICE_EVENTS_1;

   END;

   <<THREAD_OVERLAP_PRICE_EVENTS_1>>

   -- Stop the whole process L_price_event_type is invalid
   if L_price_event_type NOT IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                 RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                 RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                 RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                                 RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                 RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION_UPD,
                                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                 RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                 RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    L_program,
                                                                    'INVALID PRICE EVENT TYPE',
                                                                    L_price_event_type)));

      return 0;
   end if;

   select conflict_check_chunk_factor into LP_chunk_cc_factor
     from rpm_system_options;

   if NVL(LP_chunk_cc_factor, 0) = 0 then
      LP_chunk_cc_factor := RPM_CONSTANTS.CHUNK_CC_RATIO;
   end if;

   select thread_luw_count into LP_bulk_thread_il_count
     from rpm_batch_control
    where program_name = 'com.retek.rpm.app.bulkcc.service.BulkConflictCheckAppService';

   if NVL(LP_bulk_thread_il_count, 0) = 0 then
      LP_bulk_thread_il_count := RPM_CONSTANTS.THREAD_BULK_IL_COUNT;
   end if;

   select thread_luw_count into LP_chunk_thread_il_count
     from rpm_batch_control
    where program_name = 'com.retek.rpm.app.bulkcc.service.ChunkConflictCheckAppService';

   if NVL(LP_chunk_thread_il_count, 0) = 0 then
      LP_chunk_thread_il_count := RPM_CONSTANTS.THREAD_CHUNK_IL_COUNT;
   end if;

   LP_bulk_cc_pe_id := I_bulk_cc_pe_id;
   LP_user_name := L_user_name;
   LP_batch_name := L_batch_name;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION then

      if CHECK_FINANCE_PROMO_OVERLAP(O_cc_error_tbl,
                                     O_max_seq_number,
                                     L_price_event_type) = 0 then

         return 0;
      end if;

   else

      -- Rank the price events
      if RANK_PRICE_EVENT(O_cc_error_tbl,
                          I_bulk_cc_pe_id,
                          L_price_event_type) = 0 then
         return 0;
      end if;

      if L_end_state IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                         TO_CHAR(RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE)) then

         update rpm_bulk_cc_pe_thread
            set rank = rank * - 1
          where bulk_cc_pe_id               = I_bulk_cc_pe_id
            and man_txn_exclusion_parent_id is NULL;

      end if;

      open C_PE_IDS;
      fetch C_PE_IDS BULK COLLECT into L_price_event_ids;
      close C_PE_IDS;

      -- Stop the whole process L_price_event_ids is empty
      if L_price_event_ids is NULL or
         L_price_event_ids.COUNT = 0 then

         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    SQL_LIB.CREATE_MSG('INVALID_PRICE_EVENT',
                                                                       NULL,
                                                                       NULL,
                                                                       NULL)));
         return 0;
      end if;

      if CHECK_OVERLAP(O_cc_error_tbl,
                       O_max_seq_number,
                       L_price_event_type,
                       I_need_explode,
                       L_price_event_ids.COUNT) = 0 then
         return 0;
      end if;

      if L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) and
         L_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then

         if BULK_CREATE_UPDATE_PROMO_DEAL(O_cc_error_tbl,
                                          L_price_event_ids,
                                          LP_bulk_cc_pe_id,
                                          L_user_name) = 0 then

            return 0;
         end if;
      end if;

      if LP_skip_cc_for_cp_approval = 1 and
         L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         update rpm_bulk_cc_pe_thread
            set thread_number    = 1,
                price_event_type = L_price_event_type,
                item_loc_count   = 1,
                status           = 'I'
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

         O_max_seq_number := 1;

         return 1;

      end if;

      merge into rpm_bulk_cc_pe_thread target
      using (select /*+ LEADING(it) USE_HASH(it, lt) */
                    it.bulk_cc_pe_id,
                    it.thread_number,
                    it.price_event_id,
                    (it.itc * lt.locc) item_loc_count
               from (select bulk_cc_pe_id,
                            price_event_id,
                            thread_number,
                            COUNT(1) itc
                       from rpm_bulk_cc_pe_item
                      where bulk_cc_pe_id    = LP_bulk_cc_pe_id
                        and merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                        and rownum           > 0
                      group by bulk_cc_pe_id,
                               price_event_id,
                               thread_number) it,
                    (select bulk_cc_pe_id,
                            price_event_id,
                            COUNT(distinct location) locc
                       from rpm_bulk_cc_pe_location
                      where bulk_cc_pe_id = LP_bulk_cc_pe_id
                        and rownum        > 0
                      group by bulk_cc_pe_id,
                               price_event_id) lt
              where lt.bulk_cc_pe_id (+)  = it.bulk_cc_pe_id
                and lt.price_event_id (+) = it.price_event_id) source
      on (    target.bulk_cc_pe_id               = source.bulk_cc_pe_id
          and target.price_event_id              = source.price_event_id
          and target.man_txn_exclusion_parent_id is NULL)
      when MATCHED then
         update
            set thread_number    = source.thread_number,
                price_event_type = L_price_event_type,
                item_loc_count   = NVL(source.item_loc_count, 0),
                status           = 'I';

      -- Update Price Events that does not have any item/location to sequence 1
      update rpm_bulk_cc_pe_thread target
         set thread_number    = 1,
             price_event_type = L_price_event_type,
             item_loc_count   = 0,
             status           = 'I'
       where bulk_cc_pe_id  = LP_bulk_cc_pe_id
         and thread_number  is NULL
         and NOT EXISTS (select 1
                           from rpm_bulk_cc_pe_item ril
                          where ril.bulk_cc_pe_id    = LP_bulk_cc_pe_id
                            and ril.price_event_id   = target.price_event_id
                            and ril.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE);

   end if;

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (L_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           L_batch_name,
                                           L_program||' FOR BULK_CC_PE_ID '||I_bulk_cc_pe_id,
                                           RPM_CONSTANTS.LOG_COMPLETED,
                                           'End of '||L_program||' for ' ||L_price_event_type);
      end if;

   EXCEPTION
      when OTHERS then
         goto THREAD_OVERLAP_PRICE_EVENTS_2;

   END;

   <<THREAD_OVERLAP_PRICE_EVENTS_2>>

   LOGGER.LOG_TIME(L_program || ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END THREAD_OVERLAP_PRICE_EVENTS;

--------------------------------------------------------------------------------
FUNCTION CHUNK_PRICE_EVENT_THREADS(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                                   O_max_thread_number    OUT NUMBER,
                                   I_bulk_cc_pe_id     IN     NUMBER,
                                   I_pe_seq_number     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_THREADING_SQL.CHUNK_PRICE_EVENT_THREADS';

   L_price_event_type RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE := NULL;

   L_available_threads           NUMBER               := 0;
   L_current_max_thread_no       NUMBER               := 0;
   L_sys_gen_exclusion_tolerance NUMBER               := 0;
   L_start_state_pc              VARCHAR2(50)         := 0;
   L_end_state_pc                VARCHAR2(50)         := 0;
   L_start_state_promo           NUMBER               := 0;
   L_end_state_promo             NUMBER               := 0;
   L_persist_ind                 VARCHAR2(1)          := NULL;
   L_bulk_threads                OBJ_NUMERIC_ID_TABLE := NULL;
   L_start_time                  TIMESTAMP            := SYSTIMESTAMP;

   cursor C_BULK_THREADS is
      select thread_number
        from (select thread_number,
                     COUNT(item_parent) OVER (PARTITION BY thread_number) cnt
                from (select i.item_parent,
                             rbcpt.thread_number
                        from rpm_bulk_cc_pe_thread rbcpt,
                             rpm_bulk_cc_pe_item i,
                             rpm_bulk_cc_pe_location l
                       where rbcpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
                         and rbcpt.parent_thread_number        = I_pe_seq_number
                         and rbcpt.need_chunk                  = 1
                         and rbcpt.man_txn_exclusion_parent_id is NULL
                         and i.price_event_id                  = rbcpt.price_event_id
                         and i.bulk_cc_pe_id                   = I_bulk_cc_pe_id
                         and rbcpt.bulk_cc_pe_id               = i.bulk_cc_pe_id
                         and rbcpt.parent_thread_number        = i.thread_number
                         and i.pe_merch_level                  = i.merch_level_type
                         and i.merch_level_type                = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and i.item_parent                     is NOT NULL
                         and rbcpt.price_event_id              = l.price_event_id
                         and l.bulk_cc_pe_id                   = I_bulk_cc_pe_id
                         and rbcpt.bulk_cc_pe_id               = l.bulk_cc_pe_id
                         and i.bulk_cc_pe_id                   = l.bulk_cc_pe_id
                         and i.price_event_id                  = l.price_event_id
                         and i.itemloc_id                      = l.itemloc_id
                      union all
                      select i.item item_parent,
                             rbcpt.thread_number
                        from rpm_bulk_cc_pe_thread rbcpt,
                             rpm_bulk_cc_pe_item i,
                             rpm_bulk_cc_pe_location l
                       where rbcpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
                         and rbcpt.parent_thread_number        = I_pe_seq_number
                         and rbcpt.need_chunk                  = 1
                         and rbcpt.man_txn_exclusion_parent_id is NULL
                         and i.price_event_id                  = rbcpt.price_event_id
                         and i.bulk_cc_pe_id                   = I_bulk_cc_pe_id
                         and rbcpt.bulk_cc_pe_id               = i.bulk_cc_pe_id
                         and rbcpt.parent_thread_number        = i.thread_number
                         and i.item_parent                     is NULL
                         and i.pe_merch_level                  = i.merch_level_type
                         and i.merch_level_type                IN (RPM_CONSTANTS.PARENT_MERCH_TYPE,
                                                                   RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
                                                                   RPM_CONSTANTS.ITEM_MERCH_TYPE)
                         and rbcpt.price_event_id              = l.price_event_id
                         and l.bulk_cc_pe_id                   = I_bulk_cc_pe_id
                         and rbcpt.bulk_cc_pe_id               = l.bulk_cc_pe_id
                         and i.bulk_cc_pe_id                   = l.bulk_cc_pe_id
                         and i.price_event_id                  = l.price_event_id
                         and i.itemloc_id                      = l.itemloc_id))
       where cnt = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                       ' - I_pe_seq_number: '|| I_pe_seq_number);

   select NVL(thread_luw_count, 0) into LP_bulk_thread_il_count
     from rpm_batch_control
    where program_name = 'com.retek.rpm.app.bulkcc.service.BulkConflictCheckAppService';

   select thread_luw_count into LP_chunk_thread_il_count
     from rpm_batch_control
    where program_name = 'com.retek.rpm.app.bulkcc.service.ChunkConflictCheckAppService';

   select CEIL(SUM(item_loc_count)/LP_bulk_thread_il_count) into L_available_threads
     from rpm_bulk_cc_pe_thread
    where bulk_cc_pe_id               = I_bulk_cc_pe_id
      and thread_number               = I_pe_seq_number
      and parent_thread_number        is NULL
      and man_txn_exclusion_parent_id is NULL;

   select price_event_type into L_price_event_type
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION then

      merge into rpm_bulk_cc_pe_thread target
      using (select rpet.rowid row_id,
                    rpet.thread_number,
                    rpd.promo_dtl_display_id
               from rpm_bulk_cc_pe_thread rpet,
                    rpm_promo_dtl rpd
              where rpet.bulk_cc_pe_id = I_bulk_cc_pe_id
                and rpet.thread_number = I_pe_seq_number
                and rpd.promo_dtl_id   = rpet.price_event_id) source
      on (target.rowid = source.row_id)
      when MATCHED then
         update
            set target.parent_thread_number   = source.thread_number,
                target.thread_number          = 1,
                target.price_event_display_id = source.promo_dtl_display_id;

      O_max_thread_number := 1;

   else
      -- When there's only one thread to process, keep the update to the
      -- rpm_bulk_cc_pe_thread table to a minimum.  There's no need to waste
      -- processing if the end result is the same as a simple update.

      if L_available_threads = 1 then

         update rpm_bulk_cc_pe_thread
            set parent_thread_number = thread_number,
                thread_number        = 1
          where bulk_cc_pe_id        = I_bulk_cc_pe_id
            and thread_number        = I_pe_seq_number
            and parent_thread_number is NULL;

      else

         -- Find all price events that have an item_loc_count >= 50% of the LUW
         -- and then put each into their own thread - setting the need_chunk ind
         -- as necessary
         insert into rpm_bulk_cc_pe_thread rpet (bulk_cc_pe_id,
                                                 thread_number,
                                                 parent_thread_number,
                                                 price_event_id,
                                                 price_event_display_id,
                                                 price_event_type,
                                                 rank,
                                                 price_event_start_date,
                                                 merch_node_type,
                                                 item,
                                                 diff_id,
                                                 dept,
                                                 class,
                                                 subclass,
                                                 link_code,
                                                 zone_node_type,
                                                 zone_node_id,
                                                 item_loc_count,
                                                 status,
                                                 need_chunk,
                                                 elig_for_sys_gen_exclusions,
                                                 man_txn_excl_exists)
            select bulk_cc_pe_id,
                   ROW_NUMBER() OVER (ORDER BY item_loc_count desc) thread_number,
                   thread_number parent_thread_number,
                   price_event_id,
                   price_event_display_id,
                   price_event_type,
                   rank,
                   price_event_start_date,
                   merch_node_type,
                   item,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   link_code,
                   zone_node_type,
                   zone_node_id,
                   item_loc_count,
                   status,
                   case
                      when (item_loc_count/LP_chunk_thread_il_count) >= LP_chunk_cc_factor then
                         1
                      else
                         0
                   end need_chunk,
                   0,
                   man_txn_excl_exists
              from rpm_bulk_cc_pe_thread
             where bulk_cc_pe_id                = I_bulk_cc_pe_id
               and thread_number                = I_pe_seq_number
               and parent_thread_number        is NULL
               and item_loc_count              >= LP_bulk_thread_il_count / 2
               and man_txn_exclusion_parent_id is NULL;

         select NVL(MAX(thread_number), 0) into L_current_max_thread_no
           from rpm_bulk_cc_pe_thread
          where bulk_cc_pe_id               = I_bulk_cc_pe_id
            and parent_thread_number        is NOT NULL
            and parent_thread_number        = I_pe_seq_number
            and man_txn_exclusion_parent_id is NULL;

         L_available_threads := L_available_threads - L_current_max_thread_no;

         -- Assign thread values for the remaining price events...
         insert into rpm_bulk_cc_pe_thread rpet (bulk_cc_pe_id,
                                                 thread_number,
                                                 parent_thread_number,
                                                 price_event_id,
                                                 price_event_display_id,
                                                 price_event_type,
                                                 rank,
                                                 price_event_start_date,
                                                 merch_node_type,
                                                 item,
                                                 diff_id,
                                                 dept,
                                                 class,
                                                 subclass,
                                                 link_code,
                                                 zone_node_type,
                                                 zone_node_id,
                                                 item_loc_count,
                                                 status,
                                                 need_chunk,
                                                 elig_for_sys_gen_exclusions,
                                                 man_txn_excl_exists)
            select bulk_cc_pe_id,
                   -- This logic will ensure that only available threads are assigned to the price events
                   -- by saying that if there are 50 threads available and 10 are already assigned, then
                   -- when the pe_rank value is 50, this will equate to a thread value of 11:
                   -- MOD(50, 50) + 10 + 1 = 0 + 10 + 1 = 11
                   -- Likewise, when the pe_rank value is 49, this will equate to a thread value of 60:
                   -- MOD(49, 50) + 10 + 1 = 49 + 10 + 1 = 60
                   MOD(pe_rank - 1, L_available_threads) + L_current_max_thread_no + 1 thread_number,
                   thread_number parent_thread_number,
                   price_event_id,
                   price_event_display_id,
                   price_event_type,
                   rank,
                   price_event_start_date,
                   merch_node_type,
                   item,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   link_code,
                   zone_node_type,
                   zone_node_id,
                   item_loc_count,
                   status,
                   need_chunk,
                   0,
                   man_txn_excl_exists
              from (select bulk_cc_pe_id,
                           thread_number,
                           price_event_id,
                           item_loc_count,
                           ROW_NUMBER() OVER (ORDER BY item_loc_count desc) pe_rank,
                           price_event_display_id,
                           price_event_type,
                           rank,
                           price_event_start_date,
                           merch_node_type,
                           item,
                           diff_id,
                           dept,
                           class,
                           subclass,
                           link_code,
                           zone_node_type,
                           zone_node_id,
                           status,
                           0 need_chunk,
                           man_txn_excl_exists
                      from rpm_bulk_cc_pe_thread
                     where bulk_cc_pe_id               = I_bulk_cc_pe_id
                       and thread_number               = I_pe_seq_number
                       and parent_thread_number        is NULL
                       and item_loc_count              < LP_bulk_thread_il_count / 2
                       and man_txn_exclusion_parent_id is NULL);

         -- Now that all price events have been assigned a thread to be processed in,
         -- look for threads that will exceed the LUW and break them into multiple
         -- threads.  This will mean that more threads will be generated than was
         -- calculated by the C_COUNT_THREADS cursor above, but that's not an issue.

         loop
            select NVL(MAX(thread_number), 0)
              into L_current_max_thread_no
              from rpm_bulk_cc_pe_thread
             where bulk_cc_pe_id        = I_bulk_cc_pe_id
               and parent_thread_number is NOT NULL
               and parent_thread_number = I_pe_seq_number;

            merge into rpm_bulk_cc_pe_thread rpet
            using (select rowid,
                          bulk_cc_pe_id,
                          thread_number,
                          case
                             when MOD(pe_rank, 2) = 1 then
                                thread_number
                             else
                                L_current_max_thread_no + thread_rank
                          end new_thread_number,
                          parent_thread_number,
                          price_event_id
                     from (-- get only those threads that needs to be split and assign a rank
                           select rowid,
                                  bulk_cc_pe_id,
                                  parent_thread_number,
                                  thread_number,
                                  price_event_id,
                                  item_loc_count,
                                  ROW_NUMBER() OVER (PARTITION BY parent_thread_number,
                                                                  thread_number
                                                         ORDER BY item_loc_count) pe_rank,
                                  DENSE_RANK() OVER (PARTITION BY parent_thread_number
                                                         ORDER BY thread_number) thread_rank
                             from (-- get total item/loc counts for all threads
                                   select rowid,
                                          bulk_cc_pe_id,
                                          parent_thread_number,
                                          thread_number,
                                          price_event_id,
                                          item_loc_count,
                                          SUM(item_loc_count) OVER (PARTITION BY parent_thread_number,
                                                                                 thread_number) thread_il_count,
                                          SUM(1) OVER (PARTITION BY parent_thread_number,
                                                                    thread_number) pe_count
                                     from rpm_bulk_cc_pe_thread
                                    where bulk_cc_pe_id               = I_bulk_cc_pe_id
                                      and parent_thread_number        is NOT NULL
                                      and parent_thread_number        = I_pe_seq_number
                                      and need_chunk                  = 0
                                      and man_txn_exclusion_parent_id is NULL) t
                              where t.thread_il_count > LP_bulk_thread_il_count
                                and t.pe_count        > 1)) source
            on (rpet.rowid = source.rowid)
            when MATCHED then
               update
                  set rpet.thread_number = source.new_thread_number;

            exit when SQL%ROWCOUNT = 0;

         end loop;

      end if; -- if L_available_thread = 1

      delete rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id               = I_bulk_cc_pe_id
         and thread_number               = I_pe_seq_number
         and parent_thread_number        is NULL
         and man_txn_exclusion_parent_id is NULL;

      open C_BULK_THREADS;
      fetch C_BULK_THREADS BULK COLLECT into L_bulk_threads;
      close C_BULK_THREADS;

      if L_bulk_threads is NOT NULL and
         L_bulk_threads.COUNT > 0 then

         -- This logic is preventing a thread from going through chunking
         -- when only 1 chunk will be used
         forall i IN 1..L_bulk_threads.COUNT
            update rpm_bulk_cc_pe_thread
               set need_chunk = 0
             where bulk_cc_pe_id        = I_bulk_cc_pe_id
               and parent_thread_number = I_pe_seq_number
               and thread_number        = L_bulk_threads(i);

      end if;

      insert into rpm_bulk_cc_pe_sequence (bulk_cc_pe_id,
                                           sequence_number,
                                           thread_number)
         select distinct
                I_bulk_cc_pe_id,
                I_pe_seq_number,
                thread_number
           from rpm_bulk_cc_pe_thread
          where bulk_cc_pe_id               = I_bulk_cc_pe_id
            and parent_thread_number        = I_pe_seq_number
            and man_txn_exclusion_parent_id is NULL;

      -- update the exclusion record for txp promo
      if L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         merge into rpm_bulk_cc_pe_thread target
         using (select thread_number,
                       parent_thread_number,
                       need_chunk,
                       price_event_id
                  from rpm_bulk_cc_pe_thread
                 where bulk_cc_pe_id       = I_bulk_cc_pe_id
                   and man_txn_excl_exists = 1
                   and rownum              > 0) source
         on (target.man_txn_exclusion_parent_id = source.price_event_id)
         when MATCHED then
            update
               set target.thread_number        = source.thread_number,
                   target.parent_thread_number = source.parent_thread_number,
                   target.need_chunk           = source.need_chunk;

      end if;

      merge into rpm_bulk_cc_pe_item target
      using (select /*+ USE_HASH(t i) FULL(t) */
                    i.rowid item_rowid,
                    t.thread_number new_thread_number
               from rpm_bulk_cc_pe_thread t,
                    rpm_bulk_cc_pe_item i
              where t.bulk_cc_pe_id        = I_bulk_cc_pe_id
                and t.parent_thread_number = I_pe_seq_number
                and i.bulk_cc_pe_id        = t.bulk_cc_pe_id
                and i.price_event_id       = t.price_event_id
                and rownum                 > 0) source
      on (source.item_rowid = target.rowid)
      when MATCHED then
         update
            set target.thread_number = source.new_thread_number;

      select MAX(thread_number)
        into O_max_thread_number
        from rpm_bulk_cc_pe_sequence
       where bulk_cc_pe_id   = I_bulk_cc_pe_id
         and sequence_number = I_pe_seq_number;

      -- If reprocessing an SGE, it should never be done through chunking
      if LP_sys_gen_excl_reprocess = 1 then
         update rpm_bulk_cc_pe_thread
            set need_chunk = 0
          where bulk_cc_pe_id = I_bulk_cc_pe_id
            and need_chunk   != 0;
      end if;

      select sys_gen_exclusion_tolerance
        into L_sys_gen_exclusion_tolerance
        from rpm_system_options;

      if L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE) then

         select start_state,
                end_state,
                persist_ind into L_start_state_pc,
                                 L_end_state_pc,
                                 L_persist_ind
           from rpm_bulk_cc_pe
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

      elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

         select start_state,
                end_state,
                persist_ind into L_start_state_promo,
                                 L_end_state_promo,
                                 L_persist_ind
           from rpm_bulk_cc_pe
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

      end if;

      if L_sys_gen_exclusion_tolerance != 0 and
         L_persist_ind                  = 'Y' and
         (LP_sys_gen_excl_reprocess     = 1  or
         ((L_price_event_type    IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                     RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE) and
           L_start_state_pc      IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                     RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) and
           L_end_state_pc        =   RPM_CONSTANTS.PC_APPROVED_STATE_CODE) or
          (L_price_event_type    IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                     RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) and
           L_start_state_promo   IN (RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE,
                                     RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE) and
           L_end_state_promo     =   RPM_CONSTANTS.PR_APPROVED_STATE_CODE))) then
         ---
         if CHECK_SYS_GEN_EXCL_ELIG(O_cc_error_tbl,
                                    L_price_event_type,
                                    I_bulk_cc_pe_id,
                                    I_pe_seq_number) = 0 then
            return 0;
         end if;
      end if;
   end if;

   LOGGER.LOG_TIME(L_program || ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CHUNK_PRICE_EVENT_THREADS;
--------------------------------------------------------------------------------

FUNCTION CHECK_THREAD_SEQUENCE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                               O_sequence_complete     OUT NUMBER,
                               I_bulk_cc_pe_id      IN     NUMBER,
                               I_pe_sequence_number IN     NUMBER,
                               I_pe_thread_number   IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.CHECK_THREAD_SEQUENCE';

   L_in_progress NUMBER := NULL;

   cursor C_IN_PROGRESS is
      select COUNT(*)
        from rpm_bulk_cc_pe_sequence
       where bulk_cc_pe_id    = I_bulk_cc_pe_id
         and sequence_number  = I_pe_sequence_number
         and NVL(status, 'I') = 'I';

BEGIN

   open C_IN_PROGRESS;
   fetch C_IN_PROGRESS into L_in_progress;
   close C_IN_PROGRESS;

   if L_in_progress > 0 then
      O_sequence_complete := 0;
   else
      O_sequence_complete := 1;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CHECK_THREAD_SEQUENCE;
--------------------------------------------------------------------------------

FUNCTION UPDATE_THREAD_SEQUENCE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                I_bulk_cc_pe_id      IN     NUMBER,
                                I_pe_sequence_number IN     NUMBER,
                                I_pe_thread_number   IN     NUMBER,
                                I_thread_status      IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.UPDATE_THREAD_SEQUENCE';

   cursor C_LOCK is
      select status
        from rpm_bulk_cc_pe_sequence rpes
       where bulk_cc_pe_id   = I_bulk_cc_pe_id
         and sequence_number = I_pe_sequence_number
         and thread_number   = I_pe_thread_number
         for update of rpes.status wait 600;

BEGIN

   open C_LOCK;
   close C_LOCK;

   update rpm_bulk_cc_pe_sequence
      set status = I_thread_status
    where bulk_cc_pe_id   = I_bulk_cc_pe_id
      and sequence_number = I_pe_sequence_number
      and thread_number   = I_pe_thread_number;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPDATE_THREAD_SEQUENCE;
--------------------------------------------------------------------------------

FUNCTION UPD_AND_CHECK_THREAD_SEQUENCE(O_cc_error_tbl             OUT CONFLICT_CHECK_ERROR_TBL,
                                       O_sequence_complete        OUT NUMBER,
                                       I_bulk_cc_pe_id         IN     NUMBER,
                                       I_pe_sequence_number    IN     NUMBER,
                                       I_pe_thread_number      IN     NUMBER,
                                       I_thread_status         IN     VARCHAR2,
                                       I_post_push_back_failed IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_BULK_CC_THREADING_SQL.UPD_AND_CHECK_THREAD_SEQUENCE';

BEGIN

   update rpm_bulk_cc_pe_thread
      set status                = I_thread_status,
          post_push_back_failed = I_post_push_back_failed
    where bulk_cc_pe_id        = I_bulk_cc_pe_id
      and parent_thread_number = I_pe_sequence_number
      and thread_number        = I_pe_thread_number;

   if UPDATE_THREAD_SEQUENCE(O_cc_error_tbl,
                             I_bulk_cc_pe_id,
                             I_pe_sequence_number,
                             I_pe_thread_number,
                             I_thread_status) = 0 then
      return 0;
   end if;

   if CHECK_THREAD_SEQUENCE(O_cc_error_tbl,
                            O_sequence_complete,
                            I_bulk_cc_pe_id,
                            I_pe_sequence_number,
                            I_pe_thread_number) = 0 then
      return 0;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPD_AND_CHECK_THREAD_SEQUENCE;
--------------------------------------------------------------------------------

FUNCTION PROCESS_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                       I_bulk_cc_pe_id    IN     NUMBER,
                       I_pe_sequence_id   IN     NUMBER,
                       I_pe_thread_number IN     NUMBER,
                       I_rib_trans_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_BULK_CC_THREADING_SQL.PROCESS_CHUNK';

   L_price_event_ids         OBJ_NUMERIC_ID_TABLE := NULL;
   L_price_event_type        VARCHAR2(3)          := NULL;
   L_persist_ind             VARCHAR2(1)          := NULL;
   L_start_state             VARCHAR2(60)         := NULL;
   L_end_state               VARCHAR2(60)         := NULL;
   L_user_name               VARCHAR2(30)         := NULL;
   L_emergency_perm          NUMBER(1)            := NULL;
   L_secondary_bulk_cc_pe_id NUMBER(10)           := NULL;
   L_secondary_ind           NUMBER(1)            := NULL;

   L_old_clr_out_of_stock_date DATE          := NULL;
   L_old_clr_reset_date        DATE          := NULL;
   L_start_time                TIMESTAMP     := SYSTIMESTAMP;
   L_error_message             VARCHAR2(255) := NULL;

   cursor C_TX is
      select price_event_type,
             persist_ind,
             start_state,
             end_state,
             user_name,
             emergency_perm,
             secondary_bulk_cc_pe_id,
             secondary_ind,
             old_clr_out_of_stock_date,
             old_clr_reset_date
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_PE is
      select price_event_id
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number;

BEGIN

   LOGGER.LOG_INFORMATION(L_program||' - I_bulk_cc_pe_id: '||I_bulk_cc_pe_id ||
                                     ' - I_pe_sequence_id: '||I_pe_sequence_id ||
                                     ' - I_pe_thread_number: '||I_pe_thread_number);

   open C_TX;
   fetch C_TX into L_price_event_type,
                   L_persist_ind,
                   L_start_state,
                   L_end_state,
                   L_user_name,
                   L_emergency_perm,
                   L_secondary_bulk_cc_pe_id,
                   L_secondary_ind,
                   L_old_clr_out_of_stock_date,
                   L_old_clr_reset_date;
   close C_TX;

   open C_PE;
   fetch C_PE BULK COLLECT into L_price_event_ids;
   close C_PE;

   LP_user_name := L_user_name;

   if RPM_CAPTURE_GTT_SQL.INIT_VARS(L_error_message,
                                    L_user_name,
                                    I_bulk_cc_pe_id,
                                    I_pe_sequence_id,
                                    I_pe_thread_number) = 0 then

      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));

      return 0;

   end if;

   if RPM_BULK_CC_ACTIONS_SQL.PROCESS_PRICE_EVENTS(O_cc_error_tbl,
                                                   I_bulk_cc_pe_id,
                                                   L_price_event_ids,
                                                   L_price_event_type,
                                                   I_rib_trans_id,
                                                   L_persist_ind,
                                                   L_start_state,
                                                   L_end_state,
                                                   L_user_name,
                                                   L_emergency_perm,
                                                   L_secondary_bulk_cc_pe_id,
                                                   L_secondary_ind,
                                                   I_pe_sequence_id,
                                                   I_pe_thread_number,
                                                   L_old_clr_out_of_stock_date,
                                                   L_old_clr_reset_date ) = 0 then
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program||' - I_bulk_cc_pe_id: '||I_bulk_cc_pe_id ||
                              ' - I_pe_sequence_id: '||I_pe_sequence_id ||
                              ' - I_pe_thread_number: '||I_pe_thread_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CHUNK;

--------------------------------------------------------------------------------

FUNCTION PURGE_BULK_CC_TABLE(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                             I_bulk_cc_pe_id IN     NUMBER    DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.PURGE_BULK_CC_TABLE';

   L_truncate_pe_item     VARCHAR2(40) := 'truncate table rpm_bulk_cc_pe_item';
   L_truncate_pe_loc      VARCHAR2(40) := 'truncate table rpm_bulk_cc_pe_location';
   L_truncate_pe_chunk    VARCHAR2(40) := 'truncate table rpm_bulk_cc_pe_chunk';
   L_truncate_pe_thread   VARCHAR2(40) := 'truncate table rpm_bulk_cc_pe_thread';
   L_truncate_pe_sequence VARCHAR2(40) := 'truncate table rpm_bulk_cc_pe_sequence';
   L_truncate_pe          VARCHAR2(35) := 'truncate table rpm_bulk_cc_pe';
   L_truncate_lock        VARCHAR2(35) := 'truncate table rpm_pe_cc_lock';

   L_create_pe_item_bak     VARCHAR2(75) := 'create table rpm_bulk_cc_pe_item_bak as select * from rpm_bulk_cc_pe_item';
   L_create_pe_loc_bak      VARCHAR2(85) := 'create table rpm_bulk_cc_pe_location_bak as select * from rpm_bulk_cc_pe_location';
   L_create_pe_chunk_bak    VARCHAR2(80) := 'create table rpm_bulk_cc_pe_chunk_bak as select * from rpm_bulk_cc_pe_chunk';
   L_create_pe_thread_bak   VARCHAR2(80) := 'create table rpm_bulk_cc_pe_thread_bak as select * from rpm_bulk_cc_pe_thread';
   L_create_pe_sequence_bak VARCHAR2(85) := 'create table rpm_bulk_cc_pe_sequence_bak as select * from rpm_bulk_cc_pe_sequence';
   L_create_pe_bak          VARCHAR2(65) := 'create table rpm_bulk_cc_pe_bak as select * from rpm_bulk_cc_pe';

   L_insert_pe_item     VARCHAR2(75) := 'insert into rpm_bulk_cc_pe_item select * from rpm_bulk_cc_pe_item_bak';
   L_insert_pe_loc      VARCHAR2(80) := 'insert into rpm_bulk_cc_pe_location select * from rpm_bulk_cc_pe_location_bak';
   L_insert_pe_chunk    VARCHAR2(75) := 'insert into rpm_bulk_cc_pe_chunk select * from rpm_bulk_cc_pe_chunk_bak';
   L_insert_pe_thread   VARCHAR2(75) := 'insert into rpm_bulk_cc_pe_thread select * from rpm_bulk_cc_pe_thread_bak';
   L_insert_pe_sequence VARCHAR2(80) := 'insert into rpm_bulk_cc_pe_sequence select * from rpm_bulk_cc_pe_sequence_bak';
   L_insert_pe          VARCHAR2(65) := 'insert into rpm_bulk_cc_pe select * from rpm_bulk_cc_pe_bak';

   L_drop_pe_item_bak     VARCHAR2(40) := 'drop table rpm_bulk_cc_pe_item_bak';
   L_drop_pe_loc_bak      VARCHAR2(40) := 'drop table rpm_bulk_cc_pe_location_bak';
   L_drop_pe_chunk_bak    VARCHAR2(40) := 'drop table rpm_bulk_cc_pe_chunk_bak';
   L_drop_pe_thread_bak   VARCHAR2(40) := 'drop table rpm_bulk_cc_pe_thread_bak';
   L_drop_pe_sequence_bak VARCHAR2(40) := 'drop table rpm_bulk_cc_pe_sequence_bak';
   L_drop_pe_bak          VARCHAR2(35) := 'drop table rpm_bulk_cc_pe_bak';

   L_pending_cc_pe_ids OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_PENDING is
      select distinct bulk_cc_pe_id
        from rpm_bulk_cc_pe_thread pet,
             rpm_price_change rpc
       where pet.bulk_cc_pe_id    = NVL(I_bulk_cc_pe_id, pet.bulk_cc_pe_id)
         and pet.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and rpc.price_change_id  = pet.price_event_id
         and rpc.state            = RPM_CONSTANTS.PC_PENDING_STATE_CODE
      union all
      select distinct bulk_cc_pe_id
        from rpm_bulk_cc_pe_thread pet,
             rpm_clearance rc
       where pet.bulk_cc_pe_id    = NVL(I_bulk_cc_pe_id, pet.bulk_cc_pe_id)
         and pet.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and rc.clearance_id      = pet.price_event_id
         and rc.state             = RPM_CONSTANTS.PC_PENDING_STATE_CODE
      union all
      select distinct bulk_cc_pe_id
        from rpm_bulk_cc_pe_thread pet,
             rpm_promo_dtl rpd
       where pet.bulk_cc_pe_id    = NVL(I_bulk_cc_pe_id, pet.bulk_cc_pe_id)
         and pet.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
         and rpd.promo_dtl_id     = pet.price_event_id
         and rpd.state            = RPM_CONSTANTS.PR_PENDING_STATE_CODE;

BEGIN

   open C_PENDING;
   fetch C_PENDING BULK COLLECT into L_pending_cc_pe_ids;
   close C_PENDING;

   if I_bulk_cc_pe_id is NULL then

      if L_pending_cc_pe_ids is NULL or
         L_pending_cc_pe_ids.COUNT = 0 then

         EXECUTE IMMEDIATE L_truncate_pe_item;
         EXECUTE IMMEDIATE L_truncate_pe_loc;
         EXECUTE IMMEDIATE L_truncate_pe_chunk;
         EXECUTE IMMEDIATE L_truncate_pe_thread;
         EXECUTE IMMEDIATE L_truncate_pe_sequence;
         EXECUTE IMMEDIATE L_truncate_pe;
         EXECUTE IMMEDIATE L_truncate_lock;

         if RPM_CHUNK_CC_THREADING_SQL.PURGE_CHUNK_WS_TABLES(O_cc_error_tbl,
                                                             NULL,
                                                             NULL,
                                                             NULL) = 0 then
            return 0;
         end if;

      else

         EXECUTE IMMEDIATE L_create_pe_item_bak;
         EXECUTE IMMEDIATE L_create_pe_loc_bak;
         EXECUTE IMMEDIATE L_create_pe_chunk_bak;
         EXECUTE IMMEDIATE L_create_pe_thread_bak;
         EXECUTE IMMEDIATE L_create_pe_sequence_bak;
         EXECUTE IMMEDIATE L_create_pe_bak;

         EXECUTE IMMEDIATE L_truncate_pe_item;
         EXECUTE IMMEDIATE L_truncate_pe_loc;
         EXECUTE IMMEDIATE L_truncate_pe_chunk;
         EXECUTE IMMEDIATE L_truncate_pe_thread;
         EXECUTE IMMEDIATE L_truncate_pe_sequence;
         EXECUTE IMMEDIATE L_truncate_pe;

         EXECUTE IMMEDIATE L_insert_pe;
         EXECUTE IMMEDIATE L_insert_pe_sequence;
         EXECUTE IMMEDIATE L_insert_pe_thread;
         EXECUTE IMMEDIATE L_insert_pe_chunk;
         EXECUTE IMMEDIATE L_insert_pe_loc;
         EXECUTE IMMEDIATE L_insert_pe_item;

         EXECUTE IMMEDIATE L_drop_pe_item_bak;
         EXECUTE IMMEDIATE L_drop_pe_loc_bak;
         EXECUTE IMMEDIATE L_drop_pe_chunk_bak;
         EXECUTE IMMEDIATE L_drop_pe_thread_bak;
         EXECUTE IMMEDIATE L_drop_pe_sequence_bak;
         EXECUTE IMMEDIATE L_drop_pe_bak;

         if RPM_CHUNK_CC_THREADING_SQL.PURGE_CHUNK_WS_TABLES(O_cc_error_tbl,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             L_pending_cc_pe_ids) = 0 then
            return 0;
         end if;
      end if;

   else

      if L_pending_cc_pe_ids is NULL or
         L_pending_cc_pe_ids.COUNT = 0 then

         delete
           from rpm_bulk_cc_pe_item
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

         delete
           from rpm_bulk_cc_pe_location
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

         delete
           from rpm_bulk_cc_pe_chunk
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

         delete
           from rpm_bulk_cc_pe_thread
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

         delete
           from rpm_bulk_cc_pe_sequence
          where bulk_cc_pe_id = I_bulk_cc_pe_id;

         delete
           from rpm_bulk_cc_pe
          where bulk_cc_pe_id = I_bulk_cc_pe_id;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PURGE_BULK_CC_TABLE;
--------------------------------------------------------------------------------

FUNCTION COMPLETE_BULK_CC(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                          I_bulk_cc_pe_id IN     NUMBER,
                          I_set_error     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BULK_CC_THREADING_SQL.COMPLETE_BULK_CC';

   L_price_event_type VARCHAR2(3)  := NULL;
   L_start_state      VARCHAR2(60) := NULL;
   L_end_state        VARCHAR2(60) := NULL;
   L_auto_clean_ind   NUMBER(1)    := NULL;
   L_pb_error_exists  NUMBER(1)    := 0;

   L_old_clr_out_of_stock_date DATE := NULL;
   L_old_clr_reset_date        DATE := NULL;

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_user_name    VARCHAR2(60)             := NULL;

   cursor C_BULK_CC is
      select price_event_type,
             start_state,
             end_state,
             auto_clean_ind,
             old_clr_out_of_stock_date,
             old_clr_reset_date,
             user_name
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_LOCK is
      select status
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id
         for update of status wait 300;

   cursor C_PB_ERROR is
      select 1
        from dual
       where EXISTS (select 'x'
                       from rpm_bulk_cc_pe_chunk
                      where bulk_cc_pe_id    = I_bulk_cc_pe_id
                        and push_back_status = RPM_CONSTANTS.CC_STATUS_IN_ERROR
                     union all
                     select 'x'
                       from rpm_bulk_cc_pe_thread
                      where bulk_cc_pe_id = I_bulk_cc_pe_id
                        and need_chunk    = 1
                        and (   (    status                = RPM_CONSTANTS.CC_STATUS_IN_ERROR
                                 and post_push_back_failed = 1)
                             or status = RPM_CONSTANTS.CC_STATUS_IN_PROGRESS));

BEGIN

   open C_BULK_CC;
   fetch C_BULK_CC into L_price_event_type,
                        L_start_state,
                        L_end_state,
                        L_auto_clean_ind,
                        L_old_clr_out_of_stock_date,
                        L_old_clr_reset_date,
                        L_user_name;
   close C_BULK_CC;

   open C_LOCK;
   close C_LOCK;

   open C_PB_ERROR;
   fetch C_PB_ERROR into L_pb_error_exists;
   close C_PB_ERROR;

   update rpm_bulk_cc_pe
      set status = case
                      when L_pb_error_exists = 1 then
                         RPM_CONSTANTS.CC_STATUS_PENDING
                      when EXISTS (select 1
                                     from rpm_bulk_cc_pe_sequence
                                    where bulk_cc_pe_id = I_bulk_cc_pe_id
                                      and status        IN (RPM_CONSTANTS.CC_STATUS_IN_PROGRESS,
                                                            RPM_CONSTANTS.CC_STATUS_IN_ERROR))
                           or NVL(I_set_error, 0) = 1 then
                         RPM_CONSTANTS.CC_STATUS_IN_ERROR
                      else
                         RPM_CONSTANTS.CC_STATUS_COMPLETED
                   end
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   if L_pb_error_exists = 1 then

      if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         update rpm_price_change
            set state = RPM_CONSTANTS.PC_PENDING_STATE_CODE
          where price_change_id IN (select price_event_id
                                      from rpm_bulk_cc_pe_thread
                                     where bulk_cc_pe_id = I_bulk_cc_pe_id);

      elsif L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

         update rpm_clearance
            set state = RPM_CONSTANTS.PC_PENDING_STATE_CODE
          where clearance_id IN (select price_event_id
                                   from rpm_bulk_cc_pe_thread
                                  where bulk_cc_pe_id = I_bulk_cc_pe_id);

      elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

         update rpm_promo_dtl
            set state = RPM_CONSTANTS.PR_PENDING_STATE_CODE
          where promo_dtl_id IN (select price_event_id
                                   from rpm_bulk_cc_pe_thread
                                  where bulk_cc_pe_id = I_bulk_cc_pe_id);

      end if;

   end if;

   if L_user_name NOT IN ('NewItemLocationBatch',
                          'LocationMoveBatch') and
      L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) and
      L_end_state = RPM_CONSTANTS.PR_APPROVED_STATE_CODE then

      if DEAL_DELETE_CC(O_cc_error_tbl,
                        I_bulk_cc_pe_id) = 0 then
         L_cc_error_tbl := O_cc_error_tbl;
      end if;

   end if;

   if L_pb_error_exists = 0 and
      L_auto_clean_ind = 1 then

      if PURGE_BULK_CC_TABLE(O_cc_error_tbl,
                             I_bulk_cc_pe_id) = 0 then
         return 0;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END COMPLETE_BULK_CC;

--------------------------------------------------------------------------------

FUNCTION INSERT_CC_RESULT_TABLE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_bulk_cc_pe_id    IN     NUMBER,
                                I_pe_sequence_id   IN     NUMBER,
                                I_pe_thread_number IN     NUMBER,
                                I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                                I_error_string     IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.INSERT_CC_RESULT_TABLE';

   cursor C_BULK_CC_PE is
      select price_event_type,
             user_name
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

BEGIN

   for rec IN C_BULK_CC_PE loop

      LP_user_name := rec.user_name;

      if POPULATE_CC_RESULT_TABLE(O_cc_error_tbl,
                                  I_cc_error_tbl,
                                  I_bulk_cc_pe_id,
                                  I_pe_sequence_id,
                                  I_pe_thread_number,
                                  rec.price_event_type,
                                  I_error_string) = 0 then
         return 0;
      end if;

      -- insert the conflict check error table with the new conflicts
      if I_cc_error_tbl is NOT NULL and
         I_cc_error_tbl.COUNT > 0 and
         I_cc_error_tbl(1).ERROR_TYPE = RPM_CONSTANTS.CONFLICT_ERROR then
         ---
         if POPULATE_CC_ERROR_TABLE(O_cc_error_tbl,
                                    I_cc_error_tbl,
                                    rec.price_event_type) = 0 then
            return 0;
         end if;

      end if;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END INSERT_CC_RESULT_TABLE;

--------------------------------------------------------------------------------
--                            PRIVATE PROCEDURES                              --
--------------------------------------------------------------------------------
FUNCTION CHECK_OVERLAP(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                       O_max_seq_number       OUT NUMBER,
                       I_price_event_type  IN     VARCHAR2,
                       I_need_explode      IN     NUMBER,
                       I_price_event_count IN     NUMBER)

RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_BULK_CC_THREADING_SQL.CHECK_OVERLAP';

   L_thread_number    NUMBER(10) := NULL;
   L_thread_number_no NUMBER(10) := NULL;
   L_overlap          BOOLEAN    := NULL;
   L_price_event_id   NUMBER(15) := NULL;
   L_count            NUMBER(15) := NULL;
   L_start_time       TIMESTAMP  := SYSTIMESTAMP;

   cursor C_OVERLAPPING_PE is
      select distinct rbpt.price_event_id,
             rbpt.rank,
             rbpt.price_event_start_date
        from rpm_bulk_cc_pe_thread rbpt
       where rbpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
         and rbpt.man_txn_exclusion_parent_id is NULL
         and EXISTS (select 1
                       from rpm_bulk_cc_pe_item_gtt rpi
                      where rpi.bulk_cc_pe_id    = rbpt.bulk_cc_pe_id
                        and rpi.price_event_id   = rbpt.price_event_id
                        and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                        and rpi.thread_number    is NULL)
       order by rbpt.rank,
                rbpt.price_event_start_date,
                rbpt.price_event_id;

   cursor C_EXISTS(I_price_event_id NUMBER,
                   I_thread_number NUMBER) is
      select /*+ LEADING(di) */
             rit.price_event_id
        from rpm_item_modification_gtt di,
             rpm_bulk_cc_pe_item_gtt rit,
             rpm_bulk_cc_pe_location rloc
       where rit.item             = di.item
         and rit.bulk_cc_pe_id    = LP_bulk_cc_pe_id
         and rit.price_event_id   = I_price_event_id
         and rit.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rloc.bulk_cc_pe_id   = rit.bulk_cc_pe_id
         and rloc.price_event_id  = rit.price_event_id
         and rloc.itemloc_id      = rit.itemloc_id
         and EXISTS (select /*+ ORDERED INDEX(rit1 RPM_BULK_CC_PE_ITEM_GTT_I1) NO_INDEX_FFS(rloc1 RPM_BULK_CC_PE_LOCATION_I1) */
                            1
                       from rpm_bulk_cc_pe_item_gtt rit1,
                            rpm_bulk_cc_pe_location rloc1
                      where rit1.bulk_cc_pe_id    = LP_bulk_cc_pe_id
                        and rit1.thread_number    = I_thread_number
                        and rit1.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                        and rloc1.bulk_cc_pe_id   = rit1.bulk_cc_pe_id
                        and rloc1.price_event_id  = rit1.price_event_id
                        and rloc1.itemloc_id      = rit1.itemloc_id
                        and rit1.item             = rit.item
                        and rloc1.location        = rloc.location
                        and rownum                = 1)
         and rownum               = 1;

   cursor C_COUNT is
      select COUNT(1)
        from rpm_bulk_cc_pe_item_gtt
       where bulk_cc_pe_id    = LP_bulk_cc_pe_id
         and merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and thread_number    is NOT NULL;

   cursor C_GET_HIGHEST_PE_ID is
      select price_event_id
        from (select price_event_id
                from rpm_bulk_cc_pe_thread
               where bulk_cc_pe_id = LP_bulk_cc_pe_id
               order by rank,
                        price_event_start_date,
                        price_event_id)
       where rownum = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - LP_bulk_cc_pe_id: ' || LP_bulk_cc_pe_id ||
                                       ' - I_price_event_type: ' ||I_price_event_type);

   select do_not_run_cc_for_cp_approval
     into LP_skip_cc_for_cp_approval
     from rpm_system_options;

   if LP_skip_cc_for_cp_approval = 1 and
      I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then
      return 1;
   end if;

   -- Explode Price Change, Clearance and Clearance_reset
   if I_need_explode = 1 then

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         if EXPLODE_PRICE_CHANGES(O_cc_error_tbl) = 0 then
            return 0;
         end if;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                   RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

         if EXPLODE_CLEARANCES(O_cc_error_tbl) = 0 then
            return 0;
         end if;

      elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

         if EXPLODE_CLEARANCE_RESETS(O_cc_error_tbl) = 0 then
            return 0;
         end if;
      end if;

      -- Explode all type of Promo
      if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         if EXPLODE_PROMOS(O_cc_error_tbl) = 0 then
            return 0;
         end if;

      end if; --  end of I_price_event_type

   end if; -- end of I_need_explode

   O_max_seq_number := 1;

   if I_price_event_count > 1 then

      -- To speed up the check overlapping process move rpm_bulk_cc_pe_item to a GTT
      delete from rpm_bulk_cc_pe_item_gtt;

      insert into rpm_bulk_cc_pe_item_gtt (bulk_cc_pe_id,
                                           price_event_id,
                                           itemloc_id,
                                           dept,
                                           class,
                                           subclass,
                                           item,
                                           diff_id,
                                           merch_level_type,
                                           item_parent,
                                           link_code,
                                           thread_number,
                                           chunk_number,
                                           pe_merch_level,
                                           pe_item_rowid)
         select pit.bulk_cc_pe_id,
                pit.price_event_id,
                pit.itemloc_id,
                pit.dept,
                pit.class,
                pit.subclass,
                pit.item,
                pit.diff_id,
                pit.merch_level_type,
                pit.item_parent,
                pit.link_code,
                pit.thread_number,
                pit.chunk_number,
                pit.pe_merch_level,
                pit.rowid
           from rpm_bulk_cc_pe_thread rbcpt,
                rpm_bulk_cc_pe_item pit
          where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
            and pit.bulk_cc_pe_id                 = LP_bulk_cc_pe_id
            and rbcpt.man_txn_exclusion_parent_id is NULL
            and rbcpt.bulk_cc_pe_id               = pit.bulk_cc_pe_id
            and rbcpt.price_event_id              = pit.price_event_id
          order by bulk_cc_pe_id,
                   item;

      -- Set the all Price Events that have overlapping to have the Thread Number = NULL
      select COUNT(1) into L_count
        from (select item,
                     COUNT(1)
                from rpm_bulk_cc_pe_item_gtt pit
               where pit.bulk_cc_pe_id = LP_bulk_cc_pe_id
               group by pit.item
              having COUNT(1) > 1);

      if L_count > 0 then

         merge into rpm_bulk_cc_pe_item_gtt target
         using (select distinct t.price_event_id
                  from (with list_of_item as
                          (select item
                             from rpm_bulk_cc_pe_item_gtt pit
                            where pit.bulk_cc_pe_id = LP_bulk_cc_pe_id
                              and rownum            > 0
                            group by pit.item
                           having COUNT(1) > 1)
                        select /*+ ORDERED USE_HASH(pit, ploc) */
                               pit.price_event_id,
                               COUNT(1) OVER (PARTITION BY pit.item,
                                                           pit.diff_id,
                                                           ploc.location,
                                                           ploc.zone_node_type) cnt
                          from list_of_item it,
                               rpm_bulk_cc_pe_item_gtt pit,
                               rpm_bulk_cc_pe_location ploc
                         where pit.bulk_cc_pe_id   = LP_bulk_cc_pe_id
                           and pit.item            = it.item
                           and ploc.bulk_cc_pe_id  = pit.bulk_cc_pe_id
                           and ploc.price_event_id = pit.price_event_id
                           and ploc.itemloc_id     = pit.itemloc_id) t
                 where cnt > 1) source
         on (    target.bulk_cc_pe_id  = LP_bulk_cc_pe_id
             and target.price_event_id = source.price_event_id)
         when MATCHED then
            update
               set thread_number = NULL;

      end if;

      -- check if all price events are overlapping
      open C_COUNT;
      fetch C_COUNT into L_count;
      close C_COUNT;

      -- If all are overlapping, set the price event with the highest rank to have sequence = 1
      if L_count = 0 then

         open C_GET_HIGHEST_PE_ID;
         fetch C_GET_HIGHEST_PE_ID into L_price_event_id;
         close C_GET_HIGHEST_PE_ID;

         update rpm_bulk_cc_pe_item
            set thread_number = 1
          where bulk_cc_pe_id  = LP_bulk_cc_pe_id
            and price_event_id = L_price_event_id;

      end if;

      L_thread_number := 1;

      delete rpm_item_modification_gtt;

      insert into rpm_item_modification_gtt (item,
                                             item_parent)
         select item,
                item
           from rpm_bulk_cc_pe_item
          where bulk_cc_pe_id = LP_bulk_cc_pe_id
            and rownum        > 0
          group by item
         having COUNT(1) > 1;

      for rec IN C_OVERLAPPING_PE loop

         L_overlap := TRUE;

         for j IN 1..O_max_seq_number loop

            open C_EXISTS(rec.price_event_id,
                          j);
            fetch C_EXISTS into L_price_event_id;

            if C_EXISTS%NOTFOUND then

               L_overlap := FALSE;
               L_thread_number_no := j;
               close C_EXISTS;
               EXIT;

            end if;

            close C_EXISTS;

         end loop;

         if L_overlap then
            L_thread_number := O_max_seq_number + 1;
            L_thread_number_no := L_thread_number;
         end if;

         update rpm_bulk_cc_pe_item_gtt
            set thread_number = L_thread_number_no
          where bulk_cc_pe_id  = LP_bulk_cc_pe_id
            and price_event_id = rec.price_event_id;

         if L_thread_number > O_max_seq_number then
            O_max_seq_number := L_thread_number;
         end if;

         L_thread_number   := 1;

      end loop;

      -- Merge Back to the actual table
      merge into rpm_bulk_cc_pe_item target
      using (select pe_item_rowid,
                    thread_number
               from rpm_bulk_cc_pe_item_gtt) source
         on (target.rowid = source.pe_item_rowid)
      when MATCHED then
         update
            set target.thread_number = source.thread_number;

      -- update the child so that the thread is the same as parent
      merge into rpm_bulk_cc_pe_item target
      using (select distinct rbcpt.bulk_cc_pe_id,
                    rbcpt.price_event_id,
                    rbcpi.thread_number
               from rpm_bulk_cc_pe_thread rbcpt,
                    rpm_bulk_cc_pe_item rbcpi
              where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
                and rbcpi.bulk_cc_pe_id               = LP_bulk_cc_pe_id
                and rbcpt.man_txn_excl_exists         is NULL
                and rbcpi.bulk_cc_pe_id               = rbcpt.bulk_cc_pe_id
                and rbcpt.man_txn_exclusion_parent_id = rbcpi.price_event_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set target.thread_number = source.thread_number;

   end if;

   LOGGER.LOG_TIME(L_program || ' - LP_bulk_cc_pe_id: ' || LP_bulk_cc_pe_id ||
                                ' - I_price_event_type: '||I_price_event_type,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CHECK_OVERLAP;
--------------------------------------------------------------------------------

FUNCTION CHECK_FINANCE_PROMO_OVERLAP(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                     O_max_seq_number      OUT NUMBER,
                                     I_price_event_type IN     VARCHAR2)

RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_THREADING_SQL.CHECK_FINANCE_PROMO_OVERLAP';

   L_thread_number    NUMBER(10) := NULL;
   L_thread_number_no NUMBER(10) := NULL;
   L_overlap          BOOLEAN    := NULL;
   L_price_event_id   NUMBER(15) := NULL;
   L_count            NUMBER(15) := NULL;
   L_max_seq_number   NUMBER(15) := NULL;

   cursor C_GET_COUNT is
      select 1
        from rpm_bulk_cc_pe_thread rbpt,
             rpm_promo_credit_dtl rpd
       where thread_number       is NULL
         and rbpt.bulk_cc_pe_id  = LP_bulk_cc_pe_id
         and rpd.promo_dtl_id    = rbpt.price_event_id
      group by rpd.financial_dtl_id
      having COUNT(1) > 1;

   cursor C_OVERLAPPING_PE is
      select rbpt.price_event_id,
             rpd.financial_dtl_id
        from rpm_bulk_cc_pe_thread rbpt,
             rpm_promo_credit_dtl rpd
       where thread_number       is NULL
         and rbpt.bulk_cc_pe_id  = LP_bulk_cc_pe_id
         and rpd.promo_dtl_id    = rbpt.price_event_id
       order by rpd.financial_dtl_id;

   cursor C_EXISTS(I_financial_dtl_id NUMBER,
                   I_thread_number NUMBER) is
      select rbpt.price_event_id
        from rpm_bulk_cc_pe_thread rbpt,
             rpm_promo_credit_dtl rpd
       where bulk_cc_pe_id         = LP_bulk_cc_pe_id
         and rpd.promo_dtl_id      = rbpt.price_event_id
         and rpd.financial_dtl_id  = I_financial_dtl_id
         and rbpt.thread_number    is NULL
         and EXISTS (select 1
                       from rpm_bulk_cc_pe_thread rbpt1,
                            rpm_promo_credit_dtl rpd1
                      where bulk_cc_pe_id         = LP_bulk_cc_pe_id
                        and rbpt1.thread_number   = I_thread_number
                        and rpd1.promo_dtl_id     = rbpt1.price_event_id
                        and rpd1.financial_dtl_id = rpd.financial_dtl_id)
         and rownum                = 1;

BEGIN

   open C_GET_COUNT;
   fetch C_GET_COUNT into L_count;

   if C_GET_COUNT%NOTFOUND then

      update rpm_bulk_cc_pe_thread
         set thread_number = 1
       where bulk_cc_pe_id = LP_bulk_cc_pe_id;

      close C_GET_COUNT;
      L_max_seq_number := 1;

   else
      close C_GET_COUNT;
      L_thread_number   := 1;
      L_max_seq_number  := 1;

      for rec IN C_OVERLAPPING_PE loop
         L_overlap := TRUE;

         for j IN 1..L_max_seq_number loop

            open C_EXISTS(rec.financial_dtl_id, j);
            fetch C_EXISTS into L_price_event_id;

            if C_EXISTS%NOTFOUND then
               L_overlap := FALSE;
               L_thread_number_no := j;

               close C_EXISTS;
               EXIT;
            end if;

            close C_EXISTS;
         end loop;

         if L_overlap then
            L_thread_number := L_max_seq_number + 1;
            L_thread_number_no := L_thread_number;
         end if;

         update rpm_bulk_cc_pe_thread
            set thread_number  = L_thread_number_no
          where bulk_cc_pe_id  = LP_bulk_cc_pe_id
            and price_event_id = rec.price_event_id;

         if L_thread_number > L_max_seq_number then
            L_max_seq_number := L_thread_number;
         end if;

         L_thread_number := 1;

      end loop;
   end if;

   O_max_seq_number := L_max_seq_number;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CHECK_FINANCE_PROMO_OVERLAP;
--------------------------------------------------------------------------------

FUNCTION RANK_PRICE_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_bulk_cc_pe_id    IN     NUMBER,
                          I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.RANK_PRICE_EVENT';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                       ' - I_price_event_type: '||I_price_event_type);

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      merge /*+ INDEX(target rpm_bulk_cc_pe_thread_i3) */
       into rpm_bulk_cc_pe_thread target
      using (select /*+ INDEX(cc rpm_bulk_cc_pe_thread_i3) INDEX(rpc pk_rpm_price_change) */
                    cc.bulk_cc_pe_id,
                    cc.price_event_id,
                    rpc.price_change_display_id price_event_display_id,
                    I_price_event_type,
                    rpc.effective_date start_date,
                    case
                       when LP_sys_gen_excl_reprocess = 1 then
                          RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       when rpc.link_code is NOT NULL then
                          RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                       when rpc.diff_id is NOT NULL then
                          RPM_CONSTANTS.PARENT_ITEM_DIFF
                       when rpc.skulist is NOT NULL then
                          RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       when rpc.price_event_itemlist is NOT NULL then
                          RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                       else
                          RPM_CONSTANTS.ITEM_LEVEL_ITEM
                    end merch_node_type,
                    rpc.item,
                    rpc.diff_id,
                    rpc.link_code,
                    rpc.zone_node_type,
                    case
                       when rpc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
                          rpc.zone_id
                       else
                          rpc.location
                    end zone_node_id,
                    case
                       when rpc.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                          1
                       when rpc.exception_parent_id is NOT NULL then
                          case
                             when EXISTS (select 1
                                            from rpm_price_change rpc1
                                           where rpc1.price_change_id     = rpc.exception_parent_id
                                             and rpc1.exception_parent_id is NOT NULL
                                             and rownum                   = 1) then
                                2
                             else
                                3
                          end
                       else
                          4
                    end rank
               from rpm_bulk_cc_pe_thread cc,
                    rpm_price_change rpc
              where cc.bulk_cc_pe_id    = I_bulk_cc_pe_id
                and rpc.price_change_id = cc.price_event_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set rank                        = source.rank,
                price_event_start_date      = source.start_date,
                merch_node_type             = source.merch_node_type,
                item                        = source.item,
                diff_id                     = source.diff_id,
                link_code                   = source.link_code,
                zone_node_type              = source.zone_node_type,
                zone_node_id                = source.zone_node_id,
                price_event_display_id      = source.price_event_display_id,
                elig_for_sys_gen_exclusions = 0;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      merge /*+ INDEX(target RPM_BULK_CC_PE_THREAD_I3) */
       into rpm_bulk_cc_pe_thread target
      using (select /*+ INDEX(cc RPM_BULK_CC_PE_THREAD_I3) INDEX(rc PK_RPM_CLEARANCE) */
                    cc.bulk_cc_pe_id,
                    cc.price_event_id,
                    rc.clearance_display_id price_event_display_id,
                    I_price_event_type,
                    rc.effective_date start_date,
                    case
                       when LP_sys_gen_excl_reprocess = 1 then
                          RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       when rc.diff_id is NOT NULL then
                          RPM_CONSTANTS.PARENT_ITEM_DIFF
                       when rc.skulist is NOT NULL then
                          RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       when rc.price_event_itemlist is NOT NULL then
                          RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                       else
                          RPM_CONSTANTS.ITEM_LEVEL_ITEM
                    end merch_node_type,
                    rc.item,
                    rc.diff_id,
                    rc.zone_node_type,
                    case
                       when rc.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
                          rc.zone_id
                       else
                          rc.location
                    end zone_node_id,
                    case
                       when rc.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                          1
                       when rc.exception_parent_id is NOT NULL then
                          case
                             when EXISTS (select 1
                                            from rpm_clearance rc1
                                           where rc1.clearance_id        = rc.exception_parent_id
                                             and rc1.exception_parent_id is NOT NULL
                                             and rownum                  = 1) then
                                2
                             else
                                3
                          end
                       else
                          4
                    end rank
               from rpm_bulk_cc_pe_thread cc,
                    rpm_clearance rc
              where cc.bulk_cc_pe_id = I_bulk_cc_pe_id
                and rc.clearance_id  = cc.price_event_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set rank                        = source.rank,
                price_event_start_date      = source.start_date,
                merch_node_type             = source.merch_node_type,
                item                        = source.item,
                diff_id                     = source.diff_id,
                zone_node_type              = source.zone_node_type,
                zone_node_id                = source.zone_node_id,
                price_event_display_id      = source.price_event_display_id,
                elig_for_sys_gen_exclusions = 0;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

      merge /*+ INDEX(target RPM_BULK_CC_PE_THREAD_I3) */
       into rpm_bulk_cc_pe_thread target
      using (select /*+ INDEX(cc RPM_BULK_CC_PE_THREAD_I3) INDEX(rc PK_RPM_CLEARANCE_RESET) */
                    cc.bulk_cc_pe_id,
                    cc.price_event_id,
                    rc.clearance_display_id price_event_display_id,
                    I_price_event_type,
                    cp.new_clr_reset_date start_date,
                    RPM_CONSTANTS.ITEM_LEVEL_ITEM merch_node_type,
                    rc.item,
                    rc.zone_node_type,
                    rc.location zone_node_id
               from rpm_bulk_cc_pe cp,
                    rpm_bulk_cc_pe_thread cc,
                    rpm_clearance_reset rc
              where cp.bulk_cc_pe_id = I_bulk_cc_pe_id
                and cc.bulk_cc_pe_id = cp.bulk_cc_pe_id
                and rc.clearance_id  = cc.price_event_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set rank                   = 1,
                price_event_start_date = source.start_date,
                merch_node_type        = source.merch_node_type,
                item                   = source.item,
                zone_node_type         = source.zone_node_type,
                zone_node_id           = source.zone_node_id,
                price_event_display_id = source.price_event_display_id;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then
      merge /*+ INDEX(target RPM_BULK_CC_PE_THREAD_I3) */
       into rpm_bulk_cc_pe_thread target
      using (select /*+ INDEX(cc RPM_BULK_CC_PE_THREAD_I3) */
                    cc.bulk_cc_pe_id,
                    cc.price_event_id,
                    rpd.promo_dtl_display_id price_event_display_id,
                    I_price_event_type,
                    rpd.start_date,
                    case
                       when LP_sys_gen_excl_reprocess = 1 then
                          RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       else
                          rpdm.merch_type
                    end merch_node_type,
                    rpdm.item,
                    rpdm.diff_id,
                    rpdm.subclass,
                    rpdm.class,
                    rpdm.dept,
                    rpzl.zone_node_type,
                    case
                       when rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
                          rpzl.zone_id
                       else
                          rpzl.location
                    end zone_node_id,
                    case
                       when rpddl.change_type = RPM_CONSTANTS.RETAIL_EXCLUDE then
                          1
                       when rpd.exception_parent_id is NOT NULL then
                          case
                             when EXISTS (select 1
                                            from rpm_promo_dtl rpd1
                                           where rpd1.promo_dtl_id         = rpd.exception_parent_id
                                             and rpd1.exception_parent_id is NOT NULL
                                             and rownum                    = 1) then
                                2
                             else
                                3
                          end
                       else
                          4
                    end rank
               from rpm_bulk_cc_pe_thread cc,
                    rpm_promo_dtl rpd,
                    rpm_promo_zone_location rpzl,
                    rpm_promo_dtl_merch_node rpdm,
                    rpm_promo_dtl_list_grp rpdlg,
                    rpm_promo_dtl_list rpdl,
                    rpm_promo_dtl_disc_ladder rpddl
              where cc.bulk_cc_pe_id           = I_bulk_cc_pe_id
                and rpd.promo_dtl_id           = cc.price_event_id
                and rpzl.promo_dtl_id          = rpd.promo_dtl_id
                and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                and rpdm.promo_dtl_id          = rpd.promo_dtl_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set rank                        = source.rank,
                price_event_start_date      = source.start_date,
                merch_node_type             = source.merch_node_type,
                item                        = source.item,
                diff_id                     = source.diff_id,
                dept                        = source.dept,
                class                       = source.class,
                subclass                    = source.subclass,
                zone_node_type              = source.zone_node_type,
                zone_node_id                = source.zone_node_id,
                price_event_display_id      = source.price_event_display_id,
                elig_for_sys_gen_exclusions = 0;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD) then
      merge into rpm_bulk_cc_pe_thread target
      using (select cc.bulk_cc_pe_id,
                    cc.price_event_id,
                    rpd.promo_dtl_display_id price_event_display_id,
                    I_price_event_type,
                    rpd.start_date,
                    case
                       when LP_sys_gen_excl_reprocess = 1 then
                          RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                       else
                          rpdm.merch_type
                    end merch_node_type,
                    rpdm.item,
                    rpdm.diff_id,
                    rpdm.subclass,
                    rpdm.class,
                    rpdm.dept,
                    rpzl.zone_node_type,
                    case
                       when rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE then
                          rpzl.zone_id
                       else
                          rpzl.location
                    end zone_node_id,
                    case
                       when EXISTS (select 1
                                      from rpm_promo_dtl_list_grp rpdlg,
                                           rpm_promo_dtl_list rpdl,
                                           rpm_promo_dtl_disc_ladder rpddl
                                     where rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                                       and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                                       and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                                       and rpddl.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
                                       and rownum                     = 1) then
                          1
                       when rpd.exception_parent_id is NOT NULL then
                          case
                             when EXISTS (select 1
                                            from rpm_promo_dtl rpd1
                                           where rpd1.promo_dtl_id         = rpd.exception_parent_id
                                             and rpd1.exception_parent_id is NOT NULL
                                             and rownum                    = 1) then
                                2
                             else
                                3
                          end
                       else
                          4
                    end rank
               from rpm_bulk_cc_pe_thread cc,
                    rpm_promo_dtl rpd,
                    rpm_promo_zone_location rpzl,
                    rpm_promo_dtl_merch_node rpdm
              where cc.bulk_cc_pe_id  = I_bulk_cc_pe_id
                and rpd.promo_dtl_id  = cc.price_event_id
                and rpzl.promo_dtl_id = rpd.promo_dtl_id
                and rpdm.promo_dtl_id = rpd.promo_dtl_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set rank                        = source.rank,
                price_event_start_date      = source.start_date,
                merch_node_type             = source.merch_node_type,
                item                        = source.item,
                diff_id                     = source.diff_id,
                dept                        = source.dept,
                class                       = source.class,
                subclass                    = source.subclass,
                zone_node_type              = source.zone_node_type,
                zone_node_id                = source.zone_node_id,
                price_event_display_id      = source.price_event_display_id,
                elig_for_sys_gen_exclusions = 0;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      merge into rpm_bulk_cc_pe_thread target
      using (select cc.bulk_cc_pe_id,
                    cc.price_event_id,
                    rpd.promo_dtl_display_id price_event_display_id,
                    I_price_event_type,
                    rpd.start_date,
                    case
                       when EXISTS (select 1
                                      from rpm_promo_dtl_list_grp rpdlg,
                                           rpm_promo_dtl_list rpdl,
                                           rpm_promo_dtl_disc_ladder rpddl
                                     where rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                                       and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                                       and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                                       and rpddl.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
                                       and rownum                     = 1) then
                          1
                       else
                          4
                    end rank,
                    rpd.man_txn_excl_exists
               from rpm_bulk_cc_pe_thread cc,
                    rpm_promo_dtl rpd
              where cc.bulk_cc_pe_id = I_bulk_cc_pe_id
                and rpd.promo_dtl_id = cc.price_event_id) source
      on (    target.bulk_cc_pe_id  = source.bulk_cc_pe_id
          and target.price_event_id = source.price_event_id)
      when MATCHED then
         update
            set rank                        = source.rank,
                price_event_start_date      = source.start_date,
                price_event_display_id      = source.price_event_display_id,
                elig_for_sys_gen_exclusions = 0,
                man_txn_excl_exists         = source.man_txn_excl_exists;

      -- create exclusion records for txp promo
      if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

         if LP_user_name NOT IN ('NewItemLocationBatch',
                                 'LocationMoveBatch') then

            insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                               price_event_id,
                                               price_event_display_id,
                                               price_event_type,
                                               rank,
                                               price_event_start_date,
                                               elig_for_sys_gen_exclusions,
                                               man_txn_exclusion_parent_id)
               select I_bulk_cc_pe_id,
                      rpd.promo_dtl_id,
                      rpd.promo_dtl_display_id,
                      rbcpt.price_event_type,
                      rbcpt.rank,
                      rbcpt.price_event_start_date,
                      0,
                      rpd.exception_parent_id
                 from rpm_bulk_cc_pe_thread rbcpt,
                      rpm_promo_dtl rpd
                where rbcpt.bulk_cc_pe_id       = I_bulk_cc_pe_id
                  and rbcpt.man_txn_excl_exists = 1
                  and rbcpt.price_event_id      = rpd.exception_parent_id
                  and rpd.man_txn_exclusion     = 1;

         else

            merge into rpm_bulk_cc_pe_thread target
               using (select bulk_cc_pe_id,
                             price_event_id,
                             man_txn_exclusion_parent_id,
                             rank
                        from rpm_bulk_cc_pe_thread
                       where bulk_cc_pe_id       = I_bulk_cc_pe_id
                         and man_txn_excl_exists = 1) source
            on (    target.bulk_cc_pe_id               = source.bulk_cc_pe_id
                and target.man_txn_exclusion_parent_id = source.price_event_id)
            when MATCHED then
               update
                  set rank = source.rank;

         end if;

      end if;

   end if;

   LOGGER.LOG_TIME(L_program || ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                ' - I_price_event_type: '||I_price_event_type,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END RANK_PRICE_EVENT;

--------------------------------------------------------------------------------

FUNCTION POPULATE_CC_ERROR_TABLE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.POPULATE_CC_ERROR_TABLE';

   L_con_check_err_id       NUMBER(15)                                     := NULL;
   L_display_conflicts_only RPM_SYSTEM_OPTIONS.DISPLAY_CONFLICTS_ONLY%TYPE := NULL;
   L_days_before            NUMBER(3)                                      := NULL;
   L_days_after             NUMBER(3)                                      := NULL;
   L_effective_date         DATE                                           := NULL;
   L_fr_start_date          DATE                                           := NULL;
   L_fr_end_date            DATE                                           := NULL;

   cursor C_NO_CC_PROMO is
      select /*+ CARDINALITY (cc 10) */
             rpd.promo_dtl_id,
             rp.promo_display_id,
             rpc.comp_display_id,
             rpd.start_date,
             cc.error_string,
             cc.future_retail_id
        from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) cc,
             rpm_promo_dtl rpd,
             rpm_promo_comp rpc,
             rpm_promo rp
       where cc.price_event_id = rpd.promo_dtl_id
         and rpc.promo_comp_id = rpd.promo_comp_id
         and rp.promo_id       = rpc.promo_id;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      for rec IN C_NO_CC_PROMO loop

         L_con_check_err_id := RPM_CON_CHECK_ERR_SEQ.NEXTVAL;

         insert into rpm_con_check_err
            (con_check_err_id,
             ref_class,
             ref_id,
             ref_display_id,
             ref_secondary_display_id,
             effective_date,
             error_date,
             message_key)
         values(L_con_check_err_id,
                RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                rec.promo_dtl_id,
                rec.promo_display_id,
                rec.comp_display_id,
                rec.start_date,
                LP_vdate,
                rec.error_string);

         insert into rpm_con_check_err_detail
            (con_check_err_detail_id,
             con_check_err_id,
             ref_class,
             ref_id,
             ref_display_id,
             ref_secondary_display_id,
             effective_date,
             selling_retail,
             selling_retail_currency,
             selling_uom)
         select RPM_CON_CHECK_ERR_DETAIL_SEQ.NEXTVAL,
                L_con_check_err_id,
                RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
                rpd.promo_dtl_id,
                rp.promo_display_id,
                rpc.comp_display_id,
                rpd.start_date,
                0,
                'USD',
                'EA'
           from rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                rpm_promo rp
          where rec.future_retail_id = rpd.promo_dtl_id
            and rpc.promo_comp_id    = rpd.promo_comp_id
            and rp.promo_id          = rpc.promo_id;

      end loop;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_CC_ERROR_TABLE;

--------------------------------------------------------------------------------

FUNCTION POPULATE_CC_RESULT_TABLE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_cc_error_tbl     IN     CONFLICT_CHECK_ERROR_TBL,
                                  I_bulk_cc_pe_id    IN     NUMBER,
                                  I_pe_sequence_id   IN     NUMBER,
                                  I_pe_thread_number IN     NUMBER,
                                  I_price_event_type IN     VARCHAR2,
                                  I_error_string     IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_BULK_CC_THREADING_SQL.POPULATE_CC_RESULT_TABLE';

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      insert into rpm_conflict_check_result
         (conflict_check_result_id,
          event_type,
          result,
          results_date,
          user_id,
          event_id,
          dept,
          class,
          subclass,
          zone_id,
          error_string)
      select /*+ LEADING(ccet) */
             RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
             0,
             NVL2(ccet.price_event_id, 1, 0),
             LP_vdate,
             LP_user_name,
             t.price_event_display_id,
             dept,
             class,
             subclass,
             zone_id,
             I_error_string
        from (select distinct price_event_id
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) t) ccet,
             (select rpet.price_event_display_id,
                     im.dept,
                     im.class,
                     im.subclass,
                     cl.zone_id,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     item_master im,
                     rpm_clearance cl
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and cl.clearance_id           = rpet.price_event_id
                 and cl.item                   is NOT NULL
                 and cl.item                   = im.item
              union all
              select rpet.price_event_display_id,
                     NULL dept,
                     NULL class,
                     NULL subclass,
                     cl.zone_id,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     rpm_clearance cl
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and cl.clearance_id           = rpet.price_event_id
                 and cl.item                   is NULL) t
       where ccet.price_event_id (+) = t.price_event_id;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      insert into rpm_conflict_check_result
         (conflict_check_result_id,
          event_type,
          result,
          results_date,
          user_id,
          event_id,
          dept,
          class,
          subclass,
          zone_id,
          error_string)
      select /*+ LEADING(ccet) */
             RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
             1,
             NVL2(ccet.price_event_id, 1, 0) a2,
             LP_vdate,
             LP_user_name,
             v_table.a5,
             v_table.a9,
             v_table.a10,
             v_table.a11,
             v_table.a12,
             I_error_string
        from (select distinct price_event_id
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) t) ccet,
             (select /*+ LEADING(rpet) */
                     rpet.price_event_display_id a5,
                     im.dept a9,
                     im.class a10,
                     im.subclass a11,
                     rpc.zone_id a12,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     item_master im,
                     rpm_price_change rpc
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and rpc.price_change_id       = rpet.price_event_id
                 and rpc.item                  is NOT NULL
                 and rpc.item                  = im.item
              union all
              select /*+ LEADING(rpet) */
                     rpet.price_event_display_id a5,
                     NULL a9,
                     NULL a10,
                     NULL a11,
                     rpc.zone_id a12,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     rpm_price_change rpc
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and rpc.price_change_id       = rpet.price_event_id
                 and rpc.item                  is NULL) v_table
       where ccet.price_event_id (+) = v_table.price_event_id;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO) then

      insert into rpm_conflict_check_result
         (conflict_check_result_id,
          event_type,
          result,
          results_date,
          user_id,
          event_id,
          promo_comp_id,
          promo_comp_detail_id,
          dept,
          class,
          subclass,
          zone_id,
          error_string)
      select /*+ LEADING(ccet) */
             RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
             2,
             NVL2(ccet.price_event_id, 1, 0) a2,
             LP_vdate,
             LP_user_name,
             v_table.a5,
             v_table.a6,
             v_table.a7,
             v_table.a9,
             v_table.a10,
             v_table.a11,
             v_table.a12,
             I_error_string
        from (select distinct price_event_id
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) t) ccet,
             (select /*+ LEADING(rpet) */
                     rp.promo_display_id a5,
                     rpc.comp_display_id a6,
                     rpet.price_event_display_id a7,
                     im.dept a9,
                     im.class a10,
                     im.subclass a11,
                     rpzl.zone_id a12,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp,
                     item_master im,
                     rpm_promo_zone_location rpzl
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and rpd.promo_dtl_id          = rpet.price_event_id
                 and rpc.promo_comp_id         = rpd.promo_comp_id
                 and rp.promo_id               = rpc.promo_id
                 and rpet.item                 is NOT NULL
                 and rpet.item                 = im.item
                 and rpzl.promo_dtl_id         = rpd.promo_dtl_id
              union all
              select /*+ LEADING(rpet) */
                     rp.promo_display_id a5,
                     rpc.comp_display_id a6,
                     rpet.price_event_display_id a7,
                     rpdm.dept a9,
                     rpdm.class a10,
                     rpdm.subclass a11,
                     rpzl.zone_id a12,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp,
                     rpm_promo_dtl_merch_node rpdm,
                     rpm_promo_zone_location rpzl
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and rpd.promo_dtl_id          = rpet.price_event_id
                 and rpc.promo_comp_id         = rpd.promo_comp_id
                 and rp.promo_id               = rpc.promo_id
                 and rpet.item                 is NULL
                 and rpdm.promo_dtl_id         = rpd.promo_dtl_id
                 and rpzl.promo_dtl_id         = rpd.promo_dtl_id) v_table
       where ccet.price_event_id (+) = v_table.price_event_id;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO) then

      insert into rpm_conflict_check_result
         (conflict_check_result_id,
          event_type,
          result,
          results_date,
          user_id,
          event_id,
          promo_comp_id,
          promo_comp_detail_id,
          dept,
          class,
          subclass,
          zone_id,
          error_string)
      select /*+ LEADING(ccet) */
             RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
             2,
             NVL2(ccet.price_event_id, 1, 0) a2,
             LP_vdate,
             LP_user_name,
             v_table.a5,
             v_table.a6,
             v_table.a7,
             v_table.a9,
             v_table.a10,
             v_table.a11,
             v_table.a12,
             I_error_string
        from (select distinct price_event_id
                from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) t) ccet,
             (select /*+ LEADING(rpet) */
                     rp.promo_display_id a5,
                     rpc.comp_display_id a6,
                     rpet.price_event_display_id a7,
                     im.dept a9,
                     im.class a10,
                     im.subclass a11,
                     rpzl.zone_id a12,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp,
                     item_master im,
                     rpm_promo_zone_location rpzl
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and rpd.promo_dtl_id          = rpet.price_event_id
                 and rpc.promo_comp_id         = rpd.promo_comp_id
                 and rp.promo_id               = rpc.promo_id
                 and rpet.item                 is NOT NULL
                 and rpet.item                 = im.item
                 and rpzl.promo_dtl_id         = rpd.promo_dtl_id
              union all
              select /*+ LEADING(rpet) */
                     rp.promo_display_id a5,
                     rpc.comp_display_id a6,
                     rpet.price_event_display_id a7,
                     rpdm.dept a9,
                     rpdm.class a10,
                     rpdm.subclass a11,
                     rpzl.zone_id a12,
                     rpet.price_event_id
                from rpm_bulk_cc_pe_thread rpet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp,
                     rpm_promo_dtl_merch_node rpdm,
                     rpm_promo_zone_location rpzl
               where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpet.parent_thread_number = I_pe_sequence_id
                 and rpet.thread_number        = I_pe_thread_number
                 and rpd.promo_dtl_id          = rpet.price_event_id
                 and rpc.promo_comp_id         = rpd.promo_comp_id
                 and rp.promo_id               = rpc.promo_id
                 and rpet.item                 is NULL
                 and rpdm.promo_dtl_id         = rpd.promo_dtl_id
                 and rpzl.promo_dtl_id         = rpd.promo_dtl_id) v_table
       where ccet.price_event_id (+) = v_table.price_event_id;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      insert into rpm_conflict_check_result
            (conflict_check_result_id,
             event_type,
             result,
             results_date,
             user_id,
             event_id,
             promo_comp_id,
             promo_comp_detail_id,
             error_string)
         select /*+ LEADING(ccet) */
                RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
                2,
                NVL2(ccet.price_event_id, 1, 0),
                LP_vdate,
                LP_user_name,
                rp.promo_display_id,
                rpc.comp_display_id,
                rpet.price_event_display_id,
                I_error_string
           from (select distinct price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) t) ccet,
                rpm_bulk_cc_pe_thread rpet,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                rpm_promo rp
          where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and rpet.parent_thread_number = I_pe_sequence_id
            and rpet.thread_number        = I_pe_thread_number
            and rpd.promo_dtl_id          = rpet.price_event_id
            and rpc.promo_comp_id         = rpd.promo_comp_id
            and rp.promo_id               = rpc.promo_id
            and ccet.price_event_id (+)   = rpet.price_event_id;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION then

      insert into rpm_conflict_check_result
            (conflict_check_result_id,
             event_type,
             result,
             results_date,
             user_id,
             event_id,
             promo_comp_id,
             promo_comp_detail_id,
             error_string)
         select /*+ LEADING(ccet) */
                RPM_CONFLICT_CHECK_RESULT_SEQ.NEXTVAL,
                2,
                NVL2(ccet.price_event_id, 1, 0),
                LP_vdate,
                LP_user_name,
                rp.promo_display_id,
                rpc.comp_display_id,
                rpet.price_event_display_id,
                I_error_string
           from (select distinct price_event_id
                   from table(cast(I_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)) t) ccet,
                rpm_bulk_cc_pe_thread rpet,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc,
                rpm_promo rp
          where rpet.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and rpet.parent_thread_number = I_pe_sequence_id
            and rpet.thread_number        = I_pe_thread_number
            and rpd.promo_dtl_id          = rpet.price_event_id
            and rpc.promo_comp_id         = rpd.promo_comp_id
            and rp.promo_id               = rpc.promo_id
            and ccet.price_event_id (+)   = rpet.price_event_id;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_CC_RESULT_TABLE;

--------------------------------------------------------------------------------
FUNCTION EXPLODE_PRICE_CHANGES(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program    VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.EXPLODE_PRICE_CHANGES';
   L_trace_name VARCHAR2(10) := 'EPC';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (LP_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program,
                                           RPM_CONSTANTS.LOG_STARTED,
                                           'Start of '||L_program);

         RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(LP_batch_name||'-'||L_trace_name);

      end if;

   EXCEPTION

      when OTHERS then
         goto EXPLODE_PRICE_CHANGES_1;

   END;

   <<EXPLODE_PRICE_CHANGES_1>>

   -- Process Price Changes that ARE NOT ItemList/Zone Level, merch list/zone level, or PEIL/zone level
   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    item_parent,
                                    merch_level_type,
                                    link_code,
                                    thread_number,
                                    pe_merch_level)
      -- Tran Level Item for Price Event at Tran Level Item
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item              = rpet.item
         and im.item_level        = im.tran_level
      union all
      -- Tran Level Item for Price Event at Parent Level Item
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im ITEM_MASTER_I1)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diff_1,
             rpet.item,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent       = rpet.item
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent       = rpet.item
         and im.diff_1            is NOT NULL
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Level Item for Price Event at Parent Level Item
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item              = rpet.item
         and im.item_level        = im.tran_level - 1
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item and DIFF_ID is part of DIFF_1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im ITEM_MASTER_I1)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             rpet.diff_id,
             rpet.item,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent       = rpet.item
         and im.diff_1            = rpet.diff_id
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item and DIFF_ID is NOT part of DIFF_1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im ITEM_MASTER_I1)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent = rpet.item
         and (   im.diff_2 = rpet.diff_id
              or im.diff_3 = rpet.diff_id
              or im.diff_4 = rpet.diff_id)
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level items for Price Events at the Parent/Diff level where
      -- the diff id on the price event IS part of diff_1 for the parent
      select /*+ LEADING(rpet) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent       = rpet.item
         and im.diff_1            = rpet.diff_id
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Changes at LinkCode Level
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case
                when im.item_parent is NULL and
                     im.item_level = im.tran_level then
                   NULL
                else
                   im.diff_1
             end,
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             rpet.link_code,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_link_code_attribute rlca,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rlca.link_code       = rpet.link_code
         and im.item              = rlca.item
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Item lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_ITEM_LEVEL
         and pcsl.item                 = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Tran Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item                 = im.item_parent
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Parent Diff Level data for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item                 = im.item_parent
         and im.diff_1                 is NOT NULL
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Parent Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item                 = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level - 1
      union all
      -- Tran Level Item for Price Event Item lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_detail rmld,
             item_master im
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item                = im.item
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level            = im.tran_level
      union all
      -- Tran Level Item for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_detail rmld,
             item_master im
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item_parent
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level            = im.tran_level
      union all
      -- Parent Diff Level data for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_detail rmld,
             item_master im
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item_parent
         and im.diff_1                is NOT NULL
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Level Item for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_detail rmld,
             item_master im
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level            = im.tran_level - 1
      union all
      -- Merch Lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case
                when im.item_parent is NULL and
                     im.item_level = im.tran_level then
                   NULL
                else
                   im.diff_1
             end,
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                   = mld.item
      union all
      -- Merch Lists - parent/diff level - tran items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.diff_1                 = mld.diff_id
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      -- Merch Lists - parent/diff level - parent/diff entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             mld.diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item                   = mld.item
      union all
      -- Merch Lists - parent level items - tran item entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      -- Merch Lists - parent level items = parent/diff entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.item_level             = im.tran_level
         and im.diff_1                 is NOT NULL
      union all
      -- Merch Lists - parent level items = parent entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item                   = mld.item;

     insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                          price_event_id,
                                          itemloc_id,
                                          location,
                                          zone_node_type,
                                          zone_id)
      with merchnode as
         (select /*+ INDEX(rpet RPM_BULK_CC_PE_THREAD_I3) INDEX(im PK_ITEM_MASTER) */
                 rpet.price_event_id,
                 rpet.zone_node_id,
                 rpet.zone_node_type,
                 im.dept,
                 im.class,
                 im.subclass
            from rpm_bulk_cc_pe_thread rpet,
                 item_master im
           where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
             and rpet.merch_node_type NOT IN (RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM,
                                              RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL,
                                              RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM)
             and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.item              = rpet.item)
      -- Location level price events
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet
       where rpet.bulk_cc_pe_id    = LP_bulk_cc_pe_id
         and rpet.merch_node_type != RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rpet.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
      union all
      -- Zone level price events for NON Link Code, NON Item/Merch List and Zone is part of PZG
      select LP_bulk_cc_pe_id,
             mer.price_event_id,
             1,
             mer.zone_node_id,
             mer.zone_node_type,
             NULL
        from merchnode mer,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rz.zone_id               = mer.zone_node_id
         and rmrde.dept               = mer.dept
         and rmrde.class              = mer.class
         and rmrde.subclass           = mer.subclass
         and rmrde.regular_zone_group = rz.zone_group_id
      union all
      -- Locs in Zone Level price events NON Link Code, NON Item/Merch List
      -- For Non PZG Zone, set the zone_id to NULL
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             mer.price_event_id,
             1,
             rzl.location,
             rzl.loc_type,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   mer.zone_node_id
                else
                   NULL
             end
        from merchnode mer,
             rpm_zone rz,
             rpm_zone_location rzl,
             rpm_merch_retail_def_expl rmrde
       where rz.zone_id                   = mer.zone_node_id
         and rzl.zone_id                  = rz.zone_id
         and rmrde.dept (+)               = mer.dept
         and rmrde.class (+)              = mer.class
         and rmrde.subclass (+)           = mer.subclass
         and rmrde.regular_zone_group (+) = rz.zone_group_id
      union all
      -- link codes with location level price events
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rlca) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL zone_id
        from rpm_bulk_cc_pe_thread rpet,
             rpm_link_code_attribute rlca
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rpet.zone_node_type  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rlca.link_code       = rpet.link_code
         and rlca.location        = rpet.zone_node_id
      union all
      -- link codes - locations in zone level price events
      -- For Link Codes Price Event, RPM will assume that everything will be down to Location Level
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rzl, rlca, im) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rlca.location,
             rzl.loc_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl,
             rpm_link_code_attribute rlca
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rlca.link_code       = rpet.link_code
         and rzl.zone_id          = rpet.zone_node_id
         and rlca.location        = rzl.location;

   -- Process Price Change that ARE ItemList/Zone Level, PEIL/Zone level, or merch list/zone level
   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    item_parent,
                                    merch_level_type,
                                    link_code,
                                    thread_number,
                                    pe_merch_level)
      -- Tran Level Item for Item lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_ITEM_LEVEL
         and pcsl.item                 = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Tran Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */ distinct
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item                 = im.item_parent
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Parent Diff Level data for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item_parent item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item                 = im.item_parent
         and im.diff_1                 is NOT NULL
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Parent Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change_skulist pcsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and pcsl.price_event_id       = rpet.price_event_id
         and pcsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pcsl.item                 = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level - 1
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Tran Level Item for Price Event Item lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_price_change rpc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item                = im.item
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level            = im.tran_level
         and rz.zone_id               = rpet.zone_node_id
         and rmrde.dept               = im.dept
         and rmrde.class              = im.class
         and rmrde.subclass           = im.subclass
      union all
      -- Tran Level Item for Price Event Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_price_change rpc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item_parent
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level            = im.tran_level
         and rz.zone_id               = rpet.zone_node_id
         and rmrde.dept               = im.dept
         and rmrde.class              = im.class
         and rmrde.subclass           = im.subclass
      union all
      -- Parent Diff Level data for Price Event Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_price_change rpc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item_parent
         and im.diff_1                is NOT NULL
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rz.zone_id               = rpet.zone_node_id
         and rmrde.dept               = im.dept
         and rmrde.class              = im.class
         and rmrde.subclass           = im.subclass
      union all
      -- Parent Level Item for Price Event Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_price_change rpc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id       = LP_bulk_cc_pe_id
         and rpet.merch_node_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.price_change_id      = rpet.price_event_id
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level            = im.tran_level - 1
         and rz.zone_id               = rpet.zone_node_id
         and rmrde.dept               = im.dept
         and rmrde.class              = im.class
         and rmrde.subclass           = im.subclass
      union all
      -- Merch Lists - tran item entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case
                when im.item_parent is NULL and
                     im.item_level = im.tran_level then
                   NULL
                else
                   im.diff_1
             end,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                   = mld.item
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent/diff entries - tran items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.diff_1                 = mld.diff_id
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent/diff entries - parent/diff items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             mld.diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item                   = mld.item
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent entries - tran items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent entries - parent/diff items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item_parent item,
             im.diff_1 diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.diff_1                 is NOT NULL
         and im.item_level             = im.tran_level
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent entries - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end, -- itemloc_id
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_price_change rpc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rpc.exception_parent_id
         and rpc.price_change_id       = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item                   = mld.item
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                        price_event_id,
                                        itemloc_id,
                                        location,
                                        zone_node_type,
                                        zone_id)
      -- Zone level price events ItemList/Merchlist and Zone
      -- For Item in the Itemlist that the zone is PZG
      -- (this record will not be used if the zone is NON PZG for all items in the ItemList)
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
      union all
      -- Locs in Zone Level price events for Item List/Merchlist
      -- For PZG Zone, set the zone_id and itemloc_id to 1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rzl.location,
             rzl.loc_type,
             rpet.zone_node_id
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id
      union all
      -- Locs in Zone Level price events for Item List/Merchlist
      -- For Non PZG Zone, set the zone_id to NULL and itemloc_id to 2
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             2,
             rzl.location,
             rzl.loc_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id
      union all
      -- Zone level price event PEILS and Zone
      -- For Item in the Itemlist that the zone is PZG
      -- this record will not be used if the zone is NON PZG for all items in the ItemList)
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
      union all
      -- Locs in Zone Level price events for PEILS
      -- For PZG Zone, set the zone_id and itemloc_id to 1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rzl.location,
             rzl.loc_type,
             rpet.zone_node_id
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id
      union all
      -- Locs in Zone Level price events for PEILS
      -- For Non PZG Zone, set the zone_id to NULL and itemloc_id to 2
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             2,
             rzl.location,
             rzl.loc_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (LP_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program,
                                           RPM_CONSTANTS.LOG_COMPLETED,
                                           'End of '||L_program);

      end if;

   EXCEPTION

      when OTHERS then
         goto EXPLODE_PRICE_CHANGES_2;

   END;

   <<EXPLODE_PRICE_CHANGES_2>>

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END EXPLODE_PRICE_CHANGES;

--------------------------------------------------------------------------------
FUNCTION EXPLODE_CLEARANCES(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)

RETURN NUMBER IS

   L_program 		VARCHAR2(50) := 'RPM_BULK_CC_THREADING_SQL.EXPLODE_CLEARANCES';
   L_trace_name VARCHAR2(10) := 'ECL';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (LP_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program,
                                           RPM_CONSTANTS.LOG_STARTED,
                                           'Start of '||L_program);

         RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(LP_batch_name||'-'||L_trace_name);

      end if;

   EXCEPTION
      when OTHERS then
         goto EXPLODE_CLEARANCES_1;

   END;

   <<EXPLODE_CLEARANCES_1>>

   -- Process Clearances that ARE NOT ItemList/Zone Level or Price Event Itemlist/Zone level
   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    item_parent,
                                    merch_level_type,
                                    thread_number,
                                    pe_merch_level)
      -- Tran Level Item for Price Event at Tran Level Item
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item              = rpet.item
         and im.item_level        = im.tran_level
      union all
      -- Tran Level Item for Price Event at Parent Level Item
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im ITEM_MASTER_I1)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diff_1,
             rpet.item,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent       = rpet.item
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent       = rpet.item
         and im.diff_1            is NOT NULL
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Level Item for Price Event at Parent Level Item
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item              = rpet.item
         and im.item_level        = im.tran_level - 1
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item and the DIFF ID is from DIFF_1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im ITEM_MASTER_I1)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             rpet.diff_id,
             rpet.item,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent       = rpet.item
         and im.diff_1            = rpet.diff_id
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item and the DIFF ID is NOT from DIFF_1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im ITEM_MASTER_I1)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent       = rpet.item
         and (   im.diff_2 = rpet.diff_id
              or im.diff_3 = rpet.diff_id
              or im.diff_4 = rpet.diff_id)
         and im.item_level        = im.tran_level
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Diff Level items for Price Events at the Parent/Diff level where
      -- the diff id on the price event IS part of diff_1 for the parent
      select /*+ LEADING(rpet) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             item_master im
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent       = rpet.item
         and im.diff_1            = rpet.diff_id
         and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Item lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_ITEM_LEVEL
         and clrsl.item                = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Tran Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and clrsl.item                = im.item_parent
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Parent Diff Level data for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             NULL,
             im.diff_1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and clrsl.item                = im.item_parent
         and im.diff_1                 is NOT NULL
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Parent Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and clrsl.item                = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level - 1
      union all
      -- Tran Level Item for Price Event Item Lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item               = im.item
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
      union all
      -- Tran Level Item for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
      union all
      -- Parent Diff Level data for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.diff_1               is NOT NULL
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Level Item for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level - 1
      union all
      -- Merch Lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case
                when im.item_parent is NULL and
                     im.item_level = im.tran_level then
                   NULL
                else
                   im.diff_1
             end,
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                   = mld.item
      union all
      -- Merch Lists - parent/diff level - tran items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.diff_1                 = mld.diff_id
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      -- Merch Lists - parent/diff level - parent/diff entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             mld.diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item                   = mld.item
      union all
      -- Merch Lists - parent level items - tran item entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      -- Merch Lists - parent level items = parent/diff entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.item_level             = im.tran_level
         and im.diff_1                 is NOT NULL
      union all
      -- Merch Lists - parent level items = parent entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item                   = mld.item;

   insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                        price_event_id,
                                        itemloc_id,
                                        location,
                                        zone_node_type,
                                        zone_id)
      with merchnode as
         (select /*+ INDEX(rpet RPM_BULK_CC_PE_THREAD_I3) INDEX(im PK_ITEM_MASTER) */
                 rpet.price_event_id,
                 rpet.zone_node_id,
                 rpet.zone_node_type,
                 im.dept,
                 im.class,
                 im.subclass
            from rpm_bulk_cc_pe_thread rpet,
                 item_master im
           where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
             and rpet.merch_node_type NOT IN (RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM,
                                              RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL)
             and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.item              = rpet.item)
      -- Location level price events
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet
       where rpet.bulk_cc_pe_id  = LP_bulk_cc_pe_id
         and rpet.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
      union all
      -- Locs in Zone Level price events NOT in Item List or PEIL
      -- For Non PZG Zone, set the zone_id to NULL
      select LP_bulk_cc_pe_id,
             mer.price_event_id,
             1,
             rzl.location,
             rzl.loc_type,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   mer.zone_node_id
                else
                   NULL
             end
        from merchnode mer,
             rpm_zone rz,
             rpm_zone_location rzl,
             rpm_merch_retail_def_expl rmrde
       where rz.zone_id                   = mer.zone_node_id
         and rzl.zone_id                  = rz.zone_id
         and rmrde.dept (+)               = mer.dept
         and rmrde.class (+)              = mer.class
         and rmrde.subclass (+)           = mer.subclass
         and rmrde.regular_zone_group (+) = rz.zone_group_id
      union all
      -- Zone level price events for NOT in Item List or PEIL and Zone is part of PZG
      select LP_bulk_cc_pe_id,
             mer.price_event_id,
             1,
             mer.zone_node_id,
             mer.zone_node_type,
             NULL
        from merchnode mer,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rz.zone_id               = mer.zone_node_id
         and rmrde.dept               = mer.dept
         and rmrde.class              = mer.class
         and rmrde.subclass           = mer.subclass
         and rmrde.regular_zone_group = rz.zone_group_id;

   -- Process Clearances that ARE ItemList/Zone Level or PEIL/Zone Level
   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    item_parent,
                                    merch_level_type,
                                    thread_number,
                                    pe_merch_level)
      -- Tran Level Item for Item lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_ITEM_LEVEL
         and clrsl.item                = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
       union all
      -- Tran Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and clrsl.item                = im.item_parent
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Parent Diff Level data for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and clrsl.item                = im.item_parent
         and im.diff_1                 is NOT NULL
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Parent Level Item for Item lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance_skulist clrsl,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 0
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and clrsl.price_event_id      = rpet.price_event_id
         and clrsl.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and clrsl.item                = im.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level - 1
         and rz.zone_id                = rpet.zone_node_id
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Tran Level Item for Price Event Item Lists - tran level items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rmld.item               = im.item
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
         and rz.zone_id              = rpet.zone_node_id
         and rmrde.dept              = im.dept
         and rmrde.class             = im.class
         and rmrde.subclass          = im.subclass
      union all
      -- Tran Level Item for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level
         and rz.zone_id              = rpet.zone_node_id
         and rmrde.dept              = im.dept
         and rmrde.class             = im.class
         and rmrde.subclass          = im.subclass
      union all
      -- Parent Diff Level data for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.diff_1               is NOT NULL
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and rz.zone_id              = rpet.zone_node_id
         and rmrde.dept              = im.dept
         and rmrde.class             = im.class
         and rmrde.subclass          = im.subclass
      union all
      -- Parent Level Item for Price Event Item Lists - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_merch_list_detail rmld,
             rpm_clearance rc,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpet.bulk_cc_pe_id      = LP_bulk_cc_pe_id
         and rpet.merch_node_type    = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rc.clearance_id         = rpet.price_event_id
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level           = im.tran_level - 1
         and rz.zone_id              = rpet.zone_node_id
         and rmrde.dept              = im.dept
         and rmrde.class             = im.class
         and rmrde.subclass          = im.subclass
      union all
      -- Merch Lists - tran item entries
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             case
                when im.item_parent is NULL and
                     im.item_level = im.tran_level then
                   NULL
                else
                   im.diff_1
             end,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and im.item                   = mld.item
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent/diff entries - tran items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.diff_1                 = mld.diff_id
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent/diff entries - parent/diff items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             mld.diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and im.item                   = mld.item
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent entries - tran items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent entries - parent/diff items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item_parent            = mld.item
         and im.diff_1                 is NOT NULL
         and im.item_level             = im.tran_level
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass
      union all
      -- Merch Lists - parent entries - parent items
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
             distinct LP_bulk_cc_pe_id,
             rpet.price_event_id,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   1
                else
                   2
             end,
             im.dept,
             im.class,
             im.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE
        from rpm_bulk_cc_pe_thread rpet,
             rpm_clearance rc,
             rpm_merch_list_head mlh,
             rpm_merch_list_detail mld,
             item_master im,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where LP_sys_gen_excl_reprocess = 1
         and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
         and rpet.merch_node_type      = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id                = rpet.zone_node_id
         and rpet.price_event_id       = rc.exception_parent_id
         and rc.clearance_id           = mlh.price_event_id
         and mlh.merch_list_type       = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
         and mlh.price_event_type      = RPM_CONSTANTS.PE_TYPE_CLEARANCE
         and mlh.merch_list_id         = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and im.item                   = mld.item
         and rmrde.dept                = im.dept
         and rmrde.class               = im.class
         and rmrde.subclass            = im.subclass;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                        price_event_id,
                                        itemloc_id,
                                        location,
                                        zone_node_type,
                                        zone_id)
      -- Zone level price events ItemList and Zone
      -- For Item in the Itemlist that the zone is PZG
      -- (this record will not be used if the zone is NON PZG for all items in the ItemList)
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) use_hash(rpet,rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
      union all
      -- Locs in Zone Level price events for Item List
      -- For PZG Zone, set the zone_id and itemloc_id to 1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rzl.location,
             rzl.loc_type,
             rpet.zone_node_id
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id
      union all
      -- Locs in Zone Level price events for Item List
      -- For Non PZG Zone, set the zone_id to NULL and itemloc_id to 2
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) use_hash(RPET, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             2,
             rzl.location,
             rzl.loc_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id
      union all
      -- Zone level price events PEIL and Zone
      -- For Item in the PEIL that the zone is PZG (this record will not be used if the zone is NON PZG for all items in the ItemList)
      select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rpet.zone_node_id,
             rpet.zone_node_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
      union all
      -- Locs in Zone Level price events for PEIL
      -- For PZG Zone, set the zone_id and itemloc_id to 1
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             1,
             rzl.location,
             rzl.loc_type,
             rpet.zone_node_id
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id
      union all
      -- Locs in Zone Level price events for PEIL
      -- For Non PZG Zone, set the zone_id to NULL and itemloc_id to 2
      select /*+ LEADING(rpet) INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) USE_HASH(rpet, rz) */
             LP_bulk_cc_pe_id,
             rpet.price_event_id,
             2,
             rzl.location,
             rzl.loc_type,
             NULL
        from rpm_bulk_cc_pe_thread rpet,
             rpm_zone_location rzl
       where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
         and rpet.merch_node_type = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and rpet.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id          = rpet.zone_node_id;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (LP_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           L_program,
                                           L_trace_name|| ' - bcc_pe_id '||LP_bulk_cc_pe_id,
                                           RPM_CONSTANTS.LOG_COMPLETED,
                                           'End of '||L_program);
      end if;

   EXCEPTION

      when OTHERS then
         goto EXPLODE_CLEARANCES_2;

   END;

   <<EXPLODE_CLEARANCES_2>>

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END EXPLODE_CLEARANCES;

--------------------------------------------------------------------------------

FUNCTION EXPLODE_CLEARANCE_RESETS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_THREADING_SQL.EXPLODE_CLEARANCE_RESETS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id);

   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    merch_level_type,
                                    thread_number,
                                    pe_merch_level)
       select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) INDEX (im PK_ITEM_MASTER)*/
              LP_bulk_cc_pe_id,
              rpet.price_event_id,
              rpet.price_event_id item_loc_id,
              im.dept,
              im.class,
              im.subclass,
              im.item,
              DECODE(im.item_parent,
                     NULL, NULL,
                     im.diff_1),
              RPM_CONSTANTS.ITEM_MERCH_TYPE,
              1,
              RPM_CONSTANTS.ITEM_MERCH_TYPE
         from rpm_bulk_cc_pe_thread rpet,
              item_master im
        where rpet.bulk_cc_pe_id   = LP_bulk_cc_pe_id
          and rpet.merch_node_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
          and im.item              = rpet.item
          and im.item_level        = im.tran_level;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                        price_event_id,
                                        itemloc_id,
                                        location,
                                        zone_node_type)
       select /*+ INDEX (rpet RPM_BULK_CC_PE_THREAD_I3) */
              LP_bulk_cc_pe_id,
              rpet.price_event_id,
              rpet.price_event_id item_loc_id,
              rpet.zone_node_id,
              rpet.zone_node_type
         from rpm_bulk_cc_pe_thread rpet
        where rpet.bulk_cc_pe_id  = LP_bulk_cc_pe_id
          and rpet.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE);

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END EXPLODE_CLEARANCE_RESETS;

--------------------------------------------------------------------------------

FUNCTION EXPLODE_PROMOS(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) 		:= 'RPM_BULK_CC_THREADING_SQL.EXPLODE_PROMOS';
   L_trace_name VARCHAR2(10) 	:= 'EPR';
   L_start_time TIMESTAMP 		:= SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (LP_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then
       
         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program,
                                           RPM_CONSTANTS.LOG_STARTED,
                                           'Start of '||L_program);
         RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(LP_batch_name||'-'||L_trace_name);
      end if;

   EXCEPTION

      when others then
         goto EXPLODE_PROMOS_1;

   END;

   <<EXPLODE_PROMOS_1>>

   insert into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    dept,
                                    class,
                                    subclass,
                                    item,
                                    diff_id,
                                    item_parent,
                                    merch_level_type,
                                    thread_number,
                                    pe_merch_level,
                                    txn_man_excluded_item)
      with txn_dtl_exclusions as
         (-- merch hierarchy
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 subclass sc
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                       RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                       RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
             and sc.dept                           = rpdmn.dept
             and sc.class                          = NVL(rpdmn.class, sc.class)
             and sc.subclass                       = NVL(rpdmn.subclass, sc.subclass)
          -- Explode to tran item for exclusion at skulist parent item
          union all
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 im.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds,
                 item_master im
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpds.price_event_id               = rbcpt.price_event_id
             and rpds.skulist                      = rpdmn.skulist
             and im.item_parent                    = rpds.item
             and rpds.item_level                   = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
             and im.item_level                     = im.tran_level
          union all
          -- exclusion at skulist level for item or parent item
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpds.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpds.price_event_id               = rbcpt.price_event_id
             and rpds.skulist                      = rpdmn.skulist
          -- Explode to tran item for exclusion at peil parent item level
          union all
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 im.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_merch_list_detail rmld,
                 item_master im
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id                = rpdmn.price_event_itemlist
             and rmld.merch_level                  = RPM_CONSTANTS.PARENT_MERCH_TYPE
             and im.item_parent                    = rmld.item
             and im.item_level                     = im.tran_level
             and im.status                         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind                   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          union all
          -- exclusion at peil level for item or parent item level
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rmld.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_merch_list_detail rmld
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rmld.merch_list_id                = rpdmn.price_event_itemlist
          union all
          -- explode to tran level for exclusion at parent and parent diff item
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 im.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 item_master im
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                       RPM_CONSTANTS.PARENT_ITEM_DIFF)
             and im.item_parent                    = rpdmn.item
             and (   rpdmn.diff_id      is NULL
                  or (    rpdmn.diff_id is NOT NULL
                      and (   im.diff_1 = rpdmn.diff_id
                           or im.diff_2 = rpdmn.diff_id
                           or im.diff_3 = rpdmn.diff_id
                           or im.diff_4 = rpdmn.diff_id)))
             and im.item_level                     = im.tran_level
             and im.status                         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind                   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
          union all
          -- exclusion at tran item or parent diff item
          select rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.merch_type,
                 rpd.promo_dtl_id,
                 rpd.exception_parent_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn
           where rbcpt.bulk_cc_pe_id               = LP_bulk_cc_pe_id
             and rbcpt.man_txn_exclusion_parent_id is NOT NULL
             and rbcpt.price_event_type            IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
             and rpd.man_txn_exclusion             = 1
             and rpd.promo_dtl_id                  = rbcpt.price_event_id
             and rpd.promo_dtl_id                  = rpdmn.promo_dtl_id
             and rpdmn.merch_type                  IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                       RPM_CONSTANTS.PARENT_ITEM_DIFF)),
      merchnode as
         (select LP_bulk_cc_pe_id bulk_cc_pe_id,
                 price_event_id,
                 promo_dtl_merch_node_id,
                 merch_type,
                 dept,
                 class,
                 subclass,
                 skulist,
                 price_event_itemlist,
                 item,
                 diff_id,
                 LP_sys_gen_excl_reprocess,
                 DENSE_RANK() OVER (PARTITION BY price_event_id
                                        ORDER BY promo_dtl_merch_node_id,
                                                 dept,
                                                 class,
                                                 subclass) itemloc_id
            from (-- Merch Hier level
                  select rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         sc.dept,
                         sc.class,
                         sc.subclass,
                         rpdm.skulist,
                         rpdm.price_event_itemlist,
                         rpdm.item,
                         rpdm.diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm,
                         subclass sc
                   where LP_sys_gen_excl_reprocess  = 0
                     and rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                     and rpd.promo_dtl_id           = rpet.price_event_id
                     and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.promo_dtl_id          = rpd.promo_dtl_id
                     and rpdm.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                        RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                        RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                     and sc.dept                    = rpdm.dept
                     and sc.class                   = NVL(rpdm.class, sc.class)
                     and sc.subclass                = NVL(rpdm.subclass, sc.subclass)
                  union all
                  -- Item or Parent diff level
                  select rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         im.dept,
                         im.class,
                         im.subclass,
                         rpdm.skulist,
                         rpdm.price_event_itemlist,
                         rpdm.item,
                         rpdm.diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm,
                         item_master im
                   where LP_sys_gen_excl_reprocess  = 0
                     and rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                     and rpd.promo_dtl_id           = rpet.price_event_id
                     and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.promo_dtl_id          = rpd.promo_dtl_id
                     and rpdm.merch_type            IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                        RPM_CONSTANTS.PARENT_ITEM_DIFF)
                     and im.item                    = rpdm.item
                  union all
                  -- Skulist Level
                  select rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         im.dept,
                         im.class,
                         im.subclass,
                         rpds.skulist,
                         rpdm.price_event_itemlist,
                         rpds.item,
                         rpdm.diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm,
                         rpm_promo_dtl_skulist rpds,
                         item_master im
                   where LP_sys_gen_excl_reprocess  = 0
                     and rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                     and rpd.promo_dtl_id           = rpet.price_event_id
                     and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdm.promo_dtl_id          = rpd.promo_dtl_id
                     and rpds.price_event_id        = rpet.price_event_id
                     and rpds.skulist               = rpdm.skulist
                     and im.item                    = rpds.item
                  union all
                  -- Price Event ItemList
                  select distinct rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         im.dept,
                         im.class,
                         im.subclass,
                         rpdm.skulist,
                         rpdm.price_event_itemlist,
                         rmld.item,
                         case
                            when im.item_parent is NULL and
                                 im.item_level = im.tran_level then
                               NULL
                            else
                               im.diff_1
                         end diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm,
                         rpm_merch_list_detail rmld,
                         item_master im
                   where LP_sys_gen_excl_reprocess  = 0
                     and rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                     and rpd.promo_dtl_id           = rpet.price_event_id
                     and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                     and rpdm.promo_dtl_id          = rpd.promo_dtl_id
                     and rmld.merch_list_id         = rpdm.price_event_itemlist
                     and im.item                    = rmld.item
                  -- Storewide - store level
                  union all
                  select rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         im.dept,
                         im.class,
                         im.subclass,
                         rpdm.skulist,
                         rpdm.price_event_itemlist,
                         im.item,
                         case
                            when im.item_parent is NULL and
                                 im.item_level = im.tran_level then
                               NULL
                            else
                               im.diff_1
                         end diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_merch_node rpdm,
                         rpm_promo_zone_location rpzl,
                         item_loc il,
                         item_master im
                   where LP_sys_gen_excl_reprocess = 0
                     and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
                     and rpd.promo_dtl_id          = rpet.price_event_id
                     and rpdm.merch_type           = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                     and rpdm.promo_dtl_id         = rpd.promo_dtl_id
                     and rpzl.promo_dtl_id         = rpd.promo_dtl_id
                     and rpzl.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                     and rpzl.location             = il.loc
                     and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                     and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                     and im.item                   = il.item
                  -- Storewide - zone level
                  union all
                  select distinct rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         im.dept,
                         im.class,
                         im.subclass,
                         rpdm.skulist,
                         rpdm.price_event_itemlist,
                         im.item,
                         case
                            when im.item_parent is NULL and
                                 im.item_level = im.tran_level then
                               NULL
                            else
                               im.diff_1
                         end diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_merch_node rpdm,
                         rpm_promo_zone_location rpzl,
                         rpm_zone_location rzl,
                         item_loc il,
                         item_master im
                   where LP_sys_gen_excl_reprocess = 0
                     and rpet.bulk_cc_pe_id        = LP_bulk_cc_pe_id
                     and rpd.promo_dtl_id          = rpet.price_event_id
                     and rpdm.merch_type           = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                     and rpdm.promo_dtl_id         = rpd.promo_dtl_id
                     and rpzl.promo_dtl_id         = rpd.promo_dtl_id
                     and rpzl.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                     and rzl.loc_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                     and rzl.zone_id               = rpzl.zone_id
                     and rzl.location              = il.loc
                     and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                     and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                     and im.item                   = il.item
                  -- SGE
                  union all
                  select rpet.price_event_id,
                         rpdm.promo_dtl_merch_node_id,
                         rpdm.merch_type,
                         im.dept,
                         im.class,
                         im.subclass,
                         rpdm.skulist,
                         rpdm.price_event_itemlist,
                         mld.item,
                         case
                            when im.item_parent is NULL and
                                 im.item_level = im.tran_level then
                               NULL
                            else
                               im.diff_1
                         end diff_id
                    from rpm_bulk_cc_pe_thread rpet,
                         rpm_promo_dtl rpd,
                         rpm_promo_dtl_list_grp rpdlg,
                         rpm_promo_dtl_list rpdl,
                         rpm_promo_dtl_disc_ladder rpddl,
                         rpm_promo_dtl_merch_node rpdm,
                         rpm_merch_list_head mlh,
                         rpm_merch_list_detail mld,
                         item_master im
                   where LP_sys_gen_excl_reprocess  = 1
                     and rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                     and rpd.exception_parent_id    = rpet.price_event_id
                     and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                     and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                     and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                     and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                     and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdm.promo_dtl_id          = rpd.promo_dtl_id
                     and mlh.price_event_id         = rpd.promo_dtl_id
                     and mlh.merch_list_id          = rpdm.skulist
                     and mlh.merch_list_type        = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                     and mlh.price_event_type       IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                        RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                        RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                     and mld.merch_list_id          = mlh.merch_list_id
                     and mld.item                   = im.item))
      -- Tran Item Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             mer.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item            = mer.item
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = tde.item (+)
      union all
       -- Tran Level for Parent Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             im.diff_1,
             mer.item,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent     = mer.item
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = tde.item (+)
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent/Diff level data for price events at the Parent level
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             mer.item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item_parent     = mer.item
         and im.item_level      = im.tran_level
         and im.diff_1          is NOT NULL
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item_parent     = tde.item (+)
         and im.diff_1          = NVL(tde.diff_id (+), im.diff_1)
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Level for Parent Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
         and im.item            = mer.item
         and im.item_level      = im.tran_level - 1
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = tde.item (+)
         and im.diff_1          = NVL(tde.diff_id (+), im.diff_1)
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item and DIFF_ID is part of DIFF_1
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             mer.diff_id,
             mer.item,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent     = mer.item
         and im.diff_1          = mer.diff_id
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item_parent     = NVL(tde.item (+), im.item_parent)
      union all
      -- Tran Level Item for Price Event at Parent Diff Level Item and DIFF_ID is NOT part of DIFF_1
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent     = mer.item
         and (   im.diff_2 = mer.diff_id
              or im.diff_3 = mer.diff_id
              or im.diff_4 = mer.diff_id)
         and im.item_level      = im.tran_level
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = NVL(tde.item (+), im.item)
      union all
      -- Parent Diff data for price event at Parent Diff Level
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item_parent,
             mer.diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NVL2(tde.item, 1, 0)
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
         and im.item_parent     = mer.item
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = NVL(tde.item (+), im.item)
      union all
      -- Parent Level and transaction level items with parents for Merch Hierarchy Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                     tde.class is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                     tde.subclass is NOT NULL then
                   1
                else
                   case
                      when tde.item is NULL then
                         0
                      else
                         1
                   end
             end -- txn_man_excluded_item
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type          IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept                 = mer.dept
         and im.class                = NVL(mer.class, im.class)
         and im.subclass             = NVL(mer.subclass, im.subclass)
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and mer.price_event_id      = tde.exception_parent_id (+)
         and im.item                 = NVL(tde.item (+), im.item)
         and NVL(im.diff_1, '-999')  = NVL(tde.diff_id (+), NVL(im.diff_1, '-999'))
         and im.dept                 = NVL(tde.dept (+), im.dept)
         and im.class                = NVL(tde.class (+), im.class)
         and im.subclass             = NVL(tde.subclass (+), im.subclass)
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent-Diff Level items for Merch Hierarchy Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item_parent item,
             im.diff_1,
             NULL, -- item_parent
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                     tde.class is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                     tde.subclass is NOT NULL then
                   1
                else
                   case
                      when tde.item is NULL then
                         0
                      else
                         1
                   end
             end -- txn_man_excluded_item
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                    RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                    RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept            = mer.dept
         and im.class           = NVL(mer.class, im.class)
         and im.subclass        = NVL(mer.subclass, im.subclass)
         and im.item_level      = im.tran_level
         and im.item_parent     is NOT NULL
         and im.diff_1          is NOT NULL
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item_parent     = NVL(tde.item (+), im.item_parent)
         and im.diff_1          = NVL(tde.diff_id (+), im.diff_1)
         and im.dept            = NVL(tde.dept (+), im.dept)
         and im.class           = NVL(tde.class (+), im.class)
         and im.subclass        = NVL(tde.subclass (+), im.subclass)
      union all
      -- Tran level items with no parents for Merch Hierarchy Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             NULL, -- diff_id
             NULL, -- item_parent
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             case
                when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                     tde.class is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                     tde.subclass is NOT NULL then
                   1
                else
                   case
                      when tde.item is NULL then
                         0
                      else
                         1
                   end
             end --txn_man_excluded_item
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                    RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                    RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
         and im.dept            = mer.dept
         and im.class           = NVL(mer.class, im.class)
         and im.subclass        = NVL(mer.subclass, im.subclass)
         and im.item_level      = im.tran_level
         and im.item_parent     is NULL
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = NVL(tde.item (+), im.item)
         and im.dept            = NVL(tde.dept (+), im.dept)
         and im.class           = NVL(tde.class (+), im.class)
         and im.subclass        = NVL(tde.subclass (+), im.subclass)
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item list - tran item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             DECODE(im.item_parent,
                    NULL, NULL,
                    im.diff_1),
             NULL, -- item_parent
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_promo_dtl_skulist pdsl,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type             = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess  = 0
         and mer.skulist                = pdsl.skulist
         and mer.price_event_id         = pdsl.price_event_id
         and pdsl.item_level            = RPM_CONSTANTS.IL_ITEM_LEVEL
         and pdsl.item                  = mer.item
         and im.item                    = pdsl.item
         and im.item_level              = im.tran_level
         and mer.price_event_id         = tde.exception_parent_id (+)
         and im.item                    = tde.item (+)
         and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Tran Level for Item Lists - parent item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_promo_dtl_skulist pdsl,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 0
         and mer.skulist               = pdsl.skulist
         and mer.price_event_id        = pdsl.price_event_id
         and pdsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and pdsl.item                 = mer.item
         and im.item_parent            = pdsl.item
         and mer.price_event_id        = tde.exception_parent_id (+)
         and im.item                   = NVL(tde.item (+), im.item)
         and im.item_level             = im.tran_level
      union all
      -- Parent Diff Level for Item Lists - parent item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item_parent item,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_promo_dtl_skulist pdsl,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 0
         and mer.skulist               = pdsl.skulist
         and mer.price_event_id        = pdsl.price_event_id
         and pdsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and pdsl.item                 = mer.item
         and im.item_parent            = pdsl.item
         and im.diff_1                 is NOT NULL
         and mer.price_event_id        = tde.exception_parent_id (+)
         and mer.item                  = tde.item (+)
         and im.diff_1                 = NVL(tde.diff_id (+), im.diff_1)
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and im.item_level             = im.tran_level
      union all
      -- Parent Level for Item Lists - parent item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_promo_dtl_skulist pdsl,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 0
         and mer.skulist               = pdsl.skulist
         and mer.price_event_id        = pdsl.price_event_id
         and pdsl.item_level           = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and pdsl.item                 = mer.item
         and im.item                   = pdsl.item
         and im.item_level             = im.tran_level - 1
         and mer.price_event_id        = tde.exception_parent_id (+)
         and im.item                   = tde.item (+)
         and im.diff_1                 = NVL(tde.diff_id (+), im.diff_1)
      union all
      -- Tran Level for PEIL - tran item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             rmld.item,
             mer.diff_id,
             NULL, --item_parent
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail rmld,
             txn_dtl_exclusions tde
       where mer.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and mer.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and mer.item                 = rmld.item
         and mer.price_event_id       = tde.exception_parent_id (+)
         and mer.item                 = tde.item (+)
      union all
      -- Tran Level for PEIL - parent item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail rmld,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and mer.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and mer.item                 = rmld.item
         and im.item_parent           = rmld.item
         and im.item_level            = im.tran_level
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id       = tde.exception_parent_id (+)
         and im.item                  = NVL(tde.item (+), im.item)
      union all
      -- Parent Diff Level for PEIL - parent item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             im.dept,
             im.class,
             im.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail rmld,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and mer.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = mer.item
         and im.item_parent           = rmld.item
         and im.diff_1                is NOT NULL
         and mer.price_event_id       = tde.exception_parent_id (+)
         and mer.item                 = tde.item (+)
         and im.diff_1                = NVL(tde.diff_id (+), im.diff_1)
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      -- Parent Level for PEIL - parent item level Promotion
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             rmld.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.item is NULL then
                   0
                else
                   1
             end --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail rmld,
             txn_dtl_exclusions tde
       where mer.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
         and mer.price_event_itemlist = rmld.merch_list_id
         and mer.item                 = rmld.item
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and mer.price_event_id       = tde.exception_parent_id (+)
         and mer.item                 = tde.item (+)
         and mer.diff_id              = NVL(tde.diff_id (+), mer.diff_id)
      union all
      -- Merch Lists - Tran level items
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             mld.item,
             mer.diff_id,
             NULL, -- item_parent
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             NULL --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail mld
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 1
         and mer.skulist               = mld.merch_list_id
         and mer.item                  = mld.item
         and mld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
      union all
      -- Merch Lists - Parent/Diff level items - tran entries
      select /*+ ORDERED */ distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail mld,
             item_master im
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 1
         and mer.skulist               = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and mld.item                  = mer.item
         and im.diff_1                 = mer.diff_id
         and im.item_parent            = mld.item
         and im.diff_1                 = mld.diff_id
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      -- Merch Lists - Parent/Diff level items - parent/diff entries
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             mld.item,
             mld.diff_id,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             NULL --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail mld
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 1
         and mer.skulist               = mld.merch_list_id
         and mer.item                  = mld.item
         and mer.diff_id               = mld.diff_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
      union all
      -- Merch Lists - Parent level items - tran entries
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             im.diff_1,
             im.item_parent,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail mld,
             item_master im
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 1
         and mer.skulist               = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and mld.item                  = mer.item
         and im.item_parent            = mld.item
         and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      -- Merch Lists - Parent level items - parent/diff entries
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item_parent,
             im.diff_1,
             NULL,
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail mld,
             item_master im
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 1
         and mer.skulist               = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and mld.item                  = mer.item
         and im.item_parent            = mld.item
         and im.diff_1                 is NOT NULL
      union all
      -- Merch Lists - Parent level items - parent entries
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             mld.item,
             NULL,
             NULL,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             NULL --txn_man_excluded_item
        from merchnode mer,
             rpm_merch_list_detail mld
       where mer.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
         and LP_sys_gen_excl_reprocess = 1
         and mld.item                  = mer.item
         and mer.skulist               = mld.merch_list_id
         and mld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
      union all
      -- parent level and tran level item with parents for Storewide Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             case im.item_level
                when im.tran_level then
                   im.diff_1
                else
                   NULL
             end,
             case im.item_level
                when im.tran_level then
                   im.item_parent
                else
                   NULL
             end,
             case im.item_level
                when im.tran_level then
                   RPM_CONSTANTS.ITEM_MERCH_TYPE
                else
                   RPM_CONSTANTS.PARENT_MERCH_TYPE
             end,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                     tde.dept is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                     tde.class is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                     tde.subclass is NOT NULL then
                   1
                else
                   case
                      when tde.item is NULL then
                         0
                      else
                         1
                   end
             end --txn_man_excluded_item
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
         and mer.item           = im.item
         and (   (    im.item_level  = im.tran_level
                  and im.item_parent is NOT NULL)
              or im.item_level       = im.tran_level - 1)
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and im.item            = NVL(tde.item (+), im.item)
         and im.diff_1          = NVL(tde.diff_id (+), im.diff_1)
         and im.dept            = NVL(tde.dept (+), im.dept)
         and im.class           = NVL(tde.class (+), im.class)
         and im.subclass        = NVL(tde.subclass (+), im.subclass)
      union all
      -- Parent-Diff Level items for Storewide Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item_parent item,
             im.diff_1,
             NULL, -- item_parent
             RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE,
             1,
             RPM_CONSTANTS.PARENT_MERCH_TYPE,
             case
                when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                     tde.dept is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                     tde.class is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                     tde.subclass is NOT NULL then
                   1
                else
                   case
                      when tde.item is NULL then
                         0
                      else
                         1
                   end
             end -- txn_man_excluded_item
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
         and mer.item           = im.item
         and im.item_level      = im.tran_level
         and im.item_parent     is NOT NULL
         and im.diff_1          is NOT NULL
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and mer.dept           = NVL(tde.dept (+), mer.dept)
         and mer.class          = NVL(tde.class (+), mer.class)
         and mer.subclass       = NVL(tde.subclass (+), mer.subclass)
         and mer.diff_id        = NVL(tde.diff_id (+), mer.diff_id)
         and mer.item           = NVL(tde.item (+), mer.item)
      union all
      -- Tran level items with no parents for Storewide Level Promotion
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             mer.dept,
             mer.class,
             mer.subclass,
             im.item,
             NULL,
             NULL, -- item_parent
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             1,
             RPM_CONSTANTS.ITEM_MERCH_TYPE,
             case
                when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                     tde.dept is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                     tde.class is NOT NULL then
                   1
                when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                     tde.subclass is NOT NULL then
                   1
                else
                   case
                      when tde.item is NULL then
                         0
                      else
                         1
                   end
             end --txn_man_excluded_item
        from merchnode mer,
             item_master im,
             txn_dtl_exclusions tde
       where mer.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
         and mer.item           = im.item
         and im.item_level      = im.tran_level
         and im.item_parent     is NULL
         and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and mer.price_event_id = tde.exception_parent_id (+)
         and mer.dept           = NVL(tde.dept (+), mer.dept)
         and mer.class          = NVL(tde.class (+), mer.class)
         and mer.subclass       = NVL(tde.subclass (+), mer.subclass)
         and mer.item           = NVL(tde.item (+), mer.item);

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_item - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                        price_event_id,
                                        itemloc_id,
                                        location,
                                        zone_node_type,
                                        zone_id)
      with merchnode as
         (select bulk_cc_pe_id,
                 price_event_id,
                 merch_type,
                 dept,
                 class,
                 subclass,
                 itemloc_id
            from (select bulk_cc_pe_id,
                         price_event_id,
                         promo_dtl_merch_node_id,
                         merch_type,
                         dept,
                         class,
                         subclass,
                         DENSE_RANK() OVER (PARTITION BY price_event_id
                                                ORDER BY promo_dtl_merch_node_id,
                                                         dept,
                                                         class,
                                                         subclass) itemloc_id
                    from (-- Merch Hier Level
                          select LP_bulk_cc_pe_id bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 sc.dept,
                                 sc.class,
                                 sc.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_list_grp rpdlg,
                                 rpm_promo_dtl_list rpdl,
                                 rpm_promo_dtl_disc_ladder rpddl,
                                 rpm_promo_dtl_merch_node rpdm,
                                 subclass sc
                           where rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                             and rpd.promo_dtl_id           = rpet.price_event_id
                             and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                             and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                             and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                             and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                             and rpdm.merch_type            IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                                                RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                                                RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                             and sc.dept                    = rpdm.dept
                             and sc.class                   = NVL(rpdm.class, sc.class)
                             and sc.subclass                = NVL(rpdm.subclass, sc.subclass)
                          union all
                          -- Item or Parent Diff Level
                          select LP_bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 im.dept,
                                 im.class,
                                 im.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_list_grp rpdlg,
                                 rpm_promo_dtl_list rpdl,
                                 rpm_promo_dtl_disc_ladder rpddl,
                                 rpm_promo_dtl_merch_node rpdm,
                                 item_master im
                           where rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                             and rpd.promo_dtl_id           = rpet.price_event_id
                             and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                             and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                             and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                             and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                             and rpdm.merch_type            IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                                                RPM_CONSTANTS.PARENT_ITEM_DIFF)
                             and im.item                    = rpdm.item
                          union all
                          -- Skulist Level
                          select LP_bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 im.dept,
                                 im.class,
                                 im.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_list_grp rpdlg,
                                 rpm_promo_dtl_list rpdl,
                                 rpm_promo_dtl_disc_ladder rpddl,
                                 rpm_promo_dtl_merch_node rpdm,
                                 rpm_promo_dtl_skulist rpds,
                                 item_master im
                           where rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                             and rpd.promo_dtl_id           = rpet.price_event_id
                             and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                             and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                             and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                             and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                             and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                             and rpds.price_event_id        = rpet.price_event_id
                             and rpds.skulist               = rpdm.skulist
                             and im.item                    = rpds.item
                          union all
                          -- Storewide level - store level
                          select distinct LP_bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 im.dept,
                                 im.class,
                                 im.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_merch_node rpdm,
                                 rpm_promo_zone_location rpzl,
                                 item_loc il,
                                 item_master im
                           where rpet.bulk_cc_pe_id  = LP_bulk_cc_pe_id
                             and rpd.promo_dtl_id    = rpet.price_event_id
                             and rpdm.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                             and rpdm.promo_dtl_id   = rpd.promo_dtl_id
                             and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                             and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and rpzl.location       = il.loc
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and il.item             = im.item
                          union all
                          -- Storewide level - zone level
                          select distinct LP_bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 im.dept,
                                 im.class,
                                 im.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_merch_node rpdm,
                                 rpm_promo_zone_location rpzl,
                                 rpm_zone_location rzl,
                                 item_loc il,
                                 item_master im
                           where rpet.bulk_cc_pe_id  = LP_bulk_cc_pe_id
                             and rpd.promo_dtl_id    = rpet.price_event_id
                             and rpdm.merch_type     = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                             and rpdm.promo_dtl_id   = rpd.promo_dtl_id
                             and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                             and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and rzl.loc_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and rzl.zone_id         = rpzl.zone_id
                             and rzl.location        = il.loc
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and il.item             = im.item
                          union all
                          -- Price Event ItemList
                          select LP_bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 im.dept,
                                 im.class,
                                 im.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_list_grp rpdlg,
                                 rpm_promo_dtl_list rpdl,
                                 rpm_promo_dtl_disc_ladder rpddl,
                                 rpm_promo_dtl_merch_node rpdm,
                                 rpm_merch_list_detail rmld,
                                 item_master im
                           where rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                             and rpd.promo_dtl_id           = rpet.price_event_id
                             and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                             and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                             and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                             and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                             and rpdm.merch_type            = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                             and rmld.merch_list_id         = rpdm.price_event_itemlist
                             and im.item                    = rmld.item
                          union all
                          -- SGE
                          select LP_bulk_cc_pe_id,
                                 rpet.price_event_id,
                                 rpdm.promo_dtl_merch_node_id,
                                 rpdm.merch_type,
                                 im.dept,
                                 im.class,
                                 im.subclass
                            from rpm_bulk_cc_pe_thread rpet,
                                 rpm_promo_dtl rpd,
                                 rpm_promo_dtl_list_grp rpdlg,
                                 rpm_promo_dtl_list rpdl,
                                 rpm_promo_dtl_disc_ladder rpddl,
                                 rpm_promo_dtl_merch_node rpdm,
                                 rpm_merch_list_head mlh,
                                 rpm_merch_list_detail mld,
                                 item_master im
                           where LP_sys_gen_excl_reprocess  = 1
                             and rpet.bulk_cc_pe_id         = LP_bulk_cc_pe_id
                             and rpd.exception_parent_id    = rpet.price_event_id
                             and rpdlg.promo_dtl_id         = rpd.promo_dtl_id
                             and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id
                             and rpddl.promo_dtl_list_id    = rpdl.promo_dtl_list_id
                             and rpdm.promo_dtl_list_id     = rpddl.promo_dtl_list_id
                             and rpdm.merch_type            = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                             and mlh.price_event_id         = rpd.promo_dtl_id
                             and mlh.merch_list_id          = rpdm.skulist
                             and mlh.merch_list_type        = RPM_CONSTANTS.MERCH_LIST_HEAD_SGE
                             and mlh.price_event_type       IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
                             and mld.merch_list_id          = mlh.merch_list_id
                             and mld.item                   = im.item)))
      -- Location Level Price Events (Promotion can only be applicable to STORE)
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             rpzl.location,
             rpzl.zone_node_type,
             NULL
        from merchnode mer,
             rpm_promo_zone_location rpzl
       where rpzl.promo_dtl_id   = mer.price_event_id
         and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
      union all
      -- Locs in Zone Level price events
      -- For Non PZG Zone, set the zone_id to NULL
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             rzl.location,
             rzl.loc_type,
             case
                when rmrde.regular_zone_group = rz.zone_group_id then
                   rz.zone_id
                else
                   NULL
             end
        from merchnode mer,
             rpm_promo_zone_location rpzl,
             rpm_zone rz,
             rpm_zone_location rzl,
             rpm_merch_retail_def_expl rmrde
       where rpzl.promo_dtl_id   = mer.price_event_id
         and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id          = rpzl.zone_id
         and rzl.zone_id         = rz.zone_id
         and rzl.loc_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         and rmrde.dept          = mer.dept
         and rmrde.class         = mer.class
         and rmrde.subclass      = mer.subclass
      union all
      -- Zone level price events and Zone is part of PZG
      select distinct
             mer.bulk_cc_pe_id,
             mer.price_event_id,
             mer.itemloc_id,
             rpzl.zone_id,
             rpzl.zone_node_type,
             NULL
        from merchnode mer,
             rpm_promo_zone_location rpzl,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmrde
       where rpzl.promo_dtl_id        = mer.price_event_id
         and rpzl.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rz.zone_id               = rpzl.zone_id
         and rmrde.dept               = mer.dept
         and rmrde.class              = mer.class
         and rmrde.subclass           = mer.subclass
         and rmrde.regular_zone_group = rz.zone_group_id;

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - insert into rpm_bulk_cc_pe_location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (LP_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                           LP_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program,
                                           RPM_CONSTANTS.LOG_COMPLETED,
                                           'End of '||L_program);

      end if;

   EXCEPTION

      when others then
         goto EXPLODE_PROMOS_2;

   END;

   <<EXPLODE_PROMOS_2>>

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END EXPLODE_PROMOS;

--------------------------------------------------------------------------------

FUNCTION NEED_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                    O_need_chunk          OUT NUMBER,
                    I_bulk_cc_pe_id    IN     NUMBER,
                    I_pe_sequence_id   IN     NUMBER,
                    I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_BULK_CC_THREADING_SQL.NEED_CHUNK';

   cursor C_NEED_CHUNK is
      select NVL(need_chunk, 0)
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number;
BEGIN

   open C_NEED_CHUNK;
   fetch C_NEED_CHUNK into O_need_chunk;
   close C_NEED_CHUNK;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END NEED_CHUNK;

--------------------------------------------------------------------------------

FUNCTION LOCK_PE(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                 I_bulk_cc_pe_id        IN     NUMBER,
                 I_rib_transaction_id   IN     NUMBER,
                 I_parent_thread_number IN     NUMBER,
                 I_thread_number        IN     NUMBER,
                 I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_BULK_CC_THREADING_SQL.LOCK_PE';

   L_locked_locations_bcc_pe_ids OBJ_NUM_NUM_NUM_STR_TBL  := NULL;
   L_pe_type                     VARCHAR2(3)              := NULL;
   L_user_name                   VARCHAR2(30)             := NULL;
   L_dept                        NUMBER(4)                := NULL;
   L_depts                       OBJ_NUMERIC_ID_TABLE     := NEW OBJ_NUMERIC_ID_TABLE();
   L_dept_items                  NUMBER                   := 1;
   L_thread_items                NUMBER                   := 0;
   L_cc_error_tbl                CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   L_pe_ids                      OBJ_NUMERIC_ID_TABLE     := NULL;

   cursor C_DEPTS is
      select rpi.dept,
             COUNT(1) thread_items
        from rpm_bulk_cc_pe_thread rpt,
             rpm_bulk_cc_pe_item rpi
       where rpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
         and rpt.parent_thread_number        = I_parent_thread_number
         and rpt.thread_number               = I_thread_number
         and rpt.man_txn_exclusion_parent_id is NULL
         and rpi.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
         and rpi.price_event_id              = rpt.price_event_id
       group by rpi.dept;

   cursor C_LOCK_IL_LARGE is
      select il.rowid
        from (select /*+ ORDERED NO_MERGE */
                     rpi.price_event_id,
                     rpi.item,
                     rpl.location,
                     rpi.itemloc_id
                from rpm_bulk_cc_pe_thread rpt,
                     rpm_bulk_cc_pe_item rpi,
                     rpm_bulk_cc_pe_location rpl
               where rpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
                 and rpt.parent_thread_number        = I_parent_thread_number
                 and rpt.thread_number               = I_thread_number
                 and rpt.man_txn_exclusion_parent_id is NULL
                 and rpi.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
                 and rpi.price_event_id              = rpt.price_event_id
                 and rpl.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
                 and rpl.price_event_id              = rpt.price_event_id
                 and rpi.itemloc_id                  = rpl.itemloc_id
                 and rpl.zone_node_type              IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rpi.dept                        = L_dept
                 and rpi.merch_level_type            = RPM_CONSTANTS.ITEM_MERCH_TYPE) pe_itemloc,
             rpm_item_loc il
       where il.dept = L_dept
         and il.item = pe_itemloc.item
         and il.loc  = pe_itemloc.location
         for update of il.item nowait;

   cursor C_LOCK_IL_SMALL is
      select il.rowid
        from (select /*+ ORDERED NO_MERGE */
                     rpi.price_event_id,
                     rpi.dept,
                     rpi.item,
                     rpl.location,
                     rpi.itemloc_id
                from rpm_bulk_cc_pe_thread rpt,
                     rpm_bulk_cc_pe_item rpi,
                     rpm_bulk_cc_pe_location rpl
               where rpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
                 and rpt.parent_thread_number        = I_parent_thread_number
                 and rpt.thread_number               = I_thread_number
                 and rpt.man_txn_exclusion_parent_id is NULL
                 and rpi.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
                 and rpi.price_event_id              = rpt.price_event_id
                 and rpl.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
                 and rpl.price_event_id              = rpt.price_event_id
                 and rpi.merch_level_type            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rpi.itemloc_id                  = rpl.itemloc_id
                 and rpl.zone_node_type              IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)) pe_itemloc,
             rpm_item_loc il
       where il.dept = pe_itemloc.dept
         and il.item = pe_itemloc.item
         and il.loc  = pe_itemloc.location
         for update of il.item nowait;

   cursor C_LOCK_IL_SINGLE_PE(I_price_event_id NUMBER) is
      select il.rowid
        from (select /*+ ORDERED NO_MERGE */
                     rpi.price_event_id,
                     rpi.dept,
                     rpi.item,
                     rpl.location,
                     rpi.itemloc_id
                from rpm_bulk_cc_pe_thread rpt,
                     rpm_bulk_cc_pe_item rpi,
                     rpm_bulk_cc_pe_location rpl
               where rpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
                 and rpt.parent_thread_number        = I_parent_thread_number
                 and rpt.thread_number               = I_thread_number
                 and rpt.man_txn_exclusion_parent_id is NULL
                 and rpt.price_event_id              = I_price_event_id
                 and rpi.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
                 and rpi.price_event_id              = rpt.price_event_id
                 and rpl.bulk_cc_pe_id               = rpt.bulk_cc_pe_id
                 and rpl.price_event_id              = rpt.price_event_id
                 and rpi.merch_level_type            = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rpi.itemloc_id                  = rpl.itemloc_id
                 and rpl.zone_node_type              IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)) pe_itemloc,
             rpm_item_loc il
       where il.dept = pe_itemloc.dept
         and il.item = pe_itemloc.item
         and il.loc  = pe_itemloc.location
         for update of il.item nowait;

   cursor C_CHECK_LOCS_LOCKED is
      select OBJ_NUM_NUM_NUM_STR_REC(price_event_id,
                                     locking_bulk_cc_pe_id,
                                     locking_price_event_id,
                                     locking_price_event_type)
        from (select /*+ CARDINALITY(pe_ids 10)*/
                     distinct rpt.price_event_id,
                     pe_lock.bulk_cc_pe_id locking_bulk_cc_pe_id,
                     pe_lock.price_event_id locking_price_event_id,
                     pe_lock.price_event_type locking_price_event_type
                from table(cast(L_pe_ids as OBJ_NUMERIC_ID_TABLE)) pe_ids,
                     rpm_bulk_cc_pe_thread rpt,
                     rpm_pe_cc_lock pe_lock
               where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
                 and rpt.parent_thread_number = I_parent_thread_number
                 and rpt.thread_number        = I_thread_number
                 and rpt.price_event_id       = VALUE(pe_ids)
                 and (   (    pe_lock.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                          and EXISTS (select 1
                                        from rpm_zone_location rzl,
                                             rpm_bulk_cc_pe_location loc
                                       where loc.location       = rzl.location
                                         and rzl.loc_type       = loc.zone_node_type
                                         and rzl.zone_id        = pe_lock.zone_id
                                         and loc.bulk_cc_pe_id  = rpt.bulk_cc_pe_id
                                         and loc.price_event_id = rpt.price_event_id))
                      or (    pe_lock.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                          and EXISTS (select 1
                                        from rpm_bulk_cc_pe_location loc
                                       where loc.location = pe_lock.location
                                         and loc.bulk_cc_pe_id = rpt.bulk_cc_pe_id
                                         and loc.price_event_id  = rpt.price_event_id))));

   cursor C_CHECK_ITEMS_LOCKED is
      select CONFLICT_CHECK_ERROR_REC(number_1,
                                      NULL,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'item_locs_in_price_event_are_locked')
        from table(cast(L_locked_locations_bcc_pe_ids as OBJ_NUM_NUM_NUM_STR_TBL)) locked_ids
       where EXISTS -- DEPT
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.DEPT_LEVEL_ITEM
                        and pe_lock.dept             IN (select /*+ ORDERED USE_HASH(rpi, im) */
                                                                distinct im.dept
                                                           from rpm_bulk_cc_pe_item rpi,
                                                                item_master im
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_event_id = locked_ids.number_1
                                                            and im.item            = rpi.item))
          or EXISTS -- CLASS
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.CLASS_LEVEL_ITEM
                        and (pe_lock.dept,
                             pe_lock.class)          IN (select /*+ ORDERED USE_HASH(rpi, im) */
                                                                distinct im.dept,
                                                                im.class
                                                           from rpm_bulk_cc_pe_item rpi,
                                                                item_master im
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_event_id = locked_ids.number_1
                                                            and im.item            = rpi.item))
          or EXISTS -- SUBCLASS
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM
                        and (pe_lock.dept,
                             pe_lock.class,
                             pe_lock.subclass)       IN (select /*+ ORDERED USE_HASH(rpi, im) */
                                                                distinct im.dept,
                                                                im.class,
                                                                im.subclass
                                                           from rpm_bulk_cc_pe_item rpi,
                                                                item_master im
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_event_id = locked_ids.number_1
                                                            and im.item            = rpi.item))
          or EXISTS -- link code level
                    (select /*+ CARDINALITY(locked_ids,10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.LINK_CODE_LEVEL_ITEM
                        and EXISTS (select 1
                                      from rpm_bulk_cc_pe_item rpi,
                                           rpm_link_code_attribute lca
                                     where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                       and rpi.price_event_id = locked_ids.number_1
                                       and lca.link_code      = pe_lock.link_code_id
                                       and lca.item           = rpi.item))
          or EXISTS -- price change item lists
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        and pe_lock.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                        and EXISTS (select 1
                                      from rpm_price_change_skulist pcsl,
                                           rpm_bulk_cc_pe_item rpi
                                     where pcsl.price_event_id = pe_lock.price_event_id
                                       and pcsl.skulist        = pe_lock.skulist
                                       and pcsl.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and rpi.item            = pcsl.item
                                    union all
                                    select 1
                                      from rpm_price_change_skulist pcsl,
                                           rpm_bulk_cc_pe_item rpi,
                                           item_master im
                                     where pcsl.price_event_id = pe_lock.price_event_id
                                       and pcsl.skulist        = pe_lock.skulist
                                       and pcsl.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and pcsl.item           = im.item_parent
                                       and im.item             = rpi.item))
          or EXISTS -- clearance item lists
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        and pe_lock.price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET)
                        and EXISTS (select 1
                                      from rpm_clearance_skulist pcsl,
                                           rpm_bulk_cc_pe_item rpi
                                     where pcsl.price_event_id = pe_lock.price_event_id
                                       and pcsl.skulist        = pe_lock.skulist
                                       and pcsl.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and rpi.item            = pcsl.item
                                    union all
                                    select 1
                                      from rpm_clearance_skulist pcsl,
                                           rpm_bulk_cc_pe_item rpi,
                                           item_master im
                                     where pcsl.price_event_id = pe_lock.price_event_id
                                       and pcsl.skulist        = pe_lock.skulist
                                       and pcsl.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and pcsl.item           = im.item_parent
                                       and im.item             = rpi.item))
          or EXISTS -- promotion item lists
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                        and pe_lock.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
                        and EXISTS (select 1
                                      from rpm_promo_dtl_skulist pcsl,
                                           rpm_bulk_cc_pe_item rpi
                                     where pcsl.price_event_id = pe_lock.price_event_id
                                       and pcsl.skulist        = pe_lock.skulist
                                       and pcsl.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and rpi.item            = pcsl.item
                                    union all
                                    select 1
                                      from rpm_promo_dtl_skulist pcsl,
                                           rpm_bulk_cc_pe_item rpi,
                                           item_master im
                                     where pcsl.price_event_id = pe_lock.price_event_id
                                       and pcsl.skulist        = pe_lock.skulist
                                       and pcsl.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and pcsl.item           = im.item_parent
                                       and im.item             = rpi.item))
          or EXISTS -- price event item lists
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                        and EXISTS (select 1
                                      from rpm_merch_list_detail rmld,
                                           rpm_bulk_cc_pe_item rpi
                                     where rmld.merch_list_id  = pe_lock.price_event_itemlist
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and rpi.item            = rmld.item
                                    union all
                                    select 1
                                      from rpm_merch_list_detail rmld,
                                           rpm_bulk_cc_pe_item rpi,
                                           item_master im
                                     where rmld.merch_list_id  = pe_lock.price_event_itemlist
                                       and rpi.bulk_cc_pe_id   = I_bulk_cc_pe_id
                                       and rpi.price_event_id  = locked_ids.number_1
                                       and rmld.item           = im.item_parent
                                       and im.item             = rmld.item))
          or EXISTS -- item level and parent level
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                        and pe_lock.item             IN (select rpi.item
                                                           from rpm_bulk_cc_pe_item rpi
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_event_id = locked_ids.number_1
                                                         union all
                                                         select /*+ ORDERED USE_NL(rpi, im) */
                                                                distinct im.item_parent
                                                           from rpm_bulk_cc_pe_item rpi,
                                                                item_master im
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_event_id = locked_ids.number_1
                                                            and im.item            = rpi.item))
          or EXISTS -- parent/diff level
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.PARENT_ITEM_DIFF
                        and pe_lock.item             IN (select /*+ ORDERED USE_NL(rpi, im) */
                                                                distinct im.item_parent
                                                           from rpm_bulk_cc_pe_item rpi,
                                                                item_master im
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_event_id = locked_ids.number_1
                                                            and im.item            = rpi.item
                                                            and (   pe_lock.diff_id = im.diff_1
                                                                 or pe_lock.diff_id = im.diff_2
                                                                 or pe_lock.diff_id = im.diff_3
                                                                 or pe_lock.diff_id = im.diff_4)))
          or EXISTS -- storewide-loc
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock,
                            item_loc il
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                        and pe_lock.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                        and pe_lock.location         = il.loc
                        and il.item                  IN (select item
                                                           from rpm_bulk_cc_pe_item rpi
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_Event_id = locked_ids.number_1))
          or EXISTS -- storewide-zone
                    (select /*+ CARDINALITY(locked_ids, 10) */ 1
                       from rpm_pe_cc_lock pe_lock,
                            rpm_zone_location rzl,
                            item_loc il
                      where pe_lock.bulk_cc_pe_id    = locked_ids.number_2
                        and pe_lock.price_event_id   = locked_ids.number_3
                        and pe_lock.price_event_type = locked_ids.string
                        and pe_lock.merch_type       = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
                        and pe_lock.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                        and pe_lock.zone_id          = rzl.zone_id
                        and rzl.location             = il.loc
                        and il.item                  IN (select item
                                                           from rpm_bulk_cc_pe_item rpi
                                                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                                                            and rpi.price_Event_id = locked_ids.number_1));

   cursor C_GET_ALL_CONFLICTS is
      select CONFLICT_CHECK_ERROR_REC(price_event_id,
                                      NULL,
                                      error_type,
                                      error_string)
        from (-- locks held against RPM_ITEM_LOC
              select price_event_id,
                     error_type,
                     error_string
                from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL))
              union all
              -- item/locs on rpm_pe_cc_lock
              select price_event_id,
                     error_type,
                     error_string
                from table(cast(L_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL)));

   cursor C_PE_IDS_TO_BE_LOCKED is
      select VALUE(ids)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
      minus
      select price_event_id
        from table(cast(O_cc_error_tbl as CONFLICT_CHECK_ERROR_TBL));

BEGIN

   -- Put a DB lock in place for the item/locs affected by the
   -- events to ensure that two users aren't affecting the same item/locs at the
   -- same time.

   for rec IN C_DEPTS loop
      L_thread_items := L_thread_items + rec.thread_items;

      L_dept := rec.dept;
      L_depts.EXTEND;
      L_depts(L_depts.COUNT) := rec.dept;

      select /*+ INDEX (im ITEM_MASTER_I3) */
             COUNT(1) + L_dept_items
        into L_dept_items
        from item_master im
       where dept = L_dept;

   end loop;

   -- Check for database locks from other sessions using the same item/locs
   BEGIN

      if L_thread_items / L_dept_items > 0.25 then

         for i IN 1..L_depts.COUNT loop

            L_dept := L_depts(i);

            open C_LOCK_IL_LARGE;
            close C_LOCK_IL_LARGE;

         end loop;

      else

         open C_LOCK_IL_SMALL;
         close C_LOCK_IL_SMALL;

      end if;

   EXCEPTION

      when OTHERS then

         if SQLCODE = -54 then

            -- loop through each price event individually for which events are locked
            for i IN 1..I_price_event_ids.COUNT loop

               BEGIN

                  open C_LOCK_IL_SINGLE_PE(I_price_event_ids(i));
                  close C_LOCK_IL_SINGLE_PE;

               EXCEPTION

                  when OTHERS then

                     if SQLCODE = -54 then

                        L_cc_error_tbl.EXTEND;

                        L_cc_error_tbl(L_cc_error_tbl.COUNT) := CONFLICT_CHECK_ERROR_REC(I_price_event_ids(i),
                                                                                         NULL,
                                                                                         RPM_CONSTANTS.CONFLICT_ERROR,
                                                                                         'item_locs_in_price_event_are_locked');

                     else

                        RAISE; -- This calls the exception block at the bottom of the function

                     end if;

               END; -- End of the single price event database locking check

            end loop;

            O_cc_error_tbl := L_cc_error_tbl;

         else

            RAISE; -- This calls the exception block at the bottom of the function

         end if;

   END; -- End of the bulk price event database locking check

   select price_event_type,
          user_name
     into L_pe_type,
          L_user_name
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   --Need not check if the item-locations are locked when called from NIL.
   if NVL(L_user_name, '') != 'NewItemLocationBatch' then

      -- Verify if there are any item/locs in this price event that are currently
      -- locked for another price event. Look at locations first since that will be
      -- a smaller set of data to scan through, then look at items referencing the
      -- bulk_cc_pe_id for the location level locks found.

      open C_PE_IDS_TO_BE_LOCKED;
      fetch C_PE_IDS_TO_BE_LOCKED BULK COLLECT into L_pe_ids;
      close C_PE_IDS_TO_BE_LOCKED;

      open C_CHECK_LOCS_LOCKED;
      fetch C_CHECK_LOCS_LOCKED BULK COLLECT into L_locked_locations_bcc_pe_ids;
      close C_CHECK_LOCS_LOCKED;

      if L_locked_locations_bcc_pe_ids is NOT NULL and
         L_locked_locations_bcc_pe_ids.COUNT > 0 then

         L_cc_error_tbl := NULL;

         open C_CHECK_ITEMS_LOCKED;
         fetch C_CHECK_ITEMS_LOCKED BULK COLLECT into L_cc_error_tbl;
         close C_CHECK_ITEMS_LOCKED;

         if L_cc_error_tbl is NOT NULL and
            L_cc_error_tbl.COUNT > 0 then

            -- Sum errors from table and other session locking
            open C_GET_ALL_CONFLICTS;
            fetch C_GET_ALL_CONFLICTS BULK COLLECT into O_cc_error_tbl;
            close C_GET_ALL_CONFLICTS;

         end if;

      end if;
      -- end of location lock check

   end if;
   -- end of NIL check

   open C_PE_IDS_TO_BE_LOCKED;
   fetch C_PE_IDS_TO_BE_LOCKED BULK COLLECT into L_pe_ids;
   close C_PE_IDS_TO_BE_LOCKED;

   -- Create table locks for price events that do not have a lock currently
   if L_pe_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      insert into rpm_pe_cc_lock (bulk_cc_pe_id,
                                  price_event_id,
                                  price_event_type,
                                  merch_type,
                                  item,
                                  diff_id,
                                  link_code_id,
                                  skulist,
                                  price_event_itemlist,
                                  zone_node_type,
                                  location,
                                  zone_id,
                                  rib_transaction_id)
         select /*+ CARDINALITY(pe_ids 10)*/
                bcpt.bulk_cc_pe_id,
                bcpt.price_event_id,
                bcpt.price_event_type,
                bcpt.merch_node_type,
                rpc.item,
                rpc.diff_id,
                rpc.link_code,
                rpc.skulist,
                rpc.price_event_itemlist,
                bcpt.zone_node_type,
                rpc.location,
                rpc.zone_id,
                I_rib_transaction_id
           from table(cast(L_pe_ids as OBJ_NUMERIC_ID_TABLE)) pe_ids,
                rpm_bulk_cc_pe_thread bcpt,
                rpm_price_change rpc
          where bcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and bcpt.parent_thread_number = I_parent_thread_number
            and bcpt.thread_number        = I_thread_number
            and bcpt.price_event_id       = VALUE(pe_ids)
            and bcpt.price_event_id       = rpc.price_change_id;

   elsif L_pe_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      insert into rpm_pe_cc_lock (bulk_cc_pe_id,
                                  price_event_id,
                                  price_event_type,
                                  merch_type,
                                  item,
                                  diff_id,
                                  skulist,
                                  price_event_itemlist,
                                  zone_node_type,
                                  location,
                                  zone_id,
                                  rib_transaction_id)
         select /*+ CARDINALITY(pe_ids 10)*/
                bcpt.bulk_cc_pe_id,
                bcpt.price_event_id,
                bcpt.price_event_type,
                bcpt.merch_node_type,
                rc.item,
                rc.diff_id,
                rc.skulist,
                rc.price_event_itemlist,
                bcpt.zone_node_type,
                rc.location,
                rc.zone_id,
                I_rib_transaction_id
           from table(cast(L_pe_ids as OBJ_NUMERIC_ID_TABLE)) pe_ids,
                rpm_bulk_cc_pe_thread bcpt,
                rpm_clearance rc
          where bcpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and bcpt.parent_thread_number = I_parent_thread_number
            and bcpt.thread_number        = I_thread_number
            and bcpt.price_event_id       = VALUE(pe_ids)
            and bcpt.price_event_id       = rc.clearance_id;

   elsif L_pe_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                       RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                       RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                       RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                       RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                       RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                       RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      insert into rpm_pe_cc_lock (bulk_cc_pe_id,
                                  price_event_id,
                                  price_event_type,
                                  merch_type,
                                  dept,
                                  class,
                                  subclass,
                                  item,
                                  diff_id,
                                  skulist,
                                  price_event_itemlist,
                                  zone_node_type,
                                  location,
                                  zone_id,
                                  rib_transaction_id)
         select /*+ CARDINALITY(pe_ids 10)*/
                bcpt.bulk_cc_pe_id,
                bcpt.price_event_id,
                case
                   when bcp.end_state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
                      DECODE(bcpt.price_event_type,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,       RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,  RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,    RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,  RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,      RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO, RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,  RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE,
                             bcpt.price_event_type)
                   else
                      bcpt.price_event_type
                end price_event_type,
                pdmn.merch_type,
                pdmn.dept,
                pdmn.class,
                pdmn.subclass,
                pdmn.item,
                pdmn.diff_id,
                pdmn.skulist,
                pdmn.price_event_itemlist,
                pzl.zone_node_type,
                location,
                zone_id,
                I_rib_transaction_id
           from table(cast(L_pe_ids as OBJ_NUMERIC_ID_TABLE)) pe_ids,
                rpm_bulk_cc_pe_thread bcpt,
                rpm_bulk_cc_pe bcp,
                rpm_promo_zone_location pzl,
                rpm_promo_dtl_merch_node pdmn
          where bcpt.bulk_cc_pe_id               = I_bulk_cc_pe_id
            and bcpt.parent_thread_number        = I_parent_thread_number
            and bcpt.thread_number               = I_thread_number
            and bcpt.price_event_id              = VALUE(pe_ids)
            and bcpt.man_txn_exclusion_parent_id is NULL
            and bcp.bulk_cc_pe_id                = I_bulk_cc_pe_id
            and bcp.bulk_cc_pe_id                = bcpt.bulk_cc_pe_id
            and bcpt.price_event_id              = pzl.promo_dtl_id
            and bcpt.price_event_id              = pdmn.promo_dtl_id;

   end if;

   return 1;

EXCEPTION

   when OTHERS then

      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END LOCK_PE;

--------------------------------------------------------------------------------

FUNCTION CHECK_SYS_GEN_EXCL_ELIG(O_cc_error_tbl           OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type    IN     VARCHAR2,
                                 I_bulk_cc_pe_id       IN     NUMBER,
                                 I_pe_sequence_number  IN     NUMBER)
RETURN NUMBER IS

   L_program         VARCHAR2(60) := 'RPM_BULK_CC_THREADING_SQL.CHECK_SYS_GEN_EXCL_ELIG';

BEGIN

   merge into rpm_bulk_cc_pe_thread target
   using (select rowid t_rowid,
                 price_event_id
            from rpm_bulk_cc_pe_thread
           where bulk_cc_pe_id     = I_bulk_cc_pe_id
             and merch_node_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                       RPM_CONSTANTS.PARENT_ITEM_DIFF)
             and price_event_type  IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                       RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                       RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
             and zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select r.rowid t_rowid,
                 price_event_id
            from rpm_bulk_cc_pe_thread r,
                 item_master im
           where r.bulk_cc_pe_id   = I_bulk_cc_pe_id
             and im.item           = r.item
             and im.tran_level    != im.tran_level
             and merch_node_type   IN (RPM_CONSTANTS.ITEM_LEVEL_ITEM,
                                       RPM_CONSTANTS.PARENT_ITEM_DIFF)
             and price_event_type  IN (RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                       RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                       RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                       RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
             and zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rowid t_rowid,
                 price_event_id
            from rpm_bulk_cc_pe_thread
           where bulk_cc_pe_id    = I_bulk_cc_pe_id
             and merch_node_type  IN (RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM,
                                      RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                      RPM_CONSTANTS.DEPT_LEVEL_ITEM)
             and price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                      RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
             and zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rbcpt.rowid t_rowid,
                 rbcpt.price_event_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_price_change rpc
           where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
             and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
             and rbcpt.merch_node_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rbcpt.price_event_id   = rpc.price_change_id
             and rpc.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rbcpt.rowid t_rowid,
                 rbcpt.price_event_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_clearance rc
           where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
             and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
             and rbcpt.merch_node_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rbcpt.price_event_id   = rc.clearance_id
             and rc.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rbcpt.rowid t_rowid,
                 rbcpt.price_event_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_zone_location rpzl
           where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
             and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                            RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
             and rbcpt.merch_node_type  = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rbcpt.price_event_id   = rpd.promo_dtl_id
             and rpd.promo_dtl_id       = rpzl.promo_dtl_id
             and rpzl.zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rbcpt.rowid t_rowid,
                 rbcpt.price_event_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_price_change rpc
           where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
             and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
             and rbcpt.merch_node_type  = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rbcpt.price_event_id   = rpc.price_change_id
             and rpc.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rbcpt.rowid t_rowid,
                 rbcpt.price_event_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_clearance rc
           where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
             and rbcpt.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
             and rbcpt.merch_node_type  = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rbcpt.price_event_id   = rc.clearance_id
             and rc.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union all
          select rbcpt.rowid t_rowid,
                 rbcpt.price_event_id
            from rpm_bulk_cc_pe_thread rbcpt,
                 rpm_promo_dtl rpd,
                 rpm_promo_zone_location rpzl
           where rbcpt.bulk_cc_pe_id    = I_bulk_cc_pe_id
             and rbcpt.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                            RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO)
             and rbcpt.merch_node_type  = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rbcpt.price_event_id   = rpd.promo_dtl_id
             and rpd.promo_dtl_id       = rpzl.promo_dtl_id
             and rpzl.zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)) source
   on (source.t_rowid = target.rowid)
   when MATCHED then
     update
        set target.elig_for_sys_gen_exclusions = 1;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END CHECK_SYS_GEN_EXCL_ELIG;

--------------------------------------------------------------------------------

FUNCTION BULK_CREATE_UPDATE_PROMO_DEAL(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_user_name       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(60) := 'RPM_BULK_CC_THREADING_SQL.BULK_CREATE_UPDATE_PROMO_DEAL';

   L_return_code        VARCHAR2(1)              := NULL;
   L_error_message      RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_deal_ids_table     DEAL_IDS_TBL             := NULL;
   L_deal_head_rec      DEAL_HEAD_REC            := NULL;
   L_deal_detail_rec    DEAL_DETAIL_REC          := NULL;
   L_deal_detail_tbl    DEAL_DETAIL_TBL          := NULL;
   L_deal_comp_prom_rec DEAL_COMP_PROM_REC       := NULL;
   L_deal_comp_prom_tbl DEAL_COMP_PROM_TBL       := NULL;

   cursor C_COMPONENT is
      select rpdd.rpm_pending_deal_detail_id,
             rpdd.promo_id,
             case
                when t.promo_start_date < LP_vdate then
                   LP_vdate
                else
                   t.promo_start_date
             end promo_start_date,
             t.promo_end_date,
             rpdd.promo_comp_id,
             rpdd.deal_id,
             NULL deal_detail_id,
             rpdd.contribution_percent,
             rpd.rpm_pending_deal_id,
             rpd.partner_type,
             rpd.partner_id,
             rpd.supplier,
             DECODE(rpd.include_vat_ind,
                    1, 'Y',
                    'N') include_vat_ind,
             DECODE(rpd.stock_ledger_ind,
                    1, 'Y',
                    'N') stock_ledger_ind,
             rpd.invoice_proc_logic_type,
             rpd.invoice_proc_logic_code,
             rpd.deal_report_level_type,
             rpd.deal_report_level_code,
             rpd.bill_back_period_type,
             rpd.bill_back_period_code,
             rpd.bill_back_method_type,
             rpd.bill_back_method_code,
             rpdd.pending_deal_ind
        from (select /*+ ORDERED CARDINALITY(ids 10) INDEX(rpc PK_RPM_PROMO_COMP)*/
                     distinct rpc.promo_id,
                     rpd.promo_comp_id,
                     rpc.type,
                     rp.start_date promo_start_date,
                     rp.end_date promo_end_date
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp
               where rpd.promo_dtl_id  = VALUE(ids)
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 and rp.promo_id       = rpc.promo_id) t,
             rpm_pending_deal_detail rpdd,
             rpm_pending_deal rpd
       where rpdd.promo_id               = t.promo_id
         and rpdd.promo_comp_id          = t.promo_comp_id
         and rpd.rpm_pending_deal_id (+) = rpdd.deal_id
      union all
      select NULL rpm_pending_deal_detail_id,
             t.promo_id,
             case
                when t.promo_start_date < LP_vdate then
                   LP_vdate
                else
                   t.promo_start_date
             end promo_start_date,
             t.promo_end_date,
             t.promo_comp_id,
             dd.deal_id,
             dd.deal_detail_id,
             dcp.contribution_pct contribution_percent,
             NULL rpm_pending_deal_id,
             dh.partner_type,
             dh.partner_id,
             dh.supplier,
             include_vat_ind,
             stock_ledger_ind,
             NULL invoice_proc_logic_type,
             dh.invoice_processing_logic invoice_proc_logic_code,
             NULL deal_report_level_type,
             dh.deal_reporting_level deal_report_level_code,
             NULL bill_back_period_type,
             dh.bill_back_period bill_back_period_code,
             NULL bill_back_method_type,
             dh.bill_back_method bill_back_method_code,
             0 pending_deal_ind
        from (select /*+ ORDERED CARDINALITY(ids 10) INDEX(rpc PK_RPM_PROMO_COMP)*/
                     distinct rpc.promo_id,
                     rpd.promo_comp_id,
                     rpc.type,
                     rp.start_date promo_start_date,
                     rp.end_date promo_end_date
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo rp
               where rpd.promo_dtl_id  = VALUE(ids)
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 and rp.promo_id       = rpc.promo_id) t,
             deal_comp_prom dcp,
             deal_head dh,
             deal_detail dd
       where dcp.promotion_id  = t.promo_id
         and dcp.promo_comp_id = t.promo_comp_id
         and dh.deal_id        = dcp.deal_id
         and dd.deal_id        = dcp.deal_id
         and dd.deal_detail_id = dcp.deal_detail_id;

BEGIN

   LP_user_name := I_user_name;

   for rec IN C_COMPONENT loop

      L_deal_comp_prom_rec := NEW DEAL_COMP_PROM_REC(rec.promo_id,
                                                     rec.promo_comp_id,
                                                     rec.contribution_percent);

      L_deal_comp_prom_tbl := NEW DEAL_COMP_PROM_TBL(L_deal_comp_prom_rec);

      L_deal_detail_rec := NEW DEAL_DETAIL_REC(rec.deal_id,
                                               rec.deal_detail_id,
                                               rec.promo_start_date,
                                               rec.promo_end_date,
                                               rec.contribution_percent,
                                               LP_user_name,
                                               NULL,
                                               L_deal_comp_prom_tbl);

      L_deal_detail_tbl := NEW DEAL_DETAIL_TBL(L_deal_detail_rec);

      if rec.pending_deal_ind = 1 then

         -- Create Deal in RMS if this is a pending deal

         L_deal_head_rec := NEW DEAL_HEAD_REC(NVL(rec.partner_type, 'S'),
                                              rec.partner_id,
                                              rec.supplier,
                                              NULL,
                                              rec.promo_start_date,
                                              rec.promo_end_date,
                                              rec.bill_back_period_code,
                                              rec.deal_report_level_code,
                                              rec.bill_back_method_code,
                                              rec.invoice_proc_logic_code,
                                              rec.stock_ledger_ind,
                                              rec.include_vat_ind,
                                              LP_user_name,
                                              L_deal_detail_tbl);

         MERCH_DEALS_API_SQL.CREATE_DEAL(L_return_code,
                                         L_error_message,
                                         L_deal_ids_table,
                                         L_deal_head_rec);

         if L_return_code != API_CODES.SUCCESS then

            O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                              CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                          L_error_message,
                                                                          L_program,
                                                                          TO_CHAR(SQLCODE))));

            return 0;

         end if;
      end if;

      -- Now add the item/location into the deal for each record in L_deal_ids_table

      -- Delete the pending deal
      if rec.rpm_pending_deal_detail_id is NOT NULL then

         insert into rpm_pending_deal_detail_temp (bulk_cc_pe_id,
                                                   rpm_pending_deal_detail_id,
                                                   promo_id,
                                                   promo_comp_id,
                                                   deal_id,
                                                   pending_deal_ind,
                                                   contribution_percent,
                                                   state,
                                                   lock_version)
            select I_bulk_cc_pe_id,
                   rpm_pending_deal_detail_id,
                   promo_id,
                   promo_comp_id,
                   deal_id,
                   pending_deal_ind,
                   contribution_percent,
                   state,
                   lock_version
              from rpm_pending_deal_detail
             where rpm_pending_deal_detail_id = rec.rpm_pending_deal_detail_id;

         insert into rpm_pending_deal_temp (bulk_cc_pe_id,
                                            rpm_pending_deal_id,
                                            partner_type,
                                            partner_id,
                                            supplier,
                                            display_id,
                                            name,
                                            include_vat_ind,
                                            stock_ledger_ind,
                                            invoice_proc_logic_type,
                                            invoice_proc_logic_code,
                                            deal_report_level_type,
                                            deal_report_level_code,
                                            bill_back_period_type,
                                            bill_back_period_code,
                                            bill_back_method_type,
                                            bill_back_method_code,
                                            state,
                                            lock_version)
            select I_bulk_cc_pe_id,
                   rpm_pending_deal_id,
                   partner_type,
                   partner_id,
                   supplier,
                   display_id,
                   name,
                   include_vat_ind,
                   stock_ledger_ind,
                   invoice_proc_logic_type,
                   invoice_proc_logic_code,
                   deal_report_level_type,
                   deal_report_level_code,
                   bill_back_period_type,
                   bill_back_period_code,
                   bill_back_method_type,
                   bill_back_method_code,
                   state,
                   lock_version
              from rpm_pending_deal
             where rpm_pending_deal_id = rec.rpm_pending_deal_id;

         delete
           from rpm_pending_deal_detail
          where rpm_pending_deal_detail_id = rec.rpm_pending_deal_detail_id;

      end if;

      if rec.rpm_pending_deal_id is NOT NULL then

         delete
           from rpm_pending_deal
          where rpm_pending_deal_id = rec.rpm_pending_deal_id;

      end if;
   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END BULK_CREATE_UPDATE_PROMO_DEAL;

--------------------------------------------------------------------------------

FUNCTION DEAL_DELETE_CC(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                        I_bulk_cc_pe_id IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BULK_CC_THREADING_SQL.DEAL_DELETE_CC';

   L_state_count NUMBER(10) := 1;

   L_deal_ids      OBJ_NUMERIC_ID_TABLE              := NULL;
   L_pr_deal_ids   OBJ_NUMERIC_ID_TABLE              := NULL;
   L_promo_comp_id RPM_PROMO_COMP.PROMO_COMP_ID%TYPE := NULL;

   cursor C_DEAL_ID is
      select dcp.deal_id
        from deal_comp_prom dcp,
             rpm_promo_dtl rpd,
             rpm_bulk_cc_pe_thread rbcpt,
             deal_itemloc dil
       where dcp.promo_comp_id   = L_promo_comp_id
         and dcp.promo_comp_id   = rpd.promo_comp_id
         and rpd.promo_dtl_id    = rbcpt.price_event_id
         and rbcpt.bulk_cc_pe_id = I_bulk_cc_pe_id
         and dcp.deal_id         = dil.deal_id (+)
         and dil.deal_id         is NULL;

   cursor C_PEND_DEAL_ID is
      select deal_id
        from rpm_pending_deal_detail_temp
       where promo_comp_id = L_promo_comp_id;

   cursor C_PROMO_COMP is
      select distinct promo_comp_id
        from rpm_promo_dtl rpd,
             rpm_bulk_cc_pe_thread rbcpt
       where rpd.promo_dtl_id    = rbcpt.price_event_id
         and rbcpt.bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_PROMO_COMP_STATE is
      select COUNT(*)
        from rpm_promo_dtl rpd,
             rpm_bulk_cc_pe_thread rbcpt
       where rpd.promo_comp_id   = L_promo_comp_id
         and rpd.state           IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                     RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd.promo_dtl_id    = rbcpt.price_event_id
         and rbcpt.bulk_cc_pe_id = I_bulk_cc_pe_id;

BEGIN

   for rec1 IN C_PROMO_COMP loop

      L_promo_comp_id := rec1.promo_comp_id;
      L_deal_ids      := NULL;

      open C_PROMO_COMP_STATE;
      fetch C_PROMO_COMP_STATE into L_state_count;
      close C_PROMO_COMP_STATE;

      if L_state_count <= 0 then

         open C_DEAL_ID;
         fetch C_DEAL_ID BULK COLLECT into L_deal_ids;
         close C_DEAL_ID;

         open C_PEND_DEAL_ID;
         fetch C_PEND_DEAL_ID BULK COLLECT into L_pr_deal_ids;
         close C_PEND_DEAL_ID;

         if L_deal_ids is NOT NULL and
            L_deal_ids.COUNT > 0 then

            delete
              from deal_actuals_forecast
             where deal_id IN (select VALUE(ids)
                                 from table(cast(L_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids);

            delete
              from deal_comp_prom
             where deal_id IN (select VALUE(ids)
                                 from table(cast(L_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids);

            delete
              from deal_detail
             where deal_id IN (select VALUE(ids)
                                 from table(cast(L_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids);

            delete
              from deal_head
             where deal_id IN (select VALUE(ids)
                                 from table(cast(L_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids);

            insert into rpm_pending_deal_detail (rpm_pending_deal_detail_id,
                                                 promo_id,
                                                 promo_comp_id,
                                                 deal_id,
                                                 pending_deal_ind,
                                                 contribution_percent,
                                                 state,
                                                 lock_version)
               select /*+ CARDINALITY(ids 10)*/
                      rpddt.rpm_pending_deal_detail_id,
                      rpddt.promo_id,
                      rpddt.promo_comp_id,
                      rpddt.deal_id,
                      rpddt.pending_deal_ind,
                      rpddt.contribution_percent,
                      rpddt.state,
                      rpddt.lock_version
                 from table(cast(L_pr_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_pending_deal_detail_temp rpddt
                where rpddt.deal_id       = VALUE(ids)
                  and rpddt.bulk_cc_pe_id = I_bulk_cc_pe_id;

            insert into rpm_pending_deal (rpm_pending_deal_id,
                                          partner_type,
                                          partner_id,
                                          supplier,
                                          display_id,
                                          name,
                                          include_vat_ind,
                                          stock_ledger_ind,
                                          invoice_proc_logic_type,
                                          invoice_proc_logic_code,
                                          deal_report_level_type,
                                          deal_report_level_code,
                                          bill_back_period_type,
                                          bill_back_period_code,
                                          bill_back_method_type,
                                          bill_back_method_code,
                                          state,
                                          lock_version)
               select /*+ CARDINALITY(ids 10)*/
                      rpdt.rpm_pending_deal_id,
                      rpdt.partner_type,
                      rpdt.partner_id,
                      rpdt.supplier,
                      rpdt.display_id,
                      rpdt.name,
                      rpdt.include_vat_ind,
                      rpdt.stock_ledger_ind,
                      rpdt.invoice_proc_logic_type,
                      rpdt.invoice_proc_logic_code,
                      rpdt.deal_report_level_type,
                      rpdt.deal_report_level_code,
                      rpdt.bill_back_period_type,
                      rpdt.bill_back_period_code,
                      rpdt.bill_back_method_type,
                      rpdt.bill_back_method_code,
                      rpdt.state,
                      rpdt.lock_version
                 from table(cast(L_pr_deal_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                      rpm_pending_deal_temp rpdt
                where rpdt.rpm_pending_deal_id = VALUE(ids)
                  and rpdt.bulk_cc_pe_id       = I_bulk_cc_pe_id;

         end if;
      end if;
   end loop;

   delete
     from rpm_pending_deal_detail_temp
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   delete
     from rpm_pending_deal_temp
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END DEAL_DELETE_CC;

--------------------------------------------------------------------------------
-- This function would be called when the Price event conflict checking process fails due to an unexpected PLSQL error
-- except during or after PUSH_BACK in chunk mode.
FUNCTION RESET_PE_STATUS_ON_FAILURE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_bulk_cc_pe_id    IN     NUMBER,
                                    I_pe_sequence_id   IN     NUMBER,
                                    I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_BULK_CC_THREADING_SQL.RESET_PE_STATUS_ON_FAILURE';

   L_price_event_type          VARCHAR2(3)  := NULL;
   L_start_state               VARCHAR2(60) := NULL;
   L_old_clr_out_of_stock_date DATE         := NULL;
   L_old_clr_reset_date        DATE         := NULL;
   L_old_promo_end_date        DATE         := NULL;
   L_old_timbased_dtl_ind      NUMBER       := NULL;

   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;
   L_pe_info      OBJ_NUM_NUM_NUM_STR_TBL  := NULL;

   cursor C_PE is
      select OBJ_NUM_NUM_NUM_STR_REC(price_event_id,
                                     price_event_display_id,
                                     need_chunk,
                                     NULL)
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id        = I_bulk_cc_pe_id
         and parent_thread_number = I_pe_sequence_id
         and thread_number        = I_pe_thread_number
         and price_event_type     = L_price_event_type;

BEGIN

   select price_event_type,
          start_state,
          old_clr_out_of_stock_date,
          old_clr_reset_date,
          old_promo_end_date,
          old_timebased_dtl_ind,
          user_name into L_price_event_type,
                         L_start_state,
                         L_old_clr_out_of_stock_date,
                         L_old_clr_reset_date,
                         L_old_promo_end_date,
                         L_old_timbased_dtl_ind,
                         LP_user_name
     from rpm_bulk_cc_pe
    where bulk_cc_pe_id = I_bulk_cc_pe_id;

   open C_PE;
   fetch C_PE BULK COLLECT into L_pe_info;
   close C_PE;

   -- Set Back the state to the starting state
   if L_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      forall i IN 1 .. L_pe_info.COUNT
         update /*+ INDEX(target PK_RPM_PRICE_CHANGE) */ rpm_price_change
            set state = L_start_state
          where price_change_id = L_pe_info(i).number_1;

   elsif L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      forall i IN 1 .. L_pe_info.COUNT
         update /*+ INDEX(target PK_RPM_CLEARANCE) */ rpm_clearance
            set state = L_start_state
          where clearance_id = L_pe_info(i).number_1;

   elsif L_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET then

      forall i IN 1 .. L_pe_info.COUNT
         update /*+ INDEX(target PK_RPM_CLEARANCE) */ rpm_clearance
            set state             = L_start_state,
                out_of_stock_date = L_old_clr_out_of_stock_date,
                reset_date        = L_old_clr_reset_date
          where clearance_id = L_pe_info(i).number_1;

   elsif L_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET then

      forall i IN 1 .. L_pe_info.COUNT
         update rpm_clearance_reset
            set state = L_start_state
          where clearance_id = L_pe_info(i).number_1;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION)  then

      forall i IN 1 .. L_pe_info.COUNT
         update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl
            set state = L_start_state
          where promo_dtl_id = L_pe_info(i).number_1;

   elsif L_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      forall i IN 1 .. L_pe_info.COUNT
         update /*+ INDEX(target PK_RPM_PROMO_DTL) */ rpm_promo_dtl
            set state             = L_start_state,
                end_date          = L_old_promo_end_date,
                timebased_dtl_ind = NVL(L_old_timbased_dtl_ind, timebased_dtl_ind)
          where promo_dtl_id = L_pe_info(i).number_1;

   end if;

   if L_pe_info is NOT NULL and
      L_pe_info.COUNT > 0 and
      L_pe_info(1).number_3 = 1 then

      if RPM_CHUNK_CC_THREADING_SQL.ROLL_BACK(L_cc_error_tbl,
                                              L_price_event_type,
                                              I_bulk_cc_pe_id,
                                              L_pe_info(1).number_1,
                                              I_pe_thread_number,
                                              I_pe_sequence_id) = 0 then
         IO_cc_error_tbl := L_cc_error_tbl;
         return 0;
      end if;

      forall i IN 1 .. L_pe_info.COUNT
         delete
           from rpm_pe_cc_lock
          where bulk_cc_pe_id    = I_bulk_cc_pe_id
            and price_event_type = L_price_event_type
            and price_event_id   = L_pe_info(i).number_1;


   end if;

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then

      if POPULATE_CC_RESULT_TABLE(L_cc_error_tbl,
                                  IO_cc_error_tbl,
                                  I_bulk_cc_pe_id,
                                  I_pe_sequence_id,
                                  I_pe_thread_number,
                                  L_price_event_type,
                                  IO_cc_error_tbl(1).error_string) = 0 then

         IO_cc_error_tbl := L_cc_error_tbl;

         return 0;
      end if;

      -- populate the CC table for the binocular in GUI when error_type is 1
      insert into rpm_con_check_err
         (con_check_err_id,
          ref_class,
          ref_id,
          ref_display_id,
          error_date,
          message_key)
      select RPM_CON_CHECK_ERR_SEQ.NEXTVAL,
             RPM_CONSTANTS.COMP_DETAIL_REF_CLASS,
             ids.number_1,
             ids.number_2,
             LP_vdate,
             'plsql_conflict_checking_error'
        from table(cast(L_pe_info as OBJ_NUM_NUM_NUM_STR_TBL)) ids;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END RESET_PE_STATUS_ON_FAILURE;
--------------------------------------------------------------------------------
END RPM_BULK_CC_THREADING_SQL;
/
