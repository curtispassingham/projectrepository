CREATE OR REPLACE PACKAGE BODY RPM_CC_PROM_SPAN_LM AS

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION VALIDATE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                  I_promo_rec    IN     OBJ_RPM_CC_PROMO_REC)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_CC_PROM_SPAN_LM.VALIDATE';

   L_start_date      DATE;
   L_end_date        DATE;
   L_lm_prom_overlap RPM_SYSTEM_OPTIONS.LOC_MOVE_PROM_OVERLAP%TYPE;

   cursor C_CHECK is
      select CONFLICT_CHECK_ERROR_REC(NULL,
                                      NULL,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'event_causes_location_move_overlap')
        from table(cast(I_promo_rec.zone_ids as OBJ_NUMERIC_ID_TABLE)) z,
             rpm_location_move rlm
       where (   rlm.old_zone_id = value(z)
              or rlm.new_zone_id = value(z))
         and rlm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
         and rlm.effective_date  BETWEEN TRUNC(L_start_date) and TRUNC(L_end_date);

BEGIN

   select loc_move_prom_overlap
     into L_lm_prom_overlap
     from rpm_system_options;

   if L_lm_prom_overlap = 0 then

      L_end_date := NVL(I_promo_rec.rpcd_end_date, TO_DATE('3000', 'YYYY'));
      L_start_date := I_promo_rec.rpcd_start_date;

      open C_CHECK;
      fetch C_CHECK BULK COLLECT into O_cc_error_tbl;
      close C_CHECK;

      if O_cc_error_tbl.COUNT > 0 then
         return 0;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE;
--------------------------------------------------------------------------------

FUNCTION VALIDATE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_promo_ids        IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_CC_PROM_SPAN_LM.VALIDATE';

   L_error_rec  CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl  CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LO_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_lm_prom_overlap RPM_SYSTEM_OPTIONS.LOC_MOVE_PROM_OVERLAP%TYPE;

   cursor C_CHECK is
      select CONFLICT_CHECK_ERROR_REC(t.price_event_id,
                                      gtt.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'event_causes_location_move_overlap')
        from rpm_location_move rlm,
             (select /*+ CARDINALITY (ids 1000) CARDINALITY (ccet 1000) */
                     value(ids) price_event_id,
                     rpzl.zone_id,
                     rpd.start_date,
                     NVL(rpd.end_date, TO_DATE('3000', 'YYYY')) end_date
                from table(cast(I_promo_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl
               where value(ids)       != ccet.price_event_id
                 and rpd.promo_dtl_id  = value(ids)
                 and rpc.promo_comp_id = rpd.promo_comp_id
                 and rpc.type          = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                 and rpzl.promo_dtl_id = rpd.promo_dtl_id
                 and rpzl.zone_id     is NOT NULL) t,
             rpm_future_retail_gtt gtt
       where (   rlm.old_zone_id      = t.zone_id
              or rlm.new_zone_id      = t.zone_id)
         and rlm.state                = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
         and (    TRUNC(t.start_date) <  rlm.effective_date
              and TRUNC(t.end_date)  >= rlm.effective_date)
         and gtt.price_event_id       = t.price_event_id
         and (   gtt.location_move_id = rlm.location_move_id
              or L_lm_prom_overlap    = 0)
         and rownum                   = 1
      union all
      select CONFLICT_CHECK_ERROR_REC(t.price_event_id,
                                      gtt.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'event_causes_location_move_overlap')
        from rpm_location_move rlm,
             (select /*+ CARDINALITY (ids 1000) CARDINALITY (ccet 1000) */
                     value(ids) price_event_id,
                     rpzl.zone_id,
                     rpd.start_date,
                     NVL(rpd.end_date, TO_DATE('3000', 'YYYY')) end_date
                from table(cast(I_promo_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl
               where value(ids)         != ccet.price_event_id
                 and rpd.promo_dtl_id    = value(ids)
                 and rpc.promo_comp_id   = rpd.promo_comp_id
                 and rpc.type            = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
                 and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                 and rpzl.zone_node_type = 1) t,
             rpm_future_retail_gtt gtt
       where (   rlm.old_zone_id      = t.zone_id
              or rlm.new_zone_id      = t.zone_id)
         and rlm.state                = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
         and (    TRUNC(t.start_date) < rlm.effective_date
              and TRUNC(t.end_date)  >= rlm.effective_date)
         and gtt.price_event_id       = t.price_event_id
         and (   gtt.location_move_id = rlm.location_move_id
              or L_lm_prom_overlap    = 0)
         and rownum                   = 1
      union all
      select CONFLICT_CHECK_ERROR_REC(t.price_event_id,
                                      gtt.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'event_causes_location_move_overlap')
        from rpm_location_move rlm,
             (select /*+ CARDINALITY (ids 1000) CARDINALITY (ccet 1000) */
                     value(ids) price_event_id,
                     rpzl.zone_id,
                     rpd.start_date,
                     NVL(rpd.end_date, TO_DATE('3000', 'YYYY')) end_date
                from table(cast(I_promo_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     table(cast(L_error_tbl as conflict_check_error_tbl)) ccet,
                     rpm_promo_dtl rpd,
                     rpm_promo_comp rpc,
                     rpm_promo_zone_location rpzl
               where value(ids)         != ccet.price_event_id
                 and rpd.promo_dtl_id    = value(ids)
                 and rpc.promo_comp_id   = rpd.promo_comp_id
                 and rpc.type            = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                 and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                 and rpzl.zone_node_type = 1) t,
             rpm_future_retail_gtt gtt
       where (   rlm.old_zone_id      = t.zone_id
              or rlm.new_zone_id      = t.zone_id)
         and rlm.state                = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
         and (    TRUNC(t.start_date) <  rlm.effective_date
              and TRUNC(t.end_date)  >= rlm.effective_date)
         and gtt.price_event_id       = t.price_event_id
         and (   gtt.location_move_id = rlm.location_move_id
              or L_lm_prom_overlap    = 0)
         and rownum                   = 1;

BEGIN

   select loc_move_prom_overlap
     into L_lm_prom_overlap
     from rpm_system_options;

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then

      L_error_tbl := IO_cc_error_tbl;

   else
      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);
      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   open C_CHECK;
   fetch C_CHECK BULK COLLECT into LO_error_tbl;
   close C_CHECK;

   if LO_error_tbl is NOT NULL and
      LO_error_tbl.COUNT > 0 then

      if IO_cc_error_tbl is NULL or
         IO_cc_error_tbl.COUNT = 0 then

         IO_cc_error_tbl := LO_error_tbl;

      else

         for i IN 1..LO_error_tbl.COUNT loop
            IO_cc_error_tbl.EXTEND;
         IO_cc_error_tbl(IO_cc_error_tbl.COUNT) := LO_error_tbl(i);

         end loop;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE;
--------------------------------------------------------------------------------

END RPM_CC_PROM_SPAN_LM;
/

