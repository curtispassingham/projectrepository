CREATE OR REPLACE PACKAGE BODY RPM_CC_PROMO_LVL_EXCLUSION AS
-----------------------------------------------------------------------------------------------------------------------

FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CC_PROMO_LVL_EXCLUSION.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_PROMO_EXCLUSION_CHECK is
      select /*+ CARDINALITY(ids, 1) INDEX(rpile, RPM_PROMO_ITEM_LOC_EXPL_GTT_I2) */
             distinct gtt.price_event_id,
             gtt.future_retail_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_item_loc_expl_gtt rpile,
             rpm_future_retail_gtt gtt,
             rpm_promo_dtl rpd,
             rpm_promo_comp rpc
       where rpile.price_event_id        = VALUE(ids)
         and rpile.price_event_id        = gtt.price_event_id
         and rpile.price_event_id        NOT IN (select ccet.price_event_id
                                                   from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and rpile.dept                  = gtt.dept
         and rpile.item                  = gtt.item
         and NVL(rpile.diff_id, '-9999') = NVL(gtt.diff_id, '-9999')
         and rpile.zone_node_type        = gtt.zone_node_type
         and rpile.location              = gtt.location
         and rpile.detail_start_date     = gtt.action_date
         and rpd.promo_dtl_id            = rpile.price_event_id
         and rpd.promo_comp_id           = rpc.promo_comp_id
         and EXISTS (select 1
                       from rpm_promo_item_loc_expl_gtt rpile2,
                            rpm_promo_dtl rpd2
                      where rpile.promo_item_loc_expl_id = rpile2.promo_item_loc_expl_id
                        and rpile2.detail_change_type    = RPM_CONSTANTS.RETAIL_EXCLUDE
                        and rpile2.promo_id              = rpc.promo_id
                        and rpile2.promo_comp_id         = rpd2.promo_comp_id
                        and rpd2.state                   = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
                        and rpile2.exception_parent_id   is NULL);

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then

         L_error_tbl := IO_error_table;

      else

         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);

         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      end if;

      for rec IN C_PROMO_EXCLUSION_CHECK loop

         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'an_active_promotion_level_exclusion_exists_for_this_item_location');

         if IO_error_table is NULL then

            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

         else

            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;

         end if;

      end loop;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
-----------------------------------------------------------------------------------------------------------------------

END RPM_CC_PROMO_LVL_EXCLUSION;
/
