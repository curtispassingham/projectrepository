CREATE OR REPLACE PACKAGE RPM_ROLL_FORWARD_SQL AS
--------------------------------------------------------

FUNCTION EXECUTE(O_cc_error_tbl       OUT   CONFLICT_CHECK_ERROR_TBL,
                 I_price_event_type   IN    VARCHAR2,
                 I_nil_ind            IN    NUMBER DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION CALC_RETAIL_WITH_GUIDE(I_currency_code         IN     VARCHAR2,
                                I_change_type           IN     NUMBER,
                                I_change_amt            IN     NUMBER,
                                I_seed_retail           IN     NUMBER,
                                I_change_pct            IN     NUMBER,
                                I_price_guide_id        IN     NUMBER,
                                I_price_event_id        IN     NUMBER,
                                I_future_retail_id      IN     NUMBER,
                                I_dept_id               IN     NUMBER,
                                I_promo_retail_calc_ind IN     NUMBER DEFAULT 0,
                                I_cur_promo_retail      IN     NUMBER DEFAULT NULL,
                                I_timeline_seq          IN     NUMBER DEFAULT 0,
                                I_seq                   IN     NUMBER DEFAULT 0,
                                I_cs_promo_fr_id        IN     NUMBER DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------

FUNCTION CALC_REF_PROMO_DTL_ID (I_old_simple_promo_retail   IN     NUMBER,
                                I_new_simple_promo_retail   IN     NUMBER,
                                I_promo_dtl_id              IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------

END RPM_ROLL_FORWARD_SQL;
/