CREATE OR REPLACE PACKAGE RPM_BATCH_HIST_SQL AS
--------------------------------------------------------------------------------
FUNCTION RPM_BATCH_RUN_HIST_ENTRY(O_error_msg                 OUT VARCHAR2,
                                  I_batch_name             IN     VARCHAR2,
                                  I_user_id                IN     VARCHAR2,
                                  I_luw_value              IN     NUMBER,
                                  I_num_concurrent_threads IN     NUMBER,
                                  I_other_parameters       IN     VARCHAR2,
                                  I_log_path               IN     VARCHAR2,
                                  I_error_path             IN     VARCHAR2,
                                  I_export_path            IN     VARCHAR2,
                                  I_thread_number          IN     NUMBER,
                                  I_start_datetime         IN     DATE,
                                  I_end_datetime           IN     DATE,
                                  I_completion_status      IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PURGE_RPM_BATCH_RUN_HIST(O_error_msg       OUT VARCHAR2,
                                  I_schema_owner IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------

END RPM_BATCH_HIST_SQL;
/
