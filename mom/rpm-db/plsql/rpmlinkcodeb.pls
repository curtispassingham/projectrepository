CREATE OR REPLACE PACKAGE BODY RPM_LINK_CODE_SQL AS
--------------------------------------------------------
   ITEM_LEVEL_ITEM                  CONSTANT VARCHAR2(2) := 'I';
   ITEM_LEVEL_ITEMLIST              CONSTANT VARCHAR2(2) := 'IL';

   ITEM_TYPE_ITEM                   CONSTANT VARCHAR2(2) := 'I';
   ITEM_TYPE_PARENT                 CONSTANT VARCHAR2(2) := 'P';
   ITEM_TYPE_PARENT_DIFF            CONSTANT VARCHAR2(2) := 'PD';

   TRAN_ITEM                        CONSTANT NUMBER(1)   := 1;
   PARENT_ITEM                      CONSTANT NUMBER(1)   := 2;
   ---
   LP_hint                          VARCHAR2(4000);
--------------------------------------------------------
FUNCTION FILTER_ITEM(O_error_msg     IN OUT VARCHAR2,
                     I_pc_criteria   IN     OBJ_LINK_CODE_SEARCH_REC,
                     IO_item_from       OUT VARCHAR2,
                     IO_item_where      OUT VARCHAR2)
RETURN BOOLEAN;
---------------------------------------------------------
FUNCTION FILTER_LOC(O_error_msg     IN OUT VARCHAR2,
                    I_pc_criteria   IN     OBJ_LINK_CODE_SEARCH_REC,
                    IO_loc_from        OUT VARCHAR2,
                    IO_loc_where       OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------

PROCEDURE SEARCH(O_return_code          OUT NUMBER,
                 O_error_msg            OUT VARCHAR2,
                 O_lc_tbl               OUT OBJ_LINK_CODE_ATTRIBUTE_TBL,
                 O_lc_count             OUT NUMBER,
                 I_lc_criterias      IN OBJ_LINK_CODE_SEARCH_TBL)
IS

   ---
   L_criterias       OBJ_LINK_CODE_SEARCH_TBL;
   L_criteria        OBJ_LINK_CODE_SEARCH_REC;
   ---
   L_select          VARCHAR2(4000) := NULL;
   L_base_from       VARCHAR2(8000) := NULL;
   L_base_where      VARCHAR2(4000) := NULL;
   --
   L_link_code_from  VARCHAR2(4000) := NULL;
   L_link_code_where VARCHAR2(4000) := NULL;
   L_item_from       VARCHAR2(4000) := NULL;
   L_item_where      VARCHAR2(4000) := NULL;
   L_loc_from        VARCHAR2(4000) := NULL;
   L_loc_where       VARCHAR2(4000) := NULL;
   ---
   L_from            VARCHAR2(4000) := NULL;
   L_where           VARCHAR2(4000) := NULL;
   --
   TYPE linkCodeCursorType IS REF CURSOR;         -- define weak REF CURSOR type
   il_lc               linkCodeCursorType;       -- declare cursor variable

BEGIN

   DELETE FROM rpm_pc_search_num_tbl;
   DELETE FROM rpm_pc_search_char_tbl;
   DELETE FROM rpm_pc_search_merch_tbl;

   L_criterias := I_lc_criterias;
   L_criteria  := L_criterias(1);
   LP_hint     := NULL;

   IF l_criteria.link_code_ids IS NOT NULL AND
      l_criteria.link_code_ids.COUNT > 0 THEN

      INSERT INTO rpm_pc_search_num_tbl
      SELECT 'LINK_CODE_IDS',
              lci.*
      FROM TABLE(CAST(l_criteria.link_code_ids AS obj_numeric_id_table)) lci;

   END IF;

   IF l_criteria.dept_class_subclasses IS NOT NULL AND

      l_criteria.dept_class_subclasses.COUNT > 0 THEN

      INSERT INTO rpm_pc_search_merch_tbl
      SELECT 'DEPT_CLASS_SUBCLASSES',
             dcs.dept,
             dcs.class,
             dcs.subclass
      FROM TABLE(CAST(l_criteria.dept_class_subclasses AS obj_dept_class_subclass_tbl)) dcs;

   END IF;

   IF l_criteria.items IS NOT NULL AND

      l_criteria.items.COUNT > 0 THEN

      INSERT INTO rpm_pc_search_char_tbl
      SELECT 'ITEMS',
              i.*
      FROM TABLE(CAST(l_criteria.items AS obj_varchar_id_table)) i;

   END IF;

   IF l_criteria.diff_ids IS NOT NULL AND

      l_criteria.diff_ids.COUNT > 0 THEN

      INSERT INTO rpm_pc_search_char_tbl
      SELECT 'DIFF_IDS',
              di.*
      FROM TABLE(CAST(l_criteria.diff_ids AS obj_varchar_id_table)) di;

   END IF;


   IF l_criteria.zones IS NOT NULL AND

      l_criteria.zones.COUNT > 0 THEN

      INSERT INTO rpm_pc_search_num_tbl
      SELECT 'ZONES',
              z.*
      FROM TABLE(CAST(l_criteria.zones AS obj_numeric_id_table)) z;

   END IF;

   IF l_criteria.locations IS NOT NULL AND

      l_criteria.locations.COUNT > 0 THEN

      INSERT INTO rpm_pc_search_num_tbl
      SELECT 'LOCATIONS',
              l.*
      FROM TABLE(CAST(l_criteria.locations AS obj_numeric_id_table)) l;

   END IF;

   L_select := ' NEW obj_link_code_attribute_rec(';

   L_select :=  L_select ||
                             'rlca.link_code_attribute_id, '||             -- LINK_CODE_ATTRIBUTE_ID
                             'rlca.item, '||                               -- ITEM
                             'im.item_desc, ' ||                           -- ITEM_DESCRIPTION
                             'im.item_parent, '||                          -- ITEM_PARENT
                             'im2.item_desc, '||                           -- ITEM_PARENT_DESC
                             'loc.loctype, '||                             -- ZONE_NODE_TYPE
                             'rlca.location, ' ||                          -- LOCATION
                             'loc.name, '||                                -- LOCATION_DESCRIPTION
                             'rlca.link_code, '||                          -- LINK_CODE_ID
                             'rc.code, '||                                 -- LINK_CODE
                             'rc.description)';                            -- LINK_CODE_DESC

   L_base_from  :=         'rpm_link_code_attribute rlca, '||
                           'item_master im, '||
                           'item_master im2, '||
                           '(SELECT store loc, '||
                           '        0 loctype, '||
                           '        store_name name '||
                           '   FROM store '||
                           '  UNION '||
                           ' SELECT wh loc, '||
                           '        2 loctype, '||
                           '        wh_name name '||
                           '        FROM wh) loc, '||
                           'rpm_codes rc ';

   L_base_where := '     rlca.item = im.item ' ||
                   ' AND im.item_parent = im2.item(+) '||
                   ' AND rlca.location = loc.loc ' ||
                   ' AND rlca.link_code = rc.code_id ';

   IF L_criteria.link_code_ids IS NOT NULL THEN
        L_link_code_from  := ' rpm_pc_search_num_tbl lc, ';
        L_link_code_where := ' rlca.link_code = lc.numeric_id AND lc.criteria_type = ''LINK_CODE_IDS'' ';
        LP_hint           := ' lc , ';
   END IF;

   -- Filter the Item Criteria
   IF filter_item(O_error_msg,
                  L_criteria,
                  L_item_from,
                  L_item_where) = FALSE then
      O_return_code := 0;
      RETURN;
   END IF;

   IF filter_loc(O_error_msg,
                 L_criteria,
                 L_loc_from,
                 L_loc_where) = FALSE then
      O_return_code := 0;
      RETURN;
   END IF;

   if L_link_code_from is NOT NULL then
      L_from := ' FROM '||L_link_code_from ||' rpm_link_code_attribute rlca, ';
      if L_item_from is NOT NULL then
         if (L_criteria.items is NOT NULL and
             L_criteria.item_level_ind = ITEM_TYPE_ITEM) or
             L_criteria.item_itemlist_ind = ITEM_LEVEL_ITEMLIST then
            L_from := L_from || L_item_from || L_loc_from ||
                              'item_master im,  '||
                              'item_master im2, ';
         else
            L_from := L_from || L_loc_from ||
                              'item_master im,  '||
                              'item_master im2, '||
                              L_item_from;
         end if;
      else
         L_from := L_from || L_loc_from ||
                              'item_master im,  '||
                              'item_master im2, ';
      end if;

   elsif L_item_from is NOT NULL then
      if L_loc_where is NULL or
         L_criteria.locations is NOT NULL then
         if (L_criteria.items is NOT NULL and
             L_criteria.item_level_ind = ITEM_TYPE_ITEM) or
             L_criteria.item_itemlist_ind = ITEM_LEVEL_ITEMLIST then
            L_from := ' FROM '||L_item_from||' rpm_link_code_attribute rlca, '||
                              L_loc_from||
                              'item_master im,  '||
                              'item_master im2, ';
         else
            L_from := ' FROM '|| L_item_from ||
                              'item_master im, '||
                              'item_master im2, '||
                              'rpm_link_code_attribute rlca, '||
                       L_loc_from;
         end if;
      else
         L_from := ' FROM  '||L_loc_from||' rpm_link_code_attribute rlca, '||
                           L_item_from||
                           'item_master im,  '||
                           'item_master im2, ';
      end if;
   else
      L_from := ' FROM '||L_loc_from ||
                ' rpm_link_code_attribute rlca, '||
                ' item_master im, '||
                ' item_master im2, ';
   end if;

   L_from  := L_from ||       '(SELECT store loc, '||
                              '        0 loctype, '||
                              '        store_name name '||
                              '   FROM store '||
                              '  UNION '||
                              ' SELECT wh loc, '||
                              '        2 loctype, '||
                              '        wh_name name '||
                              '        FROM wh) loc, '||
                              'rpm_codes rc ';


   L_where  := L_link_code_where;

   if L_item_where is NOT NULL then
      if L_where is NULL then
         L_where := L_item_where;
      else
         L_where := L_where ||' AND '||L_item_where;
      end if;
   end if;

   if L_loc_where is not null then
      if L_where is NULL then
         L_where := L_loc_where;
      else
         L_where := L_where ||' AND '||L_loc_where;
      end if;
   end if;

   if L_where is NULL then
      L_where := L_base_where;
   else
      L_where := L_where ||' AND '||L_base_where;
   end if;

   L_where := ' WHERE '||L_where;

   LP_hint := '/*+ ORDERED use_nl('||LP_hint||' rlca, rc, im, im2) */';

   L_select := 'SELECT '||LP_hint||' '||L_select;

   OPEN il_lc FOR L_select||' '||L_from||' '||L_where;

   FETCH il_lc BULK COLLECT INTO O_lc_tbl;

   CLOSE il_lc;

   O_return_code := 1;

EXCEPTION

   WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_LINK_CODE_SQL.SEARCH',
                                        to_char(SQLCODE));
      O_return_code := 0;

END SEARCH;
-----------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION FILTER_ITEM(O_error_msg     IN OUT VARCHAR2,
                     I_pc_criteria   IN     OBJ_LINK_CODE_SEARCH_REC,
                     IO_item_from       OUT VARCHAR2,
                     IO_item_where      OUT VARCHAR2)
RETURN BOOLEAN IS
   ---

BEGIN

   --This function gets you to transaction level items (im.item) on item_master that meet the criteria.

   --SEARCH CRITERIA ITEM FILTERS
     --item_itemlist_ind               VARCHAR2(1),
     --item_level_ind                  VARCHAR2(1),
     --exclusive_item_level_ind        VARCHAR2(1),
     --dept_class_subclasses           obj_dept_class_subclass_tbl,
     --items                           obj_varchar_id_table,
     --diff_type                       VARCHAR2(6),
     --diff_ids                        obj_varchar_id_table,

   --
   IO_item_from  := NULL;
   IO_item_where := NULL;

   -- if item
   IF I_pc_criteria.item_itemlist_ind = ITEM_LEVEL_ITEM THEN

      -- tran level
      IF I_pc_criteria.item_level_ind = ITEM_TYPE_ITEM THEN

         IF I_pc_criteria.items IS NOT NULL THEN

          IO_item_from  := ' rpm_pc_search_char_tbl it, ';
            IO_item_where := ' rlca.item = it.VARCHAR_ID AND it.criteria_type = ''ITEMS'' ';
            LP_hint       := LP_hint || ' it, ';

         ELSIF I_pc_criteria.dept_class_subclasses IS NOT NULL THEN

          IO_item_from  := ' rpm_pc_search_merch_tbl dcs, subclass sub ,';
            IO_item_where := '      sub.dept = dcs.dept and sub.class = NVL(dcs.class, sub.class) and sub.subclass = NVL(dcs.subclass, sub.subclass) '||
                             '   and im.dept = sub.dept and im.class = sub.class and im.subclass = sub.subclass AND dcs.criteria_type = ''DEPT_CLASS_SUBCLASSES'' ';
            LP_hint       := LP_hint || ' dcs, sub, ';
         END IF;

      -- parent
      ELSIF I_pc_criteria.item_level_ind = ITEM_TYPE_PARENT THEN
         IF I_pc_criteria.items IS NOT NULL THEN
          IO_item_from  := ' rpm_pc_search_char_tbl it, ';
            IO_item_where := ' im.item_parent = it.VARCHAR_ID AND it.criteria_type = ''ITEMS'' ';
            LP_hint       := LP_hint || ' it, ';

         ELSIF I_pc_criteria.dept_class_subclasses IS NOT NULL THEN
          IO_item_from  := ' rpm_pc_search_merch_tbl dcs, subclass sub ,';
            IO_item_where := '      sub.dept = dcs.dept and sub.class = NVL(dcs.class, sub.class) and sub.subclass = NVL(dcs.subclass, sub.subclass) '||
                             '   and im.dept = sub.dept and im.class = sub.class and im.subclass = sub.subclass AND dcs.criteria_type = ''DEPT_CLASS_SUBCLASSES'' ';
            LP_hint       := LP_hint || ' dcs, sub, ';
         END IF;

      -- parent diff
      ELSIF I_pc_criteria.item_level_ind = ITEM_TYPE_PARENT_DIFF THEN

         IF I_pc_criteria.items IS NOT NULL AND
            I_pc_criteria.diff_ids IS NOT NULL THEN
          IO_item_from  := ' rpm_pc_search_char_tbl it, '||
                           ' (SELECT VARCHAR_ID FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') diff, ';
            IO_item_where := ' im.item_parent = it.VARCHAR_ID AND it.criteria_type = ''ITEMS'' '||
                ' AND (im.diff_1 = diff.VARCHAR_ID '||
                 '  OR im.diff_2 = diff.VARCHAR_ID '||
                 '  OR im.diff_3 = diff.VARCHAR_ID '||
                 '  OR im.diff_4 = diff.VARCHAR_ID '||
                 '    ) ';
            LP_hint       := LP_hint || ' it, diff, ';
         ELSIF I_pc_criteria.items IS NOT NULL THEN
          IO_item_from  := ' rpm_pc_search_char_tbl it, '||
                           ' (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_pc_criteria.diff_type||''' ) diff, ';
            IO_item_where := ' im.item_parent = it.VARCHAR_ID AND it.criteria_type = ''ITEMS'' '||
                ' AND (im.diff_1 = diff.diff_id '||
                 '  OR im.diff_2 = diff.diff_id '||
                 '  OR im.diff_3 = diff.diff_id '||
                 '  OR im.diff_4 = diff.diff_id '||
                 '    ) ';
            LP_hint       := LP_hint || ' it, diff, ';
         ELSIF I_pc_criteria.items IS NULL AND
            I_pc_criteria.diff_ids IS NOT NULL AND
            I_pc_criteria.dept_class_subclasses IS NOT NULL THEN
          IO_item_from  := ' rpm_pc_search_merch_tbl dcs, subclass sub ,'||
                           ' (SELECT VARCHAR_ID FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') diff, ';
            IO_item_where := '      sub.dept = dcs.dept and sub.class = NVL(dcs.class, sub.class) and sub.subclass = NVL(dcs.subclass, sub.subclass) '||
                             '   and im.dept = sub.dept and im.class = sub.class and im.subclass = sub.subclass AND dcs.criteria_type = ''DEPT_CLASS_SUBCLASSES'' ';
            IO_item_where := IO_item_where ||
                ' AND (im.diff_1 = diff.VARCHAR_ID '||
                 '  OR im.diff_2 = diff.VARCHAR_ID '||
                 '  OR im.diff_3 = diff.VARCHAR_ID '||
                 '  OR im.diff_4 = diff.VARCHAR_ID '||
                 '    ) ';
            LP_hint       := LP_hint || ' dcs, sub, diff, ';

         ELSIF I_pc_criteria.items IS NULL AND
            I_pc_criteria.diff_ids IS NOT NULL AND
            I_pc_criteria.dept_class_subclasses IS NULL THEN
            IO_item_from  := ' (SELECT VARCHAR_ID FROM rpm_pc_search_char_tbl WHERE criteria_type = ''DIFF_IDS'') diff, ';
            IO_item_where := IO_item_where ||
                 '    (im.diff_1 = diff.VARCHAR_ID '||
                 '  OR im.diff_2 = diff.VARCHAR_ID '||
                 '  OR im.diff_3 = diff.VARCHAR_ID '||
                 '  OR im.diff_4 = diff.VARCHAR_ID '||
                 '    ) ';
         ELSIF I_pc_criteria.dept_class_subclasses IS NOT NULL THEN
          IO_item_from  := ' rpm_pc_search_merch_tbl dcs, subclass sub ,'||
                           ' (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_pc_criteria.diff_type||''' ) diff, ';
            IO_item_where := '      sub.dept = dcs.dept and sub.class = NVL(dcs.class, sub.class) and sub.subclass = NVL(dcs.subclass, sub.subclass) '||
                             '   and im.dept = sub.dept and im.class = sub.class and im.subclass = sub.subclass AND dcs.criteria_type = ''DEPT_CLASS_SUBCLASSES'' ';
            IO_item_where := IO_item_where ||
                ' AND (im.diff_1 = diff.diff_id '||
                 '  OR im.diff_2 = diff.diff_id '||
                 '  OR im.diff_3 = diff.diff_id '||
                 '  OR im.diff_4 = diff.diff_id '||
                 '    ) ';
            LP_hint       := LP_hint || ' dcs, sub, diff, ';

         ELSE
            IO_item_from  := ' (SELECT diff_id FROM diff_ids WHERE diff_type = '''||I_pc_criteria.diff_type||''' ) diff, ';
            IO_item_where :=
                 '    (im.diff_1 = diff.diff_id '||
                 '  OR im.diff_2 = diff.diff_id '||
                 '  OR im.diff_3 = diff.diff_id '||
                 '  OR im.diff_4 = diff.diff_id '||
                 '    ) ';
         END IF;

      END IF;
   --else item list
   ELSIF I_pc_criteria.item_itemlist_ind = ITEM_LEVEL_ITEMLIST THEN
      IF I_pc_criteria.item_list_id IS NOT NULL THEN
         IO_item_from := IO_item_from || '   skulist_detail sd, ';

         IO_item_where := '    rlca.item  = sd.item ' ||
                          'AND sd.skulist = '||I_pc_criteria.item_list_id;
         LP_hint       := LP_hint || ' sd, ';
      END IF;
   END IF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_LINK_CODE_SQL.FILTER_ITEM',
                                        to_char(SQLCODE));
      return FALSE;

END FILTER_ITEM;
-----------------------------------------
FUNCTION FILTER_LOC(O_error_msg     IN OUT VARCHAR2,
                    I_pc_criteria   IN     OBJ_LINK_CODE_SEARCH_REC,
                    IO_loc_from        OUT VARCHAR2,
                    IO_loc_where       OUT VARCHAR2)
RETURN BOOLEAN IS

BEGIN


   ---LOCATION ATTRIBS
     --zone_group                     number(4),
     --zones                          obj_numeric_id_table,
     --locations                      obj_numeric_id_table,
     --exclusive_loc_level_ind        VARCHAR2(1),


   IO_loc_from := null;
   IO_loc_where := null;

   -- location
   IF I_pc_criteria.locations IS NOT NULL THEN
      IO_loc_from  := ' rpm_pc_search_num_tbl locs, ';
      IO_loc_where := ' rlca.location = locs.NUMERIC_ID and locs.criteria_type = ''LOCATIONS'' ';
      LP_hint       := LP_hint || ' locs, ';
   -- zone
   ELSIF I_pc_criteria.zones IS NOT NULL THEN
      IO_loc_from  := ' (select rzl.location '||
                      '    from rpm_zone_location rzl, '||
                      '         rpm_pc_search_num_tbl z '||
                      '   where rzl.zone_id = z.numeric_id '||
                      '     AND z.criteria_type = ''ZONES'') lz, ';

      IO_loc_where := '   rlca.location = lz.location  ';
   -- zone group
   ELSIF I_pc_criteria.zone_group IS NOT NULL THEN
      IO_loc_from  := ' (select rzl.location '||
                      '    from rpm_zone_location rzl, '||
                      '         rpm_zone rz '||
                      '   where rz.zone_group_id = '||I_pc_criteria.zone_group ||
                      '     AND rzl.zone_id = rz.zone_id ) lz,';
      IO_loc_where := '   rlca.location = lz.location  ';

   END IF;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_LINK_CODE_SQL.FILTER_LOC',
                                        to_char(SQLCODE));
      return FALSE;

END FILTER_LOC;
-----------------------------------------------------------------------------------------------------
END RPM_LINK_CODE_SQL;
/

