CREATE OR REPLACE PACKAGE BODY RPM_CUST_ATTR_VALIDATION_SQL AS

FUNCTION EXECUTE_VALIDATE(O_error_msg           OUT VARCHAR2,
                          IO_val_error_table IN OUT OBJ_VARCHAR_DESC_TABLE,
                          I_function_name    IN     RPM_CUST_ATTR_RULE_CONTROL.CUST_ATTR_FUNCTION_NAME%TYPE,
                          I_price_event_type IN     VARCHAR2,
                          I_cust_attr_tbl    IN     OBJ_CUST_ATTR_VALIDATION_TBL)
RETURN NUMBER;

--------------------------------------------------------

FUNCTION EXECUTE_VALIDATE(O_error_msg           OUT VARCHAR2,
                          IO_val_error_table IN OUT OBJ_VARCHAR_DESC_TABLE,
                          I_function_name    IN     RPM_CUST_ATTR_RULE_CONTROL.CUST_ATTR_FUNCTION_NAME%TYPE,
                          I_price_event_type IN     VARCHAR2,
                          I_cust_attr_tbl    IN     OBJ_CUST_ATTR_VALIDATION_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CUST_ATTR_VALIDATION_SQL.EXECUTE_VALIDATE';

   L_call_string VARCHAR2(255) := NULL;
   L_return      NUMBER        := 1;

BEGIN

   L_call_string := 'BEGIN :return := '||I_function_name||'(:error_msg, :error_tbl, :price_event_type, :cust_attr_tbl); END;';

   EXECUTE IMMEDIATE L_call_string
      USING OUT L_return,
            OUT O_error_msg,
            IN OUT IO_val_error_table,
            IN I_price_event_type,
            IN I_cust_attr_tbl;

   return L_return;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return 0;

END EXECUTE_VALIDATE;

--------------------------------------------------------

FUNCTION EXECUTE(O_error_msg           OUT VARCHAR2,
                 O_val_error_table     OUT OBJ_VARCHAR_DESC_TABLE,
                 I_price_event_type IN     VARCHAR2,
                 I_cust_attr_tbl    IN     OBJ_CUST_ATTR_VALIDATION_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CUST_ATTR_VALIDATION_SQL.EXECUTE';

   cursor C_RULES is
      select cust_attr_function_name
        from rpm_cust_attr_rule_control
       where active = 'Y'
       order by execution_order;

BEGIN

   O_val_error_table := NEW OBJ_VARCHAR_DESC_TABLE();

   for rec IN C_RULES loop

      if EXECUTE_VALIDATE(O_error_msg,
                          O_val_error_table,
                          rec.cust_attr_function_name,
                          I_price_event_type,
                          I_cust_attr_tbl) = 0 then
         return 0;
      end if;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return 0;

END EXECUTE;
--------------------------------------------------------
END RPM_CUST_ATTR_VALIDATION_SQL;
/

