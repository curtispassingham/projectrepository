CREATE OR REPLACE PACKAGE RPM_PURGE_CLEARANCE_SQL AS
--------------------------------------------------------------------------------
FUNCTION PURGE_UNUSED_ABANDONED_CLR(O_error_msg         OUT VARCHAR2,
                                    O_deleted_cnt       OUT NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PURGE_EXPIRED_CLEARANCE(O_error_msg         OUT VARCHAR2,
                                 O_deleted_cnt       OUT NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_PURGE_CLEARANCE_SQL;
/
