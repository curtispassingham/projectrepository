CREATE OR REPLACE PACKAGE BODY RPM_UOM_VALIDATION_SQL AS
--------------------------------------------------------
FUNCTION SAME_SELLING_UOM(O_cc_error_tbl               IN OUT CONFLICT_CHECK_ERROR_TBL,
                          O_unique_uom                    OUT RPM_FUTURE_RETAIL.SELLING_UOM%TYPE,
                          I_merch_nd_zone_nd_date_tbl  IN     OBJ_MERCH_ND_ZONE_ND_DATE_TBL,
                          I_price_event_type           IN     VARCHAR2,
                          I_price_event_id             IN     NUMBER)
RETURN NUMBER IS

   L_program  VARCHAR2(50) := 'RPM_UOM_VALIDATION_SQL.SAME_SELLING_UOM';

   L_uoms           OBJ_VARCHAR_ID_TABLE                  := OBJ_VARCHAR_ID_TABLE();
   L_is_unique_uom  RPM_SYSTEM_OPTIONS.IS_UNIQUE_UOM%TYPE := 0;
   L_uom_value      RPM_SYSTEM_OPTIONS.UOM_VALUE%TYPE     := NULL;

   cursor C_CHECK_UOM is
      select distinct t.selling_uom
        from (-- Merch Hier / Location level
              select distinct FIRST_VALUE (fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                              fr.location
                                                                     ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im
               where gtt.item        is NULL
                 and gtt.diff_id     is NULL
                 and gtt.zone_id     is NULL
                 and fr.dept         = gtt.dept
                 and fr.class        = NVL(gtt.class, im.class)
                 and fr.subclass     = NVL(gtt.subclass, im.subclass)
                 and fr.location     = gtt.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.tran_level   = im.item_level
                 and im.item         = fr.item
              union all
              -- Item or Parent Item / Location level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im
               where gtt.item        is NOT NULL
                 and gtt.zone_id     is NULL
                 and gtt.diff_id     is NULL
                 and im.item         = fr.item
                 and fr.location     = gtt.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and (   im.item        = gtt.item
                      or im.item_parent = gtt.item)
              union all
              --      Parent Diff/Location level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im
               where gtt.item        is NOT NULL
                 and gtt.zone_id     is NULL
                 and gtt.diff_id     is NOT NULL
                 and im.item_parent  = gtt.item
                 and im.item         = fr.item
                 and fr.location     = gtt.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and (   im.diff_1   = gtt.diff_id
                      or im.diff_2   = gtt.diff_id
                      or im.diff_3   = gtt.diff_id
                      or im.diff_4   = gtt.diff_id)
              union all
              --   Link Code/Location Level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_link_code_attribute rlca,
                     item_master im
               where gtt.item        is NOT NULL
                 and gtt.zone_id     is NULL
                 and gtt.item        = to_char(rlca.link_code)
                 and im.item         = rlca.item
                 and im.item         = fr.item
                 and fr.location     = rlca.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              --  Merch Hier/Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item        is NULL
                 and gtt.diff_id     is NULL
                 and gtt.zone_id     is NOT NULL
                 and fr.dept         = gtt.dept
                 and fr.class        = NVL(gtt.class, im.class)
                 and fr.subclass     = NVL(gtt.subclass, im.subclass)
                 and fr.location     = rzl.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.tran_level   = im.item_level
                 and im.item         = fr.item
                 and rzl.zone_id     = gtt.zone_id
                 and gtt.location    is NULL
              union all
              -- Item or Parent Item/Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item        is NOT NULL
                 and gtt.diff_id     is NULL
                 and gtt.zone_id     is NOT NULL
                 and fr.dept         = im.dept
                 and fr.class        = im.class
                 and fr.subclass     = im.subclass
                 and fr.item         = im.item
                 and fr.location     = rzl.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and (   im.item        = gtt.item
                      or im.item_parent = gtt.item)
                 and rzl.zone_id     = gtt.zone_id
                 and gtt.location    is NULL
              union all
              -- Parent Diff/ Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item        is NOT NULL
                 and gtt.diff_id     is NOT NULL
                 and gtt.zone_id     is NOT NULL
                 and fr.dept         = im.dept
                 and fr.class        = im.class
                 and fr.subclass     = im.subclass
                 and fr.item         = im.item
                 and fr.location     = rzl.location
                 and fr.action_date <= gtt.effective_date
                 and im.dept         = fr.dept
                 and im.class        = fr.class
                 and im.subclass     = fr.subclass
                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.item_parent  = gtt.item
                 and rzl.zone_id     = gtt.zone_id
                 and gtt.location    is NULL
                 and (   im.diff_1   = gtt.diff_id
                      or im.diff_2   = gtt.diff_id
                      or im.diff_3   = gtt.diff_id
                      or im.diff_4   = gtt.diff_id)
              union all
              -- Link Code / Zone level
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_link_code_attribute rlca,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.item         is NOT NULL
                 and gtt.zone_id      is NOT NULL
                 and gtt.location     is NULL
                 and gtt.item         = to_char(rlca.link_code)
                 and im.item          = rlca.item
                 and im.item          = fr.item
                 and rzl.zone_id      = gtt.zone_id
                 and fr.location      = rzl.location
                 and fr.action_date  <= gtt.effective_date
                 and im.dept          = fr.dept
                 and im.class         = fr.class
                 and im.subclass      = fr.subclass
                 and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS) t;

   cursor C_PC_SKULIST is
      select distinct t.selling_uom
        from (-- item level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item
                 and pcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item_parent
                 and pcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- item level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item
                 and pcs.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_price_change_skulist pcs,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pcs.skulist        = gtt.skulist
                 and pcs.price_event_id = I_price_event_id
                 and pcs.item           = im.item_parent
                 and pcs.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS) t;

   cursor C_PR_SKULIST is
      select distinct t.selling_uom
        from (-- item level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item
                 and pds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NULL
                 and gtt.location       is NOT NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item_parent
                 and pds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- item level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item
                 and pds.item_level     = RPM_CONSTANTS.IL_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
              union all
              -- parent level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_promo_dtl_skulist pds,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.skulist        is NOT NULL
                 and gtt.zone_id        is NOT NULL
                 and gtt.location       is NULL
                 and pds.skulist        = gtt.skulist
                 and pds.price_event_id = I_price_event_id
                 and pds.item           = im.item_parent
                 and pds.item_level     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                 and im.item            = fr.item
                 and rzl.zone_id        = gtt.zone_id
                 and fr.location        = rzl.location
                 and fr.action_date    <= gtt.effective_date
                 and im.dept            = fr.dept
                 and im.class           = fr.class
                 and im.subclass        = fr.subclass
                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS) t;

   cursor C_PEIL is
      select distinct t.selling_uom
        from (-- item level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.price_event_itemlist  is NOT NULL
                 and gtt.zone_id               is NULL
                 and gtt.location              is NOT NULL
                 and rmld.merch_list_id        = gtt.price_event_itemlist
                 and rmld.merch_level          = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item                 = im.item
                 and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level             = im.tran_level
                 and im.dept                   = fr.dept
                 and im.class                  = fr.class
                 and im.subclass               = fr.subclass
                 and im.item                   = fr.item
                 and fr.action_date           <= gtt.effective_date
              union all
              -- parent level - loc
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im
               where gtt.price_event_itemlist  is NOT NULL
                 and gtt.zone_id               is NULL
                 and gtt.location              is NOT NULL
                 and rmld.merch_list_id        = gtt.price_event_itemlist
                 and rmld.merch_level          = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item                 = im.item_parent
                 and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level             = im.tran_level
                 and im.dept                   = fr.dept
                 and im.class                  = fr.class
                 and im.subclass               = fr.subclass
                 and im.item                   = fr.item
                 and fr.action_date           <= gtt.effective_date
              union all
              -- item level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.price_event_itemlist  is NOT NULL
                 and gtt.zone_id               is NOT NULL
                 and gtt.location              is NULL
                 and rmld.merch_list_id        = gtt.price_event_itemlist
                 and rmld.merch_level          = RPM_CONSTANTS.ITEM_MERCH_TYPE
                 and rmld.item                 = im.item
                 and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level             = im.tran_level
                 and im.dept                   = fr.dept
                 and im.class                  = fr.class
                 and im.subclass               = fr.subclass
                 and im.item                   = fr.item
                 and rzl.zone_id               = gtt.zone_id
                 and fr.location               = rzl.location
                 and fr.action_date           <= gtt.effective_date
              union all
              -- parent level - zone
              select distinct FIRST_VALUE(fr.selling_uom) OVER (PARTITION BY fr.item,
                                                                             fr.location
                                                                    ORDER BY fr.action_date desc) selling_uom
                from table(cast(I_merch_nd_zone_nd_date_tbl as OBJ_MERCH_ND_ZONE_ND_DATE_TBL)) gtt,
                     rpm_fr_item_loc_expl_gtt fr,
                     rpm_merch_list_detail rmld,
                     item_master im,
                     rpm_zone_location rzl
               where gtt.price_event_itemlist  is NOT NULL
                 and gtt.zone_id               is NOT NULL
                 and gtt.location              is NULL
                 and rmld.merch_list_id        = gtt.price_event_itemlist
                 and rmld.merch_level          = RPM_CONSTANTS.PARENT_MERCH_TYPE
                 and rmld.item                 = im.item_parent
                 and im.status                 = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.sellable_ind           = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                 and im.item_level             = im.tran_level
                 and im.dept                   = fr.dept
                 and im.class                  = fr.class
                 and im.subclass               = fr.subclass
                 and im.item                   = fr.item
                 and rzl.zone_id               = gtt.zone_id
                 and fr.location               = rzl.location
                 and fr.action_date           <= gtt.effective_date) t;

BEGIN

   O_unique_uom := NULL;

   select is_unique_uom,
          uom_value into L_is_unique_uom,
                         L_uom_value
     from rpm_system_options;

   if L_is_unique_uom = 1 then
      O_unique_uom := L_uom_value;
      return 1;
   end if;

   if I_merch_nd_zone_nd_date_tbl(1).skulist is NOT NULL and
      I_merch_nd_zone_nd_date_tbl(1).price_event_itemlist is NULL then

      if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

         open C_PC_SKULIST;
         fetch C_PC_SKULIST BULK COLLECT into L_uoms;
         close C_PC_SKULIST;

      elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                   RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION) then

         open C_PR_SKULIST;
         fetch C_PR_SKULIST BULK COLLECT into L_uoms;
         close C_PR_SKULIST;

      end if;

   elsif I_merch_nd_zone_nd_date_tbl(1).skulist is NULL and
         I_merch_nd_zone_nd_date_tbl(1).price_event_itemlist is NOT NULL then

      open C_PEIL;
      fetch C_PEIL BULK COLLECT into L_uoms;
      close C_PEIL;

   else

      open C_CHECK_UOM;
      fetch C_CHECK_UOM BULK COLLECT into L_uoms;
      close C_CHECK_UOM;

   end if;

   if L_uoms.COUNT = 1 then
      O_unique_uom := L_uoms(1);
   end if;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;
END SAME_SELLING_UOM;
--------------------------------------------------------
END;
/