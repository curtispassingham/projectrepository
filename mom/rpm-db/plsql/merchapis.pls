CREATE OR REPLACE PACKAGE MERCH_API_SQL IS

--------------------------------------------------------------------------------

PROCEDURE GET_RPM_SYSTEM_OPTIONS (O_error_message           IN OUT  VARCHAR2,
                                  O_success                 IN OUT  VARCHAR2,
                                  O_rpm_system_options_rec  IN OUT  NOCOPY OBJ_RPM_SYSTEM_OPTIONS_REC);

--------------------------------------------------------------------------------

PROCEDURE GET_PROMOS(O_error_message    OUT  VARCHAR2,
                     O_success          OUT  VARCHAR2,
                     O_promo_table      OUT  NOCOPY OBJ_PROMO_TBL);
--------------------------------------------------------------------------------

PROCEDURE GET_VALID_PROMOS (O_error_message  IN OUT  VARCHAR2,
                            O_success        IN OUT  VARCHAR2,
                            O_promo_table    IN OUT  NOCOPY OBJ_PROMO_TBL);
--------------------------------------------------------------------------------

PROCEDURE GET_PROMO_COMPS(O_error_message       OUT  VARCHAR2,
                          O_success             OUT  VARCHAR2,
                          O_promo_comp_table    OUT  NOCOPY OBJ_PROMO_COMP_TBL);

--------------------------------------------------------------------------------

PROCEDURE VALIDATE_PROMO (O_error_message IN OUT VARCHAR2,
                          O_success       IN OUT VARCHAR2,
                          O_valid         IN OUT VARCHAR2,
                          O_promotion_rec IN OUT NOCOPY OBJ_PROMO_REC,
                          I_check_status  IN     VARCHAR2,
                          I_promotion_id  IN     RPM_PROMO.PROMO_ID%TYPE,
                          I_need_hist     IN     NUMBER DEFAULT 0);

--------------------------------------------------------------------------------

PROCEDURE VALIDATE_PROMO_COMP (O_error_message IN OUT VARCHAR2,
                               O_success       IN OUT VARCHAR2,
                               O_valid         IN OUT VARCHAR2,
                               O_component_rec IN OUT NOCOPY OBJ_PROMO_COMP_REC,
                               I_promotion_id  IN     RPM_PROMO.PROMO_ID%TYPE,
                               I_component_id  IN     RPM_PROMO_COMP.PROMO_COMP_ID%TYPE,
                               I_need_hist     IN     NUMBER DEFAULT 0);

--------------------------------------------------------------------------------

PROCEDURE DEAL_PROMO_EXISTS (O_error_message  IN OUT  VARCHAR2,
                             O_success        IN OUT  VARCHAR2,
                             O_exists         IN OUT  VARCHAR2,
                             I_deal_id        IN      DEAL_HEAD.DEAL_ID%TYPE);

--------------------------------------------------------------------------------

PROCEDURE GET_PROMOS (O_error_message  IN OUT  VARCHAR2,
                      O_success IN OUT  VARCHAR2);
-------------------------------------------------------------------------------

PROCEDURE GET_PROMO_COMPS (O_error_message     IN OUT  VARCHAR2,
                           O_success IN OUT  VARCHAR2);

---------------------------------------------------------------------------------

PRAGMA RESTRICT_REFERENCES(DEAL_PROMO_EXISTS, WNDS);

---------------------------------------------------------------------------------

FUNCTION CHECK_CUST_SEGMENT_ACTIVE (O_error_message          OUT VARCHAR2,
                                    O_cust_seg_active_ind    OUT NUMBER,
                                    I_cust_segment_type   IN     NUMBER)
RETURN NUMBER;

---------------------------------------------------------------------------------

FUNCTION CHECK_ITEMLIST_ACTIVE(O_error_message          OUT VARCHAR2,
                               O_itemlist_active_ind    OUT NUMBER,
                               I_itemlist_id         IN     NUMBER)
RETURN NUMBER;

---------------------------------------------------------------------------------

FUNCTION GET_ITEMLOC_RETAIL(O_error_message              OUT VARCHAR2,
                            O_selling_retail             OUT RPM_FUTURE_RETAIL.SELLING_RETAIL%TYPE,
                            O_selling_uom                OUT RPM_FUTURE_RETAIL.SELLING_UOM%TYPE,
                            O_selling_retail_currency    OUT RPM_FUTURE_RETAIL.SELLING_RETAIL_CURRENCY%TYPE,
                            I_item                    IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                            I_location                IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                            I_effective_date          IN     DATE DEFAULT NULL)
RETURN NUMBER;

---------------------------------------------------------------------------------

END MERCH_API_SQL;
/

