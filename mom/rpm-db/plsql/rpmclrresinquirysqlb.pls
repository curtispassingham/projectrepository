CREATE OR REPLACE PACKAGE BODY RPM_CLR_RES_INQUIRY_SQL AS
--------------------------------------------------------
-- Declaration OF LOCAL PACKAGE variables
--------------------------------------------------------
   LP_search_criteria OBJ_PRICE_INQ_SEARCH_REC := NULL;
   LP_zone_search     NUMBER(1)                := NULL;

--------------------------------------------------------
-- Procedure Prototypes
--------------------------------------------------------
PROCEDURE POP_GTT (O_return_code    OUT NUMBER,
                   O_error_msg      OUT VARCHAR2);
--------------------------------------------------------
PROCEDURE BUILD_CLR_RESET_INQ_VO (O_return_code     OUT NUMBER,
                                  O_error_msg       OUT VARCHAR2,
                                  O_clear_resets    OUT NOCOPY OBJ_CLR_RES_INQ_TBL);

--------------------------------------------------------
-- Procedure Definitions
--------------------------------------------------------
PROCEDURE POP_GTT (O_return_code    OUT NUMBER,
                   O_error_msg      OUT VARCHAR2) AS

   L_program VARCHAR2(35) := 'RPM_CLR_RES_INQUIRY_SQL.POP_GTT';

BEGIN

   -- Clean up the temporary tables
   delete
     from gtt_num_num_str_str_date_date;

   delete
     from numeric_id_gtt;

   -- Populate the Numeric_gtt_table
   if LP_search_criteria.locations is NOT NULL and
      LP_search_criteria.locations.COUNT > 0 then

      -- Populate with Locations
      LP_zone_search := RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL;

      insert into numeric_id_gtt(numeric_id)
         select /*+ CARDINALITY(L_locs 5) */
                distinct L_locs.column_value
           from table(cast(LP_search_criteria.locations as OBJ_NUMERIC_ID_TABLE)) L_locs;

   elsif LP_search_criteria.zone_ids is NOT NULL and
         LP_search_criteria.zone_ids.COUNT > 0 then

      -- Populate with Zone IDS
      LP_zone_search := RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL;

      insert into numeric_id_gtt(numeric_id)
         select /*+ CARDINALITY(L_zone_ids 5) */
                distinct L_zone_ids.column_value
           from table(cast(LP_search_criteria.zone_ids as OBJ_NUMERIC_ID_TABLE)) L_zone_ids;

   else

      -- Populate with Zone IDS of the given Zone Group
      LP_zone_search := RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL;

      insert into numeric_id_gtt(numeric_id)
         select rz.zone_id
           from rpm_zone rz
          where rz.zone_group_id = LP_search_criteria.zone_group;

   end if;

   -- Populate the temporary tables based on the input search criteria
   -- Populate Transaction Level Item data

   if LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM and
      LP_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM then

      if LP_search_criteria.items is NOT NULL and
         LP_search_criteria.items.COUNT > 0 then

         -- We need to have the right DEPT-ITEM combination in the record.
         -- In the GUI, the user might have entered more than one DEPT and just one ITEM
         -- but we only need the DEPT to which the ITEM belongs, i.e., if LP_search_criteria.ITEMS is not NULL

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item
              from table(cast(LP_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) L_tbl,
                   item_master itm,
                   numeric_id_gtt num
             where itm.item         = L_tbl.column_value
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and EXISTS (select 1
                             from rpm_item_loc il
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and il.item        = itm.item
                              and il.loc         = num.numeric_id
                           union all
                           select 1
                             from rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id    = num.numeric_id
                              and il.item        = itm.item
                              and il.loc         = rzl.location);

      elsif LP_search_criteria.items is NULL or
            LP_search_criteria.items.COUNT = 0 then

         -- If no ITEMS have been specified in the GUI, get all the ITEMS from the DEPT(s) specified.
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item
              from table(cast(LP_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) L_tbl,
                   item_master itm,
                   numeric_id_gtt num
             where itm.dept         = L_tbl.dept
               and itm.class        = NVL(L_tbl.class, itm.class)
               and itm.subclass     = NVL(L_tbl.subclass, itm.subclass)
               and itm.item_level   = itm.tran_level
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and EXISTS (select 1
                             from rpm_item_loc il
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and il.item        = itm.item
                              and il.loc         = num.numeric_id
                           union all
                           select 1
                             from rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id    = num.numeric_id
                              and il.item        = itm.item
                              and il.loc         = rzl.location);
      end if;
   end if;

   -- Populate Parent Level Item data
   if LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM and
      LP_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT then

      if LP_search_criteria.items is NOT NULL and
         LP_search_criteria.items.COUNT > 0 then

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item
              from table(cast(LP_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) L_tbl,
                   item_master itm,
                   numeric_id_gtt num
             where itm.item         = L_tbl.column_value
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and EXISTS (select 1
                             from rpm_item_loc il,
                                  item_master im
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and il.item         = im.item
                              and il.loc          = num.numeric_id
                           union all
                           select 1
                             from item_master im,
                                  rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id     = num.numeric_id
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and il.item         = im.item
                              and il.loc          = rzl.location);

      elsif LP_search_criteria.items is NULL or
            LP_search_criteria.items.COUNT = 0 then

         -- If no ITEMS have been specified in the GUI, get all the ITEMS from the DEPT(s) specified.
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item
              from table(cast(LP_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) L_tbl,
                   item_master itm,
                   numeric_id_gtt num
             where itm.dept         = L_tbl.dept
               and itm.class        = NVL(L_tbl.class, itm.class)
               and itm.subclass     = NVL(L_tbl.subclass, itm.subclass)
               and itm.tran_level   = itm.item_level + 1
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and EXISTS (select 1
                             from item_master im,
                                  rpm_item_loc il
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and il.item         = itm.item
                              and il.loc          = num.numeric_id
                           union all
                           select 1
                             from item_master im,
                                  rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id     = num.numeric_id
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and il.item         = im.item
                              and il.loc          = rzl.location);
      end if;
   end if;

   -- Populate Parent Diff Item data
   if LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM and
      LP_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF then

      if LP_search_criteria.items is NOT NULL and
         LP_search_criteria.items.COUNT > 0 and
         LP_search_criteria.diff_ids is NOT NULL and
         LP_search_criteria.diff_ids.COUNT > 0 then

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1,
                                                   varchar2_2)
            select /*+ CARDINALITY (L_tbl 5) CARDINALITY (L_diffs 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item,
                   L_diffs.column_value
              from table(cast(LP_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) L_tbl,
                   item_master itm,
                   table(cast(LP_search_criteria.diff_ids as OBJ_VARCHAR_ID_TABLE)) L_diffs,
                   numeric_id_gtt num
             where itm.item         = L_tbl.column_value
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and EXISTS (select 1
                             from item_master im,
                                  rpm_item_loc il
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = L_diffs.column_value
                                   or im.diff_2 = L_diffs.column_value
                                   or im.diff_3 = L_diffs.column_value
                                   or im.diff_4 = L_diffs.column_value)
                              and il.item         = itm.item
                              and il.loc          = num.numeric_id
                           union all
                           select 1
                             from item_master im,
                                  rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id     = num.numeric_id
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (    im.diff_1 = L_diffs.column_value
                                    or im.diff_2 = L_diffs.column_value
                                    or im.diff_3 = L_diffs.column_value
                                    or im.diff_4 = L_diffs.column_value)
                              and il.item         = im.item
                              and il.loc          = rzl.location);

      elsif LP_search_criteria.items is NOT NULL and
            LP_search_criteria.items.COUNT > 0 and
            (LP_search_criteria.diff_ids is NULL or
             LP_search_criteria.diff_ids.COUNT = 0) then

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1,
                                                   varchar2_2)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item,
                   df_ids.diff_id
              from table(cast(LP_search_criteria.items as OBJ_VARCHAR_ID_TABLE )) L_tbl,
                   item_master itm,
                   diff_ids df_ids,
                   numeric_id_gtt num
             where itm.item         = L_tbl.column_value
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and df_ids.diff_type = LP_search_criteria.diff_type
               and EXISTS (select 1
                             from item_master im,
                                  rpm_item_loc il
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = df_ids.diff_id
                                   or im.diff_2 = df_ids.diff_id
                                   or im.diff_3 = df_ids.diff_id
                                   or im.diff_4 = df_ids.diff_id)
                              and il.item         = itm.item
                              and il.loc          = num.numeric_id
                           union all
                           select 1
                             from item_master im,
                                  rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id     = num.numeric_id
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = df_ids.diff_id
                                   or im.diff_2 = df_ids.diff_id
                                   or im.diff_3 = df_ids.diff_id
                                   or im.diff_4 = df_ids.diff_id)
                              and il.item         = im.item
                              and il.loc          = rzl.location);

      elsif (LP_search_criteria.items is NULL or
             LP_search_criteria.items.COUNT = 0) and
            LP_search_criteria.diff_ids is NOT NULL and
            LP_search_criteria.diff_ids.COUNT > 0 then

         -- If no ITEMS have been specified in the GUI but DIFFS have been specified.
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1,
                                                   varchar2_2)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item,
                   L_diffs.column_value
              from table(cast(LP_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL )) L_tbl,
                   item_master itm,
                   table(cast(LP_search_criteria.diff_ids as OBJ_VARCHAR_ID_TABLE )) L_diffs,
                   numeric_id_gtt num
             where itm.dept         = L_tbl.dept
               and itm.class        = NVL(L_tbl.class, itm.class)
               and itm.subclass     = NVL(L_tbl.subclass, itm.subclass)
               and itm.tran_level   = itm.item_level + 1
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and EXISTS (select 1
                             from item_master im,
                                  rpm_item_loc il
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = L_diffs.column_value
                                   or im.diff_2 = L_diffs.column_value
                                   or im.diff_3 = L_diffs.column_value
                                   or im.diff_4 = L_diffs.column_value)
                               and il.item       = itm.item
                               and il.loc        = num.numeric_id
                           union all
                           select 1
                             from item_master im,
                                  rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id     = num.numeric_id
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = L_diffs.column_value
                                   or im.diff_2 = L_diffs.column_value
                                   or im.diff_3 = L_diffs.column_value
                                   or im.diff_4 = L_diffs.column_value)
                              and il.item         = im.item
                              and il.loc          = rzl.location);

      elsif (LP_search_criteria.items is NULL or
             LP_search_criteria.items.COUNT = 0) and
            (LP_search_criteria.diff_ids IS NULL or
             LP_search_criteria.diff_ids.COUNT = 0) then

         -- If no (ITEMS and DIFFS) have been specified in the GUI

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1,
                                                   varchar2_2)
            select /*+ CARDINALITY (L_tbl 5) */
                   distinct itm.dept,
                   num.numeric_id,
                   itm.item,
                   df_ids.diff_id
              from table(cast(LP_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) L_tbl,
                   item_master itm,
                   diff_ids df_ids,
                   numeric_id_gtt num
             where itm.dept         = L_tbl.dept
               and itm.class        = NVL(L_tbl.class, itm.class)
               and itm.subclass     = NVL(L_tbl.subclass, itm.subclass)
               and itm.tran_level   = itm.item_level + 1
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and df_ids.diff_type = LP_search_criteria.diff_type
               and EXISTS (select 1
                             from item_master im,
                                  rpm_item_loc il
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = df_ids.diff_id
                                   or im.diff_2 = df_ids.diff_id
                                   or im.diff_3 = df_ids.diff_id
                                   or im.diff_4 = df_ids.diff_id)
                              and il.item         = itm.item
                              and il.loc          = num.numeric_id
                           union all
                           select 1
                             from item_master im,
                                  rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search  = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id     = num.numeric_id
                              and im.item_parent  = itm.item
                              and im.item_level   = im.tran_level
                              and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and (   im.diff_1 = df_ids.diff_id
                                   or im.diff_2 = df_ids.diff_id
                                   or im.diff_3 = df_ids.diff_id
                                   or im.diff_4 = df_ids.diff_id)
                              and il.item         = im.item
                              and il.loc          = rzl.location);

      end if;
   end if;

   -- Populate Item List data
   if LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST then

      if LP_search_criteria.item_list_id is NOT NULL then

         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1)
            select distinct itm.dept,
                   num.numeric_id,
                   itm.item
              from item_master itm,
                   skulist_detail itm_list,
                   numeric_id_gtt num
             where itm_list.skulist = LP_search_criteria.item_list_id
               and itm_list.item    = itm.item
               and itm.tran_level   = itm.item_level
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and EXISTS (select 1
                             from rpm_item_loc il
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and il.item        = itm.item
                              and il.loc         = num.numeric_id
                           union all
                           select 1
                             from rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id    = num.numeric_id
                              and il.item        = itm.item
                              and il.loc         = rzl.location);

      elsif LP_search_criteria.item_list_id is NULL then

         -- If no ITEM_LIST_ID has been specified in the GUI, get all the ITEMS from the DEPT(s) specified.
         insert into gtt_num_num_str_str_date_date(number_1,
                                                   number_2,
                                                   varchar2_1)
            select distinct itm.dept,
                   num.numeric_id,
                   itm.item
              from (select /*+ CARDINALITY (L_tbl 5) */
                           itm_list.item
                      from table(cast(LP_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL )) L_tbl,
                           skulist_dept itm_dept,
                           skulist_detail itm_list
                     where itm_dept.dept     = NVL(L_tbl.dept, itm_dept.dept)
                       and itm_dept.class    = NVL(L_tbl.class, itm_dept.class)
                       and itm_dept.subclass = NVL(L_tbl.subclass, itm_dept.subclass)
                       and itm_dept.skulist  = itm_list.skulist) sku_dets,
                   item_master itm,
                   numeric_id_gtt num
             where sku_dets.item    = itm.item
               and itm.tran_level   = itm.item_level
               and itm.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and itm.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and EXISTS (select 1
                             from rpm_item_loc il
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL
                              and il.item        = itm.item
                              and il.loc         = num.numeric_id
                           union all
                           select 1
                             from rpm_item_loc il,
                                  rpm_zone_location rzl
                            where LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.ZONE_LEVEL
                              and rzl.zone_id    = num.numeric_id
                              and il.item        = itm.item
                              and il.loc         = rzl.location);

      end if;
   end if;

   O_return_code := 1;

EXCEPTION

   WHEN OTHERS THEN

      O_return_code := 0;
      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
END POP_GTT;

--------------------------------------------------------

PROCEDURE BUILD_CLR_RESET_INQ_VO (O_return_code    OUT NUMBER,
                                  O_error_msg      OUT VARCHAR2,
                                  O_clear_resets   OUT NOCOPY OBJ_CLR_RES_INQ_TBL) AS

   L_program          VARCHAR2(50) := 'RPM_CLR_RES_INQUIRY_SQL.BUILD_CLR_RESET_INQ_VO';
   L_ref_class        VARCHAR2(65) := RPM_CONSTANTS.CR_REF_CLASS;
   L_vdate            DATE         := GET_VDATE;
   L_bulkfetch_limit  NUMBER       := 10000;

   cursor C_ITEM_LOC is
      select NEW OBJ_CLR_RES_INQ_REC(clearance_id,
                                     item,
                                     item_desc,
                                     location,
                                     location_name,
                                     NULL,  --diff_id
                                     reset_date,
                                     state,
                                     conflicts)
       from (select rc.clearance_id,
                    im.item,
                    im.item_desc,
                    locs.location,
                    locs.location_name,
                    rc.effective_date reset_date,
                    rc.state,
                    case
                       when EXISTS (select 'x'
                                      from rpm_con_check_err err
                                     where err.ref_id     = rc.clearance_id
                                       and err.ref_class  = L_ref_class
                                       and err.item       = rc.item
                                       and err.location   = rc.location) then
                          1
                       else
                          0
                    end conflicts
               from rpm_clearance_reset rc,
                    gtt_num_num_str_str_date_date t_gtt,
                    item_master im,
                    (select store location,
                            store_name location_name,
                            currency_code
                            from store
                     union all
                     select wh location,
                            wh_name location_name,
                            currency_code
                            from wh) locs
               where im.item       = t_gtt.varchar2_1
                 and im.item       = rc.item
                 and (   rc.effective_date >= L_vdate
                      or rc.effective_date IS NULL)
                  and locs.location = t_gtt.number_2
                  and locs.location = rc.location);

   cursor C_PARENT_LOC is
      select NEW OBJ_CLR_RES_INQ_REC(clearance_id,
                                     item,
                                     item_desc,
                                     location,
                                     location_name,
                                     NULL, --diff_id
                                     reset_date,
                                     state,
                                     conflicts)
       from (select rc.clearance_id,
                    rc.item,
                    im1.item_desc,
                    locs.location,
                    locs.location_name,
                    rc.effective_date reset_date,
                    rc.state,
                    case
                       when EXISTS (select 'x'
                                      from rpm_con_check_err err
                                     where err.ref_id     = rc.clearance_id
                                       and err.ref_class  = L_ref_class
                                       and err.item       = rc.item
                                       and err.location   = rc.location) then
                          1
                       else
                          0
                    end conflicts
               from gtt_num_num_str_str_date_date t_gtt,
                    item_master im1,
                    rpm_clearance_reset rc,
                    (select store location,
                            store_name location_name,
                            currency_code
                       from store
                     union all
                     select wh location,
                            wh_name location_name,
                            currency_code
                       from wh) locs
               where im1.item_parent = t_gtt.varchar2_1
                 and rc.item         = im1.item
                 and (   rc.effective_date >= L_vdate
                      or rc.effective_date IS NULL)
                 and locs.location   = t_gtt.number_2
                 and rc.location     = locs.location);

   cursor C_PARENT_DIFF_LOC is
      select NEW OBJ_CLR_RES_INQ_REC(clearance_id,
                                     item,
                                     item_desc,
                                     location,
                                     location_name,
                                     diff_id,
                                     reset_date,
                                     state,
                                     conflicts)
       from (select rc.clearance_id,
                    rc.item,
                    im1.item_desc,
                    locs.location,
                    locs.location_name,
                    t_gtt.varchar2_2 diff_id,
                    rc.effective_date reset_date,
                    rc.state,
                    case
                       when EXISTS (select 'x'
                                      from rpm_con_check_err err
                                     where err.ref_id     = rc.clearance_id
                                       and err.ref_class  = L_ref_class
                                       and err.item       = rc.item
                                       and err.location   = rc.location) then
                          1
                       else
                          0
                    end conflicts
               from gtt_num_num_str_str_date_date t_gtt,
                    item_master im1,
                    rpm_clearance_reset rc,
                    (select store location,
                            store_name location_name,
                            currency_code
                       from store
                     union all
                     select wh location,
                            wh_name location_name,
                            currency_code
                       from wh) locs
              where im1.item_parent = t_gtt.varchar2_1
                and (   im1.diff_1 = t_gtt.varchar2_2
                     or im1.diff_2 = t_gtt.varchar2_2
                     or im1.diff_3 = t_gtt.varchar2_2
                     or im1.diff_4 = t_gtt.varchar2_2)
                and rc.item = im1.item
                and (   rc.effective_date >= L_vdate
                     or rc.effective_date IS NULL)
                and locs.location   = t_gtt.number_2
                and rc.location     = locs.location);

   cursor C_ITEM_ZONE is
      select NEW OBJ_CLR_RES_INQ_REC(clearance_id,
                                     item,
                                     item_desc,
                                     location,
                                     location_name,
                                     NULL,
                                     reset_date,
                                     state,
                                     conflicts)
       from (select rc.clearance_id,
                    rc.item,
                    im.item_desc,
                    locs.location,
                    locs.location_name,
                    rc.effective_date reset_date,
                    rc.state,
                    case
                       when EXISTS (select 'x'
                                      from rpm_con_check_err err
                                     where err.ref_id     = rc.clearance_id
                                       and err.ref_class  = L_ref_class
                                       and err.item       = rc.item
                                       and err.location   = rc.location) then
                          1
                       else
                          0
                    end conflicts
               from gtt_num_num_str_str_date_date id_gtt,
                    rpm_zone rz,
                    rpm_zone_location rzl,
                    rpm_clearance_reset rc,
                    item_master im,
                    (select store location,
                            store_name location_name,
                            currency_code
                       from store
                     union all
                     select wh location,
                            wh_name location_name,
                            currency_code
                       from wh) locs
              where rz.zone_id   = id_gtt.number_2
                and rzl.zone_id  = rz.zone_id
                and rzl.location = locs.location
                and rc.item     = id_gtt.varchar2_1
                and rc.item = im.item
                and (   rc.effective_date >= L_vdate
                     or rc.effective_date IS NULL)
                and rc.location = rzl.location);

   cursor C_PARENT_ZONE is
      select NEW OBJ_CLR_RES_INQ_REC(clearance_id,
                                     item,
                                     item_desc,
                                     location,
                                     location_name,
                                     NULL,
                                     reset_date,
                                     state,
                                     conflicts)
       from (select rc.clearance_id,
                    rc.item,
                    im1.item_desc,
                    locs.location,
                    locs.location_name,
                    rc.effective_date reset_date,
                    rc.state,
                    case
                       when EXISTS (select 'x'
                                      from rpm_con_check_err err
                                     where err.ref_id     = rc.clearance_id
                                       and err.ref_class  = L_ref_class
                                       and err.item       = rc.item
                                       and err.location   = rc.location) then
                          1
                       else
                          0
                    end conflicts
               from gtt_num_num_str_str_date_date t_gtt,
                    item_master im1,
                    rpm_zone rz,
                    rpm_zone_location rzl,
                    rpm_clearance_reset rc,
                    (select store location,
                            store_name location_name,
                            currency_code
                       from store
                     union all
                     select wh location,
                            wh_name location_name,
                            currency_code
                       from wh) locs
              where rz.zone_id      = t_gtt.number_2
                and rzl.zone_id     = rz.zone_id
                and rzl.location    = locs.location
                and im1.item_parent = t_gtt.varchar2_1
                and im1.item        = rc.item
                and (   rc.effective_date >= L_vdate
                     or rc.effective_date IS NULL)
                and rc.location     = rzl.location);

   cursor C_PARENT_DIFF_ZONE is
      select NEW OBJ_CLR_RES_INQ_REC(clearance_id,
                                     item,
                                     item_desc,
                                     location,
                                     location_name,
                                     diff_id,
                                     reset_date,
                                     state,
                                     conflicts)
       from (select rc.clearance_id,
                    rc.item,
                    im1.item_desc,
                    locs.location,
                    locs.location_name,
                    t_gtt.varchar2_2 diff_id,
                    rc.effective_date reset_date,
                    rc.state,
                    case
                       when EXISTS (select 'x'
                                      from rpm_con_check_err err
                                     where err.ref_id     = rc.clearance_id
                                       and err.ref_class  = L_ref_class
                                       and err.item       = rc.item
                                       and err.location   = rc.location) then
                          1
                       else
                          0
                    end conflicts
               from gtt_num_num_str_str_date_date t_gtt,
                    item_master im1,
                    rpm_zone rz,
                    rpm_zone_location rzl,
                    rpm_clearance_reset rc,
                    (select store location,
                            store_name location_name,
                            currency_code
                       from store
                     union all
                     select wh location,
                            wh_name location_name,
                            currency_code
                       from wh) locs
              where rz.zone_id      = t_gtt.number_2
                and rzl.zone_id     = rz.zone_id
                and rzl.location    = locs.location
                and im1.item_parent = t_gtt.varchar2_1
                and im1.item        = rc.item
                and (   im1.diff_1 = t_gtt.varchar2_2
                     or im1.diff_2 = t_gtt.varchar2_2
                     or im1.diff_3 = t_gtt.varchar2_2
                     or im1.diff_4 = t_gtt.varchar2_2)
                and (   rc.effective_date >= L_vdate
                     or rc.effective_date IS NULL)
                and rc.location     = rzl.location);

BEGIN

   -- Fetch the appropriate cursor into the OUT variable. Limit it to return only
   -- those many as given in LP_search_criteria.MAXIMUM_RESULT

   -- Location Level
   if (LP_zone_search = RPM_CLR_RES_INQUIRY_SQL.LOCATION_LEVEL) then
      -- ITEM level is item or Item List. They both have the same selection criteria
      -- if LEVEL is ITEMLIST, then there will be no ITEM_TYPE, hence the or condition.

      if (LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM) or
         (LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST) then
         ---
         if (LP_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM) or
            (LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST) then
            ---

            open C_ITEM_LOC;
            fetch C_ITEM_LOC BULK COLLECT into O_clear_resets
               LIMIT NVL(LP_search_criteria.MAXIMUM_RESULT, L_bulkfetch_limit);
            close C_ITEM_LOC;

         elsif (LP_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT) then
            ---
            open C_PARENT_LOC;
            fetch C_PARENT_LOC BULK COLLECT into O_clear_resets
               LIMIT NVL(LP_search_criteria.MAXIMUM_RESULT, L_bulkfetch_limit);
            close C_PARENT_LOC;

         elsif (LP_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF) then
            ---
            open C_PARENT_DIFF_LOC;
            fetch C_PARENT_DIFF_LOC BULK COLLECT into O_clear_resets
               LIMIT NVL(LP_search_criteria.MAXIMUM_RESULT, L_bulkfetch_limit);
            close C_PARENT_DIFF_LOC;

         end if;
      end if;
   -- Zone Level
   else
      -- ITEM level is Item or Item list. Aggregate to Item / Zone level.
      -- if LEVEL is ITEMLIST, then there will be no ITEM_TYPE, hence the or condition.

      if (LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM) or
         (LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST) then
         ---
         if (LP_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM) or
            (LP_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST) then
            ---
            open C_ITEM_ZONE;
            fetch C_ITEM_ZONE BULK COLLECT into O_clear_resets
               LIMIT NVL(LP_search_criteria.MAXIMUM_RESULT, L_bulkfetch_limit);
            close C_ITEM_ZONE;

         elsif (LP_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT) then
            ---
            open C_PARENT_ZONE;
            fetch C_PARENT_ZONE BULK COLLECT into O_clear_resets
               LIMIT NVL(LP_search_criteria.MAXIMUM_RESULT, L_bulkfetch_limit);
            close C_PARENT_ZONE;

         elsif (LP_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF) then
            ---
            open C_PARENT_DIFF_ZONE;
            fetch C_PARENT_DIFF_ZONE BULK COLLECT into O_clear_resets
               LIMIT NVL(LP_search_criteria.MAXIMUM_RESULT, L_bulkfetch_limit);
            close C_PARENT_DIFF_ZONE;

         end if;
      end if;
   end if;

   O_return_code := 1;

EXCEPTION

   WHEN OTHERS then
      O_return_code := 0;

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

END BUILD_CLR_RESET_INQ_VO;
--------------------------------------------------------------------
FUNCTION GET_CLR_RESET_INQ_VO (O_error_msg          OUT VARCHAR2,
                               O_clear_resets       OUT OBJ_CLR_RES_INQ_TBL,
                               I_search_criteria IN     OBJ_PRICE_INQ_SEARCH_TBL)
RETURN NUMBER AS
   L_program                VARCHAR2(50)              := 'RPM_CLR_RES_INQUIRY_SQL.GET_CLR_RESET_INQ_VO';
   L_return_code            NUMBER;

BEGIN

   LP_search_criteria := I_search_criteria(1);

   POP_GTT (L_return_code,
            O_error_msg);

   if (L_return_code = 0) then
      return 0;
   elsif (L_return_code = 1) then

      BUILD_CLR_RESET_INQ_VO (L_return_code,
                              O_error_msg,
                              O_clear_resets);

      if (L_return_code = 0) then
         return 0;
      end if;

   end if;

   RETURN 1;

EXCEPTION
   WHEN OTHERS then
      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      RETURN 0;
END GET_CLR_RESET_INQ_VO;
--------------------------------------------------------------------
FUNCTION GET_CLEARANCE_RESET (O_error_msg        OUT VARCHAR2,
                              O_clear_resets     OUT OBJ_CLR_RES_INQ_TBL,
                              I_item_loc_recs IN     OBJ_CREATE_PC_TBL,
                              I_user_id       IN     VARCHAR2)
RETURN NUMBER IS

   L_program                VARCHAR2(50)              := 'RPM_CLR_RES_INQUIRY_SQL.GET_CLEARANCE_RESET';

   L_item_loc_rec           OBJ_CREATE_PC_REC         := NULL;
   L_merch_nodes            OBJ_MERCH_NODE_TBL        := NULL;
   L_zone_nodes             OBJ_NUM_NUM_STR_TBL       := NULL;
   L_items                  OBJ_VARCHAR_ID_TABLE      := NULL;
   L_locations              OBJ_NUMERIC_ID_TABLE      := NULL;
   L_search_criteria_rec    OBJ_PRICE_INQ_SEARCH_REC  := NULL;
   L_search_criteria_tbl    OBJ_PRICE_INQ_SEARCH_TBL  := NULL;

   cursor C_ITEMS is
   -- transactional item
      select im.item
        from table(cast(L_merch_nodes as OBJ_MERCH_NODE_TBL)) merch,
             item_master im
       where im.item          = merch.item
         and im.tran_level    = im.item_level
         and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
   -- parent item
      select im.item
        from table(cast(L_merch_nodes as OBJ_MERCH_NODE_TBL)) merch,
             item_master im
       where im.item_parent   = merch.item
         and im.item_level    = im.tran_level
         and merch.diff_id    is NULL
         and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
   -- parent diff
      select im.item
        from table(cast(L_merch_nodes as OBJ_MERCH_NODE_TBL)) merch,
             item_master im
       where im.item_parent   = merch.item
         and im.item_level    = im.tran_level
         and merch.diff_id    is NOT NULL
         and im.tran_level    = im.item_level
         and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         and (   im.diff_1    = merch.diff_id
              or im.diff_2    = merch.diff_id
              or im.diff_3    = merch.diff_id
              or im.diff_4    = merch.diff_id );

   cursor C_LOCATIONS is
      select loc.number_1 location
        from table(cast(L_zone_nodes as OBJ_NUM_NUM_STR_TBL)) loc
       where loc.number_2 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
      union all
      select location
        from table(cast(L_zone_nodes as OBJ_NUM_NUM_STR_TBL)) loc,
             rpm_zone_location zl
       where loc.number_2   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and zl.zone_id     = loc.number_1;

BEGIN

   L_item_loc_rec  := I_item_loc_recs(1);
   L_merch_nodes   := L_item_loc_rec.merch_nodes;
   L_zone_nodes    := L_item_loc_rec.zone_nodes;


   open C_ITEMS;
   fetch C_ITEMS BULK COLLECT into L_items;
   close C_ITEMS;

   open C_LOCATIONS;
   fetch C_LOCATIONS BULK COLLECT into L_locations;
   close C_LOCATIONS;

   L_search_criteria_rec := NEW OBJ_PRICE_INQ_SEARCH_REC(NULL,                                   -- dept_class_subclass
                                                         RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM,
                                                         RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM,
                                                         NULL,                                   --item_list_id
                                                         L_items,
                                                         NULL,                                   -- diff_type
                                                         NULL,                                   -- diff_ids
                                                         NULL,                                   -- zone_group
                                                         NULL,                                   -- zone_ids
                                                         L_locations,
                                                         NULL,                                   -- effective_date
                                                         NULL,                                   -- maximum result
                                                         NULL,                                   -- customer_type
                                                         I_user_id,                              -- user_id
                                                         NULL);                                  -- price_event_item_list_id

   L_search_criteria_tbl := NEW OBJ_PRICE_INQ_SEARCH_TBL(L_search_criteria_rec);

   if GET_CLR_RESET_INQ_VO(O_error_msg,
                           O_clear_resets,
                           L_search_criteria_tbl) = 0 then
      return 0;

   end if;

   return 1;

EXCEPTION
   WHEN OTHERS then
      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return 0;

END GET_CLEARANCE_RESET;
--------------------------------------------------------------------------------

end RPM_CLR_RES_INQUIRY_SQL;
/