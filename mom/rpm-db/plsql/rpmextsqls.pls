CREATE OR REPLACE PACKAGE RPM_EXT_SQL AS
----------------------------------------------------------------------------------------

-- pre process methods
HIGH_VOLUME   CONSTANT VARCHAR2(1) := 'H';
MEDIUM_VOLUME CONSTANT VARCHAR2(1) := 'M';
LOW_VOLUME    CONSTANT VARCHAR2(1) := 'L';

-- Strategy Types
CLEARANCE    CONSTANT VARCHAR2(2) := 'CL';
COMPETITIVE  CONSTANT VARCHAR2(2) := 'CO';
MAINT_MARGIN CONSTANT VARCHAR2(2) := 'MM';
MARGIN       CONSTANT VARCHAR2(2) := 'MA';

-- Smooth Avg Indicator
SMOOTHED_AVG_SALES_CALC_METHOD CONSTANT RPM_SYSTEM_OPTIONS.SALES_CALCULATION_METHOD%TYPE := 1;

-- Do Update Indicator
UPDATE_ITEM_ATTRIBUTES_NO  CONSTANT RPM_SYSTEM_OPTIONS.UPDATE_ITEM_ATTRIBUTES%TYPE := 0;
UPDATE_ITEM_ATTRIBUTES_YES CONSTANT RPM_SYSTEM_OPTIONS.UPDATE_ITEM_ATTRIBUTES%TYPE := 1;

-- Candidate Rule Types
EXCLUSION_TYPE CONSTANT RPM_CANDIDATE_RULE.TYPE%TYPE := 0;
INCLUSION_TYPE CONSTANT RPM_CANDIDATE_RULE.TYPE%TYPE := 1;

-- Candidate Rule Fields
PHASE_ID              CONSTANT RPM_CONDITION.FIELD%TYPE := 17;
SEASON_ID             CONSTANT RPM_CONDITION.FIELD%TYPE := 24;
UDA_DATE_VALUE        CONSTANT RPM_CONDITION.FIELD%TYPE := 22;
UDA_ID_DATE           CONSTANT RPM_CONDITION.FIELD%TYPE := 34;
UDA_ID_FREE_FORM_TEXT CONSTANT RPM_CONDITION.FIELD%TYPE := 35;
UDA_ID_LOV            CONSTANT RPM_CONDITION.FIELD%TYPE := 33;
UDA_VALUE             CONSTANT RPM_CONDITION.FIELD%TYPE := 21;

-- Candidate Rule Operators
EQUAL                 CONSTANT RPM_CONDITION.OPERATOR%TYPE := 0;
GREATER_THAN          CONSTANT RPM_CONDITION.OPERATOR%TYPE := 1;
GREATER_THAN_OR_EQUAL CONSTANT RPM_CONDITION.OPERATOR%TYPE := 2;
LESS_THAN             CONSTANT RPM_CONDITION.OPERATOR%TYPE := 3;
LESS_THAN_OR_EQUAL    CONSTANT RPM_CONDITION.OPERATOR%TYPE := 4;
NOT_EQUAL             CONSTANT RPM_CONDITION.OPERATOR%TYPE := 5;

-- Cost Calculation methods
AVG_COST CONSTANT RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE := 0;
MAX_COST CONSTANT RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE := 1;

-- Markup Calc Types
COST_MARKUP   CONSTANT DEPS.MARKUP_CALC_TYPE%TYPE := 0;
RETAIL_MARKUP CONSTANT DEPS.MARKUP_CALC_TYPE%TYPE := 1;

-- Sales Period Codes
WEEKLY_HIST     CONSTANT RPM_DEPT_AGGREGATION.HISTORICAL_SALES_LEVEL%TYPE := 0;
MONTHLY_HIST    CONSTANT RPM_DEPT_AGGREGATION.HISTORICAL_SALES_LEVEL%TYPE := 1;
HALFYEARLY_HIST CONSTANT RPM_DEPT_AGGREGATION.HISTORICAL_SALES_LEVEL%TYPE := 2;
YEARLY_HIST     CONSTANT RPM_DEPT_AGGREGATION.HISTORICAL_SALES_LEVEL%TYPE := 3;

-- Compete Types
COMP_MATCH       CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 0;
COMP_PRICE_ABOVE CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 1;
COMP_PRICE_BELOW CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 2;
COMP_PRICE_CODE  CONSTANT RPM_STRATEGY_COMPETITIVE.COMPETE_TYPE%TYPE := 3;

-- Worksheet Level / Worksheet Process Level
DEPT_LEVEL     CONSTANT RPM_DEPT_AGGREGATION.WORKSHEET_LEVEL%TYPE := 0;
CLASS_LEVEL    CONSTANT RPM_DEPT_AGGREGATION.WORKSHEET_LEVEL%TYPE := 1;
SUBCLASS_LEVEL CONSTANT RPM_DEPT_AGGREGATION.WORKSHEET_LEVEL%TYPE := 2;

--RPM_PRE_ME_AGGREGATION.AGGREGATION_TYPE VALUES
WORKSHEET_PROCESS_LEVEL CONSTANT RPM_PRE_ME_AGGREGATION.AGGREGATION_TYPE%TYPE := 0;
WORKSHEET_CREATE_LEVEL  CONSTANT RPM_PRE_ME_AGGREGATION.AGGREGATION_TYPE%TYPE := 1;

-- Link Code code type
LINK_CODE_TYPE CONSTANT RPM_CODES.CODE_TYPE%TYPE := 1;

-- State
PENDING_STATE CONSTANT RPM_WORKSHEET_ITEM_DATA.STATE%TYPE := 0;
NEW_STATE     CONSTANT RPM_WORKSHEET_ITEM_DATA.STATE%TYPE := 1;
UPDATE_STATE  CONSTANT RPM_WORKSHEET_ITEM_DATA.STATE%TYPE := 2;

-- Clearance/Promotion stages based on the following start_ind values:
-- 1 = clearance/promotion starting
-- 2 = clearance/promotion in progress
-- 3 = clearance/promotion ending
-- 4 = clearance/promotion starting and ending on the same day.
EVENT_START       CONSTANT RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE := 1;
EVENT_IN_PROGRESS CONSTANT RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE := 2;
EVENT_END         CONSTANT RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE := 3;
EVENT_START_END   CONSTANT RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE := 4;

-- Item/Location Reject Reason Codes
AREA_DIFF_EXCLUSION       CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'AREA_DIFF_EXCLUSION';
INVALID_MAINT_MARGIN_COST CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'INVALID_MAINT_MARGIN_COST';
INVALID_SECONDARY_ITEM    CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'INVALID_SECONDARY_ITEM';
MISSING_LINK_ITEM         CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'MISSING_LINK_ITEM';
NEW_ITEM_LOC              CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'NEW_ITEM_LOC';
NO_COST                   CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'NO_COST';
NO_RETAIL                 CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'NO_RETAIL';
CAND_RULE_EXCLUSION       CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'CAND_RULE_EXCLUSION';
VARIABLE_LINK_CODE        CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'VARIABLE_LINK_CODE';
VARIABLE_LINK_MBC         CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'VARIABLE_LINK_MBC';
VARIABLE_LINK_SELLING_UOM CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'VARIABLE_LINK_SELLING_UOM';
VARIABLE_LINK_VAT_IND     CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'VARIABLE_LINK_VAT_IND';
VARIABLE_ZONE_SELLING_UOM CONSTANT RPM_CODES.DESCRIPTION%TYPE := 'VARIABLE_ZONE_SELLING_UOM';

-- Zone Hierarchy Types
HIER_ZONE     CONSTANT RPM_AREA_DIFF.ZONE_HIER_TYPE%TYPE := 1;
HIER_LOCATION CONSTANT RPM_AREA_DIFF.ZONE_HIER_TYPE%TYPE := 2;

-- Worksheet Action Alags
UNDECIDED CONSTANT RPM_WORKSHEET_ITEM_DATA.ACTION_FLAG%TYPE := 0;
TAKE      CONSTANT RPM_WORKSHEET_ITEM_DATA.ACTION_FLAG%TYPE := 1;
DONT_TAKE CONSTANT RPM_WORKSHEET_ITEM_DATA.ACTION_FLAG%TYPE := 2;

-- price_change_amount_calc_type
CURRENT_NEW CONSTANT RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE := 0;
NEW_CURRENT CONSTANT RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE := 1;

----------------------------------------------------------------------------------------

FUNCTION GET_PRE_PROCESS_INFO(O_error_message              OUT VARCHAR2,
                              I_worksheet_process_agg   IN     OBJ_ME_AGGREGATION_TBL,
                              I_worksheet_create_aggs   IN     OBJ_ME_AGGREGATION_TBL,
                              O_pre_process_method         OUT VARCHAR2,
                              O_item_thread_id             OUT RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                              O_pre_process_num_threads    OUT RPM_PRE_PROCESS_CONFIG.PRE_PROCESS_NUM_THREADS%TYPE)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS(O_error_message         OUT VARCHAR2,
                     I_pre_process_method IN     VARCHAR2)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_THREAD(O_error_message     OUT VARCHAR2,
                            I_item_thread_id IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                            I_thread_number  IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION COMPLETE_PRE_PROCESS(O_error_message     OUT VARCHAR2,
                              I_item_thread_id IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION EXTRACT (O_error_msg            OUT VARCHAR2,
                  I_me_sequence_id    IN     NUMBER,
                  I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                  I_dept              IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                  I_class             IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                  I_subclass          IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                  I_zone_id           IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                  I_location          IN     RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE,
                  I_review_start_date IN     DATE,
                  I_eff_date          IN     DATE,
                  I_initial_call      IN     VARCHAR2  DEFAULT 'Y')
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION UPDATE_EFFECTIVE_DATES (O_error_msg                OUT VARCHAR2,
                                 I_obj_eff_dates_upd_tbl IN     OBJ_EFF_DATES_UPD_TBL)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION RESET_WORKSPACE (O_error_msg       OUT VARCHAR2,
                          I_workspace_id IN     RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION WORKSHEET_ROLLUP(O_error_msg              OUT VARCHAR2,
                          I_worksheet_status_id IN     RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                          O_state_bitmap           OUT RPM_WORKSHEET_STATUS.STATE_BITMAP%TYPE,
                          O_stock_on_hand          OUT RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE,
                          O_price_change_amount    OUT RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE,
                          O_sales_amount           OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE,
                          O_sales_amount_ex_vat    OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE,
                          O_sales_cost             OUT RPM_WORKSHEET_STATUS.SALES_COST%TYPE,
                          O_sales_change_amount    OUT RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE,
                          O_item_count             OUT RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION DELETE_EXPIRED_WORKSHEET (O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

----------------------------------------------------------------------------------------

FUNCTION DELETE_ABANDONED_WS_STATUS (O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

----------------------------------------------------------------------------------------
FUNCTION COPY_STRATEGY (O_error_msg    OUT VARCHAR2,
                        I_eff_date  IN     DATE)
RETURN NUMBER;

----------------------------------------------------------------------------------------
FUNCTION UPDATE_COMP_PRICE_HIST (O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

----------------------------------------------------------------------------------------
FUNCTION PURGE_ITEM_LOC_WORKSPACE (O_error_msg       OUT VARCHAR2,
                                   I_workspace_id IN     NUMBER)
RETURN NUMBER;
----------------------------------------------------------------------------------------
FUNCTION FINISH_WORKSHEET_STATUS (O_error_msg         OUT VARCHAR2,
                                  I_me_sequence_id IN     NUMBER)
RETURN NUMBER;
----------------------------------------------------------------------------------------
FUNCTION CURRENCY_CONVERT_VALUE (I_output_currency IN     CURRENCIES.CURRENCY_CODE%TYPE,
                                 I_input_value     IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                                 I_input_currency  IN     CURRENCIES.CURRENCY_CODE%TYPE)
RETURN NUMBER;
----------------------------------------------------------------------------------------
END RPM_EXT_SQL;
/
