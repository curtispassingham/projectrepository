CREATE OR REPLACE PACKAGE RPM_CC_PUBLISH AS
--------------------------------------------------------
FUNCTION STAGE_PC_MESSAGES(IO_error_table    IN OUT CONFLICT_CHECK_ERROR_TBL,
                           I_transaction_id  IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                           I_bulk_cc_pe_id   IN     NUMBER,
                           I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                           I_chunk_number    IN     NUMBER DEFAULT 1,
                           I_repoppay_ind    IN     NUMBER DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_CLR_MESSAGES(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                            I_transaction_id   IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                            I_bulk_cc_pe_id    IN     NUMBER,
                            I_price_event_type IN     VARCHAR2,
                            I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                            I_chunk_number     IN     NUMBER DEFAULT 1)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_PROM_MESSAGES(IO_error_table               IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_transaction_id             IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                             I_bulk_cc_pe_id              IN     NUMBER,
                             I_price_event_ids_to_process IN     OBJ_NUMERIC_ID_TABLE,
                             I_process_price_event_type   IN     VARCHAR2,
                             I_promo_type_to_process      IN     NUMBER,
                             I_msg_types_to_process       IN     VARCHAR2,
                             I_chunk_number               IN     NUMBER DEFAULT 1,
                             I_cancel_promo_dtl_ind       IN     NUMBER DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_PROM_CIL_MESSAGES(IO_error_table               IN OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_transaction_id             IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                 I_price_event_ids_to_process IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_FINANCE_PROM_MESSAGES(IO_error_table               IN OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_transaction_id             IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                     I_price_event_ids_to_process IN     OBJ_NUMERIC_ID_TABLE,
                                     I_price_event_type           IN     VARCHAR2 DEFAULT RPM_CONSTANTS.PE_TYPE_FINANCE_PROMOTION)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_PC_REMOVE_MESSAGES(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_transaction_id   IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                  I_bulk_cc_pe_id    IN     NUMBER,
                                  I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE,
                                  I_chunk_number     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_CLR_REMOVE_MESSAGES(IO_error_table   IN OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_transaction_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                   I_bulk_cc_pe_id  IN     NUMBER,
                                   I_clearance_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                   I_chunk_number   IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_PROM_REMOVE_MESSAGES(IO_error_table         IN OUT CONFLICT_CHECK_ERROR_TBL,
                                    I_transaction_id       IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                    I_bulk_cc_pe_id        IN     NUMBER,
                                    I_price_event_type     IN     VARCHAR2,
                                    I_promotion_detail_ids IN     OBJ_NUMERIC_ID_TABLE,
                                    I_chunk_number         IN     NUMBER DEFAULT 1)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_FIN_REMOVE_PROM_MESSAGES(IO_error_table         IN OUT CONFLICT_CHECK_ERROR_TBL,
                                        I_transaction_id       IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                        I_promotion_detail_ids IN     OBJ_NUMERIC_ID_TABLE,
                                        I_promotion_type       IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_LM_SCRUB_MESSAGES(IO_error_table   IN OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_transaction_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                 I_old_zone_id    IN     RPM_ZONE.ZONE_ID%TYPE,
                                 I_location       IN     RPM_ZONE_LOCATION.LOCATION%TYPE,
                                 I_loc_type       IN     RPM_ZONE_LOCATION.LOC_TYPE%TYPE,
                                 I_move_date      IN     DATE)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_RESET_POS_MESSAGES(O_error_msg         OUT VARCHAR2,
                                  I_transaction_id IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                  L_reset_pos_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_CLR_RST_REMOVE_MESSAGES(IO_error_table    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_transaction_id  IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                       I_chunk_number    IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION GET_PAYLOAD_ID(I_is_next         NUMBER,
                        I_seq_name IN     VARCHAR2 DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------
FUNCTION STAGE_UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                                       I_reset_clearance_id IN     OBJ_CLEARANCE_ITEM_LOC_TBL,
                                       I_bulk_cc_pe_id      IN     NUMBER,
                                       I_chunk_number       IN     NUMBER DEFAULT 1)
RETURN NUMBER;
--------------------------------------------------------
END;
/