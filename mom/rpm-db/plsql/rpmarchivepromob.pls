CREATE OR REPLACE PACKAGE BODY RPM_ARCHIVE_PROMOTIONS IS

FUNCTION ARCHIVE (I_promo_id IN NUMBER,
                  O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_ARCHIVE_PROMOTIONS.ARCHIVE';

   L_promo_ids OBJ_NUM_NUM_STR_TBL := NULL;
   L_comp_ids  OBJ_NUM_NUM_STR_TBL := NULL;
   L_dtl_ids   OBJ_NUM_NUM_STR_TBL := NULL;

   L_zone_loc_ids       OBJ_NUMERIC_ID_TABLE := NULL;
   L_dtl_list_grp_ids   OBJ_NUMERIC_ID_TABLE := NULL;
   L_dtl_list_ids       OBJ_NUMERIC_ID_TABLE := NULL;
   L_dtl_merch_node_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_dtl_disc_ldr_ids   OBJ_NUMERIC_ID_TABLE := NULL;

   L_vdate            DATE := GET_VDATE;
   L_default_end_date DATE := TO_DATE('3000', 'YYYY');

   cursor C_PROMO_IDS is
      select OBJ_NUM_NUM_STR_REC(promo_id,
                                 cust_attr_id,
                                 NULL)
        from rpm_promo
       where NVL(end_date, L_default_end_date) < L_vdate;

   cursor C_ALL_COMP_IDS is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_STR_REC(c.promo_comp_id,
                                 c.cust_attr_id,
                                 NULL)
        from table(cast(L_promo_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_comp c
       where c.promo_id = ids.number_1;

   cursor C_COMP_IDS is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_STR_REC(c.promo_comp_id,
                                 c.cust_attr_id,
                                 NULL)
        from rpm_promo_comp c
       where c.promo_id = I_promo_id;

   cursor C_DTL_IDS is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_STR_REC(d.promo_dtl_id,
                                 d.cust_attr_id,
                                 NULL)
        from table(cast(L_comp_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl d
       where d.promo_comp_id = ids.number_1;

   cursor C_ZONE_LOC_IDS is
      select /*+ CARDINALITY(ids 10) */
             zl.promo_zone_id
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_zone_location zl
       where zl.promo_dtl_id = ids.number_1;

   cursor C_DTL_LIST_GRP_IDS is
      select /*+ CARDINALITY(ids 10) */
             dlg.promo_dtl_list_grp_id
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl_list_grp dlg
       where dlg.promo_dtl_id = ids.number_1;

   cursor C_DTL_LIST_IDS is
      select /*+ CARDINALITY(ids 10) */
             dl.promo_dtl_list_id
        from table(cast(L_dtl_list_grp_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list dl
       where dl.promo_dtl_list_grp_id = value(ids);

   cursor C_DTL_MERCH_NODE_IDS is
      select /*+ CARDINALITY(ids 10) */
             dmn.promo_dtl_merch_node_id
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl_merch_node dmn
       where dmn.promo_dtl_id = ids.number_1;

   cursor C_DTL_DISC_LDR_IDS is
      select /*+ CARDINALITY(ids 10) */
             ddl.promo_dtl_disc_ladder_id
        from table(cast(L_dtl_list_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_disc_ladder ddl
       where ddl.promo_dtl_list_id = value(ids);

BEGIN

   -- Gather the ids for each table that needs to have data archived..
 if I_promo_id is null then
   open C_PROMO_IDS;
   fetch C_PROMO_IDS BULK COLLECT into L_promo_ids;
   close C_PROMO_IDS;

   open C_ALL_COMP_IDS;
   fetch C_ALL_COMP_IDS BULK COLLECT into L_comp_ids;
   close C_ALL_COMP_IDS;
 else 
   open C_COMP_IDS;
   fetch C_COMP_IDS BULK COLLECT into L_comp_ids;
   close C_COMP_IDS;
 end if;

   open C_DTL_IDS;
   fetch C_DTL_IDS BULK COLLECT into L_dtl_ids;
   close C_DTL_IDS;

   open C_ZONE_LOC_IDS;
   fetch C_ZONE_LOC_IDS BULK COLLECT into L_zone_loc_ids;
   close C_ZONE_LOC_IDS;

   open C_DTL_LIST_GRP_IDS;
   fetch C_DTL_LIST_GRP_IDS BULK COLLECT into L_dtl_list_grp_ids;
   close C_DTL_LIST_GRP_IDS;

   open C_DTL_LIST_IDS;
   fetch C_DTL_LIST_IDS BULK COLLECT into L_dtl_list_ids;
   close C_DTL_LIST_IDS;

   open C_DTL_MERCH_NODE_IDS;
   fetch C_DTL_MERCH_NODE_IDS BULK COLLECT into L_dtl_merch_node_ids;
   close C_DTL_MERCH_NODE_IDS;

   open C_DTL_DISC_LDR_IDS;
   fetch C_DTL_DISC_LDR_IDS BULK COLLECT into L_dtl_disc_ldr_ids;
   close C_DTL_DISC_LDR_IDS;

   -- Archive the data...
   
if I_promo_id is null then

   insert into rpm_promo_hist (promo_id,
                               promo_display_id,
                               name,
                               description,
                               currency_code,
                               promo_event_id,
                               start_date,
                               end_date,
                               secondary_ind,
                               comments,
                               lock_version,
                               cust_attr_id,
                               cust_attr_promo_comp_ind,
                               cust_attr_promo_dtl_ind)
      select /*+ CARDINALITY(ids 10) */
             promo_id,
             promo_display_id,
             name,
             description,
             currency_code,
             promo_event_id,
             start_date,
             end_date,
             secondary_ind,
             comments,
             lock_version,
             cust_attr_id,
             cust_attr_promo_comp_ind,
             cust_attr_promo_dtl_ind
        from table(cast(L_promo_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo t
       where t.promo_id = ids.number_1;

else

   insert into rpm_promo_hist (promo_id,
                               promo_display_id,
                               name,
                               description,
                               currency_code,
                               promo_event_id,
                               start_date,
                               end_date,
                               secondary_ind,
                               comments,
                               lock_version,
                               cust_attr_id,
                               cust_attr_promo_comp_ind,
                               cust_attr_promo_dtl_ind)
      select /*+ CARDINALITY(ids 10) */
             promo_id,
             promo_display_id,
             name,
             description,
             currency_code,
             promo_event_id,
             start_date,
             end_date,
             secondary_ind,
             comments,
             lock_version,
             cust_attr_id,
             cust_attr_promo_comp_ind,
             cust_attr_promo_dtl_ind
        from rpm_promo t
       where t.promo_id = I_promo_id;  
        
 end if;
          
   insert into rpm_promo_comp_hist (promo_comp_id,
                                    comp_display_id,
                                    promo_id,
                                    type,
                                    name,
                                    customer_type,
                                    funding_percent,
                                    consignment_rate,
                                    secondary_ind,
                                    buy_get_uptake_percent,
                                    deal_id_item,
                                    deal_detail_id_item,
                                    deal_id_location,
                                    deal_detail_id_location,
                                    lock_version,
                                    cust_attr_id,
                                    cust_attr_promo_dtl_ind)
      select /*+ CARDINALITY(ids 10) */
             promo_comp_id,
             comp_display_id,
             promo_id,
             type,
             name,
             customer_type,
             funding_percent,
             consignment_rate,
             secondary_ind,
             buy_get_uptake_percent,
             deal_id_item,
             deal_detail_id_item,
             deal_id_location,
             deal_detail_id_location,
             lock_version,
             cust_attr_id,
             cust_attr_promo_dtl_ind
        from table(cast(L_comp_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_comp t
       where t.promo_comp_id = ids.number_1;

   -- Insert parent level promo dtl records

   insert into rpm_promo_dtl_hist (promo_dtl_id,
                                   promo_comp_id,
                                   promo_dtl_display_id,
                                   ignore_constraints,
                                   apply_to_code,
                                   discount_limit,
                                   start_date,
                                   end_date,
                                   approval_date,
                                   create_date,
                                   create_id,
                                   approval_id,
                                   state,
                                   attribute_1,
                                   attribute_2,
                                   attribute_3,
                                   exception_parent_id,
                                   from_location_move,
                                   price_guide_id,
                                   threshold_id,
                                   timebased_dtl_ind,
                                   cancel_il_promo_dtl_ind,
                                   sys_generated_exclusion,
                                   cust_attr_id)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_id,
             promo_comp_id,
             promo_dtl_display_id,
             ignore_constraints,
             apply_to_code,
             discount_limit,
             start_date,
             end_date,
             approval_date,
             create_date,
             create_id,
             approval_id,
             state,
             attribute_1,
             attribute_2,
             attribute_3,
             exception_parent_id,
             from_location_move,
             price_guide_id,
             threshold_id,
             timebased_dtl_ind,
             cancel_il_promo_dtl_ind,
             sys_generated_exclusion,
             cust_attr_id
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl t
       where t.promo_dtl_id        = ids.number_1
         and t.exception_parent_id is NULL;

   -- Insert any children promo dtl records

   insert into rpm_promo_dtl_hist (promo_dtl_id,
                                   promo_comp_id,
                                   promo_dtl_display_id,
                                   ignore_constraints,
                                   apply_to_code,
                                   discount_limit,
                                   start_date,
                                   end_date,
                                   approval_date,
                                   create_date,
                                   create_id,
                                   approval_id,
                                   state,
                                   attribute_1,
                                   attribute_2,
                                   attribute_3,
                                   exception_parent_id,
                                   from_location_move,
                                   price_guide_id,
                                   threshold_id,
                                   timebased_dtl_ind,
                                   cancel_il_promo_dtl_ind,
                                   sys_generated_exclusion,
                                   cust_attr_id)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_id,
             promo_comp_id,
             promo_dtl_display_id,
             ignore_constraints,
             apply_to_code,
             discount_limit,
             start_date,
             end_date,
             approval_date,
             create_date,
             create_id,
             approval_id,
             state,
             attribute_1,
             attribute_2,
             attribute_3,
             exception_parent_id,
             from_location_move,
             price_guide_id,
             threshold_id,
             timebased_dtl_ind,
             cancel_il_promo_dtl_ind,
             sys_generated_exclusion,
             cust_attr_id
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl t
       where t.promo_dtl_id        = ids.number_1
         and t.exception_parent_id is NOT NULL
         and EXISTS (select 'x'
                       from rpm_promo_dtl rpd
                      where rpd.promo_dtl_id        =  t.exception_parent_id
                        and rpd.exception_parent_id is NULL);

   -- Insert any grandchildren promo dtl records

   insert into rpm_promo_dtl_hist (promo_dtl_id,
                                   promo_comp_id,
                                   promo_dtl_display_id,
                                   ignore_constraints,
                                   apply_to_code,
                                   discount_limit,
                                   start_date,
                                   end_date,
                                   approval_date,
                                   create_date,
                                   create_id,
                                   approval_id,
                                   state,
                                   attribute_1,
                                   attribute_2,
                                   attribute_3,
                                   exception_parent_id,
                                   from_location_move,
                                   price_guide_id,
                                   threshold_id,
                                   timebased_dtl_ind,
                                   cancel_il_promo_dtl_ind,
                                   sys_generated_exclusion,
                                   cust_attr_id)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_id,
             promo_comp_id,
             promo_dtl_display_id,
             ignore_constraints,
             apply_to_code,
             discount_limit,
             start_date,
             end_date,
             approval_date,
             create_date,
             create_id,
             approval_id,
             state,
             attribute_1,
             attribute_2,
             attribute_3,
             exception_parent_id,
             from_location_move,
             price_guide_id,
             threshold_id,
             timebased_dtl_ind,
             cancel_il_promo_dtl_ind,
             sys_generated_exclusion,
             cust_attr_id
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl t
       where t.promo_dtl_id        = ids.number_1
         and t.exception_parent_id is NOT NULL
         and EXISTS (select 'x'
                       from rpm_promo_dtl rpd
                      where rpd.promo_dtl_id        =  t.exception_parent_id
                        and rpd.exception_parent_id is NOT NULL);

   insert into rpm_promo_dtl_prc_range_hist (promo_dtl_prc_range_id,
                                             promo_dtl_id,
                                             buy_list_min,
                                             buy_list_max,
                                             get_list_min,
                                             get_list_max)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_prc_range_id,
             promo_dtl_id,
             buy_list_min,
             buy_list_max,
             get_list_min,
             get_list_max
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl_prc_range rpdpr
       where rpdpr.promo_dtl_id = ids.number_1;

   insert into rpm_promo_dtl_list_grp_hist (promo_dtl_list_grp_id,
                                            promo_dtl_id)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_list_grp_id,
             promo_dtl_id
        from table(cast(L_dtl_list_grp_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list_grp t
       where t.promo_dtl_list_grp_id = value(ids);

   insert into rpm_promo_dtl_list_hist (promo_dtl_list_id,
                                        promo_dtl_list_grp_id,
                                        description,
                                        reward_application)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_list_id,
             promo_dtl_list_grp_id,
             description,
             reward_application
        from table(cast(L_dtl_list_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_list t
       where t.promo_dtl_list_id = VALUE(ids);

   insert into rpm_promo_zone_location_hist (promo_zone_id,
                                             promo_dtl_id,
                                             zone_node_type,
                                             zone_id,
                                             location)
      select /*+ CARDINALITY(ids 10) */
             promo_zone_id,
             promo_dtl_id,
             zone_node_type,
             zone_id,
             location
        from table(cast(L_zone_loc_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_zone_location t
       where t.promo_zone_id = VALUE(ids);

   insert into rpm_promo_dtl_disc_ldr_hist (promo_dtl_disc_ladder_id,
                                            promo_dtl_list_id,
                                            change_type,
                                            change_amount,
                                            change_currency,
                                            change_percent,
                                            change_selling_uom,
                                            qual_type,
                                            qual_value)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_disc_ladder_id,
             promo_dtl_list_id,
             change_type,
             change_amount,
             change_currency,
             change_percent,
             change_selling_uom,
             qual_type,
             qual_value
        from table(cast(L_dtl_disc_ldr_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_disc_ladder t
       where t.promo_dtl_disc_ladder_id = VALUE(ids);

   insert into rpm_promo_dtl_merch_node_hist (promo_dtl_merch_node_id,
                                              promo_dtl_list_id,
                                              promo_dtl_id,
                                              merch_type,
                                              dept,
                                              class,
                                              subclass,
                                              item,
                                              diff_id,
                                              skulist,
                                              price_event_itemlist)
      select /*+ CARDINALITY(ids 10) */
             promo_dtl_merch_node_id,
             promo_dtl_list_id,
             promo_dtl_id,
             merch_type,
             dept,
             class,
             subclass,
             item,
             diff_id,
             skulist,
             price_event_itemlist
        from table(cast(L_dtl_merch_node_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_merch_node t
       where t.promo_dtl_merch_node_id = VALUE(ids);

   insert into rpm_promo_dtl_skulist_hist (price_event_id,
                                           skulist,
                                           item,
                                           item_level,
                                           item_desc)
      select /*+ CARDINALITY(ids 10) */
             price_event_id,
             skulist,
             item,
             item_level,
             item_desc
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl_skulist t
       where t.price_event_id = ids.number_1;

 if I_promo_id is null then
 
   insert into rpm_promo_cust_attr_hist (cust_attr_id,
                                         varchar2_1,
                                         varchar2_2,
                                         varchar2_3,
                                         varchar2_4,
                                         varchar2_5,
                                         varchar2_6,
                                         varchar2_7,
                                         varchar2_8,
                                         varchar2_9,
                                         varchar2_10,
                                         number_11,
                                         number_12,
                                         number_13,
                                         number_14,
                                         number_15,
                                         number_16,
                                         number_17,
                                         number_18,
                                         number_19,
                                         number_20,
                                         date_21,
                                         date_22)
      select /*+ CARDINALITY(ids 10) */
             cust_attr_id,
             varchar2_1,
             varchar2_2,
             varchar2_3,
             varchar2_4,
             varchar2_5,
             varchar2_6,
             varchar2_7,
             varchar2_8,
             varchar2_9,
             varchar2_10,
             number_11,
             number_12,
             number_13,
             number_14,
             number_15,
             number_16,
             number_17,
             number_18,
             number_19,
             number_20,
             date_21,
             date_22
        from table(cast(L_promo_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_cust_attr rpca
       where ids.number_2      is NOT NULL
         and rpca.cust_attr_id = ids.number_2;

else

   insert into rpm_promo_cust_attr_hist (cust_attr_id,
                                         varchar2_1,
                                         varchar2_2,
                                         varchar2_3,
                                         varchar2_4,
                                         varchar2_5,
                                         varchar2_6,
                                         varchar2_7,
                                         varchar2_8,
                                         varchar2_9,
                                         varchar2_10,
                                         number_11,
                                         number_12,
                                         number_13,
                                         number_14,
                                         number_15,
                                         number_16,
                                         number_17,
                                         number_18,
                                         number_19,
                                         number_20,
                                         date_21,
                                         date_22)
      select /*+ CARDINALITY(ids 10) */
             cust_attr_id,
             varchar2_1,
             varchar2_2,
             varchar2_3,
             varchar2_4,
             varchar2_5,
             varchar2_6,
             varchar2_7,
             varchar2_8,
             varchar2_9,
             varchar2_10,
             number_11,
             number_12,
             number_13,
             number_14,
             number_15,
             number_16,
             number_17,
             number_18,
             number_19,
             number_20,
             date_21,
             date_22
        from rpm_promo_cust_attr rpca
       where rpca.cust_attr_id = I_promo_id;

end if;         

   insert into rpm_promo_comp_cust_attr_hist (cust_attr_id,
                                              varchar2_1,
                                              varchar2_2,
                                              varchar2_3,
                                              varchar2_4,
                                              varchar2_5,
                                              varchar2_6,
                                              varchar2_7,
                                              varchar2_8,
                                              varchar2_9,
                                              varchar2_10,
                                              number_11,
                                              number_12,
                                              number_13,
                                              number_14,
                                              number_15,
                                              number_16,
                                              number_17,
                                              number_18,
                                              number_19,
                                              number_20,
                                              date_21,
                                              date_22)
      select /*+ CARDINALITY(ids 10) */
             cust_attr_id,
             varchar2_1,
             varchar2_2,
             varchar2_3,
             varchar2_4,
             varchar2_5,
             varchar2_6,
             varchar2_7,
             varchar2_8,
             varchar2_9,
             varchar2_10,
             number_11,
             number_12,
             number_13,
             number_14,
             number_15,
             number_16,
             number_17,
             number_18,
             number_19,
             number_20,
             date_21,
             date_22
        from table(cast(L_comp_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_comp_cust_attr rpcca
       where ids.number_2       is NOT NULL
         and rpcca.cust_attr_id = ids.number_2;

   insert into rpm_promo_dtl_cust_attr_hist (cust_attr_id,
                                             varchar2_1,
                                             varchar2_2,
                                             varchar2_3,
                                             varchar2_4,
                                             varchar2_5,
                                             varchar2_6,
                                             varchar2_7,
                                             varchar2_8,
                                             varchar2_9,
                                             varchar2_10,
                                             number_11,
                                             number_12,
                                             number_13,
                                             number_14,
                                             number_15,
                                             number_16,
                                             number_17,
                                             number_18,
                                             number_19,
                                             number_20,
                                             date_21,
                                             date_22)
      select /*+ CARDINALITY(ids 10) */
             cust_attr_id,
             varchar2_1,
             varchar2_2,
             varchar2_3,
             varchar2_4,
             varchar2_5,
             varchar2_6,
             varchar2_7,
             varchar2_8,
             varchar2_9,
             varchar2_10,
             number_11,
             number_12,
             number_13,
             number_14,
             number_15,
             number_16,
             number_17,
             number_18,
             number_19,
             number_20,
             date_21,
             date_22
        from table(cast(L_dtl_ids as OBJ_NUM_NUM_STR_TBL)) ids,
             rpm_promo_dtl_cust_attr rpdca
       where ids.number_2       is NOT NULL
         and rpdca.cust_attr_id = ids.number_2;

   -- Now clean up the active/workspace tables by removing the promo data just archived...

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_promo_dtl_skulist
       where price_event_id = L_dtl_ids(i).number_1;

   forall i IN L_dtl_merch_node_ids.FIRST..L_dtl_merch_node_ids.LAST
      delete
        from rpm_promo_dtl_merch_node
       where promo_dtl_merch_node_id = L_dtl_merch_node_ids(i);

   forall i IN L_dtl_disc_ldr_ids.FIRST..L_dtl_disc_ldr_ids.LAST
      delete
        from rpm_promo_dtl_disc_ladder
       where promo_dtl_disc_ladder_id = L_dtl_disc_ldr_ids(i);

   forall i IN L_zone_loc_ids.FIRST..L_zone_loc_ids.LAST
      delete
        from rpm_promo_zone_location
       where promo_zone_id = L_zone_loc_ids(i);

   forall i IN L_dtl_list_ids.FIRST..L_dtl_list_ids.LAST
      delete
        from rpm_promo_dtl_list
       where promo_dtl_list_id = L_dtl_list_ids(i);

   forall i IN L_dtl_list_grp_ids.FIRST..L_dtl_list_grp_ids.LAST
      delete
        from rpm_promo_dtl_list_grp
       where promo_dtl_list_grp_id = L_dtl_list_grp_ids(i);

   -- Delete from Location Move Error and Exceptions table that has reference to Promo Detail

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_loc_move_promo_comp_dtl_ex t
       where t.rpm_promo_comp_detail_id = L_dtl_ids(i).number_1;

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_loc_move_promo_error t
       where t.promo_comp_detail_id = L_dtl_ids(i).number_1;

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_promo_dtl_prc_range
       where promo_dtl_id = L_dtl_ids(i).number_1;

   -- Delete any grandchild promo dtl records

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_promo_dtl t
       where t.promo_dtl_id        = L_dtl_ids(i).number_1
         and t.exception_parent_id is NOT NULL
         and EXISTS (select 'x'
                       from rpm_promo_dtl rpd
                      where rpd.promo_dtl_id        = t.exception_parent_id
                        and rpd.exception_parent_id is NOT NULL);

   -- Delete any child promo dtl records

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_promo_dtl t
       where t.promo_dtl_id        =  L_dtl_ids(i).number_1
         and t.exception_parent_id is NOT NULL
         and EXISTS (select 'x'
                       from rpm_promo_dtl rpd
                      where rpd.promo_dtl_id        =  t.exception_parent_id
                        and rpd.exception_parent_id is NULL);

   -- Delete parent promo dtl records

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_promo_dtl_cust_attr
       where L_dtl_ids(i).number_2 is NOT NULL
         and cust_attr_id          = L_dtl_ids(i).number_2;

   forall i IN L_dtl_ids.FIRST..L_dtl_ids.LAST
      delete
        from rpm_promo_dtl t
       where t.promo_dtl_id        = L_dtl_ids(i).number_1
         and t.exception_parent_id is NULL;

   forall i IN L_comp_ids.FIRST..L_comp_ids.LAST
      delete
        from rpm_pending_deal_detail
       where promo_comp_id = L_comp_ids(i).number_1;

   forall i IN L_comp_ids.FIRST..L_comp_ids.LAST
      delete
        from rpm_promo_comp_cust_attr
       where L_comp_ids(i).number_2 is NOT NULL
         and cust_attr_id           = L_comp_ids(i).number_2;

   forall i IN L_comp_ids.FIRST..L_comp_ids.LAST
      delete
        from rpm_promo_comp
       where promo_comp_id = L_comp_ids(i).number_1;

 if I_promo_id is null then
   forall i IN L_promo_ids.FIRST..L_promo_ids.LAST
      delete
        from rpm_promo_deal_link
       where promo_id = L_promo_ids(i).number_1;

   forall i IN L_promo_ids.FIRST..L_promo_ids.LAST
      delete
        from rpm_promo_cust_attr
       where L_promo_ids(i).number_2 is NOT NULL
         and cust_attr_id            = L_promo_ids(i).number_2;

   forall i IN L_promo_ids.FIRST..L_promo_ids.LAST
      delete
        from rpm_promo
       where promo_id = L_promo_ids(i).number_1;
 else
      delete
        from rpm_promo_deal_link
       where promo_id = I_promo_id;

      delete
        from rpm_promo_cust_attr
       where cust_attr_id = I_promo_id;

      delete
        from rpm_promo
       where promo_id = I_promo_id;
end if;

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END ARCHIVE;

END RPM_ARCHIVE_PROMOTIONS;
/
