CREATE OR REPLACE PACKAGE RPM_PRICE_STRATEGY_SQL AS

	CALENDAR_STATUS_UNSELECTED	CONSTANT NUMBER :=  -1;
	CALENDAR_STATUS_NONE		CONSTANT NUMBER :=  0;
	CALENDAR_STATUS_SUSPENDED	CONSTANT NUMBER :=  1;
	CALENDAR_STATUS_EXPIRED		CONSTANT NUMBER :=  2;
	CALENDAR_STATUS_ASSIGNED	CONSTANT NUMBER :=  3;

	STRATEGY_TYPE_AREA_DIFF		CONSTANT VARCHAR2(1) := '0';
	STRATEGY_TYPE_CLEARANCE		CONSTANT VARCHAR2(1) := '1';
	STRATEGY_TYPE_CLEARANCE_MD_DF	CONSTANT VARCHAR2(1) := '2';
	STRATEGY_TYPE_COMPETITIVE	CONSTANT VARCHAR2(1) := '3';
	STRATEGY_TYPE_MARGIN		CONSTANT VARCHAR2(1) := '4';
	STRATEGY_TYPE_MAINTAIN_MARGIN	CONSTANT VARCHAR2(1) := '5';

--------------------------------------------------------

PROCEDURE SEARCH(O_return_code		OUT NUMBER,
                 O_error_msg		OUT VARCHAR2,
                 O_ps_tbl		OUT OBJ_PRICE_STRATEGY_TBL,
                 O_ps_count		OUT NUMBER,
                 I_ps_strategy_ids	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_strategy_types	IN  OBJ_PRICE_STRATEGY_TYPE_TBL,
                 I_ps_depts		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_classes		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_subclasses	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_zone_groups	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_zones		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_price_guides	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_calendars		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_next_calendars	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ps_calendar_status	IN  NUMBER);

--------------------------------------------------------
PROCEDURE CHECK_OVERLAP(O_return_code          OUT NUMBER,
                        O_error_msg            OUT VARCHAR2,
                        O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                        I_dept              IN     DEPS.DEPT%TYPE,
                        I_class             IN     CLASS.CLASS%TYPE,
                        I_subclass          IN     SUBCLASS.SUBCLASS%TYPE,
                        I_zone_id           IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                        I_calendar_id       IN     RPM_CALENDAR.CALENDAR_ID%TYPE,
                        I_ps_strategy_type  IN     NUMBER,
                        i_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE                        
                        );
------------------------------------------------------------------------------
PROCEDURE CHECK_PS_OVERLAP_LOC_MOVE(O_return_code          OUT NUMBER,
                                    O_error_msg            OUT VARCHAR2,
                                    O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                                    I_zone_id           IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                                    I_sched_mov_date    IN     DATE
                                    );
-------------------------------------------------------------------------------------                                    
PROCEDURE CHECK_PS_CC_OVRLAP(O_return_code          OUT NUMBER,
                             O_error_msg            OUT VARCHAR2,
                             O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                             I_dept              IN     DEPS.DEPT%TYPE,
                             I_class             IN     CLASS.CLASS%TYPE,
                             I_subclass          IN     SUBCLASS.SUBCLASS%TYPE,
                             I_zone_id           IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                             I_calendar_id       IN     RPM_CALENDAR.CALENDAR_ID%TYPE,
                             I_ps_strategy_type  IN     NUMBER,
                             I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE
                            );
-------------------------------------------------------------------------------------
PROCEDURE CHECK_PS_CC_OVRLAP(O_return_code          OUT NUMBER,
                             O_error_msg            OUT VARCHAR2,
                             O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                             I_calendar_id       IN     RPM_ZONE_LOCATION.LOCATION%TYPE
                            );
-------------------------------------------------------------------------------------
END RPM_PRICE_STRATEGY_SQL;
/

