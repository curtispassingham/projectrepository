CREATE OR REPLACE PACKAGE BODY RPM_ITEM_LOC_DELETION_SQL AS
--------------------------------------------------------------------------------

FUNCTION DELETE_PRICE_EVENTS(O_error_msg      OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------
PROCEDURE POPULATE_STAGING_TABLE(O_return_code     OUT NUMBER,
                                 O_error_msg       OUT VARCHAR2,
                                 I_item         IN     VARCHAR2,
                                 I_location     IN     NUMBER)
IS

   L_program  VARCHAR2(50) := 'RPM_ITEM_LOC_DELETION_SQL.POPULATE_STAGING_TABLE';

BEGIN

   insert into rpm_stage_deleted_item_loc
      (deleted_item_loc_id,
       item,
       location,
       status)
   values
      (RPM_STAGE_DELETED_ITEM_LOC_SEQ.NEXTVAL,
       I_item,
       I_location,
       'N');

   O_return_code := 1;

   return;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;

      return;

END POPULATE_STAGING_TABLE;
--------------------------------------------------------------------------------
PROCEDURE DELETE_FUTURE_RETAIL(O_return_code    OUT NUMBER,
                               O_error_msg      OUT VARCHAR2)
IS

   L_program VARCHAR2(50) := 'RPM_ITEM_LOC_DELETION_SQL.DELETE_FUTURE_RETAIL';

BEGIN

   delete
     from rpm_link_code_attribute
    where (item,
           location) IN (select item,
                                location
                           from rpm_stage_deleted_item_loc
                          where status = 'N');

   delete /*+ USE_NL(rfr) INDEX(rfr rpm_future_retail_i1) */
     from rpm_future_retail rfr
    where (dept,
           item,
           location) IN (select /*+ ORDERED USE_NL(im) */ im.dept,
                                st.item,
                                st.location
                           from rpm_stage_deleted_item_loc st,
                                rpm_stage_deleted_item_master im
                          where im.item   = st.item
                            and st.status = 'N'
                         union all
                         select /*+ ORDERED USE_NL(im) */ im.dept,
                                st.item,
                                st.location
                           from rpm_stage_deleted_item_loc st,
                                item_master im
                          where im.item   = st.item
                            and st.status = 'N');

   delete /*+ USE_NL(rfr) INDEX(rfr rpm_future_retail_i1) */
     from rpm_future_retail rfr
    where (dept,
           item) IN (select /*+ ORDERED USE_NL(im) */ im.dept,
                            im.item
                       from rpm_stage_deleted_item_master im);

   delete from rpm_item_loc
    where (dept,
           item,
           loc) IN (select /*+ ORDERED USE_NL(im) */ im.dept,
                           st.item,
                           st.location
                      from rpm_stage_deleted_item_loc st,
                           rpm_stage_deleted_item_master im
                     where im.item   = st.item
                       and st.status = 'N'
                    union all
                    select /*+ ORDERED USE_NL(im) */ im.dept,
                           st.item,
                           st.location
                      from rpm_stage_deleted_item_loc st,
                           item_master im
                     where im.item   = st.item
                       and st.status = 'N');

   delete
     from rpm_item_loc
    where (dept,
           item) IN (select /*+ ORDERED USE_NL(im) */ im.dept,
                           im.item
                      from rpm_stage_deleted_item_master im);

   delete
     from rpm_link_code_attribute rlca
    where location IN (select location
                         from rpm_stage_deleted_item_loc)
      and NOT EXISTS (select 'x'
                        from item_loc il
                       where rlca.location = il.loc);

   delete
     from rpm_zone_location rzl
    where location IN (select location
                        from rpm_stage_deleted_item_loc)
      and NOT EXISTS (select 'x'
                        from item_loc il
                       where rzl.location = il.loc);


   if DELETE_PRICE_EVENTS(O_error_msg) = 0 then
      O_return_code := 0;

      return;
   end if;

   delete
     from rpm_promo_item_loc_expl
    where (dept,
           item,
           location) IN (select dim.dept,
                                dil.item,
                                dil.location
                           from rpm_stage_deleted_item_loc dil,
                                rpm_stage_deleted_item_master dim
                          where dil.item   = dim.item
                            and dil.status = 'N'
                         union all
                         select im.dept,
                                dil.item,
                                dil.location
                           from rpm_stage_deleted_item_loc dil,
                                item_master im
                          where im.item    = dil.item
                            and dil.status = 'N');

   delete
     from rpm_zone_future_retail
    where item IN (select im.item
                     from rpm_stage_deleted_item_master im);


   delete
     from rpm_item_zone_price
    where item IN (select im.item
                     from rpm_stage_deleted_item_master im);

   delete
     from rpm_mbc_attribute
     where item IN (select im.item
                      from rpm_stage_deleted_item_master im);

   delete
     from rpm_stage_deleted_item_master;

   delete
     from rpm_stage_deleted_item_loc
    where status = 'N';

   O_return_code := 1;

   return;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;

      return;

END DELETE_FUTURE_RETAIL;
--------------------------------------------------------------------------------

FUNCTION DELETE_PRICE_EVENTS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ITEM_LOC_DELETION_SQL.DELETE_PRICE_EVENTS';

   L_skulist_event_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_merch_list_ids    OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_merch_list_dtls   OBJ_NUMERIC_ID_TABLE := NULL;

   L_promo_dtl_ids_from_rpzl     OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_dtl_ids_from_del_im   OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_dtl_ids_from_del_il   OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_dtl_ids_from_del_sl   OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_dtl_ids_from_del_peil OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_dtl_ids_from_del_sge  OBJ_NUMERIC_ID_TABLE := NULL;

   L_promo_dtl_ids  OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_comp_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_promo_ids      OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_PROMO_DTL_IDS is
      select distinct promo_dtl_id
        from (select VALUE(ids) promo_dtl_id
                from table(cast(L_promo_dtl_ids_from_rpzl as OBJ_NUMERIC_ID_TABLE)) ids
              union all
              select VALUE(ids) promo_dtl_id
                from table(cast(L_promo_dtl_ids_from_del_im as OBJ_NUMERIC_ID_TABLE)) ids
              union all
              select VALUE(ids) promo_dtl_id
                from table(cast(L_promo_dtl_ids_from_del_il as OBJ_NUMERIC_ID_TABLE)) ids
              union all
              select VALUE(ids) promo_dtl_id
                from table(cast(L_promo_dtl_ids_from_del_sl as OBJ_NUMERIC_ID_TABLE)) ids
              union all
              select VALUE(ids) promo_dtl_id
                from table(cast(L_promo_dtl_ids_from_del_peil as OBJ_NUMERIC_ID_TABLE)) ids
              union all
              select VALUE(ids) promo_dtl_id
                from table(cast(L_promo_dtl_ids_from_del_sge as OBJ_NUMERIC_ID_TABLE)) ids);

   cursor C_PROMO_COMP_IDS is
      select /*+ CARDINALITY (ids, 10) */
             distinct rpd.promo_comp_id
        from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd
       where VALUE(ids) = rpd.promo_dtl_id;

   cursor C_PROMO_IDS is
      select /*+ CARDINALITY (ids, 10) */
             distinct rpc.promo_id
        from table(cast(L_promo_comp_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_comp rpc
       where VALUE(ids) = rpc.promo_comp_id;

   cursor C_PROMO_COMP_HIST_IDS is
      select /*+ CARDINALITY (ids, 10) */
             distinct rpd.promo_comp_id
        from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl_hist rpd
       where VALUE(ids) = rpd.promo_dtl_id;

   cursor C_PROMO_HIST_IDS is
      select /*+ CARDINALITY (ids, 10) */
             distinct rpc.promo_id
        from table(cast(L_promo_comp_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_comp_hist rpc
       where VALUE(ids) = rpc.promo_comp_id;

BEGIN

   delete
     from rpm_merch_list_detail
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING merch_list_id BULK COLLECT into L_merch_list_dtls;

   forall i IN 1..L_merch_list_dtls.COUNT
      delete
        from rpm_merch_list_head h
       where merch_list_id = L_merch_list_dtls(i)
         and NOT EXISTS (select 'x'
                           from rpm_merch_list_detail d
                          where h.merch_list_id = d.merch_list_id)
   RETURNING merch_list_id BULK COLLECT into L_merch_list_ids;

   delete
     from rpm_price_change_cust_attr rpcca
    where EXISTS (select 'x'
                    from rpm_stage_deleted_item_loc rsdil,
                         rpm_price_change rpc
                   where rsdil.status     = 'N'
                     and rpc.item         = rsdil.item
                     and rpc.location     = rsdil.location
                     and rpc.cust_attr_id = rpcca.cust_attr_id);

   delete
     from rpm_loc_move_prc_chng_ex
    where price_change_id IN (select price_change_id
                                from rpm_price_change rpc,
                                     rpm_stage_deleted_item_loc rsdil
                               where rsdil.status = 'N'
                                 and rpc.item     = rsdil.item
                                 and rpc.location = rsdil.location);

   delete
     from rpm_price_change
    where (item,
           location) IN (select item,
                                location
                           from rpm_stage_deleted_item_loc
                          where status = 'N');

   delete
     from rpm_price_change_cust_attr rpcca
    where EXISTS (select 'x'
                    from rpm_stage_deleted_item_master rsdim,
                         rpm_price_change rpc
                   where rpc.item         = rsdim.item
                     and rpc.cust_attr_id = rpcca.cust_attr_id);

   delete
     from rpm_loc_move_prc_chng_ex
    where price_change_id IN (select price_change_id
                                from rpm_price_change rpc,
                                     rpm_stage_deleted_item_master rsdim
                               where rpc.item = rsdim.item);

   delete
     from rpm_price_change
    where item IN (select item
                     from rpm_stage_deleted_item_master);

   delete
     from rpm_price_change_skulist
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING price_event_id BULK COLLECT into L_skulist_event_ids;

   delete
     from rpm_price_change_cust_attr rpcca
    where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                    from table(cast(L_skulist_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                         rpm_price_change rpc
                   where rpc.price_change_id = VALUE(ids)
                     and rpc.cust_attr_id    = rpcca.cust_attr_id
                     and NOT EXISTS (select 'x'
                                       from rpm_price_change_skulist rpcs
                                      where rpcs.price_event_id = rpc.price_change_id));

   forall i IN 1..L_skulist_event_ids.COUNT
      delete
        from rpm_price_change
       where price_change_id = L_skulist_event_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_price_change_skulist
                          where price_event_id = L_skulist_event_ids(i));

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_price_change
       where price_event_itemlist = L_merch_list_ids(i);

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_price_change
       where sys_generated_exclusion = 1
         and skulist                 = L_merch_list_ids(i);

   delete
     from rpm_clearance_cust_attr rcca
    where EXISTS (select 'x'
                    from rpm_stage_deleted_item_loc rsdil,
                         rpm_clearance rc
                   where rsdil.status    = 'N'
                     and rc.item         = rsdil.item
                     and rc.location     = rsdil.location
                     and rc.cust_attr_id = rcca.cust_attr_id);

   delete
     from rpm_loc_move_clearance_ex
    where clearance_id IN (select clearance_id
                             from rpm_clearance rc,
                                  rpm_stage_deleted_item_loc rsdil
                            where rsdil.status = 'N'
                              and rc.item      = rsdil.item
                              and rc.location  = rsdil.location);

   delete
     from rpm_clearance
    where (item,
           location) IN (select item,
                                location
                           from rpm_stage_deleted_item_loc
                          where status = 'N');

   delete
     from rpm_clearance_cust_attr rcca
    where EXISTS (select 'x'
                    from rpm_stage_deleted_item_master rsdim,
                         rpm_clearance rc
                   where rc.item         = rsdim.item
                     and rc.cust_attr_id = rcca.cust_attr_id);

   delete
     from rpm_loc_move_clearance_ex
    where clearance_id IN (select clearance_id
                             from rpm_clearance rc,
                                  rpm_stage_deleted_item_master rsdim
                            where rc.item = rsdim.item);

   delete
     from rpm_clearance
    where item IN (select item
                     from rpm_stage_deleted_item_master);

   delete
     from rpm_clearance_skulist
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING price_event_id BULK COLLECT into L_skulist_event_ids;

   delete
     from rpm_clearance_cust_attr rcca
    where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                    from table(cast(L_skulist_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                         rpm_clearance rc
                   where rc.clearance_id = VALUE(ids)
                     and rc.cust_attr_id = rcca.cust_attr_id
                     and NOT EXISTS (select 'x'
                                       from rpm_clearance_skulist rcs
                                      where rcs.price_event_id = rc.clearance_id));

   forall i IN 1..L_skulist_event_ids.COUNT
      delete
        from rpm_clearance
       where clearance_id = L_skulist_event_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_clearance_skulist
                          where price_event_id = L_skulist_event_ids(i));

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_clearance
       where price_event_itemlist = L_merch_list_ids(i);

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_clearance
       where sys_generated_exclusion = 1
         and skulist                 = L_merch_list_ids(i);

   delete
     from rpm_clearance_reset
    where (location,
           item) IN (select location,
                            item
                       from rpm_stage_deleted_item_loc
                      where status = 'N');

   delete
     from rpm_clearance_reset
    where item IN (select item
                     from rpm_stage_deleted_item_master);

   delete
     from rpm_promo_zone_location pzl
    where location is NOT NULL
      and NOT EXISTS (select 'x'
                        from store s
                       where pzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                         and pzl.location       = s.store)
   RETURNING pzl.promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_rpzl;

   delete
     from rpm_promo_dtl_merch_node
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_im;

   -- With all data cleaned up based on the stage tables, the process
   -- can now clean up all the other promo tables so that there is no
   -- promo detail without a loc or item under it and no comp without
   -- a detail under it and no promo without a comp under it.

   delete
     from rpm_promo_dtl_merch_node pdmn
    where (promo_dtl_id,
           item) IN (select pzl.promo_dtl_id,
                            rdil.item
                       from rpm_promo_zone_location pzl,
                            rpm_promo_dtl_merch_node rpdmn,
                            rpm_stage_deleted_item_loc rdil
                      where pzl.promo_dtl_id = rpdmn.promo_dtl_id
                        and pzl.location     = rdil.location
                        and rpdmn.item       = rdil.item)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_il;

   delete
     from rpm_promo_dtl_skulist
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING price_event_id BULK COLLECT into L_skulist_event_ids;

   forall i IN 1..L_skulist_event_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node
       where promo_dtl_id = L_skulist_event_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_skulist
                          where price_event_id = L_skulist_event_ids(i))
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_sl;

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node
       where price_event_itemlist is NOT NULL
         and price_event_itemlist = L_merch_list_ids(i)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_peil;

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node rpdmn
       where skulist is NOT NULL
         and skulist = L_merch_list_ids(i)
         and EXISTS (select 1
                       from rpm_promo_dtl rpd
                      where rpd.promo_dtl_id            = rpdmn.promo_dtl_id
                        and rpd.sys_generated_exclusion = 1)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_sge;

   open C_PROMO_DTL_IDS;
   fetch C_PROMO_DTL_IDS BULK COLLECT into L_promo_dtl_ids;
   close C_PROMO_DTL_IDS;

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node pdmn
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_zone_location pzl
                          where pzl.promo_dtl_id = pdmn.promo_dtl_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_zone_location pzl
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_merch_node pdmn
                          where pdmn.promo_dtl_id = pzl.promo_dtl_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_prc_range rpdpr
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_merch_node pdmn
                          where pdmn.promo_dtl_id = rpdpr.promo_dtl_id);

   delete
     from rpm_promo_dtl_disc_ladder pddl
    where promo_dtl_list_id IN (select /*+ CARDINALITY(ids, 10) */
                                       rpdl.promo_dtl_list_id
                                  from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                       rpm_promo_dtl rpd,
                                       rpm_promo_dtl_list_grp rpdlg,
                                       rpm_promo_dtl_list rpdl
                                 where VALUE(ids)                 = rpd.promo_dtl_id
                                   and rpd.promo_dtl_id           = rpdlg.promo_dtl_id
                                   and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id)
      and NOT EXISTS (select 'x'
                        from rpm_promo_dtl_merch_node pdmn
                       where pdmn.promo_dtl_list_id = pddl.promo_dtl_list_id);

   delete
     from rpm_promo_dtl_list pdl
    where promo_dtl_list_grp_id IN (select /*+ CARDINALITY(ids, 10) */
                                           rpdlg.promo_dtl_list_grp_id
                                      from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_promo_dtl rpd,
                                           rpm_promo_dtl_list_grp rpdlg
                                     where VALUE(ids)       = rpd.promo_dtl_id
                                       and rpd.promo_dtl_id = rpdlg.promo_dtl_id)
      and NOT EXISTS (select 'x'
                        from rpm_promo_dtl_merch_node pdmn
                       where pdmn.promo_dtl_list_id = pdl.promo_dtl_list_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_list_grp pdlg
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_list pdl
                          where pdl.promo_dtl_list_grp_id = pdlg.promo_dtl_list_grp_id);

   open C_PROMO_COMP_IDS;
   fetch C_PROMO_COMP_IDS BULK COLLECT into L_promo_comp_ids;
   close C_PROMO_COMP_IDS;

   open C_PROMO_IDS;
   fetch C_PROMO_IDS BULK COLLECT into L_promo_ids;
   close C_PROMO_IDS;

   delete
     from rpm_promo_dtl_cust_attr rpdca
     where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                     from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_promo_dtl rpd
                    where rpd.promo_dtl_id = VALUE(ids)
                      and rpd.cust_attr_id is NOT NULL
                      and rpd.cust_attr_id = rpdca.cust_attr_id
                      and NOT EXISTS (select 'x'
                                        from rpm_promo_dtl_list_grp rpdlg
                                       where rpdlg.promo_dtl_id = rpd.promo_dtl_id));

   delete
     from rpm_promo_dtl
    where exception_parent_id IN (select /*+ CARDINALITY(ids, 10) */
                                         VALUE(ids)
                                    from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids
                                   where NOT EXISTS (select 'x'
                                                       from rpm_promo_dtl_list_grp pdlg
                                                      where pdlg.promo_dtl_id = VALUE(ids)));


   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_loc_move_promo_comp_dtl_ex rpd
       where rpm_promo_comp_detail_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_list_grp pdlg
                          where pdlg.promo_dtl_id = rpd.rpm_promo_comp_detail_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl rpd
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_list_grp pdlg
                          where pdlg.promo_dtl_id = rpd.promo_dtl_id);

   forall i IN 1..L_promo_comp_ids.COUNT
      delete
        from rpm_pending_deal_detail rpdd
       where promo_comp_id = L_promo_comp_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl rpd
                          where rpd.promo_comp_id = rpdd.promo_comp_id);

   forall i IN 1..L_promo_comp_ids.COUNT
      delete
        from rpm_promo_comp_thresh_link rpctl
       where promo_comp_id = L_promo_comp_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl rpd
                          where rpd.promo_comp_id = rpctl.promo_comp_id);

   delete
     from rpm_promo_comp_cust_attr rpcca
     where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                     from table(cast(L_promo_comp_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_promo_comp rpc
                    where rpc.promo_comp_id = VALUE(ids)
                      and rpc.cust_attr_id  = rpcca.cust_attr_id
                      and NOT EXISTS (select 'x'
                                        from rpm_promo_dtl rpd
                                       where rpd.promo_comp_id = rpc.promo_comp_id));

   forall i IN 1..L_promo_comp_ids.COUNT
      delete
        from rpm_promo_comp rpc
       where promo_comp_id = L_promo_comp_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl rpd
                          where rpd.promo_comp_id = rpc.promo_comp_id);

   forall i IN 1..L_promo_ids.COUNT
      delete
        from rpm_promo_deal_link rpdl
       where promo_id = L_promo_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_comp rpc
                          where rpc.promo_id = rpdl.promo_id);

   delete
     from rpm_promo_cust_attr rpca
     where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                     from table(cast(L_promo_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_promo rp
                    where rp.promo_id     = VALUE(ids)
                      and rp.cust_attr_id = rpca.cust_attr_id
                      and NOT EXISTS (select 'x'
                                        from rpm_promo_comp rpc
                                       where rpc.promo_id = rp.promo_id));

   forall i IN 1..L_promo_ids.COUNT
      delete
        from rpm_promo rp
       where promo_id = L_promo_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_comp rpc
                          where rpc.promo_id = rp.promo_id);

   --- Lastly, the Promo History Tables...

   delete
     from rpm_promo_zone_location_hist pzl
    where location is NOT NULL
      and NOT EXISTS (select 'x'
                        from store s
                       where pzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                         and pzl.location       = s.store)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_rpzl;

   delete
     from rpm_promo_dtl_merch_node_hist
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_im;

   -- With all data cleaned up based on the stage tables, the process
   -- can now clean up all the other promo tables so that there is no
   -- promo detail without a loc or item under it and no comp without
   -- a detail under it and no promo without a comp under it.

   delete
     from rpm_promo_dtl_merch_node_hist pdmn
    where (promo_dtl_id,
           item) IN (select pzl.promo_dtl_id,
                            rdil.item
                       from rpm_promo_zone_location_hist pzl,
                            rpm_promo_dtl_merch_node_hist rpdmn,
                            rpm_stage_deleted_item_loc rdil
                      where pzl.promo_dtl_id = rpdmn.promo_dtl_id
                        and pzl.location     = rdil.location
                        and rpdmn.item       = rdil.item)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_il;

   delete
     from rpm_promo_dtl_skulist_hist
    where item IN (select item
                     from rpm_stage_deleted_item_master)
   RETURNING price_event_id BULK COLLECT into L_skulist_event_ids;

   forall i IN 1..L_skulist_event_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node_hist
       where promo_dtl_id = L_skulist_event_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_skulist_hist
                          where price_event_id = L_skulist_event_ids(i))
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_sl;

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node_hist
       where price_event_itemlist is NOT NULL
         and price_event_itemlist = L_merch_list_ids(i)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_peil;

   forall i IN 1..L_merch_list_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node_hist rpdmn
       where skulist is NOT NULL
         and skulist = L_merch_list_ids(i)
         and EXISTS (select 1
                       from rpm_promo_dtl_hist rpd
                      where rpd.promo_dtl_id            = rpdmn.promo_dtl_id
                        and rpd.sys_generated_exclusion = 1)
   RETURNING promo_dtl_id BULK COLLECT into L_promo_dtl_ids_from_del_sge;

   open C_PROMO_DTL_IDS;
   fetch C_PROMO_DTL_IDS BULK COLLECT into L_promo_dtl_ids;
   close C_PROMO_DTL_IDS;

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_merch_node_hist pdmn
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_zone_location_hist pzl
                          where pzl.promo_dtl_id = pdmn.promo_dtl_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_zone_location_hist pzl
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_merch_node_hist pdmn
                          where pdmn.promo_dtl_id = pzl.promo_dtl_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_prc_range_hist rpdpr
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_merch_node_hist pdmn
                          where pdmn.promo_dtl_id = rpdpr.promo_dtl_id);

   delete
     from rpm_promo_dtl_disc_ldr_hist pddl
    where promo_dtl_list_id IN (select /*+ CARDINALITY(ids, 10) */
                                       rpdl.promo_dtl_list_id
                                  from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                       rpm_promo_dtl_hist rpd,
                                       rpm_promo_dtl_list_grp_hist rpdlg,
                                       rpm_promo_dtl_list_hist rpdl
                                 where VALUE(ids)                 = rpd.promo_dtl_id
                                   and rpd.promo_dtl_id           = rpdlg.promo_dtl_id
                                   and rpdl.promo_dtl_list_grp_id = rpdlg.promo_dtl_list_grp_id)
      and NOT EXISTS (select 'x'
                        from rpm_promo_dtl_merch_node_hist pdmn
                       where pdmn.promo_dtl_list_id = pddl.promo_dtl_list_id);

   delete
     from rpm_promo_dtl_list_hist pdl
    where promo_dtl_list_grp_id IN (select /*+ CARDINALITY(ids, 10) */
                                           rpdlg.promo_dtl_list_grp_id
                                      from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_promo_dtl_hist rpd,
                                           rpm_promo_dtl_list_grp_hist rpdlg
                                     where VALUE(ids)       = rpd.promo_dtl_id
                                       and rpd.promo_dtl_id = rpdlg.promo_dtl_id)
      and NOT EXISTS (select 'x'
                        from rpm_promo_dtl_merch_node_hist pdmn
                       where pdmn.promo_dtl_list_id = pdl.promo_dtl_list_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_list_grp_hist pdlg
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_list_hist pdl
                          where pdl.promo_dtl_list_grp_id = pdlg.promo_dtl_list_grp_id);

   open C_PROMO_COMP_HIST_IDS;
   fetch C_PROMO_COMP_HIST_IDS BULK COLLECT into L_promo_comp_ids;
   close C_PROMO_COMP_HIST_IDS;

   open C_PROMO_HIST_IDS;
   fetch C_PROMO_HIST_IDS BULK COLLECT into L_promo_ids;
   close C_PROMO_HIST_IDS;

   delete
     from rpm_promo_dtl_cust_attr_hist rpdcah
     where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                     from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_promo_dtl_hist rpdh
                    where rpdh.promo_dtl_id = VALUE(ids)
                      and rpdh.cust_attr_id is NOT NULL
                      and rpdh.cust_attr_id = rpdcah.cust_attr_id
                      and NOT EXISTS (select 'x'
                                        from rpm_promo_dtl_list_grp_hist rpdlgh
                                       where rpdlgh.promo_dtl_id = rpdh.promo_dtl_id));

   delete
     from rpm_promo_dtl_hist
    where exception_parent_id IN (select VALUE(ids)
                                    from table(cast(L_promo_dtl_ids as OBJ_NUMERIC_ID_TABLE)) ids
                                   where NOT EXISTS (select 'x'
                                                       from rpm_promo_dtl_list_grp_hist pdlg
                                                      where pdlg.promo_dtl_id = VALUE(ids)));

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_loc_move_promo_comp_dtl_ex rpd
       where rpm_promo_comp_detail_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_list_grp_hist pdlg
                          where pdlg.promo_dtl_id = rpd.rpm_promo_comp_detail_id);

   forall i IN 1..L_promo_dtl_ids.COUNT
      delete
        from rpm_promo_dtl_hist rpd
       where promo_dtl_id = L_promo_dtl_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_list_grp_hist pdlg
                          where pdlg.promo_dtl_id = rpd.promo_dtl_id);

   delete
     from rpm_promo_comp_cust_attr_hist rpccah
     where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                     from table(cast(L_promo_comp_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_promo_comp_hist rpch
                    where rpch.promo_comp_id = VALUE(ids)
                      and rpch.cust_attr_id  = rpccah.cust_attr_id
                      and NOT EXISTS (select 'x'
                                        from rpm_promo_dtl_hist rpdh
                                       where rpdh.promo_comp_id = rpch.promo_comp_id));

   forall i IN 1..L_promo_comp_ids.COUNT
      delete
        from rpm_promo_comp_hist rpc
       where promo_comp_id = L_promo_comp_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_dtl_hist rpd
                          where rpd.promo_comp_id = rpc.promo_comp_id);

   delete
     from rpm_promo_cust_attr_hist rpcah
     where EXISTS (select /*+ CARDINALITY(ids, 10) */ 'x'
                     from table(cast(L_promo_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                          rpm_promo_hist rph
                    where rph.promo_id     = VALUE(ids)
                      and rph.cust_attr_id = rpcah.cust_attr_id
                      and NOT EXISTS (select 'x'
                                        from rpm_promo_comp_hist rpch
                                       where rpch.promo_id = rph.promo_id));

   forall i IN 1..L_promo_ids.COUNT
      delete
        from rpm_promo_hist rp
       where promo_id = L_promo_ids(i)
         and NOT EXISTS (select 'x'
                           from rpm_promo_comp_hist rpc
                          where rpc.promo_id = rp.promo_id);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return 0;

END DELETE_PRICE_EVENTS;
--------------------------------------------------------------------------------
END RPM_ITEM_LOC_DELETION_SQL;
/
