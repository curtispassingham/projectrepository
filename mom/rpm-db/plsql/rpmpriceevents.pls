CREATE OR REPLACE PACKAGE RPM_PRICE_EVENT_SQL AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------
FUNCTION UPDATE_ITEM_ZONE_PRICE(O_error_message    OUT VARCHAR2,
                                I_pc_tbl        IN     OBJ_PRICE_CHANGE_TBL)
RETURN BOOLEAN;

-----------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_LOC_LUWS_IN_RMS(O_error_message    OUT VARCHAR2,
                                      I_thd_luw_tbl   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_LOC_DEALS(O_error_message    OUT VARCHAR2)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
FUNCTION PROCESS_ITEM_LOC_DEALS(O_error_message    OUT VARCHAR2,
                                I_events_tbl    IN     OBJ_PRICE_CHANGE_TBL)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
FUNCTION INITIALIZE_ITEMLOC_THREADS(O_error_msg              OUT VARCHAR2,
                                    O_max_thread_num         OUT NUMBER,
                                    I_min_items_in_thread IN     NUMBER)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
FUNCTION UPDATE_RMS(O_error_message         OUT VARCHAR2,
                    I_pc_tbl             IN     OBJ_PRICE_CHANGE_TBL,
                    I_change_event_state IN     BOOLEAN DEFAULT FALSE)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
FUNCTION UPDATE_RMS(O_error_message         OUT VARCHAR2,
                    I_pc_il_tbl          IN     OBJ_PRICE_CHANGE_IL_TBL,
                    I_change_event_state IN     BOOLEAN DEFAULT FALSE)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
PROCEDURE ANALYZE_STAGE_TABLES_INDICES(O_return_code      OUT NUMBER,
                                       O_error_message    OUT VARCHAR2,
                                       I_schema_owner  IN     VARCHAR2,
                                       I_stage_table   IN     VARCHAR2 DEFAULT 'ALL');

-----------------------------------------------------------------------------------------
FUNCTION INITIALIZE_PRICEEVENT_THREADS(O_error_msg                     OUT VARCHAR2,
                                       O_max_thread_num                OUT NUMBER,
                                       I_min_itemloc_in_thread      IN     NUMBER,
                                       I_effective_date             IN     DATE,
                                       I_restart                    IN     NUMBER DEFAULT 0,
                                       I_emer_chunk_data_process_id IN     NUMBER DEFAULT NULL)
RETURN NUMBER;

-----------------------------------------------------------------------------------------
FUNCTION PROCESS_PRICE_EVENT_LUWS (O_error_message    OUT VARCHAR2,
                                   I_thd_luw_tbl   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

----------------------------------------------------------------------------------------
FUNCTION UPDATE_PE_STATUS (O_error_message    OUT VARCHAR2)
RETURN NUMBER;

----------------------------------------------------------------------------------------
END RPM_PRICE_EVENT_SQL;
/
