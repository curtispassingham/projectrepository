CREATE OR REPLACE PACKAGE BODY RPM_PRICE_INQUIRY_SQL AS

   LP_vdate             DATE               := GET_VDATE;
   LP_LOCATION_LEVEL    CONSTANT NUMBER(1) := 0;
   LP_ZONE_LEVEL        CONSTANT NUMBER(1) := 1;
   LP_TRAN_LEVEL        CONSTANT NUMBER(1) := 0;
   LP_PARENT_LEVEL      CONSTANT NUMBER(1) := 1;
   LP_PARENT_DIFF_LEVEL CONSTANT NUMBER(1) := 2;

   LP_I_search_criteria OBJ_PRICE_INQ_SEARCH_REC := NULL;
   LP_zone_search       NUMBER(1)                := NULL;

--------------------------------------------------------
-- Procedure Prototypes
--------------------------------------------------------

PROCEDURE POP_GTT (O_return_code    OUT NUMBER,
                   O_error_msg      OUT VARCHAR2);
--------------------------------------------------------

PROCEDURE POP_FUTURE_RETAIL_GTT (O_return_code    OUT NUMBER,
                                 O_error_msg      OUT VARCHAR2);
--------------------------------------------------------

PROCEDURE EXPLODE_ITEM_LOC (O_return_code    OUT NUMBER,
                            O_error_msg      OUT VARCHAR2);
--------------------------------------------------------

PROCEDURE BUILD_PRICE_INQ_VO (O_return_code    OUT NUMBER,
                              O_error_msg      OUT VARCHAR2,
                              O_prices         OUT NOCOPY OBJ_PRC_INQ_TBL);

--------------------------------------------------------
-- Procedure Definitions
--------------------------------------------------------
PROCEDURE POP_GTT(O_return_code    OUT NUMBER,
                  O_error_msg      OUT VARCHAR2) AS

   L_program VARCHAR2(35) := 'RPM_PRICE_INQUIRY_SQL.POP_GTT';

   L_all_itemlist OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_ALL_ILIST is
      select /*+ CARDINALITY (tbl 100) */
             im_dept.skulist
        from table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
             skulist_dept im_dept
       where im_dept.dept     = NVL(tbl.dept, im_dept.dept)
         and im_dept.class    = NVL(tbl.class, im_dept.class)
         and im_dept.subclass = NVL(tbl.subclass, im_dept.subclass);

BEGIN
   -- Clean up the temporary tables
   delete
     from rpm_price_inquiry_gtt;

   delete
     from numeric_id_gtt;

   -- Populate the Numeric_gtt_table
   if LP_I_search_criteria.locations is NOT NULL and
      LP_I_search_criteria.locations.COUNT > 0 then

      -- Populate with Locations
      LP_zone_search := LP_LOCATION_LEVEL;

      insert into numeric_id_gtt(numeric_id)
         select distinct locs.column_value
           from table(cast(LP_I_search_criteria.locations as OBJ_NUMERIC_ID_TABLE)) locs;

   elsif LP_I_search_criteria.zone_ids is NOT NULL and
         LP_I_search_criteria.zone_ids.COUNT > 0 then

      -- Populate with Zone IDS
      LP_zone_search := LP_ZONE_LEVEL;

      insert into numeric_id_gtt(numeric_id)
         select distinct zone_ids.column_value
           from table(cast(LP_I_search_criteria.zone_ids as OBJ_NUMERIC_ID_TABLE)) zone_ids;
   else
      -- Populate with Zone IDS of the given Zone Group
      LP_zone_search := LP_ZONE_LEVEL;

      insert into numeric_id_gtt(numeric_id)
         select rz.zone_id
           from rpm_zone rz
          where rz.zone_group_id = LP_I_search_criteria.zone_group;

   end if;

   -- Populate the temporary tables based on the input search criteria
   -- Populate Transaction Level Item data
   if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM and
      LP_I_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM then

      if LP_I_search_criteria.items is NOT NULL and
         LP_I_search_criteria.items.COUNT > 0 then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_1,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) */
                           distinct
                           im.item,
                           im.item_parent,
                           DECODE(im.item_parent,
                                  NULL, NULL,
                                  im.diff_1) diff_1,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) tbl,
                           item_master im,
                           numeric_id_gtt num
                     where im.item = VALUE(tbl)
                       and EXISTS (select 1
                                     from item_loc il
                                    where LP_zone_search = LP_LOCATION_LEVEL
                                      and il.item        = im.item
                                      and il.loc         = num.numeric_id
                                   union all
                                   select 1
                                     from item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search = LP_ZONE_LEVEL
                                      and rzl.zone_id    = num.numeric_id
                                      and il.item        = im.item
                                      and il.loc         = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;

      elsif LP_I_search_criteria.items is NULL or
            LP_I_search_criteria.items.COUNT = 0 then

         -- If no ITEMS have been specified in the GUI, get all the ITEMS from the DEPT(s) specified.
         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_1,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) */
                           distinct
                           im.item,
                           im.item_parent,
                           DECODE(im.item_parent,
                                  NULL, NULL,
                                  im.diff_1) diff_1,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                           item_master im,
                           numeric_id_gtt num
                     where im.dept         = tbl.dept
                       and im.class        = NVL(tbl.class, im.class)
                       and im.subclass     = NVL(tbl.subclass, im.subclass)
                       and im.item_level   = im.tran_level
                       and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and EXISTS (select 1
                                     from item_loc il
                                    where LP_zone_search = LP_LOCATION_LEVEL
                                      and il.item        = im.item
                                      and il.loc         = num.numeric_id
                                   union all
                                   select 1
                                     from item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search = LP_ZONE_LEVEL
                                      and rzl.zone_id    = num.numeric_id
                                      and il.item        = im.item
                                      and il.loc         = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;
      end if;
   end if;

   -- Populate Parent Level Item data
   if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM and
      LP_I_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT then

      if LP_I_search_criteria.items is NOT NULL and
         LP_I_search_criteria.items.COUNT > 0 then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_1,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) */
                           distinct
                           im.item,
                           NULL item_parent,
                           NULL diff_1,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) tbl,
                           item_master im,
                           numeric_id_gtt num
                     where im.item = VALUE(tbl)
                       and EXISTS (select 1
                                     from item_loc il,
                                          item_master im1
                                    where LP_zone_search   = LP_LOCATION_LEVEL
                                      and im1.item_parent  = im.item
                                      and im1.item_level   = im1.tran_level
                                      and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and il.item          = im1.item
                                      and il.loc           = num.numeric_id
                                   union all
                                   select 1
                                     from item_master im2,
                                          item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search   = LP_ZONE_LEVEL
                                      and rzl.zone_id      = num.numeric_id
                                      and im2.item_parent  = im.item
                                      and im2.item_level   = im2.tran_level
                                      and im2.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im2.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and il.item          = im2.item
                                      and il.loc           = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;

      elsif LP_I_search_criteria.items is NULL or
            LP_I_search_criteria.items.COUNT = 0 then

         -- If no ITEMS have been specified in the GUI, get all the ITEMS from the DEPT(s) specified.
         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_1,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) */
                           distinct
                           im.item,
                           NULL item_parent,
                           NULL diff_1,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                           item_master im,
                           numeric_id_gtt num
                     where im.dept           = tbl.dept
                       and im.class          = NVL(tbl.class, im.class)
                       and im.subclass       = NVL(tbl.subclass, im.subclass)
                       and im.item_level     = im.tran_level - 1
                       and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and EXISTS (select 1
                                     from item_master im1,
                                          item_loc il
                                    where LP_zone_search   = LP_LOCATION_LEVEL
                                      and im1.item_parent  = im.item
                                      and im1.item_level   = im1.tran_level
                                      and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and il.item          = im1.item
                                      and il.loc           = num.numeric_id
                                   union all
                                   select 1
                                     from item_master im2,
                                          item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search   = LP_ZONE_LEVEL
                                      and rzl.zone_id      = num.numeric_id
                                      and im2.item_parent  = im.item
                                      and im2.item_level   = im2.tran_level
                                      and im2.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im2.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and il.item          = im2.item
                                      and il.loc           = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;
      end if;
   end if;

   -- Populate Parent Diff Item data
   if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM and
      LP_I_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF then
      ---
      if (LP_I_search_criteria.items is NOT NULL and
          LP_I_search_criteria.items.COUNT > 0) and
         (LP_I_search_criteria.diff_ids is NOT NULL and
          LP_I_search_criteria.diff_ids.COUNT > 0) then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) CARDINALITY (diffs 10) */
                           distinct
                           im.item,
                           NULL item_parent,
                           VALUE(diffs) diff_id,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) tbl,
                           table(cast(LP_I_search_criteria.diff_ids as OBJ_VARCHAR_ID_TABLE)) diffs,
                           item_master im,
                           numeric_id_gtt num
                     where im.item = VALUE(tbl)
                       and EXISTS (select 1
                                     from item_master im1,
                                          item_loc il
                                    where LP_zone_search   = LP_LOCATION_LEVEL
                                      and im1.item_parent  = im.item
                                      and im1.item_level   = im1.tran_level
                                      and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im1.diff_1   = VALUE(diffs)
                                           or im1.diff_2   = VALUE(diffs)
                                           or im1.diff_3   = VALUE(diffs)
                                           or im1.diff_4   = VALUE(diffs))
                                      and il.item          = im1.item
                                      and il.loc           = num.numeric_id
                                   union all
                                   select 1
                                     from item_master im2,
                                          item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search   = LP_ZONE_LEVEL
                                      and rzl.zone_id      = num.numeric_id
                                      and im2.item_parent  = im.item
                                      and im2.item_level   = im2.tran_level
                                      and im2.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im2.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im2.diff_1   = VALUE(diffs)
                                           or im2.diff_2   = VALUE(diffs)
                                           or im2.diff_3   = VALUE(diffs)
                                           or im2.diff_4   = VALUE(diffs))
                                      and il.item          = im2.item
                                      and il.loc           = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;

      elsif (LP_I_search_criteria.items is NOT NULL and
             LP_I_search_criteria.items.COUNT > 0) and
            (LP_I_search_criteria.diff_ids is NULL or
             LP_I_search_criteria.diff_ids.COUNT = 0) then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) */
                           distinct
                           im.item,
                           im.item_parent,
                           df_ids.diff_id,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.items as OBJ_VARCHAR_ID_TABLE)) tbl,
                           item_master im,
                           diff_ids df_ids,
                           numeric_id_gtt num
                     where im.item          = VALUE(tbl)
                       and df_ids.diff_type = LP_I_search_criteria.diff_type
                       and EXISTS (select 1
                                     from item_master im1,
                                          item_loc il
                                    where LP_zone_search   = LP_LOCATION_LEVEL
                                      and im1.item_parent  = im.item
                                      and im1.item_level   = im1.tran_level
                                      and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im1.diff_1   = df_ids.diff_id
                                           or im1.diff_2   = df_ids.diff_id
                                           or im1.diff_3   = df_ids.diff_id
                                           or im1.diff_4   = df_ids.diff_id)
                                      and il.item          = im1.item
                                      and il.loc           = num.numeric_id
                                   union all
                                   select 1
                                     from item_master im2,
                                          item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search   = LP_ZONE_LEVEL
                                      and rzl.zone_id      = num.numeric_id
                                      and im2.item_parent  = im.item
                                      and im2.item_level   = im2.tran_level
                                      and im2.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im2.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im2.diff_1   = df_ids.diff_id
                                           or im2.diff_2   = df_ids.diff_id
                                           or im2.diff_3   = df_ids.diff_id
                                           or im2.diff_4   = df_ids.diff_id)
                                      and il.item          = im2.item
                                      and il.loc           = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;

      -- If no ITEMS have been specified in the GUI but DIFFS have been specified.
      elsif (LP_I_search_criteria.items is NULL or
             LP_I_search_criteria.items.COUNT = 0) and
            (LP_I_search_criteria.diff_ids is NOT NULL and
             LP_I_search_criteria.diff_ids.COUNT > 0) then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) CARDINALITY (diffs 10) */
                           distinct
                           im.item,
                           NULL item_parent,
                           VALUE(diffs) diff_id,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.diff_ids as OBJ_VARCHAR_ID_TABLE)) diffs,
                           table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                           item_master im,
                           numeric_id_gtt num
                     where im.dept         = tbl.dept
                       and im.class        = NVL(tbl.class, im.class)
                       and im.subclass     = NVL(tbl.subclass, im.subclass)
                       and im.tran_level   = im.item_level + 1
                       and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and EXISTS (select 1
                                     from item_master im1,
                                          item_loc il
                                    where LP_zone_search   = LP_LOCATION_LEVEL
                                      and im1.item_parent  = im.item
                                      and im1.item_level   = im1.tran_level
                                      and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im1.diff_1   = VALUE(diffs)
                                           or im1.diff_2   = VALUE(diffs)
                                           or im1.diff_3   = VALUE(diffs)
                                           or im1.diff_4   = VALUE(diffs))
                                      and il.item          = im1.item
                                      and il.loc           = num.numeric_id
                                   union all
                                   select 1
                                     from item_master im2,
                                          item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search   = LP_ZONE_LEVEL
                                      and rzl.zone_id      = num.numeric_id
                                      and im2.item_parent  = im.item
                                      and im2.item_level   = im2.tran_level
                                      and im2.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im2.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im2.diff_1   = VALUE(diffs)
                                           or im2.diff_2   = VALUE(diffs)
                                           or im2.diff_3   = VALUE(diffs)
                                           or im2.diff_4   = VALUE(diffs))
                                      and il.item          = im2.item
                                      and il.loc           = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;

      -- If no (ITEMS and DIFFS) have been specified in the GUI
      elsif (LP_I_search_criteria.items is NULL or
             LP_I_search_criteria.items.COUNT = 0) and
            (LP_I_search_criteria.diff_ids is NULL or
             LP_I_search_criteria.diff_ids.COUNT = 0) then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   location
              from (select /*+ CARDINALITY (tbl 10) */
                           distinct
                           im.item,
                           NULL item_parent,
                           df_ids.diff_id,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                           item_master im,
                           diff_ids df_ids,
                           numeric_id_gtt num
                     where im.dept          = tbl.dept
                       and im.class         = NVL(tbl.class, im.class)
                       and im.subclass      = NVL(tbl.subclass, im.subclass)
                       and im.tran_level    = im.item_level + 1
                       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and df_ids.diff_type = LP_I_search_criteria.diff_type
                       and EXISTS (select 1
                                     from item_master im1,
                                          item_loc il
                                    where LP_zone_search   = LP_LOCATION_LEVEL
                                      and im1.item_parent  = im.item
                                      and im1.item_level   = im1.tran_level
                                      and im1.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im1.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im1.diff_1   = df_ids.diff_id
                                           or im1.diff_2   = df_ids.diff_id
                                           or im1.diff_3   = df_ids.diff_id
                                           or im1.diff_4   = df_ids.diff_id)
                                      and il.item          = im1.item
                                      and il.loc           = num.numeric_id
                                   union all
                                   select 1
                                     from item_master im2,
                                          item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search   = LP_ZONE_LEVEL
                                      and rzl.zone_id      = num.numeric_id
                                      and im2.item_parent  = im.item
                                      and im2.item_level   = im2.tran_level
                                      and im2.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                      and im2.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                      and (   im2.diff_1   = df_ids.diff_id
                                           or im2.diff_2   = df_ids.diff_id
                                           or im2.diff_3   = df_ids.diff_id
                                           or im2.diff_4   = df_ids.diff_id)
                                      and il.item          = im2.item
                                      and il.loc           = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;
      end if;
   end if;

   -- Populate Item List data
   if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST then

      if LP_I_search_criteria.item_list_id is NOT NULL then

         if LP_I_search_criteria.dept_class_subclass IS NOT NULL and
            LP_I_search_criteria.dept_class_subclass.COUNT > 0 then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select rownum,
                      item,
                      item_parent,
                      diff_id,
                      dept,
                      class,
                      subclass,
                      location
                 from (select distinct
                              im.item,
                              im.item_parent,
                              im.diff_1 diff_id,
                              im.dept,
                              im.class,
                              im.subclass,
                              num.numeric_id location
                         from table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                              numeric_id_gtt num,
                              (select im1.item,
                                      im1.item_parent,
                                      DECODE(im1.item_parent,
                                             NULL, NULL,
                                             im1.diff_1) diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from item_master im1,
                                      skulist_detail im_list
                                where im_list.skulist    = LP_I_search_criteria.item_list_id
                                  and im_list.item_level = im_list.tran_level
                                  and im_list.item       = im1.item
                                  and im1.tran_level     = im1.item_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               union all
                               select im2.item,
                                      im2.item_parent,
                                      im2.diff_1,
                                      im2.dept,
                                      im2.class,
                                      im2.subclass
                                 from item_master im2,
                                      skulist_detail im_list
                                where im_list.skulist    = LP_I_search_criteria.item_list_id
                                  and im_list.item_level = im_list.tran_level - 1
                                  and im_list.item       = im2.item_parent
                                  and im2.tran_level     = im2.item_level
                                  and im2.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im2.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) im
                        where im.dept     = NVL(tbl.dept, im.dept)
                          and im.class    = NVL(tbl.class, im.class)
                          and im.subclass = NVL(tbl.subclass, im.subclass)
                          and EXISTS (select 1
                                        from item_loc il
                                       where LP_zone_search = LP_LOCATION_LEVEL
                                         and il.item        = im.item
                                         and il.loc         = num.numeric_id
                                      union all
                                      select 1
                                        from item_loc il,
                                             rpm_zone_location rzl
                                       where LP_zone_search = LP_ZONE_LEVEL
                                         and rzl.zone_id    = num.numeric_id
                                         and il.item        = im.item
                                         and il.loc         = rzl.location))
                where rownum <= LP_I_search_criteria.maximum_result;

         else -- dept/class/subclass not provided

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select rownum,
                      item,
                      item_parent,
                      diff_id,
                      dept,
                      class,
                      subclass,
                      location
                 from (select distinct
                              im.item,
                              im.item_parent,
                              im.diff_1 diff_id,
                              im.dept,
                              im.class,
                              im.subclass,
                              num.numeric_id location
                         from (select im1.item,
                                      im1.item_parent,
                                      DECODE(im1.item_parent,
                                             NULL, NULL,
                                             im1.diff_1) diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from item_master im1,
                                      skulist_detail im_list
                                where im_list.skulist    = LP_I_search_criteria.item_list_id
                                  and im_list.item_level = im_list.tran_level
                                  and im_list.item       = im1.item
                                  and im1.tran_level     = im1.item_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               union all
                               select im2.item,
                                      im2.item_parent,
                                      im2.diff_1,
                                      im2.dept,
                                      im2.class,
                                      im2.subclass
                                 from item_master im2,
                                      skulist_detail im_list
                                where im_list.skulist    = LP_I_search_criteria.item_list_id
                                  and im_list.item_level = im_list.tran_level - 1
                                  and im_list.item       = im2.item_parent
                                  and im2.tran_level     = im2.item_level
                                  and im2.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im2.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) im,
                              numeric_id_gtt num,
                              (select sc.dept dept,
                                      sc.class class,
                                      sc.subclass subclass
                                 from rsm_hierarchy_permission rhp,
                                      rsm_role_hierarchy_perm rrhp,
                                      rsm_hierarchy_type rht,
                                      rsm_user_role rur,
                                      deps d,
                                      subclass sc
                                where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_DEPT
                                  and rhp.id                 = rrhp.parent_id
                                  and rrhp.hierarchy_type_id = rht.id
                                  and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                                  and rrhp.role_id           = rur.role_id
                                  and rur.user_id            = LP_I_search_criteria.user_id
                                  and d.dept                 = TO_NUMBER(rhp.key_value)
                                  and sc.dept                = d.dept
                               union
                               select sc.dept dept,
                                      sc.class class,
                                      sc.subclass subclass
                                 from rsm_hierarchy_permission rhp,
                                      rsm_role_hierarchy_perm rrhp,
                                      rsm_hierarchy_type rht,
                                      rsm_user_role rur,
                                      class c,
                                      subclass sc
                                where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_CLASS
                                  and rhp.id                 = rrhp.parent_id
                                  and rrhp.hierarchy_type_id = rht.id
                                  and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                                  and rrhp.role_id           = rur.role_id
                                  and rur.user_id            = LP_I_search_criteria.user_id
                                  and TO_CHAR(c.class)       = rhp.key_value
                                  and sc.dept                = c.dept
                                  and sc.class               = c.class
                               union
                               select sc.dept dept,
                                      sc.class class,
                                      sc.subclass subclass
                                 from rsm_hierarchy_permission rhp,
                                      rsm_role_hierarchy_perm rrhp,
                                      rsm_hierarchy_type rht,
                                      rsm_user_role rur,
                                      class c,
                                      subclass sc
                                where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS
                                  and rhp.id                 = rrhp.parent_id
                                  and rrhp.hierarchy_type_id = rht.id
                                  and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                                  and rrhp.role_id           = rur.role_id
                                  and rur.user_id            = LP_I_search_criteria.user_id
                                  and TO_CHAR(sc.subclass)   = rhp.key_value
                                  and sc.dept                = c.dept
                                  and sc.class               = c.class) sec_merch_hier
                        where im.dept     = NVL(sec_merch_hier.dept, im.dept)
                          and im.class    = NVL(sec_merch_hier.class, im.class)
                          and im.subclass = NVL(sec_merch_hier.subclass, im.subclass)
                          and EXISTS (select 1
                                        from item_loc il
                                       where LP_zone_search = LP_LOCATION_LEVEL
                                         and il.item        = im.item
                                         and il.loc         = num.numeric_id
                                      union all
                                      select 1
                                        from item_loc il,
                                             rpm_zone_location rzl
                                       where LP_zone_search = LP_ZONE_LEVEL
                                         and rzl.zone_id    = num.numeric_id
                                         and il.item        = im.item
                                         and il.loc         = rzl.location))
                where rownum <= LP_I_search_criteria.maximum_result;

         end if;

      -- If no ITEM_LIST_ID has been specified in the GUI, get all the ITEMS from the DEPT(s) specified.
      elsif LP_I_search_criteria.item_list_id is NULL then

         open C_ALL_ILIST;
         fetch C_ALL_ILIST BULK COLLECT into L_all_itemlist;
         close C_ALL_ILIST;

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   item,
                   item_parent,
                   diff_id,
                   dept,
                   class,
                   subclass,
                   location
              from (select distinct
                           im.item,
                           im.item_parent,
                           im.diff_1 diff_id,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from (select im1.item,
                                      im1.item_parent,
                                      DECODE(im1.item_parent,
                                             NULL, NULL,
                                             im1.diff_1) diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from table(cast(L_all_itemlist as OBJ_NUMERIC_ID_TABLE )) ids,
                                      item_master im1,
                                      skulist_detail im_list
                                where im_list.skulist    = VALUE(ids)
                                  and im_list.item_level = im_list.tran_level
                                  and im_list.item       = im1.item
                                  and im1.tran_level     = im1.item_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               union all
                               select im2.item,
                                      im2.item_parent,
                                      im2.diff_1,
                                      im2.dept,
                                      im2.class,
                                      im2.subclass
                                 from table(cast(L_all_itemlist as OBJ_NUMERIC_ID_TABLE )) ids,
                                      item_master im2,
                                      skulist_detail im_list
                                where im_list.skulist    = VALUE(ids)
                                  and im_list.item_level = im_list.tran_level - 1
                                  and im_list.item       = im2.item_parent
                                  and im2.tran_level     = im2.item_level
                                  and im2.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im2.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) im,
                           numeric_id_gtt num
                     where EXISTS (select 1
                                     from item_loc il
                                    where LP_zone_search = LP_LOCATION_LEVEL
                                      and il.item        = im.item
                                      and il.loc         = num.numeric_id
                                   union all
                                   select 1
                                     from item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search = LP_ZONE_LEVEL
                                      and rzl.zone_id    = num.numeric_id
                                      and il.item        = im.item
                                      and il.loc         = rzl.location))
             where rownum <= LP_I_search_criteria.maximum_result;
      end if;
   end if;

   -- Populate Price Event ItemList (PEIL) data
   if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_PRICE_EVENT_ITEMLIST then
      if LP_I_search_criteria.price_event_item_list_id is NOT NULL then
         if LP_I_search_criteria.dept_class_subclass IS NOT NULL and
            LP_I_search_criteria.dept_class_subclass.COUNT > 0 then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select rownum,
                      item,
                      item_parent,
                      diff_id,
                      dept,
                      class,
                      subclass,
                      location
                 from (select distinct
                              im.item,
                              im.item_parent,
                              im.diff_1 diff_id,
                              im.dept,
                              im.class,
                              im.subclass,
                              num.numeric_id location
                         from table(cast(LP_I_search_criteria.dept_class_subclass as OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                              numeric_id_gtt num,
                              (select im1.item,
                                      im1.item_parent,
                                      im1.diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from item_master im1,
                                      rpm_merch_list_detail rmld
                                where rmld.merch_list_id = LP_I_search_criteria.price_event_item_list_id
                                  and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                  and rmld.item          = im1.item_parent
                                  and im1.tran_level     = im1.item_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               union all
                               select im1.item,
                                      im1.item_parent,
                                      DECODE(im1.item_parent,
                                             NULL, NULL,
                                             im1.diff_1) diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from item_master im1,
                                      rpm_merch_list_detail rmld
                                where rmld.merch_list_id = LP_I_search_criteria.price_event_item_list_id
                                  and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                  and rmld.item          = im1.item
                                  and im1.tran_level     = im1.item_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) im
                        where im.dept     = NVL(tbl.dept, im.dept)
                          and im.class    = NVL(tbl.class, im.class)
                          and im.subclass = NVL(tbl.subclass, im.subclass)
                          and EXISTS (select 1
                                        from item_loc il
                                       where LP_zone_search = LP_LOCATION_LEVEL
                                         and il.item        = im.item
                                         and il.loc         = num.numeric_id
                                      union all
                                      select 1
                                        from item_loc il,
                                             rpm_zone_location rzl
                                       where LP_zone_search = LP_ZONE_LEVEL
                                         and rzl.zone_id    = num.numeric_id
                                         and il.item        = im.item
                                         and il.loc         = rzl.location))
                where rownum <= LP_I_search_criteria.maximum_result;

         else -- dept/class/subclass not provided
            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select rownum,
                      item,
                      item_parent,
                      diff_id,
                      dept,
                      class,
                      subclass,
                      location
                 from (select distinct
                              im.item,
                              im.item_parent,
                              im.diff_1 diff_id,
                              im.dept,
                              im.class,
                              im.subclass,
                              num.numeric_id location
                         from (select im1.item,
                                      im1.item_parent,
                                      im1.diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from item_master im1,
                                      rpm_merch_list_detail rmld
                                where rmld.merch_list_id = LP_I_search_criteria.price_event_item_list_id
                                  and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                  and rmld.item          = im1.item_parent
                                  and im1.item_level     = im1.tran_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               union all
                               select im1.item,
                                      im1.item_parent,
                                      DECODE(im1.item_parent,
                                             NULL, NULL,
                                             im1.diff_1) diff_1,
                                      im1.dept,
                                      im1.class,
                                      im1.subclass
                                 from item_master im1,
                                      rpm_merch_list_detail rmld
                                where rmld.merch_list_id = LP_I_search_criteria.price_event_item_list_id
                                  and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                  and rmld.item          = im1.item
                                  and im1.item_level     = im1.tran_level
                                  and im1.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                  and im1.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) im,
                              numeric_id_gtt num,
                              (select sc.dept dept,
                                      sc.class class,
                                      sc.subclass subclass
                                 from rsm_hierarchy_permission rhp,
                                      rsm_role_hierarchy_perm rrhp,
                                      rsm_hierarchy_type rht,
                                      rsm_user_role rur,
                                      deps d,
                                      subclass sc
                                where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_DEPT
                                  and rhp.id                 = rrhp.parent_id
                                  and rrhp.hierarchy_type_id = rht.id
                                  and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                                  and rrhp.role_id           = rur.role_id
                                  and rur.user_id            = LP_I_search_criteria.user_id
                                  and d.dept                 = TO_NUMBER(rhp.key_value)
                                  and sc.dept                = d.dept
                               union
                               select sc.dept dept,
                                      sc.class class,
                                      sc.subclass subclass
                                 from rsm_hierarchy_permission rhp,
                                      rsm_role_hierarchy_perm rrhp,
                                      rsm_hierarchy_type rht,
                                      rsm_user_role rur,
                                      class c,
                                      subclass sc
                                where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_CLASS
                                  and rhp.id                 = rrhp.parent_id
                                  and rrhp.hierarchy_type_id = rht.id
                                  and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                                  and rrhp.role_id           = rur.role_id
                                  and rur.user_id            = LP_I_search_criteria.user_id
                                  and TO_CHAR(c.class)       = rhp.key_value
                                  and sc.dept                = c.dept
                                  and sc.class               = c.class
                               union
                               select sc.dept dept,
                                      sc.class class,
                                      sc.subclass subclass
                                 from rsm_hierarchy_permission rhp,
                                      rsm_role_hierarchy_perm rrhp,
                                      rsm_hierarchy_type rht,
                                      rsm_user_role rur,
                                      class c,
                                      subclass sc
                                where rhp.reference_class    = RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS
                                  and rhp.id                 = rrhp.parent_id
                                  and rrhp.hierarchy_type_id = rht.id
                                  and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
                                  and rrhp.role_id           = rur.role_id
                                  and rur.user_id            = LP_I_search_criteria.user_id
                                  and TO_CHAR(sc.subclass)   = rhp.key_value
                                  and sc.dept                = c.dept
                                  and sc.class               = c.class) sec_merch_hier
                        where im.dept     = NVL(sec_merch_hier.dept, im.dept)
                          and im.class    = NVL(sec_merch_hier.class, im.class)
                          and im.subclass = NVL(sec_merch_hier.subclass, im.subclass)
                          and EXISTS (select 1
                                        from item_loc il
                                       where LP_zone_search = LP_LOCATION_LEVEL
                                         and il.item        = im.item
                                         and il.loc         = num.numeric_id
                                      union all
                                      select 1
                                        from item_loc il,
                                             rpm_zone_location rzl
                                       where LP_zone_search = LP_ZONE_LEVEL
                                         and rzl.zone_id    = num.numeric_id
                                         and il.item        = im.item
                                         and il.loc         = rzl.location))
                where rownum <= LP_I_search_criteria.maximum_result;

         end if;

      -- If no PRICE_EVENT_ITEMLIST has been specified in the GUI, get all the PEIL from the DEPT(s) specified.
      elsif LP_I_search_criteria.price_event_item_list_id is NULL then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select rownum,
                   peil.item,
                   peil.item_parent,
                   peil.diff_id,
                   peil.dept,
                   peil.class,
                   peil.subclass,
                   peil.location
              from (select distinct
                           im.item item,
                           im.item_parent,
                           im.diff_1 diff_id,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.dept_class_subclass AS OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                           item_master im,
                           rpm_merch_list_detail rmld,
                           numeric_id_gtt num
                     where im.dept          = NVL(tbl.dept,im.dept)
                       and im.class         = NVL(tbl.class,im.class)
                       and im.subclass      = NVL(tbl.subclass,im.subclass)
                       and rmld.item        = im.item_parent
                       and rmld.merch_level = RPM_CONSTANTS.PARENT_MERCH_TYPE
                       and im.item_level    = im.tran_level
                       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and EXISTS (select 1
                                     from item_loc il
                                    where LP_zone_search = LP_LOCATION_LEVEL
                                      and il.item        = im.item
                                      and il.loc         = num.numeric_id
                                   union all
                                   select 1
                                     from item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search = LP_ZONE_LEVEL
                                      and rzl.zone_id    = num.numeric_id
                                      and il.item        = im.item
                                      and il.loc         = rzl.location)
                    union all
                    select distinct
                           im.item item,
                           im.item_parent,
                           DECODE(im.item_parent,
                                  NULL, NULL,
                                  im.diff_1) diff_1,
                           im.dept,
                           im.class,
                           im.subclass,
                           num.numeric_id location
                      from table(cast(LP_I_search_criteria.dept_class_subclass AS OBJ_DEPT_CLASS_SUBCLASS_TBL)) tbl,
                           item_master im,
                           rpm_merch_list_detail rmld,
                           numeric_id_gtt num
                     where im.dept          = NVL(tbl.dept, im.dept)
                       and im.class         = NVL(tbl.class, im.class)
                       and im.subclass      = NVL(tbl.subclass, im.subclass)
                       and rmld.item        = im.item
                       and rmld.merch_level = RPM_CONSTANTS.ITEM_MERCH_TYPE
                       and im.item_level    = im.tran_level
                       and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                       and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                       and EXISTS (select 1
                                     from item_loc il
                                    where LP_zone_search = LP_LOCATION_LEVEL
                                      and il.item        = im.item
                                      and il.loc         = num.numeric_id
                                   union all
                                   select 1
                                     from item_loc il,
                                          rpm_zone_location rzl
                                    where LP_zone_search = LP_ZONE_LEVEL
                                      and rzl.zone_id    = num.numeric_id
                                      and il.item        = im.item
                                      and il.loc         = rzl.location)) peil
             where rownum <= LP_I_search_criteria.maximum_result;
      end if;
   end if;

   O_return_code := 1;

EXCEPTION

   when OTHERS then

      O_return_code := 0;
      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));

END POP_GTT;
--------------------------------------------------------
PROCEDURE EXPLODE_ITEM_LOC (O_return_code    OUT NUMBER,
                            O_error_msg      OUT VARCHAR2)
AS

   L_program  VARCHAR2(40) := 'RPM_PRICE_INQUIRY_SQL.EXPLODE_ITEM_LOC';

BEGIN

   -- Location Level
   if LP_zone_search = LP_LOCATION_LEVEL then

      if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM then
      -- ITEM level is item.
         if LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select pi.price_event_id * -1,
                      pi.item,
                      pi.item_parent,
                      pi.diff_id,
                      pi.dept,
                      pi.class,
                      pi.subclass,
                      pi.location
                 from rpm_price_inquiry_gtt pi,
                      rpm_item_loc ril
                where pi.price_event_id > 0
                  and ril.dept          = pi.dept
                  and ril.item          = pi.item
                  and ril.loc           = pi.location;

         elsif LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select pi.price_event_id * -1,
                      im.item,
                      im.item_parent,
                      im.diff_1,
                      pi.dept,
                      pi.class,
                      pi.subclass,
                      pi.location
                 from rpm_price_inquiry_gtt pi,
                      item_master im,
                      rpm_item_loc ril
                where pi.price_event_id > 0
                  and im.item_parent    = pi.item
                  and im.item_level     = im.tran_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and ril.dept          = im.dept
                  and ril.item          = im.item
                  and ril.loc           = pi.location;

         elsif LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select pi.price_event_id * -1,
                      im.item,
                      im.item_parent,
                      im.diff_1,
                      pi.dept,
                      pi.class,
                      pi.subclass,
                      pi.location
                 from rpm_price_inquiry_gtt pi,
                      item_master im,
                      rpm_item_loc ril
                where pi.price_event_id  > 0
                  and im.item_parent     = pi.item
                  and im.item_level      = im.tran_level
                  and (   im.diff_1      = pi.diff_id
                       or im.diff_2      = pi.diff_id
                       or im.diff_3      = pi.diff_id
                       or im.diff_4      = pi.diff_id)
                  and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and ril.dept           = im.dept
                  and ril.item           = im.item
                  and ril.loc            = pi.location;

         end if;

      -- Item level is ITEM LIST or PRICE EVENT ITEMLIST (PEIL) level.
      elsif LP_I_search_criteria.item_level IN (RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST,
                                                RPM_CONSTANTS.PC_SRCH_PRICE_EVENT_ITEMLIST) then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select pi.price_event_id * -1,
                   pi.item,
                   pi.item_parent,
                   pi.diff_id,
                   pi.dept,
                   pi.class,
                   pi.subclass,
                   pi.location
              from rpm_price_inquiry_gtt pi,
                   rpm_item_loc ril
             where pi.price_event_id > 0
               and ril.dept          = pi.dept
               and ril.item          = pi.item
               and ril.loc           = pi.location;

      end if;

   else

      -- Zone Level
      if LP_I_search_criteria.item_level = RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM then
      -- ITEM level is item.
         if LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select pi.price_event_id * -1,
                      pi.item,
                      pi.item_parent,
                      pi.diff_id,
                      pi.dept,
                      pi.class,
                      pi.subclass,
                      input.location
                 from rpm_price_inquiry_gtt pi,
                      rpm_zone_location input,
                      rpm_item_loc ril
                where pi.price_event_id > 0
                  and input.zone_id     = pi.location
                  and ril.dept          = pi.dept
                  and ril.item          = pi.item
                  and ril.loc           = input.location;

         elsif LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select pi.price_event_id * -1,
                      im.item,
                      im.item_parent,
                      im.diff_1,
                      pi.dept,
                      pi.class,
                      pi.subclass,
                      input.location
                 from rpm_price_inquiry_gtt pi,
                      rpm_zone_location input,
                      item_master im,
                      rpm_item_loc ril
                where pi.price_event_id > 0
                  and pi.item           = im.item_parent
                  and im.tran_level     = im.item_level
                  and im.status         = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind   = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and input.zone_id     = pi.location
                  and ril.dept          = im.dept
                  and ril.item          = im.item
                  and ril.loc           = input.location;

         elsif LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF then

            insert into rpm_price_inquiry_gtt(price_event_id,
                                              item,
                                              item_parent,
                                              diff_id,
                                              dept,
                                              class,
                                              subclass,
                                              location)
               select pi.price_event_id * -1,
                      im.item,
                      im.item_parent,
                      im.diff_1,
                      pi.dept,
                      pi.class,
                      pi.subclass,
                      input.location
                 from rpm_price_inquiry_gtt pi,
                      rpm_zone_location input,
                      item_master im,
                      rpm_item_loc ril
                where pi.price_event_id > 0
                  and pi.item         = im.item_parent
                  and im.tran_level   = im.item_level
                  and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                  and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                  and (   im.diff_1   = pi.diff_id
                       or im.diff_2   = pi.diff_id
                       or im.diff_3   = pi.diff_id
                       or im.diff_4   = pi.diff_id)
                  and input.zone_id   = pi.location
                  and ril.dept        = im.dept
                  and ril.item        = im.item
                  and ril.loc         = input.location;

         end if;

      -- Item level is ITEM LIST or PRICE EVENT ITEMLIST (PEIL) level.
      elsif LP_I_search_criteria.item_level IN (RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST,
                                                RPM_CONSTANTS.PC_SRCH_PRICE_EVENT_ITEMLIST) then

         insert into rpm_price_inquiry_gtt(price_event_id,
                                           item,
                                           item_parent,
                                           diff_id,
                                           dept,
                                           class,
                                           subclass,
                                           location)
            select pi.price_event_id * -1,
                   pi.item,
                   pi.item_parent,
                   pi.diff_id,
                   pi.dept,
                   pi.class,
                   pi.subclass,
                   input.location
              from rpm_price_inquiry_gtt pi,
                   rpm_zone_location input,
                   rpm_item_loc ril
             where pi.price_event_id > 0
               and input.zone_id     = pi.location
               and ril.dept          = pi.dept
               and ril.item          = pi.item
               and ril.loc           = input.location;

      end if;

   end if;

   merge into rpm_price_inquiry_gtt target
   using (select pi.price_event_id,
                 pi.item,
                 pi.location,
                 rzl.loc_type,
                 rzl.zone_id
            from rpm_price_inquiry_gtt pi,
                 rpm_merch_retail_def_expl mrd,
                 rpm_zone rz,
                 rpm_zone_location rzl
           where pi.price_event_id      < 0
             and pi.dept                = mrd.dept
             and pi.class               = mrd.class
             and pi.subclass            = mrd.subclass
             and mrd.regular_zone_group = rz.zone_group_id
             and rz.zone_id             = rzl.zone_id
             and rzl.location           = pi.location) use_this
   on (    target.price_event_id  = use_this.price_event_id
       and target.item            = use_this.item
       and target.location        = use_this.location)
   when MATCHED then
      update
         set target.fr_zone_node_type = use_this.loc_type,
             target.fr_location       = use_this.location,
             target.fr_zone_id        = use_this.zone_id,
             target.loc_type          = use_this.loc_type;

   --get the locs not in the zone structure
   merge into rpm_price_inquiry_gtt target
   using (select pi.price_event_id,
                 pi.item,
                 pi.location,
                 loc.loc_type,
                 loc.loc
            from rpm_price_inquiry_gtt pi,
                 (select store loc,
                         RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loc_type
                    from store
                  union all
                  select wh loc,
                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loc_type
                    from wh) loc
           where pi.price_event_id < 0
             and pi.fr_location    is NULL
             and pi.location       = loc.loc) use_this
   on (    target.price_event_id  = use_this.price_event_id
       and target.item            = use_this.item
       and target.location        = use_this.location)
   when MATCHED then
      update
         set target.fr_zone_node_type = use_this.loc_type,
             target.fr_location       = use_this.loc,
             target.fr_zone_id        = NULL,
             target.loc_type          = use_this.loc_type;

   O_return_code := 1;

   return;

EXCEPTION
   when OTHERS then

      O_return_code := 0;
      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));

END EXPLODE_ITEM_LOC;
--------------------------------------------------------

PROCEDURE POP_FUTURE_RETAIL_GTT (O_return_code    OUT NUMBER,
                                 O_error_msg      OUT VARCHAR2) AS

   L_program  VARCHAR2(50) := 'RPM_PRICE_INQUIRY_SQL.POP_FUTURE_RETAIL_GTT';

   L_return_code NUMBER        := 0;
   L_error_msg   VARCHAR2(200) := NULL;

   cursor C_PRIMARY_ZONE is
      select distinct
             il.price_event_id,
             rz.zone_id
        from rpm_price_inquiry_gtt il,
             rpm_zone rz,
             rpm_merch_retail_def_expl rmd
       where rz.zone_id                     = il.location
         and il.price_event_id              > 0
         and rmd.dept                       = il.dept
         and NVL(rmd.class, il.class)       = il.class
         and NVL(rmd.subclass, il.subclass) = il.subclass
         and rmd.regular_zone_group         = rz.zone_group_id;

BEGIN

   delete
     from rpm_future_retail_gtt;

   delete
     from rpm_promo_item_loc_expl_gtt;

   -- Explode Item Location
   EXPLODE_ITEM_LOC(L_return_code,
                    L_error_msg);

   if L_return_code = 0 then
      O_return_code := L_return_code;
      O_error_msg   := L_error_msg;
      return;
   end if;

   -- Populate Future Retail GTT

   insert into rpm_future_retail_gtt(price_event_id,
                                     future_retail_id,
                                     item,
                                     dept,
                                     class,
                                     subclass,
                                     zone_node_type,
                                     location,
                                     action_date,
                                     selling_retail,
                                     selling_retail_currency,
                                     selling_uom,
                                     multi_units,
                                     multi_unit_retail,
                                     multi_unit_retail_currency,
                                     multi_selling_uom,
                                     clear_retail,
                                     clear_retail_currency,
                                     clear_uom,
                                     simple_promo_retail,
                                     simple_promo_retail_currency,
                                     simple_promo_uom,
                                     on_simple_promo_ind,
                                     on_complex_promo_ind,
                                     price_change_id,
                                     price_change_display_id,
                                     pc_exception_parent_id,
                                     pc_change_type,
                                     pc_change_amount,
                                     pc_change_currency,
                                     pc_change_percent,
                                     pc_change_selling_uom,
                                     pc_null_multi_ind,
                                     pc_multi_units,
                                     pc_multi_unit_retail,
                                     pc_multi_unit_retail_currency,
                                     pc_multi_selling_uom,
                                     pc_price_guide_id,
                                     clearance_id,
                                     clearance_display_id,
                                     clear_mkdn_index,
                                     clear_start_ind,
                                     clear_change_type,
                                     clear_change_amount,
                                     clear_change_currency,
                                     clear_change_percent,
                                     clear_change_selling_uom,
                                     clear_price_guide_id,
                                     loc_move_from_zone_id,
                                     loc_move_to_zone_id,
                                     location_move_id,
                                     item_parent,
                                     diff_id,
                                     zone_id,
                                     max_hier_level,
                                     cur_hier_level,
                                     lock_version)
      select in2.price_event_id * -1,
             in2.future_retail_id,
             in2.il_item,
             in2.dept,
             in2.class,
             in2.subclass,
             in2.il_loc_type,
             in2.il_loc,
             in2.action_date,
             in2.selling_retail,
             in2.selling_retail_currency,
             in2.selling_uom,
             in2.multi_units,
             in2.multi_unit_retail,
             in2.multi_unit_retail_currency,
             in2.multi_selling_uom,
             in2.clear_retail,
             in2.clear_retail_currency,
             in2.clear_uom,
             in2.simple_promo_retail,
             in2.simple_promo_retail_currency,
             in2.simple_promo_uom,
             in2.on_simple_promo_ind,
             in2.on_complex_promo_ind,
             in2.price_change_id,
             in2.price_change_display_id,
             in2.pc_exception_parent_id,
             in2.pc_change_type,
             in2.pc_change_amount,
             in2.pc_change_currency,
             in2.pc_change_percent,
             in2.pc_change_selling_uom,
             in2.pc_null_multi_ind,
             in2.pc_multi_units,
             in2.pc_multi_unit_retail,
             in2.pc_multi_unit_retail_currency,
             in2.pc_multi_selling_uom,
             in2.pc_price_guide_id,
             in2.clearance_id,
             in2.clearance_display_id,
             in2.clear_mkdn_index,
             in2.clear_start_ind,
             in2.clear_change_type,
             in2.clear_change_amount,
             in2.clear_change_currency,
             in2.clear_change_percent,
             in2.clear_change_selling_uom,
             in2.clear_price_guide_id,
             in2.loc_move_from_zone_id,
             in2.loc_move_to_zone_id,
             in2.location_move_id,
             in2.il_item_parent,
             in2.il_diff_id,
             in2.fr_zone_id,
             in2.max_hier_level,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC,
             in2.lock_version
        from (select in1.future_retail_id,
                     in1.item,
                     in1.dept,
                     in1.class,
                     in1.subclass,
                     in1.zone_node_type,
                     in1.location,
                     in1.action_date,
                     in1.selling_retail,
                     in1.selling_retail_currency,
                     in1.selling_uom,
                     in1.multi_units,
                     in1.multi_unit_retail,
                     in1.multi_unit_retail_currency,
                     in1.multi_selling_uom,
                     in1.clear_exception_parent_id,
                     in1.clear_retail,
                     in1.clear_retail_currency,
                     in1.clear_uom,
                     in1.simple_promo_retail,
                     in1.simple_promo_retail_currency,
                     in1.simple_promo_uom,
                     in1.on_simple_promo_ind,
                     in1.on_complex_promo_ind,
                     in1.price_change_id,
                     in1.price_change_display_id,
                     in1.pc_exception_parent_id,
                     in1.pc_change_type,
                     in1.pc_change_amount,
                     in1.pc_change_currency,
                     in1.pc_change_percent,
                     in1.pc_change_selling_uom,
                     in1.pc_null_multi_ind,
                     in1.pc_multi_units,
                     in1.pc_multi_unit_retail,
                     in1.pc_multi_unit_retail_currency,
                     in1.pc_multi_selling_uom,
                     in1.pc_price_guide_id,
                     in1.clearance_id,
                     in1.clearance_display_id,
                     in1.clear_mkdn_index,
                     in1.clear_start_ind,
                     in1.clear_change_type,
                     in1.clear_change_amount,
                     in1.clear_change_currency,
                     in1.clear_change_percent,
                     in1.clear_change_selling_uom,
                     in1.clear_price_guide_id,
                     in1.loc_move_from_zone_id,
                     in1.loc_move_to_zone_id,
                     in1.location_move_id,
                     in1.lock_version,
                     in1.item_parent,
                     in1.diff_id,
                     in1.max_hier_level,
                     in1.cur_hier_level,
                     in1.fr_zone_id,
                     in1.price_event_id,
                     in1.il_item,
                     in1.il_item_parent,
                     in1.il_diff_id,
                     in1.il_loc,
                     in1.il_loc_type,
                     in1.date_rank,
                     in1.hier_rank,
                     MAX(in1.hier_rank) OVER (PARTITION BY in1.il_item,
                                                           in1.il_loc) max_rank
                from (select /*+ INDEX(f RPM_FUTURE_RETAIL_I1) */
                             f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.zone_id fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.loc_type il_loc_type,
                             RANK() OVER (PARTITION BY il.price_event_id,
                                                       f.item,
                                                       f.location
                                              ORDER BY f.action_date desc) date_rank,
                             3 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail f
                       where il.price_event_id     <  0
                         and f.dept                = il.dept
                         and f.item                = il.item
                         and NVL(f.diff_id, -9999) = NVL(il.diff_id, -9999)
                         and f.location            = il.fr_location
                         and f.zone_node_type      = il.fr_zone_node_type
                         and f.action_date        <= LP_I_search_criteria.effective_date
                         and f.cur_hier_level      = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                      union all
                      select /*+ INDEX(f RPM_FUTURE_RETAIL_I1) */
                             f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.location fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.loc_type il_loc_type,
                             RANK() OVER (PARTITION BY il.price_event_id,
                                                       f.item,
                                                       f.location
                                              ORDER BY f.action_date desc) date_rank,
                             2 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail f
                       where il.price_event_id     <  0
                         and f.dept                = il.dept
                         and f.item                = il.item
                         and NVL(f.diff_id, -9999) = NVL(il.diff_id, -9999)
                         and f.location            = il.fr_zone_id
                         and f.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and f.action_date        <= LP_I_search_criteria.effective_date
                         and f.cur_hier_level      = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                      union all
                      select /*+ INDEX(f RPM_FUTURE_RETAIL_I1) */
                             f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.zone_id fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.loc_type il_loc_type,
                             RANK() OVER (PARTITION BY il.price_event_id,
                                                       f.item,
                                                       f.location
                                              ORDER BY f.action_date desc) date_rank,
                             2 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail f
                       where il.price_event_id    <  0
                         and f.dept                = il.dept
                         and f.item                = il.item_parent
                         and f.diff_id             = il.diff_id
                         and f.location            = il.fr_location
                         and f.zone_node_type      = il.fr_zone_node_type
                         and f.action_date        <= LP_I_search_criteria.effective_date
                         and f.cur_hier_level      = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                      union all
                      select /*+ INDEX(f RPM_FUTURE_RETAIL_I1) */
                             f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.location fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.loc_type il_loc_type,
                             RANK() OVER (PARTITION BY il.price_event_id,
                                                       f.item,
                                                       f.location
                                              ORDER BY f.action_date desc) date_rank,
                             1 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail f
                       where il.price_event_id     <  0
                         and f.dept                = il.dept
                         and f.item                = il.item_parent
                         and f.diff_id             = il.diff_id
                         and f.location            = il.fr_zone_id
                         and f.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and f.action_date        <= LP_I_search_criteria.effective_date
                         and f.cur_hier_level      = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                      union all
                      select /*+ INDEX(f RPM_FUTURE_RETAIL_I1) */
                             f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.zone_id fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.loc_type il_loc_type,
                             RANK() OVER (PARTITION BY il.price_event_id,
                                                       f.item,
                                                       f.location
                                              ORDER BY f.action_date desc) date_rank,
                             1 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail f
                       where il.price_event_id     <  0
                         and f.dept                = il.dept
                         and f.item                = il.item_parent
                         and f.diff_id             is NULL
                         and f.location            = il.fr_location
                         and f.zone_node_type      = il.fr_zone_node_type
                         and f.action_date        <= LP_I_search_criteria.effective_date
                         and f.cur_hier_level      = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                      union all
                      select /*+ INDEX(f RPM_FUTURE_RETAIL_I1) */
                             f.future_retail_id,
                             f.item,
                             f.dept,
                             f.class,
                             f.subclass,
                             f.zone_node_type,
                             f.location,
                             f.action_date,
                             f.selling_retail,
                             f.selling_retail_currency,
                             f.selling_uom,
                             f.multi_units,
                             f.multi_unit_retail,
                             f.multi_unit_retail_currency,
                             f.multi_selling_uom,
                             f.clear_exception_parent_id,
                             f.clear_retail,
                             f.clear_retail_currency,
                             f.clear_uom,
                             f.simple_promo_retail,
                             f.simple_promo_retail_currency,
                             f.simple_promo_uom,
                             f.on_simple_promo_ind,
                             f.on_complex_promo_ind,
                             f.price_change_id,
                             f.price_change_display_id,
                             f.pc_exception_parent_id,
                             f.pc_change_type,
                             f.pc_change_amount,
                             f.pc_change_currency,
                             f.pc_change_percent,
                             f.pc_change_selling_uom,
                             f.pc_null_multi_ind,
                             f.pc_multi_units,
                             f.pc_multi_unit_retail,
                             f.pc_multi_unit_retail_currency,
                             f.pc_multi_selling_uom,
                             f.pc_price_guide_id,
                             f.clearance_id,
                             f.clearance_display_id,
                             f.clear_mkdn_index,
                             f.clear_start_ind,
                             f.clear_change_type,
                             f.clear_change_amount,
                             f.clear_change_currency,
                             f.clear_change_percent,
                             f.clear_change_selling_uom,
                             f.clear_price_guide_id,
                             f.loc_move_from_zone_id,
                             f.loc_move_to_zone_id,
                             f.location_move_id,
                             f.lock_version,
                             f.item_parent,
                             f.diff_id,
                             f.max_hier_level,
                             f.cur_hier_level,
                             f.location fr_zone_id,
                             il.price_event_id,
                             il.item il_item,
                             il.item_parent il_item_parent,
                             il.diff_id il_diff_id,
                             il.location il_loc,
                             il.loc_type il_loc_type,
                             RANK() OVER (PARTITION BY il.price_event_id,
                                                       f.item,
                                                       f.location
                                              ORDER BY f.action_date desc) date_rank,
                             0 hier_rank
                        from rpm_price_inquiry_gtt il,
                             rpm_future_retail f
                       where il.price_event_id     <  0
                         and f.dept                = il.dept
                         and f.item                = il.item_parent
                         and f.diff_id             is NULL
                         and f.location            = il.fr_zone_id
                         and f.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and f.action_date        <= LP_I_search_criteria.effective_date
                         and f.cur_hier_level      = RPM_CONSTANTS.FR_HIER_PARENT_ZONE) in1
               where in1.date_rank = 1) in2
       where in2.hier_rank = in2.max_rank;

   insert into rpm_promo_item_loc_expl_gtt(price_event_id,
                                           promo_item_loc_expl_id,
                                           item,
                                           dept,
                                           class,
                                           subclass,
                                           location,
                                           promo_id,
                                           promo_display_id,
                                           promo_secondary_ind,
                                           promo_comp_id,
                                           comp_display_id,
                                           promo_dtl_id,
                                           type,
                                           customer_type,
                                           detail_secondary_ind,
                                           detail_start_date,
                                           detail_end_date,
                                           detail_apply_to_code,
                                           detail_change_type,
                                           detail_change_amount,
                                           detail_change_currency,
                                           detail_change_percent,
                                           detail_change_selling_uom,
                                           detail_price_guide_id,
                                           exception_parent_id,
                                           promo_comp_msg_type,
                                           deleted_ind,
                                           rpile_rowid)
      select in2.price_event_id,
             in2.promo_item_loc_expl_id,
             in2.item,
             in2.dept,
             in2.class,
             in2.subclass,
             in2.location,
             in2.promo_id,
             in2.promo_display_id,
             in2.promo_secondary_ind,
             in2.promo_comp_id,
             in2.comp_display_id,
             in2.promo_dtl_id,
             in2.type,
             in2.customer_type,
             in2.detail_secondary_ind,
             in2.detail_start_date,
             in2.detail_end_date,
             in2.detail_apply_to_code,
             in2.detail_change_type,
             in2.detail_change_amount,
             in2.detail_change_currency,
             in2.detail_change_percent,
             in2.detail_change_selling_uom,
             in2.detail_price_guide_id,
             in2.exception_parent_id,
             NULL, --promo_comp_msg_type
             NULL, --deleted_ind
             NULL --rpile_rowid
        from (select in1.price_event_id,
                     in1.promo_item_loc_expl_id,
                     in1.item,
                     in1.dept,
                     in1.class,
                     in1.subclass,
                     in1.location,
                     in1.promo_id,
                     in1.promo_display_id,
                     in1.promo_secondary_ind,
                     in1.promo_comp_id,
                     in1.comp_display_id,
                     in1.promo_dtl_id,
                     in1.type,
                     in1.customer_type,
                     in1.detail_secondary_ind,
                     in1.detail_start_date,
                     in1.detail_end_date,
                     in1.detail_apply_to_code,
                     in1.detail_change_type,
                     in1.detail_change_amount,
                     in1.detail_change_currency,
                     in1.detail_change_percent,
                     in1.detail_change_selling_uom,
                     in1.detail_price_guide_id,
                     in1.exception_parent_id,
                     in1.zone_node_type,
                     in1.zone_id,
                     in1.item_parent,
                     in1.diff_id,
                     in1.max_hier_level,
                     in1.cur_hier_level,
                     in1.timebased_dtl_ind,
                     in1.hier_rank,
                     MAX(in1.hier_rank) OVER (PARTITION BY in1.item,
                                                           in1.location) max_rank
                from (select fr.price_event_id,
                             pe.promo_item_loc_expl_id,
                             pe.item,
                             pe.dept,
                             pe.class,
                             pe.subclass,
                             pe.location,
                             pe.promo_id,
                             pe.promo_display_id,
                             pe.promo_secondary_ind,
                             pe.promo_comp_id,
                             pe.comp_display_id,
                             pe.promo_dtl_id,
                             pe.type,
                             pe.customer_type,
                             pe.detail_secondary_ind,
                             pe.detail_start_date,
                             pe.detail_end_date,
                             pe.detail_apply_to_code,
                             pe.detail_change_type,
                             pe.detail_change_amount,
                             pe.detail_change_currency,
                             pe.detail_change_percent,
                             pe.detail_change_selling_uom,
                             pe.detail_price_guide_id,
                             pe.exception_parent_id,
                             pe.zone_node_type,
                             pe.zone_id,
                             pe.item_parent,
                             pe.diff_id,
                             pe.max_hier_level,
                             pe.cur_hier_level,
                             pe.timebased_dtl_ind,
                             3 hier_rank
                        from rpm_promo_item_loc_expl pe,
                             rpm_future_retail_gtt fr
                       where pe.dept                             = fr.dept
                         and pe.item                             = fr.item
                         and NVL(pe.diff_id, -9999)              = NVL(fr.diff_id, -9999)
                         and pe.location                         = fr.location
                         and pe.zone_node_type                   = fr.zone_node_type
                         and pe.cur_hier_level                   = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                         and LP_I_search_criteria.effective_date BETWEEN pe.detail_start_date and NVL(pe.detail_end_date, TO_DATE('3000','YYYY'))
                      union all
                      select fr.price_event_id,
                             pe.promo_item_loc_expl_id,
                             pe.item,
                             pe.dept,
                             pe.class,
                             pe.subclass,
                             pe.location,
                             pe.promo_id,
                             pe.promo_display_id,
                             pe.promo_secondary_ind,
                             pe.promo_comp_id,
                             pe.comp_display_id,
                             pe.promo_dtl_id,
                             pe.type,
                             pe.customer_type,
                             pe.detail_secondary_ind,
                             pe.detail_start_date,
                             pe.detail_end_date,
                             pe.detail_apply_to_code,
                             pe.detail_change_type,
                             pe.detail_change_amount,
                             pe.detail_change_currency,
                             pe.detail_change_percent,
                             pe.detail_change_selling_uom,
                             pe.detail_price_guide_id,
                             pe.exception_parent_id,
                             pe.zone_node_type,
                             pe.zone_id,
                             pe.item_parent,
                             pe.diff_id,
                             pe.max_hier_level,
                             pe.cur_hier_level,
                             pe.timebased_dtl_ind,
                             2 hier_rank
                        from rpm_promo_item_loc_expl pe,
                             rpm_future_retail_gtt fr
                       where pe.dept                             = fr.dept
                         and pe.item                             = fr.item
                         and NVL(pe.diff_id, -9999)              = NVL(fr.diff_id, -9999)
                         and pe.location                         = fr.zone_id
                         and pe.zone_node_type                   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and pe.cur_hier_level                   = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                         and LP_I_search_criteria.effective_date BETWEEN pe.detail_start_date and NVL(pe.detail_end_date, TO_DATE('3000','YYYY'))
                      union all
                      select fr.price_event_id,
                             pe.promo_item_loc_expl_id,
                             pe.item,
                             pe.dept,
                             pe.class,
                             pe.subclass,
                             pe.location,
                             pe.promo_id,
                             pe.promo_display_id,
                             pe.promo_secondary_ind,
                             pe.promo_comp_id,
                             pe.comp_display_id,
                             pe.promo_dtl_id,
                             pe.type,
                             pe.customer_type,
                             pe.detail_secondary_ind,
                             pe.detail_start_date,
                             pe.detail_end_date,
                             pe.detail_apply_to_code,
                             pe.detail_change_type,
                             pe.detail_change_amount,
                             pe.detail_change_currency,
                             pe.detail_change_percent,
                             pe.detail_change_selling_uom,
                             pe.detail_price_guide_id,
                             pe.exception_parent_id,
                             pe.zone_node_type,
                             pe.zone_id,
                             pe.item_parent,
                             pe.diff_id,
                             pe.max_hier_level,
                             pe.cur_hier_level,
                             pe.timebased_dtl_ind,
                             2 hier_rank
                        from rpm_promo_item_loc_expl pe,
                             rpm_future_retail_gtt fr
                       where pe.dept                             = fr.dept
                         and pe.item                             = fr.item_parent
                         and pe.diff_id                          = fr.diff_id
                         and pe.location                         = fr.location
                         and pe.zone_node_type                   = fr.zone_node_type
                         and pe.cur_hier_level                   = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                         and LP_I_search_criteria.effective_date BETWEEN pe.detail_start_date and NVL(pe.detail_end_date, TO_DATE('3000','YYYY'))
                      union all
                      select fr.price_event_id,
                             pe.promo_item_loc_expl_id,
                             pe.item,
                             pe.dept,
                             pe.class,
                             pe.subclass,
                             pe.location,
                             pe.promo_id,
                             pe.promo_display_id,
                             pe.promo_secondary_ind,
                             pe.promo_comp_id,
                             pe.comp_display_id,
                             pe.promo_dtl_id,
                             pe.type,
                             pe.customer_type,
                             pe.detail_secondary_ind,
                             pe.detail_start_date,
                             pe.detail_end_date,
                             pe.detail_apply_to_code,
                             pe.detail_change_type,
                             pe.detail_change_amount,
                             pe.detail_change_currency,
                             pe.detail_change_percent,
                             pe.detail_change_selling_uom,
                             pe.detail_price_guide_id,
                             pe.exception_parent_id,
                             pe.zone_node_type,
                             pe.zone_id,
                             pe.item_parent,
                             pe.diff_id,
                             pe.max_hier_level,
                             pe.cur_hier_level,
                             pe.timebased_dtl_ind,
                             1 hier_rank
                        from rpm_promo_item_loc_expl pe,
                             rpm_future_retail_gtt fr
                       where pe.dept                             = fr.dept
                         and pe.item                             = fr.item_parent
                         and pe.diff_id                          = fr.diff_id
                         and pe.location                         = fr.zone_id
                         and pe.zone_node_type                   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and pe.cur_hier_level                   = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and LP_I_search_criteria.effective_date BETWEEN pe.detail_start_date and NVL(pe.detail_end_date, TO_DATE('3000','YYYY'))
                      union all
                      select fr.price_event_id,
                             pe.promo_item_loc_expl_id,
                             pe.item,
                             pe.dept,
                             pe.class,
                             pe.subclass,
                             pe.location,
                             pe.promo_id,
                             pe.promo_display_id,
                             pe.promo_secondary_ind,
                             pe.promo_comp_id,
                             pe.comp_display_id,
                             pe.promo_dtl_id,
                             pe.type,
                             pe.customer_type,
                             pe.detail_secondary_ind,
                             pe.detail_start_date,
                             pe.detail_end_date,
                             pe.detail_apply_to_code,
                             pe.detail_change_type,
                             pe.detail_change_amount,
                             pe.detail_change_currency,
                             pe.detail_change_percent,
                             pe.detail_change_selling_uom,
                             pe.detail_price_guide_id,
                             pe.exception_parent_id,
                             pe.zone_node_type,
                             pe.zone_id,
                             pe.item_parent,
                             pe.diff_id,
                             pe.max_hier_level,
                             pe.cur_hier_level,
                             pe.timebased_dtl_ind,
                             1 hier_rank
                        from rpm_promo_item_loc_expl pe,
                             rpm_future_retail_gtt fr
                       where pe.dept                             = fr.dept
                         and pe.item                             = fr.item_parent
                         and pe.diff_id                          is NULL
                         and pe.location                         = fr.location
                         and pe.zone_node_type                   = fr.zone_node_type
                         and pe.cur_hier_level                   = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                         and LP_I_search_criteria.effective_date BETWEEN pe.detail_start_date and NVL(pe.detail_end_date, TO_DATE('3000','YYYY'))
                      union all
                      select fr.price_event_id,
                             pe.promo_item_loc_expl_id,
                             pe.item,
                             pe.dept,
                             pe.class,
                             pe.subclass,
                             pe.location,
                             pe.promo_id,
                             pe.promo_display_id,
                             pe.promo_secondary_ind,
                             pe.promo_comp_id,
                             pe.comp_display_id,
                             pe.promo_dtl_id,
                             pe.type,
                             pe.customer_type,
                             pe.detail_secondary_ind,
                             pe.detail_start_date,
                             pe.detail_end_date,
                             pe.detail_apply_to_code,
                             pe.detail_change_type,
                             pe.detail_change_amount,
                             pe.detail_change_currency,
                             pe.detail_change_percent,
                             pe.detail_change_selling_uom,
                             pe.detail_price_guide_id,
                             pe.exception_parent_id,
                             pe.zone_node_type,
                             pe.zone_id,
                             pe.item_parent,
                             pe.diff_id,
                             pe.max_hier_level,
                             pe.cur_hier_level,
                             pe.timebased_dtl_ind,
                             0 hier_rank
                        from rpm_promo_item_loc_expl pe,
                             rpm_future_retail_gtt fr
                       where pe.dept                             = fr.dept
                         and pe.item                             = fr.item_parent
                         and pe.diff_id                          is NULL
                         and pe.location                         = fr.zone_id
                         and pe.zone_node_type                   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and pe.cur_hier_level                   = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                         and LP_I_search_criteria.effective_date BETWEEN pe.detail_start_date and NVL(pe.detail_end_date, TO_DATE('3000','YYYY'))) in1) in2
       where in2.hier_rank = in2.max_rank;

   --Check for Primary Zone. If for the Item, the zone group belongs to primary zone group, then update RPM_FUTURE_RETAIL_GTT
   --with the following columns from RPM_ZONE_FUTURE_RETAIL.
   --selling_retail, selling_uom, and multi_unit_retail, multi_selling_retail if they are NOT NULL in RPM_ZONE_FUTURE_RETAIL

   if LP_zone_search = LP_ZONE_LEVEL then

      for rec in C_PRIMARY_ZONE loop

         update rpm_future_retail_gtt
            set timeline_seq = 1                    -- This is to indicate that it is primary zone
          where price_event_id = rec.price_event_id;

         merge /*+ INDEX (target RPM_FUTURE_RETAIL_GTT_I2) */ into rpm_future_retail_gtt target
         using (select /*+ INDEX (rfr RPM_FUTURE_RETAIL_GTT_I1) LEADING(rfr) */
                       rfr.future_retail_id,
                       rfr.dept,
                       rfr.item,
                       rfr.diff_id,
                       rfr.location,
                       rfr.zone_node_type,
                       rfr.action_date,
                       t.selling_retail,
                       t.selling_retail_currency,
                       t.selling_uom,
                       t.multi_units,
                       t.multi_unit_retail,
                       t.multi_unit_retail_currency,
                       t.multi_selling_uom
                  from rpm_zone_location rzl,
                       (select zfr.item,
                               zfr.zone,
                               zfr.selling_retail,
                               zfr.selling_retail_currency,
                               zfr.selling_uom,
                               zfr.multi_units,
                               zfr.multi_unit_retail,
                               zfr.multi_unit_retail_currency,
                               zfr.multi_selling_uom
                          from (select /*+ LEADING(items) */ rzfr.item,
                                       rzfr.zone,
                                       rzfr.selling_retail,
                                       rzfr.selling_retail_currency,
                                       rzfr.selling_uom,
                                       rzfr.multi_units,
                                       rzfr.multi_unit_retail,
                                       rzfr.multi_unit_retail_currency,
                                       rzfr.multi_selling_uom,
                                       RANK() OVER (PARTITION BY rzfr.item,
                                                                 rzfr.zone
                                                        ORDER BY rzfr.action_date desc) rank
                                  from (select distinct
                                               ril.item
                                          from rpm_price_inquiry_gtt ril
                                         where ril.price_event_id = rec.price_event_id * -1) items,
                                       rpm_zone_future_retail rzfr
                                 where rzfr.item                = items.item
                                   and rzfr.zone                = rec.zone_id
                                   and TRUNC(rzfr.action_date) <= TRUNC(LP_I_search_criteria.effective_date)) zfr
                         where zfr.rank = 1) t,
                       rpm_future_retail_gtt rfr
                 where rzl.zone_id        = t.zone
                   and rfr.price_event_id = rec.price_event_id
                   and rfr.item           = t.item
                   and rfr.location       = rzl.location) source
            on (    target.price_event_id       = rec.price_event_id
                and target.future_retail_id     = source.future_retail_id
                and target.item                 = source.item
                and NVL(target.diff_id, -99999) = NVL(source.diff_id, -99999)
                and target.location             = source.location
                and target.zone_node_type       = source.zone_node_type
                and TRUNC(target.action_date)   = TRUNC(source.action_date))
         when MATCHED then
            update
               set target.selling_retail             = source.selling_retail,
                   target.selling_retail_currency    = source.selling_retail_currency,
                   target.selling_uom                = source.selling_uom,
                   target.multi_unit_retail          = case
                                                          when source.multi_unit_retail IS NOT NULL then
                                                             source.multi_unit_retail
                                                          else
                                                             target.multi_unit_retail
                                                        end,
                   target.multi_unit_retail_currency = case
                                                          when source.multi_unit_retail IS NOT NULL then
                                                             source.multi_unit_retail_currency
                                                          else
                                                             target.multi_unit_retail_currency
                                                       end,
                   target.multi_units                = case
                                                          when source.multi_unit_retail IS NOT NULL then
                                                             source.multi_units
                                                          else
                                                             target.multi_units
                                                       end,
                   target.multi_selling_uom          = case
                                                          when source.multi_unit_retail IS NOT NULL then
                                                             source.multi_selling_uom
                                                          else
                                                             target.multi_selling_uom
                                                       end;

      end loop;

   end if;

   O_return_code := 1;

EXCEPTION

   when OTHERS then
      O_return_code := 0;

      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
END POP_FUTURE_RETAIL_GTT;
-----------------------------------------------------------------------------------------------------------------------

PROCEDURE BUILD_PRICE_INQ_VO (O_return_code    OUT NUMBER,
                              O_error_msg      OUT VARCHAR2,
                              O_prices         OUT NOCOPY OBJ_PRC_INQ_TBL) AS

   L_program VARCHAR2(40) := 'RPM_PRICE_INQUIRY_SQL.BUILD_PRICE_INQ_VO';

   L_cost_calc_method    RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE := NULL;
   L_class_level_vat_ind SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE         := NULL;
   L_default_tax_type    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE            := NULL;

   L_end_dt         DATE             := TO_DATE('3000','YYYY');
   L_prices         OBJ_PRC_INQ_TBL  := OBJ_PRC_INQ_TBL();
   L_tax_calc_tbl   OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_custom_tax_ind NUMBER           := 0;

   cursor C_CHECK_CUSTOM is
      select /*+ CARDINALITY(t 10) */ 1
        from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
             rpm_zone_location rzl,
             (select s.store loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE zone_node_type
                from store s,
                     vat_region vr
               where s.vat_region     = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM
              union all
              select w.wh loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                from wh w,
                     vat_region vr
               where w.vat_region     = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM) locs
       where t.is_zone    = 1
         and t.location   = rzl.zone_id
         and rzl.location = locs.loc
         and rzl.loc_type = locs.zone_node_type
         and rownum       = 1;

   cursor C_ITEM_LOC is
      select NEW OBJ_PRC_INQ_REC(item,
                                 item_desc,
                                 LP_TRAN_LEVEL,
                                 NULL, -- diff_id
                                 location,
                                 location,
                                 location_name,
                                 cust_seg_promo_ind,
                                 primary_zone,
                                 is_zone,
                                 LP_I_search_criteria.effective_date,
                                 selling_retail,
                                 selling_uom,
                                 selling_currency,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_selling_uom,
                                 multi_selling_retail_currency,
                                 clear_retail,
                                 clear_uom,
                                 clear_retail_currency,
                                 simple_promo_retail,
                                 simple_promo_uom,
                                 simple_promo_retail_currency,
                                 complex_promo,
                                 reg_retail_avg,
                                 reg_multi_unit_avg,
                                 reg_multi_unit_retail_avg,
                                 clear_retail_avg,
                                 promo_retail_avg,
                                 NULL, --cost
                                 NULL) --markup_percent
        from (select f.price_event_id,
                     f.item,
                     im.item_desc,
                     f.location,
                     locs.location_name,
                     case
                        when EXISTS (select 1
                                       from rpm_promo_item_loc_expl_gtt rpile
                                      where rpile.price_event_id                = f.price_event_id
                                        and LP_I_search_criteria.effective_date BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, TRUNC(L_end_dt))
                                        and rpile.customer_type                 is NOT NULL) then
                           1
                        else
                           0
                     end cust_seg_promo_ind,
                     0 primary_zone,
                     0 is_zone,
                     f.selling_retail,
                     f.selling_uom,
                     locs.currency_code selling_currency,
                     f.multi_units,
                     f.multi_unit_retail,
                     f.multi_selling_uom,
                     locs.currency_code multi_selling_retail_currency,
                     f.clear_retail,
                     f.clear_uom,
                     locs.currency_code clear_retail_currency,
                     f.simple_promo_retail,
                     f.simple_promo_uom,
                     locs.currency_code simple_promo_retail_currency,
                     f.on_complex_promo_ind  complex_promo,
                     0 reg_retail_avg,
                     0 reg_multi_unit_avg,
                     0 reg_multi_unit_retail_avg,
                     0 clear_retail_avg,
                     0 promo_retail_avg
                from rpm_future_retail_gtt f,
                     item_master im,
                     (select store location,
                             store_name location_name,
                             currency_code
                        from store
                      union all
                      select wh location,
                             wh_name location_name,
                             currency_code
                        from wh) locs,
                     rpm_price_inquiry_gtt ril
               where im.item            = f.item
                 and locs.location      = f.location
                 and im.item_level      = im.tran_level
                 and ril.price_event_id = f.price_event_id
                 and ril.item           = im.item
                 and ril.price_event_id > 0) t;

   cursor C_PARENT_LOC is
      select NEW OBJ_PRC_INQ_REC(item,
                                 item_desc,
                                 DECODE(diff_id,
                                        NULL, LP_PARENT_LEVEL,
                                        LP_PARENT_DIFF_LEVEL),
                                 diff_id,
                                 location,
                                 location, -- location display id
                                 location_name,
                                 cust_seg_promo_ind,
                                 0, -- primary_zone
                                 0, -- is_zone
                                 LP_I_search_criteria.effective_date,
                                 selling_retail,
                                 selling_uom,
                                 currency_code, -- selling_currency
                                 multi_units,
                                 multi_unit_retail,
                                 multi_selling_uom,
                                 currency_code, -- multi_selling_retail_currency
                                 clear_retail,
                                 clear_uom,
                                 currency_code, -- clear_retail_currency
                                 case
                                    when max_simple_promo_uom != min_simple_promo_uom then
                                       NULL
                                    else
                                       simple_promo_retail
                                 end,
                                 case
                                    when max_simple_promo_uom != min_simple_promo_uom then
                                       NULL
                                    else
                                       min_simple_promo_uom
                                 end,
                                 currency_code, -- simple_promo_retail_currency
                                 complex_promo,
                                 case
                                    when max_selling_retail != min_selling_retail then
                                       1
                                    else
                                       0
                                 end, -- retail_avg
                                 case
                                    when max_multi_units != min_multi_units then
                                       1
                                    else
                                       0
                                 end, -- reg_multi_unit_avg
                                 case
                                    when max_multi_unit_retail != min_multi_unit_retail then
                                       1
                                    else
                                       0
                                 end, -- reg_multi_unit_retail_avg
                                 case
                                    when max_clear_retail != min_clear_retail then
                                       1
                                    else
                                       0
                                 end, -- clear_retail_avg
                                 case
                                    when max_simple_promo_retail != min_simple_promo_retail and
                                         max_simple_promo_uom = min_simple_promo_uom then
                                       1
                                    else
                                       0
                                 end, -- promo_retail_avg
                                 NULL,   -- cost
                                 NULL)   -- markup_percent
        from (select t.price_event_id,
                     t.item,
                     t.diff_id,
                     t.location,
                     t.item_desc,
                     t.location_name,
                     case
                        when EXISTS (select 1
                                       from rpm_promo_item_loc_expl_gtt rpile
                                      where rpile.price_event_id                = t.price_event_id
                                        and LP_I_search_criteria.effective_date BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, TRUNC(L_end_dt))
                                        and rpile.customer_type                 is NOT NULL) then
                           1
                        else
                           0
                     end cust_seg_promo_ind,
                     t.currency_code,
                     t.selling_retail,
                     t.selling_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_selling_uom,
                     t.clear_retail,
                     t.clear_uom,
                     t.simple_promo_retail,
                     t.min_simple_promo_uom,
                     t.max_simple_promo_uom,
                     t.complex_promo,
                     t.max_selling_retail,
                     t.min_selling_retail,
                     t.max_multi_units,
                     t.min_multi_units,
                     t.max_multi_unit_retail,
                     t.min_multi_unit_retail,
                     t.max_clear_retail,
                     t.min_clear_retail,
                     t.max_simple_promo_retail,
                     t.min_simple_promo_retail
                from (select f.price_event_id,
                             MIN(f.rollup_item) item,
                             MIN(f.diff_id) diff_id,
                             MIN(f.location) location,
                             MIN(im.item_desc) item_desc,
                             MIN(f.location_name) location_name,
                             MIN(f.currency_code) currency_code,
                             AVG(f.selling_retail) selling_retail,
                             MIN(f.selling_uom) selling_uom,
                             AVG(f.multi_units) multi_units,
                             AVG(f.multi_unit_retail) multi_unit_retail,
                             MIN(f.multi_selling_uom) multi_selling_uom,
                             AVG(f.clear_retail) clear_retail,
                             MIN(f.clear_uom) clear_uom,
                             AVG(f.simple_promo_retail) simple_promo_retail,
                             MIN(f.simple_promo_uom)  min_simple_promo_uom,
                             MAX(f.simple_promo_uom)  max_simple_promo_uom,
                             MAX(complex_promo) complex_promo,
                             MAX(f.selling_retail) max_selling_retail,
                             MIN(f.selling_retail) min_selling_retail,
                             MAX(f.multi_units) max_multi_units,
                             MIN(f.multi_units) min_multi_units,
                             MAX(f.multi_unit_retail) max_multi_unit_retail,
                             MIN(f.multi_unit_retail) min_multi_unit_retail,
                             MAX(f.clear_retail) max_clear_retail,
                             MIN(f.clear_retail) min_clear_retail,
                             MAX(f.simple_promo_retail) max_simple_promo_retail,
                             MIN(f.simple_promo_retail) min_simple_promo_retail
                        from (select ril.price_event_id,
                                     ril.item rollup_item,
                                     ril.diff_id,
                                     locs.currency_code,
                                     locs.location_name,
                                     gtt.item,
                                     gtt.dept,
                                     gtt.class,
                                     gtt.subclass,
                                     gtt.zone_node_type,
                                     gtt.location,
                                     gtt.action_date,
                                     gtt.selling_retail,
                                     gtt.selling_retail_currency,
                                     gtt.selling_uom,
                                     gtt.multi_units,
                                     gtt.multi_unit_retail,
                                     gtt.multi_unit_retail_currency,
                                     gtt.multi_selling_uom,
                                     gtt.clear_retail,
                                     gtt.clear_retail_currency,
                                     gtt.clear_uom,
                                     gtt.simple_promo_retail,
                                     gtt.simple_promo_retail_currency,
                                     gtt.simple_promo_uom,
                                     gtt.on_simple_promo_ind,
                                     gtt.price_change_id,
                                     gtt.price_change_display_id,
                                     gtt.pc_exception_parent_id,
                                     gtt.pc_change_type,
                                     gtt.pc_change_amount,
                                     gtt.pc_change_currency,
                                     gtt.pc_change_percent,
                                     gtt.pc_change_selling_uom,
                                     gtt.pc_null_multi_ind,
                                     gtt.pc_multi_units,
                                     gtt.pc_multi_unit_retail,
                                     gtt.pc_multi_unit_retail_currency,
                                     gtt.pc_multi_selling_uom,
                                     gtt.pc_price_guide_id,
                                     gtt.clearance_id,
                                     gtt.clearance_display_id,
                                     gtt.clear_mkdn_index,
                                     gtt.clear_start_ind,
                                     gtt.clear_change_type,
                                     gtt.clear_change_amount,
                                     gtt.clear_change_currency,
                                     gtt.clear_change_percent,
                                     gtt.clear_change_selling_uom,
                                     gtt.clear_price_guide_id,
                                     gtt.loc_move_from_zone_id,
                                     gtt.loc_move_to_zone_id,
                                     gtt.location_move_id,
                                     gtt.lock_version,
                                     gtt.on_complex_promo_ind complex_promo
                                from rpm_price_inquiry_gtt ril,
                                     rpm_future_retail_gtt gtt,
                                     (select store location,
                                             store_name location_name,
                                             currency_code
                                        from store
                                      union all
                                      select wh location,
                                             wh_name location_name,
                                             currency_code
                                        from wh) locs
                               where ril.price_event_id  >  0
                                 and gtt.price_event_id  = ril.price_event_id
                                 and gtt.item           != ril.item
                                 and gtt.location        = locs.location) f,
                               item_master im
                         where f.rollup_item = im.item
                         group by f.price_event_id) t);

   cursor C_ITEM_ZONE is
      select NEW OBJ_PRC_INQ_REC(item,
                                 item_desc,
                                 LP_TRAN_LEVEL,
                                 NULL, -- diff_id
                                 location,
                                 loc_disp_id,
                                 location_name,
                                 cust_seg_promo_ind,
                                 primary_zone,
                                 1, -- is_zone
                                 LP_I_search_criteria.effective_date,
                                 selling_retail,
                                 selling_uom,
                                 currency_code, -- selling_currency
                                 multi_units,
                                 multi_unit_retail,
                                 multi_selling_uom,
                                 currency_code, -- multi_selling_retail_currency
                                 clear_retail,
                                 clear_uom,
                                 currency_code, -- clear_retail_currency
                                 case
                                    when max_simple_promo_uom != min_simple_promo_uom then
                                       NULL
                                    else
                                      simple_promo_retail
                                 end,
                                 case
                                    when max_simple_promo_uom != min_simple_promo_uom then
                                       NULL
                                    else
                                       min_simple_promo_uom
                                 end,
                                 currency_code, -- simple_promo_retail_currency
                                 complex_promo,
                                 case
                                    when max_selling_retail != min_selling_retail then
                                       1
                                    else
                                       0
                                 end, -- retail_avg
                                 case
                                    when max_multi_units != min_multi_units then
                                       1
                                    else
                                       0
                                 end, -- reg_multi_unit_avg
                                 case
                                    when max_multi_unit_retail != min_multi_unit_retail then
                                       1
                                    else
                                       0
                                 end, -- reg_multi_unit_retail_avg
                                 case
                                    when max_clear_retail != min_clear_retail then
                                       1
                                    else
                                       0
                                 end, -- clear_retail_avg
                                 case
                                    when max_simple_promo_retail != min_simple_promo_retail and
                                         max_simple_promo_uom = min_simple_promo_uom then
                                       1
                                    else
                                       0
                                 end, -- promo_retail_avg
                                 NULL, -- cost
                                 NULL) -- markup_percent
        from (select t.price_event_id,
                     t.item,
                     t.location,
                     t.loc_disp_id,
                     t.item_desc,
                     t.location_name,
                     case
                        when EXISTS (select 1
                                       from rpm_promo_item_loc_expl_gtt rpile
                                      where rpile.price_event_id                = t.price_event_id
                                        and LP_I_search_criteria.effective_date BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, TRUNC(L_end_dt))
                                        and rpile.customer_type                 is NOT NULL) then
                           1
                        else
                           0
                     end cust_seg_promo_ind,
                     t.currency_code,
                     t.selling_retail,
                     t.selling_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_selling_uom,
                     t.clear_retail,
                     t.clear_uom,
                     t.simple_promo_retail,
                     t.min_simple_promo_uom,
                     t.max_simple_promo_uom,
                     t.complex_promo,
                     t.max_selling_retail,
                     t.min_selling_retail,
                     t.max_multi_units,
                     t.min_multi_units,
                     t.max_multi_unit_retail,
                     t.min_multi_unit_retail,
                     t.max_clear_retail,
                     t.min_clear_retail,
                     t.max_simple_promo_retail,
                     t.min_simple_promo_retail,
                     t.primary_zone
                from (select f.price_event_id,
                             MIN(f.item) item,
                             MIN(f.zone_id) location,
                             MIN(f.zone_display_id) loc_disp_id,
                             MIN(im.item_desc) item_desc,
                             MIN(f.location_name) location_name,
                             MIN(f.currency_code) currency_code,
                             AVG(f.selling_retail) selling_retail,
                             MIN(f.selling_uom) selling_uom,
                             AVG(f.multi_units) multi_units,
                             AVG(f.multi_unit_retail) multi_unit_retail,
                             MIN(f.multi_selling_uom) multi_selling_uom,
                             AVG(f.clear_retail) clear_retail,
                             MIN(f.clear_uom) clear_uom,
                             AVG(f.simple_promo_retail) simple_promo_retail,
                             MIN(f.simple_promo_uom) min_simple_promo_uom,
                             MAX(f.simple_promo_uom) max_simple_promo_uom,
                             MAX(complex_promo) complex_promo,
                             MAX(f.selling_retail) max_selling_retail,
                             MIN(f.selling_retail) min_selling_retail,
                             MAX(f.multi_units) max_multi_units,
                             MIN(f.multi_units) min_multi_units,
                             MAX(f.multi_unit_retail) max_multi_unit_retail,
                             MIN(f.multi_unit_retail) min_multi_unit_retail,
                             MAX(f.clear_retail) max_clear_retail,
                             MIN(f.clear_retail) min_clear_retail,
                             MAX(f.simple_promo_retail) max_simple_promo_retail,
                             MIN(f.simple_promo_retail) min_simple_promo_retail,
                             MAX(f.primary_zone) primary_zone
                        from (select ril.price_event_id,
                                     rz.zone_id,
                                     rz.zone_display_id,
                                     rz.currency_code,
                                     rz.name location_name,
                                     NVL(gtt.timeline_seq, 0) primary_zone,
                                     gtt.item,
                                     gtt.dept,
                                     gtt.class,
                                     gtt.subclass,
                                     gtt.zone_node_type,
                                     gtt.location,
                                     gtt.action_date,
                                     gtt.selling_retail,
                                     gtt.selling_retail_currency,
                                     gtt.selling_uom,
                                     gtt.multi_units,
                                     gtt.multi_unit_retail,
                                     gtt.multi_unit_retail_currency,
                                     gtt.multi_selling_uom,
                                     gtt.clear_retail,
                                     gtt.clear_retail_currency,
                                     gtt.clear_uom,
                                     gtt.simple_promo_retail,
                                     gtt.simple_promo_retail_currency,
                                     gtt.simple_promo_uom,
                                     gtt.on_simple_promo_ind,
                                     gtt.price_change_id,
                                     gtt.price_change_display_id,
                                     gtt.pc_exception_parent_id,
                                     gtt.pc_change_type,
                                     gtt.pc_change_amount,
                                     gtt.pc_change_currency,
                                     gtt.pc_change_percent,
                                     gtt.pc_change_selling_uom,
                                     gtt.pc_null_multi_ind,
                                     gtt.pc_multi_units,
                                     gtt.pc_multi_unit_retail,
                                     gtt.pc_multi_unit_retail_currency,
                                     gtt.pc_multi_selling_uom,
                                     gtt.pc_price_guide_id,
                                     gtt.clearance_id,
                                     gtt.clearance_display_id,
                                     gtt.clear_mkdn_index,
                                     gtt.clear_start_ind,
                                     gtt.clear_change_type,
                                     gtt.clear_change_amount,
                                     gtt.clear_change_currency,
                                     gtt.clear_change_percent,
                                     gtt.clear_change_selling_uom,
                                     gtt.clear_price_guide_id,
                                     gtt.loc_move_from_zone_id,
                                     gtt.loc_move_to_zone_id,
                                     gtt.location_move_id,
                                     gtt.lock_version,
                                     gtt.on_complex_promo_ind complex_promo
                                from rpm_price_inquiry_gtt ril,
                                     rpm_future_retail_gtt gtt,
                                     rpm_zone rz
                               where ril.price_event_id > 0
                                 and ril.item           = gtt.item
                                 and gtt.price_event_id = ril.price_event_id
                                 and rz.zone_id         = ril.location) f,
                             item_master im
                       where f.item = im.item
                       group by f.price_event_id) t);

   cursor C_PARENT_ZONE is
      select NEW OBJ_PRC_INQ_REC(item,
                                 item_desc,
                                 DECODE(diff_id,
                                        NULL, LP_PARENT_LEVEL,
                                        LP_PARENT_DIFF_LEVEL), -- parent_type -- diff Id null is parent level .If it is not null it means Parent diff.
                                 diff_id,
                                 location,
                                 loc_disp_id,
                                 location_name,
                                 cust_seg_promo_ind,
                                 primary_zone,
                                 1, -- is_zone
                                 LP_I_search_criteria.effective_date,
                                 selling_retail,
                                 selling_uom,
                                 currency_code, -- selling_currency
                                 multi_units,
                                 multi_unit_retail,
                                 multi_selling_uom,
                                 currency_code, -- multi_selling_retail_currency
                                 clear_retail,
                                 clear_uom,
                                 currency_code, -- clear_retail_currency
                                 case
                                    when max_simple_promo_uom != min_simple_promo_uom then
                                       NULL
                                    else
                                      simple_promo_retail
                                 end,
                                 case
                                    when max_simple_promo_uom != min_simple_promo_uom then
                                       NULL
                                    else
                                       min_simple_promo_uom
                                 end,
                                 currency_code, -- simple_promo_retail_currency
                                 complex_promo,
                                 case
                                    when max_selling_retail != min_selling_retail then
                                       1
                                    else
                                       0
                                 end, -- retail_avg
                                 case
                                    when max_multi_units != min_multi_units then
                                       1
                                    else
                                       0
                                 end, -- reg_multi_unit_avg
                                 case
                                    when max_multi_unit_retail != min_multi_unit_retail then
                                       1
                                    else
                                       0
                                 end, -- reg_multi_unit_retail_avg
                                 case
                                    when max_clear_retail != min_clear_retail then
                                       1
                                    else
                                       0
                                 end, -- clear_retail_avg
                                 case
                                    when max_simple_promo_retail != min_simple_promo_retail and
                                         max_simple_promo_uom = min_simple_promo_uom then
                                       1
                                    else
                                       0
                                 end, -- promo_retail_avg
                                 NULL, -- cost
                                 NULL) -- markup percent
        from (select t.price_event_id,
                     t.item,
                     t.diff_id,
                     t.location,
                     t.loc_disp_id,
                     t.item_desc,
                     t.location_name,
                     case
                        when EXISTS (select 1
                                       from rpm_promo_item_loc_expl_gtt rpile
                                      where rpile.price_event_id                = t.price_event_id
                                        and LP_I_search_criteria.effective_date BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, TRUNC(L_end_dt))
                                        and rpile.customer_type                 is NOT NULL) then
                           1
                        else
                           0
                     end cust_seg_promo_ind,
                     t.currency_code,
                     t.selling_retail,
                     t.selling_uom,
                     t.multi_units,
                     t.multi_unit_retail,
                     t.multi_selling_uom,
                     t.clear_retail,
                     t.clear_uom,
                     t.simple_promo_retail,
                     t.min_simple_promo_uom,
                     t.max_simple_promo_uom,
                     t.complex_promo,
                     t.max_selling_retail,
                     t.min_selling_retail,
                     t.max_multi_units,
                     t.min_multi_units,
                     t.max_multi_unit_retail,
                     t.min_multi_unit_retail,
                     t.max_clear_retail,
                     t.min_clear_retail,
                     t.max_simple_promo_retail,
                     t.min_simple_promo_retail,
                     t.primary_zone
                from (select f.price_event_id,
                             MIN(f.rollup_item) item,
                             MIN(f.diff_id) diff_id,
                             MIN(f.zone_id) location,
                             MIN(f.zone_display_id) loc_disp_id,
                             MIN(im.item_desc) item_desc,
                             MIN(f.location_name) location_name,
                             MIN(f.currency_code) currency_code,
                             AVG(f.selling_retail) selling_retail,
                             MIN(f.selling_uom) selling_uom,
                             AVG(f.multi_units) multi_units,
                             AVG(f.multi_unit_retail) multi_unit_retail,
                             MIN(f.multi_selling_uom) multi_selling_uom,
                             AVG(f.clear_retail) clear_retail,
                             MIN(f.clear_uom) clear_uom,
                             AVG(f.simple_promo_retail) simple_promo_retail,
                             MIN(f.simple_promo_uom) min_simple_promo_uom,
                             MAX(f.simple_promo_uom) max_simple_promo_uom,
                             MAX(complex_promo) complex_promo,
                             MAX(f.selling_retail) max_selling_retail,
                             MIN(f.selling_retail) min_selling_retail,
                             MAX(f.multi_units) max_multi_units,
                             MIN(f.multi_units) min_multi_units,
                             MAX(f.multi_unit_retail) max_multi_unit_retail,
                             MIN(f.multi_unit_retail) min_multi_unit_retail,
                             MAX(f.clear_retail) max_clear_retail,
                             MIN(f.clear_retail) min_clear_retail,
                             MAX(f.simple_promo_retail) max_simple_promo_retail,
                             MIN(f.simple_promo_retail) min_simple_promo_retail,
                             MAX(f.primary_zone) primary_zone
                        from (select ril.price_event_id,
                                     ril.item rollup_item,
                                     ril.diff_id,
                                     rz.zone_id,
                                     rz.zone_display_id,
                                     rz.currency_code,
                                     rz.name location_name,
                                     NVL(gtt.timeline_seq, 0) primary_zone,
                                     gtt.item,
                                     gtt.dept,
                                     gtt.class,
                                     gtt.subclass,
                                     gtt.zone_node_type,
                                     gtt.location,
                                     gtt.action_date,
                                     gtt.selling_retail,
                                     gtt.selling_retail_currency,
                                     gtt.selling_uom,
                                     gtt.multi_units,
                                     gtt.multi_unit_retail,
                                     gtt.multi_unit_retail_currency,
                                     gtt.multi_selling_uom,
                                     gtt.clear_retail,
                                     gtt.clear_retail_currency,
                                     gtt.clear_uom,
                                     gtt.simple_promo_retail,
                                     gtt.simple_promo_retail_currency,
                                     gtt.simple_promo_uom,
                                     gtt.on_simple_promo_ind,
                                     gtt.price_change_id,
                                     gtt.price_change_display_id,
                                     gtt.pc_exception_parent_id,
                                     gtt.pc_change_type,
                                     gtt.pc_change_amount,
                                     gtt.pc_change_currency,
                                     gtt.pc_change_percent,
                                     gtt.pc_change_selling_uom,
                                     gtt.pc_null_multi_ind,
                                     gtt.pc_multi_units,
                                     gtt.pc_multi_unit_retail,
                                     gtt.pc_multi_unit_retail_currency,
                                     gtt.pc_multi_selling_uom,
                                     gtt.pc_price_guide_id,
                                     gtt.clearance_id,
                                     gtt.clearance_display_id,
                                     gtt.clear_mkdn_index,
                                     gtt.clear_start_ind,
                                     gtt.clear_change_type,
                                     gtt.clear_change_amount,
                                     gtt.clear_change_currency,
                                     gtt.clear_change_percent,
                                     gtt.clear_change_selling_uom,
                                     gtt.clear_price_guide_id,
                                     gtt.loc_move_from_zone_id,
                                     gtt.loc_move_to_zone_id,
                                     gtt.location_move_id,
                                     gtt.lock_version,
                                     gtt.on_complex_promo_ind complex_promo
                                from rpm_price_inquiry_gtt ril,
                                     rpm_future_retail_gtt gtt,
                                     rpm_zone rz
                               where ril.price_event_id > 0
                                 and gtt.item           != ril.item
                                 and gtt.price_event_id = ril.price_event_id
                                 and rz.zone_id         = ril.location) f,
                             item_master im
                       where f.rollup_item = im.item
                       group by f.price_event_id) t);

   cursor C_ADD_MARKUP_INFO is
      select NEW OBJ_PRC_INQ_REC(t.item,
                                 t.item_desc,
                                 t.is_parent,
                                 t.diff_id,
                                 t.location,
                                 t.location_display_id,
                                 t.location_name,
                                 t.cust_seg_promo_ind,
                                 t.primary_zone,
                                 t.is_zone,
                                 t.action_date,
                                 t.selling_retail,
                                 t.selling_uom,
                                 t.selling_retail_currency,
                                 t.multi_units,
                                 t.multi_unit_retail,
                                 t.multi_selling_uom,
                                 t.multi_selling_retail_currency,
                                 t.clear_retail,
                                 t.clear_uom,
                                 t.clear_retail_currency,
                                 t.simple_promo_retail,
                                 t.simple_promo_uom,
                                 t.simple_promo_retail_currency,
                                 t.complex_promo,
                                 t.reg_retail_avg,
                                 t.reg_multi_unit_avg,
                                 t.reg_multi_unit_retail_avg,
                                 t.clear_retail_avg,
                                 t.promo_retail_avg,
                                 rilm.cost,
                                 rilm.markup_percent)
        from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
             rpm_il_markup_gtt rilm
       where t.item                 = rilm.item
         and NVL(t.diff_id, t.item) = NVL(rilm.diff_id, rilm.item)
         and t.location             = rilm.location;

BEGIN

   select default_tax_type,
          class_level_vat_ind
     into L_default_tax_type,
          L_class_level_vat_ind
     from system_options;

   select cost_calculation_method
     into L_cost_calc_method
     from rpm_system_options;

   if LP_zone_search = LP_LOCATION_LEVEL then
      -- Location Level
      if LP_I_search_criteria.item_level IN (RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM,
                                             RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST,
                                             RPM_CONSTANTS.PC_SRCH_PRICE_EVENT_ITEMLIST) then
         -- ITEM level is item, itemlist or price event itemlist. They all have the same selection criteria
         if LP_I_search_criteria.item_type  = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM then

            open C_ITEM_LOC;
            fetch C_ITEM_LOC BULK COLLECT into L_prices limit LP_I_search_criteria.maximum_result;
            close C_ITEM_LOC;

         -- Aggregate to Item Parent / Location. Avg to Item Parent at location level
         -- Aggregate to Item Parent / Diff / Location. Avg to Item Parent / Diff at location level
         elsif LP_I_search_criteria.item_type IN (RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT,
                                                  RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF) then
            ---
            open C_PARENT_LOC;
            fetch C_PARENT_LOC BULK COLLECT into L_prices limit LP_I_search_criteria.maximum_result;
            close C_PARENT_LOC;

         -- Item List or Price Event Item List
         else

            open C_ITEM_LOC;
            fetch C_ITEM_LOC BULK COLLECT into L_prices limit LP_I_search_criteria.maximum_result;
            close C_ITEM_LOC;

         end if;

      end if;

   else
      -- Zone Level
      if LP_I_search_criteria.item_level IN (RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEM,
                                             RPM_CONSTANTS.PC_SRCH_ITEM_LEVEL_ITEMLIST,
                                             RPM_CONSTANTS.PC_SRCH_PRICE_EVENT_ITEMLIST) then
      -- ITEM level is item, itemlist or price event itemlist. Aggregate to Item / Zone level.
      -- If LEVEL is ITEMLIST or Price event itemlist, then there will be no ITEM_TYPE.
         if LP_I_search_criteria.item_type = RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_ITEM then

            open C_ITEM_ZONE;
            fetch C_ITEM_ZONE BULK COLLECT into L_prices limit LP_I_search_criteria.maximum_result;
            close C_ITEM_ZONE;

         -- Aggregate to Item Parent / Zone. Avg to Item Parent at Zone level.
         -- Aggregate to Item Parent / Diff / Zone. Avg to Item Parent / Diff at Zone level
         elsif LP_I_search_criteria.item_type IN (RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT,
                                                  RPM_CONSTANTS.PC_SRCH_ITEM_TYPE_PARENT_DIFF) then

            open C_PARENT_ZONE;
            fetch C_PARENT_ZONE BULK COLLECT into L_prices limit LP_I_search_criteria.maximum_result;
            close C_PARENT_ZONE;

         -- Item List or Price Event Item List
         else

            open C_ITEM_ZONE;
            fetch C_ITEM_ZONE BULK COLLECT into L_prices limit LP_I_search_criteria.maximum_result;
            close C_ITEM_ZONE;

         end if;

      end if;

   end if;

   delete from rpm_il_markup_gtt;

   if L_default_tax_type IN (RPM_CONSTANTS.SVAT_TAX_TYPE,
                             RPM_CONSTANTS.SALES_TAX_TYPE) then

      open C_CHECK_CUSTOM;
      fetch C_CHECK_CUSTOM into L_custom_tax_ind;
      close C_CHECK_CUSTOM;

      if L_custom_tax_ind = 1 then

         select /*+ CARDINALITY(t 10) */
                OBJ_TAX_CALC_REC(item,     -- I_item
                                 NULL,     -- I_pack_ind
                                 rzl.location, -- I_from_entity
                                 DECODE(rzl.loc_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_from_entity_type
                                 rzl.location, -- I_to_entity
                                 DECODE(rzl.loc_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_to_entity_type
                                 action_date, -- I_effective_from_date
                                 selling_retail, -- I_amount
                                 selling_retail_currency, -- I_amount_curr
                                 NULL, -- I_amount_tax_incl_ind
                                 NULL, -- I_origin_country_id
                                 NULL, -- O_cum_tax_pct
                                 NULL, -- O_cum_tax_value
                                 NULL, -- O_total_tax_amount
                                 NULL, -- O_total_tax_amount_curr
                                 NULL, -- O_total_recover_amount
                                 NULL, -- O_total_recover_amount_curr
                                 NULL, -- O_tax_detail_tbl
                                 'MARKUPCALC', -- I_tran_type
                                 action_date, -- I_tran_date
                                 NULL, -- I_tran_id
                                 'R') -- I_cost_retail_ind
            BULK COLLECT into L_tax_calc_tbl
           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                rpm_zone_location rzl
          where t.is_zone  = 1
            and t.location = rzl.zone_id;

         if TAX_SQL.CALC_RETAIL_TAX(O_error_msg,
                                    L_tax_calc_tbl) = FALSE then
            return;
         end if;

      end if;

      insert into rpm_il_markup_gtt(item,
                                    location,
                                    loc_type,
                                    action_date,
                                    dept,
                                    retail,
                                    retail_currency,
                                    selling_uom,
                                    cost,
                                    cost_currency,
                                    vat_rate,
                                    diff_id)
         -- item/loc
         select /*+ CARDINALITY(t 10) */ distinct t.item,
                t.location,
                locs.loc_type,
                t.action_date,
                im.dept,
                t.selling_retail,
                t.selling_retail_currency,
                t.selling_uom,
                NULL, -- cost,
                NULL, -- cost_currency,
                NULL, -- vat_rate,
                NULL  -- diff_id
           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                (select store loc,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                   from store
                 union all
                 select wh loc,
                        RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                   from wh) locs,
                item_master im
          where t.is_zone     = 0
            and t.location    = locs.loc
            and t.item        = im.item
            and im.item_level = im.tran_level
         -- parent/loc
         union all
         select distinct tab.item,
                tab.location,
                tab.loc_type,
                tab.action_date,
                tab.dept,
                tab.selling_retail,
                tab.selling_retail_currency,
                tab.selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(tab.cost) OVER (PARTITION BY tab.item,
                                                       tab.location)
                   else
                      MAX(tab.cost) OVER (PARTITION BY tab.item,
                                                       tab.location)
                end cost,
                MIN(tab.currency_code) OVER (PARTITION BY tab.item,
                                                          tab.location) cost_currency,
                NULL, -- vat_rate
                diff_id
           from (select /*+ CARDINALITY(t 10) */ t.item,
                        im.item tran_item,
                        t.action_date,
                        im.dept,
                        t.selling_retail,
                        t.selling_retail_currency,
                        t.selling_uom,
                        rfc.pricing_cost cost,
                        rfc.currency_code cost_currency,
                        t.diff_id,
                        RANK() OVER (PARTITION BY im.item,
                                                  rfc.location
                                         ORDER BY rfc.active_date desc) rank,
                        rfc.location,
                        locs.loc_type,
                        rfc.currency_code
                   from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                        (select store loc,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                           from store
                         union all
                         select wh loc,
                                RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                           from wh) locs,
                        item_master im,
                        rpm_future_cost rfc
                  where t.is_zone        = 0
                    and t.location       = locs.loc
                    and t.item           = im.item_parent
                    and im.item_level    = im.tran_level
                    and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                    and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    and rfc.item         = im.item
                    and rfc.location     = locs.loc
                    and rfc.loc_type     = locs.loc_type
                    and rfc.active_date <= LP_I_search_criteria.effective_date
                    and (   t.diff_id is NULL
                         or im.diff_1 = t.diff_id
                         or im.diff_2 = t.diff_id
                         or im.diff_3 = t.diff_id
                         or im.diff_4 = t.diff_id)) tab
          where tab.rank = 1
         -- item/zone - Non Custom Tax
         union all
         select distinct
                item,
                zone_id,
                'Z' loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   zone_id)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   zone_id)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      zone_id) cost_currency,
                case
                   when (L_class_level_vat_ind = 'N' or
                        (L_class_level_vat_ind = 'Y' and
                        class_vat_ind = 'Y')) then
                      --
                      AVG(vat_rate) OVER (PARTITION BY item,
                                                       zone_id)
                   else
                      NULL
                end vat_rate,
                NULL -- diff_id
           from (select tab.item,
                        tab.action_date,
                        tab.dept,
                        tab.selling_retail,
                        tab.selling_retail_currency,
                        tab.selling_uom,
                        tab.cost,
                        tab.location,
                        tab.loc_type,
                        tab.zone_id,
                        tab.currency_code,
                        tab.class_vat_ind,
                        vi.vat_rate,
                        RANK() OVER (PARTITION BY vi.item,
                                                  vi.vat_region
                                         ORDER BY vi.active_date desc) vi_rank
                   from (select /*+ CARDINALITY(t 10) */ t.item,
                                t.action_date,
                                im.dept,
                                t.selling_retail,
                                t.selling_retail_currency,
                                t.selling_uom,
                                rfc.pricing_cost cost,
                                rzl.location,
                                locs.loc_type,
                                rzl.zone_id,
                                rfc.currency_code,
                                locs.vat_region,
                                c.class_vat_ind,
                                RANK() OVER (PARTITION BY t.item,
                                                          rzl.location
                                                 ORDER BY rfc.active_date desc) rank
                           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                rpm_zone_location rzl,
                                (select store loc,
                                        RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE num_loc_type,
                                        vat_region
                                   from store
                                 union all
                                 select wh loc,
                                        RPM_CONSTANTS.LOCATION_TYPE_WH loc_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE num_loc_type,
                                        vat_region
                                   from wh) locs,
                                item_master im,
                                rpm_future_cost rfc,
                                class c
                          where L_custom_tax_ind = 0
                            and t.is_zone        = 1
                            and t.location       = rzl.zone_id
                            and rzl.location     = locs.loc
                            and rzl.loc_type     = locs.num_loc_type
                            and t.item           = im.item
                            and im.dept          = c.dept
                            and im.class         = c.class
                            and im.item_level    = im.tran_level
                            and rfc.item         = t.item
                            and rfc.location     = locs.loc
                            and rfc.active_date <= LP_I_search_criteria.effective_date) tab,
                        vat_item vi
                  where tab.rank = 1
                    and vi.item (+)         = tab.item
                    and vi.vat_region (+)   = tab.vat_region
                    and vi.active_date (+) <= tab.action_date)
          where vi_rank = 1
         -- parent/zone - Non Custom
         union all
         select distinct
                item,
                zone_id,
                'Z' loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   zone_id)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   zone_id)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      zone_id) cost_currency,
                case
                   when (L_class_level_vat_ind = 'N' or
                        (L_class_level_vat_ind = 'Y' and
                        class_vat_ind = 'Y')) then
                      --
                      AVG(vat_rate) OVER (PARTITION BY item,
                                                       zone_id)
                   else
                      NULL
                end vat_rate,
                diff_id
           from (select tab.item,
                        tab.zone_id,
                        tab.action_date,
                        tab.dept,
                        tab.selling_retail,
                        tab.selling_retail_currency,
                        tab.selling_uom,
                        tab.cost,
                        tab.currency_code,
                        tab.class_vat_ind,
                        vi.vat_rate,
                        tab.diff_id,
                        RANK() OVER (PARTITION BY vi.item,
                                                  vi.vat_region
                                         ORDER BY vi.active_date desc) vat_rank
                   from (select /*+ CARDINALITY(t 10) */ t.item,
                                rzl.zone_id,
                                rfc.location,
                                rfc.item tran_item,
                                t.action_date,
                                im.dept,
                                t.selling_retail,
                                t.selling_retail_currency,
                                t.selling_uom,
                                rfc.pricing_cost cost,
                                locs.vat_region,
                                rfc.currency_code,
                                t.diff_id,
                                c.class_vat_ind,
                                RANK() OVER (PARTITION BY im.item,
                                                          rfc.location
                                                 ORDER BY rfc.active_date desc) rank
                           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                rpm_zone_location rzl,
                                (select store loc,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE num_loc_type,
                                        vat_region
                                   from store
                                 union all
                                 select wh loc,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE num_loc_type,
                                        vat_region
                                   from wh) locs,
                                item_master im,
                                rpm_future_cost rfc,
                                class c
                          where L_custom_tax_ind = 0
                            and t.is_zone        = 1
                            and t.location       = rzl.zone_id
                            and rzl.location     = locs.loc
                            and rzl.loc_type     = locs.num_loc_type
                            and t.item           = im.item_parent
                            and im.item_level    = im.tran_level
                            and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                            and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            and rfc.item         = im.item
                            and im.dept          = c.dept
                            and im.class         = c.class
                            and rfc.location     = locs.loc
                            and rfc.active_date <= LP_I_search_criteria.effective_date
                            and (   t.diff_id is NULL
                                 or im.diff_1 = t.diff_id
                                 or im.diff_2 = t.diff_id
                                 or im.diff_3 = t.diff_id
                                 or im.diff_4 = t.diff_id)) tab,
                         vat_item vi
                   where tab.rank            = 1
                     and vi.item (+)         = tab.tran_item
                     and vi.vat_region (+)   = tab.vat_region
                     and vi.active_date (+) <= tab.action_date)
          where vat_rank = 1
         -- item/zone complex pack item - Non Custom Tax
         union all
         select /*+ CARDINALITY(t 10) */ distinct t.item,
                rzl.zone_id,
                'Z' loc_type,
                t.action_date,
                im.dept,
                t.selling_retail,
                t.selling_retail_currency,
                t.selling_uom,
                NULL, -- cost,
                NULL, -- cost_currency,
                case
                   when (L_class_level_vat_ind = 'N' or
                        (L_class_level_vat_ind = 'Y' and
                        c.class_vat_ind = 'Y')) then
                      --
                      vi.vat_rate
                   else
                      NULL
                end vat_rate,
                NULL -- diff_id
           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                rpm_zone_location rzl,
                (select store loc,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE num_loc_type,
                        vat_region
                   from store
                 union all
                 select wh loc,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE num_loc_type,
                        vat_region
                   from wh) locs,
                item_master im,
                vat_item vi,
                class c
          where L_custom_tax_ind   = 0
            and t.is_zone          = 1
            and t.location         = rzl.zone_id
            and rzl.location       = locs.loc
            and rzl.loc_type       = locs.num_loc_type
            and t.item             = im.item
            and im.item_level      = im.tran_level
            and vi.item            = im.item
            and im.dept            = c.dept
            and im.class           = c.class
            and vi.vat_region      = locs.vat_region
            and im.pack_ind        = 'Y'
            and im.simple_pack_ind = 'N'
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.orderable_ind   = 'N'
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         -- Item/zone level - Custom Tax
         union all
         select distinct item,
                zone_id,
                'Z' loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   zone_id)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   zone_id)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      zone_id) cost_currency,
                case
                   when (L_class_level_vat_ind = 'N' or
                        (L_class_level_vat_ind = 'Y' and
                        class_vat_ind = 'Y')) then
                      --
                      AVG(vat_rate) OVER (PARTITION BY item,
                                                       zone_id)
                   else
                      NULL
                end vat_rate,
                NULL -- diff_id
           from (select tab.item,
                        tab.action_date,
                        tab.dept,
                        tab.selling_retail,
                        tab.selling_retail_currency,
                        tab.selling_uom,
                        tab.cost,
                        tab.location,
                        tab.loc_type,
                        tab.zone_id,
                        tab.currency_code,
                        tab.class_vat_ind,
                        tax_tbl.O_cum_tax_pct vat_rate
                   from (select /*+ CARDINALITY(t 10) */ t.item,
                                t.action_date,
                                im.dept,
                                t.selling_retail,
                                t.selling_retail_currency,
                                t.selling_uom,
                                rfc.pricing_cost cost,
                                rzl.location,
                                rzl.loc_type,
                                rzl.zone_id,
                                rfc.currency_code,
                                c.class_vat_ind,
                                RANK() OVER (PARTITION BY im.item,
                                                          rfc.location
                                                 ORDER BY rfc.active_date desc) rank
                           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                rpm_zone_location rzl,
                                item_master im,
                                rpm_future_cost rfc,
                                class c
                          where L_custom_tax_ind = 1
                            and t.is_zone        = 1
                            and t.location       = rzl.zone_id
                            and t.item           = im.item
                            and im.dept          = c.dept
                            and im.class         = c.class
                            and im.item_level    = im.tran_level
                            and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                            and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            and rfc.item         = t.item
                            and rfc.location     = rzl.location
                            and rfc.loc_type     = DECODE(rzl.loc_type,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH)
                            and rfc.active_date <= LP_I_search_criteria.effective_date) tab,
                        table(cast(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl
                  where tab.rank        = 1
                    and tab.item        = tax_tbl.I_item
                    and tab.location    = tax_tbl.I_from_entity
                    and tab.loc_type    = DECODE(tax_tbl.I_from_entity_type,
                                                 RPM_CONSTANTS.TAX_LOC_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                    and tab.action_date = tax_tbl.I_effective_from_date)
         -- parent/zone level - Custom Tax
         union all
         select distinct item,
                zone_id,
                'Z' loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   zone_id)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   zone_id)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      zone_id) cost_currency,
                case
                   when (L_class_level_vat_ind = 'N' or
                        (L_class_level_vat_ind = 'Y' and
                        class_vat_ind = 'Y')) then
                      --
                      AVG(vat_rate) OVER (PARTITION BY item,
                                                       zone_id)
                   else
                      NULL
                end vat_rate,
                diff_id
           from (select tab.item,
                        tab.diff_id,
                        tab.action_date,
                        tab.dept,
                        tab.selling_retail,
                        tab.selling_retail_currency,
                        tab.selling_uom,
                        tab.cost,
                        tab.location,
                        tab.loc_type,
                        tab.zone_id,
                        tab.currency_code,
                        tab.class_vat_ind,
                        tax_tbl.O_cum_tax_pct vat_rate
                   from (select /*+ CARDINALITY(t 10) */ t.item,
                                t.action_date,
                                im.dept,
                                t.selling_retail,
                                t.selling_retail_currency,
                                t.selling_uom,
                                t.diff_id,
                                rfc.pricing_cost cost,
                                rzl.location,
                                rzl.loc_type,
                                rzl.zone_id,
                                rfc.currency_code,
                                c.class_vat_ind,
                                RANK() OVER (PARTITION BY im.item,
                                                          rfc.location
                                                 ORDER BY rfc.active_date desc) rank
                           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                rpm_zone_location rzl,
                                item_master im,
                                rpm_future_cost rfc,
                                class c
                          where L_custom_tax_ind = 1
                            and t.is_zone        = 1
                            and t.location       = rzl.zone_id
                            and t.item           = im.item_parent
                            and (   t.diff_id is NULL
                                 or im.diff_1 = t.diff_id
                                 or im.diff_2 = t.diff_id
                                 or im.diff_3 = t.diff_id
                                 or im.diff_4 = t.diff_id)
                            and im.dept          = c.dept
                            and im.class         = c.class
                            and im.item_level    = im.tran_level
                            and im.status        = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                            and im.sellable_ind  = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            and rfc.item         = im.item
                            and rfc.location     = rzl.location
                            and rfc.loc_type     = DECODE(rzl.loc_type,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH)
                            and rfc.active_date <= LP_I_search_criteria.effective_date) tab,
                        table(cast(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl
                  where tab.rank        = 1
                    and tab.item        = tax_tbl.I_item
                    and tab.location    = tax_tbl.I_from_entity
                    and tab.loc_type    = DECODE(tax_tbl.I_from_entity_type,
                                                 RPM_CONSTANTS.TAX_LOC_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                    and tab.action_date = tax_tbl.I_effective_from_date)
         -- Item/Zone Complex Pack Item - Custom Tax
         union all
         select distinct t.item,
                rzl.zone_id,
                'Z' loc_type,
                t.action_date,
                im.dept,
                t.selling_retail,
                t.selling_retail_currency,
                t.selling_uom,
                NULL,
                NULL,
                case
                   when (L_class_level_vat_ind = 'N' or
                        (L_class_level_vat_ind = 'Y' and
                        c.class_vat_ind = 'Y')) then
                      --
                      tax_tbl.O_cum_tax_pct
                   else
                      NULL
                end,
                NULL -- diff_id
           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                table(cast(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl,
                rpm_zone_location rzl,
                item_master im,
                class c
          where L_custom_tax_ind   = 1
            and t.is_zone          = 1
            and t.location         = rzl.zone_id
            and t.item             = tax_tbl.I_item
            and t.action_date      = tax_tbl.I_effective_from_date
            and rzl.location       = tax_tbl.I_from_entity
            and rzl.loc_type       = DECODE(tax_tbl.I_from_entity_type,
                                            RPM_CONSTANTS.TAX_LOC_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
            and t.item             = im.item
            and im.item_level      = im.tran_level
            and im.dept            = c.dept
            and im.class           = c.class
            and im.pack_ind        = 'Y'
            and im.simple_pack_ind = 'N'
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.orderable_ind   = 'N'
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS;

   elsif L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then

      -- If the inquiry is being done above the item/loc level, find the minimum retail data
      -- for item/locs in the criteria.  The item/loc with the minimum retail value will be
      -- used later for tax information when working in a GTAX env.

      delete
        from rpm_mv_min_retail_il_gtt;

      insert into rpm_mv_min_retail_il_gtt(price_event_id,
                                           item,
                                           location,
                                           loc_type,
                                           selling_retail,
                                           action_date)
         select price_event_id,
                item,
                location,
                loc_type,
                selling_retail,
                action_date
           from (select price_event_id, item,
                        location,
                        loc_type,
                        selling_retail,
                        action_date,
                        ROW_NUMBER() OVER (PARTITION BY price_event_id,
                                                        selling_retail
                                               ORDER BY item,
                                                        location) rank
                   from (select price_event_id,
                                MIN(selling_retail) OVER (PARTITION BY price_event_id) min_selling_retail,
                                selling_retail,
                                item,
                                location,
                                zone_node_type loc_type,
                                action_date
                           from rpm_future_retail_gtt)
                   where selling_retail = min_selling_retail)
          where rank = 1;

     insert into rpm_il_markup_gtt(item,
                                   location,
                                   loc_type,
                                   action_date,
                                   dept,
                                   retail,
                                   retail_currency,
                                   selling_uom,
                                   cost,
                                   cost_currency,
                                   vat_rate,
                                   vat_value,
                                   diff_id)
         -- item/loc
         select /*+ CARDINALITY(t 10) */ distinct t.item,
                t.location,
                locs.loc_type,
                t.action_date,
                im.dept,
                t.selling_retail,
                t.selling_retail_currency,
                t.selling_uom,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
           from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                (select store loc,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                   from store
                 union all
                 select wh loc,
                        RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                   from wh) locs,
                item_master im
          where t.is_zone     = 0
            and t.location    = locs.loc
            and t.item        = im.item
            and im.item_level = im.tran_level
         union all
         -- parent/loc
         select distinct item,
                location,
                loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   location)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   location)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      location) cost_currency,
                tax_rate,
                tax_value,
                diff_id
           from (select item,
                        action_date,
                        dept,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        cost,
                        location,
                        loc_type,
                        currency_code,
                        effective_from_date,
                        tax_rate,
                        tax_value,
                        diff_id
                   from (select il_data.item,
                                il_data.action_date,
                                il_data.dept,
                                min_retail.selling_retail,
                                il_data.selling_retail_currency,
                                il_data.selling_uom,
                                il_data.cost,
                                il_data.location,
                                il_data.loc_type,
                                il_data.currency_code,
                                gt.effective_from_date,
                                NVL(gt.cum_tax_pct, 0) tax_rate,
                                NVL(gt.cum_tax_value, 0) tax_value,
                                il_data.diff_id,
                                FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.loc,
                                                                                        gt.item
                                                                               ORDER BY gt.effective_from_date desc) max_date
                           from (select /*+ CARDINALITY(t 10) */ t.item,
                                        t.action_date,
                                        im.dept,
                                        t.selling_retail_currency,
                                        t.selling_uom,
                                        rfc.pricing_cost cost,
                                        rfc.currency_code cost_currency,
                                        rfc.location,
                                        locs.loc_type,
                                        rfc.currency_code,
                                        t.diff_id,
                                        RANK() OVER (PARTITION BY rfc.item,
                                                                  rfc.location
                                                         ORDER BY rfc.active_date desc) rank
                                   from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                        (select store loc,
                                                RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                                           from store
                                         union all
                                         select wh loc,
                                                RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                                           from wh) locs,
                                        item_master im,
                                        rpm_future_cost rfc
                                  where t.is_zone                = 0
                                    and t.location               = locs.loc
                                    and t.item                   = im.item_parent
                                    and im.item_level            = im.tran_level
                                    and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                    and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                    and rfc.item                 = im.item
                                    and rfc.location             = locs.loc
                                    and rfc.loc_type             = locs.loc_type
                                    and rfc.active_date         <= LP_I_search_criteria.effective_date
                                    and (   t.diff_id is NULL
                                         or im.diff_1 = t.diff_id
                                         or im.diff_2 = t.diff_id
                                         or im.diff_3 = t.diff_id
                                         or im.diff_4 = t.diff_id)) il_data,
                                rpm_price_inquiry_gtt inq,
                                gtax_item_rollup gt,
                                rpm_mv_min_retail_il_gtt min_retail
                          where il_data.rank                = 1
                            and inq.item                    = il_data.item
                            and inq.location                = il_data.location
                            and NVL(inq.diff_id, -999)      = NVL(il_data.diff_id, -999)
                            and inq.price_event_id          > 0
                            and min_retail.price_event_id   = inq.price_event_id
                            and gt.item (+)                 = min_retail.item
                            and gt.loc (+)                  = min_retail.location
                            and gt.loc_type (+)             = DECODE(min_retail.loc_type,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                                     RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                            and gt.effective_from_date (+) <= LP_I_search_criteria.effective_date)
                  where NVL(effective_from_date, LP_vdate) = NVL(max_date, LP_vdate))
         union all
         -- item/zone
         select distinct
                item,
                zone_id location,
                'Z' loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   zone_id)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   zone_id)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      zone_id) cost_currency,
                tax_rate,
                tax_value,
                NULL
           from (select item,
                        action_date,
                        dept,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        cost,
                        zone_id,
                        currency_code,
                        tax_rate,
                        tax_value
                   from (select il_data.item,
                                il_data.action_date,
                                il_data.dept,
                                min_retail.selling_retail,
                                il_data.selling_retail_currency,
                                il_data.selling_uom,
                                il_data.cost,
                                il_data.zone_id,
                                il_data.currency_code,
                                gt.effective_from_date,
                                NVL(gt.cum_tax_pct, 0) tax_rate,
                                NVL(gt.cum_tax_value, 0) tax_value,
                                FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.loc,
                                                                                        gt.item
                                                                               ORDER BY gt.effective_from_date desc) max_date
                           from (select /*+ CARDINALITY(t 10) */ t.item,
                                        t.action_date,
                                        im.dept,
                                        t.selling_retail,
                                        t.selling_retail_currency,
                                        t.selling_uom,
                                        rfc.pricing_cost cost,
                                        t.location zone_id,
                                        rfc.currency_code,
                                        RANK() OVER (PARTITION BY rfc.item,
                                                                  rfc.location
                                                         ORDER BY rfc.active_date desc) rank
                                   from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                        (select store loc,
                                                RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_STORE num_loc_type
                                           from store
                                         union all
                                         select wh loc,
                                                RPM_CONSTANTS.LOCATION_TYPE_WH loc_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE num_loc_type
                                           from wh) locs,
                                        item_master im,
                                        rpm_future_cost rfc,
                                        rpm_zone_location rzl
                                  where t.is_zone          = 1
                                    and t.location         = rzl.zone_id
                                    and rzl.location       = locs.loc
                                    and rzl.loc_type       = locs.num_loc_type
                                    and t.item             = im.item
                                    and im.item_level      = im.tran_level
                                    and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                    and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                    and rfc.item           = im.item
                                    and rfc.location       = locs.loc
                                    and rfc.loc_type       = locs.loc_type
                                    and rfc.active_date   <= LP_I_search_criteria.effective_date) il_data,
                                rpm_price_inquiry_gtt inq,
                                gtax_item_rollup gt,
                                rpm_mv_min_retail_il_gtt min_retail
                          where il_data.rank                = 1
                            and inq.item                    = il_data.item
                            and inq.location                = il_data.zone_id
                            and inq.price_event_id          > 0
                            and min_retail.price_event_id   = inq.price_event_id
                            and gt.item (+)                 = min_retail.item
                            and gt.loc (+)                  = min_retail.location
                            and gt.loc_type (+)             = DECODE(min_retail.loc_type,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                                     RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                            and gt.effective_from_date (+) <= LP_I_search_criteria.effective_date)
                  where NVL(effective_from_date, LP_vdate) = NVL(max_date, LP_vdate))
         union all
         --parent/zone
         select distinct
                item,
                location,
                loc_type,
                action_date,
                dept,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                case
                   when L_cost_calc_method = RPM_CONSTANTS.PC_AVERAGE_LOCATION_COST then
                      AVG(cost) OVER (PARTITION BY item,
                                                   location)
                   else
                      MAX(cost) OVER (PARTITION BY item,
                                                   location)
                end cost,
                MIN(currency_code) OVER (PARTITION BY item,
                                                      location) cost_currency,
                tax_rate,
                tax_value,
                diff_id
           from (select item,
                        action_date,
                        dept,
                        selling_retail,
                        selling_retail_currency,
                        selling_uom,
                        cost,
                        location,
                        loc_type,
                        currency_code,
                        tax_rate,
                        tax_value,
                        diff_id
                   from (select il_data.item,
                                il_data.action_date,
                                il_data.dept,
                                min_retail.selling_retail,
                                il_data.selling_retail_currency,
                                il_data.selling_uom,
                                il_data.cost,
                                il_data.zone_id location,
                                'Z' loc_type,
                                il_data.currency_code,
                                gt.effective_from_date,
                                NVL(gt.cum_tax_pct, 0) tax_rate,
                                NVL(gt.cum_tax_value, 0) tax_value,
                                il_data.diff_id,
                                FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY gt.loc,
                                                                                        gt.item
                                                                               ORDER BY gt.effective_from_date desc) max_date
                           from (select /*+ CARDINALITY(t 10) */ t.item,
                                        t.action_date,
                                        im.dept,
                                        t.selling_retail_currency,
                                        t.selling_uom,
                                        rfc.pricing_cost cost,
                                        t.location zone_id,
                                        rfc.currency_code,
                                        t.diff_id,
                                        RANK() OVER (PARTITION BY im.item,
                                                                  rfc.location
                                                         ORDER BY rfc.active_date DESC) rank
                                   from table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                                        (select store loc,
                                                RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_STORE num_loc_type
                                           from store
                                         union all
                                         select wh loc,
                                                RPM_CONSTANTS.LOCATION_TYPE_WH loc_type,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE num_loc_type
                                           from wh) locs,
                                        item_master im,
                                        rpm_zone_location rzl,
                                        rpm_future_cost rfc
                                  where t.is_zone       = 1
                                    and t.location      = rzl.zone_id
                                    and rzl.location    = locs.loc
                                    and rzl.loc_type    = locs.num_loc_type
                                    and t.item          = im.item_parent
                                    and im.item_level   = im.tran_level
                                    and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                    and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                    and rfc.item        = im.item
                                    and rfc.location    = locs.loc
                                    and rfc.loc_type    = locs.loc_type
                                    and rfc.active_date <= LP_I_search_criteria.effective_date
                                    and (   t.diff_id is NULL
                                         or im.diff_1 = t.diff_id
                                         or im.diff_2 = t.diff_id
                                         or im.diff_3 = t.diff_id
                                         or im.diff_4 = t.diff_id)) il_data,
                                rpm_price_inquiry_gtt inq,
                                gtax_item_rollup gt,
                                rpm_mv_min_retail_il_gtt min_retail
                          where il_data.rank                = 1
                            and inq.item                    = il_data.item
                            and inq.location                = il_data.zone_id
                            and NVL(inq.diff_id, -999)      = NVL(il_data.diff_id, -999)
                            and inq.price_event_id          > 0
                            and min_retail.price_event_id   = inq.price_event_id
                            and gt.item (+)                 = min_retail.item
                            and gt.loc (+)                  = min_retail.location
                            and gt.loc_type (+)             = DECODE(min_retail.loc_type,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                                     RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                            and gt.effective_from_date (+) <= LP_I_search_criteria.effective_date)
                  where NVL(effective_from_date, LP_vdate) = NVL(max_date, LP_vdate))
         -- item/zone complex pack item
         union all
         select /*+ CARDINALITY(t 10) */ distinct t.item,
                rz.zone_id,
                'Z' loc_type,
                t.action_date,
                im.dept,
                t.selling_retail,
                t.selling_retail_currency,
                t.selling_uom,
                NULL,
                NULL,
                gt.cum_tax_pct,
                gt.cum_tax_value,
                NULL
          from  table(cast(L_prices as OBJ_PRC_INQ_TBL)) t,
                rpm_zone rz,
                rpm_zone_location rzl,
                (select store loc,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE num_loc_type,
                        RPM_CONSTANTS.TAX_LOC_TYPE_STORE gt_loc_type
                   from store
                 union all
                 select wh loc,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE num_loc_type,
                        RPM_CONSTANTS.TAX_LOC_TYPE_WH gt_loc_type
                   from wh) locs,
                item_master im,
                gtax_item_rollup gt
          where t.is_zone          = 1
            and t.location         = rz.zone_id
            and rz.zone_id         = rzl.zone_id
            and rzl.location       = locs.loc
            and rzl.loc_type       = locs.num_loc_type
            and t.item             = im.item
            and im.item_level      = im.tran_level
            and gt.item            = im.item
            and gt.loc             = locs.loc
            and gt.loc_type        = locs.gt_loc_type
            and im.pack_ind        = 'Y'
            and im.simple_pack_ind = 'N'
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and im.orderable_ind   = 'N'
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS;

   end if;

   if L_default_tax_type IN (RPM_CONSTANTS.GTAX_TAX_TYPE,
                             RPM_CONSTANTS.SVAT_TAX_TYPE,
                             RPM_CONSTANTS.SALES_TAX_TYPE) then

      if RPM_MARKUP_CALC_SQL.CALCULATE_MARKUP(O_error_msg) = 0 then
         O_return_code := 0;
         return;
      end if;

      open C_ADD_MARKUP_INFO;
      fetch C_ADD_MARKUP_INFO BULK COLLECT into O_prices;
      close C_ADD_MARKUP_INFO;

   else

      O_prices := L_prices;

   end if;

   O_return_code := 1;

   return;

EXCEPTION

   when OTHERS then
      O_return_code := 0;
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return;

END BUILD_PRICE_INQ_VO;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION GET_PRICE_INQ_VO (O_error_msg         OUT VARCHAR2,
                           O_prices            OUT OBJ_PRC_INQ_TBL,
                           I_search_criteria   IN  OBJ_PRICE_INQ_SEARCH_TBL)
RETURN NUMBER AS

   L_program  VARCHAR2(50)  := 'RPM_PRICE_INQUIRY_SQL.GET_PRICE_INQ_VO';

   L_return_code         NUMBER        := 0;
   L_error_msg           VARCHAR2(200) := NULL;

BEGIN

   LP_I_search_criteria := I_search_criteria(1);

   POP_GTT(L_return_code,
           L_error_msg);

   if L_return_code = 0 then
      O_error_msg := L_error_msg;
      return 0;
   end if;

   POP_FUTURE_RETAIL_GTT (L_return_code,
                          L_error_msg);

   if L_return_code = 0 then
      O_error_msg := L_error_msg;
      return 0;
   end if;

   BUILD_PRICE_INQ_VO (L_return_code,
                       L_error_msg,
                       O_prices);

   if L_return_code = 0 then
      O_error_msg := L_error_msg;
      return 0;
   end if;

   delete
      from rpm_future_retail_gtt;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg   := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                          SQLERRM,
                                          L_program,
                                          TO_CHAR(SQLCODE));
      return 0;

END GET_PRICE_INQ_VO;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION GET_CUST_SEGMENT_PROMOS(O_error_msg          OUT VARCHAR2,
                                 O_pc_tbl             OUT OBJ_CUST_SEG_PROMO_TBL,
                                 I_search_criteria IN     OBJ_CUST_SEG_PROMO_REQ_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PRICE_INQUIRY_SQL.GET_CUST_SEGMENT_PROMOS';

   L_parent_item_ind NUMBER(2) := NULL;

   L_specific_cust_types NUMBER := 0;

   cursor C_PARENT is
      select DECODE(im.item_level - im.tran_level,
                    0, 0,
                    1)
        from item_master im
       where im.item = I_search_criteria(1).item;

   cursor C_OUTPUT is
      select OBJ_CUST_SEG_PROMO_REC(in2.customer_type,
                                    in2.customer_segment_desc,
                                    I_search_criteria(1).item,
                                    I_search_criteria(1).location,
                                    in2.loc_display_id,
                                    AVG(in2.promo_retail),
                                    MIN(in2.promo_uom),
                                    MIN(in2.complex_promo_ind),
                                    case
                                       when MIN(in2.promo_retail) = MAX(in2.promo_retail) then
                                          0
                                       else
                                          1
                                    end,
                                    MIN(in2.promo_retail_currency),
                                    L_parent_item_ind)
        from (select in1.customer_type,
                     in1.customer_segment_desc,
                     in1.item,
                     in1.location,
                     in1.loc_display_id,
                     in1.promo_retail,
                     in1.promo_uom,
                     in1.complex_promo_ind,
                     in1.promo_retail_currency,
                     in1.rank_value,
                     in1.hier_rank,
                     MAX(in1.hier_rank) OVER (PARTITION BY in1.item,
                                                           in1.location) max_rank
                from (--cur_hier_level = 'IL'
                      select fr.customer_type,
                             cs.customer_segment_desc,
                             gtt.item item,
                             gtt.fr_location location,
                             gtt.loc_display_id,
                             fr.promo_retail,
                             fr.promo_uom,
                             fr.complex_promo_ind,
                             fr.promo_retail_currency,
                             I_search_criteria(1).item search_item,
                             I_search_criteria(1).location search_location,
                             RANK() OVER (PARTITION BY fr.customer_type,
                                                       fr.item,
                                                       fr.diff_id,
                                                       fr.location,
                                                       fr.zone_node_type
                                              ORDER BY fr.action_date desc) rank_value,
                             3 hier_rank
                        from rpm_cust_segment_promo_fr fr,
                             rpm_price_inquiry_gtt gtt,
                             customer_segments cs
                       where fr.dept                        = gtt.dept
                         and fr.item                        = gtt.item
                         and NVL(fr.diff_id, '-999')        = NVL(gtt.diff_id, '-999')
                         and fr.location                    = gtt.fr_location
                         and fr.zone_node_type              = gtt.fr_zone_node_type
                         and fr.action_date                 <= I_search_criteria(1).action_date
                         and fr.cur_hier_level              = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                         and (   (    L_specific_cust_types = 1
                                  and fr.customer_type      IN (select VALUE(ids)
                                                                  from table(cast(I_search_criteria(1).customer_types as OBJ_NUMERIC_ID_TABLE)) ids))
                              or L_specific_cust_types      = 0)
                         and fr.customer_type               = cs.customer_segment_id
                      union all
                      --cur_hier_level = 'IZ'
                      select fr.customer_type,
                             cs.customer_segment_desc,
                             gtt.item item,
                             gtt.fr_location location,
                             gtt.loc_display_id,
                             fr.promo_retail,
                             fr.promo_uom,
                             fr.complex_promo_ind,
                             fr.promo_retail_currency,
                             I_search_criteria(1).item search_item,
                             I_search_criteria(1).location search_location,
                             RANK() OVER (PARTITION BY fr.customer_type,
                                                       fr.item,
                                                       fr.diff_id,
                                                       fr.location,
                                                       fr.zone_node_type
                                              ORDER BY fr.action_date desc) rank_value,
                             2 hier_rank
                        from rpm_cust_segment_promo_fr fr,
                             rpm_price_inquiry_gtt gtt,
                             customer_segments cs
                       where fr.dept                        = gtt.dept
                         and fr.item                        = gtt.item
                         and NVL(fr.diff_id, '-999')        = NVL(gtt.diff_id, '-999')
                         and fr.location                    = gtt.fr_zone_id
                         and fr.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and fr.cur_hier_level              = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                         and fr.action_date                 <= I_search_criteria(1).action_date
                         and (   (    L_specific_cust_types = 1
                                  and fr.customer_type      IN (select VALUE(ids)
                                                                  from table(cast(I_search_criteria(1).customer_types as OBJ_NUMERIC_ID_TABLE)) ids))
                              or L_specific_cust_types      = 0)
                         and fr.customer_type               = cs.customer_segment_id
                      union all
                      --cur_hier_level = 'DL'
                      select fr.customer_type,
                             cs.customer_segment_desc,
                             gtt.item item,
                             gtt.fr_location location,
                             gtt.loc_display_id,
                             fr.promo_retail,
                             fr.promo_uom,
                             fr.complex_promo_ind,
                             fr.promo_retail_currency,
                             I_search_criteria(1).item search_item,
                             I_search_criteria(1).location search_location,
                             RANK() OVER (PARTITION BY fr.customer_type,
                                                       fr.item,
                                                       fr.diff_id,
                                                       fr.location,
                                                       fr.zone_node_type
                                              ORDER BY fr.action_date desc) rank_value,
                             2 hier_rank
                        from rpm_cust_segment_promo_fr fr,
                             rpm_price_inquiry_gtt gtt,
                             customer_segments cs
                       where fr.dept                        = gtt.dept
                         and fr.item                        = gtt.item_parent
                         and fr.diff_id                     = gtt.diff_id
                         and fr.location                    = gtt.fr_location
                         and fr.zone_node_type              = gtt.fr_zone_node_type
                         and fr.action_date                 <= I_search_criteria(1).action_date
                         and fr.cur_hier_level              = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                         and (   (    L_specific_cust_types = 1
                                  and fr.customer_type      IN (select VALUE(ids)
                                                                  from table(cast(I_search_criteria(1).customer_types as OBJ_NUMERIC_ID_TABLE)) ids))
                              or L_specific_cust_types      = 0)
                         and fr.customer_type               = cs.customer_segment_id
                      union all
                      --cur_hier_level = 'DZ'
                      select fr.customer_type,
                             cs.customer_segment_desc,
                             gtt.item item,
                             gtt.fr_location location,
                             gtt.loc_display_id,
                             fr.promo_retail,
                             fr.promo_uom,
                             fr.complex_promo_ind,
                             fr.promo_retail_currency,
                             I_search_criteria(1).item search_item,
                             I_search_criteria(1).location search_location,
                             RANK() OVER (PARTITION BY fr.customer_type,
                                                       fr.item,
                                                       fr.diff_id,
                                                       fr.location,
                                                       fr.zone_node_type
                                              ORDER BY fr.action_date desc) rank_value,
                             1 hier_rank
                        from rpm_cust_segment_promo_fr fr,
                             rpm_price_inquiry_gtt gtt,
                             customer_segments cs
                       where fr.dept                        = gtt.dept
                         and fr.item                        = gtt.item_parent
                         and fr.diff_id                     = gtt.diff_id
                         and fr.location                    = gtt.fr_zone_id
                         and fr.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and fr.cur_hier_level              = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and fr.action_date                 <= I_search_criteria(1).action_date
                         and (   (    L_specific_cust_types = 1
                                  and fr.customer_type      IN (select VALUE(ids)
                                                                  from table(cast(I_search_criteria(1).customer_types as OBJ_NUMERIC_ID_TABLE)) ids))
                              or L_specific_cust_types      = 0)
                         and fr.customer_type               = cs.customer_segment_id
                      union all
                      --cur_hier_level = 'PL'
                      select fr.customer_type,
                             cs.customer_segment_desc,
                             gtt.item item,
                             gtt.fr_location location,
                             gtt.loc_display_id,
                             fr.promo_retail,
                             fr.promo_uom,
                             fr.complex_promo_ind,
                             fr.promo_retail_currency,
                             I_search_criteria(1).item search_item,
                             I_search_criteria(1).location search_location,
                             RANK() OVER (PARTITION BY fr.customer_type,
                                                       fr.item,
                                                       fr.diff_id,
                                                       fr.location,
                                                       fr.zone_node_type
                                              ORDER BY fr.action_date desc) rank_value,
                             1 hier_rank
                        from rpm_cust_segment_promo_fr fr,
                             rpm_price_inquiry_gtt gtt,
                             customer_segments cs
                       where fr.dept                        = gtt.dept
                         and fr.item                        = gtt.item_parent
                         and fr.diff_id                     is NULL
                         and fr.location                    = gtt.fr_location
                         and fr.zone_node_type              = gtt.fr_zone_node_type
                         and fr.cur_hier_level              = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                         and fr.action_date                 <= I_search_criteria(1).action_date
                         and (   (    L_specific_cust_types = 1
                                  and fr.customer_type      IN (select VALUE(ids)
                                                                  from table(cast(I_search_criteria(1).customer_types as OBJ_NUMERIC_ID_TABLE)) ids))
                              or L_specific_cust_types      = 0)
                         and fr.customer_type               = cs.customer_segment_id
                      union all
                      --cur_hier_level = 'PZ'
                      select fr.customer_type,
                             cs.customer_segment_desc,
                             gtt.item item,
                             gtt.fr_location location,
                             gtt.loc_display_id,
                             fr.promo_retail,
                             fr.promo_uom,
                             fr.complex_promo_ind,
                             fr.promo_retail_currency,
                             I_search_criteria(1).item search_item,
                             I_search_criteria(1).location search_location,
                             RANK() OVER (PARTITION BY fr.customer_type,
                                                       fr.item,
                                                       fr.diff_id,
                                                       fr.location,
                                                       fr.zone_node_type
                                              ORDER BY fr.action_date desc) rank_value,
                             0 hier_rank
                        from rpm_cust_segment_promo_fr fr,
                             rpm_price_inquiry_gtt gtt,
                             customer_segments cs
                       where fr.dept                        = gtt.dept
                         and fr.item                        = gtt.item_parent
                         and fr.diff_id                     is NULL
                         and fr.location                    = gtt.fr_zone_id
                         and fr.zone_node_type              = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                         and fr.cur_hier_level              = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                         and fr.action_date                 <= I_search_criteria(1).action_date
                         and (   (    L_specific_cust_types = 1
                                  and fr.customer_type      IN (select VALUE(ids)
                                                                  from table(cast(I_search_criteria(1).customer_types as OBJ_NUMERIC_ID_TABLE)) ids))
                              or L_specific_cust_types      = 0)
                         and fr.customer_type               = cs.customer_segment_id
                 ) in1
               where in1.rank_value = 1) in2
       where in2.hier_rank = in2.max_rank
       group by in2.customer_type,
                in2.customer_segment_desc,
                in2.loc_display_id;

BEGIN

   delete
     from rpm_price_inquiry_gtt;

   if I_search_criteria(1).customer_types is NOT NULL and
      I_search_criteria(1).customer_types.COUNT > 0 then
      ---
      L_specific_cust_types := 1;
   end if;

   insert into rpm_price_inquiry_gtt (price_event_id,
                                      item,
                                      item_parent,
                                      diff_id,
                                      dept,
                                      class,
                                      subclass,
                                      location,
                                      loc_type,
                                      loc_display_id,
                                      fr_zone_node_type,
                                      fr_location,
                                      fr_zone_id)
   select t.price_event_id,
          t.item,
          t.item_parent,
          t.diff_id,
          t.dept,
          t.class,
          t.subclass,
          t.loc,
          t.loc_type,
          t.loc_display_id,
          t.fr_zone_node_type,
          t.fr_location,
          t.fr_zone_id
     from (select ROWNUM price_event_id,
                  item_sub.item,
                  item_sub.item_parent,
                  item_sub.diff_id,
                  item_sub.dept,
                  item_sub.class,
                  item_sub.subclass,
                  loc_sub.loc,
                  loc_sub.loc_type,
                  loc_sub.loc_display_id,
                  loc_sub.loc_type fr_zone_node_type,
                  loc_sub.loc fr_location,
                  NULL fr_zone_id
             from (select im.dept,
                          im.class,
                          im.subclass,
                          im.item,
                          im.item_parent,
                          DECODE(im.item_parent,
                                 NULL, NULL,
                                 im.diff_1) diff_id
                     from item_master im
                    where im.item       = I_search_criteria(1).item
                      and im.item_level = im.tran_level
                   union all
                   select im.dept,
                          im.class,
                          im.subclass,
                          im.item,
                          im.item_parent,
                          im.diff_1 diff_id
                     from item_master im
                    where im.item_parent               = I_search_criteria(1).item
                      and im.item_level                = im.tran_level
                      and I_search_criteria(1).diff_id is NULL
                   union all
                   select im.dept,
                          im.class,
                          im.subclass,
                          im.item,
                          im.item_parent,
                          im.diff_1 diff_id
                     from item_master im
                    where im.item_parent = I_search_criteria(1).item
                      and im.item_level  = im.tran_level
                      and (   im.diff_1  = I_search_criteria(1).diff_id
                           or im.diff_2  = I_search_criteria(1).diff_id
                           or im.diff_3  = I_search_criteria(1).diff_id
                           or im.diff_4  = I_search_criteria(1).diff_id)) item_sub,
                  (select l.loc,
                          l.loc_type,
                          l.loc loc_display_id
                     from (select store loc,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE loc_type
                             from store
                           union all
                           select wh loc,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE loc_type
                             from wh) l
                    where I_search_criteria(1).is_zone  = 0
                      and I_search_criteria(1).location = l.loc
                   union all
                   select zl.location loc,
                          zl.loc_type,
                          z.zone_display_id loc_display_id
                     from rpm_zone_location zl,
                          rpm_zone z
                    where I_search_criteria(1).is_zone  = 1
                      and I_search_criteria(1).location = z.zone_id
                      and z.zone_id                     = zl.zone_id) loc_sub) t,
          rpm_item_loc ril
    where ril.dept = t.dept
      and ril.item = t.item
      and ril.loc  = t.loc;

   merge into rpm_price_inquiry_gtt target
   using (select pi.item,
                 pi.location,
                 rzl.loc_type,
                 rzl.zone_id
            from rpm_price_inquiry_gtt pi,
                 rpm_merch_retail_def_expl mrd,
                 rpm_zone rz,
                 rpm_zone_location rzl
           where pi.dept                = mrd.dept
             and pi.class               = mrd.class
             and pi.subclass            = mrd.subclass
             and mrd.regular_zone_group = rz.zone_group_id
             and rz.zone_id             = rzl.zone_id
             and rzl.location           = pi.location
             and rzl.loc_type           = pi.loc_type) use_this
   on (    target.item     = use_this.item
       and target.location = use_this.location
       and target.loc_type = use_this.loc_type)
   when MATCHED then
      update
         set target.fr_zone_id = use_this.zone_id;

   open C_PARENT;
   fetch C_PARENT into L_parent_item_ind;
   close C_PARENT;

   open C_OUTPUT;
   fetch C_OUTPUT BULK COLLECT into O_pc_tbl;
   close C_OUTPUT;

   return  1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END GET_CUST_SEGMENT_PROMOS;

-----------------------------------------------------------------------------------------------------------------------
END RPM_PRICE_INQUIRY_SQL;
/