CREATE OR REPLACE PACKAGE BODY RPM_CC_PROM_COMP_CNT AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_PROM_COMP_CNT.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_retail_id OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_price_id  OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();
   L_cnt       OBJ_NUMERIC_ID_TABLE := OBJ_NUMERIC_ID_TABLE();

   L_multi_prom_ind             RPM_SYSTEM_OPTIONS.MULTI_ITEM_LOC_PROMO_IND%TYPE   := NULL;
   L_max_ovrlp_prom_comp_detail RPM_SYSTEM_OPTIONS.MAX_OVRLP_PROM_COMP_DETAIL%TYPE := NULL;

   L_vdate DATE := GET_VDATE;

   cursor C_CHECK is
      select inner.price_event_id,
             inner.future_retail_id,
             COUNT(1) cnt
        from (select distinct gtt.price_event_id,
                     gtt.item,
                     gtt.location,
                     gtt.action_date,
                     gtt.future_retail_id,
                     ilex.promo_dtl_id
                from rpm_future_retail_gtt gtt,
                     rpm_promo_item_loc_expl_gtt ilex
               where gtt.price_event_id           NOT IN (select numeric_id
                                                            from numeric_id_gtt)
                 and ilex.price_event_id          = gtt.price_event_id
                 and ilex.item                    = gtt.item
                 and ilex.location                = gtt.location
                 and NVL(ilex.diff_id, '-999')    = NVL(gtt.diff_id, '-999')
                 and ilex.zone_node_type          = gtt.zone_node_type
                 and NVL(ilex.deleted_ind, 0)    != 1
                 and (   ilex.detail_change_type is NULL
                      or ilex.detail_change_type != RPM_CONSTANTS.RETAIL_EXCLUDE)
                 and ilex.customer_type          is NULL
                 and ilex.detail_start_date      <= gtt.action_date
                 and gtt.action_date             <= NVL(ilex.detail_end_date, gtt.action_date)
                 and gtt.action_date             >= L_vdate
                 and (   ilex.detail_apply_to_code         = RPM_CONSTANTS.REGULAR_AND_CLEARANCE
                      or (    ilex.detail_apply_to_code    = RPM_CONSTANTS.REGULAR_ONLY
                          and (   gtt.clearance_id         is NULL
                               or (    gtt.clearance_id    is NOT NULL
                                   and gtt.clear_start_ind IN (RPM_CONSTANTS.END_IND,
                                                               RPM_CONSTANTS.START_END_IND))))
                      or (    ilex.detail_apply_to_code    = RPM_CONSTANTS.CLEARANCE_ONLY
                          and (    gtt.clearance_id        is NOT NULL
                               and gtt.clear_start_ind     NOT IN (RPM_CONSTANTS.END_IND,
                                                                   RPM_CONSTANTS.START_END_IND))))
                 and NOT EXISTS (select 1
                                   from rpm_promo_item_loc_expl_gtt ilex2
                                  where ilex2.price_event_id         = ilex.price_event_id
                                    and ilex2.item                   = ilex.item
                                    and ilex2.location               = ilex.location
                                    and NVL(ilex2.diff_id, -999)     = NVL(ilex.diff_id, -999)
                                    and ilex2.zone_node_type         = ilex.zone_node_type
                                    and ilex2.detail_start_date      = ilex.detail_start_date
                                    and ilex2.detail_end_date        = ilex.detail_end_date
                                    and (   (    ilex.customer_type  is NULL
                                             and ilex2.customer_type is NULL)
                                         or ilex.customer_type       = ilex2.customer_type)
                                    and ilex2.detail_change_type     is NOT NULL
                                    and ilex2.detail_change_type     = RPM_CONSTANTS.RETAIL_EXCLUDE
                                    and (   (    ilex2.exception_parent_id is NOT NULL
                                             and ilex.promo_id             = ilex2.promo_id
                                             and ilex.promo_comp_id        = ilex2.promo_comp_id)
                                         or (    ilex2.exception_parent_id is NULL
                                             and ilex.promo_id             = ilex2.promo_id)))) inner
       group by inner.price_event_id,
                inner.future_retail_id
       having COUNT(1) > 1;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999, NULL, NULL, NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      select multi_item_loc_promo_ind,
             max_ovrlp_prom_comp_detail
        into L_multi_prom_ind,
             L_max_ovrlp_prom_comp_detail
        from rpm_system_options;

      delete
        from numeric_id_gtt;

      insert into numeric_id_gtt (numeric_id)
         select distinct ccet.price_event_id
           from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet;

      open C_CHECK;

      loop
         fetch C_CHECK BULK COLLECT into L_price_id,
                                         L_retail_id,
                                         L_cnt limit 10000;

         for i IN 1..L_cnt.COUNT loop

           if L_multi_prom_ind = RPM_CONSTANTS.BOOLEAN_FALSE then

              L_error_rec := CONFLICT_CHECK_ERROR_REC(L_price_id(i),
                                                      L_retail_id(i),
                                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                                      'event_causes_multiple_promotion_components_to_be_active');

              if IO_error_table is NULL then
                 IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
              else
                 IO_error_table.EXTEND;
                 IO_error_table(IO_error_table.COUNT) := L_error_rec;
              end if;

           elsif L_cnt(i) > L_max_ovrlp_prom_comp_detail then

             L_error_rec := CONFLICT_CHECK_ERROR_REC(L_price_id(i),
                                                     L_retail_id(i),
                                                     RPM_CONSTANTS.CONFLICT_ERROR,
                                                     'event_causes_multiple_promotion_components_more_than_allowed');
             if IO_error_table is NULL then
                 IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
             else
                 IO_error_table.EXTEND;
                 IO_error_table(IO_error_table.COUNT) := L_error_rec;
             end if;

           end if;

         end loop;

         EXIT when C_CHECK%NOTFOUND;

      end loop;

      close C_CHECK;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

