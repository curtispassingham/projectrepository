CREATE OR REPLACE PACKAGE RPM_SYSTEM_OPTIONS_DEF_SQL AS
--------------------------------------------------------
FUNCTION RESET_GLOBALS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS_DEF_ID(O_system_options_def_id     IN OUT rpm_system_options_def.system_options_def_id%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_CURRENCY(O_def_currency     IN OUT rpm_system_options_def.def_currency%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_PRICE_CHANGE_DIFF_TYPE(O_def_price_change_diff_type     IN OUT
rpm_system_options_def.def_price_change_diff_type%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_PC_ITEM_LEVEL(O_def_price_change_item_level     IN OUT
rpm_system_options_def.def_price_change_item_level%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_PRICE_CHANGE_TYPE(O_def_price_change_type     IN OUT rpm_system_options_def.def_price_change_type%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_PRICING_STRATEGY(O_def_pricing_strategy     IN OUT rpm_system_options_def.def_pricing_strategy%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_MAINT_MARGIN_METHOD(O_def_maint_margin_method     IN OUT
rpm_system_options_def.def_maint_margin_method%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEF_WKSHT_PROMO_CONST_IND(O_def_wksht_promo_const_ind     IN OUT
rpm_system_options_def.def_wksht_promo_const_ind%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_LOCK_VERSION(O_lock_version     IN OUT rpm_system_options_def.lock_version%TYPE,
                   O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
END;
/

