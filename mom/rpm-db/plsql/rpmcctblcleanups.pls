CREATE OR REPLACE PACKAGE RPM_CC_TBL_CLEAN_UP AS

---------------------------------------------------------
FUNCTION PURGE_UNUSED_CC_TBL_RECS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

---------------------------------------------------------

END RPM_CC_TBL_CLEAN_UP;
/
