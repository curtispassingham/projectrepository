CREATE OR REPLACE PACKAGE RPM_PE_SKULIST_SQL AS

-------------------------------------------------------------------

FUNCTION SAVE_PE_SKULIST(O_error_msg           OUT VARCHAR2,
                         I_price_event_id   IN     NUMBER,
                         I_price_event_type IN     VARCHAR2,
                         I_skulist          IN     NUMBER)
RETURN NUMBER;

-------------------------------------------------------------------

FUNCTION PURGE_PE_SKULIST(O_error_msg           OUT VARCHAR2,
                          I_price_event_id   IN     NUMBER,
                          I_price_event_type IN     VARCHAR2,
                          I_skulist          IN     NUMBER)
RETURN NUMBER;

-------------------------------------------------------------------

FUNCTION COPY_PROMO_SKULIST(O_error_msg                OUT VARCHAR2,
                            I_existing_promo_dtl_id IN     NUMBER,
                            I_new_promo_dtl_id      IN     NUMBER)
RETURN NUMBER;

-------------------------------------------------------------------
FUNCTION VALIDATE_TXN_EXCLUSION_SKULIST(O_error_msg                OUT VARCHAR2,
                                        O_some_data_removed_ind    OUT NUMBER,
                                        O_all_data_removed_ind     OUT NUMBER,
                                        I_promo_dtl_id          IN     NUMBER,
                                        I_buylists              IN     OBJ_MERCH_NODE_TBL,
                                        I_skulist_id            IN     NUMBER,
                                        I_user_id               IN     VARCHAR2)
RETURN NUMBER;

-------------------------------------------------------------------

FUNCTION SAVE_TXN_EXCLUSION_SKULIST(O_error_msg        OUT VARCHAR2,
                                    I_promo_dtl_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;

-------------------------------------------------------------------

FUNCTION PURGE_TXN_EXCL_STAGE_WS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;

-------------------------------------------------------------------

FUNCTION GET_SKULIST_DCS(O_error_msg         OUT VARCHAR2,
                         O_merch_data        OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                         I_price_event_id IN     NUMBER,
                         I_skulist_ids    IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER;

-------------------------------------------------------------------
END RPM_PE_SKULIST_SQL;
/
