CREATE OR REPLACE PACKAGE BODY RPM_PE_SKULIST_SQL AS

FUNCTION SAVE_PE_SKULIST(O_error_msg           OUT VARCHAR2,
                         I_price_event_id   IN     NUMBER,
                         I_price_event_type IN     VARCHAR2,
                         I_skulist          IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_PE_SKULIST_SQL.SAVE_PE_SKULIST';

   L_pe_sl_exists  NUMBER(1) := 0;

   cursor C_PC_SL_EXISTS is
      select 1
        from dual
       where EXISTS (select 'x'
                       from rpm_price_change_skulist
                      where price_event_id = I_price_event_id
                        and skulist        = I_skulist);

   cursor C_CL_SL_EXISTS is
      select 1
        from dual
       where EXISTS (select 'x'
                       from rpm_clearance_skulist
                      where price_event_id = I_price_event_id
                        and skulist        = I_skulist);

   cursor C_PR_SL_EXISTS is
      select 1
        from dual
       where EXISTS (select 'x'
                       from rpm_promo_dtl_skulist
                      where price_event_id = I_price_event_id
                        and skulist        = I_skulist);

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_PC_SL_EXISTS;
      fetch C_PC_SL_EXISTS into L_pe_sl_exists;
      close C_PC_SL_EXISTS;

      if L_pe_sl_exists != 1 then

         insert into rpm_price_change_skulist
            (price_event_id,
             skulist,
             item,
             item_level,
             item_desc)
         select I_price_event_id,
                I_skulist,
                sld.item,
                DECODE(sld.item_level,
                       sld.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                       RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
                im.item_desc
           from skulist_detail sld,
                item_master im
          where sld.skulist = I_skulist
            and im.item     = sld.item;

      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      open C_CL_SL_EXISTS;
      fetch C_CL_SL_EXISTS into L_pe_sl_exists;
      close C_CL_SL_EXISTS;

      if L_pe_sl_exists != 1 then

         insert into rpm_clearance_skulist
            (price_event_id,
             skulist,
             item,
             item_level,
             item_desc)
         select I_price_event_id,
                I_skulist,
                sld.item,
                DECODE(sld.item_level,
                       sld.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                       RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
                im.item_desc
           from skulist_detail sld,
                item_master im
          where sld.skulist = I_skulist
            and im.item     = sld.item;

      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      open C_PR_SL_EXISTS;
      fetch C_PR_SL_EXISTS into L_pe_sl_exists;
      close C_PR_SL_EXISTS;

      if L_pe_sl_exists != 1 then

         insert into rpm_promo_dtl_skulist
            (price_event_id,
             skulist,
             item,
             item_level,
             item_desc)
         select I_price_event_id,
                I_skulist,
                sld.item,
                DECODE(sld.item_level,
                       sld.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                       RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
                im.item_desc
           from skulist_detail sld,
                item_master im
          where sld.skulist = I_skulist
            and im.item     = sld.item;

      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END;

--------------------------------------------------------------------------

FUNCTION PURGE_PE_SKULIST(O_error_msg           OUT VARCHAR2,
                          I_price_event_id   IN     NUMBER,
                          I_price_event_type IN     VARCHAR2,
                          I_skulist          IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_PE_SKULIST_SQL.PURGE_PE_SKULIST';

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      delete from rpm_price_change_skulist
       where price_event_id = I_price_event_id
         and skulist        = I_skulist;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      delete from rpm_clearance_skulist
       where price_event_id = I_price_event_id
         and skulist        = I_skulist;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      delete from rpm_promo_dtl_skulist
       where price_event_id = I_price_event_id
         and skulist        = I_skulist;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END;

--------------------------------------------------------------------------

FUNCTION COPY_PROMO_SKULIST(O_error_msg                OUT VARCHAR2,
                            I_existing_promo_dtl_id IN     NUMBER,
                            I_new_promo_dtl_id      IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PE_SKULIST_SQL.COPY_PROMO_SKULIST';
   
   L_pe_sl_exists  NUMBER(1) := 0;

   cursor C_PR_SL_EXISTS is
      select 1
        from dual
       where EXISTS (select 'x'
                       from rpm_promo_dtl_skulist
                      where price_event_id = I_new_promo_dtl_id);

BEGIN

   open C_PR_SL_EXISTS;
   fetch C_PR_SL_EXISTS into L_pe_sl_exists;
   close C_PR_SL_EXISTS;

   if L_pe_sl_exists != 1 then

      insert into rpm_promo_dtl_skulist
         (price_event_id,
          skulist,
          item,
          item_level,
          item_desc)
      select I_new_promo_dtl_id,
             skulist,
             item,
             item_level,
             item_desc
        from rpm_promo_dtl_skulist_v
       where price_event_id = I_existing_promo_dtl_id;
       
   end if;  


   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END;

--------------------------------------------------------------------------

FUNCTION VALIDATE_TXN_EXCLUSION_SKULIST(O_error_msg                OUT VARCHAR2,
                                        O_some_data_removed_ind    OUT NUMBER,
                                        O_all_data_removed_ind     OUT NUMBER,
                                        I_promo_dtl_id          IN     NUMBER,
                                        I_buylists              IN     OBJ_MERCH_NODE_TBL,
                                        I_skulist_id            IN     NUMBER,
                                        I_user_id               IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PE_SKULIST_SQL.VALIDATE_TXN_EXCLUSION_SKULIST';

   L_skulist_orig_count NUMBER := 0;

BEGIN

   O_some_data_removed_ind := 0;
   O_all_data_removed_ind := 0;

   select COUNT(1)
     into L_skulist_orig_count
     from skulist_detail
    where skulist = I_skulist_id;

   insert into rpm_prom_txn_excl_skulist_ws
      (promo_dtl_id,
       skulist,
       item,
       item_level,
       item_desc)
      with skulist_items as
         (select distinct im.item,
                 im.item_parent,
                 im.dept,
                 im.class,
                 im.subclass,
                 im.item_level,
                 im.tran_level,
                 im.item_desc,
                 im.diff_1,
                 im.diff_2,
                 im.diff_3,
                 im.diff_4
            from rsm_hierarchy_permission rhp,
                 rsm_role_hierarchy_perm rrhp,
                 rsm_hierarchy_type rht,
                 rsm_user_role rur,
                 subclass sc,
                 skulist_detail sd,
                 item_master im
           where rhp.reference_class    IN (RPM_CONSTANTS.RSM_REF_CLASS_DEPT,
                                            RPM_CONSTANTS.RSM_REF_CLASS_CLASS,
                                            RPM_CONSTANTS.RSM_REF_CLASS_SUBCLASS)
             and rhp.id                 = rrhp.parent_id
             and rrhp.hierarchy_type_id = rht.id
             and rht.external_id        = RPM_CONSTANTS.RSM_MERCH_HIER
             and rrhp.role_id           = rur.role_id
             and rur.user_id            = I_user_id
             and rhp.key_value          IN (TO_CHAR(sc.dept),
                                            TO_CHAR(sc.dept||';'||sc.class),
                                            TO_CHAR(sc.dept||';'||sc.class||';'||sc.subclass))
             and sd.skulist             = I_skulist_id
             and sd.item                = im.item
             and im.tran_level         >= im.item_level
             and im.status              = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind        = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and im.dept                = sc.dept
             and im.class               = sc.class
             and im.subclass            = sc.subclass
             and NOT EXISTS (select 'x'
                               from skulist_detail sd2
                              where sd2.item    = im.item_parent
                                and sd2.skulist = sd.skulist)) -- end skulist_items with clause
      -- buylist at parent item, list contains child
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si
       where bl.merch_level_type = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and bl.item             = si.item_parent
      union all
      -- buylist at parent item, list contains parent
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si
       where bl.merch_level_type = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and bl.item             = si.item
      union all
      -- buylist at parent diff, list contains child
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si
       where bl.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and bl.item             = si.item_parent
         and bl.diff_id          IN (si.diff_1,
                                     si.diff_2,
                                     si.diff_3,
                                     si.diff_4)
      union all
      -- buylist at D/C/S, list contains tran item or parent
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si
       where bl.merch_level_type IN (RPM_CONSTANTS.DEPT_MERCH_TYPE,
                                     RPM_CONSTANTS.CLASS_MERCH_TYPE,
                                     RPM_CONSTANTS.SUBCLASS_MERCH_TYPE)
         and si.dept     = bl.dept
         and si.class    = NVL(bl.class, si.class)
         and si.subclass = NVL(bl.subclass, si.subclass)
      union all
      -- buylist at storewide, list contains tran item or parent - no filtering done here, take the list as it is
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si
       where bl.merch_level_type = RPM_CONSTANTS.STOREWIDE_MERCH_TYPE
      -- buylist at skulist, exlusion list contains matching tran item or parent
      union all
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si,
             skulist_detail sd
       where bl.merch_level_type = RPM_CONSTANTS.ITEM_LIST_TYPE
         and bl.skulist          = sd.skulist
         and sd.item             = si.item
      -- buylist at skulist, exlusion list contains tran item under a parent
      union all
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si,
             skulist_detail sd
       where bl.merch_level_type = RPM_CONSTANTS.ITEM_LIST_TYPE
         and bl.skulist          = sd.skulist
         and sd.item             = si.item_parent
         and si.item_level       = si.tran_level
      -- buylist at PEIL, exlusion list contains tran item or parent
      union all
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si,
             rpm_merch_list_detail rmld
       where bl.merch_level_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and bl.price_event_itemlist = rmld.merch_list_id
         and rmld.item               = si.item
      -- buylist at PEIL, exlusion list contains tran item under a parent
      union all
      select I_promo_dtl_id,
             I_skulist_id,
             si.item,
             DECODE(si.item_level,
                    si.tran_level, RPM_CONSTANTS.IL_ITEM_LEVEL,
                    RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL),
             si.item_desc
        from table(cast(I_buylists as OBJ_MERCH_NODE_TBL)) bl,
             skulist_items si,
             rpm_merch_list_detail rmld
       where bl.merch_level_type     = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_TYPE
         and bl.price_event_itemlist = rmld.merch_list_id
         and rmld.item               = si.item_parent
         and si.item_level           = si.tran_level;

   if SQL%ROWCOUNT = 0 then

      O_all_data_removed_ind := 1;

   elsif SQL%ROWCOUNT < L_skulist_orig_count then

      O_some_data_removed_ind := 1;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END VALIDATE_TXN_EXCLUSION_SKULIST;

-------------------------------------------------------------------

FUNCTION SAVE_TXN_EXCLUSION_SKULIST(O_error_msg        OUT VARCHAR2,
                                    I_promo_dtl_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PE_SKULIST_SQL.SAVE_TXN_EXCLUSION_SKULIST';

BEGIN

   forall i IN 1..I_promo_dtl_ids.COUNT
      insert into rpm_promo_dtl_skulist
         (price_event_id,
          skulist,
          item,
          item_level,
          item_desc)
      select promo_dtl_id,
             skulist,
             item,
             item_level,
             item_desc
        from rpm_prom_txn_excl_skulist_ws ws
       where ws.promo_dtl_id = I_promo_dtl_ids(i);

   forall i IN 1..I_promo_dtl_ids.COUNT
      delete rpm_prom_txn_excl_skulist_ws
       where promo_dtl_id = I_promo_dtl_ids(i);

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END SAVE_TXN_EXCLUSION_SKULIST;

-------------------------------------------------------------------

FUNCTION PURGE_TXN_EXCL_STAGE_WS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_PE_SKULIST_SQL.PURGE_TXN_EXCL_STAGE_WS';

BEGIN

   EXECUTE IMMEDIATE 'truncate table rpm_prom_txn_excl_skulist_ws';

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END PURGE_TXN_EXCL_STAGE_WS;

-------------------------------------------------------------------
FUNCTION GET_SKULIST_DCS(O_error_msg         OUT VARCHAR2,
                         O_merch_data        OUT OBJ_MERCH_ZONE_ND_DESCS_TBL,
                         I_price_event_id IN     NUMBER,
                         I_skulist_ids    IN     OBJ_NUM_NUM_STR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_PE_SKULIST_SQL.GET_SKULIST_DCS';

   cursor GET_SKULIST_DCS is
      select OBJ_MERCH_ZONE_ND_DESCS_REC(NULL, --zone_node_type
                                         NULL, --location,
                                         NULL, --loc_desc,
                                         NULL, --zone_id,
                                         NULL, --zone_display_id,
                                         NULL, --zone_desc,
                                         NULL, --merch_type,
                                         t.dept,
                                         d.dept_name,
                                         t.class,
                                         c.class_name,
                                         t.subclass,
                                         sc.sub_name,
                                         NULL, --item,
                                         NULL, --item_desc,
                                         NULL, --diff_id,
                                         NULL, --diff_desc,
                                         NULL, --skulist,
                                         NULL, --skulist_desc,
                                         NULL, --price_event_itemlist,
                                         NULL) --price_event_itemlist_desc
        from (select /*+ CARDINALITY(ids 10) */
                     dept,
                     class,
                     subclass
                from table(cast(I_skulist_ids as OBJ_NUM_NUM_STR_TBL)) ids,
                     skulist_dept_class_subclass sdcs
               where ids.number_2 = 1 -- new skulist
                 and sdcs.skulist = ids.number_1
              union
              select /*+ CARDINALITY(ids 10) */
                     im.dept,
                     im.class,
                     im.subclass
                from table(cast(I_skulist_ids as OBJ_NUM_NUM_STR_TBL)) ids,
                     rpm_promo_dtl_skulist rpds,
                     item_master im
               where ids.number_2        = 0 -- snapshot already taken for the skulist
                 and rpds.price_event_id = I_price_event_id
                 and rpds.skulist        = ids.number_1
                 and rpds.item           = im.item) t,
             deps d,
             class c,
             subclass sc
       where t.dept     = d.dept
         and t.dept     = c.dept
         and t.class    = c.class
         and t.dept     = sc.dept
         and t.class    = sc.class
         and t.subclass = sc.subclass
         and d.dept     = c.dept
         and d.dept     = sc.dept
         and c.dept     = sc.dept
         and c.class    = sc.class;

BEGIN

   open GET_SKULIST_DCS;
   fetch GET_SKULIST_DCS BULK COLLECT into O_merch_data;
   close GET_SKULIST_DCS;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END GET_SKULIST_DCS;

-------------------------------------------------------------------

END RPM_PE_SKULIST_SQL;
/
