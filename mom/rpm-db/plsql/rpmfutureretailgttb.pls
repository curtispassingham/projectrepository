CREATE OR REPLACE PACKAGE BODY RPM_FUTURE_RETAIL_GTT_SQL AS

--------------------------------------------------------------------------------
--                            PRIVATE GLOBALS                                 --
--------------------------------------------------------------------------------
LP_vdate DATE := GET_VDATE;

LP_USE_BASIS_UOM CONSTANT RPM_FUTURE_RETAIL_GTT.PC_CHANGE_SELLING_UOM%TYPE := '-999';

--------------------------------------------------------------------------------
--                          PRIVATE PROTOTYPES                                --
--------------------------------------------------------------------------------
FUNCTION UPD_RESET_ON_CLR_LOC_MOVE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;

--------------------------------------------------------------------------------
--                            PUBLIC PROCEDURES                               --
--------------------------------------------------------------------------------

FUNCTION MERGE_LOCATION_MOVE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                             I_lm_rec       IN     RPM_LOCATION_MOVE%ROWTYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_LOCATION_MOVE';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program|| ' I_lm_rec.location_move_id: '|| I_lm_rec.location_move_id);

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt rfrg1
   using (select dept,
                 class,
                 subclass,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 cur_hier_level,
                 max_hier_level
            from (select rfrg2.dept,
                         rfrg2.class,
                         rfrg2.subclass,
                         rfrg2.item,
                         rfrg2.diff_id,
                         rfrg2.item_parent,
                         rfrg2.zone_node_type,
                         rfrg2.location,
                         rfrg2.zone_id,
                         rfrg2.selling_retail,
                         rfrg2.selling_retail_currency,
                         rfrg2.selling_uom,
                         rfrg2.multi_units,
                         rfrg2.multi_unit_retail,
                         rfrg2.multi_unit_retail_currency,
                         rfrg2.multi_selling_uom,
                         rfrg2.clear_retail,
                         rfrg2.clear_retail_currency,
                         rfrg2.clear_uom,
                         rfrg2.simple_promo_retail,
                         rfrg2.simple_promo_retail_currency,
                         rfrg2.simple_promo_uom,
                         rfrg2.cur_hier_level,
                         rfrg2.max_hier_level,
                         RANK() OVER (PARTITION BY rfrg2.item,
                                                   rfrg2.diff_id,
                                                   rfrg2.location,
                                                   rfrg2.zone_node_type
                                          ORDER BY rfrg2.action_date desc) as rank_value
                    from rpm_future_retail_gtt rfrg2
                   where rfrg2.action_date <= I_lm_rec.effective_date)
           where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = I_lm_rec.location_move_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = I_lm_rec.effective_date)
   when MATCHED then
      update
         set rfrg1.loc_move_from_zone_id = I_lm_rec.old_zone_id,
             rfrg1.loc_move_to_zone_id   = I_lm_rec.new_zone_id,
             rfrg1.location_move_id      = I_lm_rec.location_move_id
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              diff_id,
              item_parent,
              zone_node_type,
              location,
              zone_id,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              on_simple_promo_ind,
              on_complex_promo_ind,
              cur_hier_level,
              max_hier_level)
      values (I_lm_rec.location_move_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.diff_id,
              rfrg3.item_parent,
              rfrg3.zone_node_type,
              rfrg3.location,
              rfrg3.zone_id,
              I_lm_rec.effective_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              I_lm_rec.old_zone_id,
              I_lm_rec.new_zone_id,
              I_lm_rec.location_move_id,
              0,
              0,
              rfrg3.cur_hier_level,
              rfrg3.max_hier_level);

   LOGGER.LOG_TIME(L_program|| ' I_lm_rec.location_move_id: '|| I_lm_rec.location_move_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_LOCATION_MOVE;
--------------------------------------------------------------------------------

FUNCTION LOCATION_MOVE_SCRUB(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                             I_lm_rec       IN     RPM_LOCATION_MOVE%ROWTYPE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.LOCATION_MOVE_SCRUB';

   L_start_time                  TIMESTAMP                  := SYSTIMESTAMP;
   L_deleted_clearance_reset_ids OBJ_CLEARANCE_ITEM_LOC_TBL := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program|| ' I_lm_rec.location_move_id: '|| I_lm_rec.location_move_id);

   --scrub price changes
   merge /*+ INDEX(gtt, RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt gtt
   using (select sub_gtt.price_event_id,
                 sub_gtt.item,
                 sub_gtt.diff_id,
                 sub_gtt.location,
                 sub_gtt.zone_node_type,
                 sub_gtt.action_date
            from rpm_future_retail_gtt sub_gtt,
                 rpm_price_change pc
           where sub_gtt.price_event_id = I_lm_rec.location_move_id
             and pc.price_change_id     = sub_gtt.price_change_id
             and pc.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and pc.zone_id             = I_lm_rec.old_zone_id
             and sub_gtt.action_date   >= I_lm_rec.effective_date
             and rownum                 > 0) source
   on (    gtt.price_event_id         = source.price_event_id
       and gtt.item                   = source.item
       and NVL(gtt.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and gtt.location               = source.location
       and gtt.zone_node_type         = source.zone_node_type
       and gtt.action_date            = source.action_date)
   when MATCHED then
      update
         set gtt.price_change_id               = NULL,
             gtt.price_change_display_id       = NULL,
             gtt.pc_exception_parent_id        = NULL,
             gtt.pc_change_type                = NULL,
             gtt.pc_change_amount              = NULL,
             gtt.pc_change_currency            = NULL,
             gtt.pc_change_percent             = NULL,
             gtt.pc_change_selling_uom         = NULL,
             gtt.pc_null_multi_ind             = NULL,
             gtt.pc_multi_units                = NULL,
             gtt.pc_multi_unit_retail          = NULL,
             gtt.pc_multi_unit_retail_currency = NULL,
             gtt.pc_multi_selling_uom          = NULL,
             gtt.pc_price_guide_id             = NULL;

   --scrub clearances
   merge into rpm_future_retail_gtt gtt
   using (select sub_gtt.price_event_id,
                 sub_gtt.item,
                 sub_gtt.diff_id,
                 sub_gtt.location,
                 sub_gtt.zone_node_type,
                 sub_gtt.action_date
            from rpm_future_retail_gtt sub_gtt,
                 rpm_clearance cl
           where sub_gtt.price_event_id   = I_lm_rec.location_move_id
             and cl.clearance_id          = sub_gtt.clearance_id
             and cl.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and cl.zone_id               = I_lm_rec.old_zone_id
             and sub_gtt.clear_start_ind != RPM_CONSTANTS.END_IND
             and sub_gtt.action_date     >= I_lm_rec.effective_date) source
   on (    gtt.price_event_id         = source.price_event_id
       and gtt.item                   = source.item
       and NVL(gtt.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and gtt.location               = source.location
       and gtt.zone_node_type         = source.zone_node_type
       and gtt.action_date            = source.action_date)
   when MATCHED then
      update
         set gtt.clearance_id             = NULL,
             gtt.clearance_display_id     = NULL,
             gtt.clear_mkdn_index         = NULL,
             gtt.clear_start_ind          = NULL,
             gtt.clear_change_type        = NULL,
             gtt.clear_change_amount      = NULL,
             gtt.clear_change_currency    = NULL,
             gtt.clear_change_percent     = NULL,
             gtt.clear_change_selling_uom = NULL,
             gtt.clear_price_guide_id     = NULL;

   if REMOVE_CLEARANCE_RESETS(O_cc_error_tbl,
                              L_deleted_clearance_reset_ids) = 0 then
      return 0;
   end if;

   if RPM_CC_PUBLISH.STAGE_UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl,
                                                   NULL,
                                                   L_deleted_clearance_reset_ids,
                                                   NULL,
                                                   0) = 0 then
      return 0;
   end if;

   if UPD_RESET_ON_CLR_LOC_MOVE(O_cc_error_tbl) = 0 then
      return 0;
   end if;

   --scrub promotions
   update rpm_promo_item_loc_expl_gtt gtt
      set deleted_ind = 1
    where gtt.price_event_id     = I_lm_rec.location_move_id
      and gtt.detail_start_date >= I_lm_rec.effective_date
      and NOT (    I_lm_rec.effective_date  > gtt.detail_start_date
               and I_lm_rec.effective_date <= gtt.detail_end_date)
      and EXISTS (select 1
                    from rpm_promo_dtl rpd,
                         rpm_promo_zone_location rpzl
                   where rpd.promo_dtl_id    = gtt.promo_dtl_id
                     and rpzl.promo_dtl_id   = rpd.promo_dtl_id
                     and rpd.state           = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                     and rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                     and rpzl.zone_id        = I_lm_rec.old_zone_id);

   LOGGER.LOG_TIME(L_program|| ' I_lm_rec.location_move_id: '|| I_lm_rec.location_move_id,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END LOCATION_MOVE_SCRUB;

--------------------------------------------------------------------------------
FUNCTION MERGE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PRICE_CHANGE';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt rfrg1
   using (select price_event_id,
                 dept,
                 class,
                 subclass,
                 item,
                 zone_node_type,
                 location,
                 rpc_effective_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 rpc_price_change_id,
                 rpc_price_change_display_id,
                 rpc_exception_parent_id,
                 rpc_change_type,
                 rpc_change_amount,
                 rpc_change_currency,
                 rpc_change_percent,
                 rpc_change_selling_uom,
                 rpc_null_multi_ind,
                 rpc_multi_units,
                 rpc_multi_unit_retail,
                 rpc_multi_unit_retail_currency,
                 rpc_multi_selling_uom,
                 rpc_price_guide_id,
                 rpc_selling_retail_ind,
                 rpc_multi_unit_ind,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from (select /*+ CARDINALITY(ids 10) INDEX(pc, PK_RPM_PRICE_CHANGE) */
                         rpc.effective_date          rpc_effective_date,
                         rpc.price_change_id         rpc_price_change_id,
                         rpc.price_change_display_id rpc_price_change_display_id,
                         rpc.exception_parent_id     rpc_exception_parent_id,
                         DECODE(rpc.change_type,
                                RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE,
                                rpc.change_type) rpc_change_type,
                         rpc.change_amount   rpc_change_amount,
                         rpc.change_currency rpc_change_currency,
                         rpc.change_percent  rpc_change_percent,
                         DECODE(rpc.change_type,
                                RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, LP_USE_BASIS_UOM,
                                rpc.change_selling_uom) rpc_change_selling_uom,
                         case
                            when rpc.change_amount is NOT NULL or
                                 rpc.change_percent is NOT NULL then
                               1
                            else
                               0
                         end rpc_selling_retail_ind,
                         case
                            when rpc.multi_units is NOT NULL or
                                 rpc.null_multi_ind = 1 then
                               1
                            else
                               0
                         end rpc_multi_unit_ind,
                         rpc.null_multi_ind             rpc_null_multi_ind,
                         rpc.multi_units                rpc_multi_units,
                         rpc.multi_unit_retail          rpc_multi_unit_retail,
                         rpc.multi_unit_retail_currency rpc_multi_unit_retail_currency,
                         rpc.multi_selling_uom          rpc_multi_selling_uom,
                         rpc.price_guide_id             rpc_price_guide_id,
                         rfrg2.price_event_id,
                         rfrg2.dept,
                         rfrg2.class,
                         rfrg2.subclass,
                         rfrg2.item,
                         rfrg2.zone_node_type,
                         rfrg2.location,
                         rfrg2.selling_retail,
                         rfrg2.selling_retail_currency,
                         rfrg2.selling_uom,
                         rfrg2.multi_units,
                         rfrg2.multi_unit_retail,
                         rfrg2.multi_unit_retail_currency,
                         rfrg2.multi_selling_uom,
                         rfrg2.clear_retail,
                         rfrg2.clear_retail_currency,
                         rfrg2.clear_uom,
                         rfrg2.simple_promo_retail,
                         rfrg2.simple_promo_retail_currency,
                         rfrg2.simple_promo_uom,
                         rfrg2.item_parent,
                         rfrg2.diff_id,
                         rfrg2.zone_id,
                         rfrg2.max_hier_level,
                         rfrg2.cur_hier_level,
                         RANK() OVER (PARTITION BY rfrg2.price_event_id,
                                                   rfrg2.item,
                                                   NVL(rfrg2.diff_id, '-999'),
                                                   rfrg2.location,
                                                   rfrg2.zone_node_type
                                          ORDER BY rfrg2.action_date desc) as rank_value
                    from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                         rpm_future_retail_gtt rfrg2,
                         rpm_price_change rpc
                   where rfrg2.price_event_id = VALUE(ids)
                     and rpc.price_change_id  = VALUE(ids)
                     and rfrg2.action_date   <= rpc.effective_date)
           where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = rpc_effective_date)
   when MATCHED then
      update
         set rfrg1.price_change_id               = rpc_price_change_id,
             rfrg1.price_change_display_id       = rpc_price_change_display_id,
             rfrg1.pc_exception_parent_id        = rpc_exception_parent_id,
             rfrg1.pc_change_type                = rpc_change_type,
             rfrg1.pc_change_amount              = rpc_change_amount,
             rfrg1.pc_change_currency            = rpc_change_currency,
             rfrg1.pc_change_percent             = rpc_change_percent,
             rfrg1.pc_change_selling_uom         = DECODE(rpc_change_selling_uom,
                                                          LP_USE_BASIS_UOM, rfrg1.selling_uom,
                                                          rpc_change_selling_uom),
             rfrg1.pc_null_multi_ind             = rpc_null_multi_ind,
             rfrg1.pc_multi_units                = rpc_multi_units,
             rfrg1.pc_multi_unit_retail          = rpc_multi_unit_retail,
             rfrg1.pc_multi_unit_retail_currency = rpc_multi_unit_retail_currency,
             rfrg1.pc_multi_selling_uom          = rpc_multi_selling_uom,
             rfrg1.pc_price_guide_id             = rpc_price_guide_id,
             rfrg1.pc_msg_type                   = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE,
             rfrg1.pc_selling_retail_ind         = rpc_selling_retail_ind,
             rfrg1.pc_multi_unit_ind             = rpc_multi_unit_ind,
             rfrg1.step_identifier               = RPM_CONSTANTS.CAPT_GTT_MERGE_PC
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              pc_msg_type,
              pc_selling_retail_ind,
              pc_multi_unit_ind,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (rfrg3.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.zone_node_type,
              rfrg3.location,
              rpc_effective_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              rpc_price_change_id,
              rpc_price_change_display_id,
              rpc_exception_parent_id,
              rpc_change_type,
              rpc_change_amount,
              rpc_change_currency,
              rpc_change_percent,
              DECODE(rpc_change_selling_uom,
                     LP_USE_BASIS_UOM, rfrg3.selling_uom,
                     rpc_change_selling_uom),
              rpc_null_multi_ind,
              rpc_multi_units,
              rpc_multi_unit_retail,
              rpc_multi_unit_retail_currency,
              rpc_multi_selling_uom,
              rpc_price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE,
              rpc_selling_retail_ind,
              rpc_multi_unit_ind,
              0,
              0,
              rfrg3.item_parent,
              rfrg3.diff_id,
              rfrg3.zone_id,
              rfrg3.max_hier_level,
              rfrg3.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_PC);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_MERGE_PC,
                                                   1,
                                                   0,
                                                   0,
                                                   0,
                                                   0) = 0 then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_PRICE_CHANGE;
--------------------------------------------------------------------------------
FUNCTION MERGE_PARENT_PRICE_CHANGE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_parent_ids   IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PARENT_PRICE_CHANGE';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt rfrg1
   using (select price_event_id,
                 dept,
                 class,
                 subclass,
                 item,
                 zone_node_type,
                 location,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 rpc_price_change_id,
                 rpc_price_change_display_id,
                 rpc_exception_parent_id,
                 rpc_change_type,
                 rpc_change_amount,
                 rpc_change_currency,
                 rpc_change_percent,
                 rpc_change_selling_uom,
                 rpc_null_multi_ind,
                 rpc_multi_units,
                 rpc_multi_unit_retail,
                 rpc_multi_unit_retail_currency,
                 rpc_multi_selling_uom,
                 rpc_price_guide_id,
                 rpc_selling_retail_ind,
                 rpc_multi_unit_ind,
                 rpc_effective_date,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from (select /*+ CARDINALITY(ids 10) */
                         rpc.effective_date          rpc_effective_date,
                         rpc.price_change_id         rpc_price_change_id,
                         rpc.price_change_display_id rpc_price_change_display_id,
                         rpc.exception_parent_id     rpc_exception_parent_id,
                         DECODE(rpc.change_type,
                                RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE,
                                rpc.change_type) rpc_change_type,
                         rpc.change_amount   rpc_change_amount,
                         rpc.change_currency rpc_change_currency,
                         rpc.change_percent  rpc_change_percent,
                         DECODE(rpc.change_type,
                                RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, LP_USE_BASIS_UOM,
                                rpc.change_selling_uom) rpc_change_selling_uom,
                         case
                            when rpc.change_amount is NOT NULL or
                                 rpc.change_percent is NOT NULL then
                               1
                            else
                               0
                         end as rpc_selling_retail_ind,
                         case
                            when rpc.multi_units is NOT NULL or
                                 rpc.null_multi_ind = 1 then
                               1
                            else
                               0
                         end as rpc_multi_unit_ind,
                         rpc.null_multi_ind             rpc_null_multi_ind,
                         rpc.multi_units                rpc_multi_units,
                         rpc.multi_unit_retail          rpc_multi_unit_retail,
                         rpc.multi_unit_retail_currency rpc_multi_unit_retail_currency,
                         rpc.multi_selling_uom          rpc_multi_selling_uom,
                         rpc.price_guide_id             rpc_price_guide_id,
                         rfrg2.price_event_id,
                         rfrg2.dept,
                         rfrg2.class,
                         rfrg2.subclass,
                         rfrg2.item,
                         rfrg2.zone_node_type,
                         rfrg2.location,
                         rfrg2.selling_retail,
                         rfrg2.selling_retail_currency,
                         rfrg2.selling_uom,
                         rfrg2.multi_units,
                         rfrg2.multi_unit_retail,
                         rfrg2.multi_unit_retail_currency,
                         rfrg2.multi_selling_uom,
                         rfrg2.clear_retail,
                         rfrg2.clear_retail_currency,
                         rfrg2.clear_uom,
                         rfrg2.simple_promo_retail,
                         rfrg2.simple_promo_retail_currency,
                         rfrg2.simple_promo_uom,
                         rfrg2.item_parent,
                         rfrg2.diff_id,
                         rfrg2.zone_id,
                         rfrg2.max_hier_level,
                         rfrg2.cur_hier_level,
                         RANK() OVER (PARTITION BY rfrg2.price_event_id,
                                                   rfrg2.item,
                                                   NVL(rfrg2.diff_id, '-999'),
                                                   rfrg2.location
                                          ORDER BY rfrg2.action_date desc) as rank_value
                    from table(cast(I_parent_ids AS OBJ_NUM_NUM_DATE_TBL)) ids,
                         rpm_future_retail_gtt rfrg2,
                         rpm_price_change rpc
                   where rfrg2.price_event_id = ids.numeric_col1
                     and rpc.price_change_id  = ids.numeric_col2
                     and rfrg2.action_date   <= rpc.effective_date)
           where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = rpc_effective_date)
   when MATCHED then
      update
         set rfrg1.price_change_id               = rpc_price_change_id,
             rfrg1.price_change_display_id       = rpc_price_change_display_id,
             rfrg1.pc_exception_parent_id        = rpc_exception_parent_id,
             rfrg1.pc_change_type                = rpc_change_type,
             rfrg1.pc_change_amount              = rpc_change_amount,
             rfrg1.pc_change_currency            = rpc_change_currency,
             rfrg1.pc_change_percent             = rpc_change_percent,
             rfrg1.pc_change_selling_uom         = DECODE(rpc_change_selling_uom,
                                                          LP_USE_BASIS_UOM, rfrg1.selling_uom,
                                                          rpc_change_selling_uom),
             rfrg1.pc_null_multi_ind             = rpc_null_multi_ind,
             rfrg1.pc_multi_units                = rpc_multi_units,
             rfrg1.pc_multi_unit_retail          = rpc_multi_unit_retail,
             rfrg1.pc_multi_unit_retail_currency = rpc_multi_unit_retail_currency,
             rfrg1.pc_multi_selling_uom          = rpc_multi_selling_uom,
             rfrg1.pc_price_guide_id             = rpc_price_guide_id,
             rfrg1.pc_msg_type                   = RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE,
             rfrg1.pc_selling_retail_ind         = rpc_selling_retail_ind,
             rfrg1.pc_multi_unit_ind             = rpc_multi_unit_ind,
             rfrg1.step_identifier               = RPM_CONSTANTS.CAPT_GTT_MERGE_PARENT_PC
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              pc_msg_type,
              pc_selling_retail_ind,
              pc_multi_unit_ind,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (rfrg3.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.zone_node_type,
              rfrg3.location,
              rpc_effective_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              rpc_price_change_id,
              rpc_price_change_display_id,
              rpc_exception_parent_id,
              rpc_change_type,
              rpc_change_amount,
              rpc_change_currency,
              rpc_change_percent,
              DECODE(rpc_change_selling_uom,
                     LP_USE_BASIS_UOM, rfrg3.selling_uom,
                     rpc_change_selling_uom),
              rpc_null_multi_ind,
              rpc_multi_units,
              rpc_multi_unit_retail,
              rpc_multi_unit_retail_currency,
              rpc_multi_selling_uom,
              rpc_price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_REG_PC_CRE,
              rpc_selling_retail_ind,
              rpc_multi_unit_ind,
              0,
              0,
              rfrg3.item_parent,
              rfrg3.diff_id,
              rfrg3.zone_id,
              rfrg3.max_hier_level,
              rfrg3.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_PARENT_PC);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_MERGE_PARENT_PC,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_PARENT_PRICE_CHANGE;

--------------------------------------------------------------------------------
FUNCTION MERGE_CLEARANCE(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                         I_clearance_ids   IN     OBJ_NUMERIC_ID_TABLE,
                         I_clear_start_ind IN     RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_CLEARANCE';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt rfrg1
   using (select price_event_id,
                 dept,
                 class,
                 subclass,
                 item,
                 zone_node_type,
                 location,
                 rc_effective_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 rc_clearance_id,
                 rc_clearance_display_id,
                 rc_change_type,
                 rc_change_amount,
                 rc_change_currency,
                 rc_change_percent,
                 rc_change_selling_uom,
                 rc_price_guide_id,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from (select /*+ ORDERED */
                         t.event_id,
                         t.rc_clearance_id,
                         t.rc_clearance_display_id,
                         t.rc_effective_date,
                         t.rc_change_type,
                         t.rc_change_amount,
                         t.rc_change_currency,
                         t.rc_change_percent,
                         t.rc_change_selling_uom,
                         t.rc_price_guide_id,
                         rfrg2.price_event_id,
                         rfrg2.dept,
                         rfrg2.class,
                         rfrg2.subclass,
                         rfrg2.item,
                         rfrg2.zone_node_type,
                         rfrg2.location,
                         rfrg2.selling_retail,
                         rfrg2.selling_retail_currency,
                         rfrg2.selling_uom,
                         rfrg2.multi_units,
                         rfrg2.multi_unit_retail,
                         rfrg2.multi_unit_retail_currency,
                         rfrg2.multi_selling_uom,
                         rfrg2.clear_retail,
                         rfrg2.clear_retail_currency,
                         rfrg2.clear_uom,
                         rfrg2.simple_promo_retail,
                         rfrg2.simple_promo_retail_currency,
                         rfrg2.simple_promo_uom,
                         rfrg2.item_parent,
                         rfrg2.diff_id,
                         rfrg2.zone_id,
                         rfrg2.max_hier_level,
                         rfrg2.cur_hier_level,
                         RANK() OVER(PARTITION BY price_event_id,
                                                  item,
                                                  NVL(diff_id, '-999'),
                                                  location,
                                                  zone_node_type
                                         ORDER BY action_date desc) as rank_value
                    from (select /*+ CARDINALITY(ids 10) */
                                 rc.clearance_id event_id,
                                 rc.clearance_id            rc_clearance_id,
                                 rc.clearance_display_id    rc_clearance_display_id,
                                 rc.effective_date          rc_effective_date,
                                 DECODE(rc.change_type,
                                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE,
                                        rc.change_type)     rc_change_type,
                                 rc.change_amount           rc_change_amount,
                                 rc.change_currency         rc_change_currency,
                                 rc.change_percent          rc_change_percent,
                                 DECODE(rc.change_type,
                                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, LP_USE_BASIS_UOM,
                                        rc.change_selling_uom) rc_change_selling_uom,
                                 NVL(rc.price_guide_id, 0)  rc_price_guide_id
                            from table(cast(I_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                 rpm_clearance_gtt rc
                           where rc.price_event_id = VALUE(ids)
                             and rc.clearance_id   = VALUE(ids)) t,
                         rpm_future_retail_gtt rfrg2
                   where rfrg2.price_event_id = t.event_id
                     and rfrg2.action_date   <= t.rc_effective_date)
           where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = rfrg3.rc_effective_date)
   when MATCHED then
      update
         set rfrg1.clearance_id             = rc_clearance_id,
             rfrg1.clearance_display_id     = rc_clearance_display_id,
             rfrg1.clear_mkdn_index         = NULL,
             rfrg1.clear_start_ind          = I_clear_start_ind,
             rfrg1.clear_change_type        = rc_change_type,
             rfrg1.clear_change_amount      = rc_change_amount,
             rfrg1.clear_change_currency    = rc_change_currency,
             rfrg1.clear_change_percent     = rc_change_percent,
             rfrg1.clear_change_selling_uom = DECODE(rc_change_selling_uom,
                                                     LP_USE_BASIS_UOM, rfrg1.clear_uom,
                                                     rc_change_selling_uom),
             rfrg1.clear_price_guide_id     = rc_price_guide_id,
             rfrg1.clear_msg_type           = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
             rfrg1.step_identifier          = RPM_CONSTANTS.CAPT_GTT_MERGE_CLEARANCE
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              clearance_id,
              clearance_display_id,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              clear_msg_type,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (rfrg3.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.zone_node_type,
              rfrg3.location,
              rfrg3.rc_effective_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              rfrg3.rc_clearance_id,
              rfrg3.rc_clearance_display_id,
              I_clear_start_ind,
              rfrg3.rc_change_type,
              rfrg3.rc_change_amount,
              rfrg3.rc_change_currency,
              rfrg3.rc_change_percent,
              DECODE(rfrg3.rc_change_selling_uom,
                     LP_USE_BASIS_UOM, rfrg3.clear_uom,
                     rfrg3.rc_change_selling_uom),
              rfrg3.rc_price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
              0,
              0,
              rfrg3.item_parent,
              rfrg3.diff_id,
              rfrg3.zone_id,
              rfrg3.max_hier_level,
              rfrg3.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_CLEARANCE);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_MERGE_CLEARANCE,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_CLEARANCE;
--------------------------------------------------------------------------------
FUNCTION MERGE_CLEARANCE(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                         I_parent_ids      IN     OBJ_NUM_NUM_DATE_TBL,
                         I_clear_start_ind IN     RPM_FUTURE_RETAIL.CLEAR_START_IND%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_CLEARANCE';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt rfrg1
   using (select price_event_id,
                 dept,
                 class,
                 subclass,
                 item,
                 zone_node_type,
                 location,
                 rc_effective_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 rc_clearance_id,
                 rc_clearance_display_id,
                 rc_change_type,
                 rc_change_amount,
                 rc_change_currency,
                 rc_change_percent,
                 rc_change_selling_uom,
                 rc_price_guide_id,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from (select /*+ ORDERED */
                         t.event_id,
                         t.rc_clearance_id,
                         t.rc_clearance_display_id,
                         t.rc_effective_date,
                         t.rc_change_type,
                         t.rc_change_amount,
                         t.rc_change_currency,
                         t.rc_change_percent,
                         t.rc_change_selling_uom,
                         t.rc_price_guide_id,
                         rfrg2.price_event_id,
                         rfrg2.dept,
                         rfrg2.class,
                         rfrg2.subclass,
                         rfrg2.item,
                         rfrg2.zone_node_type,
                         rfrg2.location,
                         rfrg2.selling_retail,
                         rfrg2.selling_retail_currency,
                         rfrg2.selling_uom,
                         rfrg2.multi_units,
                         rfrg2.multi_unit_retail,
                         rfrg2.multi_unit_retail_currency,
                         rfrg2.multi_selling_uom,
                         rfrg2.clear_retail,
                         rfrg2.clear_retail_currency,
                         rfrg2.clear_uom,
                         rfrg2.simple_promo_retail,
                         rfrg2.simple_promo_retail_currency,
                         rfrg2.simple_promo_uom,
                         rfrg2.item_parent,
                         rfrg2.diff_id,
                         rfrg2.zone_id,
                         rfrg2.max_hier_level,
                         rfrg2.cur_hier_level,
                         RANK() OVER(PARTITION BY price_event_id,
                                                  item,
                                                  NVL(diff_id, '-999'),
                                                  location,
                                                  zone_node_type
                                         ORDER BY action_date desc) as rank_value
                    from (select /*+ CARDINALITY(ids 1000) */
                                 ids.numeric_col1           event_id,
                                 rc.clearance_id            rc_clearance_id,
                                 rc.clearance_display_id    rc_clearance_display_id,
                                 rc.effective_date          rc_effective_date,
                                 DECODE(rc.change_type,
                                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE,
                                        rc.change_type)     rc_change_type,
                                 rc.change_amount           rc_change_amount,
                                 rc.change_currency         rc_change_currency,
                                 rc.change_percent          rc_change_percent,
                                 DECODE(rc.change_type,
                                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, LP_USE_BASIS_UOM,
                                        rc.change_selling_uom) rc_change_selling_uom,
                                 NVL(rc.price_guide_id, 0)  rc_price_guide_id
                            from table(cast(I_parent_ids as OBJ_NUM_NUM_DATE_TBL)) ids,
                                 rpm_clearance rc
                           where ids.numeric_col2 is NOT NULL
                             and rc.clearance_id  = ids.numeric_col2) t,
                         rpm_future_retail_gtt rfrg2
                   where rfrg2.price_event_id = t.event_id
                     and rfrg2.action_date   <= t.rc_effective_date)
           where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = rfrg3.rc_effective_date)
   when MATCHED then
      update
         set rfrg1.clearance_id             = rc_clearance_id,
             rfrg1.clearance_display_id     = rc_clearance_display_id,
             rfrg1.clear_mkdn_index         = NULL,
             rfrg1.clear_start_ind          = I_clear_start_ind,
             rfrg1.clear_change_type        = rc_change_type,
             rfrg1.clear_change_amount      = rc_change_amount,
             rfrg1.clear_change_currency    = rc_change_currency,
             rfrg1.clear_change_percent     = rc_change_percent,
             rfrg1.clear_change_selling_uom = DECODE(rc_change_selling_uom,
                                                     LP_USE_BASIS_UOM, rfrg1.clear_uom,
                                                     rc_change_selling_uom),
             rfrg1.clear_price_guide_id     = rc_price_guide_id,
             rfrg1.clear_msg_type           = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
             rfrg1.step_identifier          = RPM_CONSTANTS.CAPT_GTT_MERGE_CLR_PARENT
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              clearance_id,
              clearance_display_id,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              clear_msg_type,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (rfrg3.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.zone_node_type,
              rfrg3.location,
              rfrg3.rc_effective_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              rfrg3.rc_clearance_id,
              rfrg3.rc_clearance_display_id,
              I_clear_start_ind,
              rfrg3.rc_change_type,
              rfrg3.rc_change_amount,
              rfrg3.rc_change_currency,
              rfrg3.rc_change_percent,
              DECODE(rfrg3.rc_change_selling_uom,
                     LP_USE_BASIS_UOM, rfrg3.clear_uom,
                     rfrg3.rc_change_selling_uom),
              rfrg3.rc_price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
              0,
              0,
              rfrg3.item_parent,
              rfrg3.diff_id,
              rfrg3.zone_id,
              rfrg3.max_hier_level,
              rfrg3.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_CLR_PARENT);

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt rfrg1
   using (select s.price_event_id,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item,
                 s.zone_node_type,
                 s.location,
                 s.reset_date,
                 s.selling_retail,
                 s.selling_retail_currency,
                 s.selling_uom,
                 s.multi_units,
                 s.multi_unit_retail,
                 s.multi_unit_retail_currency,
                 s.multi_selling_uom,
                 s.clear_retail,
                 s.clear_retail_currency,
                 s.clear_uom,
                 s.simple_promo_retail,
                 s.simple_promo_retail_currency,
                 s.simple_promo_uom,
                 s.rc_clearance_id,
                 s.rc_clearance_display_id,
                 s.rc_change_type,
                 s.rc_change_amount,
                 s.rc_change_currency,
                 s.rc_change_percent,
                 s.rc_change_selling_uom,
                 s.rc_price_guide_id,
                 s.item_parent,
                 s.diff_id,
                 s.zone_id,
                 s.max_hier_level,
                 s.cur_hier_level
            from (select t.event_id,
                         t.rc_clearance_id,
                         t.rc_clearance_display_id,
                         t.rc_effective_date,
                         t.rc_change_type,
                         t.rc_change_amount,
                         t.rc_change_currency,
                         t.rc_change_percent,
                         t.rc_change_selling_uom,
                         t.rc_price_guide_id,
                         t.reset_date,
                         rfrg2.price_event_id,
                         rfrg2.dept,
                         rfrg2.class,
                         rfrg2.subclass,
                         rfrg2.item,
                         rfrg2.zone_node_type,
                         rfrg2.location,
                         rfrg2.selling_retail,
                         rfrg2.selling_retail_currency,
                         rfrg2.selling_uom,
                         rfrg2.multi_units,
                         rfrg2.multi_unit_retail,
                         rfrg2.multi_unit_retail_currency,
                         rfrg2.multi_selling_uom,
                         rfrg2.clear_retail,
                         rfrg2.clear_retail_currency,
                         rfrg2.clear_uom,
                         rfrg2.simple_promo_retail,
                         rfrg2.simple_promo_retail_currency,
                         rfrg2.simple_promo_uom,
                         rfrg2.item_parent,
                         rfrg2.diff_id,
                         rfrg2.zone_id,
                         rfrg2.max_hier_level,
                         rfrg2.cur_hier_level,
                         RANK() OVER(PARTITION BY price_event_id,
                                                  item,
                                                  NVL(diff_id, '-999'),
                                                  location,
                                                  zone_node_type
                                         ORDER BY action_date desc) as rank_value
                    from rpm_future_retail_gtt rfrg2,
                         (select /*+ CARDINALITY(ids 10) */
                                 ids.numeric_col1           event_id,
                                 rc.clearance_id            rc_clearance_id,
                                 rc.clearance_display_id    rc_clearance_display_id,
                                 rc.effective_date          rc_effective_date,
                                 DECODE(rc.change_type,
                                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, RPM_CONSTANTS.RETAIL_AMOUNT_OFF_UOM_VALUE,
                                        rc.change_type)     rc_change_type,
                                 rc.change_amount           rc_change_amount,
                                 rc.change_currency         rc_change_currency,
                                 rc.change_percent          rc_change_percent,
                                 DECODE(rc.change_type,
                                        RPM_CONSTANTS.RETAIL_AMOUNT_OFF_VALUE, LP_USE_BASIS_UOM,
                                        rc.change_selling_uom) rc_change_selling_uom,
                                 NVL(rc.price_guide_id, 0)  rc_price_guide_id,
                                 rc.reset_date
                            from table(cast(I_parent_ids as OBJ_NUM_NUM_DATE_TBL)) ids,
                                 rpm_clearance rc
                           where ids.numeric_col2 is NOT NULL
                             and rc.clearance_id  = ids.numeric_col2
                             and rc.reset_date    is NOT NULL) t
                   where rfrg2.price_event_id = t.event_id
                     and rfrg2.action_date   <= t.rc_effective_date) s
           where s.rank_value = 1
             and NOT EXISTS (select 1
                               from rpm_future_retail_gtt gtt
                              where gtt.price_event_id       = s.price_event_id
                                and gtt.item                 = s.item
                                and NVL(gtt.diff_id, '-999') = NVL(s.diff_id, '-999')
                                and gtt.location             = s.location
                                and gtt.zone_node_type       = s.zone_node_type
                                and gtt.action_date          > LP_vdate
                                and gtt.clearance_id         is NOT NULL
                                and gtt.clear_start_ind      = RPM_CONSTANTS.END_IND)) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = rfrg3.reset_date)
   when MATCHED then
      update
         set rfrg1.clearance_id              = rc_clearance_id,
             rfrg1.clearance_display_id      = rc_clearance_display_id,
             rfrg1.clear_mkdn_index          = NULL,
             rfrg1.clear_start_ind           = RPM_CONSTANTS.END_IND,
             rfrg1.clear_change_type         = rc_change_type,
             rfrg1.clear_change_amount       = rc_change_amount,
             rfrg1.clear_change_currency     = rc_change_currency,
             rfrg1.clear_change_percent      = rc_change_percent,
             rfrg1.clear_change_selling_uom  = DECODE(rc_change_selling_uom,
                                                      LP_USE_BASIS_UOM,rfrg1.clear_uom,
                                                      rc_change_selling_uom),
             rfrg1.clear_price_guide_id      = rc_price_guide_id,
             rfrg1.clear_msg_type            = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
             rfrg1.step_identifier           = RPM_CONSTANTS.CAPT_GTT_MERGE_CLR_PARENT
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              clearance_id,
              clearance_display_id,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              clear_msg_type,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (rfrg3.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.zone_node_type,
              rfrg3.location,
              rfrg3.reset_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              rfrg3.rc_clearance_id,
              rfrg3.rc_clearance_display_id,
              RPM_CONSTANTS.END_IND,
              rfrg3.rc_change_type,
              rfrg3.rc_change_amount,
              rfrg3.rc_change_currency,
              rfrg3.rc_change_percent,
              DECODE(rfrg3.rc_change_selling_uom,
                     LP_USE_BASIS_UOM, rfrg3.clear_uom,
                     rfrg3.rc_change_selling_uom),
              rfrg3.rc_price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
              0,
              0,
              rfrg3.item_parent,
              rfrg3.diff_id,
              rfrg3.zone_id,
              rfrg3.max_hier_level,
              rfrg3.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_CLR_PARENT);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_MERGE_CLR_PARENT,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_CLEARANCE;

--------------------------------------------------------------------------------
FUNCTION UPD_CLEARANCE_RESET(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_clearance_ids    IN     OBJ_NUMERIC_ID_TABLE,
                             I_bulk_cc_pe_id    IN     NUMBER,
                             I_pe_sequence_id   IN     NUMBER,
                             I_pe_thread_number IN     NUMBER,
                             I_chunk_number     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.UPD_CLEARANCE_RESET';

   L_error_message VARCHAR2(255) := NULL;

   L_remove_dates OBJ_NUMERIC_DATE_TBL  := OBJ_NUMERIC_DATE_TBL();

BEGIN

   --wipe out any existing reset info
   update rpm_future_retail_gtt rfrg
      set rfrg.clearance_id             = NULL,
          rfrg.clearance_display_id     = NULL,
          rfrg.clear_mkdn_index         = NULL,
          rfrg.clear_start_ind          = NULL,
          rfrg.clear_change_type        = NULL,
          rfrg.clear_change_amount      = NULL,
          rfrg.clear_change_currency    = NULL,
          rfrg.clear_change_percent     = NULL,
          rfrg.clear_change_selling_uom = NULL,
          rfrg.clear_price_guide_id     = NULL,
          rfrg.step_identifier          = RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RST_NULLS
    where rfrg.clear_start_ind    = RPM_CONSTANTS.END_IND
      and TRUNC(rfrg.action_date) > LP_vdate
   returning OBJ_NUMERIC_DATE_REC(rfrg.price_event_id,
                                  rfrg.action_date) BULK COLLECT into L_remove_dates;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RST_NULLS,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   -- put new/updated reset on timeline
   merge /*+ INDEX (target RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt target
   using (select t.price_event_id,
                 t.future_retail_id,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.item,
                 t.zone_node_type,
                 t.location,
                 t.action_date,
                 t.selling_retail,
                 t.selling_retail_currency,
                 t.selling_uom,
                 t.multi_units,
                 t.multi_unit_retail,
                 t.multi_unit_retail_currency,
                 t.multi_selling_uom,
                 t.clear_retail,
                 t.clear_retail_currency,
                 t.clear_uom,
                 t.simple_promo_retail,
                 t.simple_promo_retail_currency,
                 t.simple_promo_uom,
                 t.on_simple_promo_ind,
                 t.on_complex_promo_ind,
                 t.effective_date,
                 rc.clearance_id,
                 rc.clearance_display_id,
                 rc.change_type,
                 rc.change_amount,
                 rc.change_currency,
                 rc.change_percent,
                 rc.change_selling_uom,
                 NVL(rc.price_guide_id, 0) price_guide_id,
                 t.item_parent,
                 t.diff_id,
                 t.zone_id,
                 t.max_hier_level,
                 t.cur_hier_level
            from (select inner.price_event_id,
                         inner.future_retail_id,
                         inner.dept,
                         inner.class,
                         inner.subclass,
                         inner.item,
                         inner.zone_node_type,
                         inner.location,
                         inner.action_date,
                         inner.selling_retail,
                         inner.selling_retail_currency,
                         inner.selling_uom,
                         inner.multi_units,
                         inner.multi_unit_retail,
                         inner.multi_unit_retail_currency,
                         inner.multi_selling_uom,
                         inner.clear_retail,
                         inner.clear_retail_currency,
                         inner.clear_uom,
                         inner.simple_promo_retail,
                         inner.simple_promo_retail_currency,
                         inner.simple_promo_uom,
                         inner.on_simple_promo_ind,
                         inner.on_complex_promo_ind,
                         inner.effective_date,
                         inner.item_parent,
                         inner.diff_id,
                         inner.zone_id,
                         inner.max_hier_level,
                         inner.cur_hier_level,
                         RANK() OVER (PARTITION BY inner.price_event_id,
                                                   inner.item,
                                                   NVL(inner.diff_id, '-999'),
                                                   inner.location,
                                                   inner.zone_node_type
                                          ORDER BY inner.action_date desc) as rank_value
                    from (-- Parent/Zone Timeline
                          select t1.price_event_id,
                                 t1.future_retail_id,
                                 t1.dept,
                                 t1.class,
                                 t1.subclass,
                                 t1.item,
                                 t1.diff_id,
                                 t1.location,
                                 t1.zone_node_type,
                                 t1.item_parent,
                                 t1.zone_id,
                                 t1.action_date,
                                 t1.selling_retail,
                                 t1.selling_retail_currency,
                                 t1.selling_uom,
                                 t1.multi_units,
                                 t1.multi_unit_retail,
                                 t1.multi_unit_retail_currency,
                                 t1.multi_selling_uom,
                                 t1.clear_retail,
                                 t1.clear_retail_currency,
                                 t1.clear_uom,
                                 t1.simple_promo_retail,
                                 t1.simple_promo_retail_currency,
                                 t1.simple_promo_uom,
                                 t1.on_simple_promo_ind,
                                 t1.on_complex_promo_ind,
                                 t1.max_hier_level,
                                 t1.cur_hier_level,
                                 t1.effective_date
                            from (select /*+ ordered */
                                         gtt.price_event_id,
                                         gtt.future_retail_id,
                                         gtt.dept,
                                         gtt.class,
                                         gtt.subclass,
                                         gtt.item,
                                         gtt.diff_id,
                                         gtt.location,
                                         gtt.zone_node_type,
                                         gtt.item_parent,
                                         gtt.zone_id,
                                         gtt.action_date,
                                         gtt.selling_retail,
                                         gtt.selling_retail_currency,
                                         gtt.selling_uom,
                                         gtt.multi_units,
                                         gtt.multi_unit_retail,
                                         gtt.multi_unit_retail_currency,
                                         gtt.multi_selling_uom,
                                         gtt.clear_retail,
                                         gtt.clear_retail_currency,
                                         gtt.clear_uom,
                                         gtt.simple_promo_retail,
                                         gtt.simple_promo_retail_currency,
                                         gtt.simple_promo_uom,
                                         gtt.on_simple_promo_ind,
                                         gtt.on_complex_promo_ind,
                                         gtt.max_hier_level,
                                         gtt.cur_hier_level,
                                         clr.effective_date,
                                         ROW_NUMBER() OVER (PARTITION BY gtt.price_event_id,
                                                                         gtt.item,
                                                                         NVL(gtt.diff_id, '-999'),
                                                                         gtt.location,
                                                                         gtt.zone_node_type
                                                                ORDER BY clr.item,
                                                                         clr.location,
                                                                         gtt.action_date desc) as rank_value
                                    from rpm_future_retail_gtt gtt,
                                         item_master im,
                                         rpm_zone_location rzl,
                                         rpm_clearance_gtt clr
                                   where gtt.cur_hier_level IN (RPM_CONSTANTS.FR_HIER_PARENT_ZONE,
                                                                RPM_CONSTANTS.FR_HIER_DIFF_ZONE)
                                     and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                     and im.item_parent     = gtt.item
                                     and im.tran_level      = im.item_level
                                     and rzl.zone_id        = gtt.location
                                     and clr.price_event_id = gtt.price_event_id
                                     and clr.item           = im.item
                                     and clr.location       = rzl.location
                                     and clr.reset_ind      = 1
                                     and clr.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                     and clr.effective_date is NOT NULL
                                     and gtt.action_date    <= clr.effective_date) t1
                           where t1.rank_value = 1
                          union all
                          -- Parent/Loc and Diff/Loc Timeline
                          select t2.price_event_id,
                                 t2.future_retail_id,
                                 t2.dept,
                                 t2.class,
                                 t2.subclass,
                                 t2.item,
                                 t2.diff_id,
                                 t2.location,
                                 t2.zone_node_type,
                                 t2.item_parent,
                                 t2.zone_id,
                                 t2.action_date,
                                 t2.selling_retail,
                                 t2.selling_retail_currency,
                                 t2.selling_uom,
                                 t2.multi_units,
                                 t2.multi_unit_retail,
                                 t2.multi_unit_retail_currency,
                                 t2.multi_selling_uom,
                                 t2.clear_retail,
                                 t2.clear_retail_currency,
                                 t2.clear_uom,
                                 t2.simple_promo_retail,
                                 t2.simple_promo_retail_currency,
                                 t2.simple_promo_uom,
                                 t2.on_simple_promo_ind,
                                 t2.on_complex_promo_ind,
                                 t2.max_hier_level,
                                 t2.cur_hier_level,
                                 t2.effective_date
                            from (select /*+ ordered */
                                         gtt.price_event_id,
                                         gtt.future_retail_id,
                                         gtt.dept,
                                         gtt.class,
                                         gtt.subclass,
                                         gtt.item,
                                         gtt.diff_id,
                                         gtt.location,
                                         gtt.zone_node_type,
                                         gtt.item_parent,
                                         gtt.zone_id,
                                         gtt.action_date,
                                         gtt.selling_retail,
                                         gtt.selling_retail_currency,
                                         gtt.selling_uom,
                                         gtt.multi_units,
                                         gtt.multi_unit_retail,
                                         gtt.multi_unit_retail_currency,
                                         gtt.multi_selling_uom,
                                         gtt.clear_retail,
                                         gtt.clear_retail_currency,
                                         gtt.clear_uom,
                                         gtt.simple_promo_retail,
                                         gtt.simple_promo_retail_currency,
                                         gtt.simple_promo_uom,
                                         gtt.on_simple_promo_ind,
                                         gtt.on_complex_promo_ind,
                                         gtt.max_hier_level,
                                         gtt.cur_hier_level,
                                         clr.effective_date,
                                         ROW_NUMBER() OVER (PARTITION BY gtt.price_event_id,
                                                                         gtt.item,
                                                                         NVL(gtt.diff_id, '-999'),
                                                                         gtt.location,
                                                                         gtt.zone_node_type
                                                                ORDER BY clr.item,
                                                                         clr.location,
                                                                         gtt.action_date desc ) as rank_value
                                    from rpm_future_retail_gtt gtt,
                                         item_master im,
                                         rpm_clearance_gtt clr
                                   where gtt.cur_hier_level IN (RPM_CONSTANTS.FR_HIER_PARENT_LOC,
                                                                RPM_CONSTANTS.FR_HIER_DIFF_LOC)
                                     and gtt.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                     and im.item_parent     = gtt.item
                                     and im.tran_level      = im.item_level
                                     and clr.price_event_id = gtt.price_event_id
                                     and clr.item           = im.item
                                     and clr.location       = gtt.location
                                     and clr.reset_ind      = 1
                                     and clr.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                     and clr.effective_date is NOT NULL
                                     and gtt.action_date    <= clr.effective_date) t2
                           where t2.rank_value = 1
                          union all
                          -- Item/Zone Timeline
                          select t3.price_event_id,
                                 t3.future_retail_id,
                                 t3.dept,
                                 t3.class,
                                 t3.subclass,
                                 t3.item,
                                 t3.diff_id,
                                 t3.location,
                                 t3.zone_node_type,
                                 t3.item_parent,
                                 t3.zone_id,
                                 t3.action_date,
                                 t3.selling_retail,
                                 t3.selling_retail_currency,
                                 t3.selling_uom,
                                 t3.multi_units,
                                 t3.multi_unit_retail,
                                 t3.multi_unit_retail_currency,
                                 t3.multi_selling_uom,
                                 t3.clear_retail,
                                 t3.clear_retail_currency,
                                 t3.clear_uom,
                                 t3.simple_promo_retail,
                                 t3.simple_promo_retail_currency,
                                 t3.simple_promo_uom,
                                 t3.on_simple_promo_ind,
                                 t3.on_complex_promo_ind,
                                 t3.max_hier_level,
                                 t3.cur_hier_level,
                                 t3.effective_date
                            from (select /*+ ordered */
                                         gtt.price_event_id,
                                         gtt.future_retail_id,
                                         gtt.dept,
                                         gtt.class,
                                         gtt.subclass,
                                         gtt.item,
                                         gtt.diff_id,
                                         gtt.location,
                                         gtt.zone_node_type,
                                         gtt.item_parent,
                                         gtt.zone_id,
                                         gtt.action_date,
                                         gtt.selling_retail,
                                         gtt.selling_retail_currency,
                                         gtt.selling_uom,
                                         gtt.multi_units,
                                         gtt.multi_unit_retail,
                                         gtt.multi_unit_retail_currency,
                                         gtt.multi_selling_uom,
                                         gtt.clear_retail,
                                         gtt.clear_retail_currency,
                                         gtt.clear_uom,
                                         gtt.simple_promo_retail,
                                         gtt.simple_promo_retail_currency,
                                         gtt.simple_promo_uom,
                                         gtt.on_simple_promo_ind,
                                         gtt.on_complex_promo_ind,
                                         gtt.max_hier_level,
                                         gtt.cur_hier_level,
                                         clr.effective_date,
                                         ROW_NUMBER() OVER (PARTITION BY gtt.price_event_id,
                                                                         gtt.item,
                                                                         NVL(gtt.diff_id, '-999'),
                                                                         gtt.location,
                                                                         gtt.zone_node_type
                                                                ORDER BY clr.item,
                                                                         clr.location,
                                                                         gtt.action_date desc) as rank_value
                                    from rpm_future_retail_gtt gtt,
                                         rpm_zone_location rzl,
                                         rpm_clearance_gtt clr
                                   where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                     and gtt.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                     and rzl.zone_id        = gtt.location
                                     and clr.price_event_id = gtt.price_event_id
                                     and clr.item           = gtt.item
                                     and clr.location       = rzl.location
                                     and clr.reset_ind      = 1
                                     and clr.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                     and clr.effective_date is NOT NULL
                                     and gtt.action_date    <= clr.effective_date) t3
                           where t3.rank_value = 1
                          union all
                          -- Item/Loc Timeline
                          select t4.price_event_id,
                                 t4.future_retail_id,
                                 t4.dept,
                                 t4.class,
                                 t4.subclass,
                                 t4.item,
                                 t4.diff_id,
                                 t4.location,
                                 t4.zone_node_type,
                                 t4.item_parent,
                                 t4.zone_id,
                                 t4.action_date,
                                 t4.selling_retail,
                                 t4.selling_retail_currency,
                                 t4.selling_uom,
                                 t4.multi_units,
                                 t4.multi_unit_retail,
                                 t4.multi_unit_retail_currency,
                                 t4.multi_selling_uom,
                                 t4.clear_retail,
                                 t4.clear_retail_currency,
                                 t4.clear_uom,
                                 t4.simple_promo_retail,
                                 t4.simple_promo_retail_currency,
                                 t4.simple_promo_uom,
                                 t4.on_simple_promo_ind,
                                 t4.on_complex_promo_ind,
                                 t4.max_hier_level,
                                 t4.cur_hier_level,
                                 t4.effective_date
                            from (select /*+ ORDERED */
                                         gtt.price_event_id,
                                         gtt.future_retail_id,
                                         gtt.dept,
                                         gtt.class,
                                         gtt.subclass,
                                         gtt.item,
                                         gtt.diff_id,
                                         gtt.location,
                                         gtt.zone_node_type,
                                         gtt.item_parent,
                                         gtt.zone_id,
                                         gtt.action_date,
                                         gtt.selling_retail,
                                         gtt.selling_retail_currency,
                                         gtt.selling_uom,
                                         gtt.multi_units,
                                         gtt.multi_unit_retail,
                                         gtt.multi_unit_retail_currency,
                                         gtt.multi_selling_uom,
                                         gtt.clear_retail,
                                         gtt.clear_retail_currency,
                                         gtt.clear_uom,
                                         gtt.simple_promo_retail,
                                         gtt.simple_promo_retail_currency,
                                         gtt.simple_promo_uom,
                                         gtt.on_simple_promo_ind,
                                         gtt.on_complex_promo_ind,
                                         gtt.max_hier_level,
                                         gtt.cur_hier_level,
                                         clr.effective_date,
                                         ROW_NUMBER() OVER (PARTITION BY gtt.price_event_id,
                                                                         gtt.item,
                                                                         NVL(gtt.diff_id, '-999'),
                                                                         gtt.location,
                                                                         gtt.zone_node_type
                                                                ORDER BY clr.item,
                                                                         clr.location,
                                                                         gtt.action_date desc) as rank_value
                                    from rpm_future_retail_gtt gtt,
                                         rpm_clearance_gtt clr
                                   where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                     and clr.price_event_id = gtt.price_event_id
                                     and gtt.item           = clr.item
                                     and gtt.location       = clr.location
                                     and clr.reset_ind      = 1
                                     and clr.state          != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                     and clr.effective_date is NOT NULL
                                     and gtt.action_date    <= clr.effective_date) t4
                           where t4.rank_value = 1 ) inner ) t,
                 rpm_clearance_gtt rc
           where t.rank_value      = 1
             and rc.price_event_id = t.price_event_id
             and rc.clearance_id   = t.price_event_id) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.action_date          = source.effective_date)
   when MATCHED then
      update
         set target.clearance_id             = source.clearance_id,
             target.clearance_display_id     = source.clearance_display_id,
             target.clear_mkdn_index         = RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX,
             target.clear_start_ind          = RPM_CONSTANTS.END_IND,
             target.clear_change_type        = source.change_type,
             target.clear_change_amount      = source.change_amount,
             target.clear_change_currency    = source.change_currency,
             target.clear_change_percent     = source.change_percent,
             target.clear_change_selling_uom = source.change_selling_uom,
             target.clear_price_guide_id     = source.price_guide_id,
             target.clear_msg_type           = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
             target.step_identifier          = RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RESET
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              clear_msg_type,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.effective_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.clearance_id,
              source.clearance_display_id,
              RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX,
              RPM_CONSTANTS.END_IND,
              source.change_type,
              source.change_amount,
              source.change_currency,
              source.change_percent,
              source.change_selling_uom,
              source.price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
              0,
              0,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RESET);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RESET,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   update rpm_future_retail_gtt rfrg
      set rfrg.clear_msg_type  = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD,
          rfrg.step_identifier = RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RST_MSG_TYPE
    where rfrg.clear_start_ind  = RPM_CONSTANTS.END_IND
      and rfrg.action_date     >= LP_vdate
      and EXISTS (select 'x'
                    from table(cast(L_remove_dates as OBJ_NUMERIC_DATE_TBL)) remove
                   where remove.numeric_col = rfrg.price_event_id);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_CLR_RST_MSG_TYPE,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPD_CLEARANCE_RESET;

--------------------------------------------------------------------------------

FUNCTION UPD_NEW_CLEARANCE_RESET(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_clearance_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.UPD_NEW_CLEARANCE_RESET';

   L_error_message VARCHAR2(255) := NULL;

   L_remove_dates OBJ_NUMERIC_DATE_TBL := OBJ_NUMERIC_DATE_TBL();

BEGIN

   --wipe out any existing reset info
   update rpm_future_retail_gtt rfrg
      set rfrg.clearance_id             = NULL,
          rfrg.clearance_display_id     = NULL,
          rfrg.clear_mkdn_index         = NULL,
          rfrg.clear_start_ind          = NULL,
          rfrg.clear_change_type        = NULL,
          rfrg.clear_change_amount      = NULL,
          rfrg.clear_change_currency    = NULL,
          rfrg.clear_change_percent     = NULL,
          rfrg.clear_change_selling_uom = NULL,
          rfrg.clear_price_guide_id     = NULL,
          rfrg.step_identifier          = RPM_CONSTANTS.CAPT_GTT_UPD_NCR_NULL
    where rfrg.clear_start_ind    = RPM_CONSTANTS.END_IND
      and TRUNC(rfrg.action_date) > LP_vdate
   returning OBJ_NUMERIC_DATE_REC(rfrg.price_event_id,
                                  rfrg.action_date) BULK COLLECT into L_remove_dates;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_NCR_NULL,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   -- put new/updated reset on timeline
   merge /*+ INDEX (rfrg1 rpm_future_retail_gtt_i1) */ into rpm_future_retail_gtt target
   using (select t.price_event_id,
                 t.future_retail_id,
                 t.item,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.zone_node_type,
                 t.location,
                 t.action_date,
                 t.selling_retail,
                 t.selling_retail_currency,
                 t.selling_uom,
                 t.multi_units,
                 t.multi_unit_retail,
                 t.multi_unit_retail_currency,
                 t.multi_selling_uom,
                 t.clear_exception_parent_id,
                 t.clear_retail,
                 t.clear_retail_currency,
                 t.clear_uom,
                 t.simple_promo_retail,
                 t.simple_promo_retail_currency,
                 t.simple_promo_uom,
                 t.on_simple_promo_ind,
                 t.on_complex_promo_ind,
                 t.promo_comp_msg_type,
                 t.price_change_id,
                 t.price_change_display_id,
                 t.pc_exception_parent_id,
                 t.pc_change_type,
                 t.pc_change_amount,
                 t.pc_change_currency,
                 t.pc_change_percent,
                 t.pc_change_selling_uom,
                 t.pc_null_multi_ind,
                 t.pc_multi_units,
                 t.pc_multi_unit_retail,
                 t.pc_multi_unit_retail_currency,
                 t.pc_multi_selling_uom,
                 t.pc_price_guide_id,
                 t.pc_msg_type,
                 t.pc_selling_retail_ind,
                 t.pc_multi_unit_ind,
                 t.clearance_id,
                 t.clearance_display_id,
                 t.clear_mkdn_index,
                 t.clear_start_ind,
                 t.clear_change_type,
                 t.clear_change_amount,
                 t.clear_change_currency,
                 t.clear_change_percent,
                 t.clear_change_selling_uom,
                 t.clear_price_guide_id,
                 t.clear_msg_type,
                 t.loc_move_from_zone_id,
                 t.loc_move_to_zone_id,
                 t.location_move_id,
                 t.lock_version,
                 t.timeline_seq,
                 t.rfr_rowid,
                 t.item_parent,
                 t.diff_id,
                 t.zone_id,
                 t.max_hier_level,
                 t.cur_hier_level,
                 t.original_fr_id,
                 t.effective_date,
                 t.rank_value,
                 rc.clearance_id            rc_clearance_id,
                 rc.clearance_display_id    rc_clearance_display_id,
                 rc.change_type             rc_change_type,
                 rc.change_amount           rc_change_amount,
                 rc.change_currency         rc_change_currency,
                 rc.change_percent          rc_change_percent,
                 rc.change_selling_uom      rc_change_selling_uom,
                 NVL(rc.price_guide_id, 0)  rc_price_guide_id
            from (select inner.price_event_id,
                         inner.future_retail_id,
                         inner.item,
                         inner.dept,
                         inner.class,
                         inner.subclass,
                         inner.zone_node_type,
                         inner.location,
                         inner.action_date,
                         inner.selling_retail,
                         inner.selling_retail_currency,
                         inner.selling_uom,
                         inner.multi_units,
                         inner.multi_unit_retail,
                         inner.multi_unit_retail_currency,
                         inner.multi_selling_uom,
                         inner.clear_exception_parent_id,
                         inner.clear_retail,
                         inner.clear_retail_currency,
                         inner.clear_uom,
                         inner.simple_promo_retail,
                         inner.simple_promo_retail_currency,
                         inner.simple_promo_uom,
                         inner.on_simple_promo_ind,
                         inner.on_complex_promo_ind,
                         inner.promo_comp_msg_type,
                         inner.price_change_id,
                         inner.price_change_display_id,
                         inner.pc_exception_parent_id,
                         inner.pc_change_type,
                         inner.pc_change_amount,
                         inner.pc_change_currency,
                         inner.pc_change_percent,
                         inner.pc_change_selling_uom,
                         inner.pc_null_multi_ind,
                         inner.pc_multi_units,
                         inner.pc_multi_unit_retail,
                         inner.pc_multi_unit_retail_currency,
                         inner.pc_multi_selling_uom,
                         inner.pc_price_guide_id,
                         inner.pc_msg_type,
                         inner.pc_selling_retail_ind,
                         inner.pc_multi_unit_ind,
                         inner.clearance_id,
                         inner.clearance_display_id,
                         inner.clear_mkdn_index,
                         inner.clear_start_ind,
                         inner.clear_change_type,
                         inner.clear_change_amount,
                         inner.clear_change_currency,
                         inner.clear_change_percent,
                         inner.clear_change_selling_uom,
                         inner.clear_price_guide_id,
                         inner.clear_msg_type,
                         inner.loc_move_from_zone_id,
                         inner.loc_move_to_zone_id,
                         inner.location_move_id,
                         inner.lock_version,
                         inner.timeline_seq,
                         inner.rfr_rowid,
                         inner.item_parent,
                         inner.diff_id,
                         inner.zone_id,
                         inner.max_hier_level,
                         inner.cur_hier_level,
                         inner.original_fr_id,
                         reset.effective_date,
                         RANK() OVER (PARTITION BY inner.price_event_id,
                                                   inner.item,
                                                   NVL(inner.diff_id, '-999'),
                                                   inner.location,
                                                   inner.zone_node_type
                                          ORDER BY inner.action_date desc) as rank_value
                    from rpm_future_retail_gtt inner,
                         (select distinct gtt.price_event_id,
                                          clr.item,
                                          clr.location,
                                          clr.effective_date
                            from rpm_clearance_gtt clr,
                                 rpm_future_retail_gtt gtt
                           where clr.reset_ind      = 1
                             and clr.state         != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                             and clr.price_event_id = gtt.price_event_id
                             and gtt.item           = clr.item
                             and gtt.location       = clr.location
                             and clr.effective_date is NOT NULL) reset
                   where inner.price_event_id = reset.price_event_id
                     and inner.action_date   <= reset.effective_date
                     and inner.item           = reset.item
                     and inner.location       = reset.location) t,
                 rpm_clearance_gtt rc
           where rank_value        = 1
             and rc.price_event_id = t.price_event_id
             and rc.item           = t.item
             and rc.location       = t.location
             and rc.reset_ind      = 1) source
   on (    target.price_event_id       = source.price_event_id
       and target.item                 = source.item
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.location             = source.location
       and target.zone_node_type       = source.zone_node_type
       and target.action_date          = source.effective_date)
   when MATCHED then
      update
         set target.clearance_id             = source.rc_clearance_id,
             target.clearance_display_id     = source.rc_clearance_display_id,
             target.clear_mkdn_index         = RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX,
             target.clear_start_ind          = RPM_CONSTANTS.END_IND,
             target.clear_change_type        = source.rc_change_type,
             target.clear_change_amount      = source.rc_change_amount,
             target.clear_change_currency    = source.rc_change_currency,
             target.clear_change_percent     = source.rc_change_percent,
             target.clear_change_selling_uom = source.rc_change_selling_uom,
             target.clear_price_guide_id     = source.rc_price_guide_id,
             target.clear_msg_type           = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
             target.step_identifier          = RPM_CONSTANTS.CAPT_GTT_UPD_NEW_CLR_RESET
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              clear_msg_type,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.zone_node_type,
              source.location,
              source.effective_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.rc_clearance_id,
              source.rc_clearance_display_id,
              RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX,
              RPM_CONSTANTS.END_IND,
              source.rc_change_type,
              source.rc_change_amount,
              source.rc_change_currency,
              source.rc_change_percent,
              source.rc_change_selling_uom,
              source.rc_price_guide_id,
              RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_CRE,
              0,
              0,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_UPD_NEW_CLR_RESET);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_NEW_CLR_RESET,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   update rpm_future_retail_gtt rfrg
      set rfrg.clear_msg_type  = RPM_CONSTANTS.RIB_MSG_TYPE_CLEAR_PC_MOD,
          rfrg.step_identifier = RPM_CONSTANTS.CAPT_GTT_NCR_MSG_TYPE
    where rfrg.clear_start_ind  = RPM_CONSTANTS.END_IND
      and rfrg.action_date     >= LP_vdate
      and EXISTS (select 'x'
                    from table(cast(L_remove_dates as OBJ_NUMERIC_DATE_TBL)) remove
                   where remove.numeric_col = rfrg.price_event_id);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_NCR_MSG_TYPE,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPD_NEW_CLEARANCE_RESET;

--------------------------------------------------------------------------------

FUNCTION UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_rib_trans_id    IN     NUMBER,
                                 I_bulk_cc_pe_id   IN     NUMBER,
                                 I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                 I_chunk_number    IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.UPD_RESET_ON_CLR_REMOVE';

   L_deleted_clearance_reset_ids OBJ_CLEARANCE_ITEM_LOC_TBL := NULL;

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   if REMOVE_CLEARANCE_RESETS(O_cc_error_tbl,
                              L_deleted_clearance_reset_ids) = 0 then
      return 0;
   end if;

   if RPM_CC_PUBLISH.STAGE_UPD_RESET_ON_CLR_REMOVE(O_cc_error_tbl,
                                                   I_price_event_ids,
                                                   L_deleted_clearance_reset_ids,
                                                   I_bulk_cc_pe_id,
                                                   I_chunk_number) = 0 then
      return 0;
   end if;

   -- remove the reset if there are no markdowns left
   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt upd
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 reset_date action_date,
                 clear_msg_type
            from (select distinct reset.price_event_id,
                         reset.item,
                         reset.diff_id,
                         reset.location,
                         reset.zone_node_type,
                         reset.reset_date,
                         reset.clear_msg_type,
                         FIRST_VALUE(basis.clear_start_ind) OVER (PARTITION BY basis.price_event_id,
                                                                               basis.item,
                                                                               NVL(basis.diff_id, '-999'),
                                                                               basis.location,
                                                                               basis.zone_node_type
                                                                      ORDER BY basis.action_date desc) basis_clear_start_ind
                    from rpm_future_retail_gtt basis,
                         (select gtt.price_event_id,
                                 gtt.item,
                                 gtt.diff_id,
                                 gtt.location,
                                 gtt.zone_node_type,
                                 gtt.action_date reset_date,
                                 gtt.clear_msg_type
                            from rpm_future_retail_gtt gtt
                           where gtt.action_date      > LP_vdate
                             and gtt.clear_mkdn_index = RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX
                             and rownum               > 0) reset
                   where basis.price_event_id         = reset.price_event_id
                     and basis.item                   = reset.item
                     and NVL(basis.diff_id, '-99999') = NVL(reset.diff_id, '-99999')
                     and basis.location               = reset.location
                     and basis.zone_node_type         = reset.zone_node_type
                     and basis.action_date            < reset.reset_date) del
           where NVL(del.basis_clear_start_ind, -999) NOT IN (RPM_CONSTANTS.START_IND,
                                                              RPM_CONSTANTS.IN_PROCESS_IND)) source
   on (    upd.price_event_id         = source.price_event_id
       and upd.item                   = source.item
       and NVL(upd.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and upd.location               = source.location
       and upd.zone_node_type         = source.zone_node_type
       and upd.action_date            = source.action_date)
   when MATCHED then
      update
         set upd.clearance_id             = NULL,
             upd.clearance_display_id     = NULL,
             upd.clear_mkdn_index         = NULL,
             upd.clear_start_ind          = NULL,
             upd.clear_change_type        = NULL,
             upd.clear_change_amount      = NULL,
             upd.clear_change_currency    = NULL,
             upd.clear_change_percent     = NULL,
             upd.clear_change_selling_uom = NULL,
             upd.clear_price_guide_id     = NULL,
             upd.clear_msg_type           = source.clear_msg_type,
             upd.step_identifier          = RPM_CONSTANTS.CAPT_GTT_UPD_RST_CLR_REM_NULLS;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_RST_CLR_REM_NULLS,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   -- update the reset if there are markdowns left
   merge /*+ INDEX (upd RPM_FUTURE_RETAIL_GTT_I1) */ into rpm_future_retail_gtt upd
   using (select price_event_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 reset_date action_date,
                 clearance_id,
                 clearance_display_id,
                 clear_mkdn_index,
                 clear_start_ind,
                 clear_change_type,
                 clear_change_amount,
                 clear_change_currency,
                 clear_change_percent,
                 clear_change_selling_uom,
                 clear_price_guide_id
            from (select /*+ LEADING(RESET) INDEX(basis RPM_FUTURE_RETAIL_GTT_I1) */ distinct reset.price_event_id,
                         reset.item,
                         reset.diff_id,
                         reset.location,
                         reset.zone_node_type,
                         reset.reset_date,
                         reset.clear_mkdn_index,
                         basis.clearance_id,
                         basis.clearance_display_id,
                         basis.clear_change_type,
                         basis.clear_change_amount,
                         basis.clear_change_currency,
                         basis.clear_change_percent,
                         basis.clear_change_selling_uom,
                         NVL(basis.clear_price_guide_id, 0) clear_price_guide_id,
                         basis.clear_start_ind,
                         RANK() OVER (PARTITION BY basis.price_event_id,
                                                   basis.item,
                                                   NVL(basis.diff_id, '-999'),
                                                   basis.location,
                                                   basis.zone_node_type
                                          ORDER BY basis.action_date desc) as rank_value
                    from rpm_future_retail_gtt basis,
                         (select gtt.price_event_id,
                                 gtt.item,
                                 gtt.diff_id,
                                 gtt.location,
                                 gtt.zone_node_type,
                                 gtt.action_date reset_date,
                                 gtt.clear_mkdn_index
                            from rpm_future_retail_gtt gtt
                           where gtt.action_date      > LP_vdate
                             and gtt.clear_mkdn_index = RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX
                             and rownum               > 0) reset
                   where basis.price_event_id         = reset.price_event_id
                     and basis.item                   = reset.item
                     and NVL(basis.diff_id, '-99999') = NVL(reset.diff_id, '-99999')
                     and basis.location               = reset.location
                     and basis.zone_node_type         = reset.zone_node_type
                     and basis.action_date            < reset.reset_date) del
           where del.rank_value      = 1
             and del.clear_start_ind IN (RPM_CONSTANTS.START_IND,
                                         RPM_CONSTANTS.IN_PROCESS_IND)) source
   on (    upd.price_event_id         = source.price_event_id
       and upd.item                   = source.item
       and NVL(upd.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and upd.location               = source.location
       and upd.zone_node_type         = source.zone_node_type
       and upd.action_date            = source.action_date)
   when MATCHED then
      update
         set upd.clearance_id             = source.clearance_id,
             upd.clearance_display_id     = source.clearance_display_id,
             upd.clear_change_type        = source.clear_change_type,
             upd.clear_change_amount      = source.clear_change_amount,
             upd.clear_change_currency    = source.clear_change_currency,
             upd.clear_change_percent     = source.clear_change_percent,
             upd.clear_change_selling_uom = source.clear_change_selling_uom,
             upd.clear_price_guide_id     = source.clear_price_guide_id,
             upd.clear_msg_type           = NULL,
             upd.step_identifier          = RPM_CONSTANTS.CAPT_GTT_UPD_RST_ON_CLR_REMOVE;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_UPD_RST_ON_CLR_REMOVE,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END UPD_RESET_ON_CLR_REMOVE;
--------------------------------------------------------------------------------

FUNCTION REMOVE_CLEARANCE_RESETS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                                 O_deleted_clearance_id    OUT OBJ_CLEARANCE_ITEM_LOC_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_CLEARANCE_RESETS';

   TYPE TYP_ROWID is TABLE of rowid;

   L_rowids TYP_ROWID := NULL;

BEGIN

   -- Clearance Reset is always stored at the Item/Location
   -- So explode the RPM_FUTURE_RETAIL_GTT into Item/Location level
   -- This process is using the RPM_ZONE_FUTURE_RETAIL_GTT
   -- Please notice the use of the column carefully
   -- We do not need to check the RANGED Item/Location here since it is guaranteed that
   -- the reset records in RPM_CLEARANCE are always for RANGED item/location

   delete from rpm_zone_future_retail_gtt;

   insert into rpm_zone_future_retail_gtt(price_event_id,        -- price_event_id
                                          zone_future_retail_id, -- future_retail_id
                                          item,                  -- transaction level item
                                          zone,                  -- location
                                          action_date,           -- action_date
                                          price_change_id,       -- clearance_id
                                          pc_change_type,        -- clear_mkdn_index
                                          pc_null_multi_ind)     -- clear_start_ind
      with clr_item_loc as
         (select  t.price_event_id,
                  t.future_retail_id,
                  t.item,
                  t.location,
                  t.action_date,
                  t.clearance_id,
                  t.clear_mkdn_index,
                  t.clear_start_ind
             from (select s.price_event_id,
                          s.future_retail_id,
                          s.item,
                          s.location,
                          s.action_date,
                          s.clearance_id,
                          s.clear_mkdn_index,
                          s.clear_start_ind,
                          RANK() OVER (PARTITION BY s.price_event_id,
                                                    s.item,
                                                    s.location,
                                                    s.action_date
                                           ORDER BY s.fr_level desc) rank
                     from (-- Item / Location
                           select gtt.price_event_id,
                                  gtt.future_retail_id,
                                  gtt.item,
                                  gtt.location,
                                  gtt.action_date,
                                  gtt.clearance_id,
                                  gtt.clear_mkdn_index,
                                  gtt.clear_start_ind,
                                  3 fr_level
                             from rpm_future_retail_gtt gtt
                            where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                           union all
                           -- Parent Diff / Location
                           select gtt.price_event_id,
                                  gtt.future_retail_id,
                                  im.item,
                                  gtt.location,
                                  gtt.action_date,
                                  gtt.clearance_id,
                                  gtt.clear_mkdn_index,
                                  gtt.clear_start_ind,
                                  2 fr_level
                             from rpm_future_retail_gtt gtt,
                                  item_master im
                            where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                              and im.item_parent     = gtt.item
                              and im.diff_1          = gtt.diff_id
                              and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           union all
                           -- Parent / Location
                           select gtt.price_event_id,
                                  gtt.future_retail_id,
                                  im.item,
                                  gtt.location,
                                  gtt.action_date,
                                  gtt.clearance_id,
                                  gtt.clear_mkdn_index,
                                  gtt.clear_start_ind,
                                  1 fr_level
                             from rpm_future_retail_gtt gtt,
                                  item_master im
                            where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                              and im.item_parent     = gtt.item
                              and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                           union all
                           -- Item / Zone
                           select gtt.price_event_id,
                                  gtt.future_retail_id,
                                  gtt.item,
                                  rzl.location,
                                  gtt.action_date,
                                  gtt.clearance_id,
                                  gtt.clear_mkdn_index,
                                  gtt.clear_start_ind,
                                  2 fr_level
                             from rpm_future_retail_gtt gtt,
                                  rpm_zone_location rzl
                            where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                              and rzl.zone_id        = gtt.location
                           union all
                           -- Parent Diff / Zone
                           select gtt.price_event_id,
                                  gtt.future_retail_id,
                                  im.item,
                                  rzl.location,
                                  gtt.action_date,
                                  gtt.clearance_id,
                                  gtt.clear_mkdn_index,
                                  gtt.clear_start_ind,
                                  1 fr_level
                             from rpm_future_retail_gtt gtt,
                                  item_master im,
                                  rpm_zone_location rzl
                            where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                              and im.item_parent     = gtt.item
                              and im.diff_1          = gtt.diff_id
                              and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and rzl.zone_id        = gtt.location
                           union all
                           -- Parent / Zone
                           select gtt.price_event_id,
                                  gtt.future_retail_id,
                                  im.item,
                                  rzl.location,
                                  gtt.action_date,
                                  gtt.clearance_id,
                                  gtt.clear_mkdn_index,
                                  gtt.clear_start_ind,
                                  0 fr_level
                             from rpm_future_retail_gtt gtt,
                                  item_master im,
                                  rpm_zone_location rzl
                            where gtt.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                              and im.item_parent     = gtt.item
                              and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                              and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                              and rzl.zone_id        = gtt.location) s) t
                             where t. rank = 1),
      clr_exclusion as
         (-- item loc level
          select clr.clearance_id,
                 clr.exception_parent_id,
                 il.item,
                 il.location
            from rpm_clearance clr,
                 clr_item_loc il
           where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
             and clr.exception_parent_id = il.price_event_id
             and clr.item                = il.item
             and clr.location            = il.location
             and clr.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
          union
          -- item zone level
          select clr.clearance_id,
                 clr.exception_parent_id,
                 il.item,
                 il.location
            from rpm_clearance clr,
                 clr_item_loc il,
                 rpm_zone_location rzl
           where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
             and clr.exception_parent_id = il.price_event_id
             and clr.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and clr.zone_id             = rzl.zone_id
             and rzl.location            = il.location
             and clr.item                = il.item
          union
          -- parent loc level
          select clr.clearance_id,
                 clr.exception_parent_id,
                 il.item,
                 il.location
            from rpm_clearance clr,
                 clr_item_loc il,
                 item_master im
           where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
             and clr.exception_parent_id = il.price_event_id
             and clr.zone_node_type      IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and clr.item                = im.item_parent
             and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and il.item                 = im.item
             and clr.location            = il.location
          union
          -- parent zone level
          select clr.clearance_id,
                 clr.exception_parent_id,
                 il.item,
                 il.location
            from rpm_clearance clr,
                 clr_item_loc il,
                 rpm_zone_location rzl,
                 item_master im
           where clr.state               = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
             and clr.exception_parent_id = il.price_event_id
             and clr.zone_node_type      = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and clr.zone_id             = rzl.zone_id
             and rzl.location            = il.location
             and clr.item                = im.item_parent
             and il.item                 = im.item
             and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES)
      select t.price_event_id,
             t.future_retail_id,
             t.item,
             t.location,
             t.action_date,
             t.clearance_id,
             t.clear_mkdn_index,
             t.clear_start_ind
        from clr_item_loc t,
             clr_exclusion rc
      where  t.item          = rc.item (+)
        and t.location       = rc.location (+)
        and t.price_event_id = rc.exception_parent_id (+)
        and rc.clearance_id  is NULL;

   -- Find all item/loc for which the clearance reset reference needs to be removed
   delete gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date(varchar2_1, -- item
                                             number_1)   -- location
      select del.item,
             del.location
        from (select distinct basis.item,
                     basis.location,
                     reset_date,
                     FIRST_VALUE(basis.clear_start_ind) OVER (PARTITION BY basis.price_event_id,
                                                                           basis.item,
                                                                           basis.location
                                                                  ORDER BY basis.action_date desc) clear_start_ind
                from (select price_event_id,
                             item,
                             zone location,
                             action_date action_date,
                             price_change_id clearance_id,
                             pc_change_type clear_mkdn_index,
                             pc_null_multi_ind clear_start_ind
                        from rpm_zone_future_retail_gtt
                       where rownum > 0) basis,
                     (select gtt.price_event_id,
                             gtt.item,
                             gtt.zone location,
                             gtt.action_date reset_date
                        from rpm_zone_future_retail_gtt gtt
                       where gtt.action_date    > LP_vdate
                         and gtt.pc_change_type = RPM_CONSTANTS.RESET_ROW_MARKDOWN_INDEX) reset
                     where basis.price_event_id = reset.price_event_id
                       and basis.item           = reset.item
                       and basis.location       = reset.location
                       and basis.action_date    < reset.reset_date) del
             where NVL(del.clear_start_ind, -999) NOT IN (RPM_CONSTANTS.START_IND,
                                                          RPM_CONSTANTS.IN_PROCESS_IND)
            union all
            select distinct none.item,
                   none.location
              from (select price_event_id,
                           item,
                           zone location,
                           action_date action_date,
                           price_change_id clearance_id,
                           pc_change_type clear_mkdn_index,
                           pc_null_multi_ind clear_start_ind
                      from rpm_zone_future_retail_gtt
                     where rownum > 0) none
             where NOT EXISTS (select 'x'
                                 from (select price_event_id,
                                              item,
                                              zone location,
                                              action_date action_date,
                                              price_change_id clearance_id,
                                              pc_change_type clear_mkdn_index,
                                              pc_null_multi_ind clear_start_ind
                                         from rpm_zone_future_retail_gtt
                                        where rownum > 0) exist
                                where none.price_event_id = exist.price_event_id
                                  and none.item           = exist.item
                                  and none.location       = exist.location
                                  and exist.action_date   > LP_vdate
                                  and exist.clearance_id  is NOT NULL);

   delete
     from rpm_clearance_reset rc1
    where rc1.rowid IN (select rc2.rowid
                          from rpm_clearance_reset rc2,
                               gtt_num_num_str_str_date_date gtt
                          where gtt.varchar2_1 = rc2.item
                            and gtt.number_1   = rc2.location)
      and rc1.state != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
    returning new OBJ_CLEARANCE_ITEM_LOC_REC(clearance_id,
                                             item,
                                             location,
                                             zone_node_type,
                                             effective_date) BULK COLLECT into O_deleted_clearance_id;

   delete
     from rpm_clearance_gtt rc1
    where rc1.reset_ind = 1
      and (rc1.item,
           rc1.location) IN (select il.varchar2_1 item,
                                    il.number_1   location
                               from gtt_num_num_str_str_date_date il)
      and rc1.state != RPM_CONSTANTS.PC_EXECUTED_STATE_CODE;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_CLEARANCE_RESETS;

--------------------------------------------------------------------------------
FUNCTION UPD_RESET_ON_CLR_LOC_MOVE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_FUTURE_RETAIL_GTT_SQL.UPD_RESET_ON_CLR_LOC_MOVE';

BEGIN

   update rpm_clearance_reset rc1
      set effective_date = NULL
    where effective_date > LP_vdate
      and (rc1.item,
           rc1.location) IN (select item,
                                    location
                               from rpm_future_retail_gtt);

   merge into rpm_future_retail_gtt target
   using (select price_event_id,
                 item,
                 location
            from rpm_future_retail_gtt
           where action_date      > LP_vdate
             and clear_start_ind  = RPM_CONSTANTS.END_IND
             and clear_mkdn_index = 0) source
   on (    target.price_event_id = source.price_event_id
       and target.item           = source.item
       and target.location       = source.location)
   when MATCHED then
      update
         set target.clear_start_ind = RPM_CONSTANTS.IN_PROCESS_IND;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;
END UPD_RESET_ON_CLR_LOC_MOVE;

--------------------------------------------------------------------------------

FUNCTION MERGE_PROMOTION(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                         I_promo_recs         IN     OBJ_RPM_CC_PROMO_TBL,
                         I_child_promo_dtl_id IN     RPM_PROMO_DTL.PROMO_DTL_ID%TYPE DEFAULT NULL,
                         I_price_event_type   IN     VARCHAR2,
                         I_nil_ind            IN     NUMBER DEFAULT 0,
                         I_bulk_cc_pe_id      IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_PROMOTION';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   -- When processing through NIL, the system should not allow an item/loc to inherit a
   -- promotion when there is an exception/exclusion to the promotion detail that should
   -- encompass the newly added item/location

   if I_nil_ind = 1 then

      delete from rpm_promo_tbl_gtt;

      insert into rpm_promo_tbl_gtt (rpm_promo_comp_detail_id,
                                     item,
                                     location,
                                     zone_node_type)
         with pr_exception as
           (select /*+ CARDINALITY(pr 10) */
                   rpd.exception_parent_id,
                   rpd.promo_dtl_id,
                   rpd.sys_generated_exclusion,
                   pr.promo_comp_detail_id
              from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr,
                   rpm_promo_dtl rpd
             where rpd.exception_parent_id = pr.promo_comp_detail_id)
         -- item/zone
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rzl.location,
                rzl.loc_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                item_master im,
                rpm_promo_zone_location rpzl,
                rpm_zone_location rzl,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
            and rpdmn.item           = im.item
            and im.item_level        = im.tran_level
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rpzl.zone_id         = rzl.zone_id
            and rzl.loc_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- parent/loc
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpdmn.item           = im.item_parent
            and im.item_level        = im.tran_level
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rpzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- parent/zone
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rzl.location,
                rzl.loc_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                rpm_zone_location rzl,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
            and rpdmn.item           = im.item_parent
            and im.item_level        = im.tran_level
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rpzl.zone_id         = rzl.zone_id
            and rzl.loc_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- parent-diff/loc
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpdmn.item           = im.item_parent
            and im.item_level        = im.tran_level
            and rpdmn.diff_id        IN (im.diff_1,
                                         im.diff_2,
                                         im.diff_3,
                                         im.diff_4)
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rpzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- parent-diff/zone
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rzl.location,
                rzl.loc_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                rpm_zone_location rzl,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
            and rpdmn.item           = im.item_parent
            and im.item_level        = im.tran_level
            and rpdmn.diff_id        IN (im.diff_1,
                                         im.diff_2,
                                         im.diff_3,
                                         im.diff_4)
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rpzl.zone_id         = rzl.zone_id
            and rzl.loc_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- merch/loc
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                         RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and im.dept              = rpdmn.dept
            and im.class             = NVL(rpdmn.class, im.class)
            and im.subclass          = NVL(rpdmn.subclass, im.subclass)
            and im.item_level        = im.tran_level
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rpzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- merch/zone - can never be dept/zone
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rzl.location,
                rzl.loc_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                item_master im,
                rpm_promo_zone_location rpzl,
                rpm_zone_location rzl,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id   = rpd.promo_dtl_id
            and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpzl.promo_dtl_id    = rpd.promo_comp_detail_id
            and rpzl.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
            and rpdmn.merch_type     IN (RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                         RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
            and im.dept              = rpdmn.dept
            and im.class             = rpdmn.class
            and im.subclass          = NVL(rpdmn.subclass, im.subclass)
            and im.status            = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind      = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rpzl.zone_id         = rzl.zone_id
            and rzl.loc_type         = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rbcpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpi.price_event_id = rpd.exception_parent_id
            and im.item              = rbcpi.item
            and rbcpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id  = rbcpi.bulk_cc_pe_id
            and rbcpl.location       = rzl.location
            and rbcpi.itemloc_id     = rbcpl.itemloc_id
         union all
         -- skulist/loc - parent items
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                rpm_promo_dtl_skulist rpds,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id                  = rpd.promo_dtl_id
            and NVL(rpd.sys_generated_exclusion, 0) = 0
            and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and rpzl.promo_dtl_id                   = rpd.promo_comp_detail_id
            and rpzl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpdmn.promo_dtl_id                  = rpds.price_event_id
            and rpdmn.skulist                       = rpds.skulist
            and rpds.item_level                     = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
            and rpds.item                           = im.item_parent
            and im.item_level                       = im.tran_level
            and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id                 = I_bulk_cc_pe_id
            and rbcpi.price_event_id                = rpd.exception_parent_id
            and im.item                             = rbcpi.item
            and rbcpl.bulk_cc_pe_id                 = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id                 = rbcpi.bulk_cc_pe_id
            and rbcpl.location                      = rpzl.location
            and rbcpi.itemloc_id                    = rbcpl.itemloc_id
         union all
         -- skulist/loc - tran items
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                rpm_promo_dtl_skulist rpds,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id                  = rpd.promo_dtl_id
            and NVL(rpd.sys_generated_exclusion, 0) = 0
            and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and rpzl.promo_dtl_id                   = rpd.promo_comp_detail_id
            and rpzl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpdmn.promo_dtl_id                  = rpds.price_event_id
            and rpdmn.skulist                       = rpds.skulist
            and rpds.item_level                     = RPM_CONSTANTS.IL_ITEM_LEVEL
            and rpds.item                           = im.item
            and im.item_level                       = im.tran_level
            and im.status                           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind                     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id                 = I_bulk_cc_pe_id
            and rbcpi.price_event_id                = rpd.exception_parent_id
            and im.item                             = rbcpi.item
            and rbcpl.bulk_cc_pe_id                 = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id                 = rbcpi.bulk_cc_pe_id
            and rbcpl.location                      = rpzl.location
            and rbcpi.itemloc_id                    = rbcpl.itemloc_id
         union all
         -- PEIL/loc - parent items
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                rpm_merch_list_detail rmld,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id         = rpd.promo_dtl_id
            and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and rpzl.promo_dtl_id          = rpd.promo_comp_detail_id
            and rpzl.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpdmn.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
            and rmld.item                  = im.item_parent
            and im.item_level              = im.tran_level
            and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and rbcpi.price_event_id       = rpd.exception_parent_id
            and im.item                    = rbcpi.item
            and rbcpl.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id        = rbcpi.bulk_cc_pe_id
            and rbcpl.location             = rpzl.location
            and rbcpi.itemloc_id           = rbcpl.itemloc_id
         union all
         -- PEIL/loc - tran items
         select /*+ ORDERED */
                rpd.exception_parent_id,
                im.item,
                rpzl.location,
                rpzl.zone_node_type
           from pr_exception rpd,
                rpm_promo_dtl_merch_node rpdmn,
                rpm_promo_zone_location rpzl,
                rpm_merch_list_detail rmld,
                item_master im,
                rpm_bulk_cc_pe_item rbcpi,
                rpm_bulk_cc_pe_location rbcpl
          where rpdmn.promo_dtl_id         = rpd.promo_dtl_id
            and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and rpzl.promo_dtl_id          = rpd.promo_comp_detail_id
            and rpzl.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpdmn.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
            and rmld.item                  = im.item
            and im.item_level              = im.tran_level
            and im.status                  = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind            = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
            and rbcpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and rbcpi.price_event_id       = rpd.exception_parent_id
            and im.item                    = rbcpi.item
            and rbcpl.bulk_cc_pe_id        = I_bulk_cc_pe_id
            and rbcpl.bulk_cc_pe_id        = rbcpi.bulk_cc_pe_id
            and rbcpl.location             = rpzl.location
            and rbcpi.itemloc_id           = rbcpl.itemloc_id;

   end if;

   merge into rpm_promo_item_loc_expl_gtt target
   using (select t.price_event_id,
                 t.item,
                 t.dept,
                 t.class,
                 t.subclass,
                 t.location,
                 t.zone_node_type,
                 t.promo_id,
                 t.promo_display_id,
                 t.rp_secondary_ind promo_secondary_ind,
                 t.promo_comp_id,
                 t.comp_display_id,
                 t.promo_comp_detail_id promo_dtl_id,
                 t.promotion_type type,
                 t.rpc_secondary_ind detail_secondary_ind,
                 t.rpcd_start_date detail_start_date,
                 t.rpcd_end_date detail_end_date,
                 t.apply_to_code detail_apply_to_code,
                 t.timebased_dtl_ind,
                 t.change_type detail_change_type,
                 t.change_amount detail_change_amount,
                 t.change_currency detail_change_currency,
                 t.change_percent detail_change_percent,
                 t.change_selling_uom detail_change_selling_uom,
                 t.price_guide_id detail_price_guide_id,
                 t.exception_parent_id exception_parent_id,
                 RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_CRE promo_comp_msg_type,
                 t.customer_type,
                 t.item_parent,
                 t.diff_id,
                 t.zone_id,
                 t.max_hier_level,
                 t.cur_hier_level
            from (select /*+ CARDINALITY(pr 10) */
                         frgtt.price_event_id,
                         frgtt.item,
                         frgtt.dept,
                         frgtt.class,
                         frgtt.subclass,
                         frgtt.location,
                         frgtt.zone_node_type,
                         RANK() OVER (PARTITION BY frgtt.price_event_id,
                                                   frgtt.item,
                                                   NVL(frgtt.diff_id, '-999'),
                                                   frgtt.location,
                                                   frgtt.zone_node_type
                                          ORDER BY action_date desc) rank_value,
                         pr.promo_id,
                         pr.promo_display_id,
                         pr.rp_secondary_ind,
                         pr.promo_comp_id,
                         pr.comp_display_id,
                         pr.promo_comp_detail_id,
                         pr.promotion_type,
                         pr.rpc_secondary_ind,
                         pr.rpcd_start_date,
                         pr.rpcd_end_date,
                         pr.apply_to_code,
                         pr.timebased_dtl_ind,
                         pr.change_type,
                         pr.change_amount,
                         pr.change_currency,
                         pr.change_percent,
                         pr.change_selling_uom,
                         pr.price_guide_id,
                         pr.exception_parent_id,
                         pr.customer_type,
                         frgtt.item_parent,
                         frgtt.diff_id,
                         frgtt.zone_id,
                         frgtt.max_hier_level,
                         frgtt.cur_hier_level,
                         pr_gtt.rpm_promo_comp_detail_id pr_gtt_dtl_id
                    from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr,
                         rpm_future_retail_gtt frgtt,
                         rpm_promo_tbl_gtt pr_gtt
                   where frgtt.price_event_id = NVL(I_child_promo_dtl_id, pr.promo_comp_detail_id)
                     and frgtt.price_event_id = pr_gtt.rpm_promo_comp_detail_id (+)
                     and frgtt.item           = pr_gtt.item (+)
                     and frgtt.zone_node_type = pr_gtt.zone_node_type (+)
                     and frgtt.location       = pr_gtt.location (+))t
             where t.pr_gtt_dtl_id is NULL
               and t.rank_value    = 1) source
   on (    target.price_event_id         = source.price_event_id
       and target.dept                   = source.dept
       and target.item                   = source.item
       and NVL(target.diff_id, '-999')   = NVL(source.diff_id, '-999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.promo_dtl_id           = source.promo_dtl_id
       and NVL(target.deleted_ind, 0)   != 1)
   when MATCHED then
      update
         set target.promo_secondary_ind       = source.promo_secondary_ind,
             target.detail_secondary_ind      = source.detail_secondary_ind,
             target.detail_start_date         = source.detail_start_date,
             target.detail_end_date           = source.detail_end_date,
             target.detail_apply_to_code      = source.detail_apply_to_code,
             target.timebased_dtl_ind         = source.timebased_dtl_ind,
             target.detail_change_type        = source.detail_change_type,
             target.detail_change_amount      = source.detail_change_amount,
             target.detail_change_currency    = source.detail_change_currency,
             target.detail_change_percent     = source.detail_change_percent,
             target.detail_change_selling_uom = source.detail_change_selling_uom,
             target.detail_price_guide_id     = source.detail_price_guide_id,
             target.promo_comp_msg_type       = source.promo_comp_msg_type,
             target.step_identifier           = RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              dept,
              class,
              subclass,
              location,
              zone_node_type,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              timebased_dtl_ind,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              promo_comp_msg_type,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.zone_node_type,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.timebased_dtl_ind,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.promo_comp_msg_type,
              source.item_parent,
              source.diff_id,
              source.zone_id,
              source.max_hier_level,
              source.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO);

   merge /*+ INDEX (rfrg1 RPM_FUTURE_RETAIL_GTT_I1) */
    into rpm_future_retail_gtt rfrg1
   using (select r.price_event_id,
                 r.future_retail_id,
                 r.item,
                 r.dept,
                 r.class,
                 r.subclass,
                 r.zone_node_type,
                 r.location,
                 r.action_date,
                 r.selling_retail,
                 r.selling_retail_currency,
                 r.selling_uom,
                 r.multi_units,
                 r.multi_unit_retail,
                 r.multi_unit_retail_currency,
                 r.multi_selling_uom,
                 r.clear_exception_parent_id,
                 r.clear_retail,
                 r.clear_retail_currency,
                 r.clear_uom,
                 r.simple_promo_retail,
                 r.simple_promo_retail_currency,
                 r.simple_promo_uom,
                 r.on_simple_promo_ind,
                 r.on_complex_promo_ind,
                 r.promo_comp_msg_type,
                 r.price_change_id,
                 r.price_change_display_id,
                 r.pc_exception_parent_id,
                 r.pc_change_type,
                 r.pc_change_amount,
                 r.pc_change_currency,
                 r.pc_change_percent,
                 r.pc_change_selling_uom,
                 r.pc_null_multi_ind,
                 r.pc_multi_units,
                 r.pc_multi_unit_retail,
                 r.pc_multi_unit_retail_currency,
                 r.pc_multi_selling_uom,
                 r.pc_price_guide_id,
                 r.pc_msg_type,
                 r.pc_selling_retail_ind,
                 r.pc_multi_unit_ind,
                 r.clearance_id,
                 r.clearance_display_id,
                 r.clear_mkdn_index,
                 r.clear_start_ind,
                 r.clear_change_type,
                 r.clear_change_amount,
                 r.clear_change_currency,
                 r.clear_change_percent,
                 r.clear_change_selling_uom,
                 r.clear_price_guide_id,
                 r.clear_msg_type,
                 r.loc_move_from_zone_id,
                 r.loc_move_to_zone_id,
                 r.location_move_id,
                 r.lock_version,
                 r.timeline_seq,
                 r.rfr_rowid,
                 r.promo_id,
                 r.promo_display_id,
                 r.rp_secondary_ind,
                 r.promo_comp_id,
                 r.comp_display_id,
                 r.rpc_secondary_ind,
                 r.promo_comp_detail_id,
                 r.rpcd_start_date,
                 r.rpcd_end_date,
                 r.old_rpcd_start_date,
                 r.old_rpcd_end_date,
                 r.apply_to_code,
                 r.change_type,
                 r.change_amount,
                 r.change_currency,
                 r.change_percent,
                 r.change_selling_uom,
                 r.price_guide_id,
                 r.promotion_type,
                 r.exclusion,
                 r.rank_value,
                 r.effective_date,
                 r.item_parent,
                 r.diff_id,
                 r.zone_id,
                 r.max_hier_level,
                 r.cur_hier_level
            from (select /*+ CARDINALITY(prom 10) */
                         rfrg2s.price_event_id,
                         rfrg2s.future_retail_id,
                         rfrg2s.item,
                         rfrg2s.dept,
                         rfrg2s.class,
                         rfrg2s.subclass,
                         rfrg2s.zone_node_type,
                         rfrg2s.location,
                         rfrg2s.action_date,
                         rfrg2s.selling_retail,
                         rfrg2s.selling_retail_currency,
                         rfrg2s.selling_uom,
                         rfrg2s.multi_units,
                         rfrg2s.multi_unit_retail,
                         rfrg2s.multi_unit_retail_currency,
                         rfrg2s.multi_selling_uom,
                         rfrg2s.clear_exception_parent_id,
                         rfrg2s.clear_retail,
                         rfrg2s.clear_retail_currency,
                         rfrg2s.clear_uom,
                         rfrg2s.simple_promo_retail,
                         rfrg2s.simple_promo_retail_currency,
                         rfrg2s.simple_promo_uom,
                         rfrg2s.on_simple_promo_ind,
                         rfrg2s.on_complex_promo_ind,
                         rfrg2s.promo_comp_msg_type,
                         rfrg2s.price_change_id,
                         rfrg2s.price_change_display_id,
                         rfrg2s.pc_exception_parent_id,
                         rfrg2s.pc_change_type,
                         rfrg2s.pc_change_amount,
                         rfrg2s.pc_change_currency,
                         rfrg2s.pc_change_percent,
                         rfrg2s.pc_change_selling_uom,
                         rfrg2s.pc_null_multi_ind,
                         rfrg2s.pc_multi_units,
                         rfrg2s.pc_multi_unit_retail,
                         rfrg2s.pc_multi_unit_retail_currency,
                         rfrg2s.pc_multi_selling_uom,
                         rfrg2s.pc_price_guide_id,
                         rfrg2s.pc_msg_type,
                         rfrg2s.pc_selling_retail_ind,
                         rfrg2s.pc_multi_unit_ind,
                         rfrg2s.clearance_id,
                         rfrg2s.clearance_display_id,
                         rfrg2s.clear_mkdn_index,
                         rfrg2s.clear_start_ind,
                         rfrg2s.clear_change_type,
                         rfrg2s.clear_change_amount,
                         rfrg2s.clear_change_currency,
                         rfrg2s.clear_change_percent,
                         rfrg2s.clear_change_selling_uom,
                         rfrg2s.clear_price_guide_id,
                         rfrg2s.clear_msg_type,
                         rfrg2s.loc_move_from_zone_id,
                         rfrg2s.loc_move_to_zone_id,
                         rfrg2s.location_move_id,
                         rfrg2s.lock_version,
                         rfrg2s.timeline_seq,
                         rfrg2s.rfr_rowid,
                         rfrg2s.item_parent,
                         rfrg2s.diff_id,
                         rfrg2s.zone_id,
                         rfrg2s.max_hier_level,
                         rfrg2s.cur_hier_level,
                         prom.promo_id,
                         prom.promo_display_id,
                         prom.rp_secondary_ind,
                         prom.promo_comp_id,
                         prom.comp_display_id,
                         prom.rpc_secondary_ind,
                         prom.promo_comp_detail_id,
                         prom.rpcd_start_date,
                         prom.rpcd_end_date,
                         prom.old_rpcd_start_date,
                         prom.old_rpcd_end_date,
                         prom.apply_to_code,
                         prom.change_type,
                         prom.change_amount,
                         prom.change_currency,
                         prom.change_percent,
                         prom.change_selling_uom,
                         prom.price_guide_id,
                         prom.promotion_type,
                         prom.exclusion,
                         RANK() OVER (PARTITION BY price_event_id,
                                                   item,
                                                   NVL(diff_id, '-999'),
                                                   location,
                                                   zone_node_type
                                          ORDER BY action_date desc) rank_value,
                         prom.rpcd_start_date effective_date
                    from rpm_future_retail_gtt rfrg2s,
                         (select distinct pr.promo_id,
                                 pr.promo_display_id,
                                 pr.rp_secondary_ind,
                                 pr.promo_comp_id,
                                 pr.comp_display_id,
                                 pr.rpc_secondary_ind,
                                 pr.promo_comp_detail_id,
                                 pr.rpcd_start_date,
                                 pr.rpcd_end_date,
                                 pr.old_rpcd_start_date,
                                 pr.old_rpcd_end_date,
                                 pr.apply_to_code,
                                 pr.change_type,
                                 pr.change_amount,
                                 pr.change_currency,
                                 pr.change_percent,
                                 pr.change_selling_uom,
                                 pr.price_guide_id,
                                 pr.promotion_type,
                                 pr.exclusion
                            from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr) prom
                   where rfrg2s.price_event_id = NVL(I_child_promo_dtl_id, prom.promo_comp_detail_id)
                     and rfrg2s.action_date   <= prom.rpcd_start_date
                  union all
                  select /*+ CARDINALITY(prom 10) */
                         rfrg2e.price_event_id,
                         rfrg2e.future_retail_id,
                         rfrg2e.item,
                         rfrg2e.dept,
                         rfrg2e.class,
                         rfrg2e.subclass,
                         rfrg2e.zone_node_type,
                         rfrg2e.location,
                         rfrg2e.action_date,
                         rfrg2e.selling_retail,
                         rfrg2e.selling_retail_currency,
                         rfrg2e.selling_uom,
                         rfrg2e.multi_units,
                         rfrg2e.multi_unit_retail,
                         rfrg2e.multi_unit_retail_currency,
                         rfrg2e.multi_selling_uom,
                         rfrg2e.clear_exception_parent_id,
                         rfrg2e.clear_retail,
                         rfrg2e.clear_retail_currency,
                         rfrg2e.clear_uom,
                         rfrg2e.simple_promo_retail,
                         rfrg2e.simple_promo_retail_currency,
                         rfrg2e.simple_promo_uom,
                         rfrg2e.on_simple_promo_ind,
                         rfrg2e.on_complex_promo_ind,
                         rfrg2e.promo_comp_msg_type,
                         rfrg2e.price_change_id,
                         rfrg2e.price_change_display_id,
                         rfrg2e.pc_exception_parent_id,
                         rfrg2e.pc_change_type,
                         rfrg2e.pc_change_amount,
                         rfrg2e.pc_change_currency,
                         rfrg2e.pc_change_percent,
                         rfrg2e.pc_change_selling_uom,
                         rfrg2e.pc_null_multi_ind,
                         rfrg2e.pc_multi_units,
                         rfrg2e.pc_multi_unit_retail,
                         rfrg2e.pc_multi_unit_retail_currency,
                         rfrg2e.pc_multi_selling_uom,
                         rfrg2e.pc_price_guide_id,
                         rfrg2e.pc_msg_type,
                         rfrg2e.pc_selling_retail_ind,
                         rfrg2e.pc_multi_unit_ind,
                         rfrg2e.clearance_id,
                         rfrg2e.clearance_display_id,
                         rfrg2e.clear_mkdn_index,
                         rfrg2e.clear_start_ind,
                         rfrg2e.clear_change_type,
                         rfrg2e.clear_change_amount,
                         rfrg2e.clear_change_currency,
                         rfrg2e.clear_change_percent,
                         rfrg2e.clear_change_selling_uom,
                         rfrg2e.clear_price_guide_id,
                         rfrg2e.clear_msg_type,
                         rfrg2e.loc_move_from_zone_id,
                         rfrg2e.loc_move_to_zone_id,
                         rfrg2e.location_move_id,
                         rfrg2e.lock_version,
                         rfrg2e.timeline_seq,
                         rfrg2e.rfr_rowid,
                         rfrg2e.item_parent,
                         rfrg2e.diff_id,
                         rfrg2e.zone_id,
                         rfrg2e.max_hier_level,
                         rfrg2e.cur_hier_level,
                         prom.promo_id,
                         prom.promo_display_id,
                         prom.rp_secondary_ind,
                         prom.promo_comp_id,
                         prom.comp_display_id,
                         prom.rpc_secondary_ind,
                         prom.promo_comp_detail_id,
                         prom.rpcd_start_date,
                         prom.rpcd_end_date,
                         prom.old_rpcd_start_date,
                         prom.old_rpcd_end_date,
                         prom.apply_to_code,
                         prom.change_type,
                         prom.change_amount,
                         prom.change_currency,
                         prom.change_percent,
                         prom.change_selling_uom,
                         prom.price_guide_id,
                         prom.promotion_type,
                         prom.exclusion,
                         RANK() OVER (PARTITION BY price_event_id,
                                                   item,
                                                   NVL(diff_id, '-999'),
                                                   location,
                                                   zone_node_type
                                          ORDER BY action_date desc) rank_value,
                         (prom.rpcd_end_date + RPM_CONSTANTS.ONE_MINUTE) effective_date
                    from rpm_future_retail_gtt rfrg2e,
                         (select distinct pr.promo_id,
                                 pr.promo_display_id,
                                 pr.rp_secondary_ind,
                                 pr.promo_comp_id,
                                 pr.comp_display_id,
                                 pr.rpc_secondary_ind,
                                 pr.promo_comp_detail_id,
                                 pr.rpcd_start_date,
                                 pr.rpcd_end_date,
                                 pr.old_rpcd_start_date,
                                 pr.old_rpcd_end_date,
                                 pr.apply_to_code,
                                 pr.change_type,
                                 pr.change_amount,
                                 pr.change_currency,
                                 pr.change_percent,
                                 pr.change_selling_uom,
                                 pr.price_guide_id,
                                 pr.promotion_type,
                                 pr.exclusion
                            from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr) prom
                    where rfrg2e.price_event_id = NVL(I_child_promo_dtl_id, prom.promo_comp_detail_id)
                      and rfrg2e.action_date   <= prom.rpcd_end_date + RPM_CONSTANTS.ONE_MINUTE) r
           where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = rfrg3.effective_date)
   when MATCHED then
      update
         set rfrg1.on_simple_promo_ind  = rfrg3.on_simple_promo_ind,
             rfrg1.on_complex_promo_ind = rfrg3.on_complex_promo_ind,
             rfrg1.step_identifier      = RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              zone_node_type,
              location,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              on_simple_promo_ind,
              on_complex_promo_ind,
              item_parent,
              diff_id,
              zone_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (rfrg3.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              rfrg3.dept,
              rfrg3.class,
              rfrg3.subclass,
              rfrg3.item,
              rfrg3.zone_node_type,
              rfrg3.location,
              rfrg3.effective_date,
              rfrg3.selling_retail,
              rfrg3.selling_retail_currency,
              rfrg3.selling_uom,
              rfrg3.multi_units,
              rfrg3.multi_unit_retail,
              rfrg3.multi_unit_retail_currency,
              rfrg3.multi_selling_uom,
              rfrg3.clear_retail,
              rfrg3.clear_retail_currency,
              rfrg3.clear_uom,
              rfrg3.simple_promo_retail,
              rfrg3.simple_promo_retail_currency,
              rfrg3.simple_promo_uom,
              0,
              0,
              rfrg3.item_parent,
              rfrg3.diff_id,
              rfrg3.zone_id,
              rfrg3.max_hier_level,
              rfrg3.cur_hier_level,
              RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO);

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select price_event_id,
                    item,
                    zone_node_type,
                    location,
                    action_date,
                    customer_type,
                    dept,
                    item_parent,
                    diff_id,
                    zone_id,
                    max_hier_level,
                    cur_hier_level
               from (select /*+ CARDINALITY(pr 10) */
                            frg.price_event_id,
                            frg.item,
                            frg.zone_node_type,
                            frg.location,
                            frg.action_date,
                            pr.customer_type,
                            frg.dept,
                            frg.item_parent,
                            frg.diff_id,
                            frg.zone_id,
                            frg.max_hier_level,
                            frg.cur_hier_level
                       from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr,
                            rpm_future_retail_gtt frg
                      where frg.price_event_id = NVL(I_child_promo_dtl_id, pr.promo_comp_detail_id)
                        and frg.action_date   >= rpcd_start_date
                        and frg.action_date   <= NVL(rpcd_end_date + RPM_CONSTANTS.ONE_MINUTE, TO_DATE('3000', 'YYYY')))) source
      on (    target.price_event_id       = source.price_event_id
          and target.dept                 = source.dept
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.customer_type        = source.customer_type
          and target.action_date          = source.action_date)
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 zone_node_type,
                 location,
                 action_date,
                 customer_type,
                 dept,
                 complex_promo_ind,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.zone_node_type,
                 source.location,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 0,      -- complex_promo_ind
                 source.item_parent,
                 source.diff_id,
                 source.zone_id,
                 source.max_hier_level,
                 source.cur_hier_level,
                 RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO);

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_MERGE_PROMO,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_PROMOTION;

--------------------------------------------------------------------------------

FUNCTION MERGE_NEW_PROMO_END_DATE(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_promo_recs         IN     OBJ_RPM_CC_PROMO_TBL,
                                  I_price_event_type   IN     VARCHAR2,
                                  I_new_promo_end_date IN     DATE   DEFAULT NULL)
RETURN NUMBER IS

   L_program  VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.MERGE_NEW_PROMO_END_DATE';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   if I_promo_recs is NOT NULL and
      I_promo_recs.COUNT > 0 then

      delete
        from rpm_id_date_gtt;

      insert into rpm_id_date_gtt
         (price_event_id,
          rf_date,
          timebased_dtl_ind)
         select distinct ccet.promo_comp_detail_id,
                ccet.rpcd_end_date,
                ccet.timebased_dtl_ind
           from table(cast(I_promo_recs AS OBJ_RPM_CC_PROMO_tbl)) ccet;

      merge into rpm_promo_item_loc_expl_gtt target
      using(select ilex.rowid row_id,
                   id.price_event_id,
                   TO_DATE(CONCAT(TO_CHAR(id.rf_date, 'YYYYMMDD'), '23:59'), 'YYYYMMDDHH24:MI') rf_date,
                   id.timebased_dtl_ind
              from rpm_id_date_gtt id,
                   rpm_promo_item_loc_expl_gtt ilex
             where ilex.promo_dtl_id = id.price_event_id) source
      on (target.rowid = source.row_id)
      when MATCHED then
         update
            set target.detail_end_date     = source.rf_date,
                target.timebased_dtl_ind   = source.timebased_dtl_ind,
                target.promo_comp_msg_type = RPM_CONSTANTS.RIB_MSG_TYPE_PROM_PC_MOD,
                target.step_identifier     = RPM_CONSTANTS.CAPT_GTT_MRG_NEW_PROMO_END_DT;

   end if;

   merge /*+ index (rfrg1 rpm_future_retail_gtt_i1) */ into rpm_future_retail_gtt rfrg1
   using (select price_event_id,
                 future_retail_id,
                 item,
                 dept,
                 class,
                 subclass,
                 zone_node_type,
                 location,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_exception_parent_id,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 NULL price_change_id,
                 NULL price_change_display_id,
                 NULL pc_exception_parent_id,
                 NULL pc_change_type,
                 NULL pc_change_amount,
                 NULL pc_change_currency,
                 NULL pc_change_percent,
                 NULL pc_change_selling_uom,
                 NULL pc_null_multi_ind,
                 NULL pc_multi_units,
                 NULL pc_multi_unit_retail,
                 NULL pc_multi_unit_retail_currency,
                 NULL pc_multi_selling_uom,
                 NULL pc_price_guide_id,
                 NULL pc_msg_type,
                 NULL pc_selling_retail_ind,
                 NULL pc_multi_unit_ind,
                 NULL clearance_id,
                 NULL clearance_display_id,
                 NULL clear_mkdn_index,
                 NULL clear_start_ind,
                 NULL clear_change_type,
                 NULL clear_change_amount,
                 NULL clear_change_currency,
                 NULL clear_change_percent,
                 NULL clear_change_selling_uom,
                 NULL clear_price_guide_id,
                 NULL clear_msg_type,
                 rank_value,
                 effective_date,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level
            from (select rfrg.price_event_id,
                         rfrg.future_retail_id,
                         rfrg.item,
                         rfrg.dept,
                         rfrg.class,
                         rfrg.subclass,
                         rfrg.zone_node_type,
                         rfrg.location,
                         rfrg.action_date,
                         rfrg.selling_retail,
                         rfrg.selling_retail_currency,
                         rfrg.selling_uom,
                         rfrg.multi_units,
                         rfrg.multi_unit_retail,
                         rfrg.multi_unit_retail_currency,
                         rfrg.multi_selling_uom,
                         rfrg.clear_exception_parent_id,
                         rfrg.clear_retail,
                         rfrg.clear_retail_currency,
                         rfrg.clear_uom,
                         rfrg.simple_promo_retail,
                         rfrg.simple_promo_retail_currency,
                         rfrg.simple_promo_uom,
                         rfrg.price_change_id,
                         rfrg.price_change_display_id,
                         rfrg.pc_exception_parent_id,
                         rfrg.pc_change_type,
                         rfrg.pc_change_amount,
                         rfrg.pc_change_currency,
                         rfrg.pc_change_percent,
                         rfrg.pc_change_selling_uom,
                         rfrg.pc_null_multi_ind,
                         rfrg.pc_multi_units,
                         rfrg.pc_multi_unit_retail,
                         rfrg.pc_multi_unit_retail_currency,
                         rfrg.pc_multi_selling_uom,
                         rfrg.pc_price_guide_id,
                         rfrg.pc_msg_type,
                         rfrg.pc_selling_retail_ind,
                         rfrg.pc_multi_unit_ind,
                         rfrg.clearance_id,
                         rfrg.clearance_display_id,
                         rfrg.clear_mkdn_index,
                         rfrg.clear_start_ind,
                         rfrg.clear_change_type,
                         rfrg.clear_change_amount,
                         rfrg.clear_change_currency,
                         rfrg.clear_change_percent,
                         rfrg.clear_change_selling_uom,
                         rfrg.clear_price_guide_id,
                         rfrg.clear_msg_type,
                         RANK() OVER (PARTITION BY rfrg.price_event_id,
                                                   rfrg.item,
                                                   NVL(rfrg.diff_id, '-999'),
                                                   rfrg.location,
                                                   rfrg.zone_node_type
                                          ORDER BY rfrg.action_date desc,
                                                   rpile.promo_dtl_id desc) as rank_value,
                        (rpile.detail_end_date + RPM_CONSTANTS.ONE_MINUTE) effective_date,
                         rfrg.item_parent,
                         rfrg.diff_id,
                         rfrg.zone_id,
                         rfrg.max_hier_level,
                         rfrg.cur_hier_level
                    from rpm_promo_item_loc_expl_gtt rpile,
                         rpm_future_retail_gtt rfrg,
                         (select distinct promo_comp_detail_id,
                                          rpcd_end_date
                            from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) pr) prom
                   where rpile.price_event_id      = prom.promo_comp_detail_id
                     and rpile.price_event_id      = rfrg.price_event_id
                     and rfrg.dept                 = rpile.dept
                     and rfrg.item                 = rpile.item
                     and NVL(rfrg.diff_id, '-999') = NVL(rpile.diff_id, '-999')
                     and rfrg.location             = rpile.location
                     and rfrg.zone_node_type       = rpile.zone_node_type
                     and rfrg.action_date          between rpile.detail_start_date and NVL(NVL(I_new_promo_end_date, rpile.detail_end_date), rfrg.action_date)) r
      where rank_value = 1) rfrg3
   on (    rfrg1.price_event_id       = rfrg3.price_event_id
       and rfrg1.item                 = rfrg3.item
       and NVL(rfrg1.diff_id, '-999') = NVL(rfrg3.diff_id, '-999')
       and rfrg1.location             = rfrg3.location
       and rfrg1.zone_node_type       = rfrg3.zone_node_type
       and rfrg1.action_date          = NVL(rfrg3.effective_date, rfrg1.action_date))
  when MATCHED then
     update
        set future_retail_id = rfrg1.future_retail_id,
            step_identifier  = RPM_CONSTANTS.CAPT_GTT_MRG_NEW_PROMO_END_DT
  when NOT MATCHED then
     insert (price_event_id,
             future_retail_id,
             item,
             dept,
             class,
             subclass,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_exception_parent_id,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             on_simple_promo_ind,
             on_complex_promo_ind,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             pc_msg_type,
             pc_selling_retail_ind,
             pc_multi_unit_ind,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             clear_msg_type,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level,
             step_identifier)
     values (rfrg3.price_event_id,
             RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
             rfrg3.item,
             rfrg3.dept,
             rfrg3.class,
             rfrg3.subclass,
             rfrg3.zone_node_type,
             rfrg3.location,
             rfrg3.effective_date,
             rfrg3.selling_retail,
             rfrg3.selling_retail_currency,
             rfrg3.selling_uom,
             rfrg3.multi_units,
             rfrg3.multi_unit_retail,
             rfrg3.multi_unit_retail_currency,
             rfrg3.multi_selling_uom,
             rfrg3.clear_exception_parent_id,
             rfrg3.clear_retail,
             rfrg3.clear_retail_currency,
             rfrg3.clear_uom,
             rfrg3.simple_promo_retail,
             rfrg3.simple_promo_retail_currency,
             rfrg3.simple_promo_uom,
             0,
             0,
             rfrg3.price_change_id,
             rfrg3.price_change_display_id,
             rfrg3.pc_exception_parent_id,
             rfrg3.pc_change_type,
             rfrg3.pc_change_amount,
             rfrg3.pc_change_currency,
             rfrg3.pc_change_percent,
             rfrg3.pc_change_selling_uom,
             rfrg3.pc_null_multi_ind,
             rfrg3.pc_multi_units,
             rfrg3.pc_multi_unit_retail,
             rfrg3.pc_multi_unit_retail_currency,
             rfrg3.pc_multi_selling_uom,
             rfrg3.pc_price_guide_id,
             rfrg3.pc_msg_type,
             rfrg3.pc_selling_retail_ind,
             rfrg3.pc_multi_unit_ind,
             rfrg3.clearance_id,
             rfrg3.clearance_display_id,
             rfrg3.clear_mkdn_index,
             rfrg3.clear_start_ind,
             rfrg3.clear_change_type,
             rfrg3.clear_change_amount,
             rfrg3.clear_change_currency,
             rfrg3.clear_change_percent,
             rfrg3.clear_change_selling_uom,
             rfrg3.clear_price_guide_id,
             rfrg3.clear_msg_type,
             rfrg3.item_parent,
             rfrg3.diff_id,
             rfrg3.zone_id,
             rfrg3.max_hier_level,
             rfrg3.cur_hier_level,
             RPM_CONSTANTS.CAPT_GTT_MRG_NEW_PROMO_END_DT);

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD) then

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select price_event_id,
                    item,
                    zone_node_type,
                    location,
                    action_date,
                    customer_type,
                    dept,
                    item_parent,
                    diff_id,
                    zone_id,
                    max_hier_level,
                    cur_hier_level
               from (select cspf.price_event_id,
                            cspf.item,
                            cspf.zone_node_type,
                            cspf.location,
                            (pr.rpcd_end_date + RPM_CONSTANTS.ONE_MINUTE) action_date,
                            pr.customer_type,
                            cspf.dept,
                            RANK() OVER (PARTITION BY cspf.dept,
                                                      cspf.item,
                                                      NVL(cspf.diff_id, '-999'),
                                                      cspf.location,
                                                      cspf.zone_node_type,
                                                      cspf.customer_type
                                             ORDER BY cspf.action_date desc) rank,
                            cspf.item_parent,
                            cspf.diff_id,
                            cspf.zone_id,
                            cspf.max_hier_level,
                            cspf.cur_hier_level
                       from rpm_promo_item_loc_expl_gtt pile,
                            rpm_cust_segment_promo_fr_gtt cspf,
                            (select distinct promo_comp_detail_id,
                                             rpcd_end_date,
                                             customer_type
                               from table(cast(I_promo_recs as OBJ_RPM_CC_PROMO_TBL)) prom) pr
                      where cspf.price_event_id       = pr.promo_comp_detail_id
                        and cspf.price_event_id       = pile.price_event_id
                        and cspf.location             = pile.location
                        and cspf.zone_node_type       = pile.zone_node_type
                        and cspf.item                 = pile.item
                        and NVL(cspf.diff_id, '-999') = NVL(pile.diff_id, '-999')
                        and cspf.dept                 = pile.dept
                        and cspf.customer_type        = pile.customer_type
                        and cspf.customer_type        = pr.customer_type
                        and cspf.action_date          between pile.detail_start_date and NVL(pile.detail_end_date, TO_DATE('3000', 'YYYY')))
              where rank = 1) source
      on (    target.price_event_id       = source.price_event_id
          and target.dept                 = source.dept
          and target.location             = source.location
          and target.zone_node_type       = source.zone_node_type
          and target.item                 = source.item
          and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
          and target.customer_type        = source.customer_type
          and target.action_date          = NVL(source.action_date, target.action_date))
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 zone_node_type,
                 location,
                 action_date,
                 customer_type,
                 dept,
                 complex_promo_ind,
                 item_parent,
                 diff_id,
                 zone_id,
                 max_hier_level,
                 cur_hier_level,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.zone_node_type,
                 source.location,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 0,      -- complex_promo_ind
                 source.item_parent,
                 source.diff_id,
                 source.zone_id,
                 source.max_hier_level,
                 source.cur_hier_level,
                 RPM_CONSTANTS.CAPT_GTT_MRG_NEW_PROMO_END_DT);

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_MRG_NEW_PROMO_END_DT,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_NEW_PROMO_END_DATE;
--------------------------------------------------------------------------------

FUNCTION GET_AFFECTED_PARENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             O_parents             OUT OBJ_NUM_NUM_DATE_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_GTT_SQL.GET_PARENTS';

   L_parents OBJ_NUM_NUM_DATE_TBL := NULL;

   cursor C_PRICE_CHANGE_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpc1.price_change_id,
                                      rpc2.price_change_id,
                                      rpc2.effective_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc1,
             rpm_price_change rpc2
       where rpc1.price_change_id = VALUE(ids)
         and rpc2.state           IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                      RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
         and rpc2.price_change_id = rpc1.exception_parent_id
      union all
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpc1.price_change_id,
                                      rpc3.price_change_id,
                                      rpc3.effective_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc1,
             rpm_price_change rpc2,
             rpm_price_change rpc3
       where rpc1.price_change_id = VALUE(ids)
         and rpc3.state           IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                      RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
         and rpc3.price_change_id = rpc2.exception_parent_id
         and rpc2.state           NOT IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                          RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
         and rpc2.price_change_id = rpc1.exception_parent_id;

   cursor C_CLEARANCE_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rc1.clearance_id,
                                      rc2.clearance_id,
                                      rc2.effective_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc1,
             rpm_clearance rc2
       where rc1.clearance_id = VALUE(ids)
         and rc2.state        IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                  RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
         and rc2.clearance_id = rc1.exception_parent_id
      union all
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rc1.clearance_id,
                                      rc3.clearance_id,
                                      rc3.effective_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc1,
             rpm_clearance rc2,
             rpm_clearance rc3
       where rc1.clearance_id = VALUE(ids)
         and rc3.state        IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                  RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
         and rc3.clearance_id = rc2.exception_parent_id
         and rc2.state        NOT IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                      RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
         and rc2.clearance_id = rc1.exception_parent_id;

   cursor C_SIMPLE_PROMO_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                      rpd2.promo_dtl_id,
                                      rpd2.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.promo_dtl_id           = VALUE(ids)
         and rpd2.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd2.promo_dtl_id           = rpd1.exception_parent_id
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE;

   cursor C_THRESHOLD_PROMO_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                      rpd2.promo_dtl_id,
                                      rpd2.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.promo_dtl_id           = VALUE(ids)
         and rpd2.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd2.promo_dtl_id           = rpd1.exception_parent_id
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
      union all
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                      rpd2.promo_dtl_id,
                                      rpd2.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl rpd3,
             rpm_promo_comp rpc,
             rpm_promo_dtl_list_grp rpdlg1,
             rpm_promo_dtl_list rpdl1,
             rpm_promo_dtl_disc_ladder rpddl1,
             rpm_promo_dtl_list_grp rpdlg2,
             rpm_promo_dtl_list rpdl2,
             rpm_promo_dtl_disc_ladder rpddl2
       where rpd1.promo_dtl_id            = VALUE(ids)
         and rpd3.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                              RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd3.promo_dtl_id            = rpd2.exception_parent_id
         and rpd2.promo_dtl_id            = rpdlg2.promo_dtl_id
         and rpdlg2.promo_dtl_list_grp_id = rpdl2.promo_dtl_list_grp_id
         and rpdl2.promo_dtl_list_id      = rpddl2.promo_dtl_list_id
         and rpd2.promo_dtl_id            = rpd1.exception_parent_id
         and rpd2.state                  != RPM_CONSTANTS.PR_APPROVED_STATE_CODE
         and rpd1.promo_dtl_id            = rpdlg1.promo_dtl_id
         and rpdlg1.promo_dtl_list_grp_id = rpdl1.promo_dtl_list_grp_id
         and rpdl1.promo_dtl_list_id      = rpddl1.promo_dtl_list_id
         and rpd1.promo_comp_id           = rpc.promo_comp_id
         and rpc.type                     = RPM_CONSTANTS.THRESHOLD_CODE;

   cursor C_COMPLEX_PROMO_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                      rpd2.promo_dtl_id,
                                      rpd2.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.promo_dtl_id           = VALUE(ids)
         and rpd2.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd2.promo_dtl_id           = rpd1.exception_parent_id
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
         and rpddl.change_type           is NOT NULL
      UNION ALL
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                      rpd2.promo_dtl_id,
                                      rpd2.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl rpd3,
             rpm_promo_comp rpc,
             rpm_promo_dtl_list_grp rpdlg1,
             rpm_promo_dtl_list rpdl1,
             rpm_promo_dtl_disc_ladder rpddl1,
             rpm_promo_dtl_list_grp rpdlg2,
             rpm_promo_dtl_list rpdl2,
             rpm_promo_dtl_disc_ladder rpddl2
       where rpd1.promo_dtl_id            = VALUE(ids)
         and rpd3.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                              RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd3.promo_dtl_id            = rpd2.exception_parent_id
         and rpd2.promo_dtl_id            = rpdlg2.promo_dtl_id
         and rpdlg2.promo_dtl_list_grp_id = rpdl2.promo_dtl_list_grp_id
         and rpdl2.promo_dtl_list_id      = rpddl2.promo_dtl_list_id
         and rpd2.promo_dtl_id            = rpd1.exception_parent_id
         and rpd2.state                  != RPM_CONSTANTS.PR_APPROVED_STATE_CODE
         and rpd1.promo_dtl_id            = rpdlg1.promo_dtl_id
         and rpdlg1.promo_dtl_list_grp_id = rpdl1.promo_dtl_list_grp_id
         and rpdl1.promo_dtl_list_id      = rpddl1.promo_dtl_list_id
         and rpd1.promo_comp_id           = rpc.promo_comp_id
         and rpc.type                     = RPM_CONSTANTS.COMPLEX_CODE
         and rpddl1.change_type           is NOT NULL;

   cursor C_TXN_PROMO_PARENTS is
      select /*+ CARDINALITY(ids 10) */
             NEW OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                      rpd2.promo_dtl_id,
                                      rpd2.start_date)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.promo_dtl_id           = VALUE(ids)
         and rpd1.man_txn_exclusion     != 1
         and rpd2.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
         and rpd2.promo_dtl_id           = rpd1.exception_parent_id
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
         and rpddl.change_type           is NOT NULL;

   cursor C_NOPARENTS is
      select NEW OBJ_NUM_NUM_DATE_REC(price_event_id,
                                      NULL,
                                      NULL)
        from (select VALUE(ids) price_event_id
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
              minus
              select parent.numeric_col1 price_event_id
                from table(cast(L_parents as OBJ_NUM_NUM_DATE_TBL)) parent);

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_PRICE_CHANGE_PARENTS;
      fetch C_PRICE_CHANGE_PARENTS BULK COLLECT into L_parents;
      close C_PRICE_CHANGE_PARENTS;

      open C_NOPARENTS;
      fetch C_NOPARENTS BULK COLLECT into O_parents;
      close C_NOPARENTS;

      if L_parents is NOT NULL and
         L_parents.COUNT > 0 then
         ---
         for i IN 1..L_parents.COUNT loop
            O_parents.EXTEND;
            O_parents(O_parents.COUNT) := L_parents(i);
         end loop;
      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      open C_CLEARANCE_PARENTS;
      fetch C_CLEARANCE_PARENTS BULK COLLECT into L_parents;
      close C_CLEARANCE_PARENTS;

      open C_NOPARENTS;
      fetch C_NOPARENTS BULK COLLECT into O_parents;
      close C_NOPARENTS;

      if L_parents is NOT NULL and
         L_parents.COUNT > 0 then
         ---
         for i IN 1..L_parents.COUNT loop
            O_parents.EXTEND;
            O_parents(O_parents.COUNT) := L_parents(i);
         end loop;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then

      open C_SIMPLE_PROMO_PARENTS;
      fetch C_SIMPLE_PROMO_PARENTS BULK COLLECT into L_parents;
      close C_SIMPLE_PROMO_PARENTS;

      open C_NOPARENTS;
      fetch C_NOPARENTS BULK COLLECT into O_parents;
      close C_NOPARENTS;

      if L_parents is NOT NULL and
         L_parents.COUNT > 0 then
         ---
         for i IN 1..L_parents.COUNT loop
            O_parents.EXTEND;
            O_parents(O_parents.COUNT) := L_parents(i);
         end loop;
      end if;

   elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD) then

      open C_THRESHOLD_PROMO_PARENTS;
      fetch C_THRESHOLD_PROMO_PARENTS BULK COLLECT into L_parents;
      close C_THRESHOLD_PROMO_PARENTS;

      open C_NOPARENTS;
      fetch C_NOPARENTS BULK COLLECT into O_parents;
      close C_NOPARENTS;

      if L_parents is NOT NULL and
         L_parents.COUNT > 0 then
         ---
         for i IN 1..L_parents.COUNT loop
            O_parents.EXTEND;
            O_parents(O_parents.COUNT) := L_parents(i);
         end loop;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD) then

      open C_COMPLEX_PROMO_PARENTS;
      fetch C_COMPLEX_PROMO_PARENTS BULK COLLECT into L_parents;
      close C_COMPLEX_PROMO_PARENTS;

      open C_NOPARENTS;
      fetch C_NOPARENTS BULK COLLECT into O_parents;
      close C_NOPARENTS;

      if L_parents is NOT NULL and
         L_parents.COUNT > 0 then
         ---
         for i IN 1..L_parents.COUNT loop
            O_parents.EXTEND;
            O_parents(O_parents.COUNT) := L_parents(i);
         end loop;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      open C_TXN_PROMO_PARENTS;
      fetch C_TXN_PROMO_PARENTS BULK COLLECT into L_parents;
      close C_TXN_PROMO_PARENTS;

      open C_NOPARENTS;
      fetch C_NOPARENTS BULK COLLECT into O_parents;
      close C_NOPARENTS;

      if L_parents is NOT NULL and
         L_parents.COUNT > 0 then
         ---
         for i IN 1..L_parents.COUNT loop
            O_parents.EXTEND;
            O_parents(O_parents.COUNT) := L_parents(i);
         end loop;
      end if;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_AFFECTED_PARENT;

--------------------------------------------------------------------------------
FUNCTION GET_CHILDREN(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      O_child_event_ids     OUT OBJ_NUM_NUM_DATE_TBL,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_GTT_SQL.GET_CHILDREN';

   cursor C_PRICE_CHANGE_EXCEPTIONS is
      select /*+ CARDINALITY(ids 10) ORDERED */
             OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                  rpc.price_change_id,
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc
       where rpc.exception_parent_id = VALUE(ids)
         and rpc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                         RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                         RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                                         RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
      union all
      select /*+ CARDINALITY(ids 10) ORDERED USE_NL(rpc1, rpc2) */
             OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                  rpc2.price_change_id,
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_price_change rpc1,
             rpm_price_change rpc2
       where rpc1.exception_parent_id = VALUE(ids)
         and rpc2.exception_parent_id = rpc1.price_change_id
         and rpc2.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                          RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                          RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                                          RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE);

   cursor C_CLEARANCE_EXCEPTIONS is
      select /*+ CARDINALITY(ids 10) ORDERED */
             OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                  rc.clearance_id,
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc
       where rc.exception_parent_id = VALUE(ids)
         and rc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                        RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                        RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                                        RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE)
      union all
      select /*+ CARDINALITY(ids 10) ORDERED USE_NL(rc1, rc2) */
             OBJ_NUM_NUM_DATE_REC(VALUE(ids),
                                  rc2.clearance_id,
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc1,
             rpm_clearance rc2
       where rc1.exception_parent_id = VALUE(ids)
         and rc2.exception_parent_id = rc1.clearance_id
         and rc2.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                         RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                                         RPM_CONSTANTS.PC_DELETED_STATE_CODE,
                                         RPM_CONSTANTS.PC_CONFLICT_CHECK_STATE_CODE);

   cursor C_SIMPLE_PROMO_EXCEPTIONS is
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd.exception_parent_id     = ids.numeric_id
         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE))
         and rpd.promo_comp_id           = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
      union all
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd2.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.exception_parent_id    = ids.numeric_id
         and rpd1.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd1.state              IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE))
         and rpd2.exception_parent_id    = rpd1.promo_dtl_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE;

   cursor C_THRESHOLD_PROMO_EXCEPTIONS is
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd.exception_parent_id     = ids.numeric_id
         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd.state               IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                             RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE))
         and rpd.promo_comp_id           = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
      union all
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd2.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.exception_parent_id    = ids.numeric_id
         and rpd1.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd1.state              IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                             RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE))
         and rpd2.exception_parent_id    = rpd1.promo_dtl_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE;

   cursor C_COMPLEX_PROMO_EXCEPTIONS is
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd.exception_parent_id     = ids.numeric_id
         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd.state               IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                             RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE))
         and rpd.promo_comp_id           = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
         and rpddl.change_type           is NOT NULL
      union all
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd2.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd1,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd1.exception_parent_id    = ids.numeric_id
         and rpd1.state                  IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd1.promo_dtl_id           = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd1.state              IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                             RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE))
         and rpd2.exception_parent_id    = rpd1.promo_dtl_id
         and rpd1.promo_comp_id          = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
         and rpddl.change_type           is NOT NULL;

   cursor C_TXN_PROMO_EXCEPTIONS is
      select /* ORDERED */
             OBJ_NUM_NUM_DATE_REC(ids.numeric_id,
                                  rpd.promo_dtl_id,
                                  NULL)
        from numeric_id_gtt ids,
             rpm_promo_dtl rpd,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_promo_comp rpc
       where rpd.exception_parent_id     = ids.numeric_id
         and rpd.state                   IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
         and rpd.man_txn_exclusion      != 1
         and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
         and rpddl.promo_dtl_list_id     = rpdl.promo_dtl_list_id
         and (   rpddl.change_type      != RPM_CONSTANTS.RETAIL_EXCLUDE
              or rpd.state               IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                             RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                             RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                                             RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                             RPM_CONSTANTS.PR_CONFLICT_CHECKING_CODE))
         and rpd.promo_comp_id           = rpc.promo_comp_id
         and rpc.type                    = RPM_CONSTANTS.TRANSACTION_CODE
         and rpddl.change_type           is NOT NULL;

BEGIN

   delete
     from numeric_id_gtt;

   forall i IN 1 .. I_price_event_ids.COUNT
      insert into numeric_id_gtt (numeric_id)
         values (I_price_event_ids(i));

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      open C_PRICE_CHANGE_EXCEPTIONS;
      fetch C_PRICE_CHANGE_EXCEPTIONS BULK COLLECT into O_child_event_ids;
      close C_PRICE_CHANGE_EXCEPTIONS;

   elsif I_price_event_type in (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      open C_CLEARANCE_EXCEPTIONS;
      fetch C_CLEARANCE_EXCEPTIONS BULK COLLECT into O_child_event_ids;
      close C_CLEARANCE_EXCEPTIONS;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD) then

      open C_SIMPLE_PROMO_EXCEPTIONS;
      fetch C_SIMPLE_PROMO_EXCEPTIONS BULK COLLECT into O_child_event_ids;
      close C_SIMPLE_PROMO_EXCEPTIONS;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD) then

      open C_THRESHOLD_PROMO_EXCEPTIONS;
      fetch C_THRESHOLD_PROMO_EXCEPTIONS BULK COLLECT into O_child_event_ids;
      close C_THRESHOLD_PROMO_EXCEPTIONS;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD) then

      open C_COMPLEX_PROMO_EXCEPTIONS;
      fetch C_COMPLEX_PROMO_EXCEPTIONS BULK COLLECT into O_child_event_ids;
      close C_COMPLEX_PROMO_EXCEPTIONS;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      open C_TXN_PROMO_EXCEPTIONS;
      fetch C_TXN_PROMO_EXCEPTIONS BULK COLLECT into O_child_event_ids;
      close C_TXN_PROMO_EXCEPTIONS;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_CHILDREN;

--------------------------------------------------------------------------------

FUNCTION REMOVE_LOC_MOVE_TIMELINES(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_zone_ids     IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_LOC_MOVE_TIMELINES';

BEGIN

   delete from rpm_future_retail_gtt gtt
    where (gtt.price_event_id,
           gtt.item,
           gtt.location) IN (select /*+ CARDINALITY(z 10) */
                                    gtt2.price_event_id,
                                    gtt2.item,
                                    gtt2.location
                               from table(cast(I_zone_ids as OBJ_NUM_NUM_DATE_TBL)) z,
                                    rpm_location_move lm,
                                    rpm_future_retail_gtt gtt2
                              where gtt2.price_event_id = z.numeric_col1
                                and lm.location_move_id = gtt2.location_move_id
                                and lm.old_zone_id      = z.numeric_col2
                                and lm.effective_date  <= z.date_col);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        conflict_check_error_rec(NULL, NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_LOC_MOVE_TIMELINES;
--------------------------------------------------------------------------------

FUNCTION REMOVE_TIMELINES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_event_type IN     VARCHAR2,
                          I_event_ids        IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_TIMELINES';

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      delete from rpm_future_retail_gtt rfrg
       where EXISTS (select /*+ CARDINALITY(cei 10) */
                            'x'
                       from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                            rpm_future_retail_gtt rfrg1
                      where rfrg.price_event_id       = rfrg1.price_event_id
                        and rfrg.location             = rfrg1.location
                        and rfrg.zone_node_type       = rfrg1.zone_node_type
                        and rfrg.item                 = rfrg1.item
                        and NVL(rfrg.diff_id, '-999') = NVL(rfrg1.diff_id, '-999')
                        and rfrg1.price_event_id      = cei.numeric_col1
                        and rfrg1.price_change_id     = cei.numeric_col2);

      --delete exclusions
      delete from rpm_item_loc_gtt;

      insert into rpm_item_loc_gtt
         (price_event_id,
          item,
          diff_id,
          location,
          regular_zone_group) -- this field will hold the zone_node_type value
      -- tran exclusion from parent/loc or ItemList/Loc PC or
      -- location exception from item/zone level PC or
      -- location exclusion from parent/zone level PC
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.item               = rpc.item
         and rpc.diff_id              is NULL
         and rfrg1.location           = rpc.location
         and rfrg1.zone_node_type     = rpc.zone_node_type
      union all
      -- exclusion from parent/zone level price change -- Item/Loc timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             item_master im
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and im.item_parent           = rpc.item
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.item               = im.item
         and rfrg1.location           = rpc.location
         and rfrg1.zone_node_type     = rpc.zone_node_type
      union all
      -- exclusion from parent-diff/zone level price change -- Parent-Diff/Loc timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.item               = rpc.item
         and rpc.diff_id              is NOT NULL
         and rfrg1.diff_id            = rpc.diff_id
         and rfrg1.location           = rpc.location
         and rfrg1.zone_node_type     = rpc.zone_node_type
      union all
      -- exclusion from parent-diff/zone level price change -- Tran/Loc for diff 1 timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             item_master im
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.item_parent        = rpc.item
         and rpc.diff_id              is NOT NULL
         and rfrg1.diff_id            = rpc.diff_id
         and im.item                  = rfrg1.item
         and rpc.diff_id              = im.diff_1
         and rfrg1.location           = rpc.location
         and rfrg1.zone_node_type     = rpc.zone_node_type
      union all
      -- exclusion from parent-diff/zone level price change -- Tran/Loc for diff 2, 3, 4 timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             item_master im
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.item_parent        = rpc.item
         and rpc.diff_id              is NOT NULL
         and im.item                  = rfrg1.item
         and rpc.diff_id              IN (im.diff_2,
                                          im.diff_3,
                                          im.diff_4)
         and rfrg1.location           = rpc.location
         and rfrg1.zone_node_type     = rpc.zone_node_type
      union all
      -- tran item is excluded from a parent zone level PC or ItemList/Zone -- Item/Loc timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             rpm_zone_location rzl
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rzl.zone_id              = rpc.zone_id
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.location           = rzl.location
         and rfrg1.zone_node_type     = rzl.loc_type
         and rfrg1.item               = rpc.item
      union all
      -- tran item is excluded from a parent zone level PC or ItemList/Zone -- Item/Zone timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.location           = rpc.zone_id
         and rfrg1.zone_node_type     = rpc.zone_node_type
         and rfrg1.item               = rpc.item
      union all
      -- location is excluded from a ItemList/Zone
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc
       where rpc.exception_parent_id     = cei.numeric_col1
         and rpc.skulist                 is NOT NULL
         and rpc.price_event_itemlist    is NULL
         and rpc.zone_node_type          IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rpc.sys_generated_exclusion = 0
         and rfrg1.price_event_id        = rpc.exception_parent_id
         and rfrg1.location              = rpc.location
         and rfrg1.zone_node_type        = rpc.zone_node_type
      union all
      -- location is excluded from a PEIL/Zone
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc
       where rpc.exception_parent_id  = cei.numeric_col1
         and rpc.skulist              is NULL
         and rpc.price_event_itemlist is NOT NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id     = rpc.exception_parent_id
         and rfrg1.location           = rpc.location
         and rfrg1.zone_node_type     = rpc.zone_node_type
      union all
      -- SGE - itemlist/zone exclusion for tran_items
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             rpm_merch_list_detail mld
       where rpc.exception_parent_id     = cei.numeric_col1
         and rpc.skulist                 is NOT NULL
         and rpc.price_event_itemlist    is NULL
         and rpc.sys_generated_exclusion = 1
         and rpc.skulist                 = mld.merch_list_id
         and rfrg1.item                  = mld.item
         and mld.merch_level             = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rpc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id        = rpc.exception_parent_id
         and rfrg1.location              = rpc.zone_id
         and rfrg1.zone_node_type        = rpc.zone_node_type
      union all
      -- SGE - itemlist/zone exclusion for parent_items
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             rpm_merch_list_detail mld
       where rpc.exception_parent_id     = cei.numeric_col1
         and rpc.skulist                 is NOT NULL
         and rpc.price_event_itemlist    is NULL
         and rpc.sys_generated_exclusion = 1
         and rpc.skulist                 = mld.merch_list_id
         and rfrg1.item                  = mld.item
         and mld.merch_level             = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rpc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id        = rpc.exception_parent_id
         and rfrg1.location              = rpc.zone_id
         and rfrg1.zone_node_type        = rpc.zone_node_type
      union all
      -- SGE - itemlist/zone exclusion for parent diff items
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_price_change rpc,
             rpm_merch_list_detail mld
       where rpc.exception_parent_id     = cei.numeric_col1
         and rpc.skulist                 is NOT NULL
         and rpc.price_event_itemlist    is NULL
         and rpc.sys_generated_exclusion = 1
         and rpc.skulist                 = mld.merch_list_id
         and rfrg1.item                  = mld.item
         and rfrg1.diff_id               = mld.diff_id
         and mld.merch_level             = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and rpc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id        = rpc.exception_parent_id
         and rfrg1.location              = rpc.zone_id
         and rfrg1.zone_node_type        = rpc.zone_node_type;

      delete from rpm_future_retail_gtt rfrg
         where EXISTS (select 1
                         from rpm_item_loc_gtt ril
                        where ril.price_event_id       = rfrg.price_event_id
                          and ril.item                 = rfrg.item
                          and NVL(ril.diff_id, '-999') = NVL(rfrg.diff_id, '-999')
                          and ril.location             = rfrg.location
                          and ril.regular_zone_group   = rfrg.zone_node_type);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      delete from rpm_future_retail_gtt rfrg
       where EXISTS (select /*+ CARDINALITY(cei 10) */
                            'x'
                       from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                            rpm_future_retail_gtt rfrg1
                      where rfrg.price_event_id       = rfrg1.price_event_id
                        and rfrg.location             = rfrg1.location
                        and rfrg.zone_node_type       = rfrg1.zone_node_type
                        and rfrg.item                 = rfrg1.item
                        and NVL(rfrg.diff_id, '-999') = NVL(rfrg1.diff_id, '-999')
                        and rfrg1.price_event_id      = cei.numeric_col1
                        and rfrg1.clearance_id        = cei.numeric_col2);

      -- delete exclusions
      delete from rpm_item_loc_gtt;

      insert into rpm_item_loc_gtt (price_event_id,
                                    item,
                                    diff_id,
                                    location,
                                    regular_zone_group) --holds zone_node_type data for this
      -- tran exclusion from parent/loc or ItemList/Loc CLR or
      -- location exception from item/zone level CLR or
      -- location exclusion from parent/zone level CLR
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.item              = rc.item
         and rc.diff_id              is NULL
         and rfrg1.location          = rc.location
         and rfrg1.zone_node_type    = rc.zone_node_type
      union all
      -- loc exclusion from parent/zone level clearance -- Item/Loc timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             item_master im
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and im.item_parent          = rc.item
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.item              = im.item
         and rfrg1.location          = rc.location
         and rfrg1.zone_node_type    = rc.zone_node_type
      union all
      -- loc exclusion from parent-diff/zone level clearance -- Parent-Diff/Loc timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.item              = rc.item
         and rc.diff_id              is NOT NULL
         and rfrg1.diff_id           = rc.diff_id
         and rfrg1.location          = rc.location
         and rfrg1.zone_node_type    = rc.zone_node_type
      union all
      -- loc exclusion from parent-diff/zone level clearance -- Tran/Loc for diff 1 timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             item_master im
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.item_parent       = rc.item
         and rc.diff_id              is NOT NULL
         and rfrg1.diff_id           = rc.diff_id
         and im.item                 = rfrg1.item
         and rc.diff_id              = im.diff_1
         and rfrg1.location          = rc.location
         and rfrg1.zone_node_type    = rc.zone_node_type
      union all
      -- loc exclusion from parent-diff/zone level clearance -- Tran/Loc for diff 2, 3, 4 timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             item_master im
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.item_parent       = rc.item
         and rc.diff_id              is NOT NULL
         and im.item                 = rfrg1.item
         and rc.diff_id              IN (im.diff_2,
                                         im.diff_3,
                                         im.diff_4)
         and rfrg1.location          = rc.location
         and rfrg1.zone_node_type    = rc.zone_node_type
      union all
      -- tran item is excluded from a parent zone level CLR or ItemList/Zone -- I/L timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             rpm_zone_location rzl
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rzl.zone_id             = rc.zone_id
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.location          = rzl.location
         and rfrg1.zone_node_type    = rzl.loc_type
         and rfrg1.item              = rc.item
      union all
      -- tran item is excluded from a parent zone level or ItemList/Zone -- I/Z timelines
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NULL
         and rc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.location          = rc.zone_id
         and rfrg1.zone_node_type    = rc.zone_node_type
         and rfrg1.item              = rc.item
      union all
      -- location is excluded from an ItemList/Zone
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc
       where rc.exception_parent_id     = cei.numeric_col1
         and rc.skulist                 is NOT NULL
         and rc.price_event_itemlist    is NULL
         and rc.zone_node_type          IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                            RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rc.sys_generated_exclusion = 0
         and rfrg1.price_event_id       = rc.exception_parent_id
         and rfrg1.location             = rc.location
         and rfrg1.zone_node_type       = rc.zone_node_type
      union all
      -- location is excluded from a PEIL/Zone
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc
       where rc.exception_parent_id  = cei.numeric_col1
         and rc.skulist              is NULL
         and rc.price_event_itemlist is NOT NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.change_type          = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id    = rc.exception_parent_id
         and rfrg1.location          = rc.location
         and rfrg1.zone_node_type    = rc.zone_node_type
      union all
      -- SGE -  itemlist/zone exclusion tran item
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             rpm_merch_list_detail mld
       where rc.exception_parent_id     = cei.numeric_col1
         and rc.skulist                 is NOT NULL
         and rc.price_event_itemlist    is NULL
         and rc.sys_generated_exclusion = 1
         and rc.skulist                 = mld.merch_list_id
         and rfrg1.item                 = mld.item
         and mld.merch_level            = RPM_CONSTANTS.ITEM_MERCH_TYPE
         and rc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id       = rc.exception_parent_id
         and rfrg1.location             = rc.zone_id
         and rfrg1.zone_node_type       = rc.zone_node_type
      union all
      -- SGE -  itemlist/zone exclusion parent item
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             rpm_merch_list_detail mld
       where rc.exception_parent_id     = cei.numeric_col1
         and rc.skulist                 is NOT NULL
         and rc.price_event_itemlist    is NULL
         and rc.sys_generated_exclusion = 1
         and rc.skulist                 = mld.merch_list_id
         and rfrg1.item                 = mld.item
         and mld.merch_level            = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id       = rc.exception_parent_id
         and rfrg1.location             = rc.zone_id
         and rfrg1.zone_node_type       = rc.zone_node_type
      union all
      -- SGE -  itemlist/zone exclusion parent diff item
      select /*+ CARDINALITY(cei 10) */
             rfrg1.price_event_id,
             rfrg1.item,
             rfrg1.diff_id,
             rfrg1.location,
             rfrg1.zone_node_type
        from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
             rpm_future_retail_gtt rfrg1,
             rpm_clearance rc,
             rpm_merch_list_detail mld
       where rc.exception_parent_id     = cei.numeric_col1
         and rc.skulist                 is NOT NULL
         and rc.price_event_itemlist    is NULL
         and rc.sys_generated_exclusion = 1
         and rc.skulist                 = mld.merch_list_id
         and rfrg1.item                 = mld.item
         and rfrg1.diff_id              = mld.diff_id
         and mld.merch_level            = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
         and rc.change_type             = RPM_CONSTANTS.RETAIL_EXCLUDE
         and rfrg1.price_event_id       = rc.exception_parent_id
         and rfrg1.location             = rc.zone_id
         and rfrg1.zone_node_type       = rc.zone_node_type;

      delete from rpm_future_retail_gtt rfrg
         where EXISTS (select 1
                         from rpm_item_loc_gtt ril
                        where ril.price_event_id       = rfrg.price_event_id
                          and ril.item                 = rfrg.item
                          and NVL(ril.diff_id, '-999') = NVL(rfrg.diff_id, '-999')
                          and ril.location             = rfrg.location
                          and ril.regular_zone_group   = rfrg.zone_node_type);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      delete
        from rpm_future_retail_gtt frgtt
       where EXISTS (select /*+ CARDINALITY(cei 10) */ 1
                       from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                            rpm_promo_item_loc_expl_gtt prgtt
                      where prgtt.price_event_id       = cei.numeric_col1
                        and prgtt.promo_dtl_id         = cei.numeric_col2
                        and prgtt.price_event_id       = frgtt.price_event_id
                        and prgtt.item                 = frgtt.item
                        and NVL(prgtt.diff_id, '-999') = NVL(frgtt.diff_id, '-999')
                        and prgtt.location             = frgtt.location
                        and prgtt.zone_node_type       = frgtt.zone_node_type);

      delete
        from rpm_cust_segment_promo_fr_gtt cspfg
       where EXISTS (select /*+ CARDINALITY(cei 10) */ 1
                       from table(cast(I_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                            rpm_promo_item_loc_expl_gtt prgtt
                      where prgtt.price_event_id       = cei.numeric_col1
                        and prgtt.promo_dtl_id         = cei.numeric_col2
                        and cspfg.price_event_id       = prgtt.price_event_id
                        and cspfg.item                 = prgtt.item
                        and NVL(cspfg.diff_id, '-999') = NVL(prgtt.diff_id, '-999')
                        and cspfg.location             = prgtt.location
                        and cspfg.zone_node_type       = prgtt.zone_node_type
                        and cspfg.customer_type        = prgtt.customer_type
                        and cspfg.action_date         >= prgtt.detail_start_date
                        and cspfg.action_date         <= NVL(prgtt.detail_end_date, cspfg.action_date));

      forall i IN 1..I_event_ids.COUNT
         delete rpm_promo_item_loc_expl_gtt
          where price_event_id = I_event_ids(i).numeric_col1
            and promo_dtl_id   = I_event_ids(i).numeric_col2;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_TIMELINES;

--------------------------------------------------------------------------------

FUNCTION REMOVE_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_ids  IN     OBJ_NUM_NUM_DATE_TBL,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT';

   L_error_message VARCHAR2(255) := NULL;

BEGIN

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      update rpm_future_retail_gtt rfrg
         set rfrg.price_change_id               = NULL,
             rfrg.price_change_display_id       = NULL,
             rfrg.pc_exception_parent_id        = NULL,
             rfrg.pc_change_type                = NULL,
             rfrg.pc_change_amount              = NULL,
             rfrg.pc_change_currency            = NULL,
             rfrg.pc_change_percent             = NULL,
             rfrg.pc_change_selling_uom         = NULL,
             rfrg.pc_null_multi_ind             = NULL,
             rfrg.pc_multi_units                = NULL,
             rfrg.pc_multi_unit_retail          = NULL,
             rfrg.pc_multi_unit_retail_currency = NULL,
             rfrg.pc_multi_selling_uom          = NULL,
             rfrg.pc_price_guide_id             = NULL,
             rfrg.step_identifier               = RPM_CONSTANTS.CAPT_GTT_REMOVE_EVENT
       where (rfrg.price_event_id,
              rfrg.price_change_id) IN (select ids.numeric_col1,
                                               ids.numeric_col2
                                          from table(cast(I_price_event_ids as OBJ_NUM_NUM_DATE_TBL)) ids);

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      update rpm_future_retail_gtt rfrg
         set rfrg.clearance_id             = NULL,
             rfrg.clearance_display_id     = NULL,
             rfrg.clear_mkdn_index         = NULL,
             rfrg.clear_start_ind          = NULL,
             rfrg.clear_change_type        = NULL,
             rfrg.clear_change_amount      = NULL,
             rfrg.clear_change_currency    = NULL,
             rfrg.clear_change_percent     = NULL,
             rfrg.clear_change_selling_uom = NULL,
             rfrg.clear_price_guide_id     = NULL,
             rfrg.step_identifier          = RPM_CONSTANTS.CAPT_GTT_REMOVE_EVENT
       where (rfrg.price_event_id,
              rfrg.clearance_id)  IN (select ids.numeric_col1,
                                             ids.numeric_col2
                                        from table(cast(I_price_event_ids as OBJ_NUM_NUM_DATE_TBL)) ids)
         and rfrg.clear_start_ind != RPM_CONSTANTS.END_IND;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      update rpm_promo_item_loc_expl_gtt prgtt
         set deleted_ind     = 1,
             step_identifier = RPM_CONSTANTS.CAPT_GTT_REMOVE_EVENT
       where EXISTS (select 1
                       from table(cast(I_price_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei
                      where cei.numeric_col1 = prgtt.price_event_id
                        and cei.numeric_col2 = prgtt.promo_dtl_id);

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_REMOVE_EVENT,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_EVENT;

--------------------------------------------------------------------------------

FUNCTION REMOVE_FR_IL_TIMELINES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_price_event_ids  IN     OBJ_NUM_NUM_DATE_TBL,
                                I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_FUTURE_RETAIL_GTT_SQL.REMOVE_FR_IL_TIMELINES';

BEGIN

   delete from rpm_item_loc_gtt;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      insert into rpm_item_loc_gtt
         (price_event_id,
          item,
          location)
      with price_change as
         (select /*+ materialize USE_NL(pc) LEADING(IDS) */
                 cei.numeric_col1 price_event_id,
                 rpc.price_change_id,
                 rpc.item,
                 rpc.location,
                 rpc.zone_id,
                 rpc.zone_node_type,
                 rpc.skulist,
                 rpc.price_event_itemlist,
                 rpc.link_code
            from table(cast(I_price_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                 rpm_price_change rpc
           where rpc.exception_parent_id = cei.numeric_col1)
      --tran exclusion from parent/loc or ItemList/Loc PC or location exception from item/zone level PC
      select /*+ USE_NL(im) ORDERED  */
             rpc.price_event_id,
             rpc.item,
             rpc.location
        from price_change rpc,
             item_master im
       where rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.item                 = im.item
         and im.item_level            = im.tran_level
      union all
      --loc exclusion from parent/zone level price change
      select /*+ USE_NL(im) ORDERED  */
             rpc.price_event_id,
             im.item,
             rpc.location
        from price_change rpc,
             item_master im
       where rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent           = rpc.item
         and im.item_level            = im.tran_level
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      --where tran item is excluded from a parent zone level PC or ItemList/Zone
      select /*+ USE_NL(im) ORDERED  */
             rpc.price_event_id,
             rpc.item,
             rzl.location
        from price_change rpc,
             rpm_zone_location rzl,
             item_master im
       where rpc.skulist              is NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id              = rpc.zone_id
         and rpc.item                 = im.item
         and im.item_level            = im.tran_level
      union all
      --where location is excluded from a ItemList/Zone - Item level
      select /*+ USE_NL(rpcs,im) ORDERED */
             rpc.price_event_id,
             rpcs.item,
             rpc.location
        from price_change rpc,
             rpm_price_change_skulist rpcs
       where rpc.skulist              is NOT NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.skulist              = rpcs.skulist
         and rpc.price_change_id      = rpcs.price_event_id
         and rpcs.item_level          = RPM_CONSTANTS.IL_ITEM_LEVEL
      union all
      --where location is excluded from a ItemList/Zone - Parent level
      select /*+ USE_NL(rpcs,im) ORDERED */
             rpc.price_event_id,
             im.item,
             rpc.location
        from price_change rpc,
             rpm_price_change_skulist rpcs,
             item_master im
       where rpc.skulist              is NOT NULL
         and rpc.price_event_itemlist is NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.skulist              = rpcs.skulist
         and rpc.price_change_id      = rpcs.price_event_id
         and rpcs.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and im.item_parent           = rpcs.item
         and im.item_level            = im.tran_level
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      --where location is excluded from a PEIL/Zone - Item level
      select rpc.price_event_id,
             rmld.item,
             rpc.location
        from price_change rpc,
             rpm_merch_list_detail rmld
       where rpc.skulist              is NULL
         and rpc.price_event_itemlist is NOT NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
      union all
      --where location is excluded from a PEIL/Zone - Parent level
      select rpc.price_event_id,
             im.item,
             rpc.location
        from price_change rpc,
             rpm_merch_list_detail rmld,
             item_master im
       where rpc.skulist              is NULL
         and rpc.price_event_itemlist is NOT NULL
         and rpc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rpc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item                = im.item_parent
         and im.item_level            = im.tran_level
         and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

      delete from rpm_fr_item_loc_expl_gtt rfrg
       where EXISTS (select 1
                       from rpm_item_loc_gtt ril
                      where ril.price_event_id = rfrg.price_event_id
                        and ril.item           = rfrg.item
                        and ril.location       = rfrg.location);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      insert into rpm_item_loc_gtt
         (price_event_id,
          item,
          location)
      with clearance as
         (select /*+ MATERIALIZE USE_NL(rc) LEADING(IDS) */
                 cei.numeric_col1 price_event_id,
                 rc.clearance_id,
                 rc.item,
                 rc.location,
                 rc.skulist,
                 rc.price_event_itemlist,
                 rc.zone_id,
                 rc.zone_node_type
            from table(cast(I_price_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                 rpm_clearance rc
           where rc.exception_parent_id = cei.numeric_col1)
      --tran exclusion from parent/loc or ItemList/Loc CLR or location exception from item/zone level CLR
      select /*+ USE_NL(im) ORDERED */
             rc.price_event_id,
             rc.item,
             rc.location
        from clearance rc,
             item_master im
       where rc.price_event_itemlist is NULL
         and rc.skulist              is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item                 = rc.item
         and im.item_level           = im.tran_level
      union all
      --loc exclusion from parent/zone level price change
      select /*+ USE_NL(im) ORDERED */
             rc.price_event_id,
             im.item,
             rc.location
        from clearance rc,
             item_master im
       where rc.price_event_itemlist is NULL
         and rc.skulist              is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and im.item_parent          = rc.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
      union all
      --where tran item is excluded from a parent zone level PC or ItemList/Zone
      select /*+ USE_NL(im) ORDERED */
             rc.price_event_id,
             rc.item,
             rzl.location
        from clearance rc,
             rpm_zone_location rzl,
             item_master im
       where rc.price_event_itemlist is NULL
         and rc.skulist              is NULL
         and rc.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
         and rzl.zone_id             = rc.zone_id
         and rc.item                 = im.item
         and im.item_level           = im.tran_level
      union all
      --where location is excluded from a ItemList/Zone - item level
      select /*+ USE_NL(rcs, im) ORDERED */
             rc.price_event_id,
             rcs.item,
             rc.location
        from clearance rc,
             rpm_clearance_skulist rcs
       where rc.price_event_itemlist is NULL
         and rc.skulist              is NOT NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.skulist              = rcs.skulist
         and rc.clearance_id         = rcs.price_event_id
         and rcs.item_level          = RPM_CONSTANTS.IL_ITEM_LEVEL
      union all
      --where location is excluded from a ItemList/Zone - parent level
      select /*+ USE_NL(rcs, im) ORDERED */
             rc.price_event_id,
             im.item,
             rc.location
        from clearance rc,
             rpm_clearance_skulist rcs,
             item_master im
       where rc.price_event_itemlist is NULL
         and rc.skulist              is NOT NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.skulist              = rcs.skulist
         and rc.clearance_id         = rcs.price_event_id
         and rcs.item_level          = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
         and im.item_parent          = rcs.item
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
      union all
      --where location is excluded from a Price_Event_ItemList/Zone - item level
      select rc.price_event_id,
             rmld.item,
             rc.location
        from clearance rc,
             rpm_merch_list_detail rmld
       where rc.skulist              is NULL
         and rc.price_event_itemlist is NOT NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
      union all
      --where location is excluded from a Price_Event_ItemList/Zone - parent level
      select rc.price_event_id,
             im.item,
             rc.location
        from clearance rc,
             rpm_merch_list_detail rmld,
             item_master im
       where rc.price_event_itemlist is NOT NULL
         and rc.skulist              is NULL
         and rc.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
         and rc.price_event_itemlist = rmld.merch_list_id
         and rmld.merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
         and rmld.item               = im.item_parent
         and im.item_level           = im.tran_level
         and im.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
         and im.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

      delete from rpm_fr_item_loc_expl_gtt rfrg
       where EXISTS (select 1
                       from rpm_item_loc_gtt ril
                      where ril.price_event_id = rfrg.price_event_id
                        and ril.item           = rfrg.item
                        and ril.location       = rfrg.location);

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD) then

      insert into rpm_item_loc_gtt
         (price_event_id,
          item,
          location)
      with promo_dtl as
         (select /*+ MATERIALIZE USE_NL(rpd) LEADING(IDS) */
                 cei.numeric_col1 price_event_id,
                 rpd.promo_dtl_id,
                 rpdmn.merch_type,
                 rpdmn.dept,
                 rpdmn.class,
                 rpdmn.subclass,
                 rpdmn.item,
                 rpdmn.diff_id,
                 rpdmn.skulist,
                 rpdmn.price_event_itemlist,
                 rpzl.location,
                 rpzl.zone_id,
                 rpzl.zone_node_type
            from table(cast(I_price_event_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                 rpm_promo_dtl rpd,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_zone_location rpzl,
                 rpm_promo_dtl_disc_ladder rpddl
           where rpd.exception_parent_id  = cei.numeric_col1
             and rpd.promo_dtl_id         = rpdmn.promo_dtl_id
             and rpd.promo_dtl_id         = rpzl.promo_dtl_id
             and rpddl.promo_dtl_list_id  = rpdmn.promo_dtl_list_id)
         -- item exception/exclusion from promo dtl - loc level
         -- OR - loc exception/exclusion from promo dtl - item level
         select /*+ USE_NL(im) ORDERED */
                rpd.price_event_id,
                rpd.item,
                rpd.location
           from promo_dtl rpd
          where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
         -- item exception/exclusion from promo dtl - zone level
         union all
         select /*+ USE_NL(im) ORDERED */
                rpd.price_event_id,
                rpd.item,
                rzl.location
           from promo_dtl rpd,
                rpm_zone_location rzl
          where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
            and rpd.zone_id        = rzl.zone_id
         -- loc exception/exclusion from promo_dtl - merch level
         union all
         select /*+ USE_NL(im) ORDERED */
                rpd.price_event_id,
                im.item,
                rpd.location
           from promo_dtl rpd,
                item_master im
          where rpd.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                       RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                       RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and im.dept            = rpd.dept
            and im.class           = NVL(rpd.class, im.class)
            and im.subclass        = NVL(rpd.subclass, im.subclass)
            and im.item_level      = im.tran_level
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         -- loc exception/exclusion from promo_dtl - parent level
         union all
         select /*+ USE_NL(im) ORDERED */
                rpd.price_event_id,
                im.item,
                rpd.location
           from promo_dtl rpd,
                item_master im
          where rpd.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and im.item_parent     = rpd.item
            and im.item_level      = im.tran_level
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         -- loc exception/exclusion from promo_dtl - parent/diff level
         union all
         select /*+ USE_NL(im) ORDERED */
                rpd.price_event_id,
                im.item,
                rpd.location
           from promo_dtl rpd,
                item_master im
          where rpd.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and im.item_parent     = rpd.item
            and im.item_level      = im.tran_level
            and rpd.diff_id        IN (im.diff_1,
                                       im.diff_2,
                                       im.diff_3,
                                       im.diff_4)
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         -- loc exception/exclusion from promo_dtl - skulist level - item level
         union all
         select /*+ USE_NL(rpds, im) ORDERED */
                rpd.price_event_id,
                rpds.item,
                rpd.location
           from promo_dtl rpd,
                rpm_promo_dtl_skulist rpds
          where rpd.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpd.promo_dtl_id   = rpds.price_event_id
            and rpd.skulist        = rpds.skulist
            and rpds.item_level    = RPM_CONSTANTS.IL_ITEM_LEVEL
         -- loc exception/exclusion from promo_dtl - skulist level - parent level
         union all
         select /*+ USE_NL(rpds, im) ORDERED */
                rpd.price_event_id,
                rpds.item,
                rpd.location
           from promo_dtl rpd,
                rpm_promo_dtl_skulist rpds,
                item_master im
          where rpd.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
            and rpd.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpd.promo_dtl_id   = rpds.price_event_id
            and rpd.skulist        = rpds.skulist
            and rpds.item_level    = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
            and im.item_parent     = rpds.item
            and im.item_level      = im.tran_level
            and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
         -- loc exception/exclusion from promo_dtl - PEIL level - item level
         union all
         select rpd.price_event_id,
                rmld.item,
                rpd.location
           from promo_dtl rpd,
                rpm_merch_list_detail rmld
          where rpd.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and rpd.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpd.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level         = RPM_CONSTANTS.ITEM_MERCH_TYPE
         -- loc exception/exclusion from promo_dtl - PEIL level - parent level
         union all
         select rpd.price_event_id,
                rmld.item,
                rpd.location
           from promo_dtl rpd,
                rpm_merch_list_detail rmld,
                item_master im
          where rpd.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
            and rpd.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
            and rpd.price_event_itemlist = rmld.merch_list_id
            and rmld.merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
            and rmld.item                = im.item_parent
            and im.item_level            = im.tran_level
            and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
            and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES;

      delete from rpm_fr_item_loc_expl_gtt rfrg
       where EXISTS (select 1
                       from rpm_item_loc_gtt ril
                      where ril.price_event_id = rfrg.price_event_id
                        and ril.item           = rfrg.item
                        and ril.location       = rfrg.location);

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_FR_IL_TIMELINES;
--------------------------------------------------------------------------------
FUNCTION GET_PROMO_SIBLINGS(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                            O_sibling_promo_ids    OUT OBJ_NUM_NUM_DATE_TBL,
                            I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_FUTURE_RETAIL_GTT_SQL.GET_PROMO_SIBLINGS';

   cursor C_SIBLING_PROMO_ID is
      select /*+ CARDINALITY(ids 10) */
             OBJ_NUM_NUM_DATE_REC(rpd1.promo_dtl_id,
                                  rpd2.promo_dtl_id,
                                  NULL)
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd2,
             rpm_promo_dtl rpd1
       where rpd1.promo_dtl_id         = VALUE(ids)
         and rpd2.man_txn_exclusion   != 1
         and rpd2.exception_parent_id  = rpd1.exception_parent_id
         and rpd2.promo_dtl_id        != rpd1.promo_dtl_id
         and rpd2.state               IN (RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                                          RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                          RPM_CONSTANTS.PR_COMPLETE_STATE_CODE,
                                          RPM_CONSTANTS.PR_CANCELLED_STATE_CODE,
                                          RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE)
    order by rpd2.exception_parent_id;

BEGIN

   open C_SIBLING_PROMO_ID;
   fetch C_SIBLING_PROMO_ID BULK COLLECT into O_sibling_promo_ids;
   close C_SIBLING_PROMO_ID;

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_PROMO_SIBLINGS;
--------------------------------------------------------------------------------

END RPM_FUTURE_RETAIL_GTT_SQL;
/
