CREATE OR REPLACE PACKAGE RPM_CC_PROM_LT_CLEAR_REG AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------
END;
/

