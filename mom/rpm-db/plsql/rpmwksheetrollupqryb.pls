CREATE OR REPLACE PACKAGE BODY RPM_WSFILTER_ROLLUP_QUERY AS
-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_PRIMARY_COMP_ALERT (I_proposed_strategy_id    IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE,
                                 I_area_diff_id            IN RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                                 I_item_id                 IN RPM_WORKSHEET_ITEM_DATA.ITEM%TYPE,
                                 I_new_retail              IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL%TYPE,
                                 I_new_retail_uom          IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_UOM%TYPE,
                                 I_primary_comp_retail     IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL%TYPE,
                                 I_primary_comp_retail_uom IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL_UOM%TYPE)
RETURN NUMBER IS

   L_error_msg             RTK_ERRORS.rtk_text%TYPE          := NULL;
   L_converted_retail      ITEM_LOC.unit_retail%TYPE         := 0;
   L_percent               RPM_STRATEGY_DETAIL.percent%TYPE  := 0;
   L_exact_retail          ITEM_LOC.unit_retail%TYPE         := 0;
   L_minimum               NUMBER                            := 0;
   L_maximum               NUMBER                            := 0;
   L_return                NUMBER                            := 0;
   L_area_bool             BOOLEAN                           := TRUE;

   cursor C_AREA_DIFF_COMP is
      select area_diff.area_diff_id,
             area_diff.compete_type,
             area_diff.percent,
             area_diff.from_percent,
             area_diff.to_percent
        from rpm_area_diff_comp area_diff,
             rpm_area_diff rad
       where rad.area_diff_id = i_area_diff_id
         and rad.area_diff_id = area_diff.area_diff_id;

   cursor C_STRATEGY_DETAILS is
      select rsd.strategy_id,
             rsd.strategy_detail_id,
             rsd.strategy_detail_class,
             rsd.compete_type,
             rsd.percent,
             rsd.from_percent,
             rsd.to_percent
        from rpm_strategy rs,
             rpm_strategy_detail rsd
       where rs.strategy_id = I_proposed_strategy_id
         and rs.strategy_id = rsd.strategy_id
         and rs.state       = 'activeState';
         
   L_area_diff_comp_rec    c_area_diff_comp%ROWTYPE;
   L_strategy_details_rec  c_strategy_details%ROWTYPE;

BEGIN

   -- Check if secondary area differential details are available

   if I_area_diff_id is NOT NULL and
      I_area_diff_id > 0         then
      open  C_AREA_DIFF_COMP;
      fetch C_AREA_DIFF_COMP into L_area_diff_comp_rec;
      close C_AREA_DIFF_COMP;
      L_area_bool := TRUE;
   else
      open  C_STRATEGY_DETAILS;
      fetch C_STRATEGY_DETAILS into L_strategy_details_rec;
      close C_STRATEGY_DETAILS;
      L_area_bool := FALSE;
   end if;

   -- Convert the competitor retail to the new retail UOM

   if I_primary_comp_retail_uom <> I_new_retail_uom then
      if RPM_WRAPPER.UOM_CONVERT_VALUE (L_error_msg,
                                        L_converted_retail,
                                        I_item_id,
                                        I_primary_comp_retail,
                                        I_primary_comp_retail_uom,
                                        I_new_retail_uom) = 0 then
         RETURN 0;
      end if;
   else
      L_converted_retail := I_primary_comp_retail;
   end if;

   -- Compare the new retail and converted comp retail

   if L_area_bool = TRUE then

      -- Condition is MATCH
      if L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
         I_new_retail                      = L_converted_retail       then
         ---
         L_return := 1;
      elsif L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
            I_new_retail                     != L_converted_retail       then
         ---
         L_return := 0;
      end if;      -- End of MATCH condition

      -- Condition is PRICE_ABOVE
      if L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_ABOVE then
         L_minimum := (L_area_diff_comp_rec.from_percent + 1) * L_converted_retail;
         L_maximum := (L_area_diff_comp_rec.to_percent + 1) * L_converted_retail;
      -- if minimum or maximum is NULL
         if L_minimum is NULL or
            L_maximum is NULL then
            ---
            L_exact_retail := (L_area_diff_comp_rec.percent + 1) * L_converted_retail;
            if L_exact_retail = L_converted_retail then
               L_return := 1;
            elsif L_exact_retail <> L_converted_retail then
               L_return := 0;
            end if;
         elsif I_new_retail < L_minimum or 
               I_new_retail > L_maximum then
            ---
            L_return := 0;
         elsif I_new_retail > L_minimum and 
               I_new_retail < L_maximum then
            ---
            L_return := 1;
         end if;
      end if;      -- End of PRICE_ABOVE condition.

      -- Condition is PRICE_BELOW
      if L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_BELOW then
         L_minimum := (L_area_diff_comp_rec.from_percent - 1) * L_converted_retail;
         L_maximum := (L_area_diff_comp_rec.to_percent - 1) * L_converted_retail;
      -- if minimum or maximum is NULL
         if L_minimum is NULL or 
            L_maximum is NULL then
            ---
            L_exact_retail := (L_area_diff_comp_rec.percent - 1) * L_converted_retail;
            if L_exact_retail = L_converted_retail then
               L_return := 1;
            elsif L_exact_retail <> L_converted_retail then
               L_return := 0;
            end if;
         elsif I_new_retail < L_minimum or 
               I_new_retail > L_maximum then
            ---
            L_return := 0;
         elsif I_new_retail > L_minimum and 
               I_new_retail < L_maximum then
            ---
            L_return := 1;
         end if;
      end if;      -- End of PRICE_BELOW condition.
   -- End of  L_area_diff_comp_rec

   else

      -- Condition is MATCH
      if L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
         I_new_retail                        = L_converted_retail       then
         ---
         L_return := 1;
      elsif L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
            I_new_retail                       != L_converted_retail    then
         ---
         L_return := 0;
      end if;      -- End of MATCH condition

      -- Condition is PRICE_ABOVE
      if L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_ABOVE then
         L_minimum := (L_strategy_details_rec.from_percent + 1) * L_converted_retail;
         L_maximum := (L_strategy_details_rec.to_percent + 1) * L_converted_retail;
      -- if minimum or maximum is NULL
         if L_minimum is NULL or 
            L_maximum is NULL then
            ---
            L_exact_retail := (L_strategy_details_rec.percent + 1) * L_converted_retail;
            if L_exact_retail = L_converted_retail then
               L_return := 1;
            elsif L_exact_retail <> L_converted_retail then
               L_return := 0;
            end if;
         elsif I_new_retail < L_minimum or 
               I_new_retail > L_maximum then
            ---
            L_return := 0;
         elsif I_new_retail > L_minimum and 
               I_new_retail < L_maximum then
            ---
            L_return := 1;
         end if;
      end if;      -- End of PRICE_ABOVE condition.

      -- Condition is PRICE_BELOW
      if L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_BELOW then
         L_minimum := (L_strategy_details_rec.from_percent - 1) * L_converted_retail;
         L_maximum := (L_strategy_details_rec.to_percent - 1) * L_converted_retail;
      -- if minimum or maximum is NULL
         if L_minimum is NULL or 
            L_maximum is NULL then
            ---
            L_exact_retail := (L_strategy_details_rec.percent - 1) * L_converted_retail;
            if L_exact_retail = L_converted_retail then
               L_return := 1;
            elsif L_exact_retail <> L_converted_retail then
               L_return := 0;
            end if;
         elsif I_new_retail < L_minimum or 
               I_new_retail > L_maximum then
            ---
            L_return := 0;
         elsif I_new_retail > L_minimum and 
               I_new_retail < L_maximum then
            ---
            L_return := 1;
         end if;
      end if;      -- End of PRICE_ABOVE condition.

   -- End of ELSE condition
   end if;

   RETURN L_return;

EXCEPTION
   when others then
      RETURN 0;

END GET_PRIMARY_COMP_ALERT;

-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_COMP_A_ALERT (I_proposed_strategy_id    IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE,
                           I_area_diff_id            IN RPM_AREA_DIFF.AREA_DIFF_ID%TYPE,
                           I_item_id                 IN RPM_WORKSHEET_ITEM_DATA.ITEM%TYPE,
                           I_new_retail              IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL%TYPE,
                           I_new_retail_uom          IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_UOM%TYPE,
                           I_primary_comp_retail     IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL%TYPE,
                           I_primary_comp_retail_uom IN RPM_WORKSHEET_ITEM_DATA.PRIMARY_COMP_RETAIL_UOM%TYPE)
RETURN NUMBER IS

   L_error_msg             RTK_ERRORS.rtk_text%TYPE          := NULL;
   L_converted_retail      ITEM_LOC.unit_retail%TYPE         := 0;
   L_percent               RPM_STRATEGY_DETAIL.percent%TYPE  := 0;
   L_exact_retail          ITEM_LOC.unit_retail%TYPE         := 0;
   L_minimum               NUMBER                            := 0;
   L_maximum               NUMBER                            := 0;
   L_return                NUMBER                            := 0;
   L_area_bool             BOOLEAN                           := TRUE;

   cursor C_AREA_DIFF_COMP is
      select area_diff.area_diff_id,
             area_diff.compete_type,
             area_diff.percent,
             area_diff.from_percent,
             area_diff.to_percent
        from rpm_area_diff_comp area_diff,
             rpm_area_diff rad
       where rad.area_diff_id = I_area_diff_id
         and rad.area_diff_id = area_diff.area_diff_id;

   cursor C_STRATEGY_DETAILS is
      select rsd.strategy_id,
             rsd.strategy_detail_id,
             rsd.strategy_detail_class,
             rsd.compete_type,
             rsd.percent,
             rsd.from_percent,
             rsd.to_percent,
             rsd.mkt_basket_code
        from rpm_strategy rs,
             rpm_strategy_detail rsd
       where rs.strategy_id = I_proposed_strategy_id
         and rs.strategy_id = rsd.strategy_id
         and rs.state       = 'activeState';

   L_area_diff_comp_rec    c_area_diff_comp%ROWTYPE;
   L_strategy_details_rec  c_strategy_details%ROWTYPE;

BEGIN

   -- Check if secondary area differential details are available

   if I_area_diff_id is NOT NULL and
      I_area_diff_id > 0         then
      open  C_AREA_DIFF_COMP;
      fetch C_AREA_DIFF_COMP into l_area_diff_comp_rec;
      close C_AREA_DIFF_COMP;
      L_area_bool := TRUE;
   else
      open  C_STRATEGY_DETAILS;
      fetch C_STRATEGY_DETAILS into l_strategy_details_rec;
      close C_STRATEGY_DETAILS;
      L_area_bool := FALSE;
   end if;

   -- Convert the competitor retail to the new retail UOM

   if I_primary_comp_retail_uom <> I_new_retail_uom then
      if RPM_WRAPPER.UOM_CONVERT_VALUE (L_error_msg,
                                        L_converted_retail,
                                        I_item_id,
                                        I_primary_comp_retail,
                                        I_primary_comp_retail_uom,
                                        I_new_retail_uom) = 0 then
         RETURN 0;
      end if;
   else
      L_converted_retail := I_primary_comp_retail;
   end if;

   -- Compare the new retail and converted comp retail and do the math if the values are different.

   if L_area_bool = TRUE then

      -- Condition is MATCH
      if L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
         I_new_retail                      = L_converted_retail       then
         ---
         L_return := 1;
      elsif L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
            I_new_retail                      != L_converted_retail      then
         ---
         L_return := 0;
      end if;      -- End of MATCH condition

      -- Condition is PRICE_ABOVE
      if L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_ABOVE then
         L_exact_retail := (L_area_diff_comp_rec.percent + 1) * L_converted_retail;
         if L_exact_retail = L_converted_retail then
            L_return := 1;
         elsif L_exact_retail <> L_converted_retail then
            L_return := 0;
         end if;
      end if;      -- End of PRICE_ABOVE condition

      -- Condition is PRICE_BELOW
      if L_area_diff_comp_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_BELOW then
         L_exact_retail := (L_area_diff_comp_rec.percent - 1) * L_converted_retail;
         if L_exact_retail = L_converted_retail then
            L_return := 1;
         elsif L_exact_retail <> L_converted_retail then
            L_return := 0;
         end if;
      end if;      -- End of PRICE_BELOW condition

   -- End of L_area_diff_comp_rec
   else

      -- Condition is MATCH
      if L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
         I_new_retail                        = L_converted_retail       then
         ---
         L_return := 1;
      elsif L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_MATCH and 
            I_new_retail                        != L_converted_retail      then
         ---
         L_return := 0;
      end if;      -- End of MATCH condition

      -- Condition is PRICE_ABOVE
      if L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_ABOVE then
         L_exact_retail := (L_strategy_details_rec.percent + 1) * L_converted_retail;
         if L_exact_retail = L_converted_retail then
            L_return := 1;
         elsif L_exact_retail <> L_converted_retail then
            L_return := 0;
         end if;
      end if;      -- End of PRICE_ABOVE condition.

      -- Condition is PRICE_BELOW
      if L_strategy_details_rec.compete_type = RPM_CONSTANTS.COMP_PRICE_BELOW then
         L_exact_retail := (L_strategy_details_rec.percent - 1) * L_converted_retail;
         if L_exact_retail = L_converted_retail then
            L_return := 1;
         elsif L_exact_retail <> L_converted_retail then
            L_return := 0;
         end if;
      end if;      -- End of PRICE_BELOW condition.

   -- End of L_strategy_details_rec
   end if;

   RETURN L_return;

EXCEPTION
   when others then
      RETURN 0;

END GET_COMP_A_ALERT;

-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_PRICE_CHANGE_AMT (I_worksheet_status_id IN RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                               I_basis_retail        IN RPM_WORKSHEET_ZONE_WORKSPACE.BASIS_ZL_REGULAR_RETAIL%TYPE,
                               I_basis_retail_uom    IN RPM_WORKSHEET_ZONE_WORKSPACE.BASIS_ZL_REGULAR_RETAIL_UOM%TYPE,
                               I_item_id             IN RPM_WORKSHEET_ZONE_WORKSPACE.ITEM%TYPE,
                               I_new_retail          IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL%TYPE,
                               I_new_retail_uom      IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_UOM%TYPE)
RETURN NUMBER AS

   L_pc_calc_type           RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE  := 0;
   L_error_msg              RTK_ERRORS.rtk_text%TYPE                                 := NULL;
   L_converted_retail       ITEM_LOC.unit_retail%TYPE                                := 0;

   cursor C_PC_CALC_TYPE IS
      select rda.price_change_amount_calc_type 
        from rpm_dept_aggregation rda,
             rpm_worksheet_status rws
       where rws.dept                = rda.dept
         and rws.worksheet_status_id = I_worksheet_status_id;


BEGIN

   if I_basis_retail     is NULL or 
      I_basis_retail_uom is NULL or
      I_new_retail       is NULL or 
      I_new_retail_uom   is NULL then
      ---
      RETURN 0;
   end if;

   -- Convert the new retail to the basis retail UOM.
   if I_basis_retail_uom <> I_new_retail_uom then
      if RPM_WRAPPER.UOM_CONVERT_VALUE (L_error_msg,
                                        L_converted_retail,
                                        I_item_id,
                                        I_new_retail,
                                        I_new_retail_uom,
                                        I_basis_retail_uom) = 0 then
         RETURN 0;
      end if;
   else
      L_converted_retail := I_new_retail;
   end if;
   
   open  C_PC_CALC_TYPE;
   fetch C_PC_CALC_TYPE into L_pc_calc_type;
   close C_PC_CALC_TYPE;

   if L_pc_calc_type = 0 then
      RETURN (I_basis_retail - L_converted_retail);
   else
      RETURN (L_converted_retail - I_basis_retail);
   end if;

EXCEPTION
   when others then
      RETURN 0;

END GET_PRICE_CHANGE_AMT;

-----------------------------------------------------------------------------------------------------------------------
FUNCTION GET_COSTCHNG_REVPERD (I_pend_cost_change_date   IN DATE,
                               I_proposed_strategy_id    IN RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE)
RETURN NUMBER IS

   L_return        NUMBER            := 0;
   L_count         NUMBER            := 0;

   cursor C_REVPRD is
      select count(*)
        from rpm_calendar_period rcp, 
             rpm_calendar rc, 
             rpm_strategy rs
       where rcp.calendar_id = rc.calendar_id
         and rc.calendar_id  = rs.current_calendar_id
         and rs.strategy_id  = I_proposed_strategy_id
         and rs.state        = 'activeState'
         and I_pend_cost_change_date between rcp.start_date and rcp.end_date;

BEGIN

   open  C_REVPRD;
   fetch C_REVPRD into L_count;
   close C_REVPRD;

   if L_count > 0 then
      L_return := 1;
   else
      L_return := 0;
   end if;

   RETURN L_return;

EXCEPTION
   when others then
      RETURN 0;

END GET_COSTCHNG_REVPERD;

-----------------------------------------------------------------------------------------------------------------------
FUNCTION UPD_SELECTED_FLAG (O_error_msg       OUT VARCHAR2,
                            I_workspace_id IN     RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE,
                            I_item_ids_tbl IN     OBJ_VARCHAR_ID_TABLE,
                            I_link_codes   IN     OBJ_VARCHAR_ID_TABLE)
RETURN NUMBER IS
BEGIN

   merge into rpm_worksheet_zone_workspace target
   using (select /*+ cardinality(ids 1000) */
                 distinct rws_parent.item,
                          rws_parent.worksheet_item_data_id,
                          rws_parent.workspace_id 
            from table(cast(I_item_ids_tbl as OBJ_VARCHAR_ID_TABLE)) ids,
                 rpm_worksheet_zone_workspace rws,
                 rpm_worksheet_zone_workspace rws_parent
           where (   rws.item               = value(ids)
                  or rws.item_parent        = value(ids))
             and rws.workspace_id           = I_workspace_id
             and rws.workspace_id           = rws_parent.workspace_id
             and (  (    rws.item_parent    is NULL
                     and rws.item           = rws_parent.item)
                  or rws.item_parent        = rws_parent.item_parent)
           union
          select /*+ cardinality(ids 1000) */
                 distinct rws.item,
                          rws.worksheet_item_data_id,
                          rws.workspace_id
            from table(cast(I_link_codes as OBJ_VARCHAR_ID_TABLE)) ids, 
                 rpm_worksheet_zone_workspace rws
           where rws.link_code            = value(ids)
             and rws.workspace_id         = I_workspace_id) src
      ON (    src.worksheet_item_data_id = target.worksheet_item_data_id
          and src.workspace_id      = target.workspace_id
          and src.item              = target.item)
    when matched then
       update 
          set selected = 1;

   update rpm_worksheet_il_workspace
      set selected = 1
    where worksheet_item_data_id in (select worksheet_item_data_id
                                       from rpm_worksheet_zone_workspace
                                      where workspace_id = I_workspace_id
                                        and selected = 1);

   RETURN 1;

EXCEPTION 
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WSFILTER_ROLLUP_QUERY.UPD_SELECTED_FLAG',
                                        TO_CHAR(SQLCODE));
      RETURN 0;

END UPD_SELECTED_FLAG;

-----------------------------------------------------------------------------------------------------------------------
FUNCTION RESET_SELECTED_FLAG (O_error_msg       OUT VARCHAR2,
                              I_workspace_id IN     RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE)
RETURN NUMBER IS
BEGIN

   update rpm_worksheet_zone_workspace
      set selected     = 0
    where workspace_id = I_workspace_id
      and selected     = 1;

   update rpm_worksheet_il_workspace
      set selected     = 0
    where workspace_id = I_workspace_id
      and selected     = 1;
	  
   RETURN 1;

EXCEPTION 
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WSFILTER_ROLLUP_QUERY.RESET_SELECTED_FLAG',
                                        TO_CHAR(SQLCODE));
      RETURN 0;

END RESET_SELECTED_FLAG;

-----------------------------------------------------------------------------------------------------------------------
END RPM_WSFILTER_ROLLUP_QUERY;
/
