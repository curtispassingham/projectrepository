CREATE OR REPLACE PACKAGE BODY RPM_CC_ONE_CUST_SEG_PROMO_MAX  AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CC_ONE_CUST_SEG_PROMO_MAX.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_end_date  DATE   := TO_DATE('3000', 'YYYY');

   cursor C_CHECK is
      select /*+ CARDINALITY(ids 10) */
             distinct gtt.price_event_id,
             gtt.cust_segment_promo_id
        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_promo_dtl rpd,
             rpm_promo_comp rpc,
             rpm_promo_dtl_list_grp rpdlg,
             rpm_promo_dtl_list rpdl,
             rpm_promo_dtl_disc_ladder rpddl,
             rpm_cust_segment_promo_fr_gtt gtt,
             rpm_promo_item_loc_expl_gtt rpile
       where gtt.price_event_id                NOT IN (select ccet.price_event_id
                                                         from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and gtt.price_event_id                = VALUE(ids)
         and gtt.price_event_id                = rpd.promo_dtl_id
         and gtt.price_event_id                = rpile.price_event_id
         and gtt.dept                          = rpile.dept
         and gtt.item                          = rpile.item
         and NVL(gtt.diff_id, '-99999')        = NVL(rpile.diff_id, '-99999')
         and gtt.location                      = rpile.location
         and gtt.zone_node_type                = rpile.zone_node_type
         and rpile.promo_dtl_id                != rpd.promo_dtl_id
         and rpd.promo_comp_id                 = rpc.promo_comp_id
         and rpc.customer_type                 = rpile.customer_type
         and rpd.exception_parent_id           is NULL
         and NVL(rpile.detail_change_type, -9) != RPM_CONSTANTS.RETAIL_EXCLUDE
         and rpd.promo_dtl_id                  = rpdlg.promo_dtl_id
         and rpdlg.promo_dtl_list_grp_id       = rpdl.promo_dtl_list_grp_id
         and rpdl.promo_dtl_list_id            = rpddl.promo_dtl_list_id
         and NVL(rpddl.change_type, -9)        != RPM_CONSTANTS.RETAIL_EXCLUDE
         and (  (   rpd.start_date             BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, L_end_date)
                 or rpd.end_date               BETWEEN rpile.detail_start_date and NVL(rpile.detail_end_date, L_end_date))
             or (   rpile.detail_start_date    BETWEEN rpd.start_date and NVL(rpd.end_date, L_end_date)
                 or rpile.detail_end_date      BETWEEN rpd.start_date and NVL(rpd.end_date, L_end_date)));

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then

         L_error_tbl := IO_error_table;

      else

         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      end if;

      for rec IN C_CHECK loop

         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 NULL,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'an_approved_promotion_for_this_item_location_date_cust_seg_already_exists',
                                                 rec.cust_segment_promo_id);

         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;

         end if;

      end loop;

   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END RPM_CC_ONE_CUST_SEG_PROMO_MAX;
/
