CREATE OR REPLACE PACKAGE BODY RPM_CC_CLEAR_MKDN AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_CC_CLEAR_MKDN.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select price_event_id,
             future_retail_id
        from (select *
                from (select gtt.price_event_id,
                             gtt.item,
                             gtt.diff_id,
                             gtt.location,
                             gtt.zone_node_type,
                             gtt.future_retail_id,
                             gtt.clear_start_ind,
                             gtt.clear_retail,
                             0 has_conflict,
                             DENSE_RANK() OVER (PARTITION BY gtt.price_event_id,
                                                             gtt.item,
                                                             gtt.diff_id,
                                                             gtt.location,
                                                             gtt.zone_node_type
                                                    ORDER BY gtt.action_date asc) rank
                        from rpm_future_retail_gtt gtt
                       where gtt.price_event_id NOT IN (select ccet.price_event_id
                                                          from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet)
                         and gtt.clearance_id is NOT NULL
                         and gtt.clear_retail is NOT NULL) t
      model partition by (price_event_id,
                          item,
                          diff_id,
                          location,
                          zone_node_type)
            dimension by (rank)
            measures (t.future_retail_id,
                      t.clear_start_ind   clearance_start_ind,
                      t.clear_retail      clearance_retail,
                      t.has_conflict      new_has_conflict)
            rules (new_has_conflict[rank] order by rank =
                      case
                         when CV(rank) = 1 then
                            0
                         when (  (clearance_retail[CV(rank)] > clearance_retail[CV(rank) - 1] and
                                  clearance_start_ind[CV(rank)] != RPM_CONSTANTS.END_IND and
                                  clearance_start_ind[CV(rank) - 1] IN (RPM_CONSTANTS.START_IND,
                                                                        RPM_CONSTANTS.IN_PROCESS_IND))
                               or(clearance_retail[CV(rank)] = clearance_retail[CV(rank) - 1] and
                                  clearance_start_ind[CV(rank)] = 1 and
                                  clearance_start_ind[CV(rank) - 1] IN (RPM_CONSTANTS.IN_PROCESS_IND,
                                                                        RPM_CONSTANTS.START_IND))) then
                            1
                         else
                            0
                      end)
                  order by rank)
       where new_has_conflict = 1;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE ) then

      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         ---
         L_error_tbl := IO_error_table;
         ---
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      for rec IN C_CHECK loop
         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'event_causes_clearance_retail_less_than_prior_clearance');
         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;
         end if;
      end loop;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

