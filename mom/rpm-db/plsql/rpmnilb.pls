CREATE OR REPLACE PACKAGE BODY RPM_NEW_ITEM_LOC_SQL AS
--------------------------------------------------------

   LP_vdate DATE := GET_VDATE;

   ERROR_STATUS       CONSTANT VARCHAR2(1) := 'E';
   IN_PROGRESS_STATUS CONSTANT VARCHAR2(1) := 'I';

   --Instrumentation
   LP_return_code NUMBER         := NULL;
   LP_error_msg   VARCHAR2(2000) := NULL;

--------------------------------------------------------
--PROTOTYPES
--------------------------------------------------------

FUNCTION REMOVE_DELETED(O_error_msg    IN OUT VARCHAR2,
                        I_stage_status IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION REMOVE_DUPLICATES(O_error_msg    IN OUT VARCHAR2,
                           I_stage_status IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION REMOVE_UNWANTED(O_error_msg    IN OUT VARCHAR2,
                         I_stage_status IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION REMOVE_EXISTING(O_error_msg    IN OUT VARCHAR2,
                         I_stage_status IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION STAGE_ITEM_LOCS_TO_PROCESS(O_error_msg         IN OUT VARCHAR2,
                                    O_max_thread_number    OUT NUMBER,
                                    I_luw               IN     NUMBER,
                                    I_stage_status      IN     VARCHAR2,
                                    I_process_id        IN     NUMBER)
RETURN BOOLEAN;

FUNCTION SEED_FUTURE_RETAIL(O_error_msg         OUT VARCHAR2,
                            I_bulk_cc_pe_ids IN     OBJ_NUMERIC_ID_TABLE,
                            I_thread_number  IN     NUMBER,
                            I_process_id     IN     NUMBER)
RETURN BOOLEAN;


FUNCTION SEED_FUTURE_RETAIL_NO_PE(O_error_msg         OUT VARCHAR2,
                                  I_chunk_bulk_ids IN     OBJ_NUM_NUM_STR_TBL,
                                  I_thread_number  IN     NUMBER,
                                  I_process_id     IN     NUMBER)
RETURN BOOLEAN;

FUNCTION MARK_LOCATION_MOVE(O_error_msg         OUT VARCHAR2,
                            I_bulk_cc_pe_ids IN     OBJ_NUMERIC_ID_TABLE,
                            I_thread_number  IN     NUMBER,
                            I_process_id     IN     NUMBER)
RETURN BOOLEAN;

FUNCTION MARK_LOCATION_MOVE_NO_PE(O_error_msg      IN OUT VARCHAR2,
                                  I_chunk_bulk_ids IN     OBJ_NUM_NUM_STR_TBL,
                                  I_thread_number  IN     NUMBER,
                                  I_process_id     IN     NUMBER)
RETURN BOOLEAN;

FUNCTION COUNT_ITEM_LOC_ROWS(O_error_msg        OUT VARCHAR2,
                             O_count            OUT NUMBER,
                             I_bulk_cc_pe_id IN     RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE)
RETURN BOOLEAN;

FUNCTION SETUP_PRICE_EVENT_INFO(O_error_msg                 OUT VARCHAR2,
                                I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                                I_thread_number          IN     VARCHAR2,
                                I_process_id             IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION SETUP_PRICE_CHANGES(O_error_msg        OUT VARCHAR2,
                             I_thread_number IN     NUMBER,
                             I_process_id    IN     NUMBER)
RETURN BOOLEAN;

FUNCTION SETUP_CLEARANCES(O_error_msg        OUT VARCHAR2,
                          I_thread_number IN     NUMBER,
                          I_process_id    IN     NUMBER)
RETURN BOOLEAN;

FUNCTION SETUP_SIMPLE_PROMOS(O_error_msg        OUT VARCHAR2,
                             I_thread_number IN     NUMBER,
                             I_process_id    IN     NUMBER)
RETURN BOOLEAN;

FUNCTION SETUP_THRESHOLD_PROMOS(O_error_msg        OUT VARCHAR2,
                                I_thread_number IN     NUMBER,
                                I_process_id    IN     NUMBER)
RETURN BOOLEAN;

FUNCTION SETUP_COMPLEX_PROMOS(O_error_msg        OUT VARCHAR2,
                              I_thread_number IN     NUMBER,
                              I_process_id    IN     NUMBER)
RETURN BOOLEAN;

FUNCTION SETUP_TRANSACTION_PROMOS(O_error_msg                 OUT VARCHAR2,
                                  I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                                  I_thread_number          IN     NUMBER,
                                  I_process_id             IN     NUMBER)
RETURN BOOLEAN;

--------------------------------------------------------
--INTERNAL PROCEDURES
--------------------------------------------------------

FUNCTION REMOVE_DELETED(O_error_msg    IN OUT VARCHAR2,
                        I_stage_status IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.REMOVE_DELETED';

   cursor C_ROWID is
      select rowid
        from rpm_stage_item_loc sil
       where status = I_stage_status
         and NOT EXISTS (select item
                          from item_master im
                         where im.item = sil.item);

   TYPE rowid_array IS TABLE OF C_ROWID%ROWTYPE;
   L_rowid_array rowid_array;

BEGIN

   open C_ROWID;
   fetch C_ROWID BULK COLLECT into L_rowid_array;
   close C_ROWID;

   forall i IN 1..L_rowid_array.COUNT
      delete
        from rpm_stage_item_loc sil
       where status = I_stage_status
         and rowid  = L_rowid_array(i).rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END REMOVE_DELETED;

--------------------------------------------------------

FUNCTION REMOVE_DUPLICATES(O_error_msg    IN OUT  VARCHAR2,
                           I_stage_status IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_NEW_ITEM_LOC_SQL.REMOVE_DUPLICATES';

   cursor C_ROWID is
      select sub.rowid
        from (select rowid,
                     ROW_NUMBER() OVER (PARTITION BY item,
                                                     loc
                                            ORDER BY status) cnt
                from rpm_stage_item_loc
               where status = I_stage_status) sub
       where sub.cnt > 1;

   TYPE rowid_array IS TABLE OF C_ROWID%ROWTYPE;
   L_rowid_array rowid_array;

BEGIN

   open C_ROWID;
   fetch C_ROWID BULK COLLECT into L_rowid_array;
   close C_ROWID;

   forall i IN 1..L_rowid_array.COUNT
      delete from rpm_stage_item_loc
       where status = I_stage_status
         and rowid  = L_rowid_array(i).rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END REMOVE_DUPLICATES;

--------------------------------------------------------

FUNCTION REMOVE_UNWANTED(O_error_msg    IN OUT VARCHAR2,
                         I_stage_status IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.REMOVE_UNWANTED';

   cursor C_ROWID is
      select s.rowid
        from rpm_stage_item_loc s
       where s.status     = I_stage_status
         and (   s.item IN (select im.item
                              from item_master im
                             where im.item = s.item
                               and (   im.item_level   != im.tran_level
                                    or im.sellable_ind != RPM_CONSTANTS.SELLABLE_ITEM_IND_YES))
              or s.loc_type NOT IN (select RPM_CONSTANTS.LOCATION_TYPE_STORE
                                      from dual
                                    union all
                                    select DECODE(recognize_wh_as_locations,
                                                  1, RPM_CONSTANTS.LOCATION_TYPE_WH,
                                                  RPM_CONSTANTS.LOCATION_TYPE_STORE)
                                      from rpm_system_options)
              or s.selling_unit_retail is NULL
              or s.selling_uom         is NULL);

   TYPE rowid_array IS TABLE OF C_ROWID%ROWTYPE;
   L_rowid_array rowid_array;

BEGIN

   open C_ROWID;
   fetch C_ROWID BULK COLLECT into L_rowid_array;
   close C_ROWID;

   forall i IN 1..L_rowid_array.COUNT
      delete from rpm_stage_item_loc s
       where status = I_stage_status
         and rowid  = L_rowid_array(i).rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END REMOVE_UNWANTED;

--------------------------------------------------------

FUNCTION REMOVE_EXISTING(O_error_msg    IN OUT VARCHAR2,
                         I_stage_status IN     VARCHAR2)
RETURN BOOLEAN IS

  L_program VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.REMOVE_EXISTING';

   cursor C_ROWID is
      select /*+ ORDERED */ rsil.rowid
        from rpm_stage_item_loc rsil,
             item_master im,
             rpm_item_loc ril
       where rsil.status     = I_stage_status
         and rsil.process_id is NULL
         and im.item         = rsil.item
         and ril.dept        = im.dept
         and ril.item        = im.item
         and ril.loc         = rsil.loc;

   TYPE rowid_array IS TABLE OF C_ROWID%ROWTYPE;
   L_rowid_array rowid_array;

BEGIN

   open C_ROWID;
   fetch C_ROWID BULK COLLECT into L_rowid_array;
   close C_ROWID;

   forall i IN 1..L_rowid_array.COUNT
      delete from rpm_stage_item_loc
       where rowid = L_rowid_array(i).rowid;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      return FALSE;

END REMOVE_EXISTING;

--------------------------------------------------------

FUNCTION STAGE_ITEM_LOCS_TO_PROCESS(O_error_msg         IN OUT VARCHAR2,
                                    O_max_thread_number    OUT NUMBER,
                                    I_luw               IN     NUMBER,
                                    I_stage_status      IN     VARCHAR2,
                                    I_process_id        IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(50) 		:= 'RPM_NEW_ITEM_LOC_SQL.STAGE_ITEM_LOCS_TO_PROCESS';
   L_trace_name VARCHAR2(20) 	:= 'SILTP';
   L_start_time TIMESTAMP 		:= SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - I_process_id '|| I_process_id);
   
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to stage item locations to process');

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_NIL||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto STAGE_ITEM_LOCS_TO_PROCESS_1;

   END;
   <<STAGE_ITEM_LOCS_TO_PROCESS_1>>

   insert into rpm_stage_item_loc_clean (stage_item_loc_id,
                                         item,
                                         loc,
                                         loc_type,
                                         selling_unit_retail,
                                         selling_uom,
                                         dept,
                                         class,
                                         subclass,
                                         diff_1,
                                         diff_2,
                                         diff_3,
                                         diff_4,
                                         create_date,
                                         item_parent,
                                         zone_id,
                                         max_hier_level,
                                         thread_num,
                                         process_id)
      select stage_item_loc_id,
             item,
             loc,
             loc_type,
             selling_unit_retail,
             selling_uom,
             dept,
             class,
             subclass,
             diff_1,
             diff_2,
             diff_3,
             diff_4,
             SYSDATE,
             DECODE(item_parent,
                    item, NULL,
                    item_parent) item_parent,
             DECODE(zone_id,
                    -9999999999, NULL,
                    zone_id) zone_id,
             max_hier_level,
             thread_number,
             I_process_id
        from (select t.stage_item_loc_id,
                     t.item,
                     t.loc,
                     t.loc_type,
                     t.selling_unit_retail,
                     t.selling_uom,
                     NVL(t.item_parent, t.item) item_parent,
                     t.dept,
                     t.class,
                     t.subclass,
                     t.diff_1,
                     t.diff_2,
                     t.diff_3,
                     t.diff_4,
                     NVL(rzl.zone_id,-9999999999) zone_id,
                     DECODE(t.item_parent,
                            NULL, 'I',
                            'P')||
                     DECODE(rzl.zone_id,
                            NULL, 'L',
                            'Z') max_hier_level
                from (select rsil.stage_item_loc_id,
                             rsil.item,
                             rsil.loc,
                             rsil.loc_type,
                             rsil.selling_unit_retail,
                             rsil.selling_uom,
                             im.item_parent,
                             im.dept,
                             im.class,
                             im.subclass,
                             case
                                when im.item_parent is NULL and
                                     im.item_level = im.tran_level then
                                   NULL
                                else
                                   im.diff_1
                             end diff_1,
                             case
                                when im.item_parent is NULL and
                                     im.item_level = im.tran_level then
                                   NULL
                                else
                                   im.diff_2
                             end diff_2,
                             case
                                when im.item_parent is NULL and
                                     im.item_level = im.tran_level then
                                   NULL
                                else
                                   im.diff_3
                             end diff_3,
                             case
                                when im.item_parent is NULL and
                                     im.item_level = im.tran_level then
                                   NULL
                                else
                                   im.diff_4
                             end diff_4,
                             mrde.regular_zone_group
                        from rpm_stage_item_loc rsil,
                             item_master im,
                             rpm_merch_retail_def_expl mrde
                       where rsil.status     = I_stage_status
                         and rsil.item       = im.item
                         and rsil.process_id is NULL
                         and im.dept         = mrde.dept
                         and im.class        = mrde.class
                         and im.subclass     = mrde.subclass
                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                         and rownum          > 0) t,
                     (select rz.zone_group_id,
                             rzl.zone_id,
                             rzl.location
                        from rpm_zone rz,
                             rpm_zone_location rzl
                       where rzl.zone_id = rz.zone_id
                         and rownum      > 0) rzl
               where t.regular_zone_group  = rzl.zone_group_id(+)
                 and t.loc                 = rzl.location(+)) s
       model
          dimension by (ROW_NUMBER() OVER (ORDER BY s.item_parent,
                                                    s.zone_id,
                                                    s.item,
                                                    s.loc) rn)
          measures (SUM(1) OVER (PARTITION BY s.item_parent,
                                              s.zone_id) iloc_count,
                    s.stage_item_loc_id,
                    s.item,
                    s.loc,
                    s.loc_type,
                    s.selling_unit_retail,
                    s.selling_uom,
                    s.item_parent,
                    s.dept,
                    s.class,
                    s.subclass,
                    s.diff_1,
                    s.diff_2,
                    s.diff_3,
                    s.diff_4,
                    s.zone_id,
                    s.max_hier_level,
                    1 running_total,
                    1 thread_number)
          rules (running_total[rn] =
                    case
                       when CV(rn) = 1 then
                          iloc_count[CV(rn)]
                       else
                          case
                             when item_parent[CV(rn)] = item_parent[CV(rn) - 1] and
                                  zone_id[CV(rn)] = zone_id[CV(rn) - 1] then
                                running_total[CV(rn)-1]
                             else
                                case
                                   when iloc_count[CV(rn)] + running_total[CV(rn) - 1] > I_luw then
                                      iloc_count[CV(rn)]
                                   else
                                      iloc_count[CV(rn)] + running_total[CV(rn) - 1]
                                end
                          end
                    end,
                 thread_number[rn] =
                    case
                       when CV(rn) = 1 then
                          1
                       else
                          case
                             when item_parent[CV(rn)] = item_parent[CV(rn) - 1] and
                                  zone_id[CV(rn)] = zone_id[CV(rn) - 1] then
                                thread_number[CV(rn)-1]
                             else
                                case
                                   when (iloc_count[CV(rn)] + running_total[CV(rn) - 1]) > I_luw then
                                      thread_number[CV(rn)-1] + 1
                                   else
                                      thread_number[CV(rn)-1]
                                end
                          end
                    end);

   LOGGER.LOG_INFORMATION(L_program||' - I_process_id: '|| I_process_id ||
                                     ' Insert into rpm_stage_item_loc_clean - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   select MAX(thread_num)
     into O_max_thread_number
     from rpm_stage_item_loc_clean
    where process_id = I_process_id;

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id,
                   L_start_time);

	 --Instrumentation
   BEGIN
      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to stage item locations to process');

   EXCEPTION

      when OTHERS then
         goto STAGE_ITEM_LOCS_TO_PROCESS_2;

   END;

   <<STAGE_ITEM_LOCS_TO_PROCESS_2>>

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END STAGE_ITEM_LOCS_TO_PROCESS;

--------------------------------------------------------

FUNCTION SEED_FUTURE_RETAIL(O_error_msg         OUT VARCHAR2,
                            I_bulk_cc_pe_ids IN     OBJ_NUMERIC_ID_TABLE,
                            I_thread_number  IN     NUMBER,
                            I_process_id     IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_NEW_ITEM_LOC_SQL.SEED_FUTURE_RETAIL';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to seed future retail for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto SEED_FUTURE_RETAIL_1;

   END;

   <<SEED_FUTURE_RETAIL_1>>

   insert into gtt_num_num_str_str_date_date(number_1,
                                             varchar2_1,
                                             date_1)
      (select rpl.location location,
              rpi.item item,
              MIN(rpd.start_date)
         from table(cast(I_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
              rpm_promo_dtl rpd,
              rpm_bulk_cc_pe_item rpi,
              rpm_bulk_cc_pe_location rpl,
              rpm_bulk_cc_pe_thread thread
        where rpi.bulk_cc_pe_id       = VALUE(ids)
          and rpd.promo_dtl_id        = rpi.price_event_id
          and rpd.state               = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE
          and rpl.bulk_cc_pe_id       = rpi.bulk_cc_pe_id
          and rpl.price_event_id      = rpi.price_event_id
          and rpl.itemloc_id          = rpi.itemloc_id
          and rpi.bulk_cc_pe_id       = thread.bulk_cc_pe_id
          and rpi.price_event_id      = thread.price_event_id
          and thread.price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                          RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                          RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                          RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                          RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                          RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                          RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                          RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO)
     group by rpi.item,
              rpl.location);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into gtt_num_num_str_str_date_date for Promotion - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into gtt_num_num_str_str_date_date (number_1,
                                              varchar2_1,
                                              date_1)
      select rpl.location location,
             rpi.item item,
             rc.effective_date
        from table(cast(I_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_clearance rc,
             rpm_bulk_cc_pe_item rpi,
             rpm_bulk_cc_pe_location rpl,
             rpm_bulk_cc_pe_thread thread
       where rpi.bulk_cc_pe_id       = VALUE(ids)
         and rc.clearance_id         = rpi.price_event_id
         and rc.state                = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
         and rpl.bulk_cc_pe_id       = rpi.bulk_cc_pe_id
         and rpl.price_event_id      = rpi.price_event_id
         and rpl.itemloc_id          = rpi.itemloc_id
         and rpi.bulk_cc_pe_id       = thread.bulk_cc_pe_id
         and rpi.price_event_id      = thread.price_event_id
         and thread.price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
       group by rpi.item,
                rpl.location,
                rc.effective_date;

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into gtt_num_num_str_str_date_date for Clearance - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_future_retail (future_retail_id,
                                  item,
                                  dept,
                                  class,
                                  subclass,
                                  zone_node_type,
                                  location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  item_parent,
                                  diff_id,
                                  zone_id,
                                  max_hier_level,
                                  cur_hier_level,
                                  lock_version)
      select RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
             item,
             dept,
             class,
             subclass,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level,
             0 -- lock_version
        from (select /*+ ORDERED INDEX(iloc, PK_ITEM_LOC) */
                     distinct s.item,
                     s.dept,
                     s.class,
                     s.subclass,
                     loc.type zone_node_type,
                     s.loc location,
                     case
                        when gtt.date_1 is NOT NULL then
                           TRUNC(gtt.date_1 - 1)
                        else
                           LP_vdate - 1
                     end action_date,
                     s.selling_unit_retail selling_retail,
                     loc.currency_code selling_retail_currency,
                     s.selling_uom,
                     s.selling_unit_retail clear_retail,
                     loc.currency_code clear_retail_currency,
                     s.selling_uom clear_uom,
                     iloc.multi_units,
                     iloc.multi_unit_retail,
                     loc.currency_code multi_unit_retail_currency,
                     iloc.multi_selling_uom,
                     s.selling_unit_retail simple_promo_retail,
                     loc.currency_code simple_promo_retail_currency,
                     s.selling_uom simple_promo_uom,
                     s.item_parent,
                     s.diff_1 diff_id,
                     s.zone_id,
                     s.max_hier_level,
                     RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level
                from table(cast(I_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_bulk_cc_pe_item rpi,
                     rpm_bulk_cc_pe_location rpl,
                     rpm_stage_item_loc_clean s,
                     (select store location,
                             currency_code,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE type
                        from store
                      union all
                      select wh location,
                             currency_code,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE type
                        from wh) loc,
                     item_loc iloc,
                     (select varchar2_1,
                             number_1,
                             MIN(date_1) date_1
                        from gtt_num_num_str_str_date_date
                       group by varchar2_1,
                                number_1) gtt
               where rpi.bulk_cc_pe_id  = VALUE(ids)
                 and s.thread_num       = I_thread_number
                 and s.process_id       = I_process_id
                 and s.loc              = loc.location
                 and s.item             = rpi.item
                 and iloc.item          = s.item
                 and iloc.loc           = s.loc
                 and s.loc              = rpl.location
                 and s.item             = gtt.varchar2_1 (+)
                 and s.loc              = gtt.number_1 (+)
                 and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                 and rpl.price_event_id = rpi.price_event_id
                 and rpl.itemloc_id     = rpi.itemloc_id) t
       where NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept        = t.dept
                            and rfr.item        = t.item
                            and rfr.location    = t.location
                            and rfr.action_date BETWEEN TRUNC(t.action_date) and TO_DATE(TO_CHAR(t.action_date, 'YYYYMMDD') || '235959', 'YYYYMMDDHH24MISS'));

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_future_retail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rpm_item_loc target
   using (select /*+ ORDERED */
                 distinct s.dept,
                 s.item,
                 s.loc
            from (select /*+ ORDERED */
                         distinct rpi.item,
                         rpl.location
                    from table(cast(I_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                         rpm_bulk_cc_pe_item rpi,
                         rpm_bulk_cc_pe_location rpl
                   where rpi.bulk_cc_pe_id  = VALUE(ids)
                     and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                     and rpl.price_event_id = rpi.price_event_id
                     and rpl.itemloc_id     = rpi.itemloc_id
                     and rownum             > 0) rp,
                 rpm_stage_item_loc_clean s
           where s.thread_num = I_thread_number
             and s.process_id = I_process_id
             and s.item       = rp.item
             and s.loc        = rp.location) source
   on (    target.dept = source.dept
       and target.item = source.item
       and target.loc  = source.loc)
   when NOT MATCHED then
      insert(dept,
             item,
             loc)
         values(source.dept,
                source.item,
                source.loc);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Merge into rpm_item_loc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number ,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to seed future retail for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SEED_FUTURE_RETAIL_2;

   END;

   <<SEED_FUTURE_RETAIL_2>>

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEED_FUTURE_RETAIL;

--------------------------------------------------------

FUNCTION SEED_FUTURE_RETAIL_NO_PE(O_error_msg         OUT VARCHAR2,
                                  I_chunk_bulk_ids IN     OBJ_NUM_NUM_STR_TBL,
                                  I_thread_number  IN     NUMBER,
                                  I_process_id     IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program 		VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.SEED_FUTURE_RETAIL_NO_PE';

   L_start_time TIMESTAMP 	 := SYSTIMESTAMP;

BEGIN

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to seed future retail with no PE for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SEED_FUTURE_RETAIL_NO_PE_1;

   END;

   <<SEED_FUTURE_RETAIL_NO_PE_1>>
   
   -- Find all the item/locs that have higher level timelines already on RFR, but that do
   -- not have any price events impacting them yet.  Mark those item/locs as not needing
   -- to have a new timeline created for them as it might not rollup even though both the
   -- existing higher level timeline and the potential new timeline would both only have
   -- seed data in place and nothing else.

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);

   merge into rpm_stage_item_loc_clean target
   using (with rsilc_temp as (select stage_item_loc_id,
                                     dept,
                                     item,
                                     item_parent,
                                     zone_id,
                                     loc,
                                     loc_type,
                                     max_hier_level,
                                     diff_1,
                                     thread_num,
                                     process_id
                                from rpm_stage_item_loc_clean s
                               where s.thread_num = I_thread_number
                                 and s.process_id = I_process_id
                                 and NOT EXISTS   (select /*+ CARDINALITY (ids, 10) */
                                                          'x'
                                                     from table(cast(I_chunk_bulk_ids as OBJ_NUM_NUM_STR_TBL)) ids,
                                                          rpm_bulk_cc_pe_item rpi,
                                                          rpm_bulk_cc_pe_location rpl
                                                    where rpi.bulk_cc_pe_id  = ids.number_2
                                                      and rpl.bulk_cc_pe_id  = ids.number_2
                                                      and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                                                      and rpl.price_event_id = rpi.price_event_id
                                                      and rpl.itemloc_id     = rpi.itemloc_id
                                                      and rpi.item           = s.item
                                                      and rpl.location       = s.loc))
         (select /*+ ORDERED INDEX_ASC(rfr, RPM_FUTURE_RETAIL_I1) */
                 silc.stage_item_loc_id,
                 COUNT(1) OVER (PARTITION BY silc.item,
                                             silc.loc) cnt,
                 FIRST_VALUE (rfr.selling_retail) OVER (PARTITION BY silc.item,
                                                                     silc.loc
                                                            ORDER BY rfr.action_date) seed_retail
            from rsilc_temp silc,
                 rpm_future_retail rfr
           where silc.max_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
             and silc.dept           = rfr.dept
             and silc.item           = rfr.item
             and silc.zone_id        = rfr.location
             and rfr.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rfr.cur_hier_level  = silc.max_hier_level
             and rfr.max_hier_level  = rfr.cur_hier_level
          union all
          -- NIL max hier is parent/loc
          select /*+ ORDERED INDEX_ASC(rfr, RPM_FUTURE_RETAIL_I1) */
                 silc.stage_item_loc_id,
                 COUNT(1) OVER (PARTITION BY silc.item,
                                             silc.loc) cnt,
                 FIRST_VALUE (rfr.selling_retail) OVER (PARTITION BY silc.item,
                                                                     silc.loc
                                                            ORDER BY rfr.action_date) seed_retail
            from rsilc_temp silc,
                 rpm_future_retail rfr
           where silc.max_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
             and silc.dept           = rfr.dept
             and silc.item_parent    = rfr.item
             and silc.loc            = rfr.location
             and rfr.zone_node_type  = DECODE(silc.loc_type,
                                              RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
             and rfr.cur_hier_level  = silc.max_hier_level
             and rfr.max_hier_level  = rfr.cur_hier_level
             -- if there is a timeline below the max level, there are price events at the level of that timelines.
             -- check at the Diff/Loc level
             and NOT EXISTS (select 'x'
                               from rpm_future_retail rfr2
                              where rfr2.dept                 = silc.dept
                                and rfr2.item                 = silc.item_parent
                                and NVL(rfr2.diff_id, '-999') = NVL(silc.diff_1, '-999')
                                and rfr2.location             = silc.loc
                                and rfr2.zone_node_type       = DECODE(silc.loc_type,
                                                                       RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                       RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rfr2.cur_hier_level       = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                                and rfr2.max_hier_level       = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                and rownum                    = 1)
          union all
          -- NIL max hier is parent/zone
          select /*+ ORDERED INDEX_ASC(rfr, RPM_FUTURE_RETAIL_I1) */
                 silc.stage_item_loc_id,
                 COUNT(1) OVER (PARTITION BY silc.item,
                                             silc.loc) cnt,
                 FIRST_VALUE (rfr.selling_retail) OVER (PARTITION BY silc.item,
                                                                     silc.loc
                                                            ORDER BY rfr.action_date) seed_retail
            from rsilc_temp silc,
                 rpm_future_retail rfr
           where silc.max_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
             and silc.dept           = rfr.dept
             and silc.item_parent    = rfr.item
             and silc.zone_id        = rfr.location
             and rfr.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
             and rfr.cur_hier_level  = silc.max_hier_level
             and rfr.max_hier_level  = rfr.cur_hier_level
             -- if there is a timeline below the max level, there are price events at the level of that timelines.
             -- check at the Diff/Zone
             and NOT EXISTS (select /*+ INDEX_ASC(rfr2, RPM_FUTURE_RETAIL_I1) */ 'x'
                               from rpm_future_retail rfr2
                              where rfr2.dept                 = silc.dept
                                and rfr2.item                 = silc.item_parent
                                and NVL(rfr2.diff_id, '-999') = NVL(silc.diff_1, '-999')
                                and rfr2.location             = silc.zone_id
                                and rfr2.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rfr2.cur_hier_level       = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                                and rfr2.max_hier_level       = silc.max_hier_level
                                and rownum                    = 1)
             -- check at the Parent/Loc level and Diff/Loc
             and NOT EXISTS (select /*+ INDEX_ASC(rfr2, RPM_FUTURE_RETAIL_I1) */ 'x'
                               from rpm_future_retail rfr3
                              where rfr3.dept                    = silc.dept
                                and rfr3.item                    = silc.item_parent
                                and rfr3.location                = silc.loc
                                and rfr3.zone_node_type          = DECODE(silc.loc_type,
                                                                          RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                          RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and (   (    rfr3.diff_id        is NULL
                                         and rfr3.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC)
                                     or (    rfr3.diff_id        = silc.diff_1
                                         and rfr3.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC))
                                and rfr3.max_hier_level          = silc.max_hier_level
                                and rownum                       = 1)
             -- check at the Item/Zone level
             and NOT EXISTS (select /*+ INDEX_ASC(rfr2, RPM_FUTURE_RETAIL_I1) */ 'x'
                               from rpm_future_retail rfr4
                              where rfr4.dept           = silc.dept
                                and rfr4.item           = silc.item
                                and rfr4.location       = silc.zone_id
                                and rfr4.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rfr4.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                and rfr4.max_hier_level = silc.max_hier_level
                                and rownum              = 1))) source
   on (    target.thread_num        = I_thread_number
       and target.process_id        = I_process_id
       and source.stage_item_loc_id = target.stage_item_loc_id
       and source.cnt               = 1
       and source.seed_retail       = target.selling_unit_retail)
   -- if there is only one record, it's just the seed record and if the retail is the same there's no need to create a new timeline
   when MATCHED then
      update
         set max_hier_level = NULL;

   LOGGER.LOG_INFORMATION(L_program||' - I_process_id: '|| I_process_id ||
                                     ' - I_thread_number: '|| I_thread_number ||
                                     ' - Merge into rpm_stage_item_loc_clean - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_future_retail (future_retail_id,
                                  item,
                                  dept,
                                  class,
                                  subclass,
                                  zone_node_type,
                                  location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  item_parent,
                                  diff_id,
                                  zone_id,
                                  max_hier_level,
                                  cur_hier_level,
                                  lock_version)
      select /*+ INDEX(ILOC, PK_ITEM_LOC) */
             RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
             s.item,
             s.dept,
             s.class,
             s.subclass,
             loc.type,
             s.loc,
             LP_vdate - 1,
             s.selling_unit_retail,
             loc.currency_code,
             s.selling_uom,
             iloc.multi_units,
             iloc.multi_unit_retail,
             loc.currency_code,
             iloc.multi_selling_uom,
             s.selling_unit_retail,
             loc.currency_code,
             s.selling_uom,
             s.selling_unit_retail,
             loc.currency_code,
             s.selling_uom,
             s.item_parent,
             s.diff_1,
             s.zone_id,
             s.max_hier_level,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
             0
        from rpm_stage_item_loc_clean s,
             (select store location,
                     currency_code,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE type
                from store
              union all
              select wh location,
                     currency_code,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE type
                from wh) loc,
             item_loc iloc
       where s.thread_num     = I_thread_number
         and s.process_id     = I_process_id
         and s.max_hier_level is NOT NULL
         and s.loc            = loc.location
         and iloc.item        = s.item
         and iloc.loc         = s.loc
         and NOT EXISTS (select /*+ CARDINALITY (ids, 10) */
                                'x'
                           from table(cast(I_chunk_bulk_ids as OBJ_NUM_NUM_STR_TBL)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id  = ids.number_2
                            and rpl.bulk_cc_pe_id  = ids.number_2
                            and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id = rpi.price_event_id
                            and rpl.itemloc_id     = rpi.itemloc_id
                            and rpi.item           = s.item
                            and rpl.location       = s.loc)
         and NOT EXISTS (select 1
                           from rpm_future_retail rfr
                          where rfr.dept               = s.dept
                            and rfr.item               = s.item
                            and rfr.location           = s.loc
                            and TRUNC(rfr.action_date) = (LP_vdate - 1));

   LOGGER.LOG_INFORMATION(L_program||' - I_process_id: '|| I_process_id ||
                                     ' - I_thread_number: '|| I_thread_number ||
                                     ' - Merge into rpm_future_retail - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   merge into rpm_item_loc target
   using (select distinct s.dept,
                 s.item,
                 s.loc
            from rpm_stage_item_loc_clean s
           where s.thread_num = I_thread_number
             and s.process_id = I_process_id
             and NOT EXISTS (select 'x'
                               from (select distinct number_2 id
                                       from table(cast(I_chunk_bulk_ids as OBJ_NUM_NUM_STR_TBL))) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl
                              where rpi.bulk_cc_pe_id  = ids.id
                                and rpl.bulk_cc_pe_id  = ids.id
                                and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id = rpi.price_event_id
                                and rpl.itemloc_id     = rpi.itemloc_id
                                and rpi.item           = s.item
                                and rpl.location       = s.loc)) source
   on (    target.dept = source.dept
       and target.item = source.item
       and target.loc  = source.loc)
   when NOT MATCHED then
      insert (dept,
              item,
              loc)
         values (source.dept,
                 source.item,
                 source.loc);

   LOGGER.LOG_INFORMATION(L_program||' - I_process_id: '|| I_process_id ||
                                     ' - I_thread_number: '|| I_thread_number ||
                                     ' - Merge into rpm_item_loc - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to seed future retail with no PE for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SEED_FUTURE_RETAIL_NO_PE_2;

   END;

   <<SEED_FUTURE_RETAIL_NO_PE_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SEED_FUTURE_RETAIL_NO_PE;

--------------------------------------------------------

FUNCTION MARK_LOCATION_MOVE(O_error_msg         OUT VARCHAR2,
                            I_bulk_cc_pe_ids IN     OBJ_NUMERIC_ID_TABLE,
                            I_thread_number  IN     NUMBER,
                            I_process_id     IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(45) := 'RPM_NEW_ITEM_LOC_SQL.MARK_LOCATION_MOVE';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to mark location move for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto MARK_LOCATION_MOVE_1;

   END;

   <<MARK_LOCATION_MOVE_1>>
   
   insert into rpm_future_retail (future_retail_id,
                                  item,
                                  dept,
                                  class,
                                  subclass,
                                  zone_node_type,
                                  location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  diff_id,
                                  item_parent,
                                  zone_id,
                                  cur_hier_level,
                                  max_hier_level)
      select RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
             item,
             dept,
             class,
             subclass,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             0, --lock_version
             diff_id,
             item_parent,
             zone_id,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC, --cur_hier_level
             max_hier_level
        from (select /*+ ORDERED */
                     distinct s.item,
                     s.dept,
                     s.class,
                     s.subclass,
                     loc.type              zone_node_type,
                     s.loc                 location,
                     lm.effective_date     action_date,
                     s.selling_unit_retail selling_retail,
                     loc.currency_code     selling_retail_currency,
                     s.selling_uom,
                     s.selling_unit_retail clear_retail,
                     loc.currency_code     clear_retail_currency,
                     s.selling_uom         clear_uom,
                     s.selling_unit_retail simple_promo_retail,
                     loc.currency_code     simple_promo_retail_currency,
                     s.selling_uom         simple_promo_uom,
                     lm.old_zone_id        loc_move_from_zone_id,
                     lm.new_zone_id        loc_move_to_zone_id,
                     lm.location_move_id,
                     s.diff_1              diff_id,
                     s.item_parent,
                     s.zone_id,
                     s.max_hier_level
                from rpm_location_move lm,
                     (select store location,
                             currency_code,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_STORE type
                        from store
                      union all
                      select wh location,
                             currency_code,
                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE type
                        from wh) loc,
                     rpm_stage_item_loc_clean s,
                     rpm_bulk_cc_pe_location rpl,
                     rpm_bulk_cc_pe_item rpi,
                     table(cast(I_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids
               where s.thread_num       = I_thread_number
                 and s.process_id       = I_process_id
                 and s.loc              = loc.location
                 and s.loc              = lm.location
                 and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                 and s.item             = rpi.item
                 and s.loc              = rpl.location
                 and rpi.bulk_cc_pe_id  = VALUE(ids)
                 and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                 and rpl.price_event_id = rpi.price_event_id
                 and rpl.itemloc_id     = rpi.itemloc_id);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to mark location move for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto MARK_LOCATION_MOVE_2;

   END;

   <<MARK_LOCATION_MOVE_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END MARK_LOCATION_MOVE;

--------------------------------------------------------

FUNCTION MARK_LOCATION_MOVE_NO_PE(O_error_msg      IN OUT VARCHAR2,
                                  I_chunk_bulk_ids IN     OBJ_NUM_NUM_STR_TBL,
                                  I_thread_number  IN     NUMBER,
                                  I_process_id     IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.MARK_LOCATION_MOVE_NO_PE';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

   BEGIN

      LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                         ' - I_thread_number: '|| I_thread_number);
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to mark location move with no PE for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto MARK_LOCATION_MOVE_NO_PE_1;

   END;

   <<MARK_LOCATION_MOVE_NO_PE_1>>

      insert into rpm_future_retail (future_retail_id,
                                  item,
                                  dept,
                                  class,
                                  subclass,
                                  zone_node_type,
                                  location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  diff_id,
                                  item_parent,
                                  zone_id,
                                  cur_hier_level,
                                  max_hier_level)
      select RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
             s.item,
             s.dept,
             s.class,
             s.subclass,
             loc.type,
             s.loc,
             lm.effective_date,
             s.selling_unit_retail,
             loc.currency_code,
             s.selling_uom,
             s.selling_unit_retail,
             loc.currency_code,
             s.selling_uom,
             s.selling_unit_retail,
             loc.currency_code,
             s.selling_uom,
             lm.old_zone_id,
             lm.new_zone_id,
             lm.location_move_id,
             0,
             s.diff_1,
             s.item_parent,
             s.zone_id,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
             s.max_hier_level
        from rpm_stage_item_loc_clean s,
             (select store as location,
                     currency_code,
                     0 as type
                from store
              union all
              select wh as location,
                     currency_code,
                     2 as type
                from wh) loc,
             rpm_location_move lm
       where s.thread_num = I_thread_number
         and s.process_id = I_process_id
         and s.loc        = loc.location
         and s.loc        = lm.location
         and lm.state     = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
         and NOT EXISTS (select 'x'
                           from table(cast(I_chunk_bulk_ids as OBJ_NUM_NUM_STR_TBL)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id  = ids.number_2
                            and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id = rpi.price_event_id
                            and rpl.itemloc_id     = rpi.itemloc_id
                            and rpi.item           = s.item
                            and rpl.location       = s.loc);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to mark location move with no PE for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto MARK_LOCATION_MOVE_NO_PE_2;

   END;

   <<MARK_LOCATION_MOVE_NO_PE_2>>

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END MARK_LOCATION_MOVE_NO_PE;

--------------------------------------------------------

FUNCTION SETUP_PRICE_EVENT_INFO(O_error_msg                 OUT  VARCHAR2,
                                I_thread_processor_class IN      RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                                I_thread_number          IN      VARCHAR2,
                                I_process_id             IN      VARCHAR2)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(45) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_PRICE_EVENT_INFO';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up price event information');

   EXCEPTION

      when OTHERS then
         goto SETUP_PRICE_EVENT_INFO_1;

   END;

   <<SETUP_PRICE_EVENT_INFO_1>>
   
   insert all
      into rpm_bulk_cc_pe (bulk_cc_pe_id,
                           price_event_type,
                           persist_ind,
                           start_state,
                           end_state,
                           user_name,
                           emergency_perm,
                           secondary_ind,
                           asynch_ind,
                           cc_request_group_id,
                           auto_clean_ind,
                           thread_processor_class,
                           status,
                           need_il_explode_ind,
                           non_asynch_single_txn_ind)
                   values (bulk_cc_pe_id,
                           price_event_type,
                           persist_ind,
                           start_state,
                           end_state,
                           user_name,
                           emergency_perm,
                           secondary_ind,
                           asynch_ind,
                           cc_request_group_id,
                           auto_clean_ind,
                           I_thread_processor_class,
                           status,
                           need_il_explode_ind,
                           non_asynch_single_txn_ind)
      into rpm_nil_bulkccpe_process_id (bulk_cc_pe_id,
                                        process_id,
                                        thread_num)
                                values (bulk_cc_pe_id,
                                        I_process_id,
                                        I_thread_number)
      select distinct gtt.number_2  bulk_cc_pe_id,
             gtt.varchar2_1         price_event_type,
             'Y'                    persist_ind,
             gtt.varchar2_2         start_state,
             gtt.varchar2_2         end_state,
             'NewItemLocationBatch' user_name,
             1                      emergency_perm,
             0                      secondary_ind,
             0                      asynch_ind,
             0                      cc_request_group_id,
             0                      auto_clean_ind,
             'I'                    status,
             0                      need_il_explode_ind,
             1                      non_asynch_single_txn_ind
        from gtt_num_num_str_str_date_date gtt,
             rpm_bulk_cc_pe_item il
       where gtt.number_2   = il.bulk_cc_pe_id
         and gtt.varchar2_1 NOT IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - Insert into rpm_bulk_cc_pe and rpm_nil_bulkccpe_process_id  - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   insert into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                      price_event_id,
                                      price_event_type,
                                      rank)
      select distinct bulk_cc_pe_id,
             price_event_id,
             gtt.varchar2_1, -- price_event_type,
             0 -- rank
        from rpm_bulk_cc_pe_item,
             gtt_num_num_str_str_date_date gtt
       where bulk_cc_pe_id  = gtt.number_2
         and gtt.varchar2_1 NOT IN (RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                    RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - Insert into rpm_bulk_cc_pe_thread - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up price event information');

   EXCEPTION

      when OTHERS then
         goto SETUP_PRICE_EVENT_INFO_2;

   END;

   <<SETUP_PRICE_EVENT_INFO_2>>   
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_PRICE_EVENT_INFO;

--------------------------------------------------------

FUNCTION COUNT_ITEM_LOC_ROWS(O_error_msg        OUT  VARCHAR2,
                             O_count            OUT  NUMBER,
                             I_bulk_cc_pe_id IN      rpm_bulk_cc_pe_thread.bulk_cc_pe_id%TYPE)
RETURN BOOLEAN IS

   L_count NUMBER := 0;

   cursor C_ROWS is
      select count(*)
        from rpm_bulk_cc_pe_item rpi,
             rpm_bulk_cc_pe_location rpl
       where rpi.bulk_cc_pe_id = I_bulk_cc_pe_id
         and rpl.bulk_cc_pe_id = rpi.bulk_cc_pe_id
         and rpl.price_event_id = rpi.price_event_id
         and rpl.itemloc_id = rpi.itemloc_id;

BEGIN

   open C_ROWS;
   fetch C_ROWS into O_count;
   close C_ROWS;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_NEW_ITEM_LOC_SQL.COUNT_ITEM_LOC_ROWS',
                                        to_char(SQLCODE));
      return FALSE;
END COUNT_ITEM_LOC_ROWS;

--------------------------------------------------------

FUNCTION SETUP_PRICE_CHANGES(O_error_msg        OUT VARCHAR2,
                             I_thread_number IN     NUMBER,
                             I_process_id    IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program 	VARCHAR(45) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_PRICE_CHANGES';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up price changes for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_PRICE_CHANGES_1;

   END;

   <<SETUP_PRICE_CHANGES_1>>

   insert all
      into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                price_event_id,
                                itemloc_id,
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number)
                        values (bulk_cc_pe_id,
                                price_event_id,
                                RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(1),
                                dept,
                                class,
                                subclass,
                                item,
                                diff_id,
                                item_parent,
                                merch_level_type,
                                thread_number)
      into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                    price_event_id,
                                    itemloc_id,
                                    location,
                                    zone_node_type,
                                    zone_id)
                            values (bulk_cc_pe_id,
                                    price_event_id,
                                    RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(0),
                                    location,
                                    zone_node_type,
                                    zone_id)
      with item_loc_clean as
         (select /*+ ORDERED CARDINALITY(gtt, 10) USE_HASH(gtt, s)  */
                 gtt.number_2 bulk_cc_pe_id,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item,
                 s.item_parent,
                 s.diff_1,
                 s.diff_2,
                 s.diff_3,
                 s.diff_4,
                 s.loc,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id
            from gtt_num_num_str_str_date_date gtt,
                 rpm_stage_item_loc_clean s
           where gtt.varchar2_1 = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
             and s.thread_num   = I_thread_number
             and s.process_id   = I_process_id
             and s.thread_num   = gtt.number_1)
         -- Item Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and s.item            = pc.item
            and (   pc.location =  s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date)
         union all
         -- Item Parent Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and s.item_parent     = pc.item
            and pc.diff_id        is NULL
            and (   pc.location = s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date)
         union all
         -- Item Parent Diff Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and s.item_parent     = pc.item
            and pc.diff_id        is NOT NULL
            and pc.diff_id        IN (s.diff_1,
                                      s.diff_2,
                                      s.diff_3,
                                      s.diff_4)
            and (   pc.location = s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date)
         union all
         -- Price Change Skulist Item Parent Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and (    pc.skulist is NOT NULL
                 and EXISTS (select 1
                               from rpm_price_change_skulist rpcs
                              where rpcs.price_event_id = pc.price_change_id
                                and rpcs.skulist        = pc.skulist
                                and rpcs.item           = s.item_parent))
            and (   pc.location = s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date)
         union all
         -- Price Change Skulist Item Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and (    pc.skulist is NOT NULL
                 and EXISTS (select 1
                               from rpm_price_change_skulist rpcs
                              where rpcs.price_event_id = pc.price_change_id
                                and rpcs.skulist        = pc.skulist
                                and rpcs.item           = s.item))
            and (   pc.location = s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date)
         union all
         -- Price Event ItemList Item Parent Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and (    pc.price_event_itemlist is NOT NULL
                  and EXISTS (select 1
                                from rpm_merch_list_detail rmld
                               where rmld.merch_list_id = pc.price_event_itemlist
                                 and rmld.merch_level   = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                 and rmld.item          = s.item_parent))
            and (   pc.location = s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date)
         union all
         -- Price Event ItemList Item Level
         select s.bulk_cc_pe_id,
                pc.price_change_id price_event_id,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_1 diff_id,
                s.loc location,
                s.zone_node_type,
                s.zone_id,
                RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
                1 thread_number
           from item_loc_clean s,
                rpm_price_change pc
          where pc.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
            and pc.effective_date > LP_vdate
            and (    pc.price_event_itemlist is NOT NULL
                 and EXISTS (select 1
                               from rpm_merch_list_detail rmld
                              where rmld.merch_list_id = pc.price_event_itemlist
                                and rmld.merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rmld.item          = s.item))
            and (   pc.location = s.loc
                 or EXISTS (select 'x'
                              from rpm_zone_location zl
                             where zl.location = s.loc
                               and pc.zone_id  = zl.zone_id
                               and rownum      = 1)
                 or pc.zone_id IN (select lm.new_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date))
            and pc.zone_id NOT IN (select lm.old_zone_id
                                     from rpm_location_move lm
                                    where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                      and lm.location        = s.loc
                                      and lm.effective_date <= pc.effective_date);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - Insert into rpm_bulk_cc_pe_item and location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up price changes for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_PRICE_CHANGES_2;

   END;

   <<SETUP_PRICE_CHANGES_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_PRICE_CHANGES;
--------------------------------------------------------

FUNCTION SETUP_CLEARANCES(O_error_msg        OUT VARCHAR2,
                          I_thread_number IN     NUMBER,
                          I_process_id    IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_CLEARANCES';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up clearances for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto SETUP_CLEARANCES_1;

   END;

   <<SETUP_CLEARANCES_1>>
   
   insert all
   into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                             price_event_id,
                             itemloc_id,
                             dept,
                             class,
                             subclass,
                             item,
                             diff_id,
                             item_parent,
                             merch_level_type,
                             thread_number)
                     values (bulk_cc_pe_id,
                             price_event_id,
                             RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(1),
                             dept,
                             class,
                             subclass,
                             item,
                             diff_id,
                             item_parent,
                             merch_level_type,
                             thread_number)
   into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                 price_event_id,
                                 itemloc_id,
                                 location,
                                 zone_node_type,
                                 zone_id)
                         values (bulk_cc_pe_id,
                                 price_event_id,
                                 RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(0),
                                 location,
                                 zone_node_type,
                                 zone_id)
      select distinct t.bulk_cc_pe_id,
             t.price_event_id,
             t.dept,
             t.class,
             t.subclass,
             t.item,
             t.item_parent,
             t.diff_id,
             t.location,
             t.zone_node_type,
             t.zone_id,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             1 thread_number
        from (-- Item Level
              select gtt.number_2 bulk_cc_pe_id,
                     cl.clearance_id price_event_id,
                     s.dept,
                     s.class,
                     s.subclass,
                     s.item,
                     s.item_parent,
                     s.diff_1 diff_id,
                     s.loc location,
                     DECODE(s.loc_type,
                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                            RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                     s.zone_id,
                     1 thread_number
                from gtt_num_num_str_str_date_date gtt,
                     rpm_stage_item_loc_clean s,
                     rpm_clearance cl
               where gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and gtt.varchar2_2    = cl.state
                 and s.thread_num      = I_thread_number
                 and s.process_id      = I_process_id
                 and s.thread_num      = gtt.number_1
                 and cl.item           = s.item
                 and cl.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and cl.effective_date > LP_vdate
                 and (   cl.location   = s.loc
                      or cl.zone_id    IN (select zl.zone_id
                                             from rpm_zone_location zl
                                            where zl.location = s.loc)
                      or cl.zone_id    IN (select lm.new_zone_id
                                             from rpm_location_move lm
                                            where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                              and lm.location        = s.loc
                                              and lm.effective_date <= cl.effective_date))
                 and NOT EXISTS (select 1
                                   from rpm_location_move lm
                                  where lm.old_zone_id     = cl.zone_id
                                    and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                    and lm.location        = s.loc
                                    and lm.effective_date <= cl.effective_date)
             union all
             -- Item Parent Level
             select gtt.number_2 bulk_cc_pe_id,
                     cl.clearance_id price_event_id,
                     s.dept,
                     s.class,
                     s.subclass,
                     s.item,
                     s.item_parent,
                     s.diff_1 diff_id,
                     s.loc location,
                     DECODE(s.loc_type,
                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                            RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                     s.zone_id,
                     1 thread_number
                from gtt_num_num_str_str_date_date gtt,
                     rpm_stage_item_loc_clean s,
                     rpm_clearance cl
               where gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and gtt.varchar2_2    = cl.state
                 and s.thread_num      = I_thread_number
                 and s.process_id      = I_process_id
                 and s.thread_num      = gtt.number_1
                 and cl.item           = s.item_parent
                 and cl.diff_id        is NULL
                 and cl.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and cl.effective_date > LP_vdate
                 and (   cl.location   = s.loc
                      or cl.zone_id    IN (select zl.zone_id
                                             from rpm_zone_location zl
                                            where zl.location = s.loc)
                      or cl.zone_id    IN (select lm.new_zone_id
                                             from rpm_location_move lm
                                            where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                              and lm.location        = s.loc
                                              and lm.effective_date <= cl.effective_date))
                 and NOT EXISTS (select 1
                                   from rpm_location_move lm
                                  where lm.old_zone_id     = cl.zone_id
                                    and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                    and lm.location        = s.loc
                                    and lm.effective_date <= cl.effective_date)
             union all
             -- Item parent Diff Level
             select gtt.number_2 bulk_cc_pe_id,
                     cl.clearance_id price_event_id,
                     s.dept,
                     s.class,
                     s.subclass,
                     s.item,
                     s.item_parent,
                     s.diff_1 diff_id,
                     s.loc location,
                     DECODE(s.loc_type,
                            RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                            RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                     s.zone_id,
                     1 thread_number
                from gtt_num_num_str_str_date_date gtt,
                     rpm_stage_item_loc_clean s,
                     rpm_clearance cl
               where gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                 and gtt.varchar2_2    = cl.state
                 and s.thread_num      = I_thread_number
                 and s.process_id      = I_process_id
                 and s.thread_num      = gtt.number_1
                 and cl.item           = s.item_parent
                 and cl.diff_id        is NOT NULL
                 and cl.diff_id        IN (s.diff_1,
                                           s.diff_2,
                                           s.diff_3,
                                           s.diff_4)
                 and cl.state          = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                 and cl.effective_date > LP_vdate
                 and (   cl.location   = s.loc
                      or cl.zone_id    IN (select zl.zone_id
                                             from rpm_zone_location zl
                                            where zl.location = s.loc)
                      or cl.zone_id    IN (select lm.new_zone_id
                                             from rpm_location_move lm
                                            where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                              and lm.location        = s.loc
                                              and lm.effective_date <= cl.effective_date))
                 and NOT EXISTS (select 1
                                   from rpm_location_move lm
                                  where lm.old_zone_id     = cl.zone_id
                                    and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                    and lm.location        = s.loc
                                    and lm.effective_date <= cl.effective_date)
             union all
             -- Clearance Skulist
             select gtt.number_2 bulk_cc_pe_id,
                    cl.clearance_id price_event_id,
                    s.dept,
                    s.class,
                    s.subclass,
                    s.item,
                    s.item_parent,
                    s.diff_1 diff_id,
                    s.loc location,
                    DECODE(s.loc_type,
                           RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                           RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                    s.zone_id,
                    1 thread_number
               from gtt_num_num_str_str_date_date gtt,
                    rpm_stage_item_loc_clean s,
                    (select cl.clearance_id,
                            cl.location,
                            cl.zone_id,
                            cl.effective_date,
                            cl.state,
                            rcs.item
                       from rpm_clearance cl,
                            rpm_clearance_skulist rcs
                      where cl.state           = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                        and cl.effective_date  > LP_vdate
                        and cl.skulist         is NOT NULL
                        and rcs.price_event_id = cl.clearance_id
                        and rcs.skulist        = cl.skulist
                        and rownum             > 0) cl
              where gtt.varchar2_1  = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                and gtt.varchar2_2  = cl.state
                and s.thread_num    = I_thread_number
                and s.process_id    = I_process_id
                and s.thread_num    = gtt.number_1
                and (   cl.item     = s.item_parent
                     or cl.item     = s.item)
                and (   cl.location = s.loc
                     or cl.zone_id  IN (select zl.zone_id
                                          from rpm_zone_location zl
                                         where zl.location = s.loc)
                     or cl.zone_id  IN (select lm.new_zone_id
                                          from rpm_location_move lm
                                         where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                           and lm.location        = s.loc
                                           and lm.effective_date <= cl.effective_date))
                and NOT EXISTS (select 1
                                  from rpm_location_move lm
                                 where lm.old_zone_id     = cl.zone_id
                                   and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                   and lm.location        = s.loc
                                   and lm.effective_date <= cl.effective_date)
             union all
             -- Price Event ItemList
             select gtt.number_2 bulk_cc_pe_id,
                    cl.clearance_id price_event_id,
                    s.dept,
                    s.class,
                    s.subclass,
                    s.item,
                    s.item_parent,
                    s.diff_1 diff_id,
                    s.loc location,
                    DECODE(s.loc_type,
                           RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                           RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                    s.zone_id,
                    1 thread_number
               from gtt_num_num_str_str_date_date gtt,
                    rpm_stage_item_loc_clean s,
                    (select cl.clearance_id,
                            cl.location,
                            cl.zone_id,
                            cl.effective_date,
                            cl.state,
                            rmld.item,
                            rmld.merch_level
                       from rpm_clearance cl,
                            rpm_merch_list_detail rmld
                      where cl.state                  = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
                        and cl.effective_date         > LP_vdate
                        and cl.price_event_itemlist   is NOT NULL
                        and cl.price_event_itemlist   = rmld.merch_list_id
                        and rownum                    > 0) cl
              where gtt.varchar2_1 = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                and gtt.varchar2_2 = cl.state
                and s.thread_num   = I_thread_number
                and s.process_id   = I_process_id
                and s.thread_num   = gtt.number_1
                and (   (    cl.merch_level = RPM_CONSTANTS.PARENT_MERCH_TYPE
                         and cl.item        = s.item_parent)
                     or (    cl.merch_level = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and cl.item        = s.item))
                and (   cl.location = s.loc
                     or cl.zone_id IN (select zl.zone_id
                                         from rpm_zone_location zl
                                        where zl.location = s.loc)
                     or cl.zone_id IN (select lm.new_zone_id
                                         from rpm_location_move lm
                                        where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                          and lm.location        = s.loc
                                          and lm.effective_date <= cl.effective_date))
                and NOT EXISTS (select 1
                                  from rpm_location_move lm
                                 where lm.old_zone_id     = cl.zone_id
                                   and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                   and lm.location        = s.loc
                                   and lm.effective_date <= cl.effective_date)
             -- Inherit Executed Clearances
             union all
             select src1.bulk_cc_pe_id,
                    src1.price_event_id,
                    src1.dept,
                    src1.class,
                    src1.subclass,
                    src1.item,
                    src1.item_parent,
                    src1.diff_id,
                    src1.location,
                    src1.zone_node_type,
                    src1.zone_id,
                    src1.thread_number
               from (select src.bulk_cc_pe_id,
                            src.price_event_id,
                            src.dept,
                            src.class,
                            src.subclass,
                            src.item,
                            src.item_parent,
                            src.diff_id,
                            src.location,
                            src.zone_node_type,
                            src.zone_id,
                            src.thread_number,
                            src.reset_date,
                            RANK () OVER (PARTITION BY src.item,
                                                    src.diff_id,
                                                    src.zone_id,
                                                    src.location
                                           ORDER BY src.effective_date desc) rank
                       from (-- Item Level
                             select gtt.number_2 bulk_cc_pe_id,
                                    cl.clearance_id price_event_id,
                                    s.dept,
                                    s.class,
                                    s.subclass,
                                    s.item,
                                    s.item_parent,
                                    s.diff_1 diff_id,
                                    s.loc location,
                                    cl.effective_date,
                                    cl.reset_date,
                                    DECODE(s.loc_type,
                                           RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                                    s.zone_id,
                                    1 thread_number
                               from gtt_num_num_str_str_date_date gtt,
                                    rpm_stage_item_loc_clean s,
                                    rpm_clearance cl
                              where gtt.varchar2_1     = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                and gtt.varchar2_2     = cl.state
                                and s.thread_num       = I_thread_number
                                and s.process_id       = I_process_id
                                and s.thread_num       = gtt.number_1
                                and cl.item            = s.item
                                and cl.state           = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                and cl.effective_date <= LP_vdate
                                and (   cl.location    = s.loc
                                     or cl.zone_id    IN (select zl.zone_id
                                                            from rpm_zone_location zl
                                                           where zl.location = s.loc)
                                     or cl.zone_id    IN (select lm.new_zone_id
                                                            from rpm_location_move lm
                                                           where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                             and lm.location        = s.loc
                                                             and lm.effective_date <= cl.effective_date))
                                and NOT EXISTS (select 1
                                                  from rpm_location_move lm
                                                 where lm.old_zone_id     = cl.zone_id
                                                   and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                   and lm.location        = s.loc
                                                   and lm.effective_date <= cl.effective_date)
                             union all
                             -- Item Parent Level
                             select gtt.number_2 bulk_cc_pe_id,
                                    cl.clearance_id price_event_id,
                                    s.dept,
                                    s.class,
                                    s.subclass,
                                    s.item,
                                    s.item_parent,
                                    s.diff_1 diff_id,
                                    s.loc location,
                                    cl.effective_date,
                                    cl.reset_date,
                                    DECODE(s.loc_type,
                                           RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                                    s.zone_id,
                                    1 thread_number
                               from gtt_num_num_str_str_date_date gtt,
                                    rpm_stage_item_loc_clean s,
                                    rpm_clearance cl
                              where gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                and gtt.varchar2_2    = cl.state
                                and s.thread_num      = I_thread_number
                                and s.process_id      = I_process_id
                                and s.thread_num      = gtt.number_1
                                and cl.item           = s.item_parent
                                and cl.diff_id        is NULL
                                and cl.state          = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                and cl.effective_date <= LP_vdate
                                and (   cl.location   = s.loc
                                     or cl.zone_id    IN (select zl.zone_id
                                                            from rpm_zone_location zl
                                                           where zl.location = s.loc)
                                     or cl.zone_id    IN (select lm.new_zone_id
                                                            from rpm_location_move lm
                                                           where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                             and lm.location        = s.loc
                                                             and lm.effective_date <= cl.effective_date))
                                and NOT EXISTS (select 1
                                                  from rpm_location_move lm
                                                 where lm.old_zone_id     = cl.zone_id
                                                   and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                   and lm.location        = s.loc
                                                   and lm.effective_date <= cl.effective_date)
                            union all
                            -- Item parent Diff Level
                            select gtt.number_2 bulk_cc_pe_id,
                                    cl.clearance_id price_event_id,
                                    s.dept,
                                    s.class,
                                    s.subclass,
                                    s.item,
                                    s.item_parent,
                                    s.diff_1 diff_id,
                                    s.loc location,
                                    cl.effective_date,
                                    cl.reset_date,
                                    DECODE(s.loc_type,
                                           RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                           RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                                    s.zone_id,
                                    1 thread_number
                               from gtt_num_num_str_str_date_date gtt,
                                    rpm_stage_item_loc_clean s,
                                    rpm_clearance cl
                              where gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                and gtt.varchar2_2    = cl.state
                                and s.thread_num      = I_thread_number
                                and s.process_id      = I_process_id
                                and s.thread_num      = gtt.number_1
                                and cl.item           = s.item_parent
                                and cl.diff_id        is NOT NULL
                                and cl.diff_id        IN (s.diff_1,
                                                          s.diff_2,
                                                          s.diff_3,
                                                          s.diff_4)
                                and cl.state          = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                and cl.effective_date <= LP_vdate
                                and (   cl.location   = s.loc
                                     or cl.zone_id    IN (select zl.zone_id
                                                            from rpm_zone_location zl
                                                           where zl.location = s.loc)
                                     or cl.zone_id    IN (select lm.new_zone_id
                                                            from rpm_location_move lm
                                                           where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                             and lm.location        = s.loc
                                                             and lm.effective_date <= cl.effective_date))
                                and NOT EXISTS (select 1
                                                  from rpm_location_move lm
                                                 where lm.old_zone_id     = cl.zone_id
                                                   and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                   and lm.location        = s.loc
                                                   and lm.effective_date <= cl.effective_date)
                            union all
                            -- Clearance Skulist
                            select gtt.number_2 bulk_cc_pe_id,
                                   cl.clearance_id price_event_id,
                                   s.dept,
                                   s.class,
                                   s.subclass,
                                   s.item,
                                   s.item_parent,
                                   s.diff_1 diff_id,
                                   s.loc location,
                                   cl.effective_date,
                                   cl.reset_date,
                                   DECODE(s.loc_type,
                                          RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                                   s.zone_id,
                                   1 thread_number
                              from gtt_num_num_str_str_date_date gtt,
                                   rpm_stage_item_loc_clean s,
                                   (select cl.clearance_id,
                                           cl.location,
                                           cl.zone_id,
                                           cl.effective_date,
                                           cl.reset_date,
                                           cl.state,
                                           rcs.item
                                      from rpm_clearance cl,
                                           rpm_clearance_skulist rcs
                                     where cl.state           = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                       and cl.effective_date  <= LP_vdate
                                       and cl.skulist         is NOT NULL
                                       and rcs.price_event_id = cl.clearance_id
                                       and rcs.skulist        = cl.skulist
                                       and rownum             > 0) cl
                             where gtt.varchar2_1  = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                               and gtt.varchar2_2  = cl.state
                               and s.thread_num    = I_thread_number
                               and s.process_id    = I_process_id
                               and s.thread_num    = gtt.number_1
                               and (   cl.item     = s.item_parent
                                    or cl.item     = s.item)
                               and (   cl.location = s.loc
                                    or cl.zone_id  IN (select zl.zone_id
                                                         from rpm_zone_location zl
                                                        where zl.location = s.loc)
                                    or cl.zone_id  IN (select lm.new_zone_id
                                                         from rpm_location_move lm
                                                        where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                          and lm.location        = s.loc
                                                          and lm.effective_date <= cl.effective_date))
                               and NOT EXISTS (select 1
                                                 from rpm_location_move lm
                                                where lm.old_zone_id     = cl.zone_id
                                                  and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                  and lm.location        = s.loc
                                                  and lm.effective_date <= cl.effective_date)
                            union all
                            -- Price Event ItemList
                            select gtt.number_2 bulk_cc_pe_id,
                                   cl.clearance_id price_event_id,
                                   s.dept,
                                   s.class,
                                   s.subclass,
                                   s.item,
                                   s.item_parent,
                                   s.diff_1 diff_id,
                                   s.loc location,
                                   cl.effective_date,
                                   cl.reset_date,
                                   DECODE(s.loc_type,
                                          RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                          RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                                   s.zone_id,
                                   1 thread_number
                              from gtt_num_num_str_str_date_date gtt,
                                   rpm_stage_item_loc_clean s,
                                   (select cl.clearance_id,
                                           cl.location,
                                           cl.zone_id,
                                           cl.effective_date,
                                           cl.reset_date,
                                           cl.state,
                                           rmld.item,
                                           rmld.merch_level
                                      from rpm_clearance cl,
                                           rpm_merch_list_detail rmld
                                     where cl.state                = RPM_CONSTANTS.PC_EXECUTED_STATE_CODE
                                       and cl.effective_date       <= LP_vdate
                                       and cl.price_event_itemlist is NOT NULL
                                       and cl.price_event_itemlist = rmld.merch_list_id
                                       and rownum                  > 0) cl
                             where gtt.varchar2_1 = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                               and gtt.varchar2_2 = cl.state
                               and s.thread_num   = I_thread_number
                               and s.process_id   = I_process_id
                               and s.thread_num   = gtt.number_1
                               and (   (    cl.merch_level = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                        and cl.item        = s.item_parent)
                                    or (    cl.merch_level = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and cl.item        = s.item))
                               and (   cl.location = s.loc
                                    or cl.zone_id IN (select zl.zone_id
                                                        from rpm_zone_location zl
                                                       where zl.location = s.loc)
                                    or cl.zone_id IN (select lm.new_zone_id
                                                        from rpm_location_move lm
                                                       where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                         and lm.location        = s.loc
                                                         and lm.effective_date <= cl.effective_date))
                               and NOT EXISTS (select 1
                                                 from rpm_location_move lm
                                                where lm.old_zone_id     = cl.zone_id
                                                  and lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                                  and lm.location        = s.loc
                                                  and lm.effective_date <= cl.effective_date)) src) src1
                          where src1.rank = 1
                            and (   src1.reset_date is NULL
                                 or src1.reset_date > LP_vdate)) t;

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - Insert into rpm_bulk_cc_pe_item and location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up clearances for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_CLEARANCES_2;

   END;

   <<SETUP_CLEARANCES_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_CLEARANCES;

--------------------------------------------------------

FUNCTION SETUP_SIMPLE_PROMOS(O_error_msg        OUT VARCHAR2,
                             I_thread_number IN     NUMBER,
                             I_process_id    IN     NUMBER)
RETURN BOOLEAN IS

      --Instrumentation
   L_program  VARCHAR(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_SIMPLE_PROMOS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up simple promotions for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto SETUP_SIMPLE_PROMOS_1;

   END;

   <<SETUP_SIMPLE_PROMOS_1>>
   
   insert all
   into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                             price_event_id,
                             itemloc_id,
                             item,
                             dept,
                             class,
                             subclass,
                             item_parent,
                             diff_id,
                             merch_level_type,
                             thread_number)
                     values (bulk_cc_pe_id,
                             price_event_id,
                             RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(1),
                             item,
                             dept,
                             class,
                             subclass,
                             item_parent,
                             diff_id,
                             merch_level_type,
                             thread_number)
   into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                 price_event_id,
                                 itemloc_id,
                                 location,
                                 zone_node_type,
                                 zone_id)
                         values (bulk_cc_pe_id,
                                 price_event_id,
                                 RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(0),
                                 location,
                                 zone_node_type,
                                 zone_id)
      select distinct gtt.number_2 bulk_cc_pe_id,
             stage_p.promo_dtl_id price_event_id,
             stage_p.stage_dept dept,
             stage_p.stage_class class,
             stage_p.stage_subclass subclass,
             stage_p.item,
             stage_p.item_parent,
             stage_p.diff_id,
             stage_p.loc location,
             stage_p.zone_node_type,
             stage_p.zone_id,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             1 thread_number
        from gtt_num_num_str_str_date_date gtt,
             rpm_promo_zone_location rpzl,
             (with stage_ps as
                 -- Item level
                 (select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num     = I_thread_number
                     and s.process_id     = I_process_id
                     and s.item           = rpdmn.item
                     and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                  union all
                  -- Parent item Level
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num     = I_thread_number
                     and s.process_id     = I_process_id
                     and s.item_parent    = rpdmn.item
                     and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                     and rpdmn.diff_id    is NULL
                  union all
                  -- Parent/Diff level
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num     = I_thread_number
                     and s.process_id     = I_process_id
                     and s.item_parent    = rpdmn.item
                     and rpdmn.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                     and rpdmn.diff_id    IN (s.diff_1,
                                              s.diff_2,
                                              s.diff_3,
                                              s.diff_4)
                  union all
                  -- Item List level - Parent Items
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_skulist rpds,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num       = I_thread_number
                     and s.process_id       = I_process_id
                     and s.item_parent      = rpds.item
                     and rpds.item_level    = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                     and rpdmn.promo_dtl_id = rpds.price_event_id
                     and rpdmn.skulist      = rpds.skulist
                     and rpdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdmn.skulist      is NOT NULL
                  union all
                  -- Item List level - Tran items
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_skulist rpds,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num       = I_thread_number
                     and s.process_id       = I_process_id
                     and s.item             = rpds.item
                     and rpds.item_level    = RPM_CONSTANTS.IL_ITEM_LEVEL
                     and rpdmn.promo_dtl_id = rpds.price_event_id
                     and rpdmn.skulist      = rpds.skulist
                     and rpdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdmn.skulist      is NOT NULL
                  union all
                  -- Price Event ItemList - Parent Item level
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_merch_list_detail rmld,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num               = I_thread_number
                     and s.process_id               = I_process_id
                     and s.item_parent              = rmld.item
                     and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
                     and rpdmn.price_event_itemlist = rmld.merch_list_id
                     and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                     and rpdmn.price_event_itemlist is NOT NULL
                  union all
                  -- Price Event ItemList - Tran Item level
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_merch_list_detail rmld,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num               = I_thread_number
                     and s.process_id               = I_process_id
                     and s.item                     = rmld.item
                     and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
                     and rpdmn.price_event_itemlist = rmld.merch_list_id
                     and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                     and rpdmn.price_event_itemlist is NOT NULL
                  union all
                  -- Merch hierarchy levels
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_merch_node rpdmn
                   where s.thread_num     = I_thread_number
                     and s.process_id     = I_process_id
                     and s.dept           = rpdmn.dept
                     and s.class          = NVL(rpdmn.class, s.class)
                     and s.subclass       = NVL(rpdmn.subclass, s.subclass)
                     and rpdmn.merch_type IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                              RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                              RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                  union all
                  -- Merch List level -- parent item
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_merch_list_detail rmld,
                         rpm_promo_dtl_merch_node rpdmn,
                         rpm_promo_dtl rpd
                   where s.thread_num                        = I_thread_number
                     and s.process_id                        = I_process_id
                     and s.item_parent                       = rmld.item
                     and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                     and rpdmn.skulist                       = rmld.merch_list_id
                     and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdmn.skulist                       is NOT NULL
                     and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                     and NVL(rpd.sys_generated_exclusion, 0) = 1
                  union all
                  -- Merch List level -- parent/diff item
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_merch_node rpdmn,
                         rpm_promo_dtl rpd,
                         rpm_merch_list_detail rmld
                   where s.thread_num                        = I_thread_number
                     and s.process_id                        = I_process_id
                     and s.item_parent                       = rmld.item
                     and s.diff_1                            = rmld.diff_id
                     and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                     and rpdmn.skulist                       = rmld.merch_list_id
                     and rpdmn.skulist                       is NOT NULL
                     and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                     and NVL(rpd.sys_generated_exclusion, 0) = 1
                     and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                  union all
                  -- Merch List level -- tran item
                  select s.item,
                         s.loc,
                         s.dept stage_dept,
                         s.class stage_class,
                         s.subclass stage_subclass,
                         s.item_parent,
                         s.diff_1 diff_id,
                         DECODE(s.loc_type,
                                RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                         s.zone_id,
                         s.thread_num,
                         rpdmn.promo_dtl_list_id
                    from rpm_stage_item_loc_clean s,
                         rpm_promo_dtl_merch_node rpdmn,
                         rpm_promo_dtl rpd,
                         rpm_merch_list_detail rmld
                   where s.thread_num                        = I_thread_number
                     and s.process_id                        = I_process_id
                     and s.item                              = rmld.item
                     and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                     and rpdmn.skulist                       = rmld.merch_list_id
                     and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                     and rpdmn.skulist                       is NOT NULL
                     and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                     and NVL(rpd.sys_generated_exclusion, 0) = 1) -- end stage_ps definition
                select rpd.promo_dtl_id,
                       rpd.state,
                       rpd.start_date,
                       s.item,
                       s.loc,
                       s.stage_dept,
                       s.stage_class,
                       s.stage_subclass,
                       s.item_parent,
                       s.diff_id,
                       s.zone_node_type,
                       s.zone_id,
                       s.thread_num,
                       rpc.customer_type
                  from stage_ps s,
                       rpm_promo_dtl_list rpdl,
                       rpm_promo_dtl_disc_ladder rpddl,
                       rpm_promo_dtl_list_grp rpdlg,
                       rpm_promo_dtl rpd,
                       rpm_promo_comp rpc
                 where s.promo_dtl_list_id         = rpdl.promo_dtl_list_id
                   and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                   and rpddl.change_type           is NOT NULL
                   and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                   and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                   and rpd.promo_comp_id           = rpc.promo_comp_id
                   and rpc.type                    = RPM_CONSTANTS.SIMPLE_CODE
                   and (   (    rpd.state                       = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                            and rpd.start_date                  > LP_vdate
                            and NVL(rpd.end_date, LP_vdate + 1) > LP_vdate)
                        or rpd.state                            IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE))) stage_p
       where (   (    gtt.varchar2_1        = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION
                  and stage_p.customer_type is NULL)
              or (    gtt.varchar2_1        = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO
                  and stage_p.customer_type is NOT NULL))
         and gtt.varchar2_2     = TO_CHAR(stage_p.state)
         and stage_p.thread_num = gtt.number_1
         and rpzl.promo_dtl_id  = stage_p.promo_dtl_id
         and (   (    rpzl.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpzl.location       = stage_p.loc)
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select zl.zone_id
                                from rpm_zone_location zl
                               where zl.location = stage_p.loc
                                 and zl.zone_id  = rpzl.zone_id))
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select lm.new_zone_id
                                from rpm_location_move lm
                               where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                 and lm.location        = stage_p.loc
                                 and lm.effective_date <= stage_p.start_date
                                 and rpzl.zone_id       = lm.new_zone_id)))
         and NOT EXISTS (select lm.old_zone_id
                           from rpm_location_move lm
                          where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                            and lm.location        = stage_p.loc
                            and lm.effective_date <= stage_p.start_date
                            and rpzl.zone_id       = lm.old_zone_id);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_bulk_cc_pe_item and location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up simple promotions for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_SIMPLE_PROMOS_2;

   END;

   <<SETUP_SIMPLE_PROMOS_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_SIMPLE_PROMOS;

--------------------------------------------------------

FUNCTION SETUP_THRESHOLD_PROMOS(O_error_msg        OUT VARCHAR2,
                                I_thread_number IN     NUMBER,
                                I_process_id    IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_THRESHOLD_PROMOS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up threshold promotions for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto SETUP_THRESHOLD_PROMOS_1;

   END;

   <<SETUP_THRESHOLD_PROMOS_1>>

   insert all
   into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                             price_event_id,
                             itemloc_id,
                             dept,
                             class,
                             subclass,
                             item,
                             item_parent,
                             diff_id,
                             merch_level_type,
                             thread_number)
                     values (bulk_cc_pe_id,
                             price_event_id,
                             RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(1),
                             dept,
                             class,
                             subclass,
                             item,
                             item_parent,
                             diff_id,
                             merch_level_type,
                             thread_number)
   into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                 price_event_id,
                                 itemloc_id,
                                 location,
                                 zone_node_type,
                                 zone_id)
                         values (bulk_cc_pe_id,
                                 price_event_id,
                                 RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(0),
                                 location,
                                 zone_node_type,
                                 zone_id)
      select distinct gtt.number_2 bulk_cc_pe_id,
             stage_p.promo_dtl_id price_event_id,
             stage_p.stage_dept dept,
             stage_p.stage_class class,
             stage_p.stage_subclass subclass,
             stage_p.item,
             stage_p.item_parent,
             stage_p.diff_id,
             stage_p.loc location,
             stage_p.zone_node_type,
             stage_p.zone_id,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             1 thread_number
        from gtt_num_num_str_str_date_date gtt,
             rpm_promo_zone_location rpzl,
             (with stage_ps as
                 -- Item level
                (select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.item           = rpdmn.item
                    and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 union all
                 -- Parent item Level
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.item_parent    = rpdmn.item
                    and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                    and rpdmn.diff_id    is NULL
                 union all
                 -- Parent/Diff level
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.item_parent    = rpdmn.item
                    and rpdmn.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                    and rpdmn.diff_id    IN (s.diff_1,
                                             s.diff_2,
                                             s.diff_3,
                                             s.diff_4)
                 union all
                 -- Item List level - Parent Item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_skulist rpds,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num       = I_thread_number
                    and s.process_id       = I_process_id
                    and s.item_parent      = rpds.item
                    and rpds.item_level    = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
                    and rpdmn.promo_dtl_id = rpds.price_event_id
                    and rpdmn.skulist      = rpds.skulist
                    and rpdmn.skulist      is NOT NULL
                    and rpdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 union all
                 -- Item List level - Tran Item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_skulist rpds,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num       = I_thread_number
                    and s.process_id       = I_process_id
                    and s.item             = rpds.item
                    and rpds.item_level    = RPM_CONSTANTS.IL_ITEM_LEVEL
                    and rpdmn.promo_dtl_id = rpds.price_event_id
                    and rpdmn.skulist      = rpds.skulist
                    and rpdmn.skulist      is NOT NULL
                    and rpdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                 union all
                 -- Price Event Item List level - Parent Item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_merch_list_detail rmld,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num               = I_thread_number
                    and s.process_id               = I_process_id
                    and s.item_parent              = rmld.item
                    and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
                    and rpdmn.price_event_itemlist = rmld.merch_list_id
                    and rpdmn.price_event_itemlist is NOT NULL
                    and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 union all
                 -- Price Event Item List level - Tran Item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_merch_list_detail rmld
                  where s.thread_num               = I_thread_number
                    and s.process_id               = I_process_id
                    and s.item                     = rmld.item
                    and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
                    and rpdmn.price_event_itemlist = rmld.merch_list_id
                    and rpdmn.price_event_itemlist is NOT NULL
                    and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 union all
                 -- Merch hierarchy levels
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.dept           = rpdmn.dept
                    and s.class          = NVL(rpdmn.class, s.class)
                    and s.subclass       = NVL(rpdmn.subclass, s.subclass)
                    and rpdmn.merch_type IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
                 union all
                 -- Merch List level -- parent item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_merch_list_detail rmld,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_promo_dtl rpd
                  where s.thread_num                        = I_thread_number
                    and s.process_id                        = I_process_id
                    and s.item_parent                       = rmld.item
                    and rmld.merch_level                    = RPM_CONSTANTS.PARENT_MERCH_TYPE
                    and rpdmn.skulist                       = rmld.merch_list_id
                    and rpdmn.skulist                       is NOT NULL
                    and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                    and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                    and NVL(rpd.sys_generated_exclusion, 0) = 1
                 union all
                 -- Merch List level -- parent/diff item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_merch_list_detail rmld,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_promo_dtl rpd
                  where s.thread_num                        = I_thread_number
                    and s.process_id                        = I_process_id
                    and s.item_parent                       = rmld.item
                    and s.diff_1                            = rmld.diff_id
                    and rpdmn.skulist                       = rmld.merch_list_id
                    and rmld.merch_level                    = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                    and rpdmn.skulist                       is NOT NULL
                    and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                    and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                    and NVL(rpd.sys_generated_exclusion, 0) = 1
                 union all
                 -- Merch List level -- tran item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_merch_list_detail rmld,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_promo_dtl rpd
                  where s.thread_num                        = I_thread_number
                    and s.process_id                        = I_process_id
                    and s.item                              = rmld.item
                    and rpdmn.skulist                       = rmld.merch_list_id
                    and rmld.merch_level                    = RPM_CONSTANTS.ITEM_MERCH_TYPE
                    and rpdmn.skulist                       is NOT NULL
                    and rpdmn.merch_type                    = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
                    and rpd.promo_dtl_id                    = rpdmn.promo_dtl_id
                    and NVL(rpd.sys_generated_exclusion, 0) = 1) -- end stage_ps definition
                select rpd.promo_dtl_id,
                       rpd.state,
                       rpd.start_date,
                       s.item,
                       s.loc,
                       s.stage_dept,
                       s.stage_class,
                       s.stage_subclass,
                       s.item_parent,
                       s.diff_id,
                       s.zone_node_type,
                       s.zone_id,
                       s.thread_num,
                       rpc.customer_type
                  from stage_ps s,
                       rpm_promo_dtl_list rpdl,
                       rpm_promo_dtl_disc_ladder rpddl,
                       rpm_promo_dtl_list_grp rpdlg,
                       rpm_promo_dtl rpd,
                       rpm_promo_comp rpc
                 where s.promo_dtl_list_id         = rpdl.promo_dtl_list_id
                   and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                   and rpddl.change_type           is NOT NULL
                   and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                   and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                   and rpd.promo_comp_id           = rpc.promo_comp_id
                   and rpc.type                    = RPM_CONSTANTS.THRESHOLD_CODE
                   and (   (    rpd.state                       = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                            and rpd.start_date                  > LP_vdate
                            and NVL(rpd.end_date, LP_vdate + 1) > LP_vdate)
                        or rpd.state                            IN (RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                                                                    RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE))) stage_p
       where (   (    gtt.varchar2_1        = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION
                  and stage_p.customer_type is NULL)
              or (    gtt.varchar2_1        = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO
                  and stage_p.customer_type is NOT NULL))
         and gtt.varchar2_2     = TO_CHAR(stage_p.state)
         and stage_p.thread_num = gtt.number_1
         and rpzl.promo_dtl_id  = stage_p.promo_dtl_id
         and (   (    rpzl.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpzl.location       = stage_p.loc)
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select zl.zone_id
                                from rpm_zone_location zl
                               where zl.location = stage_p.loc
                                 and zl.zone_id  = rpzl.zone_id))
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select lm.new_zone_id
                                from rpm_location_move lm
                               where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                 and lm.location        = stage_p.loc
                                 and lm.effective_date <= stage_p.start_date
                                 and rpzl.zone_id       = lm.new_zone_id)))
         and NOT EXISTS (select lm.old_zone_id
                           from rpm_location_move lm
                          where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                            and lm.location        = stage_p.loc
                            and lm.effective_date <= stage_p.start_date
                            and rpzl.zone_id       = lm.old_zone_id);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number||
                                      ' - Insert into rpm_bulk_cc_pe_item and location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up threshold promotions for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_THRESHOLD_PROMOS_2;

   END;

   <<SETUP_THRESHOLD_PROMOS_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_THRESHOLD_PROMOS;

--------------------------------------------------------

FUNCTION SETUP_COMPLEX_PROMOS(O_error_msg        OUT VARCHAR2,
                              I_thread_number IN     NUMBER,
                              I_process_id    IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_COMPLEX_PROMOS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up complex promotions for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto SETUP_COMPLEX_PROMOS_1;

   END;

   <<SETUP_COMPLEX_PROMOS_1>>
   
   insert all
   into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                             price_event_id,
                             itemloc_id,
                             dept,
                             class,
                             subclass,
                             item,
                             item_parent,
                             diff_id,
                             merch_level_type,
                             thread_number)
                     values (bulk_cc_pe_id,
                             price_event_id,
                             RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(1),
                             dept,
                             class,
                             subclass,
                             item,
                             item_parent,
                             diff_id,
                             merch_level_type,
                             thread_number)
   into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                 price_event_id,
                                 itemloc_id,
                                 location,
                                 zone_node_type,
                                 zone_id)
                         values (bulk_cc_pe_id,
                                 price_event_id,
                                 RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(0),
                                 location,
                                 zone_node_type,
                                 zone_id)
      select distinct gtt.number_2 bulk_cc_pe_id,
             stage_p.promo_dtl_id price_event_id,
             stage_p.stage_dept dept,
             stage_p.stage_class class,
             stage_p.stage_subclass subclass,
             stage_p.item,
             stage_p.item_parent,
             stage_p.diff_id,
             stage_p.loc location,
             stage_p.zone_node_type,
             stage_p.zone_id,
             RPM_CONSTANTS.ITEM_MERCH_TYPE merch_level_type,
             1 thread_number
        from gtt_num_num_str_str_date_date gtt,
             rpm_promo_zone_location rpzl,
             (with stage_ps as
                 -- Item level
                (select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.item           = rpdmn.item
                    and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                 union all
                 -- Parent item Level
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.item_parent    = rpdmn.item
                    and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
                    and rpdmn.diff_id    is NULL
                 union all
                 -- Parent/Diff level
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.item_parent    = rpdmn.item
                    and rpdmn.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
                    and rpdmn.diff_id    IN (s.diff_1,
                                             s.diff_2,
                                             s.diff_3,
                                             s.diff_4)
                 union all
                 -- Price Event Item List - Parent item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_merch_list_detail rmld,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num               = I_thread_number
                    and s.process_id               = I_process_id
                    and s.item_parent              = rmld.item
                    and rpdmn.price_event_itemlist = rmld.merch_list_id
                    and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
                    and rpdmn.price_event_itemlist is NOT NULL
                    and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 union all
                 -- Price Event Item List - Tran item
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn,
                        rpm_merch_list_detail rmld
                  where s.thread_num               = I_thread_number
                    and s.process_id               = I_process_id
                    and s.item                     = rmld.item
                    and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
                    and rpdmn.price_event_itemlist = rmld.merch_list_id
                    and rpdmn.price_event_itemlist is NOT NULL
                    and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
                 union all
                 -- Merch hierarchy levels
                 select s.item,
                        s.loc,
                        s.dept stage_dept,
                        s.class stage_class,
                        s.subclass stage_subclass,
                        s.item_parent,
                        s.diff_1 diff_id,
                        DECODE(s.loc_type,
                               RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                               RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                        s.zone_id,
                        s.thread_num,
                        rpdmn.promo_dtl_list_id
                   from rpm_stage_item_loc_clean s,
                        rpm_promo_dtl_merch_node rpdmn
                  where s.thread_num     = I_thread_number
                    and s.process_id     = I_process_id
                    and s.dept           = rpdmn.dept
                    and s.class          = NVL(rpdmn.class, s.class)
                    and s.subclass       = NVL(rpdmn.subclass, s.subclass)
                    and rpdmn.merch_type IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                             RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                             RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)) -- end stage_ps definition
                select rpd.promo_dtl_id,
                       rpd.state,
                       rpd.start_date,
                       s.item,
                       s.loc,
                       s.stage_dept,
                       s.stage_class,
                       s.stage_subclass,
                       s.item_parent,
                       s.diff_id,
                       s.zone_node_type,
                       s.zone_id,
                       s.thread_num,
                       rpc.customer_type
                  from stage_ps s,
                       rpm_promo_dtl_list rpdl,
                       rpm_promo_dtl_disc_ladder rpddl,
                       rpm_promo_dtl_list_grp rpdlg,
                       rpm_promo_dtl rpd,
                       rpm_promo_comp rpc
                 where s.promo_dtl_list_id         = rpdl.promo_dtl_list_id
                   and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
                   and rpddl.change_type           is NOT NULL
                   and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
                   and rpd.promo_dtl_id            = rpdlg.promo_dtl_id
                   and rpd.promo_comp_id           = rpc.promo_comp_id
                   and rpc.type                    = RPM_CONSTANTS.COMPLEX_CODE
                   and (   (    rpd.state                       = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                            and rpd.start_date                  > LP_vdate
                            and NVL(rpd.end_date, LP_vdate + 1) > LP_vdate)
                        or rpd.state                            = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)) stage_p
       where (   (    gtt.varchar2_1        = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION
                  and stage_p.customer_type is NULL)
              or (    gtt.varchar2_1        = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO
                  and stage_p.customer_type is NOT NULL))
         and gtt.varchar2_2     = TO_CHAR(stage_p.state)
         and stage_p.thread_num = gtt.number_1
         and rpzl.promo_dtl_id  = stage_p.promo_dtl_id
         and (   (    rpzl.zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpzl.location       = stage_p.loc)
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select zl.zone_id
                                from rpm_zone_location zl
                               where zl.location = stage_p.loc
                                 and zl.zone_id  = rpzl.zone_id))
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select lm.new_zone_id
                                from rpm_location_move lm
                               where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                 and lm.location        = stage_p.loc
                                 and lm.effective_date <= stage_p.start_date
                                 and rpzl.zone_id       = lm.new_zone_id)))
         and NOT EXISTS (select lm.old_zone_id
                           from rpm_location_move lm
                          where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                            and lm.location        = stage_p.loc
                            and lm.effective_date <= stage_p.start_date
                            and rpzl.zone_id       = lm.old_zone_id);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - Insert into rpm_bulk_cc_pe_item and location - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up complex promotions for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_COMPLEX_PROMOS_2;

   END;

   <<SETUP_COMPLEX_PROMOS_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_COMPLEX_PROMOS;

--------------------------------------------------------

FUNCTION SETUP_TRANSACTION_PROMOS(O_error_msg                 OUT VARCHAR2,
                                  I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                                  I_thread_number          IN     NUMBER,
                                  I_process_id             IN     NUMBER)
RETURN BOOLEAN IS

   --Instrumentation
   L_program VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_TRANSACTION_PROMOS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up transaction promotions for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto SETUP_TRANSACTION_PROMOS_1;

   END;

   <<SETUP_TRANSACTION_PROMOS_1>>

   insert all
      when head_rnk = 1 then
         into rpm_bulk_cc_pe (bulk_cc_pe_id,
                              price_event_type,
                              persist_ind,
                              start_state,
                              end_state,
                              user_name,
                              emergency_perm,
                              secondary_ind,
                              asynch_ind,
                              cc_request_group_id,
                              auto_clean_ind,
                              thread_processor_class,
                              status,
                              need_il_explode_ind,
                              non_asynch_single_txn_ind)
                      values (bulk_cc_pe_id,
                              price_event_type,
                              'Y',
                              end_state,
                              end_state,
                              'NewItemLocationBatch',
                              1, -- emergency_perm
                              0, -- secondary_ind
                              0, -- asynch_ind
                              0, -- cc_request_group_id
                              0, -- auto_clean_ind
                              I_thread_processor_class,
                              'I', -- status
                              0,  -- need_il_explode_ind
                              1) -- non_asynch_single_txn_ind
      when head_rnk = 1 then
         into rpm_nil_bulkccpe_process_id (bulk_cc_pe_id,
                                           process_id,
                                           thread_num)
                                   values (bulk_cc_pe_id,
                                           I_process_id,
                                           I_thread_number)
      when thread_rnk = 1 then
         into rpm_bulk_cc_pe_thread (bulk_cc_pe_id,
                                     price_event_id,
                                     price_event_display_id,
                                     price_event_type,
                                     price_event_start_date,
                                     rank,
                                     man_txn_excl_exists,
                                     man_txn_exclusion_parent_id,
                                     elig_for_sys_gen_exclusions)
                             values (bulk_cc_pe_id,
                                     promo_dtl_id,
                                     promo_dtl_display_id,
                                     price_event_type,
                                     start_date,
                                     0, -- rank
                                     man_txn_excl_exists,
                                     exception_parent_id,
                                     0) -- elig_for_sys_gen_exclusions
      when 1 = 1 then
         into rpm_bulk_cc_pe_item (bulk_cc_pe_id,
                                   price_event_id,
                                   itemloc_id,
                                   dept,
                                   class,
                                   subclass,
                                   item,
                                   item_parent,
                                   diff_id,
                                   merch_level_type,
                                   thread_number,
                                   pe_merch_level,
                                   txn_man_excluded_item)
                           values (bulk_cc_pe_id,
                                   promo_dtl_id,
                                   RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(1),
                                   dept,
                                   class,
                                   subclass,
                                   item,
                                   item_parent,
                                   diff_id,
                                   RPM_CONSTANTS.ITEM_MERCH_TYPE,
                                   1, -- thread_number
                                   RPM_CONSTANTS.ITEM_MERCH_TYPE,
                                   txn_man_excluded_item)
      when 1 = 1 then
         into rpm_bulk_cc_pe_location (bulk_cc_pe_id,
                                       price_event_id,
                                       itemloc_id,
                                       location,
                                       zone_node_type,
                                       zone_id)
                               values (bulk_cc_pe_id,
                                       promo_dtl_id,
                                       RPM_NEW_ITEM_LOC_SQL.GET_ITEMLOC_ID(0),
                                       loc,
                                       zone_node_type,
                                       zone_id)
      with tran_promo as
         (select gtt.number_2 bulk_cc_pe_id,
                 gtt.varchar2_1 price_event_type,
                 gtt.varchar2_2 end_state,
                 rpd.promo_dtl_id,
                 rpd.promo_dtl_display_id,
                 DECODE(rpd.man_txn_exclusion,
                        1, rpd.exception_parent_id,
                        NULL) exception_parent_id,
                 rpd.man_txn_excl_exists,
                 rpd.man_txn_exclusion,
                 rpd.start_date
            from gtt_num_num_str_str_date_date gtt,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl rpd
           where gtt.number_1      = I_thread_number
             and gtt.varchar2_2    = TO_CHAR(rpd.state)
             and gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION
             and rpc.customer_type is NULL
             and rpc.type          = RPM_CONSTANTS.TRANSACTION_CODE
             and rpc.promo_comp_id = rpd.promo_comp_id
             and (   (    rpd.state                       = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                      and rpd.start_date                  > LP_vdate
                      and NVL(rpd.end_date, LP_vdate + 1) > LP_vdate)
                  or rpd.state                            = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)
          union all
          select gtt.number_2 bulk_cc_pe_id,
                 gtt.varchar2_1 price_event_type,
                 gtt.varchar2_2 end_state,
                 rpd.promo_dtl_id,
                 rpd.promo_dtl_display_id,
                 DECODE(rpd.man_txn_exclusion,
                        1, rpd.exception_parent_id,
                        NULL) exception_parent_id,
                 rpd.man_txn_excl_exists,
                 rpd.man_txn_exclusion,
                 rpd.start_date
            from gtt_num_num_str_str_date_date gtt,
                 rpm_promo_comp rpc,
                 rpm_promo_dtl rpd
           where gtt.number_1      = I_thread_number
             and gtt.varchar2_2    = TO_CHAR(rpd.state)
             and gtt.varchar2_1    = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO
             and rpc.customer_type is NOT NULL
             and rpc.type          = RPM_CONSTANTS.TRANSACTION_CODE
             and rpc.promo_comp_id = rpd.promo_comp_id
             and (   (    rpd.state                       = RPM_CONSTANTS.PR_APPROVED_STATE_CODE
                      and rpd.start_date                  > LP_vdate
                      and NVL(rpd.end_date, LP_vdate + 1) > LP_vdate)
                  or rpd.state                            = RPM_CONSTANTS.PR_ACTIVE_STATE_CODE)),
      txn_dtl_exclusions as
         (-- Find Parent Item Level exclusion
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_stage_item_loc_clean s
           where tp.man_txn_exclusion = 1
             and tp.promo_dtl_id      = rpdmn.promo_dtl_id
             and s.thread_num         = I_thread_number
             and s.process_id         = I_process_id
             and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LEVEL_ITEM
             and rpdmn.diff_id        is NULL
             and s.item_parent        = rpdmn.item
          union all
          -- find Parent Diff Level Exclusion
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_stage_item_loc_clean s
           where tp.man_txn_exclusion = 1
             and tp.promo_dtl_id      = rpdmn.promo_dtl_id
             and s.thread_num         = I_thread_number
             and s.process_id         = I_process_id
             and rpdmn.merch_type     = RPM_CONSTANTS.PARENT_ITEM_DIFF
             and rpdmn.diff_id        is NOT NULL
             and rpdmn.diff_id        IN (s.diff_1,
                                          s.diff_2,
                                          s.diff_3,
                                          s.diff_4)
             and s.item_parent        = rpdmn.item
          union all
          -- find peil level exclusion at parent item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_merch_list_detail rmld,
                 rpm_stage_item_loc_clean s
           where tp.man_txn_exclusion       = 1
             and tp.promo_dtl_id            = rpdmn.promo_dtl_id
             and s.thread_num               = I_thread_number
             and s.process_id               = I_process_id
             and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rpdmn.price_event_itemlist is NOT NULL
             and rpdmn.price_event_itemlist = rmld.merch_list_id
             and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
             and s.item_parent              = rmld.item
          union all
          -- find peil level exclusion at tran item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_merch_list_detail rmld,
                 rpm_stage_item_loc_clean s
           where tp.man_txn_exclusion       = 1
             and tp.promo_dtl_id            = rpdmn.promo_dtl_id
             and s.thread_num               = I_thread_number
             and s.process_id               = I_process_id
             and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rpdmn.price_event_itemlist is NOT NULL
             and rpdmn.price_event_itemlist = rmld.merch_list_id
             and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
             and s.item                     = rmld.item
          union all
          -- find skulist level exclusion at parent item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds,
                 rpm_stage_item_loc_clean s
           where tp.man_txn_exclusion = 1
             and tp.promo_dtl_id      = rpdmn.promo_dtl_id
             and s.thread_num         = I_thread_number
             and s.process_id         = I_process_id
             and rpdmn.skulist        is NOT NULL
             and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpdmn.skulist        = rpds.skulist
             and rpdmn.promo_dtl_id   = rpds.price_event_id
             and rpds.item_level      = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
             and s.item_parent        = rpds.item
          union all
          -- find skulist level exclusion at tran item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds,
                 rpm_stage_item_loc_clean s
           where tp.man_txn_exclusion = 1
             and tp.promo_dtl_id      = rpdmn.promo_dtl_id
             and s.thread_num         = I_thread_number
             and s.process_id         = I_process_id
             and rpdmn.skulist        is NOT NULL
             and rpdmn.merch_type     = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpdmn.skulist        = rpds.skulist
             and rpdmn.promo_dtl_id   = rpds.price_event_id
             and rpds.item_level      = RPM_CONSTANTS.IL_ITEM_LEVEL
             and s.item               = rpds.item
          union all
          -- find merch level exclusion
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 tp.promo_dtl_id,
                 tp.exception_parent_id,
                 rpdmn.merch_type
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn
           where tp.man_txn_exclusion = 1
             and s.thread_num         = I_thread_number
             and s.process_id         = I_process_id
             and tp.promo_dtl_id      = rpdmn.promo_dtl_id
             and rpdmn.merch_type     IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                          RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                          RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
             and s.dept               = rpdmn.dept
             and s.class              = NVL(rpdmn.class, s.class)
             and s.subclass           = NVL(rpdmn.subclass, s.subclass)),
      stage_ps as
         (-- Item level
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_stage_item_loc_clean s
           where s.thread_num     = I_thread_number
             and s.process_id     = I_process_id
             and tp.promo_dtl_id  = rpdmn.promo_dtl_id
             and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
             and s.item           = rpdmn.item
          union all
          -- Parent item Level
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn
           where s.thread_num     = I_thread_number
             and s.process_id     = I_process_id
             and tp.promo_dtl_id  = rpdmn.promo_dtl_id
             and rpdmn.merch_type = RPM_CONSTANTS.ITEM_LEVEL_ITEM
             and rpdmn.diff_id    is NULL
             and s.item_parent    = rpdmn.item
          union all
          -- Parent/Diff level
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn
           where s.thread_num     = I_thread_number
             and s.process_id     = I_process_id
             and tp.promo_dtl_id  = rpdmn.promo_dtl_id
             and rpdmn.merch_type = RPM_CONSTANTS.PARENT_ITEM_DIFF
             and rpdmn.diff_id    IN (s.diff_1,
                                      s.diff_2,
                                      s.diff_3,
                                      s.diff_4)
             and s.item_parent    = rpdmn.item
          union all
          -- Skulist level parent item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where s.thread_num       = I_thread_number
             and s.process_id       = I_process_id
             and tp.promo_dtl_id    = rpdmn.promo_dtl_id
             and rpdmn.promo_dtl_id = rpds.price_event_id
             and rpdmn.skulist      is NOT NULL
             and rpdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpds.item_level    = RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL
             and rpdmn.skulist      = rpds.skulist
             and s.item_parent      = rpds.item
          union all
          -- Skulist level - tran item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_promo_dtl_skulist rpds
           where s.thread_num       = I_thread_number
             and s.process_id       = I_process_id
             and tp.promo_dtl_id    = rpdmn.promo_dtl_id
             and rpdmn.promo_dtl_id = rpds.price_event_id
             and rpdmn.skulist      is NOT NULL
             and rpdmn.merch_type   = RPM_CONSTANTS.ITEM_LIST_LEVEL_ITEM
             and rpds.item_level    = RPM_CONSTANTS.IL_ITEM_LEVEL
             and rpdmn.skulist      = rpds.skulist
             and s.item             = rpds.item
          union all
          -- Price Event Item List - Parent item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_merch_list_detail rmld,
                 rpm_promo_dtl_merch_node rpdmn
           where s.thread_num               = I_thread_number
             and s.process_id               = I_process_id
             and tp.promo_dtl_id            = rpdmn.promo_dtl_id
             and rpdmn.price_event_itemlist is NOT NULL
             and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rpdmn.price_event_itemlist = rmld.merch_list_id
             and rmld.merch_level           = RPM_CONSTANTS.PARENT_MERCH_TYPE
             and s.item_parent              = rmld.item
          union all
          -- Price Event Item List - Tran item
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn,
                 rpm_merch_list_detail rmld
           where s.thread_num               = I_thread_number
             and s.process_id               = I_process_id
             and tp.promo_dtl_id            = rpdmn.promo_dtl_id
             and rpdmn.price_event_itemlist is NOT NULL
             and rpdmn.merch_type           = RPM_CONSTANTS.PRICE_EVENT_ITEM_LIST_LEVEL
             and rpdmn.price_event_itemlist = rmld.merch_list_id
             and rmld.merch_level           = RPM_CONSTANTS.ITEM_MERCH_TYPE
             and s.item                     = rmld.item
          union all
          -- Merch hierarchy levels
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn
           where s.thread_num     = I_thread_number
             and s.process_id     = I_process_id
             and tp.promo_dtl_id  = rpdmn.promo_dtl_id
             and rpdmn.merch_type IN (RPM_CONSTANTS.DEPT_LEVEL_ITEM,
                                      RPM_CONSTANTS.CLASS_LEVEL_ITEM,
                                      RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM)
             and s.dept           = rpdmn.dept
             and s.class          = NVL(rpdmn.class, s.class)
             and s.subclass       = NVL(rpdmn.subclass, s.subclass)
          union all
          -- storewide levels
          select s.item,
                 s.loc,
                 s.dept,
                 s.class,
                 s.subclass,
                 s.item_parent,
                 s.diff_1 diff_id,
                 DECODE(s.loc_type,
                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                        RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                 s.zone_id,
                 rpdmn.promo_dtl_list_id,
                 rpdmn.promo_dtl_id
            from tran_promo tp,
                 rpm_stage_item_loc_clean s,
                 rpm_promo_dtl_merch_node rpdmn
           where s.thread_num     = I_thread_number
             and s.process_id     = I_process_id
             and tp.promo_dtl_id  = rpdmn.promo_dtl_id
             and rpdmn.merch_type = RPM_CONSTANTS.STOREWIDE_LEVEL_ITEM
             and s.dept           = NVL(rpdmn.dept, s.dept)
             and s.class          = NVL(rpdmn.class, s.class)
             and s.subclass       = NVL(rpdmn.subclass, s.subclass)) -- end stage_ps definition
         select tp.bulk_cc_pe_id,
                s.promo_dtl_id,
                tp.price_event_type,
                tp.end_state,
                tp.promo_dtl_display_id,
                tp.man_txn_excl_exists,
                tp.exception_parent_id,
                tp.start_date,
                s.dept,
                s.class,
                s.subclass,
                s.item,
                s.item_parent,
                s.diff_id,
                s.loc,
                s.zone_node_type,
                s.zone_id,
                case
                   when tde.merch_type = RPM_CONSTANTS.DEPT_LEVEL_ITEM and
                        tde.dept is NOT NULL then
                      1
                   when tde.merch_type = RPM_CONSTANTS.CLASS_LEVEL_ITEM and
                        tde.class is NOT NULL then
                      1
                   when tde.merch_type = RPM_CONSTANTS.SUBCLASS_LEVEL_ITEM and
                        tde.subclass is NOT NULL then
                      1
                   when tde.item is NULL then
                      0
                   else
                      1
                end txn_man_excluded_item,
                ROW_NUMBER() OVER (PARTITION BY tp.bulk_cc_pe_id
                                       ORDER BY NULL) head_rnk,
                ROW_NUMBER() OVER (PARTITION BY tp.bulk_cc_pe_id,
                                                tp.promo_dtl_id
                                       ORDER BY NULL) thread_rnk
           from tran_promo tp,
                stage_ps s,
                rpm_promo_dtl_list_grp rpdlg,
                rpm_promo_dtl_list rpdl,
                rpm_promo_dtl_disc_ladder rpddl,
                rpm_promo_zone_location rpzl,
                txn_dtl_exclusions tde
          where tp.promo_dtl_id             = s.promo_dtl_id
            and s.promo_dtl_list_id         = rpdl.promo_dtl_list_id
            and rpdl.promo_dtl_list_id      = rpddl.promo_dtl_list_id
            and rpddl.change_type           is NOT NULL
            and rpdlg.promo_dtl_list_grp_id = rpdl.promo_dtl_list_grp_id
            and tp.promo_dtl_id             = rpdlg.promo_dtl_id
            and rpzl.promo_dtl_id           = s.promo_dtl_id
         and (   (    rpzl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                  and rpzl.location         = s.loc)
              or (    rpzl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select zl.zone_id
                                from rpm_zone_location zl
                               where zl.location = s.loc
                                 and zl.zone_id  = rpzl.zone_id))
              or (    rpzl.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                  and EXISTS (select lm.new_zone_id
                                from rpm_location_move lm
                               where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                                 and lm.location        = s.loc
                                 and lm.effective_date <= tp.start_date
                                 and rpzl.zone_id       = lm.new_zone_id)))
         and NOT EXISTS (select lm.old_zone_id
                           from rpm_location_move lm
                          where lm.state           = RPM_CONSTANTS.LM_SCHEDULED_STATE_CODE
                            and lm.location        = s.loc
                            and lm.effective_date <= tp.start_date
                            and rpzl.zone_id       = lm.old_zone_id)
            and s.promo_dtl_id             = tde.exception_parent_id (+)
            and NVL(s.item_parent, '-999') = NVL(tde.item_parent (+), '-999')
            and NVL(s.diff_id, '-999')     = NVL(tde.diff_id (+), '-999')
            and s.item                     = tde.item (+)
            and s.loc                      = tde.loc (+)
            and s.zone_id                  = tde.zone_id (+)
            and s.zone_node_type           = tde.zone_node_type (+)
            and s.loc                      = tde.loc (+);

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - Insert into rpm_bulk_cc_pe tables - SQL%ROWCOUNT: ' || SQL%ROWCOUNT);

   LOGGER.LOG_TIME(L_program||' - I_process_id: '|| I_process_id ||
                              ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up transaction promotions for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_TRANSACTION_PROMOS_2;

   END;

   <<SETUP_TRANSACTION_PROMOS_2>>
   
   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_TRANSACTION_PROMOS;

--------------------------------------------------------

FUNCTION SETUP_BULK_HELPER_GTT(O_error_msg           OUT VARCHAR2,
                               I_price_event_type IN     RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                               I_promo_end_state  IN     RPM_BULK_CC_PE.END_STATE%TYPE,
                               I_thread_number    IN     NUMBER)
RETURN BOOLEAN IS

   L_program   VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_BULK_HELPER_GTT';

BEGIN

/*****************************************************
 *
 * GTT_NUM_NUM_STR_STR_DATE_DATE use in this package
 *
 * NUMBER_1    - chunk_num
 * NUMBER_2    - bulk_cc_pe_id
 * VARCHAR2_1  - price_event_type
 * VARCHAR2_2  - promotion end state
 * DATE_1      - not used
 * DATE_2      - not used
 *
 *****************************************************/

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2,
                                              varchar2_1,
                                              varchar2_2)
      select I_thread_number,
             RPM_BULK_CC_PE_SEQ.NEXTVAL,
             I_price_event_type,
             I_promo_end_state
        from dual;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_BULK_HELPER_GTT;

--------------------------------------------------------

FUNCTION SETUP_BULK_HELPER_GTT(O_error_msg        OUT VARCHAR2,
                               I_thread_number IN     NUMBER)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_BULK_HELPER_GTT';
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_thread_number: '||I_thread_number);

   delete
     from gtt_num_num_str_str_date_date;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                            RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                            RPM_CONSTANTS.PC_EXECUTED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                            RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                            RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                            RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                            RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                            RPM_CONSTANTS.PR_ACTIVE_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                            RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                            RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE,
                            I_thread_number) = FALSE then
      return FALSE;
   end if;

   LOGGER.LOG_TIME(L_program ||' - I_thread_number: '||I_thread_number,
                   L_start_time);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END SETUP_BULK_HELPER_GTT;

--------------------------------------------------------
--PUBLIC PROCEDURES
--------------------------------------------------------

FUNCTION INITIALIZE_PROCESS(O_error_msg            OUT VARCHAR2,
                            O_max_thread_number    OUT NUMBER,
                            O_process_id           OUT NUMBER,
                            I_luw               IN     NUMBER,
                            I_stage_status      IN     VARCHAR2)
RETURN NUMBER IS

   --Instrumentation
   L_program              VARCHAR2(40)  := 'RPM_NEW_ITEM_LOC_SQL.INITIALIZE_PROCESS';
   L_trace_name         	VARCHAR2(20) 	:= 'INP';
   L_truncate_stage_clean VARCHAR2(100) := 'truncate table rpm_stage_item_loc_clean';
   L_process_id           NUMBER(15)    := RPM_NIL_PROCESS_ID_SEQ.NEXTVAL;
   L_silc_trunc           VARCHAR2(1)   := 'Y';
   L_start_time           TIMESTAMP     := SYSTIMESTAMP;

   cursor C_CHECK_ROLLUP_THREAD is
      select 'N'
        from rpm_nil_rollup_thread
       where rownum = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program || ' - L_process_id: '|| L_process_id);
   
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to initialize the '||RPM_CONSTANTS.BATCH_NAME_NIL||' batch');

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_NIL||L_trace_name);

   EXCEPTION

      when OTHERS then
         goto INITIALIZE_PROCESS_1;

   END;

   <<INITIALIZE_PROCESS_1>>   

   open C_CHECK_ROLLUP_THREAD;
   fetch C_CHECK_ROLLUP_THREAD into L_silc_trunc;
   close C_CHECK_ROLLUP_THREAD;

   if L_silc_trunc = 'Y' then
      EXECUTE IMMEDIATE L_truncate_stage_clean;
   end if;

   if REMOVE_DELETED(O_error_msg,
                     I_stage_status) = FALSE then
      return 0;
   end if;

   if REMOVE_DUPLICATES(O_error_msg,
                        I_stage_status) = FALSE then
      return 0;
   end if;

   if REMOVE_UNWANTED(O_error_msg,
                      I_stage_status) = FALSE then
      return 0;
   end if;

   if REMOVE_EXISTING(O_error_msg,
                      I_stage_status) = FALSE then
      return 0;
   end if;

   if STAGE_ITEM_LOCS_TO_PROCESS(O_error_msg,
                                 O_max_thread_number,
                                 I_luw,
                                 I_stage_status,
                                 L_process_id) = FALSE then
      return 0;
   end if;

   update rpm_stage_item_loc sil
      set sil.status     = IN_PROGRESS_STATUS,
          sil.process_id = L_process_id
    where sil.status = I_stage_status
      and EXISTS (select 'x'
                    from rpm_stage_item_loc_clean silc
                   where silc.process_id        = L_process_id
                     and silc.stage_item_loc_id = sil.stage_item_loc_id);

   O_process_id := L_process_id;

   LOGGER.LOG_TIME(L_program || ' - L_process_id: '|| L_process_id,
                   L_start_time);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to initialize the '||RPM_CONSTANTS.BATCH_NAME_NIL||' batch');

   EXCEPTION

      when OTHERS then
         goto INITIALIZE_PROCESS_2;

   END;

   <<INITIALIZE_PROCESS_2>>
   
   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END INITIALIZE_PROCESS;

--------------------------------------------------------

FUNCTION SETUP_PRICE_EVENTS(O_error_msg                 OUT VARCHAR2,
                            O_bulk_cc_pe_ids            OUT OBJ_NUM_NUM_STR_TBL,
                            I_thread_processor_class IN     RPM_BULK_CC_PE.THREAD_PROCESSOR_CLASS%TYPE,
                            I_thread_number          IN     NUMBER,
                            I_process_id             IN     NUMBER)
RETURN NUMBER IS

   --Instrumentation
   L_program 				VARCHAR2(45) := 'RPM_NEW_ITEM_LOC_SQL.SETUP_PRICE_EVENTS';
   L_trace_name     VARCHAR2(20) := 'SPE';

   L_bulk_cc_pe_id  RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE := NULL;
   L_bulk_cc_pe_ids OBJ_NUMERIC_ID_TABLE                     := NULL;
   L_start_time     TIMESTAMP                                := SYSTIMESTAMP;

   cursor C_CHUNK_TO_BULK_CC_IDS is
      select OBJ_NUM_NUM_STR_REC(gtt.number_1,
                                 gtt.number_2,
                                 gtt.varchar2_1)
        from gtt_num_num_str_str_date_date gtt
       where EXISTS (select 'x'
                       from rpm_bulk_cc_pe_item il
                      where il.bulk_cc_pe_id = gtt.number_2
                     union all
                     select 'x'
                       from rpm_bulk_cc_pe il
                      where il.bulk_cc_pe_id = gtt.number_2);

   cursor C_BULK_CC_IDS is
      select distinct gtt.number_2
        from gtt_num_num_str_str_date_date gtt
       where EXISTS (select 'x'
                       from rpm_bulk_cc_pe_item il
                      where il.bulk_cc_pe_id = gtt.number_2);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting to set up price events');

      RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(RPM_CONSTANTS.BATCH_NAME_NIL||L_trace_name||'-'||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto SETUP_PRICE_EVENTS_1;

   END;

   <<SETUP_PRICE_EVENTS_1>>
   
   if SETUP_BULK_HELPER_GTT(O_error_msg,
                            I_thread_number) = FALSE then
      return 0;
   end if;

   if SETUP_PRICE_CHANGES(O_error_msg,
                          I_thread_number,
                          I_process_id) = FALSE then
      return 0;
   end if;

   if SETUP_CLEARANCES(O_error_msg,
                       I_thread_number,
                       I_process_id) = FALSE then
      return 0;
   end if;

   if SETUP_SIMPLE_PROMOS(O_error_msg,
                          I_thread_number,
                          I_process_id) = FALSE then
      return 0;
   end if;

   if SETUP_THRESHOLD_PROMOS(O_error_msg,
                             I_thread_number,
                             I_process_id) = FALSE then
      return 0;
   end if;

   if SETUP_COMPLEX_PROMOS(O_error_msg,
                           I_thread_number,
                           I_process_id) = FALSE then
      return 0;
   end if;

   if SETUP_TRANSACTION_PROMOS(O_error_msg,
                               I_thread_processor_class,
                               I_thread_number,
                               I_process_id) = FALSE then
      return 0;
   end if;

   if SETUP_PRICE_EVENT_INFO(O_error_msg,
                             I_thread_processor_class,
                             I_thread_number,
                             I_process_id) = FALSE then
      return 0;
   end if;

   open C_CHUNK_TO_BULK_CC_IDS;
   fetch C_CHUNK_TO_BULK_CC_IDS BULK COLLECT into O_bulk_cc_pe_ids;
   close C_CHUNK_TO_BULK_CC_IDS;

   if SEED_FUTURE_RETAIL_NO_PE(O_error_msg,
                               O_bulk_cc_pe_ids,
                               I_thread_number,
                               I_process_id) = FALSE then
      return 0;
   end if;

   if MARK_LOCATION_MOVE_NO_PE(O_error_msg,
                               O_bulk_cc_pe_ids,
                               I_thread_number,
                               I_process_id) = FALSE then
      return 0;
   end if;

   open C_BULK_CC_IDS;
   fetch C_BULK_CC_IDS BULK COLLECT into L_bulk_cc_pe_ids;
   close C_BULK_CC_IDS;

   if L_bulk_cc_pe_ids is NOT NULL and
      L_bulk_cc_pe_ids.COUNT > 0 then

      if SEED_FUTURE_RETAIL(O_error_msg,
                            L_bulk_cc_pe_ids,
                            I_thread_number,
                            I_process_id) = FALSE then
         return 0;
      end if;

      if MARK_LOCATION_MOVE(O_error_msg,
                            L_bulk_cc_pe_ids,
                            I_thread_number,
                            I_process_id) = FALSE then
         return 0;
      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending to set up price events');

   EXCEPTION

      when OTHERS then
         goto SETUP_PRICE_EVENTS_2;

   END;

   <<SETUP_PRICE_EVENTS_2>>
   
   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END SETUP_PRICE_EVENTS;
--------------------------------------------------------
FUNCTION UPDATE_NIL_FAIL(O_error_msg               OUT VARCHAR2,
                         I_bulk_cc_pe_id        IN     RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                         I_parent_thread_number IN     RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_thread_number        IN     RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                         I_item                 IN     RPM_FUTURE_RETAIL.ITEM%TYPE,
                         I_location             IN     RPM_FUTURE_RETAIL.LOCATION%TYPE,
                         I_fail_string          IN     RPM_STAGE_ITEM_LOC.ERROR_MSG%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.UPDATE_NIL_FAIL';

   L_thread_number NUMBER(10) := NULL;
   L_process_id    NUMBER(15) := NULL;

BEGIN

   -- Get the chunk_num as we need to re-process all the item/locations in the thread
   select nbpi.thread_num,
          nbpi.process_id
     into L_thread_number,
          L_process_id
     from rpm_stage_item_loc_clean silc,
          rpm_bulk_cc_pe_thread rpt,
          rpm_bulk_cc_pe_item rpi,
          rpm_bulk_cc_pe_location rpl,
          rpm_nil_bulkccpe_process_id nbpi
    where rpt.bulk_cc_pe_id        = I_bulk_cc_pe_id
      and rpt.parent_thread_number = NVL(I_parent_thread_number, rpt.parent_thread_number)
      and rpt.thread_number        = NVL(I_thread_number, rpt.thread_number)
      and rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
      and nbpi.bulk_cc_pe_id       = I_bulk_cc_pe_id
      and nbpi.bulk_cc_pe_id       = rpt.bulk_cc_pe_id
      and nbpi.bulk_cc_pe_id       = rpi.bulk_cc_pe_id
      and rpi.price_event_id       = rpt.price_event_id
      and rpi.item                 = NVL(I_item, rpi.item)
      and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
      and rpl.price_event_id       = rpi.price_event_id
      and rpl.itemloc_id           = rpi.itemloc_id
      and rpl.location             = NVL(I_location, rpl.location)
      and silc.item                = rpi.item
      and silc.loc                 = rpl.location
      and silc.process_id          = nbpi.process_id
      and silc.thread_num          = nbpi.thread_num
      and rownum                   = 1;

   -- Clean up RPM_ITEM_LOC for this thread number
   delete
     from rpm_item_loc
    where (dept,
           item,
           loc) IN (select dept,
                           item,
                           loc
                      from rpm_stage_item_loc_clean
                     where thread_num = L_thread_number
                       and process_id = L_process_id);

   -- Clean up RPM_FUTURE_RETAIL for this thread number
   delete
     from rpm_future_retail
    where (dept,
           item,
           location) IN (select dept,
                                item,
                                loc
                           from rpm_stage_item_loc_clean
                          where thread_num = L_thread_number
                            and process_id = L_process_id);

   -- Clean up RPM_PROMO_ITEM_LOC_EXPL for this thread number
   delete
     from rpm_promo_item_loc_expl
    where (dept,
           item,
           location) IN (select dept,
                                item,
                                loc
                           from rpm_stage_item_loc_clean
                          where thread_num = L_thread_number
                            and process_id = L_process_id);

   -- Clean up RPM_CUST_SEGMENT_PROMO_FR for this thread number
   delete
     from rpm_cust_segment_promo_fr
    where (dept,
           item,
           location) IN (select dept,
                                item,
                                loc
                           from rpm_stage_item_loc_clean
                          where thread_num = L_thread_number
                            and process_id = L_process_id);

   -- Update ERROR_MSG only for item/loc in Price Event that fails CC.
   update rpm_stage_item_loc il
      set il.error_msg  = SUBSTR(I_fail_string, 1, 250)
    where status        = IN_PROGRESS_STATUS
      and process_id    = L_process_id
      and EXISTS (select 'x'
                    from rpm_bulk_cc_pe_thread thread,
                         rpm_bulk_cc_pe_item rpi,
                         rpm_bulk_cc_pe_location rpl
                   where rpi.item                    = il.item
                     and rpl.location                = il.loc
                     and thread.bulk_cc_pe_id        = I_bulk_cc_pe_id
                     and thread.parent_thread_number = NVL(I_parent_thread_number, thread.parent_thread_number)
                     and thread.thread_number        = NVL(I_thread_number, thread.thread_number)
                     and rpi.bulk_cc_pe_id           = thread.bulk_cc_pe_id
                     and rpi.price_event_id          = thread.price_event_id
                     and rpi.item                    = NVL(I_item, rpi.item)
                     and rpl.bulk_cc_pe_id           = rpi.bulk_cc_pe_id
                     and rpl.price_event_id          = rpi.price_event_id
                     and rpl.itemloc_id              = rpi.itemloc_id
                     and rpl.location                = NVL(I_location, rpl.location));

   -- Update all item/loc in RPM_STAGE_ITEM_LOC to 'E' status
   update rpm_stage_item_loc il
      set il.status     = ERROR_STATUS,
          il.error_date = LP_vdate,
          il.error_msg  = SUBSTR(I_fail_string, 1, 250),
          il.process_id = NULL
    where (item,
           loc) IN (select item,
                           loc
                      from rpm_stage_item_loc_clean
                     where thread_num = L_thread_number
                       and process_id = L_process_id);

   -- Delete from RPM_STAGE_ITEM_LOC_CLEAN so the item/loc record won't be included
   -- in the roll up
   delete
      from rpm_stage_item_loc_clean
     where thread_num = L_thread_number
       and process_id = L_process_id;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END UPDATE_NIL_FAIL;
--------------------------------------------------------

FUNCTION CLEAN_UP_STAGE(O_error_msg     OUT VARCHAR2,
                        I_process_id IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.CLEAN_UP_STAGE';

   L_start_time             TIMESTAMP    := SYSTIMESTAMP;
   L_delete_ind             NUMBER(1)    := 0;
   L_status                 VARCHAR2(1)  := 'C';
   L_truncate_rollup_thread VARCHAR2(40) := 'truncate table rpm_nil_rollup_thread';

   cursor C_CHECK_OTHER_PROCESS_ID is
      select 1
        from rpm_nil_rollup_thread
       where (   process_id != I_process_id
              or (   process_id = I_process_id
                 and status    != L_status))
         and rownum = 1;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id);

   delete
     from rpm_stage_item_loc sil
    where sil.stage_item_loc_id IN (select s.stage_item_loc_id
                                      from rpm_stage_item_loc_clean s
                                     where s.process_id = I_process_id
                                       and EXISTS (select 1
                                                     from rpm_item_loc il
                                                    where il.item = s.item
                                                      and il.loc  = s.loc
                                                      and il.dept = s.dept));

   delete
      from rpm_stage_item_loc_clean s
     where s.process_id = I_process_id
       and EXISTS (select 1
                     from rpm_item_loc il
                    where il.dept = s.dept
                      and il.loc  = s.loc
                      and il.item = s.item);

   delete
      from rpm_nil_bulkccpe_process_id
     where process_id = I_process_id;

   open C_CHECK_OTHER_PROCESS_ID;
   fetch C_CHECK_OTHER_PROCESS_ID into L_delete_ind;
   close C_CHECK_OTHER_PROCESS_ID;

   if L_delete_ind = 1 then
      delete
         from rpm_nil_rollup_thread
        where process_id = I_process_id
          and status     = L_status;

   else
      EXECUTE IMMEDIATE L_truncate_rollup_thread;
   end if;

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;
END CLEAN_UP_STAGE;
-------------------------------------------------------
FUNCTION GET_PE_ITEM_LOC(O_error_msg               OUT  VARCHAR2,
                         O_price_event_type        OUT  RPM_BULK_CC_PE.PRICE_EVENT_TYPE%TYPE,
                         O_item_locs               OUT  OBJ_NUM_NUM_STR_TBL,
                         I_bulk_cc_pe_id        IN      RPM_BULK_CC_PE_THREAD.BULK_CC_PE_ID%TYPE,
                         I_parent_thread_number IN      RPM_BULK_CC_PE_THREAD.PARENT_THREAD_NUMBER%TYPE,
                         I_thread_number        IN      RPM_BULK_CC_PE_THREAD.THREAD_NUMBER%TYPE,
                         I_price_event_id       IN      RPM_BULK_CC_PE_THREAD.PRICE_EVENT_ID%TYPE)
RETURN NUMBER IS

   cursor C_PRICE_EVENT_TYPE is
      select price_event_type
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

   cursor C_PRICE_EVENT_ITEM_LOCS is
      select OBJ_NUM_NUM_STR_REC(rpl.location,
                                 l.loc_type,
                                 rpi.item)
        from rpm_bulk_cc_pe_thread thread,
             rpm_bulk_cc_pe_item rpi,
             rpm_bulk_cc_pe_location rpl,
             (select store loc,
                     0 loc_type
                from store
              union all
              select wh loc,
                     2 loc_type
                from wh) l
       where thread.bulk_cc_pe_id         = I_bulk_cc_pe_id
         and thread.parent_thread_number  = I_parent_thread_number
         and thread.thread_number         = I_thread_number
         and thread.price_event_id        = I_price_event_id
         and thread.bulk_cc_pe_id         = rpi.bulk_cc_pe_id
         and thread.price_event_id        = rpi.price_event_id
         and rpl.bulk_cc_pe_id            = rpi.bulk_cc_pe_id
         and rpl.price_event_id           = rpi.price_event_id
         and rpl.itemloc_id               = rpi.itemloc_id
         and l.loc                        = rpl.location;

BEGIN

   open C_PRICE_EVENT_TYPE;
   fetch C_PRICE_EVENT_TYPE into O_price_event_type;
   close C_PRICE_EVENT_TYPE;

   open C_PRICE_EVENT_ITEM_LOCS;
   fetch C_PRICE_EVENT_ITEM_LOCS BULK COLLECT into O_item_locs;
   close C_PRICE_EVENT_ITEM_LOCS;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_NEW_ITEM_LOC_SQL.GET_PE_ITEM_LOC',
                                        to_char(SQLCODE));
      return 0;
END GET_PE_ITEM_LOC;
--------------------------------------------------------
FUNCTION STAGE_ITEM_LOC(O_error_msg              OUT  VARCHAR2,
                        I_item                IN      ITEM_LOC.ITEM%TYPE,
                        I_loc                 IN      ITEM_LOC.LOC%TYPE,
                        I_loc_type            IN      ITEM_LOC.LOC_TYPE%TYPE,
                        I_selling_unit_retail IN      ITEM_LOC.UNIT_RETAIL%TYPE,
                        I_selling_uom         IN      ITEM_LOC.SELLING_UOM%TYPE)
RETURN NUMBER IS


BEGIN

   insert into rpm_stage_item_loc(stage_item_loc_id,
                                  item,
                                  loc,
                                  loc_type,
                                  selling_unit_retail,
                                  selling_uom,
                                  status,
                                  create_date,
                                  error_msg,
                                  error_date)
   select RPM_STAGE_ITEM_LOC_SEQ.NEXTVAL,
          I_item,
          I_loc,
          I_loc_type,
          I_selling_unit_retail,
          I_selling_uom,
          'N',
          LP_vdate,
          NULL,
          NULL
     from dual;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_NEW_ITEM_LOC_SQL.STAGE_ITEM_LOC',
                                        to_char(SQLCODE));
      return 0;
END STAGE_ITEM_LOC;
-------------------------------------------------------------------

FUNCTION GET_ITEMLOC_ID(I_is_next   IN     NUMBER)
RETURN NUMBER IS

   L_itemloc_id           NUMBER    := NULL;

BEGIN

   if I_is_next = 1 then
      select RPM_BULK_CC_PE_ITEMLOC_SEQ.nextval
        into L_itemloc_id
        from dual;
   else
      select RPM_BULK_CC_PE_ITEMLOC_SEQ.currval
        into L_itemloc_id
        from dual;
   end if;

   return L_itemloc_id;

END GET_ITEMLOC_ID;

--------------------------------------------------------

FUNCTION THREAD_ROLLUP_FOR_NIL(O_error_msg            OUT VARCHAR2,
                               O_max_thread_number    OUT NUMBER,
                               I_process_id        IN     NUMBER,
                               I_luw               IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_NEW_ITEM_LOC_SQL.THREAD_ROLLUP_FOR_NIL';

   L_max_chunk  NUMBER    := 0;
   L_max_thread NUMBER    := 0;
   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id);

	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting thread rollup for NIL');


   EXCEPTION

      when OTHERS then
         goto THREAD_ROLLUP_FOR_NIL_1;

   END;

   <<THREAD_ROLLUP_FOR_NIL_1>>
   
   select MAX(thread_num)
     into L_max_chunk
     from rpm_stage_item_loc_clean;

   if L_max_chunk > 0 then

      for i IN 1..L_max_chunk loop

         insert into rpm_nil_rollup_thread (dept,
                                            item,
                                            location,
                                            item_level,
                                            zone_node_type,
                                            thread_number,
                                            process_id,
                                            status)
         select dept,
                item,
                location,
                item_level,
                zone_node_type,
                (thread_number + L_max_thread),
                I_process_id,
                'N'
          from (select distinct dept,
                       item_parent item,
                       zone_id location,
                       item_level,
                       zone_node_type,
                       COUNT(1) OVER (PARTITION BY item_parent,
                                                   zone_id) iloc_count
                  from (with silc as
                           (select dept,
                                   item,
                                   item_parent,
                                   loc,
                                   loc_type,
                                   zone_id
                              from rpm_stage_item_loc_clean
                             where thread_num = i
                               and process_id = I_process_id)
                        -- Parent Zone
                        select distinct im.dept,
                               im.item,
                               im.item_parent,
                               RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL item_level,
                               rzl.location,
                               rzl.zone_id,
                               RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                          from silc,
                               item_master im,
                               rpm_zone_location rzl,
                               rpm_item_loc ril
                         where silc.item_parent is NOT NULL
                           and silc.item_parent = im.item_parent
                           and silc.zone_id     is NOT NULL
                           and silc.zone_id     = rzl.zone_id
                           and ril.dept         = silc.dept
                           and ril.item         = im.item
                           and ril.loc          = rzl.location
                        union all
                        -- Parent Loc
                        select distinct im.dept,
                               im.item,
                               im.item_parent,
                               RPM_CONSTANTS.IL_PARENT_ITEM_LEVEL item_level,
                               silc.loc location,
                               silc.loc zone_id,
                               DECODE(silc.loc_type,
                                      RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type
                          from silc,
                               item_master im,
                               rpm_item_loc ril
                         where silc.item_parent is NOT NULL
                           and silc.item_parent = im.item_parent
                           and silc.zone_id     is NULL
                           and ril.dept         = silc.dept
                           and ril.item         = im.item
                           and ril.loc          = silc.loc
                        union all
                        -- Item Zone
                        select distinct silc.dept,
                               silc.item,
                               silc.item item_parent,
                               RPM_CONSTANTS.IL_ITEM_LEVEL item_level,
                               rzl.location,
                               rzl.zone_id,
                               RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE zone_node_type
                          from silc,
                               rpm_zone_location rzl,
                               rpm_item_loc ril
                         where silc.item_parent is NULL
                           and silc.zone_id     is NOT NULL
                           and silc.zone_id     = rzl.zone_id
                           and ril.dept         = silc.dept
                           and ril.item         = silc.item
                           and ril.loc          = rzl.location
                        union all
                        -- Item Loc
                        select silc.dept,
                               silc.item,
                               silc.item item_parent,
                               RPM_CONSTANTS.IL_ITEM_LEVEL item_level,
                               silc.loc location,
                               silc.loc zone_id,
                               DECODE(silc.loc_type,
                                      RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                      RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type
                          from silc
                         where silc.item_parent is NULL
                           and silc.zone_id     is NULL))
                          model
                             dimension by (ROW_NUMBER() OVER (ORDER BY dept,
                                                                       item,
                                                                       location) rn)
                             measures (iloc_count,
                                       dept,
                                       item,
                                       item_level,
                                       location,
                                       zone_node_type,
                                       1 running_total,
                                       1 thread_number)
                             rules (running_total[rn] =
                                       case
                                          when CV(rn) = 1 then
                                             iloc_count[CV(rn)]
                                          else
                                             case
                                                when (item[CV(rn)] = item[CV(rn) - 1] and
                                                      location[CV(rn)] = location[CV(rn) - 1]) then
                                                   running_total[CV(rn) - 1]
                                                else
                                                   case
                                                      when iloc_count[CV(rn)] + running_total[CV(rn) - 1] > I_luw then
                                                         iloc_count[CV(rn)]
                                                      else
                                                         iloc_count[CV(rn)] + running_total[CV(rn) - 1]
                                                   end
                                             end
                                       end,
                                    thread_number[rn] =
                                       case
                                          when CV(rn) = 1 then
                                             1
                                          else
                                             case
                                                when (item[CV(rn)] = item[CV(rn) - 1] and
                                                      location[CV(rn)] = location[CV(rn) - 1]) then
                                                   thread_number[CV(rn) - 1]
                                                else
                                                   case
                                                      when (iloc_count[CV(rn)] + running_total[CV(rn) - 1]) > I_luw then
                                                         thread_number[CV(rn) - 1] + 1
                                                      else
                                                         thread_number[CV(rn) - 1]
                                                   end
                                             end
                                       end);

         select MAX(thread_number)
           into L_max_thread
           from rpm_nil_rollup_thread;

      end loop;

   end if;

   O_max_thread_number := L_max_thread;

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending thread rollup for NIL');

   EXCEPTION

      when OTHERS then
         goto THREAD_ROLLUP_FOR_NIL_2;

   END;

   <<THREAD_ROLLUP_FOR_NIL_2>>
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END THREAD_ROLLUP_FOR_NIL;

--------------------------------------------------------

FUNCTION ROLLUP_NIL_DATA(O_error_msg        OUT VARCHAR2,
                         I_process_id    IN     NUMBER,
                         I_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program    VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.ROLLUP_NIL_DATA';
   L_start_time TIMESTAMP    := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_process_id: '|| I_process_id ||
                                      ' - I_thread_number: '|| I_thread_number);
                                      
	 --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_STARTED,
                                        'Starting rollup NIL data for thread - '||I_thread_number);


   EXCEPTION

      when OTHERS then
         goto ROLLUP_NIL_DATA_1;

   END;

   <<ROLLUP_NIL_DATA_1>>

   if RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ROLLUP_FR_FOR_NIL(O_error_msg,
                                                            I_thread_number,
                                                            I_process_id) = 0 then
      return 0;
   end if;

   if RPM_ROLLUP.ROLL_FUTURE_RETAIL_FOR_NIL(O_error_msg) = 0 then
      return 0;
   end if;

   if UPDATE_NIL_THREAD_STATUS(O_error_msg,
                               I_process_id,
                               I_thread_number,
                               'C') = 0 then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program ||' - I_process_id: '|| I_process_id ||
                               ' - I_thread_number: '|| I_thread_number,
                   L_start_time);

   --Instrumentation
   BEGIN

      RPM_APP_COMM_UTILITY_SQL.LOG_THIS(LP_return_code,
                                        LP_error_msg,
                                        RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                        RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                        RPM_CONSTANTS.BATCH_NAME_NIL,
                                        L_program||'-'||I_thread_number,
                                        RPM_CONSTANTS.LOG_COMPLETED,
                                        'Ending rollup NIL data for thread - '||I_thread_number);

   EXCEPTION

      when OTHERS then
         goto ROLLUP_NIL_DATA_2;

   END;

   <<ROLLUP_NIL_DATA_2>>
   
   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END ROLLUP_NIL_DATA;

--------------------------------------------------------

FUNCTION INITIALIZE_RESTART(O_error_msg         OUT VARCHAR2,
                            O_thread_numbers    OUT OBJ_NUMERIC_ID_TABLE,
                            I_process_id     IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_NEW_ITEM_LOC_SQL.INITIALIZE_RESTART';

   cursor C_GET_THREAD_NUMS is
      select distinct thread_number
        from rpm_nil_rollup_thread
       where process_id = I_process_id
         and status    != 'C';

BEGIN

   open C_GET_THREAD_NUMS;
   fetch C_GET_THREAD_NUMS BULK COLLECT into O_thread_numbers;
   close C_GET_THREAD_NUMS;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END INITIALIZE_RESTART;

--------------------------------------------------------

FUNCTION UPDATE_NIL_THREAD_STATUS(O_error_msg        OUT VARCHAR2,
                                  I_process_id    IN     NUMBER,
                                  I_thread_number IN     NUMBER,
                                  I_status        IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_NEW_ITEM_LOC_SQL.UPDATE_NIL_THREAD_STATUS';

BEGIN

   update rpm_nil_rollup_thread
      set status     = I_status,
          start_date = DECODE(I_status,
                              'C', start_date,
                              SYSDATE)
    where process_id    = I_process_id
      and thread_number = I_thread_number;

   return 1;

EXCEPTION
   WHEN OTHERS THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END UPDATE_NIL_THREAD_STATUS;

--------------------------------------------------------
PROCEDURE ANALYZE_STAGE_CLEAN_TABLE(O_return_code      OUT NUMBER,
                                    O_error_message    OUT VARCHAR2,
                                    I_schema_owner  IN     VARCHAR2)
AS
   L_program VARCHAR2(50) := 'RPM_NEW_ITEM_LOC_SQL.ANALYZE_STAGE_CLEAN_TABLE';
BEGIN
   DBMS_STATS.GATHER_TABLE_STATS(I_schema_owner,              --ownname
                                 'RPM_STAGE_ITEM_LOC_CLEAN',  --tabname
                                 NULL,                        --partname
                                 DBMS_STATS.AUTO_SAMPLE_SIZE, --estimate_percent (constant = 0)
                                 FALSE,                       --block_sample
                                 'FOR ALL COLUMNS SIZE AUTO', --method_opt
                                 16,                          --degree
                                 'AUTO',                      --granularity
                                 TRUE);                       --cascade
   O_return_code := 1;
EXCEPTION
   WHEN OTHERS THEN
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      O_return_code := 0;
END ANALYZE_STAGE_CLEAN_TABLE;
----------------------------------------------------------------------------

END RPM_NEW_ITEM_LOC_SQL;
/
