CREATE OR REPLACE PACKAGE RPM_ZONE_FUTURE_RETAIL_SQL AS
-------------------------------------------------------
FUNCTION PRICE_CHANGE_INSERT(O_cc_error_tbl        OUT conflict_check_error_tbl,
                             I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------------
FUNCTION PRICE_CHANGE_REMOVE(O_cc_error_tbl         OUT conflict_check_error_tbl,
                             I_price_change_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------------

END RPM_ZONE_FUTURE_RETAIL_SQL;
/

