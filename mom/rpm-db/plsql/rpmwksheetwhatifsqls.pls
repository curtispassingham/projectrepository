CREATE OR REPLACE PACKAGE RPM_WORKSHEET_WHATIF_SQL AS
--------------------------------------------------------------------------------------
   -- Strategy Type
   AREA_DIFFERENTIAL    CONSTANT VARCHAR2(25) := 'areaDifferential';
   CLEARANCE            CONSTANT VARCHAR2(25) := 'clearance';
   CLEARANCE_DEFAULTS   CONSTANT VARCHAR2(25) := 'clearancedefaults';
   COMPETITIVE          CONSTANT VARCHAR2(25) := 'competitive';
   MAINTAIN_MARGIN      CONSTANT VARCHAR2(25) := 'maintainmargin';
   MARGIN               CONSTANT VARCHAR2(25) := 'margin';

   -- Markdown Basis
   REGULAR_PRICE        CONSTANT NUMBER(1) := 0;
   LAST_CLEARANCE_PRICE CONSTANT NUMBER(1) := 1;

--------------------------------------------------------------------------------------
FUNCTION APPLY_STRATEGY(O_error_msg           IN OUT VARCHAR2,
                        I_workspace_id        IN     NUMBER,
                        I_whatif_rollup_id    IN     OBJ_WHATIF_ROLLUP_ID_TBL,
                        I_whatif_strategy     IN     OBJ_WHATIF_STRATEGY_TBL,
                        I_whatif_strategy_clr IN     OBJ_WHATIF_STRATEGY_CLR_TBL,
                        I_whatif_strategy_dtl IN     OBJ_WHATIF_STRATEGY_DTL_TBL)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION UOM_CONVERT_VALUE (I_item        IN     ITEM_MASTER.ITEM%TYPE,
                            I_input_value IN     ITEM_LOC.UNIT_RETAIL%TYPE,
                            I_from_uom    IN     ITEM_MASTER.STANDARD_UOM%TYPE,
                            I_to_uom      IN     ITEM_MASTER.STANDARD_UOM%TYPE)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION CALC_CLEAR_RETAIL(I_basis_retail        IN     NUMBER,
                           I_markdown_number     IN     NUMBER,
                           I_whatif_strategy_clr IN     OBJ_WHATIF_STRATEGY_CLR_TBL,
                           I_price_guide_id      IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION CALC_MARGIN_RETAIL(I_basis_zl_base_cost      IN     NUMBER,
                            I_basis_zl_regular_retail IN     NUMBER,
                            I_markup_calc_type        IN     NUMBER,
                            I_percent                 IN     NUMBER,
                            I_from_percent            IN     NUMBER,
                            I_to_percent              IN     NUMBER,
                            I_price_guide_id          IN     NUMBER,
                            I_retail_include_vat_ind  IN     VARCHAR2,
                            I_vat_rate                IN     NUMBER,
                            I_vat_value               IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------
FUNCTION CALC_COMPETITIVE_RETAIL(I_whatif_strategy_dtl      IN     OBJ_WHATIF_STRATEGY_DTL_TBL,
                                 I_compete_type             IN     NUMBER,
                                 I_item                     IN     VARCHAR2,
                                 I_basis_zl_regular_retail  IN     NUMBER,
                                 I_basis_regular_retail     IN     NUMBER,
                                 I_basis_regular_retail_uom IN     VARCHAR2,
                                 I_primary_comp_retail      IN     NUMBER,
                                 I_primary_comp_retail_uom  IN     VARCHAR2,
                                 I_mkt_basket_code          IN     VARCHAR2,
                                 I_price_guide_id           IN     NUMBER)
RETURN NUMBER;
-------------------------------------------------------------------------------

END RPM_WORKSHEET_WHATIF_SQL;
/
