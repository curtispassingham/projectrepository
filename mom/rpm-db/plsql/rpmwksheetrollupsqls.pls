CREATE OR REPLACE PACKAGE RPM_WORKSHEET_ROLLUP_SQL AS

-- WORKSHEET ROLLUP LEVEL 
   TRAN_LEVEL                CONSTANT NUMBER(1) := 0;
   PARENT_LEVEL              CONSTANT NUMBER(1) := 1;
   PARENT_DIFF1_LEVEL        CONSTANT NUMBER(1) := 2;
   PARENT_DIFF2_LEVEL        CONSTANT NUMBER(1) := 3;        
   PARENT_DIFF3_LEVEL        CONSTANT NUMBER(1) := 4;
   PARENT_DIFF4_LEVEL        CONSTANT NUMBER(1) := 5;
   DIFF_TYPE_LEVEL           CONSTANT NUMBER(1) := 6;
   LINK_CODE_LEVEL           CONSTANT NUMBER(1) := 7;

-- WORKSHEET STATE 
   STATE_INT_NEW             CONSTANT NUMBER(1) := 1;
   STATE_INT_UPDATED         CONSTANT NUMBER(1) := 2;
   STATE_INT_IN_PROGRESS     CONSTANT NUMBER(1) := 3;
   STATE_INT_SUBMITTED       CONSTANT NUMBER(1) := 4;
   
-- AREA DIFF APPLY PERCENT TYPE 
   HIGHER_VALUE              CONSTANT NUMBER(1) := 0;
   LOWER_VALUE               CONSTANT NUMBER(1) := 1;
   EQUAL_VALUE               CONSTANT NUMBER(1) := 2;

-- COMPETE TYPE 
   MATCH                     CONSTANT NUMBER(1) := 0;
   PRICE_ABOVE               CONSTANT NUMBER(1) := 1;
   PRICE_BELOW               CONSTANT NUMBER(1) := 2;
   PRICE_BY_CODE             CONSTANT NUMBER(1) := 3;
   
FUNCTION UPDATE_ROLLUP_MODS(O_error_msg            IN OUT VARCHAR2,
                            I_obj_rollup_mods_tbl  IN     OBJ_ROLLUP_MODS_TBL,
                            I_dept                 IN     DEPS.DEPT%TYPE)
RETURN NUMBER;

FUNCTION UPDATE_ROLLUP_MODS1(O_error_msg                     IN OUT VARCHAR2,
                             I_obj_rollup_mods_parent_lvl    IN     OBJ_ROLLUP_MODS_TBL,
                             I_obj_rollup_mods_difftype_lvl  IN     OBJ_ROLLUP_MODS_TBL,
                             I_obj_rollup_mods_link_lvl      IN     OBJ_ROLLUP_MODS_TBL,
                             I_dept                          IN     DEPS.DEPT%TYPE,
                             I_workspace_id                  IN     NUMBER,
                             I_updateable_columns            IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER;

FUNCTION UPDATE_DATA_FROM_WORKSPACE(O_error_msg       IN OUT VARCHAR2,
                                    I_workspace_id    IN     NUMBER)
RETURN NUMBER;

FUNCTION UPDATE_SECONDARY_WORKSHEET(O_error_msg            IN OUT VARCHAR2,
                                    I_obj_rollup_mods_tbl  IN     OBJ_ROLLUP_MODS_TBL)
RETURN NUMBER;

FUNCTION CALC_SECONDARY_RETAIL(I_area_diff_prim_id         IN   NUMBER,
                               I_area_diff_id              IN   NUMBER,
                               I_retail_in                 IN   NUMBER,
                               I_currency_in               IN   VARCHAR2,
                               I_currency_out              IN   VARCHAR2,
                               I_percent_apply_type        IN   NUMBER,
                               I_percent                   IN   NUMBER, 
                               I_price_guide_id            IN   NUMBER,
                               I_primary_comp_retail       IN   NUMBER,
                               I_primary_comp_store        IN   NUMBER, 
                               I_primary_comp_retail_uom   IN   VARCHAR2, 
                               I_primary_comp_retail_std   IN   NUMBER,
                               I_basis_zl_regular_retail   IN   NUMBER,
                               I_comp_mkt_basket_code      IN   VARCHAR2)				 				 
RETURN NUMBER;

END RPM_WORKSHEET_ROLLUP_SQL;
/
