CREATE OR REPLACE PACKAGE BODY RPM_EXT_SQL AS
----------------------------------------------------------------------------------------

   LP_vdate              DATE    := NULL;
   LP_tomorrow           DATE    := NULL;
   LP_first_day_of_month DATE    := NULL;
   LP_last_eow_date_unit DATE    := NULL;
   LP_last_eom_date_unit DATE    := NULL;
   LP_review_not_started BOOLEAN := NULL;
   LP_do_update          BOOLEAN := NULL;

   -- Variable to store the Strategy type
   LP_strategy_type    VARCHAR2(20) := NULL;
   LP_maint_margin_ind NUMBER(1)    := 0;

   -- Variables specific to Maintain Margin Strategies.
   LP_crp_start DATE := NULL;
   LP_crp_end   DATE := NULL;

   -- Variables to store System Options
   LP_sales_calc_method      RPM_SYSTEM_OPTIONS.SALES_CALCULATION_METHOD%TYPE     := NULL;
   LP_cost_calc_method       RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE      := NULL;
   LP_dynamic_area_diff_ind  RPM_SYSTEM_OPTIONS.DYNAMIC_AREA_DIFF_IND%TYPE        := NULL;
   LP_price_change_proc_days RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROCESSING_DAYS%TYPE := NULL;

   -- Variables for Dept level info
   LP_regular_sales_ind     RPM_DEPT_AGGREGATION.REGULAR_SALES_IND%TYPE             := NULL;
   LP_clearance_sales_ind   RPM_DEPT_AGGREGATION.CLEARANCE_SALES_IND%TYPE           := NULL;
   LP_promotional_sales_ind RPM_DEPT_AGGREGATION.PROMOTIONAL_SALES_IND%TYPE         := NULL;
   LP_time_frame            RPM_DEPT_AGGREGATION.HISTORICAL_SALES_LEVEL%TYPE        := NULL;
   LP_pend_cost_chg_window  RPM_DEPT_AGGREGATION.PEND_COST_CHG_WINDOW_DAYS%TYPE     := NULL;
   LP_include_wh_on_hand    RPM_DEPT_AGGREGATION.INCLUDE_WH_ON_HAND%TYPE            := NULL;
   LP_include_wh_on_order   RPM_DEPT_AGGREGATION.INCLUDE_WH_ON_ORDER%TYPE           := NULL;
   LP_pc_amount_calc_type   RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE := NULL;
   LP_worksheet_level       RPM_DEPT_AGGREGATION.WORKSHEET_LEVEL%TYPE               := NULL;

   --determines if item/loc removals are recorded in MERCH_EXTRACT_DELETIONS
   LP_record_item_loc_removes  BOOLEAN                       := TRUE;
   LP_record_item_loc_strategy RPM_STRATEGY.STRATEGY_ID%TYPE := NULL;

   -- Eventually, clients should be able to set these based on what drives their pricing
   -- decisions eliminating expensive sql that is not needed.
   LP_uda_process_ind            BOOLEAN := TRUE;
   LP_season_process_ind         BOOLEAN := TRUE;
   LP_on_order_process_ind       BOOLEAN := TRUE;
   LP_comp_process_ind           BOOLEAN := TRUE;
   LP_proj_sales_process_ind     BOOLEAN := TRUE;
   LP_hist_sales_process_ind     BOOLEAN := TRUE;
   LP_repl_ind_process_ind       BOOLEAN := TRUE;
   LP_seasonal_sales_process_ind BOOLEAN := TRUE;
   LP_clear_dates_process_ind    BOOLEAN := TRUE;

   -- Area differential Primary and Secondary IDs
   LP_area_diff_prim_id    RPM_WORKSHEET_ITEM_DATA.AREA_DIFF_PRIM_ID%TYPE := NULL;
   LP_area_diff_id         RPM_WORKSHEET_ITEM_DATA.AREA_DIFF_ID%TYPE      := NULL;
   LP_area_diff_prim_ws_id RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE  := NULL;

   -- Global so they will still be populated for secondary area calls to EXTRACT
   LP_exclusion_ind RPM_CALENDAR_PERIOD.EXCLUSION_IND%TYPE := NULL;
   LP_exception_ind RPM_CALENDAR_PERIOD.EXCEPTION_IND%TYPE := NULL;
   LP_review_date   DATE                                   := NULL;

   -- Private function declarations
FUNCTION INIT_PRE_PROCESS(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_STRATEGY(O_error_message       OUT VARCHAR2,
                              I_aggregations     IN     OBJ_ME_AGGREGATION_TBL,
                              I_aggregation_type IN     RPM_PRE_ME_AGGREGATION.AGGREGATION_TYPE%TYPE)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_ITEM_LOC_HIGH(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_COST_HIGH(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_RETAIL_HIGH(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_ITEM_LOC_LOW(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_COST_LOW(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_RETAIL_LOW(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_ITEM_LOC_MEDIUM(O_error_message     OUT VARCHAR2,
                                     I_item_thread_id IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                                     I_thread_number  IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_COST_MEDIUM(O_error_message     OUT VARCHAR2,
                                 I_item_thread_id IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                                 I_thread_number  IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;

FUNCTION PRE_PROCESS_RETAIL_MEDIUM(O_error_message     OUT VARCHAR2,
                                   I_item_thread_id IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                                   I_thread_number  IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN;

FUNCTION REFRESH_DATA_STRUCTURES(O_error_msg    IN OUT VARCHAR2,
                                 I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION GET_DEPT_AGG(O_error_msg IN OUT VARCHAR2,
                      I_dept      IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE)
RETURN BOOLEAN;

FUNCTION POPULATE_STRATEGY_TYPE(O_error_msg   IN OUT VARCHAR2,
                                I_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE)
RETURN BOOLEAN;

FUNCTION INT_TO_BOOLEAN(I_number NUMBER)
RETURN BOOLEAN;

FUNCTION GET_CALENDAR_INFO(O_error_msg     IN OUT VARCHAR2,
                           I_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                           O_exclusion_ind    OUT RPM_CALENDAR_PERIOD.EXCLUSION_IND%TYPE,
                           O_exception_ind    OUT RPM_CALENDAR_PERIOD.EXCLUSION_IND%TYPE,
                           O_review_date      OUT DATE)
RETURN BOOLEAN;

FUNCTION GET_MAINT_MARGIN_INFO(O_error_msg   IN OUT VARCHAR2,
                               O_crp_start   IN OUT DATE,
                               O_crp_end     IN OUT DATE,
                               I_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE)
RETURN BOOLEAN;

FUNCTION INIT_RPM_EXT_DATA(O_error_msg    IN OUT VARCHAR2,
                           I_strategy_id  IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                           I_initial_call IN     VARCHAR2,
                           I_dept         IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                           I_class        IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                           I_subclass     IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                           I_zone_id      IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                           I_location     IN     RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE)
RETURN BOOLEAN;

FUNCTION POPULATE_BASIC_ITEM_INFO(O_error_msg    IN OUT VARCHAR2,
                                  I_strategy_id  IN     RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE,
                                  I_dept         IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                                  I_class        IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                                  I_subclass     IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                                  I_zone_id      IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                                  I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_UDA(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_SEASON(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_PARENT_INFO(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_COMP_RETAIL(O_error_msg   IN OUT VARCHAR2,
                              I_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE)
RETURN BOOLEAN;

FUNCTION POPULATE_COMP_RETAIL_AREA_DIFF(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_REF_WH_STOCK(O_error_msg    IN OUT VARCHAR2,
                               I_strategy_id  IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                               I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_REF_WH_ON_ORDER(O_error_msg    IN OUT VARCHAR2,
                                  I_strategy_id  IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                  I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_ITEM_LOC(O_error_msg           IN OUT VARCHAR2,
                           I_strategy_id         IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                           I_me_sequence_id      IN     RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                           I_me_area_diff_seq_id IN     RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE,
                           I_zone_id             IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                           I_eff_date            IN     DATE,
                           I_initial_call        IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_IL_STAT_RPM_IND(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_IL_STAT_RPM_IND_BATCH(O_error_msg           IN OUT VARCHAR2,
                                        I_me_sequence_id      IN     RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                                        I_me_area_diff_seq_id IN     RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE)
RETURN BOOLEAN;

FUNCTION REMOVE_AREA_DIFF_EXCLUDES(O_error_msg    IN OUT VARCHAR2,
                                   I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_ITEM_COST(O_error_msg IN OUT VARCHAR2,
                            I_curr_date IN     DATE,
                            I_eff_date  IN     DATE,
                            I_override  IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_ITEM_COST_BATCH(O_error_msg           IN OUT VARCHAR2,
                                  I_me_sequence_id      IN     RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                                  I_me_area_diff_seq_id IN     RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE,
                                  I_curr_date           IN     DATE,
                                  I_eff_date            IN     DATE,
                                  I_override            IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_PRICING_INFO(O_error_msg  IN OUT  VARCHAR2,
                               I_curr_date  IN      DATE,
                               I_dept       IN      DEPS.DEPT%TYPE)
RETURN BOOLEAN;

FUNCTION POPULATE_PRICING_INFO_BATCH(O_error_msg           IN OUT VARCHAR2,
                                     I_me_sequence_id      IN     RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                                     I_me_area_diff_seq_id IN     RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE,
                                     I_curr_date           IN     DATE,
                                     I_dept                IN     DEPS.DEPT%TYPE,
                                     I_zone_id             IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE)
RETURN BOOLEAN;

FUNCTION POPULATE_ITEM_PRICING_INFO(O_error_msg IN OUT VARCHAR2,
                                    I_curr_date IN     DATE)
RETURN BOOLEAN;

FUNCTION REMOVE_COST(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION REMOVE_RETAIL(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_REPL_AND_SALES_INFO(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_OTHER_LOC_INFO(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_VAT(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_ON_ORDER(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION CONSOLIDATE_GTT(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION CONSOLIDATE_ITEM_LOC(O_error_msg    IN OUT VARCHAR2,
                              I_zone_id      IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                              I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION CONSOLIDATE_ITEM_ROLLUP (O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION POPULATE_PROPOSED(O_error_msg    IN OUT VARCHAR2,
                           I_zone_id      IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                           I_initial_call IN     VARCHAR2)
RETURN BOOLEAN;

FUNCTION PROPOSE_UOM_PKG_CALL(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION UOM_VAT_PKG_CALLS(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION FINISH_RPM_LUW(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION UPDATE_WORKSPACE(O_error_msg       OUT  VARCHAR2,
                          I_workspace_id IN      RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE)
RETURN BOOLEAN;

FUNCTION FINISH_WORKSHEET_STATUS(O_error_msg              OUT VARCHAR2,
                                 I_strategy_id         IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                 I_dept                IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                                 I_class               IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                                 I_subclass            IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                                 I_zone_id             IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                                 I_worksheet_level     IN     RPM_DEPT_AGGREGATION.worksheet_level%TYPE,
                                 I_include_wh_on_hand  IN     RPM_DEPT_AGGREGATION.include_wh_on_hand%TYPE,
                                 I_pc_amount_calc_type IN     RPM_DEPT_AGGREGATION.price_change_amount_calc_type%TYPE,
                                 I_maint_margin_ind    IN     NUMBER)
RETURN BOOLEAN;

FUNCTION INSERT_WS_ZONE_DATA(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN;

FUNCTION RUN_AREA_DIFF(O_error_msg         IN OUT VARCHAR2,
                       I_me_sequence_id    IN     NUMBER,
                       I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                       I_dept              IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                       I_class             IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                       I_subclass          IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                       I_zone_id           IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                       I_location          IN     RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE,
                       I_review_start_date IN     DATE,
                       I_eff_date          IN     DATE)
RETURN BOOLEAN;

FUNCTION INIT_WORKSHEET_RESET(O_error_msg           IN OUT VARCHAR2,
                              O_effective_date      IN OUT RPM_WORKSHEET_ITEM_LOC_DATA.EFFECTIVE_DATE%TYPE,
                              O_zone_id             IN OUT RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                              I_worksheet_status_id IN     RPM_WORKSHEET_ITEM_DATA.WORKSHEET_STATUS_ID%TYPE,
                              I_workspace_id        IN     RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE,
                              I_strategy_id         IN     RPM_WORKSHEET_ZONE_WORKSPACE.PROPOSED_STRATEGY_ID%TYPE)
RETURN BOOLEAN;

FUNCTION INIT_EFFECTIVE_DATE_UPDATE(O_error_msg IN OUT VARCHAR2,
                                    I_edut      IN     OBJ_EFF_DATES_UPD_TBL)
RETURN BOOLEAN;

FUNCTION WORKSHEET_NOT_TAKE_ROLLUP(O_error_msg              OUT VARCHAR2,
                                   I_worksheet_status_id IN     RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                                   I_dept                IN     RPM_WORKSHEET_STATUS.DEPT%TYPE,
                                   I_include_wh_on_hand  IN     RPM_DEPT_AGGREGATION.INCLUDE_WH_ON_HAND%TYPE,
                                   I_pc_amount_calc_type IN     RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE,
                                   O_stock_on_hand          OUT RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE,
                                   O_price_change_amount    OUT RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE,
                                   O_sales_amount           OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE,
                                   O_sales_amount_ex_vat    OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE,
                                   O_sales_cost             OUT RPM_WORKSHEET_STATUS.SALES_COST%TYPE,
                                   O_sales_change_amount    OUT RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE,
                                   O_item_count             OUT RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE)
RETURN BOOLEAN;

FUNCTION WORKSHEET_TAKE_ROLLUP(O_error_msg              OUT VARCHAR2,
                               I_worksheet_status_id IN     RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                               I_dept                IN     RPM_WORKSHEET_STATUS.DEPT%TYPE,
                               I_include_wh_on_hand  IN     RPM_DEPT_AGGREGATION.include_wh_on_hand%TYPE,
                               I_pc_amount_calc_type IN     RPM_DEPT_AGGREGATION.price_change_amount_calc_type%TYPE,
                               O_stock_on_hand          OUT RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE,
                               O_price_change_amount    OUT RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE,
                               O_sales_amount           OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE,
                               O_sales_amount_ex_vat    OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE,
                               O_sales_cost             OUT RPM_WORKSHEET_STATUS.SALES_COST%TYPE,
                               O_sales_change_amount    OUT RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE,
                               O_item_count             OUT RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE)
RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY (O_error_msg           OUT VARCHAR2,
                              I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                              I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )
RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY_DETAIL (O_error_msg           OUT VARCHAR2,
                                     I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                     I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )
RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY_REFCOMP (O_error_msg           OUT VARCHAR2,
                                      I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                      I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )

RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY_CLEARANCE (O_error_msg           OUT VARCHAR2,
                                        I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                        I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )

RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY_COMPETITIV (O_error_msg           OUT VARCHAR2,
                                         I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                         I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )

RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY_MARGIN (O_error_msg           OUT VARCHAR2,
                                     I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                     I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )

RETURN BOOLEAN;

FUNCTION COPY_PRICE_STRATEGY_MNT_MARGIN (O_error_msg           OUT VARCHAR2,
                                         I_from_strategy_id IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                         I_to_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE )

RETURN BOOLEAN;


----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
--start pre-process logic
----------------------------------------------------------------------------------------

FUNCTION GET_PRE_PROCESS_INFO(O_error_message              OUT VARCHAR2,
                              I_worksheet_process_agg   IN     OBJ_ME_AGGREGATION_TBL,
                              I_worksheet_create_aggs   IN     OBJ_ME_AGGREGATION_TBL,
                              O_pre_process_method         OUT VARCHAR2,
                              O_item_thread_id             OUT RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                              O_pre_process_num_threads    OUT RPM_PRE_PROCESS_CONFIG.PRE_PROCESS_NUM_THREADS%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.GET_PRE_PROCESS_INFO';

   L_total_count   NUMBER(15)    := 0;
   L_process_count NUMBER(15)    := 0;
   L_process_pct   NUMBER(12, 4) := NULL;

   L_high_pct    RPM_PRE_PROCESS_CONFIG.PRE_PROCESS_HIGH_PERCENT%TYPE := NULL;
   L_low_pct     RPM_PRE_PROCESS_CONFIG.PRE_PROCESS_LOW_PERCENT%TYPE  := NULL;
   L_num_threads RPM_PRE_PROCESS_CONFIG.PRE_PROCESS_NUM_THREADS%TYPE  := NULL;

   cursor C_PERCENTS is
      select pre_process_high_percent,
             pre_process_low_percent,
             pre_process_num_threads
        from rpm_pre_process_config;

   cursor C_TOTAL_COUNT is
      select i.cnt * l.cnt
        from (select COUNT(*) cnt
                from item_master im
               where im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                 and im.item_level   = im.tran_level
                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES) i,
             (select SUM(recs) cnt
                from (select COUNT(*) recs
                        from store
                       where stockholding_ind = 'Y'
                      union all
                      select COUNT(*) recs
                        from wh
                       where stockholding_ind = 'Y')) l;

   cursor C_PROCESS_COUNT is
      select SUM(loc_count * item_count)
        from (select COUNT(*) loc_count,
                     i_count.item_count,
                     i_count.strategy_id,
                     i_count.dept,
                     i_count.class,
                     i_count.subclass,
                     i_count.zone_id,
                     i_count.me_sequence_id
                from (select /*+ CARDINALITY(agg 2000) */
                             COUNT(*) item_count,
                             agg.strategy_id,
                             agg.dept,
                             agg.class,
                             agg.subclass,
                             agg.zone_id,
                             agg.me_sequence_id
                        from table(cast(I_worksheet_process_agg as OBJ_ME_AGGREGATION_TBL)) agg,
                             item_master im
                       where im.dept         = agg.dept
                         and im.class        = NVL(agg.class, im.class)
                         and im.subclass     = NVL(agg.subclass, im.subclass)
                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                         and im.item_level   = im.tran_level
                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                      group by agg.strategy_id,
                               agg.dept,
                               agg.class,
                               agg.subclass,
                               agg.zone_id,
                               agg.me_sequence_id) i_count,
                     rpm_zone_location rl
               where rl.zone_id = i_count.zone_id
               group by i_count.item_count,
                        i_count.strategy_id,
                        i_count.dept,
                        i_count.class,
                        i_count.subclass,
                        i_count.zone_id,
                        i_count.me_sequence_id);

BEGIN

   if PRE_PROCESS_STRATEGY(O_error_message,
                           I_worksheet_process_agg,
                           RPM_EXT_SQL.WORKSHEET_PROCESS_LEVEL) = FALSE then
      return 0;
   end if;

   if PRE_PROCESS_STRATEGY(O_error_message,
                           I_worksheet_create_aggs,
                           RPM_EXT_SQL.WORKSHEET_CREATE_LEVEL) = FALSE then
      return 0;
   end if;

   open C_PERCENTS;
   fetch C_PERCENTS into L_high_pct,
                         L_low_pct,
                         L_num_threads;
   close C_PERCENTS;

   open C_TOTAL_COUNT;
   fetch C_TOTAL_COUNT into L_total_count;
   close C_TOTAL_COUNT;

   open C_PROCESS_COUNT;
   fetch C_PROCESS_COUNT into L_process_count;
   close C_PROCESS_COUNT;

   L_process_pct := L_process_count / L_total_count;

   if ( L_process_pct > L_high_pct) then

      O_pre_process_method := HIGH_VOLUME;
      O_pre_process_num_threads := 1;
      O_item_thread_id := -1;

   elsif ((L_process_pct <= L_high_pct) and
          (L_process_pct >= L_low_pct)) then

      O_pre_process_method      := MEDIUM_VOLUME;
      O_pre_process_num_threads := L_num_threads;
      O_item_thread_id          := RPM_ITEM_THREAD_SEQ.NEXTVAL;

     --populate rpm_item_thread--
     insert into rpm_item_thread
        select O_item_thread_id,
               ceil(t2.seq/(t2.total/pre.pre_process_num_threads)),
               t2.item
          from ( select t1.item,
                        SUM(t1.one) OVER (order by t1.item) seq,
                        COUNT(t1.one) OVER() total
                   from (select /*+ INDEX(im item_master_i3) */
                                distinct im.item,
                                1 one
                           from item_master im,
                                RPM_PRE_ME_AGGREGATION me
                          where im.dept         = me.dept
                            and im.class        = NVL(me.class, im.class)
                            and im.subclass     = NVL(me.subclass, im.subclass)
                            and im.tran_level   = im.item_level
                            and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                            and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                          order by im.item) t1) t2,
               rpm_pre_process_config pre;

   else

      O_pre_process_method      := LOW_VOLUME;
      O_pre_process_num_threads := 1;
      O_item_thread_id          := -1;

   end if;

   if INIT_PRE_PROCESS(O_error_message) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END GET_PRE_PROCESS_INFO;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS(O_error_message         OUT VARCHAR2,
                     I_pre_process_method IN     VARCHAR2)
RETURN NUMBER IS

BEGIN

   if I_pre_process_method = HIGH_VOLUME then

      if PRE_PROCESS_ITEM_LOC_HIGH(O_error_message) = FALSE then
         return 0;
      end if;

      if PRE_PROCESS_COST_HIGH(O_error_message) = FALSE then
         return 0;
      end if;

      if PRE_PROCESS_RETAIL_HIGH(O_error_message) = FALSE then
         return 0;
      end if;

   elsif I_pre_process_method = LOW_VOLUME then

      if PRE_PROCESS_ITEM_LOC_LOW(O_error_message) = FALSE then
         return 0;
      end if;

      if PRE_PROCESS_COST_LOW(O_error_message) = FALSE then
         return 0;
      end if;

      if PRE_PROCESS_RETAIL_LOW(O_error_message) = FALSE then
         return 0;
      end if;

   end if;

   ---

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.PRE_PROCESS',
                                        to_char(SQLCODE));
      return 0;
END PRE_PROCESS;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_THREAD(O_error_message         OUT VARCHAR2,
                            I_item_thread_id     IN     rpm_item_thread.item_thread_id%TYPE,
                            I_thread_number      IN     rpm_item_thread.thread_number%TYPE)
RETURN NUMBER IS

BEGIN

   if PRE_PROCESS_ITEM_LOC_MEDIUM(O_error_message,
                                  I_item_thread_id,
                                  I_thread_number) = FALSE then
      return 0;
   end if;

   if PRE_PROCESS_COST_MEDIUM(O_error_message,
                              I_item_thread_id,
                              I_thread_number) = FALSE then
      return 0;
   end if;

   if PRE_PROCESS_RETAIL_MEDIUM(O_error_message,
                                I_item_thread_id,
                                I_thread_number) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.PRE_PROCESS_THREAD',
                                        to_char(SQLCODE));
      return 0;
END PRE_PROCESS_THREAD;

----------------------------------------------------------------------------------------

FUNCTION COMPLETE_PRE_PROCESS(O_error_message       OUT VARCHAR2,
                              I_item_thread_id   IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE)
RETURN NUMBER IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.COMPLETE_PRE_PROCESS';

   L_alter_item_loc VARCHAR2(100) :=
      'alter index rpm_pre_me_item_loc_i1 rebuild parallel compute statistics unrecoverable';
   L_alter_item_loc2 VARCHAR2(100) :=
      'alter index rpm_pre_me_item_loc_i2 rebuild parallel compute statistics unrecoverable';
   L_alter_cost VARCHAR2(100) :=
      'alter index rpm_pre_me_cost_i1 rebuild parallel compute statistics unrecoverable';
   L_alter_retail VARCHAR2(100) :=
      'alter index rpm_pre_me_retail_i1 rebuild parallel compute statistics unrecoverable';

BEGIN

   delete from rpm_item_thread
    where item_thread_id = I_item_thread_id;

   EXECUTE IMMEDIATE L_alter_item_loc;
   EXECUTE IMMEDIATE L_alter_item_loc2;
   EXECUTE IMMEDIATE L_alter_cost;
   EXECUTE IMMEDIATE L_alter_retail;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;
END COMPLETE_PRE_PROCESS;

----------------------------------------------------------------------------------------

FUNCTION INIT_PRE_PROCESS(O_error_message     OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.INIT_PRE_PROCESS';

   L_truncate_item_loc VARCHAR2(100) := 'truncate table rpm_pre_me_item_loc reuse storage';
   L_truncate_cost     VARCHAR2(100) := 'truncate table rpm_pre_me_cost reuse storage';
   L_truncate_retail   VARCHAR2(100) := 'truncate table rpm_pre_me_retail reuse storage';

   L_alter_item_loc    VARCHAR2(100) := 'alter index rpm_pre_me_item_loc_i1 unusable';
   L_alter_cost        VARCHAR2(100) := 'alter index rpm_pre_me_cost_i1 unusable';
   L_alter_retail      VARCHAR2(100) := 'alter index rpm_pre_me_retail_i1 unusable';

BEGIN

   EXECUTE IMMEDIATE L_truncate_item_loc;
   EXECUTE IMMEDIATE L_truncate_cost;
   EXECUTE IMMEDIATE L_truncate_retail;

   EXECUTE IMMEDIATE L_alter_item_loc;
   EXECUTE IMMEDIATE L_alter_cost;
   EXECUTE IMMEDIATE L_alter_retail;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END INIT_PRE_PROCESS;

----------------------------------------------------------------------------------------
FUNCTION PRE_PROCESS_STRATEGY(O_error_message       OUT VARCHAR2,
                              I_aggregations     IN     OBJ_ME_AGGREGATION_TBL,
                              I_aggregation_type IN     RPM_PRE_ME_AGGREGATION.AGGREGATION_TYPE%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.PRE_PROCESS_STRATEGY';

BEGIN

   delete
     from rpm_pre_me_aggregation
    where aggregation_type = I_aggregation_type;

   insert into rpm_pre_me_aggregation (strategy_id,
                                       me_sequence_id,
                                       me_area_diff_seq_id,
                                       zone_id,
                                       dept,
                                       class,
                                       subclass,
                                       complete_ind,
                                       aggregation_type)
   select agg.strategy_id,
          agg.me_sequence_id,
          -1,  --me_area_diff_seq_id,
          agg.zone_id,
          agg.dept,
          agg.class,
          agg.subclass,
          'N',
          I_aggregation_type
     from table(cast(I_aggregations as OBJ_ME_AGGREGATION_TBL)) agg;

   insert into rpm_pre_me_aggregation (strategy_id,
                                       me_sequence_id,
                                       me_area_diff_seq_id,
                                       zone_id,
                                       dept,
                                       class,
                                       subclass,
                                       complete_ind,
                                       aggregation_type)
   select distinct pre.strategy_id,
                   pre.me_sequence_id,
                   a.area_diff_id,
                   a.secondary_zone_id,
                   a.prim_dept,
                   NVL(pre.class, a.prim_class),
                   NVL(pre.subclass, a.prim_subclass),
                   'N',
                   I_aggregation_type
     from rpm_area_diff_expl a,
          rpm_pre_me_aggregation pre,
          rpm_strategy s
    where pre.aggregation_type  = I_aggregation_type
      and pre.strategy_id       = s.strategy_id
      and a.expl_dept           = pre.dept
      and a.expl_class          = NVL(NVL(pre.class, s.class), a.expl_class)
      and a.expl_subclass       = NVL(NVL(pre.subclass, s.subclass), a.expl_subclass)
      and a.prim_zone_id        = pre.zone_id
      and exists (select 'x'
                    from rpm_strategy_margin
                   where strategy_id = pre.strategy_id
                  union all
                  select 'x'
                    from rpm_strategy_competitive
                   where strategy_id = pre.strategy_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PRE_PROCESS_STRATEGY;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_ITEM_LOC_HIGH(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.PRE_PROCESS_ITEM_LOC_HIGH';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert /*+ APPEND */ into rpm_pre_me_item_loc (me_sequence_id,
                                                  me_area_diff_seq_id,
                                                  item,
                                                  loc,
                                                  loc_type,
                                                  status,
                                                  rpm_ind,
                                                  stock_on_hand,
                                                  pack_comp_soh,
                                                  in_transit_qty,
                                                  pack_comp_intran,
                                                  first_received,
                                                  last_received,
                                                  first_sold,
                                                  primary_supp,
                                                  primary_cntry,
                                                  currency_code,
                                                  dept,
                                                  item_parent,
                                                  diff_id,
                                                  zone_id)
   select t.me_sequence_id,
          t.me_area_diff_seq_id,
          t.item,
          t.loc,
          t.loc_type,
          t.status,
          t.rpm_ind,
          t.stock_on_hand,
          t.pack_comp_soh,
          t.in_transit_qty,
          t.pack_comp_intran,
          t.first_received,
          t.last_received,
          t.first_sold,
          t.primary_supp,
          t.primary_cntry,
          t.currency_code,
          t.dept,
          t.item_parent,
          t.diff_1,
          fr.zone_id
     from (select /*+ PARALLEL(ils) PARALLEL(il) ORDERED USE_HASH(il) USE_HASH(ils) USE_HASH(im) */
                  str.me_sequence_id,
                  str.me_area_diff_seq_id,
                  ils.item,
                  ils.loc,
                  ils.loc_type,
                  il.status,
                  il.rpm_ind,
                  ils.stock_on_hand,
                  ils.pack_comp_soh,
                  ils.in_transit_qty,
                  ils.pack_comp_intran,
                  ils.first_received,
                  ils.last_received,
                  ils.first_sold,
                  ils.primary_supp,
                  ils.primary_cntry,
                  NVL(s.currency_code, z.currency_code) currency_code,
                  im.dept,
                  im.class,
                  im.subclass,
                  im.item_parent,
                  case
                     when im.item_parent is NULL and
                          im.item_level = im.tran_level then
                        NULL
                     else
                        im.diff_1
                  end diff_1
             from rpm_pre_me_aggregation str,
                  item_master im,
                  item_loc_soh ils,
                  rpm_zone_location rzl,
                  rpm_zone z,
                  sups s,
                  item_loc il,
                  rpm_strategy rs
            where ils.item             = im.item
              and im.dept              = str.dept
              and im.class             = NVL(NVL(str.class, rs.class), im.class)
              and im.subclass          = NVL(NVL(str.subclass, rs.subclass), im.subclass)
              and ils.loc              = rzl.location
              and ils.primary_supp     = s.supplier (+)
              and rzl.zone_id          = str.zone_id
              and z.zone_id            = rzl.zone_id
              and il.item              = ils.item
              and il.loc               = ils.loc
              and il.status           != 'D'
              and rs.strategy_id       = str.strategy_id
              and str.aggregation_type = RPM_EXT_SQL.WORKSHEET_PROCESS_LEVEL) t,
          ---
          (select distinct
                  e.dept,
                  e.class,
                  e.subclass,
                  ezl.location,
                  ezl.zone_id
             from rpm_pre_me_aggregation agr,
                  rpm_merch_retail_def_expl e,
                  rpm_zone ez,
                  rpm_zone_location ezl
            where e.dept               = agr.dept
              and e.class              = NVL(agr.class, e.class)
              and e.subclass           = NVL(agr.subclass, e.subclass)
              and e.regular_zone_group = ez.zone_group_id
              and ez.zone_id           = ezl.zone_id
              and rownum               > 0) fr
    where t.dept       = fr.dept (+)
      and t.class      = fr.class (+)
      and t.subclass   = fr.subclass (+)
      and t.loc        = fr.location (+)
    order by t.me_sequence_id,
             t.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then

      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             L_program,
                                             TO_CHAR(SQLCODE));
      return FALSE;

END PRE_PROCESS_ITEM_LOC_HIGH;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_COST_HIGH(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.PRE_PROCESS_COST_HIGH';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert /*+ APPEND */ into rpm_pre_me_cost i (me_sequence_id,
                                                me_area_diff_seq_id,
                                                item,
                                                loc,
                                                active_date,
                                                base_cost,
                                                pricing_cost)
   select /*+ PARALLEL(il) PARALLEL(f) USE_HASH(il f) */
          il.me_sequence_id,
          il.me_area_diff_seq_id,
          il.item,
          il.loc,
          f.active_date,
          f.base_cost,
          f.pricing_cost
     from rpm_pre_me_item_loc il,
          future_cost f
    where f.item              = il.item
      and f.location          = il.loc
      and f.supplier          = il.primary_supp
      and f.origin_country_id = il.primary_cntry
    order by il.me_sequence_id,
             il.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then

      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PRE_PROCESS_COST_HIGH;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_RETAIL_HIGH(O_error_message     OUT VARCHAR2)
RETURN BOOLEAN IS

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert /*+ append */ into rpm_pre_me_retail i
    select in2.me_sequence_id, in2.me_area_diff_seq_id, in2.item, in2.loc,
           in2.action_date, in2.selling_retail, in2.selling_uom, in2.multi_units, in2.multi_unit_retail,
           in2.multi_selling_uom, in2.clear_start_ind, in2.clearance_id, in2.clear_retail,
           in2.clear_uom, in2.price_change_id, in2.clear_mkdn_index, in2.p_ind
      from (select in1.cur_hier_level,
                   in1.cur_hier_level_rank,
                   in1.me_sequence_id, in1.me_area_diff_seq_id, in1.item, in1.loc,
                   in1.action_date, in1.selling_retail, in1.selling_uom, in1.multi_units, in1.multi_unit_retail,
                   in1.multi_selling_uom, in1.clear_start_ind, in1.clearance_id, in1.clear_retail,
                   in1.clear_uom, in1.price_change_id, in1.clear_mkdn_index, in1.p_ind,
                   ---
                   max(in1.cur_hier_level_rank) over (partition by in1.item, in1.loc) max_rank
              from (select /*+ parallel(il) parallel(f) use_hash(il f) */
                           f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'IZ'
                    UNION ALL
                    select /*+ parallel(il) parallel(f) use_hash(il f) */
                           f.cur_hier_level,
                           3 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'IL'
                    UNION ALL
                    select /*+ parallel(il) parallel(f) use_hash(il f) */
                           f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'DL'
                    UNION ALL
                    select /*+ parallel(il) parallel(f) use_hash(il f) */
                           f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'DZ'
                    UNION ALL
                    select /*+ parallel(il) parallel(f) use_hash(il f) */
                           f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'PL'
                    UNION ALL
                    select /*+ parallel(il) parallel(f) use_hash(il f) */
                           f.cur_hier_level,
                           0 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'PZ') in1) in2
     where in2.cur_hier_level_rank = in2.max_rank
     order by in2.me_sequence_id,
              in2.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.PRE_PROCESS_RETAIL_HIGH',
                                        to_char(SQLCODE));
      return FALSE;
END PRE_PROCESS_RETAIL_HIGH;

----------------------------------------------------------------------------------------
FUNCTION PRE_PROCESS_ITEM_LOC_LOW(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.PRE_PROCESS_ITEM_LOC_LOW';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert /*+ APPEND */ into rpm_pre_me_item_loc (me_sequence_id,
                                                  me_area_diff_seq_id,
                                                  item,
                                                  loc,
                                                  loc_type,
                                                  status,
                                                  rpm_ind,
                                                  stock_on_hand,
                                                  pack_comp_soh,
                                                  in_transit_qty,
                                                  pack_comp_intran,
                                                  first_received,
                                                  last_received,
                                                  first_sold,
                                                  primary_supp,
                                                  primary_cntry,
                                                  currency_code,
                                                  dept,
                                                  item_parent,
                                                  diff_id,
                                                  zone_id)
   select t.me_sequence_id,
          t.me_area_diff_seq_id,
          t.item,
          t.loc,
          t.loc_type,
          t.status,
          t.rpm_ind,
          t.stock_on_hand,
          t.pack_comp_soh,
          t.in_transit_qty,
          t.pack_comp_intran,
          t.first_received,
          t.last_received,
          t.first_sold,
          t.primary_supp,
          t.primary_cntry,
          t.currency_code,
          t.dept,
          t.item_parent,
          t.diff_1,
          fr.zone_id
     from (select /*+ INDEX(il pk_item_loc) INDEX(ils pk_item_loc_soh)  */
                  inner.me_sequence_id,
                  inner.me_area_diff_seq_id,
                  ils.item,
                  ils.loc,
                  ils.loc_type,
                  il.status,
                  il.rpm_ind,
                  ils.stock_on_hand,
                  ils.pack_comp_soh,
                  ils.in_transit_qty,
                  ils.pack_comp_intran,
                  ils.first_received,
                  ils.last_received,
                  ils.first_sold,
                  ils.primary_supp,
                  ils.primary_cntry,
                  NVL(s.currency_code, inner.currency_code) currency_code,
                  inner.dept,
                  inner.class,
                  inner.subclass,
                  inner.item_parent,
                  inner.diff_1
             from (select str.me_sequence_id,
                          str.me_area_diff_seq_id,
                          im.item,
                          rzl.location,
                          z.currency_code,
                          im.dept,
                          im.class,
                          im.subclass,
                          im.item_parent,
                          case
                             when im.item_parent is NULL and
                                  im.item_level = im.tran_level then
                                NULL
                             else
                                im.diff_1
                          end diff_1
                     from rpm_pre_me_aggregation str,
                          rpm_zone z,
                          rpm_zone_location rzl,
                          item_master im,
                          rpm_strategy rs
                    where im.dept              = str.dept
                      and im.class             = NVL(NVL(str.class, rs.class), im.class)
                      and im.subclass          = NVL(NVL(str.subclass, rs.subclass), im.subclass)
                      and rzl.zone_id          = str.zone_id
                      and z.zone_id            = rzl.zone_id
                      and rs.strategy_id       = str.strategy_id
                      and str.aggregation_type = RPM_EXT_SQL.WORKSHEET_PROCESS_LEVEL
                      and rownum               > 0) inner,
                  item_loc il,
                  item_loc_soh ils,
                  sups s
            where ils.item       = inner.item
              and ils.loc        = inner.location
              and il.item        = ils.item
              and il.loc         = ils.loc
              and il.status     != 'D'
              and s.supplier (+) = ils.primary_supp) t,
          (select distinct
                  e.dept,
                  e.class,
                  e.subclass,
                  ezl.location,
                  ezl.zone_id
             from rpm_pre_me_aggregation agr,
                  rpm_merch_retail_def_expl e,
                  rpm_zone ez,
                  rpm_zone_location ezl
            where e.dept               = agr.dept
              and e.class              = NVL(agr.class, e.class)
              and e.subclass           = NVL(agr.subclass, e.subclass)
              and e.regular_zone_group = ez.zone_group_id
              and ez.zone_id           = ezl.zone_id
              and rownum               > 0) fr
    where t.dept     = fr.dept (+)
      and t.class    = fr.class (+)
      and t.subclass = fr.subclass (+)
      and t.loc      = fr.location (+)
    order by t.me_sequence_id,
             t.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION

   when OTHERS then

      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PRE_PROCESS_ITEM_LOC_LOW;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_COST_LOW(O_error_message    OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_EXT_SQL.PRE_PROCESS_COST_LOW';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert /*+ APPEND */ into rpm_pre_me_cost i (me_sequence_id,
                                                me_area_diff_seq_id,
                                                item,
                                                loc,
                                                active_date,
                                                base_cost,
                                                pricing_cost)
   select /*+ USE_NL(f il) INDEX(il rpm_pre_me_item_loc_i2) */
          il.me_sequence_id,
          il.me_area_diff_seq_id,
          il.item,
          il.loc,
          f.active_date,
          f.base_cost,
          f.pricing_cost
     from rpm_pre_me_item_loc il,
          future_cost f
    where f.item              = il.item
      and f.location          = il.loc
      and f.supplier          = il.primary_supp
      and f.origin_country_id = il.primary_cntry
    order by il.me_sequence_id,
             il.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then

      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PRE_PROCESS_COST_LOW;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_RETAIL_LOW(O_error_message     OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RPM_EXT_SQL.PRE_PROCESS_RETAIL_LOW';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert /*+ append */ into rpm_pre_me_retail i
    select in2.me_sequence_id, in2.me_area_diff_seq_id, in2.item, in2.loc,
           in2.action_date, in2.selling_retail, in2.selling_uom, in2.multi_units, in2.multi_unit_retail,
           in2.multi_selling_uom, in2.clear_start_ind, in2.clearance_id, in2.clear_retail,
           in2.clear_uom, in2.price_change_id, in2.clear_mkdn_index, in2.p_ind
      from (select in1.cur_hier_level,
                   in1.cur_hier_level_rank,
                   in1.me_sequence_id, in1.me_area_diff_seq_id, in1.item, in1.loc,
                   in1.action_date, in1.selling_retail, in1.selling_uom, in1.multi_units, in1.multi_unit_retail,
                   in1.multi_selling_uom, in1.clear_start_ind, in1.clearance_id, in1.clear_retail,
                   in1.clear_uom, in1.price_change_id, in1.clear_mkdn_index, in1.p_ind,
                   ---
                   max(in1.cur_hier_level_rank) over (partition by in1.item, in1.loc) max_rank
              from (select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) */
                           f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'IZ'
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) */
                           f.cur_hier_level,
                           3 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'IL'
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) */
                           f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'DL'
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) */
                           f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'DZ'
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) */
                           f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'PL'
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) */
                           f.cur_hier_level,
                           0 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           decode(nvl(f.on_simple_promo_ind,0) + nvl(f.on_complex_promo_ind,0), 0, 0, 1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'PZ') in1) in2
     where in2.cur_hier_level_rank = in2.max_rank
     order by in2.me_sequence_id,
              in2.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PRE_PROCESS_RETAIL_LOW;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_ITEM_LOC_MEDIUM(O_error_message     OUT VARCHAR2,
                                     I_item_thread_id IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                                     I_thread_number  IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.PRE_PROCESS_ITEM_LOC_MEDIUM';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert into rpm_pre_me_item_loc (me_sequence_id,
                                    me_area_diff_seq_id,
                                    item,
                                    loc,
                                    loc_type,
                                    status,
                                    rpm_ind,
                                    stock_on_hand,
                                    pack_comp_soh,
                                    in_transit_qty,
                                    pack_comp_intran,
                                    first_received,
                                    last_received,
                                    first_sold,
                                    primary_supp,
                                    primary_cntry,
                                    currency_code,
                                    dept,
                                    item_parent,
                                    diff_id,
                                    zone_id)
   select t.me_sequence_id,
          t.me_area_diff_seq_id,
          t.item,
          t.loc,
          t.loc_type,
          t.status,
          t.rpm_ind,
          t.stock_on_hand,
          t.pack_comp_soh,
          t.in_transit_qty,
          t.pack_comp_intran,
          t.first_received,
          t.last_received,
          t.first_sold,
          t.primary_supp,
          t.primary_cntry,
          t.currency_code,
          t.dept,
          t.item_parent,
          t.diff_1,
          fr.zone_id
     from (select /*+ INDEX(il PK_ITEM_LOC) INDEX(ils PK_ITEM_LOC_SOH)  */
                  inner.me_sequence_id,
                  inner.me_area_diff_seq_id,
                  ils.item,
                  ils.loc,
                  ils.loc_type,
                  il.status,
                  il.rpm_ind,
                  ils.stock_on_hand,
                  ils.pack_comp_soh,
                  ils.in_transit_qty,
                  ils.pack_comp_intran,
                  ils.first_received,
                  ils.last_received,
                  ils.first_sold,
                  ils.primary_supp,
                  ils.primary_cntry,
                  NVL(s.currency_code, inner.currency_code) currency_code,
                  inner.dept,
                  inner.class,
                  inner.subclass,
                  inner.item_parent,
                  inner.diff_1
             from (select str.me_sequence_id,
                          str.me_area_diff_seq_id,
                          im.item,
                          rzl.location,
                          z.currency_code,
                          im.dept,
                          im.class,
                          im.subclass,
                          im.item_parent,
                          case
                             when im.item_parent is NULL and
                                  im.item_level = im.tran_level then
                                NULL
                             else
                                im.diff_1
                          end diff_1
                     from rpm_pre_me_aggregation str,
                          rpm_zone z,
                          rpm_zone_location rzl,
                          item_master im,
                          rpm_item_thread rit,
                          rpm_strategy rs
                    where rit.item_thread_id   = I_item_thread_id
                      and rit.thread_number    = I_thread_number
                      and im.item              = rit.item
                      and im.dept              = str.dept
                      and im.class             = NVL(str.class, im.class)
                      and im.subclass          = NVL(str.subclass, im.subclass)
                      and rzl.zone_id          = str.zone_id
                      and z.zone_id            = rzl.zone_id
                      and rs.strategy_id       = str.strategy_id
                      and str.aggregation_type = RPM_EXT_SQL.WORKSHEET_PROCESS_LEVEL
                      and rownum               > 0) inner,
                  item_loc il,
                  item_loc_soh ils,
                  sups s
            where ils.item       = inner.item
              and ils.loc        = inner.location
              and il.item        = ils.item
              and il.loc         = ils.loc
              and il.status      != 'D'
              and s.supplier (+) = ils.primary_supp) t,
          (select distinct
                  e.dept,
                  e.class,
                  e.subclass,
                  ezl.location,
                  ezl.zone_id
             from rpm_pre_me_aggregation agr,
                  rpm_merch_retail_def_expl e,
                  rpm_zone ez,
                  rpm_zone_location ezl
            where e.dept               = agr.dept
              and e.class              = NVL(agr.class, e.class)
              and e.subclass           = NVL(agr.subclass, e.subclass)
              and e.regular_zone_group = ez.zone_group_id
              and ez.zone_id           = ezl.zone_id
              and rownum               > 0) fr
    where t.dept       = fr.dept (+)
      and t.class      = fr.class (+)
      and t.subclass   = fr.subclass (+)
      and t.loc        = fr.location (+)
    order by t.me_sequence_id,
             t.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION

   when OTHERS then

      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END PRE_PROCESS_ITEM_LOC_MEDIUM;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_COST_MEDIUM(O_error_message         OUT VARCHAR2,
                                 I_item_thread_id     IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                                 I_thread_number      IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50) := 'RPM_EXT_SQL.PRE_PROCESS_COST_MEDIUM';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert into rpm_pre_me_cost i
    select /*+ use_nl(f il) index(il rpm_pre_me_item_loc_i2) */
           il.me_sequence_id,
           il.me_area_diff_seq_id,
           il.item,
           il.loc,
           f.active_date,
           f.base_cost,
           f.pricing_cost
      from rpm_pre_me_item_loc il,
           future_cost f,
           (select item
              from rpm_item_thread rit
             where rit.item_thread_id = I_item_thread_id
               and rit.thread_number  = I_thread_number) it
     where il.item             = it.item
       and f.item              = il.item
       and f.location          = il.loc
       and f.supplier          = il.primary_supp
       and f.origin_country_id = il.primary_cntry
     order by il.me_sequence_id,
              il.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PRE_PROCESS_COST_MEDIUM;

----------------------------------------------------------------------------------------

FUNCTION PRE_PROCESS_RETAIL_MEDIUM(O_error_message         OUT VARCHAR2,
                                   I_item_thread_id     IN     RPM_ITEM_THREAD.ITEM_THREAD_ID%TYPE,
                                   I_thread_number      IN     RPM_ITEM_THREAD.THREAD_NUMBER%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RPM_EXT_SQL.PRE_PROCESS_RETAIL_MEDIUM';

BEGIN

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=true';

   insert into rpm_pre_me_retail i
    select in2.me_sequence_id, in2.me_area_diff_seq_id, in2.item, in2.loc,
           in2.action_date, in2.selling_retail, in2.selling_uom, in2.multi_units, in2.multi_unit_retail,
           in2.multi_selling_uom, in2.clear_start_ind, in2.clearance_id, in2.clear_retail,
           in2.clear_uom, in2.price_change_id, in2.clear_mkdn_index, in2.p_ind
      from (select in1.cur_hier_level,
                   in1.cur_hier_level_rank,
                   in1.me_sequence_id, in1.me_area_diff_seq_id, in1.item, in1.loc,
                   in1.action_date, in1.selling_retail, in1.selling_uom, in1.multi_units, in1.multi_unit_retail,
                   in1.multi_selling_uom, in1.clear_start_ind, in1.clearance_id, in1.clear_retail,
                   in1.clear_uom, in1.price_change_id, in1.clear_mkdn_index, in1.p_ind,
                   ---
                   max(in1.cur_hier_level_rank) over (partition by in1.item, in1.loc) max_rank
              from (select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) index(il rpm_pre_me_item_loc_i2) */
                           f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           DECODE(NVL(f.on_simple_promo_ind, 0) + NVL(f.on_complex_promo_ind,0),
                                  0, 0,
                                  1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f,
                           (select item
                              from rpm_item_thread rit
                             where rit.item_thread_id = I_item_thread_id
                               and rit.thread_number  = I_thread_number) it
                     where f.item              = il.item
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'IZ'
                       and il.item             = it.item
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) index(il rpm_pre_me_item_loc_i2) */
                           f.cur_hier_level,
                           3 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           DECODE(NVL(f.on_simple_promo_ind, 0) + NVL(f.on_complex_promo_ind,0),
                                  0, 0,
                                  1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f,
                           (select item
                              from rpm_item_thread rit
                             where rit.item_thread_id = I_item_thread_id
                               and rit.thread_number  = I_thread_number) it
                     where f.item              = il.item
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'IL'
                       and il.item             = it.item
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) index(il rpm_pre_me_item_loc_i2) */
                           f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           DECODE(NVL(f.on_simple_promo_ind, 0) + NVL(f.on_complex_promo_ind,0),
                                  0, 0,
                                  1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f,
                           (select item
                              from rpm_item_thread rit
                             where rit.item_thread_id = I_item_thread_id
                               and rit.thread_number  = I_thread_number) it
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'DL'
                       and il.item             = it.item
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) index(il rpm_pre_me_item_loc_i2) */
                           f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           DECODE(NVL(f.on_simple_promo_ind, 0) + NVL(f.on_complex_promo_ind,0),
                                  0, 0,
                                  1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f,
                           (select item
                              from rpm_item_thread rit
                             where rit.item_thread_id = I_item_thread_id
                               and rit.thread_number  = I_thread_number) it
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'DZ'
                       and il.item             = it.item
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) index(il rpm_pre_me_item_loc_i2) */
                           f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           DECODE(NVL(f.on_simple_promo_ind, 0) + NVL(f.on_complex_promo_ind,0),
                                  0, 0,
                                  1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f,
                           (select item
                              from rpm_item_thread rit
                             where rit.item_thread_id = I_item_thread_id
                               and rit.thread_number  = I_thread_number) it
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.loc
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'PL'
                       and il.item             = it.item
                    UNION ALL
                    select /*+ use_nl(f il) INDEX(f RPM_FUTURE_RETAIL_I1) index(il rpm_pre_me_item_loc_i2) */
                           f.cur_hier_level,
                           0 cur_hier_level_rank,
                           il.me_sequence_id, il.me_area_diff_seq_id, il.item, il.loc,
                           f.action_date, f.selling_retail, f.selling_uom, f.multi_units, f.multi_unit_retail,
                           f.multi_selling_uom, f.clear_start_ind, f.clearance_id, f.clear_retail,
                           f.clear_uom, f.price_change_id, f.clear_mkdn_index,
                           DECODE(NVL(f.on_simple_promo_ind, 0) + NVL(f.on_complex_promo_ind,0),
                                  0, 0,
                                  1) p_ind
                      from rpm_pre_me_item_loc il,
                           rpm_future_retail f,
                           (select item
                              from rpm_item_thread rit
                             where rit.item_thread_id = I_item_thread_id
                               and rit.thread_number  = I_thread_number) it
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.zone_id
                       and f.dept              = il.dept
                       and f.cur_hier_level    = 'PZ'
                       and il.item             = it.item) in1) in2
     where in2.cur_hier_level_rank = in2.max_rank
     order by in2.me_sequence_id,
              in2.me_area_diff_seq_id;

   EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';

   commit;

   return TRUE;

EXCEPTION
   when OTHERS then
      EXECUTE IMMEDIATE 'alter session set SKIP_UNUSABLE_INDEXES=false';
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;
END PRE_PROCESS_RETAIL_MEDIUM;

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
--start extract logic
----------------------------------------------------------------------------------------

FUNCTION EXTRACT (O_error_msg            OUT VARCHAR2,
                  I_me_sequence_id    IN     NUMBER,
                  I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                  I_dept              IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                  I_class             IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                  I_subclass          IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                  I_zone_id           IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                  I_location          IN     RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE,
                  I_review_start_date IN     DATE,
                  I_eff_date          IN     DATE,
                  I_initial_call      IN     VARCHAR2 DEFAULT 'Y')
RETURN NUMBER IS

   L_program VARCHAR2(20) := 'RPM_EXT_SQL.EXTRACT';

   L_upd_item_attr  RPM_SYSTEM_OPTIONS.UPDATE_ITEM_ATTRIBUTES%TYPE := NULL;
   L_constraint_ind RPM_WORKSHEET_STATUS.CONSTRAINT_IND%TYPE       := NULL;

BEGIN

   O_error_msg := NULL;

   LP_record_item_loc_strategy := I_strategy_id;

   if REFRESH_DATA_STRUCTURES(O_error_msg,
                              I_initial_call) = FALSE then
      return 0;
   end if;

   if POPULATE_STRATEGY_TYPE(O_error_msg,
                             I_strategy_id) = FALSE then
      return 0;
   end if;

   if I_initial_call = 'Y' then
      if RPM_SYSTEM_OPTIONS_SQL.GET_UPDATE_ITEM_ATTRIBUTES(L_upd_item_attr,
                                                           O_error_msg) = FALSE then
         return 0;
      end if;

      if GET_DEPT_AGG(O_error_msg,
                      I_dept) = FALSE then
         return 0;
      end if;

      if TRUNC(LP_tomorrow) = TRUNC(I_review_start_date) then
         LP_review_not_started := TRUE;
      else
         LP_review_not_started := FALSE;
      end if;

      if ((LP_review_not_started) or
          (L_upd_item_attr = UPDATE_ITEM_ATTRIBUTES_YES)) then
         LP_do_update := TRUE;
      else
         LP_do_update := FALSE;
      end if;

      if LP_maint_margin_ind = 1 then
         if GET_MAINT_MARGIN_INFO(O_error_msg,
                                  LP_crp_start,
                                  LP_crp_end,
                                  I_strategy_id) = FALSE then
            return 0;
         end if;
      else
         LP_crp_start := NULL;
         LP_crp_end   := NULL;
      end if;

      if GET_CALENDAR_INFO(O_error_msg,
                           I_strategy_id,
                           LP_exclusion_ind,
                           LP_exception_ind,
                           LP_review_date) = FALSE then
         return 0;
      end if;

   end if;

   -- ITEM EXTRACT LOGIC
   if INIT_RPM_EXT_DATA(O_error_msg,
                        I_strategy_id,
                        I_initial_call,
                        I_dept,
                        I_class,
                        I_subclass,
                        I_zone_id,
                        I_location) = FALSE then
      return 0;
   end if;

   if POPULATE_BASIC_ITEM_INFO(O_error_msg,
                               I_strategy_id,
                               I_dept,
                               I_class,
                               I_subclass,
                               I_zone_id,
                               I_initial_call) = FALSE then
      return 0;
   end if;

   if LP_review_not_started then

      if LP_uda_process_ind then
         if POPULATE_UDA(O_error_msg) = FALSE then
            return 0;
         end if;
      end if;

      if LP_season_process_ind then
         if POPULATE_SEASON(O_error_msg) = FALSE then
            return 0;
         end if;
      end if;

      if POPULATE_PARENT_INFO(O_error_msg) = FALSE then
         return 0;
      end if;

   end if;

   if LP_comp_process_ind and LP_do_update then
      if I_initial_call = 'Y' then
         if POPULATE_COMP_RETAIL(O_error_msg,
                                 I_strategy_id) = FALSE then
            return 0;
         end if;
      else
         if POPULATE_COMP_RETAIL_AREA_DIFF(O_error_msg) = FALSE then
            return 0;
         end if;
      end if;
   end if;

   if LP_do_update then
      if POPULATE_REF_WH_STOCK(O_error_msg,
                               I_strategy_id,
                               I_initial_call) = FALSE then
         return 0;
      end if;
   end if;

   if LP_on_order_process_ind and LP_do_update then
      if POPULATE_REF_WH_ON_ORDER(O_error_msg,
                                  I_strategy_id,
                                  I_initial_call) = FALSE then
         return 0;
      end if;
   end if;

   -- ITEM LOC EXTRACT LOGIC
   if POPULATE_ITEM_LOC(O_error_msg,
                        I_strategy_id,
                        I_me_sequence_id,
                        NVL(LP_area_diff_id,-1),
                        I_zone_id,
                        I_eff_date,
                        I_initial_call) = FALSE then
      return 0;
   end if;

   if POPULATE_IL_STAT_RPM_IND_BATCH(O_error_msg,
                                     I_me_sequence_id,
                                     NVL(LP_area_diff_id,-1)) = FALSE then
      return 0;
   end if;

   if LP_do_update then
      if POPULATE_ITEM_COST_BATCH(O_error_msg,
                                  I_me_sequence_id,
                                  NVL(LP_area_diff_id,-1),
                                  LP_tomorrow,
                                  I_eff_date,
                                  'N') = FALSE then
         return 0;
      end if;
   end if;

   if REMOVE_COST(O_error_msg) = FALSE then
      return 0;
   end if;

   if POPULATE_PRICING_INFO_BATCH(O_error_msg,
                                  I_me_sequence_id,
                                  NVL(LP_area_diff_id,-1),
                                  LP_tomorrow,
                                  I_dept,
                                  I_zone_id) = FALSE then
      return 0;
   end if;

   if REMOVE_RETAIL(O_error_msg) = FALSE then
      return 0;
   end if;

   if POPULATE_REPL_AND_SALES_INFO(O_error_msg) = FALSE then
      return 0;
   end if;

   if LP_review_not_started then
      if POPULATE_OTHER_LOC_INFO(O_error_msg) = FALSE then
         return 0;
      end if;
   end if;

   if LP_review_not_started then
      if POPULATE_VAT(O_error_msg) = FALSE then
         return 0;
      end if;
   end if;

   if LP_on_order_process_ind and
      LP_do_update then
      if POPULATE_ON_ORDER(O_error_msg) = FALSE then
         return 0;
      end if;
   end if;

   -- FINIALIZATION LOGIC
   if CONSOLIDATE_GTT(O_error_msg) = FALSE then
      return 0;
   end if;

   if CONSOLIDATE_ITEM_LOC(O_error_msg,
                           I_zone_id,
                           I_initial_call) = FALSE then
      return 0;
   end if;

   if LP_review_not_started then
      if RPM_EXT_CAND_RULE_SQL.EXECUTE_CAND_RULES(O_error_msg,
                                                  LP_strategy_type,
                                                  I_dept,
                                                  LP_exclusion_ind,
                                                  LP_exception_ind) = FALSE then
         return 0;
      end if;

      if RPM_EXT_CAND_RULE_SQL.CAND_RULES_DEL_EXCL(O_error_msg,
                                                   I_strategy_id) = FALSE then
         return 0;
      end if;

      -- assumes only one zone_id on rpm_me_consolidate_itemloc_gtt table
      update rpm_me_consolidate_itemloc_gtt con
         set con.rule_boolean = 1
       where exists (select 1
                       from rpm_me_il_cand_rule_link_gtt cr
                      where cr.item     = con.item
                        and cr.location = con.location);
   end if;

   if POPULATE_PROPOSED(O_error_msg,
                        I_zone_id,
                        I_initial_call) = FALSE then
      return 0;
   end if;

   if FINISH_RPM_LUW(O_error_msg) = FALSE then
      return 0;
   end if;

   if LP_review_not_started then

      if RPM_EXT_CAND_RULE_SQL.CAND_RULES_LINK_INCL(O_error_msg,
                                                    I_strategy_id,
                                                    L_constraint_ind) = FALSE then
         return 0;
      end if;
   end if;

   if I_initial_call = 'Y' and
      (LP_strategy_type = COMPETITIVE or
       LP_strategy_type = MARGIN) then

      if RUN_AREA_DIFF(O_error_msg,
                       I_me_sequence_id,
                       I_strategy_id,
                       I_dept,
                       I_class,
                       I_subclass,
                       I_zone_id,
                       I_location,
                       I_review_start_date,
                       I_eff_date) = FALSE then
         return 0;
      end if;
   end if;

   update rpm_pre_me_aggregation
      set complete_ind     = 'Y'
    where complete_ind     = 'N'
      and strategy_id      = I_strategy_id
      and aggregation_type = RPM_EXT_SQL.WORKSHEET_PROCESS_LEVEL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END EXTRACT;

----------------------------------------------------------------------------------------
FUNCTION REFRESH_DATA_STRUCTURES(O_error_msg    IN OUT VARCHAR2,
                                 I_initial_call IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.REFRESH_DATA_STRUCTURES';

   L_uda_process_ind            NUMBER(1) := 1;
   L_season_process_ind         NUMBER(1) := 1;
   L_on_order_process_ind       NUMBER(1) := 1;
   L_comp_process_ind           NUMBER(1) := 1;
   L_proj_sales_process_ind     NUMBER(1) := 1;
   L_hist_sales_process_ind     NUMBER(1) := 1;
   L_repl_ind_process_ind       NUMBER(1) := 1;
   L_seasonal_sales_process_ind NUMBER(1) := 1;
   L_clear_dates_process_ind    NUMBER(1) := 1;

   cursor C_ME_CONFIG is
      select uda_process_ind,
             season_process_ind,
             on_order_process_ind,
             comp_process_ind,
             proj_sales_process_ind,
             hist_sales_process_ind,
             repl_ind_process_ind,
             seasonal_sales_process_ind,
             clear_dates_process_ind
        from rpm_merch_extract_config;

BEGIN

   -- Refresh all Temp Tables and Data Structures

   if DATES_SQL.RESET_GLOBALS(O_error_msg) = FALSE then
      return FALSE;
   end if;

   LP_vdate := GET_VDATE;
   LP_tomorrow := GET_VDATE + 1;

   delete from rpm_me_item_gtt;
   delete from rpm_me_item_loc_gtt;

   delete from rpm_me_il_cost_gtt;
   delete from rpm_me_il_retail_gtt;
   delete from rpm_me_il_repl_gtt;
   delete from rpm_me_il_hist_sales_gtt;
   delete from rpm_me_il_proj_sales_gtt;
   delete from rpm_me_il_season_sales_gtt;
   delete from rpm_me_il_link_code_gtt;
   delete from rpm_me_il_clear_dates_gtt;
   delete from rpm_me_il_vat_gtt;
   delete from rpm_me_il_onorder_gtt;
   delete from rpm_me_proposed_gtt;
   delete from rpm_me_il_status_rpm_ind_gtt;

   delete from rpm_me_consolidate_itemloc_gtt;

   delete from rpm_me_il_cand_cond_gtt;
   delete from rpm_me_il_cand_rule_link_gtt;

   delete from rpm_me_agg_cand_rule_link_gtt;

   if I_initial_call = 'Y' then
      LP_area_diff_prim_id := NULL;
      LP_area_diff_id := NULL;
      LP_area_diff_prim_ws_id := NULL;

      delete from rpm_area_temp_item_loc;

   end if;

   if RPM_SYSTEM_OPTIONS_SQL.GET_SALES_CALCULATION_METHOD(LP_sales_calc_method,
                                                          O_error_msg) = FALSE then
      return TRUE;
   end if;

   if RPM_SYSTEM_OPTIONS_SQL.GET_COST_CALCULATION_METHOD(LP_cost_calc_method,
                                                         O_error_msg) = FALSE then
      return TRUE;
   end if;

   if RPM_SYSTEM_OPTIONS_SQL.GET_DYNAMIC_AREA_DIFF_IND(LP_dynamic_area_diff_ind,
                                                       O_error_msg) = FALSE then
      return TRUE;
   end if;

   if RPM_SYSTEM_OPTIONS_SQL.GET_PRICE_CHANGE_PROC_DAYS(LP_price_change_proc_days,
                                                             O_error_msg) = FALSE then
      return TRUE;
   end if;

   open C_ME_CONFIG;
   fetch C_ME_CONFIG into L_uda_process_ind,
                          L_season_process_ind,
                          L_on_order_process_ind,
                          L_comp_process_ind,
                          L_proj_sales_process_ind,
                          L_hist_sales_process_ind,
                          L_repl_ind_process_ind,
                          L_seasonal_sales_process_ind,
                          L_clear_dates_process_ind;
   close C_ME_CONFIG;

   LP_uda_process_ind            := INT_TO_BOOLEAN(L_uda_process_ind);
   LP_season_process_ind         := INT_TO_BOOLEAN(L_season_process_ind);
   LP_on_order_process_ind       := INT_TO_BOOLEAN(L_on_order_process_ind);
   LP_comp_process_ind           := INT_TO_BOOLEAN(L_comp_process_ind);
   LP_proj_sales_process_ind     := INT_TO_BOOLEAN(L_proj_sales_process_ind);
   LP_hist_sales_process_ind     := INT_TO_BOOLEAN(L_hist_sales_process_ind);
   LP_repl_ind_process_ind       := INT_TO_BOOLEAN(L_repl_ind_process_ind);
   LP_seasonal_sales_process_ind := INT_TO_BOOLEAN(L_seasonal_sales_process_ind);
   LP_clear_dates_process_ind    := INT_TO_BOOLEAN(L_clear_dates_process_ind);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END REFRESH_DATA_STRUCTURES;

----------------------------------------------------------------------------------------

FUNCTION GET_DEPT_AGG(O_error_msg     IN OUT  VARCHAR2,
                      I_dept          IN      RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE)
   RETURN BOOLEAN IS

   -- Dept level information
   cursor C_DEPT_INFO is
      select regular_sales_ind,
             promotional_sales_ind,
             clearance_sales_ind,
             historical_sales_level,
             pend_cost_chg_window_days,
             include_wh_on_hand,
             include_wh_on_order,
             price_change_amount_calc_type,
             worksheet_level
        from rpm_dept_aggregation rd
       where rd.dept = I_dept;

BEGIN

   open C_DEPT_INFO;
   fetch C_DEPT_INFO into LP_regular_sales_ind,
                          LP_promotional_sales_ind,
                          LP_clearance_sales_ind,
                          LP_time_frame,
                          LP_pend_cost_chg_window,
                          LP_include_wh_on_hand,
                          LP_include_wh_on_order,
                          LP_pc_amount_calc_type,
                          LP_worksheet_level;
   close C_DEPT_INFO;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.GET_DEPT_AGG',
                                        to_char(SQLCODE));
      return FALSE;
END GET_DEPT_AGG;
----------------------------------------------------------------------------------------
FUNCTION POPULATE_STRATEGY_TYPE(O_error_msg      IN OUT  VARCHAR2,
                                I_strategy_id    IN      RPM_STRATEGY.STRATEGY_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_STRATEGY_TYPE is
      select CLEARANCE
        from rpm_strategy_clearance
       where strategy_id = I_strategy_id
      union
      select COMPETITIVE
        from rpm_strategy_competitive
       where strategy_id = I_strategy_id
      union
      select MAINT_MARGIN
        from rpm_strategy_maint_margin
       where strategy_id = I_strategy_id
      union
      select MARGIN
        from rpm_strategy_margin
       where strategy_id = I_strategy_id;


BEGIN

   open C_STRATEGY_TYPE;
   fetch C_STRATEGY_TYPE into LP_strategy_type;
   close C_STRATEGY_TYPE;

   if LP_strategy_type = MAINT_MARGIN then
      LP_maint_margin_ind := 1;
   else
      LP_maint_margin_ind := 0;
   end if;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_STRATEGY_TYPE',
                                        to_char(SQLCODE));
      return FALSE;
END POPULATE_STRATEGY_TYPE;
----------------------------------------------------------------------------------------
FUNCTION INT_TO_BOOLEAN(I_number NUMBER)
   RETURN BOOLEAN IS
BEGIN

   if I_number = 1 then
      return TRUE;
   end if;

   return FALSE;

END INT_TO_BOOLEAN;

----------------------------------------------------------------------------------------
FUNCTION GET_CALENDAR_INFO(O_error_msg     IN OUT VARCHAR2,
                           I_strategy_id   IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                           O_exclusion_ind    OUT RPM_CALENDAR_PERIOD.EXCLUSION_IND%TYPE,
                           O_exception_ind    OUT RPM_CALENDAR_PERIOD.EXCLUSION_IND%TYPE,
                           O_review_date      OUT DATE)

RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'RPM_EXT_SQL.GET_CALENDAR_INFO';

   L_tomorrow DATE := get_vdate + 1;

BEGIN

   select rcp.exclusion_ind,
          rcp.exception_ind,
          rcp.end_date into O_exclusion_ind,
                            O_exception_ind,
                            O_review_date
     from rpm_strategy rs,
          rpm_calendar rc,
          rpm_calendar_period rcp
    where rs.strategy_id = I_strategy_id
      and rc.calendar_id = rs.current_calendar_id
      and rc.calendar_id = rcp.calendar_id
      and L_tomorrow between rcp.start_date and rcp.end_date;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END GET_CALENDAR_INFO;
----------------------------------------------------------------------------------------
FUNCTION GET_MAINT_MARGIN_INFO(O_error_msg    IN OUT  VARCHAR2,
                               O_crp_start    IN OUT  DATE,
                               O_crp_end      IN OUT  DATE,
                               I_strategy_id  IN      RPM_STRATEGY.STRATEGY_ID%TYPE)
   RETURN BOOLEAN IS

   L_cost_forward_days     RPM_STRATEGY_MAINT_MARGIN.COST_FORWARD_DAYS%TYPE  := NULL;
   L_calendar_id           RPM_CALENDAR.CALENDAR_ID%TYPE                     := NULL;
   L_crp_start             RPM_CALENDAR_PERIOD.START_DATE%TYPE               := NULL;
   L_crp_end               RPM_CALENDAR_PERIOD.END_DATE%TYPE                 := NULL;
   L_days_between          RPM_CALENDAR.DAYS_BETWEEN%TYPE                    := NULL;
   L_first_review_period   VARCHAR2(1)                                       := NULL;
   L_last_review_period    VARCHAR2(1)                                       := NULL;

   cursor C_MAINT_MARGIN is
      select rsmm.cost_forward_days,
             rcp.calendar_id,
             rcp.start_date,
             rcp.end_date,
             rc.days_between
        from rpm_strategy rs,
             rpm_strategy_maint_margin rsmm,
             rpm_calendar rc,
             rpm_calendar_period rcp
       where rs.strategy_id   = I_strategy_id
         and rsmm.strategy_id = rs.strategy_id
         and rcp.calendar_id  = rs.current_calendar_id
         and rcp.calendar_id  = rc.calendar_id
         and LP_tomorrow between rcp.start_date and rcp.end_date;

   cursor C_CALENDAR_INFO is
      select decode(sign(min(cp.end_date) - LP_tomorrow),   -1, 'N', 'Y') first_rp_ind,
             decode(sign(LP_tomorrow - max(cp.start_date)), -1, 'N', 'Y') last_rp_ind
        from rpm_calendar_period cp
       where cp.calendar_id = L_calendar_id;

BEGIN

   -- Get the "Cost forward days" and current review period details
   open C_MAINT_MARGIN;
   fetch C_MAINT_MARGIN into L_cost_forward_days,
                             L_calendar_id,
                             L_crp_start,
                             L_crp_end,
                             L_days_between;
   close C_MAINT_MARGIN;

   if L_calendar_id is not NULL then
      -- Check if the Current review period is the First review period or the
      -- Last review period or niether First nor Last review period in the calendar
      open C_CALENDAR_INFO;
      fetch C_CALENDAR_INFO into L_first_review_period,
                                 L_last_review_period;
      close C_CALENDAR_INFO;

      -- The following rules will be used when determining the CRP start and end dates:
      --    a) If the Current review period is the First review period in the calendar,
      --       then the CRP start date will be equal to the Current review period's start date.
      --       If not, the CRP start date will be equal to the Current review period's
      --       start date plus the Cost forward days for the Maint margin strategy.
      --    b) If the Current review period is the Last review period in the calendar,
      --       then the CRP end date will be equal to the Current review period's end date.
      --       If not, the CRP end date will be equal to the NEXT review period's
      --       start date plus the (Cost forward days - 1) for the Maint margin strategy.

      if L_first_review_period = 'N' then
         O_crp_start := L_crp_start + L_cost_forward_days;
      else
         O_crp_start := L_crp_start;
      end if;
      if L_last_review_period = 'N' then
         O_crp_end := L_crp_end + L_days_between + L_cost_forward_days;
      else
         O_crp_end := L_crp_end;
      end if;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.GET_MAINT_MARGIN_INFO',
                                        to_char(SQLCODE));
      return FALSE;

END GET_MAINT_MARGIN_INFO;

----------------------------------------------------------------------------------------
FUNCTION INIT_EFFECTIVE_DATE_UPDATE(O_error_msg    IN OUT  VARCHAR2,
                                    I_edut         IN      OBJ_EFF_DATES_UPD_TBL)
   RETURN BOOLEAN IS

   cursor C_WORKSPACE_ID is
   select distinct workspace_id
     from rpm_me_eff_date_params_crp;

BEGIN

     delete from rpm_me_eff_date_params_crp;
     insert into rpm_me_eff_date_params_crp ( workspace_id,
                                              zone_id,
                                              item_parent,
                                              item,
                                              link_code,
                                              diff_id,
                                              proposed_strategy_id,
                                              dept,
                                              effective_date,
                                              crp_start,
                                              crp_end)
     select edut.workspace_id,
            edut.zone_id,
            edut.item_parent,
            edut.item,
            edut.link_code,
            edut.diff_id,
            edut.proposed_strategy_id,
            edut.dept,
            edut.effective_date,
            decode(sign(min(rcp2.end_date) - LP_tomorrow),
                   -1, rcp.start_date+rsmm.cost_forward_days, rcp.start_date) crp_start,
            decode(sign(LP_tomorrow - max(rcp2.start_date)),
                   -1, rcp.end_date+rc.days_between+rsmm.cost_forward_days, rcp.end_date) crp_end
       from table(cast(I_edut as OBJ_EFF_DATES_UPD_TBL)) edut,
            rpm_strategy rs,
            rpm_strategy_maint_margin rsmm,
            rpm_calendar rc,
            rpm_calendar_period rcp,
            rpm_calendar_period rcp2
      where rs.strategy_id   = edut.proposed_strategy_id
        and rsmm.strategy_id(+) = rs.strategy_id
        and rcp.calendar_id  = rs.current_calendar_id
        and rcp.calendar_id  = rc.calendar_id
        and LP_tomorrow      between rcp.start_date and rcp.end_date
        and rcp2.calendar_id = rc.calendar_id
     group by rsmm.cost_forward_days,
              rcp.calendar_id,
              rcp.start_date,
              rcp.end_date,
              rc.days_between,
              edut.workspace_id,
              edut.zone_id,
              edut.item_parent,
              edut.item,
              edut.link_code,
              edut.diff_id,
              edut.dept,
              edut.proposed_strategy_id,
              edut.effective_date;

   insert into rpm_me_item_gtt(
              worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              state,
              reset_state,
              item,
              item_desc,
              dept,
              dept_name,
              class,
              class_name,
              subclass,
              subclass_name,
              markup_calc_type,
              item_level,
              tran_level,
              standard_uom,
              diff_1,
              diff_type_1,
              diff_2,
              diff_type_2,
              diff_3,
              diff_type_3,
              diff_4,
              diff_type_4,
              retail_include_vat_ind,
              primary_supplier,
              vpn,
              uda_boolean,
              package_size,
              package_uom,
              retail_label_type,
              retail_label_value,
              season_phase_boolean,
              original_retail,
              item_parent,
              parent_desc,
              parent_diff_1,
              parent_diff_2,
              parent_diff_3,
              parent_diff_4,
              parent_diff_1_type,
              parent_diff_2_type,
              parent_diff_3_type,
              parent_diff_4_type,
              parent_diff_1_type_desc,
              parent_diff_2_type_desc,
              parent_diff_3_type_desc,
              parent_diff_4_type_desc,
              parent_vpn,
              zone_group_id,
              zone_group_display_id,
              zone_group_name,
              zone_id,
              zone_display_id,
              zone_name,
              currency,
              margin_mkt_basket_code,
              comp_mkt_basket_code,
              margin_mbc_name,
              competitive_mbc_name,
              constraint_boolean,
              ignore_constraint_boolean,
              area_diff_prim_id,
              area_diff_id,
              action_flag)
       select rww.worksheet_item_data_id,
              rww.worksheet_status_id,
              rww.proposed_strategy_id,
              rww.state,
              rww.reset_state,
              rww.item,
              rww.item_desc,
              rww.dept,
              rww.dept_name,
              rww.class,
              rww.class_name,
              rww.subclass,
              rww.subclass_name,
              rww.markup_calc_type,
              rww.item_level,
              rww.tran_level,
              rww.standard_uom,
              rww.diff_1,
              rww.diff_type_1,
              rww.diff_2,
              rww.diff_type_2,
              rww.diff_3,
              rww.diff_type_3,
              rww.diff_4,
              rww.diff_type_4,
              rww.retail_include_vat_ind,
              rww.primary_supplier,
              rww.vpn,
              rww.uda_boolean,
              rww.package_size,
              rww.package_uom,
              rww.retail_label_type,
              rww.retail_label_value,
              rww.season_phase_boolean,
              rww.original_retail,
              rww.item_parent,
              rww.parent_desc,
              rww.parent_diff_1,
              rww.parent_diff_2,
              rww.parent_diff_3,
              rww.parent_diff_4,
              rww.parent_diff_1_type,
              rww.parent_diff_2_type,
              rww.parent_diff_3_type,
              rww.parent_diff_4_type,
              rww.parent_diff_1_type_desc,
              rww.parent_diff_2_type_desc,
              rww.parent_diff_3_type_desc,
              rww.parent_diff_4_type_desc,
              rww.parent_vpn,
              rww.zone_group_id,
              rww.zone_group_display_id,
              rww.zone_group_name,
              rww.zone_id,
              rww.zone_display_id,
              rww.zone_name,
              rww.currency,
              rww.margin_mkt_basket_code,
              rww.comp_mkt_basket_code,
              rww.margin_mbc_name,
              rww.competitive_mbc_name,
              rww.constraint_boolean,
              rww.ignore_constraint_boolean,
              rww.area_diff_prim_id,
              rww.area_diff_id,
              rww.action_flag
         from rpm_worksheet_zone_workspace rww,
              rpm_me_eff_date_params_crp edut
        where rww.workspace_id = edut.workspace_id
          and (   edut.link_code is null
               or rww.link_code = edut.link_code)
          and (   edut.item is NULL
               or rww.item = edut.item)
          and (   edut.item_parent is NULL
               or (rww.item_parent = edut.item_parent and (edut.diff_id is NULL or
                                                    edut.diff_id =  rww.diff_1 or
                                                    edut.diff_id =  rww.diff_2 or
                                                    edut.diff_id =  rww.diff_3 or
                                                    edut.diff_id =  rww.diff_4)))
          and (   edut.zone_id is NULL
               or rww.zone_id = edut.zone_id);

   for rec in C_WORKSPACE_ID loop
      insert into rpm_me_item_loc_gtt (
              worksheet_item_loc_data_id,
              worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              user_strategy_id,
              item,
              workspace_rowid,
              location,
              loc_type,
              il_primary_supp,
              il_primary_cntry,
              location_stock,
              link_code,
              link_code_desc,
              link_code_display_id,
              replenish_ind,
              vat_rate,
              vat_value,
              historical_sales,
              historical_sales_units,
              historical_issues,
              projected_sales,
              seasonal_sales,
              first_received_date,
              last_received_date,
              wks_first_sale,
              wks_of_sales_exp,
              new_item_loc_boolean,
              proposed_retail,
              proposed_retail_uom,
              proposed_retail_std,
              proposed_clear_mkdn_nbr,
              proposed_clear_ind,
              action_flag,
              effective_date,
              new_retail,
              new_retail_uom,
              new_retail_std,
              new_multi_unit_ind,
              new_multi_units,
              new_multi_unit_retail,
              new_multi_unit_uom,
              new_clear_mkdn_nbr,
              new_clear_ind,
              new_reset_date,
              new_out_of_stock_date,
              original_effective_date,
              rule_boolean,
              conflict_boolean,
              lock_version,
              supp_currency,
              zone_id,
              item_parent,
              diff_id,
              crp_start,
              crp_end)
       select /*+ ORDERED */
              rww.worksheet_item_loc_data_id,
              rww.worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              user_strategy_id,
              item,
              rww.rowid,
              location,
              loc_type,
              il_primary_supp,
              il_primary_cntry,
              location_stock,
              link_code,
              link_code_desc,
              link_code_display_id,
              replenish_ind,
              vat_rate,
              vat_value,
              historical_sales,
              historical_sales_units,
              historical_issues,
              projected_sales,
              seasonal_sales,
              first_received_date,
              last_received_date,
              wks_first_sale,
              wks_of_sales_exp,
              new_item_loc_boolean,
              proposed_retail,
              proposed_retail_uom,
              proposed_retail_std,
              proposed_clear_mkdn_nbr,
              proposed_clear_ind,
              action_flag,
              effective_date,
              new_retail,
              new_retail_uom,
              new_retail_std,
              new_multi_unit_ind,
              new_multi_units,
              new_multi_unit_retail,
              new_multi_unit_uom,
              new_clear_mkdn_nbr,
              new_clear_ind,
              new_reset_date,
              new_out_of_stock_date,
              original_effective_date,
              rule_boolean,
              conflict_boolean,
              lock_version,
              nvl(s.currency_code,rwz.currency),
              zone_id,
              rwz.item_parent,
              rwz.diff_1,
              rwz.crp_start,
              rwz.crp_end
         from (select rwz.workspace_id,
                      rwz.worksheet_item_data_id,
                      rwz.currency,
                      edut.crp_start,
                      edut.crp_end,
                      rwz.item_parent,
                      rwz.diff_1
                 from rpm_worksheet_zone_workspace rwz,
                      rpm_me_eff_date_params_crp edut
                where edut.workspace_id = rec.workspace_id
                  and rwz.workspace_id = edut.workspace_id
                  and (   edut.link_code is null
                       or rwz.link_code = edut.link_code)
                  and (   edut.item is NULL
                       or rwz.item = edut.item)
                  and (   edut.item_parent is NULL
                       or (rwz.item_parent = edut.item_parent and (edut.diff_id is NULL or
                                                                   edut.diff_id =  rwz.diff_1 or
                                                                   edut.diff_id =  rwz.diff_2 or
                                                                   edut.diff_id =  rwz.diff_3 or
                                                                   edut.diff_id =  rwz.diff_4)))
                  and (   edut.zone_id is NULL
                       or rwz.zone_id = edut.zone_id)
                  and rownum > 0) rwz,
              rpm_worksheet_il_workspace rww,
              sups s
        where rww.workspace_id = rec.workspace_id
          and rww.worksheet_item_data_id = rwz.worksheet_item_data_id
          and rww.il_primary_supp = s.supplier(+);

   end loop;

   if REMOVE_AREA_DIFF_EXCLUDES(O_error_msg,
                                'Y') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.INIT_EFFECTIVE_DATE_UPDATE',
                                        to_char(SQLCODE));
      return FALSE;

END INIT_EFFECTIVE_DATE_UPDATE;

----------------------------------------------------------------------------------------
FUNCTION INIT_WORKSHEET_RESET(O_error_msg            IN OUT  VARCHAR2,
                              O_effective_date       IN OUT  RPM_WORKSHEET_ITEM_LOC_DATA.EFFECTIVE_DATE%TYPE,
                              O_zone_id              IN OUT  RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                              I_worksheet_status_id  IN      RPM_WORKSHEET_ITEM_DATA.WORKSHEET_STATUS_ID%TYPE,
                              I_workspace_id         IN      RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE,
                              I_strategy_id          IN      RPM_WORKSHEET_ZONE_WORKSPACE.PROPOSED_STRATEGY_ID%TYPE)
   RETURN BOOLEAN IS

   cursor C_OUTPUT is
      select z.zone_id,
             d.effective_date
        from (select min(zone_id) zone_id from rpm_me_item_gtt) z,
             (select min(effective_date) effective_date from rpm_me_item_loc_gtt) d;

BEGIN

   insert all
   when rank_value = 1 then
      into rpm_me_item_gtt(
              worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              item,
              item_desc,
              dept,
              dept_name,
              class,
              class_name,
              subclass,
              subclass_name,
              markup_calc_type,
              item_level,
              tran_level,
              standard_uom,
              diff_1,
              diff_type_1,
              diff_2,
              diff_type_2,
              diff_3,
              diff_type_3,
              diff_4,
              diff_type_4,
              retail_include_vat_ind,
              primary_supplier,
              vpn,
              uda_boolean,
              package_size,
              package_uom,
              retail_label_type,
              retail_label_value,
              season_phase_boolean,
              original_retail,
              item_parent,
              parent_desc,
              parent_diff_1,
              parent_diff_2,
              parent_diff_3,
              parent_diff_4,
              parent_diff_1_type,
              parent_diff_2_type,
              parent_diff_3_type,
              parent_diff_4_type,
              parent_diff_1_type_desc,
              parent_diff_2_type_desc,
              parent_diff_3_type_desc,
              parent_diff_4_type_desc,
              parent_vpn,
              primary_comp_retail,
              primary_comp_retail_uom,
              primary_comp_store,
              primary_comp_retail_std,
              primary_multi_units,
              primary_multi_unit_retail,
              primary_multi_unit_retail_uom,
              primary_comp_boolean,
              a_comp_retail,
              b_comp_retail,
              c_comp_retail,
              d_comp_retail,
              e_comp_retail,
              a_comp_store,
              b_comp_store,
              c_comp_store,
              d_comp_store,
              e_comp_store,
              zone_group_id,
              zone_group_display_id,
              zone_group_name,
              zone_id,
              zone_display_id,
              zone_name,
              currency,
              margin_mkt_basket_code,
              comp_mkt_basket_code,
              margin_mbc_name,
              competitive_mbc_name,
              ref_wh_stock,
              ref_wh_on_order,
              ref_wh_inventory,
              current_zl_regular_retail,
              current_zl_regular_retail_uom,
              current_zl_regular_retail_std,
              current_zl_multi_units,
              current_zl_multi_unit_retail,
              current_zl_multi_uom,
              current_zl_clear_retail,
              current_zl_clear_retail_uom,
              current_zl_clear_retail_std,
              basis_zl_regular_retail,
              basis_zl_regular_retail_uom,
              basis_zl_regular_retail_std,
              basis_zl_multi_units,
              basis_zl_multi_unit_retail,
              basis_zl_multi_uom,
              basis_zl_clear_retail,
              basis_zl_clear_retail_uom,
              basis_zl_clear_retail_std,
              basis_zl_clear_mkdn_nbr,
              current_zl_cost,
              basis_zl_base_cost,
              basis_zl_pricing_cost,
              pend_zl_cost_chg_cost,
              proposed_zl_reset_date,
              proposed_zl_out_of_stock_date,
              constraint_boolean,
              ignore_constraint_boolean,
              area_diff_prim_id,
              area_diff_id,
              action_flag)
      values (worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              item,
              item_desc,
              dept,
              dept_name,
              class,
              class_name,
              subclass,
              subclass_name,
              markup_calc_type,
              item_level,
              tran_level,
              standard_uom,
              diff_1,
              diff_type_1,
              diff_2,
              diff_type_2,
              diff_3,
              diff_type_3,
              diff_4,
              diff_type_4,
              retail_include_vat_ind,
              primary_supplier,
              vpn,
              uda_boolean,
              package_size,
              package_uom,
              retail_label_type,
              retail_label_value,
              season_phase_boolean,
              original_retail,
              item_parent,
              parent_desc,
              parent_diff_1,
              parent_diff_2,
              parent_diff_3,
              parent_diff_4,
              parent_diff_1_type,
              parent_diff_2_type,
              parent_diff_3_type,
              parent_diff_4_type,
              parent_diff_1_type_desc,
              parent_diff_2_type_desc,
              parent_diff_3_type_desc,
              parent_diff_4_type_desc,
              parent_vpn,
              primary_comp_retail,
              primary_comp_retail_uom,
              primary_comp_store,
              primary_comp_retail_std,
              primary_multi_units,
              primary_multi_unit_retail,
              primary_multi_unit_retail_uom,
              primary_comp_boolean,
              a_comp_retail,
              b_comp_retail,
              c_comp_retail,
              d_comp_retail,
              e_comp_retail,
              a_comp_store,
              b_comp_store,
              c_comp_store,
              d_comp_store,
              e_comp_store,
              zone_group_id,
              zone_group_display_id,
              zone_group_name,
              zone_id,
              zone_display_id,
              zone_name,
              currency,
              margin_mkt_basket_code,
              comp_mkt_basket_code,
              margin_mbc_name,
              competitive_mbc_name,
              ref_wh_stock,
              ref_wh_on_order,
              ref_wh_inventory,
              current_zl_regular_retail,
              current_zl_regular_retail_uom,
              current_zl_regular_retail_std,
              current_zl_multi_units,
              current_zl_multi_unit_retail,
              current_zl_multi_uom,
              current_zl_clear_retail,
              current_zl_clear_retail_uom,
              current_zl_clear_retail_std,
              basis_zl_regular_retail,
              basis_zl_regular_retail_uom,
              basis_zl_regular_retail_std,
              basis_zl_multi_units,
              basis_zl_multi_unit_retail,
              basis_zl_multi_uom,
              basis_zl_clear_retail,
              basis_zl_clear_retail_uom,
              basis_zl_clear_retail_std,
              basis_zl_clear_mkdn_nbr,
              current_zl_cost,
              basis_zl_base_cost,
              basis_zl_pricing_cost,
              pend_zl_cost_chg_cost,
              proposed_zl_reset_date,
              proposed_zl_out_of_stock_date,
              constraint_boolean,
              ignore_constraint_boolean,
              area_diff_prim_id,
              area_diff_id,
              action_flag)
   when 0 = 0 then
      into rpm_me_item_loc_gtt (
              worksheet_item_loc_data_id,
              worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              user_strategy_id,
              item,
              workspace_rowid,
              location,
              loc_type,
              il_primary_supp,
              il_primary_cntry,
              current_regular_retail,
              current_regular_retail_uom,
              current_regular_retail_std,
              current_multi_units,
              current_multi_unit_retail,
              current_multi_unit_uom,
              current_clear_retail,
              current_clear_retail_uom,
              current_clear_retail_std,
              basis_regular_retail,
              basis_regular_retail_uom,
              basis_regular_retail_std,
              basis_multi_units,
              basis_multi_unit_retail,
              basis_multi_unit_uom,
              basis_clear_retail,
              basis_clear_retail_uom,
              basis_clear_retail_std,
              basis_clear_mkdn_nbr,
              current_clearance_ind,
              clear_boolean,
              promo_boolean,
              price_change_boolean,
              current_cost,
              basis_base_cost,
              basis_pricing_cost,
              pend_cost_boolean,
              cost_alert,
              past_price_chg_date,
              past_cost_chg_date,
              pend_cost_chg_date,
              pend_cost_chg_cost,
              location_stock,
              location_on_order,
              location_inventory,
              link_code,
              link_code_desc,
              link_code_display_id,
              replenish_ind,
              vat_rate,
              vat_value,
              historical_sales,
              historical_sales_units,
              historical_issues,
              projected_sales,
              seasonal_sales,
              first_received_date,
              last_received_date,
              wks_first_sale,
              wks_of_sales_exp,
              new_item_loc_boolean,
              proposed_retail,
              proposed_retail_uom,
              proposed_retail_std,
              proposed_clear_mkdn_nbr,
              proposed_clear_ind,
              proposed_reset_date,
              proposed_out_of_stock_date,
              action_flag,
              effective_date,
              original_effective_date,
              rule_boolean,
              conflict_boolean,
              new_retail,
              new_retail_uom,
              new_retail_std,
              new_multi_unit_ind,
              new_multi_units,
              new_multi_unit_retail,
              new_multi_unit_uom,
              new_clear_mkdn_nbr,
              new_clear_ind,
              new_reset_date,
              new_out_of_stock_date,
              supp_currency,
              zone_id,
              item_parent,
              diff_id,
              crp_start,
              crp_end)
      values (worksheet_item_loc_data_id,
              worksheet_item_data_id,
              worksheet_status_id,
              proposed_strategy_id,
              user_strategy_id,
              item,
              rwil_rowid,
              location,
              loc_type,
              il_primary_supp,
              il_primary_cntry,
              current_regular_retail,
              current_regular_retail_uom,
              current_regular_retail_std,
              current_multi_units,
              current_multi_unit_retail,
              current_multi_unit_uom,
              current_clear_retail,
              current_clear_retail_uom,
              current_clear_retail_std,
              basis_regular_retail,
              basis_regular_retail_uom,
              basis_regular_retail_std,
              basis_multi_units,
              basis_multi_unit_retail,
              basis_multi_unit_uom,
              basis_clear_retail,
              basis_clear_retail_uom,
              basis_clear_retail_std,
              basis_clear_mkdn_nbr,
              current_clearance_ind,
              clear_boolean,
              promo_boolean,
              price_change_boolean,
              current_cost,
              basis_base_cost,
              basis_pricing_cost,
              pend_cost_boolean,
              cost_alert,
              past_price_chg_date,
              past_cost_chg_date,
              pend_cost_chg_date,
              pend_cost_chg_cost,
              location_stock,
              location_on_order,
              location_inventory,
              link_code,
              link_code_desc,
              link_code_display_id,
              replenish_ind,
              vat_rate,
              vat_value,
              historical_sales,
              historical_sales_units,
              historical_issues,
              projected_sales,
              seasonal_sales,
              first_received_date,
              last_received_date,
              wks_first_sale,
              wks_of_sales_exp,
              new_item_loc_boolean,
              proposed_retail,
              proposed_retail_uom,
              proposed_retail_std,
              proposed_clear_mkdn_nbr,
              proposed_clear_ind,
              proposed_reset_date,
              proposed_out_of_stock_date,
              action_flag,
              effective_date,
              original_effective_date,
              rule_boolean,
              conflict_boolean,
              new_retail,
              new_retail_uom,
              new_retail_std,
              new_multi_unit_ind,
              new_multi_units,
              new_multi_unit_retail,
              new_multi_unit_uom,
              new_clear_mkdn_nbr,
              new_clear_ind,
              new_reset_date,
              new_out_of_stock_date,
              currency_code,
              zone_id,
              item_parent,
              diff_1,
              crp_start,
              crp_end)
       select row_number() over(partition by rww.item, rww.zone_id order by rww.dept) rank_value,
              ---
              rww.item,
              rww.item_desc,
              rww.dept,
              rww.dept_name,
              rww.class,
              rww.class_name,
              rww.subclass,
              rww.subclass_name,
              markup_calc_type,
              item_level,
              tran_level,
              standard_uom,
              diff_1,
              diff_type_1,
              diff_2,
              diff_type_2,
              diff_3,
              diff_type_3,
              diff_4,
              diff_type_4,
              retail_include_vat_ind,
              primary_supplier,
              vpn,
              uda_boolean,
              package_size,
              package_uom,
              retail_label_type,
              retail_label_value,
              season_phase_boolean,
              original_retail,
              rww.item_parent,
              parent_desc,
              parent_diff_1,
              parent_diff_2,
              parent_diff_3,
              parent_diff_4,
              parent_diff_1_type,
              parent_diff_2_type,
              parent_diff_3_type,
              parent_diff_4_type,
              parent_diff_1_type_desc,
              parent_diff_2_type_desc,
              parent_diff_3_type_desc,
              parent_diff_4_type_desc,
              parent_vpn,
              primary_comp_retail,
              primary_comp_retail_uom,
              primary_comp_store,
              primary_comp_retail_std,
              primary_multi_units,
              primary_multi_unit_retail,
              primary_multi_unit_retail_uom,
              primary_comp_boolean,
              a_comp_retail,
              b_comp_retail,
              c_comp_retail,
              d_comp_retail,
              e_comp_retail,
              a_comp_store,
              b_comp_store,
              c_comp_store,
              d_comp_store,
              e_comp_store,
              zone_group_id,
              zone_group_display_id,
              zone_group_name,
              rww.zone_id,
              zone_display_id,
              zone_name,
              currency,
              margin_mkt_basket_code,
              comp_mkt_basket_code,
              margin_mbc_name,
              competitive_mbc_name,
              ref_wh_stock,
              ref_wh_on_order,
              ref_wh_inventory,
              current_zl_regular_retail,
              current_zl_regular_retail_uom,
              current_zl_regular_retail_std,
              current_zl_multi_units,
              current_zl_multi_unit_retail,
              current_zl_multi_uom,
              current_zl_clear_retail,
              current_zl_clear_retail_uom,
              current_zl_clear_retail_std,
              decode(rww.effective_date, original_effective_date, basis_zl_regular_retail, null) basis_zl_regular_retail,
              decode(rww.effective_date, original_effective_date, basis_zl_regular_retail_uom, null) basis_zl_regular_retail_uom,
              decode(rww.effective_date, original_effective_date, basis_zl_regular_retail_std, null) basis_zl_regular_retail_std,
              decode(rww.effective_date, original_effective_date, basis_zl_multi_units, null) basis_zl_multi_units,
              decode(rww.effective_date, original_effective_date, basis_zl_multi_unit_retail, null) basis_zl_multi_unit_retail,
              decode(rww.effective_date, original_effective_date, basis_zl_multi_uom, null) basis_zl_multi_uom,
              decode(rww.effective_date, original_effective_date, basis_zl_clear_retail, null) basis_zl_clear_retail,
              decode(rww.effective_date, original_effective_date, basis_zl_clear_retail_uom, null) basis_zl_clear_retail_uom,
              decode(rww.effective_date, original_effective_date, basis_zl_clear_retail_std, null) basis_zl_clear_retail_std,
              basis_zl_clear_mkdn_nbr,
              current_zl_cost,
              decode(rww.effective_date, original_effective_date, basis_zl_base_cost, null) basis_zl_base_cost,
              decode(rww.effective_date, original_effective_date, basis_zl_pricing_cost, null) basis_zl_pricing_cost,
              decode(rww.effective_date, original_effective_date, pend_zl_cost_chg_cost, null) pend_zl_cost_chg_cost,
              proposed_zl_reset_date,
              proposed_zl_out_of_stock_date,
              rwil.constraint_boolean,
              rww.ignore_constraint_boolean,
              area_diff_prim_id,
              area_diff_id,
              ---
              rwil.worksheet_item_loc_data_id,
              rww.worksheet_item_data_id,
              rww.worksheet_status_id,
              rwil.proposed_strategy_id,
              rwil.user_strategy_id,
              rwil.rowid as rwil_rowid,
              rwil.location,
              rwil.loc_type,
              rwil.il_primary_supp,
              rwil.il_primary_cntry,
              rwil.current_regular_retail,
              rwil.current_regular_retail_uom,
              rwil.current_regular_retail_std,
              rwil.current_multi_units,
              rwil.current_multi_unit_retail,
              rwil.current_multi_unit_uom,
              rwil.current_clear_retail,
              rwil.current_clear_retail_uom,
              rwil.current_clear_retail_std,
              decode(rww.effective_date, original_effective_date, rwil.basis_regular_retail, null) basis_regular_retail,
              decode(rww.effective_date, original_effective_date, rwil.basis_regular_retail_uom, null) basis_regular_retail_uom,
              decode(rww.effective_date, original_effective_date, rwil.basis_regular_retail_std, null) basis_regular_retail_std,
              decode(rww.effective_date, original_effective_date, rwil.basis_multi_units, null) basis_multi_units,
              decode(rww.effective_date, original_effective_date, rwil.basis_multi_unit_retail, null) basis_multi_unit_retail,
              decode(rww.effective_date, original_effective_date, rwil.basis_multi_unit_uom, null) basis_multi_unit_uom,
              decode(rww.effective_date, original_effective_date, rwil.basis_clear_retail, null) basis_clear_retail,
              decode(rww.effective_date, original_effective_date, rwil.basis_clear_retail_uom, null) basis_clear_retail_uom,
              decode(rww.effective_date, original_effective_date, rwil.basis_clear_retail_std, null) basis_clear_retail_std,
              basis_clear_mkdn_nbr,
              rwil.current_clearance_ind,
              decode(rww.effective_date, original_effective_date, rwil.clear_boolean, null) clear_boolean,
              decode(rww.effective_date, original_effective_date, rwil.promo_boolean, null) promo_boolean,
              decode(rww.effective_date, original_effective_date, rwil.price_change_boolean, null) price_change_boolean,
              current_cost,
              decode(rww.effective_date, original_effective_date, basis_base_cost, null) basis_base_cost,
              decode(rww.effective_date, original_effective_date, basis_pricing_cost, null) basis_pricing_cost,
              pend_cost_boolean,
              cost_alert,
              rwil.past_price_chg_date,
              rwil.past_cost_chg_date,
              rwil.pend_cost_chg_date,
              pend_cost_chg_cost,
              location_stock,
              location_on_order,
              location_inventory,
              rwil.link_code,
              rwil.link_code_desc,
              rwil.link_code_display_id,
              rwil.replenish_ind,
              rwil.vat_rate,
              NVL(rwil.vat_value,0) vat_value,
              rwil.historical_sales,
              historical_sales_units,
              historical_issues,
              rwil.projected_sales,
              seasonal_sales,
              rwil.first_received_date,
              rwil.last_received_date,
              rwil.wks_first_sale,
              rwil.wks_of_sales_exp,
              rwil.new_item_loc_boolean,
              rwil.proposed_retail,
              rwil.proposed_retail_uom,
              rwil.proposed_retail_std,
              rwil.proposed_clear_mkdn_nbr,
              proposed_clear_ind,
              proposed_reset_date,
              proposed_out_of_stock_date,
              rww.action_flag,
              decode(rww.effective_date, original_effective_date, rww.effective_date, original_effective_date) effective_date,
              original_effective_date,
              rwil.rule_boolean,
              rwil.conflict_boolean,
              rwil.proposed_retail          as new_retail,
              rwil.proposed_retail_uom      as new_retail_uom,
              rwil.proposed_retail_std      as new_retail_std,
              null                        as new_multi_unit_ind,
              null                        as new_multi_units,
              null                        as new_multi_unit_retail,
              null                        as new_multi_unit_uom,
              rww.proposed_clear_mkdn_nbr   as new_clear_mkdn_nbr,
              proposed_clear_ind          as new_clear_ind,
              proposed_reset_date         as new_reset_date,
              proposed_out_of_stock_date  as new_out_of_stock_date,
              s.currency_code,
              LP_crp_start                as crp_start,
              LP_crp_end                  as crp_end
         from rpm_worksheet_zone_workspace rww,
              rpm_worksheet_il_workspace rwil,
              sups s
        where rww.worksheet_status_id  = I_worksheet_status_id
          and rww.workspace_id         = I_workspace_id
          and rww.proposed_strategy_id = I_strategy_id
          and rww.state in (NEW_STATE, UPDATE_STATE)
          and rwil.workspace_id        = rww.workspace_id
          and rwil.worksheet_status_id = rww.worksheet_status_id
          and rwil.worksheet_item_data_id = rww.worksheet_item_data_id
          and rwil.il_primary_supp = s.supplier(+);

   open C_OUTPUT;
   fetch C_OUTPUT into O_zone_id,
                       O_effective_date;
   close C_OUTPUT;

   if REMOVE_AREA_DIFF_EXCLUDES(O_error_msg,
                                'Y') = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.INIT_WORKSHEET_RESET',
                                        to_char(SQLCODE));
      return FALSE;

END INIT_WORKSHEET_RESET;

----------------------------------------------------------------------------------------
FUNCTION INIT_RPM_EXT_DATA(O_error_msg     IN OUT  VARCHAR2,
                           I_strategy_id   IN      RPM_STRATEGY.STRATEGY_ID%TYPE,
                           I_initial_call  IN      VARCHAR2,
                           I_dept          IN      RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                           I_class         IN      RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                           I_subclass      IN      RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                           I_zone_id       IN      RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                           I_location      IN      RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE)
   RETURN BOOLEAN IS

   cursor C_FEOW_OF_MONTH is
       select max(c.first_day),
              sv.last_eow_date_unit,
              sv.last_eom_date_unit
         from calendar c,
              system_variables sv
        where c.first_day < sv.last_eom_date_unit
        group by sv.last_eow_date_unit,
                 sv.last_eom_date_unit;

BEGIN

   open C_FEOW_OF_MONTH;
   fetch C_FEOW_OF_MONTH INTO LP_first_day_of_month,
                              LP_last_eow_date_unit,
                              LP_last_eom_date_unit;
   close C_FEOW_OF_MONTH;

   if not LP_do_update then

      insert into rpm_me_item_gtt (
         worksheet_item_data_id,
         worksheet_status_id,
         proposed_strategy_id,
         state,
         reset_state,
         item,
         item_desc,
         dept,
         dept_name,
         class,
         class_name,
         subclass,
         subclass_name,
         markup_calc_type,
         item_level,
         tran_level,
         standard_uom,
         diff_1,
         diff_type_1,
         diff_2,
         diff_type_2,
         diff_3,
         diff_type_3,
         diff_4,
         diff_type_4,
         retail_include_vat_ind,
         primary_supplier,
         vpn,
         uda_boolean,
         package_size,
         package_uom,
         retail_label_type,
         retail_label_value,
         season_phase_boolean,
         original_retail,
         item_parent,
         parent_desc,
         parent_diff_1,
         parent_diff_2,
         parent_diff_3,
         parent_diff_4,
         parent_diff_1_type,
         parent_diff_2_type,
         parent_diff_3_type,
         parent_diff_4_type,
         parent_diff_1_type_desc,
         parent_diff_2_type_desc,
         parent_diff_3_type_desc,
         parent_diff_4_type_desc,
         parent_vpn,
         primary_comp_retail,
         primary_comp_retail_uom,
         primary_comp_store,
         primary_comp_retail_std,
         primary_multi_units,
         primary_multi_unit_retail,
         primary_multi_unit_retail_uom,
         primary_comp_boolean,
         a_comp_retail,
         b_comp_retail,
         c_comp_retail,
         d_comp_retail,
         e_comp_retail,
         a_comp_store,
         b_comp_store,
         c_comp_store,
         d_comp_store,
         e_comp_store,
         zone_group_id,
         zone_group_display_id,
         zone_group_name,
         zone_id,
         zone_display_id,
         zone_name,
         currency,
         margin_mkt_basket_code,
         comp_mkt_basket_code,
         margin_mbc_name,
         competitive_mbc_name,
         ref_wh_stock,
         ref_wh_on_order,
         ref_wh_inventory,
         current_zl_cost,
         basis_zl_base_cost,
         basis_zl_pricing_cost,
         pend_zl_cost_chg_cost,
         maint_margin_zl_retail,
         maint_margin_zl_retail_uom,
         maint_margin_zl_retail_std,
         maint_margin_zl_cost,
         constraint_boolean,
         ignore_constraint_boolean,
         area_diff_prim_id,
         area_diff_id,
         action_flag)
  select worksheet_item_data_id,
         worksheet_status_id,
         proposed_strategy_id,
         decode(state,
                PENDING_STATE, PENDING_STATE,
                UPDATE_STATE),
         decode(state,
                PENDING_STATE, PENDING_STATE,
                UPDATE_STATE),
         item,
         item_desc,
         dept,
         dept_name,
         class,
         class_name,
         subclass,
         subclass_name,
         markup_calc_type,
         item_level,
         tran_level,
         standard_uom,
         diff_1,
         diff_type_1,
         diff_2,
         diff_type_2,
         diff_3,
         diff_type_3,
         diff_4,
         diff_type_4,
         retail_include_vat_ind,
         primary_supplier,
         vpn,
         uda_boolean,
         package_size,
         package_uom,
         retail_label_type,
         retail_label_value,
         season_phase_boolean,
         original_retail,
         item_parent,
         parent_desc,
         parent_diff_1,
         parent_diff_2,
         parent_diff_3,
         parent_diff_4,
         parent_diff_1_type,
         parent_diff_2_type,
         parent_diff_3_type,
         parent_diff_4_type,
         parent_diff_1_type_desc,
         parent_diff_2_type_desc,
         parent_diff_3_type_desc,
         parent_diff_4_type_desc,
         parent_vpn,
         primary_comp_retail,
         primary_comp_retail_uom,
         primary_comp_store,
         primary_comp_retail_std,
         primary_multi_units,
         primary_multi_unit_retail,
         primary_multi_unit_retail_uom,
         primary_comp_boolean,
         a_comp_retail,
         b_comp_retail,
         c_comp_retail,
         d_comp_retail,
         e_comp_retail,
         a_comp_store,
         b_comp_store,
         c_comp_store,
         d_comp_store,
         e_comp_store,
         zone_group_id,
         zone_group_display_id,
         zone_group_name,
         zone_id,
         zone_display_id,
         zone_name,
         currency,
         margin_mkt_basket_code,
         comp_mkt_basket_code,
         margin_mbc_name,
         competitive_mbc_name,
         ref_wh_stock,
         ref_wh_on_order,
         ref_wh_inventory,
         current_zl_cost,
         basis_zl_base_cost,
         basis_zl_pricing_cost,
         pend_zl_cost_chg_cost,
         maint_margin_zl_retail,
         maint_margin_zl_retail_uom,
         maint_margin_zl_retail_std,
         maint_margin_zl_cost,
         constraint_boolean,
         ignore_constraint_boolean,
         area_diff_prim_id,
         area_diff_id,
         action_flag
    from rpm_worksheet_item_data rwd
   where proposed_strategy_id = I_strategy_id
     and dept = I_dept
     and (   I_class is NULL
          or class = I_class)
     and (   I_subclass is NULL
          or subclass = I_subclass)
     and zone_id = I_zone_id
     and ((I_initial_call = 'Y' and area_diff_id is NULL)
          or
          (I_initial_call = 'N' and area_diff_id is not NULL));

      insert into rpm_me_item_loc_gtt (
         worksheet_item_loc_data_id,
         worksheet_item_data_id,
         worksheet_status_id,
         proposed_strategy_id,
         user_strategy_id,
         item,
         location,
         loc_type,
         il_primary_supp,
         il_primary_cntry,
         current_cost,
         basis_base_cost,
         basis_pricing_cost,
         pend_cost_boolean,
         cost_alert,
         past_price_chg_date,
         past_cost_chg_date,
         pend_cost_chg_date,
         pend_cost_chg_cost,
         maint_margin_retail,
         maint_margin_retail_uom,
         maint_margin_retail_std,
         maint_margin_cost,
         location_stock,
         location_on_order,
         location_inventory,
         link_code,
         link_code_desc,
         link_code_display_id,
         replenish_ind,
         vat_rate,
         vat_value,
         historical_sales,
         historical_sales_units,
         historical_issues,
         projected_sales,
         seasonal_sales,
         first_received_date,
         last_received_date,
         wks_first_sale,
         wks_of_sales_exp,
         new_item_loc_boolean,
         proposed_retail,
         proposed_retail_uom,
         proposed_retail_std,
         proposed_clear_mkdn_nbr,
         proposed_clear_ind,
         action_flag,
         effective_date,
         new_retail,
         new_retail_uom,
         new_retail_std,
         new_multi_unit_ind,
         new_multi_units,
         new_multi_unit_retail,
         new_multi_unit_uom,
         new_clear_mkdn_nbr,
         new_clear_ind,
         new_reset_date,
         new_out_of_stock_date,
         original_effective_date,
         rule_boolean,
         conflict_boolean,
         zone_id,
         item_parent,
         diff_id,
         crp_start,
         crp_end)
  select rwil.worksheet_item_loc_data_id,
         rwil.worksheet_item_data_id,
         rwil.worksheet_status_id,
         rwil.proposed_strategy_id,
         rwil.user_strategy_id,
         rwil.item,
         rwil.location,
         rwil.loc_type,
         rwil.il_primary_supp,
         rwil.il_primary_cntry,
         rwil.current_cost,
         rwil.basis_base_cost,
         rwil.basis_pricing_cost,
         rwil.pend_cost_boolean,
         rwil.cost_alert,
         rwil.past_price_chg_date,
         rwil.past_cost_chg_date,
         rwil.pend_cost_chg_date,
         rwil.pend_cost_chg_cost,
         rwil.maint_margin_retail,
         rwil.maint_margin_retail_uom,
         rwil.maint_margin_retail_std,
         rwil.maint_margin_cost,
         rwil.location_stock,
         rwil.location_on_order,
         rwil.location_inventory,
         rwil.link_code,
         rwil.link_code_desc,
         rwil.link_code_display_id,
         rwil.replenish_ind,
         rwil.vat_rate,
         nvl(rwil.vat_value,0) vat_value,
         rwil.historical_sales,
         rwil.historical_sales_units,
         rwil.historical_issues,
         rwil.projected_sales,
         rwil.seasonal_sales,
         rwil.first_received_date,
         rwil.last_received_date,
         rwil.wks_first_sale,
         rwil.wks_of_sales_exp,
         rwil.new_item_loc_boolean,
         rwil.proposed_retail,
         rwil.proposed_retail_uom,
         rwil.proposed_retail_std,
         rwil.proposed_clear_mkdn_nbr,
         rwil.proposed_clear_ind,
         rwil.action_flag,
         rwil.effective_date,
         rwil.new_retail,
         rwil.new_retail_uom,
         rwil.new_retail_std,
         rwil.new_multi_unit_ind,
         rwil.new_multi_units,
         rwil.new_multi_unit_retail,
         rwil.new_multi_unit_uom,
         rwil.new_clear_mkdn_nbr,
         rwil.new_clear_ind,
         rwil.new_reset_date,
         rwil.new_out_of_stock_date,
         rwil.original_effective_date,
         rwil.rule_boolean,
         rwil.conflict_boolean,
         rwil.zone_id,
         rwd.item_parent,
         rwd.diff_1,
         LP_crp_start,
         LP_crp_end
    from rpm_worksheet_item_loc_data rwil,
         rpm_worksheet_item_data rwd
   where rwil.proposed_strategy_id = I_strategy_id
     and rwil.dept = I_dept
     and (   I_class is NULL
          or rwil.class = I_class)
     and (   I_subclass is NULL
          or rwil.subclass = I_subclass)
     and rwil.zone_id = I_zone_id
     and rwd.worksheet_item_data_id = rwil.worksheet_item_data_id
     and ((I_initial_call = 'Y' and rwd.area_diff_id is NULL)
          or
          (I_initial_call = 'N' and rwd.area_diff_id is not NULL));

   elsif LP_do_update then

      insert into rpm_me_item_gtt (
             worksheet_item_data_id,
             worksheet_status_id,
             proposed_strategy_id,
             state,
             reset_state,
             item,
             item_desc,
             dept,
             dept_name,
             class,
             class_name,
             subclass,
             subclass_name,
             markup_calc_type,
             item_level,
             tran_level,
             standard_uom,
             diff_1,
             diff_type_1,
             diff_2,
             diff_type_2,
             diff_3,
             diff_type_3,
             diff_4,
             diff_type_4,
             retail_include_vat_ind,
             primary_supplier,
             vpn,
             uda_boolean,
             package_size,
             package_uom,
             retail_label_type,
             retail_label_value,
             season_phase_boolean,
             original_retail,
             item_parent,
             parent_desc,
             parent_diff_1,
             parent_diff_2,
             parent_diff_3,
             parent_diff_4,
             parent_diff_1_type,
             parent_diff_2_type,
             parent_diff_3_type,
             parent_diff_4_type,
             parent_diff_1_type_desc,
             parent_diff_2_type_desc,
             parent_diff_3_type_desc,
             parent_diff_4_type_desc,
             parent_vpn,
             zone_group_id,
             zone_group_display_id,
             zone_group_name,
             zone_id,
             zone_display_id,
             zone_name,
             currency,
             margin_mkt_basket_code,
             comp_mkt_basket_code,
             margin_mbc_name,
             competitive_mbc_name,
             maint_margin_zl_retail,
             maint_margin_zl_retail_uom,
             maint_margin_zl_retail_std,
             maint_margin_zl_cost,
             constraint_boolean,
             ignore_constraint_boolean,
             area_diff_prim_id,
             area_diff_id,
             action_flag)
      select worksheet_item_data_id,
             worksheet_status_id,
             proposed_strategy_id,
             state,
             reset_state,
             item,
             item_desc,
             dept,
             dept_name,
             class,
             class_name,
             subclass,
             subclass_name,
             markup_calc_type,
             item_level,
             tran_level,
             standard_uom,
             diff_1,
             diff_type_1,
             diff_2,
             diff_type_2,
             diff_3,
             diff_type_3,
             diff_4,
             diff_type_4,
             retail_include_vat_ind,
             primary_supplier,
             vpn,
             uda_boolean,
             package_size,
             package_uom,
             retail_label_type,
             retail_label_value,
             season_phase_boolean,
             original_retail,
             item_parent,
             parent_desc,
             parent_diff_1,
             parent_diff_2,
             parent_diff_3,
             parent_diff_4,
             parent_diff_1_type,
             parent_diff_2_type,
             parent_diff_3_type,
             parent_diff_4_type,
             parent_diff_1_type_desc,
             parent_diff_2_type_desc,
             parent_diff_3_type_desc,
             parent_diff_4_type_desc,
             parent_vpn,
             zone_group_id,
             zone_group_display_id,
             zone_group_name,
             zone_id,
             zone_display_id,
             zone_name,
             currency,
             margin_mkt_basket_code,
             comp_mkt_basket_code,
             margin_mbc_name,
             competitive_mbc_name,
             maint_margin_zl_retail,
             maint_margin_zl_retail_uom,
             maint_margin_zl_retail_std,
             maint_margin_zl_cost,
             constraint_boolean,
             ignore_constraint_boolean,
             area_diff_prim_id,
             area_diff_id,
             action_flag
        from rpm_worksheet_item_data rwd
       where proposed_strategy_id = I_strategy_id
         and dept = I_dept
         and (   I_class is NULL
             or class = I_class)
         and (   I_subclass is NULL
             or subclass = I_subclass)
         and zone_id = I_zone_id
         and ((I_initial_call = 'Y' and area_diff_id is NULL)
              or
              (I_initial_call = 'N' and area_diff_id is not NULL));

      insert into rpm_me_item_loc_gtt (
             worksheet_item_loc_data_id,
             worksheet_item_data_id,
             worksheet_status_id,
             proposed_strategy_id,
             user_strategy_id,
             item,
             location,
             loc_type,
             il_primary_supp,
             il_primary_cntry,
             maint_margin_retail,
             maint_margin_retail_uom,
             maint_margin_retail_std,
             maint_margin_cost,
             location_stock,
             link_code,
             link_code_desc,
             link_code_display_id,
             replenish_ind,
             vat_rate,
             vat_value,
             historical_sales,
             historical_sales_units,
             historical_issues,
             projected_sales,
             seasonal_sales,
             first_received_date,
             last_received_date,
             wks_first_sale,
             wks_of_sales_exp,
             new_item_loc_boolean,
             proposed_retail,
             proposed_retail_uom,
             proposed_retail_std,
             proposed_clear_mkdn_nbr,
             proposed_clear_ind,
             action_flag,
             effective_date,
             new_retail,
             new_retail_uom,
             new_retail_std,
             new_multi_unit_ind,
             new_multi_units,
             new_multi_unit_retail,
             new_multi_unit_uom,
             new_clear_mkdn_nbr,
             new_clear_ind,
             new_reset_date,
             new_out_of_stock_date,
             original_effective_date,
             rule_boolean,
             conflict_boolean,
             zone_id,
             item_parent,
             diff_id,
             crp_start,
             crp_end)
      select rwil.worksheet_item_loc_data_id,
             rwil.worksheet_item_data_id,
             rwil.worksheet_status_id,
             rwil.proposed_strategy_id,
             rwil.user_strategy_id,
             rwil.item,
             rwil.location,
             rwil.loc_type,
             rwil.il_primary_supp,
             rwil.il_primary_cntry,
             rwil.maint_margin_retail,
             rwil.maint_margin_retail_uom,
             rwil.maint_margin_retail_std,
             rwil.maint_margin_cost,
             rwil.location_stock,
             rwil.link_code,
             rwil.link_code_desc,
             rwil.link_code_display_id,
             rwil.replenish_ind,
             rwil.vat_rate,
             nvl(rwil.vat_value,0) vat_value,
             rwil.historical_sales,
             rwil.historical_sales_units,
             rwil.historical_issues,
             rwil.projected_sales,
             rwil.seasonal_sales,
             rwil.first_received_date,
             rwil.last_received_date,
             rwil.wks_first_sale,
             rwil.wks_of_sales_exp,
             rwil.new_item_loc_boolean,
             rwil.proposed_retail,
             rwil.proposed_retail_uom,
             rwil.proposed_retail_std,
             rwil.proposed_clear_mkdn_nbr,
             rwil.proposed_clear_ind,
             rwil.action_flag,
             rwil.effective_date,
             rwil.new_retail,
             rwil.new_retail_uom,
             rwil.new_retail_std,
             rwil.new_multi_unit_ind,
             rwil.new_multi_units,
             rwil.new_multi_unit_retail,
             rwil.new_multi_unit_uom,
             rwil.new_clear_mkdn_nbr,
             rwil.new_clear_ind,
             rwil.new_reset_date,
             rwil.new_out_of_stock_date,
             rwil.original_effective_date,
             rwil.rule_boolean,
             rwil.conflict_boolean,
             rwil.zone_id,
             rwd.item_parent,
             rwd.diff_1,
             LP_crp_start,
             LP_crp_end
        from rpm_worksheet_item_loc_data rwil,
             rpm_worksheet_item_data rwd
       where rwil.proposed_strategy_id = I_strategy_id
         and rwil.dept = I_dept
         and (   I_class is NULL
              or rwil.class = I_class)
         and (   I_subclass is NULL
              or rwil.subclass = I_subclass)
         and rwil.zone_id = I_zone_id
         and rwd.worksheet_item_data_id = rwil.worksheet_item_data_id
         and ((I_initial_call = 'Y' and rwd.area_diff_id is NULL)
              or
              (I_initial_call = 'N' and rwd.area_diff_id is not NULL));

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.INIT_RPM_EXT_DATA',
                                        to_char(SQLCODE));
      return FALSE;

END INIT_RPM_EXT_DATA;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_BASIC_ITEM_INFO(O_error_msg    IN OUT VARCHAR2,
                                  I_strategy_id  IN     RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_STRATEGY_ID%TYPE,
                                  I_dept         IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                                  I_class        IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                                  I_subclass     IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                                  I_zone_id      IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                                  I_initial_call IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.POPULATE_BASIC_ITEM_INFO';

   L_state RPM_WORKSHEET_ITEM_DATA.STATE%TYPE := NULL;

   L_constraint_ind        RPM_SYSTEM_OPTIONS_DEF.DEF_WKSHT_PROMO_CONST_IND%TYPE := NULL;
   L_zone_group_id         RPM_WORKSHEET_ITEM_DATA.ZONE_GROUP_ID%TYPE            := NULL;
   L_zone_group_display_id RPM_WORKSHEET_ITEM_DATA.ZONE_GROUP_DISPLAY_ID%TYPE    := NULL;
   L_zone_group_name       RPM_WORKSHEET_ITEM_DATA.ZONE_GROUP_NAME%TYPE          := NULL;
   L_zone_id               RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE                  := NULL;
   L_zone_display_id       RPM_WORKSHEET_ITEM_DATA.ZONE_DISPLAY_ID%TYPE          := NULL;
   L_zone_name             RPM_WORKSHEET_ITEM_DATA.ZONE_NAME%TYPE                := NULL;
   L_zone_currency         RPM_WORKSHEET_ITEM_DATA.CURRENCY%TYPE                 := NULL;
   L_default_tax_type      SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE                  := NULL;
   L_class_level_vat_ind   SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE               := NULL;

BEGIN

   select def_wksht_promo_const_ind
     into L_constraint_ind
     from rpm_system_options_def;

   if I_initial_call = 'N' and
      LP_review_not_started then

      if LP_dynamic_area_diff_ind = 1 then
         L_state := NEW_STATE;
      else
         L_state := PENDING_STATE;
      end if;

   else
      L_state := NEW_STATE;

   end if;

   select default_tax_type,
          class_level_vat_ind
     into L_default_tax_type,
          L_class_level_vat_ind
     from system_options;

   select rzg.zone_group_id,
          rzg.zone_group_display_id,
          rzg.name,
          rz.zone_id,
          rz.zone_display_id,
          rz.name,
          rz.currency_code into L_zone_group_id,
                                L_zone_group_display_id,
                                L_zone_group_name,
                                L_zone_id,
                                L_zone_display_id,
                                L_zone_name,
                                L_zone_currency
     from rpm_zone_group rzg,
          rpm_zone rz
    where rzg.zone_group_id = rz.zone_group_id
      and rz.zone_id        = I_zone_id;

   merge into rpm_me_item_gtt target
   using (select i.item,
                 i.item_desc,
                 i.dept,
                 d.dept_name,
                 i.class,
                 c.class_name,
                 i.subclass,
                 sbc.sub_name subclass_name,
                 i.item_level,
                 i.tran_level,
                 i.standard_uom,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        i.diff_1) diff_1,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        d1.diff_type) diff_type_1,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        i.diff_2) diff_2,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        d2.diff_type) diff_type_2,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        i.diff_3) diff_3,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        d3.diff_type) diff_type_3,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        i.diff_4) diff_4,
                 DECODE(i.item_parent,
                        NULL, NULL,
                        d4.diff_type) diff_type_4,
                 s.supplier primary_supplier,
                 s.vpn,
                 i.package_size,
                 i.package_uom,
                 i.retail_label_type,
                 i.retail_label_value,
                 i.item_parent,
                 i.original_retail,
                 mrd.markup_calc_type,
                 case
                    when L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then
                       'Y'
                    when L_default_tax_type = RPM_CONSTANTS.SVAT_TAX_TYPE and
                         (L_class_level_vat_ind = 'N' or
                         (L_class_level_vat_ind = 'Y' and
                          c.class_vat_ind = 'Y')) then
                       'Y'
                    else
                       'N'
                 end retail_include_vat_ind,
                 margin.mkt_basket_code margin_mkt_basket_code,
                 comp.mkt_basket_code comp_mkt_basket_code,
                 margin.margin_name margin_mbc_name,
                 comp.comp_name competitive_mbc_name,
                 DECODE(I_initial_call,
                        'N', LP_area_diff_prim_id,
                        ad.area_diff_prim_id) area_diff_prim_id,
                 DECODE(I_initial_call,
                        'N', LP_area_diff_id,
                        NULL) area_diff_id
            from item_master i,
                 item_supplier s,
                 deps d,
                 class c,
                 subclass sbc,
                 rpm_merch_retail_def_expl mrd,
                 rpm_area_diff_prim_expl ad,
                 (select diff_id,
                         diff_type
                    from diff_ids
                  union
                  select diff_group_id diff_id,
                         diff_type
                    from diff_group_head) d1,
                 (select diff_id,
                         diff_type
                    from diff_ids
                  union
                  select diff_group_id diff_id,
                         diff_type
                    from diff_group_head) d2,
                 (select diff_id,
                         diff_type
                    from diff_ids
                  union
                  select diff_group_id diff_id,
                         diff_type
                    from diff_group_head) d3,
                 (select diff_id,
                         diff_type
                    from diff_ids
                  union
                  select diff_group_id diff_id,
                         diff_type
                    from diff_group_head) d4,
                  (select lov.name margin_name,
                          lov.mkt_basket_code,
                          mbc.item,
                          mbc.zone_id
                     from rpm_mbc_lov_values lov,
                          rpm_mbc_attribute mbc
                    where lov.mkt_basket_code = mbc.margin_mbc) margin,
                  (select lov.name comp_name,
                          lov.mkt_basket_code mkt_basket_code,
                          mbc.item,
                          mbc.zone_id
                    from rpm_mbc_lov_values lov,
                         rpm_mbc_attribute mbc
                    where lov.mkt_basket_code = mbc.competitive_mbc) comp
           where i.status               = RPM_CONSTANTS.APPROVED_ITEM_STATUS
             and i.sellable_ind         = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
             and i.item_level           = i.tran_level
             and i.diff_1               = d1.diff_id (+)
             and i.diff_2               = d2.diff_id (+)
             and i.diff_3               = d3.diff_id (+)
             and i.diff_4               = d4.diff_id (+)
             and i.dept                 = I_dept
             and i.dept                 = d.dept
             and i.dept                 = c.dept
             and i.class                = c.class
             and i.dept                 = sbc.dept
             and i.class                = sbc.class
             and i.subclass             = sbc.subclass
             and i.item                 = s.item (+)
             and s.primary_supp_ind (+) = 'Y'
             and i.dept                 = mrd.dept
             and i.class                = mrd.class
             and i.subclass             = mrd.subclass
             and i.item                 = comp.item (+)
             and I_zone_id              = comp.zone_id (+)
             and i.item                 = margin.item (+)
             and I_zone_id              = margin.zone_id (+)
             and i.dept                 = ad.dept (+)
             and i.class                = ad.class (+)
             and i.subclass             = ad.subclass (+)
             and ad.zone_hier_type (+)  = HIER_ZONE
             and ad.zone_hier_id (+)    = I_zone_id
             and (   (    I_class is NULL
                      and i.class NOT IN (select rs.class
                                            from rpm_strategy rs
                                           where rs.dept          = i.dept
                                             and rs.class         is NOT NULL
                                             and rs.subclass      is NULL
                                             and rs.zone_hier_id  = I_zone_id
                                             and rs.strategy_id  != I_strategy_id
                                             and rs.strategy_id  != (select rs1.copied_strategy_id
                                                                       from rpm_strategy rs1
                                                                      where rs1.strategy_id = I_strategy_id)))
                  or i.class = I_class)
             and (   (    I_subclass is NULL
                      and i.subclass NOT IN (select rs.subclass
                                               from rpm_strategy rs
                                              where rs.dept          = i.dept
                                                and rs.class         = i.class
                                                and rs.class         is NOT NULL
                                                and rs.subclass      is NOT NULL
                                                and rs.zone_hier_id  = I_zone_id
                                                and rs.strategy_id  != I_strategy_id
                                                and rs.strategy_id  != (select rs1.copied_strategy_id
                                                                          from rpm_strategy rs1
                                                                         where rs1.strategy_id = I_strategy_id)))
                  or i.subclass = I_subclass)) source
   on (target.item = source.item)
   when MATCHED then
      update
         set target.state                     = L_state,
             target.reset_state               = L_state,
             target.item_desc                 = source.item_desc,
             target.dept                      = source.dept,
             target.dept_name                 = source.dept_name,
             target.class                     = source.class,
             target.class_name                = source.class_name,
             target.subclass                  = source.subclass,
             target.subclass_name             = source.subclass_name,
             target.item_level                = source.item_level,
             target.tran_level                = source.tran_level,
             target.standard_uom              = source.standard_uom,
             target.diff_1                    = source.diff_1,
             target.diff_type_1               = source.diff_type_1,
             target.diff_2                    = source.diff_2,
             target.diff_type_2               = source.diff_type_2,
             target.diff_3                    = source.diff_3,
             target.diff_type_3               = source.diff_type_3,
             target.diff_4                    = source.diff_4,
             target.diff_type_4               = source.diff_type_4,
             target.primary_supplier          = source.primary_supplier,
             target.vpn                       = source.vpn,
             target.package_size              = source.package_size,
             target.package_uom               = source.package_uom,
             target.retail_label_type         = source.retail_label_type,
             target.retail_label_value        = source.retail_label_value,
             target.item_parent               = source.item_parent,
             target.original_retail           = source.original_retail,
             target.season_phase_boolean      = 0,
             target.uda_boolean               = 0,
             target.markup_calc_type          = source.markup_calc_type,
             target.retail_include_vat_ind    = source.retail_include_vat_ind,
             target.margin_mkt_basket_code    = source.margin_mkt_basket_code,
             target.comp_mkt_basket_code      = source.comp_mkt_basket_code,
             target.margin_mbc_name           = source.margin_mbc_name,
             target.competitive_mbc_name      = source.competitive_mbc_name,
             target.ignore_constraint_boolean = L_constraint_ind,
             target.zone_group_id             = L_zone_group_id,
             target.zone_group_display_id     = L_zone_group_display_id,
             target.zone_group_name           = L_zone_group_name,
             target.zone_id                   = L_zone_id,
             target.zone_display_id           = L_zone_display_id,
             target.zone_name                 = L_zone_name,
             target.currency                  = L_zone_currency,
             target.area_diff_prim_id         = source.area_diff_prim_id,
             target.area_diff_id              = source.area_diff_id
   when NOT MATCHED then
      insert (worksheet_item_data_id,
              proposed_strategy_id,
              state,
              reset_state,
              item,
              item_desc,
              dept,
              dept_name,
              class,
              class_name,
              subclass,
              subclass_name,
              item_level,
              tran_level,
              standard_uom,
              diff_1,
              diff_type_1,
              diff_2,
              diff_type_2,
              diff_3,
              diff_type_3,
              diff_4,
              diff_type_4,
              primary_supplier,
              vpn,
              package_size,
              package_uom,
              retail_label_type,
              retail_label_value,
              item_parent,
              original_retail,
              season_phase_boolean,
              uda_boolean,
              markup_calc_type,
              retail_include_vat_ind,
              margin_mkt_basket_code,
              comp_mkt_basket_code,
              margin_mbc_name,
              competitive_mbc_name,
              ignore_constraint_boolean,
              zone_group_id,
              zone_group_display_id,
              zone_group_name,
              zone_id,
              zone_display_id,
              zone_name,
              currency,
              area_diff_prim_id,
              area_diff_id,
              action_flag)
      values (RPM_WORKSHEET_ITEM_DATA_SEQ.NEXTVAL,
              I_strategy_id,
              L_state,
              L_state,
              source.item,
              source.item_desc,
              source.dept,
              source.dept_name,
              source.class,
              source.class_name,
              source.subclass,
              source.subclass_name,
              source.item_level,
              source.tran_level,
              source.standard_uom,
              source.diff_1,
              source.diff_type_1,
              source.diff_2,
              source.diff_type_2,
              source.diff_3,
              source.diff_type_3,
              source.diff_4,
              source.diff_type_4,
              source.primary_supplier,
              source.vpn,
              source.package_size,
              source.package_uom,
              source.retail_label_type,
              source.retail_label_value,
              source.item_parent,
              source.original_retail,
              0, -- season_phase_boolean
              0, -- uda_boolean
              source.markup_calc_type,
              source.retail_include_vat_ind,
              source.margin_mkt_basket_code,
              source.comp_mkt_basket_code,
              source.margin_mbc_name,
              source.competitive_mbc_name,
              L_constraint_ind, -- ignore_constraint_boolean,
              L_zone_group_id,
              L_zone_group_display_id,
              L_zone_group_name,
              L_zone_id,
              L_zone_display_id,
              L_zone_name,
              L_zone_currency,
              source.area_diff_prim_id,
              source.area_diff_id,
              0);

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_BASIC_ITEM_INFO;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_UDA(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   update rpm_me_item_gtt i
      set uda_boolean = 1
    where exists (select 'x'
                    from uda_item_date d
                   where d.item = i.item
                     and rownum = 1)
    or exists (select 'x'
                 from uda_item_lov l
                where l.item = i.item
                  and rownum = 1)
    or exists (select i.item
                 from uda_item_ff f
                where f.item = i.item
                  and rownum = 1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_UDA',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_UDA;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_SEASON(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   update rpm_me_item_gtt i
      set season_phase_boolean = 1
    where exists (select 'x'
                    from item_seasons s
                   where i.item = s.item
                     and rownum = 1);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_SEASON',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_SEASON;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_PARENT_INFO(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   update rpm_me_item_gtt up
     set (up.parent_desc,
          up.parent_diff_1,
          up.parent_diff_2,
          up.parent_diff_3,
          up.parent_diff_4,
          up.parent_diff_1_type,
          up.parent_diff_2_type,
          up.parent_diff_3_type,
          up.parent_diff_4_type,
          up.parent_diff_1_type_desc,
          up.parent_diff_2_type_desc,
          up.parent_diff_3_type_desc,
          up.parent_diff_4_type_desc,
          up.parent_vpn) =
                (select /*+ INDEX(i, pk_item_master) */
                        i.item_desc,
                        i.diff_1,
                        i.diff_2,
                        i.diff_3,
                        i.diff_4,
                        d1.diff_type,
                        d2.diff_type,
                        d3.diff_type,
                        d4.diff_type,
                        d1.description,
                        d2.description,
                        d3.description,
                        d4.description,
                        s.vpn
                   from item_master i,
                        item_supplier s,
                        v_diff_id_group_type d1,
                        v_diff_id_group_type d2,
                        v_diff_id_group_type d3,
                        v_diff_id_group_type d4
                  where up.item_parent        = i.item
                    and i.item                = s.item(+)
                    and s.primary_supp_ind(+) = 'Y'
                    and i.diff_1              = d1.id_group(+)
                    and i.diff_2              = d2.id_group(+)
                    and i.diff_3              = d3.id_group(+)
                    and i.diff_4              = d4.id_group(+))
    where exists (select 'x'
                    from item_master i
                   where up.item_parent     = i.item);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_PARENT_INFO',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_PARENT_INFO;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_COMP_RETAIL(O_error_msg    IN OUT  VARCHAR2,
                              I_strategy_id  IN      RPM_STRATEGY.STRATEGY_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.POPULATE_COMP_RETAIL';

BEGIN

   if LP_strategy_type = COMPETITIVE then
      update rpm_me_item_gtt up
         set (up.primary_comp_store,
              up.primary_comp_retail,
              up.primary_comp_retail_uom,
              up.primary_comp_retail_std,
              up.primary_multi_units,
              up.primary_multi_unit_retail,
              up.primary_multi_unit_retail_uom,
              up.primary_comp_boolean) = (select comp_store,
                                                 comp_retail,
                                                 standard_uom,
                                                 comp_retail_std,
                                                 multi_units,
                                                 multi_unit_retail,
                                                 multi_unit_retail_uom,
                                                 rpm_pull
                                            from (select cph.comp_store comp_store,
                                                         cph.comp_retail comp_retail,
                                                         gtt.standard_uom standard_uom,
                                                         cph.comp_retail comp_retail_std,
                                                         cph.multi_units multi_units,
                                                         cph.multi_unit_retail multi_unit_retail,
                                                         gtt.standard_uom multi_unit_retail_uom,
                                                         DECODE(cph.rpm_pull,
                                                                'R', 0,
                                                                1) rpm_pull,
                                                         cph.item item,
                                                         ROW_NUMBER() OVER (PARTITION BY cph.item,
                                                                                         cph.comp_store
                                                                                ORDER BY cph.rec_date desc,
                                                                                         cph.comp_retail asc) rank
                                                    from comp_price_hist cph,
                                                         rpm_strategy_competitive rsc,
                                                         rpm_me_item_gtt gtt
                                                   where rsc.strategy_id      = I_strategy_id
                                                     and cph.comp_store       = rsc.comp_store
                                                     and cph.item             = gtt.item
                                                     and cph.comp_retail_type = 'R') cph1
                                           where rank = 1
                                             and cph1.item = up.item)
       where EXISTS (select 'x'
                       from comp_price_hist cph,
                            rpm_strategy_competitive rsc
                      where rsc.strategy_id      = I_strategy_id
                        and cph.comp_store       = rsc.comp_store
                        and cph.item             = up.item
                        and cph.comp_retail_type = 'R');
   end if;

   update rpm_me_item_gtt up
      set (up.a_comp_retail,
           up.a_comp_store ,
           up.b_comp_retail,
           up.b_comp_store,
           up.c_comp_retail,
           up.c_comp_store,
           up.d_comp_retail,
           up.d_comp_store,
           up.e_comp_retail,
           up.e_comp_store) = (select NVL(a_comp_retail, up.a_comp_retail),
                                      NVL(a_comp_store, up.a_comp_store),
                                      NVL(b_comp_retail, up.b_comp_retail),
                                      NVL(b_comp_store, up.b_comp_store),
                                      NVL(c_comp_retail, up.c_comp_retail),
                                      NVL(c_comp_store, up.c_comp_store),
                                      NVL(d_comp_retail, up.d_comp_retail),
                                      NVL(d_comp_store, up.d_comp_store),
                                      NVL(e_comp_retail, up.e_comp_retail),
                                      NVL(e_comp_store, up.e_comp_store)
                                 from ((select item,
                                               MAX(DECODE(comp_index, 0, comp_retail, NULL)) a_comp_retail,
                                               MAX(DECODE(comp_index, 0, comp_store,  NULL)) a_comp_store,
                                               MAX(DECODE(comp_index, 1, comp_retail, NULL)) b_comp_retail,
                                               MAX(DECODE(comp_index, 1, comp_store,  NULL)) b_comp_store,
                                               MAX(DECODE(comp_index, 2, comp_retail, NULL)) c_comp_retail,
                                               MAX(DECODE(comp_index, 2, comp_store,  NULL)) c_comp_store,
                                               MAX(DECODE(comp_index, 3, comp_retail, NULL)) d_comp_retail,
                                               MAX(DECODE(comp_index, 3, comp_store,  NULL)) d_comp_store,
                                               MAX(DECODE(comp_index, 4, comp_retail, NULL)) e_comp_retail,
                                               MAX(DECODE(comp_index, 4, comp_store,  NULL)) e_comp_store
                                          from (select distinct cph.item,
                                                       cph.comp_store,
                                                       rsfc.comp_index,
                                                       FIRST_VALUE(cph.comp_retail) OVER (PARTITION BY cph.item,
                                                                                                       cph.comp_store,
                                                                                                       rsfc.comp_index
                                                                                              ORDER BY cph.rec_date desc,
                                                                                                       cph.comp_retail asc) as comp_retail
                                                  from comp_price_hist cph,
                                                       rpm_strategy_ref_comp rsfc,
                                                       rpm_me_item_gtt gtt
                                                 where rsfc.strategy_id     = I_strategy_id
                                                   and cph.comp_store       = rsfc.comp_store
                                                   and cph.item             = gtt.item
                                                   and cph.comp_retail_type = 'R')
                                         group by item) comp)
                                where up.item = comp.item)
    where EXISTS (select 'x'
                    from comp_price_hist cph,
                         rpm_strategy_ref_comp rsfc
                   where rsfc.strategy_id     = I_strategy_id
                     and cph.comp_store       = rsfc.comp_store
                     and cph.item             = up.item
                     and cph.comp_retail_type = 'R');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_COMP_RETAIL;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_COMP_RETAIL_AREA_DIFF(O_error_msg  IN OUT  VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RPM_EXT_SQL.POPULATE_COMP_RETAIL_AREA_DIFF';

BEGIN
   /* This function is only called by the batch program.  It assumes that only one zone
      will be in the rpm_me_item_gtt table at a time */

   update rpm_me_item_gtt up
     set (up.primary_comp_store,
          up.primary_comp_retail,
          up.primary_comp_retail_uom,
          up.primary_comp_retail_std,
          up.primary_multi_units,
          up.primary_multi_unit_retail,
          up.primary_multi_unit_retail_uom,
          up.primary_comp_boolean) = (select comp_store,
                                             comp_retail,
                                             standard_uom,
                                             comp_retail_std,
                                             multi_units,
                                             multi_unit_retail,
                                             multi_unit_retail_uom,
                                             rpm_pull
                                        from (select cph.comp_store comp_store,
                                                     cph.comp_retail comp_retail,
                                                     gtt.standard_uom standard_uom,
                                                     cph.comp_retail comp_retail_std,
                                                     cph.multi_units multi_units,
                                                     cph.multi_unit_retail multi_unit_retail,
                                                     gtt.standard_uom multi_unit_retail_uom,
                                                     cph.item item,
                                                     DECODE(cph.rpm_pull,
                                                            'R', 0,
                                                            1) rpm_pull,
                                                     gtt.zone_id,
                                                     ROW_NUMBER() OVER (PARTITION BY cph.item,
                                                                                     cph.comp_store
                                                                            ORDER BY cph.rec_date desc,
                                                                                     cph.comp_retail asc) rank
                                                 from comp_price_hist cph,
                                                      rpm_area_diff rad,
                                                      rpm_me_item_gtt gtt
                                                where rad.area_diff_id     = LP_area_diff_id
                                                  and cph.comp_store       = rad.comp_store
                                                  and cph.item             = gtt.item
                                                  and cph.comp_retail_type = 'R') cph1
                                       where rank = 1
                                         and cph1.zone_id = up.zone_id
                                         and cph1.item    = up.item)
    where EXISTS (select 'x'
                    from comp_price_hist cph,
                         rpm_area_diff rad
                   where rad.area_diff_id     = LP_area_diff_id
                     and cph.comp_store       = rad.comp_store
                     and cph.item             = up.item
                     and cph.comp_retail_type = 'R');

   update rpm_me_item_gtt up
     set (up.a_comp_retail,
          up.a_comp_store ,
          up.b_comp_retail,
          up.b_comp_store,
          up.c_comp_retail,
          up.c_comp_store,
          up.d_comp_retail,
          up.d_comp_store,
          up.e_comp_retail,
          up.e_comp_store) = (select NVL(a_comp_retail, up.a_comp_retail),
                                     NVL(a_comp_store, up.a_comp_store),
                                     NVL(b_comp_retail, up.b_comp_retail),
                                     NVL(b_comp_store, up.b_comp_store),
                                     NVL(c_comp_retail, up.c_comp_retail),
                                     NVL(c_comp_store, up.c_comp_store),
                                     NVL(d_comp_retail, up.d_comp_retail),
                                     NVL(d_comp_store, up.d_comp_store),
                                     NVL(e_comp_retail, up.e_comp_retail),
                                     NVL(e_comp_store, up.e_comp_store)
                                from ((select item,
                                              zone_id,
                                              MAX(DECODE(comp_index, 0, comp_retail, NULL)) a_comp_retail,
                                              MAX(DECODE(comp_index, 0, comp_store,  NULL)) a_comp_store,
                                              MAX(DECODE(comp_index, 1, comp_retail, NULL)) b_comp_retail,
                                              MAX(DECODE(comp_index, 1, comp_store,  NULL)) b_comp_store,
                                              MAX(DECODE(comp_index, 2, comp_retail, NULL)) c_comp_retail,
                                              MAX(DECODE(comp_index, 2, comp_store,  NULL)) c_comp_store,
                                              MAX(DECODE(comp_index, 3, comp_retail, NULL)) d_comp_retail,
                                              MAX(DECODE(comp_index, 3, comp_store,  NULL)) d_comp_store,
                                              MAX(DECODE(comp_index, 4, comp_retail, NULL)) e_comp_retail,
                                              MAX(DECODE(comp_index, 4, comp_store,  NULL)) e_comp_store
                                         from (select distinct cph.item,
                                                      gtt.zone_id,
                                                      cph.comp_store,
                                                      radrc.comp_index,
                                                      FIRST_VALUE(cph.comp_retail) OVER (PARTITION BY cph.item,
                                                                                                      cph.comp_store,
                                                                                                      radrc.comp_index
                                                                                             ORDER BY cph.rec_date desc,
                                                                                                      cph.comp_retail asc) as comp_retail
                                                 from comp_price_hist cph,
                                                      rpm_area_diff_ref_comp radrc,
                                                      rpm_me_item_gtt gtt
                                                where radrc.area_diff_id   = LP_area_diff_id
                                                  and cph.comp_store       = radrc.comp_store
                                                  and cph.item             = gtt.item
                                                  and cph.comp_retail_type = 'R')
                                         group by item, zone_id) comp )
                                        where up.item       = comp.item)
    where EXISTS (select 'x'
                    from comp_price_hist cph,
                         rpm_area_diff_ref_comp radrc
                   where radrc.area_diff_id   = LP_area_diff_id
                     and cph.comp_store       = radrc.comp_store
                     and cph.item             = up.item
                     and cph.comp_retail_type = 'R');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_COMP_RETAIL_AREA_DIFF;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_REF_WH_STOCK(O_error_msg     IN OUT  VARCHAR2,
                               I_strategy_id   IN      RPM_STRATEGY.STRATEGY_ID%TYPE,
                               I_initial_call  IN      VARCHAR2)
   RETURN BOOLEAN IS

BEGIN


   if I_initial_call = 'Y' then

      update rpm_me_item_gtt up
        set (up.ref_wh_stock,
             up.ref_wh_inventory) =
           (select sum(nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0)) stock,
                   sum(nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) +
                       nvl(ils.in_transit_qty, 0) + nvl(ils.pack_comp_intran, 0)) inventory
              from item_loc_soh ils,
                   rpm_strategy_wh rsw
             where rsw.strategy_id = I_strategy_id
               and ils.item        = up.item
               and ils.loc         = rsw.wh
             group by ils.item)
       where exists (select 'x'
                       from item_loc_soh ils,
                            rpm_strategy_wh rsw
                      where rsw.strategy_id = I_strategy_id
                        and ils.item        = up.item
                        and ils.loc         = rsw.wh);

   else

      update rpm_me_item_gtt up
        set (up.ref_wh_stock,
             up.ref_wh_inventory) =
           (select sum(nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0)) stock,
                   sum(nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) +
                       nvl(ils.in_transit_qty, 0) + nvl(ils.pack_comp_intran, 0)) inventory
              from item_loc_soh ils,
                   rpm_area_diff_wh rad
             where rad.area_diff_id = 1
               and ils.item         = up.item
               and ils.loc          = rad.wh
             group by ils.item)
       where exists (select 'x'
                       from item_loc_soh ils,
                            rpm_area_diff_wh rad
                      where rad.area_diff_id = 1
                        and ils.item        = up.item
                        and ils.loc         = rad.wh);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_REF_WH_STOCK',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_REF_WH_STOCK;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_REF_WH_ON_ORDER(O_error_msg     IN OUT  VARCHAR2,
                                  I_strategy_id   IN      RPM_STRATEGY.STRATEGY_ID%TYPE,
                                  I_initial_call  IN      VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   if I_initial_call = 'Y' then

      update rpm_me_item_gtt up
        set (up.ref_wh_on_order,
             up.ref_wh_inventory) =
           (select sum(use.on_order_qty),
                   nvl(up.ref_wh_inventory,0) + sum(use.on_order_qty)
              from (select ol.item,
                           sum(nvl(ol.qty_ordered, 0) - nvl(ol.qty_received, 0)) on_order_qty
                      from ordhead oh,
                           ordloc ol,
                           rpm_strategy_wh rsw,
                           rpm_me_item_gtt g1
                     where ol.item                = g1.item
                       and ol.order_no            = oh.order_no
                       and ol.location            = rsw.wh
                       and oh.status              = 'A'
                       and rsw.strategy_id        = I_strategy_id
                       and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
                     group by ol.item
                    union all
                    select ol.item,
                           sum(vpq.qty * (nvl(ol.qty_ordered, 0) - nvl(ol.qty_received, 0))) on_order_qty
                      from ordhead oh,
                           ordloc ol,
                           v_packsku_qty vpq,
                           rpm_strategy_wh rsw,
                           rpm_me_item_gtt g2
                     where vpq.item               = g2.item
                       and ol.item                = vpq.pack_no
                       and ol.order_no            = oh.order_no
                       and ol.location            = rsw.wh
                       and oh.status              = 'A'
                       and rsw.strategy_id        = I_strategy_id
                       and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
                     group by ol.item) use
             where use.item = up.item
           group by use.item)
       where exists (select 'x'
                       from ordhead oh,
                            ordloc ol,
                            rpm_strategy_wh rsw
                      where ol.item                = up.item
                        and ol.order_no            = oh.order_no
                        and ol.location            = rsw.wh
                        and oh.status              = 'A'
                        and rsw.strategy_id        = I_strategy_id
                        and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
                     union
                     select 'x'
                       from ordhead oh,
                            ordloc ol,
                            v_packsku_qty vpq,
                            rpm_strategy_wh rsw
                      where vpq.item               = up.item
                        and ol.item                = vpq.pack_no
                        and ol.order_no            = oh.order_no
                        and ol.location            = rsw.wh
                        and oh.status              = 'A'
                        and rsw.strategy_id        = I_strategy_id
                        and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0));

   else

      update rpm_me_item_gtt up
        set (up.ref_wh_on_order,
             up.ref_wh_inventory) =
           (select sum(use.on_order_qty),
                   nvl(up.ref_wh_inventory,0) + sum(use.on_order_qty)
              from (select ol.item,
                           sum(nvl(ol.qty_ordered, 0) - nvl(ol.qty_received, 0)) on_order_qty
                      from ordhead oh,
                           ordloc ol,
                           rpm_area_diff_wh rad,
                           rpm_me_item_gtt g1
                     where ol.item                = g1.item
                       and ol.order_no            = oh.order_no
                       and ol.location            = rad.wh
                       and oh.status              = 'A'
                       and rad.area_diff_id       = LP_area_diff_id
                       and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
                     group by ol.item
                    union all
                    select ol.item,
                           sum(vpq.qty * (nvl(ol.qty_ordered, 0) - nvl(ol.qty_received, 0))) on_order_qty
                      from ordhead oh,
                           ordloc ol,
                           v_packsku_qty vpq,
                           rpm_area_diff_wh rad,
                           rpm_me_item_gtt g2
                     where vpq.item               = g2.item
                       and ol.item                = vpq.pack_no
                       and ol.order_no            = oh.order_no
                       and ol.location            = rad.wh
                       and oh.status              = 'A'
                       and rad.area_diff_id       = LP_area_diff_id
                       and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
                     group by ol.item) use
             where use.item = up.item
           group by use.item)
       where exists (select 'x'
                       from ordhead oh,
                            ordloc ol,
                            rpm_area_diff_wh rad
                      where ol.item                = up.item
                        and ol.order_no            = oh.order_no
                        and ol.location            = rad.wh
                        and oh.status              = 'A'
                        and rad.area_diff_id       = LP_area_diff_id
                        and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
                     union
                     select 'x'
                       from ordhead oh,
                            ordloc ol,
                            v_packsku_qty vpq,
                            rpm_area_diff_wh rad
                      where vpq.item               = up.item
                        and ol.item                = vpq.pack_no
                        and ol.order_no            = oh.order_no
                        and ol.location            = rad.wh
                        and oh.status              = 'A'
                        and rad.area_diff_id       = LP_area_diff_id
                        and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0));

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_REF_WH_ON_ORDER',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_REF_WH_ON_ORDER;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_ITEM_LOC(O_error_msg           IN OUT VARCHAR2,
                           I_strategy_id         IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                           I_me_sequence_id      IN     RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                           I_me_area_diff_seq_id IN     RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE,
                           I_zone_id             IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                           I_eff_date            IN     DATE,
                           I_initial_call        IN     VARCHAR2)
   RETURN BOOLEAN IS

BEGIN
   if LP_review_not_started then
      merge into rpm_me_item_loc_gtt il
      using ( select /*+ leading(rti) USE_HASH(ils) */
                     rti.worksheet_item_data_id,
                     I_strategy_id proposed_strategy_id,
                     ils.item,
                     ils.loc,
                     decode(ils.loc_type,'S',0,'W',2) loc_type,
                     nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) soh,
                     nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) +
                     nvl(ils.in_transit_qty, 0) + nvl(ils.pack_comp_intran, 0) inv,
                     round((LP_tomorrow - ils.first_sold)/7) as sale_weeks,
                     round((LP_tomorrow - ils.first_received)/7) as receive_weeks,
                     ils.first_received,
                     ils.last_received,
                     ils.primary_supp,
                     ils.primary_cntry,
                     nvl(ils.currency_code, rti.currency) currency_code,
                     I_eff_date effective_date,
                     I_eff_date orig_effective_date,
                     rti.zone_id,
                     ils.item_parent,
                     ils.diff_id,
                     LP_crp_start crp_start,
                     LP_crp_end crp_end
                from rpm_me_item_gtt rti,
                     (select *
                        from rpm_pre_me_item_loc
                       where me_sequence_id = I_me_sequence_id
                         and me_area_diff_seq_id = I_me_area_diff_seq_id) ils
               where ils.item         = rti.item) use
      on (    il.item     = use.item
          and il.location = use.loc)
      when matched then
      update
         set location_stock = use.soh,
             location_inventory = use.inv,
             wks_first_sale = use.sale_weeks,
             wks_of_sales_exp = use.receive_weeks,
             first_received_date = use.first_received,
             last_received_date = use.last_received,
             il_primary_supp = use.primary_supp,
             il_primary_cntry = use.primary_cntry,
             effective_date = use.effective_date,
             original_effective_date = use.orig_effective_date
      when not matched then
      insert(worksheet_item_loc_data_id,
             worksheet_item_data_id,
             proposed_strategy_id,
             item,
             location,
             loc_type,
             location_stock,
             location_inventory,
             wks_first_sale,
             wks_of_sales_exp,
             first_received_date,
             last_received_date,
             il_primary_supp,
             il_primary_cntry,
             supp_currency,
             effective_date,
             original_effective_date,
             action_flag,
             zone_id,
             item_parent,
             diff_id,
             crp_start,
             crp_end)
       values(rpm_worksheet_il_data_seq.nextval,
              use.worksheet_item_data_id,
              use.proposed_strategy_id,
              use.item,
              use.loc,
              use.loc_type,
              use.soh,
              use.inv,
              use.sale_weeks,
              use.receive_weeks,
              use.first_received,
              use.last_received,
              use.primary_supp,
              use.primary_cntry,
              use.currency_code,
              use.effective_date,
              use.orig_effective_date,
              UNDECIDED,
              use.zone_id,
              use.item_parent,
              use.diff_id,
              use.crp_start,
              use.crp_end);
   else
      merge into rpm_me_item_loc_gtt il
      using ( select /*+ leading(rti) USE_HASH(ils) */
                     I_strategy_id proposed_strategy_id,
                     ils.item,
                     ils.loc,
                     decode(ils.loc_type,'S',0,'W',2) loc_type,
                     nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) soh,
                     nvl(ils.stock_on_hand, 0) + nvl(ils.pack_comp_soh, 0) +
                     nvl(ils.in_transit_qty, 0) + nvl(ils.pack_comp_intran, 0) inv,
                     round((LP_tomorrow - ils.first_sold)/7) as sale_weeks,
                     round((LP_tomorrow - ils.first_received)/7) as receive_weeks,
                     ils.first_received,
                     ils.last_received,
                     ils.primary_supp,
                     ils.primary_cntry,
                     nvl(ils.currency_code, rti.currency) currency_code,
                     I_eff_date effective_date,
                     I_eff_date orig_effective_date,
                     rti.zone_id,
                     ils.item_parent,
                     ils.diff_id,
                     LP_crp_start crp_start,
                     LP_crp_end crp_end
                from rpm_me_item_gtt rti,
                     (select *
                        from rpm_pre_me_item_loc
                       where me_sequence_id = I_me_sequence_id
                         and me_area_diff_seq_id = I_me_area_diff_seq_id) ils
               where ils.item         = rti.item) use
      on (    il.item     = use.item
          and il.location = use.loc)
      when matched then
      update
         set location_stock = use.soh,
             location_inventory = use.inv,
             wks_first_sale = use.sale_weeks,
             wks_of_sales_exp = use.receive_weeks,
             first_received_date = use.first_received,
             last_received_date = use.last_received,
             il_primary_supp = use.primary_supp,
             il_primary_cntry = use.primary_cntry,
             effective_date = use.effective_date,
             original_effective_date = use.orig_effective_date;
   end if;

   if REMOVE_AREA_DIFF_EXCLUDES(O_error_msg,
                                I_initial_call) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_ITEM_LOC',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_ITEM_LOC;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_IL_STAT_RPM_IND(O_error_msg     IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   insert into rpm_me_il_status_rpm_ind_gtt
     (item,
      location,
      status,
      rpm_ind)
   select il.item,
          il.loc,
          il.status,
          il.rpm_ind
     from item_loc il,
          rpm_me_item_loc_gtt gtt
    where il.item   = gtt.item
      and il.loc    = gtt.location;

   delete from rpm_me_item_loc_gtt
    where (item, location) in(
             select s.item,
                    s.location
               from rpm_me_il_status_rpm_ind_gtt s
              where s.status   = 'D');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_IL_STAT_RPM_IND',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_IL_STAT_RPM_IND;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_IL_STAT_RPM_IND_BATCH(O_error_msg     IN OUT  VARCHAR2,
                                        I_me_sequence_id      IN rpm_pre_me_aggregation.me_sequence_id%TYPE,
                                        I_me_area_diff_seq_id IN rpm_pre_me_aggregation.me_area_diff_seq_id%TYPE)
   RETURN BOOLEAN IS

BEGIN

   insert into rpm_me_il_status_rpm_ind_gtt
     (item,
      location,
      status,
      rpm_ind)
   select /*+ leading(gtt) use_hash(il) */ il.item,
          il.loc,
          il.status,
          il.rpm_ind
     from (select *
             from rpm_pre_me_item_loc
            where me_sequence_id = I_me_sequence_id
              and me_area_diff_seq_id = I_me_area_diff_seq_id) il,
          rpm_me_item_loc_gtt gtt
    where il.item   = gtt.item
      and il.loc    = gtt.location;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_IL_STAT_RPM_IND_BATCH',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_IL_STAT_RPM_IND_BATCH;
----------------------------------------------------------------------------------------
FUNCTION REMOVE_AREA_DIFF_EXCLUDES(O_error_msg     IN OUT  VARCHAR2,
                                   I_initial_call  IN      VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   -- Delete items/location combinations that have been excluded from a
   -- Secondary Area Differential
   if I_initial_call = 'N' then
      if LP_record_item_loc_removes then
         insert into rpm_merch_extract_deletions
                  (strategy_id,
                   delete_date,
                   item,
                   location,
                   reason_code )
            select LP_record_item_loc_strategy,
                   LP_vdate,
                   i.item,
                   i.location,
                   AREA_DIFF_EXCLUSION
              from rpm_area_diff_exclude r,
                   rpm_me_item_loc_gtt i
             where r.item_id      = i.item
               and r.area_diff_id = LP_area_diff_id;
      end if;

      delete from rpm_me_item_loc_gtt
       where (item, location) in(
                select i.item,
                       i.location
                  from rpm_area_diff_exclude r,
                       rpm_me_item_loc_gtt i
                 where r.item_id      = i.item
                   and r.area_diff_id = LP_area_diff_id);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.REMOVE_AREA_DIFF_EXCLUDES',
                                        to_char(SQLCODE));
      return FALSE;

END REMOVE_AREA_DIFF_EXCLUDES;

/*----------------------------------------------------------------------------------------

maint margin cost settings:
---------------------------

CURRENT-

   maint_margin_cost = first nvl(pricing_cost, base_cost) < crp start

   current_pricing_cost = first pricing cost <= curr date
   current_cost = first nvl(pricing_cost, case cost) <= curr date
   past_cost_chg_date = date of cost used for current_cost
   current_base_cost = first base cost <= curr date

BASIS-

   basis_pricing_cost = first pricing cost > curr date & <= crp end
   basis_base_cost = first base cost > curr date & <= crp end


PENDING-

   pend_cost_boolean - true when an event exist > curr date & <= cursor date
   pend_cost_chg_cost - first nvl(pricing_cost, base_cost) > curr date & <= cursor date
   pend_cost_chg_date - date of cost used for pend_cost_chg_cost

OTHERS-

   effective date = first event <= crp end
   original_effective_date = first event <= crp end
   cost alert = true if an event exists > curr date & <= cursor date


non maint margin cost settings:
-------------------------------

CURRENT-

   current_pricing_cost = first pricing cost <= curr date
   current_cost = first nvl(pricing_cost, case cost) <= curr date
   past_cost_chg_date = date of cost used for current_cost
   current_base_cost = first base cost <= curr date

BASIS-

   basis_pricing_cost = first pricing cost > curr date & <= effective date
   basis_base_cost = first base cost > curr date & <= effective date


PENDING-

   pend_cost_boolean - true when an event exist > curr date & <= cursor date
   pend_cost_chg_cost - first nvl(pricing_cost, base_cost) > curr date & <= cursor date
   pend_cost_chg_date - date of cost used for pend_cost_chg_cost

OTHERS-

   cost alert = true if an event exists > curr date & <= effective date

----------------------------------------------------------------------------------------*/
FUNCTION POPULATE_ITEM_COST(O_error_msg  IN OUT  VARCHAR2,
                            I_curr_date  IN      DATE,
                            I_eff_date   IN      DATE,
                            I_override   IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.POPULATE_ITEM_COST';

BEGIN

   if LP_maint_margin_ind = 0 then

      insert into rpm_me_il_cost_gtt
         (item,
          location,
          link_code,
          zone_id,
          item_parent,
          diff_id,
          crp_start,
          crp_end,
          current_cost,
          current_base_cost,
          current_pricing_cost,
          basis_base_cost,
          basis_pricing_cost,
          pend_cost_chg_date,
          pend_cost_chg_cost,
          pend_cost_boolean,
          cost_alert,
          past_cost_chg_date,
          effective_date,
          original_effective_date,
          maint_margin_cost)
      select /*+ USE_HASH(curr basis pending) */ curr.item,
             curr.location,
             curr.link_code,
             curr.zone_id,
             curr.item_parent,
             curr.diff_id,
             curr.crp_start,
             curr.crp_end,
             NVL(curr.current_pricing_cost, curr.current_base_cost),
             curr.current_base_cost,
             curr.current_pricing_cost,
             NVL(basis.basis_base_cost, curr.current_base_cost),
             NVL(NVL(basis.basis_pricing_cost, basis.basis_base_cost), curr.current_pricing_cost),
             pending.pending_cost_date,
             NVL(pending.pending_pricing_cost, pending.pending_base_cost),
             DECODE(pending.pending_cost_date,
                    NULL, 0,
                    1),
             DECODE(basis.basis_cost_date,
                    NULL, 0,
                    1),
             curr.current_cost_date,
             I_eff_date,
             I_eff_date,
             NULL
        from (select item,
                     location,
                     link_code,
                     zone_id,
                     item_parent,
                     diff_id,
                     crp_start,
                     crp_end,
                     current_base_cost,
                     current_pricing_cost,
                     current_cost_date,
                     type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             il.link_code,
                             il.zone_id,
                             il.item_parent,
                             il.diff_id,
                             il.crp_start,
                             il.crp_end,
                             f.base_cost current_base_cost,
                             f.pricing_cost current_pricing_cost,
                             f.active_date current_cost_date,
                             'CURRENT' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.location(+)            = il.location
                         and f.supplier(+)            = il.il_primary_supp
                         and f.origin_country_id(+)   = il.il_primary_cntry
                         and f.active_date(+)        <= I_curr_date) curr_in
                 where rank = 1) curr,
             (select item,
                     location,
                     pending_base_cost,
                     pending_pricing_cost,
                     pending_cost_date,
                     'PENDING' type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             f.base_cost pending_base_cost,
                             f.pricing_cost pending_pricing_cost,
                             f.active_date pending_cost_date,
                             'PENDING' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date asc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.location(+)            = il.location
                         and f.supplier(+)            = il.il_primary_supp
                         and f.origin_country_id(+)   = il.il_primary_cntry
                         and f.active_date(+)         > I_curr_date
                         and f.active_date(+)        <= GREATEST(I_curr_date + NVL(LP_pend_cost_chg_window, 0),
                                                                 I_eff_date)) pending_in
               where rank = 1) pending,
             (select item,
                     location,
                     basis_base_cost,
                     basis_pricing_cost,
                     basis_cost_date,
                     'BASIS' type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             f.base_cost basis_base_cost,
                             f.pricing_cost basis_pricing_cost,
                             f.active_date basis_cost_date,
                             'BASIS' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.location(+)            = il.location
                         and f.supplier(+)            = il.il_primary_supp
                         and f.origin_country_id(+)   = il.il_primary_cntry
                         and f.active_date(+)        <= I_eff_date) basis_in
               where rank = 1) basis
      where curr.item      = pending.item
        and curr.location  = pending.location
        and curr.item      = basis.item
        and curr.location  = basis.location;

   else

      insert into rpm_me_il_cost_gtt
         (item,
          location,
          link_code,
          zone_id,
          item_parent,
          diff_id,
          crp_start,
          crp_end,
          current_cost,
          current_base_cost,
          current_pricing_cost,
          basis_base_cost,
          basis_pricing_cost,
          pend_cost_chg_date,
          pend_cost_chg_cost,
          pend_cost_boolean,
          cost_alert,
          past_cost_chg_date,
          effective_date,
          original_effective_date,
          maint_margin_cost)
      select /*+ USE_HASH(curr crp_end crp_start pending) */ curr.item,
             curr.location,
             curr.link_code,
             curr.zone_id,
             curr.item_parent,
             curr.diff_id,
             curr.crp_start,
             curr.crp_end,
             NVL(curr.current_pricing_cost, curr.current_base_cost),
             curr.current_base_cost,
             curr.current_pricing_cost,
             NVL(crp_end.crp_end_base_cost, curr.current_base_cost),
             NVL(crp_end.crp_end_pricing_cost, curr.current_pricing_cost),
             pending.pending_cost_date,
             NVL(pending.pending_pricing_cost, pending.pending_base_cost),
             DECODE(pending.pending_cost_date,
                    NULL, 0,
                    1),
             DECODE(crp_end.crp_end_cost_date,
                    NULL, 0,
                    1),
             curr.current_cost_date,
             DECODE(I_override,
                    'N', NVL(crp_end.crp_end_cost_date, I_eff_date),
                    I_eff_date),
             DECODE(I_override,
                    'N', NVL(crp_end.crp_end_cost_date, I_eff_date),
                    I_eff_date),
             NVL(crp_start.crp_start_pricing_cost, crp_start.crp_start_base_cost)
        from (select item,
                     location,
                     link_code,
                     zone_id,
                     item_parent,
                     diff_id,
                     crp_start,
                     crp_end,
                     current_base_cost,
                     current_pricing_cost,
                     current_cost_date,
                     'CURRENT' type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             il.link_code,
                             il.zone_id,
                             il.item_parent,
                             il.diff_id,
                             il.crp_start,
                             il.crp_end,
                             f.base_cost current_base_cost,
                             f.pricing_cost current_pricing_cost,
                             f.active_date current_cost_date,
                             'CURRENT' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.location(+)            = il.location
                         and f.supplier(+)            = il.il_primary_supp
                         and f.origin_country_id(+)   = il.il_primary_cntry
                         and f.active_date(+)        <= I_curr_date) curr_in
               where rank = 1) curr,
             (select item,
                     location,
                     pending_base_cost,
                     pending_pricing_cost,
                     pending_cost_date,
                     'PENDING' type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             f.base_cost pending_base_cost,
                             f.pricing_cost pending_pricing_cost,
                             f.active_date pending_cost_date,
                             'PENDING' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date asc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.location(+)            = il.location
                         and f.supplier(+)            = il.il_primary_supp
                         and f.origin_country_id(+)   = il.il_primary_cntry
                         and f.active_date(+)         > I_curr_date
                         and f.active_date(+)        <= DECODE(I_override,
                                                               'N', GREATEST(I_curr_date + NVL(LP_pend_cost_chg_window, 0),
                                                                             LP_crp_end),
                                                               I_eff_date)) pending_in
               where rank = 1) pending,
             (select item,
                     location,
                     crp_start_base_cost,
                     crp_start_pricing_cost,
                     crp_start_cost_date,
                     'CRP_START' type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             f.base_cost crp_start_base_cost,
                             f.pricing_cost crp_start_pricing_cost,
                             f.active_date crp_start_cost_date,
                             'CRP_START' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date asc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)               = il.item
                         and f.location(+)           = il.location
                         and f.supplier(+)           = il.il_primary_supp
                         and f.origin_country_id(+)  = il.il_primary_cntry
                         and f.active_date(+)        < LP_crp_start) crp_start_in
               where rank = 1) crp_start,
             (select item,
                     location,
                     crp_end_base_cost,
                     crp_end_pricing_cost,
                     crp_end_cost_date,
                     'CRP_END' type
                from (select /*+ USE_NL(IL, F) */ il.item,
                             il.location,
                             f.base_cost crp_end_base_cost,
                             f.pricing_cost crp_end_pricing_cost,
                             f.active_date crp_end_cost_date,
                             'CRP_END' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date asc) rank
                        from future_cost f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)               = il.item
                         and f.location(+)           = il.location
                         and f.supplier(+)           = il.il_primary_supp
                         and f.origin_country_id(+)  = il.il_primary_cntry
                         and f.active_date(+)        > I_curr_date
                         and f.active_date(+)       <= DECODE(I_override,
                                                              'N', LP_crp_end,
                                                              I_eff_date)) crp_end_in
               where rank = 1) crp_end
      where curr.item      = pending.item
        and curr.location  = pending.location
        and curr.item      = crp_start.item
        and curr.location  = crp_start.location
        and curr.item      = crp_end.item
        and curr.location  = crp_end.location;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_ITEM_COST;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_ITEM_COST_BATCH(O_error_msg           IN OUT  VARCHAR2,
                                  I_me_sequence_id      IN      RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                                  I_me_area_diff_seq_id IN      RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE,
                                  I_curr_date           IN      DATE,
                                  I_eff_date            IN      DATE,
                                  I_override            IN      VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.POPULATE_ITEM_COST_BATCH';

BEGIN

   if LP_maint_margin_ind = 0 then

      insert into rpm_me_il_cost_gtt
         (item,
          location,
          zone_id,
          item_parent,
          diff_id,
          crp_start,
          crp_end,
          link_code,
          current_cost,
          current_base_cost,
          current_pricing_cost,
          basis_base_cost,
          basis_pricing_cost,
          pend_cost_chg_date,
          pend_cost_chg_cost,
          pend_cost_boolean,
          cost_alert,
          past_cost_chg_date,
          effective_date,
          original_effective_date,
          maint_margin_cost)
      select /*+ ordered */ curr.item,
             curr.location,
             curr.zone_id,
             curr.item_parent,
             curr.diff_id,
             curr.crp_start,
             curr.crp_end,
             curr.link_code,
             NVL(curr.current_pricing_cost, curr.current_base_cost),
             curr.current_base_cost,
             curr.current_pricing_cost,
             NVL(basis.basis_base_cost, curr.current_base_cost),
             NVL(NVL(basis.basis_pricing_cost, basis.basis_base_cost), curr.current_pricing_cost),
             pending.pending_cost_date,
             NVL(pending.pending_pricing_cost, pending.pending_base_cost),
             DECODE(pending.pending_cost_date,
                    NULL, 0,
                    1),
             DECODE(basis.basis_cost_date,
                    NULL, 0,
                    1),
             curr.current_cost_date,
             I_eff_date,
             I_eff_date,
             NULL
        from (select item,
                     location,
                     zone_id,
                     item_parent,
                     diff_id,
                     crp_start,
                     crp_end,
                     link_code,
                     current_base_cost,
                     current_pricing_cost,
                     current_cost_date,
                     'CURRENT' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             il.zone_id,
                             il.item_parent,
                             il.diff_id,
                             il.crp_start,
                             il.crp_end,
                             il.link_code,
                             f.base_cost current_base_cost,
                             f.pricing_cost current_pricing_cost,
                             f.active_date current_cost_date,
                             'CURRENT' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id      = I_me_sequence_id
                                 and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.loc(+)                 = il.location
                         and f.active_date(+)        <= I_curr_date) curr_in
               where rank = 1) curr,
             (select item,
                     location,
                     pending_base_cost,
                     pending_pricing_cost,
                     pending_cost_date,
                     'PENDING' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             f.base_cost pending_base_cost,
                             f.pricing_cost pending_pricing_cost,
                             f.active_date pending_cost_date,
                             'PENDING' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id      = I_me_sequence_id
                                 and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.loc(+)                 = il.location
                         and f.active_date(+)         > I_curr_date
                         and f.active_date(+)        <= GREATEST(I_curr_date + NVL(LP_pend_cost_chg_window, 0),
                                                                 I_eff_date)) pending_in
               where rank = 1) pending,
             (select item,
                     location,
                     basis_base_cost,
                     basis_pricing_cost,
                     basis_cost_date,
                     'BASIS' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             f.base_cost basis_base_cost,
                             f.pricing_cost basis_pricing_cost,
                             f.active_date basis_cost_date,
                             'BASIS' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id      = I_me_sequence_id
                                 and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.loc(+)                 = il.location
                         and f.active_date(+)        <= I_eff_date) basis_in
               where rank = 1) basis
      where curr.item      = pending.item
        and curr.location  = pending.location
        and curr.item      = basis.item
        and curr.location  = basis.location;

   else

      insert into rpm_me_il_cost_gtt
         (item,
          location,
          zone_id,
          item_parent,
          diff_id,
          crp_start,
          crp_end,
          link_code,
          current_cost,
          current_base_cost,
          current_pricing_cost,
          basis_base_cost,
          basis_pricing_cost,
          pend_cost_chg_date,
          pend_cost_chg_cost,
          pend_cost_boolean,
          cost_alert,
          past_cost_chg_date,
          effective_date,
          original_effective_date,
          maint_margin_cost)
      select /*+ ordered */ curr.item,
             curr.location,
             curr.zone_id,
             curr.item_parent,
             curr.diff_id,
             curr.crp_start,
             curr.crp_end,
             curr.link_code,
             NVL(curr.current_pricing_cost, curr.current_base_cost),
             curr.current_base_cost,
             curr.current_pricing_cost,
             NVL(crp_end.crp_end_base_cost, curr.current_base_cost),
             NVL(crp_end.crp_end_pricing_cost, curr.current_pricing_cost),
             pending.pending_cost_date,
             NVL(pending.pending_pricing_cost, pending.pending_base_cost),
             DECODE(pending.pending_cost_date,
                    NULL, 0,
                    1),
             DECODE(crp_end.crp_end_cost_date,
                    NULL, 0,
                    1),
             curr.current_cost_date,
             DECODE(I_override,
                    'N', NVL(crp_end.crp_end_cost_date, I_eff_date),
                    I_eff_date),
             DECODE(I_override,
                    'N', NVL(crp_end.crp_end_cost_date, I_eff_date),
                    I_eff_date),
             NVL(crp_start.crp_start_pricing_cost, crp_start.crp_start_base_cost)
        from (select item,
                     location,
                     zone_id,
                     item_parent,
                     diff_id,
                     crp_start,
                     crp_end,
                     link_code,
                     current_base_cost,
                     current_pricing_cost,
                     current_cost_date,
                     'CURRENT' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             il.zone_id,
                             il.item_parent,
                             il.diff_id,
                             il.crp_start,
                             il.crp_end,
                             il.link_code,
                             f.base_cost current_base_cost,
                             f.pricing_cost current_pricing_cost,
                             f.active_date current_cost_date,
                             'CURRENT' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id      = I_me_sequence_id
                                 and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.loc(+)                 = il.location
                         and f.active_date(+)        <= I_curr_date) curr_in
               where rank = 1) curr,
             (select item,
                     location,
                     pending_base_cost,
                     pending_pricing_cost,
                     pending_cost_date,
                     'PENDING' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             f.base_cost pending_base_cost,
                             f.pricing_cost pending_pricing_cost,
                             f.active_date pending_cost_date,
                             'PENDING' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id      = I_me_sequence_id
                                 and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                              rpm_me_item_loc_gtt il
                       where f.item(+)                = il.item
                         and f.loc(+)                 = il.location
                         and f.active_date(+)         > I_curr_date
                         and f.active_date(+)        <= DECODE(I_override,
                                                               'N', GREATEST(I_curr_date + NVL(LP_pend_cost_chg_window, 0),
                                                                             il.crp_end),
                                                               I_eff_date)) pending_in
               where rank = 1) pending,
             (select item,
                     location,
                     crp_start_base_cost,
                     crp_start_pricing_cost,
                     crp_start_cost_date,
                     'CRP_START' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             f.base_cost crp_start_base_cost,
                             f.pricing_cost crp_start_pricing_cost,
                             f.active_date crp_start_cost_date,
                             'CRP_START' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id     = I_me_sequence_id
                                and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                             rpm_me_item_loc_gtt il
                      where f.item(+)               = il.item
                        and f.loc(+)                = il.location
                        and f.active_date(+)        < il.crp_start) crp_start_in
               where rank = 1) crp_start,
             (select item,
                     location,
                     crp_end_base_cost,
                     crp_end_pricing_cost,
                     crp_end_cost_date,
                     'CRP_END' type
                from (select /*+ leading(il) use_hash(f) */ il.item,
                             il.location,
                             f.base_cost crp_end_base_cost,
                             f.pricing_cost crp_end_pricing_cost,
                             f.active_date crp_end_cost_date,
                             'CRP_END' type,
                             ROW_NUMBER() OVER (PARTITION BY il.item,
                                                             il.location
                                                    ORDER BY f.active_date desc) rank
                        from (select base_cost,
                                     pricing_cost,
                                     active_date,
                                     item,
                                     loc
                                from rpm_pre_me_cost
                               where me_sequence_id      = I_me_sequence_id
                                 and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                             rpm_me_item_loc_gtt il
                       where f.item(+)               = il.item
                         and f.loc(+)                = il.location
                         and f.active_date(+)        > I_curr_date
                         and f.active_date(+)       <= DECODE(I_override,
                                                              'N', il.crp_end,
                                                              I_eff_date)) crp_end_in
               where rank = 1) crp_end
      where curr.item      = pending.item
        and curr.location  = pending.location
        and curr.item      = crp_start.item
        and curr.location  = crp_start.location
        and curr.item      = crp_end.item
        and curr.location  = crp_end.location;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_ITEM_COST_BATCH;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_PRICING_INFO(O_error_msg  IN OUT  VARCHAR2,
                               I_curr_date  IN      DATE,
                               I_dept       IN      DEPS.DEPT%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.POPULATE_PRICING_INFO';

BEGIN

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      zone_node_type,
                                      location,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_selling_uom,
                                      price_change_id,
                                      clear_start_ind,
                                      clearance_id,
                                      clear_retail,
                                      clear_uom,
                                      clear_mkdn_index,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind)
    select -999999,
           -999999,
           in2.dept,
           in2.class,
           in2.subclass,
           in2.item,
           in2.zone_node_type,
           in2.location,
           in2.action_date,
           in2.selling_retail,
           in2.selling_retail_currency,
           in2.selling_uom,
           in2.multi_units,
           in2.multi_unit_retail,
           in2.multi_selling_uom,
           in2.price_change_id,
           in2.clear_start_ind,
           in2.clearance_id,
           in2.clear_retail,
           in2.clear_uom,
           in2.clear_mkdn_index,
           in2.on_simple_promo_ind,
           in2.on_complex_promo_ind
      from (select in1.cur_hier_level,
                   in1.cur_hier_level_rank,
                   ---
                   in1.item,
                   in1.dept,
                   in1.class,
                   in1.subclass,
                   loc.zone_node_type,
                   in1.location,
                   in1.action_date,
                   in1.selling_retail,
                   in1.selling_retail_currency,
                   in1.selling_uom,
                   in1.multi_units,
                   in1.multi_unit_retail,
                   in1.multi_selling_uom,
                   in1.price_change_id,
                   in1.clear_start_ind,
                   in1.clearance_id,
                   in1.clear_retail,
                   in1.clear_uom,
                   in1.clear_mkdn_index,
                   in1.on_simple_promo_ind,
                   in1.on_complex_promo_ind,
                   ---
                   max(in1.cur_hier_level_rank) over (partition by in1.item, in1.location) max_rank
              from (select f.cur_hier_level,
                           3 cur_hier_level_rank,
                           il.item,
                           f.dept,
                           f.class,
                           f.subclass,
                           il.location,
                           f.action_date,
                           f.selling_retail,
                           f.selling_retail_currency,
                           f.selling_uom,
                           f.multi_units,
                           f.multi_unit_retail,
                           f.multi_selling_uom,
                           f.price_change_id,
                           f.clear_start_ind,
                           f.clearance_id,
                           f.clear_retail,
                           f.clear_uom,
                           f.clear_mkdn_index,
                           f.on_simple_promo_ind,
                           f.on_complex_promo_ind
                      from rpm_me_il_cost_gtt il,
                           rpm_future_retail f
                     where f.item              = il.item
                       and f.location          = il.location
                       and f.dept              = I_dept
                       and f.cur_hier_level    = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                    UNION ALL
                    select f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.item,
                           f.dept,
                           f.class,
                           f.subclass,
                           il.location,
                           f.action_date,
                           f.selling_retail,
                           f.selling_retail_currency,
                           f.selling_uom,
                           f.multi_units,
                           f.multi_unit_retail,
                           f.multi_selling_uom,
                           f.price_change_id,
                           f.clear_start_ind,
                           f.clearance_id,
                           f.clear_retail,
                           f.clear_uom,
                           f.clear_mkdn_index,
                           f.on_simple_promo_ind,
                           f.on_complex_promo_ind
                      from rpm_me_il_cost_gtt il,
                           rpm_future_retail f
                     where f.item              = il.item
                       and f.location          = il.zone_id
                       and f.dept              = I_dept
                       and f.cur_hier_level    = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                    UNION ALL
                    select f.cur_hier_level,
                           2 cur_hier_level_rank,
                           il.item,
                           f.dept,
                           f.class,
                           f.subclass,
                           il.location,
                           f.action_date,
                           f.selling_retail,
                           f.selling_retail_currency,
                           f.selling_uom,
                           f.multi_units,
                           f.multi_unit_retail,
                           f.multi_selling_uom,
                           f.price_change_id,
                           f.clear_start_ind,
                           f.clearance_id,
                           f.clear_retail,
                           f.clear_uom,
                           f.clear_mkdn_index,
                           f.on_simple_promo_ind,
                           f.on_complex_promo_ind
                      from rpm_me_il_cost_gtt il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.location
                       and f.dept              = I_dept
                       and f.cur_hier_level    = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                    UNION ALL
                    select f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.item,
                           f.dept,
                           f.class,
                           f.subclass,
                           il.location,
                           f.action_date,
                           f.selling_retail,
                           f.selling_retail_currency,
                           f.selling_uom,
                           f.multi_units,
                           f.multi_unit_retail,
                           f.multi_selling_uom,
                           f.price_change_id,
                           f.clear_start_ind,
                           f.clearance_id,
                           f.clear_retail,
                           f.clear_uom,
                           f.clear_mkdn_index,
                           f.on_simple_promo_ind,
                           f.on_complex_promo_ind
                      from rpm_me_il_cost_gtt il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           = il.diff_id
                       and f.location          = il.zone_id
                       and f.dept              = I_dept
                       and f.cur_hier_level    = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                    UNION ALL
                    select f.cur_hier_level,
                           1 cur_hier_level_rank,
                           il.item,
                           f.dept,
                           f.class,
                           f.subclass,
                           il.location,
                           f.action_date,
                           f.selling_retail,
                           f.selling_retail_currency,
                           f.selling_uom,
                           f.multi_units,
                           f.multi_unit_retail,
                           f.multi_selling_uom,
                           f.price_change_id,
                           f.clear_start_ind,
                           f.clearance_id,
                           f.clear_retail,
                           f.clear_uom,
                           f.clear_mkdn_index,
                           f.on_simple_promo_ind,
                           f.on_complex_promo_ind
                      from rpm_me_il_cost_gtt il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.location
                       and f.dept              = I_dept
                       and f.cur_hier_level    = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                    UNION ALL
                    select f.cur_hier_level,
                           0 cur_hier_level_rank,
                           il.item,
                           f.dept,
                           f.class,
                           f.subclass,
                           il.location,
                           f.action_date,
                           f.selling_retail,
                           f.selling_retail_currency,
                           f.selling_uom,
                           f.multi_units,
                           f.multi_unit_retail,
                           f.multi_selling_uom,
                           f.price_change_id,
                           f.clear_start_ind,
                           f.clearance_id,
                           f.clear_retail,
                           f.clear_uom,
                           f.clear_mkdn_index,
                           f.on_simple_promo_ind,
                           f.on_complex_promo_ind
                      from rpm_me_il_cost_gtt il,
                           rpm_future_retail f
                     where f.item              = il.item_parent
                       and f.diff_id           is null
                       and f.location          = il.zone_id
                       and f.dept              = I_dept
                       and f.cur_hier_level    = RPM_CONSTANTS.FR_HIER_PARENT_ZONE) in1,
                   (select store location,
                           RPM_CONSTANTS.ZONE_NODE_TYPE_STORE zone_node_type
                      from store
                     union all
                    select wh location,
                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                      from wh) loc
             where in1.location = loc.location) in2
     where in2.cur_hier_level_rank = in2.max_rank;

   insert into rpm_me_il_retail_gtt
      (item,
       location,
       zone_id,
       crp_start,
       crp_end,
       effective_date,
       current_regular_retail,
       current_regular_retail_uom,
       current_regular_retail_std,
       current_multi_units,
       current_multi_unit_retail,
       current_multi_unit_uom,
       current_clear_retail,
       current_clear_retail_uom,
       current_clear_retail_std,
       basis_regular_retail,
       basis_regular_retail_uom,
       basis_regular_retail_std,
       basis_multi_units,
       basis_multi_unit_retail,
       basis_multi_unit_uom,
       basis_clear_retail,
       basis_clear_retail_uom,
       basis_clear_retail_std,
       basis_clear_mkdn_nbr,
       maint_margin_retail,
       maint_margin_retail_uom,
       maint_margin_retail_std,
       clear_boolean,
       promo_boolean,
       price_change_boolean,
       past_price_chg_date)
   select curr.item,
          curr.location,
          curr.zone_id,
          curr.crp_start,
          curr.crp_end,
          curr.effective_date,
          curr.selling_retail,
          curr.selling_uom,
          curr.selling_retail,
          curr.multi_units,
          curr.multi_unit_retail,
          curr.multi_selling_uom,
          DECODE(curr.action_date,
                 I_curr_date, DECODE(curr.clear_start_ind,
                                     NULL, NULL,
                                     curr.clear_retail),
                 DECODE(curr.clear_start_ind,
                        EVENT_START, curr.clear_retail,
                        EVENT_IN_PROGRESS, curr.clear_retail)),
          DECODE(curr.action_date,
                 I_curr_date, DECODE(curr.clear_start_ind,
                                     NULL, NULL,
                                     curr.clear_uom),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, curr.clear_uom,
                        EVENT_IN_PROGRESS, curr.clear_uom)),
          DECODE(curr.action_date,
                 I_curr_date, DECODE(curr.clear_start_ind,
                                     NULL, NULL,
                                     curr.clear_retail),
                 DECODE(curr.clear_start_ind,
                        EVENT_START, curr.clear_retail,
                        EVENT_IN_PROGRESS, curr.clear_retail)),
          NVL(basis.selling_retail, curr.selling_retail),
          NVL(basis.selling_uom, curr.selling_uom),
          NVL(basis.selling_retail, curr.selling_retail),
          NVL(basis.multi_units, curr.multi_units),
          NVL(basis.multi_unit_retail, curr.multi_unit_retail),
          NVL(basis.multi_selling_uom, curr.multi_selling_uom),
          DECODE(basis.action_date,
                 NULL, DECODE(curr.action_date,
                              I_curr_date, DECODE(curr.clear_start_ind,
                                                  NULL, NULL,
                                                  curr.clear_retail),
                              DECODE(curr.clear_start_ind,
                                     EVENT_START, curr.clear_retail,
                                     EVENT_IN_PROGRESS, curr.clear_retail)),
                 basis.effective_date, DECODE(basis.clear_start_ind,
                                              NULL, NULL,
                                              basis.clear_retail),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, basis.clear_retail,
                        EVENT_IN_PROGRESS, basis.clear_retail)),
          DECODE(basis.action_date,
                 NULL, DECODE(curr.action_date,
                              I_curr_date, DECODE(curr.clear_start_ind,
                                                  NULL, NULL,
                                                  curr.clear_uom),
                              DECODE(basis.clear_start_ind,
                                     EVENT_START, curr.clear_uom,
                                     EVENT_IN_PROGRESS, curr.clear_uom)),
                 basis.effective_date, DECODE(basis.clear_start_ind,
                                              NULL, NULL,
                                              basis.clear_uom),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, basis.clear_uom,
                        EVENT_IN_PROGRESS, basis.clear_uom)),
          DECODE(basis.action_date,
                 NULL, DECODE(curr.action_date,
                              I_curr_date, DECODE(curr.clear_start_ind,
                                                  NULL, NULL,
                                                  curr.clear_retail),
                              DECODE(curr.clear_start_ind,
                                     EVENT_START, curr.clear_retail,
                                     EVENT_IN_PROGRESS, curr.clear_retail)),
                 basis.effective_date, DECODE(basis.clear_start_ind,
                                              NULL, NULL,
                                              basis.clear_retail),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, basis.clear_retail,
                        EVENT_IN_PROGRESS, basis.clear_retail)),
          curr.clear_mkdn_index,
          DECODE(LP_strategy_type,
                 MAINT_MARGIN, NVL(basis.selling_retail, curr.selling_retail),
                 NULL),
          DECODE(LP_strategy_type,
                 MAINT_MARGIN, NVL(basis.selling_uom, curr.selling_uom),
                 NULL),
          DECODE(LP_strategy_type,
                 MAINT_MARGIN, NVL(basis.selling_retail, curr.selling_retail),
                 NULL),
          DECODE(future.clear_boolean_helper,
                 1, 1,
                 0),
          DECODE(DECODE(future.promo_boolean_helper1,
                        NULL, 0,
                        0, 0,
                        1)
                 + DECODE(future.promo_boolean_helper2,
                          NULL, 0,
                          0, 0,
                          1)
                 + DECODE(curr.promo_boolean_helper1,
                          NULL, 0,
                          0, 0,
                          1)
                 + DECODE(curr.promo_boolean_helper2,
                          NULL, 0,
                          0, 0,
                          1),
                 1, 1,
                 2, 1,
                 3, 1,
                 4, 1,
                 0),
          DECODE(future.price_change_boolean_helper,
                 NULL, 0,
                 1),
          DECODE(curr.price_change_id,
                 NULL, NULL,
                 curr.action_date)
     from (select item,
                  location,
                  effective_date,
                  action_date,
                  selling_retail,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  clear_start_ind,
                  clearance_id,
                  clear_retail,
                  clear_uom
             from (select i.item,
                          i.location,
                          i.effective_date,
                          f.action_date,
                          f.selling_retail,
                          f.selling_uom,
                          f.multi_units,
                          f.multi_unit_retail,
                          f.multi_selling_uom,
                          f.clear_start_ind,
                          f.clearance_id,
                          f.clear_retail,
                          f.clear_uom,
                          ROW_NUMBER() OVER (PARTITION BY i.item,
                                                          i.location
                                                 ORDER BY f.action_date desc) rank
                     from rpm_future_retail_gtt f,
                          rpm_me_il_cost_gtt i
                    where f.item(+)          = i.item
                      and f.location(+)      = i.location
                      and f.dept(+)          = I_dept
                      and f.action_date(+)   > I_curr_date
                      and f.action_date(+)  <= i.effective_date) basis_in
            where rank = 1) basis,
          (select distinct i.item,
                  i.location,
                  FIRST_VALUE(f.action_date) OVER (PARTITION BY i.item,
                                                                i.location
                                                       ORDER BY f.action_date desc) action_date,
                  MIN(f.clear_start_ind) OVER (PARTITION BY i.item,
                                                            i.location) clear_boolean_helper,
                  MAX(f.price_change_id) OVER (PARTITION BY i.item,
                                                            i.location) price_change_boolean_helper,
                  --if exclusion exists
                  MAX(NVL(f.on_simple_promo_ind, 0)) OVER (PARTITION BY i.item,
                                                                        i.location) promo_boolean_helper1,
                  MAX(NVL(f.on_complex_promo_ind, 0)) OVER (PARTITION BY i.item,
                                                                         i.location) promo_boolean_helper2
             from rpm_future_retail_gtt f,
                  rpm_me_il_cost_gtt i
            where f.item(+)          = i.item
              and f.location(+)      = i.location
              and f.dept(+)          = I_dept
              and f.action_date(+)   > I_curr_date) future,
          (select item,
                  location,
                  effective_date,
                  zone_id,
                  crp_start,
                  crp_end,
                  action_date,
                  selling_retail,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  clear_start_ind,
                  clear_mkdn_index,
                  clear_retail,
                  clear_uom,
                  price_change_id,
                  promo_boolean_helper1,
                  promo_boolean_helper2
             from (select i.item,
                          i.location,
                          i.effective_date,
                          i.zone_id,
                          i.crp_start,
                          i.crp_end,
                          f.action_date,
                          f.selling_retail,
                          f.selling_uom,
                          f.multi_units,
                          f.multi_unit_retail,
                          f.multi_selling_uom,
                          f.clear_start_ind,
                          f.clear_mkdn_index,
                          f.clear_retail,
                          f.clear_uom,
                          f.price_change_id,
                          NVL(f.on_simple_promo_ind, 0) promo_boolean_helper1,
                          NVL(f.on_complex_promo_ind, 0) promo_boolean_helper2,
                          ROW_NUMBER() OVER (PARTITION BY i.item,
                                                          i.location
                                                 ORDER BY f.action_date desc) rank
                     from rpm_future_retail_gtt f,
                          rpm_me_il_cost_gtt i
                    where f.item(+)         = i.item
                      and f.location(+)     = i.location
                      and f.dept(+)         = I_dept
                      and f.action_date(+) <= I_curr_date) curr_in
            where rank = 1) curr
    where basis.item     = future.item
      and basis.location = future.location
      and basis.item     = curr.item
      and basis.location = curr.location;

   if POPULATE_ITEM_PRICING_INFO(O_error_msg,
                                 I_curr_date) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PRICING_INFO;
----------------------------------------------------------------------------------------
FUNCTION POPULATE_PRICING_INFO_BATCH(O_error_msg           IN OUT  VARCHAR2,
                                     I_me_sequence_id      IN      RPM_PRE_ME_AGGREGATION.ME_SEQUENCE_ID%TYPE,
                                     I_me_area_diff_seq_id IN      RPM_PRE_ME_AGGREGATION.ME_AREA_DIFF_SEQ_ID%TYPE,
                                     I_curr_date           IN      DATE,
                                     I_dept                IN      DEPS.DEPT%TYPE,
                                     I_zone_id             IN      RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE)
RETURN BOOLEAN IS

   L_program  VARCHAR2(50)  := 'RPM_EXT_SQL.POPULATE_PRICING_INFO_BATCH';

BEGIN

   insert into rpm_me_il_retail_gtt
      (item,
       location,
       zone_id,
       crp_start,
       crp_end,
       effective_date,
       current_regular_retail,
       current_regular_retail_uom,
       current_regular_retail_std,
       current_multi_units,
       current_multi_unit_retail,
       current_multi_unit_uom,
       current_clear_retail,
       current_clear_retail_uom,
       current_clear_retail_std,
       basis_regular_retail,
       basis_regular_retail_uom,
       basis_regular_retail_std,
       basis_multi_units,
       basis_multi_unit_retail,
       basis_multi_unit_uom,
       basis_clear_retail,
       basis_clear_retail_uom,
       basis_clear_retail_std,
       basis_clear_mkdn_nbr,
       maint_margin_retail,
       maint_margin_retail_uom,
       maint_margin_retail_std,
       clear_boolean,
       promo_boolean,
       price_change_boolean,
       past_price_chg_date)
   select /*+ ordered */ curr.item,
          curr.location,
          curr.zone_id,
          curr.crp_start,
          curr.crp_end,
          curr.effective_date,
          curr.selling_retail,
          curr.selling_uom,
          curr.selling_retail,
          curr.multi_units,
          curr.multi_unit_retail,
          curr.multi_selling_uom,
          DECODE(curr.action_date,
                 I_curr_date, DECODE(curr.clear_start_ind,
                                     NULL, NULL,
                                     curr.clear_retail),
                 DECODE(curr.clear_start_ind,
                        EVENT_START, curr.clear_retail,
                        EVENT_IN_PROGRESS, curr.clear_retail)),
          DECODE(curr.action_date,
                 I_curr_date, DECODE(curr.clear_start_ind,
                                     NULL, NULL,
                                     curr.clear_uom),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, curr.clear_uom,
                        EVENT_IN_PROGRESS, curr.clear_uom)),
          DECODE(curr.action_date,
                 I_curr_date, DECODE(curr.clear_start_ind,
                                     NULL, NULL,
                                     curr.clear_retail),
                 DECODE(curr.clear_start_ind,
                        EVENT_START, curr.clear_retail,
                        EVENT_IN_PROGRESS, curr.clear_retail)),
          NVL(basis.selling_retail, curr.selling_retail),
          NVL(basis.selling_uom, curr.selling_uom),
          NVL(basis.selling_retail, curr.selling_retail),
          NVL(basis.multi_units, curr.multi_units),
          NVL(basis.multi_unit_retail, curr.multi_unit_retail),
          NVL(basis.multi_selling_uom, curr.multi_selling_uom),
          DECODE(basis.action_date,
                 NULL, DECODE(curr.action_date,
                              I_curr_date, DECODE(curr.clear_start_ind,
                                                  NULL, NULL,
                                                  curr.clear_retail),
                              DECODE(curr.clear_start_ind,
                                     EVENT_START, curr.clear_retail,
                                     EVENT_IN_PROGRESS, curr.clear_retail)),
                 basis.effective_date, DECODE(basis.clear_start_ind,
                                              NULL, NULL,
                                              basis.clear_retail),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, basis.clear_retail,
                        EVENT_IN_PROGRESS, basis.clear_retail)),
          DECODE(basis.action_date,
                 NULL, DECODE(curr.action_date,
                              I_curr_date, DECODE(curr.clear_start_ind,
                                                  NULL, NULL,
                                                  curr.clear_uom),
                              DECODE(basis.clear_start_ind,
                                     EVENT_START, curr.clear_uom,
                                     EVENT_IN_PROGRESS, curr.clear_uom)),
                 basis.effective_date, DECODE(basis.clear_start_ind,
                                              NULL, NULL,
                                              basis.clear_uom),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, basis.clear_uom,
                        EVENT_IN_PROGRESS, basis.clear_uom)),
          DECODE(basis.action_date,
                 NULL, DECODE(curr.action_date,
                              I_curr_date, DECODE(curr.clear_start_ind,
                                                  NULL, NULL,
                                                  curr.clear_retail),
                              DECODE(curr.clear_start_ind,
                                     EVENT_START, curr.clear_retail,
                                     EVENT_IN_PROGRESS, curr.clear_retail)),
                 basis.effective_date, DECODE(basis.clear_start_ind,
                                              NULL, NULL,
                                              basis.clear_retail),
                 DECODE(basis.clear_start_ind,
                        EVENT_START, basis.clear_retail,
                        EVENT_IN_PROGRESS, basis.clear_retail)),
          curr.clear_mkdn_index,
          DECODE(LP_strategy_type,
                 MAINT_MARGIN, NVL(basis.selling_retail, curr.selling_retail),
                 NULL),
          DECODE(LP_strategy_type,
                 MAINT_MARGIN, NVL(basis.selling_uom, curr.selling_uom),
                 NULL),
          DECODE(LP_strategy_type,
                 MAINT_MARGIN, NVL(basis.selling_retail, curr.selling_retail),
                 NULL),
          DECODE(future.clear_boolean_helper,
                 1, 1,
                 0),
          DECODE(DECODE(future.promo_boolean_helper,
                        NULL, 0,
                        0, 0,
                        1)
                 + DECODE(curr.promo_boolean_helper,
                         NULL, 0,
                         0, 0,
                         1),
                 1, 1,
                 2, 1,
                 0),
          DECODE(future.price_change_boolean_helper,
                 NULL, 0,
                 1),
          DECODE(curr.price_change_id,
                 NULL, NULL,
                 curr.action_date)
     from (select item,
                  location,
                  effective_date,
                  action_date,
                  selling_retail,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  clear_start_ind,
                  clearance_id,
                  clear_retail,
                  clear_uom
             from (select /*+ leading(i) use_hash(f) */ i.item,
                          i.location,
                          i.effective_date,
                          f.action_date,
                          f.selling_retail,
                          f.selling_uom,
                          f.multi_units,
                          f.multi_unit_retail,
                          f.multi_selling_uom,
                          f.clear_start_ind,
                          f.clearance_id,
                          f.clear_retail,
                          f.clear_uom,
                          ROW_NUMBER() OVER (PARTITION BY i.item,
                                                          i.location
                                                 ORDER BY f.action_date desc) rank
                     from (select action_date,
                                  selling_retail,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_selling_uom,
                                  clear_start_ind,
                                  clearance_id,
                                  clear_retail,
                                  clear_uom,
                                  item,
                                  loc
                             from rpm_pre_me_retail
                            where me_sequence_id      = I_me_sequence_id
                              and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                          rpm_me_il_cost_gtt i
                    where f.item(+)          = i.item
                      and f.loc(+)           = i.location
                      and f.action_date(+)   > I_curr_date
                      and f.action_date(+)  <= i.effective_date) basis_in
            where rank = 1) basis,
          (select /*+ leading(i) use_hash(f) */ distinct i.item,
                  i.location,
                  FIRST_VALUE(f.action_date) OVER (PARTITION BY i.item,
                                                                i.location
                                                       ORDER BY f.action_date desc) action_date,
                  MIN(f.clear_start_ind) OVER (PARTITION BY i.item,
                                                            i.location) clear_boolean_helper,
                  MAX(f.price_change_id) OVER (PARTITION BY i.item,
                                                            i.location) price_change_boolean_helper,
                  --if exclusion exists
                  MAX(NVL(f.promo_comp_ind, 0)) OVER (PARTITION BY i.item,
                                                                  i.location) promo_boolean_helper
             from (select action_date,
                          selling_retail,
                          selling_uom,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          clear_start_ind,
                          clearance_id,
                          clear_retail,
                          clear_uom,
                          item,
                          loc,
                          promo_comp_ind,
                          price_change_id
                     from rpm_pre_me_retail
                    where me_sequence_id      = I_me_sequence_id
                      and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                  rpm_me_il_cost_gtt i
            where f.item(+)          = i.item
              and f.loc(+)           = i.location
              and f.action_date(+)   > I_curr_date) future,
          (select item,
                  location,
                  zone_id,
                  crp_start,
                  crp_end,
                  effective_date,
                  action_date,
                  selling_retail,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  clear_start_ind,
                  clear_mkdn_index,
                  clear_retail,
                  clear_uom,
                  price_change_id,
                  promo_boolean_helper
             from (select /*+ leading(i) use_hash(f) */ i.item,
                          i.location,
                          i.zone_id,
                          i.crp_start,
                          i.crp_end,
                          i.effective_date,
                          f.action_date,
                          f.selling_retail,
                          f.selling_uom,
                          f.multi_units,
                          f.multi_unit_retail,
                          f.multi_selling_uom,
                          f.clear_start_ind,
                          f.clear_mkdn_index,
                          f.clear_retail,
                          f.clear_uom,
                          f.price_change_id,
                          NVL(f.promo_comp_ind,0) promo_boolean_helper,
                          ROW_NUMBER() OVER (PARTITION BY i.item,
                                                          i.location
                                                 ORDER BY f.action_date desc) rank
                     from (select action_date,
                                  selling_retail,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_selling_uom,
                                  clear_start_ind,
                                  clearance_id,
                                  clear_retail,
                                  clear_uom,
                                  item,
                                  loc,
                                  promo_comp_ind,
                                  price_change_id,
                                  clear_mkdn_index
                             from rpm_pre_me_retail
                            where me_sequence_id      = I_me_sequence_id
                              and me_area_diff_seq_id = I_me_area_diff_seq_id) f,
                          rpm_me_il_cost_gtt i
                    where f.item(+)         = i.item
                      and f.loc(+)          = i.location
                      and f.action_date(+) <= I_curr_date) curr_in
            where rank = 1) curr
    where basis.item     = future.item
      and basis.location = future.location
      and basis.item     = curr.item
      and basis.location = curr.location;

   if POPULATE_ITEM_PRICING_INFO(O_error_msg,
                                 I_curr_date) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PRICING_INFO_BATCH;
----------------------------------------------------------------------------------------

FUNCTION POPULATE_ITEM_PRICING_INFO(O_error_msg  IN OUT  VARCHAR2,
                                    I_curr_date  IN      DATE)
   RETURN BOOLEAN IS

BEGIN

   merge /*+ INDEX(gtt) */ into rpm_me_item_gtt gtt
   using (
      select basis.item,
             basis.zone_id,
             --
             basis.selling_retail    basis_selling_retail,
             basis.selling_uom       basis_selling_uom,
             basis.multi_units       basis_multi_units,
             basis.multi_unit_retail basis_multi_unit_retail,
             basis.multi_selling_uom basis_multi_selling_uom,
             --
             curr.selling_retail    curr_selling_retail,
             curr.selling_uom       curr_selling_uom,
             curr.multi_units       curr_multi_units,
             curr.multi_unit_retail curr_multi_unit_retail,
             curr.multi_selling_uom curr_multi_selling_uom,
             --
             decode(LP_maint_margin_ind, 1, maintmargin.selling_retail, null) maintmargin_selling_retail,
             decode(LP_maint_margin_ind, 1, maintmargin.selling_uom, null) maintmargin_selling_uom
        from
            (
             select t.item,
                    t.zone_id,
                    t.selling_retail,
                    t.selling_uom,
                    t.multi_units,
                    t.multi_unit_retail,
                    t.multi_selling_uom
               from (
                     select /*+ INDEX(f RPM_ZONE_FUTURE_RETAIL_I1) */ i.item,
                            i.zone_id,
                            f.selling_retail,
                            f.selling_uom,
                            f.multi_units,
                            f.multi_unit_retail,
                            f.multi_selling_uom,
                            rank() over(partition by i.item order by f.action_date desc) rank
                       from rpm_zone_future_retail f,
                            (select item, zone_id, min(effective_date) effective_date
                               from rpm_me_il_retail_gtt group by item, zone_id) i
                      where f.item(+)          = i.item
                        and f.zone(+)          = i.zone_id
                        and f.action_date(+)   > I_curr_date
                        and f.action_date(+)  <= i.effective_date
                    ) t
              where t.rank = 1
             ) basis,
          ---
            (
             select t.item,
                    t.zone_id,
                    t.selling_retail,
                    t.selling_uom,
                    t.multi_units,
                    t.multi_unit_retail,
                    t.multi_selling_uom
               from (
                     select /*+ INDEX(f RPM_ZONE_FUTURE_RETAIL_I1) */ i.item,
                            i.zone_id,
                            f.selling_retail,
                            f.selling_uom,
                            f.multi_units,
                            f.multi_unit_retail,
                            f.multi_selling_uom,
                            rank() over(partition by i.item order by f.action_date desc) rank
                       from rpm_zone_future_retail f,
                            rpm_me_item_gtt i
                      where f.item(+)          = i.item
                        and f.zone(+)          = i.zone_id
                        and f.action_date(+)  <= I_curr_date
                    ) t
              where t.rank = 1
            ) curr,
          ---
            (
             select t.item,
                    t.zone_id,
                    t.selling_retail,
                    t.selling_uom
               from (
                     select /*+ INDEX(f RPM_ZONE_FUTURE_RETAIL_I1) */ i.item,
                            i.zone_id,
                            f.selling_retail,
                            f.selling_uom,
                            rank() over(partition by i.item order by f.action_date desc) rank
                       from rpm_zone_future_retail f,
                            (select item, zone_id, min(effective_date) effective_date, crp_start
                               from rpm_me_il_retail_gtt group by item, zone_id, crp_start) i
                      where f.item(+)          = i.item
                        and f.zone(+)          = i.zone_id
                        and f.action_date(+)   > i.crp_start
                        and f.action_date(+)  <= i.effective_date
                    ) t
              where t.rank = 1
               ) maintmargin
       where basis.item     = curr.item
         and basis.zone_id  = curr.zone_id
         and basis.item     = maintmargin.item
         and basis.zone_id  = maintmargin.zone_id) use
   on (    gtt.item    = use.item
       and gtt.zone_id = use.zone_id)
   when matched then
   update
      set gtt.basis_zl_regular_retail       = basis_selling_retail,
          gtt.basis_zl_regular_retail_uom   = basis_selling_uom,
          --
          gtt.basis_zl_multi_units          = basis_multi_units,
          gtt.basis_zl_multi_unit_retail    = basis_multi_unit_retail,
          gtt.basis_zl_multi_uom            = basis_multi_selling_uom,
          --
          gtt.current_zl_regular_retail     = nvl(curr_selling_retail, basis_selling_retail),
          gtt.current_zl_regular_retail_uom = nvl(curr_selling_uom, basis_selling_uom),
          --
          gtt.current_zl_multi_units        = nvl(curr_multi_units, basis_multi_units),
          gtt.current_zl_multi_unit_retail  = nvl(curr_multi_unit_retail, basis_multi_unit_retail),
          gtt.current_zl_multi_uom          = nvl(curr_multi_selling_uom, basis_multi_selling_uom),
          --
          gtt.maint_margin_zl_retail        = maintmargin_selling_retail,
          gtt.maint_margin_zl_retail_uom    = maintmargin_selling_uom
   --forced fail
   when not matched then
   insert (item)
    values ('abc');

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_ITEM_PRICING_INFO',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_ITEM_PRICING_INFO;

----------------------------------------------------------------------------------------

FUNCTION REMOVE_COST(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   if LP_record_item_loc_removes then
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
         select LP_record_item_loc_strategy,
                LP_vdate,
                cost.item,
                cost.location,
                NO_COST
           from rpm_me_il_cost_gtt cost
          where (cost.current_cost = 0 or cost.current_cost is null)
            and not exists (select 'x'
                              from item_master im
                             where im.item = cost.item
                               and im.orderable_ind = 'N');
   end if;

   delete from rpm_me_item_loc_gtt gtt
    where (gtt.item) in (select distinct cost.item
                           from rpm_me_il_cost_gtt cost
                          where (cost.current_cost = 0 or cost.current_cost is null)
                            and not exists (select 'x'
                                              from item_master im
                                             where im.item = cost.item
                                               and im.orderable_ind = 'N'));
   ---

   if LP_maint_margin_ind = 1 then

      if LP_record_item_loc_removes then
         insert into rpm_merch_extract_deletions
                  (strategy_id,
                   delete_date,
                   item,
                   location,
                   reason_code )
            select LP_record_item_loc_strategy,
                   LP_vdate,
                   cost.item,
                   cost.location,
                   INVALID_MAINT_MARGIN_COST
              from rpm_me_il_cost_gtt cost
             where nvl(cost.maint_margin_cost, -999) = nvl(cost.basis_pricing_cost, -999);
      end if;

      delete from rpm_me_item_loc_gtt gtt
       where (gtt.item, gtt.location) in (select cost.item, cost.location
                                            from rpm_me_il_cost_gtt cost
                                           where nvl(cost.maint_margin_cost, -999) = nvl(cost.basis_pricing_cost, -999));

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.REMOVE_COST',
                                        to_char(SQLCODE));
      return FALSE;

END REMOVE_COST;
----------------------------------------------------------------------------------------
FUNCTION REMOVE_RETAIL(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   if LP_record_item_loc_removes then
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
         select LP_record_item_loc_strategy,
                LP_vdate,
                retail.item,
                retail.location,
                NO_RETAIL
           from rpm_me_il_retail_gtt retail
          where retail.current_regular_retail is null;
   end if;

   delete from rpm_me_item_loc_gtt gtt
    where (gtt.item, gtt.location) in (select retail.item, retail.location
                                         from rpm_me_il_retail_gtt retail
                                        where retail.current_regular_retail is null);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.REMOVE_RETAIL',
                                        to_char(SQLCODE));
      return FALSE;

END REMOVE_RETAIL;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_REPL_AND_SALES_INFO(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

   L_smooth_avg_ind   BOOLEAN := FALSE;

BEGIN

   if (LP_sales_calc_method = SMOOTHED_AVG_SALES_CALC_METHOD and LP_review_not_started) then
      L_smooth_avg_ind := TRUE;
   else
      L_smooth_avg_ind := FALSE;
   end if;

   --Repl IND
   if LP_repl_ind_process_ind then
      insert into rpm_me_il_repl_gtt (item,
                                      location,
                                      replenish_ind)
        select /*+ USE_NL(IL, RIL) */ il.item,
               il.location,
               1
          from rpm_me_item_loc_gtt il,
               repl_item_loc ril
         where ril.item           = il.item
           and ril.location       = il.location
           and ril.activate_date <= LP_tomorrow
           and nvl(ril.deactivate_date, LP_tomorrow + 1) > LP_tomorrow;
   end if;

   --Hist Sales
   if LP_hist_sales_process_ind then
      insert into rpm_me_il_hist_sales_gtt (item,
                                            location,
                                            historical_sales,
                                            historical_sales_units,
                                            historical_issues)
        select r.item,
               r.location,
               decode(ilh.loc_type, 'S', sum(value)),
               decode(ilh.loc_type, 'S', sum(sales_issues)),
               decode(ilh.loc_type, 'W', sum(value))
          from rpm_me_item_loc_gtt r,
               item_loc_hist ilh
         where ilh.item = r.item
           and ilh.loc  = r.location
           and (   (    LP_time_frame = WEEKLY_HIST
                    and ilh.eow_date  = LP_last_eow_date_unit))
           and (   ilh.sales_type = 'I'
                or ilh.sales_type = decode(LP_regular_sales_ind, 1, 'R')
                or ilh.sales_type = decode(LP_clearance_sales_ind, 1, 'C')
                or ilh.sales_type = decode(LP_promotional_sales_ind, 1, 'P'))
         group by r.item,
                  r.location,
                  ilh.loc_type
         union all
        select r.item,
               r.location,
               decode(ilh.loc_type, 'S', sum(value)),
               decode(ilh.loc_type, 'S', sum(sales_issues)),
               decode(ilh.loc_type, 'W', sum(value))
          from rpm_me_item_loc_gtt r,
               item_loc_hist ilh
         where ilh.item = r.item
           and ilh.loc  = r.location
           and (   (    LP_time_frame = MONTHLY_HIST
                    and ilh.eow_date <= LP_last_eom_date_unit
                    and ilh.eow_date >= LP_first_day_of_month))
           and (   ilh.sales_type = 'I'
                or ilh.sales_type = decode(LP_regular_sales_ind, 1, 'R')
                or ilh.sales_type = decode(LP_clearance_sales_ind, 1, 'C')
                or ilh.sales_type = decode(LP_promotional_sales_ind, 1, 'P'))
         group by r.item,
                  r.location,
                  ilh.loc_type
         union all
        select r.item,
               r.location,
               decode(ilh.loc_type, 'S', sum(value)),
               decode(ilh.loc_type, 'S', sum(sales_issues)),
               decode(ilh.loc_type, 'W', sum(value))
          from rpm_me_item_loc_gtt r,
               item_loc_hist ilh
         where ilh.item = r.item
           and ilh.loc  = r.location
           and (   (    LP_time_frame = HALFYEARLY_HIST
                    and ilh.eow_date between (LP_tomorrow - 182) and LP_tomorrow))
           and (   ilh.sales_type = 'I'
                or ilh.sales_type = decode(LP_regular_sales_ind, 1, 'R')
                or ilh.sales_type = decode(LP_clearance_sales_ind, 1, 'C')
                or ilh.sales_type = decode(LP_promotional_sales_ind, 1, 'P'))
         group by r.item,
                  r.location,
                  ilh.loc_type
         union all
        select r.item,
               r.location,
               decode(ilh.loc_type, 'S', sum(value)),
               decode(ilh.loc_type, 'S', sum(sales_issues)),
               decode(ilh.loc_type, 'W', sum(value))
          from rpm_me_item_loc_gtt r,
               item_loc_hist ilh
         where ilh.item = r.item
           and ilh.loc  = r.location
           and (   (    LP_time_frame = YEARLY_HIST
                    and ilh.eow_date  between (LP_tomorrow - 365) and LP_tomorrow))
           and (   ilh.sales_type = 'I'
                or ilh.sales_type = decode(LP_regular_sales_ind, 1, 'R')
                or ilh.sales_type = decode(LP_clearance_sales_ind, 1, 'C')
                or ilh.sales_type = decode(LP_promotional_sales_ind, 1, 'P'))
         group by r.item,
                  r.location,
                  ilh.loc_type;
   end if;

   --Projected Sales
   if LP_proj_sales_process_ind then
      insert into rpm_me_il_proj_sales_gtt (item,
                                            location,
                                            projected_sales)
        select /*+ index(irsa pk_if_rpm_smoothed_avg) */ irsa.item,
               irsa.store,
               sum(total_smoothed_avg) as projected_sales
          from if_rpm_smoothed_avg irsa,
               rpm_me_item_loc_gtt rtil
         where irsa.item  = rtil.item
           and irsa.store = rtil.location
         group by irsa.item,
               irsa.store;
   end if;

   --Season Sales
   if LP_seasonal_sales_process_ind then
      insert into rpm_me_il_season_sales_gtt (item,
                                              location,
                                              seasonal_sales)
        select /*+ INDEX(ilh, pk_item_loc_hist) */ ilh.item,
               ilh.loc,
               nvl(sum(ilh.sales_issues), 0) as sales_issues
          from item_loc_hist ilh,
               (select i.item,
                       i.location,
                       min(s.start_date) as start_date,
                       min(s.end_date) as end_date
                  from rpm_me_item_loc_gtt i,
                       item_seasons its,
                       seasons s
                 where i.item        = its.item
                   and its.season_id = s.season_id
                   and LP_vdate      between s.start_date and s.end_date
                 group by i.item,
                          i.location) r
         where ilh.item     = r.item
           and ilh.eow_date between r.start_date and r.end_date
           and ilh.loc      = r.location
           and (   ilh.sales_type = 'I'
                or ilh.sales_type = decode(LP_regular_sales_ind, 1, 'R')
                or ilh.sales_type = decode(LP_clearance_sales_ind, 1, 'C')
                or ilh.sales_type = decode(LP_promotional_sales_ind, 1, 'P'))
         group by ilh.item,
                  ilh.loc;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_REPL_AND_SALES_INFO',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_REPL_AND_SALES_INFO;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_OTHER_LOC_INFO(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   --Link Code
   insert into rpm_me_il_link_code_gtt (item,
                                        location,
                                        link_code,
                                        link_code_display_id,
                                        link_code_desc)
     select /*+ use_nl(i) */
            i.item,
            i.location,
            r.link_code,
            rc.code,
            rc.description
       from rpm_link_code_attribute r,
            rpm_me_item_loc_gtt i,
            rpm_codes rc
      where r.item          = i.item
        and r.location      = i.location
        and r.link_code     = rc.code_id(+)
        and rc.code_type(+) = LINK_CODE_TYPE;

   --Clearance Dates
   if LP_clear_dates_process_ind then
      insert into rpm_me_il_clear_dates_gtt (item,
                                             location,
                                             proposed_reset_date,
                                             proposed_out_of_stock_date)
         select c.item,
                c.location,
                max(c.effective_date),
                max(c.out_of_stock_date)
           from rpm_clearance_reset c,
                rpm_me_item_loc_gtt l
          where c.item      = l.item
            and c.location  = l.location
            and c.state     = 'pricechange.state.approved'
          group by c.item,
                   c.location;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_OTHER_LOC_INFO',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_OTHER_LOC_INFO;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_VAT(O_error_msg  IN OUT  VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_EXT_SQL.POPULATE_VAT';

   L_default_tax_type    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE    := NULL;
   L_class_level_vat_ind SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE := NULL;

   L_tax_calc_tbl   OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_custom_tax_ind NUMBER           := NULL;

   cursor C_CHECK_CUSTOM is
      select 1
        from rpm_me_item_loc_gtt gtt,
             (select s.store loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_STORE zone_node_type
                from store s,
                     vat_region vr
               where s.vat_region  = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM
              union all
              select w.wh loc,
                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type
                from wh w,
                     vat_region vr
               where w.vat_region  = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM) locs
       where gtt.loc_type = locs.zone_node_type
         and gtt.location = locs.loc
         and rownum       = 1;

BEGIN

   select default_tax_type,
          class_level_vat_ind
     into L_default_tax_type,
          L_class_level_vat_ind
     from system_options;

   if L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then

      insert into rpm_me_il_vat_gtt (item,
                                     location,
                                     vat_rate,
                                     vat_value)
        select item,
               location,
               vat_rate,
               vat_value
          from (select i.item,
                       i.location,
                       gtax.cum_tax_pct vat_rate,
                       gtax.cum_tax_value vat_value,
                       gtax.effective_from_date,
                       FIRST_VALUE(gtax.effective_from_date) OVER (PARTITION BY i.item,
                                                                                i.location
                                                                       ORDER BY gtax.effective_from_date desc) max_date
                  from gtax_item_rollup gtax,
                       rpm_me_item_loc_gtt i
                 where gtax.item = i.item
                   and gtax.loc  = i.location)
         where effective_from_date = max_date;

   elsif L_default_tax_type = RPM_CONSTANTS.SVAT_TAX_TYPE then

      open C_CHECK_CUSTOM;
      fetch C_CHECK_CUSTOM into L_custom_tax_ind;
      close C_CHECK_CUSTOM;

      if NVL(L_custom_tax_ind, 0) = 1 then

         select OBJ_TAX_CALC_REC(item,            -- I_item
                                 NULL,            -- I_pack_ind
                                 location,        -- I_from_entity
                                 DECODE(loc_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_from_entity_type
                                 location,        -- I_to_entity
                                 DECODE(loc_type,
                                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_to_entity_type
                                 effective_date,  -- I_effective_from_date
                                 basis_regular_retail, -- I_amount
                                 NULL,            -- I_amount_curr
                                 NULL,            -- I_amount_tax_incl_ind
                                 NULL,            -- I_origin_country_id
                                 NULL,            -- O_cum_tax_pct
                                 NULL,            -- O_cum_tax_value
                                 NULL,            -- O_total_tax_amount
                                 NULL,            -- O_total_tax_amount_curr
                                 NULL,            -- O_total_recover_amount
                                 NULL,            -- O_total_recover_amount_curr
                                 NULL,            -- O_tax_detail_tbl
                                 'MARKUPCALC',    -- I_tran_type
                                 effective_date,  -- I_tran_date
                                 NULL,            -- I_tran_id
                                 'R')             -- I_cost_retail_ind
            BULK COLLECT into L_tax_calc_tbl
           from rpm_me_item_loc_gtt;

         if TAX_SQL.CALC_RETAIL_TAX(O_error_msg,
                                    L_tax_calc_tbl) = FALSE then
            return FALSE;
         end if;

         if L_tax_calc_tbl is NOT NULL and
            L_tax_calc_tbl.COUNT > 0 then

            forall i IN 1..L_tax_calc_tbl.COUNT
               insert into rpm_me_il_vat_gtt (item,
                                              location,
                                              vat_rate,
                                              vat_value)
                                      values (L_tax_calc_tbl(i).I_item,
                                              L_tax_calc_tbl(i).I_from_entity,
                                              L_tax_calc_tbl(i).O_cum_tax_pct,
                                              0);
         end if;

      else

         insert into rpm_me_il_vat_gtt (item,
                                        location,
                                        vat_rate,
                                        vat_value)
           select item,
                  loc,
                  vat_rate,
                  0
             from (select i.item,
                          locs.loc,
                          v1.vat_rate,
                          v1.active_date,
                          FIRST_VALUE(v1.active_date) OVER (PARTITION BY i.item,
                                                                         locs.vat_region
                                                                ORDER BY v1.active_date desc) max_date
                     from vat_item v1,
                          (select store loc,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_STORE zone_node_type,
                                  vat_region
                             from store
                           union all
                           select wh loc,
                                  RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE zone_node_type,
                                  vat_region
                             from wh) locs,
                          rpm_me_item_loc_gtt i,
                          item_master im,
                          class c
                    where v1.item         = i.item
                      and i.location      = locs.loc
                      and i.item          = im.item
                      and im.class        = c.class
                      and i.loc_type      = locs.zone_node_type
                      and locs.vat_region = v1.vat_region
                      and v1.vat_type     IN (RPM_CONSTANTS.VAT_TYPE_RETAIL,
                                              RPM_CONSTANTS.VAT_TYPE_BOTH)
                      and (   L_class_level_vat_ind     = 'N'
                           or (    L_class_level_vat_ind = 'Y'
                               and c.class_vat_ind       = 'Y')))
            where active_date = max_date;

      end if;

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_VAT;

----------------------------------------------------------------------------------------
FUNCTION POPULATE_ON_ORDER(O_error_msg  IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   insert into rpm_me_il_onorder_gtt (item,
                                      location,
                                      location_on_order,
                                      location_inventory)
     select use.item,
            use.location,
            sum(use.on_order_item + use.on_order_pack),
            greatest(sum(inventory_item), sum(inventory_pack)) + sum(use.on_order_item + use.on_order_pack)
       from (select /*+ cardinality(i 10) */
                    i.item,
                    i.location,
                    sum(nvl(ol.qty_ordered, 0) - nvl(ol.qty_received, 0)) on_order_item,
                    i.location_inventory inventory_item,
                    0 on_order_pack,
                    0 inventory_pack
               from rpm_me_item_loc_gtt i,
                    ordloc ol,
                    ordhead oh
              where ol.item     = i.item
                and ol.order_no = oh.order_no
                and ol.location = i.location
                and oh.status   = 'A'
                and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
              group by i.item,
                       i.location,
                       i.location_inventory
             union all
             select /*+ cardinality(i 10) */
                    i.item,
                    i.location,
                    0 on_order_item,
                    0 inventory_item,
                    sum(nvl(ol.qty_ordered, 0) - nvl(ol.qty_received, 0)) on_order_pack,
                    i.location_inventory inventory_pack
               from rpm_me_item_loc_gtt i,
                    v_packsku_qty vpq,
                    ordloc ol,
                    ordhead oh
              where vpq.item    = i.item
                and ol.item     = vpq.pack_no
                and ol.order_no = oh.order_no
                and ol.location = i.location
                and oh.status   = 'A'
                and nvl(ol.qty_ordered, 0) > nvl(ol.qty_received, 0)
              group by i.item,
                       i.location,
                       i.location_inventory) use
      group by use.item,
               use.location;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.POPULATE_ON_ORDER',
                                        to_char(SQLCODE));
      return FALSE;

END POPULATE_ON_ORDER;

----------------------------------------------------------------------------------------
FUNCTION CONSOLIDATE_GTT(O_error_msg     IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

BEGIN

   insert into rpm_me_consolidate_itemloc_gtt (
              worksheet_item_loc_data_id,
              worksheet_item_data_id,
              workspace_rowid,
              worksheet_status_id,
              proposed_strategy_id,
              user_strategy_id,
              --
              item,
              margin_mkt_basket_code,
              comp_mkt_basket_code,
              margin_mbc_name,
              competitive_mbc_name,
              location,
              loc_type,
              il_primary_supp,
              il_primary_cntry,
              current_regular_retail,
              current_regular_retail_uom,
              current_regular_retail_std,
              current_multi_units,
              current_multi_unit_retail,
              current_multi_unit_uom,
              current_clear_retail,
              current_clear_retail_uom,
              current_clear_retail_std,
              basis_regular_retail,
              basis_regular_retail_uom,
              basis_regular_retail_std,
              basis_multi_units,
              basis_multi_unit_retail,
              basis_multi_unit_uom,
              basis_clear_retail,
              basis_clear_retail_uom,
              basis_clear_retail_std,
              basis_clear_mkdn_nbr,
              current_clearance_ind,
              clear_boolean,
              promo_boolean,
              price_change_boolean,
              current_cost,
              current_base_cost,
              current_pricing_cost,
              basis_base_cost,
              basis_pricing_cost,
              pend_cost_boolean,
              cost_alert,
              past_price_chg_date,
              past_cost_chg_date,
              pend_cost_chg_date,
              pend_cost_chg_cost,
              maint_margin_retail,
              maint_margin_retail_uom,
              maint_margin_retail_std,
              maint_margin_cost,
              location_stock,
              location_on_order,
              location_inventory,
              link_code,
              link_code_display_id,
              link_code_desc,
              replenish_ind,
              vat_rate,
              vat_value,
              historical_sales,
              historical_sales_units,
              historical_issues,
              projected_sales,
              seasonal_sales,
              first_received_date,
              last_received_date,
              wks_first_sale,
              wks_of_sales_exp,
              new_item_loc_boolean,
              proposed_retail,
              proposed_retail_uom,
              proposed_retail_std,
              proposed_clear_mkdn_nbr,
              proposed_clear_ind,
              proposed_reset_date,
              proposed_out_of_stock_date,
              action_flag,
              effective_date,
              new_retail,
              new_retail_uom,
              new_retail_std,
              new_multi_unit_ind,
              new_multi_units,
              new_multi_unit_retail,
              new_multi_unit_uom,
              new_clear_mkdn_nbr,
              new_clear_ind,
              new_reset_date,
              new_out_of_stock_date,
              original_effective_date,
              rule_boolean,
              conflict_boolean,
              lock_version,
              supp_currency,
              zone_id)
       select distinct
              il.worksheet_item_loc_data_id,
              il.worksheet_item_data_id,
              il.workspace_rowid,
              il.worksheet_status_id,
              il.proposed_strategy_id,
              il.user_strategy_id,
              i.item,
              i.margin_mkt_basket_code,
              i.comp_mkt_basket_code,
              i.margin_mbc_name,
              i.competitive_mbc_name,
              il.location,
              il.loc_type,
              il.il_primary_supp,
              il.il_primary_cntry,
              retail.current_regular_retail,
              retail.current_regular_retail_uom,
              retail.current_regular_retail_std,
              retail.current_multi_units,
              retail.current_multi_unit_retail,
              retail.current_multi_unit_uom,
              retail.current_clear_retail,
              retail.current_clear_retail_uom,
              retail.current_clear_retail_std,
              retail.basis_regular_retail,
              retail.basis_regular_retail_uom,
              retail.basis_regular_retail_std,
              retail.basis_multi_units,
              retail.basis_multi_unit_retail,
              retail.basis_multi_unit_uom,
              retail.basis_clear_retail,
              retail.basis_clear_retail_uom,
              retail.basis_clear_retail_std,
              retail.basis_clear_mkdn_nbr,
              decode(retail.current_clear_retail, null, 0, 1), -- current_clearance_ind
              retail.clear_boolean,
              retail.promo_boolean,
              retail.price_change_boolean,
              cost.current_cost,
              cost.current_base_cost,
              cost.current_pricing_cost,
              cost.basis_base_cost,
              cost.basis_pricing_cost,
              cost.pend_cost_boolean,
              cost.cost_alert,
              retail.past_price_chg_date,
              cost.past_cost_chg_date,
              cost.pend_cost_chg_date,
              cost.pend_cost_chg_cost,
              retail.maint_margin_retail,
              retail.maint_margin_retail_uom,
              retail.maint_margin_retail_std,
              cost.maint_margin_cost,
              il.location_stock,
              nvl(on_order.location_on_order, il.location_on_order),
              nvl(on_order.location_inventory, il.location_inventory),
              nvl(link_code.link_code, il.link_code),
              nvl(link_code.link_code_display_id, il.link_code_display_id),
              nvl(link_code.link_code_desc, il.link_code_desc),
              nvl(repl.replenish_ind, il.replenish_ind),
              nvl(vat.vat_rate, il.vat_rate),
              nvl(vat.vat_value,nvl(il.vat_value,0)),
              nvl(hist_sales.historical_sales, il.historical_sales),
              nvl(hist_sales.historical_sales_units, il.historical_sales_units),
              nvl(hist_sales.historical_issues, il.historical_issues),
              nvl(proj_sales.projected_sales, il.projected_sales),
              nvl(season_sales.seasonal_sales, il.seasonal_sales),
              il.first_received_date,
              il.last_received_date,
              il.wks_first_sale,
              il.wks_of_sales_exp,
              decode(item_loc_inds.rpm_ind, null, il.new_item_loc_boolean, 'N', 1, 0),
              nvl(proposed.proposed_retail, il.proposed_retail),
              nvl(proposed.proposed_retail_uom, il.proposed_retail_uom),
              nvl(proposed.proposed_retail_std, il.proposed_retail_std),
              nvl(proposed.proposed_clear_mkdn_nbr, il.proposed_clear_mkdn_nbr),
              nvl(proposed.proposed_clear_ind, il.proposed_clear_ind),
              nvl(clear_dates.proposed_reset_date, il.proposed_reset_date),
              nvl(clear_dates.proposed_out_of_stock_date, il.proposed_out_of_stock_date),
              il.action_flag,
              cost.effective_date,
              nvl(proposed.proposed_retail, il.new_retail),
              nvl(proposed.proposed_retail_uom, il.new_retail_uom),
              nvl(proposed.proposed_retail_std, il.new_retail_std),
              il.new_multi_unit_ind,
              il.new_multi_units,
              il.new_multi_unit_retail,
              il.new_multi_unit_uom,
              nvl(proposed.proposed_clear_mkdn_nbr, il.new_clear_mkdn_nbr),
              nvl(proposed.proposed_clear_ind, il.new_clear_ind),
              nvl(clear_dates.proposed_reset_date, il.new_reset_date),
              nvl(clear_dates.proposed_out_of_stock_date, il.new_out_of_stock_date),
              cost.original_effective_date,
              il.rule_boolean,
              il.conflict_boolean,
              il.lock_version,
              il.supp_currency,
              i.zone_id
         from rpm_me_item_gtt i,
              rpm_me_item_loc_gtt il,
              rpm_me_il_cost_gtt cost,
              rpm_me_il_retail_gtt retail,
              rpm_me_il_repl_gtt repl,
              rpm_me_il_hist_sales_gtt hist_sales,
              rpm_me_il_proj_sales_gtt proj_sales,
              rpm_me_il_season_sales_gtt season_sales,
              rpm_me_il_link_code_gtt link_code,
              rpm_me_il_clear_dates_gtt clear_dates,
              rpm_me_il_vat_gtt vat,
              rpm_me_il_onorder_gtt on_order,
              rpm_me_proposed_gtt proposed,
              rpm_me_il_status_rpm_ind_gtt item_loc_inds
        where i.item = il.item and i.zone_id = il.zone_id
          and il.item = cost.item               and il.location = cost.location
          and il.item = retail.item             and il.location = retail.location
          and il.item = repl.item(+)            and il.location = repl.location(+)
          and il.item = hist_sales.item(+)      and il.location = hist_sales.location(+)
          and il.item = proj_sales.item(+)      and il.location = proj_sales.location(+)
          and il.item = season_sales.item(+)    and il.location = season_sales.location(+)
          and il.item = link_code.item(+)       and il.location = link_code.location(+)
          and il.item = clear_dates.item(+)     and il.location = clear_dates.location(+)
          and il.item = vat.item(+)             and il.location = vat.location(+)
          and il.item = on_order.item(+)        and il.location = on_order.location(+)
          and il.item = proposed.item(+)        and il.location = proposed.location(+)
          and il.item = item_loc_inds.item(+)   and il.location = item_loc_inds.location(+);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.CONSOLIDATE_GTT',
                                        to_char(SQLCODE));
      return FALSE;

END CONSOLIDATE_GTT;
----------------------------------------------------------------------------------------

FUNCTION CONSOLIDATE_ITEM_LOC(O_error_msg     IN OUT  VARCHAR2,
                              I_zone_id       IN      RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                              I_initial_call  IN      VARCHAR2)
   RETURN BOOLEAN IS

 type L_deleted_item_type is TABLE of rpm_me_consolidate_itemloc_gtt.item%type;
 type L_deleted_loc_type  is TABLE of rpm_me_consolidate_itemloc_gtt.location%type;

 L_deleted_item_tbl L_deleted_item_type;
 L_deleted_loc_tbl L_deleted_loc_type;
BEGIN
   /* This function is only called by the batch program.  It assumes that only one zone
      will be in the rpm_me_item_gtt table at a time */

   -- a) If Worksheet review has not started, set the New_item_loc_boolean flag for all new
   --    locations that have been added to the worksheet and update the item_loc table
   -- b) If Worksheet review period has already started, delete any new locations that have
   --    been added to the items on the worksheet after the review started
   if LP_review_not_started then

      update item_loc il
         set il.rpm_ind = 'Y'
       where (il.item, il.loc) in (
          select gtt.item,
                 gtt.location
            from rpm_me_il_status_rpm_ind_gtt gtt
           where gtt.rpm_ind = 'N');

   else
      delete from rpm_me_consolidate_itemloc_gtt
       where (item, location) in(
                select gtt.item,
                       gtt.location
                  from rpm_me_il_status_rpm_ind_gtt gtt
                 where gtt.rpm_ind = 'N')
      returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

      if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
         forall i in 1..L_deleted_item_tbl.COUNT
         insert into rpm_merch_extract_deletions
                  (strategy_id,
                   delete_date,
                   item,
                   location,
                   reason_code )
         values   (LP_record_item_loc_strategy,
                   LP_vdate,
                   L_deleted_item_tbl(i),
                   L_deleted_loc_tbl(i),
                   NEW_ITEM_LOC);
      end if;
   end if;

   -- Delete the item/loc records from LP_extract_table which do not meet the following conditions:
   --   a) item should have the same Basis UOM at all locations in the zone
   --   b) item should have the same Link Code at all locations in the zone

   delete from rpm_me_consolidate_itemloc_gtt
    where (item) in(select item
                      from (select il.item,
                                   count(distinct basis_regular_retail_uom) cnt
                              from rpm_me_il_retail_gtt il
                             group by il.item) use
                     where use.cnt > 1)
   returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

   if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
      forall i in 1..L_deleted_item_tbl.COUNT
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
        values (LP_record_item_loc_strategy,
                LP_vdate,
                L_deleted_item_tbl(i),
                L_deleted_loc_tbl(i),
                VARIABLE_ZONE_SELLING_UOM);
   end if;

   ---
   delete from rpm_me_consolidate_itemloc_gtt
    where (item) in(select item
                      from (select il.item,
                                   count(distinct nvl(link_code,'fAkElInKcOdE')) cnt
                              from rpm_me_consolidate_itemloc_gtt il
                             group by il.item) use
                     where use.cnt > 1)
   returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

   if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
      forall i in 1..L_deleted_item_tbl.COUNT
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
        values (LP_record_item_loc_strategy,
                LP_vdate,
                L_deleted_item_tbl(i),
                L_deleted_loc_tbl(i),
                VARIABLE_LINK_CODE);
   end if;

   -- Delete the item/loc records from LP_extract_table which do not meet the following conditions:
   --   a) If any one item from a Link code is present in the worksheet, all items in the
   --      same Link code must also be present in the worksheet
   --   b) Items sharing the same Link code should have the same Basis UOM
   --   c) Items sharing the same Link code should all have the same Class Vat Indicator setings
   --   d) For Margin and Maintain Margin Strategies, items sharing the same Link code
   --      should have the same Margin Market Basket code
   --   e) For Competitive Margin Strategies, items sharing the same Link code
   --      should have the same Competitive Market Basket code

   delete from rpm_me_consolidate_itemloc_gtt
    where (link_code) in(select con.link_code
                           from (select /*+ index(r rpm_link_code_attribute_i1) */ r.link_code,
                                        count(*) lca_count
                                   from rpm_link_code_attribute r,
                                        rpm_zone_location zl
                                  where zl.zone_id = I_zone_id
                                    and zl.location = r.location
                                    and r.link_code in (select distinct link_code
                                                          from rpm_me_consolidate_itemloc_gtt
                                                         where link_code is not null)
                                    group by r.link_code) lca,
                                (select link_code,
                                        count(*) con_count
                                   from rpm_me_consolidate_itemloc_gtt
                                  where link_code is not null
                                    group by link_code) con
                          where lca.link_code  = con.link_code
                            and lca.lca_count != con.con_count)
   returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

   if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
      forall i in 1..L_deleted_item_tbl.COUNT
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
        values (LP_record_item_loc_strategy,
                LP_vdate,
                L_deleted_item_tbl(i),
                L_deleted_loc_tbl(i),
                MISSING_LINK_ITEM);
   end if;

   ---
   delete from rpm_me_consolidate_itemloc_gtt
    where (item) in(select item
                      from (select il.item,
                                   count(basis_regular_retail_uom) cnt
                              from rpm_me_consolidate_itemloc_gtt il
                             where il.link_code is not null
                             group by il.item,
                                      il.location) use
                     where use.cnt > 1)
   returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

   if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
      forall i in 1..L_deleted_item_tbl.COUNT
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
        values (LP_record_item_loc_strategy,
                LP_vdate,
                L_deleted_item_tbl(i),
                L_deleted_loc_tbl(i),
                VARIABLE_LINK_SELLING_UOM);
   end if;

   ---
   delete from rpm_me_consolidate_itemloc_gtt
    where (item) in(select item
                      from (select il.item,
                                   il.location,
                                   count(i.retail_include_vat_ind) cnt
                              from rpm_me_consolidate_itemloc_gtt il,
                                   rpm_me_item_gtt i
                             where il.link_code is not null
                               and il.item      = i.item
                             group by il.item,
                                      il.location) use
                      where use.cnt > 1)
   returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

   if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
      forall i in 1..L_deleted_item_tbl.COUNT
      insert into rpm_merch_extract_deletions
               (strategy_id,
                delete_date,
                item,
                location,
                reason_code )
        values (LP_record_item_loc_strategy,
                LP_vdate,
                L_deleted_item_tbl(i),
                L_deleted_loc_tbl(i),
                VARIABLE_LINK_SELLING_UOM);
   end if;

   ---

   if LP_strategy_type in(MAINT_MARGIN,MARGIN) then

      delete from rpm_me_consolidate_itemloc_gtt
       where (item) in(select distinct il2.item
                         from rpm_me_consolidate_itemloc_gtt il2,
                              (select il.link_code,
                                      count(distinct nvl(i.margin_mkt_basket_code,-999)) mbc
                                 from rpm_me_item_gtt i,
                                      rpm_me_consolidate_itemloc_gtt il
                                where i.item = il.item
                                group by il.link_code) use
                        where il2.link_code = use.link_code
                          and use.mbc > 1)
      returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

      if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
         forall i in 1..L_deleted_item_tbl.COUNT
         insert into rpm_merch_extract_deletions
                  (strategy_id,
                   delete_date,
                   item,
                   location,
                   reason_code )
           values (LP_record_item_loc_strategy,
                   LP_vdate,
                   L_deleted_item_tbl(i),
                   L_deleted_loc_tbl(i),
                   VARIABLE_LINK_MBC);
      end if;

   elsif LP_strategy_type = COMPETITIVE then
      delete from rpm_me_consolidate_itemloc_gtt
       where (item) in(select distinct il2.item
                         from rpm_me_consolidate_itemloc_gtt il2,
                              (select il.link_code,
                                      count(distinct nvl(i.comp_mkt_basket_code, -999)) mbc
                                 from rpm_me_item_gtt i,
                                      rpm_me_consolidate_itemloc_gtt il
                                where i.item = il.item
                                group by il.link_code) use
                        where il2.link_code = use.link_code
                          and use.mbc > 1)
      returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

      if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
         forall i in 1..L_deleted_item_tbl.COUNT
         insert into rpm_merch_extract_deletions
                  (strategy_id,
                   delete_date,
                   item,
                   location,
                   reason_code )
           values (LP_record_item_loc_strategy,
                   LP_vdate,
                   L_deleted_item_tbl(i),
                   L_deleted_loc_tbl(i),
                   VARIABLE_LINK_MBC);
      end if;
   end if;

   -- Delete an item from a secondary area which:
   --   a) does not belong to any link code whereas the same item belongs to a link code in
   --      the primary area
   --   b) does not exist in the primary area AND does not belong to any link code in the
   --      secondary area
   --   c) belongs to a link code that does not exist in the primary area

   if I_initial_call = 'N' then

      delete from rpm_me_consolidate_itemloc_gtt
       where (item, location) in(
             select i.item,
                    i.location
               from rpm_me_consolidate_itemloc_gtt i
              where (   (    i.link_code is NULL
                         and (   (exists (select 'x'
                                            from rpm_area_temp_item_loc o
                                           where o.item = i.item
                                             and o.link_code is not NULL))
                              or (not exists (select 'x'
                                                from rpm_area_temp_item_loc o
                                               where o.item = i.item))))
                     or (     i.link_code is not NULL
                         and (i.link_code not in (select distinct o.link_code
                                                    from rpm_area_temp_item_loc o)))))
      returning item, location bulk collect into L_deleted_item_tbl, L_deleted_loc_tbl;

      if LP_record_item_loc_removes and L_deleted_item_tbl.COUNT > 0 then
         forall i in 1..L_deleted_item_tbl.COUNT
         insert into rpm_merch_extract_deletions
                  (strategy_id,
                   delete_date,
                   item,
                   location,
                   reason_code )
           values (LP_record_item_loc_strategy,
                   LP_vdate,
                   L_deleted_item_tbl(i),
                   L_deleted_loc_tbl(i),
                   INVALID_SECONDARY_ITEM);
      end if;
   end if;

   if CONSOLIDATE_ITEM_ROLLUP(O_error_msg) = FALSE then
      return FALSE;
   end if;
   return TRUE;
EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.CONSOLIDATE_ITEM_LOC',
                                        to_char(SQLCODE));
      return FALSE;
END CONSOLIDATE_ITEM_LOC;
----------------------------------------------------------------------------------------

FUNCTION CONSOLIDATE_ITEM_ROLLUP (O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.CONSOLIDATE_ITEM_ROLLUP';

BEGIN

   if UOM_VAT_PKG_CALLS(O_error_msg) = FALSE then
      return FALSE;
   end if;

   update rpm_me_item_gtt gtt
      set (current_zl_cost,
           basis_zl_base_cost,
           basis_zl_pricing_cost,
           pend_zl_cost_chg_cost,
           maint_margin_zl_cost,
           original_retail,
           current_zl_regular_retail,
           current_zl_regular_retail_uom,
           current_zl_regular_retail_std,
           basis_zl_regular_retail,
           basis_zl_regular_retail_uom,
           basis_zl_regular_retail_std,
           maint_margin_zl_retail,
           maint_margin_zl_retail_uom,
           maint_margin_zl_retail_std,
           current_zl_clear_retail,
           current_zl_clear_retail_uom,
           current_zl_clear_retail_std,
           basis_zl_clear_retail,
           basis_zl_clear_retail_uom,
           basis_zl_clear_retail_std,
           current_zl_multi_units,
           current_zl_multi_unit_retail,
           current_zl_multi_uom,
           basis_zl_multi_units,
           basis_zl_multi_unit_retail,
           basis_zl_multi_uom,
           basis_zl_clear_mkdn_nbr,
           proposed_zl_out_of_stock_date,
           proposed_zl_reset_date) = (select DECODE(LP_cost_calc_method,
                                                    MAX_COST, MAX(con.current_cost),
                                                    AVG_COST, AVG(con.current_cost)),
                                             DECODE(LP_cost_calc_method,
                                                    MAX_COST, MAX(con.basis_base_cost),
                                                    AVG_COST, AVG(con.basis_base_cost)),
                                             DECODE(LP_cost_calc_method,
                                                    MAX_COST, MAX(con.basis_pricing_cost),
                                                    AVG_COST, AVG(con.basis_pricing_cost)),
                                             DECODE(LP_cost_calc_method,
                                                    MAX_COST, MAX(con.pend_cost_chg_cost),
                                                    AVG_COST, AVG(con.pend_cost_chg_cost)),
                                             DECODE(LP_cost_calc_method,
                                                    MAX_COST, MAX(con.maint_margin_cost),
                                                    AVG_COST, AVG(con.maint_margin_cost)),
                                             NVL(AVG(con.basis_clear_retail),AVG(con.basis_regular_retail)),
                                             NVL(gtt.current_zl_regular_retail, AVG(con.current_regular_retail)),
                                             NVL(gtt.current_zl_regular_retail_uom, MIN(con.current_regular_retail_uom)),
                                             NVL(gtt.current_zl_regular_retail_std, AVG(con.current_regular_retail_std)),
                                             NVL(gtt.basis_zl_regular_retail, AVG(con.basis_regular_retail)),
                                             NVL(gtt.basis_zl_regular_retail_uom, MIN(con.basis_regular_retail_uom)),
                                             NVL(gtt.basis_zl_regular_retail_std, AVG(con.basis_regular_retail_std)),
                                             NVL(gtt.maint_margin_zl_retail, AVG(con.maint_margin_retail)),
                                             NVL(gtt.maint_margin_zl_retail_uom, MIN(con.maint_margin_retail_uom)),
                                             NVL(gtt.maint_margin_zl_retail_std, AVG(con.maint_margin_retail_std)),
                                             NVL(gtt.current_zl_clear_retail, AVG(con.current_clear_retail)),
                                             NVL(gtt.current_zl_clear_retail_uom, MIN(con.current_clear_retail_uom)),
                                             NVL(gtt.current_zl_clear_retail_std, AVG(con.current_clear_retail_std)),
                                             NVL(gtt.basis_zl_clear_retail, AVG(NVL(con.basis_clear_retail, con.basis_regular_retail))),
                                             NVL(gtt.basis_zl_clear_retail_uom, MIN(con.basis_clear_retail_uom)),
                                             NVL(gtt.basis_zl_clear_retail_std, AVG(NVL(con.basis_clear_retail_std, con.basis_regular_retail_std))),
                                             NVL(current_zl_multi_units, MIN(current_multi_units)),
                                             NVL(current_zl_multi_unit_retail, MIN(current_multi_unit_retail)),
                                             NVL(current_zl_multi_uom, MIN(current_multi_unit_uom)),
                                             NVL(basis_zl_multi_units, MIN(basis_multi_units)),
                                             NVL(basis_zl_multi_unit_retail, MIN(basis_multi_unit_retail)),
                                             NVL(basis_zl_multi_uom, MIN(basis_multi_unit_uom)),
                                             NVL(basis_zl_clear_mkdn_nbr, MIN(basis_clear_mkdn_nbr)),
                                             NVL(proposed_zl_out_of_stock_date, MIN(proposed_out_of_stock_date)),
                                             NVL(proposed_zl_reset_date, MIN(proposed_reset_date))
                                        from rpm_me_consolidate_itemloc_gtt con
                                       where con.item    = gtt.item
                                         and con.zone_id = gtt.zone_id
                                        group by con.zone_id,
                                                 NVL(con.link_code, con.item))
    where EXISTS (select 'x'
                    from rpm_me_consolidate_itemloc_gtt con
                   where gtt.item    = con.item
                     and gtt.zone_id = con.zone_id);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END CONSOLIDATE_ITEM_ROLLUP;

----------------------------------------------------------------------------------------

FUNCTION POPULATE_PROPOSED(O_error_msg    IN OUT VARCHAR2,
                           I_zone_id      IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                           I_initial_call IN     VARCHAR2)
RETURN BOOLEAN IS

   L_program            VARCHAR2(40)         :='RPM_EXT_SQL.POPULATE_PROPOSED';
   AREA_DIFF_STRATEGY   CONSTANT VARCHAR2(2) := 'AD';
   L_strategy_type      VARCHAR2(20)         := NULL;
   L_round              NUMBER               := NULL;

   L_price_guide_id     RPM_PRICE_GUIDE.PRICE_GUIDE_ID%TYPE  := NULL;
   L_rsmm_rec           RPM_STRATEGY_MAINT_MARGIN%ROWTYPE    := NULL;
   L_rsc_rec            RPM_STRATEGY_COMPETITIVE%ROWTYPE     := NULL;
   L_default_tax_type   SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE := NULL;
   L_primary_zone_currency   RPM_ZONE.CURRENCY_CODE%TYPE           := NULL;
   L_secondary_zone_currency RPM_ZONE.CURRENCY_CODE%TYPE           := NULL;

BEGIN

   -- This function is only called by the batch program.  It assumes that only one zone
   -- will be in the rpm_me_item_gtt table at a time

   if I_initial_call = 'N' and
      LP_dynamic_area_diff_ind = 0 then
      return TRUE;
   end if;

   if I_initial_call = 'Y' then
      L_strategy_type := LP_strategy_type;
   else
      L_strategy_type := AREA_DIFF_STRATEGY;
   end if;

   select c.currency_rtl_dec into L_round
     from rpm_zone rz,
          currencies c
    where c.currency_code = rz.currency_code
      and rz.zone_id      = I_zone_id;

   select price_guide_id into L_price_guide_id
     from (select price_guide_id
             from rpm_strategy
            where strategy_id    = LP_record_item_loc_strategy
              and I_initial_call = 'Y'
           union all
           select price_guide_id
             from rpm_area_diff
            where area_diff_id   = LP_area_diff_id
              and I_initial_call = 'N');

   if SYSTEM_OPTIONS_SQL.GET_DEFAULT_TAX_TYPE(O_error_msg,
                                              L_default_tax_type) = FALSE then
      return FALSE;
   end if;

   if L_strategy_type = MAINT_MARGIN then

   select strategy_id,
          maint_margin_method,
          auto_approve,
          cost_forward_days,
          increase,
          decrease,
          lock_version into L_rsmm_rec
     from rpm_strategy_maint_margin
    where strategy_id = LP_record_item_loc_strategy;

      if L_rsmm_rec.maint_margin_method = 'M' then
         if L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then
            insert into rpm_me_proposed_gtt(item,
                                            location,
                                            proposed_retail,
                                            proposed_retail_uom,
                                            proposed_clear_ind)
            select use.item,
                   use.location,
                   use.proposed_retail,
                   use.basis_regular_retail_uom,
                   0
              from (select rmcilg.item,
                           rmcilg.location,
                           ROUND(DECODE(rmig.markup_calc_type,
                                        RETAIL_MARKUP, (NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) / (1 - DECODE(rsd.percent,
                                                                                                                               100, 99.9999,
                                                                                                                               rsd.percent) / 100)) /
                                           (1 - DECODE(rmig.retail_include_vat_ind,
                                                       'N', 0,
                                                       NVL(rmcilg.vat_rate, 0)) / 100) + DECODE(rmig.retail_include_vat_ind,
                                                                                                'N', 0,
                                                                                                rmcilg.vat_value),
                                        ---
                                        (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) * (1 + rsd.percent / 100)) /
                                           (1 - DECODE(rmig.retail_include_vat_ind,
                                                       'N', 0,
                                                       NVL(rmcilg.vat_rate, 0)) / 100) + DECODE(rmig.retail_include_vat_ind,
                                                                                                'N', 0,
                                                                                                rmcilg.vat_value)),
                                 L_round) proposed_retail,
                           ---
                           DECODE(rmig.markup_calc_type,
                                  RETAIL_MARKUP,
                                  (1 - (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)/
                                        ((rmig.basis_zl_regular_retail - ((rmig.basis_zl_regular_retail - rmcilg.vat_value)*
                                          DECODE(rmig.retail_include_vat_ind,
                                                 'N', 0,
                                                 NVL(rmcilg.vat_rate, 0))) - rmcilg.vat_value)))),
                                  ---
                                  ((((rmig.basis_zl_regular_retail - ((rmig.basis_zl_regular_retail - rmcilg.vat_value)*
                                  DECODE(rmig.retail_include_vat_ind,
                                         'N', 0,
                                         NVL(rmcilg.vat_rate, 0))) - rmcilg.vat_value))/
                                     NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)) - 1)) basis_markup,
                           rsd.from_percent,
                           rsd.to_percent,
                           rmcilg.basis_regular_retail_uom,
                           rmcilg.current_regular_retail
                      from rpm_strategy_detail rsd,
                           rpm_me_item_gtt rmig,
                           rpm_me_consolidate_itemloc_gtt rmcilg
                     where rmcilg.item                                              = rmig.item
                       and NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) != 0
                       and rmig.basis_zl_regular_retail                            != 0
                       and NVL(rmig.margin_mkt_basket_code, -999)                   = NVL(rsd.mkt_basket_code, -999)
                       and rsd.strategy_id                                          = LP_record_item_loc_strategy) use
            where (   (    use.to_percent is NOT NULL
                       and use.basis_markup * 100 NOT between use.from_percent and use.to_percent)
                   or use.to_percent              is NULL)
              and use.current_regular_retail != use.proposed_retail;
         else
            insert into rpm_me_proposed_gtt(item,
                                            location,
                                            proposed_retail,
                                            proposed_retail_uom,
                                            proposed_clear_ind)
            select use.item,
                   use.location,
                   use.proposed_retail,
                   use.basis_regular_retail_uom,
                   0
              from (select rmcilg.item,
                           rmcilg.location,
                           ROUND(DECODE(rmig.markup_calc_type,
                                        RETAIL_MARKUP, NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) / (1 - DECODE(rsd.percent,
                                                                                                                                         100, 99.9999,
                                                                                                                                         rsd.percent) / 100) *
                                                                   (1 + DECODE(rmig.retail_include_vat_ind,
                                                                               'N', 0,
                                                                               NVL(rmcilg.vat_rate, 0)) / 100),
                                        ---
                                        NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) * (1 + rsd.percent / 100) *
                                           (1 + DECODE(rmig.retail_include_vat_ind,
                                                       'N', 0,
                                                       NVL(rmcilg.vat_rate, 0)) / 100)),
                                 L_round) proposed_retail,
                           ---
                           DECODE(rmig.markup_calc_type,
                                  RETAIL_MARKUP,
                                  (1 - (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)/(rmig.basis_zl_regular_retail /
                                           (1 + DECODE(rmig.retail_include_vat_ind,
                                                       'N', 0,
                                                       NVL(rmcilg.vat_rate, 0)))))),
                                  (((rmig.basis_zl_regular_retail /
                                      (1 + DECODE(rmig.retail_include_vat_ind,
                                                  'N', 0,
                                                  NVL(rmcilg.vat_rate, 0))))/
                                           NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost)) - 1)) basis_markup,
                           rsd.from_percent,
                           rsd.to_percent,
                           rmcilg.basis_regular_retail_uom,
                           rmcilg.current_regular_retail
                      from rpm_strategy_detail rsd,
                           rpm_me_item_gtt rmig,
                           rpm_me_consolidate_itemloc_gtt rmcilg
                     where rmcilg.item                                              = rmig.item
                       and NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) != 0
                       and rmig.basis_zl_regular_retail                            != 0
                       and NVL(rmig.margin_mkt_basket_code, -999)                   = NVL(rsd.mkt_basket_code, -999)
                       and rsd.strategy_id                                          = LP_record_item_loc_strategy) use
            where (   (    use.to_percent         is NOT NULL
                       and use.basis_markup * 100 NOT between use.from_percent and use.to_percent)
                   or use.to_percent is NULL)
              and use.current_regular_retail != use.proposed_retail;
         end if;

      else
         if L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then

            insert into rpm_me_proposed_gtt(item,
                                            location,
                                            proposed_retail,
                                            proposed_retail_uom,
                                            proposed_clear_ind)
            select rmcilg.item,
                   rmcilg.location,
                   ---
                   ROUND(DECODE(rsmm.increase,
                                'M', DECODE(rmig.markup_calc_type,
                                            RETAIL_MARKUP,(NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) /
                                                           (1 - (DECODE(0,
                                                                        NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                                        NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                                        (1 - rmig.maint_margin_zl_cost / (DECODE(rmig.retail_include_vat_ind,
                                                                                                                 'N', rmig.maint_margin_zl_retail_std,
                                                                                                                 (rmig.maint_margin_zl_retail_std - ((rmig.maint_margin_zl_retail_std-rmcilg.vat_value) *
                                                                                                                  rmcilg.vat_rate/100) - rmcilg.vat_value))))))) / (1 - DECODE(rmig.retail_include_vat_ind,
                                                                                                                                                                               'N', 0,
                                                                                                                                                                               NVL(rmcilg.vat_rate, 0)) / 100) + DECODE(rmig.retail_include_vat_ind,
                                                                                                                                                                                                                        'N', 0,
                                                                                                                                                                                                                        rmcilg.vat_value)),
                                            (NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) * (1 + (DECODE(0,
                                                                                                                     NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                                                                                     NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                                                                        (DECODE(rmig.retail_include_vat_ind,
                                                                                                                'N', rmig.maint_margin_zl_retail_std,
                                                                                                                (rmig.maint_margin_zl_retail_std - ((rmig.maint_margin_zl_retail_std - rmcilg.vat_value) *
                                                                                                                (rmcilg.vat_rate/100)) - rmcilg.vat_value)) /
                                                                                                        rmig.maint_margin_zl_cost - 1)))) /
                                            (1 - DECODE(rmig.retail_include_vat_ind,
                                                        'N', 0,
                                                        NVL(rmcilg.vat_rate, 0)) / 100) + DECODE(rmig.retail_include_vat_ind,
                                                                                                 'N', 0,
                                                                                                 rmcilg.vat_value))),
                               rmig.maint_margin_zl_retail_std + ((NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) - rmig.maint_margin_zl_cost)) /
                               (1 - DECODE(rmig.retail_include_vat_ind,
                                           'N', 0,
                                           NVL(rmcilg.vat_rate, 0)) / 100) + DECODE(rmig.retail_include_vat_ind,
                                                                                    'N', 0,
                                                                                    rmcilg.vat_value)),
                   L_round),
                   ---
                   rmcilg.basis_regular_retail_uom,
                   0
              from rpm_strategy_maint_margin rsmm,
                   rpm_me_item_gtt rmig,
                   rpm_me_consolidate_itemloc_gtt rmcilg
             where rmcilg.item                  = rmig.item
               and rmig.maint_margin_zl_cost   <= NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)
               and rmig.maint_margin_zl_cost   != 0
               and rmig.maint_margin_zl_retail != 0
               and rsmm.strategy_id             = LP_record_item_loc_strategy
            union all
            select rmcilg.item,
                   rmcilg.location,
                   ROUND(DECODE(rsmm.decrease,
                                'M', DECODE(rmig.markup_calc_type,
                                            RETAIL_MARKUP, (NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) /
                                                            (1 - (DECODE(0,
                                                                         NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                                         NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                                         (1 - rmig.maint_margin_zl_cost / (DECODE(rmig.retail_include_vat_ind,
                                                                                                                  'N', rmig.maint_margin_zl_retail_std,
                                                                                                                  (rmig.maint_margin_zl_retail_std - (rmig.maint_margin_zl_retail_std - rmcilg.vat_value) *
                                                                                                                  (rmcilg.vat_rate/100) - rmcilg.vat_value))))))) /
                                                                                                                  (1 - DECODE(rmig.retail_include_vat_ind,
                                                                                                                              'N', 0,
                                                                                                                              NVL(rmcilg.vat_rate, 0)) / 100) +
                                                                                                                   DECODE(rmig.retail_include_vat_ind,
                                                                                                                          'N',0,
                                                                                                                          rmcilg.vat_value)),
                                           (NVL(rmig.basis_zl_pricing_cost,
                                                rmig.basis_zl_base_cost) *
                                                (1 + (DECODE(0,
                                                             NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                             NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                             (DECODE(rmig.retail_include_vat_ind,
                                                                     'N', rmig.maint_margin_zl_retail_std,
                                                                     (rmig.maint_margin_zl_retail_std - (rmig.maint_margin_zl_retail_std - rmcilg.vat_value) *
                                                                     (rmcilg.vat_rate/100) - rmcilg.vat_value)) /
                                                              rmig.maint_margin_zl_cost - 1)))) /
                                               (1 - DECODE(rmig.retail_include_vat_ind,
                                                           'N', 0,
                                                           NVL(rmcilg.vat_rate, 0)) / 100) + DECODE(rmig.retail_include_vat_ind,
                                                                                                    'N',0,
                                                                                                    rmcilg.vat_value))),
                               rmig.maint_margin_zl_retail_std + (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) - rmig.maint_margin_zl_cost)),
                               L_round),
                   --
                   rmcilg.basis_regular_retail_uom,
                   0
              from rpm_strategy_maint_margin rsmm,
                   rpm_me_item_gtt rmig,
                   rpm_me_consolidate_itemloc_gtt rmcilg
             where rmcilg.item                  = rmig.item
               and rmig.maint_margin_zl_cost    > NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)
               and rmig.maint_margin_zl_cost   != 0
               and rmig.maint_margin_zl_retail != 0
               and rsmm.strategy_id             = LP_record_item_loc_strategy;
         else
            insert into rpm_me_proposed_gtt(item,
                                            location,
                                            proposed_retail,
                                            proposed_retail_uom,
                                            proposed_clear_ind)
            select rmcilg.item,
                   rmcilg.location,
                   ROUND(DECODE(rsmm.increase,
                                'M', DECODE(rmig.markup_calc_type,
                                            RETAIL_MARKUP, (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) /
                                                           (1 - (DECODE(0,
                                                                        NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                                        NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                                        (1 - rmig.maint_margin_zl_cost / (DECODE(rmig.retail_include_vat_ind,
                                                                                                                 'N', rmig.maint_margin_zl_retail_std,
                                                                                                                 rmig.maint_margin_zl_retail_std /
                                                                                                                 (1 + NVL(rmcilg.vat_rate, 0) / 100))))))) *
                                                           (1 + DECODE(rmig.retail_include_vat_ind,
                                                                       'N', 0,
                                                                       NVL(rmcilg.vat_rate, 0)) / 100)),
                                           (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) *
                                           (1 + (DECODE(0, NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                           NVL(rmig.maint_margin_zl_cost, 0), 0,
                                           (DECODE(rmig.retail_include_vat_ind,
                                                   'N', rmig.maint_margin_zl_retail_std,
                                                   rmig.maint_margin_zl_retail_std /
                                                   (1 + NVL(rmcilg.vat_rate, 0) / 100)) /
                                                   rmig.maint_margin_zl_cost - 1)))) *
                                           (1 + DECODE(rmig.retail_include_vat_ind,
                                                       'N', 0,
                                                       NVL(rmcilg.vat_rate, 0)) / 100))),
                               rmig.maint_margin_zl_retail_std + ((NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) - rmig.maint_margin_zl_cost)) *
                               (1 + DECODE(rmig.retail_include_vat_ind,
                                           'N', 0,
                                           NVL(rmcilg.vat_rate, 0)) / 100)),
                         L_round),
                   ---
                   rmcilg.basis_regular_retail_uom,
                   0
              from rpm_strategy_maint_margin rsmm,
                   rpm_me_item_gtt rmig,
                   rpm_me_consolidate_itemloc_gtt rmcilg
             where rmcilg.item                  = rmig.item
               and rmig.maint_margin_zl_cost   <= NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost)
               and rmig.maint_margin_zl_cost   != 0
               and rmig.maint_margin_zl_retail != 0
               and rsmm.strategy_id             = LP_record_item_loc_strategy
            union all
            select rmcilg.item,
                   rmcilg.location,
                   ---
                   ROUND(DECODE(rsmm.decrease,
                                'M', DECODE(rmig.markup_calc_type,
                                            RETAIL_MARKUP, (NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) /
                                                           (1 - (DECODE(0,
                                                                        NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                                        NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                                        (1 - rmig.maint_margin_zl_cost /
                                                                        (DECODE(rmig.retail_include_vat_ind,
                                                                                'N', rmig.maint_margin_zl_retail_std,
                                                                                rmig.maint_margin_zl_retail_std /
                                                                                (1 + NVL(rmcilg.vat_rate, 0) / 100))))))) *
                                                           (1 + DECODE(rmig.retail_include_vat_ind,
                                                                      'N', 0,
                                                                      NVL(rmcilg.vat_rate, 0)) / 100)),
                                           (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) *
                                           (1 + (DECODE(0,
                                                        NVL(rmig.maint_margin_zl_retail_std, 0), 0,
                                                        NVL(rmig.maint_margin_zl_cost, 0), 0,
                                                        (DECODE(rmig.retail_include_vat_ind,
                                                                'N', rmig.maint_margin_zl_retail_std,
                                                                rmig.maint_margin_zl_retail_std /
                                                               (1 + NVL(rmcilg.vat_rate, 0) / 100)) /
                                                        rmig.maint_margin_zl_cost - 1)))) *
                                                        (1 + DECODE(rmig.retail_include_vat_ind,
                                                        'N', 0,
                                                        NVL(rmcilg.vat_rate, 0)) / 100))),
                                rmig.maint_margin_zl_retail_std +
                                (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) - rmig.maint_margin_zl_cost)),
                        L_round),
                   ---
                   rmcilg.basis_regular_retail_uom,
                   0
              from rpm_strategy_maint_margin rsmm,
                   rpm_me_item_gtt rmig,
                   rpm_me_consolidate_itemloc_gtt rmcilg
             where rmcilg.item                  = rmig.item
               and rmig.maint_margin_zl_cost    > NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost)
               and rmig.maint_margin_zl_cost   != 0
               and rmig.maint_margin_zl_retail != 0
               and rsmm.strategy_id             = LP_record_item_loc_strategy;

         end if;

      end if;

   elsif L_strategy_type = MARGIN then

      if L_default_tax_type = RPM_CONSTANTS.GTAX_TAX_TYPE then

         insert into rpm_me_proposed_gtt(item,
                                         location,
                                         proposed_retail,
                                         proposed_retail_uom,
                                         proposed_clear_ind)
         select use.item,
                use.location,
                use.proposed_retail,
                use.basis_regular_retail_uom,
                0
           from (select rmcilg.item,
                        rmcilg.location,
                        ---
                        ROUND(DECODE(rmig.markup_calc_type,
                                     RETAIL_MARKUP, NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) /
                                                    (1 - DECODE(rsd.percent,
                                                                100, 99.9999,
                                                                rsd.percent) / 100) /
                                                                (1 - DECODE(rmig.retail_include_vat_ind,
                                                                           'N', 0,
                                                                           NVL(rmcilg.vat_rate, 0)) / 100) +
                                                                 DECODE(rmig.retail_include_vat_ind,
                                                                        'N', 0,
                                                                        rmcilg.vat_value),
                                     NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) * (1 + rsd.percent / 100) /
                                        (1 - DECODE(rmig.retail_include_vat_ind,
                                                   'N', 0,
                                                   NVL(rmcilg.vat_rate, 0)) / 100) +
                                        DECODE(rmig.retail_include_vat_ind,
                                               'N', 0,
                                               rmcilg.vat_value)),
                              L_round) proposed_retail,
                        ---
                        DECODE(rmig.markup_calc_type,
                               RETAIL_MARKUP, (1 - (NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) /
                                              (rmig.basis_zl_regular_retail - (rmig.basis_zl_regular_retail - rmcilg.vat_value) *
                                              (rmcilg.vat_rate/100) - rmcilg.vat_value))),
                              (((rmig.basis_zl_regular_retail - (rmig.basis_zl_regular_retail - rmcilg.vat_value) *
                              (rmcilg.vat_rate/100) - rmcilg.vat_value) /
                              NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost)) - 1)) basis_markup,
                        rsd.from_percent,
                        rsd.to_percent,
                        rmcilg.basis_regular_retail_uom,
                        rmcilg.current_regular_retail
                   from rpm_strategy_detail rsd,
                        rpm_me_item_gtt rmig,
                        rpm_me_consolidate_itemloc_gtt rmcilg
                  where rmcilg.item                                              = rmig.item
                    and NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) != 0
                    and rmig.basis_zl_regular_retail                            != 0
                    and NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)  is NOT NULL
                    and NVL(rmig.margin_mkt_basket_code, -999)                   = NVL(rsd.mkt_basket_code, -999)
                    and rsd.strategy_id = LP_record_item_loc_strategy) use
            where (   (    use.to_percent is NOT NULL
                       and use.basis_markup * 100 NOT between use.from_percent and use.to_percent)
                   or use.to_percent is NULL)
              and use.current_regular_retail != use.proposed_retail;
      else
         insert into rpm_me_proposed_gtt(item,
                                         location,
                                         proposed_retail,
                                         proposed_retail_uom,
                                         proposed_clear_ind)
         select use.item,
                use.location,
                use.proposed_retail,
                use.basis_regular_retail_uom,
                0
           from (select rmcilg.item,
                        rmcilg.location,
                        ROUND(DECODE(rmig.markup_calc_type,
                                     RETAIL_MARKUP, NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) /
                                                    (1 - DECODE(rsd.percent,
                                                                100, 99.9999,
                                                                rsd.percent) / 100) *
                                    (1 + DECODE(rmig.retail_include_vat_ind,
                                                'N', 0,
                                                NVL(rmcilg.vat_rate, 0)) / 100),
                                     NVL(rmig.basis_zl_pricing_cost, rmig.basis_zl_base_cost) *
                                        (1 + rsd.percent / 100) *
                                        (1 + DECODE(rmig.retail_include_vat_ind,
                                                    'N', 0,
                                                    NVL(rmcilg.vat_rate, 0)) / 100)),
                              L_round) proposed_retail,
                        DECODE(rmig.markup_calc_type,
                               RETAIL_MARKUP,
                               (1 - (NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) /
                                    (rmig.basis_zl_regular_retail /
                                    (1 + DECODE(rmig.retail_include_vat_ind,
                                                'N', 0,
                                                NVL(rmcilg.vat_rate, 0)))))),
                               (((rmig.basis_zl_regular_retail /
                                  (1 + DECODE(rmig.retail_include_vat_ind,
                                              'N', 0, NVL(rmcilg.vat_rate, 0))))/
                                  NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost)) - 1)) basis_markup,
                        rsd.from_percent,
                        rsd.to_percent,
                        rmcilg.basis_regular_retail_uom,
                        rmcilg.current_regular_retail
                   from rpm_strategy_detail rsd,
                        rpm_me_item_gtt rmig,
                        rpm_me_consolidate_itemloc_gtt rmcilg
                  where rmcilg.item                                              = rmig.item
                    and NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) != 0
                    and rmig.basis_zl_regular_retail                            != 0
                    and NVL(rmig.basis_zl_pricing_cost,rmig.basis_zl_base_cost) is NOT NULL
                    and NVL(rmig.margin_mkt_basket_code, -999) = NVL(rsd.mkt_basket_code, -999)
                    and rsd.strategy_id = LP_record_item_loc_strategy) use
            where (   (    use.to_percent is NOT NULL
                       and use.basis_markup * 100 NOT between use.from_percent and use.to_percent)
                   or use.to_percent is NULL)
              and use.current_regular_retail != use.proposed_retail;

      end if;

   elsif L_strategy_type = CLEARANCE then

      insert into rpm_me_proposed_gtt(item,
                                      location,
                                      proposed_retail,
                                      proposed_retail_uom,
                                      proposed_clear_ind,
                                      proposed_clear_mkdn_nbr)
      select rmcilg.item,
             rmcilg.location,
             ROUND(DECODE(rsc.markdown_basis,
                          1, NVL(DECODE(rmcilg.current_clearance_ind,
                                        1, rmig.basis_zl_clear_retail,
                                        rmig.basis_zl_regular_retail),
                                 rmig.basis_zl_regular_retail),
                          rmig.basis_zl_regular_retail) * (1 - rscm1.mkdn_percent / 100),
             L_round),
             DECODE(rsc.markdown_basis, 1,
             NVL(rmig.basis_zl_clear_retail_uom, rmig.basis_zl_regular_retail_uom),
             rmig.basis_zl_regular_retail_uom),
             1,
             rscm1.mkdn_number + 1
        from rpm_me_item_gtt rmig,
             rpm_me_consolidate_itemloc_gtt rmcilg,
             rpm_strategy_clearance rsc,
             rpm_strategy_clearance_mkdn rscm1,
             (  select count(*) mkdn_count,
                       strategy_id
                  from rpm_strategy_clearance_mkdn
                 where strategy_id = LP_record_item_loc_strategy
              group by strategy_id) rscm2
       where rmig.item           = rmcilg.item
         and rsc.strategy_id     = rscm2.strategy_id
         and rscm1.mkdn_number   = NVL(rmcilg.basis_clear_mkdn_nbr, 0)
         and rscm1.strategy_id   = rscm2.strategy_id
         and rscm2.mkdn_count    > NVL(rmcilg.basis_clear_mkdn_nbr, 0)
         and rmcilg.rule_boolean = 1;

   elsif L_strategy_type = COMPETITIVE then

   select strategy_id,
          compete_type,
          comp_store,
          lock_version into L_rsc_rec
     from rpm_strategy_competitive
    where strategy_id = LP_record_item_loc_strategy;

      if L_rsc_rec.compete_type IN (COMP_MATCH, 3) then

         insert into rpm_me_proposed_gtt(item,
                                         location,
                                         proposed_retail,
                                         proposed_retail_uom,
                                         proposed_clear_ind)
         select rmcilg.item,
                rmcilg.location,
                ROUND(rmig.primary_comp_retail, L_round),
                rmcilg.basis_regular_retail_uom,
                0
           from rpm_strategy_competitive rsc,
                rpm_strategy_detail rsd,
                rpm_me_item_gtt rmig,
                rpm_me_consolidate_itemloc_gtt rmcilg
          where rmcilg.item = rmig.item
            and NVL(rmig.primary_comp_retail, rmig.basis_zl_regular_retail) != rmig.basis_zl_regular_retail
            and rsd.compete_type                                             = COMP_MATCH
            and DECODE(rsc.compete_type,
                       3, NVL(rmig.comp_mkt_basket_code, -999),
                       -999)                                                 = DECODE(rsc.compete_type,
                                                                                      3, NVL(rsd.mkt_basket_code, -999),
                                                                                      -999)
            and rsd.strategy_id                                              = rsc.strategy_id
            and rsc.strategy_id                                              = LP_record_item_loc_strategy;

      end if;

      if L_rsc_rec.compete_type IN (COMP_PRICE_BELOW,
                                    COMP_PRICE_ABOVE,
                                    3) then

         insert into rpm_me_proposed_gtt(item,
                                         location,
                                         proposed_retail,
                                         proposed_retail_uom,
                                         proposed_clear_ind)
         select item,
                location,
                ROUND(proposed_retail, L_round),
                basis_regular_retail_uom,
                0
           from (   select distinct rmcilg.item,
                           rmcilg.location,
                           rmig.primary_comp_retail * ( 1 + rsd.percent / 100) proposed_retail,
                           rmcilg.basis_regular_retail_uom
                      from rpm_strategy_competitive rsc,
                           rpm_strategy_detail rsd,
                           rpm_me_item_gtt rmig,
                           rpm_me_consolidate_itemloc_gtt rmcilg
                     where rmcilg.item     = rmig.item
                       and (   rsd.to_percent is NULL
                            or NOT rmig.basis_zl_regular_retail between
                                      rmig.primary_comp_retail * ( 1 + rsd.from_percent / 100) and
                                      rmig.primary_comp_retail * ( 1 + rsd.to_percent / 100))
                       and rmig.primary_comp_retail is NOT NULL
                       and rsd.compete_type = COMP_PRICE_ABOVE
                       and DECODE(rsc.compete_type,
                                  3, NVL(rmig.comp_mkt_basket_code, -999),
                                  -999)    = DECODE(rsc.compete_type,
                                                    3, NVL(rsd.mkt_basket_code, -999),
                                                    -999)
                       and rsd.strategy_id = rsc.strategy_id
                       and rsc.strategy_id = LP_record_item_loc_strategy
                    union all
                    select distinct rmcilg.item,
                           rmcilg.location,
                           --
                           rmig.primary_comp_retail * ( 1 - rsd.percent / 100) proposed_retail,
                           rmcilg.basis_regular_retail_uom
                      from rpm_strategy_competitive rsc,
                           rpm_strategy_detail rsd,
                           rpm_me_item_gtt rmig,
                           rpm_me_consolidate_itemloc_gtt rmcilg
                     where rmcilg.item         = rmig.item
                       and (   rsd.to_percent is NULL
                            or NOT rmig.basis_zl_regular_retail between
                                      rmig.primary_comp_retail * ( 1 - rsd.to_percent / 100) and
                                      rmig.primary_comp_retail * ( 1 - rsd.from_percent / 100))
                       and rmig.primary_comp_retail is NOT NULL
                       and rsd.compete_type    = COMP_PRICE_BELOW
                       and DECODE(rsc.compete_type,
                                  3, NVL(rmig.comp_mkt_basket_code,
                                  -999), -999) = DECODE(rsc.compete_type,
                                                        3, NVL(rsd.mkt_basket_code, -999),
                                                        -999)
                       and rsd.strategy_id     = rsc.strategy_id
                       and rsc.strategy_id     = LP_record_item_loc_strategy);

      end if;

   elsif L_strategy_type = AREA_DIFF_STRATEGY then
   
     select rz1.currency_code primary_zone_currency,
            rz2.currency_code secondary_zone_currency into  L_primary_zone_currency,
                                                            L_secondary_zone_currency
       from rpm_area_diff_prim radp,
            rpm_zone rz1,
            rpm_zone rz2
      where radp.area_diff_prim_id  = LP_area_diff_prim_id
        and rz1.zone_id             = radp.zone_hier_id
        and rz2.zone_id             = I_zone_id;

      insert into rpm_me_proposed_gtt(item,
                                      location,
                                      proposed_retail,
                                      proposed_retail_uom,
                                      proposed_clear_ind)
      select t.item,
             t.location,
             case
                when L_primary_zone_currency = L_secondary_zone_currency then
                   t.proposed_retail
                else
                   ROUND(CURRENCY_CONVERT_VALUE (L_secondary_zone_currency,
                                           t.proposed_retail,
                                           L_primary_zone_currency),
	                    L_round)
             end proposed_retail,
             t.proposed_retail_uom,
             t.proposed_clear_ind
        from (select distinct rmcilg.item,
                     rmcilg.location,
                     FIRST_VALUE(ROUND(pr, L_round)) OVER (PARTITION BY rmcilg.item,
                                                                        rmcilg.location
                                                               ORDER BY pr asc) proposed_retail,
                     FIRST_VALUE(pru) OVER (PARTITION BY rmcilg.item,
                                                         rmcilg.location
                                                ORDER BY pr asc) proposed_retail_uom,
                     0 proposed_clear_ind,
                     rmcilg.basis_regular_retail
                from rpm_me_consolidate_itemloc_gtt rmcilg,
                     (select p.item,
                             DECODE(rad.percent_apply_type,
                                    0, p.proposed_retail * (1 + rad.percent / 100),
                                    1, p.proposed_retail * (1 - rad.percent / 100),
                                    2, p.proposed_retail) pr,
                             p.proposed_retail_uom pru
                        from (select item,
                                     AVG(proposed_retail) proposed_retail,
                                     MIN(proposed_retail_uom) proposed_retail_uom
                                from rpm_area_temp_item_loc ratil
                               where proposed_retail is NOT NULL
                               group by item) p,
                           rpm_area_diff rad
                       where rad.area_diff_id = LP_area_diff_id
                      union all
                      select rmig.item,
                             --
                             case
                                when (    rad.compete_type = COMP_PRICE_ABOVE
                                      and NOT rmig.basis_zl_regular_retail between
                                             rmig.primary_comp_retail * ( 1 + rad.from_percent / 100) and
                                             rmig.primary_comp_retail * ( 1 + rad.to_percent / 100)) then
                                   rmig.primary_comp_retail * ( 1 + rad.percent / 100)
                                when (    rad.compete_type = COMP_PRICE_ABOVE
                                      and rmig.basis_zl_regular_retail between
                                             rmig.primary_comp_retail * ( 1 + rad.from_percent / 100) and
                                             rmig.primary_comp_retail * ( 1 + rad.to_percent / 100)) then
                                   rmig.basis_zl_regular_retail
                                when rad.compete_type = COMP_MATCH then
                                   rmig.primary_comp_retail
                                when (    rad.compete_type = COMP_PRICE_BELOW
                                      and NOT rmig.basis_zl_regular_retail between
                                            rmig.primary_comp_retail * ( 1 - rad.to_percent / 100) and
                                            rmig.primary_comp_retail * ( 1 - rad.from_percent / 100)) then
                                   rmig.primary_comp_retail * ( 1 - rad.percent / 100)
                                when (    rad.compete_type = COMP_PRICE_BELOW
                                      and rmig.basis_zl_regular_retail between
                                             rmig.primary_comp_retail * ( 1 - rad.to_percent / 100) and
                                             rmig.primary_comp_retail * ( 1 - rad.from_percent / 100)) then
                                   rmig.basis_zl_regular_retail
                                else
                                   rmig.basis_zl_regular_retail
                             end pr,
                             rmig.basis_zl_regular_retail_uom pru
                        from rpm_me_item_gtt rmig,
                             (select case
                                        when reg.compete_type = COMP_PRICE_CODE then
                                           code.percent
                                     else
                                        reg.percent
                                     end percent,
                                     case
                                        when reg.compete_type = COMP_PRICE_CODE then
                                           code.from_percent
                                     else
                                        reg.from_percent
                                     end from_percent,
                                     case
                                        when reg.compete_type = COMP_PRICE_CODE then
                                           code.to_percent
                                        else
                                           reg.to_percent
                                     end to_percent,
                                     case
                                        when reg.compete_type = COMP_PRICE_CODE then
                                           code.compete_type
                                     else
                                        reg.compete_type
                                     end compete_type,
                                     radm.mkt_basket_code
                                from rpm_area_diff_comp reg,
                                     rpm_area_diff_mbc radm,
                                     rpm_area_diff_comp code
                               where reg.area_diff_id      = LP_area_diff_id
                                 and reg.area_diff_comp_id = radm.area_diff_comp_id (+)
                                 and radm.area_diff_mbc_id = code.area_diff_mbc_id(+)) rad
                       where rmig.primary_comp_retail is NOT NULL
                         and NVL(rmig.comp_mkt_basket_code, -999) = NVL(rad.mkt_basket_code, NVL(rmig.comp_mkt_basket_code, -999))) ad
             where rmcilg.item = ad.item) t
          where NVL(t.proposed_retail, t.basis_regular_retail) != t.basis_regular_retail;

   end if;

   if L_price_guide_id IS NOT NULL then

      merge into rpm_me_consolidate_itemloc_gtt rmcilg
      using (select /*+ ORDERED USE_NL(rmpg1, rmcilg1)*/ rmpg1.item,
                    rmpg1.location,
                    NVL(rpgi.new_price_val, rmpg1.proposed_retail) proposed_retail,
                    rmpg1.proposed_retail_uom,
                    NVL(rpgi.new_price_val, rmpg1.proposed_retail_std) proposed_retail_std,
                    rmpg1.proposed_clear_mkdn_nbr,
                    rmpg1.proposed_clear_ind
               from rpm_me_proposed_gtt rmpg1,
                    rpm_me_consolidate_itemloc_gtt rmcilg1,
                    (select rpgi.from_price_val,
                            rpgi.to_price_val,
                            rpgi.new_price_val
                       from rpm_price_guide_interval rpgi
                      where rpgi.price_guide_id = L_price_guide_id
                        and rownum              > 0) rpgi,
                    (select distinct con.item,
                            MAX(NVL(con.clear_boolean, 0)) OVER (PARTITION BY NVL(con.link_code,
                                                                                  con.item)) clear_ind,
                            MAX(NVL(con.clear_boolean, 0) + NVL(con.current_clearance_ind, 0)) OVER (PARTITION BY NVL(con.link_code,
                                                                                                                      con.item)) not_clear_ind
                       from rpm_me_consolidate_itemloc_gtt con) zl
              where rmpg1.item              = rmcilg1.item
                and rmpg1.location          = rmcilg1.location
                and rmcilg1.item            = zl.item
                and rpgi.from_price_val(+) <= rmpg1.proposed_retail
                and rpgi.to_price_val(+)   >= rmpg1.proposed_retail
                and (   (    L_strategy_type = CLEARANCE
                         and zl.clear_ind    = 0)
                     or (    L_strategy_type != CLEARANCE
                         and zl.not_clear_ind = 0))
                and rownum                  > 0) rmpg
      on (    rmcilg.item     = rmpg.item
          and rmcilg.location = rmpg.location)
      when MATCHED then
         update
            set rmcilg.proposed_retail         = rmpg.proposed_retail,
                rmcilg.proposed_retail_uom     = rmpg.proposed_retail_uom,
                rmcilg.proposed_retail_std     = rmpg.proposed_retail,
                rmcilg.proposed_clear_mkdn_nbr = rmpg.proposed_clear_mkdn_nbr,
                rmcilg.proposed_clear_ind      = rmpg.proposed_clear_ind,
                rmcilg.new_retail              = rmpg.proposed_retail,
                rmcilg.new_retail_uom          = rmpg.proposed_retail_uom,
                rmcilg.new_retail_std          = rmpg.proposed_retail,
                rmcilg.new_clear_mkdn_nbr      = rmpg.proposed_clear_mkdn_nbr,
                rmcilg.new_clear_ind           = rmpg.proposed_clear_ind;

   else

      merge into rpm_me_consolidate_itemloc_gtt rmcilg
      using ( select rmpg1.item,
                     rmpg1.location,
                     rmpg1.proposed_retail,
                     rmpg1.proposed_retail_uom,
                     rmpg1.proposed_retail_std,
                     rmpg1.proposed_clear_mkdn_nbr,
                     rmpg1.proposed_clear_ind
                from rpm_me_proposed_gtt rmpg1,
                     rpm_me_consolidate_itemloc_gtt rmcilg1,
                     (select distinct con.item,
                             MAX(NVL(con.clear_boolean, 0)) OVER (PARTITION BY NVL(con.link_code,
                                                                                   con.item)) clear_ind,
                             MAX(NVL(con.clear_boolean, 0) + NVL(con.current_clearance_ind, 0)) OVER (PARTITION BY NVL(con.link_code,
                                                                                                                       con.item)) not_clear_ind
                        from rpm_me_consolidate_itemloc_gtt con) zl
               where rmpg1.item     = rmcilg1.item
                 and rmpg1.location = rmcilg1.location
                 and rmcilg1.item   = zl.item
                 and (   (    L_strategy_type  = CLEARANCE
                          and zl.clear_ind     = 0)
                      or (    L_strategy_type != CLEARANCE
                          and zl.not_clear_ind = 0))) rmpg
      on (    rmcilg.item     = rmpg.item
          and rmcilg.location = rmpg.location)
      when MATCHED then
         update
            set rmcilg.proposed_retail         = rmpg.proposed_retail,
                rmcilg.proposed_retail_uom     = rmpg.proposed_retail_uom,
                rmcilg.proposed_retail_std     = rmpg.proposed_retail,
                rmcilg.proposed_clear_mkdn_nbr = rmpg.proposed_clear_mkdn_nbr,
                rmcilg.proposed_clear_ind      = rmpg.proposed_clear_ind,
                rmcilg.new_retail              = rmpg.proposed_retail,
                rmcilg.new_retail_uom          = rmpg.proposed_retail_uom,
                rmcilg.new_retail_std          = rmpg.proposed_retail,
                rmcilg.new_clear_mkdn_nbr      = rmpg.proposed_clear_mkdn_nbr,
                rmcilg.new_clear_ind           = rmpg.proposed_clear_ind;

   end if;

   if PROPOSE_UOM_PKG_CALL(O_error_msg) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END POPULATE_PROPOSED;

----------------------------------------------------------------------------------------
FUNCTION PROPOSE_UOM_PKG_CALL(O_error_msg    IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

   TYPE t_item                IS TABLE OF RPM_WORKSHEET_ITEM_LOC_DATA.ITEM%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_location            IS TABLE OF RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_proposed_retail_std IS TABLE OF RPM_WORKSHEET_ITEM_LOC_DATA.PROPOSED_RETAIL_STD%TYPE INDEX BY BINARY_INTEGER;

   a_item                     t_item;
   a_location                 t_location;
   a_proposed_retail_std      t_proposed_retail_std;

   a_update_size                       BINARY_INTEGER := 0;
   update_count                        BINARY_INTEGER := 0;

   cursor C_ITEM_LOC_CONV is
      select /*  USE_HASH(il, i) */
             il.item,
             il.location,
             i.standard_uom,
             il.il_primary_supp,
             il.il_primary_cntry,
             --
             il.proposed_retail,
             il.proposed_retail_uom,
             il.proposed_retail_std
        from rpm_me_consolidate_itemloc_gtt il,
             rpm_me_item_gtt i
       where il.item          = i.item
         and i.standard_uom  != il.proposed_retail_uom;

BEGIN

   /* This function is only called by the batch program.  It assumes that only one zone
      will be in the rpm_me_item_gtt table at a time */
   FOR rec in C_ITEM_LOC_CONV LOOP

      a_update_size := a_update_size + 1;

      a_item(a_update_size) := rec.item;
      a_location(a_update_size) := rec.location;
      a_proposed_retail_std(a_update_size) := null;

      if UOM_SQL.CONVERT(O_error_msg,
                         a_proposed_retail_std(a_update_size),
                         rec.standard_uom,
                         rec.proposed_retail,
                         rec.proposed_retail_uom,
                         rec.item,
                         rec.il_primary_supp,
                         rec.il_primary_cntry) = FALSE then
         return FALSE;
      end if;

   END LOOP;

   if a_update_size > 0 then

      FORALL update_count IN 1..a_update_size
         update rpm_me_consolidate_itemloc_gtt set
                proposed_retail_std        = a_proposed_retail_std(update_count),
                new_retail_std             = a_proposed_retail_std(update_count)
          where item     = a_item(update_count)
            and location = a_location(update_count);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.PROPOSE_UOM_PKG_CALL',
                                        to_char(SQLCODE));
      return FALSE;

END PROPOSE_UOM_PKG_CALL;

----------------------------------------------------------------------------------------
FUNCTION UOM_VAT_PKG_CALLS(O_error_msg    IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

   TYPE t_item                         IS TABLE OF rpm_worksheet_item_loc_data.item%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_zone_id                      IS TABLE OF rpm_worksheet_item_loc_data.zone_id%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_location                     IS TABLE OF rpm_worksheet_item_loc_data.location%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_current_regular_retail_std   IS TABLE OF rpm_worksheet_item_loc_data.current_regular_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_maint_margin_retail_std      IS TABLE OF rpm_worksheet_item_loc_data.maint_margin_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_current_clear_retail_std     IS TABLE OF rpm_worksheet_item_loc_data.current_clear_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_basis_regular_retail_std     IS TABLE OF rpm_worksheet_item_loc_data.basis_regular_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_basis_clear_retail_std       IS TABLE OF rpm_worksheet_item_loc_data.basis_clear_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_current_cost                 IS TABLE OF rpm_worksheet_item_loc_data.current_cost%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_basis_base_cost              IS TABLE OF rpm_worksheet_item_loc_data.basis_base_cost%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_basis_pricing_cost           IS TABLE OF rpm_worksheet_item_loc_data.basis_pricing_cost%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_maint_margin_cost            IS TABLE OF rpm_worksheet_item_loc_data.maint_margin_cost%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_pend_cost_chg_cost           IS TABLE OF rpm_worksheet_item_loc_data.pend_cost_chg_cost%TYPE INDEX BY BINARY_INTEGER;
   --
   TYPE t_primary_comp_retail_std       IS TABLE OF rpm_worksheet_item_data.primary_comp_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_current_zl_reg_retail_std     IS TABLE OF rpm_worksheet_item_data.current_zl_regular_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_maint_margin_zl_retail_std    IS TABLE OF rpm_worksheet_item_data.maint_margin_zl_retail_std%TYPE INDEX BY BINARY_INTEGER;
   TYPE t_basis_zl_regular_retail_std   IS TABLE OF rpm_worksheet_item_data.basis_zl_regular_retail_std%TYPE INDEX BY BINARY_INTEGER;

   a_item                              t_item;
   a_zone_id                           t_zone_id;
   a_location                          t_location;
   a_current_regular_retail_std        t_current_regular_retail_std;
   a_maint_margin_retail_std           t_maint_margin_retail_std;
   a_current_clear_retail_std          t_current_clear_retail_std;
   a_basis_regular_retail_std          t_basis_regular_retail_std;
   a_basis_clear_retail_std            t_basis_clear_retail_std;
   a_current_cost                      t_current_cost;
   a_basis_base_cost                   t_basis_base_cost;
   a_basis_pricing_cost                t_basis_pricing_cost;
   a_maint_margin_cost                 t_maint_margin_cost;
   a_pend_cost_chg_cost                t_pend_cost_chg_cost;
   --
   a_primary_comp_retail_std           t_primary_comp_retail_std;
   a_current_zl_reg_retail_std         t_current_zl_reg_retail_std;
   a_maint_margin_zl_retail_std        t_maint_margin_zl_retail_std;
   a_basis_zl_regular_retail_std       t_basis_zl_regular_retail_std;

   a_update_size                       BINARY_INTEGER := 0;
   update_count                        BINARY_INTEGER := 0;

   cursor C_ITEM_LOC_CONV is
      select /*  USE_HASH(il, i) */ il.item,
             il.location,
             i.standard_uom,
             i.primary_supplier,
             il.il_primary_supp,
             il.il_primary_cntry,
             i.currency,
             il.supp_currency,
             --
             il.current_regular_retail,
             il.current_regular_retail_uom,
             il.current_regular_retail_std,
             decode(i.standard_uom, nvl(il.current_regular_retail_uom, i.standard_uom), 0,1) crr_ind,
             --
             il.maint_margin_retail,
             il.maint_margin_retail_uom,
             il.maint_margin_retail_std,
             decode(i.standard_uom, nvl(il.maint_margin_retail_uom, i.standard_uom), 0,1) mmr_ind,
             --
             il.current_clear_retail,
             il.current_clear_retail_uom,
             il.current_clear_retail_std,
             decode(i.standard_uom, nvl(il.current_clear_retail_uom, i.standard_uom), 0,1) ccr_ind,
             --
             il.basis_regular_retail,
             il.basis_regular_retail_uom,
             il.basis_regular_retail_std,
             decode(i.standard_uom, nvl(il.basis_regular_retail_uom, i.standard_uom), 0,1) brr_ind,
             --
             il.basis_clear_retail,
             il.basis_clear_retail_uom,
             il.basis_clear_retail_std,
             decode(i.standard_uom, nvl(il.basis_clear_retail_uom, i.standard_uom), 0,1) bcr_ind,
             --
             il.current_cost,
             il.basis_base_cost,
             il.basis_pricing_cost,
             il.maint_margin_cost,
             il.pend_cost_chg_cost,
             decode(i.currency, il.supp_currency, 0,1) cost_ind
        from rpm_me_consolidate_itemloc_gtt il,
             rpm_me_item_gtt i
       where i.item          = il.item
         and i.zone_id       = il.zone_id
         and (i.standard_uom  != il.current_regular_retail_uom
              or (i.standard_uom != il.maint_margin_retail_uom and LP_maint_margin_ind = 1)
              or i.standard_uom  != il.basis_regular_retail_uom
              or i.standard_uom  != il.basis_clear_retail_uom
              or i.currency      != nvl(il.supp_currency, i.currency));

   cursor C_ITEM_CONV is
      select i.item,
             i.zone_id,
             i.standard_uom,
             i.primary_supplier,
             --
             i.primary_comp_retail,
             i.primary_comp_retail_uom,
             i.primary_comp_retail_std,
             decode(i.standard_uom, nvl(i.primary_comp_retail_uom, i.standard_uom), 0,1) pc_ind,
             --
             i.current_zl_regular_retail,
             i.current_zl_regular_retail_uom,
             i.current_zl_regular_retail_std,
             decode(i.standard_uom, nvl(i.current_zl_regular_retail_uom, i.standard_uom), 0,1) czlreg_ind,
             --
             i.maint_margin_zl_retail,
             i.maint_margin_zl_retail_uom,
             i.maint_margin_zl_retail_std,
             decode(i.standard_uom, nvl(i.maint_margin_zl_retail_uom, i.standard_uom), 0,1) mmzl_ind,
             --
             i.basis_zl_regular_retail,
             i.basis_zl_regular_retail_uom,
             i.basis_zl_regular_retail_std,
             decode(i.standard_uom, nvl(i.basis_zl_regular_retail_uom, i.standard_uom), 0,1) bzlreg_ind
        from rpm_me_item_gtt i
       where i.standard_uom  != i.primary_comp_retail_uom
          or i.standard_uom  != i.current_zl_regular_retail_uom
          or i.standard_uom  != i.maint_margin_zl_retail_uom
          or i.standard_uom  != i.basis_zl_regular_retail_uom;

BEGIN

   FOR rec in C_ITEM_LOC_CONV LOOP

     a_update_size := a_update_size + 1;

     a_item(a_update_size) := rec.item;
     a_location(a_update_size) := rec.location;
     a_current_regular_retail_std(a_update_size) := null;
     a_maint_margin_retail_std(a_update_size)    := null;
     a_current_clear_retail_std(a_update_size)   := null;
     a_basis_regular_retail_std(a_update_size)   := null;
     a_basis_clear_retail_std(a_update_size)     := null;
     a_current_cost(a_update_size)               := null;
     a_basis_base_cost(a_update_size)            := null;
     a_basis_pricing_cost(a_update_size)         := null;
     a_maint_margin_cost(a_update_size)          := null;
     a_pend_cost_chg_cost(a_update_size)         := null;

     if rec.crr_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_current_regular_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.current_regular_retail,
                            rec.current_regular_retail_uom,
                            rec.item,
                            rec.il_primary_supp,
                            rec.il_primary_cntry) = FALSE then
            return FALSE;
         end if;
     else
        a_current_regular_retail_std(a_update_size) := rec.current_regular_retail;
     end if;
     --
     if rec.mmr_ind = 1 then

         if UOM_SQL.CONVERT(O_error_msg,
                            a_maint_margin_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.maint_margin_retail,
                            rec.maint_margin_retail_uom,
                            rec.item,
                            rec.il_primary_supp,
                            rec.il_primary_cntry) = FALSE then
            return FALSE;
         end if;
     else
        a_maint_margin_retail_std(a_update_size) := rec.maint_margin_retail;
     end if;
     --
     if rec.ccr_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_current_clear_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.current_clear_retail,
                            rec.current_clear_retail_uom,
                            rec.item,
                            rec.il_primary_supp,
                            rec.il_primary_cntry) = FALSE then
            return FALSE;
         end if;
     else
        a_current_clear_retail_std(a_update_size) := rec.current_clear_retail;
     end if;
     --
     if rec.brr_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_basis_regular_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.basis_regular_retail,
                            rec.basis_regular_retail_uom,
                            rec.item,
                            rec.il_primary_supp,
                            rec.il_primary_cntry) = FALSE then
            return FALSE;
         end if;
     else
        a_basis_regular_retail_std(a_update_size) := rec.basis_regular_retail;
     end if;
     --
     if rec.bcr_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_basis_clear_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.basis_clear_retail,
                            rec.basis_clear_retail_uom,
                            rec.item,
                            rec.il_primary_supp,
                            rec.il_primary_cntry) = FALSE then
            return FALSE;
         end if;
     else
        a_basis_clear_retail_std(a_update_size) := rec.basis_clear_retail;
     end if;
     --
     if rec.cost_ind = 1 then
         if CURRENCY_SQL.CONVERT (O_error_msg,
                                  rec.current_cost,
                                  rec.supp_currency,
                                  rec.currency,
                                  a_current_cost(a_update_size),
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.CONVERT (O_error_msg,
                                  rec.basis_base_cost,
                                  rec.supp_currency,
                                  rec.currency,
                                  a_basis_base_cost(a_update_size),
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.CONVERT (O_error_msg,
                                  rec.basis_pricing_cost,
                                  rec.supp_currency,
                                  rec.currency,
                                  a_basis_pricing_cost(a_update_size),
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.CONVERT (O_error_msg,
                                  rec.maint_margin_cost,
                                  rec.supp_currency,
                                  rec.currency,
                                  a_maint_margin_cost(a_update_size),
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL) = FALSE then
            return FALSE;
         end if;
         --
         if CURRENCY_SQL.CONVERT (O_error_msg,
                                  rec.pend_cost_chg_cost,
                                  rec.supp_currency,
                                  rec.currency,
                                  a_pend_cost_chg_cost(a_update_size),
                                  'C',
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL) = FALSE then
            return FALSE;
         end if;
     else
        a_current_cost(a_update_size)        := rec.current_cost;
        a_basis_base_cost(a_update_size)     := rec.basis_base_cost;
        a_basis_pricing_cost(a_update_size)  := rec.basis_pricing_cost;
        a_maint_margin_cost(a_update_size)   := rec.maint_margin_cost;
        a_pend_cost_chg_cost(a_update_size)  := rec.pend_cost_chg_cost;
     end if;

   END LOOP;

   if a_update_size > 0 then

      FORALL update_count IN 1..a_update_size
         update rpm_me_consolidate_itemloc_gtt set
                current_regular_retail_std = a_current_regular_retail_std(update_count),
                maint_margin_retail_std    = a_maint_margin_retail_std(update_count),
                current_clear_retail_std   = a_current_clear_retail_std(update_count),
                basis_regular_retail_std   = a_basis_regular_retail_std(update_count),
                basis_clear_retail_std     = a_basis_clear_retail_std(update_count),
                current_cost               = a_current_cost(update_count),
                basis_base_cost            = a_basis_base_cost(update_count),
                basis_pricing_cost         = a_basis_pricing_cost(update_count),
                maint_margin_cost          = a_maint_margin_cost(update_count),
                pend_cost_chg_cost         = a_pend_cost_chg_cost(update_count)
          where item     = a_item(update_count)
            and location = a_location(update_count);

   end if;

   -----------------

   a_update_size := 0;

   -----------------

   FOR rec in C_ITEM_CONV LOOP

     a_update_size := a_update_size + 1;

     a_item(a_update_size) := rec.item;
     a_zone_id(a_update_size) := rec.zone_id;
     a_primary_comp_retail_std(a_update_size)     := null;
     a_current_zl_reg_retail_std(a_update_size)   := null;
     a_maint_margin_zl_retail_std(a_update_size)  := null;
     a_basis_zl_regular_retail_std(a_update_size) := null;

     if rec.pc_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_primary_comp_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.primary_comp_retail,
                            rec.primary_comp_retail_uom,
                            rec.item,
                            rec.primary_supplier,
                            null) = FALSE then
            return FALSE;
         end if;
     else
        a_primary_comp_retail_std(a_update_size) := rec.primary_comp_retail;
     end if;
     --
     if rec.czlreg_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_current_zl_reg_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.current_zl_regular_retail,
                            rec.current_zl_regular_retail_uom,
                            rec.item,
                            rec.primary_supplier,
                            null) = FALSE then
            return FALSE;
         end if;
     else
        a_current_zl_reg_retail_std(a_update_size) := rec.current_zl_regular_retail;
     end if;
     --
     if rec.mmzl_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_maint_margin_zl_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.maint_margin_zl_retail,
                            rec.maint_margin_zl_retail_uom,
                            rec.item,
                            rec.primary_supplier,
                            null) = FALSE then
            return FALSE;
         end if;
     else
        a_maint_margin_zl_retail_std(a_update_size) := rec.maint_margin_zl_retail;
     end if;
     --
     if rec.bzlreg_ind = 1 then
         if UOM_SQL.CONVERT(O_error_msg,
                            a_basis_zl_regular_retail_std(a_update_size),
                            rec.standard_uom,
                            rec.basis_zl_regular_retail,
                            rec.basis_zl_regular_retail_uom,
                            rec.item,
                            rec.primary_supplier,
                            null) = FALSE then
            return FALSE;
         end if;
     else
        a_basis_zl_regular_retail_std(a_update_size) := rec.basis_zl_regular_retail;
     end if;

   END LOOP;

   if a_update_size > 0 then

      FORALL update_count IN 1..a_update_size
         update rpm_me_item_gtt set
                primary_comp_retail_std       = a_primary_comp_retail_std(update_count),
                current_zl_regular_retail_std = a_current_zl_reg_retail_std(update_count),
                maint_margin_zl_retail_std    = a_maint_margin_zl_retail_std(update_count),
                basis_zl_regular_retail_std   = a_basis_zl_regular_retail_std(update_count)
          where item    = a_item(update_count)
            and zone_id = a_zone_id(update_count);

   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.UOM_VAT_PKG_CALLS',
                                        to_char(SQLCODE));
      return FALSE;
END UOM_VAT_PKG_CALLS;

----------------------------------------------------------------------------------------
FUNCTION FINISH_RPM_LUW(O_error_msg    IN OUT  VARCHAR2)
   RETURN BOOLEAN IS

   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.FINISH_RPM_LUW';

BEGIN

   merge into rpm_worksheet_item_data wid
   using(
      select worksheet_item_data_id,
             worksheet_status_id,
             proposed_strategy_id,
             state,
             reset_state,
             item,
             item_desc,
             dept,
             dept_name,
             class,
             class_name,
             subclass,
             subclass_name,
             markup_calc_type,
             item_level,
             tran_level,
             standard_uom,
             diff_1,
             diff_type_1,
             diff_2,
             diff_type_2,
             diff_3,
             diff_type_3,
             diff_4,
             diff_type_4,
             retail_include_vat_ind,
             primary_supplier,
             vpn,
             uda_boolean,
             package_size,
             package_uom,
             retail_label_type,
             retail_label_value,
             season_phase_boolean,
             original_retail,
             item_parent,
             parent_desc,
             parent_diff_1,
             parent_diff_2,
             parent_diff_3,
             parent_diff_4,
             parent_diff_1_type,
             parent_diff_2_type,
             parent_diff_3_type,
             parent_diff_4_type,
             parent_diff_1_type_desc,
             parent_diff_2_type_desc,
             parent_diff_3_type_desc,
             parent_diff_4_type_desc,
             parent_vpn,
             primary_comp_retail,
             primary_comp_retail_uom,
             primary_comp_store,
             primary_comp_retail_std,
             primary_multi_units,
             primary_multi_unit_retail,
             primary_multi_unit_retail_uom,
             primary_comp_boolean,
             a_comp_retail,
             b_comp_retail,
             c_comp_retail,
             d_comp_retail,
             e_comp_retail,
             a_comp_store,
             b_comp_store,
             c_comp_store,
             d_comp_store,
             e_comp_store,
             zone_group_id,
             zone_group_display_id,
             zone_group_name,
             zone_id,
             zone_display_id,
             zone_name,
             currency,
             margin_mkt_basket_code,
             comp_mkt_basket_code,
             margin_mbc_name,
             competitive_mbc_name,
             NVL(ref_wh_stock,0) ref_wh_stock,
             NVL(ref_wh_on_order,0) ref_wh_on_order,
             NVL(ref_wh_inventory,0) ref_wh_inventory,
             current_zl_regular_retail,
             current_zl_regular_retail_uom,
             current_zl_regular_retail_std,
             current_zl_multi_units,
             current_zl_multi_unit_retail,
             current_zl_multi_uom,
             current_zl_clear_retail,
             current_zl_clear_retail_uom,
             current_zl_clear_retail_std,
             basis_zl_regular_retail,
             basis_zl_regular_retail_uom,
             basis_zl_regular_retail_std,
             basis_zl_multi_units,
             basis_zl_multi_unit_retail,
             basis_zl_multi_uom,
             basis_zl_clear_retail,
             basis_zl_clear_retail_uom,
             basis_zl_clear_retail_std,
             basis_zl_clear_mkdn_nbr,
             current_zl_cost,
             basis_zl_base_cost,
             basis_zl_pricing_cost,
             pend_zl_cost_chg_cost,
             maint_margin_zl_retail,
             maint_margin_zl_retail_uom,
             maint_margin_zl_retail_std,
             maint_margin_zl_cost,
             proposed_zl_reset_date,
             proposed_zl_out_of_stock_date,
             constraint_boolean,
             ignore_constraint_boolean,
             area_diff_prim_id,
             area_diff_id,
             NVL(action_flag,0) action_flag
        from rpm_me_item_gtt i
       where EXISTS (select 1
                       from rpm_me_consolidate_itemloc_gtt il
                      where il.item = i.item)) use
   on (    wid.worksheet_item_data_id = use.worksheet_item_data_id
       and wid.dept                   = use.dept)
   when MATCHED then
   update
      set wid.worksheet_status_id = use.worksheet_status_id,
          wid.proposed_strategy_id = use.proposed_strategy_id,
          wid.state = use.state,
          wid.reset_state = use.reset_state,
          wid.item = use.item,
          wid.item_desc = use.item_desc,
          wid.dept_name = use.dept_name,
          wid.class = use.class,
          wid.class_name = use.class_name,
          wid.subclass = use.subclass,
          wid.subclass_name = use.subclass_name,
          wid.markup_calc_type = use.markup_calc_type,
          wid.item_level = use.item_level,
          wid.tran_level = use.tran_level,
          wid.standard_uom = use.standard_uom,
          wid.diff_1 = use.diff_1,
          wid.diff_type_1 = use.diff_type_1,
          wid.diff_2 = use.diff_2,
          wid.diff_type_2 = use.diff_type_2,
          wid.diff_3 = use.diff_3,
          wid.diff_type_3 = use.diff_type_3,
          wid.diff_4 = use.diff_4,
          wid.diff_type_4 = use.diff_type_4,
          wid.retail_include_vat_ind = use.retail_include_vat_ind,
          wid.primary_supplier = use.primary_supplier,
          wid.vpn = use.vpn,
          wid.uda_boolean = use.uda_boolean,
          wid.package_size = use.package_size,
          wid.package_uom = use.package_uom,
          wid.retail_label_type = use.retail_label_type,
          wid.retail_label_value = use.retail_label_value,
          wid.season_phase_boolean = use.season_phase_boolean,
          wid.original_retail = use.original_retail,
          wid.item_parent = use.item_parent,
          wid.parent_desc = use.parent_desc,
          wid.parent_diff_1 = use.parent_diff_1,
          wid.parent_diff_2 = use.parent_diff_2,
          wid.parent_diff_3 = use.parent_diff_3,
          wid.parent_diff_4 = use.parent_diff_4,
          wid.parent_diff_1_type = use.parent_diff_1_type,
          wid.parent_diff_2_type = use.parent_diff_2_type,
          wid.parent_diff_3_type = use.parent_diff_3_type,
          wid.parent_diff_4_type = use.parent_diff_4_type,
          wid.parent_diff_1_type_desc = use.parent_diff_1_type_desc,
          wid.parent_diff_2_type_desc = use.parent_diff_2_type_desc,
          wid.parent_diff_3_type_desc = use.parent_diff_3_type_desc,
          wid.parent_diff_4_type_desc = use.parent_diff_4_type_desc,
          wid.parent_vpn = use.parent_vpn,
          wid.primary_comp_retail = use.primary_comp_retail,
          wid.primary_comp_retail_uom = use.primary_comp_retail_uom,
          wid.primary_comp_store = use.primary_comp_store,
          wid.primary_comp_retail_std = use.primary_comp_retail_std,
          wid.primary_multi_units = use.primary_multi_units,
          wid.primary_multi_unit_retail = use.primary_multi_unit_retail,
          wid.primary_multi_unit_retail_uom = use.primary_multi_unit_retail_uom,
          wid.primary_comp_boolean = use.primary_comp_boolean,
          wid.a_comp_retail = use.a_comp_retail,
          wid.b_comp_retail = use.b_comp_retail,
          wid.c_comp_retail = use.c_comp_retail,
          wid.d_comp_retail = use.d_comp_retail,
          wid.e_comp_retail = use.e_comp_retail,
          wid.a_comp_store = use.a_comp_store,
          wid.b_comp_store = use.b_comp_store,
          wid.c_comp_store = use.c_comp_store,
          wid.d_comp_store = use.d_comp_store,
          wid.e_comp_store = use.e_comp_store,
          wid.zone_group_id = use.zone_group_id,
          wid.zone_group_display_id = use.zone_group_display_id,
          wid.zone_group_name = use.zone_group_name,
          wid.zone_id = use.zone_id,
          wid.zone_display_id = use.zone_display_id,
          wid.zone_name = use.zone_name,
          wid.currency = use.currency,
          wid.margin_mkt_basket_code = use.margin_mkt_basket_code,
          wid.comp_mkt_basket_code = use.comp_mkt_basket_code,
          wid.margin_mbc_name = use.margin_mbc_name,
          wid.competitive_mbc_name = use.competitive_mbc_name,
          wid.ref_wh_stock = use.ref_wh_stock,
          wid.ref_wh_on_order = use.ref_wh_on_order,
          wid.ref_wh_inventory = use.ref_wh_inventory,
          wid.current_zl_regular_retail = use.current_zl_regular_retail,
          wid.current_zl_regular_retail_uom = use.current_zl_regular_retail_uom,
          wid.current_zl_regular_retail_std = use.current_zl_regular_retail_std,
          wid.current_zl_multi_units = use.current_zl_multi_units,
          wid.current_zl_multi_unit_retail = use.current_zl_multi_unit_retail,
          wid.current_zl_multi_uom = use.current_zl_multi_uom,
          wid.current_zl_clear_retail = use.current_zl_clear_retail,
          wid.current_zl_clear_retail_uom = use.current_zl_clear_retail_uom,
          wid.current_zl_clear_retail_std = use.current_zl_clear_retail_std,
          wid.basis_zl_regular_retail = use.basis_zl_regular_retail,
          wid.basis_zl_regular_retail_uom = use.basis_zl_regular_retail_uom,
          wid.basis_zl_regular_retail_std = use.basis_zl_regular_retail_std,
          wid.basis_zl_multi_units = use.basis_zl_multi_units,
          wid.basis_zl_multi_unit_retail = use.basis_zl_multi_unit_retail,
          wid.basis_zl_multi_uom = use.basis_zl_multi_uom,
          wid.basis_zl_clear_retail = use.basis_zl_clear_retail,
          wid.basis_zl_clear_retail_uom = use.basis_zl_clear_retail_uom,
          wid.basis_zl_clear_retail_std = use.basis_zl_clear_retail_std,
          wid.basis_zl_clear_mkdn_nbr = use.basis_zl_clear_mkdn_nbr,
          wid.current_zl_cost = use.current_zl_cost,
          wid.basis_zl_base_cost = use.basis_zl_base_cost,
          wid.basis_zl_pricing_cost = use.basis_zl_pricing_cost,
          wid.pend_zl_cost_chg_cost = use.pend_zl_cost_chg_cost,
          wid.maint_margin_zl_retail = use.maint_margin_zl_retail,
          wid.maint_margin_zl_retail_uom = use.maint_margin_zl_retail_uom,
          wid.maint_margin_zl_retail_std = use.maint_margin_zl_retail_std,
          wid.maint_margin_zl_cost = use.maint_margin_zl_cost,
          wid.proposed_zl_reset_date = use.proposed_zl_reset_date,
          wid.proposed_zl_out_of_stock_date = use.proposed_zl_out_of_stock_date,
          wid.constraint_boolean = use.constraint_boolean,
          wid.ignore_constraint_boolean = use.ignore_constraint_boolean,
          wid.area_diff_prim_id = use.area_diff_prim_id,
          wid.area_diff_id = use.area_diff_id,
          wid.action_flag = use.action_flag
   when NOT MATCHED then
   insert (worksheet_item_data_id,
           worksheet_status_id,
           proposed_strategy_id,
           state,
           reset_state,
           item,
           item_desc,
           dept,
           dept_name,
           class,
           class_name,
           subclass,
           subclass_name,
           markup_calc_type,
           item_level,
           tran_level,
           standard_uom,
           diff_1,
           diff_type_1,
           diff_2,
           diff_type_2,
           diff_3,
           diff_type_3,
           diff_4,
           diff_type_4,
           retail_include_vat_ind,
           primary_supplier,
           vpn,
           uda_boolean,
           package_size,
           package_uom,
           retail_label_type,
           retail_label_value,
           season_phase_boolean,
           original_retail,
           item_parent,
           parent_desc,
           parent_diff_1,
           parent_diff_2,
           parent_diff_3,
           parent_diff_4,
           parent_diff_1_type,
           parent_diff_2_type,
           parent_diff_3_type,
           parent_diff_4_type,
           parent_diff_1_type_desc,
           parent_diff_2_type_desc,
           parent_diff_3_type_desc,
           parent_diff_4_type_desc,
           parent_vpn,
           primary_comp_retail,
           primary_comp_retail_uom,
           primary_comp_store,
           primary_comp_retail_std,
           primary_multi_units,
           primary_multi_unit_retail,
           primary_multi_unit_retail_uom,
           primary_comp_boolean,
           a_comp_retail,
           b_comp_retail,
           c_comp_retail,
           d_comp_retail,
           e_comp_retail,
           a_comp_store,
           b_comp_store,
           c_comp_store,
           d_comp_store,
           e_comp_store,
           zone_group_id,
           zone_group_display_id,
           zone_group_name,
           zone_id,
           zone_display_id,
           zone_name,
           currency,
           margin_mkt_basket_code,
           comp_mkt_basket_code,
           margin_mbc_name,
           competitive_mbc_name,
           ref_wh_stock,
           ref_wh_on_order,
           ref_wh_inventory,
           current_zl_regular_retail,
           current_zl_regular_retail_uom,
           current_zl_regular_retail_std,
           current_zl_multi_units,
           current_zl_multi_unit_retail,
           current_zl_multi_uom,
           current_zl_clear_retail,
           current_zl_clear_retail_uom,
           current_zl_clear_retail_std,
           basis_zl_regular_retail,
           basis_zl_regular_retail_uom,
           basis_zl_regular_retail_std,
           basis_zl_multi_units,
           basis_zl_multi_unit_retail,
           basis_zl_multi_uom,
           basis_zl_clear_retail,
           basis_zl_clear_retail_uom,
           basis_zl_clear_retail_std,
           basis_zl_clear_mkdn_nbr,
           current_zl_cost,
           basis_zl_base_cost,
           basis_zl_pricing_cost,
           pend_zl_cost_chg_cost,
           maint_margin_zl_retail,
           maint_margin_zl_retail_uom,
           maint_margin_zl_retail_std,
           maint_margin_zl_cost,
           proposed_zl_reset_date,
           proposed_zl_out_of_stock_date,
           constraint_boolean,
           ignore_constraint_boolean,
           area_diff_prim_id,
           area_diff_id,
           action_flag)
   values (use.worksheet_item_data_id,
           use.worksheet_status_id,
           use.proposed_strategy_id,
           use.state,
           use.reset_state,
           use.item,
           use.item_desc,
           use.dept,
           use.dept_name,
           use.class,
           use.class_name,
           use.subclass,
           use.subclass_name,
           use.markup_calc_type,
           use.item_level,
           use.tran_level,
           use.standard_uom,
           use.diff_1,
           use.diff_type_1,
           use.diff_2,
           use.diff_type_2,
           use.diff_3,
           use.diff_type_3,
           use.diff_4,
           use.diff_type_4,
           use.retail_include_vat_ind,
           use.primary_supplier,
           use.vpn,
           use.uda_boolean,
           use.package_size,
           use.package_uom,
           use.retail_label_type,
           use.retail_label_value,
           use.season_phase_boolean,
           use.original_retail,
           use.item_parent,
           use.parent_desc,
           use.parent_diff_1,
           use.parent_diff_2,
           use.parent_diff_3,
           use.parent_diff_4,
           use.parent_diff_1_type,
           use.parent_diff_2_type,
           use.parent_diff_3_type,
           use.parent_diff_4_type,
           use.parent_diff_1_type_desc,
           use.parent_diff_2_type_desc,
           use.parent_diff_3_type_desc,
           use.parent_diff_4_type_desc,
           use.parent_vpn,
           use.primary_comp_retail,
           use.primary_comp_retail_uom,
           use.primary_comp_store,
           use.primary_comp_retail_std,
           use.primary_multi_units,
           use.primary_multi_unit_retail,
           use.primary_multi_unit_retail_uom,
           use.primary_comp_boolean,
           use.a_comp_retail,
           use.b_comp_retail,
           use.c_comp_retail,
           use.d_comp_retail,
           use.e_comp_retail,
           use.a_comp_store,
           use.b_comp_store,
           use.c_comp_store,
           use.d_comp_store,
           use.e_comp_store,
           use.zone_group_id,
           use.zone_group_display_id,
           use.zone_group_name,
           use.zone_id,
           use.zone_display_id,
           use.zone_name,
           use.currency,
           use.margin_mkt_basket_code,
           use.comp_mkt_basket_code,
           use.margin_mbc_name,
           use.competitive_mbc_name,
           use.ref_wh_stock,
           use.ref_wh_on_order,
           use.ref_wh_inventory,
           use.current_zl_regular_retail,
           use.current_zl_regular_retail_uom,
           use.current_zl_regular_retail_std,
           use.current_zl_multi_units,
           use.current_zl_multi_unit_retail,
           use.current_zl_multi_uom,
           use.current_zl_clear_retail,
           use.current_zl_clear_retail_uom,
           use.current_zl_clear_retail_std,
           use.basis_zl_regular_retail,
           use.basis_zl_regular_retail_uom,
           use.basis_zl_regular_retail_std,
           use.basis_zl_multi_units,
           use.basis_zl_multi_unit_retail,
           use.basis_zl_multi_uom,
           use.basis_zl_clear_retail,
           use.basis_zl_clear_retail_uom,
           use.basis_zl_clear_retail_std,
           use.basis_zl_clear_mkdn_nbr,
           use.current_zl_cost,
           use.basis_zl_base_cost,
           use.basis_zl_pricing_cost,
           use.pend_zl_cost_chg_cost,
           use.maint_margin_zl_retail,
           use.maint_margin_zl_retail_uom,
           use.maint_margin_zl_retail_std,
           use.maint_margin_zl_cost,
           use.proposed_zl_reset_date,
           use.proposed_zl_out_of_stock_date,
           use.constraint_boolean,
           use.ignore_constraint_boolean,
           use.area_diff_prim_id,
           use.area_diff_id,
           use.action_flag);

   merge into rpm_worksheet_item_loc_data wil
   using (
      select worksheet_item_loc_data_id,
             il.worksheet_item_data_id,
             il.worksheet_status_id,
             i.dept,
             i.class,
             i.subclass,
             i.item,
             i.item_parent,
             il.zone_id,
             il.proposed_strategy_id,
             user_strategy_id,
             il_primary_supp,
             il_primary_cntry,
             location,
             loc_type,
             current_regular_retail,
             current_regular_retail_uom,
             current_regular_retail_std,
             current_clear_retail,
             current_clear_retail_uom,
             current_clear_retail_std,
             current_multi_units,
             current_multi_unit_retail,
             current_multi_unit_uom,
             past_price_chg_date,
             past_cost_chg_date,
             pend_cost_chg_date,
             pend_cost_chg_cost,
             maint_margin_retail,
             maint_margin_retail_uom,
             maint_margin_retail_std,
             maint_margin_cost,
             pend_cost_boolean,
             cost_alert,
             current_cost,
             basis_base_cost,
             basis_pricing_cost,
             basis_clear_mkdn_nbr,
             current_clearance_ind,
             NVL(historical_sales,0) historical_sales,
             NVL(historical_sales_units,0) historical_sales_units,
             NVL(historical_issues,0) historical_issues,
             NVL(projected_sales,0) projected_sales,
             NVL(seasonal_sales,0) seasonal_sales,
             first_received_date,
             last_received_date,
             wks_of_sales_exp,
             NVL(location_stock,0) location_stock,
             NVL(location_on_order,0) location_on_order,
             NVL(location_inventory,0) location_inventory,
             link_code,
             link_code_display_id,
             link_code_desc,
             replenish_ind,
             vat_rate,
             vat_value,
             clear_boolean,
             wks_first_sale,
             promo_boolean,
             price_change_boolean,
             basis_regular_retail,
             basis_regular_retail_uom,
             basis_regular_retail_std,
             basis_clear_retail,
             basis_clear_retail_uom,
             basis_clear_retail_std,
             basis_multi_units,
             basis_multi_unit_retail,
             basis_multi_unit_uom,
             proposed_retail,
             proposed_retail_uom,
             proposed_retail_std,
             proposed_clear_mkdn_nbr,
             proposed_clear_ind,
             proposed_reset_date,
             proposed_out_of_stock_date,
             NVL(il.action_flag,0) action_flag,
             effective_date,
             new_retail,
             new_retail_uom,
             new_multi_unit_ind,
             new_multi_units,
             new_multi_unit_retail,
             new_multi_unit_uom,
             new_retail_std,
             new_clear_mkdn_nbr,
             new_clear_ind,
             new_reset_date,
             new_out_of_stock_date,
             new_item_loc_boolean,
             original_effective_date,
             rule_boolean,
             conflict_boolean
        from rpm_me_item_gtt i,
             rpm_me_consolidate_itemloc_gtt il
       where i.item = il.item) use
   on (    wil.worksheet_item_data_id = use.worksheet_item_data_id
       and wil.worksheet_item_loc_data_id = use.worksheet_item_loc_data_id
       and wil.dept = use.dept)
   when MATCHED then
   update
      set wil.worksheet_status_id = use.worksheet_status_id,
          wil.class = use.class,
          wil.subclass = use.subclass,
          wil.item = use.item,
          wil.item_parent = use.item_parent,
          wil.zone_id = use.zone_id,
          wil.proposed_strategy_id = use.proposed_strategy_id,
          wil.user_strategy_id = use.user_strategy_id,
          wil.il_primary_supp = use.il_primary_supp,
          wil.il_primary_cntry = use.il_primary_cntry,
          wil.location = use.location,
          wil.loc_type = use.loc_type,
          wil.current_regular_retail = use.current_regular_retail,
          wil.current_regular_retail_uom = use.current_regular_retail_uom,
          wil.current_regular_retail_std = use.current_regular_retail_std,
          wil.current_clear_retail = use.current_clear_retail,
          wil.current_clear_retail_uom = use.current_clear_retail_uom,
          wil.current_clear_retail_std = use.current_clear_retail_std,
          wil.current_multi_units = use.current_multi_units,
          wil.current_multi_unit_retail = use.current_multi_unit_retail,
          wil.current_multi_unit_uom = use.current_multi_unit_uom,
          wil.past_price_chg_date = use.past_price_chg_date,
          wil.past_cost_chg_date = use.past_cost_chg_date,
          wil.pend_cost_chg_date = use.pend_cost_chg_date,
          wil.pend_cost_chg_cost = use.pend_cost_chg_cost,
          wil.maint_margin_retail = use.maint_margin_retail,
          wil.maint_margin_retail_uom = use.maint_margin_retail_uom,
          wil.maint_margin_retail_std = use.maint_margin_retail_std,
          wil.maint_margin_cost = use.maint_margin_cost,
          wil.pend_cost_boolean = use.pend_cost_boolean,
          wil.cost_alert = use.cost_alert,
          wil.current_cost = use.current_cost,
          wil.basis_base_cost = use.basis_base_cost,
          wil.basis_pricing_cost = use.basis_pricing_cost,
          wil.basis_clear_mkdn_nbr = use.basis_clear_mkdn_nbr,
          wil.current_clearance_ind = use.current_clearance_ind,
          wil.historical_sales = use.historical_sales,
          wil.historical_sales_units = use.historical_sales_units,
          wil.historical_issues = use.historical_issues,
          wil.projected_sales = use.projected_sales,
          wil.seasonal_sales = use.seasonal_sales,
          wil.first_received_date = use.first_received_date,
          wil.last_received_date = use.last_received_date,
          wil.wks_of_sales_exp = use.wks_of_sales_exp,
          wil.location_stock = use.location_stock,
          wil.location_on_order = use.location_on_order,
          wil.location_inventory = use.location_inventory,
          wil.link_code = use.link_code,
          wil.link_code_display_id = use.link_code_display_id,
          wil.link_code_desc = use.link_code_desc,
          wil.replenish_ind = use.replenish_ind,
          wil.vat_rate = use.vat_rate,
          wil.vat_value = use.vat_value,
          wil.clear_boolean = use.clear_boolean,
          wil.wks_first_sale = use.wks_first_sale,
          wil.promo_boolean = use.promo_boolean,
          wil.price_change_boolean = use.price_change_boolean,
          wil.basis_regular_retail = use.basis_regular_retail,
          wil.basis_regular_retail_uom = use.basis_regular_retail_uom,
          wil.basis_regular_retail_std = use.basis_regular_retail_std,
          wil.basis_clear_retail = use.basis_clear_retail,
          wil.basis_clear_retail_uom = use.basis_clear_retail_uom,
          wil.basis_clear_retail_std = use.basis_clear_retail_std,
          wil.basis_multi_units = use.basis_multi_units,
          wil.basis_multi_unit_retail = use.basis_multi_unit_retail,
          wil.basis_multi_unit_uom = use.basis_multi_unit_uom,
          wil.proposed_retail = use.proposed_retail,
          wil.proposed_retail_uom = use.proposed_retail_uom,
          wil.proposed_retail_std = use.proposed_retail_std,
          wil.proposed_clear_mkdn_nbr = use.proposed_clear_mkdn_nbr,
          wil.proposed_clear_ind = use.proposed_clear_ind,
          wil.proposed_reset_date = use.proposed_reset_date,
          wil.proposed_out_of_stock_date = use.proposed_out_of_stock_date,
          wil.action_flag = use.action_flag,
          wil.effective_date = use.effective_date,
          wil.new_retail = use.new_retail,
          wil.new_retail_uom = use.new_retail_uom,
          wil.new_multi_unit_ind = use.new_multi_unit_ind,
          wil.new_multi_units = use.new_multi_units,
          wil.new_multi_unit_retail = use.new_multi_unit_retail,
          wil.new_multi_unit_uom = use.new_multi_unit_uom,
          wil.new_retail_std = use.new_retail_std,
          wil.new_clear_mkdn_nbr = use.new_clear_mkdn_nbr,
          wil.new_clear_ind = use.new_clear_ind,
          wil.new_reset_date = use.new_reset_date,
          wil.new_out_of_stock_date = use.new_out_of_stock_date,
          wil.new_item_loc_boolean = use.new_item_loc_boolean,
          wil.original_effective_date = use.original_effective_date,
          wil.rule_boolean = use.rule_boolean,
          wil.conflict_boolean = use.conflict_boolean
   when NOT MATCHED then
   insert (worksheet_item_loc_data_id,
           worksheet_item_data_id,
           worksheet_status_id,
           dept,
           class,
           subclass,
           item,
           item_parent,
           zone_id,
           proposed_strategy_id,
           user_strategy_id,
           il_primary_supp,
           il_primary_cntry,
           location,
           loc_type,
           current_regular_retail,
           current_regular_retail_uom,
           current_regular_retail_std,
           current_clear_retail,
           current_clear_retail_uom,
           current_clear_retail_std,
           current_multi_units,
           current_multi_unit_retail,
           current_multi_unit_uom,
           past_price_chg_date,
           past_cost_chg_date,
           pend_cost_chg_date,
           pend_cost_chg_cost,
           maint_margin_retail,
           maint_margin_retail_uom,
           maint_margin_retail_std,
           maint_margin_cost,
           pend_cost_boolean,
           cost_alert,
           current_cost,
           basis_base_cost,
           basis_pricing_cost,
           basis_clear_mkdn_nbr,
           current_clearance_ind,
           historical_sales,
           historical_sales_units,
           historical_issues,
           projected_sales,
           seasonal_sales,
           first_received_date,
           last_received_date,
           wks_of_sales_exp,
           location_stock,
           location_on_order,
           location_inventory,
           link_code,
           link_code_display_id,
           link_code_desc,
           replenish_ind,
           vat_rate,
           vat_value,
           clear_boolean,
           wks_first_sale,
           promo_boolean,
           price_change_boolean,
           basis_regular_retail,
           basis_regular_retail_uom,
           basis_regular_retail_std,
           basis_clear_retail,
           basis_clear_retail_uom,
           basis_clear_retail_std,
           basis_multi_units,
           basis_multi_unit_retail,
           basis_multi_unit_uom,
           proposed_retail,
           proposed_retail_uom,
           proposed_retail_std,
           proposed_clear_mkdn_nbr,
           proposed_clear_ind,
           proposed_reset_date,
           proposed_out_of_stock_date,
           action_flag,
           effective_date,
           new_retail,
           new_retail_uom,
           new_multi_unit_ind,
           new_multi_units,
           new_multi_unit_retail,
           new_multi_unit_uom,
           new_retail_std,
           new_clear_mkdn_nbr,
           new_clear_ind,
           new_reset_date,
           new_out_of_stock_date,
           new_item_loc_boolean,
           original_effective_date,
           rule_boolean,
           conflict_boolean)
   values (use.worksheet_item_loc_data_id,
           use.worksheet_item_data_id,
           use.worksheet_status_id,
           use.dept,
           use.class,
           use.subclass,
           use.item,
           use.item_parent,
           use.zone_id,
           use.proposed_strategy_id,
           use.user_strategy_id,
           use.il_primary_supp,
           use.il_primary_cntry,
           use.location,
           use.loc_type,
           use.current_regular_retail,
           use.current_regular_retail_uom,
           use.current_regular_retail_std,
           use.current_clear_retail,
           use.current_clear_retail_uom,
           use.current_clear_retail_std,
           use.current_multi_units,
           use.current_multi_unit_retail,
           use.current_multi_unit_uom,
           use.past_price_chg_date,
           use.past_cost_chg_date,
           use.pend_cost_chg_date,
           use.pend_cost_chg_cost,
           use.maint_margin_retail,
           use.maint_margin_retail_uom,
           use.maint_margin_retail_std,
           use.maint_margin_cost,
           use.pend_cost_boolean,
           use.cost_alert,
           use.current_cost,
           use.basis_base_cost,
           use.basis_pricing_cost,
           use.basis_clear_mkdn_nbr,
           use.current_clearance_ind,
           use.historical_sales,
           use.historical_sales_units,
           use.historical_issues,
           use.projected_sales,
           use.seasonal_sales,
           use.first_received_date,
           use.last_received_date,
           use.wks_of_sales_exp,
           use.location_stock,
           use.location_on_order,
           use.location_inventory,
           use.link_code,
           use.link_code_display_id,
           use.link_code_desc,
           use.replenish_ind,
           use.vat_rate,
           use.vat_value,
           use.clear_boolean,
           use.wks_first_sale,
           use.promo_boolean,
           use.price_change_boolean,
           use.basis_regular_retail,
           use.basis_regular_retail_uom,
           use.basis_regular_retail_std,
           use.basis_clear_retail,
           use.basis_clear_retail_uom,
           use.basis_clear_retail_std,
           use.basis_multi_units,
           use.basis_multi_unit_retail,
           use.basis_multi_unit_uom,
           use.proposed_retail,
           use.proposed_retail_uom,
           use.proposed_retail_std,
           use.proposed_clear_mkdn_nbr,
           use.proposed_clear_ind,
           use.proposed_reset_date,
           use.proposed_out_of_stock_date,
           use.action_flag,
           use.effective_date,
           use.new_retail,
           use.new_retail_uom,
           use.new_multi_unit_ind,
           use.new_multi_units,
           use.new_multi_unit_retail,
           use.new_multi_unit_uom,
           use.new_retail_std,
           use.new_clear_mkdn_nbr,
           use.new_clear_ind,
           use.new_reset_date,
           use.new_out_of_stock_date,
           use.new_item_loc_boolean,
           use.original_effective_date,
           use.rule_boolean,
           use.conflict_boolean);

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END FINISH_RPM_LUW;

----------------------------------------------------------------------------------------
FUNCTION UPDATE_WORKSPACE(O_error_msg           OUT  VARCHAR2,
                          I_workspace_id     IN      RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE)
   RETURN BOOLEAN IS

BEGIN

  merge into rpm_worksheet_il_workspace ws
  using (select distinct
                me.workspace_rowid,
                me.pend_cost_boolean,
                me.cost_alert,
                me.past_price_chg_date,
                me.past_cost_chg_date,
                me.pend_cost_chg_date,
                me.pend_cost_chg_cost,
                me.basis_base_cost,
                me.basis_pricing_cost,
                me.clear_boolean,
                me.promo_boolean,
                me.price_change_boolean,
                me.basis_regular_retail,
                me.basis_regular_retail_uom,
                me.basis_regular_retail_std,
                me.basis_clear_retail,
                me.basis_clear_retail_uom,
                me.basis_clear_retail_std,
                me.basis_multi_units,
                me.basis_multi_unit_retail,
                me.basis_multi_unit_uom,
                me.new_retail,
                me.new_retail_uom,
                me.new_retail_std,
                me.new_multi_unit_ind,
                me.new_multi_units,
                me.new_multi_unit_retail,
                me.new_multi_unit_uom,
                me.new_clear_mkdn_nbr,
                me.new_clear_ind,
                me.new_reset_date,
                me.new_out_of_stock_date,
                me.effective_date,
                1 update_ind
           from rpm_me_item_gtt i,
                rpm_me_consolidate_itemloc_gtt me
          where i.item = me.item
            and i.zone_id = me.zone_id) use
   on (ws.rowid = use.workspace_rowid)
  when MATCHED then
  update
      set ws.pend_cost_boolean = use.pend_cost_boolean,
          ws.cost_alert = use.cost_alert,
          ws.past_price_chg_date = use.past_price_chg_date,
          ws.past_cost_chg_date = use.past_cost_chg_date,
          ws.pend_cost_chg_date = use.pend_cost_chg_date,
          ws.pend_cost_chg_cost = use.pend_cost_chg_cost,
          ws.basis_base_cost = use.basis_base_cost,
          ws.basis_pricing_cost = use.basis_pricing_cost,
          ws.clear_boolean = use.clear_boolean,
          ws.promo_boolean = use.promo_boolean,
          ws.price_change_boolean = use.price_change_boolean,
          ws.basis_regular_retail = use.basis_regular_retail,
          ws.basis_regular_retail_uom = use.basis_regular_retail_uom,
          ws.basis_regular_retail_std = use.basis_regular_retail_std,
          ws.basis_clear_retail = use.basis_clear_retail,
          ws.basis_clear_retail_uom = use.basis_clear_retail_uom,
          ws.basis_clear_retail_std = use.basis_clear_retail_std,
          ws.basis_multi_units = use.basis_multi_units,
          ws.basis_multi_unit_retail = use.basis_multi_unit_retail,
          ws.basis_multi_unit_uom = use.basis_multi_unit_uom,
          ws.new_retail = use.new_retail,
          ws.new_retail_uom = use.new_retail_uom,
          ws.new_retail_std = use.new_retail_std,
          ws.new_multi_unit_ind = use.new_multi_unit_ind,
          ws.new_multi_units = use.new_multi_units,
          ws.new_multi_unit_retail = use.new_multi_unit_retail,
          ws.new_multi_unit_uom = use.new_multi_unit_uom,
          ws.new_clear_mkdn_nbr = use.new_clear_mkdn_nbr,
          ws.new_clear_ind = use.new_clear_ind,
          ws.new_reset_date = use.new_reset_date,
          ws.new_out_of_stock_date = use.new_out_of_stock_date,
          ws.effective_date = use.effective_date,
          ws.update_ind = use.update_ind;

   merge into rpm_worksheet_zone_workspace ws
   using (select I_workspace_id workspace_id,
                 i.dept,
                 i.item,
                 link_code,
                 i.zone_id,
                 max(basis_zl_base_cost) basis_zl_base_cost,
                 max(basis_zl_pricing_cost) basis_zl_pricing_cost,
                 sum(basis_zl_pricing_cost) sum_nmp_cost,
                 max(pend_zl_cost_chg_cost) pend_zl_cost_chg_cost,
                 max(basis_zl_regular_retail) basis_zl_regular_retail,
                 max(basis_zl_regular_retail_uom) basis_zl_regular_retail_uom,
                 max(basis_zl_regular_retail_std) basis_zl_regular_retail_std,
                 max(basis_zl_clear_retail) basis_zl_clear_retail,
                 max(basis_zl_clear_retail_uom) basis_zl_clear_retail_uom,
                 max(basis_zl_clear_retail_std) basis_zl_clear_retail_std,
                 max(basis_zl_clear_mkdn_nbr) basis_zl_clear_mkdn_nbr,
                 sum(case
                     when new_retail_std is not null
                        then new_retail_std
                        else
                           case
                           when current_clearance_ind = 1
                              then
                                 case
                                 when basis_zl_clear_retail_uom = standard_uom
                                    then basis_zl_clear_retail
                                    else null
                                 end
                              else
                                 case
                                 when basis_zl_regular_retail_uom = standard_uom
                                    then basis_zl_regular_retail
                                    else null
                                 end
                           end
                     end) as sum_nmp_retail,
                 avg(case
                     when new_retail_std is not null
                        then new_retail_std
                        else
                           case
                           when current_clearance_ind = 1
                              then
                                 case
                                 when basis_zl_clear_retail_uom = standard_uom
                                    then basis_zl_clear_retail
                                    else null
                                 end
                              else
                                 case
                                 when basis_zl_regular_retail_uom = standard_uom
                                    then basis_zl_regular_retail
                                    else null
                                 end
                           end
                     end) as avg_nmp_retail,
                 max(basis_zl_multi_units) basis_zl_multi_units,
                 max(basis_zl_multi_unit_retail) basis_zl_multi_unit_retail,
                 max(basis_zl_multi_uom) basis_zl_multi_uom,
                 max(past_price_chg_date) past_price_chg_date,
                 case max(nvl(past_price_chg_date, sysdate))
                 when max(nvl(past_price_chg_date, sysdate))
                    then 0
                    else 1
                 end as past_price_change_date_flag,
                 max(past_cost_chg_date) past_cost_chg_date,
                 case max(nvl(past_cost_chg_date, sysdate))
                 when max(nvl(past_cost_chg_date, sysdate))
                    then 0
                    else 1
                 end as past_cost_chg_date_flag,
                 min(pend_cost_chg_date) pend_cost_chg_date,
                 case max(nvl(basis_base_cost, 0))
                 when min (nvl(basis_base_cost, 0))
                    then 0
                    else 1
                 end as basis_base_cost_flag,
                 case max(nvl(basis_pricing_cost, 0))
                 when min (nvl(basis_pricing_cost, 0))
                    then 0
                    else 1
                 end as basis_pricing_cost_flag,
                 max(nvl(clear_boolean,0)) clear_boolean              ,
                 case max(greatest(nvl(clear_boolean,0), nvl(current_clearance_ind,0)))
                 when min(greatest(nvl(clear_boolean,0), nvl(current_clearance_ind,0)))
                    then 0
                    else 1
                 end as clear_flag,
                 max(nvl(promo_boolean,0)) promo_boolean              ,
                 max(nvl(price_change_boolean,0)) price_change_boolean,
                 case max(nvl(price_change_boolean, 0))
                 when min(nvl(price_change_boolean, 0))
                    then 0
                    else 1
                 end as price_change_boolean_flag,
                 case max(nvl(basis_regular_retail, 0))
                    when min(nvl(basis_regular_retail, 0))
                    then 0
                    else 1
                 end as basis_regular_retail_flag,
                 max(nvl(basis_regular_retail_uom, 0)) basis_regular_retail_uom,
                 case max(nvl(basis_regular_retail_uom, 0))
                    when min(nvl(basis_regular_retail_uom, 0))
                    then 0
                    else 1
                 end as basis_regular_retail_uom_flag,
                 case max(nvl(basis_clear_retail, 0))
                    when min(nvl(basis_clear_retail, 0))
                    then 0
                    else 1
                 end as basis_clear_retail_flag,
                 max(nvl(basis_clear_retail_uom, 0)) basis_clear_retail_uom,
                 case max(nvl(basis_clear_retail_uom, 0))
                    when min(nvl(basis_clear_retail_uom, 0))
                    then 0
                    else 1
                 end as basis_clear_retail_uom_flag,
                 case max(nvl(basis_multi_units,0))
                 when min(nvl(basis_multi_units,0))
                    then 0
                    else 1
                 end as basis_multi_units_flag,
                 case max(nvl(basis_multi_unit_retail,0))
                 when min(nvl(basis_multi_unit_retail,0))
                    then 0
                    else 1
                 end as basis_multi_unit_retail_flag,
                 max(nvl(basis_multi_unit_uom,0)) basis_multi_unit_uom,
                 case max(nvl(basis_multi_unit_uom,0))
                    when min(nvl(basis_multi_unit_uom,0))
                    then 0
                    else 1
                 end as basis_multi_unit_uom_flag,
                 case max(me.action_flag)
                 when 0
                    then sum((projected_sales * new_retail)-(basis_regular_retail * projected_sales))
                 else sum(case me.action_flag
                          when 1
                             then (projected_sales * new_retail)-(basis_regular_retail * projected_sales)
                          else 0
                          end)
                 end as v_sales_ch_amt_ncurrent,
                 case max(me.action_flag)
                 when 0
                    then sum(projected_sales)
                 else sum(case me.action_flag
                          when 1
                             then (projected_sales)
                          else 0
                          end)
                 end as v_sales_ch_proj_sales,
                 case max(me.action_flag)
                 when 0
                    then sum(basis_regular_retail * projected_sales)
                 else sum(case me.action_flag
                          when 1
                             then (basis_regular_retail * projected_sales)
                          else 0
                          end)
                 end as v_sales_ch_prj_reg_retail,
                 sum((projected_sales * new_retail)-(basis_regular_retail * projected_sales)) as sales_change_amount_ncurrent,
                 sum(basis_regular_retail * projected_sales) as sales_change_prj_reg_retail,
                 avg(new_retail) new_retail,
                 case max(nvl(new_retail, 0))
                 when min(nvl(new_retail, 0))
                    then 0
                 else 1
                 end as new_retail_flag,
                 min (case
                      when new_retail is null
                         then 0
                      else 1
                      end) as new_retail_is_set,
                 min (case
                      when new_retail is null and new_multi_unit_retail is null
                         then 0
                      else 1
                      end) as either_retail_is_set,
                 min(new_retail_uom) new_retail_uom,
                 case max(nvl(new_retail_uom,0))
                    when min(nvl(new_retail_uom,0))
                    then 0
                 else 1
                 end as new_retail_uom_flag,
                 avg(new_retail_std) new_retail_std,
                 max(new_multi_units) new_multi_units,
                 case max(nvl(new_multi_units,0))
                 when min(nvl(new_multi_units,0))
                    then 0
                 else 1
                 end as new_multi_units_flag,
                 max(new_multi_unit_retail) new_multi_unit_retail,
                 case max(nvl(new_multi_unit_retail, 0))
                 when min(nvl(new_multi_unit_retail, 0))
                    then 0
                 else 1
                 end as new_multi_unit_retail_flag,
                 min(case
                     when new_multi_unit_retail is null
                        then 0
                     else 1
                     end) as new_multi_unit_retail_is_set,
                 min(new_multi_unit_uom) new_multi_unit_uom,
                 case max(nvl(new_multi_unit_uom, 0))
                 when min(nvl(new_multi_unit_uom, 0))
                    then 0
                 else 1
                 end as new_multi_unit_uom_flag,
                 max(new_clear_mkdn_nbr) new_clear_mkdn_nbr,
                 case max(nvl(new_clear_mkdn_nbr, 0))
                 when min(nvl(new_clear_mkdn_nbr, 0))
                    then 0
                    else 1
                 end as new_clear_mkdn_nbr_flag,
                 min(nvl(new_clear_ind,0)) new_clear_ind,
                 case max(nvl(new_clear_ind, 0))
                 when min(nvl(new_clear_ind, 0))
                    then 0
                    else 1
                 end as new_clear_ind_flag,
                 min(new_reset_date) new_reset_date,
                 case max(nvl(new_reset_date, sysdate))
                 when min(nvl(new_reset_date, sysdate))
                    then 0
                    else 1
                 end as new_reset_date_flag,
                 min(new_out_of_stock_date) new_out_of_stock_date,
                 case max(nvl(new_out_of_stock_date, sysdate))
                 when min(nvl(new_out_of_stock_date, sysdate))
                    then 0
                    else 1
                 end as new_out_of_stock_date_flag,
                 min(effective_date) effective_date,
                 max(effective_date) last_effective_date        ,
                 case max(nvl(effective_date,sysdate))
                    when min(nvl(effective_date,sysdate))
                    then 0
                    else 1
                 end as effective_date_flag,
                 1 update_ind
            from rpm_me_item_gtt i,
                 rpm_me_consolidate_itemloc_gtt me
           where i.item = me.item
             and i.zone_id = me.zone_id
           group by I_workspace_id,
                    i.dept,
                    i.item,
                    link_code,
                    i.zone_id) use
   on (    ws.workspace_id = use.workspace_id
       and ws.dept = use.dept
       and ws.item = use.item
       and ws.link_code = use.link_code
       and ws.zone_id = use.zone_id)
   when matched then
   update
      set ws.basis_zl_base_cost = use.basis_zl_base_cost,
          ws.basis_zl_pricing_cost = use.basis_zl_pricing_cost,
          ws.pend_zl_cost_chg_cost = use.pend_zl_cost_chg_cost,
          ws.basis_zl_regular_retail = use.basis_zl_regular_retail,
          ws.basis_zl_regular_retail_uom = use.basis_zl_regular_retail_uom,
          ws.basis_zl_regular_retail_std = use.basis_zl_regular_retail_std,
          ws.basis_zl_clear_retail = use.basis_zl_clear_retail,
          ws.basis_zl_clear_retail_uom = use.basis_zl_clear_retail_uom,
          ws.basis_zl_clear_retail_std = use.basis_zl_clear_retail_std,
          ws.basis_zl_clear_mkdn_nbr = use.basis_zl_clear_mkdn_nbr,
          ws.basis_zl_multi_units = use.basis_zl_multi_units,
          ws.basis_zl_multi_unit_retail = use.basis_zl_multi_unit_retail,
          ws.basis_zl_multi_uom = use.basis_zl_multi_uom,
          ws.past_price_chg_date = use.past_price_chg_date,
          ws.past_cost_chg_date = use.past_cost_chg_date,
          ws.pend_cost_chg_date = use.pend_cost_chg_date,
          ws.clear_boolean = use.clear_boolean,
          ws.promo_boolean = use.promo_boolean,
          ws.price_change_boolean = use.price_change_boolean,
          ws.basis_regular_retail_uom = use.basis_regular_retail_uom,
          ws.basis_clear_retail_uom = use.basis_clear_retail_uom,
          ws.basis_multi_unit_uom = use.basis_multi_unit_uom,
          ws.new_retail = use.new_retail,
          ws.new_retail_uom = use.new_retail_uom,
          ws.new_retail_std = use.new_retail_std,
          ws.new_multi_units = use.new_multi_units,
          ws.new_multi_unit_retail = use.new_multi_unit_retail,
          ws.new_multi_unit_uom = use.new_multi_unit_uom,
          ws.new_clear_mkdn_nbr = use.new_clear_mkdn_nbr,
          ws.new_clear_ind = use.new_clear_ind,
          ws.new_reset_date = use.new_reset_date,
          ws.new_out_of_stock_date = use.new_out_of_stock_date,
          ws.effective_date = use.effective_date,
          ws.sum_nmp_retail = use.sum_nmp_retail,
          ws.avg_nmp_retail = use.avg_nmp_retail,
          ws.past_price_change_date_flag = use.past_price_change_date_flag,
          ws.past_cost_chg_date_flag = use.past_cost_chg_date_flag,
          ws.basis_base_cost_flag = use.basis_base_cost_flag,
          ws.basis_pricing_cost_flag = use.basis_pricing_cost_flag,
          ws.clear_flag = ws.clear_flag,
          ws.price_change_boolean_flag = use.price_change_boolean_flag,
          ws.basis_regular_retail_flag = use.basis_regular_retail_flag,
          ws.basis_regular_retail_uom_flag = use.basis_regular_retail_uom_flag,
          ws.basis_clear_retail_flag = use.basis_clear_retail_flag,
          ws.basis_clear_retail_uom_flag = use.basis_clear_retail_uom_flag,
          ws.basis_multi_units_flag = use.basis_multi_units_flag,
          ws.basis_multi_unit_retail_flag = use.basis_multi_unit_retail_flag,
          ws.basis_multi_unit_uom_flag = basis_multi_unit_uom_flag,
          ws.v_sales_ch_amt_ncurrent = use.v_sales_ch_amt_ncurrent,
          ws.v_sales_ch_proj_sales = use.v_sales_ch_proj_sales,
          ws.v_sales_ch_prj_reg_retail = use.v_sales_ch_prj_reg_retail,
          ws.sales_change_amount_ncurrent = use.sales_change_amount_ncurrent,
          ws.sales_change_prj_reg_retail = use.sales_change_prj_reg_retail,
          ws.new_retail_flag = use.new_retail_flag,
          ws.new_retail_is_set = use.new_retail_is_set,
          ws.either_retail_is_set = use.either_retail_is_set,
          ws.new_retail_uom_flag = use.new_retail_uom_flag,
          ws.new_multi_units_flag = use.new_multi_units_flag,
          ws.new_multi_unit_retail_flag = use.new_multi_unit_retail_flag,
          ws.new_multi_unit_retail_is_set = use.new_multi_unit_retail_is_set,
          ws.new_multi_unit_uom_flag = use.new_multi_unit_uom_flag,
          ws.new_clear_mkdn_nbr_flag = use.new_clear_mkdn_nbr_flag,
          ws.new_clear_ind_flag = use.new_clear_ind_flag,
          ws.new_reset_date_flag = use.new_reset_date_flag,
          ws.new_out_of_stock_date_flag = use.new_out_of_stock_date_flag,
          ws.last_effective_date = use.last_effective_date,
          ws.effective_date_flag = use.effective_date_flag,
          ws.update_ind = use.update_ind;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.UPDATE_WORKSPACE',
                                        to_char(SQLCODE));
      return FALSE;

END UPDATE_WORKSPACE;

----------------------------------------------------------------------------------------

FUNCTION FINISH_WORKSHEET_STATUS (O_error_msg         OUT VARCHAR2,
                                  I_me_sequence_id IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.FINISH_WORKSHEET_STATUS';

   cursor C_AGG is
      select a.strategy_id,
             a.dept,
             a.class,
             a.subclass,
             a.zone_id,
             rd.worksheet_level,
             rd.include_wh_on_hand,
             rd.price_change_amount_calc_type,
             DECODE(a.strategy_id,
                    mm.strategy_id, 1,
                    0) maint_margin_ind,
             a.me_area_diff_seq_id
        from rpm_pre_me_aggregation a,
             rpm_dept_aggregation rd,
             rpm_strategy_maint_margin mm
       where a.me_sequence_id   = I_me_sequence_id
         and a.aggregation_type = RPM_EXT_SQL.WORKSHEET_CREATE_LEVEL
         and a.dept             = rd.dept
         and a.strategy_id      = mm.strategy_id(+)
       order by a.strategy_id,
                a.me_area_diff_seq_id;

BEGIN

   delete from numeric_id_gtt;

   for rec IN C_AGG loop

      if rec.me_area_diff_seq_id = -1 then
         LP_area_diff_prim_ws_id := NULL;
      end if;

      if FINISH_WORKSHEET_STATUS(O_error_msg,
                                 rec.strategy_id,
                                 rec.dept,
                                 rec.class,
                                 rec.subclass,
                                 rec.zone_id,
                                 rec.worksheet_level,
                                 rec.include_wh_on_hand,
                                 rec.price_change_amount_calc_type,
                                 rec.maint_margin_ind) = FALSE then
         return 0;
      end if;
   end loop;

   if INSERT_WS_ZONE_DATA(O_error_msg) = FALSE then
      return 0;
   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END FINISH_WORKSHEET_STATUS;
----------------------------------------------------------------------------------------
FUNCTION FINISH_WORKSHEET_STATUS(O_error_msg              OUT VARCHAR2,
                                 I_strategy_id         IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                                 I_dept                IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                                 I_class               IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                                 I_subclass            IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                                 I_zone_id             IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                                 I_worksheet_level     IN     RPM_DEPT_AGGREGATION.WORKSHEET_LEVEL%TYPE,
                                 I_include_wh_on_hand  IN     RPM_DEPT_AGGREGATION.INCLUDE_WH_ON_HAND%TYPE,
                                 I_pc_amount_calc_type IN     RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE,
                                 I_maint_margin_ind    IN     NUMBER)
   RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.FINISH_WORKSHEET_STATUS';

   L_sys_opt_constraint_ind   RPM_SYSTEM_OPTIONS_DEF.DEF_WKSHT_PROMO_CONST_IND%TYPE;

   L_constraint_ind           RPM_WORKSHEET_STATUS.CONSTRAINT_IND%TYPE;

   L_dept               SUBCLASS.DEPT%TYPE                         := NULL;
   L_class              SUBCLASS.CLASS%TYPE                        := NULL;
   L_subclass           SUBCLASS.SUBCLASS%TYPE                     := NULL;
   L_zone_id            RPM_ZONE.ZONE_ID%TYPE                      := NULL;
   L_maint_margin_ind   RPM_WORKSHEET_STATUS.MAINT_MARGIN_IND%TYPE := NULL;
   L_undecided_wh_stock RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE    := NULL;
   L_take_wh_stock      RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE    := NULL;
   L_nottake_wh_stock   RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE    := NULL;

   L_new_worksheet_status_ind BOOLEAN;
   L_worksheet_status_id      RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE := NULL;
   L_class_name               CLASS.CLASS_NAME%TYPE                         := NULL;
   L_subclass_name            SUBCLASS.SUB_NAME%TYPE                        := NULL;

   L_stock_on_hand       RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE       := NULL;
   L_price_change_amount RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE := NULL;
   L_sales_amount        RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE        := NULL;
   L_sales_amount_ex_vat RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE := NULL;
   L_sales_cost          RPM_WORKSHEET_STATUS.SALES_COST%TYPE          := NULL;
   L_sales_change_amount RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE := NULL;
   L_item_count          RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE          := NULL;

   L_exclusion_ind RPM_CALENDAR_PERIOD.EXCLUSION_IND%TYPE := NULL;
   L_exception_ind RPM_CALENDAR_PERIOD.EXCEPTION_IND%TYPE := NULL;
   L_review_date   DATE                                   := NULL;

   cursor C_WORKSHEET_STATUS is
      select ws.worksheet_status_id
        from rpm_worksheet_status ws
       where ws.dept                = L_dept
         and NVL(ws.class, -999)    = NVL(L_class, -999)
         and NVL(ws.subclass, -999) = NVL(L_subclass, -999)
         and ws.zone_id             = L_zone_id
         and ws.maint_margin_ind    = L_maint_margin_ind;

   cursor C_NEXT_WORKSHEET_STATUS is
      select RPM_WORKSHEET_STATUS_SEQ.NEXTVAL
        from dual;

   cursor C_CLASS_NAME is
      select class_name
        from class
       where dept  = L_dept
         and class = L_class;

   cursor C_SUBCLASS_NAME is
      select sub_name
        from subclass
       where dept     = L_dept
         and class    = L_class
         and subclass = L_subclass;


   cursor C_WORKSHEET_DETAILS is
      select div.division,
             div.div_name,
             grp.group_no,
             grp.group_name,
             inner.dept,
             d.dept_name,
             d.profit_calc_type,
             inner.class,
             inner.subclass,
             zg.zone_group_id,
             zg.zone_group_display_id,
             zg.name zone_group_name,
             inner.zone_id,
             z.zone_display_id,
             z.name zone_name,
             z.currency_code currency,
             inner.pending,
             inner.undecided_count,
             inner.take_count,
             inner.nottake_count,
             inner.state_bitmap,
             inner.maint_margin_ind,
             inner.area_diff_prim_ind,
             inner.constraint_ind
        from rpm_zone_group zg,
             rpm_zone z,
             division div,
             groups grp,
             deps d,
             (select /*+ CARDINALITY(mm , 1024) */ I_strategy_id strategy_id,
                     I_dept dept,
                     DECODE(I_worksheet_level,
                            DEPT_LEVEL, NULL,
                            CLASS_LEVEL, I_class,
                            SUBCLASS_LEVEL, I_class) class,
                     DECODE(I_worksheet_level,
                            DEPT_LEVEL, NULL,
                            CLASS_LEVEL, NULL,
                            SUBCLASS_LEVEL, I_subclass) subclass,
                     wd.zone_id zone_id,
                     DECODE(COUNT(distinct wd.state),
                            1,
                            DECODE(MIN(wd.state),
                                   0, 'allpending',
                                   'notallpending'),
                            'notallpending') pending,
                     SUM(DECODE(wd.action_flag,UNDECIDED,1,0)) undecided_count,
                     SUM(DECODE(wd.action_flag,TAKE,1,0)) take_count,
                     SUM(DECODE(wd.action_flag,DONT_TAKE,1,0)) nottake_count,
                     I_maint_margin_ind maint_margin_ind,
                     DECODE(MIN(wd.area_diff_prim_id),
                            NULL, NULL,
                            DECODE(MIN(area_diff_id),
                                   NULL, 1,
                                   NULL)) area_diff_prim_ind,
                     sum(distinct power(2, state)) state_bitmap,
                     mm.strategy_id mm_strategy_id,
                     MIN(DECODE(cl.worksheet_item_data_id,
                                NULL, 0,
                                1)) constraint_ind
                from rpm_worksheet_item_data wd,
                     rpm_strategy_maint_margin mm,
                     rpm_worksheet_cand_link cl
               where wd.proposed_strategy_id    = mm.strategy_id(+)
                 and wd.zone_id                 = I_zone_id
                 and wd.worksheet_item_data_id  = cl.worksheet_item_data_id (+)
                 and (   (    I_worksheet_level  = DEPT_LEVEL
                          and wd.dept            = I_dept)
                      or (    I_worksheet_level  = CLASS_LEVEL
                          and wd.dept            = I_dept
                          and wd.class           = I_class)
                      or (    I_worksheet_level  = SUBCLASS_LEVEL
                          and wd.dept            = I_dept
                          and wd.class           = I_class
                          and wd.subclass        = I_subclass))
               group by wd.zone_id,
                        mm.strategy_id) inner
       where ((I_maint_margin_ind  = 1 and inner.mm_strategy_id is NOT NULL) or
              (I_maint_margin_ind != 1 and inner.mm_strategy_id is NULL))
         and z.zone_id               = inner.zone_id
         and z.zone_group_id         = zg.zone_group_id
         and d.dept                  = inner.dept
         and grp.group_no            = d.group_no
         and div.division            = grp.division;

BEGIN

   if RPM_SYSTEM_OPTIONS_DEF_SQL.GET_DEF_WKSHT_PROMO_CONST_IND(L_sys_opt_constraint_ind,
                                                               O_error_msg) = FALSE then
      return FALSE;
   end if;

   if GET_CALENDAR_INFO(O_error_msg,
                        I_strategy_id,
                        L_exclusion_ind, --not used
                        L_exception_ind, --not used
                        L_review_date) = FALSE then
      return FALSE;
   end if;

   for rec IN C_WORKSHEET_DETAILS loop

      L_dept             := rec.dept;
      L_class            := rec.class;
      L_subclass         := rec.subclass;
      L_zone_id          := rec.zone_id;
      L_maint_margin_ind := rec.maint_margin_ind;

      if L_sys_opt_constraint_ind = 0 and
         rec.constraint_ind = 1 then
         L_constraint_ind := 1;
      else
         L_constraint_ind := 0;
      end if;

      L_class_name := NULL;
      if L_class is NOT NULL then
         open C_CLASS_NAME;
         fetch C_CLASS_NAME into L_class_name;
         close C_CLASS_NAME;
      end if;

      L_subclass_name := NULL;
      if L_subclass is NOT NULL then
         open C_SUBCLASS_NAME;
         fetch C_SUBCLASS_NAME into L_subclass_name;
         close C_SUBCLASS_NAME;
      end if;

      L_worksheet_status_id := NULL;
      L_new_worksheet_status_ind := FALSE;

      open C_WORKSHEET_STATUS;
      fetch C_WORKSHEET_STATUS into L_worksheet_status_id;
      close C_WORKSHEET_STATUS;

      if L_worksheet_status_id is NULL then
         L_new_worksheet_status_ind := TRUE;
         open C_NEXT_WORKSHEET_STATUS;
         fetch C_NEXT_WORKSHEET_STATUS into L_worksheet_status_id;
         close C_NEXT_WORKSHEET_STATUS;
      end if;

      update rpm_worksheet_item_data wd
         set wd.worksheet_status_id = L_worksheet_status_id
       where wd.dept                 = L_dept
         and wd.class                = NVL(L_class, wd.class)
         and wd.subclass             = NVL(L_subclass, wd.subclass)
         and wd.zone_id              = L_zone_id
         and wd.proposed_strategy_id = I_strategy_id;

      update rpm_worksheet_item_loc_data wd
         set wd.worksheet_status_id = L_worksheet_status_id
       where wd.dept                 = L_dept
         and wd.class                = NVL(L_class, wd.class)
         and wd.subclass             = NVL(L_subclass, wd.subclass)
         and wd.zone_id              = L_zone_id
         and wd.proposed_strategy_id = I_strategy_id;

      if rec.area_diff_prim_ind = 1 then
         LP_area_diff_prim_ws_id := L_worksheet_status_id;
      end if;

      if rec.pending = 'allpending' then

         L_stock_on_hand       := 0;
         L_price_change_amount := 0;
         L_sales_amount        := 0;
         L_sales_amount_ex_vat := 0;
         L_sales_cost          := 0;
         L_sales_change_amount := 0;
         L_item_count          := 0;

      elsif rec.take_count > 0 then

         if WORKSHEET_TAKE_ROLLUP(O_error_msg,
                                  L_worksheet_status_id,
                                  L_dept,
                                  I_include_wh_on_hand,
                                  I_pc_amount_calc_type,
                                  L_stock_on_hand,
                                  L_price_change_amount,
                                  L_sales_amount,
                                  L_sales_amount_ex_vat,
                                  L_sales_cost,
                                  L_sales_change_amount,
                                  L_item_count) = FALSE then
            return FALSE;
         end if;

      else

         if WORKSHEET_NOT_TAKE_ROLLUP(O_error_msg,
                                      L_worksheet_status_id,
                                      L_dept,
                                      I_include_wh_on_hand,
                                      I_pc_amount_calc_type,
                                      L_stock_on_hand,
                                      L_price_change_amount,
                                      L_sales_amount,
                                      L_sales_amount_ex_vat,
                                      L_sales_cost,
                                      L_sales_change_amount,
                                      L_item_count) = FALSE then
            return FALSE;
         end if;

      end if;

      if L_new_worksheet_status_ind then

         insert into rpm_worksheet_status (
                worksheet_status_id,
                state_bitmap,
                maint_margin_ind,
                division,
                div_name,
                group_no,
                group_name,
                dept,
                dept_name,
                class,
                class_name,
                subclass,
                subclass_name,
                zone_group_id,
                zone_group_display_id,
                zone_group_name,
                zone_id,
                zone_display_id,
                zone_name,
                location,
                loc_type,
                location_name,
                area_diff_prim_ind,
                area_diff_prim_ws_id,
                stock_on_hand,
                currency,
                price_change_amount,
                sales_amount,
                sales_amount_ex_vat,
                sales_cost,
                sales_change_amount,
                review_date,
                reason,
                item_count,
                conflict_ind,
                constraint_ind,
                profit_calc_type,
                ws_lock_owner,
                ws_lock_date,
                ws_lock_workspace_id,
                lock_version)
         select L_worksheet_status_id,
                rec.state_bitmap,     -- state_bitmap,
                rec.maint_margin_ind,
                rec.division,
                rec.div_name,
                rec.group_no,
                rec.group_name,
                rec.dept,
                rec.dept_name,
                rec.class,
                L_class_name,
                rec.subclass,
                L_subclass_name,
                rec.zone_group_id,
                rec.zone_group_display_id,
                rec.zone_group_name,
                rec.zone_id,
                rec.zone_display_id,
                rec.zone_name,
                NULL,   -- location,
                NULL,   -- loc_type,
                NULL,   -- location_name,
                rec.area_diff_prim_ind,  -- area_diff_prim_ind,
                DECODE(rec.area_diff_prim_ind,
                       1, NULL,
                       LP_area_diff_prim_ws_id),   -- area_diff_prim_ws_id,
                L_stock_on_hand,
                rec.currency,
                L_price_change_amount,
                L_sales_amount,
                L_sales_amount_ex_vat,
                L_sales_cost,
                L_sales_change_amount,
                L_review_date,   --review_date,
                NULL,   --reason,
                L_item_count,
                0,      --conflict_ind,
                L_constraint_ind,   --constraint_ind,
                rec.profit_calc_type,   --profit_calc_type,
                NULL,   --ws_lock_owner,
                NULL,   --ws_lock_date,
                NULL,   --ws_lock_workspace_id,
                NULL    --lock_version
           from dual;

      else

         update rpm_worksheet_status ws
            set ws.state_bitmap          = rec.state_bitmap,
                ws.stock_on_hand         = L_stock_on_hand,
                ws.price_change_amount   = L_price_change_amount,
                ws.sales_amount          = L_sales_amount,
                ws.sales_amount_ex_vat   = L_sales_amount_ex_vat,
                ws.sales_cost            = L_sales_cost,
                ws.sales_change_amount   = L_sales_change_amount,
                ws.item_count            = L_item_count
          where ws.worksheet_status_id = L_worksheet_status_id;

         delete
           from rpm_worksheet_zone_data wz
          where wz.worksheet_status_id = L_worksheet_status_id;

      end if;

      insert into numeric_id_gtt (numeric_id)
      values (L_worksheet_status_id);

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;
END FINISH_WORKSHEET_STATUS;
----------------------------------------------------------------------------------------
FUNCTION INSERT_WS_ZONE_DATA(O_error_msg IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.INSERT_WS_ZONE_DATA';

BEGIN

   insert into rpm_worksheet_zone_data
      (worksheet_item_data_id,
       worksheet_status_id,
       proposed_strategy_id,
       user_strategy_id,
       state,
       reset_state,
       item,
       item_desc,
       dept,
       dept_name,
       markup_calc_type,
       class,
       class_name,
       subclass,
       subclass_name,
       item_level,
       tran_level,
       standard_uom,
       diff_1,
       diff_type_1,
       diff_2,
       diff_type_2,
       diff_3,
       diff_type_3,
       diff_4,
       diff_type_4,
       retail_include_vat_ind,
       primary_supplier,
       vpn,
       uda_boolean,
       package_size,
       package_uom,
       retail_label_type,
       retail_label_value,
       season_phase_boolean,
       item_parent,
       parent_desc,
       parent_diff_1,
       parent_diff_2,
       parent_diff_3,
       parent_diff_4,
       parent_diff_1_type,
       parent_diff_2_type,
       parent_diff_3_type,
       parent_diff_4_type,
       parent_diff_1_type_desc,
       parent_diff_2_type_desc,
       parent_diff_3_type_desc,
       parent_diff_4_type_desc,
       parent_vpn,
       primary_comp_retail,
       primary_comp_store,
       primary_comp_retail_uom,
       primary_comp_retail_std,
       primary_multi_units,
       primary_multi_unit_retail,
       primary_multi_unit_retail_uom,
       a_comp_store,
       b_comp_store,
       c_comp_store,
       d_comp_store,
       e_comp_store,
       a_comp_retail,
       b_comp_retail,
       c_comp_retail,
       d_comp_retail,
       e_comp_retail,
       zone_group_id,
       zone_group_display_id,
       zone_group_name,
       zone_id,
       zone_display_id,
       zone_name,
       loc_type,
       currency,
       original_retail,
       current_regular_retail_flag,
       current_regular_retail_uom,
       current_reg_retail_uom_flag,
       current_clear_retail_flag,
       current_clear_retail_uom,
       current_clear_retail_uom_flag,
       current_cost_markup_flag,
       current_multi_unit_flag,
       current_multi_unit_retail_flag,
       current_multi_unit_uom,
       current_multi_unit_uom_flag,
       current_zl_regular_retail,
       current_zl_regular_retail_uom,
       current_zl_regular_retail_std,
       current_zl_clear_retail,
       current_zl_clear_retail_uom,
       current_zl_clear_retail_std,
       current_zl_retail_std,
       current_zl_multi_units,
       current_zl_multi_unit_retail,
       current_zl_multi_uom,
       past_price_chg_date,
       past_price_change_date_flag,
       past_cost_chg_date,
       past_cost_chg_date_flag,
       pend_cost_chg_date,
       pend_zl_cost_chg_cost,
       maint_margin_zl_retail,
       maint_margin_zl_retail_uom,
       maint_margin_zl_retail_std,
       maint_margin_zl_cost,
       current_cost_flag,
       basis_base_cost_flag,
       basis_pricing_cost_flag,
       current_zl_cost,
       current_zl_cost_zone,
       basis_zl_base_cost,
       basis_zl_pricing_cost,
       current_clearance_ind,
       historical_sales,
       store_historical_sales_units,
       wh_historical_sales_units,
       projected_sales,
       store_seasonal_sales,
       wh_seasonal_sales,
       first_received_date,
       first_received_date_flag,
       last_received_date,
       last_received_date_flag,
       wks_of_sales_exp,
       wks_of_sales_exp_flag,
       store_stock,
       wh_stock,
       store_on_order,
       wh_on_order,
       store_inventory,
       wh_inventory,
       ref_wh_stock,
       ref_wh_stock_value,
       ref_wh_stock_count,
       ref_wh_on_order,
       ref_wh_on_order_value,
       ref_wh_on_order_count,
       ref_wh_inventory,
       margin_mkt_basket_code,
       comp_mkt_basket_code,
       link_code,
       link_code_display_id,
       link_code_desc,
       replenish_ind,
       replenish_ind_flag,
       vat_rate,
       vat_value,
       clear_boolean,
       clear_flag,
       wks_first_sale,
       wks_first_sale_flag,
       promo_boolean,
       price_change_boolean,
       price_change_boolean_flag,
       basis_regular_retail_flag,
       basis_regular_retail_uom,
       basis_regular_retail_uom_flag,
       basis_clear_retail_flag,
       basis_clear_retail_uom,
       basis_clear_retail_uom_flag,
       basis_multi_units_flag,
       basis_multi_unit_retail_flag,
       basis_multi_unit_uom,
       basis_multi_unit_uom_flag,
       basis_zl_regular_retail,
       basis_zl_regular_retail_uom,
       basis_zl_regular_retail_std,
       basis_zl_clear_retail,
       basis_zl_clear_retail_uom,
       basis_zl_clear_retail_std,
       basis_zl_clear_mkdn_nbr,
       basis_zl_multi_units,
       basis_zl_multi_unit_retail,
       basis_zl_multi_uom,
       proposed_retail,
       proposed_retail_flag,
       proposed_retail_uom,
       proposed_retail_uom_flag,
       proposed_retail_std,
       proposed_clear_mkdn_nbr,
       proposed_clear_mkdn_nbr_flag,
       proposed_zl_reset_date,
       proposed_zl_out_of_stock_date,
       action_flag,
       action_flag_varies,
       effective_date,
       last_effective_date,
       effective_date_flag,
       effective_date_is_set,
       new_retail,
       new_retail_flag,
       new_retail_is_set,
       either_retail_is_set,
       new_retail_uom,
       new_retail_uom_flag,
       new_multi_units,
       new_multi_units_flag,
       new_multi_unit_retail,
       new_multi_unit_retail_flag,
       new_multi_unit_retail_is_set,
       new_multi_unit_uom,
       new_multi_unit_uom_flag,
       new_retail_std,
       sum_nmp_retail,
       avg_nmp_retail,
       sum_nmp_cost,
       new_clear_mkdn_nbr,
       new_clear_mkdn_nbr_flag,
       new_clear_ind,
       new_clear_ind_flag,
       new_reset_date,
       new_reset_date_flag,
       new_out_of_stock_date,
       new_out_of_stock_date_flag,
       new_item_loc_boolean,
       new_item_loc_boolean_flag,
       primary_comp_boolean,
       rule_boolean,
       conflict_boolean,
       conflict_boolean_flag,
       constraint_boolean,
       constraint_boolean_flag,
       ignore_constraint_boolean,
       area_diff_prim_id,
       area_diff_id,
       margin_mbc_name,
       competitive_mbc_name,
       reviewed,
       v_sales_ch_amt_ncurrent,
       v_sales_ch_proj_sales,
       v_sales_ch_prj_reg_retail,
       sales_change_amount_ncurrent,
       sales_change_prj_reg_retail,
       detail_count)
   select rwi.worksheet_item_data_id ,
          rwi.worksheet_status_id,
          MAX(rwi.proposed_strategy_id),
          MAX(user_strategy_id),
          MAX(state),
          MAX(reset_state),
          rwi.item,
          MAX(item_desc),
          rwi.dept,
          MAX(dept_name),
          MAX(markup_calc_type),
          MAX(rwi.class),
          MAX(class_name),
          MAX(rwi.subclass),
          MAX(subclass_name),
          MAX(item_level),
          MAX(tran_level),
          MAX(standard_uom),
          MAX(diff_1),
          MAX(diff_type_1),
          MAX(diff_2),
          MAX(diff_type_2),
          MAX(diff_3),
          MAX(diff_type_3),
          MAX(diff_4),
          MAX(diff_type_4),
          MAX(retail_include_vat_ind),
          MAX(primary_supplier),
          MAX(vpn),
          MAX(uda_boolean),
          MAX(package_size),
          MAX(package_uom),
          MAX(retail_label_type),
          MAX(retail_label_value),
          MAX(season_phase_boolean),
          MAX(rwi.item_parent),
          MAX(parent_desc),
          MAX(parent_diff_1),
          MAX(parent_diff_2),
          MAX(parent_diff_3),
          MAX(parent_diff_4),
          MAX(parent_diff_1_type),
          MAX(parent_diff_2_type),
          MAX(parent_diff_3_type),
          MAX(parent_diff_4_type),
          MAX(parent_diff_1_type_desc),
          MAX(parent_diff_2_type_desc),
          MAX(parent_diff_3_type_desc),
          MAX(parent_diff_4_type_desc),
          MAX(parent_vpn),
          MAX(primary_comp_retail),
          MAX(primary_comp_store),
          MAX(primary_comp_retail_uom),
          MAX(primary_comp_retail_std),
          MAX(primary_multi_units),
          MAX(primary_multi_unit_retail),
          MAX(primary_multi_unit_retail_uom),
          MAX(a_comp_store),
          MAX(b_comp_store),
          MAX(c_comp_store),
          MAX(d_comp_store),
          MAX(e_comp_store),
          MAX(a_comp_retail),
          MAX(b_comp_retail),
          MAX(c_comp_retail),
          MAX(d_comp_retail),
          MAX(e_comp_retail),
          MAX(zone_group_id),
          MAX(zone_group_display_id),
          MAX(zone_group_name),
          rwi.zone_id,
          MAX(zone_display_id),
          MAX(zone_name),
          MAX(loc_type),
          MAX(currency),
          MAX(original_retail),
          case MAX(NVL(current_regular_retail, 0))
             when MIN(NVL(current_regular_retail, 0)) then
                0
             else
                1
          end as current_regular_retail_flag,
          MAX(NVL(current_regular_retail_uom, 0)) current_regular_retail_uom,
          case MAX(NVL(current_regular_retail_uom, 0))
             when MIN(NVL(current_regular_retail_uom, 0)) then
                0
             else
                1
          end as current_reg_retail_uom_flag,
          case MAX(NVL(current_clear_retail, 0))
             when MIN(NVL(current_clear_retail, 0)) then
                0
             else
                1
          end as current_clear_retail_flag,
          MAX(NVL(current_clear_retail_uom, 0)) current_clear_retail_uom,
          case MAX(NVL(current_clear_retail_uom, 0))
             when MIN(NVL(current_clear_retail_uom, 0)) then
                0
             else
                1
          end as current_clear_retail_uom_flag,
          case MAX((case
                       when current_clearance_ind = 1 then
                          NVL(current_clear_retail, 0)
                       else
                          NVL(current_regular_retail, 0)
                    end))
             when MIN((case
                          when current_clearance_ind = 1 then
                             NVL(current_clear_retail, 0)
                          else
                             NVL(current_regular_retail, 0)
                       end)) then
                0
             else
                1
          end as current_cost_markup_flag,
          case MAX(NVL(current_multi_units, 0))
             when MIN(NVL(current_multi_units, 0)) then
                0
             else
                1
          end as current_multi_unit_flag,
          case MAX(NVL(current_multi_unit_retail, 0))
             when MIN(NVL(current_multi_unit_retail, 0)) then
                0
             else
                1
          end as current_multi_unit_retail_flag,
          MAX(NVL(current_multi_unit_uom, 0)) current_multi_unit_uom,
          case MAX(NVL(current_multi_unit_uom, 0))
             when MIN(NVL(current_multi_unit_uom, 0)) then
                0
             else
                1
          end as current_multi_unit_uom_flag,
          MAX(current_zl_regular_retail),
          MAX(current_zl_regular_retail_uom),
          MAX(current_zl_regular_retail_std),
          MAX(current_zl_clear_retail),
          MAX(current_zl_clear_retail_uom),
          MAX(current_zl_clear_retail_std),
          SUM(case
                 when current_clearance_ind = 1 then
                    current_zl_clear_retail_std
                 else
                    current_zl_regular_retail_std
              end) current_zl_retail_std,
          MAX(current_zl_multi_units),
          MAX(current_zl_multi_unit_retail),
          MAX(current_zl_multi_uom),
          MAX(past_price_chg_date),
          case MAX(NVL(past_price_chg_date, sysdate))
             when MAX(NVL(past_price_chg_date, sysdate)) then
                0
             else
                1
          end as past_price_change_date_flag,
          MAX(past_cost_chg_date),
          case MAX(NVL(past_cost_chg_date, sysdate))
          when MAX(NVL(past_cost_chg_date, sysdate))
             then 0
             else 1
          end as past_cost_chg_date_flag,
          MIN(pend_cost_chg_date),
          MAX(pend_zl_cost_chg_cost),
          MAX(maint_margin_zl_retail),
          MAX(maint_margin_zl_retail_uom),
          MAX(maint_margin_zl_retail_std),
          MAX(maint_margin_zl_cost),
          case MAX(NVL(current_cost, 0))
             when MIN(NVL(current_cost, 0)) then
                0
             else
                1
          end as current_cost_flag,
          case MAX(NVL(basis_base_cost, 0))
             when MIN(NVL(basis_base_cost, 0)) then
                0
             else
                1
          end as basis_base_cost_flag,
          case MAX(NVL(basis_pricing_cost, 0))
             when MIN(NVL(basis_pricing_cost, 0)) then
                0
             else
                1
          end as basis_pricing_cost_flag,
          MAX(current_zl_cost) current_zl_cost,
          SUM(current_zl_cost) current_zl_cost_zone,
          MAX(basis_zl_base_cost) basis_zl_base_cost,
          MAX(basis_zl_pricing_cost) basis_zl_pricing_cost,
          MAX(current_clearance_ind) current_clearance_ind,
          SUM(historical_sales) historical_sales,
          SUM(case
                 when loc_type = 0 then
                    historical_sales_units
                 else
                    0
              end) store_historical_sales_units,
          SUM(case
                 when loc_type = 2 then
                    historical_sales_units
                 else
                    0
                 end) wh_historical_sales_units,
          SUM(projected_sales),
          SUM(case
                 when loc_type = 0 then
                    seasonal_sales
                 else
                    0
              end) store_seasonal_sales,
          SUM(case
                 when loc_type = 2 then
                    seasonal_sales
                 else
                    0
              end) wh_seasonal_sales,
          MIN(first_received_date),
          case MAX(NVL(first_received_date, sysdate))
             when MIN(NVL(first_received_date, sysdate)) then
                0
             else
                1
          end as first_received_date_flag,
          MAX(last_received_date),
          case MAX(NVL(last_received_date, sysdate))
             when MIN(NVL(last_received_date, sysdate)) then
                0
             else
                1
          end as last_received_date_flag,
          MAX(wks_of_sales_exp),
          case MAX(NVL(wks_of_sales_exp, 0))
             when MIN(NVL(wks_of_sales_exp, 0)) then
                0
             else
                1
          end as wks_of_sales_exp_flag,
          SUM(case
                 when loc_type = 0 then
                    location_stock
                 else
                    0
              end) store_stock,
          SUM(case
                 when loc_type = 2 then
                    location_stock
                 else
                    0
              end) wh_stock,
          SUM(case
                 when loc_type = 0 then
                    location_on_order
                 else
                    0
              end) store_on_order,
          SUM(case
                 when loc_type = 2 then
                    location_on_order
                 else 0
              end) wh_on_order,
          SUM(case
                 when loc_type = 0 then
                    location_inventory
                 else
                    0
              end) store_inventory,
          SUM(case
                 when loc_type = 2 then
                    location_inventory
                 else
                    0
              end) wh_inventory,
          SUM(ref_wh_stock) ref_wh_stock,
          MAX(ref_wh_stock) ref_wh_stock_value,
          COUNT(ref_wh_stock) ref_wh_stock_count,
          SUM(ref_wh_on_order) ref_wh_on_order,
          MAX(ref_wh_on_order) ref_wh_on_order_value,
          COUNT(ref_wh_on_order) ref_wh_on_order_count,
          MAX(ref_wh_inventory),
          MAX(margin_mkt_basket_code),
          MAX(comp_mkt_basket_code),
          link_code,
          MAX(link_code_display_id),
          MAX(link_code_desc),
          MAX(replenish_ind),
          case MAX(NVL(replenish_ind, 0))
             when MIN(NVL(replenish_ind, 0)) then
                0
             else
                1
          end as replenish_ind_flag,
          AVG(vat_rate),
          AVG(vat_value),
          MAX(NVL(clear_boolean, 0)) clear_boolean,
          case MAX(greatest(NVL(clear_boolean, 0), NVL(current_clearance_ind,0)))
             when MIN(greatest(NVL(clear_boolean, 0), NVL(current_clearance_ind,0)))
                then
                   0
                else
                   1
          end as clear_flag,
          MAX(wks_first_sale),
          case MAX(NVL(wks_first_sale, 0))
             when MIN(NVL(wks_first_sale, 0))
                then
                   0
                else
                   1
          end as wks_first_sale_flag,
          MAX(NVL(promo_boolean, 0)) promo_boolean,
          MAX(NVL(price_change_boolean, 0)) price_change_boolean,
          case MAX(NVL(price_change_boolean, 0))
             when MIN(NVL(price_change_boolean, 0))
                then
                   0
                else
                   1
          end as price_change_boolean_flag,
          case MAX(NVL(basis_regular_retail, 0))
             when MIN(NVL(basis_regular_retail, 0)) then
                0
             else
                1
          end as basis_regular_retail_flag,
          MAX(NVL(basis_regular_retail_uom, 0)) basis_regular_retail_uom,
          case MAX(NVL(basis_regular_retail_uom, 0))
             when MIN(NVL(basis_regular_retail_uom, 0)) then
                0
             else
                1
          end as basis_regular_retail_uom_flag,
          case MAX(NVL(basis_clear_retail, 0))
             when MIN(NVL(basis_clear_retail, 0)) then
                0
             else
                1
          end as basis_clear_retail_flag,
          MAX(NVL(basis_clear_retail_uom, 0)) basis_clear_retail_uom,
          case MAX(NVL(basis_clear_retail_uom, 0))
             when MIN(NVL(basis_clear_retail_uom, 0)) then
                0
             else
                1
          end as basis_clear_retail_uom_flag,
          case MAX(NVL(basis_multi_units, 0))
             when MIN(NVL(basis_multi_units, 0)) then
                0
             else
                1
          end as basis_multi_units_flag,
          case MAX(NVL(basis_multi_unit_retail, 0))
             when MIN(NVL(basis_multi_unit_retail, 0)) then
                0
             else
                1
          end as basis_multi_unit_retail_flag,
          MAX(NVL(basis_multi_unit_uom, 0)) basis_multi_unit_uom,
          case MAX(NVL(basis_multi_unit_uom,0))
             when MIN(NVL(basis_multi_unit_uom,0)) then
                0
             else
                1
          end as basis_multi_unit_uom_flag,
          MAX(basis_zl_regular_retail),
          MAX(basis_zl_regular_retail_uom),
          MAX(basis_zl_regular_retail_std),
          MAX(basis_zl_clear_retail),
          MAX(basis_zl_clear_retail_uom),
          MAX(basis_zl_clear_retail_std),
          MAX(basis_zl_clear_mkdn_nbr),
          MAX(basis_zl_multi_units),
          MAX(basis_zl_multi_unit_retail),
          MAX(basis_zl_multi_uom),
          AVG(proposed_retail),
          case MAX(NVL(proposed_retail, 0))
             when MIN(NVL(proposed_retail, 0)) then
                0
             else
                1
          end as proposed_retail_flag,
          MIN(proposed_retail_uom),
          case MAX(NVL(proposed_retail_uom, 0))
             when MIN(NVL(proposed_retail_uom, 0)) then
                0
             else
                1
          end as proposed_retail_uom_flag,
          SUM(proposed_retail_std),
          MAX(proposed_clear_mkdn_nbr),
          case MAX(NVL(proposed_clear_mkdn_nbr, 0))
             when MIN(NVL(proposed_clear_mkdn_nbr, 0)) then
                0
             else
                1
          end as proposed_clear_mkdn_nbr_flag,
          MIN(proposed_zl_reset_date),
          MIN(proposed_zl_out_of_stock_date),
          MAX(rwil.action_flag),
          case MAX(NVL(rwil.action_flag, 0))
             when MIN(NVL(rwil.action_flag, 0)) then
                0
             else
                1
          end as action_flag_varies,
          MIN(effective_date),
          MAX(effective_date) last_effective_date,
          case MAX(NVL(effective_date, sysdate))
             when MIN(NVL(effective_date, sysdate)) then
                0
             else
                1
          end as effective_date_flag        ,
          MIN(case
                 when effective_date is NULL then
                    0
                 else
                    1
              end) as effective_date_is_set      ,
          AVG(new_retail)            ,
          case MAX(NVL(new_retail, 0))
             when MIN(NVL(new_retail, 0))
                then
                   0
                else
                   1
          end as new_retail_flag,
          MIN (case
                  when new_retail is NULL then
                     0
                  else
                     1
               end) as new_retail_is_set,
          MIN (case
                  when new_retail is NULL and
                  new_multi_unit_retail is NULL then
                     0
                  else
                     1
               end) as either_retail_is_set,
          MIN(new_retail_uom),
          case MAX(NVL(new_retail_uom, 0))
             when MIN(NVL(new_retail_uom, 0)) then
                0
             else
                1
          end as new_retail_uom_flag,
          MAX(new_multi_units),
          case MAX(NVL(new_multi_units, 0))
             when MIN(NVL(new_multi_units, 0)) then
                0
             else
                1
          end as new_multi_units_flag,
          MAX(new_multi_unit_retail),
          case MAX(NVL(new_multi_unit_retail, 0))
             when MIN(NVL(new_multi_unit_retail, 0)) then
                0
             else
                1
          end as new_multi_unit_retail_flag,
          MIN(case
                 when new_multi_unit_retail is NULL then
                    0
                 else
                    1
              end) as new_multi_unit_retail_is_set,
          MIN(new_multi_unit_uom)    ,
          case MAX(NVL(new_multi_unit_uom, 0))
             when MIN(NVL(new_multi_unit_uom, 0))
                then
                   0
                else
                   1
          end as new_multi_unit_uom_flag,
          avg(new_retail_std)        ,
          SUM(case
                 when new_retail_std is NOT NULL then
                    new_retail_std
                 else
                    case
                       when current_clearance_ind = 1 then
                          case
                             when basis_zl_clear_retail_uom = standard_uom then
                                basis_zl_clear_retail
                             else
                                NULL
                             end
                       else
                          case
                             when basis_zl_regular_retail_uom = standard_uom then
                                basis_zl_regular_retail
                             else
                                NULL
                          end
                    end
              end) as sum_nmp_retail,
          AVG(case
                 when new_retail_std is NOT NULL then
                    new_retail_std
                 else
                    case
                       when current_clearance_ind = 1 then
                          case
                             when basis_zl_clear_retail_uom = standard_uom then
                                basis_zl_clear_retail
                             else
                                NULL
                          end
                       else
                          case
                             when basis_zl_regular_retail_uom = standard_uom then
                                basis_zl_regular_retail
                             else
                                NULL
                          end
                    end
              end) as avg_nmp_retail,
          SUM(basis_zl_pricing_cost) sum_nmp_cost,
          MAX(new_clear_mkdn_nbr),
          case MAX(NVL(new_clear_mkdn_nbr, 0))
             when MIN(NVL(new_clear_mkdn_nbr, 0))
                then
                   0
                else
                   1
          end as new_clear_mkdn_nbr_flag,
          MIN(NVL(new_clear_ind,0)),
          case MAX(NVL(new_clear_ind, 0))
             when MIN(NVL(new_clear_ind, 0))
                then
                   0
                else
                   1
          end as new_clear_ind_flag,
          MIN(new_reset_date),
          case MAX(NVL(new_reset_date, sysdate))
             when MIN(NVL(new_reset_date, sysdate))
                then
                   0
                else
                   1
          end as new_reset_date_flag,
          MIN(new_out_of_stock_date),
          case MAX(NVL(new_out_of_stock_date, sysdate))
             when MIN(NVL(new_out_of_stock_date, sysdate)) then
                0
             else
                1
          end as new_out_of_stock_date_flag,
          MAX(new_item_loc_boolean),
          case MAX(NVL(new_item_loc_boolean, 0))
             when MIN(NVL(new_item_loc_boolean, 0)) then
                0
             else
                1
          end as new_item_loc_boolean_flag,
          MAX(primary_comp_boolean),
          MAX(NVL(rule_boolean,0)),
          MAX(NVL(conflict_boolean,0)),
          case MAX(NVL(conflict_boolean, 0))
             when MIN(NVL(conflict_boolean, 0)) then
                0
             else
                1
          end as conflict_boolean_flag,
          MAX(NVL(constraint_boolean,0)),
          case MAX(NVL(constraint_boolean, 0))
             when MIN(NVL(constraint_boolean, 0)) then
                0
             else
                1
          end as constraint_boolean_flag,
          MAX(NVL(ignore_constraint_boolean,0)),
          MAX(area_diff_prim_id),
          MAX(area_diff_id),
          MAX(margin_mbc_name),
          MAX(competitive_mbc_name),
          MAX(reviewed),
          case MAX(rwil.action_flag)
             when 0 then
                SUM((projected_sales * new_retail)-(basis_regular_retail * projected_sales))
             else
                SUM(case rwil.action_flag
                       when 1 then
                          (projected_sales * new_retail)-(basis_regular_retail * projected_sales)
                       else
                          0
                    end)
          end as v_sales_ch_amt_ncurrent,
          case MAX(rwil.action_flag)
             when 0 then
                SUM(projected_sales)
             else SUM(case rwil.action_flag
                         when 1 then
                            (projected_sales)
                         else
                            0
                      end)
          end as v_sales_ch_proj_sales,
          case MAX(rwil.action_flag)
             when 0 then
                SUM(basis_regular_retail * projected_sales)
             else SUM(case rwil.action_flag
                         when 1 then
                            (basis_regular_retail * projected_sales)
                         else
                            0
                     end)
          end as v_sales_ch_prj_reg_retail,
          SUM((projected_sales * new_retail)-(basis_regular_retail * projected_sales)) as sales_change_amount_ncurrent,
          SUM(basis_regular_retail * projected_sales) as sales_change_prj_reg_retail,
          COUNT(1) detail_count
     from rpm_worksheet_item_data rwi,
          rpm_worksheet_item_loc_data rwil
    where rwi.worksheet_status_id IN (select distinct numeric_id
                                        from numeric_id_gtt)
      and rwil.dept                   = rwi.dept
      and rwil.worksheet_status_id    = rwi.worksheet_status_id
      and rwil.worksheet_item_data_id = rwi.worksheet_item_data_id
    group by rwi.dept,
             rwi.worksheet_status_id,
             rwi.worksheet_item_data_id,
             rwil.link_code,
             rwi.item,
             rwi.zone_id;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END INSERT_WS_ZONE_DATA;
----------------------------------------------------------------------------------------
FUNCTION RUN_AREA_DIFF(O_error_msg         IN OUT VARCHAR2,
                       I_me_sequence_id    IN     NUMBER,
                       I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE,
                       I_dept              IN     RPM_WORKSHEET_ITEM_DATA.DEPT%TYPE,
                       I_class             IN     RPM_WORKSHEET_ITEM_DATA.CLASS%TYPE,
                       I_subclass          IN     RPM_WORKSHEET_ITEM_DATA.SUBCLASS%TYPE,
                       I_zone_id           IN     RPM_WORKSHEET_ITEM_DATA.ZONE_ID%TYPE,
                       I_location          IN     RPM_WORKSHEET_ITEM_LOC_DATA.LOCATION%TYPE,
                       I_review_start_date IN     DATE,
                       I_eff_date          IN     DATE)
RETURN BOOLEAN IS

   L_program VARCHAR2(30) := 'RPM_EXT_SQL.RUN_AREA_DIFF';

   L_area_diff_prim_id  RPM_AREA_DIFF_PRIM.AREA_DIFF_PRIM_ID%TYPE := NULL;
   L_area_diff_id       RPM_AREA_DIFF.AREA_DIFF_ID%TYPE           := NULL;
   L_secondary_type     RPM_AREA_DIFF.ZONE_HIER_TYPE%TYPE         := NULL;
   L_secondary_id       RPM_AREA_DIFF.ZONE_HIER_ID%TYPE           := NULL;
   L_dept               RPM_AREA_DIFF_PRIM.DEPT%TYPE              := NULL;
   L_class              RPM_AREA_DIFF_PRIM.CLASS%TYPE             := NULL;
   L_subclass           RPM_AREA_DIFF_PRIM.SUBCLASS%TYPE          := NULL;
   L_secondary_location RPM_ZONE_LOCATION.LOCATION%TYPE           := NULL;

   L_first BOOLEAN := TRUE;

   cursor C_PRIMARY_AREA is
      select area_diff_prim_id,
             dept,
             class,
             subclass
        from rpm_area_diff_prim
       where zone_hier_type = HIER_ZONE
         and zone_hier_id   = I_zone_id
         and dept           = I_dept
         and class          is NULL
         and subclass       is NULL
         and I_zone_id      is NOT NULL
      union all
      select area_diff_prim_id,
             dept,
             class,
             subclass
        from rpm_area_diff_prim
       where zone_hier_type = HIER_ZONE
         and zone_hier_id   = I_zone_id
         and dept           = I_dept
         and class          = I_class
         and subclass       is NULL
         and I_zone_id      is NOT NULL
      union all
      select area_diff_prim_id,
             dept,
             class,
             subclass
        from rpm_area_diff_prim
       where zone_hier_type = HIER_ZONE
         and zone_hier_id   = I_zone_id
         and dept           = I_dept
         and class          = I_class
         and subclass       = I_subclass
         and I_zone_id      is NOT NULL;

   cursor C_SECONDARY_AREA is
      select area_diff_id,
             zone_hier_type,
             zone_hier_id
        from rpm_area_diff
       where area_diff_prim_id = L_area_diff_prim_id;

BEGIN

   for rec IN C_PRIMARY_AREA loop

      L_area_diff_prim_id := rec.area_diff_prim_id;
      L_dept              := rec.dept;
      L_class             := rec.class;
      L_subclass          := rec.subclass;

      -- Strategy Zone/Location has corresponding Secondary Zone/Locations associated with it
      -- So, create worksheet data for Secondary Zone/Locations as well. Note that the Strategy
      -- hierarchy level and the Primary area hierarchy level must match.
      LP_area_diff_prim_id := L_area_diff_prim_id;

      -- Save the primary item/loc/link-code information in global temp table
      if L_first then -- only do this once
         insert into rpm_area_temp_item_loc (item,
                                             loc,
                                             link_code,
                                             proposed_retail,
                                             proposed_retail_uom)
         select item,
                location,
                link_code,
                NVL(proposed_retail,basis_regular_retail),
                NVL(proposed_retail_uom,basis_regular_retail_uom)
           from rpm_me_consolidate_itemloc_gtt;

         L_first := FALSE;

      end if;

      -- Process Secondary areas
      open C_SECONDARY_AREA;
      loop
         fetch C_SECONDARY_AREA into L_area_diff_id,
                                     L_secondary_type,
                                     L_secondary_id;
         exit when C_SECONDARY_AREA%NOTFOUND;

         LP_area_diff_id := L_area_diff_id;

         if EXTRACT(O_error_msg,
                    I_me_sequence_id,
                    I_strategy_id,
                    L_dept,
                    NVL(L_class, I_class),
                    NVL(L_subclass, I_subclass),
                    L_secondary_id,
                    NULL,
                    I_review_start_date,
                    I_eff_date,
                    'N') = 0 then
            return FALSE;
         end if;

      end loop;
      close C_SECONDARY_AREA;

   end loop;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END RUN_AREA_DIFF;
----------------------------------------------------------------------------------------
FUNCTION UPDATE_EFFECTIVE_DATES (O_error_msg                OUT  VARCHAR2,
                                 I_obj_eff_dates_upd_tbl IN      OBJ_EFF_DATES_UPD_TBL)
RETURN NUMBER IS

L_edut                   OBJ_EFF_DATES_UPD_TBL;
L_workspace_id           NUMBER(20);
L_proposed_strategy_id   NUMBER(10);
L_dept                   NUMBER(4);
L_zone_id                NUMBER(10);
L_effective_date         DATE;

cursor C_LUW is
select distinct workspace_id,
       proposed_strategy_id,
       dept,
       effective_date
  from table(cast(I_obj_eff_dates_upd_tbl as OBJ_EFF_DATES_UPD_TBL));

cursor C_EDUT_CHUNK is
select OBJ_EFF_DATES_UPD_REC(workspace_id,
                                link_code,
                                item_parent,
                                diff_id,
                                item,
                                zone_id,
                                effective_date,
                                dept,
                                proposed_strategy_id)
  from table(cast(I_obj_eff_dates_upd_tbl as OBJ_EFF_DATES_UPD_TBL))
 where workspace_id = L_workspace_id
   and proposed_strategy_id = L_proposed_strategy_id
   and dept = L_dept
   and effective_date = L_effective_date;

BEGIN


   for c_rec in C_LUW loop

      L_workspace_id := c_rec.workspace_id;
      L_proposed_strategy_id := c_rec.proposed_strategy_id;
      L_dept := c_rec.dept;
      L_effective_date := c_rec.effective_date;

      open C_EDUT_CHUNK;
      fetch C_EDUT_CHUNK bulk collect into L_edut;
      close C_EDUT_CHUNK;

      --

      LP_review_not_started := FALSE;

      if GET_DEPT_AGG(O_error_msg,
                      L_dept) = FALSE then
         return 0;
      end if;

      LP_record_item_loc_strategy := L_proposed_strategy_id;

      -- Refresh all Data Strucutres
      if REFRESH_DATA_STRUCTURES(O_error_msg,
                                 'Y') = FALSE then
         return 0;
      end if;

      if POPULATE_STRATEGY_TYPE(O_error_msg,
                                L_proposed_strategy_id) = FALSE then
         return 0;
      end if;

      -- Get rpm_worksheet_zone_workspace and rpm_worksheet_il_workspace rows
      if INIT_EFFECTIVE_DATE_UPDATE(O_error_msg,
                                    L_edut) = FALSE then
         return 0;
      end if;

      if POPULATE_IL_STAT_RPM_IND(O_error_msg) = FALSE then
         return 0;
      end if;

      -- Get future_cost data
      if POPULATE_ITEM_COST(O_error_msg,
                            LP_vdate,
                            L_effective_date,
                            'Y') = FALSE then
         return 0;
      end if;

      -- Get future_retail data
      if POPULATE_PRICING_INFO(O_error_msg,
                               LP_vdate,
                               L_dept) = FALSE then
         return 0;
      end if;

      if CONSOLIDATE_GTT(O_error_msg) = FALSE then
         return 0;
      end if;

      if CONSOLIDATE_ITEM_ROLLUP(O_error_msg) = FALSE then
         return 0;
      end if;

      -- Update rpm_worksheet_zone_workspace and rpm_worksheet_il_workspace
      if UPDATE_WORKSPACE(O_error_msg,
                          L_workspace_id) = FALSE then
         return 0;
      end if;

   end loop;

   --

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.UPDATE_EFFECTIVE_DATES',
                                        to_char(SQLCODE));
      return 0;
END UPDATE_EFFECTIVE_DATES;

-------------------------------------------------------------------------------------------
FUNCTION RESET_WORKSPACE (O_error_msg        OUT  VARCHAR2,
                          I_workspace_id  IN      RPM_WORKSHEET_ZONE_WORKSPACE.WORKSPACE_ID%TYPE)
   RETURN NUMBER IS

   L_dept                  RPM_WORKSHEET_STATUS.DEPT%TYPE                := NULL;
   L_effective_date        RPM_WORKSHEET_ITEM_LOC_DATA.EFFECTIVE_DATE%TYPE  := NULL;
   L_zone_id               RPM_ZONE.ZONE_ID%TYPE                         := NULL;

   cursor C_WORKSHEET_STATUS is
      select distinct rww.worksheet_status_id,
                      rww.dept,
                      rww.proposed_strategy_id
        from rpm_worksheet_zone_workspace rww
       where rww.workspace_id = I_workspace_id;

BEGIN

   LP_review_not_started := FALSE;

   -- loop on worksheet statuses
   FOR rec in C_WORKSHEET_STATUS LOOP

      LP_record_item_loc_strategy := rec.proposed_strategy_id;

      -- Refresh all Data Structures
      if REFRESH_DATA_STRUCTURES(O_error_msg,
                                 'Y') = FALSE then
         return 0;
      end if;

      if GET_DEPT_AGG(O_error_msg,
                      rec.dept) = FALSE then
         return 0;
      end if;

      if POPULATE_STRATEGY_TYPE(O_error_msg,
                                rec.proposed_strategy_id) = FALSE then
         return 0;
      end if;
      if LP_maint_margin_ind = 1 then
         if GET_MAINT_MARGIN_INFO(O_error_msg,
                                  LP_crp_start,
                                  LP_crp_end,
                                  rec.proposed_strategy_id) = FALSE then
            return 0;
         end if;
      else
         LP_crp_start := null;
         LP_crp_end   := null;
      end if;
      -- Get rpm_worksheet_zone_workspace and rpm_worksheet_il_workspace rows
      if INIT_WORKSHEET_RESET(O_error_msg,
                              L_effective_date,
                              L_zone_id,
                              rec.worksheet_status_id,
                              I_workspace_id,
                              rec.proposed_strategy_id) = FALSE then
         return 0;
      end if;

      if POPULATE_IL_STAT_RPM_IND(O_error_msg) = FALSE then
         return 0;
      end if;

      -- Get future_cost data
      if POPULATE_ITEM_COST(O_error_msg,
                            LP_vdate,
                            L_effective_date,
                            'N') = FALSE then
         return 0;
      end if;

      -- Get future_retail data
      if POPULATE_PRICING_INFO(O_error_msg,
                               LP_vdate,
                               rec.dept) = FALSE then
         return 0;
      end if;

      if CONSOLIDATE_GTT(O_error_msg) = FALSE then
         return 0;
      end if;

      -- Deal with zone link stuff
      if CONSOLIDATE_ITEM_LOC(O_error_msg,
                              L_zone_id,
                              'Y') = FALSE then
         return 0;
      end if;

      -- Update rpm_worksheet_zone_workspace rpm_worksheet_il_workspace
      if UPDATE_WORKSPACE(O_error_msg,
                          I_workspace_id) = FALSE then
         return 0;
      end if;

   END LOOP;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.RESET_WORKSPACE',
                                        to_char(SQLCODE));
      return 0;

END RESET_WORKSPACE;

------------------------------------------------------------------------------------------------------------------------

FUNCTION WORKSHEET_NOT_TAKE_ROLLUP(O_error_msg              OUT VARCHAR2,
                                   I_worksheet_status_id IN     RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                                   I_dept                IN     RPM_WORKSHEET_STATUS.DEPT%TYPE,
                                   I_include_wh_on_hand  IN     RPM_DEPT_AGGREGATION.INCLUDE_WH_ON_HAND%TYPE,
                                   I_pc_amount_calc_type IN     RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE,
                                   O_stock_on_hand          OUT RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE,
                                   O_price_change_amount    OUT RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE,
                                   O_sales_amount           OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE,
                                   O_sales_amount_ex_vat    OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE,
                                   O_sales_cost             OUT RPM_WORKSHEET_STATUS.SALES_COST%TYPE,
                                   O_sales_change_amount    OUT RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE,
                                   O_item_count             OUT RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.WORKSHEET_NOT_TAKE_ROLLUP';

   cursor C_UNDECIDED_DONTTAKE is
      select SUM(il.stock_on_hand + DECODE(I_include_wh_on_hand,
                                           1, NVL(item.ref_wh_stock, 0),
                                           0)),
             SUM(il.price_change_amount + price_change_amt * DECODE(I_include_wh_on_hand,
                                                                    1, NVL(item.ref_wh_stock, 0)/il.loc_count,
                                                                    0)),
             SUM(il.sales_amount),
             SUM(il.sales_amount_ex_vat),
             SUM(il.sales_cost),
             SUM(il.sales_change_amount),
             SUM(NVL(item.item_count,0))
        from (select itemloc.item item,
                     SUM(itemloc.stock_on_hand) stock_on_hand,
                     SUM(itemloc.price_change_amount) price_change_amt,
                     SUM(itemloc.price_change_amount * itemloc.stock_on_hand) price_change_amount,
                     SUM(itemloc.sales_amount) sales_amount,
                     SUM(NVL(itemloc.sales_amount_ex_vat, itemloc.sales_amount)) sales_amount_ex_vat,
                     SUM(itemloc.sales_cost) sales_cost,
                     SUM(itemloc.sales_change_amount) sales_change_amount,
                     SUM(loc_count) loc_count
                from (select wd.item,
                             wd.location,
                             SUM(DECODE(wd.action_flag,
                                        UNDECIDED, wd.location_stock,
                                        0)) stock_on_hand,
                             SUM(DECODE(wd.action_flag, UNDECIDED,
                                        DECODE(I_pc_amount_calc_type,
                                               CURRENT_NEW,
                                                  NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std) -
                                                  NVL(wd.new_retail_std, NVL(wd.proposed_retail_std,
                                                      NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std))),
                                               NEW_CURRENT,
                                                  NVL(wd.new_retail_std, NVL(wd.proposed_retail_std,
                                                      NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std))) -
                                                  NVL(basis_clear_retail_std, basis_regular_retail_std)),
                                        0)) price_change_amount,
                             SUM(DECODE(wd.action_flag,
                                        DONT_TAKE, wd.basis_regular_retail* wd.projected_sales,
                                        UNDECIDED, NVL(NVL(wd.new_retail, wd.proposed_retail), wd.basis_regular_retail)
                                        * wd.projected_sales)) sales_amount,
                             SUM(DECODE(wd.vat_rate,
                                    NULL, NULL,
                                    0,    NULL,
                                    DECODE(wd.action_flag,
                                           DONT_TAKE, wd.basis_regular_retail* wd.projected_sales,
                                           UNDECIDED, NVL(NVL(wd.new_retail, wd.proposed_retail), wd.basis_regular_retail) *
                                                      wd.projected_sales) /
                                                      (1+(wd.vat_rate/100)))) sales_amount_ex_vat,
                             SUM(basis_pricing_cost * wd.projected_sales) sales_cost,
                             SUM(DECODE(wd.action_flag, UNDECIDED,
                                        DECODE(I_pc_amount_calc_type,
                                               CURRENT_NEW, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std) -
                                                            NVL(wd.new_retail_std, NVL(wd.proposed_retail_std, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std))),
                                               NEW_CURRENT, NVL(wd.new_retail_std, NVL(wd.proposed_retail_std, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std))) -
                                                            NVL(basis_clear_retail_std, basis_regular_retail_std))
                                        * wd.projected_sales, 0)) sales_change_amount,
                             SUM(DECODE(wd.action_flag, DONT_TAKE, 1, UNDECIDED, 1, 0)) loc_count
                        from rpm_worksheet_item_loc_data wd
                       where wd.dept                = I_dept
                         and wd.worksheet_status_id = I_worksheet_status_id
                       group by wd.item, wd.location) itemloc
               group by itemloc.item) il,
             (select wd.item,
                     wd.ref_wh_stock ref_wh_stock,
                     1 item_count
                from rpm_worksheet_item_data wd
               where wd.dept                = I_dept
                 and wd.worksheet_status_id = I_worksheet_status_id
                 and wd.action_flag        != TAKE) item
       where il.item = item.item(+);

BEGIN

   open C_UNDECIDED_DONTTAKE;
   fetch C_UNDECIDED_DONTTAKE into O_stock_on_hand,
                                   O_price_change_amount,
                                   O_sales_amount,
                                   O_sales_amount_ex_vat,
                                   O_sales_cost,
                                   O_sales_change_amount,
                                   O_item_count;
   close C_UNDECIDED_DONTTAKE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END WORKSHEET_NOT_TAKE_ROLLUP;

------------------------------------------------------------------------------------------------------------------------

FUNCTION WORKSHEET_TAKE_ROLLUP(O_error_msg              OUT VARCHAR2,
                               I_worksheet_status_id IN     RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                               I_dept                IN     RPM_WORKSHEET_STATUS.DEPT%TYPE,
                               I_include_wh_on_hand  IN     RPM_DEPT_AGGREGATION.INCLUDE_WH_ON_HAND%TYPE,
                               I_pc_amount_calc_type IN     RPM_DEPT_AGGREGATION.PRICE_CHANGE_AMOUNT_CALC_TYPE%TYPE,
                               O_stock_on_hand          OUT RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE,
                               O_price_change_amount    OUT RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE,
                               O_sales_amount           OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE,
                               O_sales_amount_ex_vat    OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE,
                               O_sales_cost             OUT RPM_WORKSHEET_STATUS.SALES_COST%TYPE,
                               O_sales_change_amount    OUT RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE,
                               O_item_count             OUT RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.WORKSHEET_TAKE_ROLLUP';

   cursor C_TAKE is
      select SUM(il.stock_on_hand + DECODE(I_include_wh_on_hand,
                                           1, NVL(item.ref_wh_stock, 0),
                                           0)),
             SUM(il.price_change_amount + price_change_amt * DECODE(I_include_wh_on_hand,
                                                                    1, DECODE(il.loc_count,
                                                                              0, 0,
                                                                              NVL(item.ref_wh_stock, 0)/il.loc_count),
                                                                    0)),
             SUM(il.sales_amount),
             SUM(il.sales_amount_ex_vat),
             SUM(il.sales_cost),
             SUM(il.sales_change_amount),
             SUM(NVL(item.item_count, 0))
        from (select itemloc.item item,
                     SUM(itemloc.stock_on_hand) stock_on_hand,
                     SUM(itemloc.price_change_amount) price_change_amt,
                     SUM(itemloc.price_change_amount * itemloc.stock_on_hand) price_change_amount,
                     SUM(itemloc.sales_amount) sales_amount,
                     SUM(NVL(itemloc.sales_amount_ex_vat, itemloc.sales_amount)) sales_amount_ex_vat,
                     SUM(itemloc.sales_cost) sales_cost,
                     SUM(itemloc.sales_change_amount) sales_change_amount,
                     SUM(loc_count) loc_count
                from (select wd.item,
                             wd.location,
                             SUM(DECODE(wd.action_flag,
                                        TAKE, wd.location_stock,
                                        0)) stock_on_hand,
                             SUM(DECODE(wd.action_flag, TAKE,
                                        DECODE(I_pc_amount_calc_type,
                                               CURRENT_NEW,
                                                  NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std) -
                                                     NVL(wd.new_retail_std, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std)),
                                               NEW_CURRENT,
                                                  NVL(wd.new_retail_std, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std)) -
                                                     NVL(basis_clear_retail_std, basis_regular_retail_std)),
                                        0)) price_change_amount,
                             SUM(DECODE(wd.action_flag,
                                        TAKE, NVL(wd.new_retail, wd.basis_regular_retail) * wd.projected_sales,
                                        DONT_TAKE, wd.basis_regular_retail * wd.projected_sales,
                                              NVL(NVL(wd.new_retail, wd.proposed_retail), wd.basis_regular_retail)
                                                * wd.projected_sales)) sales_amount,
                             SUM(DECODE(wd.vat_rate,
                                 NULL, NULL,
                                 0, NULL,
                                 DECODE(wd.action_flag,
                                        TAKE, NVL(wd.new_retail, wd.basis_regular_retail) * wd.projected_sales,
                                        DONT_TAKE, wd.basis_regular_retail * wd.projected_sales,
                                        NVL(NVL(wd.new_retail, wd.proposed_retail), wd.basis_regular_retail) *
                                        wd.projected_sales)) / (1+(wd.vat_rate/100))) sales_amount_ex_vat,
                             SUM(basis_pricing_cost * wd.projected_sales) sales_cost,
                             SUM(DECODE(wd.action_flag,
                                        TAKE, DECODE(I_pc_amount_calc_type,
                                                     CURRENT_NEW, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std) -
                                                                  NVL(wd.new_retail_std, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std)),
                                                     NEW_CURRENT, NVL(wd.new_retail_std, NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std)) -
                                                                  NVL(wd.basis_clear_retail_std, wd.basis_regular_retail_std)) * wd.projected_sales,
                                        0)) sales_change_amount,
                             SUM(DECODE(wd.action_flag,
                                        TAKE, 1,
                                        0)) loc_count
                        from rpm_worksheet_item_loc_data wd
                       where wd.dept                = I_dept
                         and wd.worksheet_status_id = I_worksheet_status_id
                       group by wd.item, wd.location) itemloc
               group by itemloc.item) il,
             (select wd.item,
                     wd.ref_wh_stock ref_wh_stock,
                     1 item_count
                from rpm_worksheet_item_data wd
               where wd.dept                = I_dept
                 and wd.worksheet_status_id = I_worksheet_status_id
                 and wd.action_flag         = TAKE) item
       where il.item = item.item(+);

BEGIN

   open C_TAKE;
   fetch C_TAKE into O_stock_on_hand,
                     O_price_change_amount,
                     O_sales_amount,
                     O_sales_amount_ex_vat,
                     O_sales_cost,
                     O_sales_change_amount,
                     O_item_count;
   close C_TAKE;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return FALSE;

END WORKSHEET_TAKE_ROLLUP;

------------------------------------------------------------------------------------------------------------------------

FUNCTION WORKSHEET_ROLLUP(O_error_msg              OUT VARCHAR2,
                          I_worksheet_status_id IN     RPM_WORKSHEET_STATUS.WORKSHEET_STATUS_ID%TYPE,
                          O_state_bitmap           OUT RPM_WORKSHEET_STATUS.STATE_BITMAP%TYPE,
                          O_stock_on_hand          OUT RPM_WORKSHEET_STATUS.STOCK_ON_HAND%TYPE,
                          O_price_change_amount    OUT RPM_WORKSHEET_STATUS.PRICE_CHANGE_AMOUNT%TYPE,
                          O_sales_amount           OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT%TYPE,
                          O_sales_amount_ex_vat    OUT RPM_WORKSHEET_STATUS.SALES_AMOUNT_EX_VAT%TYPE,
                          O_sales_cost             OUT RPM_WORKSHEET_STATUS.SALES_COST%TYPE,
                          O_sales_change_amount    OUT RPM_WORKSHEET_STATUS.SALES_CHANGE_AMOUNT%TYPE,
                          O_item_count             OUT RPM_WORKSHEET_STATUS.ITEM_COUNT%TYPE)
RETURN NUMBER IS

   L_program VARCHAR2(30) := 'RPM_EXT_SQL.WORKSHEET_ROLLUP';

   L_take_count NUMBER         := -1;
   L_dept       DEPS.DEPT%TYPE := NULL;

   cursor C_WORKSHEET_STATUS is
      select SUM(DECODE(wd.action_flag,
                        TAKE, 1,
                        0)) take_count,
             MIN(dept),
             SUM(distinct POWER(2, state)) state_bitmap
        from rpm_worksheet_item_data wd
       where wd.worksheet_status_id = I_worksheet_status_id;

BEGIN

   open C_WORKSHEET_STATUS;
   fetch C_WORKSHEET_STATUS into L_take_count,
                                 L_dept,
                                 O_state_bitmap;
   close C_WORKSHEET_STATUS;

   if GET_DEPT_AGG(O_error_msg,
                   L_dept) = FALSE then
      return 0;
   end if;

   if L_take_count > 0 then

      if WORKSHEET_TAKE_ROLLUP(O_error_msg,
                               I_worksheet_status_id,
                               L_dept,
                               LP_include_wh_on_hand,
                               LP_pc_amount_calc_type,
                               O_stock_on_hand,
                               O_price_change_amount,
                               O_sales_amount,
                               O_sales_amount_ex_vat,
                               O_sales_cost,
                               O_sales_change_amount,
                               O_item_count) = FALSE then
         return 0;
      end if;

   else

      if WORKSHEET_NOT_TAKE_ROLLUP(O_error_msg,
                                   I_worksheet_status_id,
                                   L_dept,
                                   LP_include_wh_on_hand,
                                   LP_pc_amount_calc_type,
                                   O_stock_on_hand,
                                   O_price_change_amount,
                                   O_sales_amount,
                                   O_sales_amount_ex_vat,
                                   O_sales_cost,
                                   O_sales_change_amount,
                                   O_item_count) = FALSE then
         return 0;
      end if;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END WORKSHEET_ROLLUP;
------------------------------------------------------------------------------------------------------------------------

FUNCTION DELETE_EXPIRED_WORKSHEET (O_error_msg        OUT  VARCHAR2)
   RETURN NUMBER IS

   L_worksheet_ils             OBJ_NUM_NUM_STR_TBL;

BEGIN

   delete from rpm_merch_extract_deletions d
    where d.strategy_id in(
    select rs.strategy_id
      from rpm_strategy rs
     where rs.copied_strategy_id IS NOT NULL
       and rs.current_calendar_id IN (select cal1.calendar_id
                                        from rpm_calendar cal1
                                       where cal1.calendar_id NOT IN (select cal2.calendar_id
                                                                        from rpm_calendar_period cal2,
                                                                             period pr
                                                                       where cal1.calendar_id = cal2.calendar_id
                                                                         and (pr.vdate BETWEEN cal2.start_date - 1 AND cal2.end_date)
                                                                     )
                                         or cal1.calendar_id IN (select cal3.calendar_id
                                                                   from rpm_calendar_period cal3,
                                                                        period pr
                                                                  where cal1.calendar_id = cal3.calendar_id
                                                                    and cal3.end_date = pr.vdate)
                                     )
          );

   delete rpm_worksheet_zone_workspace ww
    where ww.proposed_strategy_id in (
    select rs.strategy_id
      from rpm_strategy rs
     where rs.copied_strategy_id IS NOT NULL
       and rs.current_calendar_id IN (select cal1.calendar_id
                                        from rpm_calendar cal1
                                       where cal1.calendar_id NOT IN (select cal2.calendar_id
                                                                        from rpm_calendar_period cal2,
                                                                             period pr
                                                                       where cal1.calendar_id = cal2.calendar_id
                                                                         and (pr.vdate BETWEEN cal2.start_date - 1 AND cal2.end_date)
                                                                     )
                                         or cal1.calendar_id IN (select cal3.calendar_id
                                                                   from rpm_calendar_period cal3,
                                                                        period pr
                                                                  where cal1.calendar_id = cal3.calendar_id
                                                                    and cal3.end_date = pr.vdate)
                                     )
          ) returning obj_num_num_str_rec(ww.workspace_id, ww.worksheet_item_data_id, NULL) bulk collect into L_worksheet_ils;

   delete rpm_worksheet_il_workspace rwil
    where (rwil.workspace_id, rwil.worksheet_item_data_id) in
          (select /*+ cardinality (ids, 100) */ number_1, number_2
             from table(cast(L_worksheet_ils as OBJ_NUM_NUM_STR_TBL)) ids);

   delete rpm_worksheet_item_loc_data wil
    where (wil.dept, wil.worksheet_item_data_id) in (
    select wd.dept,
           wd.worksheet_item_data_id
      from rpm_worksheet_item_data wd
     where wd.proposed_strategy_id in (
           select rs.strategy_id
             from rpm_strategy rs
            where rs.copied_strategy_id IS NOT NULL
              and rs.current_calendar_id IN (select cal1.calendar_id
                                               from rpm_calendar cal1
                                              where cal1.calendar_id NOT IN (select cal2.calendar_id
                                                                               from rpm_calendar_period cal2,
                                                                                    period pr
                                                                              where cal1.calendar_id = cal2.calendar_id
                                                                                and (pr.vdate BETWEEN cal2.start_date - 1 AND cal2.end_date)
                                                                            )
                                                 or cal1.calendar_id IN (select cal3.calendar_id
                                                                           from rpm_calendar_period cal3,
                                                                                period pr
                                                                          where cal1.calendar_id = cal3.calendar_id
                                                                            and cal3.end_date = pr.vdate)
                                            )
          ));

   delete rpm_worksheet_item_data wd
   where wd.proposed_strategy_id in (
    select rs.strategy_id
      from rpm_strategy rs
     where rs.copied_strategy_id IS NOT NULL
       and rs.current_calendar_id IN (select cal1.calendar_id
                                        from rpm_calendar cal1
                                       where cal1.calendar_id NOT IN (select cal2.calendar_id
                                                                        from rpm_calendar_period cal2,
                                                                             period pr
                                                                       where cal1.calendar_id = cal2.calendar_id
                                                                         and (pr.vdate BETWEEN cal2.start_date - 1 AND cal2.end_date)
                                                                     )
                                         or cal1.calendar_id IN (select cal3.calendar_id
                                                                   from rpm_calendar_period cal3,
                                                                        period pr
                                                                  where cal1.calendar_id = cal3.calendar_id
                                                                    and cal3.end_date = pr.vdate)
                                     )
          ) returning obj_num_num_str_rec(wd.worksheet_status_id, wd.worksheet_item_data_id, NULL) bulk collect into L_worksheet_ils;

   delete rpm_worksheet_zone_data rwz
    where (rwz.worksheet_status_id, rwz.worksheet_item_data_id) in
          (select /*+ cardinality (ids, 100) */ number_1, number_2
             from table(cast(L_worksheet_ils as OBJ_NUM_NUM_STR_TBL)) ids);

   delete rpm_worksheet_cand_link rwc
    where not exists (select 1
                        from rpm_worksheet_item_data rw
                       where rw.worksheet_item_data_id = rwc.worksheet_item_data_id);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.DELETE_EXPIRED_WORKSHEET',
                                        to_char(SQLCODE));
      return 0;

END DELETE_EXPIRED_WORKSHEET;

----------------------------------------------------------------------------------------

FUNCTION DELETE_ABANDONED_WS_STATUS (O_error_msg        OUT  VARCHAR2)
RETURN NUMBER IS

BEGIN

   delete rpm_worksheet_status rws
    where not exists (select 1
                        from rpm_worksheet_item_data rw
                       where rw.worksheet_status_id = rws.worksheet_status_id);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.DELETE_ABANDONED_WS_STATUS',
                                        to_char(SQLCODE));
      return 0;

END DELETE_ABANDONED_WS_STATUS;


------------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_STRATEGY (O_error_msg         OUT  VARCHAR2,
                        I_eff_date           IN  DATE)

RETURN NUMBER IS

  L_strategy_type                   VARCHAR2(20);
  L_next_strategy_id                RPM_STRATEGY.STRATEGY_ID%TYPE;

-- Select all startegies to be copied
  cursor Strategy is
  select strategy.STRATEGY_ID,
         case
            when clearance.STRATEGY_ID is not null then CLEARANCE
            when competitive.STRATEGY_ID is not null then COMPETITIVE
            when maint_margin.STRATEGY_ID is not null then MAINT_MARGIN
            when margin.STRATEGY_ID is not null then MARGIN
         end STRATEGY_TYPE
      from RPM_STRATEGY strategy
         left outer join RPM_STRATEGY_CLEARANCE clearance
              on strategy.STRATEGY_ID=clearance.STRATEGY_ID
       left outer join RPM_STRATEGY_MARGIN margin
            on strategy.STRATEGY_ID=margin.STRATEGY_ID
       left outer join RPM_STRATEGY_MAINT_MARGIN maint_margin
            on strategy.STRATEGY_ID=maint_margin.STRATEGY_ID
       left outer join RPM_STRATEGY_COMPETITIVE competitive
              on strategy.STRATEGY_ID=competitive.STRATEGY_ID
   where SUSPEND = 0
     AND CURRENT_CALENDAR_ID IN(
            select calendar.CALENDAR_ID
             from RPM_CALENDAR calendar,
                   RPM_CALENDAR_PERIOD calendarperiod
             where calendar.CALENDAR_ID=calendarperiod.CALENDAR_ID
               and to_char(calendarperiod.START_DATE,'YYYYMMDD') = to_char(I_eff_date,'YYYYMMDD'))
     AND strategy.STRATEGY_ID NOT IN(select COPIED_STRATEGY_ID
                                       from RPM_STRATEGY
                                      where COPIED_STRATEGY_ID is not null )
     AND COPIED_STRATEGY_ID is null ;

 ---  Next available ID for RPM_STRATEGY
     cursor NEXT_STRATEGY_ID is
     select RPM_STRATEGY_SEQ.nextval
       from DUAL;

BEGIN

  FOR rec IN Strategy LOOP

    open  NEXT_STRATEGY_ID;
    fetch NEXT_STRATEGY_ID
    into L_next_strategy_id;
    close NEXT_STRATEGY_ID;
    CASE rec.STRATEGY_TYPE
      WHEN CLEARANCE THEN
        IF COPY_PRICE_STRATEGY_CLEARANCE(O_error_msg,rec.STRATEGY_ID,L_next_strategy_id)= FALSE THEN
           RETURN 0;
        END IF;
      WHEN COMPETITIVE THEN
        IF COPY_PRICE_STRATEGY_COMPETITIV(O_error_msg,rec.STRATEGY_ID,L_next_strategy_id)= FALSE THEN
           RETURN 0;
        END IF;
      WHEN MAINT_MARGIN THEN
        IF COPY_PRICE_STRATEGY_MNT_MARGIN(O_error_msg,rec.STRATEGY_ID,L_next_strategy_id)= FALSE THEN
           RETURN 0;
        END IF;
      WHEN MARGIN THEN
        IF COPY_PRICE_STRATEGY_MARGIN(O_error_msg,rec.STRATEGY_ID,L_next_strategy_id)= FALSE THEN
           RETURN 0;
        END IF;
    END CASE;
  END LOOP;
  RETURN 1;
EXCEPTION
  when OTHERS then
     O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_STARTEGY',
                                        to_char(SQLCODE));
     return 0;

END COPY_STRATEGY;

------------------------------------------------------------------------------------------------------------------------
-- Copy rpm_strategy table
FUNCTION COPY_PRICE_STRATEGY (O_error_msg        OUT  VARCHAR2,
                        I_from_strategy_id       IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id         IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS

BEGIN

      -- Copy the rpm_strategy table
      insert into rpm_strategy(
          STRATEGY_ID,
          STATE,
          ZONE_HIER_TYPE,
          ZONE_HIER_ID,
          MERCH_TYPE,
          COPIED_STRATEGY_ID,
          DEPT,
          CLASS,
          SUBCLASS,
          PRICE_GUIDE_ID,
          CURRENT_CALENDAR_ID,
          NEXT_CALENDAR_ID,
          SUSPEND,
          DEFAULT_EFFECTIVE_DAY )
      select
          I_to_strategy_id,
          STATE,
          ZONE_HIER_TYPE,
          ZONE_HIER_ID,
          MERCH_TYPE,
          I_from_strategy_id,
          DEPT,
          CLASS,
          SUBCLASS,
          PRICE_GUIDE_ID,
          CURRENT_CALENDAR_ID,
          NEXT_CALENDAR_ID,
          SUSPEND,
          DEFAULT_EFFECTIVE_DAY
      from rpm_strategy
      where STRATEGY_ID = I_from_strategy_id;

      -- Copy the warehouse table

      insert into rpm_strategy_wh(
        STRATEGY_ID,
        WH)
      select
        I_to_strategy_id,
        WH
      from rpm_strategy_wh
      where STRATEGY_ID = I_from_strategy_id;


  RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY;

------------------------------------------------------------------------------------------------------------------------
--- Copy rpm_strategy_detail table
FUNCTION COPY_PRICE_STRATEGY_DETAIL (O_error_msg        OUT  VARCHAR2,
                        I_from_strategy_id       IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id         IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS


BEGIN

      insert into RPM_STRATEGY_DETAIL(
              STRATEGY_DETAIL_ID,
              STRATEGY_ID,
              STRATEGY_DETAIL_CLASS,
              COMPETE_TYPE,
              MKT_BASKET_CODE,
              PERCENT,
              FROM_PERCENT,
              TO_PERCENT )
      select
              RPM_STRATEGY_DETAIL_SEQ.nextval,
              I_to_strategy_id,
              STRATEGY_DETAIL_CLASS,
              COMPETE_TYPE,
              MKT_BASKET_CODE,
              PERCENT,
              FROM_PERCENT,
              TO_PERCENT
      from RPM_STRATEGY_DETAIL
      where STRATEGY_ID = I_from_strategy_id;

  RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY_DETAIL;

------------------------------------------------------------------------------------------------------------------------
-- copy table rpm_strategy_ref_comp
FUNCTION COPY_PRICE_STRATEGY_REFCOMP (O_error_msg        OUT  VARCHAR2,
                        I_from_strategy_id       IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id         IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS


BEGIN

      insert into rpm_strategy_ref_comp(
          STRATEGY_ID,
          COMP_INDEX,
          COMP_STORE,
          COMP_TYPE,
          COMP_PERCENT)
      select
          I_to_strategy_id,
          COMP_INDEX,
          COMP_STORE,
          COMP_TYPE,
          COMP_PERCENT
      from rpm_strategy_ref_comp
      where
        STRATEGY_ID = I_from_strategy_id;
  RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY_REFCOMP;

------------------------------------------------------------------------------------------------------------------------
FUNCTION COPY_PRICE_STRATEGY_CLEARANCE (O_error_msg         OUT  VARCHAR2,
                        I_from_strategy_id      IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id        IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS


BEGIN

      if COPY_PRICE_STRATEGY(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      -- Copy rpm_strategy_clearance

      insert into rpm_strategy_clearance(
          STRATEGY_ID,
          MARKDOWN_BASIS)
      select
          I_to_strategy_id,
          MARKDOWN_BASIS
      from rpm_strategy_clearance
      where
        STRATEGY_ID = I_from_strategy_id;

     -- Copy rpm_strategy_clearance_mkdn

      insert into rpm_strategy_clearance_mkdn(
          STRATEGY_ID,
          MKDN_NUMBER,
          MKDN_PERCENT)
      select
          I_to_strategy_id,
          MKDN_NUMBER,
          MKDN_PERCENT
      from rpm_strategy_clearance_mkdn
      where
        STRATEGY_ID = I_from_strategy_id;

  RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY_CLEARANCE;

------------------------------------------------------------------------------------------------------------------------

FUNCTION COPY_PRICE_STRATEGY_COMPETITIV (O_error_msg         OUT  VARCHAR2,
                        I_from_strategy_id      IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id        IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS


BEGIN

      if COPY_PRICE_STRATEGY(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      -- Copy rpm_strategy_competitive

      insert into rpm_strategy_competitive(
          STRATEGY_ID,
          COMPETE_TYPE,
          COMP_STORE)
      select
          I_to_strategy_id,
          COMPETE_TYPE,
          COMP_STORE
      from rpm_strategy_competitive
      where
        STRATEGY_ID = I_from_strategy_id;


      if COPY_PRICE_STRATEGY_REFCOMP(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      if COPY_PRICE_STRATEGY_DETAIL(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;
   RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY_COMPETITIV;

------------------------------------------------------------------------------------------------------------------------

FUNCTION COPY_PRICE_STRATEGY_MARGIN (O_error_msg         OUT  VARCHAR2,
                        I_from_strategy_id      IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id        IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS


BEGIN

      if COPY_PRICE_STRATEGY(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      -- Copy rpm_strategy_margin

      insert into rpm_strategy_margin(
          STRATEGY_ID)
      select
          I_to_strategy_id
      from rpm_strategy_margin
      where
        STRATEGY_ID = I_from_strategy_id;


      if COPY_PRICE_STRATEGY_REFCOMP(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      if COPY_PRICE_STRATEGY_DETAIL(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;
  RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY_MARGIN;

------------------------------------------------------------------------------------------------------------------------

FUNCTION COPY_PRICE_STRATEGY_MNT_MARGIN (O_error_msg         OUT  VARCHAR2,
                        I_from_strategy_id      IN  RPM_STRATEGY.STRATEGY_ID%TYPE,
                        I_to_strategy_id        IN  RPM_STRATEGY.STRATEGY_ID%TYPE )

  RETURN BOOLEAN IS


BEGIN

      if COPY_PRICE_STRATEGY(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      -- Copy rpm_strategy_maint_margin

      insert into rpm_strategy_maint_margin(
          STRATEGY_ID,
          MAINT_MARGIN_METHOD,
          AUTO_APPROVE,
          COST_FORWARD_DAYS,
          INCREASE,
          DECREASE)
      select
          I_to_strategy_id,
          MAINT_MARGIN_METHOD,
          AUTO_APPROVE,
          COST_FORWARD_DAYS,
          INCREASE,
          DECREASE
      from rpm_strategy_maint_margin
      where
        STRATEGY_ID = I_from_strategy_id;

      if COPY_PRICE_STRATEGY_REFCOMP(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

      if COPY_PRICE_STRATEGY_DETAIL(O_error_msg,I_from_strategy_id,I_to_strategy_id)= FALSE THEN
        RETURN FALSE;
      end if;

  RETURN TRUE;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.COPY_PRICE_STRATEGY',
                                        to_char(SQLCODE));
    return FALSE;
END COPY_PRICE_STRATEGY_MNT_MARGIN;

---------------------------------------------------------------------------------------------------

FUNCTION UPDATE_COMP_PRICE_HIST (O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_EXT_SQL.UPDATE_COMP_PRICE_HIST';

BEGIN

    update comp_price_hist
       set rpm_pull = 'R'
     where (item,
            comp_store) IN (select rwd.item,
                                   rsc.comp_store
                              from rpm_worksheet_item_data rwd,
                                   rpm_pre_me_aggregation rpma,
                                   rpm_strategy_competitive rsc
                             where rwd.dept                 = rpma.dept
                               and rwd.class                = NVL(rpma.class, rwd.class)
                               and rwd.subclass             = NVL(rpma.subclass, rwd.subclass)
                               and rwd.proposed_strategy_id = rpma.strategy_id
                               and rpma.strategy_id         = rsc.strategy_id
                               and rwd.area_diff_id         IS NULL
                               and rpma.aggregation_type    = RPM_EXT_SQL.WORKSHEET_CREATE_LEVEL)
       and comp_retail_type = 'R';

    update comp_price_hist
       set rpm_pull = 'R'
     where (item,
            comp_store) IN (select rwd.item,
                                   rad.comp_store
                              from rpm_worksheet_item_data rwd,
                                   rpm_pre_me_aggregation rpma,
                                   rpm_area_diff rad
                             where rwd.dept                 = rpma.dept
                               and rwd.class                = NVL(rpma.class, rwd.class)
                               and rwd.subclass             = NVL(rpma.subclass, rwd.subclass)
                               and rwd.proposed_strategy_id = rpma.strategy_id
                               and rwd.area_diff_id         = rad.area_diff_id
                               and rpma.aggregation_type    = RPM_EXT_SQL.WORKSHEET_CREATE_LEVEL)
       and comp_retail_type = 'R';

   return 1;

EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                      SQLERRM,
                                      L_program,
                                      TO_CHAR(SQLCODE));
    return 0;

END UPDATE_COMP_PRICE_HIST;
----------------------------------------------------------------------------------------
FUNCTION PURGE_ITEM_LOC_WORKSPACE (O_error_msg        OUT  VARCHAR2,
                                   I_workspace_id  IN      NUMBER)
  RETURN NUMBER IS
   L_program  VARCHAR2(40)  := 'RPM_EXT_SQL.PURGE_ITEM_LOC_WORKSPACE';

   L_purge_worksheet_il_workspace VARCHAR2(100) :=
      'alter table rpm_worksheet_il_workspace truncate partition ws_partition_'||I_workspace_id||' reuse storage';

BEGIN

   EXECUTE IMMEDIATE L_purge_worksheet_il_workspace;

   return 1;
EXCEPTION
  when OTHERS then
    O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_EXT_SQL.PURGE_ITEM_LOC_WORKSPACE',
                                        to_char(SQLCODE));
    return 0;
END PURGE_ITEM_LOC_WORKSPACE;


---------------------------------------------------------------------------------------------------
FUNCTION CURRENCY_CONVERT_VALUE (I_output_currency        IN     currencies.currency_code%TYPE,
                                 I_input_value            IN     item_loc.unit_retail%TYPE,
                                 I_input_currency         IN     currencies.currency_code%TYPE)
RETURN NUMBER IS

   L_error_msg                   VARCHAR2(250);
   L_output_value                NUMBER(20,4);

BEGIN

   if RPM_WRAPPER.CURRENCY_CONVERT_VALUE(L_error_msg,
                                         L_output_value,
                                         I_output_currency,
                                         I_input_value,
                                         I_input_currency) = 0 then
      return I_input_value;
   end if;

   return L_output_value;

END CURRENCY_CONVERT_VALUE;
---------------------------------------------------------------------------------------------------
END RPM_EXT_SQL;
/