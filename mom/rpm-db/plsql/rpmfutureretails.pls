CREATE OR REPLACE PACKAGE RPM_FUTURE_RETAIL_SQL AS
--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------
FUNCTION PRICE_EVENT_INSERT(O_cc_error_tbl          OUT CONFLICT_CHECK_ERROR_TBL,
                            O_cc_error_gen_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                            I_price_event_type   IN     VARCHAR2,
                            I_rib_trans_id       IN     NUMBER,
                            I_persist_ind        IN     VARCHAR2,
                            I_specific_item_loc  IN     NUMBER   DEFAULT 0,
                            I_new_item_loc       IN     NUMBER   DEFAULT 0,
                            I_new_promo_end_date IN     DATE     DEFAULT NULL,
                            I_bulk_cc_pe_id      IN     NUMBER   DEFAULT NULL,
                            I_pe_sequence_id     IN     NUMBER   DEFAULT NULL,
                            I_pe_thread_number   IN     NUMBER   DEFAULT NULL,
                            I_chunk_number       IN     NUMBER   DEFAULT 0,
                            I_end_state          IN     VARCHAR2 DEFAULT 0,
                            I_reprocess_ind      IN     NUMBER   DEFAULT 0,
                            I_loc_move_ind       IN     NUMBER   DEFAULT 0)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION PRICE_EVENT_REMOVE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                            I_price_event_type IN     VARCHAR2,
                            I_rib_trans_id     IN     NUMBER,
                            I_persist_ind      IN     VARCHAR2,
                            I_bulk_cc_pe_id    IN     NUMBER   DEFAULT NULL,
                            I_pe_sequence_id   IN     NUMBER   DEFAULT NULL,
                            I_pe_thread_number IN     NUMBER   DEFAULT NULL,
                            I_chunk_number     IN     NUMBER   DEFAULT 0,
                            I_end_state        IN     VARCHAR2 DEFAULT 0)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION SCHEDULE_LOCATION_MOVE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                I_location_move_id IN     RPM_LOCATION_MOVE.LOCATION_MOVE_ID%TYPE,
                                I_rib_trans_id     IN     RPM_PRICE_EVENT_PAYLOAD.TRANSACTION_ID%TYPE,
                                I_persist_ind      IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION POPULATE_CC_ERROR_TABLE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_price_event_type IN     VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION PUBLISH_CHANGES(IO_cc_error_tbl             IN OUT CONFLICT_CHECK_ERROR_TBL,
                         I_rib_trans_id              IN     NUMBER,
                         I_bulk_cc_pe_id             IN     NUMBER,
                         I_process_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE,
                         I_process_price_event_type  IN     VARCHAR2,
                         I_remove_price_event_ids    IN     OBJ_NUMERIC_ID_TABLE,
                         I_remove_price_event_type   IN     VARCHAR2,
                         I_cancel_item_loc_promo_dtl IN     NUMBER DEFAULT 0)
RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION EXPL_TIMELINES_TO_IL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                              I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type IN     VARCHAR2,
                              I_step_identifier  IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CLEAN_SGE_ON_REMOVE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                             I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GENERATE_TIMELINE(O_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id    IN     NUMBER,
                           I_pe_sequence_id   IN     NUMBER,
                           I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                   I_price_event_type IN     VARCHAR2,
                   I_new_item_loc     IN     NUMBER DEFAULT 0)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION SKIP_CC_PROCESS(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                         I_price_event_type IN     VARCHAR2,
                         I_bulk_cc_pe_id    IN     NUMBER,
                         I_rib_trans_id     IN     NUMBER,
                         I_end_state        IN     VARCHAR2,
                         I_persist_ind      IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK_PURGE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_FUTURE_RETAIL_SQL;
/
