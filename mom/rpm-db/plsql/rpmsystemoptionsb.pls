CREATE OR REPLACE PACKAGE BODY RPM_SYSTEM_OPTIONS_SQL AS

-----------------------------------------------------------------------------------------
-- globals
-----------------------------------------------------------------------------------------

LP_system_options_id           RPM_SYSTEM_OPTIONS.SYSTEM_OPTIONS_ID%TYPE := NULL;
LP_clearance_hist_months       RPM_SYSTEM_OPTIONS.CLEARANCE_HIST_MONTHS%TYPE := NULL;
LP_clearance_promo_overlap_ind RPM_SYSTEM_OPTIONS.CLEARANCE_PROMO_OVERLAP_IND%TYPE := NULL;
LP_complex_promo_allowed_ind   RPM_SYSTEM_OPTIONS.COMPLEX_PROMO_ALLOWED_IND%TYPE := NULL;
LP_cost_calculation_method     RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE := NULL;
LP_default_out_of_stock_days   RPM_SYSTEM_OPTIONS.DEFAULT_OUT_OF_STOCK_DAYS%TYPE := NULL;
LP_default_reset_date          RPM_SYSTEM_OPTIONS.DEFAULT_RESET_DATE%TYPE := NULL;
LP_event_id_required           RPM_SYSTEM_OPTIONS.EVENT_ID_REQUIRED%TYPE := NULL;
LP_multi_item_loc_promo_ind    RPM_SYSTEM_OPTIONS.MULTI_ITEM_LOC_PROMO_IND%TYPE := NULL;
LP_open_zone_use               RPM_SYSTEM_OPTIONS.OPEN_ZONE_USE%TYPE := NULL;
LP_pc_processing_days          RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROCESSING_DAYS%TYPE := NULL;
LP_pc_promo_overlap_ind        RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROMO_OVERLAP_IND%TYPE := NULL;
LP_promo_apply_order           RPM_SYSTEM_OPTIONS.PROMO_APPLY_ORDER%TYPE := NULL;
LP_promotion_hist_months       RPM_SYSTEM_OPTIONS.PROMOTION_HIST_MONTHS%TYPE := NULL;
LP_reject_hold_days_pc_clear   RPM_SYSTEM_OPTIONS.REJECT_HOLD_DAYS_PC_CLEAR%TYPE := NULL;
LP_reject_hold_days_promo      RPM_SYSTEM_OPTIONS.REJECT_HOLD_DAYS_PROMO%TYPE := NULL;
LP_recognize_wh_as_locations   RPM_SYSTEM_OPTIONS.RECOGNIZE_WH_AS_LOCATIONS%TYPE := NULL;
LP_sales_calculation_method    RPM_SYSTEM_OPTIONS.SALES_CALCULATION_METHOD%TYPE := NULL;
LP_update_item_attributes      RPM_SYSTEM_OPTIONS.UPDATE_ITEM_ATTRIBUTES%TYPE := NULL;
LP_zero_curr_ends_in_decs      RPM_SYSTEM_OPTIONS.ZERO_CURR_ENDS_IN_DECS%TYPE := NULL;
LP_sim_ind                     RPM_SYSTEM_OPTIONS.SIM_IND%TYPE := NULL;
LP_zone_ranging                RPM_SYSTEM_OPTIONS.ZONE_RANGING%TYPE := NULL;
LP_exact_deal_dates            RPM_SYSTEM_OPTIONS.EXACT_DEAL_DATES%TYPE := NULL;
LP_loc_move_processing_days    RPM_SYSTEM_OPTIONS.LOCATION_MOVE_PROCESSING_DAYS%TYPE := NULL;
LP_location_move_purge_days    RPM_SYSTEM_OPTIONS.LOCATION_MOVE_PURGE_DAYS%TYPE := NULL;
LP_dynamic_area_diff_ind       RPM_SYSTEM_OPTIONS.DYNAMIC_AREA_DIFF_IND%TYPE := NULL;
LP_lock_version                RPM_SYSTEM_OPTIONS.LOCK_VERSION%TYPE := NULL;
LP_get_max_clr_recs            RPM_SYSTEM_OPTIONS.CLEARANCE_SEARCH_MAX%TYPE := NULL;
LP_get_max_pc_recs             RPM_SYSTEM_OPTIONS.PRICE_CHANGE_SEARCH_MAX%TYPE := NULL;
LP_past_markup_events_disp_cnt RPM_SYSTEM_OPTIONS.PAST_MARKUP_EVENTS_DISP_CNT%TYPE := NULL;
LP_skip_cc_for_submit          RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_SUBMIT%TYPE:=NULL;
LP_skip_cc_for_cp_approval     RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_CP_APPROVAL%TYPE:= NULL;
-----------------------------------------------------------------------------------------

FUNCTION RESET_GLOBALS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_SYSTEM_OPTIONS_SQL.RESET_GLOBALS';

BEGIN

   LP_system_options_id := NULL;
   LP_clearance_hist_months := NULL;
   LP_clearance_promo_overlap_ind := NULL;
   LP_complex_promo_allowed_ind := NULL;
   LP_cost_calculation_method := NULL;
   LP_default_out_of_stock_days := NULL;
   LP_default_reset_date := NULL;
   LP_event_id_required := NULL;
   LP_multi_item_loc_promo_ind := NULL;
   LP_open_zone_use := NULL;
   LP_pc_processing_days := NULL;
   LP_pc_promo_overlap_ind := NULL;
   LP_promo_apply_order := NULL;
   LP_promotion_hist_months := NULL;
   LP_reject_hold_days_pc_clear := NULL;
   LP_reject_hold_days_promo := NULL;
   LP_recognize_wh_as_locations := NULL;
   LP_sales_calculation_method := NULL;
   LP_update_item_attributes := NULL;
   LP_zero_curr_ends_in_decs := NULL;
   LP_sim_ind := NULL;
   LP_zone_ranging := NULL;
   LP_exact_deal_dates := NULL;
   LP_loc_move_processing_days := NULL;
   LP_location_move_purge_days := NULL;
   LP_dynamic_area_diff_ind := NULL;
   LP_lock_version := NULL;
   LP_get_max_clr_recs := NULL;
   LP_get_max_pc_recs := NULL;
   LP_past_markup_events_disp_cnt := NULL;
   LP_skip_cc_for_submit := NULL;
   LP_skip_cc_for_cp_approval :=NULL;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END RESET_GLOBALS;
-----------------------------------------------------------------------------------------

FUNCTION SET_GLOBALS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'RPM_SYSTEM_OPTIONS_SQL.SET_GLOBALS';

BEGIN

   select system_options_id,
          clearance_hist_months,
          clearance_promo_overlap_ind,
          complex_promo_allowed_ind,
          cost_calculation_method,
          default_out_of_stock_days,
          default_reset_date,
          event_id_required,
          multi_item_loc_promo_ind,
          open_zone_use,
          price_change_processing_days,
          price_change_promo_overlap_ind,
          promo_apply_order,
          promotion_hist_months,
          reject_hold_days_pc_clear,
          reject_hold_days_promo,
          recognize_wh_as_locations,
          sales_calculation_method,
          update_item_attributes,
          zero_curr_ends_in_decs,
          sim_ind,
          zone_ranging,
          exact_deal_dates,
          location_move_processing_days,
          location_move_purge_days,
          dynamic_area_diff_ind,
          lock_version,
          clearance_search_max,
          price_change_search_max,
          past_markup_events_disp_cnt,
          do_not_run_cc_for_submit,
          do_not_run_cc_for_cp_approval
     into LP_system_options_id,
          LP_clearance_hist_months,
          LP_clearance_promo_overlap_ind,
          LP_complex_promo_allowed_ind,
          LP_cost_calculation_method,
          LP_default_out_of_stock_days,
          LP_default_reset_date,
          LP_event_id_required,
          LP_multi_item_loc_promo_ind,
          LP_open_zone_use,
          LP_pc_processing_days,
          LP_pc_promo_overlap_ind,
          LP_promo_apply_order,
          LP_promotion_hist_months,
          LP_reject_hold_days_pc_clear,
          LP_reject_hold_days_promo,
          LP_recognize_wh_as_locations,
          LP_sales_calculation_method,
          LP_update_item_attributes,
          LP_zero_curr_ends_in_decs,
          LP_sim_ind,
          LP_zone_ranging,
          LP_exact_deal_dates,
          LP_loc_move_processing_days,
          LP_location_move_purge_days,
          LP_dynamic_area_diff_ind,
          LP_lock_version,
          LP_get_max_clr_recs,
          LP_get_max_pc_recs,
          LP_past_markup_events_disp_cnt,
          LP_skip_cc_for_submit,
          LP_skip_cc_for_cp_approval
     from rpm_system_options;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END SET_GLOBALS;
-----------------------------------------------------------------------------------------

FUNCTION GET_SYSTEM_OPTIONS_ID(O_system_options_id IN OUT RPM_SYSTEM_OPTIONS.SYSTEM_OPTIONS_ID%TYPE,
                               O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS_ID';

BEGIN

   if LP_system_options_id is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_system_options_id := LP_system_options_id;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_SYSTEM_OPTIONS_ID;
------------------------------------------------------------------------

FUNCTION GET_CLEARANCE_HIST_MONTHS(O_clearance_hist_months IN OUT RPM_SYSTEM_OPTIONS.CLEARANCE_HIST_MONTHS%TYPE,
                                   O_error_message         IN OUT VARCHAR2)
RETURN BOOLEAN IS

    L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_CLEARANCE_HIST_MONTHS';

BEGIN

   if LP_clearance_hist_months is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_clearance_hist_months := LP_clearance_hist_months;

   return TRUE;

EXCEPTION
   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));

      return FALSE;

END GET_CLEARANCE_HIST_MONTHS;
------------------------------------------------------------------------

FUNCTION GET_CLEAR_PROMO_OVERLAP_IND(O_clearance_promo_overlap_ind IN OUT RPM_SYSTEM_OPTIONS.CLEARANCE_PROMO_OVERLAP_IND%TYPE,
                                     O_error_message               IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(52) := 'RPM_SYSTEM_OPTIONS_SQL.GET_CLEAR_PROMO_OVERLAP_IND';

BEGIN

   if LP_clearance_promo_overlap_ind is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_clearance_promo_overlap_ind := LP_clearance_promo_overlap_ind;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_CLEAR_PROMO_OVERLAP_IND;
------------------------------------------------------------------------

FUNCTION GET_COMPLEX_PROMO_ALLOWED_IND(O_complex_promo_allowed_ind IN OUT RPM_SYSTEM_OPTIONS.COMPLEX_PROMO_ALLOWED_IND%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_COMPLEX_PROMO_ALLOWED_IND';

BEGIN

   if LP_complex_promo_allowed_ind is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_complex_promo_allowed_ind := LP_complex_promo_allowed_ind;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_COMPLEX_PROMO_ALLOWED_IND;
------------------------------------------------------------------------

FUNCTION GET_COST_CALCULATION_METHOD(O_cost_calculation_method IN OUT RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE,
                                     O_error_message           IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_COST_CALCULATION_METHOD';

BEGIN

   if LP_cost_calculation_method is NULL then
      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   O_cost_calculation_method := LP_cost_calculation_method;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_COST_CALCULATION_METHOD;
------------------------------------------------------------------------

FUNCTION GET_DEFAULT_OUT_OF_STOCK_DAYS(O_default_out_of_stock_days IN OUT RPM_SYSTEM_OPTIONS.DEFAULT_OUT_OF_STOCK_DAYS%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEFAULT_OUT_OF_STOCK_DAYS';

BEGIN

   if LP_default_out_of_stock_days is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_default_out_of_stock_days := LP_default_out_of_stock_days;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_DEFAULT_OUT_OF_STOCK_DAYS;
------------------------------------------------------------------------

FUNCTION GET_DEFAULT_RESET_DATE(O_default_reset_date IN OUT RPM_SYSTEM_OPTIONS.DEFAULT_RESET_DATE%TYPE,
                                O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DEFAULT_RESET_DATE';

BEGIN

   if LP_default_reset_date is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_default_reset_date := LP_default_reset_date;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_DEFAULT_RESET_DATE;
------------------------------------------------------------------------

FUNCTION GET_EVENT_ID_REQUIRED(O_event_id_required IN OUT RPM_SYSTEM_OPTIONS.EVENT_ID_REQUIRED%TYPE,
                               O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_SYSTEM_OPTIONS_SQL.GET_EVENT_ID_REQUIRED';

BEGIN

   if LP_event_id_required is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_event_id_required := LP_event_id_required;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_EVENT_ID_REQUIRED;
------------------------------------------------------------------------

FUNCTION GET_MULTI_ITEM_LOC_PROMO_IND(O_multi_item_loc_promo_ind IN OUT RPM_SYSTEM_OPTIONS.MULTI_ITEM_LOC_PROMO_IND%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_MULTI_ITEM_LOC_PROMO_IND';

BEGIN

   if LP_multi_item_loc_promo_ind is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_multi_item_loc_promo_ind := LP_multi_item_loc_promo_ind;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MULTI_ITEM_LOC_PROMO_IND;
------------------------------------------------------------------------

FUNCTION GET_OPEN_ZONE_USE(O_open_zone_use IN OUT RPM_SYSTEM_OPTIONS.OPEN_ZONE_USE%TYPE,
                           O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_SYSTEM_OPTIONS_SQL.GET_OPEN_ZONE_USE';

BEGIN

   if LP_open_zone_use is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_open_zone_use := LP_open_zone_use;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_OPEN_ZONE_USE;
------------------------------------------------------------------------

FUNCTION GET_PRICE_CHANGE_PROC_DAYS(O_price_change_processing_days IN OUT RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROCESSING_DAYS%TYPE,
                                    O_error_message                IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_PRICE_CHANGE_PROC_DAYS';

BEGIN

   if LP_pc_processing_days is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_price_change_processing_days := LP_pc_processing_days;

   return TRUE;

EXCEPTION

   when OTHERS then

      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PRICE_CHANGE_PROC_DAYS;
------------------------------------------------------------------------

FUNCTION GET_PC_PROMO_OVERLAP_IND(O_pc_promo_overlap_ind IN OUT RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROMO_OVERLAP_IND%TYPE,
                                  O_error_message        IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_PC_PROMO_OVERLAP_IND';

BEGIN

   if LP_pc_promo_overlap_ind is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_pc_promo_overlap_ind := LP_pc_promo_overlap_ind;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PC_PROMO_OVERLAP_IND;
------------------------------------------------------------------------

FUNCTION GET_PROMO_APPLY_ORDER(O_promo_apply_order IN OUT RPM_SYSTEM_OPTIONS.PROMO_APPLY_ORDER%TYPE,
                               O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_SYSTEM_OPTIONS_SQL.GET_PROMO_APPLY_ORDER';

BEGIN

   if LP_promo_apply_order is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_promo_apply_order := LP_promo_apply_order;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PROMO_APPLY_ORDER;
------------------------------------------------------------------------

FUNCTION GET_PROMOTION_HIST_MONTHS(O_promotion_hist_months IN OUT RPM_SYSTEM_OPTIONS.PROMOTION_HIST_MONTHS%TYPE,
                                   O_error_message         IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_PROMOTION_HIST_MONTHS';

BEGIN

   if LP_promotion_hist_months is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_promotion_hist_months := LP_promotion_hist_months;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PROMOTION_HIST_MONTHS;
------------------------------------------------------------------------

FUNCTION GET_REJECT_HOLD_DAYS_PC_CLEAR(O_reject_hold_days_pc_clear IN OUT RPM_SYSTEM_OPTIONS.REJECT_HOLD_DAYS_PC_CLEAR%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_REJECT_HOLD_DAYS_PC_CLEAR';

BEGIN

   if LP_reject_hold_days_pc_clear is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_reject_hold_days_pc_clear := LP_reject_hold_days_pc_clear;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_REJECT_HOLD_DAYS_PC_CLEAR;
------------------------------------------------------------------------

FUNCTION GET_REJECT_HOLD_DAYS_PROMO(O_reject_hold_days_promo IN OUT rpm_system_options.reject_hold_days_promo%TYPE,
                                    O_error_message          IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_REJECT_HOLD_DAYS_PROMO';

BEGIN

   if LP_reject_hold_days_promo is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_reject_hold_days_promo := LP_reject_hold_days_promo;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_REJECT_HOLD_DAYS_PROMO;
------------------------------------------------------------------------

FUNCTION GET_RECOGNIZE_WH_AS_LOCATIONS(O_recognize_wh_as_locations IN OUT RPM_SYSTEM_OPTIONS.RECOGNIZE_WH_AS_LOCATIONS%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_RECOGNIZE_WH_AS_LOCATIONS';

BEGIN

   if LP_recognize_wh_as_locations is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_recognize_wh_as_locations := LP_recognize_wh_as_locations;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_RECOGNIZE_WH_AS_LOCATIONS;
------------------------------------------------------------------------

FUNCTION GET_SALES_CALCULATION_METHOD(O_sales_calculation_method IN OUT RPM_SYSTEM_OPTIONS.SALES_CALCULATION_METHOD%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_SALES_CALCULATION_METHOD';

BEGIN

   if LP_sales_calculation_method is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_sales_calculation_method := LP_sales_calculation_method;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_SALES_CALCULATION_METHOD;
------------------------------------------------------------------------

FUNCTION GET_UPDATE_ITEM_ATTRIBUTES(O_update_item_attributes IN OUT RPM_SYSTEM_OPTIONS.UPDATE_ITEM_ATTRIBUTES%TYPE,
                                    O_error_message          IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_UPDATE_ITEM_ATTRIBUTES';

BEGIN

   if LP_update_item_attributes is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_update_item_attributes := LP_update_item_attributes;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_UPDATE_ITEM_ATTRIBUTES;
------------------------------------------------------------------------

FUNCTION GET_ZERO_CURR_ENDS_IN_DECS(O_zero_curr_ends_in_decs IN OUT RPM_SYSTEM_OPTIONS.ZERO_CURR_ENDS_IN_DECS%TYPE,
                                    O_error_message          IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_ZERO_CURR_ENDS_IN_DECS';

BEGIN

   if LP_zero_curr_ends_in_decs is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_zero_curr_ends_in_decs := LP_zero_curr_ends_in_decs;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_ZERO_CURR_ENDS_IN_DECS;
------------------------------------------------------------------------

FUNCTION GET_SIM_IND(O_sim_ind       IN OUT RPM_SYSTEM_OPTIONS.SIM_IND%TYPE,
                     O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(35) := 'RPM_SYSTEM_OPTIONS_SQL.GET_SIM_IND';

BEGIN

   if LP_sim_ind is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_sim_ind := LP_sim_ind;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_SIM_IND;
------------------------------------------------------------------------

FUNCTION GET_ZONE_RANGING(O_zone_ranging  IN OUT RPM_SYSTEM_OPTIONS.ZONE_RANGING%TYPE,
                          O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_SYSTEM_OPTIONS_SQL.GET_ZONE_RANGING';

BEGIN

   if LP_zone_ranging is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_zone_ranging := LP_zone_ranging;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_ZONE_RANGING;
------------------------------------------------------------------------

FUNCTION GET_EXACT_DEAL_DATES(O_exact_deal_dates IN OUT RPM_SYSTEM_OPTIONS.EXACT_DEAL_DATES%TYPE,
                              O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(45) := 'RPM_SYSTEM_OPTIONS_SQL.GET_EXACT_DEAL_DATES';

BEGIN

   if LP_exact_deal_dates is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;


   O_exact_deal_dates := LP_exact_deal_dates;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_EXACT_DEAL_DATES;
------------------------------------------------------------------------

FUNCTION GET_LOC_MOVE_PROCESSING_DAYS(O_loc_move_processing_days IN OUT RPM_SYSTEM_OPTIONS.LOCATION_MOVE_PROCESSING_DAYS%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_LOC_MOVE_PROCESSING_DAYS';

BEGIN

   if LP_loc_move_processing_days is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_loc_move_processing_days := LP_loc_move_processing_days;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_LOC_MOVE_PROCESSING_DAYS;
------------------------------------------------------------------------

FUNCTION GET_LOCATION_MOVE_PURGE_DAYS(O_location_move_purge_days IN OUT RPM_SYSTEM_OPTIONS.LOCATION_MOVE_PURGE_DAYS%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program  VARCHAR2(75) := 'RPM_SYSTEM_OPTIONS_SQL.GET_LOCATION_MOVE_PURGE_DAYS';

BEGIN

   if LP_location_move_purge_days is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_location_move_purge_days := LP_location_move_purge_days;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_LOCATION_MOVE_PURGE_DAYS;
------------------------------------------------------------------------

FUNCTION GET_DYNAMIC_AREA_DIFF_IND(O_dynamic_area_diff_ind IN OUT RPM_SYSTEM_OPTIONS.DYNAMIC_AREA_DIFF_IND%TYPE,
                                   O_error_message         IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.GET_DYNAMIC_AREA_DIFF_IND';

BEGIN

   if LP_dynamic_area_diff_ind is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_dynamic_area_diff_ind := LP_dynamic_area_diff_ind;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_DYNAMIC_AREA_DIFF_IND;
------------------------------------------------------------------------

FUNCTION GET_LOCK_VERSION(O_lock_version IN OUT RPM_SYSTEM_OPTIONS.LOCK_VERSION%TYPE,
                    O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_SYSTEM_OPTIONS_SQL.GET_LOCK_VERSION';

BEGIN

   if LP_lock_version is NULL then

      if SET_GLOBALS(O_error_message) = FALSE then
         return FALSE;
      end if;

   end if;

   O_lock_version := LP_lock_version;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_LOCK_VERSION;
------------------------------------------------------------------------

FUNCTION GET_MAX_CLR_RECS(O_get_max_clr_recs IN OUT RPM_SYSTEM_OPTIONS.CLEARANCE_SEARCH_MAX%TYPE,
                          O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_SYSTEM_OPTIONS_SQL.GET_MAX_CLR_RECS';

BEGIN

   if SET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   O_get_max_clr_recs := LP_get_max_clr_recs;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MAX_CLR_RECS;
------------------------------------------------------------------------

FUNCTION GET_MAX_PC_RECS(O_get_max_pc_recs IN OUT RPM_SYSTEM_OPTIONS.PRICE_CHANGE_SEARCH_MAX%TYPE,
                         O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(40) := 'RPM_SYSTEM_OPTIONS_SQL.GET_MAX_PC_RECS';

BEGIN

   if SET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   O_get_max_pc_recs := LP_get_max_pc_recs;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_MAX_PC_RECS;
------------------------------------------------------------------------

FUNCTION GET_PAST_MKP_EVENTS_DISP_CNT(O_past_markup_events_disp_cnt IN OUT RPM_SYSTEM_OPTIONS.PAST_MARKUP_EVENTS_DISP_CNT%TYPE,
                                      O_error_message               IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.GET_PAST_MKP_EVENTS_DISP_CNT';

BEGIN

   if SET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   O_past_markup_events_disp_cnt := LP_past_markup_events_disp_cnt;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END GET_PAST_MKP_EVENTS_DISP_CNT;
------------------------------------------------------------------------

FUNCTION DO_NOT_RUN_CC_FOR_SUBMIT(O_skip_cc_for_submit  IN OUT RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_SUBMIT%TYPE,
                                  O_error_message       IN OUT VARCHAR2)
RETURN BOOLEAN IS

   L_program VARCHAR2(50) := 'RPM_SYSTEM_OPTIONS_SQL.DO_NOT_RUN_CC_FOR_SUBMIT';

BEGIN

   if SET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   O_skip_cc_for_submit := LP_skip_cc_for_submit;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DO_NOT_RUN_CC_FOR_SUBMIT;
------------------------------------------------------------------------

FUNCTION DO_NOT_RUN_CC_FOR_CP_APPROVAL(O_skip_cc_for_cp_approval IN OUT RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_CP_APPROVAL%TYPE,
                                       O_error_message           IN OUT VARCHAR2)
RETURN BOOLEAN IS

    L_program VARCHAR2(55) := 'RPM_SYSTEM_OPTIONS_SQL.DO_NOT_RUN_CC_FOR_CP_APPROVAL';

BEGIN

   if SET_GLOBALS(O_error_message) = FALSE then
      return FALSE;
   end if;

   O_skip_cc_for_cp_approval := LP_skip_cc_for_cp_approval;

   return TRUE;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return FALSE;

END DO_NOT_RUN_CC_FOR_CP_APPROVAL;
------------------------------------------------------------------------
END;
/

