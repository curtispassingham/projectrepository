CREATE OR REPLACE PACKAGE BODY RPM_MARKUP_CALC_SQL AS
--------------------------------------------------------

--   The CALCULATE_MARKUP function should serve as a central
--   location to markup percentages for the RPM application.
--   This function will rely heavily on the RPM_IL_MARKUP_GTT
--   table.  The following columns NEED to be populated at a
--   minimum on that table prior to this function being called:
--      - item
--      - location
--      - loc_type
--      - action_date
--      - dept
--      - retail
--      - retail_currency (somewhat optional, see below)
--      - selling_uom
--
--   The following columns are optional - they can be populated
--   by the calling function if it is necessary.  These columns
--   will not be overwritten by this function and the values in
--   them will be used for calculating the markup percentage.
--      - cost
--      - cost_currency
--      - vat_rate
--      - vat_value
--
--   The values for markup type, vat rate, vat_value cost and cost currency
--   will all be looked up (if not provided) in RMS and used when
--   calculating the markup percentage.
--
--   This function will take into account different UOMs being
--   used for cost and retail along with different currency codes
--   being used for cost and retail.
--
--   After calling this function, the RPM_IL_MARKUP_GTT table will
--   contain the cost and markup percentage for all records in the
--   table.  Calling functions should then query/join to this table
--   to get the needed information.

--------------------------------------------------------
FUNCTION CALCULATE_MARKUP(O_error_msg IN OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_MARKUP_CALC_SQL.CALCULATE_MARKUP';

   L_converted_retail_value NUMBER(20,4) := 0;
   L_converted_cost         NUMBER(20,4) := 0;

   L_class_level_vat_ind SYSTEM_OPTIONS.CLASS_LEVEL_VAT_IND%TYPE := NULL;
   L_default_tax_type    SYSTEM_OPTIONS.DEFAULT_TAX_TYPE%TYPE    := NULL;

   L_tax_calc_tbl   OBJ_TAX_CALC_TBL := OBJ_TAX_CALC_TBL();
   L_custom_tax_ind NUMBER           := NULL;

   cursor C_CHECK_CUSTOM is
      select 1
        from rpm_il_markup_gtt gtt,
             (select s.store loc,
                     RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                from store s,
                     vat_region vr
               where s.vat_region     = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM
              union all
              select w.wh loc,
                     RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                from wh w,
                     vat_region vr
               where w.vat_region     = vr.vat_region
                 and vr.vat_calc_type = RPM_CONSTANTS.VAT_CALC_TYPE_CUSTOM) locs
       where gtt.vat_rate is NULL
         and gtt.loc_type = locs.loc_type
         and gtt.location = locs.loc
         and rownum       = 1;

   cursor C_GET_UOM_DATA is
      select rilm.rowid,
             im.standard_uom,
             rilm.selling_uom,
             rilm.item,
             rilm.retail_less_vat
        from rpm_il_markup_gtt rilm,
             item_master im
       where im.item          = rilm.item
         and im.standard_uom != rilm.selling_uom;

   cursor C_GET_CURR_DATA is
      select rowid,
             cost_currency,
             retail_currency,
             cost
        from rpm_il_markup_gtt
       where cost_currency   is NOT NULL
         and retail_currency is NOT NULL
         and cost_currency   != retail_currency;

BEGIN

   select default_tax_type,
          class_level_vat_ind
     into L_default_tax_type,
          L_class_level_vat_ind
     from system_options;

   update rpm_il_markup_gtt rilm
      set (cost,
           cost_currency) = (select distinct
                                    FIRST_VALUE (fc.pricing_cost) OVER (PARTITION BY fc.item,
                                                                                     fc.location
                                                                            ORDER BY fc.active_date desc) cost,
                                    FIRST_VALUE (fc.currency_code) OVER (PARTITION BY fc.item,
                                                                                      fc.location
                                                                             ORDER BY fc.active_date) cost_currency
                               from rpm_future_cost fc
                              where fc.item         = rilm.item
                                and fc.location     = rilm.location
                                and fc.loc_type     = rilm.loc_type
                                and fc.active_date <= rilm.action_date)
    where rilm.cost          is NULL
      and rilm.cost_currency is NULL;

   if L_default_tax_type != RPM_CONSTANTS.GTAX_TAX_TYPE then

      open C_CHECK_CUSTOM;
      fetch C_CHECK_CUSTOM into L_custom_tax_ind;
      close C_CHECK_CUSTOM;

      if NVL(L_custom_tax_ind, 0) = 1 then

         select OBJ_TAX_CALC_REC(item,     -- I_item
                                 NULL,     -- I_pack_ind
                                 location, -- I_from_entity
                                 DECODE(loc_type,
                                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_from_entity_type
                                 location, -- I_to_entity
                                 DECODE(loc_type,
                                        RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                        RPM_CONSTANTS.TAX_LOC_TYPE_WH), -- I_to_entity_type
                                 action_date,     -- I_effective_from_date
                                 retail,          -- I_amount
                                 retail_currency, -- I_amount_curr
                                 NULL,            -- I_amount_tax_incl_ind
                                 NULL,            -- I_origin_country_id
                                 NULL,            -- O_cum_tax_pct
                                 NULL,            -- O_cum_tax_value
                                 NULL,            -- O_total_tax_amount
                                 NULL,            -- O_total_tax_amount_curr
                                 NULL,            -- O_total_recover_amount
                                 NULL,            -- O_total_recover_amount_curr
                                 NULL,            -- O_tax_detail_tbl
                                 'MARKUPCALC',    -- I_tran_type
                                 action_date,     -- I_tran_date
                                 NULL,            -- I_tran_id
                                 'R')             -- I_cost_retail_ind
            BULK COLLECT into L_tax_calc_tbl
           from (select item,
                        location,
                        loc_type,
                        action_date,
                        retail,
                        retail_currency
                   from rpm_il_markup_gtt
                  where loc_type IN (RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                     RPM_CONSTANTS.LOCATION_TYPE_WH)
                    and vat_rate is NULL);

         if TAX_SQL.CALC_RETAIL_TAX(O_error_msg,
                                    L_tax_calc_tbl) = FALSE then
            return 0;
         end if;

      end if;

      update rpm_il_markup_gtt rilm
         set vat_rate         = case
                                   when L_default_tax_type = RPM_CONSTANTS.SALES_TAX_TYPE then
                                      NULL
                                   when rilm.vat_rate is NOT NULL then
                                      rilm.vat_rate
                                   when NVL(L_custom_tax_ind, 0) = 0 then
                                      (select distinct
                                              FIRST_VALUE (vi.vat_rate) OVER (PARTITION BY locs.loc,
                                                                                           im.item
                                                                                  ORDER BY vi.active_date desc) vat_rate
                                         from (select store loc,
                                                      RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type,
                                                      vat_region
                                                 from store
                                               union all
                                               select wh loc,
                                                      RPM_CONSTANTS.LOCATION_TYPE_WH loc_type,
                                                      vat_region
                                                 from wh) locs,
                                              vat_item vi,
                                              item_master im,
                                              class c
                                        where rilm.loc_type   IN (RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                                                  RPM_CONSTANTS.LOCATION_TYPE_WH)
                                          and im.item         = rilm.item
                                          and vi.item         = rilm.item
                                          and locs.loc        = rilm.location
                                          and im.class        = c.class
                                          and vi.vat_type     IN (RPM_CONSTANTS.VAT_TYPE_RETAIL,
                                                                  RPM_CONSTANTS.VAT_TYPE_BOTH)
                                          and vi.vat_region   = locs.vat_region
                                          and vi.active_date <= rilm.action_date
                                          and (   L_class_level_vat_ind      = 'N'
                                               or (    L_class_level_vat_ind = 'Y'
                                                   and c.class_vat_ind       = 'Y')))
                                   else
                                      -- getting vat rate for custom tax
                                      (select /*+ CARDINALITY(tax_tbl 10) */
                                              tax_tbl.O_cum_tax_pct vat_rate
                                         from table(cast(L_tax_calc_tbl as OBJ_TAX_CALC_TBL)) tax_tbl
                                        where rilm.loc_type    IN (RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                                                   RPM_CONSTANTS.LOCATION_TYPE_WH)
                                          and rilm.item        = tax_tbl.I_item
                                          and rilm.action_date = tax_tbl.I_effective_from_date
                                          and rilm.location    = tax_tbl.I_from_entity
                                          and rilm.loc_type    = DECODE(tax_tbl.I_from_entity_type,
                                                                        RPM_CONSTANTS.TAX_LOC_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                                                                        RPM_CONSTANTS.LOCATION_TYPE_WH))
                                end,
             markup_calc_type = (select case rm.markup_calc_type
                                           when 0 then
                                              RPM_CONSTANTS.COST_MARKUP
                                           when 1 then
                                              RPM_CONSTANTS.RETAIL_MARKUP
                                        end
                                from rpm_merch_retail_def_expl rm,
                                     item_master im
                               where rilm.item   = im.item
                                 and rm.dept     = im.dept
                                 and rm.class    = im.class
                                 and rm.subclass = im.subclass);

      update rpm_il_markup_gtt
         set retail_less_vat = retail / (1 + NVL(vat_rate, 0) / 100);

   else

      merge into rpm_il_markup_gtt target
      using (select item,
                    location,
                    loc_type,
                    tax_rate,
                    tax_amount,
                    action_date
               from (select item,
                            location,
                            loc_type,
                            tax_rate,
                            tax_amount,
                            action_date,
                            ROW_NUMBER() OVER (PARTITION BY item,
                                                            location
                                                   ORDER BY effective_from_date desc) rnum
                       from (select rilm.item,
                                    rilm.location,
                                    rilm.loc_type,
                                    gt.effective_from_date,
                                    gt.cum_tax_pct tax_rate,
                                    gt.cum_tax_value tax_amount,
                                    FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY rilm.location,
                                                                                            rilm.item
                                                                                   ORDER BY gt.effective_from_date desc) max_date,
                                    rilm.action_date
                               from gtax_item_rollup gt,
                                    rpm_il_markup_gtt rilm,
                                    rpm_zone_location rzl
                              where gt.loc_type (+)             = DECODE(rilm.loc_type,
                                                                         RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                                         RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                                and gt.item                     = rilm.item
                                and (   gt.loc                  = rilm.location
                                     or (    gt.loc             = rzl.location
                                         and rzl.zone_id        = rilm.location))
                                and gt.effective_from_date (+) <= rilm.action_date
                             union all
                             select rilm.item,
                                    rilm.location,
                                    rilm.loc_type,
                                    gt.effective_from_date,
                                    gt.cum_tax_pct tax_rate,
                                    gt.cum_tax_value tax_amount,
                                    FIRST_VALUE (gt.effective_from_date) OVER (PARTITION BY rilm.location,
                                                                                            rilm.item
                                                                                   ORDER BY gt.effective_from_date desc) max_date,
                                    rilm.action_date
                               from gtax_item_rollup gt,
                                    rpm_il_markup_gtt rilm,
                                    rpm_zone_location rzl
                              where gt.loc_type (+)             = DECODE(rilm.loc_type,
                                                                         RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.TAX_LOC_TYPE_STORE,
                                                                         RPM_CONSTANTS.TAX_LOC_TYPE_WH)
                                and gt.item_parent              = rilm.item
                                and (   gt.loc                  = rilm.location
                                     or(    gt.loc              = rzl.location
                                        and rzl.zone_id         = rilm.location))
                                and gt.effective_from_date (+) <= rilm.action_date)
                      where NVL(effective_from_date, TO_DATE('3000', 'YYYY')) = NVL(max_date, TO_DATE('3000', 'YYYY'))) t
              where t.rnum = 1)src
        on (    target.item        = src.item
            and target.location    = src.location
            and target.loc_type    = src.loc_type
            and target.action_date = src.action_date)
         when MATCHED then
            update
               set vat_rate  = case
                                  when target.vat_rate is NOT NULL then
                                     target.vat_rate
                                  else
                                     NVL(src.tax_rate, 0)
                                  end,
                   vat_value = case
                                  when target.vat_value is NOT NULL then
                                     target.vat_value
                                  else
                                     NVL(src.tax_amount, 0)
                                  end;

      update rpm_il_markup_gtt rilm
         set (retail_less_vat,
              markup_calc_type) = (select case
                                             when cl.class_vat_ind = 'Y' then
                                                rilm.retail - ((rilm.retail - NVL(rilm.vat_value, 0)) *
                                                               (NVL(rilm.vat_rate, 0) / 100)) - NVl(rilm.vat_value, 0)
                                             else
                                                rilm.retail
                                          end retail_less_vat,
                                          case rm.markup_calc_type
                                             when 0 then
                                                RPM_CONSTANTS.COST_MARKUP
                                             when 1 then
                                                RPM_CONSTANTS.RETAIL_MARKUP
                                          end markup_calc_type
                                     from item_master im,
                                          class cl,
                                          rpm_merch_retail_def_expl rm
                                    where im.item     = rilm.item
                                      and cl.dept     = im.dept
                                      and cl.class    = im.class
                                      and rilm.dept   = im.dept
                                      and rm.dept     = im.dept
                                      and rm.class    = im.class
                                      and rm.subclass = im.subclass);
   end if;

   for rec IN C_GET_UOM_DATA loop

      if RPM_WRAPPER.UOM_CONVERT_VALUE(O_error_msg,
                                       L_converted_retail_value,
                                       rec.item,
                                       rec.retail_less_vat,
                                       rec.standard_uom,            --- to this UOM
                                       rec.selling_uom) = 0 then    --- from this UOM
         return 0;
      end if;

      update rpm_il_markup_gtt
         set retail_less_vat = NVL(L_converted_retail_value, 0)
       where rowid = rec.rowid;

   end loop;

   for rec IN C_GET_CURR_DATA loop

      if RPM_WRAPPER.CURRENCY_CONVERT_VALUE(O_error_msg,
                                            L_converted_cost,
                                            rec.retail_currency,
                                            rec.cost,
                                            rec.cost_currency) != 1 then
         return 0;
      end if;

      update rpm_il_markup_gtt
         set cost          = L_converted_cost,
             cost_currency = rec.retail_currency
       where rowid = rec.rowid;

   end loop;

   update rpm_il_markup_gtt rilm
      set markup_percent =
             case
                when markup_calc_type = RPM_CONSTANTS.RETAIL_MARKUP then
                   case
                      when retail_less_vat is NULL or
                           retail_less_vat = 0 then
                         ---
                         NULL
                      else
                         (1 - cost/ROUND(retail_less_vat, 4)) * 100
                   end
                else
                   case
                      when cost is NULL or
                           cost = 0 then
                         ---
                         NULL
                      else
                         (ROUND(retail_less_vat, 4)/cost - 1) * 100
                   end
             end;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CALCULATE_MARKUP;
--------------------------------------------------------

END RPM_MARKUP_CALC_SQL;
/