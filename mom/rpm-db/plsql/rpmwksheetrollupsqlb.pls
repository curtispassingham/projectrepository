CREATE OR REPLACE PACKAGE BODY RPM_WORKSHEET_ROLLUP_SQL AS
-----------------------------------------------------------------------------------------------------------------------
FUNCTION POP_ROLLUP_MODS_GTT (O_error_msg        IN OUT VARCHAR2,
                              I_rollup_mods_tbl  IN     OBJ_ROLLUP_MODS_TBL)
RETURN NUMBER;

FUNCTION MERGE_PARENT_LVL (O_error_msg            IN OUT VARCHAR2,
                           I_dept                 IN     DEPS.DEPT%TYPE,
                           I_workspace_id         IN     NUMBER,
                           I_updateable_columns   IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER;

FUNCTION MERGE_DIFFTYPE_LVL (O_error_msg            IN OUT VARCHAR2,
                             I_dept                 IN     DEPS.DEPT%TYPE,
                             I_workspace_id         IN     NUMBER,
                             I_updateable_columns   IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER;

FUNCTION MERGE_LINK_LVL (O_error_msg            IN OUT VARCHAR2,
                         I_dept                 IN     DEPS.DEPT%TYPE,
                         I_workspace_id         IN     NUMBER,
                         I_updateable_columns   IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER;

FUNCTION UPDATE_WORKSHEET_ITEM_DATA(O_error_msg     IN OUT VARCHAR2,
                                    I_workspace_id  IN     NUMBER)
RETURN NUMBER;

FUNCTION UPDATE_WORKSHEET_ITEM_LOC_DATA(O_error_msg     IN OUT VARCHAR2,
                                        I_workspace_id  IN     NUMBER)
RETURN NUMBER;

FUNCTION UPDATE_WORKSHEET_ZONE_DATA(O_error_msg     IN OUT VARCHAR2,
                                    I_workspace_id  IN     NUMBER)
RETURN NUMBER;

FUNCTION APPLY_PRICE_GUIDE(I_selling_retail    IN     NUMBER,
                           I_price_guide_id    IN     NUMBER)
RETURN NUMBER;

-----------------------------------------------------------------------------------------------------------------------

FUNCTION POP_ROLLUP_MODS_GTT (O_error_msg        IN OUT VARCHAR2,
                              I_rollup_mods_tbl  IN     OBJ_ROLLUP_MODS_TBL)
RETURN NUMBER IS
BEGIN

   insert into rpm_worksheet_rollup_mods_gtt(workspace_id,
                                             rollup_level,
                                             link_code,
                                             zone_id,
                                             location,
                                             loc_type,
                                             item,
                                             item_parent,
                                             diff_1,
                                             diff_type_1,
                                             diff_2,
                                             diff_type_2,
                                             diff_3,
                                             diff_type_3,
                                             diff_4,
                                             diff_type_4,
                                             action_flag,
                                             effective_date,
                                             new_retail,
                                             new_retail_std,
                                             new_retail_uom,
                                             new_multi_units,
                                             ignore_constraint_boolean,
                                             new_multi_unit_uom,
                                             new_multi_unit_retail,
                                             new_clear_ind,
                                             new_reset_date,
                                             new_out_of_stock_date,
                                             state,
                                             new_clear_mkdn_nbr,
                                             worksheet_status_id,
                                             reviewed)
      select /*+ cardinality(t 1000) */
             t.workspace_id,
             t.rollup_level,
             t.link_code,
             t.zone_id,
             t.location,
             t.loc_type,
             t.item,
             t.item_parent,
             t.diff_1,
             t.diff_type_1,
             t.diff_2,
             t.diff_type_2,
             t.diff_3,
             t.diff_type_3,
             t.diff_4,
             t.diff_type_4,
             t.action_flag,
             t.effective_date,
             t.new_retail,
             t.new_retail_std,
             t.new_retail_uom,
             t.new_multi_units,
             t.ignore_constraint_boolean,
             t.new_multi_unit_uom,
             t.new_multi_unit_retail,
             t.new_clear_ind,
             t.new_reset_date,
             t.new_out_of_stock_date,
             t.state,
             t.new_clear_mkdn_nbr,
             t.worksheet_status_id,
             t.reviewed
        from table(cast(I_rollup_mods_tbl as OBJ_ROLLUP_MODS_TBL)) t;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.POP_ROLLUP_MODS_GTT',
                                        to_char(SQLCODE));
      RETURN 0;

END POP_ROLLUP_MODS_GTT;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION MERGE_PARENT_LVL (O_error_msg            IN OUT VARCHAR2,
                           I_dept                 IN     DEPS.DEPT%TYPE,
                           I_workspace_id         IN     NUMBER,
                           I_updateable_columns   IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER IS
BEGIN

   merge into rpm_worksheet_zone_workspace target
   using (select rwd.worksheet_item_data_id,
                 rwd.workspace_id,
                 case
                    when inp.action_flag = 1 then
                       param.action_flag
                    else
                       rwd.action_flag
                 end as action_flag,
                 case
                    when inp.effective_date = 1 then
                       param.effective_date
                    else
                       rwd.effective_date
                 end as effective_date,
                 case
                    when inp.new_retail = 1 then
                       param.new_retail
                    else
                       rwd.new_retail
                 end as new_retail,
                 case
                    when inp.new_retail_std = 1 then
                       param.new_retail_std
                    else
                       rwd.new_retail_std
                 end as new_retail_std,
                 case
                    when inp.new_retail_uom = 1 then
                       param.new_retail_uom
                    else
                       rwd.new_retail_uom
                 end as new_retail_uom,
                 case
                    when inp.new_multi_units = 1 then
                       param.new_multi_units
                    else
                       rwd.new_multi_units
                 end as new_multi_units,
                 case
                    when inp.ignore_constraint_boolean = 1 then
                       param.ignore_constraint_boolean
                    else
                       rwd.ignore_constraint_boolean
                 end as ignore_constraint_boolean,
                 case
                    when inp.new_multi_unit_uom = 1 then
                       param.new_multi_unit_uom
                    else
                       rwd.new_multi_unit_uom
                 end as new_multi_unit_uom,
                 case
                    when inp.new_multi_unit_retail = 1 then
                       param.new_multi_unit_retail
                    else
                       rwd.new_multi_unit_retail
                 end as new_multi_unit_retail,
                 case
                    when inp.new_clear_ind = 1 then
                       param.new_clear_ind
                    else
                       rwd.new_clear_ind
                 end as new_clear_ind,
                 case
                    when inp.new_reset_date = 1 then
                       param.new_reset_date
                    else
                       rwd.new_reset_date
                 end as new_reset_date,
                 case
                    when inp.new_out_of_stock_date = 1 then
                       param.new_out_of_stock_date
                    else
                       rwd.new_out_of_stock_date
                 end as new_out_of_stock_date,
                 case
                    when inp.state = 1 then
                       param.state
                    else
                       rwd.state
                 end as state,
                 case
                    when inp.new_clear_mkdn_nbr = 1 then
                       param.new_clear_mkdn_nbr
                    else
                       rwd.new_clear_mkdn_nbr
                 end as new_clear_mkdn_nbr,
                 case
                    when inp.reviewed = 1 then
                       param.reviewed
                    else
                       rwd.reviewed
                 end as reviewed,
                 case
                    when inp.effective_date +
                         inp.new_retail +
                         inp.new_retail_std +
                         inp.new_retail_uom +
                         inp.new_multi_units +
                         inp.ignore_constraint_boolean +
                         inp.new_multi_unit_uom +
                         inp.new_multi_unit_retail +
                         inp.new_clear_ind +
                         inp.new_reset_date +
                         inp.new_out_of_stock_date +
                         inp.state +
                         inp.new_clear_mkdn_nbr +
                         inp.reviewed >= 1 then 1
                     else
                        0
                 end as update_ind
            from rpm_worksheet_rollup_mods_gtt param,
                 rpm_worksheet_zone_workspace rwd,
                 (select action_flag,
                         effective_date,
                         new_retail,
                         new_retail_std,
                         new_retail_uom,
                         new_multi_units,
                         ignore_constraint_boolean,
                         new_multi_unit_uom,
                         new_multi_unit_retail,
                         new_clear_ind,
                         new_reset_date,
                         new_out_of_stock_date,
                         state,
                         new_clear_mkdn_nbr,
                         reviewed
                    from (select max(case when value(ids) = 'ACTION_FLAG' then 1 else 0 end) action_flag,
                                 max(case when value(ids) = 'EFFECTIVE_DATE' then 1 else 0 end) effective_date,
                                 max(case when value(ids) = 'NEW_RETAIL' then 1 else 0 end) new_retail,
                                 max(case when value(ids) = 'NEW_RETAIL_STD' then 1 else 0 end) new_retail_std,
                                 max(case when value(ids) = 'NEW_RETAIL_UOM' then 1 else 0 end) new_retail_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNITS' then 1 else 0 end) new_multi_units,
                                 max(case when value(ids) = 'IGNORE_CONSTRAINT_BOOLEAN' then 1 else 0 end) ignore_constraint_boolean,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_UOM' then 1 else 0 end) new_multi_unit_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_RETAIL' then 1 else 0 end) new_multi_unit_retail,
                                 max(case when value(ids) = 'NEW_CLEAR_IND' then 1 else 0 end) new_clear_ind,
                                 max(case when value(ids) = 'NEW_RESET_DATE' then 1 else 0 end) new_reset_date,
                                 max(case when value(ids) = 'NEW_OUT_OF_STOCK_DATE' then 1 else 0 end) new_out_of_stock_date,
                                 max(case when value(ids) = 'STATE' then 1 else 0 end) state,
                                 max(case when value(ids) = 'NEW_CLEAR_MKDN_NBR' then 1 else 0 end) new_clear_mkdn_nbr,
                                 max(case when value(ids) = 'REVIEWED' then 1 else 0 end) reviewed
                            from table(cast(I_updateable_columns as OBJ_VARCHAR_DESC_TABLE)) ids
                         )
                 ) inp
           where rwd.workspace_id             = I_workspace_id
             and NVL(rwd.item_parent, -99999) = NVL(param.item_parent, -99999)
             and rwd.dept                     = I_dept
             and rwd.worksheet_status_id      = param.worksheet_status_id
             and (rwd.zone_id = param.zone_id)
             and (   (    param.rollup_level = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_ITEM
                      and rwd.link_code      is NULL
                      and rwd.item           = param.item)
                  or (    param.rollup_level = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_ITEM_PARENT
                      and rwd.link_code      is NULL)
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_1
                      and rwd.link_code           is NULL
                      and NVL(rwd.diff_1, -99999) = NVL(param.diff_1, -99999))
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_2
                      and rwd.link_code           is NULL
                      and NVL(rwd.diff_2, -99999) = NVL(param.diff_2, -99999))
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_3
                      and rwd.link_code           is NULL
                      and NVL(rwd.diff_3, -99999) = NVL(param.diff_3, -99999))
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_4
                      and rwd.link_code           is NULL
                      and NVL(rwd.diff_4, -99999) = NVL(param.diff_4, -99999)))
         ) source
      on (    target.worksheet_item_data_id = source.worksheet_item_data_id
          and target.workspace_id           = source.workspace_id)
   when matched then
      update
         set action_flag               = source.action_flag,
             effective_date            = source.effective_date,
             new_retail                = source.new_retail,
             new_retail_std            = source.new_retail_std,
             new_retail_uom            = source.new_retail_uom,
             new_multi_units           = source.new_multi_units,
             ignore_constraint_boolean = source.ignore_constraint_boolean,
             new_multi_unit_uom        = source.new_multi_unit_uom,
             new_multi_unit_retail     = source.new_multi_unit_retail,
             update_ind                = source.update_ind,
             reviewed                  = source.reviewed,
             ---
             new_clear_ind = case
                                when target.clear_boolean = 0 or
                                     target.clear_boolean is NULL then
                                   source.new_clear_ind
                                else
                                   target.new_clear_ind
                             end,
             state = case
                        when source.state is NOT NULL then
                           source.state
                        else
                           target.state
                     end,
             new_clear_mkdn_nbr = case
                                     when target.new_clear_mkdn_nbr is NULL then
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              1
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                     else
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              target.new_clear_mkdn_nbr
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                  end,
             new_reset_date = case (case
                                       when target.clear_boolean = 0 or
                                            target.clear_boolean is NULL then
                                          source.new_clear_ind
                                       else
                                          target.new_clear_ind
                                    end)
                                 when 1 then
                                    source.new_reset_date
                                 when 0 then
                                    NULL
                                 when NULL then
                                    NULL
                              end,
             new_out_of_stock_date = case (case
                                              when target.clear_boolean = 0 or
                                                   target.clear_boolean is NULL then
                                                 source.new_clear_ind
                                              else
                                                 target.new_clear_ind
                                           end)
                                        when 1 then
                                           source.new_out_of_stock_date
                                        when 0 then
                                           NULL
                                        when NULL then
                                           NULL
                                     end;

   merge into rpm_worksheet_il_workspace target
   using (select /*+ ORDERED */
                 rwd.worksheet_item_data_id,
                 rwd.worksheet_item_loc_data_id,
                 rwd.workspace_id,
                 case
                    when inp.action_flag = 1 then
                       param.action_flag
                    else
                       rwd.action_flag
                 end as action_flag,
                 case
                    when inp.effective_date = 1 then
                       param.effective_date
                    else
                       rwd.effective_date
                 end as effective_date,
                 case
                    when inp.new_retail = 1 then
                       param.new_retail
                    else
                       rwd.new_retail
                 end as new_retail,
                 case
                    when inp.new_retail_std = 1 then
                       param.new_retail_std
                    else
                       rwd.new_retail_std
                 end as new_retail_std,
                 case
                    when inp.new_retail_uom = 1 then
                       param.new_retail_uom
                    else
                       rwd.new_retail_uom
                 end as new_retail_uom,
                 case
                    when inp.new_multi_units = 1 then
                       param.new_multi_units
                    else
                       rwd.new_multi_units
                 end as new_multi_units,
                 case
                    when inp.ignore_constraint_boolean = 1 then
                       param.ignore_constraint_boolean
                    else
                       rwd.ignore_constraint_boolean
                 end as ignore_constraint_boolean,
                 case
                    when inp.new_multi_unit_uom = 1 then
                       param.new_multi_unit_uom
                    else
                       rwd.new_multi_unit_uom
                 end as new_multi_unit_uom,
                 case
                    when inp.new_multi_unit_retail = 1 then
                       param.new_multi_unit_retail
                    else
                       rwd.new_multi_unit_retail
                 end as new_multi_unit_retail,
                 case
                    when inp.new_clear_ind = 1 then
                       param.new_clear_ind
                    else
                       rwd.new_clear_ind
                 end as new_clear_ind,
                 case
                    when inp.new_reset_date = 1 then
                       param.new_reset_date
                    else
                       rwd.new_reset_date
                 end as new_reset_date,
                 case
                    when inp.new_out_of_stock_date = 1 then
                       param.new_out_of_stock_date
                    else
                       rwd.new_out_of_stock_date
                 end as new_out_of_stock_date,
                 case
                    when inp.new_clear_mkdn_nbr = 1 then
                       param.new_clear_mkdn_nbr
                    else
                       rwd.new_clear_mkdn_nbr
                 end as new_clear_mkdn_nbr,
                 case
                    when inp.reviewed = 1 then
                       param.reviewed
                    else
                       rwd.reviewed
                 end as reviewed,
                 case
                    when inp.effective_date +
                         inp.new_retail +
                         inp.new_retail_std +
                         inp.new_retail_uom +
                         inp.new_multi_units +
                         inp.ignore_constraint_boolean +
                         inp.new_multi_unit_uom +
                         inp.new_multi_unit_retail +
                         inp.new_clear_ind +
                         inp.new_reset_date +
                         inp.new_out_of_stock_date +
                         inp.state +
                         inp.new_clear_mkdn_nbr +
                         inp.reviewed >= 1 then 1
                     else
                        0
                 end as update_ind
            from rpm_worksheet_rollup_mods_gtt param,
                 rpm_worksheet_zone_workspace rwz,
                 rpm_worksheet_il_workspace rwd,
                 (select action_flag,
                         effective_date,
                         new_retail,
                         new_retail_std,
                         new_retail_uom,
                         new_multi_units,
                         ignore_constraint_boolean,
                         new_multi_unit_uom,
                         new_multi_unit_retail,
                         new_clear_ind,
                         new_reset_date,
                         new_out_of_stock_date,
                         state,
                         new_clear_mkdn_nbr,
                         reviewed
                    from (select max(case when value(ids) = 'ACTION_FLAG' then 1 else 0 end) action_flag,
                                 max(case when value(ids) = 'EFFECTIVE_DATE' then 1 else 0 end) effective_date,
                                 max(case when value(ids) = 'NEW_RETAIL' then 1 else 0 end) new_retail,
                                 max(case when value(ids) = 'NEW_RETAIL_STD' then 1 else 0 end) new_retail_std,
                                 max(case when value(ids) = 'NEW_RETAIL_UOM' then 1 else 0 end) new_retail_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNITS' then 1 else 0 end) new_multi_units,
                                 max(case when value(ids) = 'IGNORE_CONSTRAINT_BOOLEAN' then 1 else 0 end) ignore_constraint_boolean,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_UOM' then 1 else 0 end) new_multi_unit_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_RETAIL' then 1 else 0 end) new_multi_unit_retail,
                                 max(case when value(ids) = 'NEW_CLEAR_IND' then 1 else 0 end) new_clear_ind,
                                 max(case when value(ids) = 'NEW_RESET_DATE' then 1 else 0 end) new_reset_date,
                                 max(case when value(ids) = 'NEW_OUT_OF_STOCK_DATE' then 1 else 0 end) new_out_of_stock_date,
                                 max(case when value(ids) = 'STATE' then 1 else 0 end) state,
                                 max(case when value(ids) = 'NEW_CLEAR_MKDN_NBR' then 1 else 0 end) new_clear_mkdn_nbr,
                                 max(case when value(ids) = 'REVIEWED' then 1 else 0 end) reviewed
                            from table(cast(I_updateable_columns as OBJ_VARCHAR_DESC_TABLE)) ids
                         )
                 ) inp
           where rwz.workspace_id             = I_workspace_id
             and NVL(rwz.item_parent, -99999) = NVL(param.item_parent, -99999)
             and rwz.dept                     = I_dept
             and rwz.worksheet_status_id      = param.worksheet_status_id
             and (rwz.zone_id = param.zone_id)
             and (   (    param.rollup_level = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_ITEM
                      and rwd.link_code      is NULL
                      and rwz.item           = param.item)
                  or (    param.rollup_level = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_ITEM_PARENT
                      and rwz.link_code      is NULL)
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_1
                      and rwz.link_code           is NULL
                      and NVL(rwz.diff_1, -99999) = NVL(param.diff_1, -99999))
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_2
                      and rwz.link_code           is NULL
                      and NVL(rwz.diff_2, -99999) = NVL(param.diff_2, -99999))
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_3
                      and rwz.link_code           is NULL
                      and NVL(rwz.diff_3, -99999) = NVL(param.diff_3, -99999))
                  or (    param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_4
                      and rwz.link_code           is NULL
                      and NVL(rwz.diff_4, -99999) = NVL(param.diff_4, -99999)))
             and rwd.workspace_id               = rwz.workspace_id
             and rwd.worksheet_item_data_id     = rwz.worksheet_item_data_id
         ) source
      on (    target.worksheet_item_data_id = source.worksheet_item_data_id
          and target.worksheet_item_loc_data_id = source.worksheet_item_loc_data_id
          and target.workspace_id           = source.workspace_id)
   when matched then
      update
         set action_flag               = source.action_flag,
             effective_date            = source.effective_date,
             new_retail                = source.new_retail,
             new_retail_std            = source.new_retail_std,
             new_retail_uom            = source.new_retail_uom,
             new_multi_units           = source.new_multi_units,
             ignore_constraint_boolean = source.ignore_constraint_boolean,
             new_multi_unit_uom        = source.new_multi_unit_uom,
             new_multi_unit_retail     = source.new_multi_unit_retail,
             update_ind                = source.update_ind,
             reviewed                  = source.reviewed,
             ---
             new_clear_ind = case
                                when target.clear_boolean = 0 or
                                     target.clear_boolean is NULL then
                                   source.new_clear_ind
                                else
                                   target.new_clear_ind
                             end,
             new_clear_mkdn_nbr = case
                                     when target.new_clear_mkdn_nbr is NULL then
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              1
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                     else
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              target.new_clear_mkdn_nbr
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                  end,
             new_reset_date = case (case
                                       when target.clear_boolean = 0 or
                                            target.clear_boolean is NULL then
                                          source.new_clear_ind
                                       else
                                          target.new_clear_ind
                                    end)
                                 when 1 then
                                    source.new_reset_date
                                 when 0 then
                                    NULL
                                 when NULL then
                                    NULL
                              end,
             new_out_of_stock_date = case (case
                                              when target.clear_boolean = 0 or
                                                   target.clear_boolean is NULL then
                                                 source.new_clear_ind
                                              else
                                                 target.new_clear_ind
                                           end)
                                        when 1 then
                                           source.new_out_of_stock_date
                                        when 0 then
                                           NULL
                                        when NULL then
                                           NULL
                                     end;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.MERGE_PARENT_LVL',
                                        to_char(SQLCODE));
      RETURN 0;

END MERGE_PARENT_LVL;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION MERGE_DIFFTYPE_LVL (O_error_msg            IN OUT VARCHAR2,
                             I_dept                 IN     DEPS.DEPT%TYPE,
                             I_workspace_id         IN     NUMBER,
                             I_updateable_columns   IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER IS
BEGIN

   merge into rpm_worksheet_zone_workspace target
   using (select rwd.worksheet_item_data_id,
                 rwd.workspace_id,
                 case
                    when inp.action_flag = 1 then
                       param.action_flag
                    else
                       rwd.action_flag
                 end as action_flag,
                 case
                    when inp.effective_date = 1 then
                       param.effective_date
                    else
                       rwd.effective_date
                 end as effective_date,
                 case
                    when inp.new_retail = 1 then
                       param.new_retail
                    else
                       rwd.new_retail
                 end as new_retail,
                 case
                    when inp.new_retail_std = 1 then
                       param.new_retail_std
                    else
                       rwd.new_retail_std
                 end as new_retail_std,
                 case
                    when inp.new_retail_uom = 1 then
                       param.new_retail_uom
                    else
                       rwd.new_retail_uom
                 end as new_retail_uom,
                 case
                    when inp.new_multi_units = 1 then
                       param.new_multi_units
                    else
                       rwd.new_multi_units
                 end as new_multi_units,
                 case
                    when inp.ignore_constraint_boolean = 1 then
                       param.ignore_constraint_boolean
                    else
                       rwd.ignore_constraint_boolean
                 end as ignore_constraint_boolean,
                 case
                    when inp.new_multi_unit_uom = 1 then
                       param.new_multi_unit_uom
                    else
                       rwd.new_multi_unit_uom
                 end as new_multi_unit_uom,
                 case
                    when inp.new_multi_unit_retail = 1 then
                       param.new_multi_unit_retail
                    else
                       rwd.new_multi_unit_retail
                 end as new_multi_unit_retail,
                 case
                    when inp.new_clear_ind = 1 then
                       param.new_clear_ind
                    else
                       rwd.new_clear_ind
                 end as new_clear_ind,
                 case
                    when inp.new_reset_date = 1 then
                       param.new_reset_date
                    else
                       rwd.new_reset_date
                 end as new_reset_date,
                 case
                    when inp.new_out_of_stock_date = 1 then
                       param.new_out_of_stock_date
                    else
                       rwd.new_out_of_stock_date
                 end as new_out_of_stock_date,
                 case
                    when inp.state = 1 then
                       param.state
                    else
                       rwd.state
                 end as state,
                 case
                    when inp.new_clear_mkdn_nbr = 1 then
                       param.new_clear_mkdn_nbr
                    else
                       rwd.new_clear_mkdn_nbr
                 end as new_clear_mkdn_nbr,
                 case
                    when inp.reviewed = 1 then
                       param.reviewed
                    else
                       rwd.reviewed
                 end as reviewed,
                 case
                    when inp.effective_date +
                         inp.new_retail +
                         inp.new_retail_std +
                         inp.new_retail_uom +
                         inp.new_multi_units +
                         inp.ignore_constraint_boolean +
                         inp.new_multi_unit_uom +
                         inp.new_multi_unit_retail +
                         inp.new_clear_ind +
                         inp.new_reset_date +
                         inp.new_out_of_stock_date +
                         inp.state +
                         inp.new_clear_mkdn_nbr +
                         inp.reviewed >= 1 then 1
                     else
                        0
                 end as update_ind
            from rpm_worksheet_rollup_mods_gtt param,
                 rpm_worksheet_zone_workspace rwd,
                 (select action_flag,
                         effective_date,
                         new_retail,
                         new_retail_std,
                         new_retail_uom,
                         new_multi_units,
                         ignore_constraint_boolean,
                         new_multi_unit_uom,
                         new_multi_unit_retail,
                         new_clear_ind,
                         new_reset_date,
                         new_out_of_stock_date,
                         state,
                         new_clear_mkdn_nbr,
                         reviewed
                    from (select max(case when value(ids) = 'ACTION_FLAG' then 1 else 0 end) action_flag,
                                 max(case when value(ids) = 'EFFECTIVE_DATE' then 1 else 0 end) effective_date,
                                 max(case when value(ids) = 'NEW_RETAIL' then 1 else 0 end) new_retail,
                                 max(case when value(ids) = 'NEW_RETAIL_STD' then 1 else 0 end) new_retail_std,
                                 max(case when value(ids) = 'NEW_RETAIL_UOM' then 1 else 0 end) new_retail_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNITS' then 1 else 0 end) new_multi_units,
                                 max(case when value(ids) = 'IGNORE_CONSTRAINT_BOOLEAN' then 1 else 0 end) ignore_constraint_boolean,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_UOM' then 1 else 0 end) new_multi_unit_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_RETAIL' then 1 else 0 end) new_multi_unit_retail,
                                 max(case when value(ids) = 'NEW_CLEAR_IND' then 1 else 0 end) new_clear_ind,
                                 max(case when value(ids) = 'NEW_RESET_DATE' then 1 else 0 end) new_reset_date,
                                 max(case when value(ids) = 'NEW_OUT_OF_STOCK_DATE' then 1 else 0 end) new_out_of_stock_date,
                                 max(case when value(ids) = 'STATE' then 1 else 0 end) state,
                                 max(case when value(ids) = 'NEW_CLEAR_MKDN_NBR' then 1 else 0 end) new_clear_mkdn_nbr,
                                 max(case when value(ids) = 'REVIEWED' then 1 else 0 end) reviewed
                            from table(cast(I_updateable_columns as OBJ_VARCHAR_DESC_TABLE)) ids
                         )
                 ) inp
           where rwd.workspace_id               = I_workspace_id
             and nvl(rwd.item_parent, rwd.item) = nvl(param.item_parent, param.item)
             and rwd.dept                       = I_dept
             and rwd.worksheet_status_id        = param.worksheet_status_id
             and rwd.zone_id    = param.zone_id
             and (    param.rollup_level = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_TYPE
                  and rwd.link_code is NULL
                  and (   (    nvl(rwd.diff_1, -99999) = nvl(param.diff_1, -99999)
                           and rwd.diff_type_1         = param.diff_type_1)
                       or (    nvl(rwd.diff_2, -99999) = nvl(param.diff_2, -99999)
                           and rwd.diff_type_2         = param.diff_type_2)
                       or (    nvl(rwd.diff_3, -99999) = nvl(param.diff_3, -99999)
                           and rwd.diff_type_3         = param.diff_type_3)
                       or (    nvl(rwd.diff_4, -99999) = nvl(param.diff_4, -99999)
                           and rwd.diff_type_4         = param.diff_type_4)))
         ) source
      on (    target.worksheet_item_data_id = source.worksheet_item_data_id
          and target.workspace_id      = source.workspace_id)
   when matched then
      update
         set action_flag               = source.action_flag,
             effective_date            = source.effective_date,
             new_retail                = source.new_retail,
             new_retail_std            = source.new_retail_std,
             new_retail_uom            = source.new_retail_uom,
             new_multi_units           = source.new_multi_units,
             ignore_constraint_boolean = source.ignore_constraint_boolean,
             new_multi_unit_uom        = source.new_multi_unit_uom,
             new_multi_unit_retail     = source.new_multi_unit_retail,
             update_ind                = source.update_ind,
             reviewed                  = source.reviewed,
             ---
             new_clear_ind = case
                                when target.clear_boolean = 0 or
                                     target.clear_boolean is NULL then
                                   source.new_clear_ind
                                else
                                   target.new_clear_ind
                             end,
             state = case
                        when source.state is NOT NULL then
                           source.state
                        else
                           target.state
                     end,
             new_clear_mkdn_nbr = case
                                     when target.new_clear_mkdn_nbr is NULL then
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              1
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                     else
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              target.new_clear_mkdn_nbr
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                  end,
             new_reset_date = case (case
                                       when target.clear_boolean = 0 or
                                            target.clear_boolean is NULL then
                                          source.new_clear_ind
                                       else
                                          target.new_clear_ind
                                    end)
                                 when 1 then
                                    source.new_reset_date
                                 when 0 then
                                    NULL
                                 when NULL then
                                    NULL
                              end,
             new_out_of_stock_date = case (case
                                              when target.clear_boolean = 0 or
                                                   target.clear_boolean is NULL then
                                                 source.new_clear_ind
                                              else
                                                 target.new_clear_ind
                                           end)
                                        when 1 then
                                           source.new_out_of_stock_date
                                        when 0 then
                                           NULL
                                        when NULL then
                                           NULL
                                     end
   when not matched then
      insert (workspace_id) values (-9999);

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.MERGE_DIFFTYPE_LVL',
                                        to_char(SQLCODE));
      RETURN 0;

END MERGE_DIFFTYPE_LVL;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION MERGE_LINK_LVL (O_error_msg            IN OUT VARCHAR2,
                         I_dept                 IN     DEPS.DEPT%TYPE,
                         I_workspace_id         IN     NUMBER,
                         I_updateable_columns   IN     OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER IS
BEGIN

   merge into rpm_worksheet_zone_workspace target
   using (select rwd.worksheet_item_data_id,
                 rwd.workspace_id,
                 case
                    when inp.action_flag = 1 then
                       param.action_flag
                    else
                       rwd.action_flag
                 end as action_flag,
                 case
                    when inp.effective_date = 1 then
                       param.effective_date
                    else
                       rwd.effective_date
                 end as effective_date,
                 case
                    when inp.new_retail = 1 then
                       param.new_retail
                    else
                       rwd.new_retail
                 end as new_retail,
                 case
                    when inp.new_retail_std = 1 then
                       param.new_retail_std
                    else
                       rwd.new_retail_std
                 end as new_retail_std,
                 case
                    when inp.new_retail_uom = 1 then
                       param.new_retail_uom
                    else
                       rwd.new_retail_uom
                 end as new_retail_uom,
                 case
                    when inp.new_multi_units = 1 then
                       param.new_multi_units
                    else
                       rwd.new_multi_units
                 end as new_multi_units,
                 case
                    when inp.ignore_constraint_boolean = 1 then
                       param.ignore_constraint_boolean
                    else
                       rwd.ignore_constraint_boolean
                 end as ignore_constraint_boolean,
                 case
                    when inp.new_multi_unit_uom = 1 then
                       param.new_multi_unit_uom
                    else
                       rwd.new_multi_unit_uom
                 end as new_multi_unit_uom,
                 case
                    when inp.new_multi_unit_retail = 1 then
                       param.new_multi_unit_retail
                    else
                       rwd.new_multi_unit_retail
                 end as new_multi_unit_retail,
                 case
                    when inp.new_clear_ind = 1 then
                       param.new_clear_ind
                    else
                       rwd.new_clear_ind
                 end as new_clear_ind,
                 case
                    when inp.new_reset_date = 1 then
                       param.new_reset_date
                    else
                       rwd.new_reset_date
                 end as new_reset_date,
                 case
                    when inp.new_out_of_stock_date = 1 then
                       param.new_out_of_stock_date
                    else
                       rwd.new_out_of_stock_date
                 end as new_out_of_stock_date,
                 case
                    when inp.state = 1 then
                       param.state
                    else
                       rwd.state
                 end as state,
                 case
                    when inp.new_clear_mkdn_nbr = 1 then
                       param.new_clear_mkdn_nbr
                    else
                       rwd.new_clear_mkdn_nbr
                 end as new_clear_mkdn_nbr,
                 case
                    when inp.reviewed = 1 then
                       param.reviewed
                    else
                       rwd.reviewed
                 end as reviewed,
                 case
                    when inp.effective_date +
                         inp.new_retail +
                         inp.new_retail_std +
                         inp.new_retail_uom +
                         inp.new_multi_units +
                         inp.ignore_constraint_boolean +
                         inp.new_multi_unit_uom +
                         inp.new_multi_unit_retail +
                         inp.new_clear_ind +
                         inp.new_reset_date +
                         inp.new_out_of_stock_date +
                         inp.state +
                         inp.new_clear_mkdn_nbr +
                         inp.reviewed >= 1 then 1
                     else
                        0
                 end as update_ind
            from rpm_worksheet_rollup_mods_gtt param,
                 rpm_worksheet_zone_workspace rwd,
                 (select action_flag,
                         effective_date,
                         new_retail,
                         new_retail_std,
                         new_retail_uom,
                         new_multi_units,
                         ignore_constraint_boolean,
                         new_multi_unit_uom,
                         new_multi_unit_retail,
                         new_clear_ind,
                         new_reset_date,
                         new_out_of_stock_date,
                         state,
                         new_clear_mkdn_nbr,
                         reviewed
                    from (select max(case when value(ids) = 'ACTION_FLAG' then 1 else 0 end) action_flag,
                                 max(case when value(ids) = 'EFFECTIVE_DATE' then 1 else 0 end) effective_date,
                                 max(case when value(ids) = 'NEW_RETAIL' then 1 else 0 end) new_retail,
                                 max(case when value(ids) = 'NEW_RETAIL_STD' then 1 else 0 end) new_retail_std,
                                 max(case when value(ids) = 'NEW_RETAIL_UOM' then 1 else 0 end) new_retail_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNITS' then 1 else 0 end) new_multi_units,
                                 max(case when value(ids) = 'IGNORE_CONSTRAINT_BOOLEAN' then 1 else 0 end) ignore_constraint_boolean,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_UOM' then 1 else 0 end) new_multi_unit_uom,
                                 max(case when value(ids) = 'NEW_MULTI_UNIT_RETAIL' then 1 else 0 end) new_multi_unit_retail,
                                 max(case when value(ids) = 'NEW_CLEAR_IND' then 1 else 0 end) new_clear_ind,
                                 max(case when value(ids) = 'NEW_RESET_DATE' then 1 else 0 end) new_reset_date,
                                 max(case when value(ids) = 'NEW_OUT_OF_STOCK_DATE' then 1 else 0 end) new_out_of_stock_date,
                                 max(case when value(ids) = 'STATE' then 1 else 0 end) state,
                                 max(case when value(ids) = 'NEW_CLEAR_MKDN_NBR' then 1 else 0 end) new_clear_mkdn_nbr,
                                 max(case when value(ids) = 'REVIEWED' then 1 else 0 end) reviewed
                            from table(cast(I_updateable_columns as OBJ_VARCHAR_DESC_TABLE)) ids
                         )
                 ) inp
           where rwd.workspace_id        = I_workspace_id
             and rwd.link_code           = param.link_code
             and rwd.dept                = I_dept
             and rwd.worksheet_status_id = param.worksheet_status_id
             and param.rollup_level      = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_LINK_CODE
             and param.link_code         is NOT NULL
             and rwd.zone_id    = param.zone_id
         ) source
      on (    target.worksheet_item_data_id = source.worksheet_item_data_id
          and target.workspace_id      = source.workspace_id)
   when matched then
      update
         set action_flag               = source.action_flag,
             effective_date            = source.effective_date,
             new_retail                = source.new_retail,
             new_retail_std            = source.new_retail_std,
             new_retail_uom            = source.new_retail_uom,
             new_multi_units           = source.new_multi_units,
             ignore_constraint_boolean = source.ignore_constraint_boolean,
             new_multi_unit_uom        = source.new_multi_unit_uom,
             new_multi_unit_retail     = source.new_multi_unit_retail,
             update_ind                = source.update_ind,
             reviewed                  = source.reviewed,
             ---
             new_clear_ind = case
                                when target.clear_boolean = 0 or
                                     target.clear_boolean is NULL then
                                   source.new_clear_ind
                                else
                                   target.new_clear_ind
                             end,
             state = case
                        when source.state is NOT NULL then
                           source.state
                        else
                           target.state
                     end,
             new_clear_mkdn_nbr = case
                                     when target.new_clear_mkdn_nbr is NULL then
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              1
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                     else
                                        case (case
                                                 when target.clear_boolean = 0 or
                                                      target.clear_boolean is NULL then
                                                    source.new_clear_ind
                                                 else
                                                    target.new_clear_ind
                                              end)
                                           when 1 then
                                              target.new_clear_mkdn_nbr
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                  end,
             new_reset_date = case (case
                                       when target.clear_boolean = 0 or
                                            target.clear_boolean is NULL then
                                          source.new_clear_ind
                                       else
                                          target.new_clear_ind
                                    end)
                                 when 1 then
                                    source.new_reset_date
                                 when 0 then
                                    NULL
                                 when NULL then
                                    NULL
                              end,
             new_out_of_stock_date = case (case
                                              when target.clear_boolean = 0 or
                                                   target.clear_boolean is NULL then
                                                 source.new_clear_ind
                                              else
                                                 target.new_clear_ind
                                           end)
                                        when 1 then
                                           source.new_out_of_stock_date
                                        when 0 then
                                           NULL
                                        when NULL then
                                           NULL
                                     end
   when not matched then
      insert (workspace_id) values (-9999);

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.MERGE_LINK_LVL',
                                        to_char(SQLCODE));
      RETURN 0;

END MERGE_LINK_LVL;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_WORKSHEET_ITEM_DATA(O_error_msg     IN OUT VARCHAR2,
                                    I_workspace_id  IN     NUMBER)
RETURN NUMBER IS
BEGIN

   merge into rpm_worksheet_item_data rwid
   using (select rww.worksheet_item_data_id,
                 rww.state,
                 rww.markup_calc_type,
                 rww.class,
                 rww.class_name,
                 rww.subclass,
                 rww.subclass_name,
                 rww.item_level,
                 rww.tran_level,
                 rww.standard_uom,
                 rww.retail_include_vat_ind,
                 rww.primary_supplier,
                 rww.vpn,
                 rww.uda_boolean,
                 rww.package_size,
                 rww.package_uom,
                 rww.retail_label_type,
                 rww.retail_label_value,
                 rww.season_phase_boolean,
                 rww.item_parent,
                 rww.parent_desc,
                 rww.primary_comp_retail,
                 rww.primary_comp_store,
                 rww.primary_comp_retail_uom,
                 rww.primary_multi_units,
                 rww.primary_multi_unit_retail,
                 rww.primary_multi_unit_retail_uom,
                 rww.a_comp_retail,
                 rww.b_comp_retail,
                 rww.c_comp_retail,
                 rww.d_comp_retail,
                 rww.e_comp_retail,
                 rww.a_comp_store,
                 rww.b_comp_store,
                 rww.c_comp_store,
                 rww.d_comp_store,
                 rww.e_comp_store,
                 rww.original_retail,
                 rww.current_zl_regular_retail,
                 rww.current_zl_regular_retail_uom,
                 rww.current_zl_regular_retail_std,
                 rww.current_zl_clear_retail,
                 rww.current_zl_clear_retail_uom,
                 rww.current_zl_clear_retail_std,
                 rww.current_zl_multi_units,
                 rww.current_zl_multi_unit_retail,
                 rww.current_zl_multi_uom,
                 rww.current_zl_cost,
                 rww.basis_zl_base_cost,
                 rww.basis_zl_pricing_cost,
                 rww.ref_wh_stock,
                 rww.ref_wh_on_order,
                 rww.ref_wh_inventory,
                 rww.basis_zl_regular_retail,
                 rww.basis_zl_regular_retail_uom,
                 rww.basis_zl_regular_retail_std,
                 rww.basis_zl_clear_retail,
                 rww.basis_zl_clear_retail_uom,
                 rww.basis_zl_clear_retail_std,
                 rww.basis_zl_clear_mkdn_nbr,
                 rww.basis_zl_multi_units,
                 rww.basis_zl_multi_unit_retail,
                 rww.basis_zl_multi_uom,
                 rww.proposed_zl_reset_date,
                 rww.proposed_zl_out_of_stock_date,
                 rww.reset_state,
                 rww.constraint_boolean,
                 rww.ignore_constraint_boolean,
                 rww.reviewed,
                 rww.action_flag
            from rpm_worksheet_zone_workspace rww
           where rww.update_ind = 1
             and rww.workspace_id = I_workspace_id) use
   on (rwid.worksheet_item_data_id = use.worksheet_item_data_id)
   when matched then
   update
      set rwid.state =  use.state,
          rwid.markup_calc_type =  use.markup_calc_type,
          rwid.class =  use.class,
          rwid.class_name =  use.class_name,
          rwid.subclass =  use.subclass,
          rwid.subclass_name =  use.subclass_name,
          rwid.item_level =  use.item_level,
          rwid.tran_level =  use.tran_level,
          rwid.standard_uom =  use.standard_uom,
          rwid.retail_include_vat_ind =  use.retail_include_vat_ind,
          rwid.primary_supplier =  use.primary_supplier,
          rwid.vpn =  use.vpn,
          rwid.uda_boolean =  use.uda_boolean,
          rwid.package_size =  use.package_size,
          rwid.package_uom =  use.package_uom,
          rwid.retail_label_type =  use.retail_label_type,
          rwid.retail_label_value =  use.retail_label_value,
          rwid.season_phase_boolean =  use.season_phase_boolean,
          rwid.item_parent =  use.item_parent,
          rwid.parent_desc =  use.parent_desc,
          rwid.primary_comp_retail =  use.primary_comp_retail,
          rwid.primary_comp_store =  use.primary_comp_store,
          rwid.primary_comp_retail_uom =  use.primary_comp_retail_uom,
          rwid.primary_multi_units =  use.primary_multi_units,
          rwid.primary_multi_unit_retail =  use.primary_multi_unit_retail,
          rwid.primary_multi_unit_retail_uom =  use.primary_multi_unit_retail_uom,
          rwid.a_comp_retail =  use.a_comp_retail,
          rwid.b_comp_retail =  use.b_comp_retail,
          rwid.c_comp_retail =  use.c_comp_retail,
          rwid.d_comp_retail =  use.d_comp_retail,
          rwid.e_comp_retail =  use.e_comp_retail,
          rwid.a_comp_store =  use.a_comp_store,
          rwid.b_comp_store =  use.b_comp_store,
          rwid.c_comp_store =  use.c_comp_store,
          rwid.d_comp_store =  use.d_comp_store,
          rwid.e_comp_store =  use.e_comp_store,
          rwid.original_retail =  use.original_retail,
          rwid.current_zl_regular_retail =  use.current_zl_regular_retail,
          rwid.current_zl_regular_retail_uom =  use.current_zl_regular_retail_uom,
          rwid.current_zl_regular_retail_std =  use.current_zl_regular_retail_std,
          rwid.current_zl_clear_retail =  use.current_zl_clear_retail,
          rwid.current_zl_clear_retail_uom =  use.current_zl_clear_retail_uom,
          rwid.current_zl_clear_retail_std =  use.current_zl_clear_retail_std,
          rwid.current_zl_multi_units =  use.current_zl_multi_units,
          rwid.current_zl_multi_unit_retail =  use.current_zl_multi_unit_retail,
          rwid.current_zl_multi_uom =  use.current_zl_multi_uom,
          rwid.current_zl_cost =  use.current_zl_cost,
          rwid.basis_zl_base_cost =  use.basis_zl_base_cost,
          rwid.basis_zl_pricing_cost =  use.basis_zl_pricing_cost,
          rwid.ref_wh_stock =  use.ref_wh_stock,
          rwid.ref_wh_on_order =  use.ref_wh_on_order,
          rwid.ref_wh_inventory =  use.ref_wh_inventory,
          rwid.basis_zl_regular_retail =  use.basis_zl_regular_retail,
          rwid.basis_zl_regular_retail_uom =  use.basis_zl_regular_retail_uom,
          rwid.basis_zl_regular_retail_std =  use.basis_zl_regular_retail_std,
          rwid.basis_zl_clear_retail =  use.basis_zl_clear_retail,
          rwid.basis_zl_clear_retail_uom =  use.basis_zl_clear_retail_uom,
          rwid.basis_zl_clear_retail_std =  use.basis_zl_clear_retail_std,
          rwid.basis_zl_clear_mkdn_nbr =  use.basis_zl_clear_mkdn_nbr,
          rwid.basis_zl_multi_units =  use.basis_zl_multi_units,
          rwid.basis_zl_multi_unit_retail =  use.basis_zl_multi_unit_retail,
          rwid.basis_zl_multi_uom =  use.basis_zl_multi_uom,
          rwid.proposed_zl_reset_date =  use.proposed_zl_reset_date,
          rwid.proposed_zl_out_of_stock_date =  use.proposed_zl_out_of_stock_date,
          rwid.reset_state =  use.reset_state,
          rwid.constraint_boolean =  use.constraint_boolean,
          rwid.ignore_constraint_boolean =  use.ignore_constraint_boolean,
          rwid.reviewed =  use.reviewed,
          rwid.action_flag = use.action_flag;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_WORKSHEET_ITEM_DATA',
                                        to_char(SQLCODE));
      RETURN 0;

END UPDATE_WORKSHEET_ITEM_DATA;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_WORKSHEET_ITEM_LOC_DATA(O_error_msg     IN OUT VARCHAR2,
                                        I_workspace_id  IN     NUMBER)
RETURN NUMBER IS

   cursor C_DEPT is
   select distinct dept
     from rpm_worksheet_zone_workspace
    where workspace_id = I_workspace_id;

   L_dept            rpm_worksheet_zone_workspace.dept%TYPE;

BEGIN

   for rec in C_DEPT loop

      L_dept := rec.dept;

      merge into rpm_worksheet_item_loc_data rwid
      using (select rwil.dept,
                    rwil.worksheet_item_loc_data_id,
                    rwil.proposed_strategy_id,
                    rwil.user_strategy_id,
                    rwil.current_regular_retail,
                    rwil.current_regular_retail_uom,
                    rwil.current_regular_retail_std,
                    rwil.current_clear_retail,
                    rwil.current_clear_retail_uom,
                    rwil.current_clear_retail_std,
                    rwil.current_multi_units,
                    rwil.current_multi_unit_retail,
                    rwil.current_multi_unit_uom,
                    rwil.pend_cost_boolean,
                    rwil.cost_alert,
                    rwil.current_cost,
                    rwil.basis_base_cost,
                    rwil.basis_pricing_cost,
                    rwil.basis_clear_mkdn_nbr,
                    rwil.current_clearance_ind,
                    rwil.historical_sales,
                    rwil.historical_sales_units,
                    rwil.historical_issues,
                    rwil.projected_sales,
                    rwil.seasonal_sales,
                    rwil.first_received_date,
                    rwil.last_received_date,
                    rwil.wks_first_sale,
                    rwil.wks_of_sales_exp,
                    rwil.location_stock,
                    rwil.location_on_order,
                    rwil.location_inventory,
                    rwil.link_code,
                    rwil.link_code_desc,
                    rwil.replenish_ind,
                    rwil.vat_rate,
                    rwil.vat_value,
                    rwil.clear_boolean,
                    rwil.promo_boolean,
                    rwil.price_change_boolean,
                    rwil.basis_regular_retail,
                    rwil.basis_regular_retail_uom,
                    rwil.basis_regular_retail_std,
                    rwil.basis_clear_retail,
                    rwil.basis_clear_retail_uom,
                    rwil.basis_clear_retail_std,
                    rwil.basis_multi_units,
                    rwil.basis_multi_unit_retail,
                    rwil.basis_multi_unit_uom,
                    rwil.proposed_retail,
                    rwil.proposed_retail_uom,
                    rwil.proposed_clear_mkdn_nbr,
                    rwil.proposed_clear_ind,
                    rwil.proposed_reset_date,
                    rwil.proposed_out_of_stock_date,
                    rwil.action_flag,
                    rwil.effective_date,
                    rwil.new_retail,
                    rwil.new_retail_std,
                    rwil.new_retail_uom,
                    rwil.new_multi_unit_ind,
                    rwil.new_multi_units,
                    rwil.new_multi_unit_retail,
                    rwil.new_multi_unit_uom,
                    rwil.new_clear_mkdn_nbr,
                    rwil.new_clear_ind,
                    rwil.new_reset_date,
                    rwil.new_out_of_stock_date,
                    rwil.new_item_loc_boolean,
                    rwil.original_effective_date,
                    rwil.rule_boolean,
                    rwil.conflict_boolean
               from rpm_worksheet_zone_workspace rww,
                    rpm_worksheet_il_workspace rwil
              where rww.update_ind = 1
                and rww.workspace_id = I_workspace_id
                and rww.dept = L_dept
                and rwil.workspace_id = rww.workspace_id
                and rwil.worksheet_item_data_id = rww.worksheet_item_data_id) use
      on (    rwid.dept = L_dept
          and rwid.worksheet_item_loc_data_id = use.worksheet_item_loc_data_id)
      when matched then
      update
         set rwid.proposed_strategy_id =  use.proposed_strategy_id,
             rwid.user_strategy_id =  use.user_strategy_id,
             rwid.current_regular_retail =  use.current_regular_retail,
             rwid.current_regular_retail_uom =  use.current_regular_retail_uom,
             rwid.current_regular_retail_std =  use.current_regular_retail_std,
             rwid.current_clear_retail =  use.current_clear_retail,
             rwid.current_clear_retail_uom =  use.current_clear_retail_uom,
             rwid.current_clear_retail_std =  use.current_clear_retail_std,
             rwid.current_multi_units =  use.current_multi_units,
             rwid.current_multi_unit_retail =  use.current_multi_unit_retail,
             rwid.current_multi_unit_uom =  use.current_multi_unit_uom,
             rwid.pend_cost_boolean =  use.pend_cost_boolean,
             rwid.cost_alert =  use.cost_alert,
             rwid.current_cost =  use.current_cost,
             rwid.basis_base_cost =  use.basis_base_cost,
             rwid.basis_pricing_cost =  use.basis_pricing_cost,
             rwid.basis_clear_mkdn_nbr =  use.basis_clear_mkdn_nbr,
             rwid.current_clearance_ind =  use.current_clearance_ind,
             rwid.historical_sales =  use.historical_sales,
             rwid.historical_sales_units =  use.historical_sales_units,
             rwid.historical_issues =  use.historical_issues,
             rwid.projected_sales =  use.projected_sales,
             rwid.seasonal_sales =  use.seasonal_sales,
             rwid.first_received_date =  use.first_received_date,
             rwid.last_received_date =  use.last_received_date,
             rwid.wks_first_sale =  use.wks_first_sale,
             rwid.wks_of_sales_exp =  use.wks_of_sales_exp,
             rwid.location_stock =  use.location_stock,
             rwid.location_on_order =  use.location_on_order,
             rwid.location_inventory =  use.location_inventory,
             rwid.link_code =  use.link_code,
             rwid.link_code_desc =  use.link_code_desc,
             rwid.replenish_ind =  use.replenish_ind,
             rwid.vat_rate =  use.vat_rate,
             rwid.vat_value =  use.vat_value,
             rwid.clear_boolean =  use.clear_boolean,
             rwid.promo_boolean =  use.promo_boolean,
             rwid.price_change_boolean =  use.price_change_boolean,
             rwid.basis_regular_retail =  use.basis_regular_retail,
             rwid.basis_regular_retail_uom =  use.basis_regular_retail_uom,
             rwid.basis_regular_retail_std =  use.basis_regular_retail_std,
             rwid.basis_clear_retail =  use.basis_clear_retail,
             rwid.basis_clear_retail_uom =  use.basis_clear_retail_uom,
             rwid.basis_clear_retail_std =  use.basis_clear_retail_std,
             rwid.basis_multi_units =  use.basis_multi_units,
             rwid.basis_multi_unit_retail =  use.basis_multi_unit_retail,
             rwid.basis_multi_unit_uom =  use.basis_multi_unit_uom,
             rwid.proposed_retail =  use.proposed_retail,
             rwid.proposed_retail_uom =  use.proposed_retail_uom,
             rwid.proposed_clear_mkdn_nbr =  use.proposed_clear_mkdn_nbr,
             rwid.proposed_clear_ind =  use.proposed_clear_ind,
             rwid.proposed_reset_date =  use.proposed_reset_date,
             rwid.proposed_out_of_stock_date =  use.proposed_out_of_stock_date,
             rwid.action_flag =  use.action_flag,
             rwid.effective_date =  use.effective_date,
             rwid.new_retail =  use.new_retail,
             rwid.new_retail_std =  use.new_retail_std,
             rwid.new_retail_uom =  use.new_retail_uom,
             rwid.new_multi_unit_ind =  use.new_multi_unit_ind,
             rwid.new_multi_units =  use.new_multi_units,
             rwid.new_multi_unit_retail =  use.new_multi_unit_retail,
             rwid.new_multi_unit_uom =  use.new_multi_unit_uom,
             rwid.new_clear_mkdn_nbr =  use.new_clear_mkdn_nbr,
             rwid.new_clear_ind =  use.new_clear_ind,
             rwid.new_reset_date =  use.new_reset_date,
             rwid.new_out_of_stock_date =  use.new_out_of_stock_date,
             rwid.new_item_loc_boolean =  use.new_item_loc_boolean,
             rwid.original_effective_date =  use.original_effective_date,
             rwid.rule_boolean =  use.rule_boolean,
             rwid.conflict_boolean =  use.conflict_boolean;

   end loop;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_WORKSHEET_ITEM_LOC_DATA',
                                        to_char(SQLCODE));
      RETURN 0;

END UPDATE_WORKSHEET_ITEM_LOC_DATA;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_WORKSHEET_ZONE_DATA(O_error_msg     IN OUT VARCHAR2,
                                    I_workspace_id  IN     NUMBER)
RETURN NUMBER IS

BEGIN

   delete rpm_worksheet_zone_data
    where (worksheet_status_id, worksheet_item_data_id) in
           (select worksheet_status_id,
                   worksheet_item_data_id
              from rpm_worksheet_zone_workspace
             where update_ind = 1
               and workspace_id = I_workspace_id);

   insert into rpm_worksheet_zone_data
   (
     worksheet_item_data_id    ,
     worksheet_status_id       ,
     proposed_strategy_id      ,
     user_strategy_id          ,
     state                     ,
     reset_state               ,
     item                      ,
     item_desc                 ,
     dept                      ,
     dept_name                 ,
     markup_calc_type          ,
     class                     ,
     class_name                ,
     subclass                  ,
     subclass_name             ,
     item_level                ,
     tran_level                ,
     standard_uom              ,
     diff_1                    ,
     diff_type_1               ,
     diff_2                    ,
     diff_type_2               ,
     diff_3                    ,
     diff_type_3               ,
     diff_4                    ,
     diff_type_4               ,
     retail_include_vat_ind    ,
     primary_supplier          ,
     vpn                       ,
     uda_boolean               ,
     package_size              ,
     package_uom               ,
     retail_label_type         ,
     retail_label_value        ,
     season_phase_boolean      ,
     item_parent               ,
     parent_desc               ,
     parent_diff_1             ,
     parent_diff_2             ,
     parent_diff_3             ,
     parent_diff_4             ,
     parent_diff_1_type        ,
     parent_diff_2_type        ,
     parent_diff_3_type        ,
     parent_diff_4_type        ,
     parent_diff_1_type_desc        ,
     parent_diff_2_type_desc        ,
     parent_diff_3_type_desc        ,
     parent_diff_4_type_desc        ,
     parent_vpn                     ,
     primary_comp_retail            ,
     primary_comp_store             ,
     primary_comp_retail_uom       ,
     primary_comp_retail_std       ,
     primary_multi_units           ,
     primary_multi_unit_retail       ,
     primary_multi_unit_retail_uom   ,
     a_comp_store                   ,
     b_comp_store                   ,
     c_comp_store                   ,
     d_comp_store                   ,
     e_comp_store                   ,
     a_comp_retail                  ,
     b_comp_retail                  ,
     c_comp_retail                  ,
     d_comp_retail                  ,
     e_comp_retail                  ,
     zone_group_id                  ,
     zone_group_display_id    ,
     zone_group_name                 ,
     zone_id                         ,
     zone_display_id                 ,
     zone_name                       ,
     loc_type,
     currency                        ,
     original_retail                 ,
     current_regular_retail_flag        ,
     current_regular_retail_uom ,
     current_reg_retail_uom_flag      ,
     current_clear_retail_flag        ,
     current_clear_retail_uom   ,
     current_clear_retail_uom_flag        ,
     current_cost_markup_flag        ,
     current_multi_unit_flag ,
     current_multi_unit_retail_flag ,
     current_multi_unit_uom,
     current_multi_unit_uom_flag        ,
     current_zl_regular_retail       ,
     current_zl_regular_retail_uom   ,
     current_zl_regular_retail_std   ,
     current_zl_clear_retail         ,
     current_zl_clear_retail_uom     ,
     current_zl_clear_retail_std     ,
     current_zl_retail_std     ,
     current_zl_multi_units          ,
     current_zl_multi_unit_retail    ,
     current_zl_multi_uom            ,
     past_price_chg_date             ,
     past_price_change_date_flag ,
     past_cost_chg_date              ,
     past_cost_chg_date_flag     ,
     pend_cost_chg_date              ,
     pend_zl_cost_chg_cost           ,
     maint_margin_zl_retail          ,
     maint_margin_zl_retail_uom      ,
     maint_margin_zl_retail_std      ,
     maint_margin_zl_cost            ,
     current_cost_flag               ,
     basis_base_cost_flag            ,
     basis_pricing_cost_flag         ,
     current_zl_cost                 ,
     current_zl_cost_zone            ,
     basis_zl_base_cost              ,
     basis_zl_pricing_cost           ,
     current_clearance_ind           ,
     historical_sales                ,
     store_historical_sales_units          ,
     wh_historical_sales_units          ,
     projected_sales                 ,
     store_seasonal_sales,
     wh_seasonal_sales,
     first_received_date             ,
     first_received_date_flag      ,
     last_received_date          ,
     last_received_date_flag         ,
     wks_of_sales_exp              ,
     wks_of_sales_exp_flag           ,
     store_stock                 ,
     wh_stock                       ,
     store_on_order              ,
     wh_on_order                 ,
     store_inventory          ,
     wh_inventory             ,
     ref_wh_stock                ,
     ref_wh_stock_value,
     ref_wh_stock_count,
     ref_wh_on_order              ,
     ref_wh_on_order_value,
     ref_wh_on_order_count,
     ref_wh_inventory             ,
     margin_mkt_basket_code       ,
     comp_mkt_basket_code         ,
     link_code                    ,
     link_code_display_id         ,
     link_code_desc               ,
     replenish_ind               ,
     replenish_ind_flag       ,
     vat_rate                     ,
     vat_value,
     clear_boolean            ,
     clear_flag                     ,
     wks_first_sale              ,
     wks_first_sale_flag         ,
     promo_boolean                ,
     price_change_boolean      ,
     price_change_boolean_flag      ,
     basis_regular_retail_flag          ,
     basis_regular_retail_uom,
     basis_regular_retail_uom_flag      ,
     basis_clear_retail_flag            ,
     basis_clear_retail_uom,
     basis_clear_retail_uom_flag        ,
     basis_multi_units_flag             ,
     basis_multi_unit_retail_flag       ,
     basis_multi_unit_uom               ,
     basis_multi_unit_uom_flag          ,
     basis_zl_regular_retail       ,
     basis_zl_regular_retail_uom     ,
     basis_zl_regular_retail_std     ,
     basis_zl_clear_retail           ,
     basis_zl_clear_retail_uom       ,
     basis_zl_clear_retail_std       ,
     basis_zl_clear_mkdn_nbr         ,
     basis_zl_multi_units            ,
     basis_zl_multi_unit_retail      ,
     basis_zl_multi_uom              ,
     proposed_retail                 ,
     proposed_retail_flag        ,
     proposed_retail_uom         ,
     proposed_retail_uom_flag          ,
     proposed_retail_std             ,
     proposed_clear_mkdn_nbr     ,
     proposed_clear_mkdn_nbr_flag       ,
     proposed_zl_reset_date          ,
     proposed_zl_out_of_stock_date   ,
     action_flag                 ,
     action_flag_varies                ,
     effective_date                  ,
     last_effective_date         ,
     effective_date_flag         ,
     effective_date_is_set       ,
     new_retail                  ,
     new_retail_flag       ,
     new_retail_is_set              ,
     either_retail_is_set        ,
     new_retail_uom                  ,
     new_retail_uom_flag         ,
     new_multi_units             ,
     new_multi_units_flag              ,
     new_multi_unit_retail       ,
     new_multi_unit_retail_flag    ,
     new_multi_unit_retail_is_set       ,
     new_multi_unit_uom          ,
     new_multi_unit_uom_flag           ,
     new_retail_std              ,
     sum_nmp_retail                ,
     avg_nmp_retail                    ,
     sum_nmp_cost  ,
     new_clear_mkdn_nbr          ,
     new_clear_mkdn_nbr_flag               ,
     new_clear_ind               ,
     new_clear_ind_flag              ,
     new_reset_date              ,
     new_reset_date_flag               ,
     new_out_of_stock_date       ,
     new_out_of_stock_date_flag        ,
     new_item_loc_boolean        ,
     new_item_loc_boolean_flag         ,
     primary_comp_boolean            ,
     rule_boolean                    ,
     conflict_boolean                ,
     conflict_boolean_flag       ,
     constraint_boolean,
     constraint_boolean_flag     ,
     ignore_constraint_boolean       ,
     area_diff_prim_id               ,
     area_diff_id                    ,
     margin_mbc_name                 ,
     competitive_mbc_name          ,
     reviewed,
     v_sales_ch_amt_ncurrent,
     v_sales_ch_proj_sales,
     v_sales_ch_prj_reg_retail,
     sales_change_amount_ncurrent,
     sales_change_prj_reg_retail,
     detail_count
   )
   select rwz.worksheet_item_data_id    ,
     rwz.worksheet_status_id   ,
     max(proposed_strategy_id)  ,
     max(user_strategy_id)      ,
     max(state)                 ,
     max(reset_state)           ,
     rwz.item                  ,
     max(item_desc)             ,
     rwz.dept                  ,
     max(dept_name)             ,
     max(markup_calc_type)      ,
     max(rwil.class)                 ,
     max(class_name)            ,
     max(rwil.subclass)              ,
     max(subclass_name)         ,
     max(item_level)            ,
     max(tran_level)            ,
     max(standard_uom)          ,
     max(diff_1)                ,
     max(diff_type_1)           ,
     max(diff_2)                ,
     max(diff_type_2)           ,
     max(diff_3)                ,
     max(diff_type_3)           ,
     max(diff_4)                ,
     max(diff_type_4)           ,
     max(retail_include_vat_ind),
     max(primary_supplier)      ,
     max(vpn)                   ,
     max(uda_boolean)           ,
     max(package_size)          ,
     max(package_uom)           ,
     max(retail_label_type)     ,
     max(retail_label_value)    ,
     max(season_phase_boolean)  ,
     max(rwz.item_parent)           ,
     max(parent_desc)           ,
     max(parent_diff_1)         ,
     max(parent_diff_2)         ,
     max(parent_diff_3)         ,
     max(parent_diff_4)         ,
     max(parent_diff_1_type)    ,
     max(parent_diff_2_type)    ,
     max(parent_diff_3_type)    ,
     max(parent_diff_4_type)    ,
     max(parent_diff_1_type_desc),
     max(parent_diff_2_type_desc),
     max(parent_diff_3_type_desc),
     max(parent_diff_4_type_desc),
     max(parent_vpn)             ,
     max(primary_comp_retail)    ,
     max(primary_comp_store)     ,
     max(primary_comp_retail_uom),
     max(primary_comp_retail_std),
     max(primary_multi_units)    ,
     max(primary_multi_unit_retail),
     max(primary_multi_unit_retail_uom),
     max(a_comp_store),
     max(b_comp_store),
     max(c_comp_store),
     max(d_comp_store),
     max(e_comp_store),
     max(a_comp_retail),
     max(b_comp_retail),
     max(c_comp_retail),
     max(d_comp_retail),
     max(e_comp_retail),
     max(zone_group_id),
     max(zone_group_display_id)   ,
     max(zone_group_name)         ,
     rwz.zone_id                 ,
     max(zone_display_id)         ,
     max(zone_name)               ,
     max(loc_type)                ,
     max(currency)                ,
     max(original_retail)         ,
     case max(nvl(current_regular_retail, 0))
        when min(nvl(current_regular_retail, 0))
        then 0
        else 1
     end as current_regular_retail_flag,
     max(nvl(current_regular_retail_uom, 0)) current_regular_retail_uom,
     case max(nvl(current_regular_retail_uom, 0))
        when min(nvl(current_regular_retail_uom, 0))
        then 0
        else 1
     end as current_reg_retail_uom_flag,
     case max(nvl(current_clear_retail,0))
        when min(nvl(current_clear_retail,0))
        then 0
        else 1
     end as current_clear_retail_flag,
     max(nvl(current_clear_retail_uom, 0)) current_clear_retail_uom,
     case max(nvl(current_clear_retail_uom, 0))
        when min(nvl(current_clear_retail_uom, 0))
        then 0
        else 1
     end as current_clear_retail_uom_flag,
     case max((case
               when current_clearance_ind = 1
                  then nvl(current_clear_retail,0)
                  else nvl(current_regular_retail, 0)
               end))
     when min((case
               when current_clearance_ind = 1
                  then nvl(current_clear_retail,0)
                  else nvl(current_regular_retail,0)
               end))
        then 0
        else 1
     end as current_cost_markup_flag,
     case max(nvl(current_multi_units,0))
     when min(nvl(current_multi_units,0))
        then 0
        else 1
     end as current_multi_unit_flag,
     case max(nvl(current_multi_unit_retail,0))
     when min(nvl(current_multi_unit_retail,0))
        then 0
        else 1
     end as current_multi_unit_retail_flag,
     max(nvl(current_multi_unit_uom,0)) current_multi_unit_uom,
     case max(nvl(current_multi_unit_uom,0))
        when min(nvl(current_multi_unit_uom,0))
        then 0
        else 1
     end as current_multi_unit_uom_flag,
     max(current_zl_regular_retail),
     max(current_zl_regular_retail_uom),
     max(current_zl_regular_retail_std),
     max(current_zl_clear_retail)  ,
     max(current_zl_clear_retail_uom),
     max(current_zl_clear_retail_std),
     sum(case
         when current_clearance_ind = 1 then
            current_zl_clear_retail_std
         else
         current_zl_regular_retail_std end) current_zl_retail_std,
     max(current_zl_multi_units)     ,
     max(current_zl_multi_unit_retail),
     max(current_zl_multi_uom)       ,
     max(past_price_chg_date)        ,
     case max(nvl(past_price_chg_date, sysdate))
     when max(nvl(past_price_chg_date, sysdate))
        then 0
        else 1
     end as past_price_change_date_flag,
     max(past_cost_chg_date)    ,
     case max(nvl(past_cost_chg_date, sysdate))
     when max(nvl(past_cost_chg_date, sysdate))
        then 0
        else 1
     end as past_cost_chg_date_flag,
     min(pend_cost_chg_date),
     max(pend_zl_cost_chg_cost) ,
     max(maint_margin_zl_retail)     ,
     max(maint_margin_zl_retail_uom) ,
     max(maint_margin_zl_retail_std) ,
     max(maint_margin_zl_cost)       ,
     case max(nvl(current_cost, 0))
     when min (nvl(current_cost, 0))
        then 0
        else 1
     end as current_cost_flag        ,
     case max(nvl(basis_base_cost, 0))
     when min (nvl(basis_base_cost, 0))
        then 0
        else 1
     end as basis_base_cost_flag  ,
      case max(nvl(basis_pricing_cost, 0))
     when min (nvl(basis_pricing_cost, 0))
        then 0
        else 1
     end as basis_pricing_cost_flag         ,
     max(current_zl_cost) current_zl_cost,
     sum(current_zl_cost) current_zl_cost_zone,
     max(basis_zl_base_cost) basis_zl_base_cost,
     max(basis_zl_pricing_cost) basis_zl_pricing_cost,
     max(current_clearance_ind) current_clearance_ind,
     sum(historical_sales) historical_sales,
     sum(case
         when loc_type = 0
            then historical_sales_units
            else 0
         end) store_historical_sales_units,
     sum(case
         when loc_type = 2
            then historical_sales_units
            else 0
         end) wh_historical_sales_units,
     sum(rwil.projected_sales)            ,
     sum(case
         when loc_type = 0
            then seasonal_sales
            else 0
         end) store_seasonal_sales,
     sum(case
         when loc_type = 2
            then seasonal_sales
            else 0
         end) wh_seasonal_sales,
     min(first_received_date)       ,
     case max(nvl(first_received_date, sysdate))
     when min(nvl(first_received_date, sysdate))
        then 0
        else 1
     end as first_received_date_flag,
     max(last_received_date)         ,
     case max(nvl(last_received_date, sysdate))
     when min(nvl(last_received_date, sysdate))
        then 0
        else 1
     end as last_received_date_flag,
     max(wks_of_sales_exp)         ,
     case max(nvl(wks_of_sales_exp, 0))
     when min(nvl(wks_of_sales_exp, 0))
        then 0
        else 1
     end as wks_of_sales_exp_flag,
     sum(case
         when loc_type = 0
            then location_stock
            else 0
         end) store_stock,
     sum(case
         when loc_type = 2
            then location_stock
            else 0
         end) wh_stock,
     sum(case
         when loc_type = 0
            then location_on_order
            else 0
         end) store_on_order,
     sum(case
         when loc_type = 2
            then location_on_order
            else 0
         end) wh_on_order,
     sum(case
         when loc_type = 0
            then location_inventory
            else 0
         end) store_inventory,
     sum(case
         when loc_type = 2
            then location_inventory
            else 0
         end) wh_inventory,
     sum(ref_wh_stock) ref_wh_stock,
     max(ref_wh_stock) ref_wh_stock_value,
     count(ref_wh_stock) ref_wh_stock_count,
     sum(ref_wh_on_order) ref_wh_on_order,
     max(ref_wh_on_order) ref_wh_on_order_value,
     count(ref_wh_on_order) ref_wh_on_order_count,
     max(ref_wh_inventory)           ,
     max(margin_mkt_basket_code)     ,
     max(comp_mkt_basket_code)       ,
     link_code                  ,
     max(link_code_display_id)       ,
     max(link_code_desc)             ,
     max(replenish_ind)              ,
     case max(nvl(replenish_ind, 0))
        when min(nvl(replenish_ind, 0)) then 0
        else 1
     end as replenish_ind_flag  ,
     avg(vat_rate)              ,
     avg(vat_value)              ,
     max(nvl(clear_boolean,0)) clear_boolean              ,
     case max(greatest(nvl(clear_boolean,0), nvl(current_clearance_ind,0)))
     when min(greatest(nvl(clear_boolean,0), nvl(current_clearance_ind,0)))
        then 0
        else 1
     end as clear_flag,
     max(wks_first_sale)        ,
     case max(nvl(wks_first_sale, 0))
     when min(nvl(wks_first_sale, 0))
        then 0
        else 1
     end as wks_first_sale_flag,
     max(nvl(promo_boolean,0)) promo_boolean              ,
     max(nvl(price_change_boolean,0)) price_change_boolean,
     case max(nvl(price_change_boolean, 0))
     when min(nvl(price_change_boolean, 0))
        then 0
        else 1
     end as price_change_boolean_flag,
     case max(nvl(basis_regular_retail, 0))
        when min(nvl(basis_regular_retail, 0))
        then 0
        else 1
     end as basis_regular_retail_flag,
     max(nvl(basis_regular_retail_uom, 0)) basis_regular_retail_uom,
     case max(nvl(basis_regular_retail_uom, 0))
        when min(nvl(basis_regular_retail_uom, 0))
        then 0
        else 1
     end as basis_regular_retail_uom_flag,
     case max(nvl(basis_clear_retail, 0))
        when min(nvl(basis_clear_retail, 0))
        then 0
        else 1
     end as basis_clear_retail_flag         ,
     max(nvl(basis_clear_retail_uom, 0)) basis_clear_retail_uom,
     case max(nvl(basis_clear_retail_uom, 0))
        when min(nvl(basis_clear_retail_uom, 0))
        then 0
        else 1
     end as basis_clear_retail_uom_flag     ,
     case max(nvl(basis_multi_units,0))
     when min(nvl(basis_multi_units,0))
        then 0
        else 1
     end as basis_multi_units_flag,
     case max(nvl(basis_multi_unit_retail,0))
     when min(nvl(basis_multi_unit_retail,0))
        then 0
        else 1
     end as basis_multi_unit_retail_flag,
     max(nvl(basis_multi_unit_uom,0)) basis_multi_unit_uom,
     case max(nvl(basis_multi_unit_uom,0))
        when min(nvl(basis_multi_unit_uom,0))
        then 0
        else 1
     end as basis_multi_unit_uom_flag,
     max(basis_zl_regular_retail)    ,
     max(basis_zl_regular_retail_uom),
     max(basis_zl_regular_retail_std),
     max(basis_zl_clear_retail)      ,
     max(basis_zl_clear_retail_uom)  ,
     max(basis_zl_clear_retail_std)  ,
     max(basis_zl_clear_mkdn_nbr)    ,
     max(basis_zl_multi_units)       ,
     max(basis_zl_multi_unit_retail) ,
     max(basis_zl_multi_uom)         ,
     avg(proposed_retail)            ,
     case max(nvl(proposed_retail,0))
        when min(nvl(proposed_retail,0))
        then 0
        else 1
     end as proposed_retail_flag,
     min(proposed_retail_uom)        ,
     case max(nvl(proposed_retail_uom,0))
        when min(nvl(proposed_retail_uom,0))
        then 0
        else 1
     end as proposed_retail_uom_flag,
     sum(proposed_retail_std)        ,
     max(proposed_clear_mkdn_nbr)    ,
     case max(nvl(proposed_clear_mkdn_nbr,0))
        when min(nvl(proposed_clear_mkdn_nbr,0))
        then 0
        else 1
     end as proposed_clear_mkdn_nbr_flag,
     min(proposed_zl_reset_date)     ,
     min(proposed_zl_out_of_stock_date),
     max(rwil.action_flag)                ,
     case max(nvl(rwil.action_flag,0))
        when min(nvl(rwil.action_flag,0))
        then 0
        else 1
     end as action_flag_varies  ,
     min(effective_date)        ,
     max(effective_date)  last_effective_date        ,
     case max(nvl(effective_date,sysdate))
        when min(nvl(effective_date,sysdate))
        then 0
        else 1
     end as effective_date_flag        ,
     min(case
         when effective_date is null
            then 0
            else 1
         end) as effective_date_is_set      ,
     avg(rwil.new_retail)            ,
     case max(nvl(rwil.new_retail, 0))
     when min(nvl(rwil.new_retail, 0))
        then 0
        else 1
     end as new_retail_flag,
     min (case
          when rwil.new_retail is null
             then 0
             else 1
          end) as new_retail_is_set,
     min (case
          when rwil.new_retail is null and new_multi_unit_retail is null
             then 0
             else 1
          end) as either_retail_is_set,
     min(rwil.new_retail_uom)        ,
     case max(nvl(rwil.new_retail_uom,0))
        when min(nvl(rwil.new_retail_uom,0))
        then 0
        else 1
     end as new_retail_uom_flag,
     max(new_multi_units)            ,
     case max(nvl(new_multi_units,0))
        when min(nvl(new_multi_units,0))
        then 0
        else 1
     end as new_multi_units_flag ,
     max(new_multi_unit_retail) ,
     case max(nvl(new_multi_unit_retail, 0))
     when min(nvl(new_multi_unit_retail, 0))
        then 0
        else 1
     end as new_multi_unit_retail_flag,
     min(case
         when new_multi_unit_retail is null
            then 0
            else 1
         end) as new_multi_unit_retail_is_set,
     min(new_multi_unit_uom)    ,
     case max(nvl(new_multi_unit_uom, 0))
     when min(nvl(new_multi_unit_uom, 0))
        then 0
        else 1
     end as new_multi_unit_uom_flag,
     avg(rwil.new_retail_std)        ,
     sum(case
         when rwil.new_retail_std is not null
            then rwil.new_retail_std
            else
               case
               when current_clearance_ind = 1
                  then
                     case
                     when basis_zl_clear_retail_uom = standard_uom
                        then basis_zl_clear_retail
                        else null
                     end
                  else
                     case
                     when basis_zl_regular_retail_uom = standard_uom
                        then basis_zl_regular_retail
                        else null
                     end
               end
         end) as sum_nmp_retail,
     avg(case
         when rwil.new_retail_std is not null
            then rwil.new_retail_std
            else
               case
               when current_clearance_ind = 1
                  then
                     case
                     when basis_zl_clear_retail_uom = standard_uom
                        then basis_zl_clear_retail
                        else null
                     end
                  else
                     case
                     when basis_zl_regular_retail_uom = standard_uom
                        then basis_zl_regular_retail
                        else null
                     end
               end
         end) as avg_nmp_retail,
     sum(basis_zl_pricing_cost) sum_nmp_cost,
     max(new_clear_mkdn_nbr)    ,
     case max(nvl(new_clear_mkdn_nbr, 0))
     when min(nvl(new_clear_mkdn_nbr, 0))
        then 0
        else 1
     end as new_clear_mkdn_nbr_flag,
     min(nvl(new_clear_ind,0))  ,
     case max(nvl(new_clear_ind, 0))
     when min(nvl(new_clear_ind, 0))
        then 0
        else 1
     end as new_clear_ind_flag,
     min(new_reset_date)        ,
     case max(nvl(new_reset_date, sysdate))
     when min(nvl(new_reset_date, sysdate))
        then 0
        else 1
     end as new_reset_date_flag,
     min(new_out_of_stock_date) ,
     case max(nvl(new_out_of_stock_date, sysdate))
     when min(nvl(new_out_of_stock_date, sysdate))
        then 0
        else 1
     end as new_out_of_stock_date_flag,
     max(new_item_loc_boolean)       ,
     case max(nvl(new_item_loc_boolean, 0))
     when min(nvl(new_item_loc_boolean, 0))
        then 0
        else 1
     end as new_item_loc_boolean_flag,
     max(primary_comp_boolean)       ,
     max(nvl(rule_boolean,0))        ,
     max(nvl(conflict_boolean,0))    ,
     case max(nvl(conflict_boolean, 0))
     when min(nvl(conflict_boolean, 0))
        then 0
        else 1
     end as conflict_boolean_flag,
     max(nvl(constraint_boolean,0))  ,
     case max(nvl(constraint_boolean, 0))
     when min(nvl(constraint_boolean, 0))
        then 0
        else 1
     end as constraint_boolean_flag,
     max(nvl(ignore_constraint_boolean,0)),
     max(rwz.area_diff_prim_id)          ,
     max(rwz.area_diff_id)               ,
     max(rwz.margin_mbc_name)            ,
     max(rwz.competitive_mbc_name)       ,
     max(rwil.reviewed),
          case max(rwil.action_flag)
          when 0
             then sum((rwil.projected_sales * rwil.new_retail)-(basis_regular_retail * rwil.projected_sales))
             else sum(case rwil.action_flag
                      when 1
                         then (rwil.projected_sales * rwil.new_retail)-(basis_regular_retail * rwil.projected_sales)
                         else 0
                      end)
          end as v_sales_ch_amt_ncurrent,
         case max(rwil.action_flag)
         when 0
           then sum(rwil.projected_sales)
           else sum(case rwil.action_flag
                    when 1
                       then (rwil.projected_sales)
                       else 0
                    end)
         end as v_sales_ch_proj_sales,
         case max(rwil.action_flag)
         when 0
            then sum(basis_regular_retail * rwil.projected_sales)
            else sum(case rwil.action_flag
                     when 1
                        then (basis_regular_retail * rwil.projected_sales)
                         else 0
                     end)
         end as v_sales_ch_prj_reg_retail,
         sum((rwil.projected_sales * rwil.new_retail)-(basis_regular_retail * rwil.projected_sales)) as sales_change_amount_ncurrent,
         sum(basis_regular_retail * rwil.projected_sales) as sales_change_prj_reg_retail,
         count(1) detail_count
     from rpm_worksheet_il_workspace rwil,
          (select worksheet_status_id,
                  worksheet_item_data_id,
                  zone_id,
                  dept,
                  item,
                  state                 ,
                  reset_state           ,
                  item_desc             ,
                  dept_name             ,
                  markup_calc_type      ,
                  class                 ,
                  class_name            ,
                  subclass              ,
                  subclass_name         ,
                  item_level            ,
                  tran_level            ,
                  standard_uom          ,
                  diff_1                ,
                  diff_type_1           ,
                  diff_2                ,
                  diff_type_2           ,
                  diff_3                ,
                  diff_type_3           ,
                  diff_4                ,
                  diff_type_4           ,
                  retail_include_vat_ind,
                  primary_supplier      ,
                  vpn                   ,
                  uda_boolean           ,
                  package_size          ,
                  package_uom           ,
                  retail_label_type     ,
                  retail_label_value    ,
                  season_phase_boolean  ,
                  item_parent           ,
                  parent_desc           ,
                  parent_diff_1         ,
                  parent_diff_2         ,
                  parent_diff_3         ,
                  parent_diff_4         ,
                  parent_diff_1_type    ,
                  parent_diff_2_type    ,
                  parent_diff_3_type    ,
                  parent_diff_4_type    ,
                  parent_diff_1_type_desc,
                  parent_diff_2_type_desc,
                  parent_diff_3_type_desc,
                  parent_diff_4_type_desc,
                  parent_vpn             ,
                  primary_comp_retail    ,
                  primary_comp_store     ,
                  primary_comp_retail_uom,
                  primary_comp_retail_std,
                  primary_multi_units    ,
                  primary_multi_unit_retail,
                  primary_multi_unit_retail_uom,
                  a_comp_store,
                  b_comp_store,
                  c_comp_store,
                  d_comp_store,
                  e_comp_store,
                  a_comp_retail,
                  b_comp_retail,
                  c_comp_retail,
                  d_comp_retail,
                  e_comp_retail,
                  zone_group_id,
                  zone_group_display_id   ,
                  zone_group_name         ,
                  zone_display_id         ,
                  zone_name               ,
                  reviewed,
                  area_diff_prim_id,
                  area_diff_id,
                  margin_mbc_name,
                  competitive_mbc_name,
                  primary_comp_boolean,
                  current_zl_regular_retail,
                  current_zl_regular_retail_uom,
                  current_zl_regular_retail_std,
                  current_zl_clear_retail,
                  current_zl_clear_retail_uom,
                  current_zl_clear_retail_std,
                  current_zl_retail_std,
                  current_zl_multi_units,
                  current_zl_multi_unit_retail,
                  current_zl_multi_uom,
                  current_zl_cost,
                  current_zl_cost_zone,
                  basis_zl_base_cost,
                  basis_zl_pricing_cost,
                  basis_zl_regular_retail,
                  basis_zl_regular_retail_uom,
                  basis_zl_regular_retail_std,
                  basis_zl_clear_retail    ,
                  basis_zl_clear_retail_uom ,
                  basis_zl_clear_retail_std,
                  basis_zl_clear_mkdn_nbr    ,
                  basis_zl_multi_units       ,
                  basis_zl_multi_unit_retail,
                  basis_zl_multi_uom,
                  proposed_zl_reset_date,
                  proposed_zl_out_of_stock_date,
                  margin_mkt_basket_code,
                  comp_mkt_basket_code,
                  ref_wh_stock,
                  ref_wh_on_order,
                  ref_wh_inventory,
                  maint_margin_zl_retail,
                  maint_margin_zl_retail_uom,
                  maint_margin_zl_retail_std,
                  maint_margin_zl_cost,
                  pend_zl_cost_chg_cost,
                  original_retail,
                  currency
             from rpm_worksheet_zone_workspace rwz
            where rwz.update_ind = 1
              and rwz.workspace_id = I_workspace_id) rwz
    where rwil.workspace_id = I_workspace_id
      and rwil.worksheet_status_id = rwz.worksheet_status_id
      and rwil.worksheet_item_data_id = rwz.worksheet_item_data_id
    group by rwz.dept,
             rwz.worksheet_status_id,
             rwz.worksheet_item_data_id,
             rwil.link_code,
             rwz.item,
             rwz.zone_id;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_WORKSHEET_ZONE_DATA',
                                        to_char(SQLCODE));
      RETURN 0;

END UPDATE_WORKSHEET_ZONE_DATA;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_ROLLUP_MODS (O_error_msg            IN OUT VARCHAR2,
                             I_obj_rollup_mods_tbl  IN     OBJ_ROLLUP_MODS_TBL,
                             I_dept                 IN     DEPS.DEPT%TYPE)
RETURN NUMBER IS
BEGIN

   if POP_ROLLUP_MODS_GTT(O_error_msg,
                          I_obj_rollup_mods_tbl) <> 1 then
      RETURN 0;
   end if;

   merge into rpm_worksheet_zone_workspace rwd
   using rpm_worksheet_rollup_mods_gtt param
      on (    rwd.workspace_id        = param.workspace_id
          and rwd.dept                = I_dept
          and rwd.worksheet_status_id = param.worksheet_status_id
          and rwd.zone_id    = param.zone_id
          and (   (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_ITEM
                   and rwd.link_code                   is NULL
                   and rwd.item                        = param.item
                   and NVL(rwd.item_parent,-99999)     = NVL(param.item_parent,-99999))
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_ITEM_PARENT
                   and rwd.link_code                   is NULL
                   and rwd.item_parent                 = param.item_parent)
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_1
                   and rwd.link_code                   is NULL
                   and rwd.item_parent                 = param.item_parent
                   and NVL(rwd.diff_1,-99999)          = NVL(param.diff_1,-99999))
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_2
                   and rwd.link_code                   is NULL
                   and rwd.item_parent                 = param.item_parent
                   and NVL(rwd.diff_2,-99999)          = NVL(param.diff_2,-99999))
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_3
                   and rwd.link_code                   is NULL
                   and rwd.item_parent                 = param.item_parent
                   and NVL(rwd.diff_3,-99999)          = NVL(param.diff_3,-99999))
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_4
                   and rwd.link_code                   is NULL
                   and rwd.item_parent                 = param.item_parent
                   and NVL(rwd.diff_4,-99999)          = NVL(param.diff_4,-99999))
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_DIFF_TYPE
                   and rwd.link_code                   is NULL
                   and NVL(rwd.item_parent,  rwd.item) = NVL(param.item_parent, param.item)
                   and (   (    NVL(rwd.diff_1,-99999) = NVL(param.diff_1,-99999)
                            and rwd.diff_type_1        = param.diff_type_1)
                        or (    NVL(rwd.diff_2,-99999) = NVL(param.diff_2,-99999)
                            and rwd.diff_type_2        = param.diff_type_2)
                        or (    NVL(rwd.diff_3,-99999) = NVL(param.diff_3,-99999)
                            and rwd.diff_type_3        = param.diff_type_3)
                        or (    NVL(rwd.diff_4,-99999) = NVL(param.diff_4,-99999)
                            and rwd.diff_type_4        = param.diff_type_4)))
               or (    param.rollup_level              = RPM_CONSTANTS.WKSHT_ROLLUP_LVL_LINK_CODE
                   and param.link_code                 is NOT NULL
                   and rwd.link_code                   = param.link_code)
             ))
   when matched then
      update
         set action_flag               = param.action_flag,
             effective_date            = param.effective_date,
             new_retail                = param.new_retail,
             new_retail_std            = param.new_retail_std,
             new_retail_uom            = param.new_retail_uom,
             new_multi_units           = param.new_multi_units,
             ignore_constraint_boolean = param.ignore_constraint_boolean,
             new_multi_unit_uom        = param.new_multi_unit_uom,
             new_multi_unit_retail     = param.new_multi_unit_retail,
             ---
             new_clear_ind = case
                                when rwd.clear_boolean = 0 or
                                     rwd.clear_boolean is NULL then
                                   param.new_clear_ind
                                else
                                   rwd.new_clear_ind
                             end,
             state = case
                        when param.state is NOT NULL then
                           param.state
                        else
                           rwd.state
                     end,
             new_clear_mkdn_nbr = case
                                     when rwd.new_clear_mkdn_nbr is NULL then
                                        case (case
                                                 when rwd.clear_boolean = 0 or
                                                      rwd.clear_boolean is NULL then
                                                    param.new_clear_ind
                                                 else
                                                    rwd.new_clear_ind
                                              end)
                                           when 1 then
                                              1
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                     else
                                        case (case
                                                 when rwd.clear_boolean = 0 or
                                                      rwd.clear_boolean is NULL then
                                                    param.new_clear_ind
                                                 else
                                                    rwd.new_clear_ind
                                              end)
                                           when 1 then
                                              rwd.new_clear_mkdn_nbr
                                           when 0 then
                                              NULL
                                           when NULL then
                                              NULL
                                        end
                                  end,
             new_reset_date = case (case
                                       when rwd.clear_boolean = 0 or
                                            rwd.clear_boolean is NULL then
                                          param.new_clear_ind
                                       else
                                          rwd.new_clear_ind
                                    end)
                                 when 1 then
                                    param.new_reset_date
                                 when 0 then
                                    NULL
                                 when NULL then
                                    NULL
                              end,
             new_out_of_stock_date = case (case
                                              when rwd.clear_boolean = 0 or
                                                   rwd.clear_boolean is NULL then
                                                 param.new_clear_ind
                                              else
                                                 rwd.new_clear_ind
                                           end)
                                        when 1 then
                                           param.new_out_of_stock_date
                                        when 0 then
                                           NULL
                                        when NULL then
                                           NULL
                                     end,
             update_ind = 1
   when not matched then
      insert (workspace_id) values (-9999);

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_ROLLUP_MODS',
                                        to_char(SQLCODE));
      RETURN 0;

END UPDATE_ROLLUP_MODS;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_ROLLUP_MODS1 (O_error_msg                     IN OUT VARCHAR2,
                              I_obj_rollup_mods_parent_lvl    IN    OBJ_ROLLUP_MODS_TBL,
                              I_obj_rollup_mods_difftype_lvl  IN    OBJ_ROLLUP_MODS_TBL,
                              I_obj_rollup_mods_link_lvl      IN    OBJ_ROLLUP_MODS_TBL,
                              I_dept                          IN    DEPS.DEPT%TYPE,
                              I_workspace_id                  IN    NUMBER,
                              I_updateable_columns            IN    OBJ_VARCHAR_DESC_TABLE)
RETURN NUMBER IS
BEGIN

   if I_obj_rollup_mods_parent_lvl.COUNT > 0 then

      if POP_ROLLUP_MODS_GTT(O_error_msg,
                             I_obj_rollup_mods_parent_lvl) <> 1 then
         RETURN 0;
      end if;

      if MERGE_PARENT_LVL(O_error_msg,
                          I_dept,
                          I_workspace_id,
                          I_updateable_columns) <> 1 then
         RETURN 0;
      end if;

      delete from rpm_worksheet_rollup_mods_gtt;

   end if;

   if I_obj_rollup_mods_difftype_lvl.COUNT > 0 then

      if POP_ROLLUP_MODS_GTT(O_error_msg,
                             I_obj_rollup_mods_difftype_lvl) <> 1 then
         RETURN 0;
      end if;

      if MERGE_DIFFTYPE_LVL(O_error_msg,
                            I_dept,
                            I_workspace_id,
                            I_updateable_columns) <> 1 then
         RETURN 0;
      end if;

      delete from rpm_worksheet_rollup_mods_gtt;

   end if;

   if I_obj_rollup_mods_link_lvl.COUNT > 0 then

      if POP_ROLLUP_MODS_GTT(O_error_msg,
                             I_obj_rollup_mods_link_lvl) <> 1 then
         RETURN 0;
      end if;

      if MERGE_LINK_LVL(O_error_msg,
                        I_dept,
                        I_workspace_id,
                        I_updateable_columns) <> 1 then
         RETURN 0;
      end if;

      delete from rpm_worksheet_rollup_mods_gtt;

   end if;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := Sql_Lib.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_ROLLUP_MODS1',
                                        TO_CHAR(SQLCODE));
      RETURN 0;

END UPDATE_ROLLUP_MODS1;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_DATA_FROM_WORKSPACE(O_error_msg       IN OUT VARCHAR2,
                                    I_workspace_id    IN     NUMBER)
RETURN NUMBER IS

BEGIN

   if UPDATE_WORKSHEET_ITEM_DATA(O_error_msg,
                                 I_workspace_id) <> 1 then
      RETURN 0;
   end if;

   if UPDATE_WORKSHEET_ITEM_LOC_DATA(O_error_msg,
                                     I_workspace_id) <> 1 then
      RETURN 0;
   end if;

   if UPDATE_WORKSHEET_ZONE_DATA(O_error_msg,
                                 I_workspace_id) <> 1 then
      RETURN 0;
   end if;

   RETURN 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_DATA_FROM_WORKSPACE',
                                        TO_CHAR(SQLCODE));
      RETURN 0;

END UPDATE_DATA_FROM_WORKSPACE;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION UPDATE_SECONDARY_WORKSHEET(O_error_msg            IN OUT VARCHAR2,
                                    I_obj_rollup_mods_tbl  IN     OBJ_ROLLUP_MODS_TBL)
RETURN NUMBER IS

   L_ids                  OBJ_NUM_NUM_STR_TBL;
   L_ids_test             OBJ_NUM_NUM_STR_TBL;

   cursor C1 is
   select OBJ_NUM_NUM_STR_REC(worksheet_status_id,
                              worksheet_item_data_id,
                              NULL)
     from (select distinct
                   wild.worksheet_status_id,
                   wild.worksheet_item_data_id
              from (select distinct
                           t.workspace_id,
                           t.worksheet_status_id,
                           rwz.dept,
                           rwz.item,
                           rwz.link_code,
                           rwz.proposed_strategy_id,
                           rwz.area_diff_prim_id,
                           t.new_retail use_retail,
                           t.new_retail_std use_retail_std,
                           t.new_retail_uom use_retail_uom
                      from table(cast(I_obj_rollup_mods_tbl as OBJ_ROLLUP_MODS_TBL)) t,
                           rpm_worksheet_zone_workspace rwz
                     where rwz.workspace_id          = t.workspace_id
                       and rwz.worksheet_status_id   = t.worksheet_status_id
                       and NVL(rwz.new_clear_ind,0) != 1
                       and (t.link_code is NULL or
                            rwz.link_code = t.link_code)
                       and rwz.zone_id = t.zone_id
                       and ((t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.TRAN_LEVEL
                         and rwz.item = t.item) or
                            (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_LEVEL
                         and rwz.item_parent = t.item_parent) or
                            (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF1_LEVEL
                         and rwz.item_parent = t.item_parent
                         and rwz.diff_1 = t.diff_1) or
                            (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF2_LEVEL
                         and rwz.item_parent = t.item_parent
                         and rwz.diff_1 = t.diff_2) or
                            (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF3_LEVEL
                         and rwz.item_parent = t.item_parent
                         and rwz.diff_1 = t.diff_3) or
                            (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF4_LEVEL
                         and rwz.item_parent = t.item_parent
                         and rwz.diff_1 = t.diff_4) or
                            (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.DIFF_TYPE_LEVEL
                         and rwz.item_parent = t.item_parent
                         and ((rwz.diff_1 = t.diff_1 and
                               rwz.diff_type_1 = t.diff_type_1) or
                              (rwz.diff_2 = t.diff_1 and
                               rwz.diff_type_2 = t.diff_type_1) or
                              (rwz.diff_3 = t.diff_1 and
                               rwz.diff_type_3 = t.diff_type_1) or
                              (rwz.diff_4 = t.diff_1 and
                               rwz.diff_type_4 = t.diff_type_1))))) ws,
                   rpm_worksheet_status rws,
                   rpm_worksheet_item_data wid,
                   rpm_worksheet_item_loc_data wild
             where rws.area_diff_prim_ws_id = ws.worksheet_status_id
               and wid.worksheet_status_id = rws.worksheet_status_id
               and wid.proposed_strategy_id = ws.proposed_strategy_id
               and wid.dept = ws.dept
               and wid.item = ws.item
               and wid.area_diff_prim_id = ws.area_diff_prim_id
               and wid.area_diff_id is NOT NULL
               and wid.state IN (RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_NEW,
                                 RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_UPDATED,
                                 RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_IN_PROGRESS,
                                 RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_SUBMITTED)
               and wild.worksheet_item_data_id = wid.worksheet_item_data_id
               and (ws.link_code is NULL or
                    wild.link_code = ws.link_code));

BEGIN

   open C1;

   fetch C1
    bulk collect into L_ids;

   close C1;

   merge into rpm_worksheet_item_loc_data target
   using (select wild.*,
                 wid.area_diff_prim_id,
                 wid.area_diff_id,
                 wid.currency to_currency,
                 ws.from_currency,
             NVL(ws.use_retail, wild.basis_regular_retail) use_retail,
                 NVL(ws.use_retail_std, wild.basis_regular_retail_std) use_retail_std,
                 case
                 when ws.use_retail is NOT NULL then
                    ws.use_retail_uom
                 else
                    wild.basis_regular_retail_uom
                 end as use_retail_uom,
                 ws.use_effective_date,
                 rad.percent_apply_type,
                 rad.percent,
                 rad.price_guide_id,
                 wid.primary_comp_retail,
                 wid.primary_comp_store,
                 wid.primary_comp_retail_uom,
                 wid.primary_comp_retail_std,
                 wid.basis_zl_regular_retail,
                 wid.comp_mkt_basket_code
            from (select distinct
                         t.workspace_id,
                         t.worksheet_status_id,
                         rwz.dept,
                         rwz.item,
                         rwz.link_code,
                         rwz.proposed_strategy_id,
                         rwz.area_diff_prim_id,
                         rwz.currency from_currency,
                         t.new_retail use_retail,
                         t.new_retail_std use_retail_std,
                         t.new_retail_uom use_retail_uom,
                         t.effective_date use_effective_date,
                         rwz.primary_comp_retail,
                         rwz.primary_comp_store,
                         rwz.primary_comp_retail_uom,
                         rwz.primary_comp_retail_std
                    from table(cast(I_obj_rollup_mods_tbl as OBJ_ROLLUP_MODS_TBL)) t,
                         rpm_worksheet_zone_workspace rwz
                   where rwz.workspace_id          = t.workspace_id
                     and rwz.worksheet_status_id   = t.worksheet_status_id
                     and NVL(rwz.new_clear_ind,0) != 1
                     and (t.link_code is NULL or
                          rwz.link_code = t.link_code)
                     and rwz.zone_id = t.zone_id
                     and ((t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.TRAN_LEVEL
                       and rwz.item = t.item) or
                          (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_LEVEL
                       and rwz.item_parent = t.item_parent) or
                          (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF1_LEVEL
                       and rwz.item_parent = t.item_parent
                       and rwz.diff_1 = t.diff_1) or
                          (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF2_LEVEL
                       and rwz.item_parent = t.item_parent
                       and rwz.diff_1 = t.diff_2) or
                          (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF3_LEVEL
                       and rwz.item_parent = t.item_parent
                       and rwz.diff_1 = t.diff_3) or
                          (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.PARENT_DIFF4_LEVEL
                       and rwz.item_parent = t.item_parent
                       and rwz.diff_1 = t.diff_4) or
                          (t.rollup_level = RPM_WORKSHEET_ROLLUP_SQL.DIFF_TYPE_LEVEL
                       and rwz.item_parent = t.item_parent
                       and ((rwz.diff_1 = t.diff_1 and
                             rwz.diff_type_1 = t.diff_type_1) or
                            (rwz.diff_2 = t.diff_1 and
                             rwz.diff_type_2 = t.diff_type_1) or
                            (rwz.diff_3 = t.diff_1 and
                             rwz.diff_type_3 = t.diff_type_1) or
                            (rwz.diff_4 = t.diff_1 and
                             rwz.diff_type_4 = t.diff_type_1))))) ws,
                 rpm_worksheet_status rws,
                 rpm_worksheet_item_data wid,
                 rpm_worksheet_item_loc_data wild,
                 rpm_area_diff rad
           where rws.area_diff_prim_ws_id = ws.worksheet_status_id
             and wid.worksheet_status_id = rws.worksheet_status_id
             and wid.proposed_strategy_id = ws.proposed_strategy_id
             and wid.dept = ws.dept
             and wid.item = ws.item
             and wid.area_diff_prim_id = ws.area_diff_prim_id
             and wid.area_diff_id is NOT NULL
             and wid.state IN (RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_NEW,
                               RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_UPDATED,
                               RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_IN_PROGRESS,
                               RPM_WORKSHEET_ROLLUP_SQL.STATE_INT_SUBMITTED)
             and wild.worksheet_item_data_id = wid.worksheet_item_data_id
             and (ws.link_code is NULL or
                  wild.link_code = ws.link_code)
             and rad.area_diff_prim_id = wid.area_diff_prim_id
             and rad.area_diff_id = wid.area_diff_id) source
   on (target.worksheet_item_loc_data_id = source.worksheet_item_loc_data_id)
   when matched then
   update
      set target.new_retail = CALC_SECONDARY_RETAIL(source.area_diff_prim_id,
                                                    source.area_diff_id,
                                                    source.use_retail,
                                                    source.from_currency,
                                                    source.to_currency,
                                                    source.percent_apply_type,
                                                    source.percent,
                                                    source.price_guide_id,
                                                    source.primary_comp_retail,
                                                    source.primary_comp_store,
                                                    source.primary_comp_retail_uom,
                                                    source.primary_comp_retail_std,
                                                    source.basis_zl_regular_retail,
                                                    source.comp_mkt_basket_code),
          target.new_retail_std
                            = CALC_SECONDARY_RETAIL(source.area_diff_prim_id,
                                                    source.area_diff_id,
                                                    source.use_retail_std,
                                                    source.from_currency,
                                                    source.to_currency,
                                                    source.percent_apply_type,
                                                    source.percent,
                                                    source.price_guide_id,
                                                    source.primary_comp_retail,
                                                    source.primary_comp_store,
                                                    source.primary_comp_retail_uom,
                                                    source.primary_comp_retail_std,
                                                    source.basis_zl_regular_retail,
                                                    source.comp_mkt_basket_code),
          target.new_retail_uom = source.use_retail_uom,
          target.effective_date = source.use_effective_date;

   -- Refresh WPM_WORKSHEET_ZONE_DATA as RPM_WORKSHEET_ITEM_LOC_DATA is updated

   delete rpm_worksheet_zone_data
    where (worksheet_status_id, worksheet_item_data_id) in
           (select t.number_1, t.number_2
              from table(cast(L_ids as OBJ_NUM_NUM_STR_TBL)) t);

   insert into rpm_worksheet_zone_data
      (worksheet_item_data_id,
       worksheet_status_id,
       proposed_strategy_id,
       user_strategy_id,
       state,
       reset_state,
       item,
       item_desc,
       dept,
       dept_name,
       markup_calc_type,
       class,
       class_name,
       subclass,
       subclass_name,
       item_level,
       tran_level,
       standard_uom,
       diff_1,
       diff_type_1,
       diff_2,
       diff_type_2,
       diff_3,
       diff_type_3,
       diff_4,
       diff_type_4,
       retail_include_vat_ind,
       primary_supplier,
       vpn,
       uda_boolean,
       package_size,
       package_uom,
       retail_label_type,
       retail_label_value,
       season_phase_boolean,
       item_parent,
       parent_desc,
       parent_diff_1,
       parent_diff_2,
       parent_diff_3,
       parent_diff_4,
       parent_diff_1_type,
       parent_diff_2_type,
       parent_diff_3_type,
       parent_diff_4_type,
       parent_diff_1_type_desc,
       parent_diff_2_type_desc,
       parent_diff_3_type_desc,
       parent_diff_4_type_desc,
       parent_vpn,
       primary_comp_retail,
       primary_comp_store,
       primary_comp_retail_uom,
       primary_comp_retail_std,
       primary_multi_units,
       primary_multi_unit_retail,
       primary_multi_unit_retail_uom,
       a_comp_store,
       b_comp_store,
       c_comp_store,
       d_comp_store,
       e_comp_store,
       a_comp_retail,
       b_comp_retail,
       c_comp_retail,
       d_comp_retail,
       e_comp_retail,
       zone_group_id,
       zone_group_display_id,
       zone_group_name,
       zone_id,
       zone_display_id,
       zone_name,
       loc_type,
       currency,
       original_retail,
       current_regular_retail_flag,
       current_regular_retail_uom,
       current_reg_retail_uom_flag,
       current_clear_retail_flag,
       current_clear_retail_uom,
       current_clear_retail_uom_flag,
       current_cost_markup_flag,
       current_multi_unit_flag,
       current_multi_unit_retail_flag,
       current_multi_unit_uom,
       current_multi_unit_uom_flag,
       current_zl_regular_retail,
       current_zl_regular_retail_uom,
       current_zl_regular_retail_std,
       current_zl_clear_retail,
       current_zl_clear_retail_uom,
       current_zl_clear_retail_std,
       current_zl_retail_std,
       current_zl_multi_units,
       current_zl_multi_unit_retail,
       current_zl_multi_uom,
       past_price_chg_date,
       past_price_change_date_flag,
       past_cost_chg_date,
       past_cost_chg_date_flag,
       pend_cost_chg_date,
       pend_zl_cost_chg_cost,
       maint_margin_zl_retail,
       maint_margin_zl_retail_uom,
       maint_margin_zl_retail_std,
       maint_margin_zl_cost,
       current_cost_flag,
       basis_base_cost_flag,
       basis_pricing_cost_flag,
       current_zl_cost,
       current_zl_cost_zone,
       basis_zl_base_cost,
       basis_zl_pricing_cost,
       current_clearance_ind,
       historical_sales,
       store_historical_sales_units,
       wh_historical_sales_units,
       projected_sales,
       store_seasonal_sales,
       wh_seasonal_sales,
       first_received_date,
       first_received_date_flag,
       last_received_date,
       last_received_date_flag,
       wks_of_sales_exp,
       wks_of_sales_exp_flag,
       store_stock,
       wh_stock,
       store_on_order,
       wh_on_order,
       store_inventory,
       wh_inventory,
       ref_wh_stock,
       ref_wh_stock_value,
       ref_wh_stock_count,
       ref_wh_on_order,
       ref_wh_on_order_value,
       ref_wh_on_order_count,
       ref_wh_inventory,
       margin_mkt_basket_code,
       comp_mkt_basket_code,
       link_code,
       link_code_display_id,
       link_code_desc,
       replenish_ind,
       replenish_ind_flag,
       vat_rate,
       vat_value,
       clear_boolean,
       clear_flag,
       wks_first_sale,
       wks_first_sale_flag,
       promo_boolean,
       price_change_boolean,
       price_change_boolean_flag,
       basis_regular_retail_flag,
       basis_regular_retail_uom,
       basis_regular_retail_uom_flag,
       basis_clear_retail_flag,
       basis_clear_retail_uom,
       basis_clear_retail_uom_flag,
       basis_multi_units_flag,
       basis_multi_unit_retail_flag,
       basis_multi_unit_uom,
       basis_multi_unit_uom_flag,
       basis_zl_regular_retail,
       basis_zl_regular_retail_uom,
       basis_zl_regular_retail_std,
       basis_zl_clear_retail,
       basis_zl_clear_retail_uom,
       basis_zl_clear_retail_std,
       basis_zl_clear_mkdn_nbr,
       basis_zl_multi_units,
       basis_zl_multi_unit_retail,
       basis_zl_multi_uom,
       proposed_retail,
       proposed_retail_flag,
       proposed_retail_uom,
       proposed_retail_uom_flag,
       proposed_retail_std,
       proposed_clear_mkdn_nbr,
       proposed_clear_mkdn_nbr_flag,
       proposed_zl_reset_date,
       proposed_zl_out_of_stock_date,
       action_flag,
       action_flag_varies,
       effective_date,
       last_effective_date,
       effective_date_flag,
       effective_date_is_set,
       new_retail,
       new_retail_flag,
       new_retail_is_set,
       either_retail_is_set,
       new_retail_uom,
       new_retail_uom_flag,
       new_multi_units,
       new_multi_units_flag,
       new_multi_unit_retail,
       new_multi_unit_retail_flag,
       new_multi_unit_retail_is_set,
       new_multi_unit_uom,
       new_multi_unit_uom_flag,
       new_retail_std,
       sum_nmp_retail,
       avg_nmp_retail,
       sum_nmp_cost,
       new_clear_mkdn_nbr,
       new_clear_mkdn_nbr_flag,
       new_clear_ind,
       new_clear_ind_flag,
       new_reset_date,
       new_reset_date_flag,
       new_out_of_stock_date,
       new_out_of_stock_date_flag,
       new_item_loc_boolean,
       new_item_loc_boolean_flag,
       primary_comp_boolean,
       rule_boolean,
       conflict_boolean,
       conflict_boolean_flag,
       constraint_boolean,
       constraint_boolean_flag,
       ignore_constraint_boolean,
       area_diff_prim_id,
       area_diff_id,
       margin_mbc_name,
       competitive_mbc_name,
       reviewed,
       v_sales_ch_amt_ncurrent,
       v_sales_ch_proj_sales,
       v_sales_ch_prj_reg_retail,
       sales_change_amount_ncurrent,
       sales_change_prj_reg_retail,
       detail_count)
   select rwi.worksheet_item_data_id    ,
          rwi.worksheet_status_id   ,
          max(rwi.proposed_strategy_id)  ,
          max(user_strategy_id)      ,
          max(state)                 ,
          max(reset_state)           ,
          rwi.item                  ,
          max(item_desc)             ,
          rwi.dept                  ,
          max(dept_name)             ,
          max(markup_calc_type)      ,
          max(rwi.class)                 ,
          max(class_name)            ,
          max(rwi.subclass)              ,
          max(subclass_name)         ,
          max(item_level)            ,
          max(tran_level)            ,
          max(standard_uom)          ,
          max(diff_1)                ,
          max(diff_type_1)           ,
          max(diff_2)                ,
          max(diff_type_2)           ,
          max(diff_3)                ,
          max(diff_type_3)           ,
          max(diff_4)                ,
          max(diff_type_4)           ,
          max(retail_include_vat_ind),
          max(primary_supplier)      ,
          max(vpn)                   ,
          max(uda_boolean)           ,
          max(package_size)          ,
          max(package_uom)           ,
          max(retail_label_type)     ,
          max(retail_label_value)    ,
          max(season_phase_boolean)  ,
          max(rwi.item_parent)           ,
          max(parent_desc)           ,
          max(parent_diff_1)         ,
          max(parent_diff_2)         ,
          max(parent_diff_3)         ,
          max(parent_diff_4)         ,
          max(parent_diff_1_type)    ,
          max(parent_diff_2_type)    ,
          max(parent_diff_3_type)    ,
          max(parent_diff_4_type)    ,
          max(parent_diff_1_type_desc),
          max(parent_diff_2_type_desc),
          max(parent_diff_3_type_desc),
          max(parent_diff_4_type_desc),
          max(parent_vpn)             ,
          max(primary_comp_retail)    ,
          max(primary_comp_store)     ,
          max(primary_comp_retail_uom),
          max(primary_comp_retail_std),
          max(primary_multi_units)    ,
          max(primary_multi_unit_retail),
          max(primary_multi_unit_retail_uom),
          max(a_comp_store),
          max(b_comp_store),
          max(c_comp_store),
          max(d_comp_store),
          max(e_comp_store),
          max(a_comp_retail),
          max(b_comp_retail),
          max(c_comp_retail),
          max(d_comp_retail),
          max(e_comp_retail),
          max(zone_group_id),
          max(zone_group_display_id)   ,
          max(zone_group_name)         ,
          rwi.zone_id                 ,
          max(zone_display_id)         ,
          max(zone_name)               ,
          max(loc_type)                ,
          max(currency)                ,
          max(original_retail)         ,
          case max(nvl(current_regular_retail, 0))
             when min(nvl(current_regular_retail, 0))
             then 0
             else 1
          end as current_regular_retail_flag,
          max(nvl(current_regular_retail_uom, 0)) current_regular_retail_uom,
          case max(nvl(current_regular_retail_uom, 0))
             when min(nvl(current_regular_retail_uom, 0))
             then 0
             else 1
          end as current_reg_retail_uom_flag,
          case max(nvl(current_clear_retail,0))
             when min(nvl(current_clear_retail,0))
             then 0
             else 1
          end as current_clear_retail_flag,
          max(nvl(current_clear_retail_uom, 0)) current_clear_retail_uom,
          case max(nvl(current_clear_retail_uom, 0))
             when min(nvl(current_clear_retail_uom, 0))
             then 0
             else 1
          end as current_clear_retail_uom_flag,
          case max((case
                    when current_clearance_ind = 1
                       then nvl(current_clear_retail,0)
                       else nvl(current_regular_retail, 0)
                    end))
          when min((case
                    when current_clearance_ind = 1
                       then nvl(current_clear_retail,0)
                       else nvl(current_regular_retail,0)
                    end))
             then 0
             else 1
          end as current_cost_markup_flag,
          case max(nvl(current_multi_units,0))
          when min(nvl(current_multi_units,0))
             then 0
             else 1
          end as current_multi_unit_flag,
          case max(nvl(current_multi_unit_retail,0))
          when min(nvl(current_multi_unit_retail,0))
             then 0
             else 1
          end as current_multi_unit_retail_flag,
          max(nvl(current_multi_unit_uom,0)) current_multi_unit_uom,
          case max(nvl(current_multi_unit_uom,0))
             when min(nvl(current_multi_unit_uom,0))
             then 0
             else 1
          end as current_multi_unit_uom_flag,
          max(current_zl_regular_retail),
          max(current_zl_regular_retail_uom),
          max(current_zl_regular_retail_std),
          max(current_zl_clear_retail)  ,
          max(current_zl_clear_retail_uom),
          max(current_zl_clear_retail_std),
          sum(case
              when current_clearance_ind = 1 then
                 current_zl_clear_retail_std
              else
              current_zl_regular_retail_std end) current_zl_retail_std,
          max(current_zl_multi_units)     ,
          max(current_zl_multi_unit_retail),
          max(current_zl_multi_uom)       ,
          max(past_price_chg_date)        ,
          case max(nvl(past_price_chg_date, sysdate))
          when max(nvl(past_price_chg_date, sysdate))
             then 0
             else 1
          end as past_price_change_date_flag,
          max(past_cost_chg_date)    ,
          case max(nvl(past_cost_chg_date, sysdate))
          when max(nvl(past_cost_chg_date, sysdate))
             then 0
             else 1
          end as past_cost_chg_date_flag,
          min(pend_cost_chg_date),
          max(pend_zl_cost_chg_cost) ,
          max(maint_margin_zl_retail)     ,
          max(maint_margin_zl_retail_uom) ,
          max(maint_margin_zl_retail_std) ,
          max(maint_margin_zl_cost)       ,
          case max(nvl(current_cost, 0))
          when min (nvl(current_cost, 0))
             then 0
             else 1
          end as current_cost_flag        ,
          case max(nvl(basis_base_cost, 0))
          when min (nvl(basis_base_cost, 0))
             then 0
             else 1
          end as basis_base_cost_flag  ,
           case max(nvl(basis_pricing_cost, 0))
          when min (nvl(basis_pricing_cost, 0))
             then 0
             else 1
          end as basis_pricing_cost_flag         ,
          max(current_zl_cost) current_zl_cost,
          sum(current_zl_cost) current_zl_cost_zone,
          max(basis_zl_base_cost) basis_zl_base_cost,
          max(basis_zl_pricing_cost) basis_zl_pricing_cost,
          max(current_clearance_ind) current_clearance_ind,
          sum(historical_sales) historical_sales,
          sum(case
              when loc_type = 0
                 then historical_sales_units
                 else 0
              end) store_historical_sales_units,
          sum(case
              when loc_type = 2
                 then historical_sales_units
                 else 0
              end) wh_historical_sales_units,
          sum(projected_sales)            ,
          sum(case
              when loc_type = 0
                 then seasonal_sales
                 else 0
              end) store_seasonal_sales,
          sum(case
              when loc_type = 2
                 then seasonal_sales
                 else 0
              end) wh_seasonal_sales,
          min(first_received_date)       ,
          case max(nvl(first_received_date, sysdate))
          when min(nvl(first_received_date, sysdate))
             then 0
             else 1
          end as first_received_date_flag,
          max(last_received_date)         ,
          case max(nvl(last_received_date, sysdate))
          when min(nvl(last_received_date, sysdate))
             then 0
             else 1
          end as last_received_date_flag,
          max(wks_of_sales_exp)         ,
          case max(nvl(wks_of_sales_exp, 0))
          when min(nvl(wks_of_sales_exp, 0))
             then 0
             else 1
          end as wks_of_sales_exp_flag,
          sum(case
              when loc_type = 0
                 then location_stock
                 else 0
              end) store_stock,
          sum(case
              when loc_type = 2
                 then location_stock
                 else 0
              end) wh_stock,
          sum(case
              when loc_type = 0
                 then location_on_order
                 else 0
              end) store_on_order,
          sum(case
              when loc_type = 2
                 then location_on_order
                 else 0
              end) wh_on_order,
          sum(case
              when loc_type = 0
                 then location_inventory
                 else 0
              end) store_inventory,
          sum(case
              when loc_type = 2
                 then location_inventory
                 else 0
              end) wh_inventory,
          sum(ref_wh_stock) ref_wh_stock,
          max(ref_wh_stock) ref_wh_stock_value,
          count(ref_wh_stock) ref_wh_stock_count,
          sum(ref_wh_on_order) ref_wh_on_order,
          max(ref_wh_on_order) ref_wh_on_order_value,
          count(ref_wh_on_order) ref_wh_on_order_count,
          max(ref_wh_inventory)           ,
          max(margin_mkt_basket_code)     ,
          max(comp_mkt_basket_code)       ,
          link_code                  ,
          max(link_code_display_id)       ,
          max(link_code_desc)             ,
          max(replenish_ind)              ,
          case max(nvl(replenish_ind, 0))
             when min(nvl(replenish_ind, 0)) then 0
             else 1
          end as replenish_ind_flag  ,
          avg(vat_rate)              ,
          avg(vat_value)              ,
          max(nvl(clear_boolean,0)) clear_boolean              ,
          case max(greatest(nvl(clear_boolean,0), nvl(current_clearance_ind,0)))
          when min(greatest(nvl(clear_boolean,0), nvl(current_clearance_ind,0)))
             then 0
             else 1
          end as clear_flag,
          max(wks_first_sale)        ,
          case max(nvl(wks_first_sale, 0))
          when min(nvl(wks_first_sale, 0))
             then 0
             else 1
          end as wks_first_sale_flag,
          max(nvl(promo_boolean,0)) promo_boolean              ,
          max(nvl(price_change_boolean,0)) price_change_boolean,
          case max(nvl(price_change_boolean, 0))
          when min(nvl(price_change_boolean, 0))
             then 0
             else 1
          end as price_change_boolean_flag,
          case max(nvl(basis_regular_retail, 0))
             when min(nvl(basis_regular_retail, 0))
             then 0
             else 1
          end as basis_regular_retail_flag,
          max(nvl(basis_regular_retail_uom, 0)) basis_regular_retail_uom,
          case max(nvl(basis_regular_retail_uom, 0))
             when min(nvl(basis_regular_retail_uom, 0))
             then 0
             else 1
          end as basis_regular_retail_uom_flag,
          case max(nvl(basis_clear_retail, 0))
             when min(nvl(basis_clear_retail, 0))
             then 0
             else 1
          end as basis_clear_retail_flag         ,
          max(nvl(basis_clear_retail_uom, 0)) basis_clear_retail_uom,
          case max(nvl(basis_clear_retail_uom, 0))
             when min(nvl(basis_clear_retail_uom, 0))
             then 0
             else 1
          end as basis_clear_retail_uom_flag     ,
          case max(nvl(basis_multi_units,0))
          when min(nvl(basis_multi_units,0))
             then 0
             else 1
          end as basis_multi_units_flag,
          case max(nvl(basis_multi_unit_retail,0))
          when min(nvl(basis_multi_unit_retail,0))
             then 0
             else 1
          end as basis_multi_unit_retail_flag,
          max(nvl(basis_multi_unit_uom,0)) basis_multi_unit_uom,
          case max(nvl(basis_multi_unit_uom,0))
             when min(nvl(basis_multi_unit_uom,0))
             then 0
             else 1
          end as basis_multi_unit_uom_flag,
          max(basis_zl_regular_retail)    ,
          max(basis_zl_regular_retail_uom),
          max(basis_zl_regular_retail_std),
          max(basis_zl_clear_retail)      ,
          max(basis_zl_clear_retail_uom)  ,
          max(basis_zl_clear_retail_std)  ,
          max(basis_zl_clear_mkdn_nbr)    ,
          max(basis_zl_multi_units)       ,
          max(basis_zl_multi_unit_retail) ,
          max(basis_zl_multi_uom)         ,
          avg(proposed_retail)            ,
          case max(nvl(proposed_retail,0))
             when min(nvl(proposed_retail,0))
             then 0
             else 1
          end as proposed_retail_flag,
          min(proposed_retail_uom)        ,
          case max(nvl(proposed_retail_uom,0))
             when min(nvl(proposed_retail_uom,0))
             then 0
             else 1
          end as proposed_retail_uom_flag,
          sum(proposed_retail_std)        ,
          max(proposed_clear_mkdn_nbr)    ,
          case max(nvl(proposed_clear_mkdn_nbr,0))
             when min(nvl(proposed_clear_mkdn_nbr,0))
             then 0
             else 1
          end as proposed_clear_mkdn_nbr_flag,
          min(proposed_zl_reset_date)     ,
          min(proposed_zl_out_of_stock_date),
          max(rwil.action_flag)                ,
          case max(nvl(rwil.action_flag,0))
             when min(nvl(rwil.action_flag,0))
             then 0
             else 1
          end as action_flag_varies  ,
          min(effective_date)        ,
          max(effective_date)  last_effective_date        ,
          case max(nvl(effective_date,sysdate))
             when min(nvl(effective_date,sysdate))
             then 0
             else 1
          end as effective_date_flag        ,
          min(case
              when effective_date is null
                 then 0
                 else 1
              end) as effective_date_is_set      ,
          avg(new_retail)            ,
          case max(nvl(new_retail, 0))
          when min(nvl(new_retail, 0))
             then 0
             else 1
          end as new_retail_flag,
          min (case
               when new_retail is null
                  then 0
                  else 1
               end) as new_retail_is_set,
          min (case
               when new_retail is null and new_multi_unit_retail is null
                  then 0
                  else 1
               end) as either_retail_is_set,
          min(new_retail_uom)        ,
          case max(nvl(new_retail_uom,0))
             when min(nvl(new_retail_uom,0))
             then 0
             else 1
          end as new_retail_uom_flag        ,
          max(new_multi_units)            ,
          case max(nvl(new_multi_units,0))
             when min(nvl(new_multi_units,0))
             then 0
             else 1
          end as new_multi_units_flag ,
          max(new_multi_unit_retail) ,
          case max(nvl(new_multi_unit_retail, 0))
          when min(nvl(new_multi_unit_retail, 0))
             then 0
             else 1
          end as new_multi_unit_retail_flag,
          min(case
              when new_multi_unit_retail is null
                 then 0
                 else 1
              end) as new_multi_unit_retail_is_set,
          min(new_multi_unit_uom)    ,
          case max(nvl(new_multi_unit_uom, 0))
          when min(nvl(new_multi_unit_uom, 0))
             then 0
             else 1
          end as new_multi_unit_uom_flag,
          avg(new_retail_std)        ,
          sum(case
              when new_retail_std is not null
                 then new_retail_std
                 else
                    case
                    when current_clearance_ind = 1
                       then
                          case
                          when basis_zl_clear_retail_uom = standard_uom
                             then basis_zl_clear_retail
                             else null
                          end
                       else
                          case
                          when basis_zl_regular_retail_uom = standard_uom
                             then basis_zl_regular_retail
                             else null
                          end
                    end
              end) as sum_nmp_retail,
          avg(case
              when new_retail_std is not null
                 then new_retail_std
                 else
                    case
                    when current_clearance_ind = 1
                       then
                          case
                          when basis_zl_clear_retail_uom = standard_uom
                             then basis_zl_clear_retail
                             else null
                          end
                       else
                          case
                          when basis_zl_regular_retail_uom = standard_uom
                             then basis_zl_regular_retail
                             else null
                          end
                    end
              end) as avg_nmp_retail,
          sum(basis_zl_pricing_cost) sum_nmp_cost,
          max(new_clear_mkdn_nbr)    ,
          case max(nvl(new_clear_mkdn_nbr, 0))
          when min(nvl(new_clear_mkdn_nbr, 0))
             then 0
             else 1
          end as new_clear_mkdn_nbr_flag,
          min(nvl(new_clear_ind,0))  ,
          case max(nvl(new_clear_ind, 0))
          when min(nvl(new_clear_ind, 0))
             then 0
             else 1
          end as new_clear_ind_flag,
          min(new_reset_date)        ,
          case max(nvl(new_reset_date, sysdate))
          when min(nvl(new_reset_date, sysdate))
             then 0
             else 1
          end as new_reset_date_flag,
          min(new_out_of_stock_date) ,
          case max(nvl(new_out_of_stock_date, sysdate))
          when min(nvl(new_out_of_stock_date, sysdate))
             then 0
             else 1
          end as new_out_of_stock_date_flag,
          max(new_item_loc_boolean)       ,
          case max(nvl(new_item_loc_boolean, 0))
          when min(nvl(new_item_loc_boolean, 0))
             then 0
             else 1
          end as new_item_loc_boolean_flag,
          max(primary_comp_boolean)       ,
          max(nvl(rule_boolean,0))        ,
          max(nvl(conflict_boolean,0))    ,
          case max(nvl(conflict_boolean, 0))
          when min(nvl(conflict_boolean, 0))
             then 0
             else 1
          end as conflict_boolean_flag,
          max(nvl(constraint_boolean,0))  ,
          case max(nvl(constraint_boolean, 0))
          when min(nvl(constraint_boolean, 0))
             then 0
             else 1
          end as constraint_boolean_flag,
          max(nvl(ignore_constraint_boolean,0)),
          max(area_diff_prim_id)          ,
          max(area_diff_id)               ,
          max(margin_mbc_name)            ,
          max(competitive_mbc_name)       ,
          max(reviewed),
          case max(rwil.action_flag)
          when 0
             then sum((projected_sales * new_retail)-(basis_regular_retail * projected_sales))
             else sum(case rwil.action_flag
                      when 1
                         then (projected_sales * new_retail)-(basis_regular_retail * projected_sales)
                         else 0
                      end)
          end as v_sales_ch_amt_ncurrent,
         case max(rwil.action_flag)
         when 0
           then sum(projected_sales)
           else sum(case rwil.action_flag
                    when 1
                       then (projected_sales)
                       else 0
                    end)
         end as v_sales_ch_proj_sales,
         case max(rwil.action_flag)
         when 0
            then sum(basis_regular_retail * projected_sales)
            else sum(case rwil.action_flag
                     when 1
                        then (basis_regular_retail * projected_sales)
                         else 0
                     end)
         end as v_sales_ch_prj_reg_retail,
         sum((projected_sales * new_retail)-(basis_regular_retail * projected_sales)) as sales_change_amount_ncurrent,
         sum(basis_regular_retail * projected_sales) as sales_change_prj_reg_retail,
         count(1) detail_count
    from rpm_worksheet_item_data rwi,
         rpm_worksheet_item_loc_data rwil,
         table(cast(L_ids as OBJ_NUM_NUM_STR_TBL)) t
   where rwi.worksheet_status_id = t.number_1
     and rwi.worksheet_item_data_id = t.number_2
     and rwil.dept = rwi.dept
     and rwil.worksheet_status_id = rwi.worksheet_status_id
     and rwil.worksheet_item_data_id = rwi.worksheet_item_data_id
   group by rwi.dept,
            rwi.worksheet_status_id,
            rwi.worksheet_item_data_id,
            rwil.link_code,
            rwi.item,
            rwi.zone_id;

   return 1;

EXCEPTION
   when others then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_WORKSHEET_ROLLUP_SQL.UPDATE_SECONDARY_WORKSHEET',
                                        TO_CHAR(SQLCODE));
      return 0;

END UPDATE_SECONDARY_WORKSHEET;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION CALC_SECONDARY_RETAIL(I_area_diff_prim_id         IN   NUMBER,
                               I_area_diff_id              IN   NUMBER,
                               I_retail_in                 IN   NUMBER,
                               I_currency_in               IN   VARCHAR2,
                               I_currency_out              IN   VARCHAR2,
                               I_percent_apply_type        IN   NUMBER,
                               I_percent                   IN   NUMBER,
                               I_price_guide_id            IN   NUMBER,
                               I_primary_comp_retail       IN   NUMBER,
                               I_primary_comp_store        IN   NUMBER,
                               I_primary_comp_retail_uom   IN   VARCHAR2,
                               I_primary_comp_retail_std   IN   NUMBER,
                               I_basis_zl_regular_retail   IN   NUMBER,
                               I_comp_mkt_basket_code      IN   VARCHAR2)
RETURN NUMBER IS

   L_program            VARCHAR2(50) := 'RPM_WORKSHEET_ROLLUP_SQL.CALC_SECONDARY_RETAIL';
   L_converted_retail   NUMBER(20,4) := NULL;
   L_retail_out         NUMBER(20,4) := NULL;

   L_compete_type       RPM_AREA_DIFF_COMP.COMPETE_TYPE%TYPE := NULL;
   L_percent            RPM_AREA_DIFF_COMP.PERCENT%TYPE      := NULL;
   L_from_percent       RPM_AREA_DIFF_COMP.FROM_PERCENT%TYPE := NULL;
   L_to_percent         RPM_AREA_DIFF_COMP.TO_PERCENT%TYPE   := NULL;

   L_comp_secondary_proposed         NUMBER(1)    := 0;
   L_comp_secondary_applied          NUMBER(1)    := 0;
   L_comp_secondary_retail           NUMBER(20,4) := NULL;
   L_min_comp_retail                 NUMBER(20,4) := NULL;
   L_max_comp_retail                 NUMBER(20,4) := NULL;

   cursor C_COMP is
   select compete_type,
          percent,
          from_percent,
          to_percent
     from rpm_area_diff_comp
    where area_diff_id = I_area_diff_id;

   cursor C_MBC is
   select comp.compete_type,
          comp.percent,
          comp.from_percent,
          comp.to_percent
     from rpm_area_diff_comp rad,
          rpm_area_diff_mbc ram,
          rpm_area_diff_comp comp
    where rad.area_diff_id      = I_area_diff_id
      and ram.area_diff_comp_id = rad.area_diff_comp_id
      and ram.mkt_basket_code   = I_comp_mkt_basket_code
      and comp.area_diff_mbc_id = ram.area_diff_mbc_id;

BEGIN

   -- Currency Conversion If neccessary
   if I_currency_in != I_currency_out then
      L_converted_retail := CURRENCY_SQL.CONVERT_VALUE('R',
                                                       I_currency_out,
                                                       I_currency_in,
                                                       I_retail_in);
   else
      L_converted_retail := I_retail_in;
   end if;

   if I_percent_apply_type = RPM_WORKSHEET_ROLLUP_SQL.HIGHER_VALUE then
      L_converted_retail := L_converted_retail * (1 + I_percent/100);
   elsif I_percent_apply_type = RPM_WORKSHEET_ROLLUP_SQL.LOWER_VALUE then
      L_converted_retail := L_converted_retail * (1 - I_percent/100);
   end if;

   open C_COMP;
   fetch C_COMP into L_compete_type,
                     L_percent,
                     L_from_percent,
                     L_to_percent;

   if C_COMP%FOUND then
      if L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.MATCH then
         if I_primary_comp_retail is NULL then
            L_comp_secondary_retail   := NULL;
            L_comp_secondary_applied  := 0;
            L_comp_secondary_proposed := 0;
         elsif I_basis_zl_regular_retail = I_primary_comp_retail then
            L_comp_secondary_retail   := I_basis_zl_regular_retail;
            L_comp_secondary_applied  := 1;
            L_comp_secondary_proposed := 0;
         else
            L_comp_secondary_retail   := I_primary_comp_retail;
            L_comp_secondary_applied  := 1;
            L_comp_secondary_proposed := 1;
         end if;
      elsif I_primary_comp_retail is NULL then
         L_comp_secondary_retail   := NULL;
         L_comp_secondary_applied  := 0;
         L_comp_secondary_proposed := 0;
      elsif L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.PRICE_ABOVE then
         L_min_comp_retail := I_primary_comp_retail * (1 + L_from_percent/100);
         L_max_comp_retail := I_primary_comp_retail * (1 + L_to_percent/100);
         if I_primary_comp_retail is NULL then
            L_comp_secondary_retail   := NULL;
            L_comp_secondary_applied  := 0;
            L_comp_secondary_proposed := 0;
         elsif I_basis_zl_regular_retail > L_max_comp_retail or
            I_basis_zl_regular_retail < L_min_comp_retail then
            L_comp_secondary_retail   := I_primary_comp_retail * (1 + L_percent/100);
            L_comp_secondary_applied  := 1;
            L_comp_secondary_proposed := 1;
         else
            L_comp_secondary_retail   := I_basis_zl_regular_retail;
            L_comp_secondary_applied  := 1;
            L_comp_secondary_proposed := 1;
         end if;
      elsif L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.PRICE_BELOW then
         L_min_comp_retail := I_primary_comp_retail * (1 - L_to_percent/100);
         L_max_comp_retail := I_primary_comp_retail * (1 - L_from_percent/100);
         if I_primary_comp_retail is NULL then
            L_comp_secondary_retail   := NULL;
            L_comp_secondary_applied  := 0;
            L_comp_secondary_proposed := 0;
         elsif I_basis_zl_regular_retail > L_max_comp_retail or
            I_basis_zl_regular_retail < L_min_comp_retail then
            L_comp_secondary_retail   := I_primary_comp_retail * (1 - L_percent/100);
            L_comp_secondary_applied  := 1;
            L_comp_secondary_proposed := 1;
         else
            L_comp_secondary_retail   := I_basis_zl_regular_retail;
            L_comp_secondary_applied  := 1;
            L_comp_secondary_proposed := 1;
         end if;
      elsif L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.PRICE_BY_CODE then

         open C_MBC;
         fetch C_MBC into L_compete_type,
                          L_percent,
                          L_from_percent,
                          L_to_percent;

        if C_MBC%FOUND then
            if L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.MATCH then
               if I_primary_comp_retail is NULL then
                  L_comp_secondary_retail   := NULL;
                  L_comp_secondary_applied  := 0;
                  L_comp_secondary_proposed := 0;
               elsif I_basis_zl_regular_retail = I_primary_comp_retail then
                  L_comp_secondary_retail   := I_basis_zl_regular_retail;
                  L_comp_secondary_applied  := 1;
                  L_comp_secondary_proposed := 0;
               else
                  L_comp_secondary_retail   := I_primary_comp_retail;
                  L_comp_secondary_applied  := 1;
                  L_comp_secondary_proposed := 1;
               end if;
            elsif L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.PRICE_ABOVE then
               L_min_comp_retail := I_primary_comp_retail * (1 + L_from_percent/100);
               L_max_comp_retail := I_primary_comp_retail * (1 + L_to_percent/100);
               if I_primary_comp_retail is NULL then
                  L_comp_secondary_retail   := NULL;
                  L_comp_secondary_applied  := 0;
                  L_comp_secondary_proposed := 0;
               elsif I_basis_zl_regular_retail > L_max_comp_retail and
                  I_basis_zl_regular_retail < L_min_comp_retail then
                  L_comp_secondary_retail   := I_primary_comp_retail * (1 + L_percent/100);
                  L_comp_secondary_applied  := 1;
                  L_comp_secondary_proposed := 1;
               else
                  L_comp_secondary_retail   := I_basis_zl_regular_retail;
                  L_comp_secondary_applied  := 1;
                  L_comp_secondary_proposed := 0;
               end if;
            elsif L_compete_type = RPM_WORKSHEET_ROLLUP_SQL.PRICE_BELOW then
               L_min_comp_retail := I_primary_comp_retail * (1 + L_from_percent/100);
               L_max_comp_retail := I_primary_comp_retail * (1 + L_to_percent/100);
               if I_primary_comp_retail is NULL then
                  L_comp_secondary_retail   := NULL;
                  L_comp_secondary_applied  := 0;
                  L_comp_secondary_proposed := 0;
               elsif I_basis_zl_regular_retail > L_max_comp_retail and
                  I_basis_zl_regular_retail < L_min_comp_retail then
                  L_comp_secondary_retail   := I_primary_comp_retail * (1 - L_percent/100);
                  L_comp_secondary_applied  := 1;
                  L_comp_secondary_proposed := 1;
               else
                  L_comp_secondary_retail   := I_basis_zl_regular_retail;
                  L_comp_secondary_applied  := 1;
                  L_comp_secondary_proposed := 0;
               end if;
            end if;
         else
            L_comp_secondary_retail   := NULL;
            L_comp_secondary_applied  := 0;
            L_comp_secondary_proposed := 0;
         end if;

       close C_MBC;

      end if;

      if L_comp_secondary_applied = 1 then
         if L_converted_retail is NOT NULL then
            if L_comp_secondary_proposed = 1 then
               if L_converted_retail > L_comp_secondary_retail then
                  -- The proposed retail is greater than the proposed value for the secondary
                  -- area differential competitor. Propose using the value for the secondary
                  -- area differential
                  L_converted_retail := L_comp_secondary_retail;
               end if;
            else
               if L_converted_retail > L_comp_secondary_retail then
                  -- The secondary area did not propose, but the primary did. If the value
                  -- proposed for the primary is greater than the retail that was found OK by the
                  -- secondary do not propose
                  L_converted_retail := NULL;
               end if;
            end if;
         else
            -- No value was proposed by the primary worksheet, apply the value proposed by the
            -- secondary area differential differential competitor if the secondary proposed.
            if L_comp_secondary_proposed = 1 then
               L_converted_retail := L_comp_secondary_retail;
            else
               -- Neither the primary worksheet nor the secondary worksheet proposed a retail.
               L_converted_retail := NULL;
            end if;

         end if;
      end if;

   end if;

   close C_COMP;

   if I_price_guide_id is NOT NULL and
      L_converted_retail is NOT NULL then
      L_retail_out := APPLY_PRICE_GUIDE(L_converted_retail,
                                        I_price_guide_id);
   else
      L_retail_out := L_converted_retail;
   end if;

   return L_retail_out;

EXCEPTION
   when OTHERS then

      RAISE_APPLICATION_ERROR(-20000,
                              L_program||SQLERRM||TO_CHAR(SQLCODE));

END CALC_SECONDARY_RETAIL;
-----------------------------------------------------------------------------------------------------------------------

FUNCTION APPLY_PRICE_GUIDE(I_selling_retail    IN     NUMBER,
                           I_price_guide_id    IN     NUMBER)
RETURN NUMBER IS

   cursor C_GUIDE is
      select new_price_val
        from rpm_price_guide_interval
       where price_guide_id = I_price_guide_id
         and ROUND(I_selling_retail,2) between from_price_val and to_price_val;

   L_new_retail                NUMBER;

BEGIN

   if I_price_guide_id is NULL then
      L_new_retail := I_selling_retail;
   else
      open C_GUIDE;
      fetch C_GUIDE into L_new_retail;

      if C_GUIDE%NOTFOUND then
         L_new_retail := I_selling_retail;
      end if;

      close C_GUIDE;
   end if;

   return L_new_retail;

EXCEPTION
   when OTHERS then
      raise_application_error(-20000, 'RPM_WORKSHEET_ROLLUP_SQL.APPLY_PRICE_GUIDE '||SQLERRM||TO_CHAR(SQLCODE));

END APPLY_PRICE_GUIDE;
-----------------------------------------------------------------------------------------------------------------------

END RPM_WORKSHEET_ROLLUP_SQL;
/
