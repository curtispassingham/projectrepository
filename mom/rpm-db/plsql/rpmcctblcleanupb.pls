CREATE OR REPLACE PACKAGE BODY RPM_CC_TBL_CLEAN_UP AS

--------------------------------------------------------------------------------
FUNCTION PURGE_UNUSED_CC_TBL_RECS(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CC_TBL_CLEAN_UP.PURGE_UNUSED_CC_TBL_RECS';

   L_con_check_err_ids OBJ_NUMERIC_ID_TABLE := NULL;

BEGIN

   ----------------------------------------------------------
   -- Clean up rpm_conflict_check_result
   ----------------------------------------------------------

   -- Clean up Price Changes
   delete
     from rpm_conflict_check_result rccr
    where event_type = RPM_CONSTANTS.PC_EVENT_TYPE
      and NOT EXISTS (select 1
                        from rpm_price_change rpc
                       where rccr.event_id = rpc.price_change_id);

   -- Clean up Clearances and Clearance Resets
   delete
     from rpm_conflict_check_result rccr
    where event_type = RPM_CONSTANTS.CL_CLR_EVENT_TYPES
      and NOT EXISTS (select 1
                        from rpm_clearance rc
                       where rccr.event_id = rc.clearance_id
                      union all
                      select 1
                        from rpm_clearance_reset rcr
                       where rccr.event_id = rcr.clearance_id);

   -- Clean up Promotions
   delete
     from rpm_conflict_check_result rccr
    where event_type = RPM_CONSTANTS.PROM_EVENT_TYPE
      and NOT EXISTS (select 1
                        from rpm_promo_dtl rpd
                       where rccr.event_id = rpd.promo_dtl_id);

   ----------------------------------------------------------
   -- Clean up rpm_con_check_err and rpm_con_check_err_detail
   ----------------------------------------------------------

   -- Clean up Price Changes
   delete
     from rpm_con_check_err err
    where ref_class = RPM_CONSTANTS.PC_REF_CLASS
      and NOT EXISTS (select 1
                        from rpm_price_change rpc
                       where err.ref_id = rpc.price_change_id)
   RETURNING err.con_check_err_id BULK COLLECT into L_con_check_err_ids;

   forall i IN 1..L_con_check_err_ids.COUNT
      delete
        from rpm_con_check_err_detail det
       where det.con_check_err_id = L_con_check_err_ids(i);

   -- Clean up Clearances
   delete
     from rpm_con_check_err err
    where ref_class = RPM_CONSTANTS.CL_REF_CLASS
      and NOT EXISTS (select 1
                        from rpm_clearance rc
                       where err.ref_id = rc.clearance_id)
   RETURNING err.con_check_err_id BULK COLLECT into L_con_check_err_ids;

   forall i IN 1..L_con_check_err_ids.COUNT
      delete
        from rpm_con_check_err_detail det
       where det.con_check_err_id = L_con_check_err_ids(i);

   -- Clean up Clearance Resets
   delete
     from rpm_con_check_err err
    where ref_class = RPM_CONSTANTS.CR_REF_CLASS
      and NOT EXISTS (select 1
                        from rpm_clearance_reset rcr
                       where err.ref_id = rcr.clearance_id)
   RETURNING err.con_check_err_id BULK COLLECT into L_con_check_err_ids;

   forall i IN 1..L_con_check_err_ids.COUNT
      delete
        from rpm_con_check_err_detail det
       where det.con_check_err_id = L_con_check_err_ids(i);

   -- Clean up Promotions
   delete
     from rpm_con_check_err err
    where ref_class = RPM_CONSTANTS.COMP_DETAIL_REF_CLASS
      and NOT EXISTS (select 1
                        from rpm_promo_dtl rpd
                       where err.ref_id = rpd.promo_dtl_id)
   RETURNING err.con_check_err_id BULK COLLECT into L_con_check_err_ids;

   forall i IN 1..L_con_check_err_ids.COUNT
      delete
        from rpm_con_check_err_detail det
       where det.con_check_err_id = L_con_check_err_ids(i);

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
   return 0;

END PURGE_UNUSED_CC_TBL_RECS;
--------------------------------------------------------------------------------

END RPM_CC_TBL_CLEAN_UP;
/
