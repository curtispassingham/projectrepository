CREATE OR REPLACE PACKAGE RPM_SYSTEM_OPTIONS_SQL AS
--------------------------------------------------------
FUNCTION RESET_GLOBALS(O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_SYSTEM_OPTIONS_ID(O_system_options_id IN OUT RPM_SYSTEM_OPTIONS.SYSTEM_OPTIONS_ID%TYPE,
                               O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_CLEARANCE_HIST_MONTHS(O_clearance_hist_months IN OUT RPM_SYSTEM_OPTIONS.CLEARANCE_HIST_MONTHS%TYPE,
                                   O_error_message         IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_CLEAR_PROMO_OVERLAP_IND(O_clearance_promo_overlap_ind IN OUT RPM_SYSTEM_OPTIONS.CLEARANCE_PROMO_OVERLAP_IND%TYPE,
                                     O_error_message               IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_COMPLEX_PROMO_ALLOWED_IND(O_complex_promo_allowed_ind IN OUT RPM_SYSTEM_OPTIONS.COMPLEX_PROMO_ALLOWED_IND%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_COST_CALCULATION_METHOD(O_cost_calculation_method IN OUT RPM_SYSTEM_OPTIONS.COST_CALCULATION_METHOD%TYPE,
                                     O_error_message           IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEFAULT_OUT_OF_STOCK_DAYS(O_default_out_of_stock_days IN OUT RPM_SYSTEM_OPTIONS.DEFAULT_OUT_OF_STOCK_DAYS%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DEFAULT_RESET_DATE(O_default_reset_date IN OUT RPM_SYSTEM_OPTIONS.DEFAULT_RESET_DATE%TYPE,
                                O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_EVENT_ID_REQUIRED(O_event_id_required IN OUT RPM_SYSTEM_OPTIONS.EVENT_ID_REQUIRED%TYPE,
                               O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_MULTI_ITEM_LOC_PROMO_IND(O_multi_item_loc_promo_ind IN OUT RPM_SYSTEM_OPTIONS.MULTI_ITEM_LOC_PROMO_IND%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_OPEN_ZONE_USE(O_open_zone_use IN OUT RPM_SYSTEM_OPTIONS.OPEN_ZONE_USE%TYPE,
                           O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_PRICE_CHANGE_PROC_DAYS(O_price_change_processing_days IN OUT RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROCESSING_DAYS%TYPE,
                                    O_error_message                IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_PC_PROMO_OVERLAP_IND(O_pc_promo_overlap_ind IN OUT RPM_SYSTEM_OPTIONS.PRICE_CHANGE_PROMO_OVERLAP_IND%TYPE,
                                  O_error_message        IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_PROMO_APPLY_ORDER(O_promo_apply_order IN OUT RPM_SYSTEM_OPTIONS.PROMO_APPLY_ORDER%TYPE,
                               O_error_message     IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_PROMOTION_HIST_MONTHS(O_promotion_hist_months IN OUT RPM_SYSTEM_OPTIONS.PROMOTION_HIST_MONTHS%TYPE,
                                   O_error_message         IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_REJECT_HOLD_DAYS_PC_CLEAR(O_reject_hold_days_pc_clear IN OUT RPM_SYSTEM_OPTIONS.REJECT_HOLD_DAYS_PC_CLEAR%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_REJECT_HOLD_DAYS_PROMO(O_reject_hold_days_promo IN OUT RPM_SYSTEM_OPTIONS.REJECT_HOLD_DAYS_PROMO%TYPE,
                                    O_error_message          IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_RECOGNIZE_WH_AS_LOCATIONS(O_recognize_wh_as_locations IN OUT RPM_SYSTEM_OPTIONS.RECOGNIZE_WH_AS_LOCATIONS%TYPE,
                                       O_error_message             IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_SALES_CALCULATION_METHOD(O_sales_calculation_method IN OUT RPM_SYSTEM_OPTIONS.SALES_CALCULATION_METHOD%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_UPDATE_ITEM_ATTRIBUTES(O_update_item_attributes IN OUT RPM_SYSTEM_OPTIONS.UPDATE_ITEM_ATTRIBUTES%TYPE,
                                    O_error_message          IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_ZERO_CURR_ENDS_IN_DECS(O_zero_curr_ends_in_decs IN OUT RPM_SYSTEM_OPTIONS.ZERO_CURR_ENDS_IN_DECS%TYPE,
                                    O_error_message          IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_SIM_IND(O_sim_ind       IN OUT RPM_SYSTEM_OPTIONS.SIM_IND%TYPE,
                     O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_ZONE_RANGING(O_zone_ranging   IN OUT RPM_SYSTEM_OPTIONS.ZONE_RANGING%TYPE,
                          O_error_message  IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_EXACT_DEAL_DATES(O_exact_deal_dates IN OUT RPM_SYSTEM_OPTIONS.EXACT_DEAL_DATES%TYPE,
                              O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_LOC_MOVE_PROCESSING_DAYS(O_loc_move_processing_days IN OUT RPM_SYSTEM_OPTIONS.LOCATION_MOVE_PROCESSING_DAYS%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_LOCATION_MOVE_PURGE_DAYS(O_location_move_purge_days IN OUT RPM_SYSTEM_OPTIONS.LOCATION_MOVE_PURGE_DAYS%TYPE,
                                      O_error_message            IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_DYNAMIC_AREA_DIFF_IND(O_dynamic_area_diff_ind IN OUT RPM_SYSTEM_OPTIONS.DYNAMIC_AREA_DIFF_IND%TYPE,
                                   O_error_message         IN OUT VARCHAR2)
RETURN BOOLEAN;
------------------------------------------------------------------------
FUNCTION GET_LOCK_VERSION(O_lock_version  IN OUT RPM_SYSTEM_OPTIONS.LOCK_VERSION%TYPE,
                          O_error_message IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION GET_MAX_CLR_RECS(O_get_max_clr_recs IN OUT RPM_SYSTEM_OPTIONS.CLEARANCE_SEARCH_MAX%TYPE,
                          O_error_message    IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION GET_MAX_PC_RECS(O_get_max_pc_recs IN OUT RPM_SYSTEM_OPTIONS.PRICE_CHANGE_SEARCH_MAX%TYPE,
                         O_error_message   IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION GET_PAST_MKP_EVENTS_DISP_CNT(O_past_markup_events_disp_cnt IN OUT RPM_SYSTEM_OPTIONS.PAST_MARKUP_EVENTS_DISP_CNT%TYPE,
                                      O_error_message               IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION DO_NOT_RUN_CC_FOR_SUBMIT(O_skip_cc_for_submit IN OUT RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_SUBMIT%TYPE,
                                  O_error_message      IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
FUNCTION DO_NOT_RUN_CC_FOR_CP_APPROVAL(O_skip_cc_for_cp_approval IN OUT RPM_SYSTEM_OPTIONS.DO_NOT_RUN_CC_FOR_CP_APPROVAL%TYPE,
                                       O_error_message           IN OUT VARCHAR2)
RETURN BOOLEAN;
--------------------------------------------------------
END;
/

