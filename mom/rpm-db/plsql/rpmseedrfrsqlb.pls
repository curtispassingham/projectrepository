CREATE OR REPLACE PACKAGE BODY RPM_SEED_RFR_SQL AS
---------------------------------------------------------

   LP_vdate DATE := GET_VDATE;

   LP_dummy_hier_level VARCHAR2(3) := 'ABC';

---------------------------------------------------------
FUNCTION CREATE_PZ_TIMELINES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION CREATE_PL_TIMELINES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION CREATE_DZ_TIMELINES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION CREATE_DL_TIMELINES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION CREATE_IZ_TIMELINES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION CREATE_IL_TIMELINES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION PUSH_BACK(O_error_msg    OUT VARCHAR2)
RETURN NUMBER;
---------------------------------------------------------
FUNCTION INIT(O_error_msg        OUT VARCHAR2,
              O_max_thread_no    OUT NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(25) := 'RPM_SEED_RFR_SQL.INIT';

BEGIN

   -- Remove any items that are not sellable, approved transaction level items
   delete
     from rpm_dc_item_loc rdcil
    where NOT EXISTS (select 'x'
                        from item_master im
                       where rdcil.item      = im.item
                         and im.item_level   = im.tran_level
                         and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                         and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES);

   -- Remove any locs that are not valid in the system
   delete
     from rpm_dc_item_loc rdcil
    where NOT EXISTS (select 'x'
                        from (select store loc,
                                     RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                                from store
                              union all
                              select wh loc,
                                     RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                                from wh,
                                     rpm_system_options rso
                               where rso.recognize_wh_as_locations = 1) locs
                       where locs.loc      = rdcil.location
                         and locs.loc_type = rdcil.loc_type);

   -- Remove any records that don't exist on item_loc in RMS already

   delete
     from rpm_dc_item_loc rdcil
    where NOT EXISTS (select 'x'
                        from item_loc il
                       where rdcil.item        = il.item
                         and rdcil.location    = il.loc
                         and rdcil.loc_type    = il.loc_type
                         and rdcil.selling_uom = il.selling_uom);


   -- Remove any records that have invalid retail values
   delete
     from rpm_dc_item_loc
    where NVL(selling_unit_retail, -1) < 0;

   delete from rpm_mrde_tmp;

   insert into rpm_mrde_tmp
      (definition_level,
       dept,
       class,
       subclass,
       regular_zone_group,
       markup_calc_type,
       markup_percent,
       merch_retail_def_id)
      select definition_level,
             dept,
             class,
             subclass,
             regular_zone_group,
             markup_calc_type,
             markup_percent,
             merch_retail_def_id
        from rpm_merch_retail_def_expl;

   delete from rpm_subclass_tmp;

   insert into rpm_subclass_tmp (dept,
                                 class,
                                 subclass,
                                 thread_num)
      select dept,
             class,
             subclass,
             rownum thread_num
        from subclass
      order by dept,
               class,
               subclass;

   delete from rpm_zone_location_tmp;

   insert into rpm_zone_location_tmp (zone_group_id,
                                      zone_id,
                                      location)
      select rz.zone_group_id,
             rzl.zone_id,
             rzl.location
        from rpm_zone rz,
             rpm_zone_location rzl
       where rzl.zone_id = rz.zone_id;


   select MAX(thread_num)
     into O_max_thread_no
     from rpm_subclass_tmp;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

   return 0;

END INIT;

---------------------------------------------------------

FUNCTION PROCESS_THREAD(O_error_msg     OUT VARCHAR2,
                        I_thread_num IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_SEED_RFR_SQL.PROCESS_THREAD';

   L_rec_wh_as_store NUMBER := 0;

BEGIN

   select NVL(recognize_wh_as_locations, 0)
     into L_rec_wh_as_store
     from rpm_system_options;

   delete /*+ FULL(rfr_gtt) */
     from rpm_future_retail_gtt rfr_gtt;

   delete /*+ FULL(rfr_clean_gtt) */
     from rpm_dc_rfr_clean_gtt rfr_clean_gtt;

   insert into rpm_dc_rfr_clean_gtt
      (item,
       loc,
       loc_type,
       selling_unit_retail,
       selling_uom,
       currency_code,
       dept,
       class,
       subclass,
       diff_id,
       create_date,
       item_parent,
       zone_id,
       max_hier_level,
       multi_units,
       multi_unit_retail,
       multi_selling_uom)
      select t.item,
             t.location,
             t.loc_type,
             t.selling_unit_retail,
             t.selling_uom,
             t.currency_code,
             t.dept,
             t.class,
             t.subclass,
             t.diff_id,
             SYSDATE,
             t.item_parent,
             rzl.zone_id zone_id,
             DECODE(t.item_parent,
                    NULL, 'I',
                    'P')||
             DECODE(rzl.zone_id,
                    NULL, 'L',
                    'Z') max_hier_level,
             t.multi_units,
             t.multi_unit_retail,
             t.multi_selling_uom
        from (select /*+ ORDERED */
                     rdcil.item,
                     rdcil.location,
                     rdcil.loc_type,
                     rdcil.selling_unit_retail,
                     rdcil.selling_uom,
                     locs.currency_code,
                     im.item_parent,
                     im.dept,
                     im.class,
                     im.subclass,
                     case
                        when im.item_parent is NULL and
                             im.item_level = im.tran_level then
                           NULL
                        else
                           im.diff_1
                     end diff_id,
                     mrde.regular_zone_group,
                     rdcil.multi_units,
                     rdcil.multi_unit_retail,
                     rdcil.multi_selling_uom
                from rpm_subclass_tmp rst,
                     item_master im,
                     rpm_dc_item_loc rdcil,
                     rpm_mrde_tmp mrde,
                     (select store loc,
                             currency_code,
                             RPM_CONSTANTS.LOCATION_TYPE_STORE loc_type
                        from store
                      union all
                      select wh loc,
                             currency_code,
                             RPM_CONSTANTS.LOCATION_TYPE_WH loc_type
                        from wh
                       where L_rec_wh_as_store = 1) locs
               where rst.thread_num = I_thread_num
                 and im.dept        = rst.dept
                 and im.class       = rst.class
                 and im.subclass    = rst.subclass
                 and rdcil.item     = im.item
                 and mrde.dept      = rst.dept
                 and mrde.class     = rst.class
                 and mrde.subclass  = rst.subclass
                 and rdcil.location = locs.loc
                 and rdcil.loc_type = locs.loc_type
                 and rownum         > 0) t,
             rpm_zone_location_tmp rzl
       where t.regular_zone_group  = rzl.zone_group_id (+)
         and t.location            = rzl.location (+);

   if CREATE_PZ_TIMELINES(O_error_msg) = 0 then
      return 0;
   end if;

   if CREATE_PL_TIMELINES(O_error_msg) = 0 then
      return 0;
   end if;

   if CREATE_DZ_TIMELINES(O_error_msg) = 0 then
      return 0;
   end if;

   if CREATE_IZ_TIMELINES(O_error_msg) = 0 then
      return 0;
   end if;

   if CREATE_DL_TIMELINES(O_error_msg) = 0 then
      return 0;
   end if;

   if CREATE_IL_TIMELINES(O_error_msg) = 0 then
      return 0;
   end if;

   if PUSH_BACK(O_error_msg) = 0 then
      return 0;
   end if;

   -- Create the corresponding data on RPM_ITEM_LOC for everything that has been seeded on RFR
   insert into rpm_item_loc
      (dept,
       item,
       loc)
   select dept,
          item,
          loc
     from rpm_dc_rfr_clean_gtt;

   return 1;

EXCEPTION

   when OTHERS then
     O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END PROCESS_THREAD;

---------------------------------------------------------

FUNCTION CREATE_PZ_TIMELINES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.CREATE_PZ_TIMELINES';

BEGIN

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       max_hier_level,
       cur_hier_level)
   select -999999999,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          dept,
          class,
          subclass,
          item,
          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
          loc,
          LP_vdate - 1,
          -- price change...
          selling_unit_retail,
          currency_code,
          selling_uom,
          multi_units,
          multi_unit_retail,
          currency_code,
          multi_selling_uom,
          -- Clearance...
          selling_unit_retail,
          currency_code,
          selling_uom,
          -- simple promos...
          selling_unit_retail,
          currency_code,
          selling_uom,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_PARENT_ZONE
     from (select dept,
                  class,
                  subclass,
                  item_parent item,
                  zone_id loc,
                  selling_unit_retail,
                  selling_uom,
                  currency_code,
                  max_hier_level,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  retail_unit_curr_cnt,
                  ROW_NUMBER() OVER (PARTITION BY item_parent,
                                                  zone_id,
                                                  retail_unit_curr_cnt
                                         ORDER BY NULL) rn,
                  MAX(retail_unit_curr_cnt) OVER (PARTITION BY item_parent,
                                                               zone_id) mx
             from (select dept,
                          class,
                          subclass,
                          item_parent,
                          item,
                          zone_id,
                          selling_unit_retail,
                          selling_uom,
                          currency_code,
                          max_hier_level,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      zone_id,
                                                      selling_unit_retail,
                                                      selling_uom,
                                                      currency_code) retail_unit_curr_cnt
                     from rpm_dc_rfr_clean_gtt
                    where item_parent    is NOT NULL
                      and zone_id        is NOT NULL
                      and max_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE))
    where mx = retail_unit_curr_cnt
      and rn = 1;

   merge into rpm_dc_rfr_clean_gtt target
   using (select item,
                 location,
                 selling_retail,
                 selling_uom
            from rpm_future_retail_gtt
           where cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_ZONE) source
   on (    target.item_parent         = source.item
       and target.zone_id             = source.location
       and target.selling_unit_retail = source.selling_retail
       and target.selling_uom         = source.selling_uom)
   when MATCHED then
      update
         set max_hier_level = NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END CREATE_PZ_TIMELINES;

---------------------------------------------------------

FUNCTION CREATE_DZ_TIMELINES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.CREATE_DZ_TIMELINES';

BEGIN

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       diff_id,
       max_hier_level,
       cur_hier_level)
   select -999999999,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          dept,
          class,
          subclass,
          item,
          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
          loc,
          LP_vdate - 1,
          -- price change...
          selling_unit_retail,
          currency_code,
          selling_uom,
          multi_units,
          multi_unit_retail,
          currency_code,
          multi_selling_uom,
          -- Clearance...
          selling_unit_retail,
          currency_code,
          selling_uom,
          -- simple promos...
          selling_unit_retail,
          currency_code,
          selling_uom,
          diff_id,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_DIFF_ZONE
     from (select dept,
                  class,
                  subclass,
                  item_parent item,
                  zone_id loc,
                  diff_id,
                  selling_unit_retail,
                  selling_uom,
                  currency_code,
                  max_hier_level,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  cnt_by_unseeded,
                  cnt_by_diff_zone,
                  ROW_NUMBER() OVER (PARTITION BY item_parent,
                                                  diff_id,
                                                  zone_id,
                                                  NVL(max_hier_level, LP_dummy_hier_level)
                                         ORDER BY NULL) rn
             from (select dept,
                          class,
                          subclass,
                          item_parent,
                          zone_id,
                          selling_unit_retail,
                          selling_uom,
                          currency_code,
                          max_hier_level,
                          diff_id,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      diff_id,
                                                      zone_id,
                                                      NVL(max_hier_level, LP_dummy_hier_level),
                                                      selling_unit_retail,
                                                      selling_uom,
                                                      currency_code) cnt_by_unseeded,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      diff_id,
                                                      zone_id) cnt_by_diff_zone
                     from rpm_dc_rfr_clean_gtt
                    where item_parent is NOT NULL
                      and zone_id     is NOT NULL
                      and diff_id     is NOT NULL))
    where cnt_by_unseeded = cnt_by_diff_zone
      and max_hier_level  is NOT NULL
      and rn              = 1;

   merge into rpm_dc_rfr_clean_gtt target
   using (select item,
                 diff_id,
                 location,
                 selling_retail,
                 selling_uom
            from rpm_future_retail_gtt
           where cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE) source
   on (    target.item_parent         = source.item
       and target.diff_id             = source.diff_id
       and target.zone_id             = source.location
       and target.selling_unit_retail = source.selling_retail
       and target.selling_uom         = source.selling_uom)
   when MATCHED then
      update
         set max_hier_level = NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END CREATE_DZ_TIMELINES;

---------------------------------------------------------

FUNCTION CREATE_PL_TIMELINES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.CREATE_PL_TIMELINES';

BEGIN

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       zone_id,
       max_hier_level,
       cur_hier_level)
   select -999999999,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          dept,
          class,
          subclass,
          item,
          loc_type,
          loc,
          LP_vdate - 1,
          -- price change...
          selling_unit_retail,
          currency_code,
          selling_uom,
          multi_units,
          multi_unit_retail,
          currency_code,
          multi_selling_uom,
          -- Clearance...
          selling_unit_retail,
          currency_code,
          selling_uom,
          -- simple promos...
          selling_unit_retail,
          currency_code,
          selling_uom,
          zone_id,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_PARENT_LOC
     from (select dept,
                  class,
                  subclass,
                  item_parent item,
                  loc,
                  loc_type,
                  zone_id,
                  selling_unit_retail,
                  selling_uom,
                  currency_code,
                  max_hier_level,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  cnt_by_unseeded,
                  cnt_by_parent_loc,
                  ROW_NUMBER() OVER (PARTITION BY item_parent,
                                                  loc,
                                                  loc_type,
                                                  NVL(max_hier_level, LP_dummy_hier_level)
                                         ORDER BY NULL) rn
             from (select dept,
                          class,
                          subclass,
                          item_parent,
                          zone_id,
                          loc,
                          DECODE(loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) loc_type,
                          selling_unit_retail,
                          selling_uom,
                          currency_code,
                          max_hier_level,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      loc,
                                                      loc_type,
                                                      NVL(max_hier_level, LP_dummy_hier_level),
                                                      selling_unit_retail,
                                                      selling_uom,
                                                      currency_code) cnt_by_unseeded,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      loc) cnt_by_parent_loc
                     from rpm_dc_rfr_clean_gtt
                    where item_parent is NOT NULL))
    where cnt_by_unseeded = cnt_by_parent_loc
      and max_hier_level  is NOT NULL
      and rn              = 1;

   merge into rpm_dc_rfr_clean_gtt target
   using (select item,
                 location,
                 DECODE(zone_node_type,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                 selling_retail,
                 selling_uom
            from rpm_future_retail_gtt
           where cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC) source
   on (    target.item_parent         = source.item
       and target.loc                 = source.location
       and target.loc_type            = source.loc_type
       and target.selling_unit_retail = source.selling_retail
       and target.selling_uom         = source.selling_uom)
   when MATCHED then
      update
         set max_hier_level = NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END CREATE_PL_TIMELINES;

---------------------------------------------------------

FUNCTION CREATE_DL_TIMELINES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.CREATE_DL_TIMELINES';

BEGIN

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       diff_id,
       zone_id,
       max_hier_level,
       cur_hier_level)
   select -999999999,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          dept,
          class,
          subclass,
          item,
          loc_type,
          loc,
          LP_vdate - 1,
          -- price change...
          selling_unit_retail,
          currency_code,
          selling_uom,
          multi_units,
          multi_unit_retail,
          currency_code,
          multi_selling_uom,
          -- Clearance...
          selling_unit_retail,
          currency_code,
          selling_uom,
          -- simple promos...
          selling_unit_retail,
          currency_code,
          selling_uom,
          diff_id,
          zone_id,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_DIFF_LOC
     from (select dept,
                  class,
                  subclass,
                  item_parent item,
                  diff_id,
                  loc,
                  loc_type,
                  zone_id,
                  selling_unit_retail,
                  selling_uom,
                  currency_code,
                  max_hier_level,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  cnt_by_unseeded,
                  cnt_by_diff_loc,
                  ROW_NUMBER() OVER (PARTITION BY item_parent,
                                                  diff_id,
                                                  loc,
                                                  loc_type,
                                                  NVL(max_hier_level, LP_dummy_hier_level)
                                         ORDER BY NULL) rn
             from (select dept,
                          class,
                          subclass,
                          item_parent,
                          diff_id,
                          zone_id,
                          loc,
                          DECODE(loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) loc_type,
                          selling_unit_retail,
                          selling_uom,
                          currency_code,
                          max_hier_level,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      diff_id,
                                                      loc,
                                                      loc_type,
                                                      NVL(max_hier_level, LP_dummy_hier_level),
                                                      selling_unit_retail,
                                                      selling_uom,
                                                      currency_code) cnt_by_unseeded,
                          COUNT(1) OVER (PARTITION BY item_parent,
                                                      diff_id,
                                                      loc,
                                                      loc_type) cnt_by_diff_loc
                     from rpm_dc_rfr_clean_gtt
                    where item_parent is NOT NULL
                      and diff_id     is NOT NULL))
    where cnt_by_unseeded = cnt_by_diff_loc
      and max_hier_level  is NOT NULL
      and rn              = 1;

   merge into rpm_dc_rfr_clean_gtt target
   using (select item,
                 diff_id,
                 location,
                 DECODE(zone_node_type,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                 selling_retail,
                 selling_uom
            from rpm_future_retail_gtt
           where cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC) source
   on (    target.item_parent         = source.item
       and target.diff_id             = source.diff_id
       and target.loc                 = source.location
       and target.loc_type            = source.loc_type
       and target.selling_unit_retail = source.selling_retail
       and target.selling_uom         = source.selling_uom)
   when MATCHED then
      update
         set max_hier_level = NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END CREATE_DL_TIMELINES;

---------------------------------------------------------

FUNCTION CREATE_IZ_TIMELINES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.CREATE_IZ_TIMELINES';

BEGIN

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       item_parent,
       diff_id,
       max_hier_level,
       cur_hier_level)
   select -999999999,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          dept,
          class,
          subclass,
          item,
          RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE,
          loc,
          LP_vdate - 1,
          -- price change...
          selling_unit_retail,
          currency_code,
          selling_uom,
          multi_units,
          multi_unit_retail,
          currency_code,
          multi_selling_uom,
          -- Clearance...
          selling_unit_retail,
          currency_code,
          selling_uom,
          -- simple promos...
          selling_unit_retail,
          currency_code,
          selling_uom,
          item_parent,
          diff_id,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_ITEM_ZONE
     from (select dept,
                  class,
                  subclass,
                  item,
                  item_parent,
                  diff_id,
                  zone_id loc,
                  selling_unit_retail,
                  selling_uom,
                  currency_code,
                  max_hier_level,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  cnt_by_unseeded,
                  cnt_by_item_zone,
                  ROW_NUMBER() OVER (PARTITION BY item,
                                                  zone_id,
                                                  NVL(max_hier_level, LP_dummy_hier_level)
                                         ORDER BY NULL) rn
             from (select dept,
                          class,
                          subclass,
                          item_parent,
                          item,
                          diff_id,
                          zone_id,
                          selling_unit_retail,
                          selling_uom,
                          currency_code,
                          max_hier_level,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          COUNT(1) OVER (PARTITION BY item,
                                                      zone_id,
                                                      NVL(max_hier_level, LP_dummy_hier_level),
                                                      selling_unit_retail,
                                                      selling_uom,
                                                      currency_code) cnt_by_unseeded,
                          COUNT(1) OVER (PARTITION BY item,
                                                      zone_id) cnt_by_item_zone
                     from rpm_dc_rfr_clean_gtt
                    where zone_id is NOT NULL))
    where cnt_by_unseeded = cnt_by_item_zone
      and max_hier_level  is NOT NULL
      and rn              = 1;

   merge into rpm_dc_rfr_clean_gtt target
   using (select item,
                 location,
                 selling_retail,
                 selling_uom
            from rpm_future_retail_gtt
           where cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE) source
   on (    target.item                = source.item
       and target.zone_id             = source.location
       and target.selling_unit_retail = source.selling_retail
       and target.selling_uom         = source.selling_uom)
   when MATCHED then
      update
         set max_hier_level = NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END CREATE_IZ_TIMELINES;

---------------------------------------------------------

FUNCTION CREATE_IL_TIMELINES(O_error_msg     OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.CREATE_IL_TIMELINES';

BEGIN

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       item_parent,
       diff_id,
       zone_id,
       max_hier_level,
       cur_hier_level)
   select -999999999,
          RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
          dept,
          class,
          subclass,
          item,
          loc_type,
          loc,
          LP_vdate - 1,
          -- price change...
          selling_unit_retail,
          currency_code,
          selling_uom,
          multi_units,
          multi_unit_retail,
          currency_code,
          multi_selling_uom,
          -- Clearance...
          selling_unit_retail,
          currency_code,
          selling_uom,
          -- simple promos...
          selling_unit_retail,
          currency_code,
          selling_uom,
          item_parent,
          diff_id,
          zone_id,
          max_hier_level,
          RPM_CONSTANTS.FR_HIER_ITEM_LOC
     from (select dept,
                  class,
                  subclass,
                  item,
                  item_parent,
                  diff_id,
                  loc,
                  loc_type,
                  zone_id,
                  selling_unit_retail,
                  selling_uom,
                  currency_code,
                  max_hier_level,
                  multi_units,
                  multi_unit_retail,
                  multi_selling_uom,
                  cnt_by_unseeded,
                  cnt_by_item_loc,
                  ROW_NUMBER() OVER (PARTITION BY item,
                                                  loc,
                                                  loc_type,
                                                  NVL(max_hier_level, LP_dummy_hier_level)
                                         ORDER BY NULL) rn
             from (select dept,
                          class,
                          subclass,
                          item_parent,
                          item,
                          diff_id,
                          zone_id,
                          loc,
                          DECODE(loc_type,
                                 RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                 RPM_CONSTANTS.LOCATION_TYPE_WH, RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) loc_type,
                          selling_unit_retail,
                          selling_uom,
                          currency_code,
                          max_hier_level,
                          multi_units,
                          multi_unit_retail,
                          multi_selling_uom,
                          COUNT(1) OVER (PARTITION BY item,
                                                      loc,
                                                      loc_type,
                                                      NVL(max_hier_level, LP_dummy_hier_level),
                                                      selling_unit_retail,
                                                      selling_uom,
                                                      currency_code) cnt_by_unseeded,
                          COUNT(1) OVER (PARTITION BY item,
                                                      loc,
                                                      loc_type) cnt_by_item_loc
                     from rpm_dc_rfr_clean_gtt))
    where cnt_by_unseeded = cnt_by_item_loc
      and max_hier_level  is NOT NULL
      and rn              = 1;

   merge into rpm_dc_rfr_clean_gtt target
   using (select item,
                 location,
                 DECODE(zone_node_type,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, RPM_CONSTANTS.LOCATION_TYPE_STORE,
                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE, RPM_CONSTANTS.LOCATION_TYPE_WH) loc_type,
                 selling_retail,
                 selling_uom
            from rpm_future_retail_gtt
           where cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC) source
   on (    target.item                = source.item
       and target.loc                 = source.location
       and target.loc_type            = source.loc_type
       and target.selling_unit_retail = source.selling_retail
       and target.selling_uom         = source.selling_uom)
   when MATCHED then
      update
         set max_hier_level = NULL;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END CREATE_IL_TIMELINES;

---------------------------------------------------------

FUNCTION PUSH_BACK(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.PUSH_BACK';

BEGIN

   insert /*+ APPEND */ into rpm_future_retail
      (future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       on_simple_promo_ind,
       on_complex_promo_ind,
       item_parent,
       diff_id,
       zone_id,
       max_hier_level,
       cur_hier_level)
      select future_retail_id,
             dept,
             class,
             subclass,
             item,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             NVL(on_simple_promo_ind, 0),
             NVL(on_complex_promo_ind, 0),
             item_parent,
             diff_id,
             zone_id,
             max_hier_level,
             cur_hier_level
        from rpm_future_retail_gtt;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END PUSH_BACK;

---------------------------------------------------------

FUNCTION TRUNC_RPM_DC_ITEM_LOC(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.TRUNC_RPM_DC_ITEM_LOC';

   L_truncate_rpm_dc_item_loc VARCHAR2(100) := 'truncate table rpm_dc_item_loc';

BEGIN

   EXECUTE IMMEDIATE L_truncate_rpm_dc_item_loc;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END TRUNC_RPM_DC_ITEM_LOC;

---------------------------------------------------------

FUNCTION TRUNC_TMP_TABLES(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_SEED_RFR_SQL.TRUNC_TMP_TABLES';

   L_truncate_rpm_sc_tmp   VARCHAR2(100) := 'truncate table rpm_subclass_tmp';
   L_truncate_rpm_zl_tmp   VARCHAR2(100) := 'truncate table rpm_zone_location_tmp';
   L_truncate_rpm_mrde_tmp VARCHAR2(100) := 'truncate table rpm_mrde_tmp';

BEGIN

   EXECUTE IMMEDIATE L_truncate_rpm_sc_tmp;
   EXECUTE IMMEDIATE L_truncate_rpm_zl_tmp;
   EXECUTE IMMEDIATE L_truncate_rpm_mrde_tmp;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

       return 0;

END TRUNC_TMP_TABLES;

---------------------------------------------------------
END RPM_SEED_RFR_SQL;
/

