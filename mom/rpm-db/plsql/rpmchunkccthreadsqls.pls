CREATE OR REPLACE PACKAGE RPM_CHUNK_CC_THREADING_SQL AS
--------------------------------------------------------------------------------
FUNCTION INIT_CHUNKS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                     I_bulk_cc_pe_id    IN     NUMBER,
                     I_pe_sequence_id   IN     NUMBER,
                     I_pe_thread_number IN     NUMBER,
                     I_transaction_id   IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION CHUNK_THREAD_ITEM_LOCS(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                O_max_chunk           OUT NUMBER,
                                I_bulk_cc_pe_id    IN     NUMBER,
                                I_pe_sequence_id   IN     NUMBER,
                                I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CHUNK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                       I_bulk_cc_pe_id    IN     NUMBER,
                       I_pe_sequence_id   IN     NUMBER,
                       I_pe_thread_number IN     NUMBER,
                       I_pe_chunk_number  IN     NUMBER,
                       I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_THREAD(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                        O_purge_ws_tables     OUT NUMBER,
                        I_bulk_cc_pe_id    IN     NUMBER,
                        I_pe_sequence_id   IN     NUMBER,
                        I_pe_thread_number IN     NUMBER,
                        I_price_event_type IN     VARCHAR2,
                        I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PUSH_BACK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                   I_bulk_cc_pe_id    IN     NUMBER,
                   I_pe_sequence_id   IN     NUMBER,
                   I_thread_number    IN     NUMBER,
                   I_chunk_number     IN     NUMBER,
                   I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION UPDATE_CHUNK_STATUS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                             I_bulk_cc_pe_id        IN     NUMBER,
                             I_parent_thread_number IN     NUMBER,
                             I_pe_thread_number     IN     NUMBER,
                             I_pe_chunk_number      IN     NUMBER,
                             I_status               IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PURGE_CHUNK_WS_TABLES(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                               I_bulk_cc_pe_id        IN     NUMBER    DEFAULT NULL,
                               I_parent_thread_number IN     NUMBER    DEFAULT NULL,
                               I_thread_number        IN     NUMBER    DEFAULT NULL,
                               I_pending_cc_pe_ids    IN     OBJ_NUMERIC_ID_TABLE DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_PUSH_BACK_THREAD(O_cc_error_tbl             OUT CONFLICT_CHECK_ERROR_TBL,
                                  O_purge_ws_tables          OUT NUMBER,
                                  O_post_push_back_failed    OUT NUMBER,
                                  I_bulk_cc_pe_id         IN     NUMBER,
                                  I_pe_sequence_id        IN     NUMBER,
                                  I_pe_thread_number      IN     NUMBER,
                                  I_price_event_type      IN     VARCHAR2,
                                  I_rib_trans_id          IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_PUSH_BACK_CHUNK(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_bulk_cc_pe_id    IN     NUMBER,
                                 I_pe_sequence_id   IN     NUMBER,
                                 I_pe_thread_number IN     NUMBER,
                                 I_chunk_number     IN     NUMBER,
                                 I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION PROCESS_PE_POST_PUSH_BACK(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                   I_bulk_cc_pe_id    IN     NUMBER,
                                   I_pe_sequence_id   IN     NUMBER,
                                   I_pe_thread_number IN     NUMBER,
                                   I_rib_trans_id     IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION RESET_PE_PUSH_BACK_FAILURE (IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                                     I_bulk_cc_pe_id    IN     NUMBER,
                                     I_pe_sequence_id   IN     NUMBER,
                                     I_pe_thread_number IN     NUMBER,
                                     I_rib_trans_id     IN     NUMBER,
                                     I_chunk_number     IN     NUMBER DEFAULT NULL)
RETURN NUMBER;
-----------------------------------------------------------------------------------
FUNCTION ROLL_BACK(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                   I_price_event_type     IN     VARCHAR2,
                   I_bulk_cc_pe_id        IN     NUMBER,
                   I_price_event_id       IN     NUMBER,
                   I_thread_number        IN     NUMBER,
                   I_parent_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_CHUNK_CC_THREADING_SQL;
/
