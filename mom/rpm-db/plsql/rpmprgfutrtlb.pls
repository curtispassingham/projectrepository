CREATE OR REPLACE PACKAGE BODY RPM_PURGE_FUTURE_RETAIL_SQL AS
--------------------------------------------------------------------------------

LP_purge_rfr_batch CONSTANT VARCHAR2(45) := 'com.retek.rpm.batch.PurgeFutureRetailBatch';

--------------------------------------------------------------------------------
FUNCTION PURGE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_PURGE_FUTURE_RETAIL_SQL.PURGE';

   L_clearance_hist_months NUMBER := 0;
   L_promotion_hist_months NUMBER := 0;

   L_vdate                PERIOD.VDATE%TYPE := GET_VDATE;
   L_clearance_purge_date DATE              := NULL;
   L_promotion_purge_date DATE              := NULL;
   L_error_msg            VARCHAR2(255)     := NULL;

BEGIN

   delete rpm_pc_rec_gtt;

   select clearance_hist_months,
          promotion_hist_months
     into L_clearance_hist_months,
          L_promotion_hist_months
     from rpm_system_options;

   -- Get clearance Purge Date
   L_clearance_purge_date := ADD_MONTHS(L_vdate,
                                        -L_clearance_hist_months);

   -- get Promotion Purge date
   L_promotion_purge_date := ADD_MONTHS(L_vdate,
                                        -L_promotion_hist_months);

   insert into rpm_pc_rec_gtt
      (price_change_id,
       item,
       diff_id,
       location,
       zone_node_type,
       effective_date)
      select price_event_id,
             item,
             diff_id,
             location,
             zone_node_type,
             promo_date
        from (select price_event_id,
                     item,
                     diff_id,
                     location,
                     zone_node_type,
                     promo_date,
                     ROW_NUMBER() OVER (PARTITION BY price_event_id,
                                                     item,
                                                     NVL(diff_id, '-999'),
                                                     location,
                                                     zone_node_type
                                            ORDER BY promo_date desc) rank
                from (select cl.price_event_id,
                             cl.item,
                             cl.diff_id,
                             cl.location,
                             cl.zone_node_type,
                             cl.clear_date,
                             ilex.promo_dtl_id,
                             ilex.detail_start_date,
                             ilex.detail_end_date,
                             case
                                when ilex.promo_dtl_id is NULL then
                                   cl.clear_date
                                when ilex.detail_end_date < cl.clear_date and
                                     ilex.detail_end_date < L_promotion_purge_date then
                                   ilex.detail_end_date + RPM_CONSTANTS.ONE_MINUTE
                                when ilex.detail_end_date < cl.clear_date and
                                     ilex.detail_end_date >= L_promotion_purge_date and
                                     ilex.detail_start_date >= L_promotion_purge_date then
                                   L_promotion_purge_date
                                when ilex.detail_end_date < cl.clear_date and
                                     ilex.detail_end_date >= L_promotion_purge_date and
                                     ilex.detail_start_date < L_promotion_purge_date then
                                   ilex.detail_start_date
                                when cl.clear_date >= ilex.detail_start_date and
                                     cl.clear_date <= NVL(ilex.detail_end_date, TO_DATE('31-DEC-3000', 'DD-MON-YYYY')) and
                                     ilex.detail_start_date >= L_promotion_purge_date then
                                   L_promotion_purge_date
                                when cl.clear_date >= ilex.detail_start_date and
                                     cl.clear_date <= NVL(ilex.detail_end_date, TO_DATE('31-DEC-3000', 'DD-MON-YYYY')) and
                                     ilex.detail_start_date < L_promotion_purge_date then
                                   ilex.detail_start_date
                                else
                                   case
                                      when ilex.detail_start_date < cl.clear_date and
                                           ilex.detail_start_date < L_promotion_purge_date then
                                         ilex.detail_start_date
                                      when ilex.detail_start_date < cl.clear_date and
                                           ilex.detail_start_date >= L_promotion_purge_date then
                                         L_promotion_purge_date
                                      when cl.clear_date < L_promotion_purge_date then
                                         cl.clear_date
                                      else
                                         L_promotion_purge_date
                                   end
                             end promo_date
                        from rpm_promo_item_loc_expl_gtt ilex,
                             (select price_event_id,
                                     item,
                                     diff_id,
                                     location,
                                     zone_node_type,
                                     action_date,
                                     pc_date,
                                     clear_date
                                from (select price_event_id,
                                             item,
                                             diff_id,
                                             location,
                                             zone_node_type,
                                             action_date,
                                             pc_date,
                                             case
                                                when pc_date < clear_date then
                                                   pc_date
                                                else
                                                   clear_date
                                             end clear_date,
                                             ROW_NUMBER() OVER (PARTITION BY price_event_id,
                                                                             item,
                                                                             NVL(diff_id, '-999'),
                                                                             location,
                                                                             zone_node_type
                                                                    ORDER BY action_date desc) rank
                                        from (select price_event_id,
                                                     item,
                                                     diff_id,
                                                     location,
                                                     zone_node_type,
                                                     action_date,
                                                     clearance_id,
                                                     clear_start_ind,
                                                     pc_date,
                                                     MIN(clear_date) OVER (PARTITION BY price_event_id,
                                                                                        item,
                                                                                        NVL(diff_id, '-999'),
                                                                                        location,
                                                                                        zone_node_type) clear_date
                                                from (select price_event_id,
                                                             item,
                                                             diff_id,
                                                             location,
                                                             zone_node_type,
                                                             action_date,
                                                             clearance_id,
                                                             clear_start_ind,
                                                             pc_date,
                                                             case
                                                                when clearance_id is NOT NULL and
                                                                     action_date >= L_clearance_purge_date then
                                                                   action_date
                                                                else
                                                                   pc_date
                                                             end clear_date
                                                        from (select price_event_id,
                                                                     item,
                                                                     diff_id,
                                                                     location,
                                                                     zone_node_type,
                                                                     action_date,
                                                                     clearance_id,
                                                                     clear_start_ind,
                                                                     MAX(action_date) OVER (PARTITION BY price_event_id,
                                                                                                         item,
                                                                                                         NVL(diff_id, '-999'),
                                                                                                         location,
                                                                                                         zone_node_type
                                                                                                ORDER BY action_date desc) pc_date
                                                                from rpm_future_retail_gtt rfr
                                                               where action_date < L_vdate))))
                               where rank = 1) cl
                       where cl.price_event_id          = ilex.price_event_id (+)
                         and cl.item                    = ilex.item (+)
                         and NVL(cl.diff_id, '-999')    = NVL(ilex.diff_id (+), '-999')
                         and cl.location                = ilex.location (+)
                         and cl.zone_node_type          = ilex.zone_node_type (+)
                         and ilex.detail_start_date (+) < L_vdate
                         and L_vdate                    > NVL(ilex.detail_end_date, TO_DATE('31-DEC-3000', 'DD-MON-YYYY'))))

       where rank = 1;

   merge into rpm_future_retail_gtt target
   using (select price_change_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 effective_date
            from rpm_pc_rec_gtt
           order by price_change_id,
                    item,
                    location) source
   on (    target.price_event_id       = source.price_change_id
       and target.item                 = source.item
       and target.location             = source.location
       and NVL(target.diff_id, '-999') = NVL(source.diff_id, '-999')
       and target.zone_node_type       = source.zone_node_type
       and target.action_date          < source.effective_date)
   when MATCHED then
      update
         set target.timeline_seq    = -999,
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RFR_PURGE;

   merge into rpm_promo_item_loc_expl_gtt target
   using (select price_change_id,
                 item,
                 diff_id,
                 location,
                 zone_node_type,
                 effective_date
            from rpm_pc_rec_gtt
           order by price_change_id,
                    item,
                    location) source
   on (    target.price_event_id            = source.price_change_id
       and target.item                      = source.item
       and target.location                  = source.location
       and NVL(target.diff_id, '-999')      = NVL(source.diff_id, '-999')
       and NVL(target.zone_node_type, -999) = NVL(source.zone_node_type, -999)
       and target.detail_start_date         < source.effective_date
       and L_vdate                          > NVL(target.detail_end_date, TO_DATE('31-DEC-3000', 'DD-MON-YYYY')))
   when MATCHED then
      update
         set target.deleted_ind     = 1,
             target.step_identifier = RPM_CONSTANTS.CAPT_GTT_RFR_PURGE;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_msg,
                                                   RPM_CONSTANTS.CAPT_GTT_RFR_PURGE,
                                                   1, -- RFR
                                                   1, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_msg));
      return 0;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      L_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));

      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_msg));
      return 0;

END PURGE;
-----------------------------------------------------------------------------
FUNCTION THREAD_PURGE_DATA(O_error_msg            OUT VARCHAR2,
                           O_max_thread_number    OUT NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_PURGE_FUTURE_RETAIL_SQL.THREAD_PURGE_DATA';

   L_bookmark_value     RPM_BATCH_BOOKMARK.BOOKMARK_VALUE%TYPE := NULL;
   L_dept_bookmark      SUBCLASS.DEPT%TYPE                     := NULL;
   L_class_bookmark     SUBCLASS.CLASS%TYPE                    := NULL;
   L_subclass_bookmark  SUBCLASS.SUBCLASS%TYPE                 := NULL;
   L_first_colon        NUMBER                                 := NULL;
   L_second_colon       NUMBER                                 := NULL;
   L_temp_max_thread_no NUMBER(15)                             := NULL;

BEGIN

   select bookmark_value
     into L_bookmark_value
     from rpm_batch_bookmark
    where batch_name = LP_purge_rfr_batch;

   if L_bookmark_value is NOT NULL then
      -- get the dept, class and subclass values based on the bookmark value
      -- the bookmark value is separated by colon, for example: '1234:12:34'
      -- get the 1st position and 2nd position of the colon then get the actual values
      -- of the merch hierarchy values based on the positions of the string separator
      L_first_colon       := INSTR(L_bookmark_value, ':');
      L_second_colon      := INSTR(L_bookmark_value, ':', 1, 2);
      L_dept_bookmark     := TO_NUMBER(SUBSTR(L_bookmark_value, 1, L_first_colon - 1));
      L_class_bookmark    := TO_NUMBER(SUBSTR(L_bookmark_value, L_first_colon + 1, L_second_colon - L_first_colon - 1));
      L_subclass_bookmark := TO_NUMBER(SUBSTR(L_bookmark_value, L_second_colon + 1));

      -- to start on the bookmark and those dept/class/subclass combination greater than
      -- the current bookmark
      insert into rpm_rfr_purge_thread (dept,
                                        class,
                                        subclass,
                                        thread_number,
                                        status)
         select dept,
                class,
                subclass,
                ROW_NUMBER() OVER (ORDER BY dept,
                                            class,
                                            subclass),
                'N'
           from subclass
          where dept          > L_dept_bookmark
             or (    dept     = L_dept_bookmark
                 and class    > L_class_bookmark)
             or (    dept     = L_dept_bookmark
                 and class    = L_class_bookmark
                 and subclass > L_subclass_bookmark);

      select NVL(MAX(thread_number), 0)
        into L_temp_max_thread_no
        from rpm_rfr_purge_thread;

      -- to start at the least dept/class/subclass combination up to the current bookmark
      insert into rpm_rfr_purge_thread (dept,
                                        class,
                                        subclass,
                                        thread_number,
                                        status)
         select dept,
                class,
                subclass,
                ROW_NUMBER() OVER (ORDER BY dept,
                                            class,
                                            subclass) + L_temp_max_thread_no,
                'N'
           from subclass
          where dept          <  L_dept_bookmark
             or (    dept      = L_dept_bookmark
                 and class    <  L_class_bookmark)
             or (    dept      = L_dept_bookmark
                 and class     = L_class_bookmark
                 and subclass <= L_subclass_bookmark);

   else
      insert into rpm_rfr_purge_thread (dept,
                                        class,
                                        subclass,
                                        thread_number,
                                        status)
         select dept,
                class,
                subclass,
                ROW_NUMBER() OVER (ORDER BY dept,
                                            class,
                                            subclass),
                'N'
           from subclass;
   end if;

   select MAX(thread_number)
     into O_max_thread_number
     from rpm_rfr_purge_thread;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END THREAD_PURGE_DATA;
-----------------------------------------------------------------------------
FUNCTION PROCESS_PURGE_THREAD(O_error_msg        OUT VARCHAR2,
                              I_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program      VARCHAR2(50)             := 'RPM_PURGE_FUTURE_RETAIL_SQL.PROCESS_PURGE_THREAD';
   L_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := NULL;

   L_dept           SUBCLASS.DEPT%TYPE     := NULL;
   L_class          SUBCLASS.CLASS%TYPE    := NULL;
   L_subclass       SUBCLASS.SUBCLASS%TYPE := NULL;
   L_price_event_id NUMBER(15)             := -9999999999;

BEGIN

   select dept,
          class,
          subclass
     into L_dept,
          L_class,
          L_subclass
     from rpm_rfr_purge_thread
    where thread_number = I_thread_number;

   insert into rpm_future_retail_gtt
      (price_event_id,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       lock_version,
       rfr_rowid,
       on_simple_promo_ind,
       on_complex_promo_ind,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
      select L_price_event_id,
             future_retail_id,
             dept,
             class,
             subclass,
             item,
             zone_node_type,
             location,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             rowid  rfr_rowid,
             on_simple_promo_ind,
             on_complex_promo_ind,
             item_parent,
             diff_id,
             zone_id,
             cur_hier_level,
             max_hier_level
        from rpm_future_retail
       where dept     = L_dept
         and class    = L_class
         and subclass = L_subclass;

   insert into rpm_promo_item_loc_expl_gtt
      (price_event_id,
       promo_item_loc_expl_id,
       item,
       dept,
       class,
       subclass,
       location,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       promo_comp_id,
       comp_display_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       exception_parent_id,
       rpile_rowid,
       zone_node_type,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select L_price_event_id,
          promo_item_loc_expl_id,
          item,
          dept,
          class,
          subclass,
          location,
          promo_id,
          promo_display_id,
          promo_secondary_ind,
          promo_comp_id,
          comp_display_id,
          promo_dtl_id,
          type,
          customer_type,
          detail_secondary_ind,
          detail_start_date,
          detail_end_date,
          detail_apply_to_code,
          detail_change_type,
          detail_change_amount,
          detail_change_currency,
          detail_change_percent,
          detail_change_selling_uom,
          detail_price_guide_id,
          exception_parent_id,
          rowid rpile_rowid,
          zone_node_type,
          item_parent,
          diff_id,
          zone_id,
          cur_hier_level,
          max_hier_level
     from rpm_promo_item_loc_expl
    where dept     = L_dept
      and class    = L_class
      and subclass = L_subclass;

   insert into rpm_cust_segment_promo_fr_gtt
      (price_event_id,
       cust_segment_promo_id,
       item,
       zone_node_type,
       location,
       action_date,
       customer_type,
       dept,
       promo_retail,
       promo_retail_currency,
       promo_uom,
       complex_promo_ind,
       cspfr_rowid,
       item_parent,
       diff_id,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select L_price_event_id,
          cspfr.cust_segment_promo_id,
          cspfr.item,
          cspfr.zone_node_type,
          cspfr.location,
          cspfr.action_date,
          cspfr.customer_type,
          cspfr.dept,
          cspfr.promo_retail,
          cspfr.promo_retail_currency,
          cspfr.promo_uom,
          cspfr.complex_promo_ind,
          cspfr.rowid  cspfr_rowid,
          cspfr.item_parent,
          cspfr.diff_id,
          cspfr.zone_id,
          cspfr.cur_hier_level,
          cspfr.max_hier_level
     from (select t.item,
                  t.location
             from (select gtt.item,
                          gtt.location,
                          RANK() OVER (PARTITION BY gtt.item,
                                                    gtt.location
                                           ORDER BY gtt.action_date desc) rank
                     from rpm_future_retail_gtt gtt) t
            where t.rank = 1) fr,
          rpm_cust_segment_promo_fr cspfr
    where cspfr.dept     = L_dept
      and cspfr.item     = fr.item
      and cspfr.location = fr.location;

   if PURGE(L_cc_error_tbl) = 0 then
      O_error_msg := L_cc_error_tbl(1).error_string;
      return 0;
   end if;

   if RPM_FUTURE_RETAIL_SQL.PUSH_BACK_PURGE(L_cc_error_tbl) = 0 then
      O_error_msg := L_cc_error_tbl(1).error_string;
      return 0;
   end if;

   update rpm_rfr_purge_thread
      set status = 'C'
    where thread_number = I_thread_number;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END PROCESS_PURGE_THREAD;
-----------------------------------------------------------------------------
FUNCTION CLEAN_UP(O_error_msg    OUT VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_PURGE_FUTURE_RETAIL_SQL.CLEAN_UP';

   L_max_thread_number         NUMBER(15)             := NULL;
   L_dept                      SUBCLASS.DEPT%TYPE     := NULL;
   L_class                     SUBCLASS.CLASS%TYPE    := NULL;
   L_subclass                  SUBCLASS.SUBCLASS%TYPE := NULL;
   L_truncate_rfr_purge_thread VARCHAR2(35)           := 'truncate table rpm_rfr_purge_thread';

BEGIN

   select MAX(thread_number)
     into L_max_thread_number
     from rpm_rfr_purge_thread
    where status = 'C';

   -- get the dept, class and subclass with
   -- the maximum thread number and complete status for bookmark
   if L_max_thread_number is NOT NULL then
      select dept,
             class,
             subclass
        into L_dept,
             L_class,
             L_subclass
        from rpm_rfr_purge_thread
       where thread_number = L_max_thread_number;
   end if;

   if L_dept is NOT NULL then
      update rpm_batch_bookmark
         set bookmark_value = L_dept || ':' || L_class || ':' || L_subclass
       where batch_name     = LP_purge_rfr_batch;
   end if;

   EXECUTE IMMEDIATE L_truncate_rfr_purge_thread;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END CLEAN_UP;
-----------------------------------------------------------------------------
END RPM_PURGE_FUTURE_RETAIL_SQL;
/
