CREATE OR REPLACE PACKAGE RPM_CC_ONE_PC_PER_DAY AS

--------------------------------------------------------------------------------
--                          PUBLIC PROTOTYPES                                 --
--------------------------------------------------------------------------------

--FUNCTION VALIDATE(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
--                  I_price_change_date IN     DATE,
--                  I_parent_rpc_id     IN     NUMBER)
--RETURN NUMBER;

--------------------------------------------------------------------------------
FUNCTION VALIDATE(IO_cc_error_tbl    IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_parent_rpcs      IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER;

--------------------------------------------------------------------------------

END RPM_CC_ONE_PC_PER_DAY;
/

