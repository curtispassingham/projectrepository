CREATE OR REPLACE PACKAGE BODY RPM_WORKSHEET_STATUS_SQL AS

--------------------------------------------------------------------------------------
FUNCTION DELETE_ACTION(O_error_msg             IN OUT VARCHAR2,
                       I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS  

   L_program   VARCHAR2(100) := 'RPM_WORKSHEET_STATUS_SQL.DELETE_ACTION';

BEGIN

   merge into rpm_worksheet_item_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 distinct
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id      
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)
             and state in (1, 2)) source   -- ('New', 'Updated') 
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = 7;   	         					

   merge into rpm_worksheet_zone_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,   
                 max(rwi.state) state
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)			
           group by rwi.worksheet_status_id,
                    rwi.worksheet_item_data_id) source
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = source.state;   	         					

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END DELETE_ACTION;
  
--------------------------------------------------------------------------------------
FUNCTION RESET_ACTION(O_error_msg             IN OUT VARCHAR2,
                      I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS 

   L_program   VARCHAR2(100) := 'RPM_WORKSHEET_STATUS_SQL.RESET_ACTION';

BEGIN

   merge into rpm_worksheet_item_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 distinct
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,
                 nvl(rwi.reset_state, 1) reset_state      
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)
             and state in (7, 8)) source   -- ('Pending Delete', 'Delete Reject') 
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = decode(reset_state, 2, 2, 1);   	         					

   merge into rpm_worksheet_zone_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,   
                 max(rwi.state) state
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)			
           group by rwi.worksheet_status_id,
                    rwi.worksheet_item_data_id) source
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = source.state;   	         					

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END RESET_ACTION;
  
--------------------------------------------------------------------------------------
FUNCTION REJECT_ACTION(O_error_msg             IN OUT VARCHAR2,
                       I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS  

   L_program   VARCHAR2(100) := 'RPM_WORKSHEET_STATUS_SQL.REJECT_ACTION';

BEGIN

   merge into rpm_worksheet_item_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 distinct
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,
                 decode(rwi.state, 4, 5, 8) new_state      
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)
             and rwi.state in (4, 7)) source   -- ('Submitted', 'Pending Delete') 
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = source.new_state;   	         					

   merge into rpm_worksheet_zone_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,   
                 max(rwi.state) state
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)			
           group by rwi.worksheet_status_id,
                    rwi.worksheet_item_data_id) source
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = source.state;   	         					

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END REJECT_ACTION;
  
--------------------------------------------------------------------------------------
FUNCTION APPROVE_DELETE_ACTION(O_error_msg             IN OUT VARCHAR2,
                               I_worksheet_status_ids  IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS  

   L_program   VARCHAR2(100) := 'RPM_WORKSHEET_STATUS_SQL.APPROVE_DELETE_ACTION';

BEGIN

   merge into rpm_worksheet_item_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 distinct
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id      
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)
             and rwi.state = 7) source   --  'Pending Delete'  
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = 9;   -- Delete Approved    	         					

   merge into rpm_worksheet_zone_data target
   using (select /*+ CARDINALITY (ids 100) */ 
                 rwi.worksheet_status_id,
                 rwi.worksheet_item_data_id,   
                 max(rwi.state) state
            from table(cast(I_worksheet_status_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_worksheet_item_data rwi
           where rwi.worksheet_status_id = value(ids)			
           group by rwi.worksheet_status_id,
                    rwi.worksheet_item_data_id) source
   on (    target.worksheet_status_id = source.worksheet_status_id
       and target.worksheet_item_data_id = source.worksheet_item_data_id)
   when matched then update
      set target.state = source.state;   	         					

   return 1;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        to_char(SQLCODE));
      return 0;
END APPROVE_DELETE_ACTION;
  
--------------------------------------------------------------------------------------

END RPM_WORKSHEET_STATUS_SQL;
/
