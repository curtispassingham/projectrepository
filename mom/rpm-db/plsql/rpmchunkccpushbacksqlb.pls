CREATE OR REPLACE PACKAGE BODY RPM_CHUNK_CC_PUSH_BACK_SQL AS
--------------------------------------------------------------------------------------------
FUNCTION GET_CHUNKS_FOR_PB(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL,
                           O_chunk_info      OUT OBJ_CHUNKS_FOR_PB_BATCH_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_PUSH_BACK_SQL.GET_CHUNKS_FOR_PB';

   L_chunk_info      OBJ_CHUNKS_FOR_PB_BATCH_TBL := NULL;
   L_bulk_cc_pe_ids  OBJ_NUMERIC_ID_TABLE        := NULL;
   L_bulk_cc_pe_ids1 OBJ_NUMERIC_ID_TABLE        := NULL;
   L_price_event_ids OBJ_NUMERIC_ID_TABLE        := NULL;

   cursor C_BCC_PE_IDS is
      select distinct bulk_cc_pe_id
        from rpm_bulk_cc_pe_chunk
       where status           = RPM_CONSTANTS.CC_STATUS_COMPLETED
         and push_back_status = RPM_CONSTANTS.CC_STATUS_IN_ERROR
      union
      select bulk_cc_pe_id
        from rpm_bulk_cc_pe_thread
       where status                = RPM_CONSTANTS.CC_STATUS_IN_ERROR
         and post_push_back_failed = 1;

   cursor C_DATA_FOR_CHUNKS is
      select OBJ_CHUNKS_FOR_PB_BATCH_REC(bulk_cc_pe_id,
                                         transaction_id,
                                         parent_thread_number,
                                         thread_number,
                                         NULL)
        from (select distinct
                     bulk_cc_pe_id,
                     transaction_id,
                     parent_thread_number,
                     thread_number
                from table(cast(L_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_bulk_cc_pe_chunk rbcpc
               where status           = RPM_CONSTANTS.CC_STATUS_COMPLETED
                 and push_back_status = RPM_CONSTANTS.CC_STATUS_IN_ERROR
                 and bulk_cc_pe_id    = value(ids))
       union all
       select OBJ_CHUNKS_FOR_PB_BATCH_REC(rpcpt.bulk_cc_pe_id,
                                          rbcpc.transaction_id,
                                          rpcpt.parent_thread_number,
                                          rpcpt.thread_number,
                                          NULL)
        from table(cast(L_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids,
             rpm_bulk_cc_pe_thread rpcpt,
             rpm_bulk_cc_pe_chunk rbcpc
       where rpcpt.status                 = RPM_CONSTANTS.CC_STATUS_IN_ERROR
         and rpcpt.post_push_back_failed  = 1
         and rpcpt.bulk_cc_pe_id          = rbcpc.bulk_cc_pe_id
         and rpcpt.parent_thread_number   = rbcpc.PARENT_THREAD_NUMBER
         and rpcpt.thread_number          = rbcpc.thread_number
         and ROWNUM                       = 1
         and rpcpt.bulk_cc_pe_id          = value(ids);

   cursor C_CHUNKS (I_bulk_cc_pe_id        IN NUMBER,
                    I_transaction_id       IN NUMBER,
                    I_parent_thread_number IN NUMBER,
                    I_thread_number        IN NUMBER) is
      select rbcpc.chunk_number
        from rpm_bulk_cc_pe_chunk rbcpc
       where rbcpc.bulk_cc_pe_id         = I_bulk_cc_pe_id
         and rbcpc.transaction_id        = I_transaction_id
         and rbcpc.parent_thread_number  = I_parent_thread_number
         and rbcpc.thread_number         = I_thread_number
         and rbcpc.status                = RPM_CONSTANTS.CC_STATUS_COMPLETED
         and rbcpc.push_back_status      = RPM_CONSTANTS.CC_STATUS_IN_ERROR;

   cursor C_PE is
      select price_event_id
        from rpm_bulk_cc_pe_thread
       where bulk_cc_pe_id IN (select value(ids)
                                 from table(cast(L_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

BEGIN

   open C_BCC_PE_IDS;
   fetch C_BCC_PE_IDS BULK COLLECT into L_bulk_cc_pe_ids;
   close C_BCC_PE_IDS;

   open C_DATA_FOR_CHUNKS;
   fetch C_DATA_FOR_CHUNKS BULK COLLECT into L_chunk_info;
   close C_DATA_FOR_CHUNKS;

   for i IN 1..L_chunk_info.COUNT loop

      open C_CHUNKS(L_chunk_info(i).bulk_cc_pe_id,
                    L_chunk_info(i).transaction_id,
                    L_chunk_info(i).parent_thread_number,
                    L_chunk_info(i).thread_number);
      fetch C_CHUNKS BULK COLLECT into L_chunk_info(i).chunk_numbers;
      close C_CHUNKS;

   end loop;

   O_chunk_info := L_chunk_info;

   for i IN 1..L_chunk_info.COUNT loop

      forall j IN 1..L_chunk_info(i).chunk_numbers.COUNT
         update rpm_bulk_cc_pe_chunk
            set push_back_status     = RPM_CONSTANTS.CC_STATUS_IN_PROGRESS
          where status               = RPM_CONSTANTS.CC_STATUS_COMPLETED
            and push_back_status     = RPM_CONSTANTS.CC_STATUS_IN_ERROR
            and bulk_cc_pe_id        = L_chunk_info(i).bulk_cc_pe_id
            and transaction_id       = L_chunk_info(i).transaction_id
            and parent_thread_number = L_chunk_info(i).parent_thread_number
            and thread_number        = L_chunk_info(i).thread_number
            and chunk_number         = L_Chunk_info(i).chunk_numbers(j);

   update rpm_bulk_cc_pe_thread
      set status                = RPM_CONSTANTS.CC_STATUS_IN_PROGRESS,
          post_push_back_failed = 0
    where status                 = RPM_CONSTANTS.CC_STATUS_IN_ERROR
      and post_push_back_failed  = 1
      and bulk_cc_pe_id          = L_chunk_info(i).bulk_cc_pe_id
      and parent_thread_number   = L_chunk_info(i).parent_thread_number
      and thread_number          = L_chunk_info(i).thread_number;

   end loop;

   open C_PE;
   fetch C_PE BULK COLLECT into L_price_event_ids;
   close C_PE;

   delete
     from rpm_conflict_check_result
    where event_id IN (select value(ids)
                       from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   delete
     from rpm_con_check_err_detail
    where ref_id IN (select value(ids)
                       from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   delete
     from rpm_con_check_err
    where ref_id IN (select value(ids)
                       from table(cast(L_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   delete
     from rpm_chunk_con_check_tbl
    where bulk_cc_pe_id IN (select value(ids)
                              from table(cast(L_bulk_cc_pe_ids as OBJ_NUMERIC_ID_TABLE)) ids);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_CHUNKS_FOR_PB;
--------------------------------------------------------------------------------
END RPM_CHUNK_CC_PUSH_BACK_SQL;
/
