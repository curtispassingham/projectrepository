CREATE OR REPLACE PACKAGE BODY RPM_BATCH_HIST_SQL AS
--------------------------------------------------------------------------------

FUNCTION RPM_BATCH_RUN_HIST_ENTRY(O_error_msg                 OUT VARCHAR2,
                                  I_batch_name             IN     VARCHAR2,
                                  I_user_id                IN     VARCHAR2,
                                  I_luw_value              IN     NUMBER,
                                  I_num_concurrent_threads IN     NUMBER,
                                  I_other_parameters       IN     VARCHAR2,
                                  I_log_path               IN     VARCHAR2,
                                  I_error_path             IN     VARCHAR2,
                                  I_export_path            IN     VARCHAR2,
                                  I_thread_number          IN     NUMBER,
                                  I_start_datetime         IN     DATE,
                                  I_end_datetime           IN     DATE,
                                  I_completion_status      IN     VARCHAR2)

RETURN NUMBER IS
PRAGMA AUTONOMOUS_TRANSACTION;

   L_program VARCHAR2(45) := 'RPM_BATCH_HIST_SQL.RPM_BATCH_RUN_HIST_ENTRY';

BEGIN

   insert into rpm_batch_run_history (batch_run_hist_id,
                                      batch_name,
                                      user_id,
                                      luw_value,
                                      num_concurrent_threads,
                                      other_parameters,
                                      log_path,
                                      error_path,
                                      export_path,
                                      thread_number,
                                      start_datetime,
                                      end_datetime,
                                      completion_status,
                                      log_datetime)
      values (RPM_BATCH_RUN_HISTORY_SEQ.NEXTVAL,
              I_batch_name,
              I_user_id,
              I_luw_value,
              I_num_concurrent_threads,
              SUBSTR(I_other_parameters, 0, 500),
              SUBSTR(I_log_path, 0, 500),
              SUBSTR(I_error_path, 0, 500),
              SUBSTR(I_export_path, 0, 500),
              I_thread_number,
              I_start_datetime,
              I_end_datetime,
              I_completion_status,
              SYSDATE);

   COMMIT;

   return 1;

EXCEPTION

   when OTHERS then

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END RPM_BATCH_RUN_HIST_ENTRY;
-----------------------------------------------------------------------------

FUNCTION PURGE_RPM_BATCH_RUN_HIST(O_error_msg       OUT VARCHAR2,
                                  I_schema_owner IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_BATCH_HIST_SQL.PURGE_RPM_BATCH_RUN_HIST';

   L_high_val       TIMESTAMP := NULL;
   L_batch_run_hist TIMESTAMP := NULL;

BEGIN

   select ADD_MONTHS(SYSDATE, (batch_run_hist_months * -1))
     into L_batch_run_hist
     from rpm_system_options;

   for v_rec IN (select partition_name,
                        high_value
                   from (select partition_name,
                                high_value,
                                partition_position,
                                MIN(partition_position) KEEP (DENSE_RANK FIRST ORDER BY table_name)
                                                              OVER (PARTITION BY table_name) first_row
                           from dba_tab_partitions
                          where table_owner = I_schema_owner
                            and table_name  = 'RPM_BATCH_RUN_HISTORY')
                  where partition_position > first_row) loop

      EXECUTE IMMEDIATE 'BEGIN :1 := ' || v_rec.high_value || '; END;' using out L_high_val;

      if L_high_val < L_batch_run_hist then
         EXECUTE IMMEDIATE 'ALTER TABLE RPM_BATCH_RUN_HISTORY DROP PARTITION ' || v_rec.partition_name;
      end if;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then

      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END PURGE_RPM_BATCH_RUN_HIST;
-----------------------------------------------------------------------------
END RPM_BATCH_HIST_SQL;
/
