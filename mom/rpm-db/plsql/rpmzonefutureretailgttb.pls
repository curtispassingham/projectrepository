CREATE OR REPLACE PACKAGE BODY RPM_ZONE_FUTURE_RETAIL_GTT_SQL AS

------------------------------------------------------------------------------------------------
FUNCTION GET_CHILDREN(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      O_child_event_ids     OUT OBJ_NUM_NUM_DATE_TBL,
                      I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ZONE_FUTURE_RETAIL_GTT_SQL.GET_CHILDREN';

   cursor C_PRICE_CHANGE_EXCEPTIONS is
      select NEW OBJ_NUM_NUM_DATE_REC(rpc.price_change_id,
                                      rpc1.price_change_id,
                                      NULL)
        from rpm_price_change rpc,
             (select /*+ CARDINALITY(ids 10)*/
                     rpc.price_change_id
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc
               where rpc.price_change_id = VALUE(ids)
                 and rpc.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rownum              > 0) rpc1
       where rpc.state               IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                         RPM_CONSTANTS.PC_EXECUTED_STATE_CODE)
         and rpc.exception_parent_id = rpc1.price_change_id;

BEGIN

   open C_PRICE_CHANGE_EXCEPTIONS;
   fetch C_PRICE_CHANGE_EXCEPTIONS BULK COLLECT into O_child_event_ids;
   close C_PRICE_CHANGE_EXCEPTIONS;

   return 1;

EXCEPTION

   when OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_CHILDREN;
--------------------------------------------------------------------------------

FUNCTION REMOVE_TIMELINES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                          I_price_change_ids IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_GTT_SQL.REMOVE_TIMELINES';

BEGIN

   delete from rpm_zone_future_retail_gtt rzfrg
    where EXISTS (select /*+ CARDINALITY(cei 10) */
                         'x'
                    from table(cast(I_price_change_ids as OBJ_NUM_NUM_DATE_TBL)) cei,
                         rpm_zone_future_retail_gtt rzfrg1
                   where rzfrg.zone             = rzfrg1.zone
                     and rzfrg.item             = rzfrg1.item
                     and rzfrg1.price_event_id  = cei.numeric_col2
                     and rzfrg.price_event_id   = rzfrg1.price_event_id
                     and rzfrg1.price_change_id = cei.numeric_col1);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_TIMELINES;
--------------------------------------------------------------------------------

FUNCTION GET_AFFECTED_PARENT(O_cc_error_tbl           OUT CONFLICT_CHECK_ERROR_TBL,
                             O_affected_parent_ids    OUT OBJ_NUM_NUM_DATE_TBL,
                             I_price_change_ids    IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_GTT_SQL.GET_AFFECTED_PARENT';

   cursor C_PRICE_CHANGE_PARENTS IS
      select NEW OBJ_NUM_NUM_DATE_REC(rpc1.price_change_id,
                                      rpc2.price_change_id,
                                      rpc2.effective_date)
        from (select /*+ CARDINALITY(ids 10) */
                     rpc.price_change_id,
                     rpc.exception_parent_id
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc
               where rpc.price_change_id = value(ids)
                 and rpc.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rownum              > 0) rpc1,
             rpm_price_change rpc2
       where rpc2.state           = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
         and rpc2.price_change_id = rpc1.exception_parent_id
      union all
      select NEW OBJ_NUM_NUM_DATE_REC(rpc1.price_change_id,
                                      rpc3.price_change_id,
                                      rpc3.effective_date)
        from (select /*+ CARDINALITY(ids 10) */
                     rpc.price_change_id,
                     rpc.exception_parent_id
                from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc
               where rpc.price_change_id = value(ids)
                 and rpc.zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rownum              > 0) rpc1,
             rpm_price_change rpc2,
             rpm_price_change rpc3
       where rpc3.state            = RPM_CONSTANTS.PC_APPROVED_STATE_CODE
         and rpc3.price_change_id  = rpc2.exception_parent_id
         and rpc2.state           != RPM_CONSTANTS.PC_APPROVED_STATE_CODE
         and rpc2.price_change_id  = rpc1.exception_parent_id;

BEGIN

   open C_PRICE_CHANGE_PARENTS;
   fetch C_PRICE_CHANGE_PARENTS BULK COLLECT into O_affected_parent_ids;
   close C_PRICE_CHANGE_PARENTS;

   return 1;

EXCEPTION

   when OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GET_AFFECTED_PARENT;
--------------------------------------------------------------------------------

FUNCTION MERGE_PRICE_CHANGE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_ZONE_FUTURE_RETAIL_GTT_SQL.MERGE_PRICE_CHANGE';

BEGIN

   merge into rpm_zone_future_retail_gtt rzfr1
   using (select item,
                 zone,
                 effective_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 rzfr_multi_units,
                 rzfr_multi_unit_retail,
                 rzfr_mur_currency,
                 rzfr_multi_selling_uom,
                 price_change_id,
                 price_change_display_id,
                 change_type,
                 change_amount,
                 change_currency,
                 change_percent,
                 change_selling_uom,
                 null_multi_ind,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 price_guide_id
            from (select rzfr2.item,
                         rzfr2.zone,
                         rzfr2.selling_retail,
                         rzfr2.selling_retail_currency,
                         rzfr2.selling_uom,
                         rzfr2.multi_units rzfr_multi_units,
                         rzfr2.multi_unit_retail rzfr_multi_unit_retail,
                         rzfr2.multi_unit_retail_currency rzfr_mur_currency,
                         rzfr2.multi_selling_uom rzfr_multi_selling_uom,
                         rpc.price_change_id,
                         rpc.price_change_display_id,
                         rpc.change_type,
                         rpc.change_amount,
                         rpc.change_currency,
                         rpc.change_percent,
                         rpc.change_selling_uom,
                         rpc.null_multi_ind,
                         rpc.multi_units,
                         rpc.multi_unit_retail,
                         rpc.multi_unit_retail_currency,
                         rpc.multi_selling_uom,
                         NVL(rpc.price_guide_id, 0) price_guide_id,
                         rpc.effective_date,
                         RANK() OVER (PARTITION BY rzfr2.item,
                                                   rzfr2.zone,
                                                   price_event_id
                                          ORDER BY rzfr2.action_date DESC) as rank_value
                    from rpm_zone_future_retail_gtt rzfr2,
                         (select price_change_id,
                                 price_change_display_id,
                                 change_type,
                                 change_amount,
                                 change_currency,
                                 change_percent,
                                 change_selling_uom,
                                 null_multi_ind,
                                 multi_units,
                                 multi_unit_retail,
                                 multi_unit_retail_currency,
                                 multi_selling_uom,
                                 price_guide_id,
                                 effective_date
                            from table(cast(I_price_change_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                 rpm_price_change
                           where price_change_id = value(ids)
                             and zone_node_type  = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE) rpc
                   where rzfr2.action_date    <= rpc.effective_date
                     and rzfr2.price_event_id  = rpc.price_change_id)
           where rank_value = 1) rzfr3
   on (    rzfr1.item        = rzfr3.item
       and rzfr1.zone        = rzfr3.zone
       and rzfr1.action_date = rzfr3.effective_date)
   when MATCHED then
      update
         set rzfr1.price_change_id               = rzfr3.price_change_id,
             rzfr1.price_change_display_id       = rzfr3.price_change_display_id,
             rzfr1.pc_change_type                = rzfr3.change_type,
             rzfr1.pc_change_amount              = rzfr3.change_amount,
             rzfr1.pc_change_currency            = rzfr3.change_currency,
             rzfr1.pc_change_percent             = rzfr3.change_percent,
             rzfr1.pc_change_selling_uom         = rzfr3.change_selling_uom,
             rzfr1.pc_null_multi_ind             = rzfr3.null_multi_ind,
             rzfr1.pc_multi_units                = rzfr3.multi_units,
             rzfr1.pc_multi_unit_retail          = rzfr3.multi_unit_retail,
             rzfr1.pc_multi_unit_retail_currency = rzfr3.multi_unit_retail_currency,
             rzfr1.pc_multi_selling_uom          = rzfr3.multi_selling_uom,
             rzfr1.pc_price_guide_id             = rzfr3.price_guide_id
   when NOT MATCHED then
      insert(price_event_id,
             zone_future_retail_id,
             item,
             zone,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             price_change_id,
             price_change_display_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id)
      values(rzfr3.price_change_id,
             RPM_ZONE_FUTURE_RETAIL_SEQ.NEXTVAL,
             rzfr3.item,
             rzfr3.zone,
             rzfr3.effective_date,
             rzfr3.selling_retail,
             rzfr3.selling_retail_currency,
             rzfr3.selling_uom,
             rzfr3.rzfr_multi_units,
             rzfr3.rzfr_multi_unit_retail,
             rzfr3.rzfr_mur_currency,
             rzfr3.rzfr_multi_selling_uom,
             rzfr3.price_change_id,
             rzfr3.price_change_display_id,
             rzfr3.change_type,
             rzfr3.change_amount,
             rzfr3.change_currency,
             rzfr3.change_percent,
             rzfr3.change_selling_uom,
             rzfr3.null_multi_ind,
             rzfr3.multi_units,
             rzfr3.multi_unit_retail,
             rzfr3.multi_unit_retail_currency,
             rzfr3.multi_selling_uom,
             rzfr3.price_guide_id);

   return 1;

EXCEPTION

   WHEN OTHERS THEN
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END MERGE_PRICE_CHANGE;

------------------------------------------------------------

FUNCTION REMOVE_EVENT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_change_ids IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_ZONE_FUTURE_RETAIL_GTT_SQL.REMOVE_EVENT';

BEGIN

   update rpm_zone_future_retail_gtt rzfrg
      set rzfrg.price_change_id               = NULL,
          rzfrg.price_change_display_id       = NULL,
          rzfrg.pc_change_type                = NULL,
          rzfrg.pc_change_amount              = NULL,
          rzfrg.pc_change_currency            = NULL,
          rzfrg.pc_change_percent             = NULL,
          rzfrg.pc_change_selling_uom         = NULL,
          rzfrg.pc_null_multi_ind             = NULL,
          rzfrg.pc_multi_units                = NULL,
          rzfrg.pc_multi_unit_retail          = NULL,
          rzfrg.pc_multi_unit_retail_currency = NULL,
          rzfrg.pc_multi_selling_uom          = NULL,
          rzfrg.pc_price_guide_id             = NULL
    where rzfrg.rowid IN (select /*+ CARDINALITY(ids 1000) */
                                 rzfrg1.rowid
                            from table(cast(I_price_change_ids as OBJ_NUM_NUM_DATE_TBL)) ids,
                                 rpm_zone_future_retail_gtt rzfrg1
                           where rzfrg1.price_event_id  = ids.numeric_col1
                             and rzfrg1.price_change_id = ids.numeric_col2);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END REMOVE_EVENT;
--------------------------------------------------------------------------------
END RPM_ZONE_FUTURE_RETAIL_GTT_SQL;
/

