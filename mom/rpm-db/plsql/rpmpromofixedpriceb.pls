CREATE OR REPLACE PACKAGE BODY RPM_CC_PROMO_FIXED_PRICE AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50)  := 'RPM_CC_PROMO_FIXED_PRICE.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   L_simple_promo_overlap NUMBER := NULL;

   cursor C_CHECK is
      select gtt.price_event_id,
             gtt.future_retail_id,
             COUNT(*) cnt
        from rpm_future_retail_gtt gtt,
             rpm_promo_fr_item_loc_gtt il
       where gtt.price_event_id     = il.price_event_id
         and gtt.item               = il.item
         and (   (    gtt.diff_id   is NULL
                  and il.diff_id    is NULL)
              or gtt.diff_id        = il.diff_id)
         and gtt.location           = il.location
         and gtt.zone_node_type     = il.zone_node_type
         and gtt.action_date        = il.action_date
         and il.detail_change_type  = RPM_CONSTANTS.RETAIL_FIXED_AMOUNT_VALUE
         and il.customer_type       is NULL
         and NVL(il.deleted_ind, 0) != 1
         and gtt.price_event_id     NOT IN (select distinct ccet.price_event_id
                                              from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
      having COUNT(*) > 1
      group by gtt.price_event_id,
               gtt.future_retail_id,
               gtt.item,
               gtt.diff_id,
               gtt.location,
               gtt.zone_node_type,
               gtt.action_date;

BEGIN

   select simple_promo_overlap_rule into L_simple_promo_overlap
     from rpm_system_options;

   if L_simple_promo_overlap = RPM_CONSTANTS.PR_OVERLAP_COMPOUNDING and
      I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE) then

      -- Set up local error table to be used by cursor so that
      -- we don't check price events that already have issues.
      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         ---
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                                 NULL,
                                                 NULL,
                                                 NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      -- loop through the records that have too many
      -- fixed priced promos associated with them...

      for rec IN C_CHECK loop
         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'event_causes_multiple_promotion_details_to_be_fixed');

         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;
         end if;
      end loop;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/