CREATE OR REPLACE PACKAGE RPM_ZONE_GROUP_SQL AS
--------------------------------------------------------
PROCEDURE SEARCH(O_return_code      OUT NUMBER,
                 O_error_msg        OUT VARCHAR2,
                 O_z_tbl            OUT OBJ_ZONE_TBL,
                 O_z_count          OUT NUMBER,
                 I_zg_criterias     IN  OBJ_ZONE_GROUP_SEARCH_TBL);
--------------------------------------------------------
PROCEDURE PROMO_ZONE_SEARCH_LM(O_return_code      OUT NUMBER,
                               O_error_msg        OUT VARCHAR2,
                               O_z_tbl            OUT OBJ_ZONE_TBL,
                               O_z_count          OUT NUMBER,
                               I_zg_criterias     IN  OBJ_ZONE_GROUP_SEARCH_TBL
                               );
----------------------------------------------------------                               
END RPM_ZONE_GROUP_SQL;
/

