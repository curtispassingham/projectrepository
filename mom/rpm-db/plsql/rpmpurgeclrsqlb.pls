CREATE OR REPLACE PACKAGE BODY RPM_PURGE_CLEARANCE_SQL AS

LP_vdate DATE := GET_VDATE();

--------------------------------------------------------------------------------
FUNCTION PURGE_UNUSED_ABANDONED_CLR(O_error_msg      OUT VARCHAR2,
                                    O_deleted_cnt    OUT NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_PURGE_CLEARANCE_SQL.PURGE_UNUSED_ABANDONED_CLR';

   L_clearance_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_reset_ids     OBJ_NUMERIC_ID_TABLE := NULL;

   L_clear_hist_months NUMBER := NULL;
   L_reject_hold_days  NUMBER := NULL;
   L_deleted_cnt       NUMBER := 0;

   cursor C_CLR is
      select clearance_id
        from rpm_clearance c
       where c.effective_date <= ADD_MONTHS(LP_vdate, -1 * L_clear_hist_months)
         and (   c.state      IN (RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE,
                                  RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE)
              or (    c.state IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                  RPM_CONSTANTS.PC_EXECUTED_STATE_CODE)
                  and NOT EXISTS (select 'x'
                                    from rpm_future_retail fr
                                   where fr.clearance_id = c.clearance_id)))
      union all
      select clearance_id
        from rpm_clearance c
       where c.effective_date <= LP_vdate - L_reject_hold_days
         and c.state           = RPM_CONSTANTS.PC_REJECTED_STATE_CODE;

   cursor C_RESET is
   select clearance_id
     from rpm_clearance_reset res,
          item_master im
    where res.effective_date <= ADD_MONTHS(LP_vdate, -1 * L_clear_hist_months)
      and im.item             = res.item
      and NOT EXISTS (select 'x'
                       from rpm_future_retail fr
                      where fr.dept            = im.dept
                        and fr.item            = res.item
                        and fr.location        = res.location
                        and fr.action_date     = res.effective_date
                        and fr.clear_start_ind = 3);

BEGIN

   select clearance_hist_months,
          reject_hold_days_pc_clear
     into L_clear_hist_months,
          L_reject_hold_days
     from rpm_system_options;

   open C_CLR;
   fetch C_CLR BULK COLLECT into L_clearance_ids;
   close C_CLR;

   if L_clearance_ids is NOT NULL and
      L_clearance_ids.COUNT > 0 then

      delete from rpm_merch_list_detail rmld
       where rmld.merch_list_id IN (select /*+ CARDINALITY(ids 10) */
                                           price_event_itemlist
                                      from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.clearance_id         = VALUE(ids)
                                       and rc.price_event_itemlist is NOT NULL
                                       and rmld.merch_list_id      = rc.price_event_itemlist);

      delete from rpm_merch_list_head rmlh
       where rmlh.merch_list_id IN (select /*+ CARDINALITY(ids 10) */
                                           price_event_itemlist
                                      from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.clearance_id         = VALUE(ids)
                                       and rc.price_event_itemlist is NOT NULL
                                       and rc.price_event_itemlist = rmlh.merch_list_id);

      delete from rpm_clearance_skulist rcs
       where rcs.price_event_id IN (select /*+ CARDINALITY(ids 10) */
                                           value(ids)
                                      from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.clearance_id    = VALUE(ids)
                                       and rc.skulist         is NOT NULL
                                       and rcs.price_event_id = rc.clearance_id
                                       and rcs.skulist        = rc.skulist);

      delete from rpm_clearance_cust_attr
       where cust_attr_id IN (select /*+ CARDINALITY(ids 10) */
                                     cust_attr_id
                                from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_clearance
                               where clearance_id        = VALUE(ids)
                                 and exception_parent_id is NOT NULL
                                 and cust_attr_id        is NOT NULL);

      -- delete any grandchildren records
      delete from rpm_clearance
       where clearance_id IN (select /*+ CARDINALITY(ids 10) */
                                     clearance_id
                                from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_clearance rc1
                               where rc1.clearance_id        = VALUE(ids)
                                 and rc1.exception_parent_id is NOT NULL
                                 and EXISTS (select 'x'
                                               from rpm_clearance rc2
                                              where rc2.clearance_id        = rc1.exception_parent_id
                                                and rc2.exception_parent_id is NOT NULL));

      -- delete any children records
      delete from rpm_clearance
       where clearance_id IN (select /*+ CARDINALITY(ids 10) */
                                     clearance_id
                                from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_clearance
                               where clearance_id        = VALUE(ids)
                                 and exception_parent_id is NOT NULL);

      delete from rpm_clearance
       where clearance_id IN (select /*+ CARDINALITY(ids 10) */
                                     clearance_id
                                from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                     rpm_clearance rc1
                               where rc1.clearance_id            = VALUE(ids)
                                 and rc1.exception_parent_id is NULL
                                 and NOT EXISTS (select 'x'
                                                   from rpm_clearance rc2
                                                  where rc2.exception_parent_id = rc1.clearance_id));

      L_deleted_cnt := L_clearance_ids.COUNT;

   end if;

   open C_RESET;
   fetch C_RESET BULK COLLECT into L_reset_ids;
   close C_RESET;

   if L_reset_ids is NOT NULL and
      L_reset_ids.COUNT > 0 then

      forall i IN L_reset_ids.FIRST..L_reset_ids.LAST
         delete
           from rpm_clearance_reset
          where clearance_id = L_reset_ids(i);

      L_deleted_cnt := L_deleted_cnt + L_reset_ids.COUNT;

   end if;

   O_deleted_cnt := L_deleted_cnt;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END PURGE_UNUSED_ABANDONED_CLR;

--------------------------------------------------------------------------------
FUNCTION PURGE_EXPIRED_CLEARANCE(O_error_msg      OUT VARCHAR2,
                                 O_deleted_cnt    OUT NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_PURGE_CLEARANCE_SQL.PURGE_EXPIRED_CLEARANCE';

   L_clearance_ids OBJ_NUMERIC_ID_TABLE := NULL;
   L_reset_ids     OBJ_NUMERIC_ID_TABLE := NULL;

   L_clear_hist_months NUMBER := 0;
   L_deleted_cnt       NUMBER := 0;

   cursor C_EXPIRED_CLR is
      select distinct number_1 clearance_id,
             number_2 reset_id
        from gtt_num_num_str_str_date_date t
       where EXISTS (select 'x'
                       from rpm_clearance rc
                      where rc.exception_parent_id is NULL
                        and rc.clearance_id        = t.number_1
                     union all
                     -- to get the child clearance
                     select 'x'
                       from rpm_clearance rc
                      where rc.exception_parent_id is NOT NULL
                        and rc.clearance_id        = t.number_1
                     union all
                     -- to ensure that if it is a parent clearance, the child is also eligible for deletion
                     select 'x'
                       from rpm_clearance rc
                      where rc.exception_parent_id is NOT NULL
                        and rc.exception_parent_id = t.number_1
                        and EXISTS (select 'x'
                                      from gtt_num_num_str_str_date_date t2
                                     where t2.number_1 = rc.clearance_id));

BEGIN

   select clearance_hist_months
     into L_clear_hist_months
     from rpm_system_options;

   -- Get All Old Clearances and all Item/location that are still in the Future Retail

   delete rpm_future_retail_gtt;

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      zone_node_type,
                                      location,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind)
      with clearance as
         (select clearance_id,
                 effective_date
            from rpm_clearance
           where effective_date <= ADD_MONTHS(LP_vdate, -1 * L_clear_hist_months)
             and state          IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                    RPM_CONSTANTS.PC_EXECUTED_STATE_CODE))
            -- Tran item/loc
            select rc.clearance_id,
                   rfr.future_retail_id,
                   rfr.dept,
                   rfr.class,
                   rfr.subclass,
                   rfr.item,
                   rfr.zone_node_type,
                   rfr.location,
                   rfr.action_date,
                   rfr.selling_retail,
                   rfr.selling_retail_currency,
                   rfr.selling_uom,
                   rfr.on_simple_promo_ind,
                   rfr.on_complex_promo_ind
              from clearance rc,
                   rpm_future_retail rfr
             where rfr.clearance_id    = rc.clearance_id
               and rfr.clear_start_ind = 1
               and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_LOC
            union all
            -- Tran item/zone
            select rc.clearance_id,
                   rfr.future_retail_id,
                   rfr.dept,
                   rfr.class,
                   rfr.subclass,
                   rfr.item,
                   rfr.zone_node_type,
                   rzl.location,
                   rfr.action_date,
                   rfr.selling_retail,
                   rfr.selling_retail_currency,
                   rfr.selling_uom,
                   rfr.on_simple_promo_ind,
                   rfr.on_complex_promo_ind
              from clearance rc,
                   rpm_future_retail rfr,
                   rpm_zone_location rzl,
                   rpm_item_loc ril
             where rfr.clearance_id    = rc.clearance_id
               and rfr.clear_start_ind = 1
               and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
               and rfr.location        = rzl.zone_id
               and ril.dept            = rfr.dept
               and ril.item            = rfr.item
               and ril.loc             = rzl.location
            union all
            -- Diff item/loc
            select rc.clearance_id,
                   rfr.future_retail_id,
                   rfr.dept,
                   rfr.class,
                   rfr.subclass,
                   im.item,
                   rfr.zone_node_type,
                   rfr.location,
                   rfr.action_date,
                   rfr.selling_retail,
                   rfr.selling_retail_currency,
                   rfr.selling_uom,
                   rfr.on_simple_promo_ind,
                   rfr.on_complex_promo_ind
              from clearance rc,
                   rpm_future_retail rfr,
                   item_master im,
                   rpm_item_loc ril
             where rfr.clearance_id    = rc.clearance_id
               and rfr.clear_start_ind = 1
               and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_LOC
               and im.item_parent      = rfr.item
               and im.diff_1           = rfr.diff_id
               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and ril.dept            = rfr.dept
               and ril.item            = im.item
               and ril.loc             = rfr.location
            union all
            -- Diff item/zone
            select rc.clearance_id,
                   rfr.future_retail_id,
                   rfr.dept,
                   rfr.class,
                   rfr.subclass,
                   im.item,
                   rfr.zone_node_type,
                   rzl.location,
                   rfr.action_date,
                   rfr.selling_retail,
                   rfr.selling_retail_currency,
                   rfr.selling_uom,
                   rfr.on_simple_promo_ind,
                   rfr.on_complex_promo_ind
              from clearance rc,
                   rpm_future_retail rfr,
                   item_master im,
                   rpm_zone_location rzl,
                   rpm_item_loc ril
             where rfr.clearance_id    = rc.clearance_id
               and rfr.clear_start_ind = 1
               and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
               and im.item_parent      = rfr.item
               and im.diff_1           = rfr.diff_id
               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and rfr.location        = rzl.zone_id
               and ril.dept            = rfr.dept
               and ril.item            = im.item
               and ril.loc             = rzl.location
            union all
            -- Parent item/loc
            select rc.clearance_id,
                   rfr.future_retail_id,
                   rfr.dept,
                   rfr.class,
                   rfr.subclass,
                   im.item,
                   rfr.zone_node_type,
                   rfr.location,
                   rfr.action_date,
                   rfr.selling_retail,
                   rfr.selling_retail_currency,
                   rfr.selling_uom,
                   rfr.on_simple_promo_ind,
                   rfr.on_complex_promo_ind
              from clearance rc,
                   rpm_future_retail rfr,
                   item_master im,
                   rpm_item_loc ril
             where rfr.clearance_id    = rc.clearance_id
               and rfr.clear_start_ind = 1
               and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_LOC
               and im.item_parent      = rfr.item
               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and ril.dept            = rfr.dept
               and ril.item            = im.item
               and ril.loc             = rfr.location
            union all
            -- Parent item/zone
            select rc.clearance_id,
                   rfr.future_retail_id,
                   rfr.dept,
                   rfr.class,
                   rfr.subclass,
                   im.item,
                   rfr.zone_node_type,
                   rzl.location,
                   rfr.action_date,
                   rfr.selling_retail,
                   rfr.selling_retail_currency,
                   rfr.selling_uom,
                   rfr.on_simple_promo_ind,
                   rfr.on_complex_promo_ind
              from clearance rc,
                   rpm_future_retail rfr,
                   item_master im,
                   rpm_zone_location rzl,
                   rpm_item_loc ril
             where rfr.clearance_id    = rc.clearance_id
               and rfr.clear_start_ind = 1
               and rfr.cur_hier_level  = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
               and im.item_parent      = rfr.item
               and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
               and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
               and rfr.location        = rzl.zone_id
               and ril.dept            = rfr.dept
               and ril.item            = im.item
               and ril.loc             = rzl.location;

   -- Now make sure that all clearance reset records are expired so both clearance reset
   -- and clearance records can be purged

   delete from gtt_num_num_str_str_date_date;

   insert into gtt_num_num_str_str_date_date (number_1,
                                              number_2)
      select distinct clearance_id,
             reset_id
        from (select fr.price_event_id clearance_id,
                     rc.clearance_id reset_id
                from rpm_future_retail_gtt fr,
                     rpm_clearance_reset rc
               where rc.item (+)           = fr.item
                 and rc.location (+)       = fr.location
                 and rc.state (+)          IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                                               RPM_CONSTANTS.PC_EXECUTED_STATE_CODE)
                 and rc.effective_date (+) <= ADD_MONTHS(LP_vdate, -1 * L_clear_hist_months)) t
       where t.reset_id is NOT NULL;

   open C_EXPIRED_CLR;
   fetch C_EXPIRED_CLR BULK COLLECT into L_clearance_ids,
                                         L_reset_ids;
   close C_EXPIRED_CLR;

   if L_clearance_ids is NOT NULL and
      L_clearance_ids.COUNT > 0 then

      delete from rpm_merch_list_detail rmld
       where rmld.merch_list_id IN (select /*+ CARDINALITY(ids 10) */
                                           price_event_itemlist
                                      from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.clearance_id         = VALUE(ids)
                                       and rc.price_event_itemlist is NOT NULL
                                       and rmld.merch_list_id      = rc.price_event_itemlist);

      delete from rpm_merch_list_head rmlh
       where rmlh.merch_list_id IN (select /*+ CARDINALITY(ids 10) */
                                           price_event_itemlist
                                      from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.clearance_id         = VALUE(ids)
                                       and rc.price_event_itemlist is NOT NULL
                                       and rc.price_event_itemlist = rmlh.merch_list_id);

      delete from rpm_clearance_skulist rcs
       where rcs.price_event_id IN (select /*+ CARDINALITY(ids 10) */
                                           VALUE(ids)
                                      from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                           rpm_clearance rc
                                     where rc.clearance_id     = VALUE(ids)
                                       and rc.skulist          is NOT NULL
                                       and rcs.price_event_id  = rc.clearance_id
                                       and rcs.skulist         = rc.skulist);

      delete from rpm_clearance_cust_attr rcca
       where EXISTS (select /*+ CARDINALITY(ids 10) */ 'x'
                       from table(cast(L_clearance_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                            rpm_clearance rc
                      where rc.clearance_id = VALUE(ids)
                        and rc.cust_attr_id = rcca.cust_attr_id);

      forall i IN L_clearance_ids.FIRST..L_clearance_ids.LAST
         delete
           from rpm_clearance
          where exception_parent_id = L_clearance_ids(i)
            and change_type         = RPM_CONSTANTS.RETAIL_EXCLUDE;

      -- Delete any child records first
      forall i IN L_clearance_ids.FIRST..L_clearance_ids.LAST
         delete
           from rpm_clearance rc
          where rc.exception_parent_id is NOT NULL
            and rc.clearance_id = L_clearance_ids(i);

      forall i IN L_clearance_ids.FIRST..L_clearance_ids.LAST
         delete
           from rpm_clearance
          where clearance_id = L_clearance_ids(i);

      L_deleted_cnt := L_deleted_cnt + L_clearance_ids.COUNT;

   end if;

   if L_reset_ids is NOT NULL and
      L_reset_ids.COUNT > 0 then

      forall i IN L_reset_ids.FIRST..L_reset_ids.LAST
         delete
           from rpm_clearance_reset
          where clearance_id = L_reset_ids(i);

      L_deleted_cnt := L_deleted_cnt + L_reset_ids.COUNT;

   end if;

   O_deleted_cnt := L_deleted_cnt;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      return 0;

END PURGE_EXPIRED_CLEARANCE;
--------------------------------------------------------------------------------
END RPM_PURGE_CLEARANCE_SQL;
/
