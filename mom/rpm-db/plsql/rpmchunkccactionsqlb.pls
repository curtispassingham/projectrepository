CREATE OR REPLACE PACKAGE BODY RPM_CHUNK_CC_ACTIONS_SQL AS
-------------------------------------------------------------------------------
   LP_bulk_cc_pe_id NUMBER(10) := NULL;
   LP_vdate         DATE       := GET_VDATE;

   LP_price_event_type    VARCHAR2(3)                             := NULL;
   LP_need_il_explode_ind RPM_BULK_CC_PE.NEED_IL_EXPLODE_IND%TYPE := NULL;
   LP_chunk_number        NUMBER                                  := 0;

   LP_cc_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LP_batch_name	      VARCHAR2(200)			  := NULL;

--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_bulk_cc_pe_id    IN     NUMBER,
                      I_thread_number    IN     NUMBER,
                      I_chunk_number     IN     NUMBER,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PROCESS_PRICE_CHANGES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id     IN     NUMBER,
                               I_persist_ind      IN     VARCHAR2,
                               I_start_state      IN     VARCHAR2,
                               I_end_state        IN     VARCHAR2,
                               I_user_name        IN     VARCHAR2,
                               I_pe_sequence_id   IN     NUMBER,
                               I_pe_thread_number IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PROCESS_CLEARANCES(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE,
                            I_rib_trans_id         IN     NUMBER,
                            I_persist_ind          IN     VARCHAR2,
                            I_start_state          IN     VARCHAR2,
                            I_end_state            IN     VARCHAR2,
                            I_user_name            IN     VARCHAR2,
                            I_bulk_cc_pe_id        IN     NUMBER,
                            I_parent_thread_number IN     NUMBER,
                            I_pe_thread_number     IN     NUMBER,
                            I_chunk_number         IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PROCESS_CLEARANCE_RESETS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE,
                                  I_rib_trans_id         IN     NUMBER,
                                  I_persist_ind          IN     VARCHAR2,
                                  I_start_state          IN     VARCHAR2,
                                  I_end_state            IN     VARCHAR2,
                                  I_user_name            IN     VARCHAR2,
                                  I_bulk_cc_pe_id        IN     NUMBER,
                                  I_parent_thread_number IN     NUMBER,
                                  I_pe_thread_number     IN     NUMBER,
                                  I_chunk_number         IN     NUMBER,
                                  I_old_clr_oostock_date IN     DATE DEFAULT NULL,
                                  I_old_clr_reset_date   IN     DATE DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PROCESS_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id    IN     NUMBER,
                               I_persist_ind     IN     VARCHAR2,
                               I_start_state     IN     VARCHAR2,
                               I_end_state       IN     VARCHAR2,
                               I_user_name       IN     VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PROCESS_UPDATE_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                                      I_rib_trans_id    IN     NUMBER,
                                      I_persist_ind     IN     VARCHAR2,
                                      I_start_state     IN     VARCHAR2,
                                      I_end_state       IN     VARCHAR2,
                                      I_user_name       IN     VARCHAR2,
                                      I_promo_end_date  IN     DATE)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION PUSH_BACK_WS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_type     IN     VARCHAR2,
                      I_bulk_cc_pe_id        IN     NUMBER,
                      I_parent_thread_number IN     NUMBER,
                      I_thread_number        IN     NUMBER,
                      I_chunk_number         IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION GENERATE_TIMELINE(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id IN     NUMBER,
                           I_thread_number IN     NUMBER,
                           I_chunk_number  IN     NUMBER,
                           I_pe_no_frs     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION GENERATE_TL_FROM_INTERSECTION(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_thread_number   IN     NUMBER,
                                       I_chunk_number    IN     NUMBER,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION GEN_PILE_TL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                     I_bulk_cc_pe_id    IN     NUMBER,
                     I_thread_number    IN     NUMBER,
                     I_chunk_number     IN     NUMBER,
                     I_price_event_type IN     VARCHAR2,
                     I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                     I_step_identifier  IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION GENERATE_CSPFR_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_bulk_cc_pe_id    IN     NUMBER,
                                 I_thread_number    IN     NUMBER,
                                 I_chunk_number     IN     NUMBER,
                                 I_price_event_type IN     VARCHAR2,
                                 I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_step_identifier  IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------

FUNCTION GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_bulk_cc_pe_id    IN     NUMBER,
                               I_thread_number    IN     NUMBER,
                               I_chunk_number     IN     NUMBER,
                               I_price_event_type IN     VARCHAR2,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_step_identifier  IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------------------------------
--FUNCTION DETAILS
--------------------------------------------------------------------------------
FUNCTION POPULATE_GTT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                      I_bulk_cc_pe_id    IN     NUMBER,
                      I_thread_number    IN     NUMBER,
                      I_chunk_number     IN     NUMBER,
                      I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                      I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_ACTIONS_SQL.POPULATE_GTT';

   L_pe_no_frs     OBJ_NUMERIC_ID_TABLE := NULL;
   L_start_time    TIMESTAMP            := SYSTIMESTAMP;
   L_error_message VARCHAR2(255)        := NULL;

   cursor C_NO_FR is
      select distinct price_event_id
        from ((select i.price_event_id,
                      i.item,
                      i.diff_id,
                      l.location,
                      l.zone_node_type
                 from (select it.bulk_cc_pe_id,
                              it.thread_number,
                              it.price_event_id,
                              it.item,
                              it.diff_id,
                              it.itemloc_id,
                              case
                                 when t.pe_zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) then
                                    t.pe_zone_node_type
                                 when t.pe_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                                      t.zone_group_id = rmrde.regular_zone_group then
                                    t.pe_zone_node_type
                                 else
                                    NULL
                              end pe_zone_node_type,
                              case
                                 when t.pe_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE and
                                      t.zone_group_id = rmrde.regular_zone_group then
                                    t.location
                                 else
                                    NULL
                              end pe_zone_id,
                              RANK() OVER (PARTITION BY it.item,
                                                        it.diff_id,
                                                        t.pe_zone_node_type,
                                                        t.location
                                               ORDER BY NVL(it.txn_man_excluded_item, 0),
                                                        it.pe_merch_level desc,
                                                        it.price_event_id) rank
                         from rpm_bulk_cc_pe_item it,
                              rpm_merch_retail_def_expl rmrde,
                              (select /*+ CARDINALITY (ids 10) */
                                      rpc.price_change_id price_event_id,
                                      DECODE(rpc.zone_node_type,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpc.zone_id,
                                             rpc.location) location,
                                      rpc.zone_node_type pe_zone_node_type,
                                      rz.zone_group_id
                                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_price_change rpc,
                                      rpm_zone rz
                                where I_price_event_type  = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                                  and rpc.price_change_id = VALUE(ids)
                                  and rpc.zone_id         = rz.zone_id (+)
                                  and rpc.link_code       is NULL
                               union all
                               select /*+ CARDINALITY (ids 10) */
                                      rc.clearance_id price_event_id,
                                      DECODE(rc.zone_node_type,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rc.zone_id,
                                             rc.location) location,
                                      rc.zone_node_type pe_zone_node_type,
                                      rz.zone_group_id
                                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_clearance rc,
                                      rpm_zone rz
                                where I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE
                                  and rc.clearance_id    = VALUE(ids)
                                  and rc.zone_id         = rz.zone_id (+)
                               union all
                               select /*+ CARDINALITY (ids 10) */
                                      rc.clearance_id price_event_id,
                                      rc.location,
                                      rc.zone_node_type pe_zone_node_type,
                                      NULL
                                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_clearance_reset rc
                                where I_price_event_type = RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET
                                  and rc.clearance_id    = VALUE(ids)
                               union all
                               select /*+ CARDINALITY (ids 10) */
                                      rpzl.promo_dtl_id price_event_id,
                                      DECODE(rpzl.zone_node_type,
                                             RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE, rpzl.zone_id,
                                             rpzl.location) location,
                                      rpzl.zone_node_type pe_zone_node_type,
                                      rz.zone_group_id
                                 from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                      rpm_promo_zone_location rpzl,
                                      rpm_zone rz
                                where I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
                                  and rpzl.promo_dtl_id = VALUE(ids)
                                  and rpzl.zone_id      = rz.zone_id (+)) t,
                              rpm_bulk_cc_pe bcp
                        where bcp.bulk_cc_pe_id                 = I_bulk_cc_pe_id
                          and bcp.location_move_id              is NULL
                          and it.bulk_cc_pe_id                  = bcp.bulk_cc_pe_id
                          and it.thread_number                  = I_thread_number
                          and it.chunk_number                   = I_chunk_number
                          and NVL(it.txn_man_excluded_item, 0) != 1
                          and it.merch_level_type               = it.pe_merch_level
                          and rmrde.dept                        = it.dept
                          and rmrde.class                       = it.class
                          and rmrde.subclass                    = it.subclass
                          and t.price_event_id                  = it.price_event_id
                          and rownum                            > 0) i,
                      rpm_bulk_cc_pe_location l
                where i.rank           = 1
                  and l.bulk_cc_pe_id  = i.bulk_cc_pe_id
                  and l.price_event_id = i.price_event_id
                  and l.itemloc_id     = i.itemloc_id
                  and (   (    i.pe_zone_node_type IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                           and l.zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE))
                       or (    i.pe_zone_node_type is NULL
                           and l.zone_node_type    IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                       RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE))
                       or (    i.pe_zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           and l.zone_node_type    = i.pe_zone_node_type
                           and l.location          = i.pe_zone_id))
              union all
              -- Price Change is Location Level and Link Code is not NULL
              select /*+ CARDINALITY (ids 10) */
                     rpc.price_change_id price_event_id,
                     rlca.item,
                     NULL diff_id,
                     rlca.location,
                     rlca.loc_type zone_node_type
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_link_code_attribute rlca,
                     rpm_bulk_cc_pe bcp
               where I_price_event_type   = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and bcp.bulk_cc_pe_id    = I_bulk_cc_pe_id
                 and bcp.location_move_id is NULL
                 and rpc.price_change_id  = VALUE(ids)
                 and rpc.link_code        is NOT NULL
                 and rpc.link_code        = rlca.link_code
                 and rpc.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                 and rpc.location         = rlca.location
              union all
              -- Price Changes with Link Code and Zone Level
              select /*+ CARDINALITY (ids 10) */
                     rpc.price_change_id price_event_id,
                     rlca.item,
                     NULL diff_id,
                     rlca.location,
                     rlca.loc_type zone_node_type
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_price_change rpc,
                     rpm_link_code_attribute rlca,
                     rpm_zone_location rzl,
                     rpm_bulk_cc_pe bcp
               where I_price_event_type   = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE
                 and bcp.bulk_cc_pe_id    = I_bulk_cc_pe_id
                 and bcp.location_move_id is NULL
                 and rpc.price_change_id  = VALUE(ids)
                 and rpc.link_code        is NOT NULL
                 and rpc.link_code        = rlca.link_code
                 and rpc.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                 and rpc.zone_id          = rzl.zone_id
                 and rzl.location         = rlca.location
                union all
                select /*+ CARDINALITY (ids 10) */
                       i.price_event_id,
                       i.item,
                       i.diff_id,
                       l.location,
                       l.zone_node_type
                  from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                       rpm_bulk_cc_pe_item i,
                       rpm_bulk_cc_pe_location l,
                       rpm_bulk_cc_pe bcp
                 where bcp.bulk_cc_pe_id                = I_bulk_cc_pe_id
                   and bcp.location_move_id             is NOT NULL
                   and i.price_event_id                 = VALUE(ids)
                   and i.bulk_cc_pe_id                  = bcp.bulk_cc_pe_id
                   and i.thread_number                  = I_thread_number
                   and i.chunk_number                   = I_chunk_number
                   and i.merch_level_type               = i.pe_merch_level
                   and NVL(i.txn_man_excluded_item, 0) != 1
                   and l.bulk_cc_pe_id                  = i.bulk_cc_pe_id
                   and l.price_event_id                 = i.price_event_id
                   and l.itemloc_id                     = i.itemloc_id)
               minus
               select price_event_id,
                      item,
                      diff_id,
                      location,
                      regular_zone_group zone_node_type
                 from rpm_item_loc_gtt);

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: ' || I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   delete rpm_future_retail_gtt;
   delete rpm_promo_item_loc_expl_gtt;
   delete rpm_cust_segment_promo_fr_gtt;

   insert into rpm_future_retail_gtt (price_event_id,
                                      future_retail_id,
                                      dept,
                                      class,
                                      subclass,
                                      item,
                                      diff_id,
                                      item_parent,
                                      zone_node_type,
                                      location,
                                      zone_id,
                                      action_date,
                                      selling_retail,
                                      selling_retail_currency,
                                      selling_uom,
                                      multi_units,
                                      multi_unit_retail,
                                      multi_unit_retail_currency,
                                      multi_selling_uom,
                                      clear_retail,
                                      clear_retail_currency,
                                      clear_uom,
                                      simple_promo_retail,
                                      simple_promo_retail_currency,
                                      simple_promo_uom,
                                      price_change_id,
                                      price_change_display_id,
                                      pc_exception_parent_id,
                                      pc_change_type,
                                      pc_change_amount,
                                      pc_change_currency,
                                      pc_change_percent,
                                      pc_change_selling_uom,
                                      pc_null_multi_ind,
                                      pc_multi_units,
                                      pc_multi_unit_retail,
                                      pc_multi_unit_retail_currency,
                                      pc_multi_selling_uom,
                                      pc_price_guide_id,
                                      clearance_id,
                                      clearance_display_id,
                                      clear_mkdn_index,
                                      clear_start_ind,
                                      clear_change_type,
                                      clear_change_amount,
                                      clear_change_currency,
                                      clear_change_percent,
                                      clear_change_selling_uom,
                                      clear_price_guide_id,
                                      loc_move_from_zone_id,
                                      loc_move_to_zone_id,
                                      location_move_id,
                                      lock_version,
                                      rfr_rowid,
                                      on_simple_promo_ind,
                                      on_complex_promo_ind,
                                      max_hier_level,
                                      cur_hier_level,
                                      step_identifier)
      with iloc as
         (select /*+ ORDERED CARDINALITY(ids, 10) */
                 distinct rpi.dept,
                 rpi.price_event_id,
                 rpi.item,
                 rpi.diff_id,
                 rpl.location,
                 rpl.zone_node_type,
                 DENSE_RANK() OVER (PARTITION BY rpi.item,
                                                 rpi.diff_id,
                                                 rpl.zone_node_type,
                                                 rpl.location
                                        ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                 rpi.pe_merch_level desc,
                                                 rpi.price_event_id) rank
            from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                 rpm_bulk_cc_pe_item rpi,
                 rpm_bulk_cc_pe_location rpl
           where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
             and rpi.thread_number  = I_thread_number
             and rpi.chunk_number   = I_chunk_number
             and rpi.price_event_id = VALUE(ids)
             and rpl.price_event_id = VALUE(ids)
             and rpl.bulk_cc_pe_id  = I_bulk_cc_pe_id
             and rpl.price_event_id = rpi.price_event_id
             and rpl.itemloc_id     = rpi.itemloc_id
             and rownum             > 0)
      select /*+ ORDERED INDEX(fr, RPM_FUTURE_RETAIL_I1) */
             t.price_event_id,
             future_retail_id,
             fr.dept,
             class,
             subclass,
             fr.item,
             fr.diff_id,
             fr.item_parent,
             fr.zone_node_type,
             fr.location,
             fr.zone_id,
             action_date,
             selling_retail,
             selling_retail_currency,
             selling_uom,
             multi_units,
             multi_unit_retail,
             multi_unit_retail_currency,
             multi_selling_uom,
             clear_retail,
             clear_retail_currency,
             clear_uom,
             simple_promo_retail,
             simple_promo_retail_currency,
             simple_promo_uom,
             price_change_id,
             price_change_display_id,
             pc_exception_parent_id,
             pc_change_type,
             pc_change_amount,
             pc_change_currency,
             pc_change_percent,
             pc_change_selling_uom,
             pc_null_multi_ind,
             pc_multi_units,
             pc_multi_unit_retail,
             pc_multi_unit_retail_currency,
             pc_multi_selling_uom,
             pc_price_guide_id,
             clearance_id,
             clearance_display_id,
             clear_mkdn_index,
             clear_start_ind,
             clear_change_type,
             clear_change_amount,
             clear_change_currency,
             clear_change_percent,
             clear_change_selling_uom,
             clear_price_guide_id,
             loc_move_from_zone_id,
             loc_move_to_zone_id,
             location_move_id,
             lock_version,
             fr.rowid,
             fr.on_simple_promo_ind,
             fr.on_complex_promo_ind,
             fr.max_hier_level,
             fr.cur_hier_level,
             RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR
        from iloc t,
             rpm_future_retail fr
       where t.rank                  = 1
         and fr.dept                 = t.dept
         and fr.item                 = t.item
         and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
         and fr.location             = t.location
         and fr.zone_node_type       = t.zone_node_type;

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_thread_number: '||I_thread_number ||
                                      ' - I_chunk_number: '||I_chunk_number ||
                                      ' - Insert into rpm_future_retail_gtt - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_POP_GTT_RFR,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   -- Use rpm_item_loc_gtt to store what timeline already exists in Future Retail
   delete rpm_item_loc_gtt;

   insert into rpm_item_loc_gtt (price_event_id,
                                 item,
                                 diff_id,
                                 location,
                                 regular_zone_group)  -- Note that this is used as zone_node_type
      select t.price_event_id,
             t.item,
             t.diff_id,
             t.location,
             t.zone_node_type
        from (select /*+ CARDINALITY (ids, 10) */
                     fr.price_event_id,
                     fr.item,
                     fr.diff_id,
                     fr.location,
                     fr.zone_node_type,
                     ROW_NUMBER() OVER(PARTITION BY fr.price_event_id,
                                                    fr.item,
                                                    fr.diff_id,
                                                    fr.location,
                                                    fr.zone_node_type
                                           ORDER BY fr.action_date desc) rank
                from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                     rpm_future_retail_gtt fr
               where fr.price_event_id = VALUE(ids)) t
       where t.rank = 1;

   open C_NO_FR;
   fetch C_NO_FR BULK COLLECT into L_pe_no_frs;
   close C_NO_FR;

   if L_pe_no_frs is NOT NULL and
      L_pe_no_frs.COUNT > 0 then

      -- Populate FR GTT for Price Events that are defined at the level that there is no FR record yet.
      -- By copying existing parent level Future Retail records. Need to populate the FUTURE_RETAIL_ID
      -- column from the sequence but should leave the rfr_rowid to NULL so it will be pushed back

      if GENERATE_TIMELINE(O_cc_error_tbl,
                           I_bulk_cc_pe_id,
                           I_thread_number,
                           I_chunk_number,
                           L_pe_no_frs) != 1 then
         return 0;
      end if;

      -- Need to verify if a child timeline needs to be generated due to a new intersection.
      if GENERATE_TL_FROM_INTERSECTION(O_cc_error_tbl,
                                       I_bulk_cc_pe_id,
                                       I_thread_number,
                                       I_chunk_number,
                                       I_price_event_ids) != 1 then
         return 0;
      end if;

   end if;

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      -- get all promo_item_loc_expl data where it's not cust segment
      -- specific - i.e., all cust segment promos could be affected regardless
      -- of the customer type on the promo

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               timebased_dtl_ind,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               diff_id,
                                               item_parent,
                                               zone_id,
                                               cur_hier_level,
                                               max_hier_level,
                                               step_identifier)
         select /*+ ORDERED INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(ilex) */
                t.price_event_id,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.zone_node_type,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.timebased_dtl_ind,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                ilex.diff_id,
                ilex.item_parent,
                ilex.zone_id,
                ilex.cur_hier_level,
                ilex.max_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE_NON_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                ROW_NUMBER() OVER (PARTITION BY rpi.dept,
                                                                rpi.price_event_id,
                                                                rpi.item,
                                                                rpi.diff_id,
                                                                rpl.location,
                                                                rpl.zone_node_type
                                                       ORDER BY rpi.item) rank_value
                           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                            and rpi.thread_number  = I_thread_number
                            and rpi.chunk_number   = I_chunk_number
                            and rpi.price_event_id = VALUE(ids)
                            and rpl.price_event_id = VALUE(ids)
                            and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id = rpi.price_event_id
                            and rpl.itemloc_id     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_promo_item_loc_expl ilex
          where ilex.dept                 = t.dept
            and ilex.item                 = t.item
            and NVL(ilex.diff_id, '-999') = NVL(t.diff_id, '-999')
            and ilex.location             = t.location
            and ilex.zone_node_type       = t.zone_node_type;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - I_chunk_number: '|| I_chunk_number ||
                                         ' - Insert into rpm_promo_item_loc_expl_gtt for non-promo price events - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE_NON_PR,
                                                      0, -- RFR
                                                      1, -- RPILE
                                                      0, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if GEN_PILE_TL(O_cc_error_tbl,
                     I_bulk_cc_pe_id,
                     I_thread_number,
                     I_chunk_number,
                     I_price_event_type,
                     L_pe_no_frs,
                     RPM_CONSTANTS.CAPT_GTT_POP_GTT_GEN_PILE_TL) = 0 then
         return 0;
      end if;

      -- Get all records from cust segment promo fr where the customer
      -- type doesn't matter - i.e. all customer types could be affected
      -- by the price event...

      insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                                 cust_segment_promo_id,
                                                 item,
                                                 location,
                                                 zone_node_type,
                                                 action_date,
                                                 customer_type,
                                                 dept,
                                                 promo_retail,
                                                 promo_retail_currency,
                                                 promo_uom,
                                                 complex_promo_ind,
                                                 cspfr_rowid,
                                                 diff_id,
                                                 item_parent,
                                                 zone_id,
                                                 cur_hier_level,
                                                 max_hier_level,
                                                 step_identifier)
         select /*+ ORDERED */
                t.price_event_id,
                cspfr.cust_segment_promo_id,
                cspfr.item,
                cspfr.location,
                cspfr.zone_node_type,
                cspfr.action_date,
                cspfr.customer_type,
                cspfr.dept,
                cspfr.promo_retail,
                cspfr.promo_retail_currency,
                cspfr.promo_uom,
                cspfr.complex_promo_ind,
                cspfr.rowid,
                cspfr.diff_id,
                cspfr.item_parent,
                cspfr.zone_id,
                cspfr.cur_hier_level,
                cspfr.max_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_NON_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                ROW_NUMBER() OVER (PARTITION BY rpi.dept,
                                                                rpi.price_event_id,
                                                                rpi.item,
                                                                rpi.diff_id,
                                                                rpl.location,
                                                                rpl.zone_node_type
                                                       ORDER BY rpi.item) rank_value
                           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id  = I_bulk_cc_pe_id
                            and rpi.thread_number  = I_thread_number
                            and rpi.chunk_number   = I_chunk_number
                            and rpi.price_event_id = VALUE(ids)
                            and rpl.price_event_id = VALUE(ids)
                            and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id = rpi.price_event_id
                            and rpl.itemloc_id     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_cust_segment_promo_fr cspfr
          where cspfr.dept                 = t.dept
            and cspfr.item                 = t.item
            and NVL(cspfr.diff_id, '-999') = NVL(t.diff_id, '-999')
            and cspfr.location             = t.location
            and cspfr.zone_node_type       = t.zone_node_type;

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_NON_PR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      1, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      -- Use rpm_item_loc_gtt to store what timeline already exists in Future Retail
      delete rpm_item_loc_gtt;

      insert into rpm_item_loc_gtt (price_event_id,
                                    item,
                                    diff_id,
                                    location,
                                    item_parent,         -- Note that this is used as customer_type
                                    regular_zone_group)  -- Note that this is used as zone_node_type
         select t.price_event_id,
                t.item,
                t.diff_id,
                t.location,
                t.customer_type,
                t.zone_node_type
           from (select /*+ CARDINALITY (ids, 10) */
                        fr.price_event_id,
                        fr.item,
                        fr.diff_id,
                        fr.location,
                        fr.customer_type,
                        fr.zone_node_type,
                        ROW_NUMBER() OVER (PARTITION BY fr.price_event_id,
                                                        fr.customer_type,
                                                        fr.item,
                                                        fr.diff_id,
                                                        fr.location,
                                                        fr.zone_node_type
                                               ORDER BY fr.action_date desc) rank
                   from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                        rpm_cust_segment_promo_fr_gtt fr
                  where fr.price_event_id = VALUE(ids)) t
          where t.rank = 1;

      if GENERATE_CSPFR_TIMELINE(O_cc_error_tbl,
                                 I_bulk_cc_pe_id,
                                 I_thread_number,
                                 I_chunk_number,
                                 I_price_event_type,
                                 I_price_event_ids,
                                 RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_NPR) = 0 then
         return 0;
      end if;

      if GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl,
                               I_bulk_cc_pe_id,
                               I_thread_number,
                               I_chunk_number,
                               I_price_event_type,
                               I_price_event_ids,
                               RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_INT_NON_PR) = 0 then
         return 0;
      end if;

   end if;

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE) then

      -- Get all promo item loc expl records for regular simple,
      -- threshold and complex promos and updates

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               timebased_dtl_ind,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               diff_id,
                                               item_parent,
                                               zone_id,
                                               cur_hier_level,
                                               max_hier_level,
                                               step_identifier)
         select /*+ ORDERED INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(ilex) */
                t.price_event_id,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.zone_node_type,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.timebased_dtl_ind,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                ilex.diff_id,
                ilex.item_parent,
                ilex.zone_id,
                ilex.cur_hier_level,
                ilex.max_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                RANK() OVER (PARTITION BY rpi.item,
                                                          rpi.diff_id,
                                                          rpl.zone_node_type,
                                                          rpl.location
                                                 ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                          rpi.pe_merch_level desc,
                                                          rpi.price_event_id) rank_value
                           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                            and rpi.thread_number                  = I_thread_number
                            and rpi.chunk_number                   = I_chunk_number
                            and NVL(rpi.txn_man_excluded_item, 0) != 1
                            and rpi.price_event_id                 = VALUE(ids)
                            and rpl.price_event_id                 = VALUE(ids)
                            and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id                 = rpi.price_event_id
                            and rpl.itemloc_id                     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_promo_item_loc_expl ilex
          where ilex.dept                 = t.dept
            and ilex.item                 = t.item
            and NVL(ilex.diff_id, '-999') = NVL(t.diff_id, '-999')
            and ilex.location             = t.location
            and ilex.zone_node_type       = t.zone_node_type
            and ilex.customer_type        is NULL;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - I_chunk_number: '|| I_chunk_number ||
                                         ' - Insert into rpm_promo_item_loc_expl_gtt for non customer segment promo events - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_RPILE,
                                                      0, -- RFR
                                                      1, -- RPILE
                                                      0, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if GEN_PILE_TL(O_cc_error_tbl,
                     I_bulk_cc_pe_id,
                     I_thread_number,
                     I_chunk_number,
                     I_price_event_type,
                     L_pe_no_frs,
                     RPM_CONSTANTS.CAPT_GTT_GEN_PILE_PR) = 0 then
         return 0;
      end if;

   end if;

   if I_price_event_type  IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                              RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) then

      -- Get all promo_item_loc_expl data for the scenario where dealing with
      -- a specific customer type...

      insert into rpm_promo_item_loc_expl_gtt (price_event_id,
                                               promo_item_loc_expl_id,
                                               item,
                                               dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               promo_id,
                                               promo_display_id,
                                               promo_secondary_ind,
                                               promo_comp_id,
                                               comp_display_id,
                                               promo_dtl_id,
                                               type,
                                               customer_type,
                                               detail_secondary_ind,
                                               detail_start_date,
                                               detail_end_date,
                                               detail_apply_to_code,
                                               timebased_dtl_ind,
                                               detail_change_type,
                                               detail_change_amount,
                                               detail_change_currency,
                                               detail_change_percent,
                                               detail_change_selling_uom,
                                               detail_price_guide_id,
                                               exception_parent_id,
                                               rpile_rowid,
                                               diff_id,
                                               item_parent,
                                               zone_id,
                                               cur_hier_level,
                                               max_hier_level,
                                               step_identifier)
         select /*+ ORDERED INDEX(ilex, RPM_PROMO_ITEM_LOC_EXPL_I1) USE_NL(ilex) */
                t.price_event_id,
                ilex.promo_item_loc_expl_id,
                ilex.item,
                ilex.dept,
                ilex.class,
                ilex.subclass,
                ilex.location,
                ilex.zone_node_type,
                ilex.promo_id,
                ilex.promo_display_id,
                ilex.promo_secondary_ind,
                ilex.promo_comp_id,
                ilex.comp_display_id,
                ilex.promo_dtl_id,
                ilex.type,
                ilex.customer_type,
                ilex.detail_secondary_ind,
                ilex.detail_start_date,
                ilex.detail_end_date,
                ilex.detail_apply_to_code,
                ilex.timebased_dtl_ind,
                ilex.detail_change_type,
                ilex.detail_change_amount,
                ilex.detail_change_currency,
                ilex.detail_change_percent,
                ilex.detail_change_selling_uom,
                ilex.detail_price_guide_id,
                ilex.exception_parent_id,
                ilex.rowid,
                ilex.diff_id,
                ilex.item_parent,
                ilex.zone_id,
                ilex.cur_hier_level,
                ilex.max_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_PILE_CS_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                RANK() OVER (PARTITION BY rpi.item,
                                                          rpi.diff_id,
                                                          rpl.zone_node_type,
                                                          rpl.location
                                                 ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                          rpi.pe_merch_level desc,
                                                          rpi.price_event_id) rank_value
                           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                            and rpi.thread_number                  = I_thread_number
                            and rpi.chunk_number                   = I_chunk_number
                            and NVL(rpi.txn_man_excluded_item, 0) != 1
                            and rpi.price_event_id                 = VALUE(ids)
                            and rpl.price_event_id                 = VALUE(ids)
                            and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id                 = rpi.price_event_id
                            and rpl.itemloc_id                     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_promo_item_loc_expl ilex,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where ilex.dept                 = t.dept
            and ilex.item                 = t.item
            and NVL(ilex.diff_id, '-999') = NVL(t.diff_id, '-999')
            and ilex.location             = t.location
            and ilex.zone_node_type       = t.zone_node_type
            and ilex.customer_type        is NOT NULL
            and t.price_event_id          = rpd.promo_dtl_id
            and rpd.promo_comp_id         = rpc.promo_comp_id
            and rpc.customer_type         = ilex.customer_type;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - I_chunk_number: '|| I_chunk_number ||
                                         ' - Insert into rpm_promo_item_loc_expl_gtt for customer segment promo events - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_PILE_CS_PR,
                                                      0, -- RFR
                                                      1, -- RPILE
                                                      0, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      -- Get all cust seg promo fr data where it's for a specific
      -- customer type...

      insert into rpm_cust_segment_promo_fr_gtt (price_event_id,
                                                 cust_segment_promo_id,
                                                 item,
                                                 location,
                                                 zone_node_type,
                                                 action_date,
                                                 customer_type,
                                                 dept,
                                                 promo_retail,
                                                 promo_retail_currency,
                                                 promo_uom,
                                                 complex_promo_ind,
                                                 cspfr_rowid,
                                                 diff_id,
                                                 item_parent,
                                                 zone_id,
                                                 cur_hier_level,
                                                 max_hier_level,
                                                 step_identifier)
         select /*+ ORDERED */
                t.price_event_id,
                cspfr.cust_segment_promo_id,
                cspfr.item,
                cspfr.location,
                cspfr.zone_node_type,
                cspfr.action_date,
                cspfr.customer_type,
                cspfr.dept,
                cspfr.promo_retail,
                cspfr.promo_retail_currency,
                cspfr.promo_uom,
                cspfr.complex_promo_ind,
                cspfr.rowid,
                cspfr.diff_id,
                cspfr.item_parent,
                cspfr.zone_id,
                cspfr.cur_hier_level,
                cspfr.max_hier_level,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_CS_PR
           from (select s.dept,
                        s.price_event_id,
                        s.item,
                        s.diff_id,
                        s.location,
                        s.zone_node_type
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                distinct rpi.dept,
                                rpi.price_event_id,
                                rpi.item,
                                rpi.diff_id,
                                rpl.location,
                                rpl.zone_node_type,
                                RANK() OVER (PARTITION BY rpi.item,
                                                          rpi.diff_id,
                                                          rpl.zone_node_type,
                                                          rpl.location
                                                 ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                          rpi.pe_merch_level desc,
                                                          rpi.price_event_id) rank_value
                           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                            and rpi.thread_number                  = I_thread_number
                            and rpi.chunk_number                   = I_chunk_number
                            and NVL(rpi.txn_man_excluded_item, 0) != 1
                            and rpi.price_event_id                 = VALUE(ids)
                            and rpl.price_event_id                 = VALUE(ids)
                            and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                            and rpl.price_event_id                 = rpi.price_event_id
                            and rpl.itemloc_id                     = rpi.itemloc_id) s
                  where s.rank_value = 1) t,
                rpm_cust_segment_promo_fr cspfr,
                rpm_promo_dtl rpd,
                rpm_promo_comp rpc
          where cspfr.dept                 = t.dept
            and cspfr.item                 = t.item
            and NVL(cspfr.diff_id, '-999') = NVL(t.diff_id, '-999')
            and cspfr.location             = t.location
            and cspfr.zone_node_type       = t.zone_node_type
            and cspfr.customer_type        is NOT NULL
            and t.price_event_id           = rpd.promo_dtl_id
            and rpd.promo_comp_id          = rpc.promo_comp_id
            and cspfr.customer_type        = rpc.customer_type;

      LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '||I_price_event_type ||
                                         ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                         ' - I_thread_number: '||I_thread_number ||
                                         ' - I_chunk_number: '|| I_chunk_number ||
                                         ' - Insert into rpm_cust_segment_promo_fr_gtt for customer segment promo events - SQL%ROWCOUNT: '|| SQL%ROWCOUNT);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CSPFR_CS_PR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      1, -- CSPFR
                                                      0, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

      if GEN_PILE_TL(O_cc_error_tbl,
                     I_bulk_cc_pe_id,
                     I_thread_number,
                     I_chunk_number,
                     I_price_event_type,
                     L_pe_no_frs,
                     RPM_CONSTANTS.CAPT_GTT_GEN_PILE_CSPFR) != 1 then
         return 0;
      end if;

      -- Use rpm_item_loc_gtt to store what timeline already exists in Future Retail
      delete rpm_item_loc_gtt;

      insert into rpm_item_loc_gtt (price_event_id,
                                    item,
                                    diff_id,
                                    location,
                                    item_parent,         -- Note that this is used as customer_type
                                    regular_zone_group)  -- Note that this is used as zone_node_type
         select t.price_event_id,
                t.item,
                t.diff_id,
                t.location,
                t.customer_type,
                t.zone_node_type
           from (select /*+ CARDINALITY (ids, 10) */
                        fr.price_event_id,
                        fr.item,
                        fr.diff_id,
                        fr.location,
                        fr.customer_type,
                        fr.zone_node_type,
                        ROW_NUMBER() OVER (PARTITION BY fr.price_event_id,
                                                        fr.customer_type,
                                                        fr.item,
                                                        fr.diff_id,
                                                        fr.location,
                                                        fr.zone_node_type
                                               ORDER BY fr.action_date desc) rank
                   from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                        rpm_cust_segment_promo_fr_gtt fr
                  where fr.price_event_id = VALUE(ids)) t
          where t.rank = 1;

      if GENERATE_CSPFR_TIMELINE(O_cc_error_tbl,
                                 I_bulk_cc_pe_id,
                                 I_thread_number,
                                 I_chunk_number,
                                 I_price_event_type,
                                 I_price_event_ids,
                                 RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_PR) = 0 then
         return 0;
      end if;

      if GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl,
                               I_bulk_cc_pe_id,
                               I_thread_number,
                               I_chunk_number,
                               I_price_event_type,
                               I_price_event_ids,
                               RPM_CONSTANTS.CAPT_GTT_GEN_CSPFR_INT_CS_PR) = 0 then
         return 0;
      end if;

   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      delete from rpm_clearance_gtt;

      insert into rpm_clearance_gtt (price_event_id,
                                     clearance_id,
                                     clearance_display_id,
                                     state,
                                     reason_code,
                                     exception_parent_id,
                                     reset_ind,
                                     item,
                                     diff_id,
                                     zone_id,
                                     location,
                                     zone_node_type,
                                     effective_date,
                                     out_of_stock_date,
                                     reset_date,
                                     change_type,
                                     change_amount,
                                     change_currency,
                                     change_percent,
                                     change_selling_uom,
                                     price_guide_id,
                                     vendor_funded_ind,
                                     funding_type,
                                     funding_amount,
                                     funding_amount_currency,
                                     funding_percent,
                                     supplier,
                                     deal_id,
                                     deal_detail_id,
                                     partner_type,
                                     partner_id,
                                     create_date,
                                     create_id,
                                     approval_date,
                                     approval_id,
                                     lock_version,
                                     rc_rowid,
                                     skulist,
                                     step_identifier)
         select /*+ CARDINALITY(ids 10) INDEX(rc PK_RPM_CLEARANCE) */
                VALUE(ids),
                rc.clearance_id,
                rc.clearance_display_id,
                rc.state,
                rc.reason_code,
                rc.exception_parent_id,
                0, -- reset_ind
                rc.item,
                rc.diff_id,
                rc.zone_id,
                rc.location,
                rc.zone_node_type,
                rc.effective_date,
                rc.out_of_stock_date,
                rc.reset_date,
                rc.change_type,
                rc.change_amount,
                rc.change_currency,
                rc.change_percent,
                rc.change_selling_uom,
                rc.price_guide_id,
                rc.vendor_funded_ind,
                rc.funding_type,
                rc.funding_amount,
                rc.funding_amount_currency,
                rc.funding_percent,
                rc.supplier,
                rc.deal_id,
                rc.deal_detail_id,
                rc.partner_type,
                rc.partner_id,
                rc.create_date,
                rc.create_id,
                rc.approval_date,
                rc.approval_id,
                rc.lock_version,
                rc.rowid,
                rc.skulist,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR
           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                rpm_clearance rc
          where rc.clearance_id = VALUE(ids);

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then

      delete from rpm_clearance_gtt;

      insert into rpm_clearance_gtt (price_event_id,
                                     clearance_id,
                                     clearance_display_id,
                                     state,
                                     reason_code,
                                     reset_ind,
                                     item,
                                     location,
                                     zone_node_type,
                                     effective_date,
                                     out_of_stock_date,
                                     change_type,
                                     vendor_funded_ind,
                                     create_date,
                                     create_id,
                                     approval_date,
                                     approval_id,
                                     lock_version,
                                     rc_rowid,
                                     step_identifier)
         select /*+ CARDINALITY(ids 10) */
                t.price_event_id,
                rc.clearance_id,
                rc.clearance_display_id,
                rc.state,
                rc.reason_code,
                1, -- reset_ind
                t.item,
                t.location,
                rc.zone_node_type,
                rc.effective_date,
                rc.out_of_stock_date,
                -9, -- change_type
                0,  -- vendor_funded_ind
                rc.create_date,
                rc.create_id,
                rc.approval_date,
                rc.approval_id,
                rc.lock_version,
                rc.rowid,
                RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR_PC
           from (select s.price_event_id,
                        s.item,
                        s.location
                   from (select /*+ LEADING(ids) USE_NL(ids) ORDERED INDEX(rpl RPM_BULK_CC_PE_LOCATION_I1) */
                                rpi.price_event_id,
                                rpi.item,
                                rpl.location,
                                ROW_NUMBER() OVER (PARTITION BY rpi.price_event_id,
                                                                rpi.item,
                                                                rpl.location
                                                       ORDER BY rpi.item) rank_value
                           from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                rpm_bulk_cc_pe_item rpi,
                                rpm_bulk_cc_pe_location rpl
                          where rpi.price_event_id   = VALUE(ids)
                            and rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                            and rpi.thread_number    = I_thread_number
                            and rpi.chunk_number     = I_chunk_number
                            and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                            and rpl.price_event_id   = VALUE(ids)
                            and rpl.bulk_cc_pe_id    = I_bulk_cc_pe_id
                            and rpl.price_event_id   = rpi.price_event_id
                            and rpl.itemloc_id       = rpi.itemloc_id
                            and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)) s
                  where s.rank_value = 1) t,
                rpm_clearance_reset rc
          where rc.item     = t.item
            and rc.location = t.location;

      if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                      RPM_CONSTANTS.CAPT_GTT_POP_GTT_CLR_PC,
                                                      0, -- RFR
                                                      0, -- RPILE
                                                      0, -- CSPFR
                                                      1, -- CLR
                                                      0) = 0 then -- RFR_IL_EXPL
         O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                           CONFLICT_CHECK_ERROR_REC(NULL,
                                                    NULL,
                                                    RPM_CONSTANTS.PLSQL_ERROR,
                                                    L_error_message));
         return 0;

      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: ' || I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END POPULATE_GTT;
--------------------------------------------------------------------------------
FUNCTION PROCESS_PRICE_CHANGES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id     IN     NUMBER,
                               I_persist_ind      IN     VARCHAR2,
                               I_start_state      IN     VARCHAR2,
                               I_end_state        IN     VARCHAR2,
                               I_user_name        IN     VARCHAR2,
                               I_pe_sequence_id   IN     NUMBER,
                               I_pe_thread_number IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_PRICE_CHANGES';

   L_area_diff_ind NUMBER(1) := NULL;
   L_start_time    TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_pe_sequence_id: ' || I_pe_sequence_id ||
                                      ' - I_thread_number: '|| I_pe_thread_number ||
                                      ' - I_chunk_number: ' || LP_chunk_number);

   if I_end_state IN (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                      RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE) or
      I_persist_ind != 'Y' then

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                  LP_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  NULL,
                                                  0,
                                                  NULL,
                                                  LP_bulk_cc_pe_id,
                                                  NULL, -- pe_sequence_id
                                                  NULL, -- pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then

         return 0;
      end if;

      if O_cc_error_tbl is NULL or
         O_cc_error_tbl.COUNT = 0 then

         select area_diff_ind into L_area_diff_ind
           from rpm_bulk_cc_pe_thread
          where bulk_cc_pe_id        = LP_bulk_cc_pe_id
            and parent_thread_number = I_pe_sequence_id
            and thread_number        = I_pe_thread_number;

         if L_area_diff_ind = 1 then

            insert into rpm_chunk_area_diff (bulk_cc_pe_id,
                                             chunk_number,
                                             avg_retail,
                                             min_uom,
                                             price_change_id,
                                             rec_count,
                                             item,
                                             location)
               select LP_bulk_cc_pe_id,
                      LP_chunk_number,
                      AVG(selling_retail),
                      MIN(selling_uom),
                      price_change_id,
                      COUNT(1),
                      item,
                      location
                 from (select /*+ CARDINALITY (ids 10) */
                              price_change_id,
                              item,
                              location,
                              selling_retail,
                              selling_uom,
                              RANK() OVER (PARTITION BY price_change_id,
                                                        item,
                                                        location
                                               ORDER BY promo_dtl_id) fr_rank
                         from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                              rpm_fr_item_loc_expl_gtt fr
                        where fr.price_change_id = VALUE(ids))
                where fr_rank = 1
                group by price_change_id,
                         item,
                         location;

         end if;
      end if;

   elsif I_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE then
      --
      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE(O_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  LP_bulk_cc_pe_id,
                                                  NULL, --I_pe_sequence_id
                                                  NULL, --I_pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then
         return 0;
      end if;
   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_pe_sequence_id: ' || I_pe_sequence_id ||
                               ' - I_thread_number: '|| I_pe_thread_number ||
                               ' - I_chunk_number: ' || LP_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PRICE_CHANGES;
--------------------------------------------------------------------------------
FUNCTION PROCESS_CLEARANCES(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                            I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE,
                            I_rib_trans_id         IN     NUMBER,
                            I_persist_ind          IN     VARCHAR2,
                            I_start_state          IN     VARCHAR2,
                            I_end_state            IN     VARCHAR2,
                            I_user_name            IN     VARCHAR2,
                            I_bulk_cc_pe_id        IN     NUMBER,
                            I_parent_thread_number IN     NUMBER,
                            I_pe_thread_number     IN     NUMBER,
                            I_chunk_number         IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_CLEARANCES';

   L_specific_item_loc NUMBER    := 1;
   L_start_time        TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: ' || I_parent_thread_number ||
                                      ' - I_thread_number: '|| I_pe_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   if NVL(LP_need_il_explode_ind, 0) = 1 then
      L_specific_item_loc := 0;
   else
      L_specific_item_loc := 1;
   end if;

   if I_end_state in (RPM_CONSTANTS.PC_APPROVED_STATE_CODE,
                      RPM_CONSTANTS.PC_SUBMITTED_STATE_CODE,
                      RPM_CONSTANTS.PC_EXECUTED_STATE_CODE) or
      I_persist_ind != 'Y' then

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                  LP_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  L_specific_item_loc,
                                                  0,
                                                  NULL,
                                                  I_bulk_cc_pe_id,
                                                  I_parent_thread_number,
                                                  I_pe_thread_number,
                                                  I_chunk_number,
                                                  I_end_state) = 0 then
         return 0;

      end if;

   elsif I_end_state = RPM_CONSTANTS.PC_WORKSHEET_STATE_CODE then

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE(O_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  LP_bulk_cc_pe_id,
                                                  NULL, --I_pe_sequence_id
                                                  NULL, --I_pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then
         return 0;

      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: ' || I_parent_thread_number ||
                               ' - I_thread_number: '|| I_pe_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CLEARANCES;
--------------------------------------------------------------------------------

FUNCTION PROCESS_CLEARANCE_RESETS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                                  I_price_event_ids      IN     OBJ_NUMERIC_ID_TABLE,
                                  I_rib_trans_id         IN     NUMBER,
                                  I_persist_ind          IN     VARCHAR2,
                                  I_start_state          IN     VARCHAR2,
                                  I_end_state            IN     VARCHAR2,
                                  I_user_name            IN     VARCHAR2,
                                  I_bulk_cc_pe_id        IN     NUMBER,
                                  I_parent_thread_number IN     NUMBER,
                                  I_pe_thread_number     IN     NUMBER,
                                  I_chunk_number         IN     NUMBER,
                                  I_old_clr_oostock_date IN     DATE DEFAULT NULL,
                                  I_old_clr_reset_date   IN     DATE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_CLEARANCE_RESETS';

   L_specific_item_loc NUMBER    := 1;
   L_start_time        TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: ' || I_parent_thread_number ||
                                      ' - I_thread_number: '|| I_pe_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   if NVL(LP_need_il_explode_ind, 0) = 1 then
      L_specific_item_loc := 0;
   else
      L_specific_item_loc := 1;
   end if;

   if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                               LP_cc_error_tbl,
                                               I_price_event_ids,
                                               LP_price_event_type,
                                               I_rib_trans_id,
                                               I_persist_ind,
                                               L_specific_item_loc,
                                               0,
                                               NULL,
                                               I_bulk_cc_pe_id,
                                               I_parent_thread_number,
                                               I_pe_thread_number,
                                               I_chunk_number,
                                               I_end_state) = 0 then
      return 0;
   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| I_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: ' || I_parent_thread_number ||
                               ' - I_thread_number: '|| I_pe_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_CLEARANCE_RESETS;
--------------------------------------------------------------------------------

FUNCTION PROCESS_PROMO_DETAILS(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE,
                               I_rib_trans_id    IN     NUMBER,
                               I_persist_ind     IN     VARCHAR2,
                               I_start_state     IN     VARCHAR2,
                               I_end_state       IN     VARCHAR2,
                               I_user_name       IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_PROMO_DETAILS';

   L_price_event_type VARCHAR2(3) := NULL;
   L_start_time       TIMESTAMP   := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - LP_chunk_number: ' || LP_chunk_number);

   if I_end_state IN (RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE,
                      RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                      RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) or
      I_persist_ind != 'Y' then

      -- Do the conflict checking
      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                  LP_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  NULL,
                                                  0,
                                                  NULL,
                                                  LP_bulk_cc_pe_id,
                                                  NULL, -- pe_sequence_id
                                                  NULL, -- pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then
         return 0;
      end if;

   elsif I_end_state = RPM_CONSTANTS.PR_WORKSHEET_STATE_CODE then

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_REMOVE(O_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  LP_bulk_cc_pe_id,
                                                  NULL, --I_pe_sequence_id
                                                  NULL, --I_pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then
         return 0;

      end if;

   elsif I_end_state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then

      if LP_price_event_type = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE;
      elsif LP_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO then
         L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE;

      end if;

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                  LP_cc_error_tbl,
                                                  I_price_event_ids,
                                                  L_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  0,        --I_specific_item_loc
                                                  NULL,     --I_new_item_loc
                                                  LP_vdate, --I_new_promo_end_date
                                                  LP_bulk_cc_pe_id,
                                                  NULL, --I_pe_sequence_id
                                                  NULL, --I_pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then
         return 0;

      end if;

   end if;

   LOGGER.LOG_TIME(L_program ||' - LP_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - LP_chunk_number: ' || LP_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PROMO_DETAILS;

--------------------------------------------------------------------------------
FUNCTION PROCESS_UPDATE_PROMO_DETAILS(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                                      I_price_event_ids   IN     OBJ_NUMERIC_ID_TABLE,
                                      I_rib_trans_id      IN     NUMBER,
                                      I_persist_ind       IN     VARCHAR2,
                                      I_start_state       IN     VARCHAR2,
                                      I_end_state         IN     VARCHAR2,
                                      I_user_name         IN     VARCHAR2,
                                      I_promo_end_date    IN     DATE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_UPDATE_PROMO_DETAILS';

BEGIN

   if I_end_state IN (RPM_CONSTANTS.PR_SUBMITTED_STATE_CODE,
                      RPM_CONSTANTS.PR_APPROVED_STATE_CODE,
                      RPM_CONSTANTS.PR_ACTIVE_STATE_CODE) or
      I_persist_ind != 'Y' then

      if RPM_FUTURE_RETAIL_SQL.PRICE_EVENT_INSERT(O_cc_error_tbl,
                                                  LP_cc_error_tbl,
                                                  I_price_event_ids,
                                                  LP_price_event_type,
                                                  I_rib_trans_id,
                                                  I_persist_ind,
                                                  0,
                                                  0,
                                                  I_promo_end_date,
                                                  LP_bulk_cc_pe_id,
                                                  NULL, -- pe_sequence_id
                                                  NULL, -- pe_thread_number
                                                  LP_chunk_number,
                                                  I_end_state) = 0 then
         return 0;
      end if;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_UPDATE_PROMO_DETAILS;

--------------------------------------------------------------------------------
FUNCTION PROCESS_PRICE_EVENTS(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                              I_bulk_cc_pe_id           IN     NUMBER,
                              I_parent_thread_number    IN     NUMBER,
                              I_thread_number           IN     NUMBER,
                              I_chunk_number            IN     NUMBER,
                              I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type        IN     VARCHAR2,
                              I_rib_trans_id            IN     NUMBER,
                              I_persist_ind             IN     VARCHAR2,
                              I_start_state             IN     VARCHAR2,
                              I_end_state               IN     VARCHAR2,
                              I_user_name               IN     VARCHAR2,
                              I_emergency_perm          IN     NUMBER DEFAULT 0,
                              I_secondary_bulk_cc_pe_id IN     NUMBER,
                              I_secondary_ind           IN     NUMBER)
RETURN NUMBER IS

   L_program    VARCHAR2(45) := 'RPM_CHUNK_CC_ACTIONS_SQL.PROCESS_PRICE_EVENTS';
   L_trace_name VARCHAR2(10) := 'PPE';

   L_start_time         TIMESTAMP                              := SYSTIMESTAMP;
   L_new_promo_end_date RPM_BULK_CC_PE.NEW_PROMO_END_DATE%TYPE := NULL;
   L_cc_error_tbl       CONFLICT_CHECK_ERROR_TBL               := NULL;
   L_price_event_type   VARCHAR2(3)                            := NULL;

   L_old_clr_out_of_stock_date DATE := NULL;
   L_old_clr_reset_date        DATE := NULL;

   --Instrumentation
   L_return_code NUMBER(1)      := NULL;
   L_error_msg   VARCHAR2(2000) := NULL;

   cursor C_BULK_CC_PE is
      select need_il_explode_ind,
             new_promo_end_date,
             old_clr_out_of_stock_date,
             old_clr_reset_date
        from rpm_bulk_cc_pe
       where bulk_cc_pe_id = I_bulk_cc_pe_id;

BEGIN

   LP_price_event_type := I_price_event_type;
   LP_bulk_cc_pe_id    := I_bulk_cc_pe_id;
   LP_chunk_number     := I_chunk_number;

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || LP_chunk_number);

   LP_batch_name  :=  case 
                        when REGEXP_LIKE (I_user_name, '[Nn]ew') then
                           'NIL'
                        when REGEXP_LIKE (I_user_name, '[Ii]njector') then
                           'INJ'
                        when REGEXP_LIKE (I_user_name, '[Ll]oc') then
                           'LM'
                      end;                                      

   --Instrumentation
   BEGIN

      if REGEXP_LIKE(I_user_name, '[Ii]njector|[Ll]oc') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(L_return_code,
                                           L_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program||'-'||I_bulk_cc_pe_id||'-'||I_parent_thread_number||'-'||I_thread_number||'-'||LP_chunk_number,
                                           RPM_CONSTANTS.LOG_STARTED,
                                           'Start of '||L_program||' for '||I_price_event_type);

            RPM_APP_COMM_UTILITY_SQL.ENABLE_TRACE(LP_batch_name||L_trace_name||'-'||LP_chunk_number||'-'||I_parent_thread_number||'-'||I_thread_number);

      end if;

   EXCEPTION

      when OTHERS then
         goto PROCESS_PRICE_EVENTS_1;

   END;

   <<PROCESS_PRICE_EVENTS_1>>

   open C_BULK_CC_PE;
   fetch C_BULK_CC_PE into LP_need_il_explode_ind,
                           L_new_promo_end_date,
                           L_old_clr_out_of_stock_date,
                           L_old_clr_reset_date;
   close C_BULK_CC_PE;

   if POPULATE_GTT(O_cc_error_tbl,
                   I_bulk_cc_pe_id,
                   I_thread_number,
                   I_chunk_number,
                   I_price_event_ids,
                   I_price_event_type) = 0 then
      return 0;
   end if;

   if I_price_event_type = RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE then
      if PROCESS_PRICE_CHANGES(O_cc_error_tbl,
                               I_price_event_ids,
                               I_rib_trans_id,
                               I_persist_ind,
                               I_start_state,
                               I_end_state,
                               I_user_name,
                               I_parent_thread_number,
                               I_thread_number) = 0 then
         return 0;
      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE then

      if PROCESS_CLEARANCES(O_cc_error_tbl,
                            I_price_event_ids,
                            I_rib_trans_id,
                            I_persist_ind,
                            I_start_state,
                            I_end_state,
                            I_user_name,
                            I_bulk_cc_pe_id,
                            I_parent_thread_number,
                            I_thread_number,
                            I_chunk_number) = 0 then
         return 0;
      end if;

   elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET then

      if PROCESS_CLEARANCE_RESETS(O_cc_error_tbl,
                                  I_price_event_ids,
                                  I_rib_trans_id,
                                  I_persist_ind,
                                  I_start_state,
                                  I_end_state,
                                  I_user_name,
                                  I_bulk_cc_pe_id,
                                  I_parent_thread_number,
                                  I_thread_number,
                                  I_chunk_number,
                                  L_old_clr_out_of_stock_date,
                                  L_old_clr_reset_date) = 0 then
         return 0;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

      if PROCESS_PROMO_DETAILS(O_cc_error_tbl,
                               I_price_event_ids,
                               I_rib_trans_id,
                               I_persist_ind,
                               I_start_state,
                               I_end_state,
                               I_user_name) = 0 then
         return 0;
      end if;

   elsif I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE) and
         L_new_promo_end_date is NOT NULL then

      if PROCESS_UPDATE_PROMO_DETAILS(O_cc_error_tbl,
                                      I_price_event_ids,
                                      I_rib_trans_id,
                                      I_persist_ind,
                                      I_start_state,
                                      I_end_state,
                                      I_user_name,
                                      L_new_promo_end_date) = 0 then
         return 0;
      end if;
   end if;

   L_cc_error_tbl := O_cc_error_tbl;

   if L_cc_error_tbl is NULL or
      (L_cc_error_tbl is NOT NULL and
       L_cc_error_tbl.COUNT = 0) then

      L_price_event_type := I_price_event_type;

      if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO) then

         if I_end_state = RPM_CONSTANTS.PR_PENDING_CANCEL_STATE_CODE then
            if I_price_event_type = RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE;
            elsif I_price_event_type = RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO then
               L_price_event_type := RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE;
            end if;
         end if;
      end if;

      if PUSH_BACK_WS(O_cc_error_tbl,
                      L_price_event_type,
                      I_bulk_cc_pe_id,
                      I_parent_thread_number,
                      I_thread_number,
                      I_chunk_number) = 0 then
         return 0;
      end if;

   end if;

   O_cc_error_tbl := L_cc_error_tbl;

   --Instrumentation
   BEGIN

      if REGEXP_LIKE (I_user_name, '[Ii]njector|[Ll]oc|[Nn]ew') then

         RPM_APP_COMM_UTILITY_SQL.LOG_THIS(L_return_code,
                                           L_error_msg,
                                           RPM_CONSTANTS.INSTRUMENTATION_BU_RGBU,
                                           RPM_CONSTANTS.INSTRUMENTATION_PROD_RPM,
                                           LP_batch_name,
                                           L_program||'-'||I_bulk_cc_pe_id||'-'||I_parent_thread_number||'-'||I_thread_number||'-'||LP_chunk_number,
                                           RPM_CONSTANTS.LOG_COMPLETED,
                                           'End of '||L_program||' for '||I_price_event_type);
      end if;

   EXCEPTION

      when OTHERS then
         goto PROCESS_PRICE_EVENTS_2;

   END;

   <<PROCESS_PRICE_EVENTS_2>>

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: '|| I_parent_thread_number ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || LP_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PROCESS_PRICE_EVENTS;
--------------------------------------------------------------------------------

FUNCTION PUSH_BACK_WS(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                      I_price_event_type     IN     VARCHAR2,
                      I_bulk_cc_pe_id        IN     NUMBER,
                      I_parent_thread_number IN     NUMBER,
                      I_thread_number        IN     NUMBER,
                      I_chunk_number         IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_ACTIONS_SQL.PUSH_BACK_WS';

   L_start_time TIMESTAMP := SYSTIMESTAMP;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_parent_thread_number: '|| I_parent_thread_number ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number ||
                                      ' - I_price_event_type: '|| I_price_event_type);

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET) then

      if RPM_CLEARANCE_GTT_SQL.PUSH_BACK_WS(O_cc_error_tbl,
                                            I_bulk_cc_pe_id,
                                            I_parent_thread_number,
                                            I_thread_number,
                                            I_chunk_number) = 0 then
         return 0;
      end if;

   end if;

   insert into rpm_promo_item_loc_expl_ws
      (bulk_cc_pe_id,
       parent_thread_number,
       thread_number,
       chunk_number,
       promo_item_loc_expl_id,
       item,
       dept,
       class,
       subclass,
       location,
       zone_node_type,
       promo_id,
       promo_display_id,
       promo_secondary_ind,
       promo_comp_id,
       comp_display_id,
       promo_dtl_id,
       type,
       customer_type,
       detail_secondary_ind,
       detail_start_date,
       detail_end_date,
       detail_apply_to_code,
       timebased_dtl_ind,
       detail_change_type,
       detail_change_amount,
       detail_change_currency,
       detail_change_percent,
       detail_change_selling_uom,
       detail_price_guide_id,
       deleted_ind,
       exception_parent_id,
       rpile_rowid,
       diff_id,
       item_parent,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select I_bulk_cc_pe_id,
          I_parent_thread_number,
          I_thread_number,
          I_chunk_number,
          promo_item_loc_expl_id,
          item,
          dept,
          class,
          subclass,
          location,
          zone_node_type,
          promo_id,
          promo_display_id,
          promo_secondary_ind,
          promo_comp_id,
          comp_display_id,
          promo_dtl_id,
          type,
          customer_type,
          detail_secondary_ind,
          detail_start_date,
          detail_end_date,
          detail_apply_to_code,
          timebased_dtl_ind,
          detail_change_type,
          detail_change_amount,
          detail_change_currency,
          detail_change_percent,
          detail_change_selling_uom,
          detail_price_guide_id,
          deleted_ind,
          exception_parent_id,
          rpile_rowid,
          diff_id,
          item_parent,
          zone_id,
          cur_hier_level,
          max_hier_level
     from rpm_promo_item_loc_expl_gtt;

   insert into rpm_cust_segment_promo_fr_ws
      (bulk_cc_pe_id,
       parent_thread_number,
       thread_number,
       chunk_number,
       cust_segment_promo_id,
       item,
       zone_node_type,
       location,
       action_date,
       customer_type,
       dept,
       promo_retail,
       promo_retail_currency,
       promo_uom,
       complex_promo_ind,
       cspfr_rowid,
       diff_id,
       item_parent,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select I_bulk_cc_pe_id,
          I_parent_thread_number,
          I_thread_number,
          I_chunk_number ,
          cust_segment_promo_id,
          item,
          zone_node_type,
          location,
          action_date,
          customer_type,
          dept,
          promo_retail,
          promo_retail_currency,
          promo_uom,
          complex_promo_ind,
          cspfr_rowid,
          diff_id,
          item_parent,
          zone_id,
          cur_hier_level,
          max_hier_level
     from rpm_cust_segment_promo_fr_gtt
    where promo_retail is NOT NULL;

   insert into rpm_future_retail_ws
      (bulk_cc_pe_id,
       parent_thread_number,
       thread_number,
       chunk_number,
       future_retail_id,
       dept,
       class,
       subclass,
       item,
       zone_node_type,
       location,
       action_date,
       selling_retail,
       selling_retail_currency,
       selling_uom,
       multi_units,
       multi_unit_retail,
       multi_unit_retail_currency,
       multi_selling_uom,
       clear_exception_parent_id,
       clear_retail,
       clear_retail_currency,
       clear_uom,
       simple_promo_retail,
       simple_promo_retail_currency,
       simple_promo_uom,
       price_change_id,
       price_change_display_id,
       pc_exception_parent_id,
       pc_change_type,
       pc_change_amount,
       pc_change_currency,
       pc_change_percent,
       pc_change_selling_uom,
       pc_null_multi_ind,
       pc_multi_units,
       pc_multi_unit_retail,
       pc_multi_unit_retail_currency,
       pc_multi_selling_uom,
       pc_price_guide_id,
       clearance_id,
       clearance_display_id,
       clear_mkdn_index,
       clear_start_ind,
       clear_change_type,
       clear_change_amount,
       clear_change_currency,
       clear_change_percent,
       clear_change_selling_uom,
       clear_price_guide_id,
       loc_move_from_zone_id,
       loc_move_to_zone_id,
       location_move_id,
       on_simple_promo_ind,
       on_complex_promo_ind,
       deleted_ind,
       rfr_rowid,
       diff_id,
       item_parent,
       zone_id,
       cur_hier_level,
       max_hier_level)
   select I_bulk_cc_pe_id,
          I_parent_thread_number,
          I_thread_number,
          I_chunk_number,
          future_retail_id,
          dept,
          class,
          subclass,
          item,
          zone_node_type,
          location,
          action_date,
          selling_retail,
          selling_retail_currency,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_unit_retail_currency,
          multi_selling_uom,
          clear_exception_parent_id,
          clear_retail,
          clear_retail_currency,
          clear_uom,
          simple_promo_retail,
          simple_promo_retail_currency,
          simple_promo_uom,
          price_change_id,
          price_change_display_id,
          pc_exception_parent_id,
          pc_change_type,
          pc_change_amount,
          pc_change_currency,
          pc_change_percent,
          pc_change_selling_uom,
          pc_null_multi_ind,
          pc_multi_units,
          pc_multi_unit_retail,
          pc_multi_unit_retail_currency,
          pc_multi_selling_uom,
          pc_price_guide_id,
          clearance_id,
          clearance_display_id,
          clear_mkdn_index,
          clear_start_ind,
          clear_change_type,
          clear_change_amount,
          clear_change_currency,
          clear_change_percent,
          clear_change_selling_uom,
          clear_price_guide_id,
          loc_move_from_zone_id,
          loc_move_to_zone_id,
          location_move_id,
          on_simple_promo_ind,
          on_complex_promo_ind,
          case
             when timeline_seq = -999 then
                1
             else
                0
          end deleted_ind,
          rfr_rowid,
          diff_id,
          item_parent,
          zone_id,
          cur_hier_level,
          max_hier_level
     from rpm_future_retail_gtt;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_parent_thread_number: '|| I_parent_thread_number ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number ||
                               ' - I_price_event_type: '|| I_price_event_type,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END PUSH_BACK_WS;
----------------------------------------------------------------------------------------------

FUNCTION GENERATE_TIMELINE(O_cc_error_tbl     OUT CONFLICT_CHECK_ERROR_TBL,
                           I_bulk_cc_pe_id IN     NUMBER,
                           I_thread_number IN     NUMBER,
                           I_chunk_number  IN     NUMBER,
                           I_pe_no_frs     IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(45) := 'RPM_CHUNK_CC_ACTIONS_SQL.GENERATE_TIMELINE';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number ||
                                      ' - I_pe_no_frs.COUNT: ' || I_pe_no_frs.COUNT);

   -- This function will generate a new timeline (if it does not exist yet)
   -- at the level of the price event being approved

   -- For Item/Location Level - Populate the RPM_PRICE_INQUIRY_GTT to prevent some performance issue
   delete rpm_price_inquiry_gtt;

   insert into rpm_price_inquiry_gtt (dept,
                                      price_event_id,
                                      item,
                                      diff_id,
                                      item_parent,
                                      location,
                                      loc_type,
                                      fr_zone_id)
      select t.dept,
             t.price_event_id,
             t.item,
             t.diff_id,
             t.item_parent,
             t.location,
             t.zone_node_type,
             t.zone_id
        from (select distinct
                     il.dept,
                     il.price_event_id,
                     il.item,
                     il.diff_id,
                     il.item_parent,
                     il.location,
                     il.zone_node_type,
                     zl.zone_id
                from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                             rpi.price_event_id,
                             rpi.dept,
                             rpi.item,
                             case
                                when im.item_parent is NULL and
                                     im.item_level = im.tran_level then
                                   NULL
                                else
                                   im.diff_1
                             end diff_id,
                             im.item_parent,
                             rpl.location,
                             rpl.zone_node_type,
                             rmrde.regular_zone_group,
                             RANK() OVER (PARTITION BY rpi.item,
                                                       rpi.diff_id,
                                                       rpl.zone_node_type,
                                                       rpl.location
                                              ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                       rpi.pe_merch_level desc,
                                                       rpi.price_event_id) rank
                        from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                             rpm_bulk_cc_pe_item rpi,
                             rpm_bulk_cc_pe_location rpl,
                             item_master im,
                             rpm_merch_retail_def_expl rmrde,
                             rpm_item_loc ril
                       where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                         and rpi.thread_number                  = I_thread_number
                         and rpi.chunk_number                   = I_chunk_number
                         and NVL(rpi.txn_man_excluded_item, 0) != 1
                         and rpi.price_event_id                 = VALUE(ids)
                         and rpl.price_event_id                 = VALUE(ids)
                         and rpi.item_parent                    is NULL
                         and rpl.zone_id                        is NULL
                         and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                         and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                         and rpl.price_event_id                 = rpi.price_event_id
                         and rpl.itemloc_id                     = rpi.itemloc_id
                         and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                    RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                         and im.item                            = rpi.item
                         and rmrde.dept                         = im.dept
                         and rmrde.class                        = im.class
                         and rmrde.subclass                     = im.subclass
                         and ril.dept                           = im.dept
                         and ril.item                           = im.item
                         and ril.loc                            = rpl.location
                         and rownum                             > 0) il,
                     (select rz.zone_group_id,
                             rz.zone_id,
                             rzl.location
                        from rpm_zone rz,
                             rpm_zone_location rzl
                       where rzl.zone_id = rz.zone_id
                         and rownum      > 0) zl
               where il.rank               = 1
                 and il.regular_zone_group = zl.zone_group_id (+)
                 and il.location           = zl.location (+)
                 and rownum               >= 1) t
       where NOT EXISTS (select 1
                           from rpm_item_loc_gtt iloc
                          where iloc.price_event_id       = t.price_event_id
                            and iloc.item                 = t.item
                            and NVL(iloc.diff_id, '-999') = NVL(t.diff_id, '-999')
                            and iloc.location             = t.location
                            and iloc.regular_zone_group   = t.zone_node_type);

   merge into rpm_future_retail_gtt target
   -- Get all records on rpm_future_retail...
   using (select price_event_id,
          dept,
          class,
          subclass,
          item,
          diff_id,
          item_parent,
          zone_node_type,
          location,
          zone_id,
          action_date,
          selling_retail,
          selling_retail_currency,
          selling_uom,
          multi_units,
          multi_unit_retail,
          multi_unit_retail_currency,
          multi_selling_uom,
          clear_retail,
          clear_retail_currency,
          clear_uom,
          simple_promo_retail,
          simple_promo_retail_currency,
          simple_promo_uom,
          price_change_id,
          price_change_display_id,
          pc_exception_parent_id,
          pc_change_type,
          pc_change_amount,
          pc_change_currency,
          pc_change_percent,
          pc_change_selling_uom,
          pc_null_multi_ind,
          pc_multi_units,
          pc_multi_unit_retail,
          pc_multi_unit_retail_currency,
          pc_multi_selling_uom,
          pc_price_guide_id,
          clearance_id,
          clearance_display_id,
          clear_mkdn_index,
          clear_start_ind,
          clear_change_type,
          clear_change_amount,
          clear_change_currency,
          clear_change_percent,
          clear_change_selling_uom,
          clear_price_guide_id,
          loc_move_from_zone_id,
          loc_move_to_zone_id,
          location_move_id,
          lock_version,
          on_simple_promo_ind,
          on_complex_promo_ind,
          max_hier_level,
          cur_hier_level,
          future_retail_id
     from (-- price events at the Parent/Diff-Zone level
           select t.price_event_id,
                  fr.dept,
                  class,
                  subclass,
                  t.item,
                  t.diff_id,
                  NULL item_parent,
                  t.zone_node_type,
                  fr.location,
                  NULL zone_id,
                  action_date,
                  selling_retail,
                  selling_retail_currency,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_unit_retail_currency,
                  multi_selling_uom,
                  clear_retail,
                  clear_retail_currency,
                  clear_uom,
                  simple_promo_retail,
                  simple_promo_retail_currency,
                  simple_promo_uom,
                  price_change_id,
                  price_change_display_id,
                  pc_exception_parent_id,
                  pc_change_type,
                  pc_change_amount,
                  pc_change_currency,
                  pc_change_percent,
                  pc_change_selling_uom,
                  pc_null_multi_ind,
                  pc_multi_units,
                  pc_multi_unit_retail,
                  pc_multi_unit_retail_currency,
                  pc_multi_selling_uom,
                  pc_price_guide_id,
                  clearance_id,
                  clearance_display_id,
                  clear_mkdn_index,
                  clear_start_ind,
                  clear_change_type,
                  clear_change_amount,
                  clear_change_currency,
                  clear_change_percent,
                  clear_change_selling_uom,
                  clear_price_guide_id,
                  loc_move_from_zone_id,
                  loc_move_to_zone_id,
                  location_move_id,
                  lock_version,
                  fr.on_simple_promo_ind,
                  fr.on_complex_promo_ind,
                  fr.max_hier_level,
                  RPM_CONSTANTS.FR_HIER_DIFF_ZONE cur_hier_level,
                  fr.future_retail_id
             from (select s.dept,
                          s.price_event_id,
                          s.item,
                          s.diff_id,
                          s.location,
                          s.zone_node_type
                     from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                  distinct
                                  rpi.dept,
                                  rpi.price_event_id,
                                  rpi.item,
                                  rpi.diff_id,
                                  rpl.location,
                                  rpl.zone_node_type,
                                  RANK() OVER (PARTITION BY rpi.item,
                                                            rpi.diff_id,
                                                            rpl.zone_node_type,
                                                            rpl.location
                                                   ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                            rpi.pe_merch_level desc,
                                                            rpi.price_event_id) rank
                             from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                  rpm_bulk_cc_pe_item rpi,
                                  rpm_bulk_cc_pe_location rpl,
                                  item_master im,
                                  rpm_zone_location rzl,
                                  rpm_item_loc ril
                            where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                              and rpi.thread_number                  = I_thread_number
                              and rpi.chunk_number                   = I_chunk_number
                              and NVL(rpi.txn_man_excluded_item, 0) != 1
                              and rpi.price_event_id                 = VALUE(ids)
                              and rpl.price_event_id                 = VALUE(ids)
                              and rpi.item_parent                    is NULL
                              and rpl.zone_id                        is NULL
                              and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                              and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                              and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                              and rpl.price_event_id                 = rpi.price_event_id
                              and rpl.itemloc_id                     = rpi.itemloc_id
                              and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                              and im.item_parent                     = rpi.item
                              and im.diff_1                          = rpi.diff_id
                              and ril.dept                           = im.dept
                              and ril.item                           = im.item
                              and ril.loc                            = rzl.location
                              and rzl.zone_id                        = rpl.location) s
                    where rank = 1
                      and NOT EXISTS (select 1
                                        from rpm_item_loc_gtt iloc
                                       where iloc.price_event_id       = s.price_event_id
                                         and iloc.item                 = s.item
                                         and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                         and iloc.location             = s.location
                                         and iloc.regular_zone_group   = s.zone_node_type)
                      and rownum >= 1) t,
                  rpm_future_retail fr
            where fr.dept           = t.dept
              and fr.item           = t.item
              and fr.diff_id        is NULL
              and fr.location       = t.location
              and fr.zone_node_type = t.zone_node_type
           union all
           -- price events at the parent and location level
           -- For location that is part of the primary zone group - get it from Parent/Zone Level
           -- For location that is not part of the primary zone group - it should be available already
           select t.price_event_id,
                  fr.dept,
                  class,
                  subclass,
                  fr.item,
                  NULL diff_id,
                  NULL item_parent,
                  t.zone_node_type,
                  t.location,
                  t.zone_id,
                  action_date,
                  selling_retail,
                  selling_retail_currency,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_unit_retail_currency,
                  multi_selling_uom,
                  clear_retail,
                  clear_retail_currency,
                  clear_uom,
                  simple_promo_retail,
                  simple_promo_retail_currency,
                  simple_promo_uom,
                  price_change_id,
                  price_change_display_id,
                  pc_exception_parent_id,
                  pc_change_type,
                  pc_change_amount,
                  pc_change_currency,
                  pc_change_percent,
                  pc_change_selling_uom,
                  pc_null_multi_ind,
                  pc_multi_units,
                  pc_multi_unit_retail,
                  pc_multi_unit_retail_currency,
                  pc_multi_selling_uom,
                  pc_price_guide_id,
                  clearance_id,
                  clearance_display_id,
                  clear_mkdn_index,
                  clear_start_ind,
                  clear_change_type,
                  clear_change_amount,
                  clear_change_currency,
                  clear_change_percent,
                  clear_change_selling_uom,
                  clear_price_guide_id,
                  loc_move_from_zone_id,
                  loc_move_to_zone_id,
                  location_move_id,
                  lock_version,
                  fr.on_simple_promo_ind,
                  fr.on_complex_promo_ind,
                  fr.max_hier_level,
                  RPM_CONSTANTS.FR_HIER_PARENT_LOC cur_hier_level,
                  fr.future_retail_id
             from (select s.dept,
                          s.price_event_id,
                          s.item,
                          s.location,
                          s.zone_node_type,
                          s.zone_id
                     from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                  distinct
                                  rpi.dept,
                                  rpi.price_event_id,
                                  rpi.item,
                                  rpl.location,
                                  rpl.zone_node_type,
                                  rz.zone_id,
                                  RANK() OVER (PARTITION BY rpi.item,
                                                            rpi.diff_id,
                                                            rpl.zone_node_type,
                                                            rpl.location
                                                   ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                            rpi.pe_merch_level desc,
                                                            rpi.price_event_id) rank
                             from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                  rpm_bulk_cc_pe_item rpi,
                                  rpm_bulk_cc_pe_location rpl,
                                  item_master im,
                                  rpm_merch_retail_def_expl rmrde,
                                  rpm_zone rz,
                                  rpm_zone_location rzl,
                                  rpm_item_loc ril
                            where rpl.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                              and rpi.thread_number                  = I_thread_number
                              and rpi.chunk_number                   = I_chunk_number
                              and NVL(rpi.txn_man_excluded_item, 0) != 1
                              and rpl.price_event_id                 = VALUE(ids)
                              and rpi.price_event_id                 = VALUE(ids)
                              and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                              and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_MERCH_TYPE
                              and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                              and rpl.zone_id                        is NULL
                              and rpi.item_parent                    is NULL
                              and im.item_parent                     = rpi.item
                              and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                              and rpl.price_event_id                 = rpi.price_event_id
                              and rpl.itemloc_id                     = rpi.itemloc_id
                              and rmrde.dept                         = im.dept
                              and rmrde.class                        = im.class
                              and rmrde.subclass                     = im.subclass
                              and rmrde.regular_zone_group           = rz.zone_group_id
                              and rz.zone_id                         = rzl.zone_id
                              and rzl.location                       = rpl.location
                              and ril.dept                           = im.dept
                              and ril.item                           = im.item
                              and ril.loc                            = rpl.location) s
                    where rank = 1
                      and NOT EXISTS (select 1
                                        from rpm_item_loc_gtt iloc
                                       where iloc.price_event_id     = s.price_event_id
                                         and iloc.item               = s.item
                                         and iloc.diff_id            is NULL
                                         and iloc.location           = s.location
                                         and iloc.regular_zone_group = s.zone_node_type)
                      and rownum >= 1) t,
                  rpm_future_retail fr
            where fr.dept           = t.dept
              and fr.item           = t.item
              and fr.diff_id        is NULL
              and fr.location       = t.zone_id
              and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
           union all
           -- price events at the tran item and zone level
           -- Get it from the two possible levels (either PD/Z or P/Z) and choose the lowest level available
           select price_event_id,
                  dept,
                  class,
                  subclass,
                  item,
                  diff_id,
                  item_parent,
                  zone_node_type,
                  location,
                  NULL zone_id,
                  action_date,
                  selling_retail,
                  selling_retail_currency,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_unit_retail_currency,
                  multi_selling_uom,
                  clear_retail,
                  clear_retail_currency,
                  clear_uom,
                  simple_promo_retail,
                  simple_promo_retail_currency,
                  simple_promo_uom,
                  price_change_id,
                  price_change_display_id,
                  pc_exception_parent_id,
                  pc_change_type,
                  pc_change_amount,
                  pc_change_currency,
                  pc_change_percent,
                  pc_change_selling_uom,
                  pc_null_multi_ind,
                  pc_multi_units,
                  pc_multi_unit_retail,
                  pc_multi_unit_retail_currency,
                  pc_multi_selling_uom,
                  pc_price_guide_id,
                  clearance_id,
                  clearance_display_id,
                  clear_mkdn_index,
                  clear_start_ind,
                  clear_change_type,
                  clear_change_amount,
                  clear_change_currency,
                  clear_change_percent,
                  clear_change_selling_uom,
                  clear_price_guide_id,
                  loc_move_from_zone_id,
                  loc_move_to_zone_id,
                  location_move_id,
                  lock_version,
                  on_simple_promo_ind,
                  on_complex_promo_ind,
                  max_hier_level,
                  RPM_CONSTANTS.FR_HIER_ITEM_ZONE cur_hier_level,
                  future_retail_id
             from (-- Get the two possible levels (either PD/Z or P/Z) and choose the one with max rank
                   select fr_level_rank,
                          price_event_id,
                          dept,
                          class,
                          subclass,
                          item,
                          diff_id,
                          item_parent,
                          zone_node_type,
                          location,
                          action_date,
                          selling_retail,
                          selling_retail_currency,
                          selling_uom,
                          multi_units,
                          multi_unit_retail,
                          multi_unit_retail_currency,
                          multi_selling_uom,
                          clear_retail,
                          clear_retail_currency,
                          clear_uom,
                          simple_promo_retail,
                          simple_promo_retail_currency,
                          simple_promo_uom,
                          price_change_id,
                          price_change_display_id,
                          pc_exception_parent_id,
                          pc_change_type,
                          pc_change_amount,
                          pc_change_currency,
                          pc_change_percent,
                          pc_change_selling_uom,
                          pc_null_multi_ind,
                          pc_multi_units,
                          pc_multi_unit_retail,
                          pc_multi_unit_retail_currency,
                          pc_multi_selling_uom,
                          pc_price_guide_id,
                          clearance_id,
                          clearance_display_id,
                          clear_mkdn_index,
                          clear_start_ind,
                          clear_change_type,
                          clear_change_amount,
                          clear_change_currency,
                          clear_change_percent,
                          clear_change_selling_uom,
                          clear_price_guide_id,
                          loc_move_from_zone_id,
                          loc_move_to_zone_id,
                          location_move_id,
                          lock_version,
                          on_simple_promo_ind,
                          on_complex_promo_ind,
                          max_hier_level,
                          future_retail_id,
                          MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                item,
                                                                diff_id,
                                                                location,
                                                                zone_node_type) max_fr_lvl_rank
                     from (-- Look at the Parent/Zone level - rank 0
                           select 0 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  class,
                                  subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.zone_node_type,
                                  fr.location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from (select s.dept,
                                          s.price_event_id,
                                          s.item,
                                          s.diff_id,
                                          s.item_parent,
                                          s.location,
                                          s.zone_node_type
                                     from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                  distinct
                                                  rpi.dept,
                                                  rpi.price_event_id,
                                                  rpi.item,
                                                  im.diff_1 diff_id,
                                                  im.item_parent,
                                                  rpl.location,
                                                  rpl.zone_node_type,
                                                  RANK() OVER (PARTITION BY rpi.item,
                                                                            rpi.diff_id,
                                                                            rpl.zone_node_type,
                                                                            rpl.location
                                                                   ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                            rpi.pe_merch_level desc,
                                                                            rpi.price_event_id) rank
                                             from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_bulk_cc_pe_item rpi,
                                                  rpm_bulk_cc_pe_location rpl,
                                                  item_master im,
                                                  rpm_zone_location rzl,
                                                  rpm_item_loc ril
                                            where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                              and rpi.thread_number                  = I_thread_number
                                              and rpi.chunk_number                   = I_chunk_number
                                              and NVL(rpi.txn_man_excluded_item, 0) != 1
                                              and rpi.price_event_id                 = VALUE(ids)
                                              and rpl.price_event_id                 = VALUE(ids)
                                              and rpi.item_parent                    is NULL
                                              and rpl.zone_id                        is NULL
                                              and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                              and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                              and im.item                            = rpi.item
                                              and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                              and rpl.price_event_id                 = rpi.price_event_id
                                              and rpl.itemloc_id                     = rpi.itemloc_id
                                              and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                              and rzl.zone_id                        = rpl.location
                                              and ril.dept                           = im.dept
                                              and ril.item                           = im.item
                                              and ril.loc                            = rzl.location) s
                                    where rank = 1
                                      and NOT EXISTS (select 1
                                                        from rpm_item_loc_gtt iloc
                                                       where iloc.price_event_id       = s.price_event_id
                                                         and iloc.item                 = s.item
                                                         and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                         and iloc.location             = s.location
                                                         and iloc.regular_zone_group   = s.zone_node_type)
                                      and rownum >= 1) t,
                                  rpm_future_retail fr
                            where fr.dept           = t.dept
                              and fr.item           = t.item_parent
                              and fr.diff_id        is NULL
                              and fr.location       = t.location
                              and fr.zone_node_type = t.zone_node_type
                           union all
                           -- Look at the Parent-Diff/Zone level - rank 1
                           select 1 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  class,
                                  subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.zone_node_type,
                                  fr.location,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from (select s.dept,
                                          s.price_event_id,
                                          s.item,
                                          s.diff_id,
                                          s.item_parent,
                                          s.location,
                                          s.zone_node_type
                                     from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                  distinct
                                                  rpi.dept,
                                                  rpi.price_event_id,
                                                  rpi.item,
                                                  im.diff_1 diff_id,
                                                  im.item_parent,
                                                  rpl.location,
                                                  rpl.zone_node_type,
                                                  RANK() OVER (PARTITION BY rpi.item,
                                                                            rpi.diff_id,
                                                                            rpl.zone_node_type,
                                                                            rpl.location
                                                                   ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                            rpi.pe_merch_level desc,
                                                                            rpi.price_event_id) rank
                                             from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_bulk_cc_pe_item rpi,
                                                  rpm_bulk_cc_pe_location rpl,
                                                  item_master im,
                                                  rpm_zone_location rzl,
                                                  rpm_item_loc ril
                                            where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                              and rpi.thread_number                  = I_thread_number
                                              and rpi.chunk_number                   = I_chunk_number
                                              and NVL(rpi.txn_man_excluded_item, 0) != 1
                                              and rpi.price_event_id                 = VALUE(ids)
                                              and rpl.price_event_id                 = VALUE(ids)
                                              and rpi.item_parent                    is NULL
                                              and rpl.zone_id                        is NULL
                                              and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                              and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                              and im.item                            = rpi.item
                                              and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                              and rpl.price_event_id                 = rpi.price_event_id
                                              and rpl.itemloc_id                     = rpi.itemloc_id
                                              and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                              and rzl.zone_id                        = rpl.location
                                              and ril.dept                           = im.dept
                                              and ril.item                           = im.item
                                              and ril.loc                            = rzl.location) s
                                    where rank = 1
                                      and NOT EXISTS (select 1
                                                        from rpm_item_loc_gtt iloc
                                                       where iloc.price_event_id       = s.price_event_id
                                                         and iloc.item                 = s.item
                                                         and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                         and iloc.location             = s.location
                                                         and iloc.regular_zone_group   = s.zone_node_type)
                                      and rownum >= 1) t,
                                  rpm_future_retail fr
                            where fr.dept                 = t.dept
                              and fr.item                 = t.item_parent
                              and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                              and fr.location             = t.location
                              and fr.zone_node_type       = t.zone_node_type))
            where fr_level_rank = max_fr_lvl_rank
           union all
           -- price events at the Parent-Diff and Loc level
           -- Get the three possible levels (PD/Z, P/L or P/Z) and choose the lowest level available
           select price_event_id,
                  dept,
                  class,
                  subclass,
                  item,
                  diff_id,
                  NULL item_parent,
                  zone_node_type,
                  location,
                  zone_id,
                  action_date,
                  selling_retail,
                  selling_retail_currency,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_unit_retail_currency,
                  multi_selling_uom,
                  clear_retail,
                  clear_retail_currency,
                  clear_uom,
                  simple_promo_retail,
                  simple_promo_retail_currency,
                  simple_promo_uom,
                  price_change_id,
                  price_change_display_id,
                  pc_exception_parent_id,
                  pc_change_type,
                  pc_change_amount,
                  pc_change_currency,
                  pc_change_percent,
                  pc_change_selling_uom,
                  pc_null_multi_ind,
                  pc_multi_units,
                  pc_multi_unit_retail,
                  pc_multi_unit_retail_currency,
                  pc_multi_selling_uom,
                  pc_price_guide_id,
                  clearance_id,
                  clearance_display_id,
                  clear_mkdn_index,
                  clear_start_ind,
                  clear_change_type,
                  clear_change_amount,
                  clear_change_currency,
                  clear_change_percent,
                  clear_change_selling_uom,
                  clear_price_guide_id,
                  loc_move_from_zone_id,
                  loc_move_to_zone_id,
                  location_move_id,
                  lock_version,
                  on_simple_promo_ind,
                  on_complex_promo_ind,
                  max_hier_level,
                  RPM_CONSTANTS.FR_HIER_DIFF_LOC cur_hier_level,
                  future_retail_id
             from (-- Get the three possible levels (PD/Z, P/L or P/Z) and choose the one with max rank
                   select fr_level_rank,
                          price_event_id,
                          dept,
                          class,
                          subclass,
                          item,
                          diff_id,
                          zone_node_type,
                          location,
                          zone_id,
                          action_date,
                          selling_retail,
                          selling_retail_currency,
                          selling_uom,
                          multi_units,
                          multi_unit_retail,
                          multi_unit_retail_currency,
                          multi_selling_uom,
                          clear_retail,
                          clear_retail_currency,
                          clear_uom,
                          simple_promo_retail,
                          simple_promo_retail_currency,
                          simple_promo_uom,
                          price_change_id,
                          price_change_display_id,
                          pc_exception_parent_id,
                          pc_change_type,
                          pc_change_amount,
                          pc_change_currency,
                          pc_change_percent,
                          pc_change_selling_uom,
                          pc_null_multi_ind,
                          pc_multi_units,
                          pc_multi_unit_retail,
                          pc_multi_unit_retail_currency,
                          pc_multi_selling_uom,
                          pc_price_guide_id,
                          clearance_id,
                          clearance_display_id,
                          clear_mkdn_index,
                          clear_start_ind,
                          clear_change_type,
                          clear_change_amount,
                          clear_change_currency,
                          clear_change_percent,
                          clear_change_selling_uom,
                          clear_price_guide_id,
                          loc_move_from_zone_id,
                          loc_move_to_zone_id,
                          location_move_id,
                          lock_version,
                          on_simple_promo_ind,
                          on_complex_promo_ind,
                          max_hier_level,
                          future_retail_id,
                          MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                item,
                                                                diff_id,
                                                                location,
                                                                zone_node_type) max_fr_lvl_rank
                     from (-- Look at the Parent/Zone level - rank 0
                           -- Only available when Location is part of the Primary Zone Group
                           select 0 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  class,
                                  subclass,
                                  t.item,
                                  t.diff_id,
                                  t.zone_node_type,
                                  t.location,
                                  t.zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from (select s.dept,
                                          s.price_event_id,
                                          s.item,
                                          s.diff_id,
                                          s.location,
                                          s.zone_node_type,
                                          s.zone_id
                                    from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                 distinct
                                                 rpi.dept,
                                                 rpi.price_event_id,
                                                 rpi.item,
                                                 rpi.diff_id,
                                                 rpl.location,
                                                 rpl.zone_node_type,
                                                 rz.zone_id,
                                                 RANK() OVER (PARTITION BY rpi.item,
                                                                           rpi.diff_id,
                                                                           rpl.zone_node_type,
                                                                           rpl.location
                                                                  ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                           rpi.pe_merch_level desc,
                                                                           rpi.price_event_id) rank
                                            from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                 rpm_bulk_cc_pe_item rpi,
                                                 rpm_bulk_cc_pe_location rpl,
                                                 item_master im,
                                                 rpm_merch_retail_def_expl rmrde,
                                                 rpm_zone rz,
                                                 rpm_zone_location rzl,
                                                 rpm_item_loc ril
                                           where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                             and rpi.thread_number                  = I_thread_number
                                             and rpi.chunk_number                   = I_chunk_number
                                             and NVL(rpi.txn_man_excluded_item, 0) != 1
                                             and rpi.price_event_id                 = VALUE(ids)
                                             and rpl.price_event_id                 = VALUE(ids)
                                             and rpi.item_parent                    is NULL
                                             and rpl.zone_id                        is NULL
                                             and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                             and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                             and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                             and rpl.price_event_id                 = rpi.price_event_id
                                             and rpl.itemloc_id                     = rpi.itemloc_id
                                             and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                        RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                             and im.item_parent                     = rpi.item
                                             and im.diff_1                          = rpi.diff_id
                                             and rmrde.dept                         = im.dept
                                             and rmrde.class                        = im.class
                                             and rmrde.subclass                     = im.subclass
                                             and rmrde.regular_zone_group           = rz.zone_group_id
                                             and rz.zone_id                         = rzl.zone_id
                                             and rzl.location                       = rpl.location
                                             and ril.dept                           = im.dept
                                             and ril.item                           = im.item
                                             and ril.loc                            = rpl.location) s
                                    where rank = 1
                                      and NOT EXISTS (select 1
                                                        from rpm_item_loc_gtt iloc
                                                       where iloc.price_event_id       = s.price_event_id
                                                         and iloc.item                 = s.item
                                                         and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                         and iloc.location             = s.location
                                                         and iloc.regular_zone_group   = s.zone_node_type)
                                      and rownum >= 1) t,
                                  rpm_future_retail fr
                            where fr.dept           = t.dept
                              and fr.item           = t.item
                              and fr.diff_id        is NULL
                              and fr.location       = t.zone_id
                              and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           union all
                           -- Look at the Parent-Diff/Zone level - rank 1
                           -- Only available when Location is part of the Primary Zone Group
                           select 1 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  class,
                                  subclass,
                                  t.item,
                                  t.diff_id,
                                  t.zone_node_type,
                                  t.location,
                                  t.zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from (select s.dept,
                                          s.price_event_id,
                                          s.item,
                                          s.diff_id,
                                          s.location,
                                          s.zone_node_type,
                                          s.zone_id
                                     from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                  distinct
                                                  rpi.dept,
                                                  rpi.price_event_id,
                                                  rpi.item,
                                                  rpi.diff_id,
                                                  rpl.location,
                                                  rpl.zone_node_type,
                                                  rz.zone_id,
                                                  RANK() OVER (PARTITION BY rpi.item,
                                                                            rpi.diff_id,
                                                                            rpl.zone_node_type,
                                                                            rpl.location
                                                                   ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                            rpi.pe_merch_level desc,
                                                                            rpi.price_event_id) rank
                                             from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_bulk_cc_pe_item rpi,
                                                  rpm_bulk_cc_pe_location rpl,
                                                  item_master im,
                                                  rpm_merch_retail_def_expl rmrde,
                                                  rpm_zone rz,
                                                  rpm_zone_location rzl,
                                                  rpm_item_loc ril
                                            where rpi.bulk_cc_pe_id                   = I_bulk_cc_pe_id
                                              and rpi.thread_number                   = I_thread_number
                                              and rpi.chunk_number                    = I_chunk_number
                                              and NVL(rpi.txn_man_excluded_item, 0)  != 1
                                              and rpi.price_event_id                  = VALUE(ids)
                                              and rpl.price_event_id                  = VALUE(ids)
                                              and rpi.item_parent                     is NULL
                                              and rpl.zone_id                         is NULL
                                              and rpi.merch_level_type                = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                              and rpi.pe_merch_level                  = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                              and im.item_parent                      = rpi.item
                                              and im.diff_1                           = rpi.diff_id
                                              and rpl.bulk_cc_pe_id                   = rpi.bulk_cc_pe_id
                                              and rpl.price_event_id                  = rpi.price_event_id
                                              and rpl.itemloc_id                      = rpi.itemloc_id
                                              and rpl.zone_node_type                  IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                          RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                              and rmrde.dept                          = im.dept
                                              and rmrde.class                         = im.class
                                              and rmrde.subclass                      = im.subclass
                                              and rmrde.regular_zone_group            = rz.zone_group_id
                                              and rz.zone_id                          = rzl.zone_id
                                              and rzl.location                        = rpl.location
                                              and ril.dept                            = im.dept
                                              and ril.item                            = im.item
                                              and ril.loc                             = rpl.location) s
                                    where rank = 1
                                      and NOT EXISTS (select 1
                                                        from rpm_item_loc_gtt iloc
                                                       where iloc.price_event_id       = s.price_event_id
                                                         and iloc.item                 = s.item
                                                         and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                         and iloc.location             = s.location
                                                         and iloc.regular_zone_group   = s.zone_node_type)
                                      and rownum >= 1) t,
                                  rpm_future_retail fr
                            where fr.dept                 = t.dept
                              and fr.item                 = t.item
                              and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                              and fr.location             = t.zone_id
                              and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           union all
                           -- Look at the Parent/Loc level - rank 1 (same rank as PD/Z - can't have both and be here)
                           -- Available for both Location is part of the PZG and Location is NOT part of PZG
                           select 1 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  class,
                                  subclass,
                                  t.item,
                                  t.diff_id,
                                  t.zone_node_type,
                                  t.location,
                                  fr.zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from (select s.dept,
                                          s.price_event_id,
                                          s.item,
                                          s.diff_id,
                                          s.location,
                                          s.zone_node_type
                                     from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                                  distinct
                                                  rpi.dept,
                                                  rpi.price_event_id,
                                                  rpi.item,
                                                  rpi.diff_id,
                                                  rpl.location,
                                                  rpl.zone_node_type,
                                                  RANK() OVER (PARTITION BY rpi.item,
                                                                            rpi.diff_id,
                                                                            rpl.zone_node_type,
                                                                            rpl.location
                                                                   ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                            rpi.pe_merch_level desc,
                                                                            rpi.price_event_id) rank
                                             from table(cast(I_pe_no_frs as OBJ_NUMERIC_ID_TABLE)) ids,
                                                  rpm_bulk_cc_pe_item rpi,
                                                  rpm_bulk_cc_pe_location rpl,
                                                  item_master im,
                                                  rpm_item_loc ril
                                            where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                              and rpi.thread_number                  = I_thread_number
                                              and rpi.chunk_number                   = I_chunk_number
                                              and NVL(rpi.txn_man_excluded_item, 0) != 1
                                              and rpi.price_event_id                 = VALUE(ids)
                                              and rpl.price_event_id                 = VALUE(ids)
                                              and rpi.item_parent                    is NULL
                                              and rpl.zone_id                        is NULL
                                              and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                              and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                              and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                              and rpl.price_event_id                 = rpi.price_event_id
                                              and rpl.itemloc_id                     = rpi.itemloc_id
                                              and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                              and im.item_parent                     = rpi.item
                                              and im.diff_1                          = rpi.diff_id
                                              and ril.dept                           = im.dept
                                              and ril.item                           = im.item
                                              and ril.loc                            = rpl.location) s
                                            where rank = 1
                                              and NOT EXISTS (select 1
                                                                from rpm_item_loc_gtt iloc
                                                               where iloc.price_event_id       = s.price_event_id
                                                                 and iloc.item                 = s.item
                                                                 and NVL(iloc.diff_id, '-999') = NVL(s.diff_id, '-999')
                                                                 and iloc.location             = s.location
                                                                 and iloc.regular_zone_group   = s.zone_node_type)
                                      and rownum >= 1) t,
                                  rpm_future_retail fr
                            where fr.dept           = t.dept
                              and fr.item           = t.item
                              and fr.diff_id        is NULL
                              and fr.location       = t.location
                              and fr.zone_node_type = t.zone_node_type))
            where fr_level_rank = max_fr_lvl_rank
           union all
           -- price events at the item and location level
           -- Get all levels above I/L (I/Z, PD/L, PD/Z, P/L or P/Z) and choose the lowest level available
           select price_event_id,
                  dept,
                  class,
                  subclass,
                  item,
                  diff_id,
                  item_parent,
                  zone_node_type,
                  location,
                  zone_id,
                  action_date,
                  selling_retail,
                  selling_retail_currency,
                  selling_uom,
                  multi_units,
                  multi_unit_retail,
                  multi_unit_retail_currency,
                  multi_selling_uom,
                  clear_retail,
                  clear_retail_currency,
                  clear_uom,
                  simple_promo_retail,
                  simple_promo_retail_currency,
                  simple_promo_uom,
                  price_change_id,
                  price_change_display_id,
                  pc_exception_parent_id,
                  pc_change_type,
                  pc_change_amount,
                  pc_change_currency,
                  pc_change_percent,
                  pc_change_selling_uom,
                  pc_null_multi_ind,
                  pc_multi_units,
                  pc_multi_unit_retail,
                  pc_multi_unit_retail_currency,
                  pc_multi_selling_uom,
                  pc_price_guide_id,
                  clearance_id,
                  clearance_display_id,
                  clear_mkdn_index,
                  clear_start_ind,
                  clear_change_type,
                  clear_change_amount,
                  clear_change_currency,
                  clear_change_percent,
                  clear_change_selling_uom,
                  clear_price_guide_id,
                  loc_move_from_zone_id,
                  loc_move_to_zone_id,
                  location_move_id,
                  lock_version,
                  on_simple_promo_ind,
                  on_complex_promo_ind,
                  max_hier_level,
                  RPM_CONSTANTS.FR_HIER_ITEM_LOC cur_hier_level,
                  future_retail_id
             from (-- Get all levels above I/L (I/Z, PD/L, PD/Z, P/L or P/Z) and choose the one with max rank
                   select fr_level_rank,
                          price_event_id,
                          dept,
                          class,
                          subclass,
                          item,
                          diff_id,
                          item_parent,
                          zone_node_type,
                          location,
                          zone_id,
                          action_date,
                          selling_retail,
                          selling_retail_currency,
                          selling_uom,
                          multi_units,
                          multi_unit_retail,
                          multi_unit_retail_currency,
                          multi_selling_uom,
                          clear_retail,
                          clear_retail_currency,
                          clear_uom,
                          simple_promo_retail,
                          simple_promo_retail_currency,
                          simple_promo_uom,
                          price_change_id,
                          price_change_display_id,
                          pc_exception_parent_id,
                          pc_change_type,
                          pc_change_amount,
                          pc_change_currency,
                          pc_change_percent,
                          pc_change_selling_uom,
                          pc_null_multi_ind,
                          pc_multi_units,
                          pc_multi_unit_retail,
                          pc_multi_unit_retail_currency,
                          pc_multi_selling_uom,
                          pc_price_guide_id,
                          clearance_id,
                          clearance_display_id,
                          clear_mkdn_index,
                          clear_start_ind,
                          clear_change_type,
                          clear_change_amount,
                          clear_change_currency,
                          clear_change_percent,
                          clear_change_selling_uom,
                          clear_price_guide_id,
                          loc_move_from_zone_id,
                          loc_move_to_zone_id,
                          location_move_id,
                          lock_version,
                          on_simple_promo_ind,
                          on_complex_promo_ind,
                          max_hier_level,
                          future_retail_id,
                          MAX(fr_level_rank) OVER (PARTITION BY price_event_id,
                                                                item,
                                                                diff_id,
                                                                location,
                                                                zone_node_type) max_fr_lvl_rank
                     from (-- Look at the Parent/Zone level - rank 0
                           -- Only possible if Location is part of PZG
                           select 0 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  fr.class,
                                  fr.subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.loc_type zone_node_type,
                                  t.location,
                                  t.fr_zone_id zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from rpm_price_inquiry_gtt t,
                                  rpm_future_retail fr
                            where fr.dept           = t.dept
                              and fr.item           = t.item_parent
                              and fr.diff_id        is NULL
                              and fr.location       = t.fr_zone_id
                              and fr.zone_node_type = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           union all
                           -- Look at the Parent-Diff/Zone level - rank 1
                           -- Only possible if Location is part of PZG
                           select 1 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  fr.class,
                                  fr.subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.loc_type zone_node_type,
                                  t.location,
                                  t.fr_zone_id zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from rpm_price_inquiry_gtt t,
                                  rpm_future_retail fr
                            where fr.dept                 = t.dept
                              and fr.item                 = t.item_parent
                              and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                              and fr.location             = t.fr_zone_id
                              and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           union all
                           -- Look at the Parent/Loc level - rank 1 (same rank as PD/Z - can't have both and be here)
                           -- Location can be either part of the PZG or NOT
                           select 1 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  fr.class,
                                  fr.subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.loc_type zone_node_type,
                                  t.location,
                                  t.fr_zone_id zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from rpm_price_inquiry_gtt t,
                                  rpm_future_retail fr
                            where fr.dept           = t.dept
                              and fr.item           = t.item_parent
                              and fr.diff_id        is NULL
                              and fr.location       = t.location
                              and fr.zone_node_type = t.loc_type
                           union all
                           -- Look at the Item/Zone level - rank 2
                           -- Only possible if Location is part of PZG
                           select 2 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  fr.class,
                                  fr.subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.loc_type zone_node_type,
                                  t.location,
                                  t.fr_zone_id zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from rpm_price_inquiry_gtt t,
                                  rpm_future_retail fr
                            where fr.dept                 = t.dept
                              and fr.item                 = t.item
                              and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                              and fr.location             = t.fr_zone_id
                              and fr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                           union all
                           -- Look at the Parent-Diff/Loc level - rank 2 (same rank as I/Z - can't have both and be here)
                           -- Location can be either part of the PZG or NOT
                           select 2 fr_level_rank,
                                  t.price_event_id,
                                  fr.dept,
                                  fr.class,
                                  fr.subclass,
                                  t.item,
                                  t.diff_id,
                                  t.item_parent,
                                  t.loc_type zone_node_type,
                                  t.location,
                                  t.fr_zone_id zone_id,
                                  action_date,
                                  selling_retail,
                                  selling_retail_currency,
                                  selling_uom,
                                  multi_units,
                                  multi_unit_retail,
                                  multi_unit_retail_currency,
                                  multi_selling_uom,
                                  clear_retail,
                                  clear_retail_currency,
                                  clear_uom,
                                  simple_promo_retail,
                                  simple_promo_retail_currency,
                                  simple_promo_uom,
                                  price_change_id,
                                  price_change_display_id,
                                  pc_exception_parent_id,
                                  pc_change_type,
                                  pc_change_amount,
                                  pc_change_currency,
                                  pc_change_percent,
                                  pc_change_selling_uom,
                                  pc_null_multi_ind,
                                  pc_multi_units,
                                  pc_multi_unit_retail,
                                  pc_multi_unit_retail_currency,
                                  pc_multi_selling_uom,
                                  pc_price_guide_id,
                                  clearance_id,
                                  clearance_display_id,
                                  clear_mkdn_index,
                                  clear_start_ind,
                                  clear_change_type,
                                  clear_change_amount,
                                  clear_change_currency,
                                  clear_change_percent,
                                  clear_change_selling_uom,
                                  clear_price_guide_id,
                                  loc_move_from_zone_id,
                                  loc_move_to_zone_id,
                                  location_move_id,
                                  lock_version,
                                  fr.on_simple_promo_ind,
                                  fr.on_complex_promo_ind,
                                  fr.max_hier_level,
                                  fr.future_retail_id
                             from rpm_price_inquiry_gtt t,
                                  rpm_future_retail fr
                            where fr.dept                 = t.dept
                              and fr.item                 = t.item_parent
                              and NVL(fr.diff_id, '-999') = NVL(t.diff_id, '-999')
                              and fr.location             = t.location
                              and fr.zone_node_type       = t.loc_type))
                      where fr_level_rank = max_fr_lvl_rank)) source
   on (    target.price_event_id         = source.price_event_id
       and target.item                   = source.item
       and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.action_date            = source.action_date)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              diff_id,
              item_parent,
              zone_node_type,
              location,
              zone_id,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              on_simple_promo_ind,
              on_complex_promo_ind,
              max_hier_level,
              cur_hier_level,
              original_fr_id,
              step_identifier)
          values
             (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.diff_id,
              source.item_parent,
              source.zone_node_type,
              source.location,
              source.zone_id,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.max_hier_level,
              source.cur_hier_level,
              source.future_retail_id,
              RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number ||
                               ' - I_pe_no_frs.COUNT: ' || I_pe_no_frs.COUNT,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;
END GENERATE_TIMELINE;

--------------------------------------------------------------------------------

FUNCTION GENERATE_TL_FROM_INTERSECTION(O_cc_error_tbl       OUT CONFLICT_CHECK_ERROR_TBL,
                                       I_bulk_cc_pe_id   IN     NUMBER,
                                       I_thread_number   IN     NUMBER,
                                       I_chunk_number    IN     NUMBER,
                                       I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program VARCHAR2(55) := 'RPM_CHUNK_CC_ACTIONS_SQL.GENERATE_TL_FROM_INTERSECTION';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   merge into rpm_future_retail_gtt target
   using (select /*+ ORDERED */
                 t.price_event_id,
                 fr.dept,
                 class,
                 subclass,
                 t.to_item item,
                 t.to_diff_id diff_id,
                 t.to_item_parent item_parent,
                 t.to_zone_node_type zone_node_type,
                 t.to_location location,
                 t.to_zone_id zone_id,
                 action_date,
                 selling_retail,
                 selling_retail_currency,
                 selling_uom,
                 multi_units,
                 multi_unit_retail,
                 multi_unit_retail_currency,
                 multi_selling_uom,
                 clear_retail,
                 clear_retail_currency,
                 clear_uom,
                 simple_promo_retail,
                 simple_promo_retail_currency,
                 simple_promo_uom,
                 price_change_id,
                 price_change_display_id,
                 pc_exception_parent_id,
                 pc_change_type,
                 pc_change_amount,
                 pc_change_currency,
                 pc_change_percent,
                 pc_change_selling_uom,
                 pc_null_multi_ind,
                 pc_multi_units,
                 pc_multi_unit_retail,
                 pc_multi_unit_retail_currency,
                 pc_multi_selling_uom,
                 pc_price_guide_id,
                 clearance_id,
                 clearance_display_id,
                 clear_mkdn_index,
                 clear_start_ind,
                 clear_change_type,
                 clear_change_amount,
                 clear_change_currency,
                 clear_change_percent,
                 clear_change_selling_uom,
                 clear_price_guide_id,
                 loc_move_from_zone_id,
                 loc_move_to_zone_id,
                 location_move_id,
                 lock_version,
                 fr.on_simple_promo_ind,
                 fr.on_complex_promo_ind,
                 fr.max_hier_level,
                 t.to_cur_hier_level cur_hier_level,
                 fr.future_retail_id
            from (select price_event_id,
                         dept,
                         from_item,
                         from_diff_id,
                         from_location,
                         from_zone_node_type,
                         to_item,
                         to_diff_id,
                         to_item_parent,
                         to_location,
                         to_zone_node_type,
                         to_zone_id,
                         to_cur_hier_level,
                         fr_rank,
                         MAX(fr_rank) OVER (PARTITION BY price_event_id,
                                                         to_item,
                                                         to_diff_id,
                                                         to_location,
                                                         to_zone_node_type) max_fr_rank
                    from (-- PD/Z and P/L intersection - Generate PD/L timeline:
                          -- working with PD/Z and P/L timeline exists
                          with item_locs as
                             (select /*+ ORDERED USE_HASH(i) USE_HASH(l) */ i.price_event_id,
                                     i.item,
                                     i.dept,
                                     i.diff_id,
                                     i.pe_merch_level,
                                     i.item_parent,
                                     i.merch_level_type,
                                     i.subclass,
                                     i.class,
                                     l.location,
                                     l.zone_id,
                                     l.zone_node_type
                                from (select /*+ ORDERED USE_HASH(i) */
                                             bulk_cc_pe_id,
                                             price_event_id,
                                             item,
                                             dept,
                                             diff_id,
                                             pe_merch_level,
                                             item_parent,
                                             merch_level_type,
                                             subclass,
                                             class,
                                             itemloc_id,
                                             RANK() OVER (PARTITION BY item,
                                                                       diff_id
                                                              ORDER BY NVL(txn_man_excluded_item, 0),
                                                                       pe_merch_level desc,
                                                                       price_event_id) rank
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_bulk_cc_pe_item
                                       where bulk_cc_pe_id          = I_bulk_cc_pe_id
                                         and price_event_id         = VALUE(ids)
                                         and thread_number          = I_thread_number
                                         and chunk_number           = I_chunk_number
                                         and txn_man_excluded_item != 1) i,
                                     rpm_bulk_cc_pe_location l
                               where i.rank           = 1
                                 and i.bulk_cc_pe_id  = l.bulk_cc_pe_id
                                 and i.price_event_id = l.price_event_id
                                 and i.itemloc_id     = l.itemloc_id)
                          select /*+ ORDERED */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 NULL                           from_diff_id,
                                 rzl.location                   from_location,
                                 rzl.loc_type                   from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 NULL                           to_item_parent,
                                 rzl.location                   to_location,
                                 rzl.loc_type                   to_zone_node_type,
                                 rzl.zone_id                    to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.item_parent      is NULL
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and il.zone_id          is NULL
                             and rzl.zone_id         = il.location
                             and im.item_parent      = il.item
                             and im.diff_1           = il.diff_id
                             and ril.dept            = im.dept
                             and ril.item            = im.item
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.loc             = rzl.location
                             -- look for an existing timeline at the P/L level
                             and rfr.dept            = il.dept
                             and rfr.item            = il.item
                             and rfr.diff_id         is NULL
                             and rfr.location        = rzl.location
                             and rfr.zone_node_type  = rzl.loc_type
                             and rownum              > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = rzl.location
                                                and gtt.zone_node_type       = rzl.loc_type)
                          union all
                          -- working with P/L and PD/Z timeline exists
                          select /*+ ORDERED */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 il.diff_id                     from_diff_id,
                                 rzl.zone_id                    from_location,
                                 rfr.zone_node_type             from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 NULL                           to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 rzl.zone_id                    to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_merch_retail_def_expl rmrde,
                                 rpm_zone rz,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.item_parent           is NULL
                             and il.zone_node_type        IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id               is NULL
                             and il.dept                  = rmrde.dept
                             and il.class                 = rmrde.class
                             and il.subclass              = rmrde.subclass
                             and rmrde.regular_zone_group = rz.zone_group_id
                             and rz.zone_id               = rzl.zone_id
                             and rzl.location             = il.location
                             and im.item_parent           = il.item
                             and im.diff_1                = il.diff_id
                             and ril.dept                 = im.dept
                             and ril.item                 = im.item
                             and im.status                = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind          = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             and ril.loc                  = il.location
                             -- Look for an existing PD/Z timeline
                             and rfr.dept                 = il.dept
                             and rfr.item                 = il.item
                             and rfr.diff_id              is NOT NULL
                             and rfr.diff_id              = il.diff_id
                             and rfr.location             = rzl.zone_id
                             and rfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and rownum                   > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- P/L and I/Z intersection - Generate I/L timeline:
                          -- working with P/L and I/Z exists
                          select /*+ ORDERED */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 il.diff_id                     from_diff_id,
                                 rz.zone_id                     from_location,
                                 rfr.zone_node_type             from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 rz.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 rpm_merch_retail_def_expl rmrde,
                                 rpm_zone rz,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.item_parent           is NOT NULL
                             and il.zone_node_type        IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id               is NULL
                             and il.dept                  = rmrde.dept
                             and il.class                 = rmrde.class
                             and il.subclass              = rmrde.subclass
                             and rmrde.regular_zone_group = rz.zone_group_id
                             and rz.zone_id               = rzl.zone_id
                             and rzl.location             = il.location
                             -- Look for an existing timeline at the I/Z level
                             and rfr.dept                 = il.dept
                             and rfr.item                 = il.item
                             and NVL(rfr.diff_id, '-999') = NVL(il.diff_id, '-999')
                             and rfr.location             = rz.zone_id
                             and rfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept                 = il.dept
                             and ril.item                 = il.item
                             and ril.loc                  = il.location
                             and rownum                   > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- working with I/Z and P/L exists
                          select /*+ ORDERED USE_NL(ril) */
                                 distinct 0 fr_rank,
                                 il.price_event_id,
                                 im.dept,
                                 im.item_parent                 from_item,
                                 NULL                           from_diff_id,
                                 il.location                    from_loc,
                                 il.zone_node_type              from_zone_node_type,
                                 im.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 im.item_parent                 to_item_parent,
                                 il.location                    to_loc,
                                 il.zone_node_type              to_zone_node_type,
                                 il.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.item_parent      is NULL
                             and il.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id          is NOT NULL
                             and im.item             = il.item
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             -- Look for an existing P/L timeline
                             and rfr.dept            = im.dept
                             and rfr.item            = im.item_parent
                             and rfr.diff_id         is NULL
                             and rfr.location        = il.location
                             and rfr.zone_node_type  = il.zone_node_type
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept            = im.dept
                             and ril.item            = im.item
                             and ril.loc             = il.location
                             and rownum              > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = im.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- PD/L and I/Z intersection - Generate I/L timeline:
                          -- working with PD/L and I/Z exists
                          select /*+ ORDERED */
                                 distinct 1 fr_rank,
                                 il.price_event_id,
                                 il.dept,
                                 il.item                        from_item,
                                 il.diff_id                     from_diff_id,
                                 rz.zone_id                     from_location,
                                 rfr.zone_node_type             from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 rz.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 rpm_merch_retail_def_expl rmrde,
                                 rpm_zone rz,
                                 rpm_zone_location rzl,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.item_parent           is NOT NULL
                             and il.zone_node_type        IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                              RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id               is NULL
                             and il.dept                  = rmrde.dept
                             and il.class                 = rmrde.class
                             and il.subclass              = rmrde.subclass
                             and rmrde.regular_zone_group = rz.zone_group_id
                             and rz.zone_id               = rzl.zone_id
                             and rzl.location             = il.location
                             -- Look for an existing timeline at the I/Z level
                             and rfr.dept                 = il.dept
                             and rfr.item                 = il.item
                             and NVL(rfr.diff_id, '-999') = NVL(il.diff_id, '-999')
                             and rfr.location             = rz.zone_id
                             and rfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept                 = il.dept
                             and ril.item                 = il.item
                             and ril.loc                  = il.location
                             and rownum                   > 0
                             -- Make sure that there is not a timeline at the I/L level already
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = il.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type)
                          union all
                          -- working with I/Z and PD/L exists
                          select /*+ ORDERED USE_NL(ril) */
                                 distinct 1 fr_rank,
                                 il.price_event_id,
                                 im.dept,
                                 im.item_parent                 from_item,
                                 il.diff_id                     from_diff_id,
                                 il.location                    from_loc,
                                 il.zone_node_type              from_zone_node_type,
                                 im.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 im.item_parent                 to_item_parent,
                                 il.location                    to_loc,
                                 il.zone_node_type              to_zone_node_type,
                                 il.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from item_locs il,
                                 item_master im,
                                 rpm_item_loc ril,
                                 rpm_future_retail rfr
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.item_parent      is NULL
                             and il.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                             and il.zone_id          is NOT NULL
                             and im.item             = il.item
                             and im.status           = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                             and im.sellable_ind     = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                             -- Look for an existing PD/L timeline
                             and rfr.dept            = im.dept
                             and rfr.item            = im.item_parent
                             and rfr.diff_id         is NOT NULL
                             and rfr.diff_id         = il.diff_id
                             and rfr.location        = il.location
                             and rfr.zone_node_type  = il.zone_node_type
                             -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                             and ril.dept            = im.dept
                             and ril.item            = im.item
                             and ril.loc             = il.location
                             and rownum              > 0
                             and NOT EXISTS (select 'x'
                                               from rpm_future_retail_gtt gtt
                                              where gtt.dept                 = im.dept
                                                and gtt.item                 = il.item
                                                and NVL(gtt.diff_id, '-999') = NVL(il.diff_id, '-999')
                                                and gtt.location             = il.location
                                                and gtt.zone_node_type       = il.zone_node_type))) t,
                    rpm_future_retail fr
              where t.fr_rank               = t.max_fr_rank
                and fr.dept                 = t.dept
                and fr.item                 = t.from_item
                and NVL(fr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and fr.location             = t.from_location
                and fr.zone_node_type       = t.from_zone_node_type) source
   on (    target.price_event_id         = source.price_event_id
       and target.item                   = source.item
       and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.action_date            = source.action_date)
   when NOT MATCHED then
      insert (price_event_id,
              future_retail_id,
              dept,
              class,
              subclass,
              item,
              diff_id,
              item_parent,
              zone_node_type,
              location,
              zone_id,
              action_date,
              selling_retail,
              selling_retail_currency,
              selling_uom,
              multi_units,
              multi_unit_retail,
              multi_unit_retail_currency,
              multi_selling_uom,
              clear_retail,
              clear_retail_currency,
              clear_uom,
              simple_promo_retail,
              simple_promo_retail_currency,
              simple_promo_uom,
              price_change_id,
              price_change_display_id,
              pc_exception_parent_id,
              pc_change_type,
              pc_change_amount,
              pc_change_currency,
              pc_change_percent,
              pc_change_selling_uom,
              pc_null_multi_ind,
              pc_multi_units,
              pc_multi_unit_retail,
              pc_multi_unit_retail_currency,
              pc_multi_selling_uom,
              pc_price_guide_id,
              clearance_id,
              clearance_display_id,
              clear_mkdn_index,
              clear_start_ind,
              clear_change_type,
              clear_change_amount,
              clear_change_currency,
              clear_change_percent,
              clear_change_selling_uom,
              clear_price_guide_id,
              loc_move_from_zone_id,
              loc_move_to_zone_id,
              location_move_id,
              lock_version,
              on_simple_promo_ind,
              on_complex_promo_ind,
              max_hier_level,
              cur_hier_level,
              original_fr_id,
              step_identifier)
      values (source.price_event_id,
              RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
              source.dept,
              source.class,
              source.subclass,
              source.item,
              source.diff_id,
              source.item_parent,
              source.zone_node_type,
              source.location,
              source.zone_id,
              source.action_date,
              source.selling_retail,
              source.selling_retail_currency,
              source.selling_uom,
              source.multi_units,
              source.multi_unit_retail,
              source.multi_unit_retail_currency,
              source.multi_selling_uom,
              source.clear_retail,
              source.clear_retail_currency,
              source.clear_uom,
              source.simple_promo_retail,
              source.simple_promo_retail_currency,
              source.simple_promo_uom,
              source.price_change_id,
              source.price_change_display_id,
              source.pc_exception_parent_id,
              source.pc_change_type,
              source.pc_change_amount,
              source.pc_change_currency,
              source.pc_change_percent,
              source.pc_change_selling_uom,
              source.pc_null_multi_ind,
              source.pc_multi_units,
              source.pc_multi_unit_retail,
              source.pc_multi_unit_retail_currency,
              source.pc_multi_selling_uom,
              source.pc_price_guide_id,
              source.clearance_id,
              source.clearance_display_id,
              source.clear_mkdn_index,
              source.clear_start_ind,
              source.clear_change_type,
              source.clear_change_amount,
              source.clear_change_currency,
              source.clear_change_percent,
              source.clear_change_selling_uom,
              source.clear_price_guide_id,
              source.loc_move_from_zone_id,
              source.loc_move_to_zone_id,
              source.location_move_id,
              source.lock_version,
              source.on_simple_promo_ind,
              source.on_complex_promo_ind,
              source.max_hier_level,
              source.cur_hier_level,
              source.future_retail_id,
              RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL_FROM_INT);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   RPM_CONSTANTS.CAPT_GTT_GEN_RFR_TL_FROM_INT,
                                                   1, -- RFR
                                                   0, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_TL_FROM_INTERSECTION;
--------------------------------------------------------------------------------

FUNCTION GEN_PILE_TL(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                     I_bulk_cc_pe_id    IN     NUMBER,
                     I_thread_number    IN     NUMBER,
                     I_chunk_number     IN     NUMBER,
                     I_price_event_type IN     VARCHAR2,
                     I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                     I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_CHUNK_CC_ACTIONS_SQL.GEN_PILE_TL';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   -- Generate RPM_PROMO_ITEM_LOC_EXPL records for the price events being processed when there is already
   -- an existing "parent" timeline.  The new records need to be at the level of the new promotion and for
   -- the exist promotion.  Need to populate the PROMO_ITEM_LOC_EXPL_ID column from the sequence but should
   -- leave the rpile_rowid to NULL so it will be pushed back.

   merge into rpm_promo_item_loc_expl_gtt target
   using (select price_event_id,
                 to_item item,
                 to_diff_id diff_id,
                 to_item_parent item_parent,
                 dept,
                 class,
                 subclass,
                 to_location location,
                 to_zone_node_type zone_node_type,
                 to_zone_id zone_id,
                 promo_id,
                 promo_display_id,
                 promo_secondary_ind,
                 promo_comp_id,
                 comp_display_id,
                 promo_dtl_id,
                 type,
                 customer_type,
                 detail_secondary_ind,
                 detail_start_date,
                 detail_end_date,
                 detail_apply_to_code,
                 detail_change_type,
                 detail_change_amount,
                 detail_change_currency,
                 detail_change_percent,
                 detail_change_selling_uom,
                 detail_price_guide_id,
                 exception_parent_id,
                 promo_item_loc_expl_id,
                 max_hier_level,
                 to_cur_hier_level cur_hier_level,
                 timebased_dtl_ind
            from (select /*+ ORDERED */
                         t.price_event_id,
                         t.to_item,
                         t.to_diff_id,
                         t.to_item_parent,
                         ilex.dept,
                         ilex.class,
                         ilex.subclass,
                         t.to_location,
                         t.to_zone_node_type,
                         t.to_zone_id,
                         ilex.promo_id,
                         ilex.promo_display_id,
                         ilex.promo_secondary_ind,
                         ilex.promo_comp_id,
                         ilex.comp_display_id,
                         ilex.promo_dtl_id,
                         ilex.type,
                         ilex.customer_type,
                         ilex.detail_secondary_ind,
                         ilex.detail_start_date,
                         ilex.detail_end_date,
                         ilex.detail_apply_to_code,
                         ilex.detail_change_type,
                         ilex.detail_change_amount,
                         ilex.detail_change_currency,
                         ilex.detail_change_percent,
                         ilex.detail_change_selling_uom,
                         ilex.detail_price_guide_id,
                         ilex.exception_parent_id,
                         ilex.promo_item_loc_expl_id,
                         ilex.max_hier_level,
                         t.to_cur_hier_level,
                         ilex.timebased_dtl_ind,
                         t.rank,
                         MAX(rank) OVER (PARTITION BY t.price_event_id,
                                                      ilex.promo_dtl_id,
                                                      t.to_item,
                                                      t.to_diff_id,
                                                      t.to_location,
                                                      t.to_zone_node_type) max_rank
                    from (with item_locs as
                             (select /*+ MATERIALIZE ORDERED */
                                     rpi.price_event_id,
                                     rpi.item,
                                     rpl.location,
                                     rpl.zone_node_type,
                                     rpl.zone_id,
                                     rpi.diff_id,
                                     rpi.pe_merch_level,
                                     rpi.merch_level_type,
                                     im.subclass,
                                     im.class,
                                     im.dept,
                                     im.item_parent
                                from (select bulk_cc_pe_id,
                                             price_event_id,
                                             item,
                                             diff_id,
                                             pe_merch_level,
                                             merch_level_type,
                                             itemloc_id,
                                             RANK() OVER (PARTITION BY item,
                                                                       diff_id
                                                              ORDER BY NVL(txn_man_excluded_item, 0),
                                                                       pe_merch_level desc,
                                                                       price_event_id) rank
                                        from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                             rpm_bulk_cc_pe_item
                                       where bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                         and price_event_id                 = VALUE(ids)
                                         and thread_number                  = I_thread_number
                                         and chunk_number                   = I_chunk_number
                                         and NVL(txn_man_excluded_item, 0) != 1) rpi,
                                     item_master im,
                                     rpm_bulk_cc_pe_location rpl
                               where rpi.rank           = 1
                                 and rpl.bulk_cc_pe_id  = rpi.bulk_cc_pe_id
                                 and rpl.price_event_id = rpi.price_event_id
                                 and rpl.itemloc_id     = rpi.itemloc_id
                                 and im.status          = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind    = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and im.item            = rpi.item),
                          zone_locations as
                             (select /*+ MATERIALIZE */ rz.zone_group_id,
                                     rz.zone_id,
                                     rzl.location,
                                     rzl.loc_type
                                from rpm_zone rz,
                                     rpm_zone_location rzl
                               where rzl.zone_id = rz.zone_id),
                          merch_retail_def_expl as
                             (select /*+ MATERIALIZE */
                                     dept,
                                     class,
                                     subclass,
                                     regular_zone_group
                                from rpm_merch_retail_def_expl)
                          -- Parent Diff Zone price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                         from_item,
                                 NULL                            from_diff_id,
                                 il.location                     from_location,
                                 il.zone_node_type               from_zone_node_type,
                                 il.item                         to_item,
                                 il.diff_id                      to_diff_id,
                                 NULL                            to_item_parent,
                                 il.location                     to_location,
                                 il.zone_node_type               to_zone_node_type,
                                 NULL                            to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level
                            from item_locs il
                           where il.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and rownum             >= 1
                          union all
                          -- Parent Loc price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 NULL                              from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 NULL                              to_diff_id,
                                 NULL                              to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_MERCH_TYPE
                             and il.location              = zl.location
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.zone_id               = il.zone_id
                             and rownum                  >= 1
                          union all
                          -- Item Zone price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                  from_item,
                                 NULL                            from_diff_id,
                                 il.location                     from_location,
                                 il.zone_node_type               from_zone_node_type,
                                 il.item                         to_item,
                                 il.diff_id                      to_diff_id,
                                 il.item_parent                  to_item_parent,
                                 il.location                     to_location,
                                 il.zone_node_type               to_zone_node_type,
                                 NULL                            to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                            from item_locs il
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and il.zone_id          is NULL
                             and rownum             >= 1
                          union all
                          -- Look at the PDZ level
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                  from_item,
                                 il.diff_id                      from_diff_id,
                                 il.location                     from_location,
                                 il.zone_node_type               from_zone_node_type,
                                 il.item                         to_item,
                                 il.diff_id                      to_diff_id,
                                 il.item_parent                  to_item_parent,
                                 il.location                     to_location,
                                 il.zone_node_type               to_zone_node_type,
                                 NULL                            to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                            from item_locs il
                           where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                             and il.zone_id          is NULL
                             and rownum             >= 1
                          union all
                          -- Parent Diff Loc price Events
                          -- Look at the PZ level
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 NULL                              from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 NULL                              to_item_parent,
                                 zl.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and il.zone_id               = zl.zone_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and rownum                  >= 1
                          union all
                          -- Look at the PDZ level
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 il.diff_id                        from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 NULL                              to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and rownum                  >= 1
                          union all
                          -- Look at the PL level
                          select distinct
                                 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                        from_item,
                                 NULL                           from_diff_id,
                                 il.location                    from_location,
                                 il.zone_node_type              from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 NULL                           to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 zl.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level
                            from (select il.price_event_id,
                                         il.dept,
                                         il.item,
                                         il.location,
                                         il.zone_node_type,
                                         il.diff_id,
                                         rmrde.regular_zone_group
                                    from item_locs il,
                                         merch_retail_def_expl rmrde
                                   where il.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                     and il.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                     and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                     and il.zone_id          is NULL
                                     and rmrde.dept          = il.dept
                                     and rmrde.class         = il.class
                                     and rmrde.subclass      = il.subclass) il,
                                 zone_locations zl
                           where il.regular_zone_group = zl.zone_group_id (+)
                             and il.location           = zl.location (+)
                             and rownum               >= 1
                          union all
                          -- Item Location Level Price Events
                          -- Look at all levels above and rank, then use the data with the highest rank
                          -- Look at P/Z level first and rank that data 0
                          select distinct 0 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                    from_item,
                                 NULL                              from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 il.item_parent                    to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rownum                  >= 1
                          union all
                          -- Look at PD/Z level and rank that data 1
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                    from_item,
                                 il.diff_id                        from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 il.item_parent                    to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and il.zone_id               is NULL
                             and rownum                  >= 1
                          union all
                          -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                          select distinct 1 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                 from_item,
                                 NULL                           from_diff_id,
                                 il.location                    from_location,
                                 il.zone_node_type              from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 zl.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from (select il.price_event_id,
                                         il.dept,
                                         il.diff_id,
                                         il.item_parent,
                                         il.location,
                                         il.zone_node_type,
                                         il.item,
                                         rmrde.regular_zone_group
                                    from item_locs il,
                                         merch_retail_def_expl rmrde
                                   where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                     and il.zone_id          is NULL
                                     and rmrde.dept          = il.dept
                                     and rmrde.class         = il.class
                                     and rmrde.subclass      = il.subclass
                                     and rownum              > 0) il,
                                 zone_locations zl
                           where il.regular_zone_group = zl.zone_group_id (+)
                             and il.location           = zl.location (+)
                             and rownum               >= 1
                          union all
                          -- Look at I/Z level and rank that data 2
                          select distinct 2 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item                           from_item,
                                 il.diff_id                        from_diff_id,
                                 zl.zone_id                        from_location,
                                 RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                 il.item                           to_item,
                                 il.diff_id                        to_diff_id,
                                 il.item_parent                    to_item_parent,
                                 il.location                       to_location,
                                 il.zone_node_type                 to_zone_node_type,
                                 zl.zone_id                        to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                            from item_locs il,
                                 merch_retail_def_expl rmrde,
                                 zone_locations zl
                           where il.merch_level_type      = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.pe_merch_level        = RPM_CONSTANTS.ITEM_MERCH_TYPE
                             and il.zone_node_type        = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                             and rmrde.dept               = il.dept
                             and rmrde.class              = il.class
                             and rmrde.subclass           = il.subclass
                             and rmrde.regular_zone_group = zl.zone_group_id
                             and zl.location              = il.location
                             and zl.loc_type              = il.zone_node_type
                             and il.zone_id               is NULL
                             and rownum                  >= 1
                          union all
                          -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                          select distinct 2 rank,
                                 il.dept,
                                 il.price_event_id,
                                 il.item_parent                 from_item,
                                 il.diff_id                     from_diff_id,
                                 il.location                    from_location,
                                 il.zone_node_type              from_zone_node_type,
                                 il.item                        to_item,
                                 il.diff_id                     to_diff_id,
                                 il.item_parent                 to_item_parent,
                                 il.location                    to_location,
                                 il.zone_node_type              to_zone_node_type,
                                 zl.zone_id                     to_zone_id,
                                 RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                            from (select il.price_event_id,
                                         il.dept,
                                         il.item_parent,
                                         il.diff_id,
                                         il.location,
                                         il.zone_node_type,
                                         il.item,
                                         rmrde.regular_zone_group
                                    from item_locs il,
                                         merch_retail_def_expl rmrde
                                   where il.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                     and il.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_STORE
                                     and il.zone_id          is NULL
                                     and rmrde.dept          = il.dept
                                     and rmrde.class         = il.class
                                     and rmrde.subclass      = il.subclass) il,
                                 zone_locations zl
                           where il.regular_zone_group = zl.zone_group_id (+)
                             and il.location           = zl.location (+)
                             and rownum               >= 1) t,
                         rpm_promo_item_loc_expl ilex,
                         rpm_promo_dtl rpd,
                         rpm_promo_comp rpc
                   where ilex.dept                 = t.dept
                     and ilex.item                 = t.from_item
                     and NVL(ilex.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                     and ilex.location             = t.from_location
                     and ilex.zone_node_type       = t.from_zone_node_type
                     and ilex.promo_dtl_id         = case
                                                        when I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                                                                                    RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                                                    RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                                                    RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then
                                                           ilex.promo_dtl_id
                                                        else
                                                           t.price_event_id
                                                     end
                     and rpd.promo_dtl_id          = ilex.promo_dtl_id
                     and rpc.promo_comp_id         = rpd.promo_comp_id
                     -- if dealing with PC, CL, CR, NR or LM, we don't care about the customer type - we want
                     -- to process promotions that are both customer specific and those that are not.
                     and (   I_price_event_type      IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                                                         RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                                                         RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                                                         RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET)
                     -- If dealing with a promotion (CS or not), we need to make sure that we only pull back
                     -- the appropriate data - if CS, need to get the righ customer type, if not CS, we need to
                     -- make sure that there is no customer type in the "parent" promo record
                          or (    I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_PROMOTION,
                                                         RPM_CONSTANTS.PE_TYPE_TRANSACTION_UPDATE)
                              and rpc.customer_type  is NULL
                              and ilex.customer_type is NULL)
                          or (    I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_PROMO,
                                                         RPM_CONSTANTS.PE_TYPE_CUST_SEG_TRAN_UPDATE)
                              and rpc.customer_type  = ilex.customer_type)))
           where rank = max_rank) source
   on (    target.price_event_id         = source.price_event_id
       and target.item                   = source.item
       and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
       and target.location               = source.location
       and target.zone_node_type         = source.zone_node_type
       and target.promo_dtl_id           = source.promo_dtl_id)
   when NOT MATCHED then
      insert (price_event_id,
              promo_item_loc_expl_id,
              item,
              diff_id,
              item_parent,
              dept,
              class,
              subclass,
              location,
              zone_node_type,
              zone_id,
              promo_id,
              promo_display_id,
              promo_secondary_ind,
              promo_comp_id,
              comp_display_id,
              promo_dtl_id,
              type,
              customer_type,
              detail_secondary_ind,
              detail_start_date,
              detail_end_date,
              detail_apply_to_code,
              detail_change_type,
              detail_change_amount,
              detail_change_currency,
              detail_change_percent,
              detail_change_selling_uom,
              detail_price_guide_id,
              exception_parent_id,
              timebased_dtl_ind,
              original_pile_id,
              max_hier_level,
              cur_hier_level,
              step_identifier)
      values (source.price_event_id,
              RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
              source.item,
              source.diff_id,
              source.item_parent,
              source.dept,
              source.class,
              source.subclass,
              source.location,
              source.zone_node_type,
              source.zone_id,
              source.promo_id,
              source.promo_display_id,
              source.promo_secondary_ind,
              source.promo_comp_id,
              source.comp_display_id,
              source.promo_dtl_id,
              source.type,
              source.customer_type,
              source.detail_secondary_ind,
              source.detail_start_date,
              source.detail_end_date,
              source.detail_apply_to_code,
              source.detail_change_type,
              source.detail_change_amount,
              source.detail_change_currency,
              source.detail_change_percent,
              source.detail_change_selling_uom,
              source.detail_price_guide_id,
              source.exception_parent_id,
              source.timebased_dtl_ind,
              source.promo_item_loc_expl_id,
              source.max_hier_level,
              source.cur_hier_level,
              I_step_identifier);

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   1, -- RPILE
                                                   0, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GEN_PILE_TL;
--------------------------------------------------------------------------------

FUNCTION GENERATE_CSPFR_TIMELINE(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                                 I_bulk_cc_pe_id    IN     NUMBER,
                                 I_thread_number    IN     NUMBER,
                                 I_chunk_number     IN     NUMBER,
                                 I_price_event_type IN     VARCHAR2,
                                 I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                                 I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_ACTIONS_SQL.GENERATE_CSPFR_TIMELINE';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   -- if dealing with PC, CL, CR, NR or LM, we don't care about the customer type - we want
   -- to process promotions that are both customer specific and those that are not.
   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    action_date,
                    customer_type,
                    dept,
                    promo_retail,
                    promo_retail_currency,
                    promo_uom,
                    complex_promo_ind,
                    max_hier_level,
                    to_cur_hier_level cur_hier_level,
                    cust_segment_promo_id
               from (select /*+ ORDERED */
                            t.price_event_id,
                            t.to_item,
                            t.to_diff_id,
                            t.to_item_parent,
                            t.to_zone_node_type,
                            t.to_location,
                            t.to_zone_id,
                            cspfr.action_date,
                            cspfr.customer_type,
                            cspfr.dept,
                            cspfr.promo_retail,
                            cspfr.promo_retail_currency,
                            cspfr.promo_uom,
                            cspfr.complex_promo_ind,
                            NULL,
                            cspfr.max_hier_level,
                            t.to_cur_hier_level,
                            cspfr.cust_segment_promo_id,
                            t.rank,
                            MAX(t.rank) OVER (PARTITION BY t.price_event_id,
                                                           cspfr.customer_type,
                                                           t.to_item,
                                                           t.to_diff_id,
                                                           t.to_location,
                                                           t.to_zone_node_type) max_rank
                       from (-- Parent Diff Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                        from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    NULL                            to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                and rpi.thread_number    = I_thread_number
                                and rpi.chunk_number     = I_chunk_number
                                and rpi.price_event_id   = VALUE(ids)
                                and rpl.price_event_id   = VALUE(ids)
                                and rpi.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                and rpl.price_event_id   = rpi.price_event_id
                                and rpl.itemloc_id       = rpi.itemloc_id
                                and rpl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id          is NULL
                                and im.item_parent       = rpi.item
                                and im.diff_1            = rpi.diff_id
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = rzl.location
                                and rzl.zone_id          = rpl.location
                                and rownum               >= 1
                             union all
                             -- Parent Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    NULL                              to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    NULL                              to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.thread_number        = I_thread_number
                                and rpi.chunk_number         = I_chunk_number
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rzl.location             = rpl.location
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and im.item_parent           = rpi.item
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                   >= 1
                             union all
                             -- Item Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                and rpi.thread_number    = I_thread_number
                                and rpi.chunk_number     = I_chunk_number
                                and rpi.price_event_id   = VALUE(ids)
                                and rpl.price_event_id   = VALUE(ids)
                                and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                and rpl.price_event_id   = rpi.price_event_id
                                and rpl.itemloc_id       = rpi.itemloc_id
                                and rpl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id          is NULL
                                and im.item              = rpi.item
                                and rzl.zone_id          = rpl.location
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = rzl.location
                                and rownum              >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    rpi.diff_id                     from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                and rpi.thread_number    = I_thread_number
                                and rpi.chunk_number     = I_chunk_number
                                and rpi.price_event_id   = VALUE(ids)
                                and rpl.price_event_id   = VALUE(ids)
                                and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                and rpl.price_event_id   = rpi.price_event_id
                                and rpl.itemloc_id       = rpi.itemloc_id
                                and rpl.zone_node_type   = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id          is NULL
                                and im.item              = rpi.item
                                and rzl.zone_id          = rpl.location
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = rzl.location
                                and rownum              >= 1
                             union all
                             -- Parent Diff Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.thread_number        = I_thread_number
                                and rpi.chunk_number         = I_chunk_number
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and im.item_parent           = rpi.item
                                and im.diff_1                = rpi.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.thread_number        = I_thread_number
                                and rpi.chunk_number         = I_chunk_number
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and im.item_parent           = rpi.item
                                and im.diff_1                = rpi.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at the PL level
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item                           from_item,
                                    NULL                              from_diff_id,
                                    il.location                       from_location,
                                    il.zone_node_type                 from_zone_node_type,
                                    il.item                           to_item,
                                    il.diff_id                        to_diff_id,
                                    NULL                              to_item_parent,
                                    il.location                       to_location,
                                    il.zone_node_type                 to_zone_node_type,
                                    zl.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.item,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.diff_id,
                                            rmrde.regular_zone_group
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                        and rpi.thread_number    = I_thread_number
                                        and rpi.chunk_number     = I_chunk_number
                                        and rpi.price_event_id   = VALUE(ids)
                                        and rpl.price_event_id   = VALUE(ids)
                                        and rpi.merch_level_type = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpi.pe_merch_level   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id   = rpi.price_event_id
                                        and rpl.itemloc_id       = rpi.itemloc_id
                                        and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id          is NULL
                                        and rmrde.dept           = rpi.dept
                                        and rmrde.class          = rpi.class
                                        and rmrde.subclass       = rpi.subclass
                                        and im.item_parent       = rpi.item
                                        and im.diff_1            = rpi.diff_id
                                        and ril.dept             = im.dept
                                        and ril.item             = im.item
                                        and ril.loc              = rpl.location
                                        and rownum               > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Item Location Level Price Events
                             -- Look at all levels above and rank, then use the data with the highest rank
                             -- Look at P/Z level first and rank that data 0
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.thread_number        = I_thread_number
                                and rpi.chunk_number         = I_chunk_number
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                  = rpi.item
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at PD/Z level and rank that data 1
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.thread_number        = I_thread_number
                                and rpi.chunk_number         = I_chunk_number
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                  = rpi.item
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                      is NULL
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.diff_id,
                                            im.item_parent,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                        and rpi.thread_number    = I_thread_number
                                        and rpi.chunk_number     = I_chunk_number
                                        and rpi.price_event_id   = VALUE(ids)
                                        and rpl.price_event_id   = VALUE(ids)
                                        and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item              = rpi.item
                                        and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id   = rpi.price_event_id
                                        and rpl.itemloc_id       = rpi.itemloc_id
                                        and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id          is NULL
                                        and rmrde.dept           = rpi.dept
                                        and rmrde.class          = rpi.class
                                        and rmrde.subclass       = rpi.subclass
                                        and ril.dept             = im.dept
                                        and ril.item             = im.item
                                        and ril.loc              = rpl.location
                                        and rownum               > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Look at I/Z level and rank that data 2
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    2 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id        = I_bulk_cc_pe_id
                                and rpi.thread_number        = I_thread_number
                                and rpi.chunk_number         = I_chunk_number
                                and rpi.price_event_id       = VALUE(ids)
                                and rpl.price_event_id       = VALUE(ids)
                                and rpi.merch_level_type     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id        = rpi.bulk_cc_pe_id
                                and rpl.price_event_id       = rpi.price_event_id
                                and rpl.itemloc_id           = rpi.itemloc_id
                                and rpl.zone_node_type       IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id              is NULL
                                and rmrde.dept               = rpi.dept
                                and rmrde.class              = rpi.class
                                and rmrde.subclass           = rpi.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = rpl.location
                                and rzl.loc_type             = rpl.zone_node_type
                                and rpl.zone_id              is NULL
                                and rpi.item                 = im.item
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = rpl.location
                                and rownum                  >= 1
                             union all
                             -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                             select distinct
                                    2 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    il.diff_id                     from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            im.item_parent,
                                            rpi.diff_id,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id    = I_bulk_cc_pe_id
                                        and rpi.thread_number    = I_thread_number
                                        and rpi.chunk_number     = I_chunk_number
                                        and rpi.price_event_id   = VALUE(ids)
                                        and rpl.price_event_id   = VALUE(ids)
                                        and rpi.merch_level_type = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item              = rpi.item
                                        and rpl.bulk_cc_pe_id    = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id   = rpi.price_event_id
                                        and rpl.itemloc_id       = rpi.itemloc_id
                                        and rpl.zone_node_type   IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                     RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id          is NULL
                                        and rmrde.dept           = rpi.dept
                                        and rmrde.class          = rpi.class
                                        and rmrde.subclass       = rpi.subclass
                                        and ril.dept             = im.dept
                                        and ril.item             = im.item
                                        and ril.loc              = rpl.location
                                        and rownum               > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1) t,
                            rpm_cust_segment_promo_fr cspfr
                      where cspfr.dept                 = t.dept
                        and cspfr.item                 = t.from_item
                        and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                        and cspfr.location             = t.from_location
                        and cspfr.zone_node_type       = t.from_zone_node_type
                        and cspfr.customer_type        is NOT NULL) s
       where rank = max_rank
         and NOT EXISTS (select 1
                           from rpm_item_loc_gtt iloc
                          where iloc.price_event_id       = s.price_event_id
                            and iloc.item                 = s.to_item
                            and NVL(iloc.diff_id, '-999') = NVL(s.to_diff_id, '-999')
                            and iloc.location             = s.to_location
                            and iloc.regular_zone_group   = s.to_zone_node_type
                            and iloc.item_parent          = s.customer_type)
         and EXISTS (select 1
                       from rpm_promo_item_loc_expl_gtt ilex
                      where ilex.price_event_id       = s.price_event_id
                        and ilex.item                 = s.to_item
                        and NVL(ilex.diff_id, '-999') = NVL(s.to_diff_id, '-999')
                        and ilex.location             = s.to_location
                        and ilex.zone_node_type       = s.to_zone_node_type
                        and ilex.customer_type        = s.customer_type)) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert(price_event_id,
                cust_segment_promo_id,
                item,
                diff_id,
                item_parent,
                zone_node_type,
                location,
                zone_id,
                action_date,
                customer_type,
                dept,
                promo_retail,
                promo_retail_currency,
                promo_uom,
                complex_promo_ind,
                cspfr_rowid,
                max_hier_level,
                cur_hier_level,
                original_cs_promo_id,
                step_identifier)
         values(source.price_event_id,
                RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                source.item,
                source.diff_id,
                source.item_parent,
                source.zone_node_type,
                source.location,
                source.zone_id,
                source.action_date,
                source.customer_type,
                source.dept,
                source.promo_retail,
                source.promo_retail_currency,
                source.promo_uom,
                source.complex_promo_ind,
                NULL,
                source.max_hier_level,
                source.cur_hier_level,
                source.cust_segment_promo_id,
                I_step_identifier);

   else

      -- If dealing with a promotion (CS or not), we need to make sure that we only pull back
      -- the appropriate data - if CS, need to get the right customer type, if not CS, we need to
      -- make sure that there is no customer type in the "parent" promo record
      merge into rpm_cust_segment_promo_fr_gtt target
      using (select price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    action_date,
                    customer_type,
                    dept,
                    promo_retail,
                    promo_retail_currency,
                    promo_uom,
                    complex_promo_ind,
                    max_hier_level,
                    to_cur_hier_level cur_hier_level,
                    cust_segment_promo_id
               from (select /*+ ORDERED */
                            t.price_event_id,
                            t.to_item,
                            t.to_diff_id,
                            t.to_item_parent,
                            t.to_zone_node_type,
                            t.to_location,
                            t.to_zone_id,
                            cspfr.action_date,
                            cspfr.customer_type,
                            cspfr.dept,
                            cspfr.promo_retail,
                            cspfr.promo_retail_currency,
                            cspfr.promo_uom,
                            cspfr.complex_promo_ind,
                            NULL,
                            cspfr.max_hier_level,
                            t.to_cur_hier_level,
                            cspfr.cust_segment_promo_id,
                            t.rank,
                            MAX(t.rank) OVER (PARTITION BY t.price_event_id,
                                                           cspfr.customer_type,
                                                           t.to_item,
                                                           t.to_diff_id,
                                                           t.to_location,
                                                           t.to_zone_node_type) max_rank
                       from (-- Parent Diff Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                        from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    NULL                            to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_ZONE to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id                        is NULL
                                and im.item_parent                     = rpi.item
                                and im.diff_1                          = rpi.diff_id
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rzl.location
                                and rzl.zone_id                        = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Parent Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    NULL                              to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    NULL                              to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_PARENT_LOC  to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rzl.location                       = rpl.location
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and im.item_parent                     = rpi.item
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Item Zone price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    NULL                            from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id                        is NULL
                                and im.item                            = rpi.item
                                and rzl.zone_id                        = rpl.location
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rzl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                  from_item,
                                    rpi.diff_id                     from_diff_id,
                                    rpl.location                    from_location,
                                    rpl.zone_node_type              from_zone_node_type,
                                    rpi.item                        to_item,
                                    rpi.diff_id                     to_diff_id,
                                    im.item_parent                  to_item_parent,
                                    rpl.location                    to_location,
                                    rpl.zone_node_type              to_zone_node_type,
                                    NULL                            to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_ZONE to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rpl.zone_id                        is NULL
                                and im.item                            = rpi.item
                                and rzl.zone_id                        = rpl.location
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rzl.location
                                and rownum                            >= 1
                             union all
                             -- Parent Diff Loc price Events
                             -- Look at the PZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and im.item_parent                     = rpi.item
                                and im.diff_1                          = rpi.diff_id
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PDZ level
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    NULL                              to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and im.item_parent                     = rpi.item
                                and im.diff_1                          = rpi.diff_id
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PL level
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item                           from_item,
                                    NULL                              from_diff_id,
                                    il.location                       from_location,
                                    il.zone_node_type                 from_zone_node_type,
                                    il.item                           to_item,
                                    il.diff_id                        to_diff_id,
                                    NULL                              to_item_parent,
                                    il.location                       to_location,
                                    il.zone_node_type                 to_zone_node_type,
                                    zl.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC    to_cur_hier_level,
                                    promo_excl_rank
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.item,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.diff_id,
                                            rmrde.regular_zone_group,
                                            RANK() OVER (PARTITION BY rpi.item,
                                                                      rpi.diff_id,
                                                                      rpl.zone_node_type,
                                                                      rpl.location
                                                             ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                      rpi.pe_merch_level desc,
                                                                      rpi.price_event_id) promo_excl_rank
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                        and rpi.thread_number                  = I_thread_number
                                        and rpi.chunk_number                   = I_chunk_number
                                        and rpi.price_event_id                 = VALUE(ids)
                                        and rpl.price_event_id                 = VALUE(ids)
                                        and NVL(rpi.txn_man_excluded_item, 0) != 1
                                        and rpi.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpi.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                        and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id                 = rpi.price_event_id
                                        and rpl.itemloc_id                     = rpi.itemloc_id
                                        and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id                        is NULL
                                        and rmrde.dept                         = rpi.dept
                                        and rmrde.class                        = rpi.class
                                        and rmrde.subclass                     = rpi.subclass
                                        and im.item_parent                     = rpi.item
                                        and im.diff_1                          = rpi.diff_id
                                        and ril.dept                           = im.dept
                                        and ril.item                           = im.item
                                        and ril.loc                            = rpl.location
                                        and rownum                             > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.promo_excl_rank    = 1
                                and il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Item Location Level Price Events
                             -- Look at all levels above and rank, then use the data with the highest rank
                             -- Look at P/Z level first and rank that data 0
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    NULL                              from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                            = rpi.item
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at PD/Z level and rank that data 1
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    im.item_parent                    from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and im.item                            = rpi.item
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rpl.zone_id                        is NULL
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at P/L level and rank that data 1 (same as PD/Z - can't have both and be here)
                             select distinct
                                    1 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    promo_excl_rank
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            rpi.diff_id,
                                            im.item_parent,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group,
                                            RANK() OVER (PARTITION BY rpi.item,
                                                                      rpi.diff_id,
                                                                      rpl.zone_node_type,
                                                                      rpl.location
                                                             ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                      rpi.pe_merch_level desc,
                                                                      rpi.price_event_id) promo_excl_rank
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                        and rpi.thread_number                  = I_thread_number
                                        and rpi.chunk_number                   = I_chunk_number
                                        and rpi.price_event_id                 = VALUE(ids)
                                        and rpl.price_event_id                 = VALUE(ids)
                                        and NVL(rpi.txn_man_excluded_item, 0) != 1
                                        and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item                            = rpi.item
                                        and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id                 = rpi.price_event_id
                                        and rpl.itemloc_id                     = rpi.itemloc_id
                                        and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id                        is NULL
                                        and rmrde.dept                         = rpi.dept
                                        and rmrde.class                        = rpi.class
                                        and rmrde.subclass                     = rpi.subclass
                                        and ril.dept                           = im.dept
                                        and ril.item                           = im.item
                                        and ril.loc                            = rpl.location
                                        and rownum                             > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.promo_excl_rank    = 1
                                and il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1
                             union all
                             -- Look at I/Z level and rank that data 2
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    2 rank,
                                    rpi.dept,
                                    rpi.price_event_id,
                                    rpi.item                          from_item,
                                    rpi.diff_id                       from_diff_id,
                                    rz.zone_id                        from_location,
                                    RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE from_zone_node_type,
                                    rpi.item                          to_item,
                                    rpi.diff_id                       to_diff_id,
                                    im.item_parent                    to_item_parent,
                                    rpl.location                      to_location,
                                    rpl.zone_node_type                to_zone_node_type,
                                    rz.zone_id                        to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC    to_cur_hier_level,
                                    RANK() OVER (PARTITION BY rpi.item,
                                                              rpi.diff_id,
                                                              rpl.zone_node_type,
                                                              rpl.location
                                                     ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                              rpi.pe_merch_level desc,
                                                              rpi.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item rpi,
                                    rpm_bulk_cc_pe_location rpl,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril
                              where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and rpi.thread_number                  = I_thread_number
                                and rpi.chunk_number                   = I_chunk_number
                                and rpi.price_event_id                 = VALUE(ids)
                                and rpl.price_event_id                 = VALUE(ids)
                                and NVL(rpi.txn_man_excluded_item, 0) != 1
                                and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                and rpl.price_event_id                 = rpi.price_event_id
                                and rpl.itemloc_id                     = rpi.itemloc_id
                                and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                           RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and rmrde.dept                         = rpi.dept
                                and rmrde.class                        = rpi.class
                                and rmrde.subclass                     = rpi.subclass
                                and rmrde.regular_zone_group           = rz.zone_group_id
                                and rz.zone_id                         = rzl.zone_id
                                and rzl.location                       = rpl.location
                                and rzl.loc_type                       = rpl.zone_node_type
                                and rpl.zone_id                        is NULL
                                and rpi.item                           = im.item
                                and ril.dept                           = im.dept
                                and ril.item                           = im.item
                                and ril.loc                            = rpl.location
                                and rownum                            >= 1
                             union all
                             -- Look at the PD/L level and rank that data 2 - same as I/Z (can't have both IZ and PDL and be here)
                             select distinct
                                    2 rank,
                                    il.dept,
                                    il.price_event_id,
                                    il.item_parent                 from_item,
                                    il.diff_id                     from_diff_id,
                                    il.location                    from_location,
                                    il.zone_node_type              from_zone_node_type,
                                    il.item                        to_item,
                                    il.diff_id                     to_diff_id,
                                    il.item_parent                 to_item_parent,
                                    il.location                    to_location,
                                    il.zone_node_type              to_zone_node_type,
                                    zl.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    promo_excl_rank
                               from (select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                            rpi.price_event_id,
                                            rpi.dept,
                                            im.item_parent,
                                            rpi.diff_id,
                                            rpl.location,
                                            rpl.zone_node_type,
                                            rpi.item,
                                            rmrde.regular_zone_group,
                                            RANK() OVER (PARTITION BY rpi.item,
                                                                      rpi.diff_id,
                                                                      rpl.zone_node_type,
                                                                      rpl.location
                                                             ORDER BY NVL(rpi.txn_man_excluded_item, 0),
                                                                      rpi.pe_merch_level desc,
                                                                      rpi.price_event_id) promo_excl_rank
                                       from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                            rpm_bulk_cc_pe_item rpi,
                                            rpm_bulk_cc_pe_location rpl,
                                            item_master im,
                                            rpm_merch_retail_def_expl rmrde,
                                            rpm_item_loc ril
                                      where rpi.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                        and rpi.thread_number                  = I_thread_number
                                        and rpi.chunk_number                   = I_chunk_number
                                        and rpi.price_event_id                 = VALUE(ids)
                                        and rpl.price_event_id                 = VALUE(ids)
                                        and NVL(rpi.txn_man_excluded_item, 0) != 1
                                        and rpi.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and rpi.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                        and im.item                            = rpi.item
                                        and rpl.bulk_cc_pe_id                  = rpi.bulk_cc_pe_id
                                        and rpl.price_event_id                 = rpi.price_event_id
                                        and rpl.itemloc_id                     = rpi.itemloc_id
                                        and rpl.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                        and rpl.zone_id                        is NULL
                                        and rmrde.dept                         = rpi.dept
                                        and rmrde.class                        = rpi.class
                                        and rmrde.subclass                     = rpi.subclass
                                        and ril.dept                           = im.dept
                                        and ril.item                           = im.item
                                        and ril.loc                            = rpl.location
                                        and rownum                             > 0) il,
                                    (select rz.zone_group_id,
                                            rz.zone_id,
                                            rzl.location
                                       from rpm_zone rz,
                                            rpm_zone_location rzl
                                      where rzl.zone_id = rz.zone_id
                                        and rownum      > 0) zl
                              where il.promo_excl_rank    = 1
                                and il.regular_zone_group = zl.zone_group_id (+)
                                and il.location           = zl.location (+)
                                and rownum               >= 1) t,
                            rpm_cust_segment_promo_fr cspfr,
                            rpm_promo_dtl rpd,
                            rpm_promo_comp rpc
                      where t.promo_excl_rank          = 1
                        and cspfr.dept                 = t.dept
                        and cspfr.item                 = t.from_item
                        and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                        and cspfr.location             = t.from_location
                        and cspfr.zone_node_type       = t.from_zone_node_type
                        and cspfr.customer_type        is NOT NULL
                        and rpd.promo_dtl_id           = t.price_event_id
                        and rpc.promo_comp_id          = rpd.promo_comp_id
                        and rpc.customer_type          = cspfr.customer_type)
       where rank = max_rank) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert(price_event_id,
                cust_segment_promo_id,
                item,
                diff_id,
                item_parent,
                zone_node_type,
                location,
                zone_id,
                action_date,
                customer_type,
                dept,
                promo_retail,
                promo_retail_currency,
                promo_uom,
                complex_promo_ind,
                cspfr_rowid,
                max_hier_level,
                cur_hier_level,
                original_cs_promo_id,
                step_identifier)
         values(source.price_event_id,
                RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                source.item,
                source.diff_id,
                source.item_parent,
                source.zone_node_type,
                source.location,
                source.zone_id,
                source.action_date,
                source.customer_type,
                source.dept,
                source.promo_retail,
                source.promo_retail_currency,
                source.promo_uom,
                source.complex_promo_ind,
                NULL,
                source.max_hier_level,
                source.cur_hier_level,
                source.cust_segment_promo_id,
                I_step_identifier);

   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION

   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;

END GENERATE_CSPFR_TIMELINE;

--------------------------------------------------------------------------------

FUNCTION GEN_CSPFR_TL_FROM_INT(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                               I_bulk_cc_pe_id    IN     NUMBER,
                               I_thread_number    IN     NUMBER,
                               I_chunk_number     IN     NUMBER,
                               I_price_event_type IN     VARCHAR2,
                               I_price_event_ids  IN     OBJ_NUMERIC_ID_TABLE,
                               I_step_identifier  IN     NUMBER)
RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_CHUNK_CC_ACTIONS_SQL.GEN_CSPFR_TL_FROM_INT';

   L_start_time    TIMESTAMP     := SYSTIMESTAMP;
   L_error_message VARCHAR2(255) := NULL;

BEGIN

   LOGGER.LOG_INFORMATION(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                                      ' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                                      ' - I_thread_number: '|| I_thread_number ||
                                      ' - I_chunk_number: ' || I_chunk_number);

   -- if dealing with PC, CL, CR, NR or LM, we don't care about the customer type - we want
   -- to process promotions that are both customer specific and those that are not.

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET) then

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select t.price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    cspfr.action_date,
                    cspfr.customer_type,
                    cspfr.dept,
                    cspfr.promo_retail,
                    cspfr.promo_retail_currency,
                    cspfr.promo_uom,
                    cspfr.complex_promo_ind,
                    NULL,
                    cspfr.max_hier_level,
                    t.to_cur_hier_level cur_hier_level,
                    cspfr.cust_segment_promo_id
               from (select price_event_id,
                            dept,
                            from_item,
                            from_diff_id,
                            from_location,
                            from_zone_node_type,
                            to_item,
                            to_diff_id,
                            to_item_parent,
                            to_location,
                            to_zone_node_type,
                            to_zone_id,
                            to_cur_hier_level,
                            cspfr_rank,
                            customer_type,
                            MAX(cspfr_rank) OVER (PARTITION BY price_event_id,
                                                               customer_type,
                                                               to_item,
                                                               to_diff_id,
                                                               to_location,
                                                               to_zone_node_type) max_cspfr_rank
                       from (-- PD/Z and P/L intersection - Generate PD/L timeline:
                             -- working with PD/Z and P/L timeline exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    NULL                           from_diff_id,
                                    rzl.location                   from_location,
                                    rzl.loc_type                   from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    rzl.location                   to_location,
                                    rzl.loc_type                   to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id      = I_bulk_cc_pe_id
                                and i.thread_number      = I_thread_number
                                and i.chunk_number       = I_chunk_number
                                and i.price_event_id     = VALUE(ids)
                                and l.price_event_id     = VALUE(ids)
                                and i.pe_merch_level     = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.merch_level_type   = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent        is NULL
                                and i.bulk_cc_pe_id      = l.bulk_cc_pe_id
                                and i.itemloc_id         = l.itemloc_id
                                and i.price_event_id     = l.price_event_id
                                and l.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and l.zone_id            is NULL
                                and rzl.zone_id          = l.location
                                and im.item_parent       = i.item
                                and im.diff_1            = i.diff_id
                                and ril.dept             = im.dept
                                and ril.item             = im.item
                                and ril.loc              = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = rzl.location
                                                   and gtt.zone_node_type       = rzl.loc_type)
                                -- look for an existing timeline at the P/L level
                                and cspfr.dept           = i.dept
                                and cspfr.item           = i.item
                                and cspfr.diff_id        is NULL
                                and cspfr.location       = rzl.location
                                and cspfr.zone_node_type = rzl.loc_type
                                and rownum               > 0
                             union all
                             -- working with P/L and PD/Z timeline exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rzl.zone_id                    from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.thread_number          = I_thread_number
                                and i.chunk_number           = I_chunk_number
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.merch_level_type       = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent            is NULL
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NULL
                                and i.dept                   = rmrde.dept
                                and i.class                  = rmrde.class
                                and i.subclass               = rmrde.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = l.location
                                and im.item_parent           = i.item
                                and im.diff_1                = i.diff_id
                                and ril.dept                 = im.dept
                                and ril.item                 = im.item
                                and ril.loc                  = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/Z timeline
                                and cspfr.dept               = i.dept
                                and cspfr.item               = i.item
                                and cspfr.diff_id            is NOT NULL
                                and cspfr.diff_id            = i.diff_id
                                and cspfr.location           = rzl.zone_id
                                and cspfr.zone_node_type     = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rownum                   > 0
                             union all
                             -- P/L and I/Z intersection - Generate I/L timeline:
                             -- working with P/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id          = I_bulk_cc_pe_id
                                and i.thread_number          = I_thread_number
                                and i.chunk_number           = I_chunk_number
                                and i.price_event_id         = VALUE(ids)
                                and l.price_event_id         = VALUE(ids)
                                and i.merch_level_type       = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level         = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent            is NOT NULL
                                and i.bulk_cc_pe_id          = l.bulk_cc_pe_id
                                and i.itemloc_id             = l.itemloc_id
                                and i.price_event_id         = l.price_event_id
                                and l.zone_node_type         IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                 RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                is NULL
                                and i.dept                   = rmrde.dept
                                and i.class                  = rmrde.class
                                and i.subclass               = rmrde.subclass
                                and rmrde.regular_zone_group = rz.zone_group_id
                                and rz.zone_id               = rzl.zone_id
                                and rzl.location             = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                 = i.dept
                                and cspfr.item                 = i.item
                                and NVL(cspfr.diff_id, '-999') = NVL(i.diff_id, '-999')
                                and cspfr.location             = rz.zone_id
                                and cspfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                    = i.dept
                                and il.item                    = i.item
                                and il.loc                     = l.location
                                and rownum                     > 0
                             union all
                             -- working with I/Z and P/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id      = I_bulk_cc_pe_id
                                and i.thread_number      = I_thread_number
                                and i.chunk_number       = I_chunk_number
                                and i.bulk_cc_pe_id      = l.bulk_cc_pe_id
                                and l.price_event_id     = VALUE(ids)
                                and i.price_event_id     = VALUE(ids)
                                and i.itemloc_id         = l.itemloc_id
                                and i.price_event_id     = l.price_event_id
                                and i.merch_level_type   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent        is NULL
                                and l.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id            is NOT NULL
                                and im.item              = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing P/L timeline
                                and cspfr.dept           = im.dept
                                and cspfr.item           = im.item_parent
                                and cspfr.diff_id        is NULL
                                and cspfr.location       = l.location
                                and cspfr.zone_node_type = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept              = im.dept
                                and il.item              = im.item
                                and il.loc               = l.location
                                and rownum               > 0
                             union all
                             -- PD/L and I/Z intersection - Generate I/L timeline:
                             -- working with PD/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id            = I_bulk_cc_pe_id
                                and i.thread_number            = I_thread_number
                                and i.chunk_number             = I_chunk_number
                                and i.price_event_id           = VALUE(ids)
                                and l.price_event_id           = VALUE(ids)
                                and i.merch_level_type         = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level           = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent              is NOT NULL
                                and i.bulk_cc_pe_id            = l.bulk_cc_pe_id
                                and i.itemloc_id               = l.itemloc_id
                                and i.price_event_id           = l.price_event_id
                                and l.zone_node_type           IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                  is NULL
                                and i.dept                     = rmrde.dept
                                and i.class                    = rmrde.class
                                and i.subclass                 = rmrde.subclass
                                and rmrde.regular_zone_group   = rz.zone_group_id
                                and rz.zone_id                 = rzl.zone_id
                                and rzl.location               = l.location
                                -- Make sure that there is not a timeline at the I/L level already
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                 = i.dept
                                and cspfr.item                 = i.item
                                and NVL(cspfr.diff_id, '-999') = NVL(i.diff_id, '-999')
                                and cspfr.location             = rz.zone_id
                                and cspfr.zone_node_type       = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                    = i.dept
                                and il.item                    = i.item
                                and il.loc                     = l.location
                                and rownum                     > 0
                             union all
                             -- working with I/Z and PD/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    i.diff_id                      from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id      = I_bulk_cc_pe_id
                                and i.thread_number      = I_thread_number
                                and i.chunk_number       = I_chunk_number
                                and i.bulk_cc_pe_id      = l.bulk_cc_pe_id
                                and i.price_event_id     = VALUE(ids)
                                and l.price_event_id     = VALUE(ids)
                                and i.itemloc_id         = l.itemloc_id
                                and i.price_event_id     = l.price_event_id
                                and i.merch_level_type   = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level     = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent        is NULL
                                and l.zone_node_type     IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                             RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id            is NOT NULL
                                and im.item              = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/L timeline
                                and cspfr.dept           = im.dept
                                and cspfr.item           = im.item_parent
                                and cspfr.diff_id        is NOT NULL
                                and cspfr.diff_id        = i.diff_id
                                and cspfr.location       = l.location
                                and cspfr.zone_node_type = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept              = im.dept
                                and il.item              = im.item
                                and il.loc               = l.location
                                and rownum               > 0)) t,
                    rpm_cust_segment_promo_fr cspfr
              where t.cspfr_rank               = t.max_cspfr_rank
                and cspfr.dept                 = t.dept
                and cspfr.item                 = t.from_item
                and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and cspfr.location             = t.from_location
                and cspfr.zone_node_type       = t.from_zone_node_type
                and cspfr.customer_type        = t.customer_type) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT matched then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 cspfr_rowid,
                 max_hier_level,
                 cur_hier_level,
                 original_cs_promo_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.diff_id,
                 source.item_parent,
                 source.zone_node_type,
                 source.location,
                 source.zone_id,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 source.promo_retail,
                 source.promo_retail_currency,
                 source.promo_uom,
                 source.complex_promo_ind,
                 NULL,
                 source.max_hier_level,
                 source.cur_hier_level,
                 source.cust_segment_promo_id,
                 I_step_identifier);

   else

      -- If dealing with a promotion (CS or not), we need to make sure that we only pull back
      -- the appropriate data - if CS, need to get the right customer type, if not CS, we need to
      -- make sure that there is no customer type in the "parent" promo record

      merge into rpm_cust_segment_promo_fr_gtt target
      using (select t.price_event_id,
                    to_item item,
                    to_diff_id diff_id,
                    to_item_parent item_parent,
                    to_zone_node_type zone_node_type,
                    to_location location,
                    to_zone_id zone_id,
                    cspfr.action_date,
                    cspfr.customer_type,
                    cspfr.dept,
                    cspfr.promo_retail,
                    cspfr.promo_retail_currency,
                    cspfr.promo_uom,
                    cspfr.complex_promo_ind,
                    cspfr.max_hier_level,
                    t.to_cur_hier_level cur_hier_level,
                    cspfr.cust_segment_promo_id
               from (select price_event_id,
                            dept,
                            from_item,
                            from_diff_id,
                            from_location,
                            from_zone_node_type,
                            to_item,
                            to_diff_id,
                            to_item_parent,
                            to_location,
                            to_zone_node_type,
                            to_zone_id,
                            to_cur_hier_level,
                            cspfr_rank,
                            customer_type,
                            promo_excl_rank,
                            MAX(cspfr_rank) OVER (PARTITION BY price_event_id,
                                                               customer_type,
                                                               to_item,
                                                               to_diff_id,
                                                               to_location,
                                                               to_zone_node_type) max_cspfr_rank
                       from (-- PD/Z and P/L intersection - Generate PD/L timeline:
                             -- working with PD/Z and P/L timeline exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    NULL                           from_diff_id,
                                    rzl.location                   from_location,
                                    rzl.loc_type                   from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    rzl.location                   to_location,
                                    rzl.loc_type                   to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.thread_number                  = I_thread_number
                                and i.chunk_number                   = I_chunk_number
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and l.zone_node_type                 = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and l.zone_id                        is NULL
                                and rzl.zone_id                      = l.location
                                and im.item_parent                   = i.item
                                and im.diff_1                        = i.diff_id
                                and ril.dept                         = im.dept
                                and ril.item                         = im.item
                                and ril.loc                          = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = rzl.location
                                                   and gtt.zone_node_type       = rzl.loc_type)
                                -- look for an existing timeline at the P/L level
                                and cspfr.dept                       = i.dept
                                and cspfr.item                       = i.item
                                and cspfr.diff_id                    is NULL
                                and cspfr.location                   = rzl.location
                                and cspfr.zone_node_type             = rzl.loc_type
                                and rownum                           > 0
                             union all
                             -- working with P/L and PD/Z timeline exists
                            select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rzl.zone_id                    from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    NULL                           to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rzl.zone_id                    to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_DIFF_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc ril,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.thread_number                  = I_thread_number
                                and i.chunk_number                   = I_chunk_number
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.merch_level_type               = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NULL
                                and i.dept                           = rmrde.dept
                                and i.class                          = rmrde.class
                                and i.subclass                       = rmrde.subclass
                                and rmrde.regular_zone_group         = rz.zone_group_id
                                and rz.zone_id                       = rzl.zone_id
                                and rzl.location                     = l.location
                                and im.item_parent                   = i.item
                                and im.diff_1                        = i.diff_id
                                and ril.dept                         = im.dept
                                and ril.item                         = im.item
                                and ril.loc                          = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/Z timeline
                                and cspfr.dept                       = i.dept
                                and cspfr.item                       = i.item
                                and cspfr.diff_id                    is NOT NULL
                                and cspfr.diff_id                    = i.diff_id
                                and cspfr.location                   = rzl.zone_id
                                and cspfr.zone_node_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                and rownum                           > 0
                             union all
                             -- P/L and I/Z intersection - Generate I/L timeline:
                             -- working with P/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.thread_number                  = I_thread_number
                                and i.chunk_number                   = I_chunk_number
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.PARENT_MERCH_TYPE
                                and i.item_parent                    is NOT NULL
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NULL
                                and i.dept                           = rmrde.dept
                                and i.class                          = rmrde.class
                                and i.subclass                       = rmrde.subclass
                                and rmrde.regular_zone_group         = rz.zone_group_id
                                and rz.zone_id                       = rzl.zone_id
                                and rzl.location                     = l.location
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                       = i.dept
                                and cspfr.item                       = i.item
                                and NVL(cspfr.diff_id, '-999')       = NVL(i.diff_id, '-999')
                                and cspfr.location                   = rz.zone_id
                                and cspfr.zone_node_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                          = i.dept
                                and il.item                          = i.item
                                and il.loc                           = l.location
                                and rownum                           > 0
                             union all
                             -- working with I/Z and P/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    0 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    NULL                           from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.thread_number                  = I_thread_number
                                and i.chunk_number                   = I_chunk_number
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and i.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NOT NULL
                                and im.item                          = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing P/L timeline
                                and cspfr.dept                       = im.dept
                                and cspfr.item                       = im.item_parent
                                and cspfr.diff_id                    is NULL
                                and cspfr.location                   = l.location
                                and cspfr.zone_node_type             = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                          = im.dept
                                and il.item                          = im.item
                                and il.loc                           = l.location
                                and rownum                           > 0
                             union all
                             -- PD/L and I/Z intersection - Generate I/L timeline:
                             -- working with PD/L and I/Z exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                     distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    i.dept,
                                    i.item                         from_item,
                                    i.diff_id                      from_diff_id,
                                    rz.zone_id                     from_location,
                                    cspfr.zone_node_type           from_zone_node_type,
                                    i.item                         to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_location,
                                    l.zone_node_type               to_zone_node_type,
                                    rz.zone_id                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    rpm_merch_retail_def_expl rmrde,
                                    rpm_zone rz,
                                    rpm_zone_location rzl,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.thread_number                  = I_thread_number
                                and i.chunk_number                   = I_chunk_number
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.PARENT_DIFF_MERCH_TYPE
                                and i.item_parent                    is NOT NULL
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NULL
                                and i.dept                           = rmrde.dept
                                and i.class                          = rmrde.class
                                and i.subclass                       = rmrde.subclass
                                and rmrde.regular_zone_group         = rz.zone_group_id
                                and rz.zone_id                       = rzl.zone_id
                                and rzl.location                     = l.location
                                -- Make sure that there is not a timeline at the I/L level already
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = i.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing timeline at the I/Z level
                                and cspfr.dept                       = i.dept
                                and cspfr.item                       = i.item
                                and NVL(cspfr.diff_id, '-999')       = NVL(i.diff_id, '-999')
                                and cspfr.location                   = rz.zone_id
                                and cspfr.zone_node_type             = RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                          = i.dept
                                and il.item                          = i.item
                                and il.loc                           = l.location
                                and rownum                           > 0
                             union all
                             -- working with I/Z and PD/L exists
                             select /*+ CARDINALITY(ids 10) USE_NL(IDS) ORDERED */
                                    distinct
                                    1 cspfr_rank,
                                    i.price_event_id,
                                    im.dept,
                                    im.item_parent                 from_item,
                                    i.diff_id                      from_diff_id,
                                    l.location                     from_loc,
                                    l.zone_node_type               from_zone_node_type,
                                    im.item                        to_item,
                                    i.diff_id                      to_diff_id,
                                    i.item_parent                  to_item_parent,
                                    l.location                     to_loc,
                                    l.zone_node_type               to_zone_node_type,
                                    l.location                     to_zone_id,
                                    RPM_CONSTANTS.FR_HIER_ITEM_LOC to_cur_hier_level,
                                    cspfr.customer_type,
                                    RANK() OVER (PARTITION BY i.item,
                                                              i.diff_id,
                                                              l.zone_node_type,
                                                              l.location
                                                     ORDER BY NVL(i.txn_man_excluded_item, 0),
                                                              i.pe_merch_level desc,
                                                              i.price_event_id) promo_excl_rank
                               from table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids,
                                    rpm_bulk_cc_pe_item i,
                                    rpm_bulk_cc_pe_location l,
                                    item_master im,
                                    rpm_item_loc il,
                                    rpm_cust_segment_promo_fr cspfr
                              where i.bulk_cc_pe_id                  = I_bulk_cc_pe_id
                                and i.thread_number                  = I_thread_number
                                and i.chunk_number                   = I_chunk_number
                                and i.bulk_cc_pe_id                  = l.bulk_cc_pe_id
                                and i.price_event_id                 = VALUE(ids)
                                and l.price_event_id                 = VALUE(ids)
                                and NVL(i.txn_man_excluded_item, 0) != 1
                                and i.itemloc_id                     = l.itemloc_id
                                and i.price_event_id                 = l.price_event_id
                                and i.merch_level_type               = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.pe_merch_level                 = RPM_CONSTANTS.ITEM_MERCH_TYPE
                                and i.item_parent                    is NULL
                                and l.zone_node_type                 IN (RPM_CONSTANTS.ZONE_NODE_TYPE_STORE,
                                                                         RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE)
                                and l.zone_id                        is NOT NULL
                                and im.item                          = i.item
                                and NOT EXISTS (select 'x'
                                                  from rpm_cust_segment_promo_fr_gtt gtt
                                                 where gtt.dept                 = im.dept
                                                   and gtt.item                 = i.item
                                                   and NVL(gtt.diff_id, '-999') = NVL(i.diff_id, '-999')
                                                   and gtt.location             = l.location
                                                   and gtt.zone_node_type       = l.zone_node_type)
                                -- Look for an existing PD/L timeline
                                and cspfr.dept                       = im.dept
                                and cspfr.item                       = im.item_parent
                                and cspfr.diff_id                    is NOT NULL
                                and cspfr.diff_id                    = i.diff_id
                                and cspfr.location                   = l.location
                                and cspfr.zone_node_type             = l.zone_node_type
                                -- Make sure that only I/L timelines are generated when the there is an I/L relationship
                                and il.dept                          = im.dept
                                and il.item                          = im.item
                                and il.loc                           = l.location
                                and rownum                           > 0)) t,
                    rpm_cust_segment_promo_fr cspfr,
                    rpm_promo_dtl rpd,
                    rpm_promo_comp rpc
              where t.promo_excl_rank          = 1
                and t.cspfr_rank               = t.max_cspfr_rank
                and cspfr.customer_type        = t.customer_type
                and cspfr.dept                 = t.dept
                and cspfr.item                 = t.from_item
                and NVL(cspfr.diff_id, '-999') = NVL(t.from_diff_id, '-999')
                and cspfr.location             = t.from_location
                and cspfr.zone_node_type       = t.from_zone_node_type
                and rpd.promo_dtl_id           = t.price_event_id
                and rpc.promo_comp_id          = rpd.promo_comp_id
                and rpc.customer_type          = cspfr.customer_type) source
      on (    target.price_event_id         = source.price_event_id
          and target.item                   = source.item
          and NVL(target.diff_id, '-99999') = NVL(source.diff_id, '-99999')
          and target.location               = source.location
          and target.zone_node_type         = source.zone_node_type
          and target.action_date            = source.action_date
          and target.customer_type          = source.customer_type)
      when NOT MATCHED then
         insert (price_event_id,
                 cust_segment_promo_id,
                 item,
                 diff_id,
                 item_parent,
                 zone_node_type,
                 location,
                 zone_id,
                 action_date,
                 customer_type,
                 dept,
                 promo_retail,
                 promo_retail_currency,
                 promo_uom,
                 complex_promo_ind,
                 max_hier_level,
                 cur_hier_level,
                 original_cs_promo_id,
                 step_identifier)
         values (source.price_event_id,
                 RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                 source.item,
                 source.diff_id,
                 source.item_parent,
                 source.zone_node_type,
                 source.location,
                 source.zone_id,
                 source.action_date,
                 source.customer_type,
                 source.dept,
                 source.promo_retail,
                 source.promo_retail_currency,
                 source.promo_uom,
                 source.complex_promo_ind,
                 source.max_hier_level,
                 source.cur_hier_level,
                 source.cust_segment_promo_id,
                 I_step_identifier);
   end if;

   if RPM_CAPTURE_GTT_SQL.PROCESS_GTT_DATA_CAPTURE(L_error_message,
                                                   I_step_identifier,
                                                   0, -- RFR
                                                   0, -- RPILE
                                                   1, -- CSPFR
                                                   0, -- CLR
                                                   0) = 0 then -- RFR_IL_EXPL
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 L_error_message));
      return 0;

   end if;

   LOGGER.LOG_TIME(L_program ||' - I_price_event_type: '|| I_price_event_type ||
                               ' - I_bulk_cc_pe_id: '|| LP_bulk_cc_pe_id ||
                               ' - I_thread_number: '|| I_thread_number ||
                               ' - I_chunk_number: ' || I_chunk_number,
                   L_start_time);

   return 1;

EXCEPTION
   when OTHERS then
      O_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                        CONFLICT_CHECK_ERROR_REC(NULL,
                                                 NULL,
                                                 RPM_CONSTANTS.PLSQL_ERROR,
                                                 SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                    SQLERRM,
                                                                    L_program,
                                                                    TO_CHAR(SQLCODE))));
      return 0;
END GEN_CSPFR_TL_FROM_INT;

----------------------------------------------------------------------------------------------

END RPM_CHUNK_CC_ACTIONS_SQL;
/
