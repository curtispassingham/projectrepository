CREATE OR REPLACE PACKAGE RPM_CHUNK_CC_ACTIONS_SQL AS
--------------------------------------------------------------------------------
--                          GLOBAL VARIABLES                                  --
--------------------------------------------------------------------------------

FUNCTION PROCESS_PRICE_EVENTS(O_cc_error_tbl               OUT CONFLICT_CHECK_ERROR_TBL,
                              I_bulk_cc_pe_id           IN     NUMBER,
                              I_parent_thread_number    IN     NUMBER,
                              I_thread_number           IN     NUMBER,
                              I_chunk_number            IN     NUMBER,
                              I_price_event_ids         IN     OBJ_NUMERIC_ID_TABLE,
                              I_price_event_type        IN     VARCHAR2,
                              I_rib_trans_id            IN     NUMBER,
                              I_persist_ind             IN     VARCHAR2,
                              I_start_state             IN     VARCHAR2,
                              I_end_state               IN     VARCHAR2,
                              I_user_name               IN     VARCHAR2,
                              I_emergency_perm          IN     NUMBER DEFAULT 0,
                              I_secondary_bulk_cc_pe_id IN     NUMBER,
                              I_secondary_ind           IN     NUMBER)
RETURN NUMBER;
--------------------------------------------------------------------------------
END RPM_CHUNK_CC_ACTIONS_SQL;
/

