CREATE OR REPLACE PACKAGE BODY RPM_CC_POST_RESET_CLR AS

--------------------------------------------------------------------------------
--                          PUBLIC PROCEDURES                                 --
--------------------------------------------------------------------------------

FUNCTION VALIDATE(IO_cc_error_tbl   IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_ids IN     OBJ_NUMERIC_ID_TABLE)
RETURN NUMBER IS

   L_program   VARCHAR2(50) := 'RPM_CC_POST_RESET_CLR.VALIDATE';
   L_vdate     DATE         := GET_VDATE;

   L_error_rec         CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl         CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LO_error_tbl        CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select /*+ CARDINALITY (ccet 10) */
             CONFLICT_CHECK_ERROR_REC(rfrg.price_event_id,
                                      rfrg.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'future_retail_price_change_rule24')
        from rpm_future_retail_gtt rfrg,
             table(cast(L_error_tbl as conflict_check_error_tbl)) ccet,
             (select /*+ CARDINALITY(ids 10) */value(ids) ids,
                     rc.effective_date
                from rpm_clearance_gtt rc,
                     table(cast(I_price_event_ids as OBJ_NUMERIC_ID_TABLE)) ids
               where rc.price_event_id  = value(ids)
                 and rc.clearance_id    = value(ids)
                 and rc.effective_date != L_vdate) t
       where rfrg.price_event_id    != ccet.price_event_id
         and rfrg.price_event_id     = t.ids
         and rfrg.clear_start_ind    = RPM_CONSTANTS.END_IND
         and rfrg.clearance_id      is NOT NULL
         and TRUNC(rfrg.action_date) between (L_vdate + 1) and t.effective_date;

BEGIN

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then
      L_error_tbl := IO_cc_error_tbl;
   else
      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);
      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   open C_CHECK;
   fetch C_CHECK BULK COLLECT into LO_error_tbl;
   close C_CHECK;

   if LO_error_tbl is NOT NULL and
      LO_error_tbl.COUNT > 0 then
      if IO_cc_error_tbl is NULL or
         IO_cc_error_tbl.COUNT = 0 then
         IO_cc_error_tbl := LO_error_tbl;
      else
         for I IN 1..LO_error_tbl.COUNT loop
             IO_cc_error_tbl.EXTEND;
             IO_cc_error_tbl(IO_cc_error_tbl.COUNT) := LO_error_tbl(I);
         end loop;
      end if;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE;

--------------------------------------------------------------------------------
FUNCTION VALIDATE(IO_cc_error_tbl     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_parent_rpcs       IN     OBJ_NUM_NUM_DATE_TBL)
RETURN NUMBER IS

   L_program   VARCHAR2(50) := 'RPM_CC_POST_RESET_CLR.VALIDATE';

   L_vdate     DATE          := GET_VDATE;

   L_error_rec  CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl  CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();
   LO_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select /*+ CARDINALITY(t 10) CARDINALITY (ccet 10) */
             CONFLICT_CHECK_ERROR_REC(rfrg.price_event_id,
                                      rfrg.future_retail_id,
                                      RPM_CONSTANTS.CONFLICT_ERROR,
                                      'future_retail_price_change_rule24')
        from rpm_future_retail_gtt rfrg,
             table(cast(L_error_tbl as conflict_check_error_tbl)) ccet,
             table(cast(I_parent_rpcs as OBJ_NUM_NUM_DATE_TBL)) t
       where rfrg.price_event_id    != ccet.price_event_id
         and rfrg.price_event_id     = t.numeric_col1
         and t.numeric_col2          is NOT NULL
         and t.date_col              is NOT NULL
         and rfrg.clear_start_ind    = RPM_CONSTANTS.END_IND
         and rfrg.clearance_id       is NOT NULL
         and TRUNC(rfrg.action_date) between (L_vdate + 1) and t.date_col;

BEGIN

   if IO_cc_error_tbl is NOT NULL and
      IO_cc_error_tbl.COUNT > 0 then
      L_error_tbl := IO_cc_error_tbl;
   else
      L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999,
                                              NULL,
                                              NULL,
                                              NULL);
      L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
   end if;

   open C_CHECK;
   fetch C_CHECK BULK COLLECT into LO_error_tbl;
   close C_CHECK;

   if LO_error_tbl is NOT NULL and
      LO_error_tbl.COUNT > 0 then
      if IO_cc_error_tbl is NULL or
         IO_cc_error_tbl.COUNT = 0 then
         IO_cc_error_tbl := LO_error_tbl;
      else
         for I IN 1..LO_error_tbl.COUNT loop
             IO_cc_error_tbl.EXTEND;
             IO_cc_error_tbl(IO_cc_error_tbl.COUNT) := LO_error_tbl(I);
         end loop;
      end if;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      IO_cc_error_tbl := CONFLICT_CHECK_ERROR_TBL(
                         CONFLICT_CHECK_ERROR_REC(NULL,
                                                  NULL,
                                                  RPM_CONSTANTS.PLSQL_ERROR,
                                                  SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                     SQLERRM,
                                                                     L_program,
                                                                     TO_CHAR(SQLCODE))));
      return 0;

END VALIDATE;

--------------------------------------------------------------------------------

END RPM_CC_POST_RESET_CLR;
/

