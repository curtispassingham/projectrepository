CREATE OR REPLACE PACKAGE BODY RPM_PRICE_STRATEGY_SQL AS

FUNCTION BUILD_ID_FILTER(I_ids           IN    OBJ_NUMERIC_ID_TABLE,
                         I_field_name    IN    STRING)
RETURN VARCHAR2;

PROCEDURE SEARCH(O_return_code           OUT NUMBER,
                 O_error_msg             OUT VARCHAR2,
                 O_ps_tbl                OUT OBJ_PRICE_STRATEGY_TBL,
                 O_ps_count              OUT NUMBER,
                 I_ps_strategy_ids	  IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_strategy_types  IN     OBJ_PRICE_STRATEGY_TYPE_TBL,
                 I_ps_depts           IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_classes         IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_subclasses      IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_zone_groups     IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_zones           IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_price_guides    IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_calendars       IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_next_calendars  IN     OBJ_NUMERIC_ID_TABLE,
                 I_ps_calendar_status IN     NUMBER) IS

   cursor C_WH (L_strategy_id NUMBER) is
      select new OBJ_LISTABLE_REC(
             w.wh,
             w.wh,
             w.wh_name)
        from rpm_strategy_wh rsw,wh w
       where rsw.strategy_id = L_strategy_id
         and w.wh = rsw.wh;

   cursor C_ZLOC (L_zone_id NUMBER) is
      select new OBJ_ZONE_LOC_REC(
             zone_location_id,
             loc.loc_name,
             loc.loc_id,
             loc.loc_type,
             loc.def_currency_code,
             loc.store_type,
             null)
        from rpm_zone_location rzl,
             (select store loc_id,
                     0 loc_type,
                     store_name loc_name,
                     currency_code def_currency_code,
                     store_type store_type
                from store
               union
              select wh loc_id,
                     2 loc_type,
                     wh_name loc_name,
                     currency_code def_currency_code,
                     null store_type
                from wh) loc
       where rzl.zone_id = L_zone_id
         and loc.loc_id = rzl.location;

   cursor C_PG (L_price_guide_id NUMBER) is
      select new OBJ_LISTABLE4_REC(
             d.dept,
             d.dept,
             d.dept_name)
        from rpm_price_guide_dept rpgd,deps d
       where rpgd.price_guide_id = L_price_guide_id
         and d.dept = rpgd.dept;

   L_select           VARCHAR2(4000) := NULL;
   L_from             VARCHAR2(4000) := NULL;
   L_where            VARCHAR2(8000) := NULL;
   L_query            VARCHAR2(16000) := NULL;
   L_iter             NUMBER;
   L_zone_locations   OBJ_ZONE_LOC_TBL;
   L_pg_depts         OBJ_LISTABLE4_TBL;
   L_ps_rec           OBJ_PRICE_STRATEGY_REC;
   L_warehouses       OBJ_LISTABLE_TBL;

   TYPE priceStrategyCursorType IS REF CURSOR;
   il_lc        priceStrategyCursorType;

BEGIN
  L_select := 'SELECT NEW OBJ_PRICE_STRATEGY_REC('    ||
      'stra.strategy_id,'           || --  strategy id
      'stra.strategy_type,'            || --  strategy type
      'stra.state,'              || --  state
      'dcs.dept,'             || --  dept
      'dcs.dept_name,'           || --  dept name
      'dcs.class,'               || --  class
      'dcs.class_name,'          || --  class name
      'dcs.subclass,'               || --  subclass
      'dcs.sub_name,'               || --  subclass name
      'rzg.zone_group_id,'          || --  zone group id
      'rzg.zone_group_display_id,'        || --  zone group display id
      'rzg.name,'             || --  zone group name
      'rz.zone_id,'              || --  zone id
      'rz.zone_display_id,'            || --  zone display id
      'rz.name,'              || --  zone name
      'rz.currency_code,'           || --  zone currency
      'rz.base_ind,'             || --  zone base indicator
      'stra.suspend,'               || --  strategy calendar suspend flag
      'stra.current_calendar_id,'         || --  strategy current calendar id
      'cc.name,'              || --  current calendar name
      'cc.description,'          || --  current calendar description
      'cc.end_date,'             || --  current calendar end date
      'stra.next_calendar_id,'         || --  strategy new calendar id
      'nc.name,'              || --  new calendar name
      'nc.description,'          || --  new calendar description
      'nc.end_date,'             || --  new calendar end date
      'stra.price_guide_id,'           || --  strategy price guide id
      'pg.name,'              || --  price guide name
      'pg.description,'          || --  price guide description
      'pg.currency_code,'           || --  price guide currency code
      'pg.corp_ind,'             || --  price guide corp indicator
      'NULL,'                 || --  price guide depts
      'NULL,'                 || --  strategy warehouses
      'NULL'                  || --  zone locations
      ')';

  L_from := ' FROM '                ||
      '(select rs.*,' || STRATEGY_TYPE_CLEARANCE || ' strategy_type '   ||
      ' from rpm_strategy rs,rpm_strategy_clearance rsc '   ||
      ' where rs.strategy_id = rsc.strategy_id '   ||
      ' union all '              ||
      ' select rs.*,' || STRATEGY_TYPE_CLEARANCE_MD_DF || ' strategy_type '   ||
      ' from rpm_strategy rs,rpm_strategy_cl_dflt rsc '  ||
      ' where rs.strategy_id = rsc.strategy_id '   ||
      ' union all '              ||
      ' select rs.*,' || STRATEGY_TYPE_COMPETITIVE || ' strategy_type ' ||
      ' from rpm_strategy rs,rpm_strategy_competitive rsc ' ||
      ' where rs.strategy_id = rsc.strategy_id '   ||
      ' union all '              ||
      ' select rs.*,' || STRATEGY_TYPE_MARGIN || ' strategy_type '   ||
      ' from rpm_strategy rs,rpm_strategy_margin rsm '   ||
      ' where rs.strategy_id = rsm.strategy_id '   ||
      ' union all '              ||
      ' select rs.*,' || STRATEGY_TYPE_MAINTAIN_MARGIN || ' strategy_type '   ||
      ' from rpm_strategy rs,rpm_strategy_maint_margin rsm '   ||
      ' where rs.strategy_id = rsm.strategy_id) stra,'   ||
      '(select rs.strategy_id id,d.dept,d.dept_name,null class,null class_name,null subclass,null sub_name ' ||
      ' from rpm_strategy rs,deps d '        ||
      ' where rs.merch_type = 3 and rs.dept = d.dept '   ||
      ' union all '              ||
      ' select rs.strategy_id id,d.dept,d.dept_name,cl.class,cl.class_name,null subclass,null sub_name ' ||
      ' from rpm_strategy rs,deps d,class cl '  ||
      ' where rs.merch_type = 2 and rs.dept = d.dept and rs.dept = cl.dept and rs.class = cl.class ' ||
      ' union all '              ||
      ' select rs.strategy_id id,d.dept,d.dept_name,cl.class,cl.class_name,scl.subclass,scl.sub_name ' ||
      ' from rpm_strategy rs,deps d,class cl,subclass scl ' ||
      ' where rs.merch_type = 1 and rs.dept = d.dept and rs.dept = cl.dept and rs.class = cl.class and scl.dept = rs.dept and scl.class = rs.class and scl.subclass = rs.subclass) dcs,' ||
      'rpm_calendar cc,'            ||
      'rpm_calendar nc,'            ||
      'rpm_price_guide pg,'            ||
      'rpm_zone rz,'             ||
      'rpm_zone_group rzg';

  L_where := ' WHERE '                 ||
      'stra.COPIED_STRATEGY_ID is null '     ||
      'AND stra.strategy_id = dcs.id '    ||
      'AND stra.current_calendar_id = cc.calendar_id(+) '   ||
      'AND stra.next_calendar_id = nc.calendar_id(+) '   ||
      'AND stra.price_guide_id = pg.price_guide_id(+) '  ||
  		'AND rz.zone_id (+) = stra.zone_hier_id '		||
  		'AND rzg.zone_group_id (+) = rz.zone_group_id ';

  --
  --	Strategy IDs
  --
  IF I_ps_strategy_ids.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_strategy_ids, 'stra.strategy_id');
  END IF;
  --
  --  Strategy Types
  --
  IF I_ps_strategy_types.count > 0 THEN
    L_where := L_where || ' AND ( ';
    FOR L_iter in 1..I_ps_strategy_types.count LOOP
      IF L_iter > 1 THEN
        L_where := L_where || ' OR ';
      END IF;
      L_where := L_where || ' stra.strategy_type = ''' ||  I_ps_strategy_types(L_iter).strategy_type || ''' ';
    END LOOP;
    L_where := L_where || ' ) ';
  END IF;


  --
  --  Depts/Classes/Subclasses
  --
  IF I_ps_subclasses.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_subclasses, 'stra.subclass') ||
           BUILD_ID_FILTER(I_ps_classes, 'stra.class') ||
           BUILD_ID_FILTER(I_ps_depts, 'stra.dept');
  ELSIF I_ps_classes.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_classes, 'stra.class') ||
           BUILD_ID_FILTER(I_ps_depts, 'stra.dept');
  ELSIF I_ps_depts.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_depts, 'stra.dept');
  END IF;


  --
  --  Zone criteria is specified
  --
  IF I_ps_zones.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_zones, 'rz.zone_id');
  --
  --  Zone Group criteria is specified
  --
  ELSIF I_ps_zone_groups.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_zone_groups, 'rzg.zone_group_id');
  END IF;


  --
  --  Price Guides
  --
  IF I_ps_price_guides.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_price_guides, 'stra.price_guide_id');
  END IF;


  --
  --  Current Calendars
  --
  IF I_ps_calendars.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_calendars, 'stra.current_calendar_id');
  END IF;


  --
  --  Next Calendars
  --
  IF I_ps_next_calendars.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ps_next_calendars, 'stra.next_calendar_id');
  END IF;


  --
  --  Calendar Status
  --
  IF I_ps_calendar_status = CALENDAR_STATUS_EXPIRED THEN
    L_from := L_from || ',period per';
    L_where := L_where || ' AND cc.end_date < per.vdate ';
  ELSIF I_ps_calendar_status = CALENDAR_STATUS_ASSIGNED THEN
    L_from := L_from || ',period per';
    L_where := L_where || ' AND cc.end_date >= per.vdate AND stra.suspend = 0 ';
  ELSIF I_ps_calendar_status = CALENDAR_STATUS_SUSPENDED THEN
    L_where := L_where || ' AND stra.suspend = 1 ';
  ELSIF I_ps_calendar_status = CALENDAR_STATUS_NONE THEN
    L_where := L_where || ' AND stra.current_calendar_id is NULL ';
  END IF;


  L_query := L_select || ' ' || L_from || ' ' || L_where;

  OPEN il_lc FOR L_query;
  FETCH il_lc BULK COLLECT INTO O_ps_tbl;
  CLOSE il_lc;

  FOR L_iter in 1..O_ps_tbl.count LOOP

    L_ps_rec := O_ps_tbl(L_iter);
    L_warehouses := OBJ_LISTABLE_TBL();

    OPEN C_WH (L_ps_rec.strategy_id);
    FETCH C_WH BULK COLLECT INTO L_warehouses;
    CLOSE C_WH;

    O_ps_tbl(L_iter).WAREHOUSES := L_warehouses;
    L_zone_locations := OBJ_ZONE_LOC_TBL();

    OPEN C_ZLOC (L_ps_rec.zone_id);
    FETCH C_ZLOC BULK COLLECT INTO L_zone_locations;
    CLOSE C_ZLOC;

    O_ps_tbl(L_iter).ZONE_LOCATIONS := L_zone_locations;
    L_pg_depts := OBJ_LISTABLE4_TBL();

    OPEN C_PG (L_ps_rec.price_guide_id);
    FETCH C_PG BULK COLLECT INTO L_pg_depts;
    CLOSE C_PG;

    O_ps_tbl(L_iter).PRICE_GUIDE_DEPTS := L_pg_depts;

  END LOOP;

  O_ps_count := O_ps_tbl.COUNT;
  O_return_code := 1;

EXCEPTION

  WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_STRATEGY_SQL.SEARCH',
                                        to_char(SQLCODE));
      O_return_code := 0;


END SEARCH;

-----------------------------------------------------------------------------------------

FUNCTION BUILD_ID_FILTER(I_ids           IN  OBJ_NUMERIC_ID_TABLE,
                         I_field_name    IN  STRING)
RETURN VARCHAR2 IS

  L_filter         VARCHAR2(8000);
  L_i              NUMBER;

BEGIN
  L_filter := ' AND ( ';

  FOR L_i in 1..I_ids.count LOOP
    IF L_i > 1 THEN
      L_filter := L_filter || ' OR ';
    END IF;
    L_filter := L_filter || ' ' || I_field_name || ' = ' ||  I_ids(L_i) || ' ';
  END LOOP;
  L_filter := L_filter || ' ) ';

  return L_filter;

EXCEPTION
  WHEN others THEN

  return '';

END BUILD_ID_FILTER;

-----------------------------------------------------------------------------------------
PROCEDURE CHECK_OVERLAP(O_return_code          OUT NUMBER,
                        O_error_msg            OUT VARCHAR2,
                        O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                        I_dept              IN     DEPS.DEPT%TYPE,
                        I_class             IN     CLASS.CLASS%TYPE,
                        I_subclass          IN     SUBCLASS.SUBCLASS%TYPE,
                        I_zone_id           IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                        I_calendar_id       IN     RPM_CALENDAR.CALENDAR_ID%TYPE,
                        I_ps_strategy_type  IN     NUMBER,
                        I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE) IS

   L_calendar RPM_CALENDAR.NAME%TYPE;

   cursor C_GET_STRATEGIES is
      select distinct
             rs.strategy_id,
             rs.dept,
             rs.strategy_class class,
             rs.strategy_subclass subclass,
             rs.calendar_id,
             rs.copied_strategy_id,
             rz.zone_display_id,
             rzg.zone_group_display_id,
             rs.ps_type
        from (select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                          st.next_calendar_id
                     else
                          st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_CLEARANCE ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_clearance rsc
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsc.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))
               UNION ALL
              select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                        st.next_calendar_id
                     else
                        st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_COMPETITIVE ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_competitive rsc
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsc.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))
               UNION ALL
              select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                        st.next_calendar_id
                     else
                        st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_MARGIN ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_margin rsm
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsm.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))) rs,
                 rpm_zone rz,
                 rpm_zone_group rzg
       where rs.dept = I_dept
         and rs.class = NVL(I_class, rs.class)
         and rs.subclass = NVL(I_subclass, rs.subclass)
         and rs.calendar_id is NOT NULL
         and rs.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id
         and rs.strategy_id != I_strategy_id
         and (exists (select location
                        from rpm_zone_location
                       where zone_id = rs.zone_hier_id
                      INTERSECT
                      select location
                        from rpm_zone_location
                       where zone_id = I_zone_id
                     )
              or (not exists (select 1
                               from rpm_zone_location rzl
                              where rzl.zone_id = I_zone_id
                            )
                  and rs.zone_hier_id = I_zone_id)
              );

   cursor C_GET_MM_STRATEGIES is
      select distinct
             rs.strategy_id,
             rs.dept,
             rs.strategy_class class,
             rs.strategy_subclass subclass,
             rs.calendar_id,
             rs.copied_strategy_id,
             rz.zone_display_id,
             rzg.zone_group_display_id,
             rs.ps_type
        from (select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                        st.next_calendar_id
                     else
                        st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_MAINTAIN_MARGIN ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_maint_margin rsm
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsm.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))) rs,
                 rpm_zone rz,
                 rpm_zone_group rzg
       where rs.dept = I_dept
         and rs.class = NVL(I_class, rs.class)
         and rs.subclass = NVL(I_subclass, rs.subclass)
         and rs.calendar_id is NOT NULL
         and rs.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id
         and rs.strategy_id != I_strategy_id
         and (exists (select location
                        from rpm_zone_location
                       where zone_id = rs.zone_hier_id
                      INTERSECT
                      select location
                        from rpm_zone_location
                       where zone_id = I_zone_id
                     )
              or (not exists (select 1
                               from rpm_zone_location rzl
                              where rzl.zone_id = I_zone_id
                            )
                  and rs.zone_hier_id = I_zone_id)
              );

   cursor C_GET_CD_STRATEGIES is
      select distinct
             st.strategy_id,
             st.dept,
             st.class,
             st.subclass,
             NULL calendar_id,
             NULL copied_strategy_id,
             NULL zone_display_id,
             NULL zone_group_display_id,
             STRATEGY_TYPE_CLEARANCE_MD_DF ps_type
        from rpm_strategy st,			 
             rpm_strategy_cl_dflt rsc
       where rsc.strategy_id = st.strategy_id
         and st.strategy_id != I_strategy_id
         and st.dept = I_dept
         and ((st.class is NULL and I_class is NULL)
           or (st.class is NOT NULL and I_class is NOT NULL and st.class = I_class))
         and ((st.subclass is NULL and I_subclass is NULL)
           or (st.subclass is NOT NULL and I_subclass is NOT NULL and st.class = I_subclass));         		   		 		 		 	   			 			 

   cursor C_CHECK_CAL_OVRLAP(p_calendar_id NUMBER) is
      select that.name
        from (select rcp.calendar_id, t1.name, rcp.start_date, rcp.end_date
                from (select calendar_id,
                             name,
                             start_date,
                             end_date,
                             sys_connect_by_path(to_char(end_date,'YYYYMMDD'),'*') enddate
                        from rpm_calendar
                        start with calendar_id = I_calendar_id
                        connect by prior expiration_calendar_id = calendar_id) t1,
                     rpm_calendar_period rcp
               where rcp.calendar_id = t1.calendar_id
                 and rcp.start_date >= decode(instr(t1.enddate,'*',-1,2),0,t1.start_date,
                                       to_date(substr(t1.enddate,instr(t1.enddate,'*',-1,2)+1,8),'YYYYMMDD')+1)) this,
             (select rcp.calendar_id, t1.name, rcp.start_date, rcp.end_date
                from (select calendar_id,
                             name,
                             start_date,
                             end_date,
                             sys_connect_by_path(to_char(end_date,'YYYYMMDD'),'*') enddate
                        from rpm_calendar
                        start with calendar_id = p_calendar_id
                        connect by prior expiration_calendar_id = calendar_id) t1,
                        rpm_calendar_period rcp
               where rcp.calendar_id = t1.calendar_id
                 and rcp.start_date >= decode(instr(t1.enddate,'*',-1,2),0,t1.start_date,
                                       to_date(substr(t1.enddate,instr(t1.enddate,'*',-1,2)+1,8),'YYYYMMDD')+1)) that
       where (this.start_date between that.start_date and that.end_date
              or this.end_date   between that.start_date and that.end_date)
         and this.end_date >= get_vdate
         and that.end_date >= get_vdate
       order by that.start_date;

BEGIN

   O_strategy_tbl := NEW OBJ_PRICE_STRATEGY_TBL();

   if I_ps_strategy_type NOT IN(STRATEGY_TYPE_MAINTAIN_MARGIN, STRATEGY_TYPE_CLEARANCE_MD_DF) then
      for rec in C_GET_STRATEGIES loop
         open C_CHECK_CAL_OVRLAP(rec.calendar_id);
         fetch C_CHECK_CAL_OVRLAP into L_calendar;
         if C_CHECK_CAL_OVRLAP%FOUND then
            O_strategy_tbl.extend();
            O_strategy_tbl(O_strategy_tbl.count) :=
               NEW OBJ_PRICE_STRATEGY_REC (
                   rec.strategy_id,                                  -- STRATEGY_ID
	               rec.ps_type,                                      -- TYPE_CODE			VARCHAR2(1),
                   rec.copied_strategy_id,                           --	STATE_CODE			VARCHAR2(50),
                   rec.dept,                                         --	DEPT_ID				NUMBER(4),
                   NULL,                                             --	DEPT_NAME			VARCHAR2(80),
                   rec.class,                                        -- CLASS_ID			NUMBER(4),
                   NULL,                                             --	CLASS_NAME			VARCHAR2(80),
                   rec.subclass,                                     --	SUBCLASS_ID			NUMBER(4),
                   NULL,                                             -- SUBCLASS_NAME			VARCHAR2(80),
                   NULL,                                             -- ZONE_GROUP_ID			NUMBER(4),
                   rec.zone_group_display_id,                        -- ZONE_GROUP_DISP_ID		NUMBER(4),
                   NULL,                                             -- ZONE_GROUP_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_ID				NUMBER(10),
                   rec.zone_display_id,                              -- ZONE_DISP_ID			NUMBER(10),
                   NULL,                                             -- ZONE_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_CURRENCY_CODE		VARCHAR2(3),
                   NULL,                                             -- ZONE_BASE_IND			NUMBER(6),
                   NULL,                                             -- SUSPEND				NUMBER(1),
                   NULL,                                             -- CURRENT_CAL_ID			NUMBER(10),
                   L_calendar,                                       -- CURRENT_CAL_NAME		VARCHAR2(100),
                   NULL,                                             -- CURRENT_CAL_DESC		VARCHAR(1000),
                   NULL,                                             -- CURRENT_CAL_END_DATE		DATE,
                   NULL,                                             -- NEW_CAL_ID			NUMBER(10),
                   NULL,                                             -- NEW_CAL_NAME			VARCHAR2(100),
                   NULL,                                             -- NEW_CAL_DESC			VARCHAR(1000),
                   NULL,                                             -- NEW_CAL_END_DATE		DATE,
                   NULL,                                             -- PRICE_GUIDE_ID			NUMBER(20),
                   NULL,                                             -- PRICE_GUIDE_NAME		VARCHAR(100),
                   NULL,                                             -- PRICE_GUIDE_DESCRIPTION		VARCHAR2(1000),
                   NULL,                                             -- PRICE_GUIDE_CURRENCY_CODE	VARCHAR2(3),
                   NULL,                                             -- PRICE_GUIDE_CORP		VARCHAR(1),
                   NULL,                                             -- PRICE_GUIDE_DEPTS		OBJ_LISTABLE4_TBL,
                   NULL,                                             -- WAREHOUSES			OBJ_LISTABLE_TBL,
                   NULL);                                            -- ZONE_LOCATIONS			OBJ_ZONE_LOC_TBL
         end if;
         close C_CHECK_CAL_OVRLAP;
      end loop;
   elsif I_ps_strategy_type = STRATEGY_TYPE_CLEARANCE_MD_DF then
      for rec in C_GET_CD_STRATEGIES loop
         O_strategy_tbl.extend();
         O_strategy_tbl(O_strategy_tbl.count) :=
            NEW OBJ_PRICE_STRATEGY_REC (
                rec.strategy_id,                                  -- STRATEGY_ID
                rec.ps_type,                                      -- TYPE_CODE   VARCHAR2(1),
                rec.copied_strategy_id,                           -- STATE_CODE   VARCHAR2(50),
                rec.dept,                                         -- DEPT_ID    NUMBER(4),
                NULL,                                             -- DEPT_NAME   VARCHAR2(80),
                rec.class,                                        -- CLASS_ID   NUMBER(4),
                NULL,                                             -- CLASS_NAME   VARCHAR2(80),
                rec.subclass,                                     -- SUBCLASS_ID   NUMBER(4),
                NULL,                                             -- SUBCLASS_NAME   VARCHAR2(80),
                NULL,                                             -- ZONE_GROUP_ID   NUMBER(4),
                rec.zone_group_display_id,                        -- ZONE_GROUP_DISP_ID  NUMBER(4),
                NULL,                                             -- ZONE_GROUP_NAME   VARCHAR2(120),
                NULL,                                             -- ZONE_ID    NUMBER(10),
                rec.zone_display_id,                              -- ZONE_DISP_ID   NUMBER(10),
                NULL,                                             -- ZONE_NAME   VARCHAR2(120),
                NULL,                                             -- ZONE_CURRENCY_CODE  VARCHAR2(3),
                NULL,                                             -- ZONE_BASE_IND   NUMBER(6),
                NULL,                                             -- SUSPEND    NUMBER(1),
                NULL,                                             -- CURRENT_CAL_ID   NUMBER(10),
                L_calendar,                                       -- CURRENT_CAL_NAME  VARCHAR2(100),
                NULL,                                             -- CURRENT_CAL_DESC  VARCHAR(1000),
                NULL,                                             -- CURRENT_CAL_END_DATE  DATE,
                NULL,                                             -- NEW_CAL_ID   NUMBER(10),
                NULL,                                             -- NEW_CAL_NAME   VARCHAR2(100),
                NULL,                                             -- NEW_CAL_DESC   VARCHAR(1000),
                NULL,                                             -- NEW_CAL_END_DATE  DATE,
                NULL,                                             -- PRICE_GUIDE_ID   NUMBER(20),
                NULL,                                             -- PRICE_GUIDE_NAME  VARCHAR(100),
                NULL,                                             -- PRICE_GUIDE_DESCRIPTION  VARCHAR2(1000),
                NULL,                                             -- PRICE_GUIDE_CURRENCY_CODE VARCHAR2(3),
                NULL,                                             -- PRICE_GUIDE_CORP  VARCHAR(1),
                NULL,                                             -- PRICE_GUIDE_DEPTS  OBJ_LISTABLE4_TBL,
                NULL,                                             -- WAREHOUSES   OBJ_LISTABLE_TBL,
                NULL);                                            -- ZONE_LOCATIONS   OBJ_ZONE_LOC_TBL
      end loop;
   else
      for rec in C_GET_MM_STRATEGIES loop
         open C_CHECK_CAL_OVRLAP(rec.calendar_id);
         fetch C_CHECK_CAL_OVRLAP into L_calendar;
         if C_CHECK_CAL_OVRLAP%FOUND then
            O_strategy_tbl.extend();
            O_strategy_tbl(O_strategy_tbl.count) :=
               NEW OBJ_PRICE_STRATEGY_REC (
                   rec.strategy_id,                                  -- STRATEGY_ID
	               rec.ps_type,                                      -- TYPE_CODE			VARCHAR2(1),
                   rec.copied_strategy_id,                           --	STATE_CODE			VARCHAR2(50),
                   rec.dept,                                         --	DEPT_ID				NUMBER(4),
                   NULL,                                             --	DEPT_NAME			VARCHAR2(80),
                   rec.class,                                        -- CLASS_ID			NUMBER(4),
                   NULL,                                             --	CLASS_NAME			VARCHAR2(80),
                   rec.subclass,                                     --	SUBCLASS_ID			NUMBER(4),
                   NULL,                                             -- SUBCLASS_NAME			VARCHAR2(80),
                   NULL,                                             -- ZONE_GROUP_ID			NUMBER(4),
                   rec.zone_group_display_id,                        -- ZONE_GROUP_DISP_ID		NUMBER(4),
                   NULL,                                             -- ZONE_GROUP_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_ID				NUMBER(10),
                   rec.zone_display_id,                              -- ZONE_DISP_ID			NUMBER(10),
                   NULL,                                             -- ZONE_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_CURRENCY_CODE		VARCHAR2(3),
                   NULL,                                             -- ZONE_BASE_IND			NUMBER(6),
                   NULL,                                             -- SUSPEND				NUMBER(1),
                   NULL,                                             -- CURRENT_CAL_ID			NUMBER(10),
                   L_calendar,                                       -- CURRENT_CAL_NAME		VARCHAR2(100),
                   NULL,                                             -- CURRENT_CAL_DESC		VARCHAR(1000),
                   NULL,                                             -- CURRENT_CAL_END_DATE		DATE,
                   NULL,                                             -- NEW_CAL_ID			NUMBER(10),
                   NULL,                                             -- NEW_CAL_NAME			VARCHAR2(100),
                   NULL,                                             -- NEW_CAL_DESC			VARCHAR(1000),
                   NULL,                                             -- NEW_CAL_END_DATE		DATE,
                   NULL,                                             -- PRICE_GUIDE_ID			NUMBER(20),
                   NULL,                                             -- PRICE_GUIDE_NAME		VARCHAR(100),
                   NULL,                                             -- PRICE_GUIDE_DESCRIPTION		VARCHAR2(1000),
                   NULL,                                             -- PRICE_GUIDE_CURRENCY_CODE	VARCHAR2(3),
                   NULL,                                             -- PRICE_GUIDE_CORP		VARCHAR(1),
                   NULL,                                             -- PRICE_GUIDE_DEPTS		OBJ_LISTABLE4_TBL,
                   NULL,                                             -- WAREHOUSES			OBJ_LISTABLE_TBL,
                   NULL);                                            -- ZONE_LOCATIONS			OBJ_ZONE_LOC_TBL
         end if;
         close C_CHECK_CAL_OVRLAP;
      end loop;
   end if;
   O_return_code := 1;
EXCEPTION

  WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_STRATEGY_SQL.CHECK_OVERLAP',
                                        to_char(SQLCODE));
      O_return_code := 0;
END CHECK_OVERLAP;
------------------------------------------------------------------------------------
PROCEDURE CHECK_PS_OVERLAP_LOC_MOVE(O_return_code          OUT NUMBER,
                                    O_error_msg            OUT VARCHAR2,
                                    O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                                    I_zone_id           IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                                    I_sched_mov_date    IN     DATE
                                    ) is


   cursor C_GET_ZONE_STRATEGIES is
      select st.strategy_id,
             st.dept,
			 st.class strategy_class,
             sc.class,
	         st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
             case
                when st.copied_strategy_id IS NULL then
                     0
                else
                     1
                end copied_strategy_id,
             rz.zone_display_id,           
             rzg.zone_group_display_id,   
             STRATEGY_TYPE_CLEARANCE ps_type
        from rpm_strategy st,
             subclass sc,
             rpm_strategy_clearance rsc,
             rpm_zone rz,
             rpm_zone_group rzg
       where st.dept = sc.dept
         and nvl(st.class, sc.class) = sc.class
         and nvl(st.subclass, sc.subclass) = sc.subclass
         and st.strategy_id = rsc.strategy_id
         and zone_hier_id = I_zone_id
         and st.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id
      UNION ALL
      select st.strategy_id,
             st.dept,
	  	     st.class strategy_class,
             sc.class,
			 st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
             case
                when st.copied_strategy_id IS NULL then
                     0
                else
                     1
                end copied_strategy_id,
             rz.zone_display_id,           
             rzg.zone_group_display_id,                
             STRATEGY_TYPE_COMPETITIVE ps_type
        from rpm_strategy st,
             subclass sc,
             rpm_strategy_competitive rsc,
             rpm_zone rz,
             rpm_zone_group rzg             
       where st.dept = sc.dept
         and nvl(st.class, sc.class) = sc.class
         and nvl(st.subclass, sc.subclass) = sc.subclass
         and st.strategy_id = rsc.strategy_id
         and zone_hier_id = I_zone_id
         and st.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id         
      UNION ALL
      select st.strategy_id,
             st.dept,
	         st.class strategy_class,
             sc.class,
			 st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
             case
                when st.copied_strategy_id IS NULL then
                     0
                else
                     1
                end copied_strategy_id,
             rz.zone_display_id,           
             rzg.zone_group_display_id,                
             STRATEGY_TYPE_MARGIN ps_type
        from rpm_strategy st,
             subclass sc,
             rpm_strategy_margin rsm,
             rpm_zone rz,
             rpm_zone_group rzg             
       where st.dept = sc.dept
         and nvl(st.class, sc.class) = sc.class
         and nvl(st.subclass, sc.subclass) = sc.subclass
         and st.strategy_id = rsm.strategy_id
         and zone_hier_id = I_zone_id
         and st.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id         
      UNION ALL
      select st.strategy_id,
             st.dept,
	         st.class strategy_class,
             sc.class,
			 st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
            case
               when st.copied_strategy_id IS NULL then
                    0
               else
                   1
               end copied_strategy_id,
            rz.zone_display_id,           
            rzg.zone_group_display_id,               
            STRATEGY_TYPE_MAINTAIN_MARGIN ps_type
       from rpm_strategy st,
            subclass sc,
            rpm_strategy_maint_margin rsm,
            rpm_zone rz,
            rpm_zone_group rzg            
      where st.dept = sc.dept
        and nvl(st.class, sc.class) = sc.class
        and nvl(st.subclass, sc.subclass) = sc.subclass
        and st.strategy_id = rsm.strategy_id
        and zone_hier_id = I_zone_id
        and st.zone_hier_id = rz.zone_id
        and rz.zone_group_id = rzg.zone_group_id;
         
   cursor C_CHECK_CAL_OVRLAP(p_calendar_id NUMBER) is
      select this.name
        from (select rcp.calendar_id, t1.name, rcp.start_date, rcp.end_date
                from (select calendar_id,
                             name,
                             start_date,
                             end_date,
                             sys_connect_by_path(to_char(end_date,'YYYYMMDD'),'*') enddate
                        from rpm_calendar
                        start with calendar_id = p_calendar_id
                        connect by prior expiration_calendar_id = calendar_id) t1,
                     rpm_calendar_period rcp
               where rcp.calendar_id = t1.calendar_id
                 and rcp.start_date >= decode(instr(t1.enddate,'*',-1,2),0,t1.start_date,
                                       to_date(substr(t1.enddate,instr(t1.enddate,'*',-1,2)+1,8),'YYYYMMDD')+1)) this
       where this.start_date <= I_sched_mov_date
         and this.end_date >= I_sched_mov_date
       order by this.start_date;

   cursor C_CHECK_PS_REVIEW_OVERLAP is
      select loc_move_ps_review_overlap
        from rpm_system_options;       
          
   p_strategy_tbl OBJ_PRICE_STRATEGY_TBL;
   
   L_loc_move_ps_review_overlap RPM_SYSTEM_OPTIONS.LOC_MOVE_PS_REVIEW_OVERLAP%TYPE;
   
   L_calendar RPM_CALENDAR.NAME%TYPE;
BEGIN
   O_strategy_tbl := new OBJ_PRICE_STRATEGY_TBL();
   
   select loc_move_ps_review_overlap
     into L_loc_move_ps_review_overlap
     from rpm_system_options;

   if L_loc_move_ps_review_overlap = 0 then
      for rec in C_GET_ZONE_STRATEGIES loop
         open C_CHECK_CAL_OVRLAP(rec.calendar_id);
         fetch C_CHECK_CAL_OVRLAP into L_calendar;
         if C_CHECK_CAL_OVRLAP%FOUND then
            O_strategy_tbl.extend();
            O_strategy_tbl(O_strategy_tbl.count) :=
               NEW OBJ_PRICE_STRATEGY_REC (
                   rec.strategy_id,                                  -- STRATEGY_ID
	               rec.ps_type,                                      -- TYPE_CODE			VARCHAR2(1),
                   rec.copied_strategy_id,                           --	STATE_CODE			VARCHAR2(50),
                   rec.dept,                                         --	DEPT_ID				NUMBER(4),
                   NULL,                                             --	DEPT_NAME			VARCHAR2(80),
                   rec.class,                                        -- CLASS_ID			NUMBER(4),
                   NULL,                                             --	CLASS_NAME			VARCHAR2(80),
                   rec.subclass,                                     --	SUBCLASS_ID			NUMBER(4),
                   NULL,                                             -- SUBCLASS_NAME			VARCHAR2(80),
                   NULL,                                             -- ZONE_GROUP_ID			NUMBER(4),
                   rec.zone_group_display_id,                        -- ZONE_GROUP_DISP_ID		NUMBER(4),
                   NULL,                                             -- ZONE_GROUP_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_ID				NUMBER(10),
                   rec.zone_display_id,                              -- ZONE_DISP_ID			NUMBER(10),
                   NULL,                                             -- ZONE_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_CURRENCY_CODE		VARCHAR2(3),
                   NULL,                                             -- ZONE_BASE_IND			NUMBER(6),
                   NULL,                                             -- SUSPEND				NUMBER(1),
                   NULL,                                             -- CURRENT_CAL_ID			NUMBER(10),
                   L_calendar,                                       -- CURRENT_CAL_NAME		VARCHAR2(100),
                   NULL,                                             -- CURRENT_CAL_DESC		VARCHAR(1000),
                   NULL,                                             -- CURRENT_CAL_END_DATE		DATE,
                   NULL,                                             -- NEW_CAL_ID			NUMBER(10),
                   NULL,                                             -- NEW_CAL_NAME			VARCHAR2(100),
                   NULL,                                             -- NEW_CAL_DESC			VARCHAR(1000),
                   NULL,                                             -- NEW_CAL_END_DATE		DATE,
                   NULL,                                             -- PRICE_GUIDE_ID			NUMBER(20),
                   NULL,                                             -- PRICE_GUIDE_NAME		VARCHAR(100),
                   NULL,                                             -- PRICE_GUIDE_DESCRIPTION		VARCHAR2(1000),
                   NULL,                                             -- PRICE_GUIDE_CURRENCY_CODE	VARCHAR2(3),
                   NULL,                                             -- PRICE_GUIDE_CORP		VARCHAR(1),
                   NULL,                                             -- PRICE_GUIDE_DEPTS		OBJ_LISTABLE4_TBL,
                   NULL,                                             -- WAREHOUSES			OBJ_LISTABLE_TBL,
                   NULL                                              -- ZONE_LOCATIONS			OBJ_ZONE_LOC_TBL
                                           );
         end if;
         close C_CHECK_CAL_OVRLAP;
      end loop;   
   end if;
   O_return_code := 1;
EXCEPTION
   WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_STRATEGY_SQL.CHECK_PS_OVERLAP_LOC_MOVE',
                                        to_char(SQLCODE));
      O_return_code := 0;
END CHECK_PS_OVERLAP_LOC_MOVE;
------------------------------------------------------------------------------------
PROCEDURE CHECK_PS_CC_OVRLAP(O_return_code          OUT NUMBER,
                             O_error_msg            OUT VARCHAR2,
                             O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                             I_dept              IN     DEPS.DEPT%TYPE,
                             I_class             IN     CLASS.CLASS%TYPE,
                             I_subclass          IN     SUBCLASS.SUBCLASS%TYPE,
                             I_zone_id           IN     RPM_ZONE_LOCATION.ZONE_ID%TYPE,
                             I_calendar_id       IN     RPM_CALENDAR.CALENDAR_ID%TYPE,
                             I_ps_strategy_type  IN     NUMBER,
                             I_strategy_id       IN     RPM_STRATEGY.STRATEGY_ID%TYPE
                            ) is
   L_calendar RPM_CALENDAR.NAME%TYPE;

   cursor C_GET_STRATEGIES is
      select distinct
             rs.strategy_id,
             rs.dept,
             rs.strategy_class class,
             rs.strategy_subclass subclass,
             rs.calendar_id,
             rs.copied_strategy_id,
             rz.zone_display_id,
             rzg.zone_group_display_id,
             rs.ps_type
        from (select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                          st.next_calendar_id
                     else
                          st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_CLEARANCE ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_clearance rsc
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsc.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))
               UNION ALL
              select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                          st.next_calendar_id
                     else
                          st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_COMPETITIVE ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_competitive rsc
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsc.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))
               UNION ALL
              select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                          st.next_calendar_id
                     else
                          st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_MARGIN ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_margin rsm
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsm.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))) rs,
                 rpm_zone rz,
                 rpm_zone_group rzg
       where rs.dept = I_dept
         and rs.class = NVL(I_class, rs.class)
         and rs.subclass = NVL(I_subclass, rs.subclass)
         and rs.calendar_id is NOT NULL
         and rs.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id
         and rs.strategy_id != I_strategy_id
         and exists ((select location
                        from rpm_zone_location
                       where zone_id = zone_hier_id)
                      intersect
                      (select location
                         from rpm_zone_location
                        where zone_id = I_zone_id))
                        ;

   cursor C_GET_MM_STRATEGIES is
      select distinct
             rs.strategy_id,
             rs.dept,
             rs.strategy_class class,
             rs.strategy_subclass subclass,
             rs.calendar_id,
             rs.copied_strategy_id,
             rz.zone_display_id,
             rzg.zone_group_display_id,
             rs.ps_type
        from (select st.strategy_id,
                     st.dept,
					 st.class strategy_class,
                     sc.class,
					 st.subclass strategy_subclass,
                     sc.subclass,
                     st.zone_hier_id,
                     case
                     when st.suspend = CALENDAR_STATUS_SUSPENDED then
                          st.next_calendar_id
                     else
                          st.current_calendar_id
                     end calendar_id,
                     case
                     when st.copied_strategy_id IS NULL then
                        0
                     else
                        1
                     end copied_strategy_id,
                     STRATEGY_TYPE_MAINTAIN_MARGIN ps_type
                from rpm_strategy st,
                     subclass sc,
                     rpm_strategy_maint_margin rsm
               where st.dept = sc.dept
                 and nvl(st.class, sc.class) = sc.class
                 and nvl(st.subclass, sc.subclass) = sc.subclass
                 and st.strategy_id = rsm.strategy_id
                 and (st.copied_strategy_id IS NULL or (st.copied_strategy_id != I_strategy_id))) rs,
                 rpm_zone rz,
                 rpm_zone_group rzg
       where rs.dept = I_dept
         and rs.class = NVL(I_class, rs.class)
         and rs.subclass = NVL(I_subclass, rs.subclass)
         and rs.calendar_id is NOT NULL
         and rs.zone_hier_id = rz.zone_id
         and rz.zone_group_id = rzg.zone_group_id
         and strategy_id != I_strategy_id
         and exists ((select location
                        from rpm_zone_location
                       where zone_id = zone_hier_id)
                      intersect
                      (select location
                         from rpm_zone_location
                        where zone_id = I_zone_id));


   cursor C_CHECK_CAL_OVRLAP(p_calendar_id NUMBER) is
      select that.name
        from (select rcp.calendar_id, t1.name, rcp.start_date, rcp.end_date
                from (select calendar_id,
                             name,
                             start_date,
                             end_date,
                             sys_connect_by_path(to_char(end_date,'YYYYMMDD'),'*') enddate
                        from rpm_calendar
                        start with calendar_id = I_calendar_id
                        connect by prior expiration_calendar_id = calendar_id) t1,
                     rpm_calendar_period rcp
               where rcp.calendar_id = t1.calendar_id
                 and rcp.start_date >= decode(instr(t1.enddate,'*',-1,2),0,t1.start_date,
                                       to_date(substr(t1.enddate,instr(t1.enddate,'*',-1,2)+1,8),'YYYYMMDD')+1)) this,
             (select rcp.calendar_id, t1.name, rcp.start_date, rcp.end_date
                from (select calendar_id,
                             name,
                             start_date,
                             end_date,
                             sys_connect_by_path(to_char(end_date,'YYYYMMDD'),'*') enddate
                        from rpm_calendar
                        start with calendar_id = p_calendar_id
                        connect by prior expiration_calendar_id = calendar_id) t1,
                        rpm_calendar_period rcp
               where rcp.calendar_id = t1.calendar_id
                 and rcp.start_date >= decode(instr(t1.enddate,'*',-1,2),0,t1.start_date,
                                       to_date(substr(t1.enddate,instr(t1.enddate,'*',-1,2)+1,8),'YYYYMMDD')+1)) that
       where (this.start_date between that.start_date and that.end_date
              or this.end_date   between that.start_date and that.end_date)
         and this.end_date >= get_vdate
         and that.end_date >= get_vdate
       order by that.start_date;

BEGIN
   O_strategy_tbl := NEW OBJ_PRICE_STRATEGY_TBL();
   if I_ps_strategy_type != STRATEGY_TYPE_MAINTAIN_MARGIN then
      for rec in C_GET_STRATEGIES loop
      if I_calendar_id != rec.calendar_id then
         open C_CHECK_CAL_OVRLAP(rec.calendar_id);
         fetch C_CHECK_CAL_OVRLAP into L_calendar;
         if C_CHECK_CAL_OVRLAP%FOUND then
            O_strategy_tbl.extend();
            O_strategy_tbl(O_strategy_tbl.count) :=
               NEW OBJ_PRICE_STRATEGY_REC (
                   rec.strategy_id,                                  -- STRATEGY_ID
	               rec.ps_type,                                      -- TYPE_CODE			VARCHAR2(1),
                   rec.copied_strategy_id,                           --	STATE_CODE			VARCHAR2(50),
                   rec.dept,                                         --	DEPT_ID				NUMBER(4),
                   NULL,                                             --	DEPT_NAME			VARCHAR2(80),
                   rec.class,                                        -- CLASS_ID			NUMBER(4),
                   NULL,                                             --	CLASS_NAME			VARCHAR2(80),
                   rec.subclass,                                     --	SUBCLASS_ID			NUMBER(4),
                   NULL,                                             -- SUBCLASS_NAME			VARCHAR2(80),
                   NULL,                                             -- ZONE_GROUP_ID			NUMBER(4),
                   rec.zone_group_display_id,                        -- ZONE_GROUP_DISP_ID		NUMBER(4),
                   NULL,                                             -- ZONE_GROUP_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_ID				NUMBER(10),
                   rec.zone_display_id,                              -- ZONE_DISP_ID			NUMBER(10),
                   NULL,                                             -- ZONE_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_CURRENCY_CODE		VARCHAR2(3),
                   NULL,                                             -- ZONE_BASE_IND			NUMBER(6),
                   NULL,                                             -- SUSPEND				NUMBER(1),
                   NULL,                                             -- CURRENT_CAL_ID			NUMBER(10),
                   L_calendar,                                       -- CURRENT_CAL_NAME		VARCHAR2(100),
                   NULL,                                             -- CURRENT_CAL_DESC		VARCHAR(1000),
                   NULL,                                             -- CURRENT_CAL_END_DATE		DATE,
                   NULL,                                             -- NEW_CAL_ID			NUMBER(10),
                   NULL,                                             -- NEW_CAL_NAME			VARCHAR2(100),
                   NULL,                                             -- NEW_CAL_DESC			VARCHAR(1000),
                   NULL,                                             -- NEW_CAL_END_DATE		DATE,
                   NULL,                                             -- PRICE_GUIDE_ID			NUMBER(20),
                   NULL,                                             -- PRICE_GUIDE_NAME		VARCHAR(100),
                   NULL,                                             -- PRICE_GUIDE_DESCRIPTION		VARCHAR2(1000),
                   NULL,                                             -- PRICE_GUIDE_CURRENCY_CODE	VARCHAR2(3),
                   NULL,                                             -- PRICE_GUIDE_CORP		VARCHAR(1),
                   NULL,                                             -- PRICE_GUIDE_DEPTS		OBJ_LISTABLE4_TBL,
                   NULL,                                             -- WAREHOUSES			OBJ_LISTABLE_TBL,
                   NULL                                              -- ZONE_LOCATIONS			OBJ_ZONE_LOC_TBL
                                           );
         end if;
         close C_CHECK_CAL_OVRLAP;
         end if;
      end loop;
   else
      for rec in C_GET_MM_STRATEGIES loop
      if I_calendar_id != rec.calendar_id then
         open C_CHECK_CAL_OVRLAP(rec.calendar_id);
         fetch C_CHECK_CAL_OVRLAP into L_calendar;
         if C_CHECK_CAL_OVRLAP%FOUND then
            O_strategy_tbl.extend();
            O_strategy_tbl(O_strategy_tbl.count) :=
               NEW OBJ_PRICE_STRATEGY_REC (
                   rec.strategy_id,                                  -- STRATEGY_ID
	               rec.ps_type,                                      -- TYPE_CODE			VARCHAR2(1),
                   rec.copied_strategy_id,                           --	STATE_CODE			VARCHAR2(50),
                   rec.dept,                                         --	DEPT_ID				NUMBER(4),
                   NULL,                                             --	DEPT_NAME			VARCHAR2(80),
                   rec.class,                                        -- CLASS_ID			NUMBER(4),
                   NULL,                                             --	CLASS_NAME			VARCHAR2(80),
                   rec.subclass,                                     --	SUBCLASS_ID			NUMBER(4),
                   NULL,                                             -- SUBCLASS_NAME			VARCHAR2(80),
                   NULL,                                             -- ZONE_GROUP_ID			NUMBER(4),
                   rec.zone_group_display_id,                        -- ZONE_GROUP_DISP_ID		NUMBER(4),
                   NULL,                                             -- ZONE_GROUP_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_ID				NUMBER(10),
                   rec.zone_display_id,                              -- ZONE_DISP_ID			NUMBER(10),
                   NULL,                                             -- ZONE_NAME			VARCHAR2(120),
                   NULL,                                             -- ZONE_CURRENCY_CODE		VARCHAR2(3),
                   NULL,                                             -- ZONE_BASE_IND			NUMBER(6),
                   NULL,                                             -- SUSPEND				NUMBER(1),
                   NULL,                                             -- CURRENT_CAL_ID			NUMBER(10),
                   L_calendar,                                       -- CURRENT_CAL_NAME		VARCHAR2(100),
                   NULL,                                             -- CURRENT_CAL_DESC		VARCHAR(1000),
                   NULL,                                             -- CURRENT_CAL_END_DATE		DATE,
                   NULL,                                             -- NEW_CAL_ID			NUMBER(10),
                   NULL,                                             -- NEW_CAL_NAME			VARCHAR2(100),
                   NULL,                                             -- NEW_CAL_DESC			VARCHAR(1000),
                   NULL,                                             -- NEW_CAL_END_DATE		DATE,
                   NULL,                                             -- PRICE_GUIDE_ID			NUMBER(20),
                   NULL,                                             -- PRICE_GUIDE_NAME		VARCHAR(100),
                   NULL,                                             -- PRICE_GUIDE_DESCRIPTION		VARCHAR2(1000),
                   NULL,                                             -- PRICE_GUIDE_CURRENCY_CODE	VARCHAR2(3),
                   NULL,                                             -- PRICE_GUIDE_CORP		VARCHAR(1),
                   NULL,                                             -- PRICE_GUIDE_DEPTS		OBJ_LISTABLE4_TBL,
                   NULL,                                             -- WAREHOUSES			OBJ_LISTABLE_TBL,
                   NULL                                              -- ZONE_LOCATIONS			OBJ_ZONE_LOC_TBL
                                           );
         end if;
         close C_CHECK_CAL_OVRLAP;
         end if;
      end loop;
   end if;
   O_return_code := 1;
EXCEPTION

  WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_STRATEGY_SQL.CHECK_PS_OVERLAP_LOC_MOVE',
                                        to_char(SQLCODE));
      O_return_code := 0;
END CHECK_PS_CC_OVRLAP;
----------------------------------------------------------------------------------------------
PROCEDURE CHECK_PS_CC_OVRLAP(O_return_code          OUT NUMBER,
                             O_error_msg            OUT VARCHAR2,
                             O_strategy_tbl         OUT OBJ_PRICE_STRATEGY_TBL,
                             I_calendar_id       IN     RPM_ZONE_LOCATION.LOCATION%TYPE
                            ) is


   cursor C_GET_STRATEGIES is
      select st.strategy_id,
             st.dept,
			 st.class strategy_class,
             sc.class,
	         st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
             case
                when st.copied_strategy_id IS NULL then
                     0
                else
                     1
                end copied_strategy_id,
             STRATEGY_TYPE_CLEARANCE ps_type
        from rpm_strategy st,
             subclass sc,
             rpm_strategy_clearance rsc
       where st.dept = sc.dept
         and nvl(st.class, sc.class) = sc.class
         and nvl(st.subclass, sc.subclass) = sc.subclass
         and st.strategy_id = rsc.strategy_id
         and st.current_calendar_id = I_calendar_id
      UNION ALL
      select st.strategy_id,
             st.dept,
	  	     st.class strategy_class,
             sc.class,
			 st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
             case
                when st.copied_strategy_id IS NULL then
                     0
                else
                     1
                end copied_strategy_id,
             STRATEGY_TYPE_COMPETITIVE ps_type
        from rpm_strategy st,
             subclass sc,
             rpm_strategy_competitive rsc
       where st.dept = sc.dept
         and nvl(st.class, sc.class) = sc.class
         and nvl(st.subclass, sc.subclass) = sc.subclass
         and st.strategy_id = rsc.strategy_id
         and st.current_calendar_id = I_calendar_id
      UNION ALL
      select st.strategy_id,
             st.dept,
	         st.class strategy_class,
             sc.class,
			 st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
             case
                when st.copied_strategy_id IS NULL then
                     0
                else
                     1
                end copied_strategy_id,
                     STRATEGY_TYPE_MARGIN ps_type
        from rpm_strategy st,
             subclass sc,
             rpm_strategy_margin rsm
       where st.dept = sc.dept
         and nvl(st.class, sc.class) = sc.class
         and nvl(st.subclass, sc.subclass) = sc.subclass
         and st.strategy_id = rsm.strategy_id
         and st.current_calendar_id = I_calendar_id
      UNION ALL
      select st.strategy_id,
             st.dept,
	         st.class strategy_class,
             sc.class,
			 st.subclass strategy_subclass,
             sc.subclass,
             st.zone_hier_id,
             case
                when st.suspend = CALENDAR_STATUS_SUSPENDED then
                     st.next_calendar_id
                else
                     st.current_calendar_id
                end calendar_id,
            case
               when st.copied_strategy_id IS NULL then
                    0
               else
                    1
               end copied_strategy_id,
            STRATEGY_TYPE_MAINTAIN_MARGIN ps_type
       from rpm_strategy st,
            subclass sc,
            rpm_strategy_maint_margin rsm
      where st.dept = sc.dept
        and nvl(st.class, sc.class) = sc.class
        and nvl(st.subclass, sc.subclass) = sc.subclass
        and st.strategy_id = rsm.strategy_id
        and st.current_calendar_id = I_calendar_id;

   p_strategy_tbl OBJ_PRICE_STRATEGY_TBL;

BEGIN
   O_strategy_tbl := new OBJ_PRICE_STRATEGY_TBL();
   for rec in C_GET_STRATEGIES loop
      p_strategy_tbl := new OBJ_PRICE_STRATEGY_TBL();
      CHECK_PS_CC_OVRLAP(O_return_code,
                         O_error_msg,
                         p_strategy_tbl,
                         rec.dept,
                         rec.class,
                         rec.subclass,
                         rec.zone_hier_id,
                         rec.calendar_id,
                         rec.ps_type,
                         rec.strategy_id
                         );
      if p_strategy_tbl.count > 0 then
         for i in p_strategy_tbl.first..p_strategy_tbl.last loop
            O_strategy_tbl.extend();
            O_strategy_tbl(O_strategy_tbl.count) := p_strategy_tbl(i);
         end loop;
      end if;
   end loop;
   O_return_code := 1;
EXCEPTION
   WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_PRICE_STRATEGY_SQL.CHECK_PS_OVERLAP_LOC_MOVE',
                                        to_char(SQLCODE));
      O_return_code := 0;
END CHECK_PS_CC_OVRLAP;
-----------------------------------------------------------------------------------------
END RPM_PRICE_STRATEGY_SQL;
/

