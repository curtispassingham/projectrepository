CREATE OR REPLACE PACKAGE BODY RPM_ITEM_RECLASS_SQL AS
-----------------------------------------------------------------------------------------------------------------------

PROCEDURE POPULATE_RPM_ITEM_MOD(O_return_code          OUT NUMBER,
                                O_error_msg            OUT VARCHAR2,
                                I_item              IN     VARCHAR2,
                                I_message_date      IN     DATE,
                                I_dept              IN     NUMBER,
                                I_class             IN     NUMBER,
                                I_subclass          IN     NUMBER) IS

   L_dept            RPM_FUTURE_RETAIL.DEPT%TYPE      := 0;
   L_class           RPM_FUTURE_RETAIL.CLASS%TYPE     := 0;
   L_subclass        RPM_FUTURE_RETAIL.SUBCLASS%TYPE  := 0;
   L_new_dept        RPM_FUTURE_RETAIL.DEPT%TYPE      := 0;
   L_new_class       RPM_FUTURE_RETAIL.CLASS%TYPE     := 0;
   L_new_subclass    RPM_FUTURE_RETAIL.SUBCLASS%TYPE  := 0;

   cursor C_FR is
      select /*+ FIRST_ROWS */
             dept,
             class,
             subclass
        from rpm_future_retail
       where item   = I_item
         and rownum = 1;

   cursor C_ITEM is
      select dept,
             class,
             subclass
        from item_master
       where item = I_item;

BEGIN

   O_return_code := 1;

   open  C_FR;
   fetch C_FR into L_dept,
                   L_class,
                   L_subclass;

   close C_FR;

   open  C_ITEM;
   fetch C_item into L_new_dept,
                     L_new_class,
                     L_new_subclass;

   if C_ITEM%NOTFOUND then
      close C_ITEM;
      return;
   end if;

   close C_ITEM;

   if NVL(L_dept,-999)     != L_new_dept or
      NVL(L_class,-999)    != L_new_class or
      NVL(L_subclass,-999) != L_new_subclass then
      merge into rpm_item_modification rim
      using (select I_item item,
                    I_message_date message_date,
                    L_new_dept dept,
                    L_new_class class,
                    L_new_subclass subclass
               from dual) merch
      on (rim.item = merch.item)
      when MATCHED then
         update
            set message_date = merch.message_date,
                dept         = merch.dept,
                class        = merch.class,
                subclass     = merch.subclass
      when NOT MATCHED then
         insert (rpm_item_modification_id,
                 item,
                 message_date,
                 dept,
                 class,
                 subclass,
                 lock_version)
         values (rpm_item_mod_id_seq.nextval,
                 merch.item,
                 merch.message_date,
                 merch.dept,
                 merch.class,
                 merch.subclass,
                 0);
   end if;

EXCEPTION
   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_ITEM_RECLASS_SQL.POPULATE_RPM_ITEM_MOD',
                                        to_char(SQLCODE));
      O_return_code := 0;

END POPULATE_RPM_ITEM_MOD;
-----------------------------------------------------------------------------------------------------------------------
PROCEDURE RECLASS_FUTURE_RETAIL(O_return_code    OUT NUMBER,
                                O_error_msg      OUT VARCHAR2)
IS

   L_program VARCHAR2(45) := 'RPM_ITEM_RECLASS_SQL.RECLASS_FUTURE_RETAIL';

   L_im_item         ITEM_TBL             := NULL;
   L_fr_rowid_char   ROWID_CHAR_TBL       := NULL;
   L_expl_rowid_char ROWID_CHAR_TBL       := NULL;
   L_csp_rowid_char  ROWID_CHAR_TBL       := NULL;
   L_zfr_rowid_char  ROWID_CHAR_TBL       := NULL;
   L_izp_rowid_char  ROWID_CHAR_TBL       := NULL;
   L_items_to_rollup OBJ_VARCHAR_ID_TABLE := NULL;

   cursor C_ITEM_MOD is
      select item
        from rpm_item_modification;

   cursor C_FR_SAME_ZONE is
      select ROWIDTOCHAR(fr.rowid) rfr_rowid
        from rpm_future_retail fr,
             rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group   = gtt.new_regular_zone_group
         and fr.dept                      = gtt.old_dept
         and (fr.item_parent = gtt.item_parent or
 		      (fr.item       = gtt.item_parent and
               fr.item_parent is null))
         for update of fr.lock_version nowait;

   cursor C_EXPL_SAME_ZONE is
      select ROWIDTOCHAR(rpi.rowid) expl_rowid
        from rpm_promo_item_loc_expl rpi,
             rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group     = gtt.new_regular_zone_group
         and rpi.dept                       = gtt.old_dept
         and NVL(rpi.item_parent, rpi.item) = gtt.item_parent
         for update of rpi.customer_type nowait;

   cursor C_CSFR_SAME_ZONE is
      select ROWIDTOCHAR(csp.rowid) cspr_rowid
        from rpm_cust_segment_promo_fr csp,
             rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group     = gtt.new_regular_zone_group
         and gtt.new_dept                  != gtt.old_dept
         and csp.dept                       = gtt.old_dept
         and NVL(csp.item_parent, csp.item) = gtt.item_parent
         for update of csp.complex_promo_ind nowait;

   cursor C_OLD_ZFR is
      select ROWIDTOCHAR(zfr.rowid) zfr_rowid
        from (select distinct im.item,
                     im.dept,
                     im.class,
                     im.subclass,
                     mrd.regular_zone_group new_zone_group
                from rpm_item_modification im,
                     rpm_merch_retail_def mrd
               where im.dept     = mrd.dept
                 and im.class    = NVL(mrd.class, im.class)
                 and im.subclass = NVL(mrd.subclass, im.subclass)) reclass,
             rpm_zone_future_retail zfr
       where zfr.item = reclass.item
         and zfr.zone NOT IN (select zone_id
                                from rpm_zone
                               where zone_group_id = reclass.new_zone_group)
         for update of zone_future_retail_id nowait;

   cursor C_OLD_IZP is
      select ROWIDTOCHAR(izp.rowid) izp_rowid
        from (select distinct im.item,
                     im.dept,
                     im.class,
                     im.subclass,
                     mrd.regular_zone_group new_zone_group
                from rpm_item_modification im,
                     rpm_merch_retail_def mrd
               where im.dept     = mrd.dept
                 and im.class    = NVL(mrd.class, im.class)
                 and im.subclass = NVL(mrd.subclass, im.subclass)) reclass,
             rpm_item_zone_price izp
       where izp.item    = reclass.item
         and izp.zone_id NOT IN (select zone_id
                                   from rpm_zone
                                  where zone_group_id = reclass.new_zone_group)
         for update of item_zone_price_id nowait;

   cursor C_ITEMS_TO_ROLLUP is
      select distinct item_parent
        from rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group != gtt.new_regular_zone_group;

   cursor C_FR_NEW_ZONE is
      select ROWIDTOCHAR(fr.rowid) rfr_rowid
        from rpm_future_retail fr,
             rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group  != gtt.new_regular_zone_group
         and fr.dept                      = gtt.old_dept
         and (fr.item_parent              = gtt.item_parent or
 		      (fr.item                    = gtt.item_parent and
               fr.item_parent is null))
         for update of fr.lock_version nowait;

   cursor C_EXPL_NEW_ZONE is
      select ROWIDTOCHAR(rpi.rowid) expl_rowid
        from rpm_promo_item_loc_expl rpi,
             rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group    != gtt.new_regular_zone_group
         and rpi.dept                       = gtt.old_dept
         and NVL(rpi.item_parent, rpi.item) = gtt.item_parent
         for update of rpi.customer_type nowait;

   cursor C_CSFR_NEW_ZONE is
      select ROWIDTOCHAR(csp.rowid) cspr_rowid
        from rpm_cust_segment_promo_fr csp,
             rpm_item_modification_gtt gtt
       where gtt.old_regular_zone_group    != gtt.new_regular_zone_group
         and csp.dept                       = gtt.old_dept
         and NVL(csp.item_parent, csp.item) = gtt.item_parent
         for update of csp.complex_promo_ind nowait;

BEGIN

   O_return_code := 1;

   open C_ITEM_MOD;
   fetch C_ITEM_MOD BULK COLLECT into L_im_item;
   close C_ITEM_MOD;

   if L_im_item is NULL or
      L_im_item.COUNT = 0 then
      --
      return;
   end if;

   delete from rpm_item_modification_gtt;

   -- This function assumes that the whole Item Hierarchy will be reclassed to the same Dept/Class/Subclass

   -- If RMS populate the old dept/class/subclass to RPM_ITEM_MODIFICATION
   -- the following insert statement can be simplified and the merge statement can be eliminated
   insert into rpm_item_modification_gtt(item,
                                         item_parent,
                                         new_dept,
                                         new_class,
                                         new_subclass,
                                         new_regular_zone_group,
                                         old_dept)
      select q.item,
             q.item_parent,
             q.new_dept,
             q.new_class,
             q.new_subclass,
             q.new_regular_zone_group,
             q.old_dept
        from (select r.item,
                     r.item_parent,
                     r.new_dept,
                     r.new_class,
                     r.new_subclass,
                     r.new_regular_zone_group,
                     ril.dept old_dept,
                     ROW_NUMBER() OVER (PARTITION BY r.item_parent
                                            ORDER BY ril.loc asc) locrank
                from (select item,
                             item_parent,
                             new_dept,
                             new_class,
                             new_subclass,
                             new_regular_zone_group
                        from (select rim.item,
                                     NVL(im.item_parent, im.item) item_parent,
                                     rim.dept new_dept,
                                     rim.class new_class,
                                     rim.subclass new_subclass,
                                     def.regular_zone_group new_regular_zone_group
                                from rpm_item_modification rim,
                                     item_master im,
                                     rpm_merch_retail_def_expl def
                               where im.item         = rim.item
                                 and im.tran_level   = im.item_level
                                 and im.status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                                 and im.sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                                 and def.dept        = rim.dept
                                 and def.class       = rim.class
                                 and def.subclass    = rim.subclass)) r,
                     rpm_item_loc ril
               where ril.item = r.item) q
       where q.locrank = 1;

   merge into rpm_item_modification_gtt target
   using (select s.item,
                 s.item_parent,
                 s.old_dept,
                 s.old_class,
                 s.old_subclass,
                 def.regular_zone_group old_regular_zone_group
            from (select t.item,
                         t.item_parent,
                         t.dept old_dept,
                         t.class old_class,
                         t.subclass old_subclass
                    from (select gtt.item,
                                 gtt.item_parent,
                                 fr.dept,
                                 fr.class,
                                 fr.subclass,
                                 ROW_NUMBER() OVER (PARTITION BY gtt.item_parent
                                                        ORDER BY fr.future_retail_id) frrank
                            from rpm_item_modification_gtt gtt,
                                 rpm_future_retail fr
                           where fr.dept                      = gtt.old_dept
                             and (fr.item_parent              = gtt.item_parent or
 		                          (fr.item                    = gtt.item_parent and
                                   fr.item_parent is null))) t
                   where t.frrank = 1) s,
                 rpm_merch_retail_def_expl def
           where def.dept     = s.old_dept
             and def.class    = s.old_class
             and def.subclass = s.old_subclass) source
   on (    target.item        = source.item
       and target.item_parent = source.item_parent)
   when MATCHED then
      update
         set target.old_class              = source.old_class,
             target.old_subclass           = source.old_subclass,
             target.old_regular_zone_group = source.old_regular_zone_group;

   -- Reclass Future Retail Tables for which the new Merch Hierarchy has the same Primary Zone as the
   -- Old Merch Hierarchy

   open C_FR_SAME_ZONE;
   fetch C_FR_SAME_ZONE BULK COLLECT into L_fr_rowid_char;
   close C_FR_SAME_ZONE;

   if L_fr_rowid_char is NOT NULL and
      L_fr_rowid_char.COUNT > 0 then

      insert into rpm_future_retail(future_retail_id,
                                    item,
                                    dept,
                                    class,
                                    subclass,
                                    zone_node_type,
                                    location,
                                    action_date,
                                    selling_retail,
                                    selling_retail_currency,
                                    selling_uom,
                                    multi_units,
                                    multi_unit_retail,
                                    multi_unit_retail_currency,
                                    multi_selling_uom,
                                    clear_exception_parent_id,
                                    clear_retail,
                                    clear_retail_currency,
                                    clear_uom,
                                    simple_promo_retail,
                                    simple_promo_retail_currency,
                                    simple_promo_uom,
                                    price_change_id,
                                    price_change_display_id,
                                    pc_exception_parent_id,
                                    pc_change_type,
                                    pc_change_amount,
                                    pc_change_currency,
                                    pc_change_percent,
                                    pc_change_selling_uom,
                                    pc_null_multi_ind,
                                    pc_multi_units,
                                    pc_multi_unit_retail,
                                    pc_multi_unit_retail_currency,
                                    pc_multi_selling_uom,
                                    pc_price_guide_id,
                                    clearance_id,
                                    clearance_display_id,
                                    clear_mkdn_index,
                                    clear_start_ind,
                                    clear_change_type,
                                    clear_change_amount,
                                    clear_change_currency,
                                    clear_change_percent,
                                    clear_change_selling_uom,
                                    clear_price_guide_id,
                                    loc_move_from_zone_id,
                                    loc_move_to_zone_id,
                                    location_move_id,
                                    on_simple_promo_ind,
                                    on_complex_promo_ind,
                                    item_parent,
                                    diff_id,
                                    zone_id,
                                    max_hier_level,
                                    cur_hier_level)
         select RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
                fr.item,
                gtt.new_dept,
                gtt.new_class,
                gtt.new_subclass,
                zone_node_type,
                location,
                action_date,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                clear_exception_parent_id,
                clear_retail,
                clear_retail_currency,
                clear_uom,
                simple_promo_retail,
                simple_promo_retail_currency,
                simple_promo_uom,
                price_change_id,
                price_change_display_id,
                pc_exception_parent_id,
                pc_change_type,
                pc_change_amount,
                pc_change_currency,
                pc_change_percent,
                pc_change_selling_uom,
                pc_null_multi_ind,
                pc_multi_units,
                pc_multi_unit_retail,
                pc_multi_unit_retail_currency,
                pc_multi_selling_uom,
                pc_price_guide_id,
                clearance_id,
                clearance_display_id,
                clear_mkdn_index,
                clear_start_ind,
                clear_change_type,
                clear_change_amount,
                clear_change_currency,
                clear_change_percent,
                clear_change_selling_uom,
                clear_price_guide_id,
                loc_move_from_zone_id,
                loc_move_to_zone_id,
                location_move_id,
                on_simple_promo_ind,
                on_complex_promo_ind,
                fr.item_parent,
                diff_id,
                zone_id,
                max_hier_level,
                cur_hier_level
           from rpm_future_retail fr,
                rpm_item_modification_gtt gtt
          where gtt.old_regular_zone_group   = gtt.new_regular_zone_group
            and fr.dept                      = gtt.old_dept
            and (fr.item_parent = gtt.item_parent or
   			     (fr.item       = gtt.item_parent and
                  fr.item_parent is null ));

      forall i IN 1 .. l_fr_rowid_char.COUNT
         delete from rpm_future_retail
          where rowid = L_fr_rowid_char(i);

   end if;

   open C_EXPL_SAME_ZONE;
   fetch C_EXPL_SAME_ZONE BULK COLLECT into L_expl_rowid_char;
   close C_EXPL_SAME_ZONE;

   if L_expl_rowid_char is NOT NULL and
      L_expl_rowid_char.COUNT > 0 then

      insert into rpm_promo_item_loc_expl(promo_item_loc_expl_id,
                                          item,
                                          dept,
                                          class,
                                          subclass,
                                          location,
                                          promo_id,
                                          promo_display_id,
                                          promo_secondary_ind,
                                          promo_comp_id,
                                          comp_display_id,
                                          type,
                                          customer_type,
                                          detail_secondary_ind,
                                          detail_start_date,
                                          detail_end_date,
                                          detail_apply_to_code,
                                          detail_change_type,
                                          detail_change_amount,
                                          detail_change_currency,
                                          detail_change_percent,
                                          detail_change_selling_uom,
                                          detail_price_guide_id,
                                          exception_parent_id,
                                          promo_dtl_id,
                                          zone_node_type,
                                          zone_id,
                                          item_parent,
                                          diff_id,
                                          max_hier_level,
                                          cur_hier_level,
                                          timebased_dtl_ind)
         select RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
                expl.item,
                gtt.new_dept,
                gtt.new_class,
                gtt.new_subclass,
                location,
                promo_id,
                promo_display_id,
                promo_secondary_ind,
                promo_comp_id,
                comp_display_id,
                type,
                customer_type,
                detail_secondary_ind,
                detail_start_date,
                detail_end_date,
                detail_apply_to_code,
                detail_change_type,
                detail_change_amount,
                detail_change_currency,
                detail_change_percent,
                detail_change_selling_uom,
                detail_price_guide_id,
                exception_parent_id,
                promo_dtl_id,
                zone_node_type,
                zone_id,
                expl.item_parent,
                diff_id,
                max_hier_level,
                cur_hier_level,
                timebased_dtl_ind
           from rpm_promo_item_loc_expl expl,
                rpm_item_modification_gtt gtt
          where gtt.old_regular_zone_group       = gtt.new_regular_zone_group
            and expl.dept                        = gtt.old_dept
            and NVL(expl.item_parent, expl.item) = gtt.item_parent;

      forall i IN 1 .. l_expl_rowid_char.COUNT
         delete from rpm_promo_item_loc_expl
          where rowid = L_expl_rowid_char(i);

   end if;

   open C_CSFR_SAME_ZONE;
   fetch C_CSFR_SAME_ZONE BULK COLLECT into L_csp_rowid_char;
   close C_CSFR_SAME_ZONE;

   if L_csp_rowid_char is NOT NULL and
      L_csp_rowid_char.COUNT > 0 then

      insert into rpm_cust_segment_promo_fr(cust_segment_promo_id,
                                            item,
                                            zone_node_type,
                                            location,
                                            action_date,
                                            customer_type,
                                            dept,
                                            promo_retail,
                                            promo_retail_currency,
                                            promo_uom,
                                            complex_promo_ind,
                                            zone_id,
                                            item_parent,
                                            diff_id,
                                            max_hier_level,
                                            cur_hier_level)
         select RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                csp.item,
                zone_node_type,
                location,
                action_date,
                customer_type,
                gtt.new_dept,
                promo_retail,
                promo_retail_currency,
                promo_uom,
                complex_promo_ind,
                zone_id,
                csp.item_parent,
                diff_id,
                max_hier_level,
                cur_hier_level
           from rpm_cust_segment_promo_fr csp,
                rpm_item_modification_gtt gtt
          where gtt.old_regular_zone_group     = gtt.new_regular_zone_group
            and gtt.new_dept                  != gtt.old_dept
            and csp.dept                       = gtt.old_dept
            and NVL(csp.item_parent, csp.item) = gtt.item_parent;

      forall i IN 1 .. L_csp_rowid_char.COUNT
         delete from rpm_cust_segment_promo_fr
          where rowid = L_csp_rowid_char(i);

   end if;

   -- Reclass Future Retail Tables for which the new Merch Hierarchy has different Primary Zone from the
   -- Old Merch Hierarchy

   open C_ITEMS_TO_ROLLUP;
   fetch C_ITEMS_TO_ROLLUP BULK COLLECT into L_items_to_rollup;
   close C_ITEMS_TO_ROLLUP;

   if L_items_to_rollup is NOT NULL and
      L_items_to_rollup.COUNT > 0 then

      open C_FR_NEW_ZONE;
      fetch C_FR_NEW_ZONE BULK COLLECT into L_fr_rowid_char;
      close C_FR_NEW_ZONE;

      open C_EXPL_NEW_ZONE;
      fetch C_EXPL_NEW_ZONE BULK COLLECT into L_expl_rowid_char;
      close C_EXPL_NEW_ZONE;

      open C_CSFR_NEW_ZONE;
      fetch C_CSFR_NEW_ZONE BULK COLLECT into L_csp_rowid_char;
      close C_CSFR_NEW_ZONE;

      if L_fr_rowid_char is NOT NULL and
         L_fr_rowid_char.COUNT > 0 then

         insert into rpm_future_retail(future_retail_id,
                                       item,
                                       dept,
                                       class,
                                       subclass,
                                       zone_node_type,
                                       location,
                                       action_date,
                                       selling_retail,
                                       selling_retail_currency,
                                       selling_uom,
                                       multi_units,
                                       multi_unit_retail,
                                       multi_unit_retail_currency,
                                       multi_selling_uom,
                                       clear_exception_parent_id,
                                       clear_retail,
                                       clear_retail_currency,
                                       clear_uom,
                                       simple_promo_retail,
                                       simple_promo_retail_currency,
                                       simple_promo_uom,
                                       price_change_id,
                                       price_change_display_id,
                                       pc_exception_parent_id,
                                       pc_change_type,
                                       pc_change_amount,
                                       pc_change_currency,
                                       pc_change_percent,
                                       pc_change_selling_uom,
                                       pc_null_multi_ind,
                                       pc_multi_units,
                                       pc_multi_unit_retail,
                                       pc_multi_unit_retail_currency,
                                       pc_multi_selling_uom,
                                       pc_price_guide_id,
                                       clearance_id,
                                       clearance_display_id,
                                       clear_mkdn_index,
                                       clear_start_ind,
                                       clear_change_type,
                                       clear_change_amount,
                                       clear_change_currency,
                                       clear_change_percent,
                                       clear_change_selling_uom,
                                       clear_price_guide_id,
                                       loc_move_from_zone_id,
                                       loc_move_to_zone_id,
                                       location_move_id,
                                       on_simple_promo_ind,
                                       on_complex_promo_ind)
            select RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
                   item,
                   dept,
                   class,
                   subclass,
                   zone_node_type,
                   location,
                   action_date,
                   selling_retail,
                   selling_retail_currency,
                   selling_uom,
                   multi_units,
                   multi_unit_retail,
                   multi_unit_retail_currency,
                   multi_selling_uom,
                   clear_exception_parent_id,
                   clear_retail,
                   clear_retail_currency,
                   clear_uom,
                   simple_promo_retail,
                   simple_promo_retail_currency,
                   simple_promo_uom,
                   price_change_id,
                   price_change_display_id,
                   pc_exception_parent_id,
                   pc_change_type,
                   pc_change_amount,
                   pc_change_currency,
                   pc_change_percent,
                   pc_change_selling_uom,
                   pc_null_multi_ind,
                   pc_multi_units,
                   pc_multi_unit_retail,
                   pc_multi_unit_retail_currency,
                   pc_multi_selling_uom,
                   pc_price_guide_id,
                   clearance_id,
                   clearance_display_id,
                   clear_mkdn_index,
                   clear_start_ind,
                   clear_change_type,
                   clear_change_amount,
                   clear_change_currency,
                   clear_change_percent,
                   clear_change_selling_uom,
                   clear_price_guide_id,
                   loc_move_from_zone_id,
                   loc_move_to_zone_id,
                   location_move_id,
                   on_simple_promo_ind,
                   on_complex_promo_ind
              from (select item,
                           new_dept dept,
                           new_class class,
                           new_subclass subclass,
                           zone_node_type,
                           location,
                           action_date,
                           selling_retail,
                           selling_retail_currency,
                           selling_uom,
                           multi_units,
                           multi_unit_retail,
                           multi_unit_retail_currency,
                           multi_selling_uom,
                           clear_exception_parent_id,
                           clear_retail,
                           clear_retail_currency,
                           clear_uom,
                           simple_promo_retail,
                           simple_promo_retail_currency,
                           simple_promo_uom,
                           price_change_id,
                           price_change_display_id,
                           pc_exception_parent_id,
                           pc_change_type,
                           pc_change_amount,
                           pc_change_currency,
                           pc_change_percent,
                           pc_change_selling_uom,
                           pc_null_multi_ind,
                           pc_multi_units,
                           pc_multi_unit_retail,
                           pc_multi_unit_retail_currency,
                           pc_multi_selling_uom,
                           pc_price_guide_id,
                           clearance_id,
                           clearance_display_id,
                           clear_mkdn_index,
                           clear_start_ind,
                           clear_change_type,
                           clear_change_amount,
                           clear_change_currency,
                           clear_change_percent,
                           clear_change_selling_uom,
                           clear_price_guide_id,
                           loc_move_from_zone_id,
                           loc_move_to_zone_id,
                           location_move_id,
                           on_simple_promo_ind,
                           on_complex_promo_ind,
                           ROW_NUMBER() OVER (PARTITION BY t.item,
                                                           t.location,
                                                           t.action_date
                                                  ORDER BY fr_level) rank
                      from (select fr.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   zone_node_type,
                                   location,
                                   action_date,
                                   selling_retail,
                                   selling_retail_currency,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_unit_retail_currency,
                                   multi_selling_uom,
                                   clear_exception_parent_id,
                                   clear_retail,
                                   clear_retail_currency,
                                   clear_uom,
                                   simple_promo_retail,
                                   simple_promo_retail_currency,
                                   simple_promo_uom,
                                   price_change_id,
                                   price_change_display_id,
                                   pc_exception_parent_id,
                                   pc_change_type,
                                   pc_change_amount,
                                   pc_change_currency,
                                   pc_change_percent,
                                   pc_change_selling_uom,
                                   pc_null_multi_ind,
                                   pc_multi_units,
                                   pc_multi_unit_retail,
                                   pc_multi_unit_retail_currency,
                                   pc_multi_selling_uom,
                                   pc_price_guide_id,
                                   clearance_id,
                                   clearance_display_id,
                                   clear_mkdn_index,
                                   clear_start_ind,
                                   clear_change_type,
                                   clear_change_amount,
                                   clear_change_currency,
                                   clear_change_percent,
                                   clear_change_selling_uom,
                                   clear_price_guide_id,
                                   loc_move_from_zone_id,
                                   loc_move_to_zone_id,
                                   location_move_id,
                                   on_simple_promo_ind,
                                   on_complex_promo_ind,
                                   0 fr_level
                              from rpm_future_retail fr,
                                   rpm_item_modification_gtt gtt
                             where gtt.old_regular_zone_group  != gtt.new_regular_zone_group
                               and fr.dept                      = gtt.old_dept
                               and (fr.item_parent              = gtt.item_parent or
 		                            (fr.item                    = gtt.item_parent and
                                     fr.item_parent is null))
                               and fr.cur_hier_level            = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   zone_node_type,
                                   location,
                                   action_date,
                                   selling_retail,
                                   selling_retail_currency,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_unit_retail_currency,
                                   multi_selling_uom,
                                   clear_exception_parent_id,
                                   clear_retail,
                                   clear_retail_currency,
                                   clear_uom,
                                   simple_promo_retail,
                                   simple_promo_retail_currency,
                                   simple_promo_uom,
                                   price_change_id,
                                   price_change_display_id,
                                   pc_exception_parent_id,
                                   pc_change_type,
                                   pc_change_amount,
                                   pc_change_currency,
                                   pc_change_percent,
                                   pc_change_selling_uom,
                                   pc_null_multi_ind,
                                   pc_multi_units,
                                   pc_multi_unit_retail,
                                   pc_multi_unit_retail_currency,
                                   pc_multi_selling_uom,
                                   pc_price_guide_id,
                                   clearance_id,
                                   clearance_display_id,
                                   clear_mkdn_index,
                                   clear_start_ind,
                                   clear_change_type,
                                   clear_change_amount,
                                   clear_change_currency,
                                   clear_change_percent,
                                   clear_change_selling_uom,
                                   clear_price_guide_id,
                                   loc_move_from_zone_id,
                                   loc_move_to_zone_id,
                                   location_move_id,
                                   on_simple_promo_ind,
                                   on_complex_promo_ind,
                                   1 fr_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_future_retail fr,
                                   item_master im
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and fr.dept                     = gtt.old_dept
                               and fr.item                     = gtt.item_parent
                               and fr.cur_hier_level           = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                               and im.item_parent              = fr.item
                               and im.diff_1                   = fr.diff_id
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   zone_node_type,
                                   location,
                                   action_date,
                                   selling_retail,
                                   selling_retail_currency,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_unit_retail_currency,
                                   multi_selling_uom,
                                   clear_exception_parent_id,
                                   clear_retail,
                                   clear_retail_currency,
                                   clear_uom,
                                   simple_promo_retail,
                                   simple_promo_retail_currency,
                                   simple_promo_uom,
                                   price_change_id,
                                   price_change_display_id,
                                   pc_exception_parent_id,
                                   pc_change_type,
                                   pc_change_amount,
                                   pc_change_currency,
                                   pc_change_percent,
                                   pc_change_selling_uom,
                                   pc_null_multi_ind,
                                   pc_multi_units,
                                   pc_multi_unit_retail,
                                   pc_multi_unit_retail_currency,
                                   pc_multi_selling_uom,
                                   pc_price_guide_id,
                                   clearance_id,
                                   clearance_display_id,
                                   clear_mkdn_index,
                                   clear_start_ind,
                                   clear_change_type,
                                   clear_change_amount,
                                   clear_change_currency,
                                   clear_change_percent,
                                   clear_change_selling_uom,
                                   clear_price_guide_id,
                                   loc_move_from_zone_id,
                                   loc_move_to_zone_id,
                                   location_move_id,
                                   on_simple_promo_ind,
                                   on_complex_promo_ind,
                                   2 fr_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_future_retail fr,
                                   item_master im
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and fr.dept                     = gtt.old_dept
                               and fr.item                     = gtt.item_parent
                               and fr.cur_hier_level           = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                               and im.item_parent              = fr.item
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            select fr.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   rzl.loc_type zone_node_type,
                                   rzl.location,
                                   action_date,
                                   selling_retail,
                                   selling_retail_currency,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_unit_retail_currency,
                                   multi_selling_uom,
                                   clear_exception_parent_id,
                                   clear_retail,
                                   clear_retail_currency,
                                   clear_uom,
                                   simple_promo_retail,
                                   simple_promo_retail_currency,
                                   simple_promo_uom,
                                   price_change_id,
                                   price_change_display_id,
                                   pc_exception_parent_id,
                                   pc_change_type,
                                   pc_change_amount,
                                   pc_change_currency,
                                   pc_change_percent,
                                   pc_change_selling_uom,
                                   pc_null_multi_ind,
                                   pc_multi_units,
                                   pc_multi_unit_retail,
                                   pc_multi_unit_retail_currency,
                                   pc_multi_selling_uom,
                                   pc_price_guide_id,
                                   clearance_id,
                                   clearance_display_id,
                                   clear_mkdn_index,
                                   clear_start_ind,
                                   clear_change_type,
                                   clear_change_amount,
                                   clear_change_currency,
                                   clear_change_percent,
                                   clear_change_selling_uom,
                                   clear_price_guide_id,
                                   loc_move_from_zone_id,
                                   loc_move_to_zone_id,
                                   location_move_id,
                                   on_simple_promo_ind,
                                   on_complex_promo_ind,
                                   1 fr_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_future_retail fr,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group  != gtt.new_regular_zone_group
                               and fr.dept                      = gtt.old_dept
                               and (fr.item_parent              = gtt.item_parent or
 		                            (fr.item                    = gtt.item_parent and
                                     fr.item_parent is null))
                               and fr.cur_hier_level            = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                               and rzl.zone_id                  = fr.location
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   rzl.loc_type zone_node_type,
                                   rzl.location,
                                   action_date,
                                   selling_retail,
                                   selling_retail_currency,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_unit_retail_currency,
                                   multi_selling_uom,
                                   clear_exception_parent_id,
                                   clear_retail,
                                   clear_retail_currency,
                                   clear_uom,
                                   simple_promo_retail,
                                   simple_promo_retail_currency,
                                   simple_promo_uom,
                                   price_change_id,
                                   price_change_display_id,
                                   pc_exception_parent_id,
                                   pc_change_type,
                                   pc_change_amount,
                                   pc_change_currency,
                                   pc_change_percent,
                                   pc_change_selling_uom,
                                   pc_null_multi_ind,
                                   pc_multi_units,
                                   pc_multi_unit_retail,
                                   pc_multi_unit_retail_currency,
                                   pc_multi_selling_uom,
                                   pc_price_guide_id,
                                   clearance_id,
                                   clearance_display_id,
                                   clear_mkdn_index,
                                   clear_start_ind,
                                   clear_change_type,
                                   clear_change_amount,
                                   clear_change_currency,
                                   clear_change_percent,
                                   clear_change_selling_uom,
                                   clear_price_guide_id,
                                   loc_move_from_zone_id,
                                   loc_move_to_zone_id,
                                   location_move_id,
                                   on_simple_promo_ind,
                                   on_complex_promo_ind,
                                   2 fr_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_future_retail fr,
                                   item_master im,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and fr.dept                     = gtt.old_dept
                               and fr.item                     = gtt.item_parent
                               and fr.cur_hier_level           = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                               and im.item_parent              = fr.item
                               and im.diff_1                   = fr.diff_id
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and rzl.zone_id                 = fr.location
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   rzl.loc_type zone_node_type,
                                   rzl.location,
                                   action_date,
                                   selling_retail,
                                   selling_retail_currency,
                                   selling_uom,
                                   multi_units,
                                   multi_unit_retail,
                                   multi_unit_retail_currency,
                                   multi_selling_uom,
                                   clear_exception_parent_id,
                                   clear_retail,
                                   clear_retail_currency,
                                   clear_uom,
                                   simple_promo_retail,
                                   simple_promo_retail_currency,
                                   simple_promo_uom,
                                   price_change_id,
                                   price_change_display_id,
                                   pc_exception_parent_id,
                                   pc_change_type,
                                   pc_change_amount,
                                   pc_change_currency,
                                   pc_change_percent,
                                   pc_change_selling_uom,
                                   pc_null_multi_ind,
                                   pc_multi_units,
                                   pc_multi_unit_retail,
                                   pc_multi_unit_retail_currency,
                                   pc_multi_selling_uom,
                                   pc_price_guide_id,
                                   clearance_id,
                                   clearance_display_id,
                                   clear_mkdn_index,
                                   clear_start_ind,
                                   clear_change_type,
                                   clear_change_amount,
                                   clear_change_currency,
                                   clear_change_percent,
                                   clear_change_selling_uom,
                                   clear_price_guide_id,
                                   loc_move_from_zone_id,
                                   loc_move_to_zone_id,
                                   location_move_id,
                                   on_simple_promo_ind,
                                   on_complex_promo_ind,
                                   3 fr_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_future_retail fr,
                                   item_master im,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and fr.dept                     = gtt.old_dept
                               and fr.item                     = gtt.item_parent
                               and fr.cur_hier_level           = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                               and im.item_parent              = fr.item
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and rzl.zone_id                 = fr.location) t) s
             where s.rank = 1;

         forall i IN 1 .. L_fr_rowid_char.COUNT
            delete from rpm_future_retail
             where rowid = L_fr_rowid_char(i);

      end if;

      if L_expl_rowid_char is NOT NULL and
         L_expl_rowid_char.COUNT > 0 then

         insert into rpm_promo_item_loc_expl(promo_item_loc_expl_id,
                                             item,
                                             dept,
                                             class,
                                             subclass,
                                             location,
                                             promo_id,
                                             promo_display_id,
                                             promo_secondary_ind,
                                             promo_comp_id,
                                             comp_display_id,
                                             type,
                                             customer_type,
                                             detail_secondary_ind,
                                             detail_start_date,
                                             detail_end_date,
                                             detail_apply_to_code,
                                             detail_change_type,
                                             detail_change_amount,
                                             detail_change_currency,
                                             detail_change_percent,
                                             detail_change_selling_uom,
                                             detail_price_guide_id,
                                             exception_parent_id,
                                             promo_dtl_id,
                                             zone_node_type,
                                             timebased_dtl_ind)
            select RPM_PROM_IL_EXPL_SEQ.NEXTVAL,
                   item,
                   new_dept,
                   new_class,
                   new_subclass,
                   location,
                   promo_id,
                   promo_display_id,
                   promo_secondary_ind,
                   promo_comp_id,
                   comp_display_id,
                   type,
                   customer_type,
                   detail_secondary_ind,
                   detail_start_date,
                   detail_end_date,
                   detail_apply_to_code,
                   detail_change_type,
                   detail_change_amount,
                   detail_change_currency,
                   detail_change_percent,
                   detail_change_selling_uom,
                   detail_price_guide_id,
                   exception_parent_id,
                   promo_dtl_id,
                   zone_node_type,
                   timebased_dtl_ind
              from (select item,
                           new_dept,
                           new_class,
                           new_subclass,
                           location,
                           promo_id,
                           promo_display_id,
                           promo_secondary_ind,
                           promo_comp_id,
                           comp_display_id,
                           type,
                           customer_type,
                           detail_secondary_ind,
                           detail_start_date,
                           detail_end_date,
                           detail_apply_to_code,
                           detail_change_type,
                           detail_change_amount,
                           detail_change_currency,
                           detail_change_percent,
                           detail_change_selling_uom,
                           detail_price_guide_id,
                           exception_parent_id,
                           promo_dtl_id,
                           zone_node_type,
                           ROW_NUMBER() OVER (PARTITION BY t.item,
                                                           t.location,
                                                           t.promo_dtl_id
                                                  ORDER BY expl_level) rank,
                           timebased_dtl_ind
                      from (select expl.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   location,
                                   promo_id,
                                   promo_display_id,
                                   promo_secondary_ind,
                                   promo_comp_id,
                                   comp_display_id,
                                   type,
                                   customer_type,
                                   detail_secondary_ind,
                                   detail_start_date,
                                   detail_end_date,
                                   detail_apply_to_code,
                                   detail_change_type,
                                   detail_change_amount,
                                   detail_change_currency,
                                   detail_change_percent,
                                   detail_change_selling_uom,
                                   detail_price_guide_id,
                                   exception_parent_id,
                                   promo_dtl_id,
                                   zone_node_type,
                                   0 expl_level,
                                   timebased_dtl_ind
                              from rpm_promo_item_loc_expl expl,
                                   rpm_item_modification_gtt gtt
                             where gtt.old_regular_zone_group      != gtt.new_regular_zone_group
                               and expl.dept                        = gtt.old_dept
                               and NVL(expl.item_parent, expl.item) = gtt.item_parent
                               and expl.cur_hier_level              = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   location,
                                   promo_id,
                                   promo_display_id,
                                   promo_secondary_ind,
                                   promo_comp_id,
                                   comp_display_id,
                                   type,
                                   customer_type,
                                   detail_secondary_ind,
                                   detail_start_date,
                                   detail_end_date,
                                   detail_apply_to_code,
                                   detail_change_type,
                                   detail_change_amount,
                                   detail_change_currency,
                                   detail_change_percent,
                                   detail_change_selling_uom,
                                   detail_price_guide_id,
                                   exception_parent_id,
                                   promo_dtl_id,
                                   zone_node_type,
                                   1 expl_level,
                                   timebased_dtl_ind
                              from rpm_item_modification_gtt gtt,
                                   rpm_promo_item_loc_expl expl,
                                   item_master im
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and expl.dept                   = gtt.old_dept
                               and expl.item                   = gtt.item_parent
                               and expl.cur_hier_level         = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                               and im.item_parent              = expl.item
                               and im.diff_1                   = expl.diff_id
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   location,
                                   promo_id,
                                   promo_display_id,
                                   promo_secondary_ind,
                                   promo_comp_id,
                                   comp_display_id,
                                   type,
                                   customer_type,
                                   detail_secondary_ind,
                                   detail_start_date,
                                   detail_end_date,
                                   detail_apply_to_code,
                                   detail_change_type,
                                   detail_change_amount,
                                   detail_change_currency,
                                   detail_change_percent,
                                   detail_change_selling_uom,
                                   detail_price_guide_id,
                                   exception_parent_id,
                                   promo_dtl_id,
                                   zone_node_type,
                                   2 expl_level,
                                   timebased_dtl_ind
                              from rpm_item_modification_gtt gtt,
                                   rpm_promo_item_loc_expl expl,
                                   item_master im
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and expl.dept                   = gtt.old_dept
                               and expl.item                   = gtt.item_parent
                               and expl.cur_hier_level         = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                               and im.item_parent              = expl.item
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            select expl.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   rzl.location,
                                   promo_id,
                                   promo_display_id,
                                   promo_secondary_ind,
                                   promo_comp_id,
                                   comp_display_id,
                                   type,
                                   customer_type,
                                   detail_secondary_ind,
                                   detail_start_date,
                                   detail_end_date,
                                   detail_apply_to_code,
                                   detail_change_type,
                                   detail_change_amount,
                                   detail_change_currency,
                                   detail_change_percent,
                                   detail_change_selling_uom,
                                   detail_price_guide_id,
                                   exception_parent_id,
                                   promo_dtl_id,
                                   rzl.loc_type zone_node_type,
                                   1 expl_level,
                                   timebased_dtl_ind
                              from rpm_item_modification_gtt gtt,
                                   rpm_promo_item_loc_expl expl,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group      != gtt.new_regular_zone_group
                               and expl.dept                        = gtt.old_dept
                               and NVL(expl.item_parent, expl.item) = gtt.item_parent
                               and expl.cur_hier_level              = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                               and rzl.zone_id                      = expl.location
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   rzl.location,
                                   promo_id,
                                   promo_display_id,
                                   promo_secondary_ind,
                                   promo_comp_id,
                                   comp_display_id,
                                   type,
                                   customer_type,
                                   detail_secondary_ind,
                                   detail_start_date,
                                   detail_end_date,
                                   detail_apply_to_code,
                                   detail_change_type,
                                   detail_change_amount,
                                   detail_change_currency,
                                   detail_change_percent,
                                   detail_change_selling_uom,
                                   detail_price_guide_id,
                                   exception_parent_id,
                                   promo_dtl_id,
                                   rzl.loc_type zone_node_type,
                                   2 expl_level,
                                   timebased_dtl_ind
                              from rpm_item_modification_gtt gtt,
                                   rpm_promo_item_loc_expl expl,
                                   item_master im,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and expl.dept                   = gtt.old_dept
                               and expl.item                   = gtt.item_parent
                               and expl.cur_hier_level         = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                               and im.item_parent              = expl.item
                               and im.diff_1                   = expl.diff_id
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and rzl.zone_id                 = expl.location
                            union all
                            select im.item,
                                   gtt.new_dept,
                                   gtt.new_class,
                                   gtt.new_subclass,
                                   rzl.location,
                                   promo_id,
                                   promo_display_id,
                                   promo_secondary_ind,
                                   promo_comp_id,
                                   comp_display_id,
                                   type,
                                   customer_type,
                                   detail_secondary_ind,
                                   detail_start_date,
                                   detail_end_date,
                                   detail_apply_to_code,
                                   detail_change_type,
                                   detail_change_amount,
                                   detail_change_currency,
                                   detail_change_percent,
                                   detail_change_selling_uom,
                                   detail_price_guide_id,
                                   exception_parent_id,
                                   promo_dtl_id,
                                   rzl.loc_type zone_node_type,
                                   3 expl_level,
                                   timebased_dtl_ind
                              from rpm_item_modification_gtt gtt,
                                   rpm_promo_item_loc_expl expl,
                                   item_master im,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and expl.dept                   = gtt.old_dept
                               and expl.item                   = gtt.item_parent
                               and expl.cur_hier_level         = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                               and im.item_parent              = expl.item
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and rzl.zone_id                 = expl.location) t) s
             where s.rank = 1;

         forall i IN 1 .. L_expl_rowid_char.COUNT
            delete from rpm_promo_item_loc_expl
            where rowid = L_expl_rowid_char(i);

      end if;

      if L_csp_rowid_char is NOT NULL and
         L_csp_rowid_char.COUNT > 0 then

         insert into rpm_cust_segment_promo_fr(cust_segment_promo_id,
                                               item,
                                               zone_node_type,
                                               location,
                                               action_date,
                                               customer_type,
                                               dept,
                                               promo_retail,
                                               promo_retail_currency,
                                               promo_uom,
                                               complex_promo_ind)
            select RPM_CUST_SEG_PROMO_SEQ.NEXTVAL,
                   item,
                   zone_node_type,
                   location,
                   action_date,
                   customer_type,
                   new_dept,
                   promo_retail,
                   promo_retail_currency,
                   promo_uom,
                   complex_promo_ind
              from (select item,
                           zone_node_type,
                           location,
                           action_date,
                           customer_type,
                           new_dept,
                           promo_retail,
                           promo_retail_currency,
                           promo_uom,
                           complex_promo_ind,
                           ROW_NUMBER() OVER (PARTITION BY t.item,
                                                           t.location,
                                                           t.customer_type,
                                                           t.action_date
                                                  ORDER BY csp_level) rank
                      from (select csp.item,
                                   zone_node_type,
                                   location,
                                   action_date,
                                   customer_type,
                                   gtt.new_dept,
                                   promo_retail,
                                   promo_retail_currency,
                                   promo_uom,
                                   complex_promo_ind,
                                   0 csp_level
                              from rpm_cust_segment_promo_fr csp,
                                   rpm_item_modification_gtt gtt
                             where gtt.old_regular_zone_group    != gtt.new_regular_zone_group
                               and csp.dept                       = gtt.old_dept
                               and NVL(csp.item_parent, csp.item) = gtt.item_parent
                               and csp.cur_hier_level             = RPM_CONSTANTS.FR_HIER_ITEM_LOC
                            union all
                            select im.item,
                                   zone_node_type,
                                   location,
                                   action_date,
                                   customer_type,
                                   gtt.new_dept,
                                   promo_retail,
                                   promo_retail_currency,
                                   promo_uom,
                                   complex_promo_ind,
                                   1 csp_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_cust_segment_promo_fr csp,
                                   item_master im
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and csp.dept                    = gtt.old_dept
                               and csp.item                    = gtt.item_parent
                               and csp.cur_hier_level          = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                               and im.item_parent              = csp.item
                               and im.diff_1                   = csp.diff_id
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            select im.item,
                                   zone_node_type,
                                   location,
                                   action_date,
                                   customer_type,
                                   gtt.new_dept,
                                   promo_retail,
                                   promo_retail_currency,
                                   promo_uom,
                                   complex_promo_ind,
                                   2 csp_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_cust_segment_promo_fr csp,
                                   item_master im
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and csp.dept                    = gtt.old_dept
                               and csp.item                    = gtt.item_parent
                               and csp.cur_hier_level          = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                               and im.item_parent              = csp.item
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                            union all
                            select csp.item,
                                   rzl.loc_type zone_node_type,
                                   rzl.location,
                                   action_date,
                                   customer_type,
                                   gtt.new_dept,
                                   promo_retail,
                                   promo_retail_currency,
                                   promo_uom,
                                   complex_promo_ind,
                                   1 csp_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_cust_segment_promo_fr csp,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group    != gtt.new_regular_zone_group
                               and csp.dept                       = gtt.old_dept
                               and NVL(csp.item_parent, csp.item) = gtt.item_parent
                               and csp.cur_hier_level             = RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                               and rzl.zone_id                    = csp.location
                            union all
                            select im.item,
                                   rzl.loc_type zone_node_type,
                                   rzl.location,
                                   action_date,
                                   customer_type,
                                   gtt.new_dept,
                                   promo_retail,
                                   promo_retail_currency,
                                   promo_uom,
                                   complex_promo_ind,
                                   2 csp_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_cust_segment_promo_fr csp,
                                   item_master im,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and csp.dept                    = gtt.old_dept
                               and csp.item                    = gtt.item_parent
                               and csp.cur_hier_level          = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                               and im.item_parent              = csp.item
                               and im.diff_1                   = csp.diff_id
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and rzl.zone_id                 = csp.location
                            union all
                            select im.item,
                                   rzl.loc_type zone_node_type,
                                   rzl.location,
                                   action_date,
                                   customer_type,
                                   gtt.new_dept,
                                   promo_retail,
                                   promo_retail_currency,
                                   promo_uom,
                                   complex_promo_ind,
                                   3 csp_level
                              from rpm_item_modification_gtt gtt,
                                   rpm_cust_segment_promo_fr csp,
                                   item_master im,
                                   rpm_zone_location rzl
                             where gtt.old_regular_zone_group != gtt.new_regular_zone_group
                               and csp.dept                    = gtt.old_dept
                               and csp.item                    = gtt.item_parent
                               and csp.cur_hier_level          = RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                               and im.item_parent              = csp.item
                               and im.status                   = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                               and im.sellable_ind             = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                               and rzl.zone_id                 = csp.location) t) s
             where s.rank = 1;

         forall i IN 1 .. L_csp_rowid_char.COUNT
            delete from rpm_cust_segment_promo_fr
             where rowid = L_csp_rowid_char(i);

      end if;

      if RPM_GENERATE_ROLLUP_FR_SQL.GENERATE_ROLLUP_FR_BY_ITEMS(O_error_msg,
                                                                L_items_to_rollup) = 0 then
         O_return_code := 0;
         return;
      end if;

      if RPM_ROLLUP.ROLL_FUTURE_RETAIL_BY_ITEMS(O_error_msg,
                                                L_items_to_rollup) = 0 then
         O_return_code := 0;
         return;
      end if;

   end if;

   update (select ril.item,
                  ril.loc,
                  ril.dept old_dept,
                  rim.dept new_dept
             from rpm_item_modification rim,
                  rpm_item_loc ril,
                  item_master im
            where ril.item      = rim.item
              and im.item       = rim.item
              and ril.dept     != rim.dept
              and im.item_level = im.tran_level) upd
      set upd.old_dept = upd.new_dept;

   -- Clean up Zone Future Retail and RPM_ITEM_ZONE_PRICE if the new Merch Hierarchy has
   -- different Primary Zone from the Old Merch Hierarchy

   open C_OLD_ZFR;
   fetch C_OLD_ZFR BULK COLLECT into L_zfr_rowid_char;
   close C_OLD_ZFR;

   forall i IN 1 .. L_zfr_rowid_char.COUNT
      delete from rpm_zone_future_retail
       where rowid = L_zfr_rowid_char(i);

   open  C_OLD_IZP;
   fetch C_OLD_IZP BULK COLLECT into L_izp_rowid_char;
   close C_OLD_IZP;

   forall i IN 1 .. L_izp_rowid_char.COUNT
      delete from rpm_item_zone_price
       where rowid = L_izp_rowid_char(i);

   -- Clean up RPM_ITEM_MODIFICATION

   forall i IN 1 .. L_im_item.COUNT
      delete from rpm_item_modification
       where item = L_im_item(i);

   return;

EXCEPTION

   when OTHERS then
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        L_program,
                                        TO_CHAR(SQLCODE));
      O_return_code := 0;

      return;

END RECLASS_FUTURE_RETAIL;
--------------------------------------------------------------------------------
END RPM_ITEM_RECLASS_SQL;
/

