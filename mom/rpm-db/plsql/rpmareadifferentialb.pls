CREATE OR REPLACE PACKAGE BODY RPM_AREA_DIFFERENTIAL_SQL AS

FUNCTION BUILD_ID_FILTER(I_ids           IN    OBJ_NUMERIC_ID_TABLE,
                         I_field_name    IN    STRING)
RETURN VARCHAR2;

PROCEDURE SEARCH(O_return_code		OUT NUMBER,
                 O_error_msg		OUT VARCHAR2,
                 O_ad_tbl		OUT OBJ_AREA_DIFF_TBL,
                 O_ad_count		OUT NUMBER,
                 I_ad_strategy_ids	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_depts		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_classes		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_subclasses	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_zone_groups	IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_zones		IN  OBJ_NUMERIC_ID_TABLE,
                 I_ad_price_guides	IN  OBJ_NUMERIC_ID_TABLE)

IS

  cursor C_WH (L_strategy_id NUMBER) is
    select new OBJ_LISTABLE_REC(
    	w.wh,
  	w.wh,
  	w.wh_name)
    from rpm_area_diff_wh radw,wh w
    where radw.area_diff_id = L_strategy_id
  	and w.wh = radw.wh;


  cursor C_ZLOC (L_zone_id NUMBER) is
    select new OBJ_ZONE_LOC_REC(
  	zone_location_id,
  	loc.loc_name,
  	loc.loc_id,
  	loc.loc_type,
  	loc.def_currency_code,
        loc.store_type,
        null)
    from rpm_zone_location rzl,
  	(select store loc_id,
  		0 loc_type,
  		store_name loc_name,
  		currency_code def_currency_code,
      store_type store_type 
  	 from store
  	 union
  	 select wh loc_id,
	   	2 loc_type,
	   	wh_name loc_name,
	   	currency_code def_currency_code,
                null store_type
  	 from wh) loc
    where rzl.zone_id = L_zone_id
  	and loc.loc_id = rzl.location;

  cursor C_PG (L_price_guide_id NUMBER) is
    select new OBJ_LISTABLE4_REC(
  	d.dept,
  	d.dept,
  	d.dept_name)
    from rpm_price_guide_dept rpgd,deps d
    where rpgd.price_guide_id = L_price_guide_id
  	and d.dept = rpgd.dept;


  cursor C_SAD (L_primary_area_diff_id NUMBER) is
    select new OBJ_AREA_DIFF_SEC_REC(
        rad.area_diff_id,
        rad.auto_approve_ind,
        rad.percent_apply_type,
        rad.percent,
        rad.comp_store,
        cst.store_name,
        c.competitor,
	c.comp_name,
        radc.compete_type,
        radc.area_diff_comp_id,
        rzg.zone_group_id,
        rzg.zone_group_display_id,
        rzg.name,
        rz.zone_id,
        rz.zone_display_id,
        rz.name,
        rz.currency_code,
        rz.base_ind,
        radc.from_percent,
        radc.to_percent,
        radc.percent,
        rad.price_guide_id,
        pg.name,
        pg.description,
        pg.currency_code,
        pg.corp_ind,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
      )
    from rpm_area_diff rad,rpm_zone rz,rpm_zone_group rzg,
	 rpm_price_guide pg,comp_store cst,rpm_area_diff_comp radc,
	 competitor c
    where rad.area_diff_prim_id = L_primary_area_diff_id
          and rz.zone_id = rad.zone_hier_id
          and rzg.zone_group_id = rz.zone_group_id
          and rad.price_guide_id = pg.price_guide_id(+)
          and rad.comp_store = cst.store(+)
	  and rad.area_diff_id = radc.area_diff_id(+)
	  and cst.competitor = c.competitor(+);


  cursor C_REFCOMP (L_primary_area_diff_id NUMBER) is
    select new OBJ_REF_COMP_REC(
        radrc.comp_index,
        radrc.comp_store,
        cst.store_name,
        cst.competitor,
        c.comp_name,
        radrc.comp_type,
        radrc.comp_percent
      )
    from rpm_area_diff rad,rpm_area_diff_ref_comp radrc,
         comp_store cst,competitor c
    where rad.area_diff_prim_id = L_primary_area_diff_id
          and radrc.area_diff_id = rad.area_diff_id
          and cst.store = rad.comp_store
          and c.competitor = cst.competitor;




  L_select		VARCHAR2(4000) := NULL;
  L_from		VARCHAR2(4000) := NULL;
  L_where		VARCHAR2(8000) := NULL;
  L_query		VARCHAR2(16000) := NULL;
  L_iter		NUMBER;
  L_iter_sec		NUMBER;
  L_zone_locations	OBJ_ZONE_LOC_TBL;
  L_pg_depts		OBJ_LISTABLE4_TBL;
  L_ad_rec		OBJ_AREA_DIFF_REC;
  L_warehouses		OBJ_LISTABLE_TBL;
  L_sad_tbl		OBJ_AREA_DIFF_SEC_TBL;
  L_sad_rec		OBJ_AREA_DIFF_SEC_REC;
  L_ref_comps		OBJ_REF_COMP_TBL;

  TYPE priceStrategyCursorType IS REF CURSOR;
  il_lc			priceStrategyCursorType;


BEGIN
  L_select := 'SELECT NEW OBJ_AREA_DIFF_REC('			||
		'radp.area_diff_prim_id,'			||	-- strategy id
		'radp.dept,'					||	-- dept
		'dcs.dept_name,'					||	-- dept name
		'dcs.markup_calc_type,'				||	-- markup calculation basis
		'radp.class,'					||	-- class
		'dcs.class_name,'				||	-- class name
		'radp.subclass,'				||	-- subclass
		'dcs.sub_name,'					||	-- subclass name
		'rzg.zone_group_id,'				||	-- zone group id
		'rzg.zone_group_display_id,'			||	-- zone group display id
		'rzg.name,'					||	-- zone group name
		'rz.zone_id,'					||	-- zone id
		'rz.zone_display_id,'				||	-- zone display id
		'rz.name,'					||	-- zone name
		'rz.currency_code,'				||	-- zone currency
		'rz.base_ind,'					||	-- zone base indicator
		'NULL,'						||	-- strategy warehouses
		'NULL,'						||	-- zone locations
		'NULL'						||	-- secondary area diffs
		')';

  L_from := ' FROM '						||
		'rpm_area_diff_prim radp,'			||
		'(select radp.area_diff_prim_id id,d.dept,d.dept_name,d.markup_calc_type,null class,null class_name,null subclass,null sub_name ' ||
		' from rpm_area_diff_prim radp,deps d '			||
		' where radp.merch_type = 3 and radp.dept = d.dept '	||
		' union all '					||
		' select radp.area_diff_prim_id id,d.dept,d.dept_name,d.markup_calc_type,cl.class,cl.class_name,null subclass,null sub_name ' ||
		' from rpm_area_diff_prim radp,deps d,class cl '	||
		' where radp.merch_type = 2 and radp.dept = d.dept and radp.dept = cl.dept and radp.class = cl.class ' ||
		' union all '					||
		' select radp.area_diff_prim_id id,d.dept,d.dept_name,d.markup_calc_type,cl.class,cl.class_name,scl.subclass,scl.sub_name ' ||
		' from rpm_area_diff_prim radp,deps d,class cl,subclass scl '	||
		' where radp.merch_type = 1 and radp.dept = d.dept and radp.dept = cl.dept and radp.class = cl.class and scl.dept = radp.dept and scl.class = radp.class and scl.subclass = radp.subclass) dcs,' ||
		'rpm_zone rz,'					||
		'rpm_zone_group rzg';

  L_where := ' WHERE '						||
		'radp.area_diff_prim_id = dcs.id '		||
  		'AND rz.zone_id = radp.zone_hier_id '		||
  		'AND rzg.zone_group_id = rz.zone_group_id ';


   --
   --	Strategy id is specified
   --
   IF I_ad_strategy_ids.count > 0 THEN
     L_where := L_where || BUILD_ID_FILTER(I_ad_strategy_ids, 'radp.area_diff_prim_id');
  END IF;
  --
  --	Depts/Classes/Subclasses
  --
  IF I_ad_subclasses.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ad_subclasses, 'radp.subclass') ||
			  BUILD_ID_FILTER(I_ad_classes, 'radp.class') ||
			  BUILD_ID_FILTER(I_ad_depts, 'radp.dept');
  ELSIF I_ad_classes.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ad_classes, 'radp.class') ||
			  BUILD_ID_FILTER(I_ad_depts, 'radp.dept');
  ELSIF I_ad_depts.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ad_depts, 'radp.dept');
  END IF;



  --
  --	Zone criteria is specified
  --
  IF I_ad_zones.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ad_zones, 'rz.zone_id');
  --
  --	Zone Group criteria is specified
  --
  ELSIF I_ad_zone_groups.count > 0 THEN
    L_where := L_where || BUILD_ID_FILTER(I_ad_zone_groups, 'rzg.zone_group_id');
  END IF;


  --
  --	Price Guides
  --
  IF I_ad_price_guides.count > 0 THEN
    L_from := L_from || ',(select rad.area_diff_prim_id prim_id,rad.price_guide_id from rpm_area_diff rad,rpm_price_guide pg where rad.price_guide_id = pg.price_guide_id(+)) radpg';
    L_where := L_where || BUILD_ID_FILTER(I_ad_price_guides, 'radpg.price_guide_id') ||
               ' AND radp.area_diff_prim_id = radpg.prim_id ';
  END IF;


  L_query := L_select || ' ' || L_from || ' ' || L_where;

  OPEN il_lc FOR L_query;
  FETCH il_lc BULK COLLECT INTO O_ad_tbl;
  CLOSE il_lc;

  FOR L_iter in 1..O_ad_tbl.count LOOP

    L_ad_rec := O_ad_tbl(L_iter);
    L_warehouses := OBJ_LISTABLE_TBL();

    OPEN C_WH (L_ad_rec.strategy_id);
    FETCH C_WH BULK COLLECT INTO L_warehouses;
    CLOSE C_WH;

    O_ad_tbl(L_iter).warehouses := L_warehouses;
    L_zone_locations := OBJ_ZONE_LOC_TBL();

    OPEN C_ZLOC (L_ad_rec.zone_id);
    FETCH C_ZLOC BULK COLLECT INTO L_zone_locations;
    CLOSE C_ZLOC;

    O_ad_tbl(L_iter).zone_locations := L_zone_locations;

    OPEN C_SAD (L_ad_rec.strategy_id);
    FETCH C_SAD BULK COLLECT INTO L_sad_tbl;
    CLOSE C_SAD;

    FOR L_iter_sec in 1..L_sad_tbl.count LOOP

      L_sad_rec := L_sad_tbl(L_iter_sec);
      L_pg_depts := OBJ_LISTABLE4_TBL();

      OPEN C_PG (L_sad_rec.price_guide_id);
      FETCH C_PG BULK COLLECT INTO L_pg_depts;
      CLOSE C_PG;

      L_sad_tbl(L_iter_sec).price_guide_depts := L_pg_depts;
      L_warehouses := OBJ_LISTABLE_TBL();

      OPEN C_WH (L_sad_rec.secondary_id);
      FETCH C_WH BULK COLLECT INTO L_warehouses;
      CLOSE C_WH;

      L_sad_tbl(L_iter_sec).warehouses := L_warehouses;
      L_zone_locations := OBJ_ZONE_LOC_TBL();

      OPEN C_ZLOC (L_sad_rec.zone_id);
      FETCH C_ZLOC BULK COLLECT INTO L_zone_locations;
      CLOSE C_ZLOC;

      L_sad_tbl(L_iter_sec).zone_locations := L_zone_locations;
      L_ref_comps := OBJ_REF_COMP_TBL();

      OPEN C_REFCOMP (L_ad_rec.strategy_id);
      FETCH C_REFCOMP BULK COLLECT INTO L_ref_comps;
      CLOSE C_REFCOMP;

      L_sad_tbl(L_iter_sec).reference_competitors := L_ref_comps;

    END LOOP;

    O_ad_tbl(L_iter).secondary_area_diffs := L_sad_tbl;

  END LOOP;

  O_ad_count := O_ad_tbl.COUNT;
  O_return_code := 1;

EXCEPTION

  WHEN others THEN
      O_error_msg := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                        SQLERRM,
                                        'RPM_AREA_DIFFERENTIAL_SQL.SEARCH',
                                        to_char(SQLCODE));
      O_return_code := 0;

END SEARCH;

-----------------------------------------------------------------------------------------

FUNCTION BUILD_ID_FILTER(I_ids           IN  OBJ_NUMERIC_ID_TABLE,
                         I_field_name    IN  STRING)

RETURN VARCHAR2 IS

  L_filter         VARCHAR2(8000);
  L_i              NUMBER;

BEGIN
  L_filter := ' AND ( ';
  FOR L_i in 1..I_ids.count LOOP
  IF L_i > 1 THEN
    L_filter := L_filter || ' OR ';
  END IF;
  L_filter := L_filter || ' ' || I_field_name || ' = ' ||  I_ids(L_i) || ' ';
  END LOOP;
  L_filter := L_filter || ' ) ';

  return L_filter;

EXCEPTION
  WHEN others THEN

  return '';

END BUILD_ID_FILTER;

-----------------------------------------------------------------------------------------

END RPM_AREA_DIFFERENTIAL_SQL;
/

