CREATE OR REPLACE PACKAGE RPM_PRICE_INQUIRY_SQL AS
--------------------------------------------------------
FUNCTION GET_PRICE_INQ_VO (O_error_msg          OUT VARCHAR2,
                           O_prices             OUT OBJ_PRC_INQ_TBL,
                           I_search_criteria IN     OBJ_PRICE_INQ_SEARCH_TBL) RETURN NUMBER;

--------------------------------------------------------
FUNCTION GET_CUST_SEGMENT_PROMOS(O_error_msg           OUT VARCHAR2,
                                 O_pc_tbl              OUT OBJ_CUST_SEG_PROMO_TBL,
                                 I_search_criteria  IN     OBJ_CUST_SEG_PROMO_REQ_TBL)
RETURN NUMBER;
--------------------------------------------------------                                   
END RPM_PRICE_INQUIRY_SQL;
/