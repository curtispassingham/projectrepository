CREATE OR REPLACE PACKAGE RPM_PURGE_FUTURE_RETAIL_SQL AS
-----------------------------------------------------------------------------
FUNCTION PURGE(O_cc_error_tbl    OUT CONFLICT_CHECK_ERROR_TBL)

RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION THREAD_PURGE_DATA(O_error_msg            OUT VARCHAR2,
                           O_max_thread_number    OUT NUMBER)

RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION PROCESS_PURGE_THREAD(O_error_msg        OUT VARCHAR2,
                              I_thread_number IN     NUMBER)

RETURN NUMBER;
-----------------------------------------------------------------------------
FUNCTION CLEAN_UP(O_error_msg    OUT VARCHAR2)

RETURN NUMBER;
-----------------------------------------------------------------------------
END RPM_PURGE_FUTURE_RETAIL_SQL;
/