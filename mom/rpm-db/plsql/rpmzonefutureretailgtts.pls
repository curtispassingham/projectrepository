CREATE OR REPLACE PACKAGE RPM_ZONE_FUTURE_RETAIL_GTT_SQL AS

   --------------------------------------------------------

   FUNCTION GET_CHILDREN(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                         O_child_event_ids     OUT OBJ_NUM_NUM_DATE_TBL,
                         I_price_change_ids IN     OBJ_NUMERIC_ID_TABLE)
   RETURN NUMBER;

   --------------------------------------------------------

   FUNCTION REMOVE_TIMELINES(O_cc_error_tbl        OUT CONFLICT_CHECK_ERROR_TBL,
                             I_price_change_ids IN     OBJ_NUM_NUM_DATE_TBL)
   RETURN NUMBER;

   --------------------------------------------------------
   FUNCTION GET_AFFECTED_PARENT(O_cc_error_tbl            OUT CONFLICT_CHECK_ERROR_TBL,
                                O_affected_parent_ids     OUT OBJ_NUM_NUM_DATE_TBL,
                                I_price_change_ids     IN     OBJ_NUMERIC_ID_TABLE)
   RETURN NUMBER;

   --------------------------------------------------------
   FUNCTION MERGE_PRICE_CHANGE(O_cc_error_tbl         OUT CONFLICT_CHECK_ERROR_TBL,
                               I_price_change_ids  IN     OBJ_NUMERIC_ID_TABLE)
   RETURN NUMBER;

   --------------------------------------------------------
   FUNCTION REMOVE_EVENT(O_cc_error_tbl      OUT CONFLICT_CHECK_ERROR_TBL,
                         I_price_change_ids IN   OBJ_NUM_NUM_DATE_TBL)
   RETURN NUMBER;

END RPM_ZONE_FUTURE_RETAIL_GTT_SQL;
/

