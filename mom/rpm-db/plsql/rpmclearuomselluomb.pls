CREATE OR REPLACE PACKAGE BODY RPM_CC_CLEARUOM_SELLUOM AS
--------------------------------------------------------
FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_program VARCHAR2(35) := 'RPM_CC_CLEARUOM_SELLUOM.VALIDATE';

   L_error_rec CONFLICT_CHECK_ERROR_REC := NULL;
   L_error_tbl CONFLICT_CHECK_ERROR_TBL := CONFLICT_CHECK_ERROR_TBL();

   cursor C_CHECK is
      select price_event_id,
             future_retail_id
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id NOT IN (select ccet.price_event_id
                                          from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet)
         and selling_uom        != clear_uom;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE) then
      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999, NULL, NULL, NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      for rec in C_CHECK loop
         L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                 rec.future_retail_id,
                                                 RPM_CONSTANTS.CONFLICT_ERROR,
                                                 'future_retail_fixed_price_clearance_uom_not_equal_selling_uom');
         if IO_error_table is NULL then
            IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
         else
            IO_error_table.EXTEND;
            IO_error_table(IO_error_table.COUNT) := L_error_rec;
         end if;
      end loop;
   end if;

   return 1;

EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/

