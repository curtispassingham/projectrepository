CREATE OR REPLACE PACKAGE BODY RPM_CC_PROM_LT_CLEAR_REG AS
--------------------------------------------------------

FUNCTION VALIDATE(IO_error_table     IN OUT CONFLICT_CHECK_ERROR_TBL,
                  I_price_event_type IN     VARCHAR2)
RETURN NUMBER IS

   L_error_key  VARCHAR2(255) := NULL;
   L_error_type VARCHAR2(255) := NULL;
   L_program    VARCHAR2(61)  := 'RPM_CC_PROM_LT_CLEAR_REG.VALIDATE';

   L_future_retail_id RPM_FUTURE_RETAIL_GTT.FUTURE_RETAIL_ID%TYPE := NULL;
   L_error_rec        CONFLICT_CHECK_ERROR_REC                    := NULL;
   L_error_tbl        CONFLICT_CHECK_ERROR_TBL                    := CONFLICT_CHECK_ERROR_TBL();

   L_gtt_count NUMBER := NULL;

   L_simple_promo_retail RPM_FUTURE_RETAIL.SIMPLE_PROMO_RETAIL%TYPE := NULL;

   cursor C_CHECK is
      select price_event_id,
             item,
             location,
             action_date,
             future_retail_id,
             selling_retail,
             selling_uom,
             clear_retail,
             clear_uom,
             simple_promo_retail,
             simple_promo_uom,
             'UOM' type,
             NULL cs_promo_fr_id     
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id NOT IN (select /*+ CARDINALITY (ccet, 10) */ ccet.price_event_id
                                          from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet)
         and selling_uom != simple_promo_uom
      union all
      select price_event_id,
             item,
             location,
             action_date,
             future_retail_id,
             selling_retail,
             selling_uom,
             clear_retail,
             clear_uom,
             simple_promo_retail,
             simple_promo_uom,
             'CONFLICT' type,
             NULL cs_promo_fr_id    
        from rpm_future_retail_gtt gtt
       where gtt.price_event_id NOT IN (select /*+ CARDINALITY (ccet, 10) */ ccet.price_event_id
                                          from table(cast(L_error_tbl as conflict_check_error_tbl)) ccet)
      and ((    selling_uom     = simple_promo_uom
            and selling_retail  < simple_promo_retail)
           or (    clear_uom    = simple_promo_uom
               and clear_retail < simple_promo_retail))
      union all
      select cspfg.price_event_id,
             fr.item,
             fr.location,
             fr.action_date,
             fr.future_retail_id,
             fr.selling_retail,
             fr.selling_uom,
             fr.clear_retail,
             fr.clear_uom,
             cspfg.promo_retail simple_promo_retail,
             cspfg.promo_uom simple_promo_uom,
             'UOM' type,
             cspfg.cust_segment_promo_id cs_promo_fr_id     
        from rpm_cust_segment_promo_fr_gtt cspfg,
             rpm_future_retail_gtt fr
       where cspfg.price_event_id    NOT IN (select /*+ CARDINALITY (ccet, 10) */ ccet.price_event_id
                                            from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and cspfg.price_event_id    = fr.price_event_id
         and cspfg.dept              = fr.dept
         and cspfg.item              = fr.item
         and NVL(cspfg.diff_id,-999) = NVL(fr.diff_id,-999)
         and cspfg.location          = fr.location
         and cspfg.zone_node_type    = fr.zone_node_type
         and fr.action_date          = cspfg.action_date
         and fr.selling_uom         != cspfg.promo_uom
       union all
      select cspfg.price_event_id,
             fr.item,
             fr.location,
             fr.action_date,
             fr.future_retail_id,
             fr.selling_retail,
             fr.selling_uom,
             fr.clear_retail,
             fr.clear_uom,
             cspfg.promo_retail simple_promo_retail,
             cspfg.promo_uom simple_promo_uom,
             'CONFLICT' type,
             cspfg.cust_segment_promo_id cs_promo_fr_id     
        from rpm_cust_segment_promo_fr_gtt cspfg,
             rpm_future_retail_gtt fr
       where cspfg.price_event_id     NOT IN (select /*+ CARDINALITY (ccet, 10) */ ccet.price_event_id
                                            from table(cast(L_error_tbl as CONFLICT_CHECK_ERROR_TBL)) ccet)
         and cspfg.price_event_id     = fr.price_event_id
         and cspfg.dept               = fr.dept
         and cspfg.item               = fr.item
         and NVL(cspfg.diff_id,-999)  = NVL(fr.diff_id,-999)
         and cspfg.location           = fr.location
         and cspfg.zone_node_type     = fr.zone_node_type
         and fr.action_date           = cspfg.action_date
         and ((    fr.selling_uom     = cspfg.promo_uom
               and fr.selling_retail  < cspfg.promo_retail)
              or (    fr.clear_uom    = cspfg.promo_uom
                  and fr.clear_retail < cspfg.promo_retail))
      order by type;

BEGIN

   if I_price_event_type IN (RPM_CONSTANTS.PE_TYPE_LOCATION_MOVE,
                             RPM_CONSTANTS.PE_TYPE_PRICE_CHANGE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE,
                             RPM_CONSTANTS.PE_TYPE_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_NEW_CLEARANCE_RESET,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_SIMPLE_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_THRESHOLD_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_PROMOTION,
                             RPM_CONSTANTS.PE_TYPE_COMPLEX_UPDATE,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_SIMPLE_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_THRESH_UPD,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_PROMO,
                             RPM_CONSTANTS.PE_TYPE_CUST_SEG_COMPLEX_UPD) then
      if IO_error_table is NOT NULL and
         IO_error_table.COUNT > 0 then
         L_error_tbl := IO_error_table;
      else
         L_error_rec := CONFLICT_CHECK_ERROR_REC(-99999, NULL, NULL, NULL);
         L_error_tbl := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
      end if;

      for rec IN C_CHECK loop
         if rec.type = 'CONFLICT' then
            L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                    rec.future_retail_id,
                                                    RPM_CONSTANTS.CONFLICT_ERROR,
                                                    'event_must_cause_price_to_decrease',
                                                    rec.cs_promo_fr_id);
            if IO_error_table is NULL then
               IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
            else
               IO_error_table.EXTEND;
               IO_error_table(IO_error_table.COUNT) := L_error_rec;
            end if;
         elsif rec.type = 'UOM' then
            if UOM_SQL.CONVERT(L_error_key,
                               L_simple_promo_retail,
                               rec.simple_promo_uom,
                               rec.simple_promo_retail,
                               rec.selling_uom,
                               rec.item,
                               NULL,                      --supplier
                               NULL) = FALSE then         --origin country
               L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                                       NULL,
                                                       RPM_CONSTANTS.PLSQL_ERROR,
                                                       L_error_key);
               IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
               return 0;
            end if;

            if rec.selling_retail < L_simple_promo_retail or
               rec.clear_retail < L_simple_promo_retail then

               L_error_rec := CONFLICT_CHECK_ERROR_REC(rec.price_event_id,
                                                       rec.future_retail_id,
                                                       RPM_CONSTANTS.CONFLICT_ERROR,
                                                       'event_must_cause_price_to_decrease',
                                                       rec.cs_promo_fr_id);
               if IO_error_table is NULL then
                  IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);
               else
                  IO_error_table.EXTEND;
                  IO_error_table(IO_error_table.COUNT) := L_error_rec;
               end if;
             
            end if;

         end if;
      end loop;

   end if;

   return 1;   
   
EXCEPTION

   when OTHERS then
      L_error_rec := CONFLICT_CHECK_ERROR_REC(NULL,
                                              NULL,
                                              RPM_CONSTANTS.PLSQL_ERROR,
                                              SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                                                 SQLERRM,
                                                                 L_program,
                                                                 TO_CHAR(SQLCODE)));
      IO_error_table := CONFLICT_CHECK_ERROR_TBL(L_error_rec);

      return 0;

END VALIDATE;
--------------------------------------------------------
END;
/
