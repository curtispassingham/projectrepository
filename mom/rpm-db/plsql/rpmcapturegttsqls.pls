CREATE OR REPLACE PACKAGE RPM_CAPTURE_GTT_SQL AS

--------------------------------------------------------------------------------

FUNCTION INIT_VARS(O_error_message           OUT VARCHAR2,
                   I_user_id              IN     VARCHAR2,
                   I_bulk_cc_pe_id        IN     NUMBER,
                   I_parent_thread_number IN     NUMBER,
                   I_thread_number        IN     NUMBER,
                   I_chunk_number         IN     NUMBER DEFAULT NULL)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION PROCESS_GTT_DATA_CAPTURE(O_error_message     OUT VARCHAR2,
                                  I_step_indicator IN     NUMBER,
                                  I_rfr_ind        IN     NUMBER,
                                  I_rpile_ind      IN     NUMBER,
                                  I_cspfr_ind      IN     NUMBER,
                                  I_clr_ind        IN     NUMBER,
                                  I_rfr_ile_ind    IN     NUMBER)
RETURN NUMBER;

--------------------------------------------------------------------------------

FUNCTION PURGE_CAPTURE_GTT_DATA(O_error_message    OUT VARCHAR2)
RETURN NUMBER;

--------------------------------------------------------------------------------

END RPM_CAPTURE_GTT_SQL;
/
