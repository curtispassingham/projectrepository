CREATE OR REPLACE PACKAGE BODY RPM_DATA_VALIDATION_SQL AS

--------------------------------------------------------------------------------
FUNCTION VALIDATE_FR (O_error_message    OUT VARCHAR2,
                      I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                      I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                      I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER;
---------------------------------------------------------------------------------
FUNCTION VALIDATE_PILE (O_error_message    OUT VARCHAR2,
                        I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                        I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                        I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER;
---------------------------------------------------------------------------------
FUNCTION VALIDATE_CSPFR(O_error_message    OUT VARCHAR2,
                        I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                        I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                        I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER;
---------------------------------------------------------------------------------
FUNCTION FIND_DUPLICATE_FR_DATA (O_error_message    OUT VARCHAR2,
                                 I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                                 I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                                 I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)

RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_DATA_VALIDATION_SQL.RUN_DUPLICATE_FR_DATA';

   L_depts OBJ_NUMERIC_ID_TABLE := NULL;

   cursor C_DEPT is
      select dept
        from deps;

BEGIN

   EXECUTE IMMEDIATE 'truncate table rpm_duplicate_fr_data';

   if I_dept is NULL then

      open C_DEPT;
      fetch C_DEPT BULK COLLECT into L_depts;
      close C_DEPT;

      for i IN 1..L_depts.COUNT loop

          if VALIDATE_FR(O_error_message,
                         L_depts(i)) = 0 then
             return 0;
          end if;

          if VALIDATE_PILE(O_error_message,
                           L_depts(i)) = 0 then
             return 0;
          end if;

          if VALIDATE_CSPFR(O_error_message,
                            L_depts(i)) = 0 then
             return 0;
          end if;

      end loop;

   else

      if VALIDATE_FR(O_error_message,
                     I_dept,
                     I_class,
                     I_subclass) = 0 then
         return 0;
      end if;

      if VALIDATE_PILE(O_error_message,
                       I_dept,
                       I_class,
                       I_subclass) = 0 then
         return 0;
      end if;

      if VALIDATE_CSPFR(O_error_message,
                        I_dept,
                        I_class,
                        I_subclass) = 0 then
         return 0;
      end if;

   end if;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END FIND_DUPLICATE_FR_DATA;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_FR (O_error_message    OUT VARCHAR2,
                      I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                      I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                      I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_DATA_VALIDATION_SQL.VALIDATE_FR';

BEGIN

   insert into rpm_duplicate_fr_data (count,
                                      dept,
                                      item,
                                      diff_id,
                                      location,
                                      zone_node_type,
                                      action_date,
                                      tbl_found_in)
   select COUNT(*),
          dept,
          item,
          diff_id,
          location,
          zone_node_type,
          action_date,
          'RPM_FUTURE_RETAIL'
     from rpm_future_retail
    where dept     = I_dept
      and class    = NVL(I_class, class)
      and subclass = NVL(I_subclass, subclass)
    group by dept,
             item,
             diff_id,
             location,
             zone_node_type,
             action_date
   having COUNT(*) > 1;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_FR;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_PILE (O_error_message    OUT VARCHAR2,
                        I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                        I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                        I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_DATA_VALIDATION_SQL.VALIDATE_PILE';

BEGIN

   insert into rpm_duplicate_fr_data (count,
                                      item,
                                      dept,
                                      diff_id,
                                      location,
                                      zone_node_type,
                                      promo_dtl_id,
                                      tbl_found_in)
   select COUNT(*),
          item,
          dept,
          diff_id,
          location,
          zone_node_type,
          promo_dtl_id,
          'RPM_PROMO_ITEM_LOC_EXPL'
     from rpm_promo_item_loc_expl
    where dept     = I_dept
      and class    = NVL(I_class, class)
      and subclass = NVL(I_subclass, subclass)
    group by item,
             dept,
             diff_id,
             location,
             zone_node_type,
             promo_dtl_id
   having COUNT(*) > 1;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_PILE;
--------------------------------------------------------------------------------
FUNCTION VALIDATE_CSPFR(O_error_message    OUT VARCHAR2,
                        I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                        I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                        I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)
RETURN NUMBER IS

   L_program VARCHAR2(40) := 'RPM_DATA_VALIDATION_SQL.VALIDATE_CSPFR';

BEGIN

   insert into rpm_duplicate_fr_data (count,
                                      item,
                                      dept,
                                      diff_id,
                                      location,
                                      zone_node_type,
                                      action_date,
                                      customer_type,
                                      tbl_found_in)
   select COUNT(*),
          rcspf.item,
          rcspf.dept,
          rcspf.diff_id,
          rcspf.location,
          rcspf.zone_node_type,
          rcspf.action_date,
          rcspf.customer_type,
          'RPM_CUST_SEGMENT_PROMO_FR'
     from rpm_cust_segment_promo_fr rcspf,
          item_master im
    where rcspf.item  = im.item
      and rcspf.dept  = I_dept
      and im.class    = NVL(I_class, im.class)
      and im.subclass = NVL(I_subclass, im.subclass)
    group by rcspf.item,
             rcspf.dept,
             rcspf.diff_id,
             rcspf.location,
             rcspf.zone_node_type,
             rcspf.action_date,
             rcspf.customer_type
   having COUNT(*) > 1;

   return 1;

EXCEPTION
   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END VALIDATE_CSPFR;
--------------------------------------------------------------------------------
FUNCTION FIND_MISSING_FR_DATA (O_error_message    OUT VARCHAR2,
                               I_dept          IN     RPM_FUTURE_RETAIL.DEPT%TYPE     DEFAULT NULL,
                               I_class         IN     RPM_FUTURE_RETAIL.CLASS%TYPE    DEFAULT NULL,
                               I_subclass      IN     RPM_FUTURE_RETAIL.SUBCLASS%TYPE DEFAULT NULL)

RETURN NUMBER IS

   L_program VARCHAR2(50) := 'RPM_DATA_VALIDATION_SQL.FIND_MISSING_FR_DATA';

   cursor C_SUBCLASS is
      select dept,
             class,
             subclass
        from subclass
       where dept     = NVL(I_dept, dept)
         and class    = NVL(I_class, class)
         and subclass = NVL(I_subclass, subclass)
       order by dept,
                class,
                subclass;

BEGIN

   execute immediate 'truncate table rpm_missing_timeline';

   for rec IN C_SUBCLASS loop

      -- Check for missing timelines on RPM_FUTURE_RETAIL

      insert into rpm_missing_timeline (item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        zone_node_type,
                                        zone_id,
                                        cur_hier_level,
                                        table_missing_from)
      select distinct r.item,
             r.diff_id,
             NULL,
             r.location,
             r.zone_node_type,
             r.zone_id,
             RPM_CONSTANTS.FR_HIER_DIFF_LOC,
             'RPM_FUTURE_RETAIL'
        from (select distinct t.item,
                     t.diff_id,
                     t.location zone_id,
                     fr.location,
                     t.zone_node_type
                from (select distinct item,
                             diff_id,
                             location,
                             zone_node_type
                        from rpm_future_retail
                       where dept           = rec.dept
                         and class          = rec.class
                         and subclass       = rec.subclass
                         and cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and rownum         > 0) t,
                     rpm_future_retail fr
               where fr.dept           = rec.dept
                 and fr.class          = rec.class
                 and fr.subclass       = rec.subclass
                 and fr.item           = t.item
                 and fr.zone_id        = t.location
                 and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                 and rownum            > 0) r,
             item_master im,
             rpm_item_loc ril
       where im.item_parent = r.item
         and im.diff_1      = r.diff_id
         and ril.dept       = rec.dept
         and ril.item       = im.item
         and ril.loc        = r.location
         and NOT EXISTS (select 1
                           from rpm_future_retail fr
                          where fr.dept           = rec.dept
                            and fr.class          = rec.class
                            and fr.subclass       = rec.subclass
                            and fr.item           = r.item
                            and fr.location       = r.location
                            and fr.diff_id        = r.diff_id
                            and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC);

      insert into rpm_missing_timeline (item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        zone_node_type,
                                        zone_id,
                                        cur_hier_level,
                                        table_missing_from)
      select distinct r.item,
             r.diff_id,
             r.item_parent,
             r.location,
             r.zone_node_type,
             r.zone_id,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC,
             'RPM_FUTURE_RETAIL'
        from (select distinct s.item,
                     s.diff_id,
                     fr.location,
                     fr.zone_node_type,
                     s.item_parent,
                     fr.cur_hier_level,
                     s.zone_id
                from (select t.item,
                             im.item_parent,
                             im.diff_1 diff_id,
                             t.zone_id
                        from (select item,
                                     location zone_id
                                from (select item,
                                             location,
                                             ROW_NUMBER() OVER (PARTITION BY item,
                                                                             location
                                                                    ORDER BY action_date) rnk
                                        from rpm_future_retail
                                       where dept           = rec.dept
                                         and class          = rec.class
                                         and subclass       = rec.subclass
                                         and cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE)
                               where rnk = 1) t,
                             item_master im
                       where im.item        = t.item
                         and im.item_parent is NOT NULL) s,
                     rpm_future_retail fr
               where fr.dept     = rec.dept
                 and fr.class    = rec.class
                 and fr.subclass = rec.subclass
                 and fr.item     = s.item_parent
                 and fr.zone_id  = s.zone_id
                 and (   (    fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                          and fr.diff_id        is NULL)
                      or (    fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                          and fr.diff_id        = s.diff_id))
                 and rownum      > 0) r,
             rpm_item_loc ril
       where ril.dept = rec.dept
         and ril.item = r.item
         and ril.loc  = r.location
         and NOT EXISTS (select 1
                           from rpm_future_retail fr
                          where fr.dept           = rec.dept
                            and fr.class          = rec.class
                            and fr.subclass       = rec.subclass
                            and fr.item           = r.item
                            and fr.location       = r.location
                            and fr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC);

      -- Check for missing timelines on RPM_PROMO_ITEM_LOC_EXPL

      insert into rpm_missing_timeline (item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        zone_node_type,
                                        zone_id,
                                        cur_hier_level,
                                        table_missing_from)
      select distinct r.item,
             r.diff_id,
             NULL,
             r.location,
             r.zone_node_type,
             r.zone_id,
             RPM_CONSTANTS.FR_HIER_DIFF_LOC,
             'RPM_PROMO_ITEM_LOC_EXPL'
        from (select distinct t.item,
                     t.diff_id,
                     t.location zone_id,
                     pile.location,
                     t.zone_node_type
                from (select distinct item,
                             diff_id,
                             location,
                             zone_node_type
                        from rpm_promo_item_loc_expl
                       where dept           = rec.dept
                         and class          = rec.class
                         and subclass       = rec.subclass
                         and cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and rownum         > 0) t,
                     rpm_promo_item_loc_expl pile
               where pile.dept           = rec.dept
                 and pile.class          = rec.class
                 and pile.subclass       = rec.subclass
                 and pile.item           = t.item
                 and pile.zone_id        = t.location
                 and pile.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                 and rownum              > 0) r,
             item_master im,
             rpm_item_loc ril
       where im.item_parent = r.item
         and im.diff_1      = r.diff_id
         and ril.dept       = rec.dept
         and ril.item       = im.item
         and ril.loc        = r.location
         and NOT EXISTS (select 1
                           from rpm_promo_item_loc_expl pile
                          where pile.dept           = rec.dept
                            and pile.class          = rec.class
                            and pile.subclass       = rec.subclass
                            and pile.item           = r.item
                            and pile.location       = r.location
                            and pile.diff_id        = r.diff_id
                            and pile.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC);

      insert into rpm_missing_timeline (item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        zone_node_type,
                                        zone_id,
                                        cur_hier_level,
                                        table_missing_from)
      select distinct r.item,
             r.diff_id,
             r.item_parent,
             r.location,
             r.zone_node_type,
             r.zone_id,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC,
             'RPM_PROMO_ITEM_LOC_EXPL'
        from (select distinct s.item,
                     s.diff_id,
                     pile.location,
                     pile.zone_node_type,
                     s.item_parent,
                     pile.cur_hier_level,
                     s.zone_id
                from (select t.item,
                             im.item_parent,
                             im.diff_1 diff_id,
                             t.zone_id
                        from (select item,
                                     location zone_id
                                from (select item,
                                             location,
                                             ROW_NUMBER() OVER (PARTITION BY item,
                                                                             location
                                                                    ORDER BY promo_dtl_id) rnk
                                        from rpm_promo_item_loc_expl
                                       where dept           = rec.dept
                                         and class          = rec.class
                                         and subclass       = rec.subclass
                                         and cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_ZONE)
                               where rnk = 1) t,
                             item_master im
                       where im.item        = t.item
                         and im.item_parent is NOT NULL) s,
                     rpm_promo_item_loc_expl pile
               where pile.dept     = rec.dept
                 and pile.class    = rec.class
                 and pile.subclass = rec.subclass
                 and pile.item     = s.item_parent
                 and pile.zone_id  = s.zone_id
                 and (   (    pile.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                          and pile.diff_id        is NULL)
                      or (    pile.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                          and pile.diff_id        = s.diff_id))
                 and rownum        > 0) r,
             rpm_item_loc ril
       where ril.dept = rec.dept
         and ril.item = r.item
         and ril.loc  = r.location
         and NOT EXISTS (select 1
                           from rpm_promo_item_loc_expl pile
                          where pile.dept           = rec.dept
                            and pile.class          = rec.class
                            and pile.subclass       = rec.subclass
                            and pile.item           = r.item
                            and pile.location       = r.location
                            and pile.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC);

      -- Check for missing timelines on RPM_CUST_SEGMENT_PROMO_FR

      insert into rpm_missing_timeline (item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        zone_node_type,
                                        zone_id,
                                        cur_hier_level,
                                        table_missing_from)
      select distinct r.item,
             r.diff_id,
             NULL,
             r.location,
             r.zone_node_type,
             r.zone_id,
             RPM_CONSTANTS.FR_HIER_DIFF_LOC,
             'RPM_CUST_SEGMENT_PROMO_FR'
        from (select distinct t.item,
                     t.diff_id,
                     t.location zone_id,
                     cspfr.location,
                     t.zone_node_type
                from (select distinct t.item,
                             t.diff_id,
                             t.location,
                             t.zone_node_type
                        from rpm_cust_segment_promo_fr t,
                             item_master inner_im1
                       where t.dept             = rec.dept
                         and t.item             = inner_im1.item
                         and inner_im1.class    = rec.class
                         and inner_im1.subclass = rec.subclass
                         and t.cur_hier_level   = RPM_CONSTANTS.FR_HIER_DIFF_ZONE
                         and rownum             > 0) t,
                     rpm_cust_segment_promo_fr cspfr,
                     item_master inner_im2
               where cspfr.dept           = rec.dept
                 and cspfr.item           = inner_im2.item
                 and inner_im2.class      = rec.class
                 and inner_im2.subclass   = rec.subclass
                 and cspfr.item           = t.item
                 and cspfr.zone_id        = t.location
                 and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                 and rownum               > 0) r,
             item_master im,
             rpm_item_loc ril
       where im.item_parent = r.item
         and im.diff_1      = r.diff_id
         and ril.dept       = rec.dept
         and ril.item       = im.item
         and ril.loc        = r.location
         and NOT EXISTS (select 1
                           from rpm_cust_segment_promo_fr cspfr,
                                item_master im
                          where cspfr.dept           = rec.dept
                            and cspfr.item           = im.item
                            and im.class             = rec.class
                            and im.subclass          = rec.subclass
                            and cspfr.item           = r.item
                            and cspfr.location       = r.location
                            and cspfr.diff_id        = r.diff_id
                            and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC);

      insert into rpm_missing_timeline (item,
                                        diff_id,
                                        item_parent,
                                        location,
                                        zone_node_type,
                                        zone_id,
                                        cur_hier_level,
                                        table_missing_from)
      select distinct r.item,
             r.diff_id,
             r.item_parent,
             r.location,
             r.zone_node_type,
             r.zone_id,
             RPM_CONSTANTS.FR_HIER_ITEM_LOC,
             'RPM_CUST_SEGMENT_PROMO_FR'
        from (select distinct s.item,
                     s.diff_id,
                     cspfr.location,
                     cspfr.zone_node_type,
                     s.item_parent,
                     cspfr.cur_hier_level,
                     s.zone_id
                from (select t.item,
                             im.item_parent,
                             im.diff_1 diff_id,
                             t.zone_id
                        from (select item,
                                     location zone_id
                                from (select t.item,
                                             t.location,
                                             ROW_NUMBER() OVER (PARTITION BY t.item,
                                                                             t.location
                                                                    ORDER BY action_date) rnk
                                        from rpm_cust_segment_promo_fr t,
                                             item_master inner_im1
                                       where t.dept             = rec.dept
                                         and t.item             = inner_im1.item
                                         and inner_im1.class    = rec.class
                                         and inner_im1.subclass = rec.subclass
                                         and t.cur_hier_level   = RPM_CONSTANTS.FR_HIER_ITEM_ZONE)
                               where rnk = 1) t,
                             item_master im
                       where im.item        = t.item
                         and im.item_parent is NOT NULL) s,
                     rpm_cust_segment_promo_fr cspfr,
                     item_master inner_im2
               where cspfr.dept         = rec.dept
                 and cspfr.item         = inner_im2.item
                 and inner_im2.class    = rec.class
                 and inner_im2.subclass = rec.subclass
                 and cspfr.item         = s.item_parent
                 and cspfr.zone_id      = s.zone_id
                 and (   (    cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_PARENT_LOC
                          and cspfr.diff_id        is NULL)
                      or (    cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_DIFF_LOC
                          and cspfr.diff_id        = s.diff_id))
                 and rownum             > 0) r,
             rpm_item_loc ril
       where ril.dept = rec.dept
         and ril.item = r.item
         and ril.loc  = r.location
         and NOT EXISTS (select 1
                           from rpm_cust_segment_promo_fr cspfr,
                                item_master im
                          where cspfr.dept           = rec.dept
                            and cspfr.item           = im.item
                            and im.class             = rec.class
                            and im.subclass          = rec.subclass
                            and cspfr.item           = r.item
                            and cspfr.location       = r.location
                            and cspfr.cur_hier_level = RPM_CONSTANTS.FR_HIER_ITEM_LOC);

      commit;

   end loop;

   return 1;

EXCEPTION

   when OTHERS then
      O_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            L_program,
                                            TO_CHAR(SQLCODE));
      return 0;

END FIND_MISSING_FR_DATA;
---------------------------------------------------------------------------------
END RPM_DATA_VALIDATION_SQL;
/
